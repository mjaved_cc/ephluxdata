<?php
class UsersController extends AppController {

	var $name = 'Users';
    
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('registerTwitterUser');
	}

	function registerTwitterUser(){
		$this->twitterObj->setToken($_GET['oauth_token']);
		$token = $this->twitterObj->getAccessToken();
		$this->twitterObj->setToken($token->oauth_token, $token->oauth_token_secret);
		$twitterInfo= $this->twitterObj->get_accountVerify_credentials();
		$data = array();
		$data['twitter_id'] = $twitterInfo->id;		
		$id = $this->Auth->user('id');
		if(!isset($id) || $id == ''){
			$count = $this->User->find('count',array('conditions' => array('twitter_id' => $data['twitter_id'])));
			if($count<1){
				$this->User->create();
				$this->User->save($data);
				$this->Auth->login($this->User->id);
			}else{
				$data = $this->User->find('first',array('conditions' => array('twitter_id' => $data['twitter_id'])));
				$this->Auth->login($data['User']['id']);
			}
		}else{
			$data['id'] = $this->Auth->user('id');
			$this->User->save($data);
			$this->Auth->login($data['id']);
		}
		$this->redirect(array('controller'=>'twitters','action'=>'home'));
	}
}