<?php

App::uses('OAuth', 'Lib/Twitter');
App::uses('TwitterOAuth', 'Lib/Twitter');

class TwitterComponent extends Component {

    private $consumer_key;
    private $consumer_key_secret;
    private $requesttokenurl;
    private $accesstokenurl;
    private $authurl;
    public $oauth_token;
    public $oauth_token_secret;
    public $twitter_obj;
    public $site_base_url;

    // if user wants to know its own stats - registered users

    const TWITTER_USER_DETAILS = 'https://api.twitter.com/1/account/verify_credentials.json';
    const TWITTER_RETWEETS_BY_ME = 'https://api.twitter.com/1/statuses/retweets_of_me.json?trim_user=1&count=100';
    const TWITTER_MENTIONS = 'https://api.twitter.com/1/statuses/mentions.json?trim_user=1&include_rts=1&count=100';
    const TWITTER_SEARCH_USERS = 'https://api.twitter.com/1/users/search.json?q=KEYWORD&per_page=20';

    // if third party stats is concerned - non registered users
    const TWITTER_USER_LOOKUP = 'https://api.twitter.com/1/users/lookup.json?screen_name=SCREEN_NAME';
    const TWITTER_RETWEETS_BY_USER = 'https://api.twitter.com/1/statuses/retweeted_by_user.json?screen_name=SCREEN_NAME&count=100&trim_user=1';

    // here trailing slash is explicitly given otherwise through exception 'Invalid protected resource url, unable to generate signature base string' ...  Reference URL: http://www.lornajane.net/posts/2011/invalid-protected-resource-url-in-pecl_oauth
    const TWITTER_MENTIONS_BY_SCREEN_NAME = 'http://search.twitter.com/search.json?q=%40SCREEN_NAME&since=SINCE/';

    function startup(Controller $controller) {

        Configure::load('twitter');
        $consumer_key = Configure::read('Twitt.CONSUMER_KEY');
        $consumer_secret = Configure::read('Twitt.CONSUMER_SECRET');

        $this->consumer_key = $consumer_key;
        $this->consumer_key_secret = $consumer_secret;
        $this->requesttokenurl = "https://api.twitter.com/oauth/request_token";
        $this->accesstokenurl = "https://api.twitter.com/oauth/access_token";
        $this->authurl = "https://api.twitter.com/oauth/authorize";

        $this->site_base_url = SMC_BASE_URI;
        $this->twitter_obj = new TwitterOAuth($this->consumer_key, $this->consumer_key_secret);
    }

    function redirect() {
        $request_token = $this->twitter_obj->getRequestToken();

        /* Save temporary credentials to session. */
        $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

        /* If last connection failed don't display authorization link. */
        switch ($this->twitter_obj->http_code) {
            case 200:
                /* Build authorize URL and redirect user to Twitter. */
                $url = $this->twitter_obj->getAuthorizeURL($token);
                header('Location: ' . $url);
                break;
            default:
                /* Show notification if something went wrong. */
                echo 'Could not connect to Twitter. Refresh the page or try again later.';
        }
    }

    function callback() {
        session_start();

        /* If the oauth_token is old redirect to the connect page. */
        if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
            $_SESSION['oauth_status'] = 'oldtoken';
        }

        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $this->twitter_obj = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

        /* Request access tokens from twitter */
        $access_token = $this->twitter_obj->getAccessToken($_REQUEST['oauth_verifier']);

        /* Save the access tokens. Normally these would be saved in a database for future use. */
        $_SESSION['access_token'] = $access_token;

        /* Remove no longer needed request tokens */
        unset($_SESSION['oauth_token']);
        unset($_SESSION['oauth_token_secret']);

        /* If HTTP response is 200 continue otherwise send to connect page to retry */
        if (200 == $this->twitter_obj->http_code) {
            /* The user has been verified and the access tokens can be saved for future use */
            $_SESSION['status'] = 'verified';
            
        }
    }

}

?>