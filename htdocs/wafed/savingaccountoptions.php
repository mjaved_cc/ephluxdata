<div id="account_content">
	<div id="account_sidebar" style="width: 275px;">
		<div class="sidebar_shadow">
			<img src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>
		<img src="/wafed/sites/all/themes/WAFED/graphics/sidePic_dreamBig.jpg" alt="Dream Big, Start small. The sooner you start saving the closer you are to your goal. Check out some of our savings accounts that can help you get there."  />
		</br><a href="/wafed/fees">View Fees</a>
        <a href="/wafed/wafedrates">View Rates</a>
	</div>
	<div id="AccountOptions_main">
		<h2>Savings Account Options</h2>
        <p>Whatever you <b>imagine</b>, you can make it happen. Select from our smart Savings Account options to start saving today, for a great tomorrow.  </p>
        <table id="AccountOptions" style="width: 655px;">
			<thead>
				<tr>
					<th class="firstCol" style="font-weight:normal;">Product</th>
                    <th>Minimum to Open</th>
                    <th>Minimum to Earn Interest</th>
                    <th class="lastCol">Account Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="firstCol">Interest Savings<br><a href="/wafed/personalbanking/savings/interestsavings">Details</a></td>
                    <td>$50</td>
                    <td>$50</td>
                    <td class="lastCol">No deposit limitations exist.</td>
                </tr>
                <tr>
                    <td class="firstCol">Money Market Account<br><a href="/wafed/personalbanking/savings/moneymarketaccount">Details</a></td>
                    <td>$1000</td>
                    <td>$250</td>
                    <td class="lastCol">Maintain a $250 Balance and avoid a service charge.</td>
                </tr>

                <tr>
                    <td class="firstCol">Holiday Club<br><a href="/wafed/holidayclubaccounts">Details</a></td>
                    <td>$10</td>
                    <td>NA</td>
                    <td class="lastCol">No monthly service charge.</td>
                </tr>
                <tr>
                    <td class="firstCol">Holiday Club w Interest<br><a href="/wafed/holidayclubaccounts">Details</a></td>
                    <td>$10</td>
                    <td>$10</td>
                    <td class="lastCol">No monthly service charge.</td>
                </tr>
            </tbody>
       	</table>
		<div id="accountLocation">
			<p><a href="/wafed/locations">Find a location</a> near you to open your Holiday Club Account today and start saving for tomorrow. </p>
		</div>
		<div style="clear:both;"></div>
	</div>
