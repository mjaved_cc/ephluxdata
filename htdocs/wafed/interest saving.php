
<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="400" width="12" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>

        <div id="info_area">
			</br><a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a>
        </div>
	</div>
	<div id="wafed_main">
		<h2>Interest Savings</h2>
		<p>Earn a great rate on your account with Interest Savings from Washington Federal Bank for Savings. Plus with our new online banking features, you can manage your account from the <b>convenience</b> of home. Check out some of the features you get when you open an Interest Savings Account with us below. </p>
        <div id="features"><strong>Your Interest Savings Account features:</strong></div>
        <ul>
			<li>Only a $50 balance to open and earn Interest</li>
			<li>No deposit limitations</li>
			<li>No pre-authorized or automatic electronic funds transfer debits are permitted</li>
			<li>Option of Passbook or Monthly statements</li>
        </ul></br>
		<a href="/wafed/contactus">Contact us</a> for a complete list of terms and conditions.

		<div id="side_rates">
			<h3 style="padding: 20px;">Current Interest Rate*</h3>
            <div id="rate">.XX%</div>
            <div id="effective" style="padding: 20px;">Effective May XX, 2012</div>
        </div>
	</div>
    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to open your Interest Savings account today. </p>
    </div>
    <div style="clear:both;"></div>
</div>