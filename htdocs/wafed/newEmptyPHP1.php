<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="400" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>


		<div id="info_area">
			</br><a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a>
		</div>

	</div>
	<div id="wafed_main">
		<h2>Money Market Account</h2>
		<p>Get the <b>flexibility</b> of a check card and still earn a higher rate than traditional savings.</p>
        <div id="features"><strong>Your Money Market Account features:</strong></div>
        <ul>
			<li>$1000 minimum to open </li>
			<li>Only a $250 balance to earn Interest</li>
			<li>Interest Compounded Monthly</li>
        </ul>

		<div id="side_rates">
			<h3 style="padding: 20px;">Current Interest Rate*</h3>
            <div id="rate">.XX%</div>
            <div id="effective" style="padding: 20px;">Effective May XX, 2012</div>
        </div>


	</div>



    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to start saving today. </p>
    </div>
    <div style="clear:both;"></div>


</div>
