<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="400" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>

<img src="/wafed/sites/all/themes/WAFED/graphics/checkin-options-sidebar.jpg" alt="Checking In.
Online or in store. Access to your checking account has never been easier.">
        
        	<p class="fee_rates_links"><a href="/wafed/fees">View Fees</a>
        <a href="/wafed/wafedrates">View Rates</a>
        </p>
	</div>
	<div id="wafed_main">
		<h2>Interest Checking</h2>
		<p>Earn a great rate on your checking account with Interest Checking from Washington Federal Bank for Savings. Now you can enjoy the benefits of a checking account that pays you. </p>
        <div id="features"><strong>Your Interest Checking account features:</strong></div>
        <ul>
        <li>$250 to open</li>
		<li>$500 Balance to earn Interest</li>
		<li>Waived $10 monthly service charge when you keep a $250 Balance</li>
        <li>Free online banking</li>
        <li>Free bill pay</li>
        <li>Free telephone banking</li>
        <li>Free Debit Card</li>
        <li>Free Notary</li>
        </ul>
	</div>
    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to open your Interest Checking account today. </p>
<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/wafed/fees">Complete Terms and Conditions</a>
    </div>
    <div style="clear:both;"></div>
</div>
