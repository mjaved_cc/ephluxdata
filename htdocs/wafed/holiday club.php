<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="500px" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>
		<!--        <div id="side_rates">
					<h3>Current Checking Rate</h3>
					<div id="rate">.XX%</div>
					<div id="effective">Effective May XX, 2012</div>
				</div>-->
        
		<img src="/wafed/sites/all/themes/WAFED/imgs/holidayclub_options-sidebar.jpg" style="margin-left: -7px;" />
		<div id="info_area"></br><a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a></div>
	</div>
	<div id="wafed_main">
		<h2>Holiday Club Accounts</h2>
		<p>‘Tis the season to save for holiday expenses! Save year round so holiday spending doesn’t turn you into a grinch. Open a Holiday Club account, it’s a present to yourself. We have two options for a Holiday Club Account that can help you save. </p> 
        <div id="holiday_club_info">
			<ul class="rightCol">
                <h3>Non-Interest Bearing</h3>
                <li>Only a $10 balance to open</li>
                <li>No deposit limitations</li>
            </ul>
            <ul class="leftCol">
                <h3>Interest Bearing</h3>
                <li>Only a $10 balance to open and earn Interest</li>
                <li>No deposit limitations</li>
            </ul>

			<div id="side_rates">
				<h3 style="padding: 20px;">Current Interest Rate*</h3>
				<div id="rate">.XX%</div>
				<div id="effective" style="padding: 20px;">Effective May XX, 2012</div>
			</div>

        </div>
	</div>
    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to open your Holiday Club account today. </p>
    </div>
    <div style="clear:both;"></div>
</div>