<link rel="stylesheet" href="/wafed/business.css">
<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>
<!--        <div id="side_rates">
        	<h3>Current Interest Rate*</h3>
            <div id="rate">.XX%</div>
            <div id="effective">Effective May XX, 2012</div>
        </div>-->
        <img src="/wafed/sites/all/themes/WAFED/imgs/wf_biz_chk2_sidebar.jpg" style="margin-top: 15px; margin-left: 10px;" />
		
		<p class="fee_rates_links">
			<a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a>
		</p>
		
	</div>
	<div id="wafed_main">
		<h2>Business Checking with Interest</h2>
		<p>With an Interest Bearing Checking Account from Washington Federal Bank for Savings you can earn a great rate on your funds. What does that say about your bottom line? To learn more check out the details below or stop in and chat with a representitive to see how they can help you <b>grow</b>.</p> 
        <div id="features"><strong>Your Interest Bearing Checking account features:</strong></div>
        <ul>
        <li>Only $250 Minimum balance to open the account </li>
		<li>Only $500 balance to earn Interest</li>
		<li>Waived $10 monthly service charge when you keep a $250 balance</li>
        <li>Free online banking</li>
        <li>Free bill pay</li>
        <li>Free telephone banking</li>
        <li>Free Debit Card</li>
        </ul>
		
		
		<div id="side_rates">
				<h3 style="padding: 20px;">Current Interest Rate*</h3>
				<div id="rate">.XX%</div>
				<div id="effective" style="padding: 20px;">Effective May XX, 2012</div>
			</div>
		
    
	</div>
    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to speak with a Loan Officer today. </p>
    </div>
    <div style="clear:both;"></div>
</div>