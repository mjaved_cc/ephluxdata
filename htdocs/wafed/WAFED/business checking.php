<link rel="stylesheet" href="/wafed/business.css">
<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="500" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>
        <a href="/wafed/contact"><img src="/wafed/sites/all/themes/WAFED/imgs/wf_biz_chk2_sidebar.jpg" /></a>
		
		<p class="fee_rates_links" style="margin-top: 10px;">
			<a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a>
		</p>
		
	</div>
	<div id="wafed_main">
		<h2>Business Checking</h2>
		<p>With a Business Checking Account from Washington Federal Bank for Savings you get No Monthly Service charges* and with a low minimum balance of only $250 you <b>relax</b>, knowing that your bottom line is top of mind with us. Plus with our new online features you can manage your account from the office. How easy is that?</p>
        <div id="features"><strong>Your Business Checking Account features:</strong></div>
        <ul>
        <li>$250 to open</li>
		<li>Waived $10 monthly service charge when you keep a $250 Balance</li>
		<li>Free online banking</li>
        <li>Free bill pay</li>
        <li>Free telephone banking</li>
        <li>Free Debit Card</li>
        </ul>
	</div>

  <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to open a Business Checking Account Today!</p>
    </div>

    
    <div style="clear:both;"></div>
</div>