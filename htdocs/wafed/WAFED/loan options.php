<div id="poc_content">
	<div id="poc_sidebar">
		<div class="sidebar_shadow">
			<img height="900" width="12" src="/wafed/sites/all/themes/WAFED/graphics/sidebar_shadow.jpg" />
		</div>
        <img src="/wafed/sites/all/themes/WAFED/imgs/loan_options_sidebar.png" alt="We know your goals are as unique as you are. That's why Washington Federal Bank for Savings offers a variety of lending solutions and loans. So Whether you're buying a home or paying for an education, we can help. Contact Us." />

		<p class="fee_rates_links">
			<a href="/wafed/fees">View Fees</a>
			<a href="/wafed/wafedrates">View Rates</a>
		</p>
	</div>
	<div>
        <table id="AccountOptions" style="width: 605px;">
			<thead>
				<tr>
					<th class="firstCol" style="font-weight:normal;">Product</th>
                    <th class="lastCol">Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="firstCol">Fixed Rate Loan<br><a href="/wafed/wafedrates">Rate</a></td>
                    <td class="lastCol">
						<ul>
							<li>• 10 and 15 year fixed mortgages with no points </li>
							<li>• New purchases require 20% down </li>
							<li>• A $350 non-refundable fee is required at time of application </li>
							<li>• Other fees and conditions may apply </li>

						</ul>
                    </td>
                </tr>

                <tr>
                    <td class="firstCol">Refinance<br>
						<div id="side_rates">
							<h3>Current Interest Rate*</h3>
							<div id="rate">.XX%</div>
							<div id="effective">Effective May XX, 2012</div>
						</div></td>
                    <td class="lastCol"><ul>
							<li>Contact our <a href="/wafed/contact">Lending Department</a> for refinance options <br>(773) 254-3422 
							</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="firstCol">Construction Loan<br>
						<div id="side_rates">
							<h3>Current Interest Rate*</h3>
							<div id="rate">.XX%</div>
							<div id="effective">Effective May XX, 2012</div>
						</div></td>
                    <td class="lastCol"><ul>
							<li>• 1-4 properties only</li>
							<li>• 12 month terms </li>
							<li>• Fixed interest rate (based on prime + 2%) for duration of loan </li>
							<li>• A $350 non-refundable fee is required at time of application </li>
							<li>• Other fees and conditions may apply </li>
							<li>•Contact our <a href="/wafed/contact">Lending Department</a> for refinance options <br>(773) 254-3422 
							</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="firstCol">Multi Family Loans<br>
						<div id="side_rates">
							<h3>Current Interest Rate*</h3>
							<div id="rate">.XX%</div>
							<div id="effective">Effective May XX, 2012</div>
						</div></td>
                    <td class="lastCol"><ul>
							<li>• Up to 4 unit properties only </li>
							<li>• All other terms comparable to our other residential mortgage products </li>
							<li>• A $550 non-refundable fee is required at time of application</li>
							<li>• Other fees and conditions may apply </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="firstCol">Loan on Deposit<br>
						<div id="side_rates">
							<h3>Current Interest Rate*</h3>
							<div id="rate">.XX%</div>
							<div id="effective">Effective May XX, 2012</div>
						</div></td>
                    <td class="lastCol"><ul>
							<li>• Loan up to 90% of deposit account </li>
							<li>• Interest rate is 4% above stated passbook/CD rate </li>
							<li>• Loan term not to exceed 5 years; loans on CDs mature on CD maturity date</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="firstCol">Home Equity Loan<br>
						<div id="side_rates">
							<h3>Current Interest Rate*</h3>
							<div id="rate">.XX%</div>
							<div id="effective">Effective May XX, 2012</div>
						</div></td>
                    <td class="lastCol"><ul>
							<li>• Loans up to 80% of home value </li>
							<li>• 5 year maximum term </li>
							<li>• Fixed interest rate  </li>
							<li>• A $175 non-refundable fee is required at time of application</li>
							<li>• Other fees and conditions may apply </li>
                        </ul>
                    </td>
                </tr>
            </tbody>
       	</table>
	</div>
    <div id="accountLocation">
        <p><a href="/wafed/locations">Find a location</a> near you to speak with a Loan Officer today. </p>
    </div>
    <div style="clear:both;"></div>
</div>
