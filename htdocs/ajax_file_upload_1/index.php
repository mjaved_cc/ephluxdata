<?php
require_once('image_resize.php');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
die('heere');
// Access the $_FILES global variable for this specific file being uploaded
// and create local PHP variables from the $_FILES array of information
	$fileName = $_FILES["uploaded_file"]["name"]; // The file name
	$fileTmpLoc = $_FILES["uploaded_file"]["tmp_name"]; // File in the PHP tmp folder
// Place it into your "uploads" folder mow using the move_uploaded_file() function
	$file_path = "uploads/$fileName";
	$moveResult = move_uploaded_file($fileTmpLoc, $file_path);
// Check to make sure the move result is true before continuing
	if ($moveResult == true) {
		echo "<div id='filename'>$fileName</div>";
	}

	$resizeObj = new resize($file_path);
	$thumbnail_path = "thumbnail/$fileName";

	$flag = $resizeObj->resizeImage(80,100, 'crop');
	$is_saved = $resizeObj->saveImage($thumbnail_path, 100);
	$success = $flag && $is_saved;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Ajax File Upload</title>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {

				//if submit button is clicked
				$('form#upload').submit(function(){  

					$('#upload_wrapper').hide();
					$('#loading').show();
		
					// get the uploaded file name from the iframe
					$('#upload_target').unbind().load( function(){								
						var img = $('#upload_target').contents().find('#filename').html();
		
						$('#loading').hide();

						// load to preview image
						if (img){
							$('#preview').show();
							$('#preview').attr('src', 'uploads/'+img);
							$('#image_wrapper').show();
						}

					});
				});
			});
		</script>
	</head>
	<body>
		<div id="upload_wrapper">
			<form id="upload" name="upload" enctype="multipart/form-data" method="post" action="index.php" target="upload_target">
				<input name="uploaded_file" type="file" size="30" id="uploaded_file"  />
				<input id="sent" name="sent" type="submit"  value="Upload" />
			</form>
		</div>
		<div id="loading" style="background:url(ajax-loader.gif) no-repeat left; height:50px; width:370px; display:none;">
			<p style="margin-left:40px; padding-top:15px;">Uploading File... Please wait</p>
		</div>
		<div id="image_wrapper" style="display:none;"><img id="preview" src="" /></div>
		<iframe id="upload_target" name="upload_target" style="width:10px; height:10px; display:none"></iframe>
	</body>
</html>