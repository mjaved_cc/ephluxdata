CompanyID|Company|Address_1|Address_2|Address_3|City|Country|Postcode|Phone|Fax|Email|CompanyURL
18|Nintendo|The Quadrant|55-57 High Street||Windsor|UK|SL4 1LP|01753 483727|01753 864710 ||www.nintendo.co.uk
20|Flight1|PO Box 51137|||Sparks|USA|NV 89434||||www.flight1.com
21|Pinnacle|Heather Court|6 Maidstone Road||Sidcup|UK|DA14 5HH|020 8309 3600 |020 8309 3911 |info@pinnacle-software.co.uk|www.pinnacle-software.co.uk
22|Ascaron|St. Peters House|Church Hill||Coleshill|UK|B46 3AL|01675 433220|||www.ascaron.com
23|JoWooD|Pyhrnstra�e 40|||A-8940 Liezen|Austria||+43 3612 2828 - 0|+43 3612 2828 - 1064|office@jowood.com|www.jowood.com
24|Square Enix|2nd Floor|Castle House|37-45 Paul Street|London|UK|EC2A 4LS|020 7324 5200|||www.square-enix.com/eu/en
25|Kalypso|Imperial House|St. Nicholas Circle||Leicester|UK|LE1 4LF|0116 242 4130|0116 242 4139|info@kalypsomedia.co.uk|www.kalypsomedia.com
26|South Peak||||||||||www.southpeakgames.com
27|Eidos|Wimbledon Bridge House|1 Hartfield Road||London|UK|SW19 3RU|020 8636 3000|020 8636 3001||www.eidosinteractive.co.uk
28|DTP||||||||||
29|Anaconda||||||||||
30|Mekensleep||||||||||
31|Warner Bros.||||||||||
32|Ivolgamus|Savanoriu 61|||Vilnius 03149|Lithuania||+370-5 265-34-16|+370-5 233-58-30|info@ivolgamus.com|www.ivolgamus.com
33|One To Another||||||||||
35|Lighthouse Interactive|Robert Kochlaan 330|||2035 BK Haarlem|The Netherlands||+31 23 540 9 449|+31 23 540 1 153||www.lighthouse-interactive.com
36|City Interactive|CBX II|West Wing|282-390 Midsummer Boulevard|Milton Keynes|UK|MK9 2RG|||info@city-interactive.com|www.city-interactive.com
37|Lexicon Entertainment|15 Falmouth Road|||Sheffield|UK|S7 2DG|||distribution@lexiconentertainment.com|www.lexiconentertainment.com
38|The Game Factory|3000 Hillswood Drive|||Chertsey|UK|KT16 ORS|0870 3514352|0870 3514383||gamefactorygames.eu
40|Meridian4|124 Meridian|Kirkland||Quebec|Canada|H9H 4A3||||www.meridian4.com
41|Take 2 Interactive|Saxon House|2-4 Victoria Street||Windsor|UK|SL4 1EN|01753 496600|||www.take2games.co.uk
42|Rockstar||||||||||
43|Konami|389 Chiswick High Road|||London|UK|W4 4AL|020 8987 5730|020 8987 5731||uk.gs.konami-europe.com/home.do
44|Oxygen|17 Bridle Close|Finedon Road Industrial Estate||Wellingborough|UK|NN8 4RN|01933 442660|01933 446420||www.oxygengames.net
45|Codemasters|PO Box 6|||Leamington Spa|UK|CV47 2ZT|01926 814132|01926 817595||www.codemasters.co.uk
46|Capcom Entertainment|26-28 Hammersmith Grove|||London|UK|W6 7HA|||feedback@capcomeuro.com|www.capcom-europe.com
47|Majesco||||||||||
49|Razorback||||||||||
50|Backbone||||||||||
51|Avalanche||||||||||
52|Funcom||||||||||
53|Widescreen||||||||||
54|Brash Entertainment||||||||||
55|Midway|43 Worship Street|||London|UK|EC2A 2DX|020 7382 7720|||www.midway.com
57|Trilogy Logistics|Salthouse Road|Brackmills Industrial Estate||Northampton|UK|NN4 7BD|0845 4566400|0845 3303086|enquiries@trilogy-uk.com|www.trilogy-uk.com
58|Ubisoft|First Floor|Chertsey Gate East |London Street|Chertsey|UK|KT16 8AP|01932 578000 |01932 578001||www.ubi.com/UK/
59|Magic Pockets||||||||||
60|Free Radical||||||||||
61|Electronic Arts|Onslow House|Onslow Street||Guildford|UK|GU1 4TN||||www.electronicarts.co.uk
62|Harmonix||||||||||
63|BioWare||||||||||
64|Sega|27 Great West Road|||Brentford|UK|TW8 9BW||||www.sega.co.uk
65|Centresoft|6 Pavilion Drive|Holford||Birmingham|UK|B6 7BB|0121 625 3388|0121 625 3236 |sales@centresoft.co.uk|www.centresoft.co.uk
66|Criterion||||||||||
67|Petroglyph||||||||||
68|Zoo Digital|Arundel Court|177 Arundel Street||Sheffield|UK|S1 2NU|0114 263 6030|0114 263 6031|info@zoodigitalpublishing.com|www.zoodigitalpublishing.com
69|505 Games|Suite 366|Silbury Court|Silbury Boulevard|Milton Keynes|UK|MK9 2AF|01908 607772|01908 607062||www.505games.co.uk/default.aspx
70|Advantage||||||||||
71|Sony Computer Entertainment|10 Great Marlborough Street|||London|UK|W1F 7LP||||uk.playstation.com
73|Success||||||||||
74|D3Publisher|Poseidon House|Castle Park||Cambridge|UK|CB3 0RD|||info@d3p.co.uk|www.d3p.co.uk
75|Bizarre Creations||||||||||
77|Rising Star|Basepoint Business & Innovation Centre|Butterfield||Luton|UK|LU2 8DL|01582 433700||richard.barclay@risingstargames.com|www.risingstargames.com
79|Office Create||||||||||
80|Disney Interactive|3 Queen Caroline Street|||London|UK|W6 9PE||||home.disney.co.uk/games/
81|Atari|||||||||uk.info@atari.com|www.uk.atari.com
82|Koch Media|The Bull's Pen|Manor Court|Scratchface Lane|Herriard||RG25 2PH |0870 0270985 |0870 0270978 |ukoffice@kochmedia.com|www.kochmedia.co.uk
84|THQ|Duke's Court|Duke Street||Woking||GU21 5BH||||www.thq-games.com/uk
85|ArcSystem||||||||||
86|Microsoft|Microsoft Campus|Thames Valley Park||Reading|UK|RG6 1WG|0870 6010100|0870 6020100||www.microsoft.com/uk/games
87|Gem|St. George House|Parkway|Harlow Business Park|Harlow|UK|CM19 5QF|01279 822800|01279 822822|marketing@gem.co.uk|www.gem.co.uk
88|Mistwalker||||||||||
89|Koei|Unit 209a|The Spirella Building|Bridge Road|Letchworth|UK|SG6 4ET|01462 476130|01462 476135||www.koei.co.uk
90|Dimple||||||||||
91|Activision|Parliament House|St. Laurence Way||Slough|UK|SL1 2BW|01753 756100|||www.activision.com
92|Relentless Software||||||||||
93|Empire Interactive|The Spires|677 High Road||London|UK|N12 0DA|020 8343 7337|020 8343 7447|csales@empire.co.uk|www.empireinteractive.com
94|Blast!|1st Floor|1 Benjamin Street||London|UK|EC1M 5QG|0845 234 4242||support@blast-games.com|www.blast-games.com/en/default.asp
95|Vivendi|PO Box 2510|||Reading|UK|RG2 0ZJ||||
96|Just Flight|2 Stonehill|Stukeley Meadows||Huntingdon|UK|PE29 6ED|0845 2342471 |||www.justflight.com
97|Xplosiv||||||||||www.xplosiv.net
98|Clap Hanz||||||||||
99|Mastertronic|2 Stonehill|Stukely Meadows||Huntingdon|UK|PE29 6ED||0845 2344243||www.mastertronic.com
103|Camelot||||||||||
104|Banpresto||||||||||
105|Media.Vision||||||||||
106|Data Design Interactive|Warwick House|159 Lower High Street||Stourbridge|UK|DY8 1TS|01384 447900||admin@datadesign.uk.com|www.ddi-games.com
107|Hyper Devbox||||||||||
108|Beeworks||||||||||
109|Pronto Games||||||||||
110|Apeiron||||||||||
111|SkyRiver Studios||||||||||
112|X-Bow Software||||||||||
113|Best Way||||||||||
114|Arise||||||||||
115|Gaijin Entertainment||||||||||
116|Ciroland||||||||||
117|Electronic Arts||||||||||
118|Namco Bandai||||||||||
119|Epic Games||||||||||
120|Microsoft||||||||||
121|Team Ninja||||||||||
122|Topware||||||||||
123|Kerberos||||||||||
124|Mistic Software||||||||||
125|Tragnarion|C/Sargento Gracia Armengol N 10|||07007 Palma de Mallorca|Spain||||info@tragnarion.com|www.tragnarion.com
126|Beautiful Game||||||||||
127|Spike Co.||||||||||
128|IO Interactive||||||||||
129|Pivotal||||||||||
130|Deep Silver|The Bull's Pen|Manor Court|Scratchface Lane|Herriard||RG25 2PH|01256 707767|01256 707277|info@deepsilver.co.uk|www.deepsilver.co.uk
131|Jester Interactive||||||||||www.jesterinteractive.co.uk
132|Bungie Software||||||||||
133|Leftfield||||||||||
134|Plato||||||||||
135|Telltale Games||||||||||
136|Blue Byte||||||||||
137|Wizarbox||||||||||
138|Nival Interactive||||||||||
139|GameLoft||||||||||
140|So! Games||||||||||
141|Sogoplay||||||||||
142|Midas Interactive|Unit 14|Stansted Distribution Centre|Start Hill|Bishops Stortford|UK|CM22 7DG||||www.midasinteractive.com
143|StarFish||||||||||
144|Sold Out|2 Stonehill|Stukely Meadows||Huntingdon|UK|PE29 6ED||||www.mastertronic.com
145|Kuju Entertainment||||||||||
146|High Moon||||||||||
147|Crystal Dynamics||||||||||
148|Coyote Console||||||||||
149|Collision||||||||||
150|Neko||||||||||
151|LucasArts||||||||||
152|Gamecock||||||||||
153|ZeniMax|1370 Piccard Drive|Suite 120||Rockville|USA|MD 20850||||www.zenimax.com
154|Paradox Interactive|Sveav�gen 9-13, Plan 6|||101 52 Stockholm|Sweden||+46 (0) 8 566 148 00|+46 (0) 8 566 148 19||www.paradoxplaza.com
155|Mindscape|73-77 Rue de Sevres|||92514 Boulogne Cedex|France|||||www.mindscape.co.uk
156|Europress||||||||||
157|Valve||||||||||
158|Gearbox Software||||||||||
159|First Class Simulations|PO Box 586|||Banbury|UK|OX16 6BY|01869 338428|||www.firstclass-simulations.com
160|Relic Entertainment||||||||||
161|Volition||||||||||
162|Climax||||||||||
163|Ghostlight|Unit 14|Stansted Distribution Centre|Start Hill|Bishops Stortford|UK|CM22 7DG||||www.ghostlight.uk.com
165|Magenta||||||||||
166|Taito||||||||||
167|Eurocom||||||||||
169|Namco||||||||||
170|Bethesda||||||||||
171|Bethesda||||||||||
172|GSP Software|Meadow Lane|||St. Ives|UK|PE27 4LG||||www.avanquest.com/UK
173|Silicon Knights||||||||||
174|Red Octane||||||||||
175|Saber Interactive||||||||||
176|Sierra Entertainment||||||||||
177|Shin'en||||||||||
178|Two Tribes||||||||||
179|DC Studios||||||||||
180|BHV|Novesiastr. 60|||D-41564 Kaarst|Germany||||info@bhv.de|www.bhv.de
181|Frogster|Bleibtreustr. 38|||10623 Berlin|Germany||||info@fip-publishing.de|www.fip-publishing.de
182|In2Games|2nd Floor|135 High Street||Rickmansworth|UK|WD3 1AR||||www.in2games.uk.com
183|Sniper Entertainment|||||||||international@sniper.fr|www.sniper.fr
184|Playlogic|World Trade Centre|C-Tower 10th Floor|Strawinskylaan 1041|1077 XX Amsterdam|The Netherlands||+31 (0) 20 676 03 04|+31 (0) 20 673 17 13 |info@playlogicint.com|www.playlogicinternational.com
185|Virgin Play|Paseo de la Castellana|9-11 Planta Baja||28046 Madrid|Spain||||virginplay@virginplay.es|www.virginplay.es
186|Sony Online Entertainment|8928 Terman Court|||San Diego|USA|California||||www.soepress.com
187|Warner Bros. Interactive Entertainment|4000 Warner Boulevard|||Burbank|USA|CA 91522||||www.wbie.com
188|Blizzard||||||||||
189|Stormregion||||||||||
190|Maxis||||||||||
193|Crytek||||||||||
194|Focus Multimedia|The Studios|Lea Hall Enterprise Park|Wheelhouse Road|Rugeley|UK|WS15 1LH||||www.focusmm.co.uk
195|Technicolor|Chiswick Park|566 Chiswick High Road||London|UK|W4 5AN||020 8100 1001||technicolornetworkservices.com
196|Black Rock||||||||||
197|Artificial Mind & Movement||||||||||
198|Fall Line||||||||||
199|Altron||||||||||
200|Propaganda||||||||||
201|Avalon Style Entertainment||||||||||
202|1C Company||||||||||
203|Bohemia Interactive||||||||||www.bistudio.com
204|Jaleco||||||||||
205|Point of View||||||||||
206|Action Forms||||||||||
207|SkyFallen Entertainment||||||||||
208|Creative Patterns||||||||||
209|The Farm 51||||||||||
210|Dreams||||||||||
211|SonicPowered||||||||||
212|Sting||||||||||
213|DEL||||||||||
215|Ninja Theory||||||||||
216|Naughty Dog||||||||||
217|Incognito Entertainment||||||||||
218|Factor 5||||||||||
219|Game Republic||||||||||
220|Insomniac Games||||||||||
221|Lionhead||||||||||
222|Firaxis Games||||||||||
223|Excalibur|PO Box 586|||Banbury|UK|OX16 6BY|||info@excalibur-publishing.co.uk|www.excalibur-publishing.co.uk
224|SCS||||||||||
225|Starbreeze||||||||||
226|Black Bean|Via Adua, 22|||21045 Gazzada|Italy|||||www.blackbeangames.com
227|Akella||||||||||
228|Pandemic||||||||||
229|Digital Illusions||||||||||
230|Cat Daddy||||||||||
231|Aerosoft||||||||||
232|Contact Sales|PO Box 586|||Banbury|UK|OX16 6BY|01869 338833|0870 1321026 |robert@contact-sales.co.uk|www.contact-sales.co.uk
233|Take Interactive||||||||||
235|Tantalus||||||||||
236|System 3|9-10 Grafton Street|||London|United Kingdom|W1S 4EN|||press@system3.com|www.system3.com
237|Firebrand||||||||||
238|Eutechnyx||||||||||
242|PlayV|210a Ground Floor|The Village|Butterfield|Great Marlings|UK|LU2 8DL|||info@playv.co.uk|www.playv.co.uk
243|Treasure||||||||||
247|Rare||||||||||
248|WXP||||||||||
249|Krome||||||||||
250|Zoe Mode||||||||||
252|Blueside||||||||||
253|Ensemble Studios||||||||||
254|Big Huge Games||||||||||
255|Aces Studio||||||||||
256|Blue Fang||||||||||
257|Graphsim||||||||||
258|Avanquest||||||||||
259|Phoenix||||||||||
262|NCSoft|Fifth Floor|Mocatta House|Trafalgar Place|Brighton|United Kingdom|BN1 4DU|+44-0-1273-872000||dblundell@ncsoft.com|www.ncsoft.net
265|Introversion Software||||||||||
266|Strawdog Studios|Strawdog Studios|The ID Centre, Lathkill House|The RTC Business Park, London Road|Derby||DE24 8UP|01332258862|||http://www.strawdogstudios.com
267|Asylum|300 Upper Street|Islington||London|United Kingdom|N1 2TU|020 7354 7303|020 7354 7304|info@asylum-entertainment.com|www.asylum-entertainment.com
268|Sports Director|||||||||contact@sportsdirector.co.uk|http://www.sportsdirector.co.uk/
269|Gas Powered Games||||||||||
270|Games 4 All||||||||||
271|Eversim|||||||||press@eversim.com|www.eversim.com
272|Just Trains|2 Stonehill|Stukeley Meadows|Huntingdon|Cambs|United Kingdom|PE29 6ED|0845 2342471 |08452342472|enquiries@justflight.com|http://www.justtrains.net/
273|Humanhead||||||||||
274|Tecmo||||||||||
275|Ignition Entertainment|168-172 Brooker Road|Walktham Abbey||Essex|United Kingdom|EN9 1JH|01992703970|01992703971|press@ignitionent.com|http://www.ignitionent.com
276|Swordfish||||||||||
277|Black Box||||||||||
278|Nordcurrent|Vytenio 50|Vilnius|03229||Lithuania||+370 52310490||info@nordcurrent.com|www.nordcurrent.com
279|Blade Interactive|Suite A|Great Northern Warehouse|1 Deansgate Mews|Manchester|United Kindgom|M3 4EN|0161 839 6622||info@bladeinteractive.com|www.bladeinteractive.com
280|FireSky|4140 East Baseline Road|Suite 208||Arizona|United States|85206|||press@firesky.com|www.firesky.com
281|Majesco Europe|City Point, Temple Gate|||Bristol|United Kingdom|BS1 6PL|||infoeurope@majescoentertainment.com|www.majescoeurope.com/
282|Jackhammer Interactive|||||||||contact@jackhammerint.co.uk|www.jackhammerint.co.uk
283|Slitherine Software|The White Cottage|8 Westhill Avenue|Epsom|Surrey|United Kindgom|KT19 8LE|||press@slitherine.co.uk|http://www.slitherine.com
284|Entain8|||||||||info@entain8.com|www.entain8.com
286|Nordic Games Publishing|||||||||info@nordicgames.se|www.nordicgames.se
287|Nobilis Games|||||||||contact@nobilis-france.com|www.nobilis-games.com
288|Fighter Collection|||||||||friends@fighter-collection.com|http://www.fighter-collection.com
289|Bluestone Interactive|||||||||info@bluestonegames.co.uk|www.bluestonegames.co.uk
290|Gazillion Entertainment|||||||||gazillion@outcastpr.com|www.gazillion.com
291|Gamebridge|Gamebridge Ltd|Basepoint Business & Innovation Centre|Butterfield|Luton|United Kindgom|LU2 8DL|01582 433 712||martin.defries@risingstargames.com|www.gamebridge.co.uk
292|Zushi Games|Arundel Court|177 Arundel Street||Sheffield|United Kingdom|S1 2NU|0114 263 6030|0114 263 6031|info@zushigames.com|www.zushigames.com
293|Focus Home Interactive|||||||||communication@focus-home.com|www.focus-home.com
294|Rail Simulator Developments|Compton House|Walnut Tree Close|Guildford|Surrey|United Kindgom|GU1 4TX||||www.railsimulator.com
295|Dusk2Dawn|||||||||info@d2dgames.co.uk|www.d2dgames.co.uk
296|CyberSports|Dunstan House|14a St Cross Street||London|United Kindgom|EC1N 8XA|||enquiries@cybersportsworld.com|www.cybersportsworld.com
297|P2 Games|12 Marwell|Westerham||Kent|United Kingdom|TN16 1SB|07785 231 702|||
298|Tecmo Koei Europe||||||||||www.tecmokoei-europe.com
299|Namco Bandai Games Europe||||||||||www.uk.atari.com
300|Iceberg Interactive||||||||||www.iceberg-interactive.com
301|PQube|5 The Long Yard|Ermin St|Wickfield|West Berkshire|United Kingdom|RG17 7EH||||www.pqube.co.uk
302|Alten8|Basepoint Business and Innovation Centre|110 Butterfield|Great Marlings|Luton|Bedfordshire|LU2 8DL|01582 434325|||www.alten8.com
303|Equestrian Vision|Palmerston House|Cowfold||Horsham|West Sussex|RH13 8BP|01403 865320|01403 865321|info@equestrianvision.demon.co.uk|www.equestrianvision.co.uk
304|Conspiracy Entertainment|||||||||info@conspiracygames.com|www.conspiracygames.com
305|Digital Jesters||||||||||
306|Dice Multimedia||||||||||
307|Endforce||||||||||
308|EndLight Games||||||||||
309|GMX Media||||||||||www.gmxmedia.net
310|Metro 3D||||||||||
311|IncaGold||||||||||www.incagoldplc.com
312|Jelly Bridge|The Pavilion|Newbury Business Park||Newbury||RG14 2PZ|||info@jellybridge.com|www.jellybridge.com
313|Lime Ltd||||||||||
314|Project 3||||||||||
315|Worm That Turned||||||||||
316|Yoostar||||||||||
318|U&I Entertainment||||||||||
319|CDV|||||||||pr@cdvus.com|www.cdvusa.com
320|cdv Software Entertainment|||||||||pr@cdvus.com|www.cdvusa.com
322|G2 Games||||||||||www.g2games.com
324|Avalon Interactive||||||||||
325|Rage Games||||||||||
326|LEGO||||||||||
327|Xicat Interactive||||||||||
328|Bam!||||||||||
330|Grab It||||||||||
331|U Wish Games||||||||||
332|Whiptail Interactive||||||||||
333|PlayV|210a Ground Floor|The Village|Butterfield|Great Marlings|Luton|LU2 8DL||||www.playv.co.uk
334|2VG Group||||||||||
335|3DO||||||||||
336|Abacus||||||||||
337|Acclaim||||||||||
338|AdventureSoft|P.O. Box 786|Sutton Coldfield||West Midlands|United Kingdom|B75 5RS||||www.adventuresoft.com
339|Akaei|Crown House|Linton Road||Barking, Essex|United Kingdom|IG11 8HJ||||
340|Aspyr Media|||||||||info_europe@aspyr.com|www.aspyr.com
341|BigBen Interactive||||||||||
342|Cosmi Europe||||||||||
343|Crucial Entertainment||||||||||
344|Cryo Interactive Entertainment||||||||||
345|Davilex Games||||||||||www.davilex.com
346|DreamForge Software||||||||||
347|eGames||||||||||
348|Enlight Software||||||||||www.enlight.com/eng/
349|Fasttrak Software Publishing||||||||||www.fasttrak.co.uk
350|Fox Interactive||||||||||www.foxinteractive.com
351|Fusion Academia||||||||||
352|Hasbro Games||||||||||www.hasbro.com/games
353|Greenstreet||||||||||
354|GT Interactive||||||||||
355|The Learning Company||||||||||
356|Creature Labs||||||||||
357|SoftKey International||||||||||
358|Horizon Simulation|||||||||support@horizonsimulation.com|www.horizonsimulation.co.uk
359|Jaleco Entertainment||||||||||
360|Mattel Interactive||||||||||
361|Knowledge Adventure||||||||||www.knowledgeadventure.com
362|Contact Sales|||||||||info@contact-sales.co.uk|www.contact-sales.co.uk
363|Sammy Studios||||||||||www.sammystudios.com
364|Hemming||||||||||
365|Hip Interactive||||||||||www.hipinteractive.com
366|Humongous||||||||||
367|id Software||||||||||www.idsoftware.com
368|Infogrames Entertainment||||||||||
369|Interplay||||||||||www.interplay.com
370|Kemco||||||||||
371|Lucas Learning||||||||||
372|Mercury Games||||||||||
373|Microids|||||||||support@microids.com|www.microids.com/en
374|MicroProse Software||||||||||
375|Neechez Innovation||||||||||
376|NovaLogic||||||||||www.novalogic.com
377|Psygnosis||||||||||
378|Reef Entertainment|Regus House|Fairbourne Drive|Atterbury|Milton Keynes|United Kingdom|MK10 9RG|01908 487 587|01908 487 501||www.reef-entertainment.com
379|Smart Saver||||||||||
380|Spectrum HoloByte||||||||||
381|Strategic Simulations||||||||||
382|Strategy First||||||||||www.strategyfirst.com/en
383|TDK Mediactive||||||||||www.tdk-mediactive.com
384|Team17||||||||||www.team17.com
385|The Adventure Company||||||||||www.adventurecompanygames.com
386|Titus Software||||||||||
387|Tivola||||||||||
388|Universal Interactive||||||||||www.universal-interactive.com
389|Virgin Interactive||||||||||
390|Xider Games||||||||||/www.xider-games.com
391|XS Games||||||||||www.xsgames.biz
392|Encore Software||||||||||
393|Global Star Software||||||||||
394|Simon & Schuster Interactive||||||||||
395|BBC Multimedia||||||||||www.bbcworldwide.com/multimedia
396|GTE Interactive Media||||||||||
397|Plaid Banana Entertainment||||||||||
398|Serious Global||||||||||
399|TopWare Interactive||||||||||www.topware.com
400|Lace Mamba|Office 404|4th Floor, Albany House|324/326 Regent Street|London|United Kingdom|W1B 3HH||||www.mamba-games.co.uk
401|HALYCON||||||||||www.contact-sales.co.uk
402|O Games|17 Bridle Close|Finedon Road Ind Est||Wellingborough|United Kingdom|NN8 4RN|01933 442660|01933 446420||www.ogames.net
403|Enjoy Gaming|Manor Barn|Manor Lane|Great Gransden Sandy|Bedfordshire|United Kingdom|SG19 3RA|||info@enjoygaming.com|www.enjoygaming.com
404|Gametrak||||||||||
405|Liquid Games||||||||||
406|bitComposer Games||||||||||www.bit-composer.com
407|Tradewest||||||||||
408|Alternative Software||||||||||www.alternativesoft.co.uk
409|Foreign Media Games||||||||||www.foreignmediagames.com
410|dtp|||||||||info@dtp-young.com|www.dtp-young.com
411|Funbox Media|Office 7|The Cube|1-10 Brittain Street|Sheffield|United Kingdom|S1 4RJ||||
412|Creative Distribution|||||||0208 664 3456|0208 664 8848|craig@creativedistribution.co.uk|www.creativedistribution.co.uk
413|Playsims Publishing||||||||||www.playsimspublishing.com
414|Licensed 4U|||||||020 8941 8877||www.licensed4u.com|
415|Easy Interactive||||||||||www.easyinteractive.nl
416|HPN Associates||||||||||
417|Wendros||||||||||www.wendros.se
418|RC Simulations||||||||||www.rcsimulations.com
419|Atomic Games|||||||||press@atomic.com|www.atomic.com
420|Trion Worlds||||||||||www.trionworlds.com
421|Ikaron||||||||||
422|Paramount||||||||||
423|Merge Games||||||||||www.mergegames.com
424|Extra Play||||||||||www.contact-sales.co.uk
425|Tubby Games||||||||||www.tubbygames.com
426|CCP Games||||||||||www.ccpgames.com/en/home
427|Gust||||||||||
428|Gust||||||||||
429|Gust||||||||||
430|Mad Catz||||||||||
431|NIS America||||||||||
432|GungHo Online Entertainment America||||||||||
