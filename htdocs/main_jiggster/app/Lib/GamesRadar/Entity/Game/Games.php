<?php
require_once "Game.php";

/**
* @package Games Entity
*/

class Games extends ApiGame
{
	/**
	 * @var array
	 */
	public $name = array(
		'us' => null,
		'uk' => null,
	);

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $alternativeNames;

	/**
	 * @var string
	 */
	public $platform;

	/**
	 * @var array
	 */
	public $expectedReleaseDate = array(
		'us' => null,
		'uk' => null,
	);

	/**
	 * @var array
	 */
	public $releateDate = array(
		'us' => null,
		'uk' => null,
	);

	/**
	 * @var int
	 */
	public $updatedDate;

	/**
	 * @var array
	 */
	public $score = array(
		'id' => null,
		'name' => null,
	);

	/**
	 * @var string
	 */
	public $url;

	/**
	 * @var array
	 */
	public $images = array(
		'thumbnail' => null,
		'boxart'    => array(
			'us' => null,
			'uk' => null,
		),
	);
}