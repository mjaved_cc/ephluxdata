<?php
require_once "Requester.php";
require_once "Entity/Platform.php";
require_once "Entity/Genre.php";
require_once "Entity/Game/Games.php";

/**
 * GamesRadar client
 */
class Client
{
	/**
	 * @var Requester
	 */
	private $requester;

	/**
	 * @var string
	 */
	private $apiKey;

	/**
	 * @param Requester $requester
	 */
	public function __construct(Requester $requester, $apiKey)
	{
		$this->apiKey = $apiKey;
		$this->requester = $requester;
	}

	/**
	 * @param string $resource
	 * @param array $args
	 * @return mixed
	 */
	private function request($resource, array $args = array())
	{
		$args += array(
			'api_key' => $this->apiKey,
		);
		$xml = $this->requester->request($resource, $args);
		$data = $this->processResponse($xml, $resource);
		return $data;
	}

	/**
	 * @param SimpleXMLElement $xml
	 * @param string $uri
	 * @return data from SimpleXMLElement
	 */
	private function processResponse($xml, $uri)
	{

		$data = array();
		
		switch ($uri) 
		{
			case 'platforms':
			
				foreach ($xml->platform as $node) {
					$platform = new Platform();
					$platform->id   =  $node->id;
					$platform->name = (string) $node->name;
					$data[] = $platform;
				}
				
		  	break;

			case 'genres':
			
				foreach ($xml->genre as $node) {
					$genre = new ApiGenre();
					$genre->id   = (int) $node->id;
					$genre->name = (string) $node->name;
					$data[] = $genre;
				}

		  	break;

			case 'games':
			
				foreach ($xml->game as $node) {
					$entity = new ApiGame;
					$entity->id = (int) $node->id;
					$entity->description = (string) $node->description;
					$entity->url = (string) $node->url;
					$entity->name['us'] = (string) $node->name->us;
					$entity->releaseDate['us'] = strtotime($node->release_date->us);
					if($entity->score['id']) {
						$entity->score['id'] = (int) $node->score->id;
					}
					$entity->images['thumbnail'] = (string) $node->images->thumbnail;
					$entity->platform = $node->platform->id;

					$gameXml = $this->processGames('game/'.$entity->id);
					$entity->genre = (int) $gameXml->genre->id;
					
					if ($gameXml->publishers) {
					  if ($gameXml->publishers->us && $gameXml->publishers->us->company) {
						  foreach ($gameXml->publishers->us->company as $publisherNode) {
							  $entity->publishers['us'][] = (string) $publisherNode->name;
						  }
					   }
				  	}

				  if ($gameXml->developers->company) {
					  foreach ($gameXml->developers->company as $developerNode) {
						  $entity->developers[] = (string) $developerNode->name;
					  }
				  }

				   $data[] = $entity;
						
				}
				
  			break;
		}
		//print '<pre>'; print_r($data); die(); 
		return $data;
	}

	/**
	 * @param SimpleXMLElement $xml
	 * @return data from SimpleXMLElement
	 */

	private function processGames($resource, array $args = array())
	{
		$args += array(
			'api_key' => $this->apiKey,
		);		
		$xml = $this->requester->request($resource, $args);
		return $xml;		
	}

	/**
	 * @param array $options
	 * @return array
	 */
	private static function processOptions(array $options)
	{
		$args = array();

		if (isset($options['platform'])) {
			$args['platform'] = $options['platform'];
		}

		if (isset($options['genre'])) {
			$args['genre'] = $options['genre'];
		}

		if (isset($options['gamename'])) {
			$args['game_name'] = $options['game_name'];
		}

		if (isset($options['page'])) {
			$args['page_num'] = $options['page'];
		}

		if (isset($options['limit'])) {
			$args['page_size'] = $options['limit'];
		}

		if (isset($options['sort'])) {
			$args['sort'] = $options['sort'];
		}

		if (isset($options['unique'])) {
			$args['unique_game'] = $options['unique'] ? 'true' : 'false';
		}

		if (isset($options['content'])) {
			$args['content'] = $options['content'];
		}

		return $args;
	}

	/**
	 * Get games list.
	 * @param array $options
	 * @param string $options['platform']
	 * @param string $options['genre']
	 * @param string $options['gamename']
	 * @param int $options['page']
	 * @param int $options['limit']
	 * @param string $options['sort']
	 */
	public function games(array $options = array())
	{
		$args = self::processOptions($options);
		$data = $this->request('games', $args);
		return $data;
	}

	/**
	 * @return array platforms
	 */
	public function platforms()
	{
		$data = $this->request('platforms');
		return $data;
	}

	/**
	 * @return array genres
	 */
	public function genres()
	{
		$data = $this->request('genres');
		return $data;
	}

}
