<?php

class DtUser {

	private $_fields = array();
	private $_allowed_keys = array('id','first_name','last_name','nickname','email', 'dob','username','group_id','created','status','gaming_id', 'shipping_address', 'address_1', 'address_2', 'zipcode', 'state', 'country', 'latitude', 'longitude', 'location', 'paypal_id', 'contact', 'profile_text', 'trusted_user', 'trusted_since', 'image_url', 'push_notification');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified' ) {
				//pr($this->_fields); die();
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get() {

		return $this->_fields;
	}

	/**
	 * Add single social account to user object
	 * 
	 * @param array $user_social_data user social information
	 */
	function add_user_social_account($user_social_data) {
		if (isset($this->_fields["UserSocialAccount"]) && !is_array($this->_fields["UserSocialAccount"])) {
			$prev_value = $this->UserSocialAccount; // restore previous value
			unset($this->_fields["UserSocialAccount"]); // now unset it so that we can convert object to array
			$this->_fields["UserSocialAccount"][] = $prev_value;
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		} else if (isset($this->_fields["UserSocialAccount"]) && is_array($this->_fields["UserSocialAccount"]))
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		else
			$this->_fields["UserSocialAccount"] = new DtUserSocialAccount($user_social_data);
	}

	/**
	 * Add multiple social accounts to user object
	 * 
	 * @param array $user_social_data user social information
	 */
	function add_user_social_accounts($user_social_data) {
		if (!empty($user_social_data)) {
			foreach ($user_social_data as $value)
				$this->add_user_social_account($value);
		}
	}

	function get_field() {

		$temp = $this->image_url;
		$image_url = '';
		
		if (strpos($temp, 'https') === FALSE && strpos($temp, 'http') === FALSE) {

			$image_url = SITE_URI.$this->image_url;

		} else {

			$image_url = $this->image_url;

		}

		$trust = true;
		
		if($this->trusted_user == 0){
			$trust = false;
		}
		
		return array(
			'id' => $this->id,
			'nickname' => $this->nickname,
			'email' => $this->email,
			'dob' => $this->dob,
			'gaming_id' => $this->gaming_id,
			'shipping_address' => $this->shipping_address,
			'address_1' => $this->address_1,
			'address_2' => $this->address_2,
			'zipcode' => $this->zipcode,
			'state' => $this->state,
			'country' => $this->country,
			'latitude' => $this->latitude,
			'longitude' => $this->longitude,
			'location' => $this->location,
			'status' => $this->status,
			'paypal_id' => $this->paypal_id,
			'contact' => $this->contact,
			'profile_text' => $this->profile_text,
			'trusted_user' => $trust,
			'trusted_since' => $this->trusted_since,
			'profile_image_url' => $image_url,
			'push_notification' => $this->push_notification,
			'created' => $this->created
		);
	}

}