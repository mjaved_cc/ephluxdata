<?php

class DtGame {

	private $_fields = array();
	private $_allowed_keys = array('id', 'title', 'sub_title', 'desc', 'console_id', 'genre_id', 'publisher', 'developers', 'buying', 'selling', 'ongoing_deals', 'rating', 'image_url', 'trading', 'status', 'amazon', 'google_play', 'release_date', 'jiggster_pick','barcode');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if ($key == 'image_url') {
				return $this->_get_fully_qualified_image_url();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get() {

		return $this->_fields;
	}

	private function _get_fully_qualified_image_url() {
		return self::get_fully_qualified_image_url($this->_fields['image_url']);
	}

	public static function get_fully_qualified_image_url($image_json) {

		if (!empty($image_json) && $image_json != "") {

			$image = json_decode($image_json, TRUE);
			$last_two_letter = '';

			foreach ($image as $key => $val) {

				$file_details = pathinfo($val);

				$last_two_letter = substr($file_details['filename'], -2);

				$image_url = GAME_COVER_IMAGE_URI . $last_two_letter . '/' . $val; 

				return $image_url;
			}
		} else {

			$image_url = GAME_COVER_IMAGE_URI . 'game_preview.png';

			return $image_url;
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'title' => mysql_escape_string($this->title),
			'sub_title' => mysql_escape_string($this->sub_title),
			'description' => mysql_escape_string($this->desc),
			'publisher' => $this->publisher,
			'developers' => $this->developers,
			'image_url' => $this->image_url, 
			'buying' => $this->buying,
			'selling' => $this->selling,
			'rating' => $this->rating,
			'console_id' => $this->console_id,
			'ongoing_deals' => $this->ongoing_deals,
			'trading' => $this->trading,
			'genre_id' => $this->genre_id,
			'console_id' => $this->console_id,
			'status' => $this->status,
			'amazon' => $this->amazon,
			'google_play' => $this->google_play,
			'release_date' => $this->release_date,
			'jiggster_pick' => $this->jiggster_pick,
			'barcode' => $this->barcode
		);
	}

	function add_user_game($user_game) {
		if (isset($this->_fields["UserGame"]) && !is_array($this->_fields["UserGame"])) {
			$prev_value = $this->UserGame; // restore previous value
			unset($this->_fields["UserGame"]); // now unset it so that we can convert object to array
			$this->_fields["UserGame"][] = $prev_value;
			$this->_fields["UserGame"][] = new DtUserGame($user_game);
		} else if (isset($this->_fields["UserGame"]) && is_array($this->_fields["UserGame"]))
			$this->_fields["UserGame"][] = new DtUserGame($user_game);
		else
			$this->_fields["UserGame"] = new DtUserGame($user_game);
	}

	function get_user_game() {
		$data = array();
		if (isset($this->_fields['UserGame'])) {
			if (is_array($this->_fields['UserGame'])) {
				foreach ($this->_fields['UserGame'] as $usergame) {
					$data[] = $usergame->get_field();
				}
			} else if ($this->_fields['UserGame'] instanceof DtUserGame) {
				$data = $this->_fields['UserGame']->get_field();
			}
		}
		return $data;
	}

	function add_genre($game_genre) {
		if (isset($this->_fields["Genre"]) && !is_array($this->_fields["Genre"])) {
			$prev_value = $this->Genre; // restore previous value
			unset($this->_fields["Genre"]); // now unset it so that we can convert object to array
			$this->_fields["Genre"][] = $prev_value;
			$this->_fields["Genre"][] = new DtGenre($game_genre);
		} else if (isset($this->_fields["Genre"]) && is_array($this->_fields["Genre"]))
			$this->_fields["Genre"][] = new DtGenre($game_genre);
		else
			$this->_fields["Genre"] = new DtGenre($game_genre);
	}

	function get_genre() {
		$data = array();
		if (isset($this->_fields['Genre'])) {
			if (is_array($this->_fields['Genre'])) {
				foreach ($this->_fields['Genre'] as $genre) {
					$data[] = $genre->get_field();
				}
			} else if ($this->_fields['Genre'] instanceof DtGenre) {
				$data = $this->_fields['Genre']->get_field();
			}
		}
		return $data;
	}

	function add_wishlist($user_wishlist) {
		if (isset($this->_fields["Wishlist"]) && !is_array($this->_fields["Wishlist"])) {
			$prev_value = $this->Wishlist; // restore previous value
			unset($this->_fields["Wishlist"]); // now unset it so that we can convert object to array
			$this->_fields["Wishlist"][] = $prev_value;
			$this->_fields["Wishlist"][] = new DtUserWishlist($user_wishlist);
		} else if (isset($this->_fields["Wishlist"]) && is_array($this->_fields["Wishlist"]))
			$this->_fields["Wishlist"][] = new DtUserWishlist($user_wishlist);
		else
			$this->_fields["Wishlist"] = new DtUserWishlist($user_wishlist);
	}

	function get_wishlist() {
		$data = array();
		if (isset($this->_fields['Wishlist'])) {
			if (is_array($this->_fields['Wishlist'])) {
				foreach ($this->_fields['Wishlist'] as $wishlist) {
					$data[] = $wishlist->get_field();
				}
			} else if ($this->_fields['Wishlist'] instanceof DtUserWishlist) {
				$data = $this->_fields['Wishlist']->get_field();
			}
		}
		return $data;
	}

}