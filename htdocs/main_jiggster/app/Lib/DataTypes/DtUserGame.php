<?php

class DtUserGame {

	private $_fields = array();
	private $_allowed_keys = array('id','user_id', 'game_id','trade','condition','comes_with', 'amount', 'comment');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get() {

		return $this->_fields;
	}



	function get_field() {

		return array(
			'id' => $this->id,
			'user_id' => $this->user_id,
			'game_id' => $this->game_id,
			'trade' => $this->trade,		
			'condition' => $this->condition,		
			'comes_with' => $this->comes_with,		
			'amount' => $this->amount,		
			'comment' => $this->comment		
		);
	}
	
	function add_user($user) {
		if (isset($this->_fields["User"]) && !is_array($this->_fields["User"])) {
			$prev_value = $this->User; // restore previous value
			unset($this->_fields["User"]); // now unset it so that we can convert object to array
			$this->_fields["User"][] = $prev_value;
			$this->_fields["User"][] = new DtUser($user);
		} else if (isset($this->_fields["User"]) && is_array($this->_fields["User"]))
			$this->_fields["User"][] = new DtUser($user);
		else
			$this->_fields["User"] = new DtUser($user);
	}
	
	function get_user() {
		$data = array();
		if(isset($this->_fields['User'])) {
			if (is_array($this->_fields['User'])) {
				foreach ($this->_fields['User'] as $usergame) {
					$data[] = $usergame->get_field();
				}
			} else if ($this->_fields['User'] instanceof DtUser) {
				$data = $this->_fields['User']->get_field();
			}
		}
		return $data;
	}
	
	function add_game($game) {
		if (isset($this->_fields["Game"]) && !is_array($this->_fields["Game"])) {
			$prev_value = $this->Game; // restore previous value
			unset($this->_fields["Game"]); // now unset it so that we can convert object to array
			$this->_fields["Game"][] = $prev_value;
			$this->_fields["Game"][] = new DtGame($game);
		} else if (isset($this->_fields["Game"]) && is_array($this->_fields["Game"]))
			$this->_fields["Game"][] = new DtGame($game);
		else
			$this->_fields["Game"] = new DtGame($game);
	}
	
	function get_game() {
		$data = array();
		if(isset($this->_fields['Game'])) {
			if (is_array($this->_fields['Game'])) {
				foreach ($this->_fields['Game'] as $usergame) {
					$data[] = $usergame->get_field();
				}
			} else if ($this->_fields['Game'] instanceof DtGame) {
				$data = $this->_fields['Game']->get_field();
			}
		}
		return $data;
	}

}