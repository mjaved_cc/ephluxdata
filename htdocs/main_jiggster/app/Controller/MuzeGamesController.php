<?php

App::uses('MuzeGames', 'Lib/MuzeGames');

// Controller/MuzeGamesController.php
class MuzeGamesController extends AppController {

	public $components = array('RequestHandler');
	public $name = 'MuzeGames';
	public $uses = array('Console', 'Genre', 'Game', 'SimilarGame');
	static $muze;

	function beforeFilter() {
		$this->Auth->allow('*');
		self::$muze = new MuzeGames();
	}

	function index() {
		
	}

	public function get_games() {

		$games[] = self::$muze->getGames();
		if (isset($games)) {
			$this->Game->save_game($games);
		}
		exit();
	}

	public function get_genres() {
		$genres = self::$muze->getAllGenre();
		$this->Genre->save_genre($genres);
		exit();
	}

	public function get_consoles() {
		$platforms = self::$muze->getAllConsole();
		$this->Console->save_console($platforms);
		exit();
	}

	public function get_similar_games() {

		$similar_games = self::$muze->getSimilarGames();
		$this->SimilarGame->save_similar_games($similar_games);
		exit('Similar games saved');
	}

}