<?php

class ApiPageComponent extends Component {

	const BASE_CODE = API_PAGE_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get Page Data
	 * @param $id
	 * @return array $data
	 * Error code range : 1-10
	 */
	function get_page_data($id) {

		$data = $this->_controller->Page->get_data($id);

		if (!empty($data)) {

			$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Page data found";
			$this->ApiResponse->body = $data;
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Page data could not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Add page
	 * @param array $data
	 * @return array 
	 * Error code range : 11-20
	 */
	function add($data) {

		if (!empty($data['title']) && !empty($data['desc'])) {

			$status = $this->_controller->Page->add_record($data); 

			if (!empty($status)) {

				$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Page added";
				$this->ApiResponse->body = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
				$this->ApiResponse->offset = '11';
				$this->ApiResponse->msg = "Page could not added";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
			$this->ApiResponse->offset = '12';
			$this->ApiResponse->msg = "Some fields are missing";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}
	
	/**
	 * Get All Pages
	 * @param null
	 * @return $pages 
	 * Error code range : 21-30
	 */
	function get_all_pages($page_type) {
		
		$pages = $this->_controller->Page->get_all($page_type);
		
		if (!empty($pages)) {

				$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Pages found";
				$this->ApiResponse->body = $pages;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiPageComponent::BASE_CODE;
				$this->ApiResponse->offset = '21';
				$this->ApiResponse->msg = "Pages are not found ";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		
	}

}

?>