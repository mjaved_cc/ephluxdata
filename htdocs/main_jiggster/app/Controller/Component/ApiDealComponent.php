<?php

class ApiDealComponent extends Component {

	const BASE_CODE = API_DEAL_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Save offer
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 1-10
	 */
	function save_offer($json_data) {

		$data = json_decode($json_data);

		if (
				!empty($data->body->user_id) &&
				!empty($data->body->member_id) &&
				isset($data->body->what_i_have) &&
				isset($data->body->what_i_want) &&
				!empty($data->body->user_trade) &&
				!empty($data->body->member_trade) &&
				!empty($data->body->action_taken_by) &&
				!empty($data->body->action_taken) &&
				md5(SIGNATURE) == $data->body->key) {

			$deal_id = $this->_controller->Deal->save_deal($data->body);

			if (!empty($data->body->comment) && $data->body->action_taken != 'rejected' && $data->body->action_taken != 'cancelled') {

				$sender_deal_msg = $data->body->action_taken_by;
				$receiver_deal_msg = '';

				if ($data->body->action_taken_by == $data->body->user_id) {

					$receiver_deal_msg = $data->body->member_id;
				}

				if ($data->body->action_taken_by == $data->body->member_id) {

					$receiver_deal_msg = $data->body->user_id;
				}

				$param = array(
					'deal_id' => $deal_id,
					'id_sender' => $sender_deal_msg,
					'id_receiver' => $receiver_deal_msg,
					'user_message' => Sanitize::escape($data->body->comment)
				);

				$deal_chatlog_id = $this->_controller->DealsChatlog->save_deal_message($param);
			}
			
			if($data->body->action_taken == 'accepted'){
				
				$this->_controller->Game->cencel_relevent_deals($data->body->user_id, $data->body->member_id, $data->body->what_i_have, $data->body->what_i_want);
				
			}

			if (!empty($deal_id)) {

				$this->ApiResponse->base = ApiDealComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "offer Saved";
				$this->ApiResponse->body = array('deal_id' => $deal_id);
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiDealComponent::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "offer not Saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiDealComponent::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save Deal chatlog
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 11-20
	 */
	function save_deal_chatlog($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) &&
				!empty($data->body->id_sender) &&
				!empty($data->body->id_receiver) &&
				!empty($data->body->user_message) &&
				md5(SIGNATURE) == $data->body->key) {

			$param = array(
				'deal_id' => $data->body->deal_id,
				'id_sender' => $data->body->id_sender,
				'id_receiver' => $data->body->id_receiver,
				'user_message' => Sanitize::escape($data->body->user_message)
			);

			$status = $this->_controller->DealsChatlog->save_deal_message($param);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Deal Chat successfully saved";
				$this->ApiResponse->body = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '11';
				$this->ApiResponse->msg = "Deal Chat could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '12';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Geat Deal Chat
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 21-30
	 */
	function chat_deal($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->DealsChatlog->user_deal_chat($data->body->deal_id);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Deal Chat found";
				$this->ApiResponse->body = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '21';
				$this->ApiResponse->msg = "Deal Chat not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '22';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 *  Count Unread Deal Message
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 31-40
	 */
	function count_deal_unread_messages($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->id_receiver) && md5(SIGNATURE) == $data->body->key) {

			$message_count = $this->_controller->DealsChatlog->unread_msg_count($data->body->id_receiver);

			if (!empty($message_count)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Messages Count Found";
				$this->ApiResponse->body = $message_count;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '31';
				$this->ApiResponse->msg = "Messages count count not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '32';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Mark Deal Messages Read
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 41-50
	 */
	function mark_deal_message_read($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) &&
				!empty($data->body->id_receiver) &&
				md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->DealsChatlog->mark_msg_read($data->body->deal_id, $data->body->id_receiver);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Messages marked read";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '41';
				$this->ApiResponse->msg = "Messages could not marked read";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '42';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Awaiting Rating
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 51-60
	 */
	function awaiting_rating($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$response = $this->_controller->UsersDeal->awaiting_ratings($data->body->user_id);

			if (!empty($response['buy']) || !empty($response['sell']) || !empty($response['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $response;
				$this->ApiResponse->msg = "awaiting rating found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '51';
				$this->ApiResponse->msg = "awaiting rating not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '52';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Deal Rating
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 61-70
	 */
	function rating_deal($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && !empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$response = $this->_controller->UsersDeal->rating($data->body->deal_id, $data->body->user_id, $data->body->rating);

			if (!empty($response)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Deal rating saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '61';
				$this->ApiResponse->msg = "Deal rating could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '62';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save Deal Archive
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 71-80
	 */
	function save_deal_archive($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && isset($data->body->is_archive) && md5(SIGNATURE) == $data->body->key) {

			$response = $this->_controller->Deal->deal_archive_save($data->body->deal_id, $data->body->is_archive);

			if (!empty($response)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Deal archive saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '71';
				$this->ApiResponse->msg = "Deal archive could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '72';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Deal save flag and comment
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 81-90
	 */
	function save_flag_and_comment($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) &&
				!empty($data->body->user_id) &&
				!empty($data->body->flag) &&
				isset($data->body->flag_comment) &&
				md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->Deal->flag_and_comment_save($data->body->deal_id, $data->body->user_id, $data->body->flag, $data->body->flag_comment);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Thank you for your feedback. We will look into this matter as soon as possible.";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '81';
				$this->ApiResponse->msg = "Flag and comment could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '82';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Deal Details
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 91-100
	 */
	function detail_deal($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && !empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$data = $this->_controller->Deal->detail($data->body->deal_id, $data->body->user_id);

			if (!empty($data)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $data;
				$this->ApiResponse->msg = "Deal detail found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '91';
				$this->ApiResponse->msg = "Deal detail could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '92';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Send Receive Details of Specific Deal
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 101-110
	 */
	function specific_deal_send_receive($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && !empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Deal->send_receive_deal($data->body->deal_id, $data->body->user_id);

			if (!empty($games['receive_confirm']['buy']) || !empty($games['receive_confirm']['sell']) || !empty($games['receive_confirm']['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '101';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '102';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Unread Deal Messages AND Count
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 110-120
	 */
	function unread_msg_count_and_last_msg($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->deal_id) && !empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$data = $this->_controller->DealsChatlog->deal_last_msg_and_count_unread($data->body->deal_id, $data->body->user_id);

			if (!empty($data['DealChat'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $data;
				$this->ApiResponse->msg = "Record Found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '111';
				$this->ApiResponse->msg = "No Recored found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '112';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

}

?>