<?php

App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Xml', 'Utility');
App::uses('CakeTime', 'Utility');
App::uses('Sanitize', 'Utility');

class AppComponent extends Component
{
    /*private $_controller;

    function startup(Controller $controller) {

        $this->_controller = $controller;
    }*/

    /**
     * encodes json
     * @param array $data
     * @return string json
     */
    function encoding_json($data)
    {

        return json_encode($data);
    }

    /**
     * decodes json
     * @param array $data
     * @return string json
     */
    function decoding_json($data)
    {

        return json_decode($data, true);
    }

    /**
     * get json that could be saved in db
     * @param: $data
     * @return json
     */
    function get_db_save_json($data)
    {
        return Sanitize::escape(json_encode($data));
    }

    /**
     * check for valid format of email
     */
    function is_valid_email($email = null)
    {
        if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$",
            $email))
            return true;
        return false;
    }

    function get_current_datetime()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * get expiry date for expired_at field of Tmp table
     */
    function get_tmp_expiry()
    {
        return date('Y-m-d', strtotime("+3 month"));
    }

    /**
     * Send an email using the specified content, template and layout.
     *
     * @uses CakeEmail
     */
    function send_email($to, $subject, $message, $template = 'default', $layout =
    'default')
    {
        if (SEND_EMAIL_WITH_AUTH) {
            $email = new CakeEmail('gmail');
            $email->to($to);
            $email->subject($subject);
            //   $email->template($template, $layout);
            //   $email->emailFormat('html');
            //   $email->viewVars(array('message' => $message));
            //   $email->send();
        } else
            $email = CakeEmail::deliver($to, $subject, $message, array('from' => array(EMAIL_NOREPLY =>
                SITE_NAME)), false);

        $email->template($template, $layout);
        $email->emailFormat('html');
        $email->viewVars(array('message' => $message));
        return $email->send();

        /*$email = new CakeEmail('gmail');
        $email->to($to);
        $email->template($template, $layout);
        $email->emailFormat('html');
        $email->viewVars(array('message' => $message));
        $email->subject($subject);
        $email->send(); */
    }

    /*	 * ***** How to save IP Address in DB ****** */
    /* INET_NTOA and INET_ATON functions in mysql. They convert between dotted notation IP address to 32 bit integers. This allows			you to store the IP in just 4 bytes rather than a 15 bytes
    */

    /**
     * Get an (IPv4) Internet network address into a string in Internet standard dotted format.
     * @param string $number numeric-ip-representation
     * @return string ip-address-with-dotted-format
     */
    function get_ip_address($number)
    {
        // analogous to INET_NTOA in mysql
        return sprintf("%s", long2ip($number));
    }

    /**
     * Get string containing an (IPv4) Internet Protocol dotted address into a proper address
     *
     * @return int numeric-ip-representation
     *
     */
    function get_numeric_ip_representation()
    {
        // analogous to INET_ATON in mysql
        return sprintf("%u", ip2long(getenv('REMOTE_ADDR')));
    }

    /*	 * ***** END: How to save IP Address in DB ***** */

    /**
     * Return a well-formed XML string for given array
     *
     * @param array $data
     * @return XML
     */
    function get_xml($data)
    {

        if (!empty($data)) {
            $response[XML_ROOT_TAG] = $data;
        } else {
            $response = array(XML_ROOT_TAG => array('code' => '400', 'message' =>
                'Bad request'));
        }

        $xml = Xml::fromArray($response);
        return $xml->asXML();
    }

    /**
     * Return as mysql datetime format
     *
     * @return string Date
     */
    function get_current_date() {
        $date = $this->get_date('now');
        return $date;
    }

    /**
     * Returns a formatted date string. Wrapper of CakeTime::format()
     *
     * @param integer|string|DateTime $date UNIX timestamp, strtotime() valid string or DateTime object (or a date format string)
     * @param integer|string|DateTime $format date format string (or UNIX timestamp, strtotime() valid string or DateTime object)
     */
    function get_date($date , $format = 'Y-m-d') {
        return CakeTime::format($format, $date );
    }

    /**
     * Logging in muze_games.log
    */
    function muze_game_logging($data){
        CakeLog::write('muze_games' , $data );
    }


}

?>