<?php

App::uses('MuzeGames', 'Lib/MuzeGames');

class CronsController extends AppController {

	public $components = array('App', 'Curl', 'ApiGame');
	public $uses = array('Game', 'Delete', 'Cron');
	private $_obj_muze;

	function beforeFilter() {

		$this->Auth->allow('*');
		$this->autoRender = false;
		$this->_obj_muze = new MuzeGames();

		//$this->Delete->useDbConfig = 'muze_game_data';
	}

	/**
	 * Update games on daily basis. Muze games provide update files (.txt) with name dgascii.zip
	 * & cover images with name latestgm.zip
	 */
	function update_games_daily_basis() {

//		$this->ApiGame->update_games_via_muze_api();
//		die;

		$this->App->muze_game_logging('/**** START ' . __FUNCTION__ . '***/');

		$file_name_txt = 'dgascii.zip';
		$file_name_img = 'latestgm.zip';

		// url is different - txt
		$this->Curl->url = MUZE_GAME_FTP_HOST . MUZE_GAME_FTP_GAME_DATA_PATH . $file_name_txt;
		$this->Curl->data = array(
			'file_name' => $file_name_txt,
			'tmp_location' => MUZE_GAME_TMP_LOCATION_TXT_UNZIPPED
		);

		$response_txt = $this->Curl->download_file_via_ftp();

		if ($response_txt) {

			$this->App->muze_game_logging('/**** SUCCESSFULLY DOWNLOADED TEXT FILES ***/');

			// url is different - images
			$this->Curl->url = MUZE_GAME_FTP_HOST . MUZE_GAME_FTP_GAME_COVER_PATH . $file_name_img;
			$this->Curl->data = array(
				'file_name' => $file_name_img,
				'tmp_location' => MUZE_GAME_TMP_LOCATION_IMAGES_UNZIPPED
			);

			$response_img = $this->Curl->download_file_via_ftp();

			if ($response_img) {

				$this->App->muze_game_logging('/**** SUCCESSFULLY DOWNLOADED COVER IMAGES FILES ***/');

				$this->_obj_muze->move_images_from_tmp();
				//die('done migrating');

				$this->ApiGame->update_games_via_muze_api();
				//die(__FUNCTION__);
			} else {

				$this->App->muze_game_logging('/**** FAILURE - COVER IMAGES FILES ***/');
			}
		} else {

			$this->App->muze_game_logging('/**** FAILURE - TEXT FILES ***/');
		}


		$this->App->muze_game_logging('/**** END ' . __FUNCTION__ . '***/');
	}

	/**
	 * Muze games also maintain history for those files in case we miss any update on daily basis.
	 *
	 * We will fetch last entry of games based on created date.
	 */
	function update_games_history() {
		
		//Date format should be yyyymmdd i.e. 20140205

		if ($this->params['url']['date'] != "") {
			
			$this->App->muze_game_logging('/**** START HISTORY UPDATE ' . __FUNCTION__ . ' ***/');
			
			$this->App->muze_game_logging('Date provided : ' . $this->params['url']['date']);

			$file_name_txt = 'hist' . $this->params['url']['date'] . '.zip';
			$file_name_img = 'hist' . $this->params['url']['date'] . '.zip';

			// url is different - txt
			$this->Curl->url = MUZE_GAME_FTP_HOST . MUZE_GAME_FTP_GAME_DATA_PATH_HISTORY . $file_name_txt;
			$this->Curl->data = array(
				'file_name' => $file_name_txt,
				'tmp_location' => MUZE_GAME_TMP_LOCATION_TXT_UNZIPPED
			);

			$response_txt = $this->Curl->download_file_via_ftp();

			if ($response_txt) {

				$this->App->muze_game_logging('/**** SUCCESSFULLY DOWNLOADED TEXT FILES ***/');

				// url is different - images
				$this->Curl->url = MUZE_GAME_FTP_HOST . MUZE_GAME_FTP_GAME_COVER_PATH_HISTORY . $file_name_img;
				$this->Curl->data = array(
					'file_name' => $file_name_img,
					'tmp_location' => MUZE_GAME_TMP_LOCATION_IMAGES_UNZIPPED
				);

				$response_img = $this->Curl->download_file_via_ftp();

				if ($response_img) {

					$this->App->muze_game_logging('/**** SUCCESSFULLY DOWNLOADED COVER IMAGES FILES ***/');

					$this->_obj_muze->move_images_from_tmp();
					//die('done migrating');

					$this->ApiGame->update_games_via_muze_api();
					//die(__FUNCTION__);
				} else {

					$this->App->muze_game_logging('/**** FAILURE - COVER IMAGES FILES ***/');
				}
			} else {

				$this->App->muze_game_logging('/**** FAILURE - TEXT FILES ***/');
			}
		} else {
			$this->App->muze_game_logging('Please provide valid date');
		}

		$this->App->muze_game_logging('/**** END HISTORY UPDATE ***/');
	}

}