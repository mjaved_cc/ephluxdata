<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {

	public $uses = array('Group', 'ArosAco');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Group.name' => 'asc'
		),
		'conditions' => array('Group.is_active' => IS_ACTIVE)
	);

	function beforeFilter() {
		parent::beforeFilter();
		$this->layout = 'admin';
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Group->recursive = 0;

		$data = $this->paginate();
		$groups = array();
		foreach ($data as $key => $group) {
			$obj_groups = new DtGroup($group[$this->modelClass]);
			$groups[$key][$this->modelClass] = $obj_groups->get_field();
		}

		$this->set(compact('groups'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}

		$group = array();

		$result = $this->Group->get_details_by_id($id);

		$obj_group = new DtGroup($result[0][$this->modelClass]);
		$group[$this->modelClass] = $obj_group->get_field();

		foreach ($result as $key => $value) {
			$obj_group->add_user($value['User']);
		}

		if (is_array($obj_group->User)) {
			foreach ($obj_group->User as $key => $value) {
				$group['User'][$key] = $value->get_field();
			}
		} else if ($obj_group->User instanceof DtUser) {
			// just to have consistency in view
			$group['User'][0] = $obj_group->User->get_field();
		}

		$this->set(compact('group'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Group->set($this->request->data);
			if ($this->Group->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Group->create($data)) {
					$this->Session->setFlash(__('The group has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Group->set($this->request->data);
			if ($this->Group->validates(array('fieldList' => array('name')))) {
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				if ($this->Group->update($data)) {
					$this->Session->setFlash(__('The group has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Group->get_by_id($id);
			$obj_group = new DtGroup($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_group->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Group->remove($data)) {
			$this->Session->setFlash(__('Group deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Group was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Edit Permissions
	 * 
	 * @param int $group_id Group id
	 * @return void
	 */
	function edit_permissions($group_id) {

		if ($this->request->is('post') || $this->request->is('put')) {

			if (!empty($this->request->data['is_allowed'])) {
				$this->ArosAco->remove_all_by_group_id($group_id);
				foreach ($this->request->data['is_allowed'] as $aco => $value) {
					$this->Acl->allow(ARO_ALIAS . $group_id, $aco);
				}
			}
		}

		$results = $this->Group->get_all_permissions();
		$obj_permissions = array();
		if (!empty($results)) {
			foreach ($results as $key => $value) {
				$obj_permissions[$key]['Child'] = new DtAco($value['Child']);
				$obj_permissions[$key]['AuthActionMap'] = new DtAuthActionMap($value['AuthActionMap']);
				$obj_permissions[$key]['Feature'] = new DtFeature($value['Feature']);
				$obj_permissions[$key]['Parent'] = new DtAco($value['Parent']);
			}
		}

		// permission allowed for given aros
		$allowed_permission = $this->_get_allowed_permission_by_list($group_id);

		// all permissions
		$permissions = array();
		if (!empty($obj_permissions)) {
			foreach ($obj_permissions as $key => $value) {
				$permissions[$value['Feature']->description][$key]['Child'] = $value['Child']->get_field();
				$permissions[$value['Feature']->description][$key]['Parent'] = $value['Parent']->get_field();
				$permissions[$value['Feature']->description][$key]['AuthActionMap']['id'] = $value['AuthActionMap']->id;
				
				if ($value['AuthActionMap']->is_desc_empty()) {
					$permissions[$value['Feature']->description][$key]['AuthActionMap']['description'] = $value['Child']->alias . ' -> ' . $value['Parent']->alias ;
				} else {
					$permissions[$value['Feature']->description][$key]['AuthActionMap']['description'] = $value['AuthActionMap']->get_formatted_desc();
				}
				$permissions[$value['Feature']->description][$key]['is_allowed'] = in_array($value['Child']->id, $allowed_permission);
			}
		}

		$this->set(compact('permissions'));
		$this->JCManager->add_css('admin/table_style');
	}

	/**
	 * Get allowed permission by list (1D array)
	 * 
	 * @param int $group_id Group id
	 */
	private function _get_allowed_permission_by_list($group_id) {

		$allowed_permission = $this->ArosAco->get_all_by_group_id($group_id);
		
		$allowed_permission_list = array();
		if ($allowed_permission) {
			foreach ($allowed_permission as $key => $value) {
				$obj_arosaco = new DtArosAco($value['ArosAco']);
				$allowed_permission_list[$key] = $obj_arosaco->aco_id;
			}
		}
		
		return $allowed_permission_list;
	}

}
