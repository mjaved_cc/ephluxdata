<?php
if (!empty($users)) {

	$users = json_decode($users); //pr($users); 
}

if (!empty($search_result)) {

	$users = json_decode($search_result); //pr($users); 
}
?>

<div class="ain_column">
	<div class="box">
		<div class="body">

			<div>

				<?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'search', 'class' => 'searchform')); ?>

				<p> <?php echo $this->Form->input('search', array('label' => 'Search User')); ?> </p>
				<p> <?php echo $this->Form->submit('Search'); ?> </p>

			</div>




			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

				<thead>
					<tr>
						<th><?php echo __('ID'); ?></th>
     	                  <th><?php echo __('Image'); ?></th>
						<th><?php echo __('Username'); ?></th>
						<th><?php echo __('Email'); ?></th>
						<th><?php echo __('Status'); ?></th>			
						<th><?php echo __('Created'); ?></th>			
					</tr>
				</thead>
				<tbody>
					<?php
					if (!empty($users->body)) {
						foreach ($users->body as $user): //pr($game);
							?>
							<tr>
								<td align="center"><?php echo $user->User->id ?>&nbsp;</td>
                                	<?php $image = $user->User->profile_image_url ?>
								<td align="center"><?php
						if (!empty($image)) {
							echo $this->Html->image($image, array('width' => '75px'));
						} else {
							echo 'No Image found';
						}
								?></td>

								<td align="center"><?php echo $this->Html->link(__($user->User->nickname), array('controller' => 'users', 'action' => 'details', $user->User->id));
							?>&nbsp;</td>

							
								<td align="center"><?php echo $user->User->email ?>&nbsp;</td>
								<td align="center"><?php
							$options = array('0' => 'Disable', '1' => 'Enable');
							echo $this->Form->input('status', array('options' => $options, 'default' => $user->User->status, 'label' => false, 'class' => 'user_status', 'id' => $user->User->id));
								?>&nbsp;</td>

								<td align="center"><?php echo $user->User->created ?>&nbsp;</td>

							</tr>

							<?php
						endforeach;
					} else {
						?>
						<tr>
							<td colspan="9">No User found.</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>






		</div>
	</div>
</div>

<script src=<?php echo $this->webroot . 'js/jquery_library.js'; ?>>
</script>
<script>
	
	$(document).ready(function(){
	
		$( ".user_status" ).change(function() {
			var status_val = $(this);
			var send_status = status_val.val();
			
			var send_id = $(this ).attr("id");
				
			
			$.ajax({				
				url: "<?php echo SITE_URI . '/users/update_status' ?>",
				
				type: "POST",
				
				data: {value_to_send: send_status, id_to_send: send_id},
				
				
				success: function(data) {
					
					alert(data);
				
				}
			});
		});
	});
 
</script>