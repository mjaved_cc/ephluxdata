<?php
if (!isset($request_variable)) {
	$request_variable = '';
}
if (isset($api_response)) {
	$response = json_decode($api_response);

	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}

	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>



<div class="box">
	<div class="header">
		<p><img src="<?php echo $this->webroot; ?>img/half_width_icon.png" alt="Half Width Box" width="30" height="30" />Jiggster - Reset Password</p>
	</div>
	<div class="body">
		<div class="logo_login"><img src="<?php echo $this->webroot; ?>img/logo_login.png" alt="Jiggster" width="200" height="78" /></div>

		<?php echo $this->Form->create('User', array('class' => 'form_login_signup')); ?>
		<p> <?php echo $this->Form->input('password', array('class' => 'textfield large', 'div' => false)); ?> </p>
		<p> <?php echo $this->Form->input('confirm_password', array('type' => 'password','class' => 'textfield large', 'label' => __('Confirm Password'), 'div' => false)); ?> </p>
		<p> <?php echo $this->Form->input('hidden', array('value' => $request_variable, 'type' => 'hidden', 'div' => false)); ?> </p>
		<p> <?php echo $this->Form->submit('Submit', array('class' => 'button2', 'div' => false)); ?> </p>
<div class="clearfix"></div>
        <?php $this->Form->end();?>
	</div>
</div>