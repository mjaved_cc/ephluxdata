<div class="authActionMaps form">
	<?php
	echo $this->Form->create('AuthActionMap');
	?>
	<fieldset>
		<legend><?php echo __('Permission'); ?></legend>		
		<div class="input text">
			<label><?php echo __('Controller'); ?></label>
			<label><?php echo $this->request->data['Controller']['alias']; ?></label>			
		</div>
		<div class="input text">
			<label><?php echo __('Action'); ?></label>
			<label><?php echo $this->request->data['Action']['alias']; ?></label>			
		</div>
		<div class="input text">
			<label><?php echo __('Right'); ?></label>
			<label><?php echo $this->request->data['AuthActionMap']['crud']; ?></label>			
		</div>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->hidden('Aco.id', array('value' => $this->request->data['Action']['id']));
		echo $this->Form->hidden('crud');
		echo $this->Form->input('Group.id', array('multiple' => 'checkbox', 'options' => $groups, 'selected' => $assigned_groups,'label' => __('Groups'), 'div' => false));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AuthActionMap.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AuthActionMap.id')));
	?></li>
		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Acos'), array('controller' => 'acos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('controller' => 'acos', 'action' => 'add')); ?> </li>
	</ul>
</div>
