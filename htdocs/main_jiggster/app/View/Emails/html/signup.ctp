<?php
//list($first_name, $ver_link) = explode('|', $message);
echo "    Hello, Welcome to Jiggster!<br/><br/>

We love video games and love trading them even more, and Jiggster is the place where you create your own deals, all around you, without ever getting ripped off.<br/><br/>

As we build our new Jiggster community of fun, like-minded people to play and trade with, please share Jiggster with your friends, and give us a Follow/Tweet <a href='https://twitter.com/jiggster_uk' target='_blank'>@Jiggster_UK</a> and 'Like' on our new <a href='https://www.facebook.com/JiggsterApp' target='_blank'>Facebook.com/JiggsterApp</a> for fun news, live meet-ups, competitions and more!<br/><br/>

Thanks again & and see you 'in-app'!<br/><br/>

Team Jiggster<br/><br/>

www.jiggster.com";
?>