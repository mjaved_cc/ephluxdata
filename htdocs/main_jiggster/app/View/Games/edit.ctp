<?php
$game = json_decode($game_detail);
//pr($game);
?>

<div class="main_column">
	<div class="box">
		<div class="body">

			<?php 
			
			$image = $game->body[0]->Game->image_url;
			$checked = '';
			
			?>

			<?php echo $this->Form->create('Game', array('controller' => 'games', 'action' => 'edit')); ?>

			<p> <?php echo $this->Form->input('id', array('type'=>'hidden','value' => $game->body[0]->Game->id)); ?> <p>
			<p> <?php echo $this->Form->input('title', array('label' => 'Title', 'value' => $game->body[0]->Game->title)); ?> <p>
			<p> <?php echo $this->Form->input('desc', array('label' => 'Description', 'value' => $game->body[0]->Game->description)); ?> <p>
			<p> <?php echo $this->Form->input('publisher', array('label' => 'Publisher', 'value' => $game->body[0]->Game->publisher)); ?> <p>
			<p> <?php echo $this->Form->input('developers', array('label' => 'Developers', 'value' => $game->body[0]->Game->developers)); ?> <p>
        	<p> <?php echo $this->Form->input('google_play', array('label' => 'Google Play', 'value' => $game->body[0]->Game->google_play)); ?> <p>
			<p> <?php echo $this->Form->input('amazon', array('label' => 'Amazon', 'value' => $game->body[0]->Game->amazon)); ?> <p>

				<?php
				if (!empty($image)) {
					echo '<p>'.$this->Html->image($image,array('width'=>'250px', 'class'=>'preview_img')).'</p>';
				} else {
					echo '<p> No Image found </p>';
				}
				?>
				
				<?php
				
				if($game->body[0]->Game->jiggster_pick == 1) {
					
					$checked = 'checked';					
				}		
				
				?>
				
				<p> <?php echo $this->Form->input('jiggster_pick', array('label' => 'Jiggster Pick', 'type' => 'checkbox', 'checked' => $checked)); ?> <p>

			<p>  <?php echo $this->Form->submit('update'); ?> </p>


		</div>
	</div>
</div>