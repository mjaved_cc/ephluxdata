

<?php
//pr($games); exit;
//if (!empty($search_result)) {
//
//	$search_result = json_decode($search_result);
//}
if (!empty($status)) {

	echo $status;
}
?>

<div class="main_column">
	<div class="box">
		<div class="body">

			<div>

				<?php echo $this->Form->create('Game', array('controller' => 'games', 'action' => 'search','class' => 'searchform')); ?>

				<p> <?php echo $this->Form->input('search', array('label' => 'Search Game')); ?> </p>
				<p> <?php echo $this->Form->submit('Search'); ?> </p>
<div class="clearfix"></div>
			</div>

            
			<?php if (!empty($search_result)) { ?>

				<h2>Search Results</h2>			

				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

					<?php $pagination_params = $this->Paginator->params(); ?>

					<thead>
						<tr>
							
							<th><?php echo __('Image'); ?></th>
                            <th><?php echo __('Title'); ?></th>
							<th><?php echo __('Status'); ?></th>
							<th><?php echo __('Action'); ?></th>			
						</tr>
					</thead>
					<tbody>
						<?php
						if (!empty($search_result)) {
							foreach ($search_result as $search): //pr($game);
								?>
								<tr>
								
									<?php $image =  $search['Game']['image_url'] ?>
									<td align="center"><?php
						if (!empty($image)) {
							echo $this->Html->image($image, array('width' => '75px'));
						} else {
							echo 'No Image found';
						}
									?></td>
                                    	<td align="center"><?php echo $search['Game']['title'] ?>&nbsp;</td>
									<td align="center"><?php
							if ($search['Game']['status'] == 1) {
								echo 'Enable';
							} else {
								echo 'Disable';
							}
									?>&nbsp;</td>

									<td class="actions" align="center">		
										<?php echo $this->Html->link(__('View'), array('controller' => 'games', 'action' => 'view', $search['Game']['id'])); ?> |
										<?php echo $this->Html->link(__('Delete'), array('controller' => 'games', 'action' => 'delete', $search['Game']['id'])); ?>
									</td>
								</tr>

								<?php
							endforeach;
						} else {
							?>
							<tr>
								<td colspan="9">No result found.</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>

				<?php echo $this->element('pagination', array('pagination_params' => $pagination_params)); ?>


			<?php } else { ?>

				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

					<?php $pagination_params = $this->Paginator->params(); ?>

					<thead>
						<tr>
                        	<th><?php echo __('Image'); ?></th>
							<th><?php echo __('Title'); ?></th>
						      <th><?php echo __('Status'); ?></th>
							<th><?php echo __('Action'); ?></th>			
						</tr>
					</thead>
					<tbody>
						<?php
						if (!empty($games)) {
							foreach ($games as $game): //pr($game);
								?>
								<tr>
									
									<?php $image = $game['Game']['image_url'] ?>
									<td align="center"><?php
						if (!empty($image)) {
							echo $this->Html->image($image, array('width' => '75px'));
						} else {
							echo 'No Image found';
						}
									?></td>
                                    <td align="center"><?php echo $game['Game']['title'] ?>&nbsp;</td>
									<td align="center"><?php
							if ($game['Game']['status'] == 1) {
								$select = 'Enable';
							} else {
								$select = 'Disable';
							}
							$options = array('0' => 'Disable', '1' => 'Enable');
							echo $this->Form->input('status', array('options' => $options, 'default' => $game['Game']['status'], 'label' => false, 'class' => 'game_status', 'id' => $game['Game']['id']));
									?>&nbsp;</td>

									<td class="actions" align="center">		
										<?php echo $this->Html->link(__('View'), array('controller' => 'games', 'action' => 'view', $game['Game']['id'])); ?> |
										<?php echo $this->Html->link(__('Edit'), array('controller' => 'games', 'action' => 'edit', $game['Game']['id'])); ?> |
										<?php echo $this->Html->link(__('Delete'), array('controller' => 'games', 'action' => 'delete', $game['Game']['id']), null, 'Are you sure'); ?> 

									</td>
								</tr>

								<?php
							endforeach;
						} else {
							?>
							<tr>
								<td colspan="9">No Games found.</td>
							</tr>
							<?php
						}
						?>
					</tbody> 			

				</table>
				<?php echo $this->element('pagination', array('pagination_params' => $pagination_params)); ?>
			<?php } ?>

		</div>
	</div>
</div>
<script src=<?php echo $this->webroot . 'js/jquery_library.js'; ?>>
</script>
<script>
	
	$(document).ready(function(){
		
		
	
		$( ".game_status" ).change(function() {
			var status_val = $(this);
			var send_status = status_val.val();
			
			var send_id = $(this ).attr("id");
				
			
			$.ajax({				
				url: "<?php echo SITE_URI . '/games/update_status' ?>",
				
				type: "POST",
				
				data: {value_to_send: send_status, id_to_send: send_id},
				
				
				success: function(data) {
					
					alert(data);
				
				}
			});
		});
		
		
		$("#paginator").click(function(){
		
			$("#GameSearchForm").submit();
//			var abc = $("#GameSearch").val();
//			alert(abc);
//			
//			
//			$.ajax({				
//				url: "<?php //echo SITE_URI . '/games/search' ?>",
//				
//				type: "POST",
//				
//				data: {Game: abc},
//				
//				
//				success: function(data) {
//					
//					alert(data);
//				
//				}
//			});

			
			
		});
		
				
	});
	
	
 
</script>
