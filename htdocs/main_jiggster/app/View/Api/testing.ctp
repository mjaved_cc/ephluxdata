<html>
	<head>	

	</head>
	<body>

		<h1>List of webservices.</h1>
		<ol>
			<li><a href="#forgot_password">forgot_password</a></li>
			<li><a href="#login">login</a></li>
			<li><a href="#signup">signup</a></li>
			<li><a href="#detail_by_id">detail_by_id</a></li>
			<li><a href="#detail_by_barcode">detail_by_barcode</a></li>
			<li><a href="#detail_by_title">detail_by_title</a></li>
			<li><a href="#what_i_have">what_i_have</a></li>
			<li><a href="#console_get">console_get</a></li>
			<li><a href="#genre_get">genre_get</a></li>
			<li><a href="#console_save">console_save</a></li>
			<li><a href="#genre_save">genre_save</a></li>
			<li><a href="#all_games">all_games</a></li>
			<li><a href="#save_record">save_record</a></li>
			<li><a href="#save_wishlist">save_wishlist</a></li>
			<li><a href="#make_offer">make_offer</a></li>
			<li><a href="#wishlist_user_all">wishlist_user_all</a></li>
			<li><a href="#wishlist_user_all">signup_social</a></li>
			<li><a href="#owned_by_users">owned_by_users</a></li>
			<li><a href="#wished_by_users">wished_by_users</a></li>
			<li><a href="#most_wanted">most_wanted</a></li>
			<li><a href="#most_owned">most_owned</a></li>        
			<li><a href="#upcoming_new_releases">upcoming_new_releases</a></li> 
			<li><a href="#save_chatlog">save_chatlog</a></li>  
			<li><a href="#unread_message_count">unread_message_count</a></li>
			<li><a href="#chat">chat</a></li>
			<li><a href="#save_profile_info">save_profile_info</a></li>
			<li><a href="#get_profile_info">get_profile_info</a></li>  
			<li><a href="#save_profile_image">save_profile_image</a></li>      
			<li><a href="#selling_game">selling_game</a></li>
			<li><a href="#buying_game">buying_game</a></li> 
			<li><a href="#trading_game">trading_game</a></li>     
			<li><a href="#for_you">for_you</a></li>
			<li><a href="#all_game">all_game</a></li>  
			<li><a href="#ongoing_offers_for_wish_game">ongoing_offers_for_wish_game</a></li> 
			<li><a href="#ongoing_offers_for_have_game">ongoing_offers_for_have_game</a></li>  
			<li><a href="#current_deals">current_deals</a></li>
			<li><a href="#send_receive_deals">send_receive_deals</a></li>
			<li><a href="#send_receive_action">send_receive_action</a></li>
			<li><a href="#user_current_deal_on_specific_game">user_current_deal_on_specific_game</a></li>
			<li><a href="#user_update_have_game">user_update_have_game</a></li>

		</ol>


		<hr/>
		<h1><a id="forgot_password">forgot_password</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/forgot_password.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"fahad.abdullah@ephlux.com"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"fahad.abdullah@ephlux.com"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

		<hr/>
		<h1><a id="login">login</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/login.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"fahad.abdullah@ephlux.com","password":"12345678","gcm_regid":"1"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"fahad.abdullah@ephlux.com","password":"12345678","gcm_regid":"1"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="signup">signup</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/signup.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"admin@abc.com","password":"12345678","nickname":"abc","dob":"12345","gcm_regid":"123456","latitude":"0","longitude":"0"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","email":"admin@abc.com","password":"12345678","nickname":"abc","dob":"","gcm_regid":"123456","latitude":"0","longitude":"0"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="detail_by_id">detail_by_id</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/detail_by_id.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":"39","game_id":"10"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":"39","game_id":"10"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="detail_by_barcode">detail_by_barcode</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/detail_by_barcode.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":"39","game_barcode":"0045496901080"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":"39","game_barcode":"0045496901080"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="detail_by_title">detail_by_title</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/detail_by_title.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"119","start":"0","game_title":"gra","end":"25","key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"119","start":"0","game_title":"gra","end":"25","key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="what_i_have">what_i_have</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/what_i_have.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
		<h1><a id="console_get">console_get</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>console/api/get.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="genre_get">genre_get</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>genre/api/get.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="console_save">console_save</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>console/api/save.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","console_id":"5"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","console_id":"5"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

		<hr/>
        <h1><a id="genre_save">genre_save</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>genre/api/save.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","genre_id":"5"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","genre_id":"5"}}</span>				<br/>



				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="all_games">all_games</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/all_games.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="save_record">save_record</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/save_record.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"amount":"9.0","status":"1","condition":"new","trade":"sell","game_id":"13602","user_id":"39","comment":"","comes_with":"box + dvd","key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"amount":"9.0","status":"1","condition":"new","trade":"sell","game_id":"13602","user_id":"39","comment":"","comes_with":"box + dvd","key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="save_wishlist">save_wishlist</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/save_wishlist.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"trade":"sell","game_id":"13602","user_id":"39","key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"trade":"sell","game_id":"13602","user_id":"39","key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>


        <h1><a id="make_offer">make_offer</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>deal/api/make_offer.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"amount":"63.0","member_trade":"exchange","what_i_have":"6","action_taken":"created","user_id":"39","user_trade":"exchange","comment":"","deal_id":"0","is_ensurend":"0","what_i_want":"9","key":"a45f8702297474d9a80ff36cb6a48200","action_taken_by":"39","member_id":"62"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"amount":"63.0","member_trade":"exchange","what_i_have":"6","action_taken":"created","user_id":"39","user_trade":"exchange","comment":"","deal_id":"0","is_ensurend":"0","what_i_want":"9","key":"a45f8702297474d9a80ff36cb6a48200","action_taken_by":"39","member_id":"62"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="wishlist_user_all">wishlist_user_all</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>wishlist/api/user_all.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"39","key":"a45f8702297474d9a80ff36cb6a48200"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"39","key":"a45f8702297474d9a80ff36cb6a48200"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="signup_social">signup_social</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/signup_social.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"ip_address":"192.175.0.72","access_token_secret":"ya29.1.AADtN_Wzgnnj2vWTTLJMa-SKWI9el4D1l26ub-V2-lpC9kSeZbm1t76C7tnlVYPIL4lbmAA","image_url":"https:\/\/lh3.googleusercontent.com\/-XdUIqdMkCWA\/AAAAAAAAAAI\/AAAAAAAAAAA\/4252rscbv5M\/photo.jpg?sz=50","screen_name":"Testid Jigg","link_id":"105457163842086201916","access_token":"ya29.1.AADtN_Wzgnnj2vWTTLJMa-SKWI9el4D1l26ub-V2-lpC9kSeZbm1t76C7tnlVYPIL4lbmAA","type_id":"google_plus","key":"a45f8702297474d9a80ff36cb6a48200","email_address":"ephluxqa87@gmail.com","latitude":"47","longitude":"50"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"ip_address":"192.175.0.72","access_token_secret":"ya29.1.AADtN_Wzgnnj2vWTTLJMa-SKWI9el4D1l26ub-V2-lpC9kSeZbm1t76C7tnlVYPIL4lbmAA","image_url":"https:\/\/lh3.googleusercontent.com\/-XdUIqdMkCWA\/AAAAAAAAAAI\/AAAAAAAAAAA\/4252rscbv5M\/photo.jpg?sz=50","screen_name":"Testid Jigg","link_id":"105457163842086201916","access_token":"ya29.1.AADtN_Wzgnnj2vWTTLJMa-SKWI9el4D1l26ub-V2-lpC9kSeZbm1t76C7tnlVYPIL4lbmAA","type_id":"google_plus","key":"a45f8702297474d9a80ff36cb6a48200","email_address":"ephluxqa87@gmail.com","latitude":"47","longitude":"50"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="owned_by_users">signup_social</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/owned_by_users.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"game_id":"5","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"game_id":"5","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="wished_by_users">wished_by_users</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/wished_by_users.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"game_id":"5","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"game_id":"5","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

		<hr/>
        <h1><a id="most_wanted">most_wanted</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/most_wanted.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"console_ids":"4,5,6"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"console_ids":"4,5,6"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

		<hr/>
        <h1><a id="most_owned">most_owned</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/most_wanted.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"console_ids":"4,5,6"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"console_ids":"4,5,6"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="upcoming_new_releases">upcoming_new_releases</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/upcoming_new_releases.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"search_param":"2"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":0,"start":0,"end":25,"search_param":"2"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="save_chatlog">save_chatlog</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/save_chatlog.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_sender":21,"id_receiver":39,"user_message":"save this test chatlog"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_sender":21,"id_receiver":39,"user_message":"save this test chatlog"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="unread_message_count">unread_message_count</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/unread_message_count.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_user":21,"id_receiver":39}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_user":21,"id_receiver":39}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="chat">chat</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/chat.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_sender":21,"id_receiver":39}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_sender":21,"id_receiver":39}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="mark_message_read">mark_message_read</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/mark_message_read.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_chatlog":5,"id_receiver":39}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","id_chatlog":5,"id_receiver":39}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

        <hr/>
        <h1><a id="save_profile_info">save_profile_info</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/save_profile_info.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"status":"details","location":"karachi","gaming_id":"num2022","nickname":"numair qadir","zipcode":"36","state":"","address_1":"3","address_2":"3","contact":"421013333","country":"pakistan","city":"karachi","shipping_address":"business","username":"numair qadir","paypal_id":"","user_id":"6","profile_text":"","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"status":"details","location":"karachi","gaming_id":"num2022","nickname":"numair qadir","zipcode":"36","state":"","address_1":"3","address_2":"3","contact":"421013333","country":"pakistan","city":"karachi","shipping_address":"business","username":"numair qadir","paypal_id":"","user_id":"6","profile_text":"","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="get_profile_info">get_profile_info</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/get_profile_info.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"6","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"6","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="save_profile_image">save_profile_image</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/save_profile_image.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"6","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<label>file</label>
				<input type="file" name="image"/>
				<br/>
				<span>{"body":{"user_id":"6","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="selling_game">selling_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/selling_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","game_id":"5","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","game_id":"5","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="buying_game">buying_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/buying_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","game_id":"5","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","game_id":"5","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="trading_game">trading_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/trading_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}'/>
				<br/>
				<span>{"header":{"code":0,"message":"success"},"body":{"user_id":"21","start":"0","end":"25","key":"a45f8702297474d9a80ff36cb6a48200","dist":"1000"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="for_you">for_you</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>game/api/for_you.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"genre_ids":"","console_ids":"","user_id":"12","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"genre_ids":"","console_ids":"","user_id":"12","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="all_game">all_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/all_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="ongoing_offers_for_wish_game">ongoing_offers_for_wish_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/ongoing_offers_for_wish_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","game_id":"21"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","game_id":"21"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="ongoing_offers_for_have_game">ongoing_offers_for_have_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/ongoing_offers_for_have_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","game_id":"21"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200","game_id":"21"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		<hr/>
        <h1><a id="current_deals">current_deals</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/current_deals.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="send_receive_deals">send_receive_deals</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/send_receive_deals.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
        <hr/>
        <h1><a id="send_receive_action">send_receive_action</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/send_receive_action.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":3, "deal_id": 3, 
					   "receive_action":1, "send_action":1, "action" : "send_confirm"}}'/>
				<br/>
				<span>{header":{"code":0,"message":"success"},"body":{"key":"a45f8702297474d9a80ff36cb6a48200","user_id":3, "deal_id": 3, 
					"receive_action":1, "send_action":1, "action" : "send_confirm"}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>

		<h1><a id="user_current_deal_on_specific_game">user_current_deal_on_specific_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/current_deal_on_specific_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","game_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","game_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>
		
		<h1><a id="user_update_have_game">user_update_have_game</a></h1> 
		<form target="_blank" action="<?php echo SITE_URI ?>user/api/current_deal_on_specific_game.json" enctype="multipart/form-data" method="post" id ="getAssestCommnet">
			<fieldset>	
				<label>data</label>
				<input type="text" name="data" style="width:800px"  value='{"body":{"user_id":"21","game_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}'/>
				<br/>
				<span>{"body":{"user_id":"21","game_id":"21","key":"a45f8702297474d9a80ff36cb6a48200"},"header":{"message":"success","code":0}}</span>				<br/>

				<p><input name="" type="submit" class="button2" value="Submit" />

				</p>
			</fieldset>
		</form>