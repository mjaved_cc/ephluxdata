<div class="features form">
<?php echo $this->Form->create('Feature'); ?>
	<fieldset>
		<legend><?php echo __('Add Feature'); ?></legend>
	<?php
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Features'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('controller' => 'auth_action_maps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Auth Action Map'), array('controller' => 'auth_action_maps', 'action' => 'add')); ?> </li>
	</ul>
</div>
