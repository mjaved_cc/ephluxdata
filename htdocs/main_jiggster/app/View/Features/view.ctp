<div class="features view">
<h2><?php  echo __('Feature'); ?></h2>
	<dl>		
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($feature['Feature']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($feature['Feature']['created']); ?>
			&nbsp;
		</dd>		
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Feature'), array('action' => 'edit', $feature['Feature']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Feature'), array('action' => 'delete', $feature['Feature']['id']), null, __('Are you sure you want to delete # %s?', $feature['Feature']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Features'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Feature'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('controller' => 'auth_action_maps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Auth Action Map'), array('controller' => 'auth_action_maps', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Auth Action Maps'); ?></h3>
	<?php if (!empty($feature['AuthActionMap'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($feature['AuthActionMap'] as $authActionMap): ?>
		<tr>
			<td><?php echo $authActionMap['description']; ?></td>
			<td><?php echo $authActionMap['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'auth_action_maps', 'action' => 'view', $authActionMap['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'auth_action_maps', 'action' => 'edit', $authActionMap['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'auth_action_maps', 'action' => 'delete', $authActionMap['id']), null, __('Are you sure you want to delete # %s?', $authActionMap['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Auth Action Map'), array('controller' => 'auth_action_maps', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
