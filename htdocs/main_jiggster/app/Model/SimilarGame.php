<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class SimilarGame extends AppModel {

	function save_similar_games($data) {

		foreach ($data as $key => $value) {

			$exitst = $this->query("call get_similar_games('" . $value['TitleID'] . "', '" . $value['SimilarTitleID'] . "')");

			if (empty($exitst)) {
				$this->query("call save_similar_games('" . $value['TitleID'] . "', '" . $value['SimilarTitleID'] . "')");
			}
		}
	}

}
