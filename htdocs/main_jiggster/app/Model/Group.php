<?php

App::uses('AppModel', 'Model');

/**
 * Group Model
 *
 * @property User $User
 */
class Group extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field is required',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	public $hasOne = array(
		'Aro' => array(
			'className' => 'Aro',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => array('id', 'alias'),
			'order' => ''
		)
	);
	public $actsAs = array('Acl' => array('type' => 'requester'));

	public function parentNode() {
		return null;
	}

	/**
	 * Get all records 
	 * 
	 * @return array groups
	 */
	function getAll() {

		$this->unbindModel(array(
			'hasMany' => array('User')
		));

		return $this->find('list');
	}

	function get_all_permissions() {
		$result = $this->query('call get_all_permissions()');

		if (!empty($result))
			return $result;

		return false;
	}

	/**
	 * Get details for given id
	 * 
	 * @param int|string $group_id Feature id
	 */
	function get_details_by_id($group_id) {
		$result = $this->query('call get_group_details_by_group_id("' . $group_id . '")');

		if (!empty($result))
			return $result;
		return false;
	}
	
	/**
	 * Create Group
	 * 
	 * @param array $data
	 */
	function create($data) {

		$this->query('call create_group(
			"' . $data['name'] . '",
			"' . $data['created'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}
	
	/**
	 * Update group
	 * 
	 * @param array $data id & description
	 */
	function update($data) {

		$this->query('call update_group(
			"' . $data['id'] . '",
			"' . $data['name'] . '",
			"' . $data['modified'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}
	
	/**
	 * Get details for given id. Its extracting data from groups table only.
	 * 
	 * @param int|string $group_id Group id
	 */
	function get_by_id($group_id) {
		$result = $this->query('call get_group_by_group_id("' . $group_id . '")');

		if (!empty($result))
			return $result[0];
		return false;
	}
	
	/**
	 * Remove group by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		$this->query('call remove_group(
			"' . $data['id'] . '",
			"' . $data['modified'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}

}
