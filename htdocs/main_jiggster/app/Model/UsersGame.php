<?php



App::uses('AppModel', 'Model');



/**

 * User Model

 *

 */

class UsersGame extends AppModel {



	function get_all_games_by_user_id($user_id) {



		App::uses('DtGame', 'Lib/DataTypes');

		App::uses('DtUserGame', 'Lib/DataTypes');

		App::uses('DtGenre', 'Lib/DataTypes');



		$games = $this->query("call get_all_games_by_user_id('" . $user_id . "' )"); //pr($games); exit;



		$data = array();



		if (!empty($games)) {



			foreach ($games as $key => $value) {



				$obj_game = new DtGame($value['games']);

				$obj_game->add_user_game($value['users_games']);

				$obj_game->add_genre($value['genres']);



				$data[$key]['Game'] = $obj_game->get_field();

				$data[$key]['UserGame'] = $obj_game->get_user_game();

				$data[$key]['Genre'] = $obj_game->get_genre();

			}

		}



		return $data;

	}



	function save_user_game_record($data) {



		$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $data['user_id'] . "', '" . $data['game_id'] . "')");


		if($data['trade'] === 'trade'):
			
			$game = $this->query("call get_game_srp('" . $data['game_id'] . "')");
			
			$data['amount'] = $game[0]['Game']['srp']; 
			
		endif;
		
		
		if ($existance[0][0]['UserGame'] == 0 && $existance[0][0]['UserWish'] == 0) {

			$this->query("call save_user_game_record(

			   '" . $data['user_id'] . "',

			   '" . $data['game_id'] . "',

			   '" . $data['trade'] . "',

			   '" . $data['condition'] . "',
				   
			   '" . $data['amount'] . "',

			   '" . $data['comes_with'] . "',

			   '" . $data['comment'] . "',

				   @status	 
			)");

		}
		
		return $this->get_status();

	}



	function users_who_sell_specific_game($game_id) {



		App::uses('DtUserGame', 'Lib/DataTypes');

		App::uses('DtGame', 'Lib/DataTypes');



		$users = $this->query("call users_who_posess_specific_game( '" . $game_id . "')");



		$data = array();



		if (!empty($users)) {



			foreach ($users as $key => $value) {



				$obj_game = new DtUserGame($value['UserGame']);

				$obj_game->add_user($value['Users']);



				$data[$key]['UserGame'] = $obj_game->get_field();

				$data[$key]['User'] = $obj_game->get_user();

				$data[$key]['MemberSince'] = $value[0]['MemberSince'];

				$data[$key]['GameOwned'] = $value[0]['game_owned'];

			}

		}



		return $data;

	}



	function users_who_buy_specific_game($game_id) {



		App::uses('DtUserWishlist', 'Lib/DataTypes');



		$users = $this->query("call users_who_wish_specific_game( '" . $game_id . "')");



		$data = array();



		if (!empty($users)) {



			foreach ($users as $key => $value) {



				$obj_game = new DtUserWishlist($value['UserWishlist']);

				$obj_game->add_user($value['Users']);



				$data[$key]['UserWishlist'] = $obj_game->get_field();

				$data[$key]['User'] = $obj_game->get_user();

				$data[$key]['MemberSince'] = $value[0]['MemberSince'];

				$data[$key]['GameOwned'] = $value[0]['game_owned'];

			}

		}



		return $data;

	}
	
	function update_user_game_record($data) { //pr($data); exit;
		
		if($data['trade'] === 'trade'):
			
			$game = $this->query("call get_game_srp('" . $data['game_id'] . "')");
			
			$data['amount'] = $game[0]['Game']['srp']; 
			
		endif;
		
		
		$this->query("call update_user_game_record(
			
			   '" . $data['user_id'] . "',
			   '" . $data['game_id'] . "',
			   '" . $data['amount'] . "',
			   '" . $data['trade'] . "',
			   '" . $data['condition'] . "',
			   '" . $data['comes_with'] . "',
			   '" . $data['comment'] . "'	 

			)");
		
		return 'success';
	}
	
	
	function delete_have_or_wish($user_id, $game_id, $game_type) {
		
		if($game_type == 'wishlist'){
			
			$this->query("call delete_wish_or_have_game('" . $user_id . "','" . $game_id . "','" . $game_type . "')");
			
			return 'success';
		}
		
		if($game_type == 'my_game'){
			
			$this->query("call delete_wish_or_have_game('" . $user_id . "','" . $game_id . "','" . $game_type . "')");
			
			return 'success';
		}
		
	}



//	function get_all_games_by_user_id($user_id) {

//

//		App::uses('DtUserGame', 'Lib/DataTypes');

//		

//		$games = $this->query("call get_all_games_by_user_id(

//			 '" . $user_id . "' )");

//

//		$data = array();

//

//		if (!empty($games)) {

//

//			foreach ($games as $key => $value) {

//				

//				$obj_game = new DtUserGame($value['games']);

//				

//				$data['games'] = $obj_game->get_field();

//			}

//			

//			//pr ($data); exit;

//		}

//

//		return $data;

//	}

}

