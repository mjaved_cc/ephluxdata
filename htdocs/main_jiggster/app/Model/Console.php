<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class Console extends AppModel {

	function get_all_console() {

		App::uses('DtConsole', 'Lib/DataTypes');

		$consoles = $this->query("call get_all_console()");

		$data = array();

		if (!empty($consoles)) {

			foreach ($consoles as $key => $value) {

				$obj_console = new DtConsole($value['Console']);

				$data[$key]['Console'] = $obj_console->get_field();
			}
		}

		return $data;
	}

	function save_console($data) {

		foreach ($data as $value) {

			$name = Sanitize::escape($value);
			$console = $this->query("call check_genre_or_console(NULL, '" . $name. "')");

			if (empty($console)) {

				$this->query("call console_cron( '" . $name . "' )");
			}
		}
	}

}
