<?php

App::uses('AppModel', 'Model');

/**

 * User Model

 *

 */
class WallPaper extends AppModel {

    function get_all($offset , $limit){

        App::uses('DtWallPaper', 'Lib/DataTypes');

        $wall_papers = $this->query('CALL get_all_wall_papers(
            "' . $offset . '" ,
             "' . $limit . '"
            )');

        $data = array();

        if(is_array($wall_papers) && !empty($wall_papers)){
            foreach($wall_papers as $key => $single_wp){
                $obj_wall_paper = new DtWallPaper($single_wp['WallPaper']);
                $data[$key] = $obj_wall_paper->get_field();
            }
        }

        return $data ;
    }

}