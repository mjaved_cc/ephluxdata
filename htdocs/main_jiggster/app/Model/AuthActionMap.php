<?php

App::uses('AppModel', 'Model');

/**
 * AuthActionMap Model
 *
 * @property Aco $Aco
 */
class AuthActionMap extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'aco_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Action' => array(
			'className' => 'Aco',
			'foreignKey' => 'aco_id',
			'conditions' => '',
			'fields' => array('id', 'alias'),
			'order' => ''
		),
		'Feature' => array(
			'className' => 'Feature',
			'foreignKey' => 'feature_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasOne = array(
		'Controller' => array(
			'className' => 'Aco',
			'foreignKey' => false,
			'conditions' => 'Action.parent_id = Controller.id',
			'fields' => array('id', 'alias'),
			'order' => ''
		)
	);

	function get_assigned_groups($aco_id) {

		return $this->query(
				'SELECT Group.id,Group.name 
				FROM aros_acos AS ArosAco
				LEFT JOIN aros AS Aro ON Aro.id = ArosAco.aro_id
				LEFT JOIN groups AS `Group` ON Group.id = Aro.foreign_key
				WHERE ArosAco.aco_id = ' . $aco_id
		);
	}

}
