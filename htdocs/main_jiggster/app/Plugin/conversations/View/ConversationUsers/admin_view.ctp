<div class="conversationUsers view">
<h2><?php  echo __('Conversation User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($conversationUser['ConversationUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Conversation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($conversationUser['Conversation']['title'], array('controller' => 'conversations', 'action' => 'view', $conversationUser['Conversation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($conversationUser['User']['username'], array('controller' => 'users', 'action' => 'view', $conversationUser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($conversationUser['ConversationUser']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last View'); ?></dt>
		<dd>
			<?php echo h($conversationUser['ConversationUser']['last_view']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($conversationUser['ConversationUser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Folder'); ?></dt>
		<dd>
			<?php echo h($conversationUser['ConversationUser']['folder']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Conversation User'), array('action' => 'edit', $conversationUser['ConversationUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Conversation User'), array('action' => 'delete', $conversationUser['ConversationUser']['id']), null, __('Are you sure you want to delete # %s?', $conversationUser['ConversationUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversation Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversations'), array('controller' => 'conversations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation'), array('controller' => 'conversations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
