<?php
require_once "Games.php";
require_once "MuzeGames.php";
//require_once ("db.mysql.php");
/**
 * @author fahad abdullah
 * @copyright 2014
 */
$sqlhost = "localhost";
$sqlusername = "root";
$sqlpassword = "";
$sqldatabase = "jig_db";
$ftppath = "/GamesUKdaily/";
$ftphost = "FTP.muze.com";
$ftpusername = "jiggster";
$ftppass = "h2Rtc7s2";
$updatefile = 'dgascii.zip';
$coverimagefile='latestgm.zip';
$ftp_cover_imagepath="/CovergUKdaily/".$coverimagefile;
$logs = "";

$curl = curl_init();
$file = fopen("dgascii.zip", 'w');
curl_setopt($curl, CURLOPT_URL, $ftphost . $ftppath . $updatefile); #input
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FILE, $file); #output
curl_setopt($curl, CURLOPT_USERPWD, "$ftpusername:$ftppass");
curl_exec($curl);
curl_close($curl);
fclose($file);
if ($file) {
    $zip = new ZipArchive;
    if ($zip->open($updatefile) === true) {
        $zip->extractTo('muze_game_data/');
        $zip->close();
        $logs .= "upzipped ok........... \n";
    } else {
        $logs .= "upzipped failed........... \n";
    }

}
$curl = curl_init();
$file = fopen("latestgm.zip", 'w');
curl_setopt($curl, CURLOPT_URL, $ftphost.$ftp_cover_imagepath); #input
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FILE, $file); #output
//curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,0);
//curl_setopt($curl,CURLOPT_TIMEOUT,3000);
curl_setopt($curl, CURLOPT_USERPWD, "$ftpusername:$ftppass");
curl_exec($curl);
curl_close($curl);
fclose($file);

$unzippath=$_SERVER['DOCUMENT_ROOT'].dirname($_SERVER['PHP_SELF']);
$unzippath=str_replace('muze_cron_job','',$unzippath);
$unzippath=str_replace('//','/',$unzippath);

if ($file) {
    $zip = new ZipArchive;
    if ($zip->open($coverimagefile) === true) {
        $zip->extractTo($unzippath.'app/webroot/muze_games/');
        $zip->close();
        $logs .= "upzipped ok........... \n";
    } else {
        $logs .= "upzipped failed........... \n";
    }

}
$obj = new MuzeGames();
$data = $obj->getGames();
save_game($data);
function save_game($data)
{

    global $sqlhost, $sqlusername, $sqlpassword, $sqldatabase;
    $dblink = mysql_connect($sqlhost, $sqlusername, $sqlpassword);
    $dsn = "mysql:dbname=$sqldatabase;host=$sqlhost";

    try {
        $dpdo = new PDO($dsn, $sqlusername, $sqlpassword);
    }
    catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
    foreach ($data as $gameArray) {
        $api = $gameArray->api;
        $title = mysql_real_escape_string($gameArray->title, $dblink);
        $titleID = mysql_real_escape_string($gameArray->titleID, $dblink);
        $desc = mysql_real_escape_string($gameArray->desc, $dblink);
        $console = mysql_real_escape_string($gameArray->console, $dblink);
        $console_id = "";
        if ($console) {
            foreach ($dpdo->query("SELECT id FROM consoles WHERE consoles.console = '" . $console .
                "'") as $row) {

                $console_id = $row['id'];

            }

        }
        $genre = $gameArray->genre;
        $genre_id = "";
        if ($genre) {
            foreach ($dpdo->query("SELECT id FROM genres WHERE genres.genre = '" . $genre .
                "'") as $row) {
                $genre_id = $row['id'];

            }
        }
        $screenshot = $gameArray->screenshot;
        $publisher = mysql_real_escape_string($gameArray->publisher, $dblink);
        $developers = mysql_real_escape_string($gameArray->developers, $dblink);
        $barcode = $gameArray->barcode;
        $image = $gameArray->image;
        $releaseDate = date("Y-m-d H:m:s", $gameArray->releaseDate);
        $api_id = null;

        foreach ($dpdo->query("SELECT api FROM games WHERE api = '" . $api . "'") as $row) {

            $api_id = $row[0];

        }


        //pr($console_id);

        $updatequery = "";
        if ($api_id === null) {
            // echo "insert\n";
            $insertquery = "call game_cron('" . $api . "',
            '" . $title . "',
            '" . $titleID . "',
            '" . $desc . "',
            '" . $console_id . "',
            '" . $genre_id . "',
            '" . $publisher . "',
            '" . $developers . "',
            '" . $barcode . "',
            '" . $image . "',
            '" . $screenshot . "',
            '" . $releaseDate . "'
            )";
            // echo "\n\n<br/>".$insertquery."\n\n<br/>";
            $stmt = $dpdo->prepare($insertquery);
            $stmt->execute();
            $insert_id = $dpdo->lastInsertId();
            $logs .= date("H:i:s.u d.m.Y") . " Game is inserted of api id:" . $api . "\n";
        } else {
            $updatequery = " update `games` set ";

            if ($title) {

                $updatequery .= " title='" . $title . "',";

            }
            if ($titleID) {

                $updatequery .= " titleID='" . $titleID . "',";

            }
            if ($desc) {
                $updatequery .= " `desc`='" . $desc . "',";

            }
            if ($console_id) {
                $updatequery .= " console_id='" . $console_id . "',";
            }
            if ($genre_id) {
                $updatequery .= " genre_id='" . $genre_id . "',";
            }
            if ($publisher) {
                $updatequery .= " publisher='" . $publisher . "',";
            }
            if ($developers) {
                $updatequery .= " developers='" . $developers . "',";
            }
            if ($barcode) {
                $updatequery .= " barcode='" . $barcode . "',";
            }
            if ($image) {
                $updatequery .= " image_url='" . $image . "',";
            }
            if ($screenshot) {
                $updatequery .= " screenshot='" . $screenshot . "',";

            }
            // echo "<br/>" . $updatequery . "<br/>";
            $updatequery = substr($updatequery, 0, strlen($updatequery) - 1);
            //   echo "<br/>" . $updatequery . "<br/>";
            $updatequery = $updatequery . " where api=$api";
            // echo "<br/>" . $updatequery . "\n\n<br/>";
            $stmt = $dpdo->prepare($updatequery);
            $stmt->execute();
            $logs .= date("H:i:s.u d.m.Y") . " Game is updated of api id :" . $insert_id . "$api \n";
        }
    }
    $logs .= "\n\n--------------------------Completed---------------------------------------------\n\n";

    error_log($logs, 3, 'cron_logs.txt');
}
