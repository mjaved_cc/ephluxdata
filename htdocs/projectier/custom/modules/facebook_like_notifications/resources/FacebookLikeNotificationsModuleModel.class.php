<?php

  // Include applicaiton specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';
	
  /**
   * NotificationPlus module model definition
   *
   * @package custom.modules.notifications_plus
   * @subpackage resources
   */
  class FacebookLikeNotificationsModuleModel extends ActiveCollabModuleModel {
  
    /**
     * Construct NotificationPlus module model definition
     *
     * @param NotificationPlusModuleModel $parent
     */
  function __construct(NotificationsPlusModule $parent) {
      parent::__construct($parent);
  
    }
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
	    $this->addConfigOption('num_of_records', 10);
	    $this->addConfigOption('ajax_call_duration',20);
      
	    parent::loadInitialData($environment);
    }
    
  }
  