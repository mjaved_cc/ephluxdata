{title}Facebook like Notifications Settings{/title}
{add_bread_crumb}Facebook Like Notifications Plus{/add_bread_crumb}
<script language="javascript">
	<!--
	function checkvalid() {ldelim}
	// js function here
	var numrecords=document.getElementById('num_of_records').value;
	var cduration=document.getElementById('ajax_call_duration').value;
	var jerror=true;
	if(numrecords < 10 || numrecords>25 )
	   {
	 	   
			   document.getElementById('numerror').innerHTML="Please Enter value between 10 and 25";
		   jerror=false;
	   }else
	  {
		  document.getElementById('numerror').innerHTML="";
  }	  
  if(cduration<5)
	   {
	   document.getElementById('cerror').innerHTML="Please Enter value greater then 5 ";
  	   
   jerror=false;  
}else
	  {
	   document.getElementById('cerror').innerHTML="";
     
}	  
	  
if(jerror==true)
		 {
	 return true;
}else
		 {
 return false		 
}	 


	  
	{rdelim}
//-->
</script>

<div>
	{form action="{assemble route=facebook_like_notifications_admin}"  method=POST onsubmit="return checkvalid() "}
	<div class="content_stack_wrapper">


		<div class="content_stack_element">
			<div class="content_stack_element_info">
				{lang}Settings{/lang}
			</div>
			<div id="days_off_wrapper" class="content_stack_element_body">
				{wrap field=num_of_records}
				{label for=num_of_records required=yes}Number of Notifications to be shown{/label}

				<input type="text" class="required" name="notifications_plus[num_of_records]" id='num_of_records' value={$notifications_plus_data.num_of_records} style="width:25px;" onKeyUp='this.onchange();'onChange="this.value=this.value.replace(/[^0-9]/g,'');" > 					

				{/wrap}
				<span style="color:red" id="numerror"></span>
				{wrap field=ajax_call_duration}
				{label for=ajax_call_duration required=yes}Refresh after seconds{/label}
				<input type="text" class="required" name="notifications_plus[ajax_call_duration]" id='ajax_call_duration' value={$notifications_plus_data.ajax_call_duration} style="width:25px;" onKeyUp='this.onchange();'onChange="this.value=this.value.replace(/[^0-9]/g,'');"> 	       		
				Seconds
				{/wrap}
				<span style="color:red" id="cerror"></span>

			</div>
		</div>  
	</div>
	{wrap_buttons}
	{submit}Save Changes{/submit}
    {/wrap_buttons}
	{/form}
</div>


