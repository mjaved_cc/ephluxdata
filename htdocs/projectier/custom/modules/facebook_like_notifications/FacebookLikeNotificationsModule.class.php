<?php

/**
 * My Reports module definition class
 */
class FacebookLikeNotificationsModule extends AngieModule {

    /**
     * Short module name (should be the same as module directory name)
     *
     * @var string
     */
    protected $name = 'facebook_like_notifications';

    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0';

    /**
     * Return module name (displayed in activeCollab administration panel)
     *
     * @return string
     */
    function getDisplayName() {
        return lang('Facebook Like Notifications');
    }

    /**
     * Return module description (displayed in activeCollab administration panel)
     *
     * @return string
     */
    function getDescription() {
        return lang('A Notifications module');
    }

    /**
     * List events that this module listens to and define event handlers
     */
    function defineHandlers() {

        EventsManager::listen('on_status_bar', 'on_status_bar');
        EventsManager::listen('on_admin_panel', 'on_admin_panel');
    }

    /**
     * List routes defined and used by this module
     */
    function defineRoutes() {
        // Place where you can define your routes
        Router::map('facebook_like_notifications', 'notifications/my-notifications', array('controller' => 'facebook_like_notifications', 'action' => 'index'));
        //Router::map('facebook_like_temp', 'notifications/temp', array('controller' => 'facebook_like_notifications', 'action' => 'a'));

        Router::map('notifications_count', 'notifications-count', array('controller' => 'facebook_like_notifications', 'action' => 'count'));

        Router::map('facebook_like_notifications_admin', 'admin/facebook_like_notifications', array('controller' => 'facebook_like_notifications_admin', 'action' => 'index'));
        Router::map('call_duration', 'call-duration', array('controller' => 'facebook_like_notifications', 'action' => 'duration'));
    }

    // Adds a column into database at the time of installation
    function install() {
        DB::execute('ALTER TABLE ' . TABLE_PREFIX . 'mailing_activity_logs ADD COLUMN is_acknowledged bit default 0');
        return parent::install();
    }

    //Removes column from database when uninstallation
    function uninstall() {
        DB::execute('ALTER TABLE ' . TABLE_PREFIX . 'mailing_activity_logs DROP COLUMN is_acknowledged');
        return parent::uninstall();
    }

}
