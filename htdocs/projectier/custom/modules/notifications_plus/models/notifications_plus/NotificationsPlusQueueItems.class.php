<?php

/**
 *
 * @package custom.modules.notifications_plus
 * @subpackage models
 */
class NotificationsPlusQueueItems extends DataManager {
    /**
     * Name of the table where records are stored
     *
     * @var string
     */

    /**
     * Do a SELECT query over database with specified arguments
     * 
     * This function can return single instance or array of instances that match 
     * requirements provided in $arguments associative array
     *
     * @param array $arguments Array of query arguments. Fields:
     * 
     *  - one        - select first row
     *  - conditions - additional conditions
     *  - order      - order by string
     *  - offset     - limit offset, valid only if limit is present
     *  - limit      - number of rows that need to be returned
     * 
     * @return mixed
     * @throws DBQueryError
     */
    static function find($arguments = null) {
        return parent::find($arguments, TABLE_PREFIX . 'notifications_queue', DataManager::CLASS_NAME_FROM_TABLE, 'NotificationsPlusQueueItem', '');
    }

// find

    /**
     * Return array of objects that match specific SQL
     *
     * @param string $sql
     * @param array $arguments
     * @param boolean $one
     * @return mixed
     */
    static function findBySQL($sql, $arguments = null, $one = false) {
        return parent::findBySQL($sql, $arguments, $one, TABLE_PREFIX . 'notifications_queue', DataManager::CLASS_NAME_FROM_TABLE, 'NotificationsPlusQueueItem', '');
    }

// findBySQL

    /**
     * Return object by ID
     *
     * @param mixed $id
     * @return StatusUpdate
     */
    static function findById($id) {
        return parent::findById($id, TABLE_PREFIX . 'notifications_queue', DataManager::CLASS_NAME_FROM_TABLE, 'NotificationsPlusQueueItem', '');
    }

// findById

    static function count($conditions = null) {
        return parent::count($conditions, TABLE_PREFIX . 'notifications_queue');
    }

// count

    /**
     * Update table
     * 
     * $updates is associative array where key is field name and value is new 
     * value
     *
     * @param array $updates
     * @param string $conditions
     * @return boolean
     * @throws DBQueryError
     */
    static function update($updates, $conditions = null) {
        return parent::update($updates, $conditions, TABLE_PREFIX . 'notifications_queue');
    }

// update

    /**
     * Delete all rows that match given conditions
     *
     * @param string $conditions Query conditions
     * @param string $table_name
     * @return boolean
     * @throws DBQueryError
     */
    static function delete($conditions = null) {
        return parent::delete($conditions, TABLE_PREFIX . 'notifications_queue');
    }

// delete

    static function findNotificationsByUserIds($user_ids) {
        return self::findBySQL('SELECT * FROM ' . TABLE_PREFIX . 'notifications_queue WHERE `seen` = 0 AND `user_id` IN ( ? )', $user_ids);
    }

}
