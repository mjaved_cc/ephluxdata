<?php

class NotificationPlus extends ActivityLogs {
	
	public function getRecentActivities(User $user) {
		
		list ( $contexts, $ignore_contexts ) = ApplicationObjects::getVisibileContexts( $user );
		$last_fetch_time = ConfigOptions::getValueFor( 'recent_activity_time', $user );
		if ($contexts) {
			$result = DB::execute( 'SELECT * FROM ' . TABLE_PREFIX . 'activity_logs WHERE ' . self::conditionsFromContexts( $contexts, $ignore_contexts ) . ' AND `created_on` > ? AND `created_on` > ? AND `created_by_id` != ?  ORDER BY created_on ASC', $last_fetch_time, $user->getLastLoginOn(), $user->getId() );
			
			if ($result instanceof DBResult) {
				$result->setCasting( array ('id' => DBResult::CAST_INT, 'subject_id' => DBResult::CAST_INT, 'target_id' => DBResult::CAST_INT, 'created_on' => DATETIME_MYSQL, 'created_by_id' => DBResult::CAST_INT ) );
			} // if
			$recent_activities = array ();
			if (is_foreachable( $result )) {
				foreach ( $result as $item ) {
					$log = new ActivityLog();
					$log->loadFromRow( $item );
					// Clean up action for display
					$action = $log->getAction();
					$action = str_replace('_', ' ', array_pop( explode('/', $action)) );
					$values ['action'] = $action; 
					$object = $log->getSubject();
					$values ['url'] = $object->getViewUrl();
					$values ['name'] = $object->getName();
					$values ['type'] = $object->getVerboseType( false, $user->getLanguage() );
					// Skip those types where name contains the type already
					if ( strpos( $values['name'], $values ['type'] ) == 0) {
						$values ['name'] = str_replace($values ['type'], '', $values ['name']);
					}
					$values ['created_by_name'] = $item ['created_by_name'];
					$values ['created_by_id'] = $item ['created_by_id'];
					array_push( $recent_activities, $values );
				} //foreach
			} //if
			return $recent_activities;
		} else {
			return null;
		} // if
	

	}
	
	public static function getOnlineUser(IUser $user) {
		$visible_user_ids = Users::findVisibleUserIds( $user );
		$last_fetch_time = ConfigOptions::getValueFor( 'recent_activity_time', $user );
		$online_users = array ();
		if (is_foreachable( $visible_user_ids )) {
			$users_table = TABLE_PREFIX . 'users';
			$online_users = Users::findBySQL( "SELECT * FROM $users_table WHERE id IN (?) AND last_login_on > ? ORDER BY CONCAT(first_name, last_name, email)", array ($visible_user_ids, $last_fetch_time ) );
		} // if
		return $online_users;
	}
	
	public static function getDueObjects($user = null) {
		
		// Time is now?
		$objects = $notify_types = array ();
		$available_notify_types = ( array ) ConfigOptions::getValue( 'notifications_plus_notify_types' );
		if (is_foreachable( $available_notify_types )) {
			foreach ( $available_notify_types as $type => $enabled ) {
				if ($enabled == 1) {
					$notify_types [] = strtolower( $type );
				}
			}
		}
		
		// Get objects due in $days_to_due time (in days) or due today
		$days_to_due = ConfigOptions::getValue( 'notifications_plus_days_to_due' );
		$notify_today = ConfigOptions::getValue( 'notifications_plus_notify_today' );
		
		$conditions = $project_objects = $subtasks = array ();
		$conditions [0] = ' (';
		$today = gmdate(DATE_MYSQL);
		if ($notify_today) {
			$conditions[0] .= ' ( (TO_DAYS(due_on) - TO_DAYS(?) = 0) AND completed_on IS null AND state >= ? AND type IN ( ? ) )';
			$conditions[] = $today;
			$conditions[] = STATE_VISIBLE;
			$conditions[] = $notify_types;
		}
		if ($days_to_due > 0) {
			if (strlen($conditions[0]) > 3) {
				$conditions[0] .= ' OR '; 
			}
			$conditions[0] .= ' ( (TO_DAYS(due_on) - TO_DAYS(?) = ?) AND completed_on IS null AND state >= ? AND type IN ( ? ) )';
			$conditions[] = $today;
			$conditions[] = $days_to_due;
			$conditions[] = STATE_VISIBLE;
			$conditions[] = $notify_types;
		}
		 
		// If user given, only bring objects this user is responsible for
		$flag = true;
		if ($user instanceof User) {
			if (strlen($conditions[0]) > 3) {
				$conditions[0] .= ' ) AND ';
				$flag = false;
			}
			$conditions [0] .= ' ( assignee_id = ? )';
			$conditions [] = $user->getId();
		}
		
		if($flag) {
			$conditions [0] .= ') ';
		}
		
		if (is_foreachable( $conditions )) {
			
			$results = ProjectObjects::find( array ('conditions' => $conditions, 'order' => 'type DESC' ) );
			
			if ($results instanceof DBResult) {
				$project_objects = $results->toArray();
			}
			
			if (in_array( 'SubTask', $notify_types )) {
				$results = SubTasks::find( array ('conditions' => $conditions, 'order' => 'type DESC' ) );
				
				if ($results instanceof DBResult) {
					$subtasks = $results->toArray();
				}
			}
		}
		
		$objects = array_merge( $project_objects, $subtasks );
		
		unset( $project_objects );
		unset( $subtasks );
		
		return $objects;
	}
	
	public static function sendDueDateReminders(&$objects) {
	
		$notify_owner_company = ConfigOptions::getValue( 'notifications_plus_notify_owner_company' );
		$owner_company = Companies::findOwnerCompany();
		$owner_company_id = $owner_company->getId();
		foreach ( $objects as $object ) {
			// Find out if object type is allowed
			if ($object->assignees()->hasAssignee()) {
				
				$due_on = $object->getDueOn();
				$due_on_text = '';
				if ($due_on->isToday()) {
					$due_on_text = lang('today');
				} else if ($due_on->isTomorrow()) {
					$due_on_text = lang('tomorrow');
				} else {
					$due_on_text = lang('on') . ' '.$due_on->formatForUser(Authentication::getLoggedUser());
				}
				
				// Get project from Id
				$project = Projects::findById( $object->getProjectId() );
				
				if (ConfigOptions::getValue( 'notifications_plus_notify_responsible' )) {
					$assignees = $object->assignees()->getAssignee();
				} else {
					$assignees = $object->assignees()->getAllAssignees();
				}
			
				if (is_foreachable( $assignees ) && $notify_owner_company) {
					foreach ( $assignees as $assignee ) {
						if ($assignee->getCompanyId() == $owner_company_id) {
							NotificationPlus::sendReminder( $assignee, $object, $project, $due_on_text, $owner_company );
						}
					}
				} else {
					NotificationPlus::sendReminder( $assignees, $object, $project, $due_on_text, $owner_company );
				} // if
			} // if
		} // foreach
	} // notifications_module_notify_users
	

	public static function sendReminder($assignees, &$object, &$project, $due_on, $owner_company) {
		
		// Send notifications to all assigned users
		ApplicationMailer::notifier()->notifyUsers( 
			$assignees, $object, 
			'notifications_plus/notify_due', 
			array ('object_type' => $object->getType(), 
				   'object_name' => $object->getName(), 
				   'object_url' => $object->getViewUrl(), 
				   'project_name' => $project->getName(),
				   'owner_company_name' => $owner_company->getName(), 
				   'due_on' => $due_on, 				    
			) 
		);
	} // notifications_module_notify_send
	
	
}