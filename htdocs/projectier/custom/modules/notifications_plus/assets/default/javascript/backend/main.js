App.notifications_plus = {
	controllers : {},
	models      : {}
};

App.notifications_plus.init = function (item, mute_notification_url, notification_url, show_notifications, enabled_updates) {
   		
		// Save values to config for later use...
   		App.Config.set('mute_notification_url', mute_notification_url);
   		App.Config.set('notification_url', notification_url);
   		App.Config.set('show_notifications', show_notifications);
   		App.Config.set('notifications_plus_enabled_activity_updates', enabled_updates);
   		
   		// Remove caption
   		$("#statusbar_item_notifications_plus a").css('height', '29px');
		$("#statusbar_item_notifications_plus a").text('');
                
   		if(enabled_updates == true) {
   			if(mute_notification_url != '') {
   				$("#statusbar_item_notifications_plus").click(function(){
   					$.ajax({
   						type: "POST",
   						url: App.extendUrl(mute_notification_url, { async : 1 }),
   						'success' : function(response) {
   							var image_name = (response == 'true') ? 'unmute' : 'mute';
   							$("#statusbar_item_notifications_plus a").css('background-image', 'url('+App.Wireframe.Utils.imageUrl('status-bar/'+ image_name +'.png', 'notifications_plus')+')');
   						}
	   				});
	   		    });
   			}	
   		} else {
   			$("#statusbar_item_notifications_plus").hide();
   		}
   		
   		if(enabled_updates == true && notification_url != '') {
   		   setInterval(function() {
   			    if (show_notifications == 'false') {
   			    	return;
   			    }
   				$.ajax({
   					type: "GET",
   					url: App.extendUrl(notification_url, { async : 1 }),
   					'success' : function(response) {
                            if(typeof(response) == 'object' && response != null && response.length > 0){
                            	for(var i=0; i<response.length ;i++){
       								App.Wireframe.Flash.message(response[i].message_text, null, false, response[i].message_type, response[i].color_code);
       							}
                            }
   		            },
   		            'error' : function (response) {
   		            	
   		            }			 	 
   		         });
   			}, 60000);
   		}
   	   
};




/**
 * Flash implementation
 */
App.Wireframe.Flash = function() {
  
  /**
   * Flash message instance
   *
   * @var jQuery
   */
  var message_flash;
  
  /**
   * Currently focused element
   *
   * @var jQuery
   */
  var currently_focused_item;
  
  /**
   * Show flash message box
   *
   * @var String message
   * @var Object parameters
   * @var Boolean is_error
   * @var Boolean auto_hide
   */
  var show_flash_message = function(message, parameters, flash_class, auto_hide, time_out,color_code) {
    parameters = parameters || null;
    
    
    var $avaialble_toastr = ['info', 'warning', 'success', 'error'];
    
    if(jQuery.inArray(flash_class, $avaialble_toastr) == -1) {
    	flash_class = 'info';
    } 
    
    currently_focused_item = $(':focus');
    title = '';
    
    if (typeof(time_out) == 'undefined' ) {
    	time_out = 5000;
    }
 
    toastr.options = {
			debug: false,
			tapToDismiss:false,
			timeOut: time_out,
			positionClass: 'toast-top-right'
                     }
    
    if(parameters) {
    	message = App.lang(message, parameters);
    }
    var $toast = toastr[flash_class](message, title);
    if(color_code){
    	$('.toast-' + flash_class).css('background-color', color_code);
    }
  }; // show_flash_message
  
  // Public interface
  return {
    
    /**
     * Display success message
     *
     * @param String message
     * @param Object parameters
     * @param Boolean auto_hide
     */
   success : function(message, paremeters, auto_hide) {
      show_flash_message(message, paremeters, 'success', auto_hide);
    },
   
  
    /**
     * Display error message
     *
     * @param String message
     * @param Object parameters
     * @param Boolean auto_hide
     */
    error : function(message, paremeters, auto_hide) {
      show_flash_message(message, paremeters, 'error', auto_hide);
    },
    
    message : function(message, paremeters, auto_hide, message_type, color_code) {
      show_flash_message(message, paremeters, message_type, auto_hide, 6000, color_code);
    }
        
    
  };
  
}();

