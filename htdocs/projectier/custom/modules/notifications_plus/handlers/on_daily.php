<?php

  /**
   * notifications_plus_handle_on_daily event handler
   *
   * @package custom.modules.notifications_plus
   * @subpackage handlers
   */

  function notifications_plus_handle_on_daily() {  
  	  $users = Users::find( array( 'conditions' => array('state >= ? ', STATE_VISIBLE) ) );
  	  if (is_foreachable($users)) {
  	  	foreach ($users as $user) {
  	  		ConfigOptions::setValueFor('is_due_object_shown', $user, true);
  	  	}
  	  }
  } // notifications_plus_handle_on_daily 