<?php

  /**
   * otifications_plus module on_status_bar event handler
   *
   * @package custom.modules.notifications_plus
   * @subpackage handlers
   */

  /**
   * Register status bar items
   *
   * @param StatusBar $status_bar
   * @param IUser $logged_user
   */
  function notifications_plus_handle_on_status_bar(StatusBar &$status_bar, IUser &$user) {
  
	  $show_notifications = ConfigOptions::getValueFor('show_notifications', $user) ? 'true' : 'false';
	  $notifications_plus_enabled_activity_updates = ConfigOptions::getValue('notifications_plus_enabled_activity_updates') ? 'true' : 'false';
	  $image = ($show_notifications == true) ? 'unmute' : 'mute';
      $status_bar->add('notifications_plus', lang('Notifications'), '', AngieApplication::getImageUrl('status-bar/'.$image.'.png', NOTIFICATION_PLUS_MODULE), array(
        'group' => StatusBar::GROUP_LEFT,
      	// the onclick is eval'd in JS - so putting a function here will execute it
        'onclick' => 'function() { App.notifications_plus.init(this, "'.clean(Router::assemble('notifications_plus_mute_notifications')).'", "'.clean(Router::assemble('notifications_plus')).'", '.clean($show_notifications).', '.clean($notifications_plus_enabled_activity_updates).'); }', 
      ));
  } // notifications_plus_handle_on_status_bar