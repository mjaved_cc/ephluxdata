<?php
  /**
   * Notifications module on_hourly event handler
   *
   * @package activeCollab.modules.notifications
   * @subpackage handlers
   */

  /**
   * Send daily mails, this is the actual logic of the module :)
   *
   * @param null
   * @return null
   */
  function notifications_plus_handle_on_hourly() {
  	
  	// Enabled?
    if (!ConfigOptions::getValue('notifications_plus_enabled_due_dates_email')) {
      return true;
    } // if
    
    if ( intval( gmdate('H') ) != intval (ConfigOptions::getValue('notifications_plus_when_to_send') )) {
      return true;
    } // if
    
    //Find due objects
    $objects = NotificationPlus::getDueObjects();

    if (count($objects) <= 0) {
      return true;
    } else {
    	NotificationPlus::sendDueDateReminders($objects);
    }

    //log_message('Sent out daily notifications', LOG_LEVEL_INFO, 'notifications');
  } // notifications_plus_handle_on_hourly
?>