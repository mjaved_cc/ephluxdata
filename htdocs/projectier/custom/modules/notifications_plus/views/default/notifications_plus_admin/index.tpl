{title}Notifications Plus Settings{/title}
{add_bread_crumb}Notifications Plus{/add_bread_crumb}


<div>
	{form action="{assemble route=notifications_plus_admin}"  method=POST}
	<div class="content_stack_wrapper">
		<div class="content_stack_element">
	        <div class="content_stack_element_info">
	        	{lang}Activity Updates{/lang}
	        </div>
	        <div id="days_off_wrapper" class="content_stack_element_body">
	        	{wrap field=enabled}
		        	{label for=enabled required=yes}Enable activity update notifications{/label}
		          	{yes_no name='notifications_plus[enabled_activity_updates]' id='enabled_activity_updates' value=$notifications_plus_data.notifications_plus_enabled_activity_updates}
		        {/wrap}
	        </div>
	    </div> 
	    
	    
	    <div class="content_stack_element">
	        <div class="content_stack_element_info">
	        	{lang}Email Reminders{/lang}
	        </div>
	        <div id="days_off_wrapper" class="content_stack_element_body">
	        	{wrap field=enabled}
		        	{label for=enabled required=yes}Enable email reminders for due dates{/label}
		          	{yes_no name='notifications_plus[enabled_due_dates_email]' id='enabled_due_dates_email' value=$notifications_plus_data.notifications_plus_enabled_due_dates_email}
		        {/wrap}
		        
		        {wrap field=notify_today}
          			{label for=notify_today required=no}Send notification on the due date{/label}
          			{yes_no name='notifications_plus[notify_today]' id='notify_today' value=$notifications_plus_data.notify_today}
        		{/wrap}
        		
        		{wrap field=days_to_due}
			    	{label for=days_to_due required=yes}Send notifications in advance{/label}
			        <select class="required" name="notifications_plus[days_to_due]" id='days_to_due' value=$notifications_plus_data.days_to_due>
			        	{foreach from=$days_to_due_values item=days_to_due_value}
			            	{if $days_to_due_value == $notifications_plus_data.days_to_due}
			            		{if $days_to_due_value == 0} 
			            			<option value="{$days_to_due_value}" selected="selected">{lang}No advance notification{/lang}</option>
			            		{else}
			            			<option value="{$days_to_due_value}" selected="selected">{$days_to_due_value} {lang}days before due date{/lang}</option>
			            		{/if}
			            	{else}
			            		{if $days_to_due_value == 0} 
			            			<option value="{$days_to_due_value}">{lang}No advance notification{/lang}</option>
			            		{else}
			            			<option value="{$days_to_due_value}">{$days_to_due_value} {lang}days before due date{/lang}</option>
			            		{/if}
			            	{/if}
			          	{/foreach}
			       </select>
        		{/wrap}
        		
        		{wrap field=when_to_send}
          			{label for=when_to_send required=yes}When should the notifications be sent (Hour of day in UTC){/label}
          			<select class="required" name="notifications_plus[when_to_send]" id='when_to_send' value=$notifications_plus_data.when_to_send>
          				{foreach from=$when_to_send_values item=when_to_send_value}
            				{if $when_to_send_value == $notifications_plus_data.when_to_send}
            					<option value="{$when_to_send_value}" selected="selected">{$when_to_send_names.$when_to_send_value}</option>
            				{else}
            					<option value="{$when_to_send_value}">{$when_to_send_names.$when_to_send_value}</option>
            				{/if}
          				{/foreach}
          			</select>
          			<p class="aid">{lang}Current UTC time is {/lang} <b>{$current_time}</b></p>
        		{/wrap}
        		
        		{wrap field=notify_owner_company}
          			{label for=notify_owner_company required=no}Notify only Assignees from Owner Company{/label}
          			{yes_no name='notifications_plus[notify_owner_company]' id='notify_owner_company' value=$notifications_plus_data.notify_owner_company}
        		{/wrap}
        		
        		{wrap field=notify_responsible}
          			{label for=notify_responsible required=no}Notify only Responsible assignee{/label}
          			{yes_no name='notifications_plus[notify_responsible]' id='notify_responsible' value=$notifications_plus_data.notify_responsible}
        		{/wrap}
        		
        		
		       
		          
		          {wrap field=types}
          			{label for=types required=yes}What items to send notifications for{/label}
          			{foreach from=$notify_types item=type}
          				<input type="checkbox" name="notifications_plus[types][{$type}]" value=1 {if $notifications_plus_data.types.$type == 1}checked="checked"{/if}  class="inline">{$type|capitalize}<br />
		  			{/foreach}
        		{/wrap}
	        </div>
	    </div>  
	</div>
	{wrap_buttons}
  	  {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>


