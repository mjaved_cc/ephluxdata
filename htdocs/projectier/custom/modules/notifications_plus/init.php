<?php

/**
 * Init notifications_plus module
 *
 * @package custom.modules.notifications_plus
 */
define('NOTIFICATION_PLUS_MODULE', 'notifications_plus');
define('NOTIFICATION_PLUS_MODULE_PATH', CUSTOM_PATH . '/modules/notifications_plus');

define('NOTIFICAITON_TYPE_INFO', 'info');

$filename = (APPLICATION_VERSION >= '3.2.3') ? 'NotificationsPlusQueueItems.class.php' : 'NotificationsPlusQueueItemsNonStatic.class.php';
  
AngieApplication::setForAutoload(array(
    'NotificationPlus' => NOTIFICATION_PLUS_MODULE_PATH . '/models/notifications_plus/NotificationPlus.class.php',
    'NotificationsPlusQueueItem' => NOTIFICATION_PLUS_MODULE_PATH . '/models/notifications_plus/NotificationsPlusQueueItem.class.php',
    'NotificationsPlusQueueItems' => NOTIFICATION_PLUS_MODULE_PATH . '/models/notifications_plus/'.$filename,
));
