<?php

  /**
   * NotificationsPlus module definition
   *
   * @package custom.modules.notifications_plus
   * @subpackage models
   */
  class NotificationsPlusModule extends AngieModule  {
    
    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'notifications_plus';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '3.1.1';
    
    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------
    
    /**
     * Define module routes
     */
    function defineRoutes() {
    	Router::map('notifications_plus', 'notifications-plus', array('controller' => 'notifications_plus', 'action' => 'index'));
    	Router::map('notifications_plus_admin', 'admin/notifications-plus', array('controller' => 'notifications_plus_admin', 'action' => 'index'));
        Router::map('notifications_plus_mute_notifications', 'notifications-plus/mute-notifications', array('controller' => 'notifications_plus', 'action' => 'mute_notifications'));
    } // defineRoutes
    
    /**
     * Define event handlers
     */
    function defineHandlers() {
      EventsManager::listen('on_admin_panel', 'on_admin_panel');
      EventsManager::listen('on_daily', 'on_daily');
      EventsManager::listen('on_hourly', 'on_hourly');
      EventsManager::listen('on_notifications_plus_notifier', 'on_notifications_plus_notifier');
      EventsManager::listen('on_status_bar', 'on_status_bar');
    } // defineHandlers
    
    
    function enable() {
    	$fields = DB::getConnection()->listTableFields( TABLE_PREFIX . 'notifications_queue');
    	if(!in_array('user_id', $fields)) {
			try {
                DB::execute('ALTER TABLE  ' . TABLE_PREFIX . 'notifications_queue ADD  `user_id` int(10) NOT NULL');
			} catch (Exception $e) {
				throw $e;
			}      		
    	}
    	
    	parent::enable();
    }
    
    // ---------------------------------------------------
    //  (Un)Install
    // ---------------------------------------------------
    function uninstall() {
    	parent::uninstall();
    	
    	try {
    		DB::execute("DROP table " . TABLE_PREFIX . "notifications_queue");
    	} catch (Exception $e) {
    		// Nothing to do
    	} 
    }
    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Notifications Plus');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
      return lang('Unobtrusive and useful notifications for activities, due dates and more');
    } // getDescription
    
    /**
     * Return module uninstallation message
     *
     * @return string
     */
    function getUninstallMessage() {
      return lang('Module will be deactivated.');
    } // getUninstallMessage
    
  }