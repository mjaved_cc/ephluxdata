<?php

/**
 * mailing_activity_log helper implementation
 * 
 * @package angie.frameworks.email
 * @subpackage helpers
 */
/**
 * mailing_activity_log helper implementation
 * 
 * @package angie.frameworks.email
 * @subpackage helpers
 */

/**
 * Render mailing activity log
 * 
 * @param array $params
 * @param Smarty $smarty
 * @return string
 */
function smarty_function_get_user_notifications($params, &$smarty) {

    if (empty($params['id'])) {
        $params['id'] = HTML::uniqueId('get_user_notifications');
    } // if

    $logged_user = $params['logged_userId']; // getting the logged user id from view
    
    

   // $per_load = (integer) array_var($params, 'per_load');
$per_load = (integer)  ConfigOptions::getValue('num_of_records');
    if ($per_load < 1) {
        $per_load = 10;
    } // if


    $additional_params = array('per_load' => $per_load);
    $additional_conditions = array();


    $direction = array_var($params, 'direction');
    if ($direction == MailingActivityLog::DIRECTION_IN || $direction == MailingActivityLog::DIRECTION_OUT) {
        $additional_params['direction'] = $direction;
        $additional_conditions[] = DB::prepare('direction = ?', array($direction));
    } // if

    $additional_conditions = count($additional_conditions) ? '(' . implode(' AND ', $additional_conditions) . ')' : null;
    $additional_conditions .= ' to_id = ' . $logged_user;
    $conditions = 'to_id = ' . $logged_user . ' AND is_acknowledged = 1';

    return '<div id="' . $params['id'] . '" class="mailing_activity_log"></div><script type="text/javascript">$("#' . $params['id'] . '").getUserNotificationsActivityLog(' . JSON::encode(array(
                'load_more_url' => Router::assemble('notification_alerts', $additional_params),
                'entries' => NotificationAlertsController::slice_manager($per_load, null, null, $additional_conditions, $logged_user),
                'entries_per_load' => $per_load,
                'total_entries' => MailingActivityLogs::count($conditions)
            )) . ');</script>';
}

