<?php

class NotificationAlert extends ActivityLogs {

	// notifications_module_notify_send

	public static function getSliceByUser($user_id) {
       // echo $user_id;
		
		DB::execute('UPDATE ' . TABLE_PREFIX . 'mailing_activity_logs SET is_acknowledged = 1 WHERE is_acknowledged = 0 AND to_id = ?' , $user_id);        
  
        ///$user_id = explode( "logged_user" ,$additional_conditions );
		
//		return MailingActivityLogs::find(array(
//					'conditions' => array("to_id = ? ", $user_id),
//					'order' => 'created_on DESC , is_acknowledged ASC',
//					'limit' => $num,
//				));
        
        
        
        
//        	$max_date = $timestamp ? new DateTimeValue($timestamp) : new DateTimeValue();
//  		
//  		if($additional_conditions) {
//  			$additional_conditions = " AND $additional_conditions";
//  		} // if
//  		
//  		if($exclude) {
//  			return MailingActivityLogs::find(array(
//  			  'conditions' => array("id NOT IN (?) AND created_on <= ? $additional_conditions ADN to_id = ?", $exclude, $max_date, $user_id), 
//  			  'order' => 'created_on DESC', 
//  			  'limit' => $num,  
//  			));
//  		} else {
//  			return MailingActivityLogs::find(array( 
//  				'conditions' => array("created_on <= ? $additional_conditions ADN to_id = ?", $max_date, $user_id),
//  			  'order' => 'created_on DESC', 
//  			  'limit' => $num,  
//  			));
//  		} // if



//        $table_name = "acx_mailing_activity_logs";
//
//        $result_column = DB::execute('SHOW COLUMNS FROM acx_mailing_activity_logs LIKE "is_acknowledged"');
//        if (empty($result_column)) {
//            DB::execute('ALTER TABLE acx_mailing_activity_logs ADD COLUMN is_acknowledged bit default 0');
//        }
//
//        $result = DB::execute('SELECT * FROM acx_mailing_activity_logs WHERE to_id = ? order by is_acknowledged asc, created_on desc limit 10', $userid);
//        $rows = array();
//        if (is_foreachable($result)) {
//            foreach ($result as $item) {
//
//                $rows[] = $item;
//                //print_r($item);
//            }
//        }
//
//        DB::execute('UPDATE acx_mailing_activity_logs SET is_acknowledged = 1 WHERE is_acknowledged = 0 AND to_id = ?', $userid);
//
//
//        return $rows;
		//return $result_column;
	}
    
    /**
     * get the tasks count from acx_mailing_activity_log_table
     * input : conditions like in where clause.
     * output : count
     */    
    public static function getTasksCount($conditions){  
       
        return MailingActivityLogs::count($conditions);
    }
	
	    function loadInitialData($environment = null) {
	    $this->addConfigOption('recent_activity_time', gmdate(DATETIME_MYSQL));
	    $this->addConfigOption('show_notifications', true);
	    $this->addConfigOption('is_due_object_shown', true);
	    
	    $this->addConfigOption('notifications_plus_enabled_activity_updates', true);
	    $this->addConfigOption('notifications_plus_enabled_due_dates_email', true);
      	$this->addConfigOption('notifications_plus_days_to_due', false);
      	$this->addConfigOption('notifications_plus_notify_today', true);
      	$this->addConfigOption('notifications_plus_notify_owner_company', true);
      	$this->addConfigOption('notifications_plus_notify_responsible', true);
      	$this->addConfigOption('notifications_plus_when_to_send', 9);
      	$this->addConfigOption('notifications_plus_notify_types', array('Milestone'=>1, 'Task'=>1, 'SubTask' => 1));
      
	    parent::loadInitialData($environment);
    }

}