<?php

/**
 * My Reports module definition class
 */
class NotificationAlertsModule extends AngieModule {

    /**
     * Short module name (should be the same as module directory name)
     *
     * @var string
     */
    protected $name = 'notification_alerts';

    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0';

    /**
     * Return module name (displayed in activeCollab administration panel)
     *
     * @return string
     */
    function getDisplayName() {
        return lang('Notification Alerts');
    }

    /**
     * Return module description (displayed in activeCollab administration panel)
     *
     * @return string
     */
    function getDescription() {
        return lang('A Notification Alerts module');
    }

    /**
     * List events that this module listens to and define event handlers
     */
    function defineHandlers() {

        EventsManager::listen('on_status_bar', 'on_status_bar');
        EventsManager::listen('on_admin_panel', 'on_admin_panel');
    }

    /**
     * List routes defined and used by this module
     */
    function defineRoutes() {
        // Place where you can define your routes
        Router::map('notification_alerts', 'notifications/my-notifications', array('controller' => 'notification_alerts', 'action' => 'index'));
        //Router::map('notification_alerts_temp', 'notifications/temp', array('controller' => 'notification_alerts', 'action' => 'a'));

        Router::map('notifications_count', 'notifications-count', array('controller' => 'notification_alerts', 'action' => 'count'));

        Router::map('notification_alerts_admin', 'admin/notification_alerts', array('controller' => 'notification_alerts_admin', 'action' => 'index'));
        Router::map('call_duration', 'call-duration', array('controller' => 'notification_alerts', 'action' => 'duration'));
    }

    // Adds a column into database at the time of installation
    function install() {
        DB::execute('ALTER TABLE ' . TABLE_PREFIX . 'mailing_activity_logs ADD COLUMN is_acknowledged bit default 0');
        return parent::install();
    }

    //Removes column from database when uninstallation
    function uninstall() {
        DB::execute('ALTER TABLE ' . TABLE_PREFIX . 'mailing_activity_logs DROP COLUMN is_acknowledged');
        return parent::uninstall();
    }

}
