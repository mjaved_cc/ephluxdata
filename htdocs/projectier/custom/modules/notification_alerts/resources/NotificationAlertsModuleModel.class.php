<?php

  // Include applicaiton specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';
	
  /**
   * NotificationAlerts module model definition
   *
   * @package custom.modules.notification_alerts
   * @subpackage resources
   */
  class NotificationAlertsModuleModel extends ActiveCollabModuleModel {
  
    /**
     * Construct NotificationAlerts module model definition
     *
     * @param NotificationAlertsModuleModel $parent
     */
  function __construct(NotificationAlertsModule $parent) {
      parent::__construct($parent);
  
    }
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
	    $this->addConfigOption('num_of_records', 10);
	    $this->addConfigOption('ajax_call_duration',20);
      
	    parent::loadInitialData($environment);
    }
    
  }
  