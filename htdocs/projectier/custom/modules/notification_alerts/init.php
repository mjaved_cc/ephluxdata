<?php

  /**
   * My Reports module initialisation file
   */
  
  define('NOTIFICATION_ALERTS_MODULE', 'notification_alerts');
  define('NOTIFICATION_ALERTS_MODULE_PATH', __DIR__);
  define('FACE_TYPE_INFO', 'info');
  
AngieApplication::setForAutoload(array(
    'NotificationAlert' => NOTIFICATION_ALERTS_MODULE_PATH . '/models/notification_alerts/NotificationAlert.class.php',
	
));
  
  ?>
