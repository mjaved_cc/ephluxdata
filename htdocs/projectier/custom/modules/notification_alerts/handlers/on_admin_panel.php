<?php

  /**
   * on_admin_panel event handler
   * 
   * @package 
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function notification_alerts_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToTools('notification_alerts_', lang('Notification Alerts'), Router::assemble('notification_alerts_admin'), AngieApplication::getImageUrl('module.png', NOTIFICATION_ALERTS_MODULE));
  } // on_admin_panel