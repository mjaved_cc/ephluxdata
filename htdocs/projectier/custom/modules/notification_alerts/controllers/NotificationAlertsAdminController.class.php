<?php

// We need admin controller
AngieApplication::useController('admin');

/**
 * Manages notification_alerts_admin settings
 * 
 * @package custom.modules.notification_alerts_admin
 * @subpackage controllers
 * 
 */
class NotificationAlertsAdminController extends AdminController {

    /**
     * Controller name
     *
     * @var string
     */
    var $controller_name = 'notification_alerts_admin';

    /**
     * number of records in notification?
     *
     * @var integer 10
     */
    var $num_of_records = 10;

    /**
     * ajax call duration in seconds?
     *
     * @var integer 10
     */
    var $ajax_call_duration = 20;

    /**
     * Controller constructor
     *
     */
    function __construct($request) {
        parent::__construct($request);


        $reference_time = new DateTimeValue(time());
        $this->num_of_records = (int) ConfigOptions::getValue('num_of_records');
        $this->ajax_call_duration = (int) ConfigOptions::getValue('ajax_call_duration');

        $this->smarty->assign(array(
            'num_of_records' => $this->num_of_records,
            'ajax_call_duration' => $this->ajax_call_duration,
        ));
    }

// __construct

    /**
     * Main Notification settings page
     *
     */
    function index() {

        $notifications_alerts_data = $this->request->post('notifications_alerts');
        if (!is_array($notifications_alerts_data)) {
            $notifications_alerts_data = array(
                'num_of_records' => $this->num_of_records,
                'ajax_call_duration' => $this->ajax_call_duration,
            );
        } // if

        $this->smarty->assign(array(
            'notifications_alerts_data' => $notifications_alerts_data,
            'num_of_records_values' => array(
                10, 15, 20, 25
            ),
            'ajax_call_duration_values' => array(
                5, 10, 30, 45, 60
            ),
        ));

        if ($this->request->isSubmitted()) {
            ConfigOptions::setValue('num_of_records', (integer) array_var($notifications_alerts_data, 'num_of_records', 10), true);
            ConfigOptions::setValue('ajax_call_duration', (integer) array_var($notifications_alerts_data, 'ajax_call_duration', 20), true);

            $this->response->redirectTo('notification_alerts_admin');
        } // if
    }

// index
}