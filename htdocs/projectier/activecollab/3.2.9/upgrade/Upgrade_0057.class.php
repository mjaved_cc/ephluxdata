<?php

  /**
   * Update activeCollab 3.2.8 to activeCollab 3.2.9
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0057 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.2.8';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.2.9';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return array(
        'scheduleIndexesRebuild' => 'Schedule index rebuild',
        'endUpgrade' => 'Finish upgrade',
      );
    } // getActions

  }