<?php

  /**
   * Documents module initialization file
   * 
   * @package activeCollab.modules.documents
   */
  
  define('DOCUMENTS_MODULE', 'documents');
  define('DOCUMENTS_MODULE_PATH', APPLICATION_PATH . '/modules/documents');
  
  AngieApplication::useModel('documents', DOCUMENTS_MODULE);
  
  AngieApplication::setForAutoload(array(
    'DocumentCategory' => DOCUMENTS_MODULE_PATH . '/models/DocumentCategory.class.php', 
    'IDocumentCategoryImplementation' => DOCUMENTS_MODULE_PATH . '/models/IDocumentCategoryImplementation.class.php',

    'IDocumentSearchItemImplementation' => DOCUMENTS_MODULE_PATH . '/models/search/IDocumentSearchItemImplementation.class.php',
    'DocumentsSearchIndex' => DOCUMENTS_MODULE_PATH . '/models/search/DocumentsSearchIndex.class.php',

    'IDocumentPreviewImplementation' => DOCUMENTS_MODULE_PATH . '/models/IDocumentPreviewImplementation.class.php'
  ));