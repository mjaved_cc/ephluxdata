{title}Revision Details{/title}
{add_bread_crumb}Revision Details{/add_bread_crumb}

{object object=$active_commit user=$logged_user repository=$project_object_repository}
  {if !(RepositoryEngine::pathInFolder($active_file))}
	  <div class="wireframe_content_wrapper">
	  	<div id="commit_info" class="commit_files">
	  	  <ul>
	  	    {foreach from=$grouped_paths item=path name=files_list key=action}
	  	      {foreach from=$path item=item}
	  	        <li>
	  	          <span class="{$action|source_module_get_state_name}">{$action|source_module_get_state_label}</span>
	  	          <a href="{$project_object_repository->getBrowseUrl($active_commit, $item, $active_commit->getRevisionNumber())}">{$item}</a>
	  	        </li>
	  	      {/foreach}
	  	    {/foreach}
	  	  </ul>
	  	</div>
	  </div>
  {/if}

  <div id="repository_commit" class="wireframe_content_wrapper">
    <div id="source" class="browser">
    {if is_foreachable($diff)}
      {foreach $diff as $key=>$file}
      <div id="{$file.file}">
  			<div class="wireframe_content_wrapper source_navbar">
  			  <h3>{$file.file}</h3>
  			</div>
  	    <div class="file_diff" id="file_{$key}">
  	      <div class="lines" valign="top"><pre>{$file.lines}</pre></div>
          <div class="source" valign="top"><pre><table cellspacing="0"><tr><td>{$file.content nofilter}</td></tr></table></pre></div>
  	    </div>
      </div>
      {/foreach}
    {else}
      <p>{lang}No diff available{/lang}</p>
    {/if}
    </div>
  </div>
{/object}