<?php

  /**
   * Source search item implementation
   * 
   * @package activeCollab.modules.source
   * @subpackage models
   */
  class ISourceSearchItemImplementation extends ISearchItemImplementation {
  
    /**
     * Return list of indices that index parent object
     * 
     * Result is an array where key is the index name, while value is list of 
     * fields that's watched for changes
     * 
     * @return array
     */
    function getIndices() {
      return array(
        'source' => array('repository_id', 'message_body'), 
      );
    } // getIndices
  
//    /**
//     * Return item context for given index
//     * 
//     * @param SearchIndex $index
//     * @return string
//     */
//    function getContext(SearchIndex $index) {
//      if($index instanceof SourceSearchIndex) {
//        return 'source/' . $this->object->getRepositoryId();
//      } else {
//        throw new InvalidInstanceError('index', $index, 'SourceSearchIndex');
//      } // if
//    } // getContext
    
    /**
     * Return additional properties for a given index
     * 
     * @param SearchIndex $index
     * @return mixed
     */
    function getAdditional(SearchIndex $index) {
      if($index instanceof SourceSearchIndex) {
        return array(
          'repository_id' => $this->object->getRepositoryId(), 
          'repository' => $this->object->getRepository() instanceof SourceRepository ? $this->object->getRepository()->getName() : null, 
          'body' => stripslashes($this->object->getMessageBody()),
        );
      } else {
        throw new InvalidInstanceError('index', $index, 'SourceSearchIndex');
      } // if
    } // getAdditional
    
  }