<?php

  /**
   * BaseRecurringProfile class
   *
   * @package ActiveCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseRecurringProfile extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'recurring_profiles';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'company_id', 'company_address', 'currency_id', 'language_id', 'name', 'note', 'our_comment', 'created_on', 'created_by_id', 'created_by_name', 'created_by_email', 'start_on', 'frequency', 'occurrences', 'auto_issue', 'invoice_due_after', 'allow_payments', 'project_id', 'state', 'original_state', 'triggered_number', 'last_triggered_on', 'next_trigger_on', 'visibility', 'recipient_id', 'recipient_name', 'recipient_email');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of company_id field
     *
     * @return integer
     */
    function getCompanyId() {
      return $this->getFieldValue('company_id');
    } // getCompanyId
    
    /**
     * Set value of company_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCompanyId($value) {
      return $this->setFieldValue('company_id', $value);
    } // setCompanyId

    /**
     * Return value of company_address field
     *
     * @return string
     */
    function getCompanyAddress() {
      return $this->getFieldValue('company_address');
    } // getCompanyAddress
    
    /**
     * Set value of company_address field
     *
     * @param string $value
     * @return string
     */
    function setCompanyAddress($value) {
      return $this->setFieldValue('company_address', $value);
    } // setCompanyAddress

    /**
     * Return value of currency_id field
     *
     * @return integer
     */
    function getCurrencyId() {
      return $this->getFieldValue('currency_id');
    } // getCurrencyId
    
    /**
     * Set value of currency_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCurrencyId($value) {
      return $this->setFieldValue('currency_id', $value);
    } // setCurrencyId

    /**
     * Return value of language_id field
     *
     * @return integer
     */
    function getLanguageId() {
      return $this->getFieldValue('language_id');
    } // getLanguageId
    
    /**
     * Set value of language_id field
     *
     * @param integer $value
     * @return integer
     */
    function setLanguageId($value) {
      return $this->setFieldValue('language_id', $value);
    } // setLanguageId

    /**
     * Return value of name field
     *
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of note field
     *
     * @return string
     */
    function getNote() {
      return $this->getFieldValue('note');
    } // getNote
    
    /**
     * Set value of note field
     *
     * @param string $value
     * @return string
     */
    function setNote($value) {
      return $this->setFieldValue('note', $value);
    } // setNote

    /**
     * Return value of our_comment field
     *
     * @return string
     */
    function getOurComment() {
      return $this->getFieldValue('our_comment');
    } // getOurComment
    
    /**
     * Set value of our_comment field
     *
     * @param string $value
     * @return string
     */
    function setOurComment($value) {
      return $this->setFieldValue('our_comment', $value);
    } // setOurComment

    /**
     * Return value of created_on field
     *
     * @return DateTimeValue
     */
    function getCreatedOn() {
      return $this->getFieldValue('created_on');
    } // getCreatedOn
    
    /**
     * Set value of created_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setCreatedOn($value) {
      return $this->setFieldValue('created_on', $value);
    } // setCreatedOn

    /**
     * Return value of created_by_id field
     *
     * @return integer
     */
    function getCreatedById() {
      return $this->getFieldValue('created_by_id');
    } // getCreatedById
    
    /**
     * Set value of created_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCreatedById($value) {
      return $this->setFieldValue('created_by_id', $value);
    } // setCreatedById

    /**
     * Return value of created_by_name field
     *
     * @return string
     */
    function getCreatedByName() {
      return $this->getFieldValue('created_by_name');
    } // getCreatedByName
    
    /**
     * Set value of created_by_name field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByName($value) {
      return $this->setFieldValue('created_by_name', $value);
    } // setCreatedByName

    /**
     * Return value of created_by_email field
     *
     * @return string
     */
    function getCreatedByEmail() {
      return $this->getFieldValue('created_by_email');
    } // getCreatedByEmail
    
    /**
     * Set value of created_by_email field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByEmail($value) {
      return $this->setFieldValue('created_by_email', $value);
    } // setCreatedByEmail

    /**
     * Return value of start_on field
     *
     * @return DateValue
     */
    function getStartOn() {
      return $this->getFieldValue('start_on');
    } // getStartOn
    
    /**
     * Set value of start_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setStartOn($value) {
      return $this->setFieldValue('start_on', $value);
    } // setStartOn

    /**
     * Return value of frequency field
     *
     * @return string
     */
    function getFrequency() {
      return $this->getFieldValue('frequency');
    } // getFrequency
    
    /**
     * Set value of frequency field
     *
     * @param string $value
     * @return string
     */
    function setFrequency($value) {
      return $this->setFieldValue('frequency', $value);
    } // setFrequency

    /**
     * Return value of occurrences field
     *
     * @return integer
     */
    function getOccurrences() {
      return $this->getFieldValue('occurrences');
    } // getOccurrences
    
    /**
     * Set value of occurrences field
     *
     * @param integer $value
     * @return integer
     */
    function setOccurrences($value) {
      return $this->setFieldValue('occurrences', $value);
    } // setOccurrences

    /**
     * Return value of auto_issue field
     *
     * @return boolean
     */
    function getAutoIssue() {
      return $this->getFieldValue('auto_issue');
    } // getAutoIssue
    
    /**
     * Set value of auto_issue field
     *
     * @param boolean $value
     * @return boolean
     */
    function setAutoIssue($value) {
      return $this->setFieldValue('auto_issue', $value);
    } // setAutoIssue

    /**
     * Return value of invoice_due_after field
     *
     * @return integer
     */
    function getInvoiceDueAfter() {
      return $this->getFieldValue('invoice_due_after');
    } // getInvoiceDueAfter
    
    /**
     * Set value of invoice_due_after field
     *
     * @param integer $value
     * @return integer
     */
    function setInvoiceDueAfter($value) {
      return $this->setFieldValue('invoice_due_after', $value);
    } // setInvoiceDueAfter

    /**
     * Return value of allow_payments field
     *
     * @return string
     */
    function getAllowPayments() {
      return $this->getFieldValue('allow_payments');
    } // getAllowPayments
    
    /**
     * Set value of allow_payments field
     *
     * @param string $value
     * @return string
     */
    function setAllowPayments($value) {
      return $this->setFieldValue('allow_payments', $value);
    } // setAllowPayments

    /**
     * Return value of project_id field
     *
     * @return integer
     */
    function getProjectId() {
      return $this->getFieldValue('project_id');
    } // getProjectId
    
    /**
     * Set value of project_id field
     *
     * @param integer $value
     * @return integer
     */
    function setProjectId($value) {
      return $this->setFieldValue('project_id', $value);
    } // setProjectId

    /**
     * Return value of state field
     *
     * @return integer
     */
    function getState() {
      return $this->getFieldValue('state');
    } // getState
    
    /**
     * Set value of state field
     *
     * @param integer $value
     * @return integer
     */
    function setState($value) {
      return $this->setFieldValue('state', $value);
    } // setState

    /**
     * Return value of original_state field
     *
     * @return integer
     */
    function getOriginalState() {
      return $this->getFieldValue('original_state');
    } // getOriginalState
    
    /**
     * Set value of original_state field
     *
     * @param integer $value
     * @return integer
     */
    function setOriginalState($value) {
      return $this->setFieldValue('original_state', $value);
    } // setOriginalState

    /**
     * Return value of triggered_number field
     *
     * @return integer
     */
    function getTriggeredNumber() {
      return $this->getFieldValue('triggered_number');
    } // getTriggeredNumber
    
    /**
     * Set value of triggered_number field
     *
     * @param integer $value
     * @return integer
     */
    function setTriggeredNumber($value) {
      return $this->setFieldValue('triggered_number', $value);
    } // setTriggeredNumber

    /**
     * Return value of last_triggered_on field
     *
     * @return DateValue
     */
    function getLastTriggeredOn() {
      return $this->getFieldValue('last_triggered_on');
    } // getLastTriggeredOn
    
    /**
     * Set value of last_triggered_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setLastTriggeredOn($value) {
      return $this->setFieldValue('last_triggered_on', $value);
    } // setLastTriggeredOn

    /**
     * Return value of next_trigger_on field
     *
     * @return DateValue
     */
    function getNextTriggerOn() {
      return $this->getFieldValue('next_trigger_on');
    } // getNextTriggerOn
    
    /**
     * Set value of next_trigger_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setNextTriggerOn($value) {
      return $this->setFieldValue('next_trigger_on', $value);
    } // setNextTriggerOn

    /**
     * Return value of visibility field
     *
     * @return integer
     */
    function getVisibility() {
      return $this->getFieldValue('visibility');
    } // getVisibility
    
    /**
     * Set value of visibility field
     *
     * @param integer $value
     * @return integer
     */
    function setVisibility($value) {
      return $this->setFieldValue('visibility', $value);
    } // setVisibility

    /**
     * Return value of recipient_id field
     *
     * @return integer
     */
    function getRecipientId() {
      return $this->getFieldValue('recipient_id');
    } // getRecipientId
    
    /**
     * Set value of recipient_id field
     *
     * @param integer $value
     * @return integer
     */
    function setRecipientId($value) {
      return $this->setFieldValue('recipient_id', $value);
    } // setRecipientId

    /**
     * Return value of recipient_name field
     *
     * @return string
     */
    function getRecipientName() {
      return $this->getFieldValue('recipient_name');
    } // getRecipientName
    
    /**
     * Set value of recipient_name field
     *
     * @param string $value
     * @return string
     */
    function setRecipientName($value) {
      return $this->setFieldValue('recipient_name', $value);
    } // setRecipientName

    /**
     * Return value of recipient_email field
     *
     * @return string
     */
    function getRecipientEmail() {
      return $this->getFieldValue('recipient_email');
    } // getRecipientEmail
    
    /**
     * Set value of recipient_email field
     *
     * @param string $value
     * @return string
     */
    function setRecipientEmail($value) {
      return $this->setFieldValue('recipient_email', $value);
    } // setRecipientEmail

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'company_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'company_address':
          return parent::setFieldValue($real_name, (string) $value);
        case 'currency_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'language_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'name':
          return parent::setFieldValue($real_name, (string) $value);
        case 'note':
          return parent::setFieldValue($real_name, (string) $value);
        case 'our_comment':
          return parent::setFieldValue($real_name, (string) $value);
        case 'created_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'created_by_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'created_by_name':
          return parent::setFieldValue($real_name, (string) $value);
        case 'created_by_email':
          return parent::setFieldValue($real_name, (string) $value);
        case 'start_on':
          return parent::setFieldValue($real_name, dateval($value));
        case 'frequency':
          return parent::setFieldValue($real_name, (string) $value);
        case 'occurrences':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'auto_issue':
          return parent::setFieldValue($real_name, (boolean) $value);
        case 'invoice_due_after':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'allow_payments':
          return parent::setFieldValue($real_name, (string) $value);
        case 'project_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'state':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'original_state':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'triggered_number':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'last_triggered_on':
          return parent::setFieldValue($real_name, dateval($value));
        case 'next_trigger_on':
          return parent::setFieldValue($real_name, dateval($value));
        case 'visibility':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'recipient_id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'recipient_name':
          return parent::setFieldValue($real_name, (string) $value);
        case 'recipient_email':
          return parent::setFieldValue($real_name, (string) $value);
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }