<?php

  /**
   * InvoiceNoteTemplates class
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class InvoiceNoteTemplates extends BaseInvoiceNoteTemplates {
  	
  	/**
  	 * Find notes for select
  	 * 
  	 * @return array
  	 */
  	public function findForSelect() {
  		$invoice_notes = InvoiceNoteTemplates::find();
  		
      $cleaned_notes = array();
      if (is_foreachable($invoice_notes)) {
        foreach ($invoice_notes as $invoice_note) {
        	$cleaned_notes[$invoice_note->getId()] = $invoice_note->getContent();
        } // foreach
      } // if
      
      return $cleaned_notes;
  	} // getCleanedNotes
  	
  	/**
  	 * Return slice of invoice note  definitions based on given criteria
  	 * 
  	 * @param integer $num
  	 * @param array $exclude
  	 * @param integer $timestamp
  	 * @return DBResult
  	 */
  	function getSlice($num = 10, $exclude = null, $timestamp = null) {
  		if($exclude) {
  			return self::find(array(
  			  'conditions' => array('id NOT IN (?)', $exclude), 
  			  'order' => 'name', 
  			  'limit' => $num,  
  			));
  		} else {
  			return self::find(array(
  			  'order' => 'name', 
  			  'limit' => $num,  
  			));
  		} // if
  	} // getSlice
  
  }