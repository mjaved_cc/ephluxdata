{add_bread_crumb}Details{/add_bread_crumb}

<div id="recurring_profile_details">
{object object=$active_recurring_profile user=$logged_user}

  <div class="wireframe_content_wrapper">
    <div id="invoice_{$active_recurring_profile->getId()}">
      <div class="object_inspector"><div class="object_inspector_inner">
                
      </div></div>
    </div>
  </div>
  
  <div class="invoice_paper_wrapper recurring_profile_wrapper recurring_profile_{$active_recurring_profile->getId()}">
    <div class="invoice_paper recurring_profile">
      <div class="invoice_paper_below">
        <div class="invoice_paper_top"></div>
        <div class="invoice_paper_center"><div class="invoice_paper_area"></div></div>
        <div class="invoice_paper_bottom"></div>
      </div>
      
      <div class="invoice_paper_top"></div>
      <div class="invoice_paper_center">
        <div class="invoice_paper_area">
          <div class="invoice_paper_logo"></div>
          
          <div class="invoice_comment property_wrapper" style="display: {if $active_recurring_profile->getOurComment()}block{else}none{/if}"><span class="invoice_comment_paperclip"></span><span class="property_invoice_comment">{$active_recurring_profile->getOurComment()}</span></div>
          
          <div class="invoice_paper_header">
            <div class="invoice_paper_details">
              <h2><span class="property_invoice_name">{$active_recurring_profile->getName()}</span></h2>
              <ul>                
                <li class="invoice_created_on property_wrapper">{lang}Created On{/lang}: <span class="property_invoice_created_on">{$active_recurring_profile->getCreatedOn()|date:0}</span></li>                
              </ul>
            </div>
            
            <div class="invoice_paper_client"><div class="invoice_paper_client_inner">
              <div class="invoice_paper_client_name property_wrapper"><span class="property_invoice_client_name">{company_link company=$active_recurring_profile->getCompany()}</span></div>
              <div class="invoice_paper_client_address property_wrapper"><span class="property_invoice_client_address">{$active_recurring_profile->getCompanyAddress()|clean|nl2br nofilter}</span></div>
            </div></div>
          </div>
          
          <div class="invoice_paper_items">
            {if is_foreachable($active_recurring_profile->getItems())}
              <table cellspacing="0" >
                <thead>
                  <tr>
                    <td class="num"></th>
                    <td class="description">{lang}Description{/lang}</td>
                    <td class="quantity">{lang}Qty.{/lang}</td>
                    <td class="unit_cost">{lang}Unit Cost{/lang}</td>
                    <td class="tax_rate">{lang}Tax{/lang}</td>
                    <td class="total">{lang}Total{/lang}</td>
                  </tr>
                </thead>
                <tbody>
                {foreach from=$active_recurring_profile->getItems() item=invoice_item}
                  <tr class="{cycle values='odd,even'}">
                    <td class="num">#{$invoice_item->getPosition()}</td>
                    <td class="description">{$invoice_item->getDescription()}</td>
                    <td class="quantity">{$invoice_item->getQuantity()|money}</td>
                    <td class="unit_cost">{$invoice_item->getUnitCost()|money}</td>
                    <td class="tax_rate">{$invoice_item->getTaxRateName()}</td>
                    <td class="total">{$invoice_item->getTotal()|money}</td>
                  </tr>
                {/foreach}
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5" class="label">{lang}Subtotal{/lang}</td>
                    <td class="value"><span class="property_wrapper property_invoice_subtotal">{$active_recurring_profile->getTotal()|money}</span></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="label">{lang}Tax{/lang}</td>
                    <td class="value"><span class="property_wrapper property_invoice_tax">{$active_recurring_profile->getTax()|money}</span></td>
                  </tr>
                  <tr class="total">
                    <td colspan="5" class="label">{lang}Total{/lang}</td>
                    <td class="value total"><span class="property_wrapper property_invoice_total">{$active_recurring_profile->getTaxedTotal()|money}</span></td>
                  </tr>
                </tfoot>
              </table>
            {else}
              <p class="empty_page"><span class="inner">{lang}This invoice has no items{/lang}</span></p>
            {/if}
          </div>
          
          
          <div class="invoice_paper_notes property_wrapper" style="display: {if $active_recurring_profile->getNote()}block{else}none{/if}">
            <h3>{lang}Note{/lang}</h3>
            <p><span class="property_invoice_note">{$active_recurring_profile->getNote()|nl2br nofilter}</span></p>
          </div>
          
        </div>
      </div>
      <div class="invoice_paper_bottom"></div>
      
      <div class="invoice_paper_peel_draft"></div>
      <div class="invoice_paper_stamp_paid"></div>
      <div class="invoice_paper_stamp_canceled"></div>
    </div>
  </div>
{/object}
</div>
  
<script type="text/javascript">
  var scope = "{$request->getEventScope()|json}";

{literal}
  App.Wireframe.Events.bind('request_resolved.' + scope, function(event, approval_request) {
		$("#approval_req_view").remove();
	});

  App.Wireframe.Events.bind('recurring_profile_updated.' + scope, function(event, invoice) {
		var wrapper = $('.recurring_profile_' + invoice.id);
    var wrapper_paper = wrapper.find('.invoice_paper:first');

    wrapper.updateProperty([
      {name: 'invoice_name', value: invoice.name.clean(), auto_hide : false },
      {name: 'invoice_currency', value: invoice.currency.code.clean(), auto_hide: false},
      {name: 'invoice_project', value: invoice.project ? '<a href="' + invoice.project.permalink + '">' + invoice.project.name.clean() + '</a>' : null},
      {name: 'invoice_created_on', value : invoice.created_on.formatted_date},
      {name: 'invoice_issued_on', value : invoice.issued_on},
      {name: 'invoice_due_on', value : invoice.due_on, auto_hide : true },
      {name: 'invoice_paid_on', value : invoice.paid_on},
      {name: 'invoice_closed_on', value : invoice.closed_on},
      {name: 'invoice_client_name', value : '<a href="' + invoice.client.permalink + '">' + invoice.client.name.clean() + '</a>'},
      {name: 'invoice_client_address', value : invoice.client_address.clean().nl2br()},
      {name: 'invoice_subtotal', value : invoice.subtotal.toFixed(2), auto_hide : false},
      {name: 'invoice_tax', value : invoice.tax.toFixed(2), auto_hide : false},
      {name: 'invoice_total', value : invoice.total.toFixed(2), auto_hide : false},
      {name: 'invoice_note', value : invoice.note.clean().nl2br(), auto_hide : true},
      {name: 'invoice_comment', value : invoice.our_comment.clean(), auto_hide : true}
    ]);
    
    var items_parent = wrapper.find('.invoice_paper_items table tbody').empty();
    var new_items = '';
    if (invoice.items && invoice.items.length) {
      $.each(invoice.items, function (item_index, item) {
        new_items += '<tr>' +
          '<td class="num">#' + item.position + '</td>' +
          '<td class="description">' + item.description.clean() + '</td>' +
          '<td class="unit_cost">' + item.unit_cost.toFixed(2) + '</td>' +
          '<td class="quantity">' + item.quantity.toFixed(2) + '</td>' +
	         '<td class="tax_rate">' + item.tax.name.clean() + '</td>' +
	         '<td class="total">' + item.total.toFixed(2) + '</td>' +
        '</tr>';
      });
      items_parent.html(new_items);
    } // if
  });
  
{/literal}
</script>