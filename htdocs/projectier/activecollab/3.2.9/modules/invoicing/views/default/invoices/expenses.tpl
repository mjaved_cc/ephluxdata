{title lang=false}{lang name=$active_invoice->getName()}:name Expenses{/lang}{/title}
{add_bread_crumb}Expenses{/add_bread_crumb}

<div id="invoice_time">
{if is_foreachable($expenses)}
  <div id="timerecords">
    <table class="common">
      <tr>
        <th class="date">{lang}Date{/lang}</th>
        <th class="user">{lang}User{/lang}</th>
        <th class="hours">{lang}Cost{/lang}</th>
        <th class="description">{lang}Description{/lang}</th>
      </tr>
    {foreach from=$expenses item=expense}
      <tr>
        <td class="date">{$expense->getRecordDate()|date:0}</td>
        <td class="user">{user_link user=$expense->getUser()}</td>
        <td class="hours"><b>{$expense->getValue()}</b></td>
        <td class="description">
        {if $expense->getParent() instanceof ProjectObject}
          {object_link object=$expense->getParent()} 
          {if $expense->getSummary()}
            &mdash; {$expense->getSummary()}
          {/if}
        {else}
          {$expense->getSummary()}
        {/if}
        </td>
        
      </tr>
    {/foreach}
    </table>
  </div>
  <p id="release_invoice_time_records" style='text-align: center;'>{link href=$active_invoice->getReleaseExpenseUrl() method=post confirm='Are you sure that you want to remove relation between this invoice and expenses listed above? Note that expenses will NOT be deleted!'}Release Expenses{/link}</p>
  {empty_slate name=expenses module=invoicing}
{else}
  <p class="empty_page"><span class="inner">{lang}There is no expenses attached to this invoice{/lang}</span></p>
{/if}
</div>