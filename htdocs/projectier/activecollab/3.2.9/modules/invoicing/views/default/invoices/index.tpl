{title}Invoices{/title}
{add_bread_crumb}All Invoices{/add_bread_crumb}
<!-- mass_edit_url="{assemble route=invoices}" -->

<div id="invoices">
	<div class="empty_content">
		<div class="objects_list_title">{lang}Invoices{/lang}</div>
		<div class="objects_list_icon"><img src="{image_url name='icons/48x48/invoicing.png' module=invoicing}" alt=""/></div>
		<div class="objects_list_details_actions">
        <ul>
            <li><a href="{assemble route='invoices_add'}" id="new_invoice">{lang}New Invoice{/lang}</a></li>
        </ul>
		</div>


		<div class="object_lists_details_tips">
		  <h3>{lang}Tips{/lang}:</h3>
		  <ul>
		    <li>{lang}To select a invoice and load its details, please click on it in the list on the left{/lang}</li>
		    <!--<li>{lang}It is possible to select multiple invoices at the same time. Just hold Ctrl key on your keyboard and click on all the invoices that you want to select{/lang}</li>-->
		  </ul>
		</div>
	</div>

<!--	<div class="multi_content">-->
<!--		<table>-->
<!--	    <tr>-->
<!--	      <td class="checkbox"><label><input type="checkbox" value="change_completed" />{lang}Change Status{/lang}</label></td>-->
<!--	      <td class="new_value">-->
<!--	        <select name="is_completed">-->
<!--	          <option value="0">{lang}Active{/lang}</option>-->
<!--	          <option value="1">{lang}Completed{/lang}</option>-->
<!--	        </select>-->
<!--	      </td>-->
<!--	    </tr>-->
<!--	    <tr>-->
<!--	      <td class="checkbox"><label><input type="checkbox" value="change_category" />{lang}Change Category{/lang}</label></td>-->
<!--	      <td class="new_value">{select_project_category name=category_id user=$logged_user id="mass_edit_project_category" on_new_category=on_new_project_category}</td>-->
<!--	    </tr>-->
<!--	    <tr>-->
<!--      	<td class="checkbox"><label><input type="checkbox" value="change_client" />{lang}Change Client{/lang}</label></td>-->
<!--		    <td class="new_value">{select_company name=company_id user=$logged_user optional=true can_create_new=false}</td>-->
<!--		  </tr>-->
<!--	    <tr>-->
<!--      	<td class="checkbox"><label><input type="checkbox" value="change_label" />{lang}Change Label{/lang}</label></td>-->
<!--		    <td class="new_value">{select_project_label name=label_id optional=true user=$logged_user can_create_new=false}</td>-->
<!--		  </tr>-->
<!--		</table>-->
<!--	</div>-->
</div>

<script type="text/javascript">
  $('#new_invoice').flyoutForm({
    'success_event' : 'invoice_created',
    'title': App.lang('New Invoice')
  });
  
  $('#invoices').each(function() {
    var objects_list_wrapper = $(this);
    
    var items = {$invoices|json nofilter};
    var companies_map = {$companies_map|json nofilter};
    var states_map = {$invoice_states_map|json nofilter};
    var dates_map = {$invoice_dates_map|json nofilter};
    var mass_edit_url = '{assemble route=invoices}';
    var print_url = '{assemble route=invoices print=1}';

    objects_list_wrapper.objectsList({
      'id' : 'invoices',
      'items' : items,
      'objects_type' : 'invoices',
      'required_fields' : ['id', 'long_name', 'name', 'client_id', 'client_name', 'status', 'permalink'],
      'print_url' : print_url,
      'events' : App.standardObjectsListEvents(),
  		'multi_title' : App.lang(':num Invoices Selected'),
  		'multi_url' : mass_edit_url,
      'prepare_item' : function (item) {
          // update issued on grouping map
          if (item['issued_on_month'] && item['issued_on_month_verbose']) {
            var issued_on_grouping = objects_list_wrapper.objectsList('grouping_map_get', 'issued_on_month');
            if (!issued_on_grouping.map[item['issued_on_month']]) {
              var issued_on_grouping_map = issued_on_grouping.map;
              var new_issued_on_grouping_map = new Object();

              var inserted = false;
              $.each(issued_on_grouping_map, function (index, value) {
                if ((item['issued_on_month'] > index) && !inserted) {
                  new_issued_on_grouping_map[item['issued_on_month']] = item['issued_on_month_verbose'];
                  inserted = true;
                } // if
                new_issued_on_grouping_map[index] = value;
              });

              if (!inserted) {
                new_issued_on_grouping_map[item['issued_on_month']] = item['issued_on_month_verbose'];
              } // if

              objects_list_wrapper.objectsList('grouping_map_replace', 'issued_on_month', new_issued_on_grouping_map);
            } // if
          } // if

          // update due on grouping map
          if (item['due_on_month'] && item['due_on_month_verbose']) {
            var due_on_grouping = objects_list_wrapper.objectsList('grouping_map_get', 'due_on_month');
            if (!due_on_grouping.map[item['due_on_month']]) {
              var due_on_grouping_map = due_on_grouping.map;
              var new_due_on_grouping_map = new Object();

              var inserted = false;
              $.each(due_on_grouping_map, function (index, value) {
                if ((item['due_on_month'] > index) && !inserted) {
                  new_due_on_grouping_map[item['due_on_month']] = item['due_on_month_verbose'];
                  inserted = true;
                } // if
                new_due_on_grouping_map[index] = value;
              });

              if (!inserted) {
                new_due_on_grouping_map[item['due_on_month']] = item['due_on_month_verbose'];
              } // if

              objects_list_wrapper.objectsList('grouping_map_replace', 'due_on_month', new_due_on_grouping_map);
            } // if
          } // if

        return {
          'id' : item['id'],
  	  		'name' : item['short_name'],
  	  		'long_name' : item['name'],
  	  		'client_id' : item['client']['id'],
  	  		'client_name' : item['client']['name'],
          'issued_on_month' : item['issued_on_month'],
          'due_on_month' : item['due_on_month'],
  	  		'status' : item['status'],
  	  		'permalink' : item['urls']['view']
        };
      },

      'grouping' : [{ 
        'label' : App.lang("Don't group"), 
        'property' : '', 
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      }, { 
        'label' : App.lang('By Client'), 
        'property' : 'client_id',  
        'map' : companies_map, 
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-client.png', 'system') 
      }, { 
        'label' : App.lang('By Status'), 
        'property' : 'status', 
        'map' : states_map , 
        'default' : true, 
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-status.png', 'environment')
      }, {
        'label' : App.lang('Issued On'),
        'property' : 'issued_on_month',
        'map' : dates_map.issued_on,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-status.png', 'environment'),
        'uncategorized_label' : App.lang('Not Issued')
      }, {
        'label' : App.lang('Payment Due On'),
        'property' : 'due_on_month',
        'map' : dates_map.due_on,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-status.png', 'environment'),
        'uncategorized_label' : App.lang('No Due Date')
      }],

      'filtering' : [{ 
        'label' : App.lang('Status'), 
        'property'  : 'status', 
        'values'  : [{ 
          'label' : App.lang('All Invoices'), 
          'value' : '', 
          'icon' : App.Wireframe.Utils.imageUrl('objects-list/all-invoices.png', 'invoicing') , 
          'default' : true, 
          'breadcrumbs' : App.lang('All Invoices')  
        }, { 
          'label' : App.lang('Drafts'), 
          'value' : '0', 
          'icon' : App.Wireframe.Utils.imageUrl('objects-list/draft-invoices.png', 'invoicing'), 
          'breadcrumbs' : App.lang('Drafts') 
        }, { 
          'label' : App.lang('Issued'), 
          'value' : '1', 
          'icon' : App.Wireframe.Utils.imageUrl('objects-list/issued-invoices.png', 'invoicing'), 
          'breadcrumbs' : App.lang('Issued')
        }, { 
          'label' : App.lang('Paid'),
          'value' : '2', 
          'icon' : App.Wireframe.Utils.imageUrl('objects-list/paid-invoices.png', 'invoicing'),
          'breadcrumbs' : App.lang('Paid')
        }, { 
          'label' : App.lang('Canceled'), 
          'value' : '3', 
          'icon' : App.Wireframe.Utils.imageUrl('objects-list/canceled-invoices.png', 'invoicing'), 
          'breadcrumbs' : App.lang('Canceled') 
        }]
      }]
    });

    // invoice added
    App.Wireframe.Events.bind('invoice_created.content', function (event, invoice) {
      objects_list_wrapper.objectsList('add_item', invoice);
    });

    // invoice updated
    App.Wireframe.Events.bind('invoice_updated.content invoice_issued.content invoice_canceled.content', function (event, invoice) {
      objects_list_wrapper.objectsList('update_item', invoice);
    });

    // invoice deleted
    App.Wireframe.Events.bind('invoice_deleted.content', function (event, invoice) {
      objects_list_wrapper.objectsList('delete_item', invoice['id']);
      objects_list_wrapper.objectsList('load_empty');
    });

    App.Wireframe.Events.bind('payment_updated.content payment_deleted.content', function (event, payment) {
      objects_list_wrapper.objectsList('update_item', payment['invoice']);
      $('#render_object_payments').paymentContainer('update_payment',payment);
      App.Wireframe.Events.trigger('invoice_updated', payment['invoice']);
    });

    App.Wireframe.Events.bind('payment_created.content', function (event, payment) {
      if(typeof(payment) == 'object') {
        objects_list_wrapper.objectsList('update_item', payment['invoice']);
        $('#render_object_payments').paymentContainer('add_payment',payment);
        App.Wireframe.Events.trigger('invoice_updated', payment['invoice']);
      } else {
        App.Wireframe.Flash.error(payment);
      }
    });


    // keep client_id map up to date
    App.objects_list_keep_companies_map_up_to_date(objects_list_wrapper, 'client_id', 'content');

	  {if $active_invoice->isLoaded()}
    objects_list_wrapper.objectsList('load_item', {$active_invoice->getId()}, {$active_invoice->getViewUrl()|json nofilter}); // Pre select item if this is permalink
	  {/if}
  });
</script>