{object object=$active_quote user=$logged_user show_inspector=true}
  {assign_var name=quote_class}
    {if $active_quote->isDraft()}
      quote_draft
    {elseif $active_quote->isSent()}
      quote_sent
    {elseif $active_quote->isWon()}
      quote_won
    {elseif $active_quote->isLost()}
      quote_lost
    {/if}
  {/assign_var}

  <div class="invoice_paper_wrapper {$quote_class|trim}_wrapper quote_{$active_quote->getId()}">
    <div class="invoice_paper {$quote_class|trim}">
      <div class="invoice_paper_below">
        <div class="invoice_paper_top"></div>
        <div class="invoice_paper_center"><div class="invoice_paper_area"></div></div>
        <div class="invoice_paper_bottom"></div>
      </div>

      <div class="invoice_paper_top"></div>
      <div class="invoice_paper_center">
        <div class="invoice_paper_area">
          <div class="invoice_paper_logo"></div>
          {if Quotes::canManage($logged_user)}
          <div class="invoice_comment property_wrapper" style="display: {if $active_quote->getPrivateNote()}block{else}none{/if}"><span class="invoice_comment_paperclip"></span><span class="property_invoice_comment property_quote_private_note">{$active_quote->getPrivateNote()}</span></div>
          {/if}
          <div class="invoice_paper_header">
            <div class="invoice_paper_details">
              <h2><span class="property_quote_name">{$active_quote->getName()}</span></h2>
              <ul>

                <li class="quote_currency property_wrapper">{lang}Currency{/lang}: <strong><span class="property_quote_currency">{$active_quote->getCurrencyCode()}</span></strong></li>
                <li class="quote_created_on property_wrapper">{lang}Created On{/lang}: <span class="property_quote_created_on">{$active_quote->getCreatedOn()|date}</span></li>
                <li class="quote_sent_on property_wrapper">{lang}Sent On{/lang}: <span class="property_quote_sent_on">{$active_quote->getSentOn()|date}</span></li>
                <li class="quote_closed_on property_wrapper">{lang}Closed On{/lang}: <span class="property_quote_closed_on">{$active_quote->getClosedOn()|date}</span></li>

              </ul>
            </div>

            <div class="invoice_paper_client"><div class="invoice_paper_client_inner">
              <div class="invoice_paper_client_name property_wrapper">
                <span class="property_quote_client_name">
                  {if $active_quote->getCompany() instanceof Company}
                    {company_link company=$active_quote->getCompany()}
                  {else}
                    <b>{$active_quote->getCompanyName()}</b>
                  {/if}
                  <br/>
                  {$active_quote->getCompanyAddress()|clean|nl2br nofilter}
                </span>
              </div>
              <div class="invoice_paper_client_address property_wrapper">
                <span class="property_quote_client_address">
                  {lang}Contact Person{/lang}: {user_link user=$active_quote->getRecipient()}
                </span>
              </div>
              {if !($active_quote->getCompany() instanceof Company) && $logged_user->isPeopleManager()}
              <!--<div class="property_wrapper">-->
                <span class="client_save_data">
                  <a href="{$active_quote->getSaveClientUrl()}" title="{lang}Add Client to People{/lang}">
                    <img src="{image_url name='icons/16x16/save_client.png' module=system}" alt="{lang}Add Client to People{/lang}"/>
                  </a>
                </span>
              <!--</div>-->
              {/if}
            </div></div>
          </div>

          <div class="invoice_paper_items">
            {if is_foreachable($active_quote->getItems())}
              <table cellspacing="0" >
                <thead>
                  <tr>
                    <td class="num"></td>
                    <td class="description">{lang}Description{/lang}</td>
                    <td class="quantity">{lang}Qty.{/lang}</td>
                    <td class="unit_cost">{lang}Unit Cost{/lang}</td>
                    <td class="tax_rate">{lang}Tax{/lang}</td>
                    <td class="total">{lang}Total{/lang}</td>
                  </tr>
                </thead>
                <tbody>
                {foreach from=$active_quote->getItems() item=quote_item}
                  <tr class="{cycle values='odd,even'}">
                    <td class="num">#{$quote_item->getPosition()}</td>
                    <td class="description">{$quote_item->getDescription()}</td>
                    <td class="quantity">{$quote_item->getQuantity()|money}</td>
                    <td class="unit_cost">{$quote_item->getUnitCost()|money}</td>
                    <td class="tax_rate">{$quote_item->getTaxRateName()}</td>
                    <td class="total">{$quote_item->getTotal()|money}</td>
                  </tr>
                {/foreach}
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="5" class="label">{lang}Subtotal{/lang}</td>
                    <td class="value"><span class="property_wrapper property_quote_subtotal">{$active_quote->getTotal()|money}</span></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="label">{lang}Tax{/lang}</td>
                    <td class="value"><span class="property_wrapper property_quote_tax">{$active_quote->getTax()|money}</span></td>
                  </tr>
                  <tr class="total">
                    <td colspan="5" class="label">{lang}Total{/lang}</td>
                    <td class="value total"><span class="property_wrapper property_quote_total">{$active_quote->getTaxedTotal()|money}</span></td>
                  </tr>
                </tfoot>
              </table>
            {else}
              <p class="empty_page"><span class="inner">{lang}This quote has no items{/lang}</span></p>
            {/if}
          </div>


          <div class="invoice_paper_notes property_wrapper" style="display: {if $active_quote->getNote()}block{else}none{/if}">
            <h3>{lang}Note{/lang}</h3>
            <p><span class="property_quote_note">{$active_quote->getNote()|clean|nl2br nofilter}</span></p>
          </div>

        </div>
      </div>
      <div class="invoice_paper_bottom"></div>

      <div class="invoice_paper_peel_draft"></div>
      <div class="invoice_paper_stamp_paid"></div>
      <div class="invoice_paper_stamp_canceled"></div>
    </div>
  </div>

  <div class="wireframe_content_wrapper">
    {object_comments object=$active_quote user=$logged_user}
  </div>

  <script type="text/javascript">
    var client_save_link = $('div.invoice_paper_client').find('span.client_save_data');
    var can_save_client = {if $logged_user->isPeopleManager() && !$active_quote->getCompany() instanceof Company}true{else}false{/if};

    /**
     * Handle what happens when invoices get edited
     *
     */
    App.Wireframe.Events.bind('quote_updated.{$request->getEventScope()}', function(event, quote) {
      var wrapper = $('.quote_' + quote.id).removeClass('quote_draft_wrapper quote_sent_wrapper quote_won_wrapper quote_lost_wrapper');
      var wrapper_paper = wrapper.find('.invoice_paper:first').removeClass('quote_draft quote_sent quote_won quote_lost');

      var quote_client_name;
      if (quote.client['class'] == undefined) {
        quote_client_name = '<b>' + quote.client.name.clean() + '</b>';
      } else {
        if (client_save_link && client_save_link.length) {
          client_save_link.hide();
        } // if
        quote_client_name = '<a href="' + quote.client.permalink + '">' + quote.client.name.clean() + '</a>';
      } // if
      quote_client_name += '<br/>' + quote.company_address;

      wrapper.updateProperty(
        [{
          'name' : 'quote_name',
          'value' : quote.name.clean(),
          'auto_hide' : false
        }, {
          'name' : 'quote_currency',
          'value' : quote.currency.code.clean(),
          'auto_hide' : false
        }, {
          'name' : 'quote_project',
          'value' : quote.project ? '<a href="' + quote.project.permalink + '">' + quote.project.name.clean() + '</a>' : null
        }, {
          'name' : 'quote_sent_on',
          'value' : quote.sent_on ? quote.sent_on.formatted_date : ''
        }, {
          'name' : 'quote_closed_on',
          'value' : quote.closed_on ? quote.closed_on.formatted_date : ''
        }, {
          'name' : 'quote_client_name',
          'value' : quote_client_name
        },{
          'name' : 'quote_client_address',
          'value' : App.lang('Contact Person') + ': <a href="' + quote.recipient.permalink + '">' + quote.recipient.display_name + '</a>'
        },{
          'name' : 'quote_subtotal',
          'value' : quote.subtotal.toFixed(2),
          'auto_hide' : false
        }, {
          'name' : 'quote_tax',
          'value' : quote.tax.toFixed(2),
          'auto_hide' : false
        }, {
          'name' : 'quote_total',
          'value' : quote.total.toFixed(2),
          'auto_hide' : false
        }, {
          'name' : 'quote_note',
          'value' : quote.note.clean().nl2br(),
          'auto_hide' : true
        }, {
          'name' : 'quote_private_note',
          'value' : quote.private_note.clean(),
          'auto_hide' : true
        }]
      );

      var items_parent = wrapper.find('.invoice_paper_items table tbody').empty();
      var new_items = '';
      if (quote.items && quote.items.length) {
        $.each(quote.items, function (item_index, item) {
          new_items += '<tr>' +
            '<td class="num">#' + item.position + '</td>' +
            '<td class="description">' + item.description.clean() + '</td>' +
            '<td class="quantity">' + item.quantity.toFixed(2) + '</td>' +
            '<td class="unit_cost">' + item.unit_cost.toFixed(2) + '</td>' +
            '<td class="tax_rate">' + item.tax.name.clean() + '</td>' +
            '<td class="total">' + item.total.toFixed(2) + '</td>' +
          '</tr>';
        });
        items_parent.html(new_items);
      } // if

      if (quote.status.is_draft) {
        wrapper.addClass('quote_draft_wrapper');
        wrapper_paper.addClass('quote_draft');
      } else if (quote.status.is_sent) {
        wrapper.addClass('quote_sent_wrapper');
        wrapper_paper.addClass('quote_sent');
      } else if (quote.status.is_won) {
        wrapper.addClass('quote_won_wrapper');
        wrapper_paper.addClass('quote_won');
      } else if (quote.status.is_list) {
        wrapper.addClass('quote_lost_wrapper');
        wrapper_paper.addClass('quote_lost');
      } // if
    });

    App.Wireframe.Events.bind('client_saved client_saved_for_invoice', function (event, quote) {


      if (event == 'client_saved_for_invoice') {

      } // if
    });

    App.Wireframe.Events.bind('create_invoice_from_quote', function (event, invoice) {
      if (invoice['class'] == 'Invoice') {
        App.Wireframe.Flash.success(App.lang('New invoice created.'));
        App.Wireframe.Content.setFromUrl(invoice['urls']['view']);
      } // if
    });

    $('div.invoice_paper_client').each(function() {
      if (can_save_client) {
        var link = $(this).find('span.client_save_data a');
        if (link) {
          link.flyoutForm({
            'title' : App.lang('Add Client to People'),
            'success_event' : 'quote_updated',
            'width' : 550
          });
        }
      } // if
    });

  </script>
{/object}