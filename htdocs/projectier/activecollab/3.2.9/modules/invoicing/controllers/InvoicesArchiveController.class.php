<?php

  // We need invoices controller
  AngieApplication::useController('invoices', INVOICING_MODULE);

  /**
   * Invoices archive controller
   *
   * @package activeCollab.modules.invoicing
   * @subpackage controllers
   */
  class InvoicesArchiveController extends InvoicesController {
    
    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->breadcrumbs->add('invoices_archive', lang('Archive'), Router::assemble('invoices_archive'));
    } // __construct
    
    /**
     * Show archive page
     */
    function index() {
      $this->smarty->assign('invoiced_companies', Invoices::findInvoicedCompaniesInfo($this->logged_user, array(INVOICE_STATUS_PAID, INVOICE_STATUS_CANCELED)));
    } // index
    
    /**
     * Show paid / canceled company invoices
     */
    function company() {
      
      $status = $this->request->get('status') ? $this->request->get('status') : 'paid';
      $company = null;
      
      $company_id = $this->request->getId('company_id');
      if($company_id) {
        $company = Companies::findById($company_id);
      } // if
      
      if($company instanceof Company) {
        $this->wireframe->breadcrumbs->add('company_invoices_archive', $company->getName(), Router::assemble('company_invoices', array('company_id' => $company->getId())));
      } else {
        $this->response->notFound();
      } // if

      $invoices = Invoices::findByCompany($company, $this->logged_user, $status == 'canceled' ? INVOICE_STATUS_CANCELED : INVOICE_STATUS_PAID, 'closed_on DESC');
      
      $this->smarty->assign(array(
        'company' => $company,
        'invoices' => group_invoices_by_currency($invoices),
        'status' => $status
      ));
    } // company
    
  }