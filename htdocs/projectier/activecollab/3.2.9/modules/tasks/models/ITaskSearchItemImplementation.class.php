<?php

  /**
   * Task search item implementation
   * 
   * @package activeCollab.modules.tasks
   * @subpackage models
   */
  class ITaskSearchItemImplementation extends IProjectObjectSearchItemImplementation {
    
//    /**
//     * Return item context for given index
//     * 
//     * @param SearchIndex $index
//     * @return string
//     */
//    function getContext(SearchIndex $index) {
//      return 'projects/' . $this->object->getProjectId() . '/tasks';
//    } // getContext
  
    /**
     * Return additional properties for a given index
     * 
     * @param SearchIndex $index
     * @return mixed
     */
    function getAdditional(SearchIndex $index) {
      if($index instanceof NamesSearchIndex) {
        return array(
          'name' => $this->object->getName(), 
          'short_name' => '#' . $this->object->getTaskId(), 
          'visibility' => $this->object->getVisibility(),
        );
      } else {
        return parent::getAdditional($index);
      } // if
    } // getAdditional
    
  }