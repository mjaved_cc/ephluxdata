<?php

  // Build on top of frontend controller
  AngieApplication::useController('frontend', SYSTEM_MODULE);

  /**
   * Public tasks controller
   * 
   * @package package
   * @subpackage subpackage
   */
  class PublicTasksController extends FrontendController {
  
    function index() {
      $this->smarty->assign('task_forms', PublicTaskForms::findEnabled());
    } // index
    
  }