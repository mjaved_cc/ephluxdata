<?php

  /**
   * notification_task_responsibility helper implementation
   *
   * @package activecollab.modules.tasks
   * @subpackage helpers
   */

  /**
   * Render new task responisble person on notification
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_notification_task_responsibility($params, &$smarty) {
    $context = array_required_var($params, 'context', false, 'ApplicationObject');
    $recipient = array_required_var($params, 'recipient', false, 'IUser');
    $language = array_required_var($params, 'language', false, 'Language');

    if($context->assignees()->getAssignee() instanceof IUser) {
      if($context->assignees()->isResponsible($recipient)) {
        $result = lang('<u>You are responsible</u> for this :type!', array('type' => $context->getVerboseType(true, $language)), null, $language);
      } elseif($context->assignees()->isAssignee($recipient)) {
        $result = lang('You are <u>assigned to this :type</u> and :responsible_name is responsible.', array('type' => $context->getVerboseType(true, $language), 'responsible_name' => $context->assignees()->getAssignee()->getDisplayName(true)), null, $language);
      }//if
      
      if($context->getDueOn()) {
        $result .= ' ' . lang('It is due on <u>:due_on</u>', array('due_on' => $context->getDueOn()->formatForUser($recipient)));
      }//if
      
      return $result;
    }//if
  
  } // smarty_function_notification_task_responsibility