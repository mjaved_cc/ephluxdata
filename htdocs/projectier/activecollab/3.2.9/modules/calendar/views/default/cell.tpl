<td class="day_cell {if $day->isToday()}today{/if} {if $day->isDayOff()}day_off{elseif $day->isWeekend()}weekend{else}weekday{/if} {if is_foreachable($day_data)}not_empty_day{else}empty_day{/if} {if $is_current_month}current_month{else}not_current_month{/if}" id="day-{$day->getYear()}-{$day->getMonth()}-{$day->getDay()}" date_title="{$day|date:0}">
  <div class="inner">
    <div class="day_num"><span day_url="{$day_url}">{$day->getDay()}</span></div>
    <div class="day_brief">
    {if is_foreachable($day_data)}
      <ul>
      {foreach from=$day_data item=object}
        <li class="object_type object_type_{$object->getType()|strtolower}" title="{$object->getVerboseType()}">
        {if $object instanceof IComplete && $object->complete()->isCompleted()}
          <del>{$object->getName()|excerpt:40}</del>
        {else}
          {$object->getName()|excerpt:40}
        {/if}
        </li>
      {/foreach}
      </ul>
    {/if}
    </div>
    <div class="day_details">
      {if is_foreachable($day_data)}
      <ul>
	      {foreach from=$day_data item=object}
	        <li>
	          <span class="object_type object_type_{$object->getType()|strtolower} inverse">{$object->getVerboseType()}</span>
            <span class="object_project">{project_link project=$object->getProject()}</span>
						<span class="object_link">{object_link object=$object}</span>
	        </li>
	      {/foreach}
      </ul>
      {else}
        <p class="empty_page"><span class="inner">{lang}No tasks for this day{/lang}</span></p>
      {/if}
    </div>
    <div class="hidden_data"></div>
  </div>
</td>