<?php

  /**
   * Calendar module on_main_menu event handler
   *
   * @package activeCollab.modules.calendar
   * @subpackage handlers
   */
  
  /**
   * Add options to main menu
   *
   * @param MainMenu $menu
   * @param User $user
   */
  function calendar_handle_on_main_menu(MainMenu &$menu, User &$user) {
    $menu->addBefore('calendar', lang('Calendar'), Calendar::getDashboardCalendarUrl(), AngieApplication::getImageUrl('main-menu/calendar.png', CALENDAR_MODULE, AngieApplication::INTERFACE_DEFAULT), null, 'admin');
  } // calendar_handle_on_main_menu