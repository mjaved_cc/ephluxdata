<?php

  /**
   * add_user_projects_select helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render select projects from all clients box
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_add_user_projects_select($params, &$smarty) {
    $user = array_required_var($params, 'user', true, 'IUser');
    $interface = array_var($params, 'interface', AngieApplication::getPreferedInterface(), true);
    
    $object = array_var($params, 'object', null, true);
    
    // We need to get all active projects
    $projects = Projects::find(array(
    	'conditions' => array("state >= ? AND completed_on IS NULL", STATE_VISIBLE),
      'order' => 'name DESC'
    ));
    
    // User projects
    $user_projects = ($temp = Projects::findActiveByUser($user)) ? $temp->toArray() : array();
    
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('select_users');
    } // if
    
    $name = array_var($params, 'name', 'select_projects', true);
    $value = array_var($params, 'value', null, true);
    if(empty($value)) {
      $value = array();
    } // if

    $label = array_var($params, 'label', null, true);

    if(isset($params['class'])) {
      $params['class'] .= ' select_projects_inline';
    } else {
      $params['class'] = 'select_projects_inline';
    } // if

    $result = HTML::openTag('div', $params);

    if($label) {
      $result .= HTML::label($label, null, array_var($params, 'required'), array('class' => 'main_label'));
    } // if

    if($projects) {
      $result .= '<ul id="add_projects_list">';
      foreach ($projects as $project) {
        if (!in_array($project, $user_projects)) {
          $result .= '<li>' . HTML::checkbox($name . '[]', false, array(
            'label' => $project->getName(),
            'value' => $project->getId(),
            'class' => 'project_checkbox',
          )) . '</li>';
        } //if
      } //foreach
      $result .= '</ul>';
    } // if

    return $result . '</div>';
  } // smarty_function_add_user_projects_select