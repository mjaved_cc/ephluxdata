<?php

  /**
   * List invoices helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render invoice list 
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_invoice_list($params, &$smarty) {
    $invoices = array_required_var($params, 'invoices');
    $user = array_var($params, 'user'); 
    
    $smarty->assign(array(
      '_invoices' => $invoices,
      '_user' => $user
    ));
    
    $interface = array_var($params, 'interface', AngieApplication::getPreferedInterface(), true);
    
    return $smarty->fetch(get_view_path('_invoice_list', 'project', SYSTEM_MODULE, $interface));
    
    
  } // smarty_function_project_progress