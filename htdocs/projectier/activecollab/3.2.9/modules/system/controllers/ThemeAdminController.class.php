<?php

  // Build on top of framework level implementation
  AngieApplication::useController('fw_theme_admin', ENVIRONMENT_FRAMEWORK);

  /**
   * Application level theme admin controller implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class ThemeAdminController extends FwThemeAdminController {
  
  }