<?php

  // Exted framework administration controller
  AngieApplication::useController('fw_admin', ENVIRONMENT_FRAMEWORK);

  /**
   * Base administration controller
   *
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class AdminController extends FwAdminController {

    /**
     * Execute before any action
     */
    function __before() {
      parent::__before();

      $this->wireframe->javascriptAssign(array(
        'check_application_version_url' => extend_url(CHECK_APPLICATION_VERSION_URL, array('license_key' => LICENSE_KEY)),
      ));
    } // __before

    /**
     * New version page action
     */
    function new_version() {
      $config_options = ConfigOptions::getValue(array(
        'latest_version',
        'latest_available_version',
        'renew_support_url',
        'update_instructions_url'
      ));

      $this->response->assign(array(
        'current_version' => AngieApplication::getVersion(),
        'latest_version' => array_var($config_options, 'latest_version'),
        'latest_available_version' => array_var($config_options, 'latest_available_version'),
        'update_instructions_url' => $config_options['update_instructions_url'] ? $config_options['update_instructions_url'] : UPDATE_INSTRUCTIONS_URL,
        'renew_support_url' => $config_options['renew_support_url'] ? $config_options['renew_support_url'] : RENEW_SUPPORT_URL,
      ));
    } // new_version

    /**
     * Save system information
     */
    function save_license_details() {
      if ($this->request->isSubmitted()) {
        $license_details = $this->request->post('license_details');

        try {
          ConfigOptions::setValue(array(
            'license_details_updated_on'  => time(),
            'latest_version'              => array_var($license_details, 'latest_version'),
            'latest_available_version'    => array_var($license_details, 'latest_available_version'),
            'license_package'             => array_var($license_details, 'license_package'),
            'license_expires'             => array_var($license_details, 'license_expires'),
            'license_copyright_removed'   => (bool) array_var($license_details, 'license_copyright_removed'),
            'upgrade_to_corporate_url'    => array_var($license_details, 'upgrade_to_corporate_url'),
            'remove_branding_url'         => array_var($license_details, 'remove_branding_url'),
            'renew_support_url'           => array_var($license_details, 'renew_support_url'),
            'update_instructions_url'     => array_var($license_details, 'update_instructions_url')
          ));
          $this->response->ok();
        } catch (Exception $e) {
          $this->response->exception($e);
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // save_system_info
    
  }