<div id="print_container">
{object object=$active_milestone user=$logged_user}
	<div class="wireframe_content_wrapper">{object_comments object=$active_milestone user=$logged_user show_first=yes}</div>
  <div class="wireframe_content_wrapper">{milestone_list_objects object=$active_milestone user=$logged_user type=tasks}</div>
  <div class="wireframe_content_wrapper">{milestone_list_objects object=$active_milestone user=$logged_user type=discussions}</div>
  <div class="wireframe_content_wrapper">{milestone_list_objects object=$active_milestone user=$logged_user type=todo}</div>
{/object}
</div>