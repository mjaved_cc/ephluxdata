{title}Update Settings{/title}
{add_bread_crumb}Update Settings{/add_bread_crumb}

<div id="edit_user_settings">
  {form action=$active_user->getEditSettingsUrl() csfr_protect=true}
    <div class="content_stack_wrapper">
      <div class="content_stack_element odd">
        <div class="content_stack_element_info">
          <h3>{lang}Localization{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=language}
            {select_language name='user[language]' value=$user_data.language optional=true label='Language'}
          {/wrap}
        
          <div class="col">
          {wrap field=format_date}
            {select_date_format name='user[format_date]' value=$user_data.format_date optional=true label='Date Format'}
          {/wrap}
          </div>
          
          <div class="col">
          {wrap field=format_time}
            {select_time_format name='user[format_time]' value=$user_data.format_time optional=true label='Time Format'}
          {/wrap}
          </div>
        </div>
      </div>
      
      <div class="content_stack_element even">
        <div class="content_stack_element_info">
          <h3>{lang}Date and Time{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=first_weekday}
            {select_week_day name='user[time_first_week_day]' value=$user_data.time_first_week_day label='First Day of the Week'}
          {/wrap}
          
          <div class="col">
          {wrap field=timezone}
            {select_timezone name='user[time_timezone]' value=$user_data.time_timezone optional=false label='Timezone'}
          {/wrap}
          </div>
          
          <div class="col">
          {wrap field=dst}
            {yes_no_default name='user[time_dst]' value=$user_data.time_dst default=$default_dst_value label='Daylight Saving Time'}
          {/wrap}
          </div>
        </div>
      </div>
      
      <div class="content_stack_element even{if !$logged_user->isPeopleManager()}last{/if}">
        <div class="content_stack_element_info">
          <h3>{lang}Miscellaneous{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          <!--
          {wrap field=theme}
            {select_theme name="user[theme]" value=$user_data.theme optional=true label='Theme'}
          {/wrap}
          -->
          
          {wrap field=visual_editor}
            {yes_no name="user[visual_editor]" value=$user_data.visual_editor label='Visual Editor'}  
          {/wrap}
        </div>
      </div>
      
      {if $logged_user->isPeopleManager()}
      <div class="content_stack_element odd last">
        <div class="content_stack_element_info">
          <h3>{lang}Auto-Assign{/lang}</h3>
        </div>
        <div class="content_stack_element_body" id="auto_assign_user">
          {wrap field=auto_assign}
            {label first_name=$active_user->getFirstName(true)}Automatically Add :first_name to New Projects{/label}
            {yes_no name="user[auto_assign]" value=$user_data.auto_assign id=userAutoAssign}
            <p class="details">{lang}Select <b>Yes</b> to have this user automatically added to each new project when the project is created{/lang}</p>
          {/wrap}
          
          <div id="auto_assign_role_and_permissions" {if !$user_data.auto_assign}style="display: none"{/if}>
            <p>{lang}Please select a role or set custom permissions for user in this project{/lang}:</p>
            {select_user_project_permissions name=user role_id=$user_data.auto_assign_role_id permissions=$user_data.auto_assign_permissions role_id_field=auto_assign_role_id permissions_field=auto_assign_permissions}
          </div>
        </div>
      </div>
      {/if}
    </div>
  
    {wrap_buttons}
    	{submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>

<script type="text/javascript">
  $('#userAutoAssignYesInput').click(function() {
    $('#auto_assign_role_and_permissions').show();
  });
  
  $('#userAutoAssignNoInput').click(function() {
    $('#auto_assign_role_and_permissions').hide();
  });
</script>