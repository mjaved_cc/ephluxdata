{title}Identity{/title}
{add_bread_crumb}Identity Settings{/add_bread_crumb}

<div id="identity_admin">
  {form action=Router::assemble('identity_admin') method="post" enctype="multipart/form-data"}
    <div class="content_stack_wrapper">
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}General{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=identity_name}
          	{text_field name="settings[identity_name]" value=$settings_data.identity_name label='System Name'}
          	<p class="aid">{lang}Name your project collaboration system. This name will be used as prefix for title of all pages, as well as Welcome home screen widget{/lang}</p>
          {/wrap}
          
          {wrap field=identity_logo}
            {label}System Logo{/label}
            <table cellspacing="0" class="logo_table">
              <tr>
                <td class="logo_cell">
                  <img src="{$large_logo_url}" alt="{lang}Logo{/lang}">
                </td>
                <td class="logo_input">
                  <input type="file" name="logo" />
                </td>
              </tr>
            </table>

          	<p class="aid">{lang small_logo_url=$small_logo_url medium_logo_url=$medium_logo_url large_logo_url=$large_logo_url larger_logo_url=$larger_logo_url photo_logo_url=$photo_logo_url}This logo is used in email notifications, welcome messages and more. It is saved in four sizes: <a href=":small_logo_url" target="_blank">16x16px</a>, <a href=":medium_logo_url" target="_blank">40x40px</a>, <a href=":large_logo_url" target="_blank">80x80px</a>, <a href=":larger_logo_url" target="_blank">128x128</a> and <a href=":photo_logo_url" target="_blank">256x256px</a>. System uses different sizes for different purposes, depending on the need{/lang}</p>
        	{/wrap}
        </div>
      </div>
    </div>
    
    {wrap_buttons}
  	  {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>