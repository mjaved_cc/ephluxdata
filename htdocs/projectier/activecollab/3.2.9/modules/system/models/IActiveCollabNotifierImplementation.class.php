<?php

  /**
   * activeCollab notifier implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class IActiveCollabNotifierImplementation extends INotifierImplementation {
  
    /**
  	 * Notify administrators
  	 * 
  	 * @param INotifierContext $context
  	 * @param NotifierEvent $event
  	 * @param mixed $event_params
  	 * @param mixed $additional
  	 */
  	function notifyFinancialManagers(INotifierContext $context, $event, $event_params = null, $additional = null) {
  		$this->notifyUsers(Users::findFinancialManagers(), $context, $event, $event_params, $additional);
  	} // notifyFinancialManagers
    
  }