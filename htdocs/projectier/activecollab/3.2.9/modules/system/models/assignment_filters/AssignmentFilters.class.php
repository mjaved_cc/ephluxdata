<?php

  /**
   * AssignmentFilters class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class AssignmentFilters extends BaseAssignmentFilters {
  
    /**
     * Returns true if $user can create a new filter
     * 
     * @param User $user
     * @return boolean
     */
    static function canAdd(User $user) {
      return AssignmentFilters::canManage($user);
    } // canAdd

    /**
     * Returns true if $user can manage assignment filters
     *
     * @param User $user
     * @return bool
     */
    static function canManage(User $user) {
      return $user->isProjectManager() || $user->getSystemPermission('can_manage_assignment_filters');
    } // canManage
    
    // ---------------------------------------------------
    //  Finders
    // ---------------------------------------------------
    
    /**
     * Return assignment filters for a given user
     * 
     * @param User $user
     * @return DBResult
     */
    static function findByUser(User $user) {
      return AssignmentFilters::find(array(
        'conditions' => array('is_private = ? OR (created_by_id = ? AND is_private = ?)', false, $user->getId(), true), 
        'order' => 'name'
      ));
    } // findByUser
    
    /**
     * Return all public filters
     * 
     * @return DBResult
     */
    static function findPublic() {
      return AssignmentFilters::find(array(
        'conditions' => array('is_private = ?', false), 
        'order' => 'name'
      ));
    } // findPublic
    
    /**
     * Return all private filters for a given user
     * 
     * @param User $user
     * @return DBResult
     */
    static function findPrivate(User $user) {
      return AssignmentFilters::find(array(
        'conditions' => array('created_by_id = ? AND is_private = ?', $user->getId(), true), 
        'order' => 'name'
      ));
    } // findPrivate
    
    /**
     * Return ID name map of filters that $user can see
     * 
     * @param User $user
     * @return array
     */
    static function getIdNameMap(User $user) {
      $rows = DB::execute('SELECT id, name FROM ' . TABLE_PREFIX . 'assignment_filters WHERE is_private = ? OR (created_by_id = ? AND is_private = ?) ORDER BY name', false, $user->getId(), true);
      
      if($rows) {
        $result = array();
        
        foreach($rows as $row) {
          $result[(integer) $row['id']] = $row['name'];
        } // foreach
        
        return $result;
      } else {
        return null;
      } // if
    } // getIdNameMap

    // ---------------------------------------------------
    //  Utility
    // ---------------------------------------------------

    /**
     * Go through result entries and make sure that they can be reliable converted to JavaScript maps
     *
     * @param $result
     */
    static function filterResultToMap(&$result) {
      if($result) {
        foreach($result as $k => $v) {
          if($result[$k]['assignments']) {
            foreach($result[$k]['assignments'] as $j => $assignment) {
              if(array_key_exists('subtasks', $result[$k]['assignments'][$j])) {
                $result[$k]['assignments'][$j]['subtasks'] = JSON::valueToMap($result[$k]['assignments'][$j]['subtasks']); // Convert subtasks to map
              } // if
            } // foreach

            $result[$k]['assignments'] = JSON::valueToMap($result[$k]['assignments']); // Convert group assignments to map
          } // if
        } // foreach
      } // if
    } // filterResultToMap
  
  }