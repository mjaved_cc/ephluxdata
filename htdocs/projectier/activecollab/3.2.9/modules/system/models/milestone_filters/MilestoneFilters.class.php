<?php


  /**
   * MilestoneFilters class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class MilestoneFilters extends BaseMilestoneFilters {

    /**
     * Returns true if $user can create a new filter
     *
     * @param User $user
     * @return boolean
     */
    static function canAdd(User $user) {
      return MilestoneFilters::canManage($user);
    } // canAdd

    /**
     * Returns true if $user can manage milestone filters
     *
     * @param User $user
     * @return bool
     */
    static function canManage(User $user) {
      return $user->isProjectManager() || $user->getSystemPermission('can_manage_milestone_filters');
    } // canManage

    // ---------------------------------------------------
    //  Finders
    // ---------------------------------------------------

    /**
     * Return milestone filters for a given user
     *
     * @param User $user
     * @return DBResult
     */
    static function findByUser(User $user) {
      return MilestoneFilters::find(array(
        'conditions' => array('is_private = ? OR (created_by_id = ? AND is_private = ?)', false, $user->getId(), true),
        'order' => 'name'
      ));
    } // findByUser
  
  }

