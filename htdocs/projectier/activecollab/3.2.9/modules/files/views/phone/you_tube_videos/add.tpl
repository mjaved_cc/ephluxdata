{title}New YouTube Video{/title}
{add_bread_crumb}New Youtube Video{/add_bread_crumb}

<div id="add_you_tube_video">
  {form action=$add_youtube_url}
    {include file=get_view_path('_you_tube_video_form', 'you_tube_videos', $smarty.const.FILES_MODULE)}
    
    {wrap_buttons}
      {submit}Add Video{/submit}
    {/wrap_buttons}
  {/form}
</div>