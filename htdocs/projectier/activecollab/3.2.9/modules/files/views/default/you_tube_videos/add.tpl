{title}New YouTube Video{/title}
{add_bread_crumb}New Youtube Video{/add_bread_crumb}

<div id="add_youtube">
  {form action=$add_youtube_url method=post enctype="multipart/form-data" autofocus=yes ask_on_leave=yes class='big_form youtube_form'}
    {include file=get_view_path('_you_tube_video_form', 'you_tube_videos', 'files')}
    
    {wrap_buttons}
      {submit}Add YouTube Video{/submit}
    {/wrap_buttons}
  {/form}
</div>