<?php

  /**
   * BaseTestDataObject class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class BaseTestDataObject extends DataObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'test_data_objects';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'name', 'description', 'type', 'created_on', 'updated_on');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @param void
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of name field
     *
     * @param void
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of description field
     *
     * @param void
     * @return string
     */
    function getDescription() {
      return $this->getFieldValue('description');
    } // getDescription
    
    /**
     * Set value of description field
     *
     * @param string $value
     * @return string
     */
    function setDescription($value) {
      return $this->setFieldValue('description', $value);
    } // setDescription

    /**
     * Return value of type field
     *
     * @param void
     * @return string
     */
    function getType() {
      return $this->getFieldValue('type');
    } // getType
    
    /**
     * Set value of type field
     *
     * @param string $value
     * @return string
     */
    function setType($value) {
      return $this->setFieldValue('type', $value);
    } // setType

    /**
     * Return value of created_on field
     *
     * @param void
     * @return DateTimeValue
     */
    function getCreatedOn() {
      return $this->getFieldValue('created_on');
    } // getCreatedOn
    
    /**
     * Set value of created_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setCreatedOn($value) {
      return $this->setFieldValue('created_on', $value);
    } // setCreatedOn

    /**
     * Return value of updated_on field
     *
     * @param void
     * @return DateTimeValue
     */
    function getUpdatedOn() {
      return $this->getFieldValue('updated_on');
    } // getUpdatedOn
    
    /**
     * Set value of updated_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setUpdatedOn($value) {
      return $this->setFieldValue('updated_on', $value);
    } // setUpdatedOn

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'name':
          return parent::setFieldValue($real_name, strval($value));
        case 'description':
          return parent::setFieldValue($real_name, strval($value));
        case 'type':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'updated_on':
          return parent::setFieldValue($real_name, datetimeval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

?>