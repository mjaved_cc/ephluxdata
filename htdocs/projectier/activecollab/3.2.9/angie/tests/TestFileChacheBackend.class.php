<?php

  class TestFileCacheBackend extends UnitTestCase {
    
    /**
     * Cache instance
     *
     * @var Cache
     */
    var $cache;
  
    function setUp() {
      $this->recreateCacheInstance();
    } // setUp
    
    function tearDown() {
      $this->cache->backend->clear();
    } // tearDown
    
    function testSetGet() {
      $to_set = array('some', 'value', 'here');
      
      // Set value...
      $this->cache->backend->set('variable', $to_set);
      $this->cache->backend->save();
      
      // Check if file is present and we have a good value
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable')));
      
      $value = include $this->cache->backend->getFilePath('variable');
      $this->assertEqual($value, $to_set);
      
      $this->recreateCacheInstance();
      
      // Check if we have a value...
      $this->assertEqual($this->cache->backend->get('variable'), $to_set);
      
      // Remove value
      $this->cache->backend->remove('variable');
      
      $this->assertFalse(is_file($this->cache->backend->getFilePath('variable')));
      
      $this->cache->backend->clear();
    } // testSetGet
    
    function testUpdate() {
      $to_set = array('some', 'value', 'here');
      
      // Set value...
      $this->cache->backend->set('variable', $to_set);
      $this->cache->backend->save();
      
      $this->recreateCacheInstance();
      
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable')));
      
      $value = include $this->cache->backend->getFilePath('variable');
      $this->assertEqual($value, $to_set);
      
      $this->recreateCacheInstance();
      
      $new_value = false;
      
      // Update...
      $this->cache->backend->set('variable', $new_value);
      $this->assertEqual($this->cache->backend->updated_data['variable'], $new_value);
      $this->cache->backend->save();
      
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable')));
      
      $value = include $this->cache->backend->getFilePath('variable');
      $this->assertEqual($value, $new_value);
    } // testUpdate
    
    function checkClean() {
      $this->cache->backend->set('variable1', 'value1');
      $this->cache->backend->set('variable2', 'value2');
      $this->cache->backend->set('variable3', 'value3');
      
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable1')));
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable2')));
      $this->assertTrue(is_file($this->cache->backend->getFilePath('variable3')));
      
      $this->cache->backend->clear();
      
      $this->assertFalse(is_file($this->cache->backend->getFilePath('variable1')));
      $this->assertFalse(is_file($this->cache->backend->getFilePath('variable2')));
      $this->assertFalse(is_file($this->cache->backend->getFilePath('variable3')));
    } // checkClean
    
    function recreateCacheInstance() {
      $this->cache = new Cache();
      $this->cache->useBackend('FileCacheBackend');
      $this->cache->backend->setCacheDir(TESTS_PATH . '/support');
    } // recreateCacheInstance
  
  } // TestFileCacheBackend

?>