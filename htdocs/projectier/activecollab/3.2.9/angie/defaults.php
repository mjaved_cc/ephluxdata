<?php

  /**
   * Default configuration option, that application and user can override via config/config.php or application level
   * defaults.php file
   *
   * @package angie
   */

  if(!defined('ROOT_URL') && php_sapi_name() == 'cli') {
    define('ROOT_URL', 'unknown'); // In case we are executing this command via CLI, for testing and initialization
  } // if

  if(!defined('ADMIN_EMAIL')) {
    define('ADMIN_EMAIL', false);
  } // if

  if(!defined('PUBLIC_FOLDER_NAME')) {
    define('PUBLIC_FOLDER_NAME', 'public');
  } // if

  if(!defined('PUBLIC_AS_DOCUMENT_ROOT')) {
    define('PUBLIC_AS_DOCUMENT_ROOT', false);
  } // if

  if(!defined('FORCE_QUERY_STRING')) {
    define('FORCE_QUERY_STRING', true);
  } // if

  if(!defined('PATH_INFO_THROUGH_QUERY_STRING')) {
    define('PATH_INFO_THROUGH_QUERY_STRING', true); // Force query string for hosts that does not support PATH_INFO or make problems with it
  } // if

  if(!defined('FORCE_ROOT_URL')) {
    define('FORCE_ROOT_URL', true);
  } // if

  if(!defined('URL_BASE')) {
    define('URL_BASE', ROOT_URL . '/index.php');
  } // if

  if(!defined('ASSETS_URL')) {
    define('ASSETS_URL', ROOT_URL . '/assets');
  } // if

  if(!defined('ASSETS_PATH')) {
    define('ASSETS_PATH', PUBLIC_PATH . '/assets');
  } // if

  if(!defined('FORCE_INTERFACE')) {
    define('FORCE_INTERFACE', false);
  } // if

  if(!defined('FORCE_DEVICE_CLASS')) {
    define('FORCE_DEVICE_CLASS', false);
  } // if

  if(!defined('PROTECT_SCHEDULED_TASKS')) {
    define('PROTECT_SCHEDULED_TASKS', true);
  } // if

  if(!defined('PROTECT_ASSETS_FOLDER')) {
    define('PROTECT_ASSETS_FOLDER', false);
  } // if

  if(!defined('PURIFY_HTML')) {
    define('PURIFY_HTML', true);
  } // if

  if(!defined('REMOVE_EMPTY_PARAGRAPHS')) {
    define('REMOVE_EMPTY_PARAGRAPHS', true);
  } // if

  if(!defined('MAINTENANCE_MESSAGE')) {
    define('MAINTENANCE_MESSAGE', null);
  } // if

  if(!defined('CREATE_THUMBNAILS')) {
    define('CREATE_THUMBNAILS', true);
  } // if

  if(!defined('RESIZE_SMALLER_THAN')) {
    define('RESIZE_SMALLER_THAN', 524288);
  } // if

  if(!defined('IMAGE_SIZE_CONSTRAINT')) {
    define('IMAGE_SIZE_CONSTRAINT', '2240x1680');
  } // if

  if(!defined('COMPRESS_ASSET_REQUESTS')) {
    define('COMPRESS_ASSET_REQUESTS', true);
  } // if

  if(!defined('PAGE_PLACEHOLDER')) {
    define('PAGE_PLACEHOLDER', '-PAGE-');
  } // if

  if(!defined('NUMBER_FORMAT_DEC_SEPARATOR')) {
    define('NUMBER_FORMAT_DEC_SEPARATOR', '.');
  } // if

  if(!defined('NUMBER_FORMAT_THOUSANDS_SEPARATOR')) {
    define('NUMBER_FORMAT_THOUSANDS_SEPARATOR', ',');
  } // if

  if(!defined('DEFAULT_CSV_SEPARATOR')) {
    define('DEFAULT_CSV_SEPARATOR', ',');
  } // if

  if(!defined('CACHE_PATH')) {
    define('CACHE_PATH', ENVIRONMENT_PATH . '/cache');
  } // if

  if(!defined('COLLECTOR_CHECK_ETAG')) {
    define('COLLECTOR_CHECK_ETAG', true);
  } // if

  // Caching
  define('USE_CACHE', true);
  if (!defined('CACHE_BACKEND')) {
    if(extension_loaded('apc')) {
      define('CACHE_BACKEND', 'APCCacheBackend');
    } else {
      define('CACHE_BACKEND', 'FileCacheBackend');
    } // if
  } // if
  define('CACHE_LIFETIME', 7200);

  // Cookies
  define('USE_COOKIES', true);
  if(!defined('COOKIE_DOMAIN')) {
    $parts = parse_url(ROOT_URL);
    if(is_array($parts) && isset($parts['host'])) {
      define('COOKIE_DOMAIN', $parts['host']);
    } else {
      define('COOKIE_DOMAIN', '');
    } // if
  } // if

  define('COOKIE_PATH', '/');
  if(substr(ROOT_URL, 0, 5) == 'https') {
    define('COOKIE_SECURE', 1);
  } else {
    define('COOKIE_SECURE', 0);
  } // if
  define('COOKIE_PREFIX', 'ac');

  if(!defined('DEFAULT_MODULE')) {
    define('DEFAULT_MODULE', 'system');
  } // if

  if(!defined('DEFAULT_CONTROLLER')) {
    define('DEFAULT_CONTROLLER', 'backend');
  } // if

  if(!defined('DEFAULT_ACTION')) {
    define('DEFAULT_ACTION', 'index');
  } // if

  if(!defined('DEFAULT_FORMAT')) {
    define('DEFAULT_FORMAT', 'html');
  } // if

  // Formats can be overriden with constants with same name that start with
  // USER_ (USER_FORMAT_DATE will override FORMAT_DATE)
  if(DIRECTORY_SEPARATOR == '\\') {
    if(!defined('FORMAT_DATETIME')) {
      define('FORMAT_DATETIME', '%b %#d. %Y, %I:%M %p');
    } // if

    if(!defined('FORMAT_DATE')) {
      define('FORMAT_DATE', '%b %#d. %Y');
    } // if
  } else {
    if(!defined('FORMAT_DATETIME')) {
      define('FORMAT_DATETIME', '%b %e. %Y, %I:%M %p');
    } // if

    if(!defined('FORMAT_DATE')) {
      define('FORMAT_DATE', '%b %e. %Y');
    } // if
  } // if

  if(!defined('FORMAT_TIME')) {
    define('FORMAT_TIME', '%I:%M %p');
  } // if

  // Read environment name from environment path
  if(!defined('ENVIRONMENT')) {
    define('ENVIRONMENT', substr(ENVIRONMENT_PATH, strrpos(ENVIRONMENT_PATH, '/') + 1));
  } // if

  if(!defined('COMPILE_PATH')) {
    define('COMPILE_PATH', ENVIRONMENT_PATH . '/compile');
  } // if

  if(!defined('DEVELOPMENT_PATH')) {
    define('DEVELOPMENT_PATH', ROOT . '/development');
  } // if

  if(!defined('UPLOAD_PATH')) {
    define('UPLOAD_PATH', ENVIRONMENT_PATH . '/upload');
  } // if

  if(!defined('LIMIT_DISK_SPACE_USAGE')) {
    define('LIMIT_DISK_SPACE_USAGE', null);
  } // if

  if(!defined('BASIC_FILE_UPLOADS')) {
    define('BASIC_FILE_UPLOADS', false);
  } // if

  if(!defined('CUSTOM_PATH')) {
    define('CUSTOM_PATH', ENVIRONMENT_PATH . '/custom');
  } // if

  if(!defined('IMPORT_PATH')) {
    define('IMPORT_PATH', ENVIRONMENT_PATH . '/import');
  } // if

  if(!defined('THUMBNAILS_PATH')) {
    define('THUMBNAILS_PATH', ENVIRONMENT_PATH . '/thumbnails');
  } // if

  if(!defined('WORK_PATH')) {
    define('WORK_PATH', ENVIRONMENT_PATH . '/work');
  } // if

  if(!defined('THEMES_PATH')) {
    define('THEMES_PATH', ENVIRONMENT_PATH . '/' . PUBLIC_FOLDER_NAME . '/assets/themes');
  } // if