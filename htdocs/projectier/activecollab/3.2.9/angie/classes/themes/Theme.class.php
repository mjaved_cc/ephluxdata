<?php

  /**
   * Class description
   *
   * @package
   * @subpackage
   */
  final class Theme {

    /**
     * Loaded theme adapter
     *
     * @var ThemeAdapter
     */
    static private $adapter;

    /**
     * Return theme adapter
     *
     * @return ThemeAdapter
     */
    static function getAdapter() {
      return self::$adapter;
    } // getAdapter

    /**
     * Set theme adapter
     *
     * @param ThemeAdapter $adapter
     * @return ThemeAdapter
     */
    static function setAdapter(ThemeAdapter $adapter) {
      if($adapter instanceof ThemeAdapter) {
        self::$adapter = $adapter;
      } else {
        throw new InvalidInstanceError('adapter', $adapter, 'ThemeAdapter');
      } // if

      return self::$adapter;
    } // setAdapter

    /**
     * Return theme property
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    static function getProperty($name, $default = null) {
      return self::$adapter instanceof ThemeAdapter ? self::$adapter->getProperty($name, $default) : $default;
    } // getProperty

    /**
     * Set theme property
     *
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    static function setProperty($name, $value) {
      return self::$adapter->setProperty($name, $value);
    } // setProperty

  }