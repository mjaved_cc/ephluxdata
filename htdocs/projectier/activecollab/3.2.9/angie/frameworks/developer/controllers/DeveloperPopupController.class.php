<?php

  // Build on top of admin controller
  AngieApplication::useController('admin', SYSTEM_MODULE);

  /**
   * Developer popup controller
   *
   * @package angie.frameworks.developer
   * @subpackage controllers
   */
  class DeveloperPopupController extends AdminController {
    
    /**
  * Active module
  *
  * @var string
  */
    protected $active_module = DEVELOPER_FRAMEWORK;

    /**
     * Execute before any action
     */
    function __before() {
      parent::__before();

      if (!$this->request->isAsyncCall()) {
        $this->response->badRequest();
      } // if
    } // __before

    /**
     * Popup action
     */
    function index() {

    } // index

    /**
     * Empty cache
     */
    function empty_cache() {
      if($this->request->isSubmitted()) {
        Router::cleanUpCache(true);
        cache_clear();

        $this->response->ok();
      } else {
        $this->response->badRequest();
      } // if
    } // empty_cache

    /**
     * Delete compiled templates
     */
    function delete_compiled_templates() {
      if($this->request->isSubmitted()) {
        SmartyForAngie::clearCompiledTemplates();

        $this->response->ok();
      } else {
        $this->response->badRequest();
      } // if
    } // delete_compiled_templates

    /**
     * Rebuild images
     */
    function rebuild_images() {
      if($this->request->isSubmitted()) {
        $protect_assets = defined('PROTECT_ASSETS_FOLDER') && PROTECT_ASSETS_FOLDER;

        if(empty($protect_assets)) {
          AngieApplication::rebuildAssets();
        } // if

        $this->response->ok();
      } else {
        $this->response->badRequest();
      } // if
    } // rebuild_images

    /**
     * Rebuild lozalization
     */
    function rebuild_localization() {
      if($this->request->isSubmitted()) {
        AngieApplication::rebuildLocalization();

        $this->response->ok();
      } else {
        $this->response->badRequest();
      } // if
    } // rebuild_localization
    
  }