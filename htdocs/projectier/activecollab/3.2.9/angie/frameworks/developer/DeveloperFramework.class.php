<?php

  /**
   * Developer framework definition
   *
   * @package angie.frameworks.developer
   */
  class DeveloperFramework extends AngieFramework {
    
    /**
     * Short module name
     *
     * @var string
     */
    protected $name = 'developer';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0';
    
    /**
     * Define module routes
     */
    function defineRoutes() {
    	Router::map('developer_playground', 'developer/playground', array('controller' => 'developer_playground', 'module' => DEVELOPER_FRAMEWORK));
    } // defineRoutes
    
    /**
     * Define event handlers
     */
    function defineHandlers() {
      EventsManager::listen('on_status_bar', 'on_status_bar');
    } // defineHandlers
    
    // ---------------------------------------------------
    //  Name
    // ---------------------------------------------------
    
    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Developer');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
      return lang('Adds useful set of development and debugging tools');
    } // getDescription
    
    /**
     * Return module uninstallation message
     *
     * @return string
     */
    function getUninstallMessage() {
      return lang('Module will be deactivated');
    } // getUninstallMessage
    
  }