<?php

  // Build on top of administration controller
  AngieApplication::useController('admin', GLOBALIZATION_FRAMEWORK_INJECT_INTO);

  /**
   * Home screens administration controller
   * 
   * @package angie.frameworks.homescreens
   * @subpackage controllers
   */
  abstract class FwHomescreensAdminController extends AdminController {
    
    /**
     * Default home screen
     *
     * @var Homescreen
     */
    protected $default_homescreen;
    
    /**
     * Home screen controller delegate
     *
     * @var HomescreenController
     */
    protected $homescreen_delegate;
    
    /**
     * Construct home screens admin controller controller
     *
     * @param Request $parent
     * @param mixed $context
     */
    function __construct($parent, $context = null) {
      parent::__construct($parent, $context);
      
      if($this->getControllerName() == 'homescreens_admin') {
        $this->homescreen_delegate = $this->__delegate('homescreen', HOMESCREENS_FRAMEWORK_INJECT_INTO, 'homescreens_admin');
      } // if
    } // __construct
    
    /**
     * Execute before action
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->breadcrumbs->add('homescreens_admin', lang('Home Screens'), Router::assemble('homescreens_admin'));
      
      $this->default_homescreen = Homescreens::findDefault();
      $this->response->assign('default_homescreen', $this->default_homescreen);
    } // __before
    
    /**
     * Show home screens administration page
     */
    function index() {
      $this->response->assign(array(
        'roles' => Roles::find()      
      ));
    } // index
    
    /**
     * Show and manage home screen for a given role
     */
    function role_homescreen() {
      
    } // role_homescreen
    
  }