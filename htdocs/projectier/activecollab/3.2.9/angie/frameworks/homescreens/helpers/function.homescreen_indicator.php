<?php

  /**
   * homescreen_indicator helper implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage helpers
   */

  /**
   * Render homescreen_indicator indicator
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_homescreen_indicator($params, &$smarty) {
    if(isset($params['homescreen']) && $params['homescreen'] instanceof Homescreen) {
      $has = true;
      $url = $params['homescreen']->getViewUrl();
      $name = $params['homescreen']->getParent() instanceof IHomescreen ? $params['homescreen']->getParent()->getName() : lang('Default Home Screen');
    } elseif(isset($params['parent']) && $params['parent'] instanceof IHomescreen) {
      if($params['parent']->homescreen()->hasOwn()) {
        $has = true;
        $url = $params['parent']->homescreen()->getManageUrl();
      } else {
        $has = false;
        $url = $params['parent']->homescreen()->getCreateUrl();
      } // if
      
      $name = $params['parent']->getName();
    } else {
      throw new InvalidParamError('homescreen', null);
    } // if
    
    $id = isset($params['id']) && $params['id'] ? $params['id'] : HTML::uniqueId('homescreen_indicator');
    
    return '<div class="homescreen_indicator ' . ($has ? 'has_own' : 'doesnt_have_own') . '" id="' . $id . '"><a href="' . clean($url) . '" title="' . lang('Click to Configure') . '"><span>' . clean($name) . '</span></a></div><script type="text/javascript">$("#' . $id . '").homescreenIndicator();</script>';
  } // smarty_function_homescreen_indicator