<?php

  /**
   * Test reminders case
   *
   * @package angie.frameworks.reminders
   * @subpackage tests
   */
  class TestReminders extends AngieModelTestCase {
  	
  	/**
  	 * Test remind self
  	 */
  	function testRemindSelf() {
  		
  	} // testRemindSelf
  
  	/**
  	 * Test remind subscribers
  	 */
  	function testRemindSubscribers() {
  		
  	} // testRemindSubscribers
  	
  	/**
  	 * Test remind assignees
  	 */
  	function testRemindAssignees() {
  		
  	} // testRemindAssignees
  	
  }