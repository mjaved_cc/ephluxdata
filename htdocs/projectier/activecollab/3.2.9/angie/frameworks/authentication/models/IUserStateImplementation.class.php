<?php

  /**
   * User state implementation
   *
   * @package angie.frameworks.authentication
   * @subpackage models
   */
  class IUserStateImplementation extends IStateImplementation {
    
    /**
     * Returns true if $user can mark this object as archived
     *
     * @param IUser $user
     * @return boolean
     */
    function canArchive(IUser $user) {
      if($user instanceof User) {
        if($this->object->getId() == $user->getId()) {
          return false;
        } else {
          return $this->object->isAdministrator() ? $user->isAdministrator() : $user->isPeopleManager();
        } // if
      } // if
      
      return false;
    } // canArchive
    
    /**
     * Returns true if $user can mark this object as trashed
     *
     * @param IUser $user
     * @return boolean
     */
    function canTrash(IUser $user) {
      return $this->canDelete($user);
    } // canTrash
    
    /**
     * Cached can delete data
     *
     * @var array
     */
    private $can_delete = array();
    
    /**
     * Returns true if $user can mark this object as deleted
     *
     * @param IUser $user
     * @return boolean
     */
    function canDelete(IUser $user) {
      if($user instanceof User) {
        $user_id = $user->getId();
        
        if($this->object->getId() == $user_id) {
          return false;
        } // if
        
        if($user_id) {
          if(!array_key_exists($user_id, $this->can_delete)) {
            if($user->getId() == $this->object->getId()) {
              $this->can_delete[$user_id] = false;
            } else {
              if($this->object->isAdministrator() && !$user->isAdministrator()) {
                $this->can_delete[$user_id] = false; // Administrators can be deleted by administrators only
              } else {
                if($this->object->isLastAdministrator()) {
                  $this->can_delete[$user_id] = false; // Last Administrator can't be deleted
                } else {
                  $this->can_delete[$user_id] = $user->isPeopleManager();
                } // if
              } // if
            } // if
          } // if
          
          return $this->can_delete[$user_id];
        } else {
          return $user->isPeopleManager() || $this->object->getCompany()->isManager($user);
        } // if
      } else {
        return true;
      } // if
    } // canDelete
    
  }