<?php

  /**
   * Authentication framwork initialization class
   *
   * @package angie.frameworks.authentication
   */

  const AUTHENTICATION_FRAMEWORK = 'authentication';
  const AUTHENTICATION_FRAMEWORK_PATH = __DIR__;

  // ---------------------------------------------------
  //  Configuration
  // ---------------------------------------------------

  if(!defined('AUTH_PROVIDER')) {
    define('AUTH_PROVIDER', 'BasicAuthenticationProvider'); // Authentication provider
  } // if

  if(!defined('USER_SESSION_LIFETIME')) {
    define('USER_SESSION_LIFETIME', 1800); // 30 minutes
  } // if

  if(!defined('AUTHENTICATION_FRAMEWORK_INJECT_INTO')) {
    define('AUTHENTICATION_FRAMEWORK_INJECT_INTO', 'system');
  } // if

  if(!defined('AUTHENTICATION_FRAMEWORK_ADMIN_ROUTE_BASE')) {
    define('AUTHENTICATION_FRAMEWORK_ADMIN_ROUTE_BASE', 'admin');
  } // if
  
  require_once AUTHENTICATION_FRAMEWORK_PATH . '/models/Authentication.class.php';
  require_once AUTHENTICATION_FRAMEWORK_PATH . '/models/providers/AuthenticationProvider.class.php';
  
  AngieApplication::setForAutoload(array(
    'IUser' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUser.class.php',

    'PasswordPolicy' => AUTHENTICATION_FRAMEWORK_PATH . '/models/PasswordPolicy.class.php',
    
    'IUsersContext' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUsersContext.class.php', 
    'IUsersContextImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUsersContextImplementation.class.php', 
    
    'IUserAvatarImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUserAvatarImplementation.class.php',
    'IUserInspectorImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUserInspectorImplementation.class.php',
    'IUserStateImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IUserStateImplementation.class.php',

    'FwAnonymousUser' => AUTHENTICATION_FRAMEWORK_PATH . '/models/FwAnonymousUser.class.php', 
    'IAnonymousUserAvatarImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/IAnonymousUserAvatarImplementation.class.php', 
    
    'FwUser' => AUTHENTICATION_FRAMEWORK_PATH . '/models/users/FwUser.class.php', 
    'FwUsers' => AUTHENTICATION_FRAMEWORK_PATH . '/models/users/FwUsers.class.php', 
    
    'FwRole' => AUTHENTICATION_FRAMEWORK_PATH . '/models/roles/FwRole.class.php', 
    'FwRoles' => AUTHENTICATION_FRAMEWORK_PATH . '/models/roles/FwRoles.class.php',
   
    'WhosOnlineHomescreenWidget' => AUTHENTICATION_FRAMEWORK_PATH . '/models/homescreen_widgets/WhosOnlineHomescreenWidget.class.php',

    // Errors
  	'AuthenticationError' => AUTHENTICATION_FRAMEWORK_PATH . '/errors/AuthenticationError.class.php',

    // Search
    'FwUsersSearchIndex' => AUTHENTICATION_FRAMEWORK_PATH . '/models/search/FwUsersSearchIndex.class.php', 
    'FwIUserSearchItemImplementation' => AUTHENTICATION_FRAMEWORK_PATH . '/models/search/FwIUserSearchItemImplementation.class.php', 
  
    // API client subscriptions
    'FwApiClientSubscription' => AUTHENTICATION_FRAMEWORK_PATH . '/models/api_client_subscriptions/FwApiClientSubscription.class.php',
    'FwApiClientSubscriptions' => AUTHENTICATION_FRAMEWORK_PATH . '/models/api_client_subscriptions/FwApiClientSubscriptions.class.php', 
    'ApiClientSubscriptionError' => AUTHENTICATION_FRAMEWORK_PATH . '/models/api_client_subscriptions/ApiClientSubscriptionError.class.php',

    // Callbacks
    'LoginAsFormCallback' => AUTHENTICATION_FRAMEWORK_PATH . '/models/javascript_callbacks/LoginAsFormCallback.class.php', 
    'ProfileMenuItemCallback' => AUTHENTICATION_FRAMEWORK_PATH . '/models/javascript_callbacks/ProfileMenuItemCallback.class.php',
  ));