<?php

  /**
   * Visibility helper
   *
   * @package angie.frameworks.environment
   * @subpackage models
   */
  class IVisibilityImplementation {
    
    /**
     * Parent object instance
     *
     * @var IVisibility
     */
    private $object;
    
    /**
     * Construct visibility implementation helper
     *
     * @param IVisibility $object
     */
    function __construct(IVisibility $object) {
      $this->object = $object;
    } // __construct
    
    /**
     * Return verbose information on who can see this object
     * 
     * @return string
     */
    function getStatement($full = false) {
      switch($this->object->getVisibility()) {
        case VISIBILITY_PRIVATE:
          if($full) {
            $role_names = Roles::whoCanSeePrivateNames();
            
            if($role_names) {
              return lang('This :type is private. It is visible only to user with following system roles: :roles', array(
                'type' => $this->object->getVerboseType(), 
                'roles' => implode(', ', $role_names), 
              ));
            } else {
              return lang('Private');
            } // if
          } else {
            return lang('Private');
          } // if
          
          break;
        case VISIBILITY_NORMAL:
        default:
          return lang('Visible to everyone');
      } // switch
    } // getStatement
    
    /**
     * Describe state of the parent object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @param array $result
     */
    function describe(IUser $user, $detailed, $for_interface, &$result) {
      $result['visibility'] = $this->object->getVisibility();
      $result['permissions']['can_change_visibility'] = $this->canChange($user);
    } // describe

    /**
     * Describe state of the parent object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param array $result
     */
    function describeForApi(IUser $user, $detailed, &$result) {
      $result['visibility'] = $this->object->getVisibility();

      if($detailed) {
        $result['permissions']['can_change_visibility'] = $this->canChange($user);
      } // if
    } // describeForApi
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if $user can change visibility of parent object
     *
     * @param IUser $user
     * @return boolean
     */
    function canChange(IUser $user) {
      return $this->object->canEdit($user);
    } // canChange
    
  }