<?php

  /**
   * Framework level backend controller
   * 
   * @package angie.frameworks.environment
   * @subpackage controllers
   */
  class FwBackendController extends ApplicationController {

    /**
     * Activity logs delegate
     *
     * @var ActivityLogsController
     */
    protected $activity_logs_delegate;

    /**
     * Construct backend controller
     *
     * @param Request $parent
     * @param mixed $context
     */
    function __construct(Request $parent, $context = null) {
      parent::__construct($parent, $context);

      if($this->getControllerName() == 'backend') {
        $this->activity_logs_delegate = $this->__delegate('activity_logs', ACTIVITY_LOGS_FRAMEWORK_INJECT_INTO, 'backend');
      } // if
    } // __construct
    
    /**
     * Get wireframe updates
     */
    function wireframe_updates() {
      if($this->request->isSubmitted()) {
        $wireframe_data = $this->request->post('data');
        if(!is_array($wireframe_data)) {
          $wireframe_data = array();
        } // if
        
        $response_data = array(
          'status_bar_badges' => array(),
        );
        
        EventsManager::trigger('on_wireframe_updates', array(&$wireframe_data, &$response_data, &$this->logged_user));
        
        $this->response->respondWithData($response_data, array(
          'format' => FORMAT_JSON, 
        ));
      } else {
        $this->response->badRequest();
      } // if
    } // wireframe_updates
    
    /**
     * Return response interface
     * 
     * @return Response
     */
    protected function getResponseInstance() {
      if($this->request->isApiCall()) {
        return new ApiResponse($this, $this->request);
      } else {
        return new BackendWebInterfaceResponse($this, $this->request);
      } // if
    } // getResponseInstance
    
    /**
     * Return wireframe instance for this controller
     *
     * @return BackendWireframe
     */
    protected function getWireframeInstance() {
      if($this->request->isPhone()) {
        return new PhoneBackendWireframe($this->request);
      } elseif($this->request->isTablet()) {
        return new TabletBackendWireframe($this->request);
      } else {
        return new WebBrowserBackendWireframe($this->request);
      } // if
    } // getWireframe
  
  }