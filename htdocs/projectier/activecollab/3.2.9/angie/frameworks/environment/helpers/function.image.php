<?php

  /**
   * Image tag helper implementation
   * 
   * @package angie.frameworks.environment
   * @subpackage helpers
   */

  /**
   * Generate image tag based on properties (same as for image_url)
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_image($params, &$smarty) {
    $name = array_required_var($params, 'name', true);
    
    $params['src'] = AngieApplication::getImageUrl($name, array_var($params, 'module', DEFAULT_MODULE, true), array_var($params, 'interface'));
    
    if(!isset($params['alt'])) {
      $params['alt'] = '';
    } // if
    
    return HTML::openTag('img', $params, true);
  } // smarty_function_image