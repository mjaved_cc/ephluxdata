<?php

  /**
   * select_visibility helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render select visibility control
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_visibility($params, &$smarty) {
    $name = array_required_var($params, 'name');
    
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('select_visibility');
    } // if
    
    $optional = array_var($params, 'optional', false, true);
    $value = array_var($params, 'value', ($optional ? null : VISIBILITY_NORMAL));
    
    if(isset($params['class'])) {
      $params['class'] .= ' select_visibility';
    } else {
      $params['class'] = 'select_visibility';
    } // if
    
    $possibilities = array(
      VISIBILITY_NORMAL => lang('Normal'), 
      VISIBILITY_PRIVATE => lang('Private'), 
    );
    
    if($optional) {
      if(ConfigOptions::getValue('default_project_object_visibility') == VISIBILITY_NORMAL) {
        $label = lang('-- System Default (Normal) --');
      } else {
        $label = lang('-- System Default (Private) --');
      } // if
      
      $return = HTML::optionalSelectFromPossibilities($name, $possibilities, $value, $params, $label, '');
    } else {
      $return = HTML::selectFromPossibilities($name, $possibilities, $value, $params);
    } // if
    
    return $return . '<script type="text/javascript">$("#' . $params['id'] . '").selectVisibility()</script>';
  } // smarty_function_select_visibility