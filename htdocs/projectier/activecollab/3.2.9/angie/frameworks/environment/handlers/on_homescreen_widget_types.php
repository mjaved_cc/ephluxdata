<?php

  /**
   * on_homescreen_widget_types event handler
   * 
   * @package angie.frameworks.environment
   * @subpackage handlers
   */

  /**
   * Handle on_homescreen_widget_types event
   * 
   * @param array $types
   * @param IUser $user
   */
  function environment_handle_on_homescreen_widget_types(&$types, IUser &$user) {
    $types[] = new SystemNotificationsHomescreenWidget();
  } // environment_handle_on_homescreen_widget_types