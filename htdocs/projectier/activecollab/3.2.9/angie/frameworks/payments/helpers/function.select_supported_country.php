<?php
  /**
   * select_country helper
   *
   * @package angie.framework.payments
   * @subpackage helpers
   */
  
  /**
   * Render select country control
   * 
   * Params:
   * 
   * - countries - available countries - array
   * 
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_supported_country($params, &$smarty) {
    $countries = array_var($params, 'countries', null, true);
    if(!array($countries)) {
      throw new InvalidParamError('countries', $countries, '$countries value is expected to be an array ov available countries', true);
    } // if
    $options = array();
    if(is_foreachable($countries)) {
      $completed_options = array();
      foreach($countries as $country) {
        if(is_foreachable($country)) {
          $options[] = option_tag(lang(ucwords(strtolower_utf($country['name']))), $country['value'], $country['attr']);
        }//if
      } // foreach
   } // if
    
    return select_box($options, $params);
  } // smarty_function_select_milestone

?>