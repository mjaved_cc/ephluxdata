<?php

  /**
   * datetime modifier implementation
   * 
   * @package angie.library.smarty
   */

  /**
   * Return formated datetime
   *
   * @param string $content
   * @param integer $offset
   * @return string
   */
  function smarty_modifier_datetime($content, $offset = null) {
    if($content instanceof DateValue) {
      return $content->formatForUser(Authentication::getLoggedUser(), $offset);
    } else {
      $date = new DateTimeValue($content);
      return $date->formatForUser(Authentication::getLoggedUser(), $offset);
    } // if
  } // smarty_modifier_datetime