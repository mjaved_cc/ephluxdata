<?php

  /**
   * Base notifier event implementation
   * 
   * @package angie.frameworks.email
   * @subpackage models
   */
  class NotifierEvent {
  	
  	/**
  	 * Event name
  	 * 
  	 * @var string
  	 */
  	protected $name;
  	
  	/**
  	 * Event module
  	 * 
  	 * @var string
  	 */
  	protected $module;
  	
  	/**
  	 * Construct notifier event instance
  	 * 
  	 * @param string $name
  	 * @param string $module
  	 */
  	function __construct($name, $module = DEFAULT_MODULE) {
  		if(strpos($name, '/') === false) {
  			$this->name = $name;
  		  $this->module = $module;
  		} else {
  			list($this->module, $this->name) = explode('/', $name);
  		} // if
  	} // __construct
  	
  	/**
  	 * Return path of the email template that's used to generate notificaiton 
  	 * body for this event
  	 * 
  	 * @return string
  	 */
  	function getTemplatePath() {
  		return AngieApplication::getEmailTemplatePath($this->name, $this->module);
  	} // getTemplatePath
  	
  }