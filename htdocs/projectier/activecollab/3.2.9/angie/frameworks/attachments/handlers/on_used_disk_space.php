<?php

  /**
   * on_used_disk_space event handler implementation
   *
   * @package angie.frameworks.attachments
   * @subpackage handlers
   */

  /**
   * Handle on_used_disk_space event
   *
   * @param integer $used_disk_space
   */
  function attachments_handle_on_used_disk_space(&$used_disk_space) {
    $used_disk_space += DB::executeFirstCell('SELECT SUM(size) FROM ' . TABLE_PREFIX . 'attachments WHERE state > ?', STATE_DELETED);
  } // attachments_handle_on_used_disk_space