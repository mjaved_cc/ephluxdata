<?php

  /**
   * Angie specific template loader for Smarty
   * 
   * @package angie.vendor.smarty
   */
  class PharSmartyResource extends Smarty_Resource_Custom {
  
  /**
     * Fetch a template and its modification time from database
     *
     * @param string $name template name
     * @param string $source template source
     * @param integer $mtime template modification timestamp (epoch)
     * @return void
     */
    protected function fetch($name, &$source, &$mtime) {
      $source = file_get_contents('phar:' . $name);
      $mtime = filemtime('phar:' . $name);
    } // fetch
    
    /**
     * Fetch a template's modification time from database
     *
     * @note implementing this method is optional. Only implement it if modification times can be accessed faster than loading the comple template source.
     * @param string $name template name
     * @return integer timestamp (epoch) the template was modified
     */
    protected function fetchTimestamp($name) {
      return filemtime('phar:' . $name);
    } // fetchTimestamp
    
  }