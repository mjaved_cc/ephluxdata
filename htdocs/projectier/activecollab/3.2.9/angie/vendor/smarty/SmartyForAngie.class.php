<?php

  /**
   * Smarty implementation for Angie framework
   * 
   * @package angie.vendors.smarty
   */
  final class SmartyForAngie {
    
    /**
     * Global Smarty instance
     *
     * @var Smarty
     */
    static private $instance;
  
    /**
     * Return main Smarty instance
     * 
     * @return Smarty
     */
    static function &getInstance() {
      if(empty(self::$instance)) {
        self::$instance = new Smarty();
      } // if
      
      return self::$instance;
    } // getInstance

    /**
     * Remove all compiled templates
     */
    static function clearCompiledTemplates() {
      if(is_dir(COMPILE_PATH)) {
        foreach(glob(COMPILE_PATH . '*.tpl.php') as $v){
          @unlink($v);
        } // foreach
      } // if
    } // clearCompiledTemplates
    
  }