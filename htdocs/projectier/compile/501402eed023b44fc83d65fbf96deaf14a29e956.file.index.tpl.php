<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 10:20:59
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\email\views\default\fw_email_admin\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:28032511b690b6985f5-46614698%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '501402eed023b44fc83d65fbf96deaf14a29e956' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\email\\views\\default\\fw_email_admin\\index.tpl',
      1 => 1360573561,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '28032511b690b6985f5-46614698',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'queue_unsent' => 0,
    'queue_total' => 0,
    'mailbox_active' => 0,
    'mailbox_total' => 0,
    'conflict_total' => 0,
    'filter_active' => 0,
    'filter_total' => 0,
    'mailing' => 0,
    'smtp_host' => 0,
    'smtp_port' => 0,
    'mailing_method' => 0,
    'from_name' => 0,
    'from_email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b690bdc85f',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b690bdc85f')) {function content_511b690bdc85f($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_assemble')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.assemble.php';
if (!is_callable('smarty_block_link')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.link.php';
if (!is_callable('smarty_modifier_json')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Email<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Control Panel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>



<div id="email_admin" class="wireframe_content_wrapper settings_panel">
  <div class="settings_panel_header">
    <table class="settings_panel_header_cell_wrapper two_cells">
      <tr>
        <td class="settings_panel_header_cell">
			      <h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Outgoing Mail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2>
			      <div class="properties">
			        <div class="property" id="mailing_settings_mailing">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Engine<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data"></div>
			        </div>
			        
			        <div class="property" id="mailing_settings_mailing_method">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Method<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data"></div>
			        </div>
			        
			        <div class="property" id="mailing_settings_notifications_from">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
From<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data"></div>
			        </div>
			        
			        <div class="property" id="mailing_settings_queue">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Queue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data <?php if ($_smarty_tpl->tpl_vars['queue_unsent']->value){?>has_unsent<?php }?>">
			            <?php if ($_smarty_tpl->tpl_vars['queue_total']->value==1){?>
			              <a href="<?php echo smarty_function_assemble(array('route'=>'outgoing_messages_admin'),$_smarty_tpl);?>
"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
One Message in Queue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a> 
			            <?php }else{ ?>
			              <a href="<?php echo smarty_function_assemble(array('route'=>'outgoing_messages_admin'),$_smarty_tpl);?>
"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('total'=>$_smarty_tpl->tpl_vars['queue_total']->value)); $_block_repeat=true; echo smarty_block_lang(array('total'=>$_smarty_tpl->tpl_vars['queue_total']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:total Messages in Queue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('total'=>$_smarty_tpl->tpl_vars['queue_total']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a> 
			            <?php }?>
			          <?php if ($_smarty_tpl->tpl_vars['queue_unsent']->value){?>
			            &mdash; <span class="error"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('unsent'=>$_smarty_tpl->tpl_vars['queue_unsent']->value)); $_block_repeat=true; echo smarty_block_lang(array('unsent'=>$_smarty_tpl->tpl_vars['queue_unsent']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Failures: :unsent<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('unsent'=>$_smarty_tpl->tpl_vars['queue_unsent']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
			          <?php }?>
			          </div>
			        </div>
			      </div>
	          
	          <ul class="settings_panel_header_cell_actions">
	            <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('outgoing_email_admin_settings'),'mode'=>'flyout_form','success_event'=>"mailing_settings_updated",'title'=>"Email Settings",'class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('outgoing_email_admin_settings'),'mode'=>'flyout_form','success_event'=>"mailing_settings_updated",'title'=>"Email Settings",'class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('outgoing_email_admin_settings'),'mode'=>'flyout_form','success_event'=>"mailing_settings_updated",'title'=>"Email Settings",'class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
	          </ul>
        </td>
        
        <td class="settings_panel_header_cell">
			      <h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Incoming Mail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2>
			      <div class="properties">
			        <div class="property">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mailboxes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data"><?php echo clean($_smarty_tpl->tpl_vars['mailbox_active']->value,$_smarty_tpl);?>
 active of <?php echo clean($_smarty_tpl->tpl_vars['mailbox_total']->value,$_smarty_tpl);?>
 defined</div>
			        </div>
			        
			        <div class="property" id="incoming_mailing_settings_conflicts">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Conflicts<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data">
			          	
			          	<?php if ($_smarty_tpl->tpl_vars['conflict_total']->value>0){?>
			          		<a href="<?php echo smarty_function_assemble(array('route'=>'incoming_email_admin_conflict'),$_smarty_tpl);?>
">
			            		<?php if ($_smarty_tpl->tpl_vars['conflict_total']->value==1){?>
			            			<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
You have 1 conflict.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			            		<?php }else{ ?>
			            			<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('num'=>$_smarty_tpl->tpl_vars['conflict_total']->value)); $_block_repeat=true; echo smarty_block_lang(array('num'=>$_smarty_tpl->tpl_vars['conflict_total']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
You have :num conflicts.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('num'=>$_smarty_tpl->tpl_vars['conflict_total']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			            		<?php }?>
			            	</a>
			          	<?php }else{ ?>
			          		<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No Conflicts<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			          	<?php }?>
			          </div>
			        </div>
			        
			        <div class="property">
			          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filters<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			          <div class="data"><?php echo clean($_smarty_tpl->tpl_vars['filter_active']->value,$_smarty_tpl);?>
 active of <?php echo clean($_smarty_tpl->tpl_vars['filter_total']->value,$_smarty_tpl);?>
 defined</div>
			        </div>
			      </div>
            
	          <ul class="settings_panel_header_cell_actions">
	          	<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('incoming_email_admin_change_settings'),'mode'=>'flyout_form','success_event'=>"incoming_mail_settings_updated",'title'=>"Incoming Email Settings",'class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_change_settings'),'mode'=>'flyout_form','success_event'=>"incoming_mail_settings_updated",'title'=>"Incoming Email Settings",'class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_change_settings'),'mode'=>'flyout_form','success_event'=>"incoming_mail_settings_updated",'title'=>"Incoming Email Settings",'class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
	            <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('incoming_email_admin_mailboxes'),'class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_mailboxes'),'class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Manage Mailboxes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_mailboxes'),'class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
	            <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('incoming_email_admin_filters'),'class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_filters'),'class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Manage Filters<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('incoming_email_admin_filters'),'class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
	          </ul>

        </td>
      
      </tr>
    </table>
  
  </div>
  
  <div class="settings_panel_body">
    <?php echo $_smarty_tpl->getSubTemplate (get_view_path('_inline_tabs',null,'system'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  </div>
</div>

<script type="text/javascript">
  $('#email_admin').each(function() {
    var wrapper = $(this);

    var min_height = 0;
    wrapper.find('td.settings_panel_header_cell div.properties').each(function () { min_height = Math.max(min_height, $(this).height()); });
    wrapper.find('td.settings_panel_header_cell div.properties').css('min-height' , min_height + 'px');

    var inline_tabs = wrapper.find('div.inline_tabs:first');

    // when user clicks on to the link in properties, content will be loaded in inline tabs
    wrapper.find('#mailing_settings_queue a').click(function () {
      inline_tabs.find('#' + inline_tabs.attr('id') + '_outgoing_queue').click();
      return false;
    });

    // when user clicks on to the link in properties, content will be loaded in inline tabs
    wrapper.find('#incoming_mailing_settings_conflicts a').click(function () {
      inline_tabs.find('#' + inline_tabs.attr('id') + '_incoming_mail_conflicts').click();
      return false;    
    });  


    //  Incoming mail settings

    // On update
  	App.Wireframe.Events.bind('incoming_mail_settings_updated.content', function(e, response) {
  	 
  	});
		







    
    /**
   	 * Update settings display
   	 * 
   	 *  @param String mailing
   	 *  @param String smtp_host
   	 *  @param String smtp_port
   	 *  @param String method
   	 *  @param String from_name
   	 *  @param String from_email
     */
    var update_settings_display = function(mailing, smtp_host, smtp_port, method, from_name, from_email) {
      switch(mailing) {
      	case 'disabled':
        	wrapper.find('#mailing_settings_mailing div.data').text(App.lang('Disabled'));
        	break;
      	case 'silent':
      	  wrapper.find('#mailing_settings_mailing div.data').text(App.lang('Silent'));
        	break;
      	case 'native':
      	  wrapper.find('#mailing_settings_mailing div.data').text(App.lang('Native'));
        	break;
      	case 'smtp':
      	  wrapper.find('#mailing_settings_mailing div.data').text(App.lang('SMTP (:host::port)', {
        	  'host' : smtp_host, 
        	  'port' : smtp_port
        	}));
        	break;
        default:
          wrapper.find('#mailing_settings_mailing div.data').text(App.lang('Invalid Mailer'));
      } // switch

      if(method == 'in_background') {
        wrapper.find('#mailing_settings_mailing_method div.data').text(App.lang('Send Messages in Background'));
      } else {
        wrapper.find('#mailing_settings_mailing_method div.data').text(App.lang('Send Instantly'));
      } // if

      if(typeof(from_name) == 'string' && from_name && typeof('from_email') == 'string' && from_email && from_name != from_email) {
        wrapper.find('#mailing_settings_notifications_from div.data').html(from_name.clean() + ' &lt;' + from_email.clean() + '&gt;');
      } else if(typeof('from_email') == 'string' && from_email) {
        wrapper.find('#mailing_settings_notifications_from div.data').html(from_email.clean());
      } else {
        wrapper.find('#mailing_settings_notifications_from div.data').html('<span class="details">' + App.lang('-- Not Set --') + '</span>');
      } // if
    }; // update_settings_display

    // Initial values
    update_settings_display(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['mailing']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['smtp_host']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['smtp_port']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['mailing_method']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['from_name']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['from_email']->value);?>
);

    
  	// On update
  	App.Wireframe.Events.bind('mailing_settings_updated.content', function(e, response) {
  	  if(typeof(response) == 'object') {
  			var mailing = response['mailing'];
  			var method = response['mailing_method'];
  			
  			var from_email = typeof(response['notifications_from_email']) == 'string' && response['notifications_from_email'] ? response['notifications_from_email'] : '';
  			var from_name = typeof(response['notifications_from_name']) == 'string' && response['notifications_from_name'] ? response['notifications_from_name'] : '';

  			if(mailing == 'smtp') {
  				var smtp_host = response['mailing_smtp_host'];
  				var smtp_port = response['mailing_smtp_port'];
  			} // if
  		} else {
  		  var mailing = null, method = null;
  			var from_name = '', from_email = '';
  		} // if

  		if(typeof(smtp_host) == 'undefined') {
  			var smtp_host = '';
  		} // if

  		if(typeof(smtp_port) == 'undefined') {
  			var smtp_port = '';
  		} // if
  		
  		update_settings_display(mailing, smtp_host, smtp_port, method, from_name, from_email);
  	});

		//update incoming mail conflict number
  	App.Wireframe.Events.bind('incoming_mail_conflict_deleted.content incoming_mail_conflict_resolved.content', function(e, response) {
  		refresh_conflict_number(response.conflicts);
    });

  	App.Wireframe.Events.bind('refresh_conflict_number.content', function(e, conflict_number) {
  		refresh_conflict_number(conflict_number);
    });

  	
  	var refresh_conflict_number = function(conflict_number) {
			
			var incoming_mailing_settings_conflicts = wrapper.find('#incoming_mailing_settings_conflicts .data').empty();

			if(conflict_number == 0) {
	  			incoming_mailing_settings_conflicts.append(App.lang('No Conflicts'));
	  	} else {
	  	  	var link = $('<a href="<?php echo smarty_function_assemble(array('route'=>'incoming_email_admin_conflict'),$_smarty_tpl);?>
"></a>');
					if(conflict_number == 1) {
	  				link.append(App.lang('You have 1 conflict.'));
	  			} else {
		  			var text_link = App.lang('You have :num conflicts.', {
			  			'num' : conflict_number
			  			});
						link.append(text_link);
					}//if

					 // when user clicks on to the link in properties, content will be loaded in inline tabs
					link.click(function () {
						inline_tabs.find('#' + inline_tabs.attr('id') + '_incoming_mail_conflicts').click();
			      return false;    
			    });  
					incoming_mailing_settings_conflicts.append(link);
		  }//if
		};//refresh_conflict_number
  	
  	
  });
</script>
<?php }} ?>