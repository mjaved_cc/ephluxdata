<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 16:03:12
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\discussions\views\default\discussions\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8207511a67c062afe7-50151995%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ba0cf610df1c5ad580e386de62f6d7c044286239' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\discussions\\views\\default\\discussions\\index.tpl',
      1 => 1360573784,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8207511a67c062afe7-50151995',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'add_discussion_url' => 0,
    'active_project' => 0,
    'manage_categories_url' => 0,
    'can_manage_discussions' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a67c0889a8',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a67c0889a8')) {function content_511a67c0889a8($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_image_url')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.image_url.php';
if (!is_callable('smarty_function_assemble')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.assemble.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Discussions<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
List<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="discussions">
  <div class="empty_content">
      <div class="objects_list_title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Discussions<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
      <div class="objects_list_icon"><img src="<?php echo smarty_function_image_url(array('name'=>'icons/48x48/discussions.png','module'=>'discussions'),$_smarty_tpl);?>
" alt=""/></div>
      <div class="objects_list_details_actions">
        <ul>
          <?php if ($_smarty_tpl->tpl_vars['add_discussion_url']->value){?><li><a href="<?php echo smarty_function_assemble(array('route'=>'project_discussions_add','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
" id="new_discussion"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
New Discussion<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['manage_categories_url']->value){?><li><a href="<?php echo clean($_smarty_tpl->tpl_vars['manage_categories_url']->value,$_smarty_tpl);?>
" class="manage_objects_list_categories" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Manage Discussion Categories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Manage Categories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li><?php }?>
        </ul>
      </div>

      <?php if ($_smarty_tpl->tpl_vars['can_manage_discussions']->value){?>
        <div class="object_list_details_additional_actions">
          <a href="<?php echo smarty_function_assemble(array('route'=>'project_discussions_archive','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
" id="view_archive"><span><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/archive.png",'module'=>"environment"),$_smarty_tpl);?>
"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Browse Archive<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></a>
        </div>
      <?php }?>

      <div class="object_lists_details_tips">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tips<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</h3>
        <ul>
          <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
To select a discussion and load its details, please click on it in the list on the left<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
          <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
It is possible to select multiple discussions at the same time. Just hold Ctrl key on your keyboard and click on all the discussions that you want to select<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
        </ul>
      </div>
  </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate (get_view_path('_initialize_objects_list','discussions',@DISCUSSIONS_MODULE), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>