<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 16:03:12
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\discussions\views\default\discussions\_initialize_objects_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26780511a67c090a2e5-02336024%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '009faa850258fd4b4359bef83adc07144e9c4742' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\discussions\\views\\default\\discussions\\_initialize_objects_list.tpl',
      1 => 1360573784,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26780511a67c090a2e5-02336024',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'categories' => 0,
    'milestones' => 0,
    'read_statuses' => 0,
    'discussions' => 0,
    'mass_manager' => 0,
    'in_archive' => 0,
    'active_discussion' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a67c0bdc42',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a67c0bdc42')) {function content_511a67c0bdc42($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_json')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\modifier.json.php';
if (!is_callable('smarty_modifier_map')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\modifier.map.php';
if (!is_callable('smarty_function_assemble')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.assemble.php';
?><script type="text/javascript">
  $('#new_discussion').flyoutForm({
    'success_event' : 'discussion_created',
    'title' 				: App.lang('New Discussion')
  });

  $('#discussions').each(function() {
    var wrapper = $(this);

    var project_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
;
    var categories_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['categories']->value);?>
;
    var milestones_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['milestones']->value);?>
;
    var read_status_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['read_statuses']->value);?>
;
    var current_discussion_id;

    var init_options = {
      'id' : 'project_' + <?php echo clean($_smarty_tpl->tpl_vars['active_project']->value->getId(),$_smarty_tpl);?>
 + '_discussions',
      'refresh_url' : '<?php echo smarty_function_assemble(array('route'=>'project_discussions','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug(),'async'=>true,'objects_list_refresh'=>true),$_smarty_tpl);?>
',
      'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['discussions']->value);?>
,
      'required_fields' : ['id', 'name', 'category_id', 'milestone_id', 'icon', 'is_read', 'is_pinned', 'permalink', 'is_archived'],
      'requirements' : {},
      'objects_type' : 'discussions',
      'events' : App.standardObjectsListEvents(),
      'multi_title' : App.lang(':num Discussions Selected'),
      'multi_url' : '<?php echo smarty_function_assemble(array('route'=>'project_discussions_mass_edit','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
',
      'multi_actions' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['mass_manager']->value);?>
,
      'print_url' : '<?php echo smarty_function_assemble(array('route'=>'project_discussions','print'=>1,'project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
',
      'prepare_item' : function (item) {
        var result = {
          'id'            : item['id'],
          'name'          : item['name'],
          'icon'          : item['icon'],
          'is_read'       : item['is_read'],
          'is_pinned'     : item['is_pinned'],
          'permalink'     : item['permalink'],
          'is_favorite'   : item['is_favorite'],
          'is_archived'   : item['state'] == '2' ? '1' : '0',
          'is_trashed'    : item['state'] == '1' ? 1 : 0,
          'visibility'    : item['visibility']
        };

        if (typeof(item['category']) == 'undefined') {
          result['category_id'] = item['category_id'];
        } else {
          result['category_id'] = item['category'] ? item['category']['id'] : 0;
        } // if

        if(typeof(item['milestone']) == 'undefined') {
          result['milestone_id'] = item['milestone_id'];
        } else {
          result['milestone_id'] = item['milestone'] ? item['milestone']['id'] : 0;
        } // if

        return result;
      },

      'render_item' : function (item) {
        return '<td class="icon"><img src="' + item['icon'] + '" alt=""></td><td class="name" is_pinned="' + (item['is_pinned'] ? 1 : 0) + '">' + item['name'].clean() + App.Wireframe.Utils.renderVisibilityIndicator(item['visibility']) + '</td><td class="discussion_options"></td>';
      },

      'grouping' : [{
        'label' : App.lang("Don't group"),
        'property' : '', icon : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      }, {
        'label' : App.lang('By Category'),
        'property' : 'category_id',
        'map' : categories_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-category.png', 'categories')
      }, {
        'label' : App.lang('By Milestone'),
        'property' : 'milestone_id',
        'map' : milestones_map ,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-milestones.png', 'system'),
        'uncategorized_label' : App.lang('Unknown Milestone')
      },{
        'label' : App.lang('By Read Status'),
        'property' : 'is_read',
        'map' : read_status_map ,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-status.png', 'environment'),
        'default' : true,
        'uncategorized_label' : App.lang('Unread')
      }]
    };

    if (<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['in_archive']->value);?>
) {
      init_options.requirements.is_archived = 1;
    } else {
      init_options.requirements.is_archived = 0;
    } // if

    wrapper.objectsList(init_options);

    // discussion added
    App.Wireframe.Events.bind('discussion_created.content', function (event, discussion) {
      if (discussion['project_id'] == project_id) {
        wrapper.objectsList('add_item', discussion, true);
      } else {
        if ($.cookie('ac_redirect_to_target_project')) {
          App.Wireframe.Content.setFromUrl(discussion['urls']['view']);
        } // if
      } // if
    });

    // discussion updated
    App.Wireframe.Events.bind('discussion_updated.content', function (event, discussion) {
      if (discussion['project_id'] == project_id) {
        var existing_item = wrapper.objectsList('get_item', discussion['id']);
        if (existing_item && existing_item.length) {
          if (existing_item.find('td.name').attr('is_pinned') != discussion.is_pinned) {
            wrapper.objectsList('refresh');
            return true;
          } // if
        } //if

        wrapper.objectsList('update_item', discussion, true);
      } else {
        if ($.cookie('ac_redirect_to_target_project')) {
          App.Wireframe.Content.setFromUrl(discussion['urls']['view']);
        } else {
          wrapper.objectsList('delete_selected_item');
        } // if
      } // if
    });

    // Discussion deleted
    App.Wireframe.Events.bind('discussion_deleted.content', function (event, discussion) {
      if (discussion['project_id'] == project_id) {
        if (wrapper.objectsList('is_loaded', discussion['id'], false)) {
          wrapper.objectsList('load_empty');
        } // if
        wrapper.objectsList('delete_item', discussion['id']);
      } // if
    });

    // Manage milestones
    App.objects_list_keep_milestones_map_up_to_date(wrapper, 'milestone_id', project_id);

    // Kepp categories map up to date
    App.objects_list_keep_categories_map_up_to_date(wrapper, 'category_id', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_discussion']->value->category()->getCategoryContextString());?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_discussion']->value->category()->getCategoryClass());?>
);

  <?php if ($_smarty_tpl->tpl_vars['active_discussion']->value->isLoaded()){?>
    wrapper.objectsList('load_item', <?php echo clean($_smarty_tpl->tpl_vars['active_discussion']->value->getId(),$_smarty_tpl);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_discussion']->value->getViewUrl());?>
); // Pre select item if this is permalink
  <?php }?>
  });
</script><?php }} ?>