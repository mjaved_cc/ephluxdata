<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 12:50:05
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\environment\layouts\error.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25511b8bfd533335-22902264%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b50eff3e647e7f11f34a728755ad691c25285e31' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\environment\\layouts\\error.tpl',
      1 => 1360573591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25511b8bfd533335-22902264',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_title' => 0,
    'wireframe' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b8bfd60fec',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b8bfd60fec')) {function content_511b8bfd60fec($_smarty_tpl) {?><?php if (!is_callable('smarty_function_brand')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.brand.php';
if (!is_callable('smarty_function_assemble')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.assemble.php';
if (!is_callable('smarty_function_year')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.year.php';
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?php echo clean($_smarty_tpl->tpl_vars['page_title']->value,$_smarty_tpl);?>
</title>
  <link rel="shortcut icon" href="<?php echo smarty_function_brand(array('what'=>'favicon'),$_smarty_tpl);?>
" type="image/x-icon" />
  <link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedStylesheetsUrl(AngieApplication::INTERFACE_DEFAULT,AngieApplication::getDeviceClass(),$_smarty_tpl->tpl_vars['wireframe']->value->getAssetsContext()),$_smarty_tpl);?>
" type="text/css" media="screen">
</head>
<body id="error_page">
  <div id="company_logo">
    <a href="<?php echo smarty_function_assemble(array('route'=>'homepage'),$_smarty_tpl);?>
"><img src="<?php echo smarty_function_brand(array('what'=>'logo'),$_smarty_tpl);?>
" alt="" /></a>
  </div>
  <div id="error_box">
    <h1><?php echo clean($_smarty_tpl->tpl_vars['page_title']->value,$_smarty_tpl);?>
</h1>

    <div class="description">
      <?php if ($_smarty_tpl->tpl_vars['message']->value){?>
        <p><strong><?php echo clean($_smarty_tpl->tpl_vars['message']->value,$_smarty_tpl);?>
</strong></p>
      <?php }?>
      <p>Please check the URL for proper spelling and capitalization. If you're having trouble locating page or object, try using <a href="<?php echo smarty_function_assemble(array('route'=>"backend_search"),$_smarty_tpl);?>
">search</a>, you can go back to <a onclick="history.back(1); return false" href="#">page you came from</a>, or alternatively you can visit <a href="<?php echo smarty_function_assemble(array('route'=>"homepage"),$_smarty_tpl);?>
">homepage</a></p>
    </div>
  </div>
  <div id="footer">
    <p id="copyright">&copy;<?php echo smarty_function_year(array(),$_smarty_tpl);?>
</p>
  </div>
</body>
</html><?php }} ?>