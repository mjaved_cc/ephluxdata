<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 15:13:14
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\system\views\default\users\view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18473511bad8a986fb3-86998859%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cbb2792bf71086bf7875d8edfb7a162ba97c2c8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\system\\views\\default\\users\\view.tpl',
      1 => 1360573751,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18473511bad8a986fb3-86998859',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_user' => 0,
    'logged_user' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511bad8aa269e',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511bad8aa269e')) {function content_511bad8aa269e($_smarty_tpl) {?><?php if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
if (!is_callable('smarty_block_object')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.object.php';
if (!is_callable('smarty_function_inline_tabs')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/modules/system/helpers\\function.inline_tabs.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Profile<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('object', array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value)); $_block_repeat=true; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="wireframe_content_wrapper">
    <?php echo smarty_function_inline_tabs(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value),$_smarty_tpl);?>

  </div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
  App.Wireframe.Events.bind('user_updated.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function (event, user) {
    if(user['class'] == 'User' && user.id == '<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->getId(),$_smarty_tpl);?>
') {
    	// update avatar on the profile page
      $('#user_page_' + user.id + ' #select_user_icon .properties_icon').attr('src', user.avatar.photo);

      // if user is changing their own avatar, update the image at the bottom left corner
      if ('<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->getId(),$_smarty_tpl);?>
' == '<?php echo clean($_smarty_tpl->tpl_vars['logged_user']->value->getId(),$_smarty_tpl);?>
') {
        $('#menu_item_profile img').attr('src', user.avatar.large);
        $('#menu_item_profile span.label').html(user.display_name);
      } // if
    } // if
  });
</script><?php }} ?>