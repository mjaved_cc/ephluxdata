<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 15:57:46
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\tasks\email\new.tpl" */ ?>
<?php /*%%SmartyHeaderCode:24111511a667a8a7948-40011583%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5556f9001b313971df11763d5b8fedcfd93c279' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\tasks\\email\\new.tpl',
      1 => 1360573773,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '24111511a667a8a7948-40011583',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'context' => 0,
    'language' => 0,
    'context_view_url' => 0,
    'recipient' => 0,
    'sender' => 0,
    'style' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a667a9b378',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a667a9b378')) {function content_511a667a9b378($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_block_notification_wrapper')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/email/helpers\\block.notification_wrapper.php';
if (!is_callable('smarty_block_notification_wrap_body')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/email/helpers\\block.notification_wrap_body.php';
if (!is_callable('smarty_function_notification_task_responsibility')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/modules/tasks/helpers\\function.notification_task_responsibility.php';
?>[<?php echo clean($_smarty_tpl->tpl_vars['context']->value->getProject()->getName(),$_smarty_tpl);?>
] <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task ':name' has been Created<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

================================================================================
<?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrapper', array('title'=>'Task Created','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value)); $_block_repeat=true; echo smarty_block_notification_wrapper(array('title'=>'Task Created','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('author_name'=>$_smarty_tpl->tpl_vars['context']->value->getCreatedBy()->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('author_name'=>$_smarty_tpl->tpl_vars['context']->value->getCreatedBy()->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:author_name has just created "<a href=":url" style=":link_style" target="_blank">:name</a>" task<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('author_name'=>$_smarty_tpl->tpl_vars['context']->value->getCreatedBy()->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrap_body', array()); $_block_repeat=true; echo smarty_block_notification_wrap_body(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['context']->value->getBody();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrap_body(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	<?php echo smarty_function_notification_task_responsibility(array('context'=>$_smarty_tpl->tpl_vars['context']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'language'=>$_smarty_tpl->tpl_vars['language']->value),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrapper(array('title'=>'Task Created','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>