<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 06:51:06
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\modules\views\default\fw_modules_admin\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14049511b37dacfedf8-46950888%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '375119642797ed1cd3262ca030a51161f9d75756' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\modules\\views\\default\\fw_modules_admin\\index.tpl',
      1 => 1360573630,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14049511b37dacfedf8-46950888',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'modules' => 0,
    'module' => 0,
    'logged_user' => 0,
    'disable_custom_modules_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b37db16c3b',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b37db16c3b')) {function content_511b37db16c3b($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_cycle')) include 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\vendor\\smarty\\smarty\\plugins\\function.cycle.php';
if (!is_callable('smarty_function_checkbox_field')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.checkbox_field.php';
if (!is_callable('smarty_modifier_clickable')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\modifier.clickable.php';
if (!is_callable('smarty_block_button')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.button.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="modules_admin">
<?php if (is_foreachable($_smarty_tpl->tpl_vars['modules']->value['native_modules'])){?>
  <table class="common modules_list" cellspacing="0">
    <tr>
      <th class="is_enabled"></th>
      <th class="name" colspan="2"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Native Modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
      <th class="options"></th>
    </tr>
  <?php  $_smarty_tpl->tpl_vars['module'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['module']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['modules']->value['native_modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['module']->key => $_smarty_tpl->tpl_vars['module']->value){
$_smarty_tpl->tpl_vars['module']->_loop = true;
?>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'even, odd'),$_smarty_tpl);?>
 <?php echo clean($_smarty_tpl->tpl_vars['module']->value->getName(),$_smarty_tpl);?>
">
      <td class="is_enabled">
      <?php if ($_smarty_tpl->tpl_vars['module']->value->isInstalled()){?>
        <?php echo smarty_function_checkbox_field(array('on_url'=>$_smarty_tpl->tpl_vars['module']->value->getEnableUrl(),'off_url'=>$_smarty_tpl->tpl_vars['module']->value->getDisableUrl(),'class'=>"enabling_disabling_chx",'checked'=>$_smarty_tpl->tpl_vars['module']->value->isEnabled(),'name'=>"enabling",'disabled'=>!$_smarty_tpl->tpl_vars['module']->value->canDisable($_smarty_tpl->tpl_vars['logged_user']->value)),$_smarty_tpl);?>

      <?php }?>
      </td>
      <td class="icon"><img src="<?php echo clean($_smarty_tpl->tpl_vars['module']->value->getIconUrl(),$_smarty_tpl);?>
"></td>
      <td class="name">
        <?php echo clean($_smarty_tpl->tpl_vars['module']->value->getDisplayName(),$_smarty_tpl);?>
, <span class="details">v<?php echo clean($_smarty_tpl->tpl_vars['module']->value->getVersion(),$_smarty_tpl);?>
</span>
      <?php if ($_smarty_tpl->tpl_vars['module']->value->getDescription()){?>
        <span class="details block"><?php echo smarty_modifier_clickable($_smarty_tpl->tpl_vars['module']->value->getDescription());?>
</span>
      <?php }?>
      </td>
      <td class="options">
      <?php if ($_smarty_tpl->tpl_vars['module']->value->isInstalled()){?>
        <?php if ($_smarty_tpl->tpl_vars['module']->value->canUninstall($_smarty_tpl->tpl_vars['logged_user']->value)){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted")); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Uninstall<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
      <?php }else{ ?>
        <?php if ($_smarty_tpl->tpl_vars['module']->value->canInstall($_smarty_tpl->tpl_vars['logged_user']->value)){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form")); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Install<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
      <?php }?>
        </td>
    </tr>
  <?php } ?>
  </table>
  
  <?php if (is_foreachable($_smarty_tpl->tpl_vars['modules']->value['custom_modules'])){?>
    <table class="common modules_list" cellspacing="0">
      <tr>
        <th class="is_enabled"></th>
        <th class="name" colspan="2"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Custom Modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
        <th class="options"></th>
      </tr>
      <?php  $_smarty_tpl->tpl_vars['module'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['module']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['modules']->value['custom_modules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['module']->key => $_smarty_tpl->tpl_vars['module']->value){
$_smarty_tpl->tpl_vars['module']->_loop = true;
?>
        <tr class="<?php echo smarty_function_cycle(array('values'=>'even, odd'),$_smarty_tpl);?>
 <?php echo clean($_smarty_tpl->tpl_vars['module']->value->getName(),$_smarty_tpl);?>
">
          <td class="is_enabled">
            <?php if ($_smarty_tpl->tpl_vars['module']->value->isInstalled()){?>
          <?php echo smarty_function_checkbox_field(array('on_url'=>$_smarty_tpl->tpl_vars['module']->value->getEnableUrl(),'off_url'=>$_smarty_tpl->tpl_vars['module']->value->getDisableUrl(),'class'=>"enabling_disabling_chx",'checked'=>$_smarty_tpl->tpl_vars['module']->value->isEnabled(),'name'=>"enabling",'disabled'=>!$_smarty_tpl->tpl_vars['module']->value->canDisable($_smarty_tpl->tpl_vars['logged_user']->value)),$_smarty_tpl);?>

        <?php }?>
          </td>
          <td class="icon"><img src="<?php echo clean($_smarty_tpl->tpl_vars['module']->value->getIconUrl(),$_smarty_tpl);?>
"></td>
          <td class="name">
            <?php echo clean($_smarty_tpl->tpl_vars['module']->value->getDisplayName(),$_smarty_tpl);?>
, <span class="details">v<?php echo clean($_smarty_tpl->tpl_vars['module']->value->getVersion(),$_smarty_tpl);?>
</span>
            <?php if ($_smarty_tpl->tpl_vars['module']->value->getDescription()){?>
              <span class="details block"><?php echo smarty_modifier_clickable($_smarty_tpl->tpl_vars['module']->value->getDescription());?>
</span>
            <?php }?>
          </td>
          <td class="options">
            <?php if ($_smarty_tpl->tpl_vars['module']->value->isInstalled()){?>
              <?php if ($_smarty_tpl->tpl_vars['module']->value->canUninstall($_smarty_tpl->tpl_vars['logged_user']->value)){?>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted")); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Uninstall<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallUrl(),'class'=>"uninstall_module_btn",'confirm'=>$_smarty_tpl->tpl_vars['module']->value->getUninstallMessage(),'success_event'=>"module_deleted"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              <?php }?>
              <?php }else{ ?>
              <?php if ($_smarty_tpl->tpl_vars['module']->value->canInstall($_smarty_tpl->tpl_vars['logged_user']->value)){?>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form")); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Install<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['module']->value->getInstallUrl(),'class'=>"install_module_btn",'title'=>"Install Module",'mode'=>"flyout_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              <?php }?>
            <?php }?>
          </td>
        </tr>
      <?php } ?>
        <tr>
          <td class="is_enabled"></td>
          <td colspan="2" class="disable_all_custom_modules">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['disable_custom_modules_url']->value,'class'=>"disable_custom_modules_btn",'confirm'=>'Are you sure that you want to disable all custom modules?','title'=>"Disable all custom modules",'success_event'=>"modules_disabled")); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['disable_custom_modules_url']->value,'class'=>"disable_custom_modules_btn",'confirm'=>'Are you sure that you want to disable all custom modules?','title'=>"Disable all custom modules",'success_event'=>"modules_disabled"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Disable all custom modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['disable_custom_modules_url']->value,'class'=>"disable_custom_modules_btn",'confirm'=>'Are you sure that you want to disable all custom modules?','title'=>"Disable all custom modules",'success_event'=>"modules_disabled"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </td>
          <td class="options"></td>
        </tr>
    </table>
  <?php }?>
<?php }else{ ?>
  <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no modules<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
<?php }?>
</div>
<script type="text/javascript">
  (function() {
    $('.enabling_disabling_chx').asyncCheckbox({
      'success' : function() {
        if(this.checked) {
          App.Wireframe.Flash.success(App.lang('Module has been enabled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));
        } else {
          App.Wireframe.Flash.success(App.lang('Module has been disabled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));
        } // if

        location.reload();
      }
    });

    App.Wireframe.Events.bind('modules_disabled.content', function(event, module) {
      App.Wireframe.Flash.success(App.lang('Custom modules has been disabled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));

      location.reload();
    });

    App.Wireframe.Events.bind('module_created.content', function(event, module) {
      App.Wireframe.Flash.success(App.lang('Module has been installed') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));

      location.reload();
    });

    App.Wireframe.Events.bind('module_deleted.content', function(event, module) {
      App.Wireframe.Flash.success(App.lang('Module has been uninstalled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));

      location.reload();
    });

  })();
</script><?php }} ?>