<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 15:58:22
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\environment\views\default\fw_control_tower\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:30438511a669e8478e4-90600014%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd063ad613846c13ea97d07bd4e63e98c810f4f85' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\environment\\views\\default\\fw_control_tower\\index.tpl',
      1 => 1360573620,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30438511a669e8478e4-90600014',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'control_tower' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a669e8aab6',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a669e8aab6')) {function content_511a669e8aab6($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_json')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\modifier.json.php';
?><?php echo $_smarty_tpl->tpl_vars['control_tower']->value->render();?>


<script type="text/javascript">
  App.Wireframe.Statusbar.setItemBadge('statusbar_item_control_tower', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['control_tower']->value->loadBadgeValue());?>
);

  $('#control_tower').each(function() {
    var wrapper = $(this);
    var original_image = false;

    wrapper.find('td.submit a').each(function() {
      var anchor = $(this);
      var image = anchor.find('img:first');

      if (original_image === false) {
        original_image = image.attr('src');
      } // if

      anchor.click(function () {
        image.attr('src', App.Wireframe.Utils.indicatorUrl());
        $.ajax({
          'url' : App.extendUrl(anchor.attr('href'), { 'async' : 1 }),
          'type' : 'post',
          'data' : { 'submitted' : 'submitted' },
          success : function () {
            App.Wireframe.Flash.success(anchor.attr('while_working_message') + ' ' + App.lang('succeeded'));
            image.attr('src', original_image);
          },
          error : function () {
            App.Wireframe.Flash.error(anchor.attr('while_working_message') + ' ' + App.lang('failed'));
            image.attr('src', original_image);
          }
        });

        return false;
      });
    });
  });
</script><?php }} ?>