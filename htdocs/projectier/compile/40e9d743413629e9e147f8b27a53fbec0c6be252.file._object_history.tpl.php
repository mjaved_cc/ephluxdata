<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 15:57:50
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\history\views\default\_object_history.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11851511a667e3dbd60-12384131%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40e9d743413629e9e147f8b27a53fbec0c6be252' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\history\\views\\default\\_object_history.tpl',
      1 => 1360573585,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11851511a667e3dbd60-12384131',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_history_object' => 0,
    '_history_modifications' => 0,
    '_history_modification' => 0,
    '_history_modification_modification' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a667e48202',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a667e48202')) {function content_511a667e48202($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_assemble')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.assemble.php';
?><div class="resource object_history object_section" id="object_history_<?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getId(),$_smarty_tpl);?>
">
  <div class="content_section_title"><h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
History<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2></div>
  
  <div class="object_history_logs object_section_content common_object_section_content">
  <?php if (is_foreachable($_smarty_tpl->tpl_vars['_history_modifications']->value)){?>
    <?php  $_smarty_tpl->tpl_vars['_history_modification'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_history_modification']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_history_modifications']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_history_modification']->key => $_smarty_tpl->tpl_vars['_history_modification']->value){
$_smarty_tpl->tpl_vars['_history_modification']->_loop = true;
?>
    <div class="object_history_log">
      <div class="object_history_modification_head"><?php echo $_smarty_tpl->tpl_vars['_history_modification']->value['head'];?>
</div>
      <ul>
      <?php  $_smarty_tpl->tpl_vars['_history_modification_modification'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['_history_modification_modification']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_history_modification']->value['modifications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['_history_modification_modification']->key => $_smarty_tpl->tpl_vars['_history_modification_modification']->value){
$_smarty_tpl->tpl_vars['_history_modification_modification']->_loop = true;
?>
        <li><?php echo $_smarty_tpl->tpl_vars['_history_modification_modification']->value;?>
</li>
      <?php } ?>
      </ul>
    </div>
    <?php } ?>
  <?php }else{ ?>
    <p class="empty_page"><span class="inner"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
History is empty<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></p>
  <?php }?>
  </div>
</div>

<script type="text/javascript">
  var wrapper = $('#object_history_<?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getId(),$_smarty_tpl);?>
');
  var refresh_history_url = '<?php echo smarty_function_assemble(array('route'=>'object_history','object_id'=>$_smarty_tpl->tpl_vars['_history_object']->value->getId(),'object_class'=>get_class($_smarty_tpl->tpl_vars['_history_object']->value),'async'=>1),$_smarty_tpl);?>
';

  var modifications_wrapper = wrapper.find('div.object_history_logs');
   
  App.Wireframe.Events.bind('<?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getUpdatedEventName(),$_smarty_tpl);?>
.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
 <?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getDeletedEventName(),$_smarty_tpl);?>
.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function (event, object) {
    if (object['id'] != '<?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getId(),$_smarty_tpl);?>
' || object['class'] != '<?php echo clean($_smarty_tpl->tpl_vars['_history_object']->value->getType(),$_smarty_tpl);?>
') {
      return false;
    } // if

    $.ajax({
       'url'      : refresh_history_url,
       'success'  : function (response) {
         response = $.trim(response);
         modifications_wrapper.empty();
         if (response) {
           modifications_wrapper.append(response);  
         } else {
           modifications_wrapper.append('<p class="empty_page"><span class="inner">' + App.lang('History is empty') + '</span></p>');
         } // if

       }
    });
  });
</script><?php }} ?>