<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 15:57:49
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\tasks\views\default\tasks\view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7228511a667d679c66-07844529%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c08f39acfacdb16be23aecc5c68c92764997f1bb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\tasks\\views\\default\\tasks\\view.tpl',
      1 => 1360573774,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7228511a667d679c66-07844529',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_task' => 0,
    'logged_user' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a667d838d3',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a667d838d3')) {function content_511a667d838d3($_smarty_tpl) {?><?php if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
if (!is_callable('smarty_block_object')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.object.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_object_attachments')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/attachments/helpers\\function.object_attachments.php';
if (!is_callable('smarty_function_object_subtasks')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/subtasks/helpers\\function.object_subtasks.php';
if (!is_callable('smarty_function_object_comments')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/comments/helpers\\function.object_comments.php';
if (!is_callable('smarty_function_object_history')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/history/helpers\\function.object_history.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('object', array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value)); $_block_repeat=true; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="wireframe_content_wrapper">
    <div class="object_body with_shadow">
      <div class="object_content_wrapper"><div class="object_body_content formatted_content">
        <?php if ($_smarty_tpl->tpl_vars['active_task']->value->inspector()->hasBody()){?>
          <?php echo $_smarty_tpl->tpl_vars['active_task']->value->inspector()->getBody();?>

        <?php }else{ ?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No description provided<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
      </div>
        <?php echo smarty_function_object_attachments(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>

      </div>
      
      <?php echo smarty_function_object_subtasks(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>

    </div>
  </div>
  
  <div class="wireframe_content_wrapper"><?php echo smarty_function_object_comments(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'show_first'=>'yes'),$_smarty_tpl);?>
</div>
  <div class="wireframe_content_wrapper"><?php echo smarty_function_object_history(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
  App.Wireframe.Events.bind('create_invoice_from_task.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function (event, invoice) {
   	if (invoice['class'] == 'Invoice') {
   		App.Wireframe.Flash.success(App.lang('New invoice created'));
   		App.Wireframe.Content.setFromUrl(invoice['urls']['view']);
	  } // if
	});
</script>	<?php }} ?>