<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 07:47:48
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\modules\system\layouts\frontend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3662511b45246a6664-95932209%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2878f440d7a18ab4021ad4014448ccbaf7e3b40b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\modules\\system\\layouts\\frontend.tpl',
      1 => 1360573726,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3662511b45246a6664-95932209',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'locale_code' => 0,
    'prefered_interface' => 0,
    'wireframe' => 0,
    'head_tag' => 0,
    'rss_feed' => 0,
    'content_for_layout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b45247b053',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b45247b053')) {function content_511b45247b053($_smarty_tpl) {?><?php if (!is_callable('smarty_function_image_url')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.image_url.php';
if (!is_callable('smarty_function_page_title')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.page_title.php';
if (!is_callable('smarty_function_template_vars_to_js')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.template_vars_to_js.php';
if (!is_callable('smarty_function_brand')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.brand.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo clean($_smarty_tpl->tpl_vars['locale_code']->value,$_smarty_tpl);?>
" lang="<?php echo clean($_smarty_tpl->tpl_vars['locale_code']->value,$_smarty_tpl);?>
">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

  <?php if (activeCollab::getBrandingRemoved()){?>
    <meta name="msapplication-TileImage" content="<?php echo smarty_function_image_url(array('name'=>'layout/windows-tiles/not-branded.png','module'=>@SYSTEM_MODULE,'interface'=>@AngieApplication::INTERFACE_DEFAULT),$_smarty_tpl);?>
">
    <?php }else{ ?>
    <meta name="msapplication-TileImage" content="<?php echo smarty_function_image_url(array('name'=>'layout/windows-tiles/branded.png','module'=>@SYSTEM_MODULE,'interface'=>@AngieApplication::INTERFACE_DEFAULT),$_smarty_tpl);?>
">
  <?php }?>
    <meta name="msapplication-TileColor" content="#95000">
    <meta name="robots" content="noindex, nofollow">

    <title><?php echo smarty_function_page_title(array('default'=>"Index"),$_smarty_tpl);?>
</title>
    
    <!--  Stylesheets -->
    <link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedStylesheetsUrl($_smarty_tpl->tpl_vars['prefered_interface']->value,'unknown',$_smarty_tpl->tpl_vars['wireframe']->value->getAssetsContext()),$_smarty_tpl);?>
" type="text/css" media="screen"/>
    
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_DEFAULT,AngieApplication::getDeviceClass(),$_smarty_tpl->tpl_vars['wireframe']->value->getAssetsContext()),$_smarty_tpl);?>
"></script>
    
    <?php echo smarty_function_template_vars_to_js(array('wireframe'=>$_smarty_tpl->tpl_vars['wireframe']->value),$_smarty_tpl);?>

    
    <!-- Head tags -->
    <?php if ($_smarty_tpl->tpl_vars['wireframe']->value->getAllHeadTags()){?>
      <?php  $_smarty_tpl->tpl_vars['head_tag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['head_tag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wireframe']->value->getAllHeadTags(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['head_tag']->key => $_smarty_tpl->tpl_vars['head_tag']->value){
$_smarty_tpl->tpl_vars['head_tag']->_loop = true;
?>
        <?php echo $_smarty_tpl->tpl_vars['head_tag']->value;?>

      <?php } ?>
    <?php }?>
    
    <!-- Meta -->
    <link rel="shortcut icon" href="<?php echo smarty_function_brand(array('what'=>'favicon'),$_smarty_tpl);?>
" type="image/x-icon" />
    
    <?php if (is_foreachable($_smarty_tpl->tpl_vars['wireframe']->value->getRssFeeds())){?>
      <?php  $_smarty_tpl->tpl_vars['rss_feed'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rss_feed']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wireframe']->value->getRssFeeds(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['rss_feed']->key => $_smarty_tpl->tpl_vars['rss_feed']->value){
$_smarty_tpl->tpl_vars['rss_feed']->_loop = true;
?>
        <link rel="alternate" type="<?php echo clean($_smarty_tpl->tpl_vars['rss_feed']->value['feed_type'],$_smarty_tpl);?>
" title="<?php echo clean($_smarty_tpl->tpl_vars['rss_feed']->value['title'],$_smarty_tpl);?>
" href="<?php echo clean($_smarty_tpl->tpl_vars['rss_feed']->value['url'],$_smarty_tpl);?>
" />
      <?php } ?>
    <?php }?>
  </head>
  <body>
    <div id="public_header"><div class="public_wrapper">
      <img src="<?php echo smarty_function_brand(array('what'=>"logo"),$_smarty_tpl);?>
" alt="Logo"/>
    </div></div>
    
    <div id="public_page_title"><div class="public_wrapper">
      <h1><?php echo smarty_function_page_title(array('default'=>"Index"),$_smarty_tpl);?>
</h1>
    </div></div>
    
    <div id="public_content"><div class="public_wrapper">
    	<?php echo $_smarty_tpl->tpl_vars['content_for_layout']->value;?>

    </div></div>
    
    <div id="public_footer"><div class="public_wrapper">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Powered by<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<br />
      <img src="<?php echo smarty_function_brand(array('what'=>"logo",'size'=>"40x40"),$_smarty_tpl);?>
" alt="Logo" class="ac_logo"/>
    </div></div>
  </body>
</html><?php }} ?>