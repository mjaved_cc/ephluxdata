<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 07:47:30
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\email\views\default\activity_log_details\message_sent.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8016511b451200ae10-35115091%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5182b08dcf0265fc9f96c37afb872f4af3dbf00c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\email\\views\\default\\activity_log_details\\message_sent.tpl',
      1 => 1360573560,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8016511b451200ae10-35115091',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'log_entry' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b4512108ab',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b4512108ab')) {function content_511b4512108ab($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
if (!is_callable('smarty_function_user_link')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/authentication/helpers\\function.user_link.php';
?><div id="message_sent_details" class="message_mailing_log_details">
  <div class="head">
    <div class="properties">
      <div class="property">
        <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sender<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
        <div class="data"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['log_entry']->value->getFrom()),$_smarty_tpl);?>
</div>
      </div>
      
      <div class="property">
        <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Recipient<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
        <div class="data"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['log_entry']->value->getTo()),$_smarty_tpl);?>
</div>
      </div>
      
      <div class="property">
        <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subject<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
        <div class="data"><?php echo clean($_smarty_tpl->tpl_vars['log_entry']->value->getAdditionalProperty('subject'),$_smarty_tpl);?>
</div>
      </div>
    </div>
  </div>
  
  <div class="body"><?php echo $_smarty_tpl->tpl_vars['log_entry']->value->getAdditionalProperty('body');?>
</div>
</div><?php }} ?>