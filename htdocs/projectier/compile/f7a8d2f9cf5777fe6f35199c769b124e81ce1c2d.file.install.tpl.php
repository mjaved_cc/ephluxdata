<?php /* Smarty version Smarty-3.1.7, created on 2013-02-13 07:15:48
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\modules\views\default\fw_modules_admin\install.tpl" */ ?>
<?php /*%%SmartyHeaderCode:30523511b3da4365b30-57744514%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7a8d2f9cf5777fe6f35199c769b124e81ce1c2d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\modules\\views\\default\\fw_modules_admin\\install.tpl',
      1 => 1360573630,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30523511b3da4365b30-57744514',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'installation_steps' => 0,
    'installation_step' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511b3da4411e5',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511b3da4411e5')) {function content_511b3da4411e5($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.add_bread_crumb.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Install Module<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Install<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<!--  Install -->
<div id="install_module" class="content_stack_element">
  
  	<div id="installation_steps">
  	  <ul id="initialize_upgrade_process" class="async_process">
      <?php  $_smarty_tpl->tpl_vars['installation_step'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['installation_step']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['installation_steps']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['installation_step']->key => $_smarty_tpl->tpl_vars['installation_step']->value){
$_smarty_tpl->tpl_vars['installation_step']->_loop = true;
?>
        <li class="step" step_url="<?php echo clean($_smarty_tpl->tpl_vars['installation_step']->value['url'],$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['installation_step']->value['text'],$_smarty_tpl);?>
</li>
        
      <?php } ?>
      </ul>
  	</div>
	
	<p style="display: none; margin-left:10px;" id="message"></p>
	
  </div>

<script type="text/javascript">
	
	$('#initialize_upgrade_process').asyncProcess({
    'success' : function(response, step_num) {
	  $('#initialize_upgrade_process').after(response);
      //$('#install_module #message').html(App.ucfirst(response.name) + ' module successfully installed').show();
	    App.widgets.FlyoutDialog.front().close();
      App.Wireframe.Events.trigger('module_created',response);
//	  $("#close_btn_box").show().find('button').click(function(){
//			App.Wireframe.Events.trigger('module_created',response);
//		  App.widgets.FlyoutDialog.front().close();
//		});
    },
    'error' : function (response) {
    	$('#install_module #message').html(App.Wireframe.Utils.responseToErrorMessage(response)).css('color','red').show();
//    	$("#close_btn_box").show().find('button').click(function(){
//			App.widgets.FlyoutDialog.front().close();
//		});
    },
    'on_step_success' : function (response) {
		
    },
    'on_step_error' : function (response) {
        //App.Wireframe.Flash.error(App.Wireframe.Utils.responseToErrorMessage(response));
    	$('#install_module #message').html(App.Wireframe.Utils.responseToErrorMessage(response)).css('color','red').show();
//    	$("#close_btn_box").show().find('button').click(function(){
//			App.widgets.FlyoutDialog.front().close();
//		});
    }
  });
</script>


<?php }} ?>