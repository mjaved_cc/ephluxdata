<?php /* Smarty version Smarty-3.1.7, created on 2013-02-12 15:57:49
         compiled from "C:\xampp\htdocs\projectier\activecollab\3.2.9\angie\frameworks\subtasks\views\default\_object_subtask_form_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32197511a667d8f4948-25043677%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd6fe2365a1fb553cae03038710be1c889a51a8ab' => 
    array (
      0 => 'C:\\xampp\\htdocs\\projectier\\activecollab\\3.2.9\\angie\\frameworks\\subtasks\\views\\default\\_object_subtask_form_row.tpl',
      1 => 1360573579,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32197511a667d8f4948-25043677',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'subtask' => 0,
    'subtask_parent' => 0,
    'subtask_data' => 0,
    'subtasks_id' => 0,
    'user' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_511a667dadfb4',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_511a667dadfb4')) {function content_511a667dadfb4($_smarty_tpl) {?><?php if (!is_callable('smarty_function_text_field')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.text_field.php';
if (!is_callable('smarty_block_label')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.label.php';
if (!is_callable('smarty_function_select_assignee')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/assignees/helpers\\function.select_assignee.php';
if (!is_callable('smarty_function_select_priority')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/complete/helpers\\function.select_priority.php';
if (!is_callable('smarty_function_select_label')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/labels/helpers\\function.select_label.php';
if (!is_callable('smarty_function_select_due_on')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\function.select_due_on.php';
if (!is_callable('smarty_block_submit')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/environment/helpers\\block.submit.php';
if (!is_callable('smarty_block_lang')) include 'C:\\xampp\\htdocs\\projectier\\activecollab/3.2.9/angie/frameworks/globalization/helpers\\block.lang.php';
?><?php if ($_smarty_tpl->tpl_vars['subtask']->value instanceof Subtask&&$_smarty_tpl->tpl_vars['subtask']->value->isLoaded()){?>
<tr class="edit_subtask">
<?php }else{ ?>
<tr class="new_subtask" style="display: none">
<?php }?>
  <td class="task_reorder"></td>
  <td class="task_meta"></td>
  <td colspan="2" class="task_content">
  <?php if ($_smarty_tpl->tpl_vars['subtask']->value instanceof Subtask&&$_smarty_tpl->tpl_vars['subtask']->value->isLoaded()){?>
    <form action="<?php echo clean($_smarty_tpl->tpl_vars['subtask']->value->getEditUrl(),$_smarty_tpl);?>
" method="post" class="subtask_form">
  <?php }else{ ?>
    <form action="<?php echo clean($_smarty_tpl->tpl_vars['subtask_parent']->value->subtasks()->getAddUrl(),$_smarty_tpl);?>
" method="post" class="subtask_form">
  <?php }?>
      <div class="subtask_summary">
        <?php echo smarty_function_text_field(array('name'=>'subtask[body]','value'=>$_smarty_tpl->tpl_vars['subtask_data']->value['body'],'class'=>'long','id'=>($_smarty_tpl->tpl_vars['subtasks_id']->value)."_summary_field"),$_smarty_tpl);?>

      </div>
      
      <div class="subtask_attributes">
        <div class="subtask_attribute subtask_assignee">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_select_assignee")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_select_assignee"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Assignee<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_select_assignee"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_assignee(array('name'=>'subtask[assignee_id]','value'=>$_smarty_tpl->tpl_vars['subtask_data']->value['assignee_id'],'parent'=>$_smarty_tpl->tpl_vars['subtask']->value,'user'=>$_smarty_tpl->tpl_vars['user']->value,'id'=>($_smarty_tpl->tpl_vars['subtasks_id']->value)."_select_assignee"),$_smarty_tpl);?>

        </div>
        
      <?php if ($_smarty_tpl->tpl_vars['subtask']->value->usePriority()){?>
        <div class="subtask_attribute subtask_priority">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_task_priority")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_task_priority"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Priority<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_task_priority"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_priority(array('name'=>'subtask[priority]','value'=>$_smarty_tpl->tpl_vars['subtask_data']->value['priority'],'id'=>($_smarty_tpl->tpl_vars['subtasks_id']->value)."_task_priority"),$_smarty_tpl);?>

        </div>
      <?php }?>
        
      <?php if ($_smarty_tpl->tpl_vars['subtask']->value->useLabels()){?>
        <div class="subtask_attribute subtask_label">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_label")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_label"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Label<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_label"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_label(array('name'=>'subtask[label_id]','value'=>$_smarty_tpl->tpl_vars['subtask_data']->value['label_id'],'type'=>get_class($_smarty_tpl->tpl_vars['subtask']->value->label()->newLabel()),'id'=>($_smarty_tpl->tpl_vars['subtasks_id']->value)."_label",'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'can_create_new'=>false),$_smarty_tpl);?>

        </div>
      <?php }?>
        
        <div class="subtask_attribute subtask_due_on">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_due_on")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_due_on"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Due On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".($_smarty_tpl->tpl_vars['subtasks_id']->value).")_due_on"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_due_on(array('name'=>'subtask[due_on]','value'=>$_smarty_tpl->tpl_vars['subtask_data']->value['due_on'],'id'=>($_smarty_tpl->tpl_vars['subtasks_id']->value)."_due_on"),$_smarty_tpl);?>

        </div>
      </div>
      
      <input type="hidden" name="submitted" value="submitted" />
      
      <div class="subtask_buttons_wrapper">
      <?php if ($_smarty_tpl->tpl_vars['subtask']->value instanceof Subtask&&$_smarty_tpl->tpl_vars['subtask']->value->isLoaded()){?>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <a href="#" class="subtask_cancel"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Close<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a>
      <?php }else{ ?>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add Subtask<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <a href="#" class="subtask_cancel"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Close<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a>
      <?php }?>
      </div>
    </form>
  </td>
</tr><?php }} ?>