<?php

  /**
   * activeCollab configuration file
   */

  define('ROOT', 'C:\\xampp\\htdocs\\projectier\\activecollab');
  define('ROOT_URL', 'http://192.175.0.154/projectier/public');
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'projectier');
  define('DB_CAN_TRANSACT', true);
  define('TABLE_PREFIX', 'acx_');
  define('ADMIN_EMAIL', 'shakeel.uddin@ephlux.com');
  define('USE_UNPACKED_FILES', true);
  define('DEBUG', 0);


  if(!defined('CONFIG_PATH')) {
    define('CONFIG_PATH', dirname(__FILE__));
  }

  require_once CONFIG_PATH . '/version.php';
  require_once CONFIG_PATH . '/license.php';
  require_once CONFIG_PATH . '/defaults.php';