<?php

App::uses('OAuth', 'Lib/Twitter');
App::uses('TwitterOAuth', 'Lib/Twitter');
App::uses('Component', 'Controller');

class TwitterComponent extends Component {
	
	public $components = array('Session', 'App');
	private $_consumer_key;
	private $_consumer_key_secret;
	private $_request_token_url;
	private $_access_token_url;
	private $_auth_url;
	private $_twitter_obj;
	private $_controller;

	const TWITTER_USER_DETAILS = 'account/verify_credentials';

	function startup(Controller $controller) {

		Configure::load('twitter');
		$consumer_key = Configure::read('Twitter.CONSUMER_KEY');
		$consumer_secret = Configure::read('Twitter.CONSUMER_SECRET');

		$this->_controller = $controller;
		$this->_consumer_key = $consumer_key;
		$this->_consumer_key_secret = $consumer_secret;
		$this->_request_token_url = "https://api.twitter.com/oauth/request_token";
		$this->_access_token_url = "https://api.twitter.com/oauth/access_token";
		$this->_auth_url = "https://api.twitter.com/oauth/authorize";

		$this->_twitter_obj = new TwitterOAuth($this->_consumer_key, $this->_consumer_key_secret);
	}
	
	/**
	 * Get Authorization URL
	 * 
	 * @return string URL
	 */
	function getAuthorizeUrl() {

		try {
			$request_token = $this->_twitter_obj->getRequestToken();

			/* Save temporary credentials to session. */
			$this->Session->write('oauth_token', $request_token['oauth_token']);
			$this->Session->write('oauth_token_secret', $request_token['oauth_token_secret']);

			/* If last connection failed don't display authorization link. */
			switch ($this->_twitter_obj->http_code) {
				case 200:
					/* Build authorize URL and redirect user to Twitter. */
					return $this->_twitter_obj->getAuthorizeURL($request_token['oauth_token']);
					break;
				default:
					/* Show notification if something went wrong. */
					$this->log('Could not connect to Twitter. Refresh the page or try again later.', 'social');
			}
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}
	
	/**
	 * Set tokens 
	 * 
	 * @param string $oauth_token Oauth Token
	 * @param string $oauth_token_secret Oauth Token Secret
	 * @return void
	 */
	function setToken($oauth_token, $oauth_token_secret) {
		$this->_twitter_obj = new TwitterOAuth(
						$this->_consumer_key, $this->_consumer_key_secret,
						$oauth_token, $oauth_token_secret);
	}
	
	/**
	 * Get access token
	 * 
	 * @return array token details
	 */
	function getAccessToken() {

		try {

			if (
					isset($this->_controller->request->query['oauth_token']) &&
					$this->Session->read('oauth_token') !== $this->_controller->request->query['oauth_token']
			) {
				$this->_controller->redirect(array(
					'controller' => 'demos', 'action' => 'login_with_twitter'
				));
			}

			$this->setToken($this->Session->read('oauth_token'), $this->Session->read('oauth_token_secret'));

			$access_token = $this->_twitter_obj->getAccessToken($this->_controller->request->query['oauth_verifier']);

			$this->Session->write('access_token', $access_token);
			$this->Session->delete('oauth_token');
			$this->Session->delete('oauth_token_secret');

			if (200 == $this->_twitter_obj->http_code) {
				return $access_token;
			} else {
				$this->_controller->redirect(array(
					'controller' => 'demos', 'action' => 'login_with_twitter'
				));
			}
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}
	
	/**
	 * Get user details
	 * 
	 * @param string $oauth_token Oauth Token
	 * @param string $oauth_token_secret Oauth Token Secret
	 * @return array response
	 */
	function getUserDetails($oauth_token, $oauth_token_secret) {

		try {
			$this->setToken($oauth_token, $oauth_token_secret);
			return Set::reverse($this->_twitter_obj->get(self::TWITTER_USER_DETAILS));
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}
	
	/**
	 * Get data for given url
	 * 
	 * @param string $url TwitterApi Url
	 * @return array response
	 */
	function getDataFromFeed($url) {
		try {
			return Set::reverse($this->_twitter_obj->get($url));
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}
	
	/**
	 * Get profile image url
	 * 
	 * @param string $profile_image url
	 * @return string url
	 */
	static function getImageUrl($profile_image) {
		
		$path = pathinfo($profile_image);
		$filename = str_replace('_normal', '_bigger', $path['filename']);
		$profile_image_url = $path['dirname'] . '/' . $filename . '.' . $path['extension'];
		
		return $profile_image_url;
	}
}

?>