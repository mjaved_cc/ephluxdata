<?php

class DtAuthActionMap {

	private $_fields = array();
	private $_allowed_keys = array('id', 'description', 'aco_id', 'feature_id', 'crud', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if ($key == 'formatted_desc') {
				return $this->get_formatted_desc();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}
	
	/**
	 * Returns description formatted as Upper case First
	 * 
	 * @return string description 
	 */
	function get_formatted_desc(){
		return ucfirst($this->description) ;
	}
	
	function is_desc_empty(){ 
		return ($this->description === '0') ? true : false ;
	}
	
	function get_field() {

		return array(
			'id' => $this->id,
			'description' => $this->formatted_desc,
			'created' => $this->created
		);
	}

}

?>
