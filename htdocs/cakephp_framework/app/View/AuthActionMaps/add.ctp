<div class="authActionMaps form">
<?php echo $this->Form->create('AuthActionMap'); ?>
	<fieldset>
		<legend><?php echo __('Add Auth Action Map'); ?></legend>
	<?php
		echo $this->Form->input('aco_id');
		echo $this->Form->input('crud');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Acos'), array('controller' => 'acos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('controller' => 'acos', 'action' => 'add')); ?> </li>
	</ul>
</div>
