<div class="authActionMaps view">
	<h2><?php echo __('Auth Action Map'); ?></h2>
	<dl>
<!--		<dt><?php echo __('Id'); ?></dt>
		<dd>
		<?php echo h($authActionMap['AuthActionMap']['id']); ?>
			&nbsp;
		</dd>-->
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($authActionMap['Feature']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Controller'); ?></dt>
		<dd>
			<?php echo $authActionMap['Controller']['alias']; // $this->Html->link($authActionMap['Aco']['id'], array('controller' => 'acos', 'action' => 'view', $authActionMap['Aco']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo $authActionMap['Action']['alias']; // $this->Html->link($authActionMap['Aco']['id'], array('controller' => 'acos', 'action' => 'view', $authActionMap['Aco']['id'])); ?>
			&nbsp;
		</dd>
<!--		<dt><?php echo __('Treated as'); ?></dt>
		<dd>
			<?php echo h($authActionMap['AuthActionMap']['crud']); ?>
			&nbsp;
		</dd>-->		
		<dt><?php echo __('Allowed Groups'); ?></dt>
		<dd>
			<?php if (!empty($assigned_groups)) { ?>
				<ul>
					<?php foreach ($assigned_groups as $value) { ?>

						<li><?php echo $value['Group']['name'] ?></li>

					<?php } ?>
				</ul>
			<?php
			} else {
				echo 'None';
			}
			?>
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
<?php echo h($authActionMap['AuthActionMap']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
<?php echo h($authActionMap['AuthActionMap']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Auth Action Map'), array('action' => 'edit', $authActionMap['AuthActionMap']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Auth Action Map'), array('action' => 'delete', $authActionMap['AuthActionMap']['id']), null, __('Are you sure you want to delete # %s?', $authActionMap['AuthActionMap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Auth Action Map'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Acos'), array('controller' => 'acos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('controller' => 'acos', 'action' => 'add')); ?> </li>
	</ul>
</div>
