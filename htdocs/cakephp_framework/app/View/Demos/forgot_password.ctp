
<?php
if (isset($api_response)) {
	
	$response = json_decode($api_response);
	
	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}
	
	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>


<div class="box">
	
	<div class="header">
		<p><img src="<?php echo $this->webroot; ?>img/half_width_icon.png" alt="Half Width Box" width="30" height="30" />Enter your email address</p>
	</div>
	
	<div class="body">

		<?php echo $this->Form->create('User'); ?>
		<p> <?php echo $this->Form->input('username', array('label' => 'Email', 'class' => 'textfield large', 'div' => false)); ?> </p>
		<p> <?php echo $this->Form->submit('Submit', array('class' => 'button2', 'div' => false)); ?> </p>

	</div>
</div>