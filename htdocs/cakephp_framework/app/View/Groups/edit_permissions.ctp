<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="Actions" width="24" height="24" />Actions
		</div>
		<div class="body">
			<div class="actions">				
				<ul>					
					<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="main_column">
	<div class="box">
		<div class="body">
			<?php
			echo $this->Form->create('Group');
			if (!empty($permissions)) {
				?>
				<table class="style_1" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th>Permissions</th>
						<th>Allow</th>
					</tr>
					<?php foreach ($permissions as $feature => $data) { ?>
						<tr>
							<td colspan="2"><h3><?php echo $feature ?></h3></td>
						</tr>
						<?php foreach ($data as $permission) { ?>
							<tr>
								<td><?php echo $permission['AuthActionMap']['description'] ?></td>
								<td><?php
				echo $this->Form->checkbox('is_allow', array(
					'hiddenField' => false,
					'name' => 'is_allowed[' . $permission['Parent']['alias'] . '/' . $permission['Child']['alias'] . ']',
					'checked' => $permission['is_allowed']
				));
							?></td>
							</tr>
							<?php
						}
					}
					?>					
				</table>
			<?php } ?> 
			<p style="margin: 1em 0;"><?php echo $this->Form->submit('Submit', array('class' => 'button2', 'div' => false)); ?></p>
		</div>
	</div>
</div>
