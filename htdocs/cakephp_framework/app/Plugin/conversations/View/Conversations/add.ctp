<?php 
	echo $this->Html->css('/conversations/css/chosen.jquery');
	echo $this->Html->script('/conversations/js/jquery.1.6.2.min');
	echo $this->Html->script('/conversations/js/chosen.jquery.min');
	echo $this->Html->script('/conversations/js/jquery.validate');
	echo $this->Html->script('/conversations/js/additional-methods');
?>
<script>
	jQuery(document).ready(function($) {
		$('.i-select').chosen();
		
		$.validator.addMethod(     //adding a method to validate select box//
            "chosen",
            function(value, element) {
                return (value == null ? false : (value.length == 0 ? false : true))
            },
            "please select an option"//custom message
		);
			
		/*$('#ConversationAddForm').On('submit', function(e) {
			if(!$('#RecipientsTo').valid()) {
				e.preventDefault();
			}
		}); */ 
		$("#ConversationAddForm").validate({
			rules: {
				'data[Recipients][to][]': {
					chosen: true
				}
			}
		});
		$("#ConversationAddForm").submit(function()
		{
			if(!$('#RecipientsTo').valid()) {
				return false;
			}
	
		});
		//$("#ConversationAddForm").validate();
	});
</script>


<div class="conversations form">
<?php echo $this->Form->create('Conversation', array('action' => 'add')); ?>
	<fieldset>
		<legend><?php echo __('Compose Message'); ?></legend>
        From: <?php echo AuthComponent::user('username'); ?>
	<?php
		echo $this->Form->input('Recipients.to', 
			array(
				'type' => 'select', 
				'multiple' => true, 
				'class' => 'i-select chosen', 
				'data-placeholder' => 'Recipients', 
				'tabindex' => 1,
				'options' => array(
					'Groups' => $groups,
					'Individuals' => $userRecipients
				)
			));
		echo $this->Form->input('Conversation.title', array('type' => 'text', 'label' => 'Subject', 'class' =>'required'));
		echo $this->Tinymce->input('ConversationMessage.0.message', array(
            'label' => 'Content'
            ),array(
                'language'=>'en'
            ),
            ''
        ); 
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
