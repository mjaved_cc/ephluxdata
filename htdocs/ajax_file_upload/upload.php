<?php

require_once('image_resize.php');

class UploadHandler {

	private $_options;

	function __construct() {

		$this->_options['upload_dir'] = "uploads/";
		$this->_options['thumbnails_dir'] = "thumbnail/";
		$this->_options['thumbnail_max_width'] = 80;
		$this->_options['thumbnail_max_height'] = 80;
		$this->_options['image_quality'] = 100;
	}

	public function upload($file_name, $fileTmpLoc) {



		$new_file_name = $this->get_unique_name($file_name);

		$this->_options['upload_dir'].= $new_file_name;
		$this->_options['thumbnails_dir'].= $new_file_name;



		$moveResult = move_uploaded_file($fileTmpLoc, $this->_options['upload_dir']);


		$resizeObj = new resize($this->_options['upload_dir']);

		$flag = $resizeObj->resizeImage($this->_options['thumbnail_max_width'], $this->_options['thumbnail_max_height'], 'crop');
		$is_saved = $resizeObj->saveImage($this->_options['thumbnails_dir'], $this->_options['image_quality']);
		$success = $flag && $is_saved;

		return $this->_options['thumbnails_dir'];
	}

	public function get_unique_name($file_name) {

		$random = md5(time());

		$extension = pathinfo($file_name, PATHINFO_EXTENSION);
		$new_file_name = md5($filename) . $random . '.' . $extension;
		return $new_file_name;
	}

}

?>