<html>
    <head>
        <title>Mouse Coordinates</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>

       

        <script>
            $(document).ready(function() {
                $(document).mousedown(function() {
                    $(this).data('mousedown', true);
                });
                $(document).mouseup(function() {
                    $(this).data('mousedown', false);
                });
    
                $(document).mousemove(function(e) {
                    if($(this).data('mousedown')) {
                        $('body').text('X: ' + e.pageX + ', Y: ' + e.pageY);
                    }
                });
            });
        </script>
    </head>
    <body>

    </body>
</html>