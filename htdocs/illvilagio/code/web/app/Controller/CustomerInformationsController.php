<?php
App::uses('AppController', 'Controller');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class CustomerInformationsController extends AppController {

	 public $uses = array('CustomerInformation','FeedBack');
	
	 
	function index(){
		$this->layout = 'admin';
		$this->set('customer', $this->FeedBack->get_customerinfo());
//		 $this->FeedBack->recursive = 0;
//        $this->set('customer', $this->paginate());
		
	}
	
	function view($id){
		$this->layout = 'admin';
		$this->set('customer', $this->CustomerInformation->findById($id));
	}
	
	function add(){
		$this->layout = 'admin';
	}
	function edit(){
		$this->layout = 'admin';
	}
	function delete($id){
		$this->layout = 'admin';
		$this->CustomerInformation->delete_customer($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index'));
	}
}
