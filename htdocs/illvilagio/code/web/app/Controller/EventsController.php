<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class EventsController extends AppController {

	public $components = array('ApiEvent', 'RequestHandler');

	function beforeFilter() {



		parent::beforeFilter();
		$this->Auth->allowedActions = array('upload');
	}

	function success() {
		die('Success');
	}

	function get_event() {
		$result = $this->ApiEvent->get_event();

		$this->set(compact('result'));
		//$this->render('/Event/json/get_event');
	}

//	public function view($id = null) {
//		$this->Event->id = $id;
//		if (!$this->Event->exists()) {
//			throw new NotFoundException(__('Invalid event'));
//		}
//
//		debug($this->Event->read(null, $id));
//		exit;
//		$this->set('user', $this->Event->read(null, $id));
//	}
//	

	function index() {
		$this->set('event', $this->Event->get_event_list());
		$this->layout = 'admin';

//		$this->Event->recursive = 0;
//        $this->set('event', $this->paginate());
	}

	function view($id) {
		$this->set('event', $this->Event->findById($id));
		$this->set('image', $this->Event->get_image_event_id($id));
		//$_image = $this->Event->get_image_event_id($id);
		//print_r($_image);
		//exit;
		$this->layout = 'admin';
	}

	function add() {
		$this->layout = 'admin';
		if ($this->request->is('post')) {

			//pr($this->request->data['Events']);exit;
			$udid = $this->Event->get_udid();
			$email = $this->Event->get_email();
//	pr ($udid['udids']['udid']);
			//pr($email[6]['customer_information']['email'] );





			echo '<span class="mesg-inserted">Record has been inserted!</span>';

			$result = $this->Event->add_event($this->request->data['Events']);

			//pr($result['id']);	
			//$oldfolderpath = $this->webroot.'app/uploads';
			//$newfolderpath = $this->webroot.'app/webroot/events/'.$result['id'];
			//rename($oldfolderpath,$newfolderpath);
			mkdir('events/' . $result['id'], 0777);

			//echo env('DOCUMENT_ROOT').''.$this->webroot.'app/uploads/';
			//$this->recurse_copy(env('DOCUMENT_ROOT').''.$this->webroot.'app/uploads/', env('DOCUMENT_ROOT').''.$this->webroot.'app/webroot/events/'.$result['id']);

			$folder1 = new Folder(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/uploads/');

			$folder1->copy(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/webroot/events/' . $result['id']);
			//pr($udid);

			$files = glob(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/uploads/*');

			foreach ($files as $file) {
				unlink($file);
			}

			//$this->emptyDir(env('DOCUMENT_ROOT').''.$this->webroot.'app/uploads');
			foreach ($udid as $udids) {
				//pr ($udids['udids']['udid']);
				App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
				PushNotificationIOS::send($udids['udids']['udid'], "New Event has been added");
			}
			//pr($email[4]['customer_information']['email']);exit;
			/* foreach ($email as $emails) {
			  App::uses('CakeEmail', 'Network/Email');
			  $customer_email= ($emails['customer_information']['emails']);
			  //pr($customer_email);

			  $Email = new CakeEmail();
			  $Email->from(array('shahshobo@gmail.com' => 'Illvillagio.com'))
			  ->to($customer_email)
			  ->subject('New Event')
			  ->send('Dear customer, A New Event Has been announced, Hurry Up for reservations');
			  //
			  } */
			//$this->redirect('index', null, true);
		}
	}

	function edit($id) {

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Event']['id'] = $id;
			//pr($this->request->data);
			//exit;


			$promoted_home = $this->Event->get_promoted_home();

			if ($this->request->data['Event']['promotedhome'] == 1 && $promoted_home >= 2) {

				echo '<span class="mesg-inserted">Could not promote to home. Record not inserted!</span>';
			} else {



				echo '<span class="mesg-inserted">Record has been inserted!</span>';

				$this->Event->edit_event($this->request->data['Event']);

				$files = glob(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/uploads/*');

				foreach ($files as $file) {
					unlink($file);

				}


		
			
			
			//$this->Event->event_image($file);
			
			
			$folder1 = new Folder(env('DOCUMENT_ROOT').''.$this->webroot.'app/uploads/');
			$folder1->copy(env('DOCUMENT_ROOT').''.$this->webroot.'app/webroot/events/'.$this->request->data['Event']['id']);
			
			$files = glob(env('DOCUMENT_ROOT').''.$this->webroot.'app/uploads/*');
			
			foreach ($files as $file) {
				unlink($file);
			}		


				/* if($this->request->data['Event']['images_old'] != ''){
				  foreach($this->request->data['Event']['images_old'] as $file) {
				  $event_id = $this->Event->event_image($file);
				  pr($event_id);
				  exit;
				  $files = glob(env('DOCUMENT_ROOT') . '' . $this->webroot .'app/webroot/events/'.$this->request->data['Event']['id'].'/'.$file);
				  unlink($files);
				  //pr($file);
				  //$this->Event->event_image($file);
				  }
				  } */


				//$this->Event->event_image($file);


				$folder1 = new Folder(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/uploads/');
				$folder1->copy(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/webroot/events/' . $this->request->data['Event']['id']);

				$files = glob(env('DOCUMENT_ROOT') . '' . $this->webroot . 'app/uploads/*');
			}
		}

		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->request->data = $this->Event->findById($id);
		$this->request->data['Event']['participate_description_arabic'] = $this->request->data['Event']['particapte_description_arabic'];
		$this->request->data['Event']['event_time'] = $this->request->data['Event']['event_time'];

		//pr($this->Event->findById($id));

		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}
		//$this->set('recipe', $this->Gallery->findById($id));		
		$item = $this->Event->find('list');

		$this->set('item', $item);
		$this->set('id', $id);
		$this->set('image', $this->Event->get_image_event_id($id));

		$this->layout = 'admin';
		//$this->redirect(array('action' => 'index'));
	}

	function check_exist() {
		// Define a destination
		$targetFolder = '/uploads'; // Relative to the root and should match the upload folder in the uploader script

		if (file_exists($_SERVER['DOCUMENT_ROOT'] . $targetFolder . '/' . $_POST['filename'])) {
			echo 1;
		} else {
			echo 0;
		}
	}

	function upload() {

		//ini_set('post_max_size', '10M');
		//ini_set('upload_max_filesize', '256MB');
		//php_value upload_max_filesize 256MB
		//php_value post_max_size 10MB

		$targetFolder = $this->webroot . 'app/uploads'; // Relative to the root
		//$verifyToken = md5('unique_salt' . $_POST['timestamp']);
		//if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
		$tempFile = $_FILES['Filedata']['tmp_name'];

		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
		$targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

		// Validate the file type
		$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
		$fileParts = pathinfo($_FILES['Filedata']['name']);

		if (in_array($fileParts['extension'], $fileTypes)) {
			move_uploaded_file($tempFile, $targetFile);
			echo '1';
		} else {
			echo 'Invalid file type.';
		}
		//}
	}

	function uploadify() {// Define a destination
		$targetFolder = '/uploads'; // Relative to the root

		$verifyToken = md5('unique_salt' . $_POST['timestamp']);

		if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
			$targetFile = rtrim($targetPath, '/') . '/' . $_FILES['Filedata']['name'];

			// Validate the file type
			$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);

			if (in_array($fileParts['extension'], $fileTypes)) {
				move_uploaded_file($tempFile, $targetFile);
				echo '1';
			} else {
				echo 'Invalid file type.';
			}
		}
	}

	function delete($id = null) {
		$this->layout = 'admin';
		$this->Event->delete_event($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect('index', null, true);
	}

	function recurse_copy($src, $dst) {

		$dir = opendir($src);
		$result = ($dir === false ? false : true);

		if ($result !== false) {
			$result = @mkdir($dst);

			if ($result === true) {
				while (false !== ( $file = readdir($dir))) {
					if (( $file != '.' ) && ( $file != '..' ) && $result) {
						if (is_dir($src . '/' . $file)) {
							$result = recurseCopy($src . '/' . $file, $dst . '/' . $file);
						} else {
							$result = copy($src . '/' . $file, $dst . '/' . $file);
						}
					}
				}
				closedir($dir);
			}
		}

		return $result;
	}

	function emptyDir($path) {

		// init the debug string
		$debugStr = '';
		$debugStr .= "Deleting Contents Of: $path<br /><br />";

		// parse the folder
		if ($handle = opendir($path)) {

			while (FALSE !== ($file = opendir($handle))) {

				if ($file != "." && $file != "..") {

					// if it's a file, delete it
					if (is_file($path . "/" . $file)) {

						if (unlink($path . "/" . $file)) {
							$debugStr .= "Deleted File: " . $file . "<br />";
						}
					} else {

						// It's a directory...
						// crawl through the directory and delete the contents
						if ($handle2 = opendir($path . "/" . $file)) {

							while (FALSE !== ($file2 = opendir($handle2))) {

								if ($file2 != "." && $file2 != "..") {
									if (unlink($path . "/" . $file . "/" . $file2)) {
										$debugStr .= "Deleted File: $file/$file2<br />";
									}
								}
							}
						}

						if (rmdir($path . "/" . $file)) {
							$debugStr .= "Directory: " . $file . "<br />";
						}
					}
				}
			}
		}
		return $debugStr;
	}

}
