<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class MenusController extends AppController {

	public $name = 'Menu';
	public $components = array('ApiMenu');
	public $uses = array('Menu');

	function beforeFilter() {
		parent::beforeFilter('add_menu_items');
		$this->Auth->allow();
	}

	function success() {
		die('Success');
	}

	function add_menu_items() {
		$data = array();


		if ($this->request->data) {
			$this->Menu->set($this->request->data);
			if ($this->Menu->validates(array('fieldList' => array('name', 'displayOrder', 'status')))) {
				//pr($this->request->params['form']);
				/*
				  if ($this->request->params['form']) {
				  $images_arr = array();
				  pr($this->request->params['form']);
				  //pr(print_r($_POST));

				  foreach ($this->request->params['form']['imageUrl']['name'] as $key => $images) {
				  //echo"$key<br/>";
				  $images_arr[$key] = $this->save_multiple_images($this->request->params['form']['imageUrl'], IMAGE_MENU, $key);
				  }
				  if(!empty($images_arr)){

				  $data['imageUrl']= $this->App->get_db_save_json($images_arr);
				  //	pr($data['imageUrl']);
				  //	pr($images_arr); die;
				  }

				  } */


				//	pr($this->request->params['form']['imageUrl']); die;
				//	if (is_uploaded_file($this->request->params['form']['imageUrl'])) {

				$data['imageUrl'] = $this->saveToFile($this->request->params['form']['imageUrl'], IMAGE_MENU); //file url
				//	}

				//echo $data['imageUrl'];exit;


				$data['name'] = $this->request->data['name'];
				$data['displayOrder'] = $this->request->data['displayOrder'];
				$data['status'] = $this->request->data['status'];
				$data['created'] = $this->App->get_current_datetime();



				if ($this->Menu->create($data)) {

					//	pr($this->Menu->create($data));
					$this->Session->setFlash(__('The Menu has been saved'));
					//$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Menu could not been saved'));
				}
			}


			/* 	$this->Menu->create();
			  $this->Menu->set('name', $this->request->data['name']);
			  $this->Menu->set('imageURL', $this->request->data['imageUrl']);
			  $this->Menu->set('displayOrder', $this->request->data['displayOrder']);
			  $this->Menu->set('status', $this->request->data['status']);
			  if($this->Menu->save()){

			  echo 'data inserted successfully';

			  } */
		} else {
			$this->Session->setFlash(__('Nothing posted'));
			//pr($this->request->data);
		}
	}

//	function get_all_menus() {
//		$result = $this->ApiMenu->get_all_menus();
//
//		$this->set(compact('result'));		
//	}
//
//	function get_status_active_menus() {
//		$result = $this->ApiMenu->get_status_active_menus();
//
//		$this->set(compact('result'));
//		$this->render('/Menu/get_all_menus');
//	}


	function get_all_menus() {
		$result = $this->ApiMenu->get_all_menus();
		debug(json_decode($result, true));
		exit;
		$this->set(compact('result'));
		$this->render('/Menu/json/get_all_menus');
	}

	function view() {
		$this->layout = 'admin';
		//$data = $this->ApiMenu->get_menu("2013-06-14 08:51:04");
		$result = $this->Menu->get_menu_treeview("2013-06-14 08:51:04");
		//pr($data);exit();
		//$result = $this->App->decoding_json($data);
		//debug($view_tree);exit();
		$this->set(compact('result'));
	}

	function index() {
		$this->layout = 'admin';
		$time = '';
		//$result = $this->Menu->get_menu_category_list();
		$this->set('menu', $this->Menu->get_menu_category_list());

		//$this->set('msisdn', "");
//		$this->Menu->recursive = 0;
//		$this->set('menu', $this->paginate());
	}

	function index_item() {
		$this->layout = 'admin';
		$time = '';
		$this->set('menus', $this->Menu->get_item_list());
		//$this->set('msisdn', "");
//        $this->Menu->recursive = 0;
//        $this->set('menus', $this->paginate());
	}

	function add() {
		if ($this->request->is('post')) {
			$image_path = $this->saveToFile($this->request->data['Menu']['filename'], 'menu');
			$this->Menu->add_menu($this->request->data['Menu'], $this->request->data['Menu']['filename']);
			$this->Menu->menu_parent_date_modify($this->request->data['Menu']['parent_id']);
			echo '<span class="mesg-inserted">Record has been inserted!</span>';
			//pr($this->request->data);		
			//exit;
		}
		$this->layout = 'admin';
		$item = $this->Menu->find('list');
		$this->set('item', $item);
	}

	function edit( $id ) {


		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Menu']['id'] = $id;
			//pr($this->request->data);
			//exit;

			if ( !empty($this->request->data['Menu']['filename']['name']) ) { 
				$image_path = $this->saveToFile($this->request->data['Menu']['filename'], 'menu');
			}
			
			$this->Menu->edit_menu($this->request->data['Menu'], $this->request->data['Menu']['filename']);
			echo 'Menu has been edited!';
			$this->redirect(array('action'=>'index'));
		}

		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->request->data = $this->Menu->findById($id);
		//pr($this->Menu->findById($id));
		$this->request->data['Menu']['name_arabic'] = $this->request->data['Menu']['nameArabic'];
		$this->request->data['Menu']['Display_Order'] = $this->request->data['Menu']['displayOrder'];
		$this->request->data['Menu']['description_arabic'] = $this->request->data['Menu']['descriptionArabic'];

		$this->set('parent_id', $this->request->data['Menu']['parentId']);
		
		$image_url  = $this->Menu->get_menuCategoryList_by__id( $id );
		$this->set('image_url', $image_url);

		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}



		//$this->set('recipe', $this->Gallery->findById($id));

		$this->layout = 'admin';
		$item = $this->Menu->find('list', array(
			'conditions' => array('is_item =' => '0')
				));
		$this->set('item', $item);
	
	}

	function edit_item($id) {

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Menu']['id'] = $id;
			//pr($this->request->data['Menu']);
			//exit;
			$image_path = $this->saveToFile($this->request->data['Menu']['filename'], 'menu');
			$this->Menu->edit_menu($this->request->data['Menu'], $this->request->data['Menu']['filename']);
			$this->Menu->menu_parent_date_modify($this->request->data['Menu']['parent_id']);
			echo 'Menu has been edited!';
		}

		$this->request->data = $this->Menu->findById($id);
		//$this->request->data = $data;
		//$new =array();
		//pr($this->request->data); 

		$this->set('parent_id', $this->request->data['Menu']['parentId']);

		$this->request->data['Menu']['parent_id'] = $this->request->data['Menu']['parentId'];
		$this->request->data['Menu']['description_arabic'] = $this->request->data['Menu']['descriptionArabic'];
		;
		$this->request->data['Menu']['name_arabic'] = $this->request->data['Menu']['nameArabic'];
		;
		$this->request->data['Menu']['Display_Order'] = $this->request->data['Menu']['displayOrder'];
		;

		//$new['Menu'][]		
		//pr($this->Menu->findById($id));		

		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}


		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->layout = 'admin';
		$item = $this->Menu->find('list');
		$this->set('item', $item);
	}

	function add_item() {
		if ($this->request->is('post')) { 
			$image_path = $this->saveToFile($this->request->data['Menu']['filename'], 'menu');
			$this->Menu->add_menu($this->request->data['Menu'], $this->request->data['Menu']['filename']);
			$this->Menu->menu_parent_date_modify($this->request->data['Menu']['parent_id']);
			echo '<span class="mesg-inserted">Record has been inserted!</span>';
			//pr($this->request->data);
			//exit;
		}
		$this->layout = 'admin';
		$item = $this->Menu->find('list');
		$this->set(compact('item'));
	}

	function saveToFile($file, $path) {
		$info = pathinfo($file['name']); // split filename and extension
		$saveName = $info['basename'];
		$savePath = WWW_ROOT . $path . DS . $saveName;

		move_uploaded_file($file['tmp_name'], $savePath);
		$path = FULL_BASE_URL . Router::url('/') . 'app/webroot' . DS . $path . DS . $saveName;
		return $path;
	}

	function view_item($id) {
		$this->layout = 'admin';
		$this->set('menu', $this->Menu->get_menu_item_by_id($id));
	}

	function view_category($id) {
		$this->layout = 'admin';
		$this->set('menu', $this->Menu->get_menuCategoryList_by__id($id));
	}

	function delete($id = null) {
		$this->layout = 'admin';
		$this->Menu->delete_menu($id);
		$this->Session->setFlash('The Item with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index'));
	}

	function delete_category($id = null) {

		$this->layout = 'admin';
		$this->Menu->delete_menu($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index_item'));
	}

}

