<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class FeedbacksController extends AppController {
    
    //public $name = 'FeedBacks';
    public $uses = array('FeedBack','CustomerInformation');
	public $components = array('ApiFeedBack');
	
	function beforeFilter()
	{
		$this->Auth->allow();	
	} 

   

    function success() {
    	die('Success');
    }
	
    function add_feedback() 
	{
	
		$check = $this->FeedBack->set($this->request->data);
		
		if ( $this->FeedBack->validates($this->request->data) )  {
			//pr($this->request->data);die;
			if( !empty($this->request->data) ){
				//$this->FeedBack->customerID = $this->request->data['customer_id'];
				$this->FeedBack->customerName = $this->request->data['name'];
				$this->FeedBack->telephoneNo = $this->request->data['telephone_no'];
				$this->FeedBack->Address  = $this->request->data['address'];
				$this->FeedBack->feedBackText = $this->request->data['feedback_text'];
				$this->FeedBack->email = $this->request->data['emailaddress'];
				$this->FeedBack->create_date_time = $this->request->data['create_date_time'];

				//pr($this->request->data);die;exit;
				$result = $this->ApiFeedBack->add_feedback();
				$this->set(compact('result'));
			}
			else{
				$result = '{"status":"0", "error":"Please Specify Input Parameters"}';
				$this->set(compact('result'));
			}
		
		} else {
			// didn't validate logic
			debug($this->FeedBack->validationErrors) ;
		}		
    }
    
	function index(){
		
		$this->set('feedback', $this->FeedBack->get_feedback());
		$this->layout = 'admin';
	}
	
	function view($feedback_id){
		$this->layout = 'admin';
		$this->set('feedback', $this->FeedBack->findById($feedback_id));
		//$feedback_result =$this->FeedBack->get_feedback();
		
		//$this->set('feedback',$feedback_result );
		$result =$this->FeedBack->get_customerinfo($feedback_id);
		$this->set('customer',$result[0] );
		//$result=$this->FeedBack->get_customerinfo();
		//$this->set('customer', $this->FeedBack->get_customerinfo());
		
		
	}
	
	function add(){
		$this->layout = 'admin';
	}
	function edit(){
		$this->layout = 'admin';
	}
	
	function delete($id = null) { 
		$this->layout = 'admin';
		$this->FeedBack->delete_feedback($id);
	
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.'); 
		$this->redirect(array('action' => 'index'));
	}

}
?>
