<?php

class DashboardsController extends AppController {

	function index() {
		/*
		App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
		print_r(PushNotificationIOS::send('d4da2a905b9c992787d99f9400436402ac93dbb7b0c09035a642695bd3b510f2', "Han bhai, Penalty Lunch ??"));
		 */
		$this->layout = 'admin';
		$this->set('event', $this->Dashboard->get_upcoming_events());
		$this->set('reservations', $this->Dashboard->get_pending_reservations());
		$this->set('todayorder', $this->Dashboard->get_today_order());
		$this->set('tomorder', $this->Dashboard->get_tomorrow_order());
		$this->set('yesorder', $this->Dashboard->get_yesterday_order());
		$this->set('todayreservation', $this->Dashboard->get_today_reservation());
		$this->set('tomreservation', $this->Dashboard->get_tomorrow_reservation());
		$this->set('upreservation', $this->Dashboard->get_upcoming_reservations());
		$this->set('dinein', $this->Dashboard->get_today_dinein());
		$this->set('takeaway', $this->Dashboard->get_today_takeaway());
		$this->set('delivery', $this->Dashboard->get_today_delivery());
		
		
	}

}