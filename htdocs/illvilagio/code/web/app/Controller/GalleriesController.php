<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class GalleriesController extends AppController {

	public $name = 'Galleries';
	public $uses = array('Gallery');
	public $components = array('ApiGallery');

	//var $helpers = array('Validation');
//	function index() {
//		die('Welcome');
//	}

	function success() {
		die('Success');
	}

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	function getGalleryInfo() {
		//function get_gallery_info() {
		//$result = $this->Gallery->getGalleryInfo();
		//$result = $this->ApiGallery->getGalleryInfo();


		$this->set(compact('result'));
		//$this->render('/Gallery/json/getGalleryInfo');
	}

	function add_gallery() {
		$result = $this->Gallery->insert();
		$this->redirect('/Gallery/getGalleryInfo.json');
	}

	function index() {
		$lang_name = '';
		$modified_date = '';

		$this->set('gallery', $this->Gallery->getGalleryInfo($lang_name, $modified_date));
		//$this->Gallery->recursive = 0;
		//$this->set('gallery', $this->paginate());
		$this->layout = 'admin';
	}

	function view($id) {
		$this->layout = 'admin';
		$this->set('gallery', $this->Gallery->findById($id));
	}

	function add() {

		if ($this->request->is('post')) {
			//pr($this->request->data);
			//exit;
			echo '<span class="mesg-inserted">Record has been inserted!</span>';
			$image_path = $this->saveToFile($this->request->data['Gallery']['filename'], 'gallery');
			$this->Gallery->add_galleryInfo($this->request->data['Gallery'], $this->request->data['Gallery']['filename']);
		}


		$this->layout = 'admin';
		
	}

	function edit($id) {

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Gallery']['id'] = $id;

			//pr($this->request->data['Gallery']['filename']);
			//exit;
			if (!empty($this->request->data['Gallery']['filename']['name'])) { 
				$image_path = $this->saveToFile($this->request->data['Gallery']['filename'], 'gallery');
			}
			
			
			
			if( empty($this->request->data['Gallery']['filename']['name'] )){
				$item = $this->Gallery->get_image_gallery_id( $id ); 
				$this->request->data['Gallery']['filename']['name'] =  $item[0]['gallery']['image_name'];
				
			}

			$this->Gallery->edit_galleryInfo($this->request->data['Gallery'], $this->request->data['Gallery']['filename']);
			echo 'Records has been edited now!';
			$this->redirect(array('action'=>'index'));
		}


		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->request->data = $this->Gallery->findById($id);
		$this->request->data['Gallery']['caption'] = $this->request->data['Gallery']['image_caption'];
		//pr($this->Gallery->findById($id));
		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}



		//$this->set('recipe', $this->Gallery->findById($id));
		$this->layout = 'admin';
	}

	function saveToFile($file, $path) {
		$info = pathinfo($file['name']); // split filename and extension
		$saveName = $info['basename'];
		$savePath = WWW_ROOT . $path . DS . $saveName;

		move_uploaded_file($file['tmp_name'], $savePath);
		$path = FULL_BASE_URL . Router::url('/') . 'app/webroot' . DS . $path . DS . $saveName;
		return $path;
	}

	function delete($id = null) {
		$this->layout = 'admin';
		$this->Gallery->delete_galleryInfo($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index'));
		
	}

}

?>
       