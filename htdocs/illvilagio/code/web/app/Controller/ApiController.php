<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ApiController extends AppController {

	public $components = array('ApiReservation', 'ApiEvent', 'App', 'ApiContent', 'ApiGallery', 'ApiFeedBack', 'ApiOrder', 'ApiAllInformation', 'ApiCustomer', 'ApiMenu', 'ApiHome', 'ApiUdid');
	public $name = 'Api';
	public $uses = array('Event', 'Reservation', 'Content', 'Gallery', 'FeedBack', 'Order', 'AllInformation', 'Customer', 'Menu', 'Udid');

	function beforeFilter() {

		if ($this->request->is('post')) {
			$posted_params = $this->data;
		} elseif ($this->request->is('get')) {
			$posted_params = $this->request->query;
			//debug($this->params);die();
		}

		$this->App->get_validate_signature_response($posted_params);

		$this->Auth->allow();
	}

	/**
	 * Reservation
	 * @method add
	 * @method get
	 * @method edit
	 * @method delete
	 */
	function reservation_add() {

//print_r($this->request->data);
//die();
//		$reservation_data = array(
//			'no_of_children' => '3',
//			'no_of_oldage' => '4',
//			'confirm_via_email' => '0',
//			'email_address' => 'sadieee@hotmail.com',
//			'phone_no' => '123456',
//			'created' => '2013-06-14 17:36:08',
//			'no_of_guest' => '45',
//			'time' => '2013-06-14 17:36:08',
//			'event_id' => '1',
//			'floor_id' => '1',
//			'table_id' => '1'
//		); 
		//print_r($this->request->data);
		if ($this->request->is('post') && !empty($this->request->data)) {

			$reservation_posted_info = $this->request->data;

			$data = $this->ApiReservation->add_reservation($reservation_posted_info);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	function reservation_edit() {

		//print_r($this->request->data);
		//die();
		//		$reservation_data = array(
		//			'no_of_children' => '3',
		//			'no_of_oldage' => '4',
		//			'confirm_via_email' => '0',
		//			'email_address' => 'sadieee@hotmail.com',
		//			'phone_no' => '123456',
		//			'created' => '2013-06-14 17:36:08',
		//			'no_of_guest' => '45',
		//			'time' => '2013-06-14 17:36:08',
		//			'event_id' => '1',
		//			'floor_id' => '1',
		//			'table_id' => '1'
		//		);

		if ($this->request->is('post') && !empty($this->request->data)) {

			$reservation_posted_info = $this->request->data;
			$data = $this->ApiReservation->edit_reservation($reservation_posted_info);
			$this->set(compact('data'));
		}

		$this->render('render_json');
	}

	function reservation_get() {
		$data = $this->ApiReservation->get_reservation();
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function reservation_delete() {

		if ($this->request->is('post') && !empty($this->request->data)) {

			$data = $this->ApiReservation->delete_reservation($this->request->data);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	/**
	 * Event
	 * @method add
	 * @method get
	 * @method edit
	 * @method delete
	 */
	function event_get() {
		//echo $_GET['lang'];
		//die();
		if (!empty($_GET['lang'])) {
			$lang_name = $_GET['lang'];
		} else {
			$lang_name = '';
		}

		if (!empty($_GET['modified_date'])) {
			$modified_date = $_GET['modified_date'];
		} else {
			$modified_date = '';
		}
		$is_deal = 0;
		$data = $this->ApiEvent->get_event($lang_name, $modified_date, $is_deal);
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function event_add() {

		if ($this->request->is('post') && !empty($this->request->data)) {

			$add_event_info = $this->request->data;
			print_r($_FILES);
			exit;
			$data = $this->ApiEvent->add_event($add_event_info, $_FILES);
		}

		$this->set(compact('data'));
		$this->render('render_json');
	}

	function event_edit() {

		if ($this->request->is('post') && !empty($this->request->data)) {

			$edit_event_info = $this->request->data;
			//print_r($add_content_info);
			//exit;
			$data = $this->ApiEvent->edit_event($edit_event_info, $_FILES);
		}

		$this->set(compact('data'));
		$this->render('render_json');
	}

	function event_delete() {

		if ($this->request->is('post') && !empty($this->request->data)) {

			$delete_event = $this->request->data;
			//print_r($delete_event);
			//exit;
			$data = $this->ApiEvent->delete_event($delete_event);
		}

		$this->set(compact('data'));
		$this->render('render_json');
	}

	/*	 * *********************event end*************************** */

	/**
	 * Content
	 * @method add
	 * @method get
	 * @method edit
	 * @method delete
	 */
	function content_get() {

		if (!empty($_GET['lang'])) {
			$lang_name = $_GET['lang'];
		} else {
			$lang_name = '';
		}

		if (!empty($_GET['modified_date'])) {
			$modified_date = $_GET['modified_date'];
		} else {
			$modified_date = '';
		}

		//echo $orderId = $this->data['orderid'];
		//$content_key = '';
		//print_r($modified_date);
		//exit;

		$data = $this->ApiContent->get_content_by_key($lang_name, $modified_date);

		$this->set(compact('data'));
		$this->render('render_json');
	}

	function content_add() {

		if ($this->request->is('post') && !empty($this->request->data)) {

			$add_content_info = $this->request->data;
			//print_r($add_content_info);
			//exit;
			$data = $this->ApiContent->add_content($add_content_info);
		}



		$this->set(compact('data'));
		$this->render('render_json');
	}

	function content_edit() {
		if ($this->request->is('post') && !empty($this->request->data)) {
			//print_r($this->request->data);
			//exit;
			$edit_content_info = $this->request->data;
			//print_r($add_content_info);
			//exit;
			$data = $this->ApiContent->edit_content($edit_content_info);
		}


		$this->set(compact('data'));
		$this->render('render_json');
	}

	function content_delete() {
		if ($this->request->is('post') && !empty($this->request->data)) {
			//print_r($this->request->data);
			//exit;
			$content_id = $this->request->data;
			//print_r($add_content_info);
			//exit;
			$data = $this->ApiContent->delete_content($content_id);
		}


		$this->set(compact('data'));
		$this->render('render_json');
	}

	/**
	 * Gallery
	 * @method add
	 * @method get
	 * @method edit
	 * @method delete
	 */
	function gallery_get() {
		if (!empty($_GET['lang'])) {
			$lang_name = $_GET['lang'];
		} else {
			$lang_name = '';
		}

		if (!empty($_GET['modified_date'])) {
			$modified_date = $_GET['modified_date'];
		} else {
			$modified_date = '';
		}


		$data = $this->ApiGallery->getGalleryInfo($lang_name, $modified_date);

		$this->set(compact('data'));
		$this->render('render_json');
	}

	function gallery_add() {
		//print_r($this->data);
		//print_r($_FILES['file']);
		//print $this->data["file"]["type"];
		//exit;
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiGallery->add_galleryInfo($this->data, $_FILES['file']);
		}
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function gallery_edit() {
		//print_r($this->data);
		//print_r($_FILES['file']);
		//print $this->data["file"]["type"];
		//exit;
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiGallery->edit_galleryInfo($this->data, $_FILES['file']);
		}
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function gallery_delete() {
		//print_r($this->data);
		//print_r($_FILES['file']);
		//print $this->data["file"]["type"];
		//exit;
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiGallery->delete_galleryInfo($this->data);
		}
		$this->set(compact('data'));
		$this->render('render_json');
	}

	/**
	 * Feedback
	 * @method add
	 * @method get
	 */
	function feedback_add() {

		//print_r($this->request->data);
		//die();
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiFeedBack->add_feedback($this->request->data);
		}
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function feedback_get() {

		//print_r($this->request->data);
		//die();
		$data = $this->ApiFeedBack->get_feedback();
		$this->set(compact('data'));
		$this->render('render_json');
	}

	/**
	 * Order
	 * @method add
	 * @method delete
	 */
	function order_add() {


		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiOrder->makeOrder($this->request->data);
			//print_r($this->request->data);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	function order_delete() {

		//print_r($this->request->data);
		//exit;
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiOrder->delete_order($this->request->data);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	/**
	 * All information
	 * @method get
	 */
	function allinformation_get() {

		if (!empty($_GET['lang'])) {
			$lang_name = $_GET['lang'];
		} else {
			$lang_name = 'en';
		}

		if (!empty($_GET['modified_date'])) {
			$modified_date = $_GET['modified_date'];
		} else {
			$modified_date = '';
		}

		$data = array();
		$data = $this->ApiAllInformation->AllInformation($lang_name, $modified_date);

		$this->set(compact('data'));
		$this->render('render_json');
	}

	/**
	 * Customer
	 * @method get
	 */
	function customer_get() {

		$data = $this->ApiCustomer->get_customer();
		$this->set(compact('data'));

		$this->render('render_json');
	}

	/**
	 * Menu
	 * @method add
	 * @method get
	 */
	function menu_add() {

		//print_r($this->request->data);
		//exit;
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiMenu->add_menu($this->request->data, $_FILES['file']);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	function menu_edit() {
		if ($this->request->is('post') && !empty($this->request->data)) {
			if (!empty($_FILES['file'])) {
				$data = $this->ApiMenu->edit_menu($this->request->data, $_FILES['file']);
				$this->set(compact('data'));
			}
		}
		$this->render('render_json');
	}

	function menu_delete() {
		if ($this->request->is('post') && !empty($this->request->data)) {
			$data = $this->ApiMenu->delete_menu($this->request->data);
			$this->set(compact('data'));
		}
		$this->render('render_json');
	}

	function menu_get() {

		if (!empty($_GET['time'])) {
			$time = $_GET['time'];
		} else {
			$time = '';
		}


		$data = $this->ApiMenu->get_menu($time);
		$this->set(compact('data'));
		$this->render('render_json');
	}

	function home_get() {

		if (!empty($_GET['lang'])) {
			$lang_name = $_GET['lang'];
		} else {
			$lang_name = '';
		}

		$data = $this->ApiHome->get_home($lang_name);
		$this->set(compact('data'));
		$this->render('render_json');

		//print_r($data);
		//exit;
	}

	function udid_add() {



		if ($this->request->is('post') && !empty($this->request->data)) {

			$id = $this->request->data['udid'];
			$version = $this->request->data['version'];

			if (!empty($id) && !empty($version)) {



				$data = $this->ApiUdid->setUdid($id, $version);
				$this->set(compact('data'));
				$this->render('render_json');
			}


//			$is_exist = $this->Udid->is_id_exists( $id ); 
//			
//			
//			
//			if( empty($is_exist )){
//				
//				
//				$this->Udid->save_udid( $id );
//			}
		}
	}

	function notification_add() {

		// Put your device token here (without spaces):


		$deviceToken = 'd4da2a905b9c992787d99f9400436402ac93dbb7b0c09035a642695bd3b510f2';

// Put your private key's passphrase here:
		$passphrase = '1234';

// Put your alert message here:
		$message = 'My first push notification!';

////////////////////////////////////////////////////////////////////////////////

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
		$fp = stream_socket_client(
				'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
		);

// Encode the payload as JSON
		$payload = json_encode($body);

// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
		fclose($fp);
	}

}