<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class OrdersController extends AppController {

	public $name = 'Orders';
	public $uses = array('Order', 'Item', 'CustomerInformation');
	public $components = array('ApiOrder');

	function beforeFilter() {
		$this->Auth->allow();
	}

	/* function success() {
	  die('Success');
	  } */

	function makeOrder() {


		$arrayData = $this->request->data;

		//print_r($arrayData);
		//exit;
//		$arrayData = array(
//	'orderType' => 'dinein',
//    'status' => 1,
//    'TableId' => 1,
//    'FloorId' => 1,
//    'version' => 1.0,
//    'entryDateTime' => '2013-06-19  05:48:43',
//    
//    'customerDetail' => array(
//            'name' => 'Syed Sami',
//            'entryDateTime' => '2013-06-19  05:48:43',
//            'telephoneNo' => 0333373075,
//            'email' => 'syed.sami@ephlux.com',
//            'shippingAddress' => 'Suit 414'
//        ),
//
//    'items' => array(
//            '0' => array(
//                    'entryDateTime' => '2013-06-19 05:10:17',
//                    'itemId' => 5,
//                    'price' => 22,
//                    'comment' => 'Click Here For Comments.',
//                    'quantity' => 1,
//                    'active' => 1,
//                    'seatNo' => 5
//                ),
//
//            '1' => array(
//                    'entryDateTime' => '2013-06-18 03:22:04',
//                    'itemId' => 6,
//                    'price' => 22,
//                    'comment' => 'Click Here For Comments.',
//                    'quantity' => 1,
//                    'active' => 1,
//                    'seatNo' => 5
//                ),
//
//            '2' => array(
//                    'entryDateTime' => '2013-06-19 05:10:19',
//                    'itemId' => 7,
//                    'price' => 22,
//                    'comment' => 'Click Here For Comments.',
//                    'quantity' => 1,
//                    'active' => 1,
//                    'seatNo' => 5
//                ),
//
//            '3' => array(
//                    'entryDateTime' => '2013-06-19 05:10:16',
//                    'itemId' => 8,
//                    'price' => 22,
//                    'comment' => 'Click Here For Comments.',
//                    'quantity' => 4,
//                    'active' => 1,
//                    'seatNo' => 5
//                ),
//
//            '4' => array(
//                    'entryDateTime' => '2013-06-18 03:22:03',
//                    'itemId' => 9,
//                    'price' => 22,
//                    'comment' => 'Click Here For Comments.',
//                    'quantity' => 1,
//                    'active' => 1,
//                    'seatNo' => 5
//                )
//
//        )  
//);

		if ($arrayData['status'] != null) {

			//pr($arrayData);die;

			$this->Order->order_type = $arrayData['orderType'];
			$this->Order->order_table_id = $arrayData['TableId'];
			$this->Order->order_items = array($arrayData['items']);
			$this->Order->customer_details = array($arrayData['customerDetail']);

			$this->Order->order_entry_date_time = $arrayData['entryDateTime'];

			$result = $this->ApiOrder->makeOrder();
			$this->set(compact('result'));
		} else {
			$result = '{"status":"0", "error":"Please Specify Input Parameters"}';
			$this->set(compact('result'));
		}


		//pr($myArray);
		//die;
//				$this->set(compact('result'));
//		$inputsArray = json_decode($this->request->data);
//		print_r($inputsArray);die;		
		/* 		echo "status".$inputArray->['status']."<br/>";
		  echo "tableId".$inputArray->['TableId'];die
		 */


//		$check = $this->Order->set($this->request->data);
//		if ( $this->Order->validates($this->request->data) ) {
//			//pr($this->request->data);die;
//			if( !empty($this->request->data) ){
//				$this->Order->order_type = $this->request->data['order_type'];
//				$this->Order->order_item_id = $this->request->data['order_item_id'];
//				$this->Order->order_item_quantity = $this->request->data['order_item_quantity'];
//				$this->Order->order_table_id  = $this->request->data['order_table_id'];
//				$this->Order->order_seat_no = $this->request->data['order_seat_no'];
//				$this->Order->order_amount = $this->request->data['order_amount'];
//				$this->Order->order_amount_to_be_paid = $this->request->data['order_amount_to_be_paid'];
//
//				//pr($this->request->data);die;exit;
//				$result = $this->ApiOrder->makeOrder();
//				$this->set(compact('result'));
//			}
//			else{
//				$result = '{"status":"0", "error":"Please Specify Input Parameters"}';
//				$this->set(compact('result'));
//			}
//		
//		} else {
//			// didn't validate logic
//			debug($this->Order->validationErrors);
//		}		
	}

	function updateOrder() {
		$result = $this->ApiOrder->updateOrder();
		$this->set(compact('result'));
	}

	function index() {
		$this->layout = 'admin';

		$this->set('orders', $this->Order->get_order());
		
		//$this->set('order', $this->Order->findByid($id));
		// $this->Order->recursive = 0;
		//$this->set('order', $this->paginate());
	}
	
	
	
	function invoice($id){
		$this->layout = 'admin';
		$order_id = $this->Order->findByid($id);

		$this->set('order', $this->Order->findByid($id));
		$this->set('info', $this->CustomerInformation->findByid($order_id['Order']['customer_id']));



		$this->set('orders', $this->Order->view_order_items($id));
		$result = $this->Order->view_table_floor($id);

		$this->set('floor', $this->Order->view_table_floor($id));
	}

	function view($id) {
		$this->layout = 'admin';
		$order_id = $this->Order->findByid($id);

		$this->set('order', $this->Order->findByid($id));
		$this->set('info', $this->CustomerInformation->findByid($order_id['Order']['customer_id']));



		$this->set('orders', $this->Order->view_order_items($id));
		$result = $this->Order->view_table_floor($id);

		$this->set('floor', $this->Order->view_table_floor($id));
	}

	function add() {

		if ($this->request->is('post')) {
			//pr($this->request->data);

			$result = $this->request->data;

		

				$array = array($result['Order']['table_id'], $result['Order']['table_id1'], $result['Order']['table_id2'], $result['Order']['table_id3'], $result['Order']['table_id4'], $result['Order']['table_id5'], $result['Order']['table_id6'], $result['Order']['table_id7'], $result['Order']['table_id8'], $result['Order']['table_id9'], $result['Order']['table_id10'], $result['Order']['table_id11'], $result['Order']['table_id12'], $result['Order']['table_id13'], $result['Order']['table_id14'], $result['Order']['table_id15'], $result['Order']['table_id16'], $result['Order']['table_id17'], $result['Order']['table_id18'], $result['Order']['table_id19'],);
				$comma_separated = implode(",", $array);

				$arr = array_diff($array, array("0"));
				$comma_separated = implode(",", $arr);

				//pr($comma_separated);
				$this->request->data['Order']['table_id'] = $comma_separated;
				$this->request->data['Order']['is_notification'] = 0;

				//exit();

				$this->ApiOrder->makeOrder($this->request->data);
				echo 'Your order has been sent!';
			
		}
		$this->layout = 'admin';
		$item = $this->Item->find('list', array('fields' => array('id', 'name')));
		$this->set('item', $item);
		//pr($_REQUEST);exit();
	}

	function edit($id) {

		if ($this->request->is('post') || $this->request->is('put')) {

			$this->request->data['id'] = $id;
			//pr($this->request->data);
			//exit;
			$comma_separated = implode(",", $this->request->data['Order']['table_id']);
			$this->request->data['Order']['table_ids'] = $comma_separated;
			//pr($this->request->data['Order']['table_ids']);
			//pr($this->request->data['Order']['table_ids']);

			$email = $this->request->data['customerDetail']['email'];

			$this->Order->editOrder($this->request->data);

			if ($this->request->data['order_type'] == 'Take Away' || $this->request->data['order_type'] == 'Delivery') {
				if ($this->request->data['Order']['udid'] != '0' || $this->request->data['Order']['udid'] != '') {
					App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
					PushNotificationIOS::send($this->request->data['Order']['udid'], "Your order has been accepted");
					App::uses('CakeEmail', 'Network/Email');

						$Email = new CakeEmail();
						$Email->from(array('shahshobo@gmail.com' => 'Illvillagio.com'))
							->to($email)
						->subject('Order')
						->send('Dear customer, Your Order has been taken');
//		$this->redirect(array('controller' => 'reservations', 'action' => 'index'));
				}
			}

			echo 'Order has been edited!';
		}

		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$tables_floors = $this->Order->get_tables_floors($id);

		$data = $this->Order->findById($id);

		$customer = $this->CustomerInformation->findByid($data['Order']['customer_id']);

		$order_item = $this->Order->view_order_items($id);


		$data_items = array();
		foreach ($order_item as $key => $orders) {

			//$data[$key] = $orders['orders']; 
			//$data[$key] = $orders['items'];

			$data_items[$key] = array_merge($orders['orders'], $orders['items']);
		}
		//pr($order_item);

		foreach ($tables_floors as $o_items) {

			if ($o_items['order_tables']['floor_id'] == 1) {
				$tables[] = 'MT' . $o_items['order_tables']['table_id'];
			} elseif ($o_items['order_tables']['floor_id'] == 2) {
				$tables[] = 'GT' . $o_items['order_tables']['table_id'];
			}
		}

		$this->set('udid', $data);
		$this->set('order', $data_items);
		$this->set('order_table_floor', $tables);
		//$order_items = ;
		//pr($order_item);
		//pr($customer);

		$this->request->data = $data['Order'];
		$this->request->data['Order']['no_seats'] = $this->request->data['no_of_seats'];
		$this->request->data['Order']['status'] = $this->request->data['status'];
		$this->request->data['Order']['order_type'] = $this->request->data['ordertype'];
		$this->request->data['Order']['order_table_id'] = $this->request->data['table_id'];
		$this->set('order_data', $data['Order']);
		//pr($this->request->data);


		$this->request->data['customerDetail']['name'] = $customer['CustomerInformation']['name'];
		$this->request->data['customerDetail']['telephoneNo'] = $customer['CustomerInformation']['telephone_no'];
		$this->request->data['customerDetail']['email'] = $customer['CustomerInformation']['email'];
		$this->request->data['customerDetail']['shippingAddress'] = $customer['CustomerInformation']['address'];





		//$this->request->data
		//pr($this->request->data);
		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}



		//$this->set('recipe', $this->Gallery->findById($id));
		$this->layout = 'admin';


		$item = $this->Item->find('list', array('fields' => array('id', 'name')));
		$this->set('item', $item);
	}

	function delete($id = null) {
		$this->layout = 'admin';
		$this->Order->delete_order($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index'));
	}

	function get_item_id() {
		
	}

}

?>
