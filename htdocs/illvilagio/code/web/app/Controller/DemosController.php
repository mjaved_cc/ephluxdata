<?php

App::uses('DemosController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class DemosController extends AppController {
	
	public $components = array('Facebook', 'Twitter', 'ApiUser');
	public $name = 'Demos';
	public $uses = array('User', 'Tmp', 'UserSocialAccount', 'Role');

	function beforeFilter() {
		parent::beforeFilter();
	}

	/**
	 * welcome method - dummy
	 *
	 * @return void
	 */
	function index() {
		//die('Welcome');
		//echo "wellcome screen";
		$this->redirect('/dashboards/index');
	}

	function secure() {
		$user = $this->Auth->User();
		$this->set(compact('user'));
	}

	/**
	 * Get login url 
	 */
	function login_with_facebook() {
		$loginUrl = $this->Facebook->getFbLoginurl();
		$this->redirect($loginUrl);
	}

	/**
	 * Get access token and user data from facebook & save it in DB.
	 * 
	 */
	function facebook_connect() {
		try {
			$access_token = $this->Facebook->getAccessToken();
			$user = $this->Facebook->getUserDetails($access_token);
			$user['access_token'] = $access_token;

			$api_response = $this->ApiUser->login_with_facebook($user);
			$response = $this->App->decoding_json($api_response);

			if ($response['header']['code'] == ECODE_SUCCESS) {
				$this->redirect($this->Auth->redirect());
			}
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}

	function login_with_twitter() {
		$url = $this->Twitter->getAuthorizeUrl();
		$this->redirect($url);
	}

	/**
	 * Response from twitter
	 * Get user data from twitter 
	 */
	function twitter_connect() {
		try {
			$access_token_info = $this->Twitter->getAccessToken();

			$user = $this->Twitter->getUserDetails($access_token_info['oauth_token'], $access_token_info['oauth_token_secret']);
			$user = array_merge($user, $access_token_info);
			
			// get fields by using any UI
			$user['email'] = 'a6@test.com'; // email is necessary
			$user['first_name'] = 'John'; 
			$user['last_name'] = 'Mike'; 
			
			$api_response = $this->ApiUser->login_with_twitter($user);
			$response = $this->App->decoding_json($api_response);

			if ($response['header']['code'] == ECODE_SUCCESS) {
				$this->redirect($this->Auth->redirect());
			}			
			
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}

	/**
	 * Create new user 
	 * 
	 * Error code range 1091-1120
	 * 
	 * @return void
	 */
	function signup() {

		$this->layout = 'login';

		if (!empty($this->data['User']) && $this->request->is('post')) {
			$user_info = $this->data['User'];
			$api_response = $this->ApiUser->signup($user_info);
			$this->set('api_response', $api_response);
		}
	}

	/**
	 * @desc: verify email & move it from Tmp table to user table
	 * @params: v (verification code) , email
	 * Error code range : 1121-1150 
	 */
	function verify_account() {

		$this->layout = 'login';
		if (
				!empty($this->request->query['v']) &&
				!empty($this->request->query['email']) &&
				$this->App->is_valid_email($this->request->query['email'])
		) {
			$verification_code = $this->request->query['v'];
			$email = $this->request->query['email'];

			$api_response = $this->ApiUser->verify_user_account($verification_code, $email);
			$this->set('api_response', $api_response);
		}
	}

	/**
	 * User login 
	 * @return void
	 * Error code range : 1061-1090
	 */
	function login() {
		$this->layout = 'login';

		$this->User->set($this->data);

		if (!empty($this->data)) {
			$login_info = $this->data['User'];

			$api_response = $this->ApiUser->login($login_info);
			$response = $this->App->decoding_json($api_response);

			if ($response['code'] == ECODE_SUCCESS) {
				$this->redirect($this->Auth->redirect());
			}
		}
	}

	/**
	 * User forget password
	 * @params null
	 * @return null
	 * Error code range : 1031-1060
	 */
	function forgot_password() {
		$this->layout = 'login';
		//$this->User->set($this->data);

		if (!empty($this->data['User']) && $this->request->is('post')) {

			$user_info = $this->data['User'];

			$api_response = $this->ApiUser->forgot_password($user_info);
			$this->set('api_response', $api_response);
		}
	}

	/**
	 * Account reset password
	 * 	@params null
	 *  @return null
	 * Error code range : 1000-1030
	 */
	function reset_account_password() {

		$this->layout = 'login';

		//if form is post
		if ($this->request->is('post') && !empty($this->data['User'])) {
			$user_pass_info = $this->data['User'];
			$verification_code = $user_pass_info['hidden'];
			$api_response = $this->ApiUser->reset_password($verification_code, $user_pass_info);
			$this->set('api_response', $api_response);
		}
		if (!empty($this->request->query['v'])) { //if returned from email
			$verification_code = $this->request->query['v'];
			$this->set('request_variable', $verification_code);
		}
	}

	function logout() {
		$this->layout = false;
		$this->Session->destroy();
		$this->redirect($this->Auth->logout());
	}
	
	function test(){
		
	}

}

