<?php

class ApiOrderComponent extends Component {

	const BASE_CODE = '9000';
	const DELIVERY_CHARGES = '20';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Make Orders 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	
	function makeOrder($order) {		
		
		if($order['Order']['table_id'] != ''){
		$pieces = explode(",", $order['Order']['table_id']);
		$table_data = array();
		
		
		foreach ($pieces as $key=>$tables){
				
			$pieces_1 = explode("T", $tables);
				
			if($pieces_1[0] != 'M'){
				$table_data[$key]['floor_id'] = 1;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} elseif ($pieces_1[0] != 'G') {
				$table_data[$key]['floor_id'] = 2;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} else {
				$table_data[$key]['floor_id'] = 0;
				$table_data[$key]['table_id'] = $pieces_1[1];
			}
				
		}
		}
		else{
			$table_data = 0;
		}
		
		
		
		if ($this->_controller->Order->validates(array('fieldList' => array('order_type')))) {
			if($this->_controller->Order->validates(array('fieldList' => array('order_item_id')))) {
				if ($this->_controller->Order->validates(array('fieldList' => array('order_item_quantity')))) {
					
						if ($this->_controller->Order->validates(array('fieldList' => array('order_seat_no')))) {
																	
									$data = $this->_controller->Order->makeOrder($order,$table_data);
									
									/*foreach ($result as $key => $value){
										$obj = new DtOrder($value['orders']);
										$data[$key] = $obj->get_field();
									}			*/						
						
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '14';
						$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Order->validationErrors);
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
					
							
				
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '14';
				$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Order->validationErrors);
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
				
		}else{
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '14';
			$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Order->validationErrors);
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
			 
			//pr($data);die;exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}
	
	
	
	
	
	function delete_order($order_id){
		
		
		if (!empty($order_id)) {
			//$this->_controller->Order->set($order_id);
			if(!empty($order_id['id'])){		
				
				
				$result = $this->_controller->Order->delete_order($order_id);		
				
			//	print_r($result);
				//exit;
				if($result == 1){
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Record Inserted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record Inserted";
				return $this->ApiResponse->get();
				}
					
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "can not find id to remove record";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		
		}	
		
	}

}

?>
