<?php

class ApiAllInformationComponent extends Component {

	const BASE_CODE = '7000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all information 
	 * 
	 * 1. Event
	 * 2. Gallery
	 * 3. Content
	 * 4. Deal for home page 
	 * 5. Events for home
	 *  
	 * Error Range 0 - 30
	 * 
	 */


	function AllInformation($lang_name,$modified_date){
		
		
		$count_gallery = $this->_controller->Gallery->galleries_count();
		$data['gallery_count'] = $count_gallery[0][0]['count'];
		
		$count_events = $this->_controller->Event->events_count();
		$data['events_count'] = $count_events[0][0]['count'];
		
		$count_content = $this->_controller->Content->contents_count();
		$data['content_count'] = $count_content[0][0]['count'];
		
		
		
		//$lang_name = '';
		//$modified_date = '';
		$content_raw = $this->_controller->Content->get_content_by_key($lang_name,$modified_date);
		if($content_raw !=1){
			foreach ($content_raw as $key=>$contents){
			//$data[$key] = $results;
				foreach ($contents as $content){		
					$data['content'][$key] = $content;
				//print_r($content);
				}		
			}
		}
		else{
			$data['content'] = array();
		}
		//exit;
		
		
	/*	$feedback_raw = $this->_controller->FeedBack->get_feedback();
		$i=0;
		foreach ($feedback_raw as $feedbacks){
			$data['feedbacks'][$i] = $feedbacks['feedbacks'];
			//print_r($data);
			$i++;
		}
		
	*/
			
		/*
		 *  Events
		 */
		 
		$is_deal = 0;
		$event_raw = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);
		
		if($event_raw != 1){
			//print_r($result);
			//exit;
		$formatted_data = $this->_get_formatted_event($event_raw);
				
			/** Wrapping into Datatype **/
		$obj_events = array();
		
		//print_r($formatted_data);
		//exit;
		foreach ($formatted_data['Event'] as $key => $value) {
					
				$obj_events[$key] = new DtEvent($value);
				$obj_events[$key]->add_event_images($value['EventImage']);
		}
			/** END: Wrapping into Datatype **/
			/** Retrieving data from Datatype **/
		$i=0;
		
		foreach ($obj_events as $out => $obj_event){
				//print
				$imageArray = array();
		
				$data['events'][$i] = $obj_event->get_field();
		
				//print_r($obj_event);
				//exit;
				foreach ($obj_event->EventImage as $obj_event_image):
					
				$imageArray[]= $obj_event_image->get_field();
				//$data[$out]['images'] = $obj_event_image->get_field();
					
				endforeach;
				//echo $out."<br/>";
				$data['events'][$i]['images']=$imageArray;
				//print_r($data);die;
				//$data['images'][]=$imageArray;
				$imageArray = NULL;
				//print_r($data);
				//exit;
				//exit;
				$i++;
		
		}
		}
		else{
			$data['events'] = array();
		}
		/*
		 *  Gallery
		*/
		
		$gallery = $this->_controller->Gallery->getGalleryInfo($lang_name,$modified_date);
		
		if($gallery != 1){
		foreach ($gallery as $key => $value){
				//$obj_gallery = new DtGallery($value['Gallery']);
				//$data[$key] = $obj_gallery->get_field();
				foreach ($value as $key_gal=>$gallery)
					//$data['gallery'][$key] = $gallery;
				$obj_gallery = new DtGallery($gallery);
				$data['gallery'][$key] = $obj_gallery->get_field($lang_name);
		}
		} else{
			$data['gallery'] = array();
		}

		
		
		//print_r($data['gallery_count']);
		/*
		 *  Menu Service
		*/
		//$data['menu'] = $this->_controller->Menu->get_menu($modified_date);

		
		
		
		
		/*
		 *  Deal for home page
		*/
		//$lang_name = '' ;
		$modified_date = '';
		$is_deal = 1;
		$event_raw = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);
		
		
		$formatted_data = $this->_get_formatted_event($event_raw);
		
		
		/** Wrapping into Datatype **/
		$obj_events = array();
		foreach ($formatted_data['Event'] as $key => $value) {
		
			$obj_events[$key] = new DtEvent($value);
			$obj_events[$key]->add_event_images($value['EventImage']);
		}
		/** END: Wrapping into Datatype **/
		/** Retrieving data from Datatype **/
		$i=0;
		
		foreach ($obj_events as $out => $obj_event){
			//print
			$imageArray = array();
		
			$data['deals'][$i] = $obj_event->get_field();
		
			//print_r($obj_event);
			//exit;
			foreach ($obj_event->EventImage as $obj_event_image):
		
			$imageArray[]= $obj_event_image->get_field();
			//$data[$out]['images'] = $obj_event_image->get_field();
		
			endforeach;
			//echo $out."<br/>";
			$data['deals'][$i]['images']=$imageArray;
			//print_r($data);die;
			//$data['images'][]=$imageArray;
			$imageArray = NULL;
			//print_r($data);
			//exit;
			//exit;
			$i++;
		
		}
		
		
		/*
		 *  Home events
		*/
		
		
		$is_deal = 2;
		$event_raw = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);
		
		//print_r($result);
		//exit;
		$formatted_data = $this->_get_formatted_event($event_raw);
		
		/** Wrapping into Datatype **/
		$obj_events = array();
		foreach ($formatted_data['Event'] as $key => $value) {
		
			$obj_events[$key] = new DtEvent($value);
			$obj_events[$key]->add_event_images($value['EventImage']);
		}
		/** END: Wrapping into Datatype **/
		/** Retrieving data from Datatype **/
		$i=0;
		
		foreach ($obj_events as $out => $obj_event){
			//print
			$imageArray = array();
		
			$data['homeEvents'][$i] = $obj_event->get_field();
		
			//print_r($obj_event);
			//exit;
			foreach ($obj_event->EventImage as $obj_event_image):
		
			$imageArray[]= $obj_event_image->get_field();
			//$data[$out]['images'] = $obj_event_image->get_field();
		
			endforeach;
			//echo $out."<br/>";
			$data['homeEvents'][$i]['images']=$imageArray;
			//print_r($data);die;
			//$data['images'][]=$imageArray;
			$imageArray = NULL;
			//print_r($data);
			//exit;
			//exit;
			$i++;
		
		}
		
		
		$this->ApiResponse->base = self::BASE_CODE;
		$this->ApiResponse->offset = '0';
		$this->ApiResponse->msg = "Result found";
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		$this->ApiResponse->body = $data;
		return $this->ApiResponse->get();
	
		
	}
	
	
	function _get_formatted_event($data) {
		$return_data = array();
	
		foreach ($data as $value) {
	
			if (
					empty($return_data) ||
					!array_key_exists($value['Event']['id'], $return_data['Event'])
			) {
				$return_data['Event'][$value['Event']['id']] = $value['Event'];
			}
	
			if (
					!isset($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id']) ||
					!array_key_exists($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id'], $return_data['Event'][$value['Event']['id']]['EventImage'])
			) {
				$return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']] = $value['EventImage'];
			}
		}
		//		debug($return_data);die();
	
		return $return_data;
	
	
	}

}
