<?php

class ApiResponseEventComponent extends Component {

	public $components = array('App');

	const JSON = 'json';
	const XML = 'xml';

	public $base = '0';
	public $offset;
	public $msg;
	public $body = '';
	public $format;

	/**
	 * Get response
	 * 
	 * @access public
	 */
	function get() {

		return $this->_get_response();
	}

	/**
	 * Get code
	 * 
	 * @access private
	 */
	private function _get_code() {
		// in case of any error
		if ($this->offset > ECODE_SUCCESS)
			return $this->base + $this->offset;
		else
			return $this->offset;
	}

	/**
	 * Get response
	 * 
	 * @access private
	 */
	private function _get_response() {

		
		if($this->msg == 'Result not found'){
			
			$response = array(
			//			'header' => array(
					//			'code' => $this->_get_code(),
					//				'message' => $this->msg
					//			),
					'status' => 0,
					'code' => $this->_get_code(),
					'timestamp' => date('Y-m-d'),
					'message' => $this->msg
					
			);
				
			
		}
		else {

			$response = array(
			//			'header' => array(
					//			'code' => $this->_get_code(),
					//				'message' => $this->msg
					//			),
					'status' => 1,
					'timestamp' => date('Y-m-d'),
					'Event' => $this->body
			);
				
			
		}
		
		
		if ($this->format == self::XML)
			return $this->_get_xml($response);
		else // default is json
			return $this->_get_json($response);
	}

	/**
	 * Get response in JSON
	 * 
	 * @param array $response Response
	 * @return json response
	 * @access private
	 */
	private function _get_json($response) {
		return $this->App->encoding_json($response);
	}

	/**
	 * Get response in XML
	 * 
	 * @param array $response Response
	 * @return XML response
	 * @access private
	 */
	private function _get_xml($response) {
		
		return $this->App->get_xml($response);
		
	}

}

?>