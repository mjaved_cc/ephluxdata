<?php

class ApiGalleryComponent extends Component {

	const BASE_CODE = '4000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function getGalleryInfo($lang_name,$modified_date) {
		$result = $this->_controller->Gallery->getGalleryInfo($lang_name,$modified_date);
		$data = array();
		

		if ($result != 1) {
			
			foreach ($result as $key => $value){
				//$obj_gallery = new DtGallery($value['Gallery']);
				//$data[$key] = $obj_gallery->get_field();
				foreach ($value as $key_gal=>$gallery)
				//$data['gallery'][$key] = $gallery;
					$obj_gallery = new DtGallery($gallery);
					$data['gallery'][$key] = $obj_gallery->get_field($lang_name);
			}
			
			//print_r($data);
			//exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		}
	}
	
	/*
	 * function to add gallery images.
	 * @param $add_images_info containsts params from fields
	 * @param $file_info contains params for image 
	 */
	
	function add_galleryInfo($add_images_info,$file_info){
		//print_r($add_images_info);
		//print_r($file_info);
		$this->_controller->Gallery->set($add_images_info);
		if (!empty($add_images_info['name']) || !empty($add_images_info['name_arabic']) || !empty($add_images_info['caption']) || !empty($add_images_info['image_caption_arabic']) ){
			//print_r($add_images_info);
			//exit;
			
		
		if(!empty($add_images_info['name'])){
			if(!empty($add_images_info['name_arabic'])){
				if(!empty($add_images_info['caption'])){
					if(!empty($add_images_info['image_caption_arabic'])){
						if(!empty($file_info['name'])){		
							if($file_info['type'] == 'image/png' || $file_info['type'] == 'image/jpeg' ||  $file_info['type'] == 'image/gif' ){				
							
							$image_path = $this->saveToFile($file_info, 'gallery');
							$result = $this->_controller->Gallery->add_galleryInfo($add_images_info,$file_info);
							
							//print_r($result);
							//exit;
							$obj_gallery = new DtGallery($result);
							$data = array();
							$data['gallery'] = $obj_gallery->get_field();		
							
							//print $data;
							//exit;
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '0';
							$this->ApiResponse->msg = "Record inserted";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = $data;
							return $this->ApiResponse->get();
							}
							else {
								
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '1';
								$this->ApiResponse->msg = "file has not image type";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = "Record not Inserted";
								return $this->ApiResponse->get();			
								
								
							}
						}
						else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '1';
							$this->ApiResponse->msg = "Image not found";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}	
					}
					else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '2';
						$this->ApiResponse->msg = "Image caption in arabic not found";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
							
				}
				else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '3';
					$this->ApiResponse->msg = "Image caption not found";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
			}
			else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '4';
				$this->ApiResponse->msg = "Name in arabic not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}					
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '5';
			$this->ApiResponse->msg = "Name field is required";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Record not Inserted";
			return $this->ApiResponse->get();
		}
		} else {
			
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '6';
			$this->ApiResponse->msg = "Sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		}
		
	}
	
	
	function edit_galleryInfo($add_images_info,$file_info){
		//print_r($add_images_info);
		//print_r($file_info);
		$this->_controller->Gallery->set($add_images_info);
		if (!empty($add_images_info['name']) || !empty($add_images_info['name_arabic']) || !empty($add_images_info['caption']) || !empty($add_images_info['image_caption_arabic']) ){
			//print_r($file_info);
			//exit;
				
		if(!empty($add_images_info['id'])){
			if(!empty($add_images_info['name'])){
				if(!empty($add_images_info['name_arabic'])){
					if(!empty($add_images_info['caption'])){
						if(!empty($add_images_info['image_caption_arabic'])){
							if(!empty($file_info['name'])){
								if($file_info['type'] == 'image/png' || $file_info['type'] == 'image/jpeg' ||  $file_info['type'] == 'image/gif' ){
									
								$image_path = $this->saveToFile($file_info, 'gallery');
								$result = $this->_controller->Gallery->edit_galleryInfo($add_images_info,$file_info);
									
								//print_r($result);
								//exit;
								$obj_gallery = new DtGallery($result);
								$data = array();
								$data['gallery'] = $obj_gallery->get_field();
									
								//print $data;
								//exit;
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '0';
								$this->ApiResponse->msg = "Record inserted";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = $data;
								return $this->ApiResponse->get();
								}
								else {
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '1';
									$this->ApiResponse->msg = "file has not image type";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = "Record not Inserted";
									return $this->ApiResponse->get();
										
								}
							}
							else {
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '1';
								$this->ApiResponse->msg = "Image not found";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = "Record not Inserted";
								return $this->ApiResponse->get();
							}
						}
						else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '2';
							$this->ApiResponse->msg = "Image caption in arabic not found";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}
							
					}
					else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '3';
						$this->ApiResponse->msg = "Image caption not found";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
				}
				else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '4';
					$this->ApiResponse->msg = "Name in arabic not found";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '5';
				$this->ApiResponse->msg = "Name field is required";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '6';
			$this->ApiResponse->msg = "Id field is required";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Record not Inserted";
			return $this->ApiResponse->get();
		  }
		
		} else {
				
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '6';
			$this->ApiResponse->msg = "Sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		}
	
	}
	
	function delete_galleryInfo($gallery_id){
	
	
		if (!empty($gallery_id)) {
			$this->_controller->Content->set($gallery_id);
			if(!empty($gallery_id['id'])){
	
				$edit_info = $this->_controller->Gallery->delete_galleryInfo($gallery_id);
	
				$obj_content = new DtGallery($edit_info);
	
				$data = array();
				$data['content'] = $obj_content->get_field();
	
					
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Record Inserted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record Inserted";
				return $this->ApiResponse->get();
					
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "can not find id to remove record";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
	
		}
	
	
	}
	
	function saveToFile($file, $path) {
		$info = pathinfo($file['name']); // split filename and extension
		$saveName = $info['basename'] ;
		$savePath = WWW_ROOT . $path . DS . $saveName;
		
		move_uploaded_file($file['tmp_name'], $savePath);
		$path = FULL_BASE_URL . Router::url('/') .'app/webroot' . DS . $path  . DS . $saveName;
		return $path;
	}

}

?>
