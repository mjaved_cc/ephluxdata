<?php

App::uses('Folder', 'Utility');

class ApiEventComponent extends Component {

	const BASE_CODE = '6000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}


	function get_event($lang_name,$modified_date,$is_deal) {
		$result = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);

		//print_r($result);
		//exit;
		
		//print_r($result);
		//die();
		$data = array();
		if (!empty($result) && $result != 1 ) {
			//print_r($result);
			//exit;
			$formatted_data = $this->_get_formatted_event($result);
			
			/** Wrapping into Datatype **/
			$obj_events = array();
			foreach ($formatted_data['Event'] as $key => $value) {
			
				$obj_events[$key] = new DtEvent($value);
				$obj_events[$key]->add_event_images($value['EventImage']);				
			}
			/** END: Wrapping into Datatype **/		
			/** Retrieving data from Datatype **/
			$i=0;
				
			foreach ($obj_events as $out => $obj_event){
				//print 
				$imageArray = array();
				
				$data[] = $obj_event->get_field();
				
				//print_r($obj_event);
				//exit;
				foreach ($obj_event->EventImage as $obj_event_image):
					
				$imageArray[]= $obj_event_image->get_field();
					//$data[$out]['images'] = $obj_event_image->get_field();
					
				endforeach;
				//echo $out."<br/>";
				$data[$i]['images']=$imageArray;
				//print_r($data);die;
				//$data['images'][]=$imageArray;
				$imageArray = NULL;
				//print_r($data);
				//exit;
				//exit;
				$i++;
				
			}
			//die;
//			debug($data); exit;
			/** END: Retrieving data from Datatype **/
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} 
		
		elseif ($result == 1){
			
			/** END: Retrieving data from Datatype **/
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();			
			
		} else {
			//print_r($result);
			//exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	
	/**
	 * Format menu according to requirement.
	 * 
	 * @param array $data Menu Details
	 * @return array Result
	 */
	function _get_formatted_event($data) {
		$return_data = array();

		foreach ($data as $value) {

			if (
					empty($return_data) ||
					!array_key_exists($value['Event']['id'], $return_data['Event'])
			) {
				$return_data['Event'][$value['Event']['id']] = $value['Event'];
			}	

			if (
					!isset($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id']) ||
					!array_key_exists($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id'], $return_data['Event'][$value['Event']['id']]['EventImage'])
			) {
				$return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']] = $value['EventImage'];
			}
		}
//		debug($return_data);die();
		
		return $return_data;
		
		
	}
	
	function add_event($add_event_info,$_images){		
		//print_r($_images);
		//exit;
		if (!empty($add_event_info)) {
			$this->_controller->Event->set($add_event_info);
			if(!empty($add_event_info['title'])){
				if(!empty($add_event_info['description'])){
					if(!empty($add_event_info['participate_description'])){
						if(!empty($add_event_info['event_time'])){
							if(!empty($add_event_info['title_arabic'])){
								if(!empty($add_event_info['description_arabic'])){
									if(!empty($add_event_info['participate_description_arabic'])){
										if(!empty($_images)){
										$add_info = $this->_controller->Event->add_event($add_event_info,$_images);
										
										
										$obj_event = new DtEvent($add_info);
										
										$data = array();
										$data['event'] = $obj_event->get_field();									
										
										
										$data['event']['images'] = $this->saveToFile($_images, 'event' ,$data['event']['id']);
										//$obj_event_gallery = new DtEventImage($file);
																				
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '0';
										$this->ApiResponse->msg = "Record Inserted";
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										$this->ApiResponse->body = "Record Inserted";
										return $this->ApiResponse->get();
										
										}
										else {
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '1';
											$this->ApiResponse->msg = "Can not found images file";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = "Record not Inserted";
											return $this->ApiResponse->get();
										}
									
									}
									else {
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '2';
										$this->ApiResponse->msg = "Arabic description field is missing";
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										$this->ApiResponse->body = "Record not Inserted";
										return $this->ApiResponse->get();
									}
									
								}
								else {
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '3';
									$this->ApiResponse->msg = "Arabic description field is missing";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = "Record not Inserted";
									return $this->ApiResponse->get();
								}
							}
							else {
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '4';
								$this->ApiResponse->msg = "Arabic description field is missing";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = "Record not Inserted";
								return $this->ApiResponse->get();
							}
		
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '5';
							$this->ApiResponse->msg = "Arabic description field is missing";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '6';
						$this->ApiResponse->msg = "Arabic title field is missing";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
						
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '7';
					$this->ApiResponse->msg = "Description field is missing";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
		
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '8';
				$this->ApiResponse->msg = "Title field is missing---------------";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '9';
			$this->ApiResponse->msg = "Sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
				
		}
		
	}
	
	
	function edit_event($edit_event_info,$_images){
	
		//print_r($_images);
		//exit;
		if (!empty($edit_event_info)) {
			$this->_controller->Event->set($edit_event_info);
			if(!empty($edit_event_info['id'])){				
				if(!empty($edit_event_info['title'])){
				if(!empty($edit_event_info['description'])){
					if(!empty($edit_event_info['participate_description'])){
						if(!empty($edit_event_info['event_time'])){
							if(!empty($edit_event_info['title_arabic'])){
								if(!empty($edit_event_info['description_arabic'])){
									if(!empty($edit_event_info['participate_description_arabic'])){
										if(!empty($_images)){
											$edit_info = $this->_controller->Event->edit_event($edit_event_info,$_images);	
	
											$obj_event = new DtEvent($edit_info);
	
											$data = array();
											$data['event'] = $obj_event->get_field();	
	
											$data['event']['images'] = $this->saveToFile($_images, 'event' ,$data['event']['id']);
											//$obj_event_gallery = new DtEventImage($file);
	
	
	
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '0';
											$this->ApiResponse->msg = "Record Inserted";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = "Record inserted";
											return $this->ApiResponse->get();
	
										}
										else {
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '1';
											$this->ApiResponse->msg = "Can not found images file";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = "Record not Inserted";
											return $this->ApiResponse->get();
										}
											
									}
									else {
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '2';
										$this->ApiResponse->msg = "arabic participate description field is missing";
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										$this->ApiResponse->body = "Record not Inserted";
										return $this->ApiResponse->get();
									}
										
								}
								else {
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '3';
									$this->ApiResponse->msg = "arabic description field is missing";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = "Record not Inserted";
									return $this->ApiResponse->get();
								}
							}
							else {
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '4';
								$this->ApiResponse->msg = "title arabic field is missing";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = "Record not Inserted";
								return $this->ApiResponse->get();
							}
	
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '5';
							$this->ApiResponse->msg = "event time field is missing";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '6';
						$this->ApiResponse->msg = "participate description field is missing";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
	
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '7';
					$this->ApiResponse->msg = "description field is missing";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
	
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '8';
				$this->ApiResponse->msg = "title field is missing---------------";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
			}
			else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '9';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '10';
			$this->ApiResponse->msg = "sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
	
		}
	
	}

	function delete_event($event_id){
		
		if (!empty($event_id)) {
			$this->_controller->Event->set($event_id);
			if(!empty($event_id['id'])){
		
				$edit_info = $this->_controller->Event->delete_event($event_id);
		
				$obj_content = new DtEvent($edit_info);
		
				$data = array();
				$data['Event'] = $obj_content->get_field();		
					
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Record Inserted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record Inserted";
				return $this->ApiResponse->get();
					
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "can not find id to remove record";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		
		}
		
	}
	
	function saveToFile($file, $path,$id) {
		
		//$info = pathinfo($file['name']); // split filename and extension
		//$saveName = $info['basename'];
		//$savePath = WWW_ROOT . 'events' . DS .$id. DS. $saveName;
		
		$folder = new Folder();
		$folder->delete(WWW_ROOT. DS .'events'. DS .$id);
		$folder->create(WWW_ROOT. DS .'events'. DS .$id); //create a folder
				
		$path = array();
		foreach ($file as $key=>$image_file){	
			$path_info = pathinfo($image_file['name']);
			//print_r($path_info['basename']);
			//exit;
				
			move_uploaded_file($image_file['tmp_name'], WWW_ROOT . 'events' . DS .$id. DS. $path_info['basename']); // save file paths
			$path[$key] = FULL_BASE_URL . Router::url('/') .'app/webroot' . DS .'events'.DS .$id. DS . $path_info['basename'];
		}
		//print_r($path);
		//exit;
		
		return $path;
	}
	
	
}
