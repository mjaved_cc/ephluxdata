<?php

class ApiUdidComponent extends Component {

	const BASE_CODE = '12000';

	public $components = array('ApiResponseTable', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function setUdid( $id, $version) {

		$is_exist = $this->_controller->Udid->is_id_exists( $id );



		if (empty($is_exist)) {


			$this->_controller->Udid->save_udid( $id, $version);

			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '0';
			$this->ApiResponseTable->msg = "inserted";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			//$this->ApiResponseTable->body = $data;
			return $this->ApiResponseTable->get();
		} else {

			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '1';
			$this->ApiResponseTable->msg = "duplicate entry";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			//$this->ApiResponseTable->body = $data;
			return $this->ApiResponseTable->get();
		}
	}

	function push_notification() {

		$deviceToken = 'd4da2a905b9c992787d99f9400436402ac93dbb7b0c09035a642695bd3b510f2';

// Put your private key's passphrase here:
		$passphrase = '1234';

// Put your alert message here:
		$message = 'My first push notification!';
		
		$err = "error";
		$errstr = "error str";
		
////////////////////////////////////////////////////////////////////////////////

		$ctx = stream_context_create();
		
		$path = WEBROOT_DIR.'/files/ck.pem';
	
		stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
		$fp = stream_socket_client(
				'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
		);

// Encode the payload as JSON
		$payload = json_encode($body);

// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result) {
			//echo 'Message not delivered' . PHP_EOL;
			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '0';
			$this->ApiResponseTable->msg = "Message not delivered";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			//$this->ApiResponseTable->body = $data;
			return $this->ApiResponseTable->get();
			
		}

		else {
			//echo 'Message successfully delivered' . PHP_EOL;
			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '0';
			$this->ApiResponseTable->msg = "Message successfully delivered";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			//$this->ApiResponseTable->body = $data;
			return $this->ApiResponseTable->get();
			
		}

// Close the connection to the server
		fclose($fp);
	}

}

?>
