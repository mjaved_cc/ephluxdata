<?php

class ApiTableComponent extends Component {

	const BASE_CODE = '6000';

	public $components = array('ApiResponseTable', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function setTableStatus() {
		$result = $this->_controller->Table->setTableStatus();
		$data = array();

		if (!empty($result)) {
			
			foreach ($result as $key => $value){
				//pr($value );die;
				$obj_gallery = new DtTable($value['TABLES']);
				$data[$key] = $obj_gallery->get_field();
			}

			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '0';
			$this->ApiResponseTable->msg = "Result found";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			$this->ApiResponseTable->body = $data;
			return $this->ApiResponseTable->get();
		} else {
			$this->ApiResponseTable->base = self::BASE_CODE;
			$this->ApiResponseTable->offset = '1';
			$this->ApiResponseTable->msg = "Result not found";
			$this->ApiResponseTable->format = ApiResponseTableComponent::JSON;
			return $this->ApiResponseTable->get();
		}
	}

}

?>
