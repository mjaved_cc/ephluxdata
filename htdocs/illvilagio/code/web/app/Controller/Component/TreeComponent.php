<?php

/**
 * Modified Preorder Tree Traversal for Aros & Acos for Cakephp ACL implementation
 * 
 * @author Anas Anjaria <anas.anjaria@ephlux.com>
 * @link http://www.sitepoint.com/hierarchical-data-database-2/ Storing Hierarchical Data in a Database
 */
class TreeComponent extends Component {

	private $_ctrl;

	function startup(Controller $controller) {

		$this->_ctrl = $controller;
	}

	/**
	 * Populate lft & rght values for Aco table
	 */
	function build_aco() {
		$this->build('Aco');
	}

	/**
	 * Populate lft & rght values for Aro table
	 */
	function build_aro() {
		$this->build('Aro');
	}

	/**
	 * Build tree structure.
	 * 
	 * @param string $model <aro|aco>
	 */
	function build($model = NULL) {
		if (!empty($model)) {
			$parent_nodes = $this->_ctrl->{$model}->findAllByParentId(NULL);
		
			
			$left = 1;
			
			foreach ($parent_nodes as $node) {

				
				
				$left = $this->_create_child($node[$model]['id'], $left, $model);

				
				
				
			}
		} else {
			die('Model name missing');
		}
	}

	/**
	 * Create child based on parent.
	 * 
	 * @param int $parent_id parent_id
	 * @param int $left lft
	 * @param string $model <aro|aco>
	 */
	private function _create_child($parent_id, $left, $model) {

		// the right value of this node is the left value + 1   
		$right = $left + 1;
	

		// get all children of this node   
		$result = $this->_ctrl->{$model}->findAllByParentId($parent_id);

		if (!empty($result)) {
			foreach ($result as $value) {
	

				// recursive execution of this function for each   
				// child of this node   
				// $right is the current right value, which is   
				// incremented by the rebuild_tree function   

				$right = $this->_create_child($value[$model]['id'], $right, $model);
				

			}
		}

		// we've got the left value, and now that we've processed   
		// the children of this node we also know the right value   

		$this->_ctrl->{$model}->id = $parent_id;
		$this->_ctrl->{$model}->set('lft', $left);
		$this->_ctrl->{$model}->set('rght', $right);
		$this->_ctrl->{$model}->save();

		// return the right value of this node + 1   

		return $right + 1;

	}

	/**
	 * Add a node to the tree
	 * 
	 * <code>
	 * $this->Tree->add_node(26, 'Item', function($right_node, $ctrl_instance) {
	 * 
	 * 	  $ctrl_instance->Item->create();
	 * 
	 * 	  $ctrl_instance->Item->set('parent_id', 26 );
	 * 
	 * 	  $ctrl_instance->Item->set('model', 'Item');
	 * 
	 * 	  $ctrl_instance->Item->set('foreign_key', '1');
	 * 
	 * 	  $ctrl_instance->Item->set('alias', 'zuhair');
	 * 
	 * 	  $ctrl_instance->Item->set('lft', ($right_node + 1));
	 * 
	 * 	  $ctrl_instance->Item->set('rght', ($right_node + 2));
	 * 
	 * 	  $ctrl_instance->Item->save();
	 * 	  });
	 * </code>
	 * @link http://www.sitepoint.com/hierarchical-data-database-3/ 
	 * 
	 */
	function add_node($parent_id, $model, $callback = NULL) {

		if (!empty($parent_id) && $callback instanceof Closure) {

			$result = $this->_ctrl->{$model}->find('first', array(
				'conditions' => array(
					$model . '.id' => $parent_id
				),
				'order' => array($model . '.id DESC')
					));

			$right = $result[$model]['rght'];
			$lft = $result[$model]['lft'];
			
			// updating right nodes
			$this->_ctrl->{$model}->updateAll(
					array($model . '.rght ' => $model . '.rght + 2'), array(
				$model . '.rght > ' => $right
					)
			);

			// updating left nodes
			$this->_ctrl->{$model}->updateAll(
					array($model . '.lft ' => $model . '.lft + 2'), array(
				$model . '.lft > ' => $lft
					)
			);


			$callback($right, $this->_ctrl);
		}
	}
	
	
	
	
	
	

}
