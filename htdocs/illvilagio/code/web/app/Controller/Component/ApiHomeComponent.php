<?php 
class ApiHomeComponent extends Component {

	const BASE_CODE = '4000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function get_home($lang_name) {
		
		
		$modified_date = '';
		$is_deal = 1;
		$event_raw = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);
		
		//print_r($result);
		//exit;
		$formatted_data = $this->_get_formatted_event($event_raw);
		
		/** Wrapping into Datatype **/
		$obj_events = array();
		foreach ($formatted_data['Event'] as $key => $value) {
				
			$obj_events[$key] = new DtEvent($value);
			$obj_events[$key]->add_event_images($value['EventImage']);
		}
		/** END: Wrapping into Datatype **/
		/** Retrieving data from Datatype **/
		$i=0;
		
		foreach ($obj_events as $out => $obj_event){
			//print
			$imageArray = array();
		
			$data['deals'][$i] = $obj_event->get_field();
		
			//print_r($obj_event);
			//exit;
			foreach ($obj_event->EventImage as $obj_event_image):
				
			$imageArray[]= $obj_event_image->get_field();
			//$data[$out]['images'] = $obj_event_image->get_field();
				
			endforeach;
			//echo $out."<br/>";
			$data['deals'][$i]['images']=$imageArray;
			//print_r($data);die;
			//$data['images'][]=$imageArray;
			$imageArray = NULL;
			//print_r($data);
			//exit;
			//exit;
			$i++;
		
		}
		
		$is_deal = 2;
		$event_raw = $this->_controller->Event->get_event($lang_name,$modified_date,$is_deal);
		
		//print_r($result);
		//exit;
		$formatted_data = $this->_get_formatted_event($event_raw);
		
		/** Wrapping into Datatype **/
		$obj_events = array();
		foreach ($formatted_data['Event'] as $key => $value) {
		
			$obj_events[$key] = new DtEvent($value);
			$obj_events[$key]->add_event_images($value['EventImage']);
		}
		/** END: Wrapping into Datatype **/
		/** Retrieving data from Datatype **/
		$i=0;
		
		foreach ($obj_events as $out => $obj_event){
			//print
			$imageArray = array();
		
			$data['homeEvents'][$i] = $obj_event->get_field();
		
			//print_r($obj_event);
			//exit;
			foreach ($obj_event->EventImage as $obj_event_image):
		
			$imageArray[]= $obj_event_image->get_field();
			//$data[$out]['images'] = $obj_event_image->get_field();
		
			endforeach;
			//echo $out."<br/>";
			$data['homeEvents'][$i]['images']=$imageArray;
			//print_r($data);die;
			//$data['images'][]=$imageArray;
			$imageArray = NULL;
			//print_r($data);
			//exit;
			//exit;
			$i++;
		
		}
		
		
		
		
		
		
		
		
		$this->ApiResponse->base = self::BASE_CODE;
		$this->ApiResponse->offset = '0';
		$this->ApiResponse->msg = "Result found";
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		$this->ApiResponse->body = $data;
		return $this->ApiResponse->get();
		
		
	}
	
	
	function _get_formatted_event($data) {
		$return_data = array();
	
		foreach ($data as $value) {
	
			if (
					empty($return_data) ||
					!array_key_exists($value['Event']['id'], $return_data['Event'])
			) {
				$return_data['Event'][$value['Event']['id']] = $value['Event'];
			}
	
			if (
					!isset($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id']) ||
					!array_key_exists($return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']]['id'], $return_data['Event'][$value['Event']['id']]['EventImage'])
			) {
				$return_data['Event'][$value['Event']['id']]['EventImage'][$value['EventImage']['id']] = $value['EventImage'];
			}
		}
		//		debug($return_data);die();
	
		return $return_data;
	
	
	}
	
	
	
}
	?>