<?php

class ApiItemComponent extends Component {

	const BASE_CODE = '3000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	function get_all_menus() {
		$result = $this->_controller->Item->get_all_menus();

		$data = array();
//$obj_menu = new DtMenu($result[0]['Menu']);
//$temp = $obj_menu->get_field();
//debug ($temp);exit;

		if (!empty($result)) {
			$formatted_data = $this->_get_formatted_menu($result);

			$obj_item = array();
			foreach ($formatted_data['Item'] as $key => $value) {

				$obj_item[$key] = new DtItem($value);
				$data[$key]['Item'] = $obj_item[$key]->get_field();
				//debug($formatted_data);die();
	//debug($result);die();
			}
//			debug($obj_item); exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Format menu according to requirement.
	 * 
	 * @param array $data Menu Details
	 * @return array Result
	 */
	function _get_formatted_menu($data) {
		$return_data = array();

		foreach ($data as $value) {

			if (
					empty($return_data) ||
					!array_key_exists($value['Menu']['id'], $return_data['Menu'])
			) {
				$return_data['Menu'][$value['Menu']['id']] = $value['Menu'];
			}

			if (
					!isset($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['id']) ||
					!array_key_exists($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['id'], $return_data['Menu'][$value['Menu']['id']]['Category'])
			) {
				$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']] = $value['Category'];
			}
			if (
					!isset($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['SubCategory']['id']) ||
					!array_key_exists($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]
							['SubCategory'][$value['Subcategory']['id']]['id'], $return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['SubCategory'])
			) {
				$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]
						['SubCategory'][$value['SubCategory']['id']] = $value['SubCategory'];
			

			if (
					!isset($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['SubCategory']['id']['CategoryItem']['id']) ||
					!array_key_exists($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]
							['SubCategory'][$value['Subcategory']['id']]['CategoryItem'][$value['CategoryItem']['id']]['id'],
							$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]
							['SubCategory']['id']['CategoryItem'])
			) {


				$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]
						['SubCategory'][$value['SubCategory']['id']]['CategoryItem'][$value['CategoryItem']['id']] = $value['CategoryItem'];
			}
		}

		}
		debug($return_data);
		die();
		return $return_data;
	}

}
?>


