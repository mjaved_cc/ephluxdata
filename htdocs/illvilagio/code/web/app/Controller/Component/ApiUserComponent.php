<?php

class ApiUserComponent extends Component {

	const BASE_CODE = '1000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Forgot password component function
	 * @param array $user_info user information
	 * @return: Apiresponse
	 */
	function forgot_password($user_info) {

		$email = $user_info['username'];

		if (!empty($email)) {

			if ($this->App->is_valid_email($email)) {  //checking that the email is in valid format
				$user_detail = $this->_controller->User->get_by_email($email);  // fetching user data 

				if (!empty($user_detail)) {
					//making object of user class and passing user data to it
					$obj_user_data_type = new DtUser($user_detail['User']);

					$id = $obj_user_data_type->id;
					$first_name = $obj_user_data_type->first_name;

					$forgot_pass_code = $this->get_forgot_pass_code($id);  //generate forgot password code
					//generate forgot password link for email
					$forgot_pass_link = $this->get_forgot_pass_link($forgot_pass_code);

					$user_data = $this->App->get_db_save_json($user_detail); // make json for db save

					$status = $this->_controller->Tmp->save_tmp_record(
							$forgot_pass_code, FORGOT_PASSWORD, $user_data, $this->App->get_tmp_expiry(), $this->App->get_current_datetime());

					if ($status) { // if data is saved in tmp table
						$message = $first_name . '|' . $forgot_pass_link;

						$this->App->send_email($email, SITE_NAME . ': Forgot Password Request', $message, 'forgot_password');

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "Check your email to reset your password";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '31';
					$this->ApiResponse->msg = "We don't recognise that email address. Give it another shot.";

					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '32';
				$this->ApiResponse->msg = "Please provide valid email address";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '33';
			$this->ApiResponse->msg = "Email address is required";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * get link for forget password
	 * @param $forgot_pass_code
	 * @return string $forgot_pass_link
	 */
	function get_forgot_pass_link($forgot_pass_code) {


		$forgot_pass_link = CAKEPHP_FRAMEWORK_URI . '/' . $this->_controller->params['controller'] . '/' . 'reset_account_password?v=' . $forgot_pass_code;

		return $forgot_pass_link;
	}

	/**
	 * get code for forget password
	 * @param int $id user id
	 * @return $forgot_pass_code
	 */
	function get_forgot_pass_code($id) {

		$SALT = Configure::read('Security.salt');
		$date = new DateTime();
		$time_stamp = $date->getTimestamp();
		$code = $time_stamp . $id . $SALT;
		$forgot_pass_code = substr(md5($code), 8, 5);

		return $forgot_pass_code;
	}

	/**
	 * Account reset password method in component
	 * @param string $request_variable verification code
	 * @return json response from api
	 */
	function reset_password($verification_code, $user_pass_info = 0) {

		$user_tmp_detail = $this->_controller->Tmp->get_user_tmp_data($verification_code, FORGOT_PASSWORD); //retrieving user data from tmp table

		if (!empty($user_tmp_detail)) {  //if record is found in database			
			$user_tmp_data = $this->App->decoding_json($user_tmp_detail['Tmp']['data']);
			$user_id = $user_tmp_data['User']['id'];

			if (!empty($user_pass_info['password']) && !empty($user_pass_info['confirm_password'])) {

				$this->_controller->User->set($this->_controller->data);

				if ($this->_controller->User->validates(array('fieldList' => array('password')))) {  //validating password field
					if ($user_pass_info['password'] == $user_pass_info['confirm_password']) { // if password and confirm password are not equal 
						if (strlen($user_pass_info['password']) >= 7) {

							$user_pass_info['password'] = AuthComponent::password($user_pass_info['password']); //encrypting password
							//reset password in db
							$status = $this->_controller->User->reset_password($user_pass_info['password'], $user_id);
							if ($status) //delete record from tmp table
								$this->_controller->Tmp->remove_by_id($user_tmp_detail['Tmp']['id']);
						} else {
							$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
							$this->ApiResponse->offset = '1';
							$this->ApiResponse->msg = "password length is less than 8";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							return $this->ApiResponse->get();
						}
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "Password have successfully been reset";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					} else {
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '3';
						$this->ApiResponse->msg = "Passwords does not match";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '5';
				$this->ApiResponse->msg = "Fields are empty";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '4';
			$this->ApiResponse->msg = "You have reset your password from this email";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User login
	 * @param $login_info array
	 * @return array $api_response
	 */
	function login($login_info) {

		if (!empty($login_info['username']) && !empty($login_info['password'])) {

			$email = $login_info['username'];

			$this->_controller->User->set($this->_controller->data);

			if ($this->_controller->User->validates(array('fieldList' => array('password')))) {
				if ($this->App->is_valid_email($email)) {
					$password = AuthComponent::password($login_info['password']);

					$user_authorized = $this->_controller->User->verify_login($email, $password);  //if user is authorized

					if (!empty($user_authorized)) {

						//retieving user data
						$user_details = $this->_controller->User->get_details_by_id($user_authorized['User']['id']);

						if (!empty($user_details)) {

							/* Wrap data with rendering logic */
							$obj_user = new DtUser($user_details['0']['User']); //sending user data to the user class

							foreach ($user_details as $value) {
								$obj_user->add_user_social_account($value['UserSocialAccount']);
							}
						}
						/* END: Wrap data with rendering logic */

						/* Retrieve data from rendering logic */
						$data = array();
						$data['User'] = $obj_user->get_field();

						if (is_array($obj_user->UserSocialAccount)) {
							foreach ($obj_user->UserSocialAccount as $key => $value) {
								$data['UserSocialAccount'][$key] = $value->get_field();
							}
						} else if ($obj_user->UserSocialAccount instanceof DtUserSocialAccount) {
							$data['UserSocialAccount'] = $obj_user->UserSocialAccount->get_field();
						}

						/* END: Retrieve data from rendering logic */

						// login user using Auth 
						// since we have already check it credential via verify_login method hence by pass check 
						$this->_controller->Auth->login($data['User']);

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "You are successfully logged in";
						$this->ApiResponse->body = $data;
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					} else {
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '61';
						$this->ApiResponse->msg = $this->_controller->Auth->loginError;

						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '62';
				$this->ApiResponse->msg = "Pleasee provide valid email address";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '63';
			$this->ApiResponse->msg = "Username or password is empty";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Create new user
	 * 
	 * @param array $user_info user information
	 * @return array response
	 */
	function signup($user_info) {

		$this->_controller->User->set($user_info);

		if ($this->_controller->User->validates()) {

			$email = $user_info['username'];


			if (!$this->_controller->Tmp->is_unverified_email_exists($email)) { //if this user already exist as unverified
				if ($user_info['password'] == $user_info['confirm_password']) {

					$user_info['password'] = AuthComponent::password($user_info['password']);
					$user_data = $this->App->get_db_save_json($user_info);

					$status = $this->_controller->Tmp->save_tmp_record($email, SIGNUP_REGISTRATION, $user_data, $this->App->get_tmp_expiry(), $this->App->get_current_datetime());  //save record in tmp table

					if ($status) {

						$ver_link = $this->_get_verification_link($email);
						$first_name = $user_info['first_name'];

						$message = $first_name . '|' . $ver_link;
						$this->App->send_email($email, SITE_NAME . ': Account Activation', $message, 'signup'); //send email for verification

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "You are successfully sign up, please check your email to verify your account";

						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '91';
					$this->ApiResponse->msg = "Passwords does not match";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '92';
				$this->ApiResponse->msg = "We have found your email as unverified email. Please check your email.";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		}
	}

	/**
	 * Get link for email verification
	 * @param string $email email address
	 * @return string signup_link
	 */
	function _get_verification_link($email) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		$verification_code = substr(md5($code), 8, 5);

		$signup_link = CAKEPHP_FRAMEWORK_URI . '/' . $this->_controller->params['controller'] . '/' . 'verify_account?v=' . $verification_code . '&email=' . $email;

		return $signup_link;
	}

	/**
	 * verify email & move it from Tmp table to user table
	 * @param varchar v (verification code) , email
	 * @return array $api_response
	 */
	function verify_user_account($verification_code, $email) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		if (substr(md5($code), 8, 5) == $verification_code) {

			$user_tmp_details = $this->_controller->Tmp->get_user_tmp_data($email, SIGNUP_REGISTRATION); //check if this user exist in tmp table

			if (!empty($user_tmp_details)) {
				$user_tmp_data = $this->App->decoding_json($user_tmp_details['Tmp']['data']);
			}

			if (!empty($user_tmp_data)) {
				return $api_response = $this->_save_user_on_signup($user_tmp_data, $user_tmp_details);
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '1122';
				$this->ApiResponse->msg = "Your account has already been activated.";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '1123';
			$this->ApiResponse->msg = "Verification code is invalid.";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * save user info when user just signup & verifies email
	 * @param array $user_tmp_data
	 * @param array $user_tmp_details
	 * @return array $api_response
	 */
	private function _save_user_on_signup($user_tmp_data, $user_tmp_details) {
		$user_tmp_data['group_id'] = GROUP_ID_USER;
		$user_tmp_data['created'] = $this->App->get_current_datetime();
		$user_tmp_data['ip_address'] = $this->App->get_numeric_ip_representation();
		$status = $this->_controller->User->create_user($user_tmp_data); // save record in user table

		if ($status) {
			$this->_controller->Tmp->remove_by_id($user_tmp_details['0']['Tmp']['id']);

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "You have successfully verified your account";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '1121';
			$this->ApiResponse->msg = "There is an error occured, please try again later.";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Login with facebook. It serves as signup as well. If user uses login for first time it serves as signup. 
	 * 
	 * We save necessary details in our system. In other cases it serves as normal login
	 * 
	 * @param array $data facebook details
	 */
	function login_with_facebook($user) {

		// username is basically email in our system
		$login_info['username'] = $user['email'];
		// so that encryption does not exceeds character limit.				
		$login_info['password'] = substr($user['id'], 0 , 8);

		$api_response = $this->login($login_info);
		$response = $this->App->decoding_json($api_response);
		
		// if user has already 
		if ($response['header']['code'] == ECODE_SUCCESS) {
			return $api_response;
		} else { 
			$data = array(
				'User' => array(
					'first_name' => $user['first_name'],
					'last_name' => $user['last_name'],
					'username' => $user['email'],
					'ip_address' => $this->App->get_numeric_ip_representation(),
					'password' => AuthComponent::password($login_info['password']),
					'group_id' => GROUP_ID_USER,
					'created' => $this->App->get_current_datetime()
				),
				'UserSocialAccount' => array(
					'link_id' => $user['id'],
					'email_address' => $user['email'],
					'image_url' => $user['picture']['data']['url'],
					'type_id' => FACEBOOK_TYPE_ID,
					'access_token' => $user['access_token'],
					'screen_name' => $user['username'],
					'ip_address' => $this->App->get_numeric_ip_representation(),
					'created' => $this->App->get_current_datetime()
				)
			);

			$xml = $this->App->get_xml($data);
			$status = $this->_controller->User->signup_with_social_account($xml);
			
			if ($status) {
				return $this->login($login_info);
			}
		}
	}
	
	/**
	 * Login with facebook. It serves as signup as well. If user uses login for first time it serves as signup. 
	 * 
	 * We save necessary details in our system. In other cases it serves as normal login
	 * 
	 * @param array $data facebook details
	 */
	function login_with_twitter($user) {

		// username is basically email in our system
		$login_info['username'] = $user['email'];
		// so that encryption does not exceeds character limit.				
		$login_info['password'] = substr($user['id'], 0 , 8);

		$api_response = $this->login($login_info);
		$response = $this->App->decoding_json($api_response);
		
		// if user has already 
		if ($response['header']['code'] == ECODE_SUCCESS) {
			return $api_response;
		} else { 
			$data = array(
				'User' => array(
					'first_name' => $user['first_name'],
					'last_name' => $user['last_name'],
					'username' => $user['email'],
					'ip_address' => $this->App->get_numeric_ip_representation(),
					'password' => AuthComponent::password($login_info['password']),
					'group_id' => GROUP_ID_USER,
					'created' => $this->App->get_current_datetime()
				),
				'UserSocialAccount' => array(
					'link_id' => $user['id'],
					'email_address' => $user['email'],
					'image_url' => TwitterComponent::getImageUrl($user['profile_image_url']),
					'type_id' => TWITTER_TYPE_ID,
					'access_token' => $user['oauth_token'],
					'access_token_secret' => $user['oauth_token_secret'],
					'screen_name' => $user['screen_name'],
					'ip_address' => $this->App->get_numeric_ip_representation(),
					'created' => $this->App->get_current_datetime()
				)
			);

			$xml = $this->App->get_xml($data);
			$status = $this->_controller->User->signup_with_social_account($xml);
			
			if ($status) {
				return $this->login($login_info);
			}
		}
	}

}

?>