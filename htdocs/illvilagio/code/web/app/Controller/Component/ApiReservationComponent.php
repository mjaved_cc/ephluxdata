<?php

class ApiReservationComponent extends Component {

	const BASE_CODE = '6000';

	public $components = array('ApiResponse', 'App','Ssl');
	private $_controller;
	
	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	
	
		
	
	
	/**
	 * Method
	 * add_reservation 
	 * @param type $name Description
	 */
	function add_reservation($json_reservation_info) {
		
		//$reservation_info = $this->App->decoding_json($json_reservation_info);
		//$reservation_keys = array_keys($reservation_info);
		$reservation_info = $json_reservation_info;
		//print_r($reservation_info);
		//exit;
		$allowed_keys = array('no_of_children', 'no_of_oldage', 'confirm_via_email', 'email_address', 'phone_no', 'created', 'no_of_guest', 'time', 'event_id', 'floor_id', 'table_id');
	
		$pieces = explode(",", $reservation_info['table_id']);		
		$table_data = array();

		
		foreach ($pieces as $key=>$tables){			
			
			$pieces_1 = explode("T", $tables);
			
			if($pieces_1[0] != 'M'){
				$table_data[$key]['floor_id'] = 1;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} elseif ($pieces_1[0] != 'G') {
				$table_data[$key]['floor_id'] = 2;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} else {
				$table_data[$key]['floor_id'] = 0;
				$table_data[$key]['table_id'] = $pieces_1[1];
			}
			
		}
		
		//print_r($table_data);
		//exit;

		if($reservation_info['event_id'] == 0){
			$reservation_info['event_id'] =12;
		}
		
		if (empty($reservation_info['udid'])){
			$reservation_info['udid'] =0;
		}
		//print_r($reservation_info);
		

		/*if (!empty($result)) {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Missing Parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {*/

// validating reservation values 

		//print_r($reservation_info);
		//exit;
		
			$this->_controller->Reservation->set($reservation_info);
			if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_children')))) {
				if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_oldage')))) {
					if ($this->_controller->Reservation->validates(array('fieldList' => array('confirm_via_email')))) {
						if ($this->_controller->Reservation->validates(array('fieldList' => array('email_address')))) {
							if ($this->_controller->Reservation->validates(array('fieldList' => array('phone_no')))) {
								
									if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_guest')))) {
										if ($this->_controller->Reservation->validates(array('fieldList' => array('time')))) {
											if ($this->_controller->Reservation->validates(array('fieldList' => array('event_id')))) {
												if ($this->_controller->Reservation->validates(array('fieldList' => array('floor_id')))) {
													if ($this->_controller->Reservation->validates(array('fieldList' => array('table_id')))) {
														$this->_controller->Reservation->ReservationTable->set($reservation_info);
														


																/**
																 * END: validation
																 * authenticating reservations
																 * weather the table is also reserved or not
																 */
//		if (!empty($reservation_info['table_id']) && !empty($reservation_info['floor_id']) && !empty($reservation_info['time'])) {
																///print_r($table_data);
																//exit;
																foreach ($table_data as $tables){
																$is_already_reserved = $this->_controller->Reservation->verify_reservation($tables['floor_id'], $tables['table_id'], $reservation_info['time']);
																}
																	
																if ($is_already_reserved) {

// display error code
																	$this->ApiResponse->base = self::BASE_CODE;
																	$this->ApiResponse->offset = '14';
																	$this->ApiResponse->msg = "Already Reserved";
																	$this->ApiResponse->format = ApiResponseComponent::JSON;
																	return $this->ApiResponse->get();
																} else {
																	$time_span = self::get_reservation_time_span($reservation_info['time']);
																	$reservation_info['time_from'] = $time_span['time_from'];
																	$reservation_info['time'] = $time_span['time_to'];

																	if (!empty($reservation_info)) {


																		$reservation_info = $this->_controller->Reservation->add_reservation($reservation_info,$table_data);

																		//App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
																		 //PushNotificationIOS::send('b38ca7ea75ffc8fe6e800846f47c9c3fbb3f1da48c35175e8185bfdc7a70bed6', "Test notification message!");

																		/* Wrap data with rendering logic */
																		//sending user data to the user class

																		//$obj_reservation = new DtReservation($reservation_info['Reservation']);
																		//$obj_reservation->add_reservation_table($reservation_info['ReservationTable']);


																		/* END: Wrap data with rendering logic */
																		/* Retrieve data from rendering logic */


																		$data = array();
																		//$data['Reservation'] = $obj_reservation->get_field();
																		//$data['ReservationTable'] = $obj_reservation->ReservationTable->get_field();
																		$data = $reservation_info;

																		/* END: Retrieve data from rendering logic */

//	$data = $this->App->get_db_save_json($data); // save data as json to db
//																if (!empty($result)) {
//																	
//																}
																		$this->ApiResponse->base = self::BASE_CODE;
																		$this->ApiResponse->offset = '0';
																		$this->ApiResponse->msg = "Record Inserted";
																		$this->ApiResponse->format = ApiResponseComponent::JSON;

																		//$this->ApiResponse->body = $data;
																		return $this->ApiResponse->get();
																	}
																}
															
																												
												} else {
													$this->ApiResponse->base = self::BASE_CODE;
													$this->ApiResponse->offset = '4';
													$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
													$this->ApiResponse->format = ApiResponseComponent::JSON;
													return $this->ApiResponse->get();
												}
											} else {
												$this->ApiResponse->base = self::BASE_CODE;
												$this->ApiResponse->offset = '5';
												$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
												$this->ApiResponse->format = ApiResponseComponent::JSON;
												return $this->ApiResponse->get();
											}
										} else {
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '6';
											$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											return $this->ApiResponse->get();
										}
									} else {
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '7';
										$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										return $this->ApiResponse->get();
									}
								} else {
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '8';
									$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									return $this->ApiResponse->get();
								}
							
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '10';
							$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							return $this->ApiResponse->get();
						}
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '11';
						$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '12';
					$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '13';
				$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
		$this->ApiResponse->base = self::BASE_CODE;
		$this->ApiResponse->offset = '14';
		$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}
//} else {
//$this->ApiResponse->base = self::BASE_CODE;
//$this->ApiResponse->offset = '13';
//$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
//$this->ApiResponse->format = ApiResponseComponent::JSON;
//return $this->ApiResponse->get();
//}

		
		
		//}
	}

	
	
	function edit_reservation($reservation_info_raw) {
		
		
		
		//$reservation_info = $this->App->decoding_json($json_reservation_info);
		//$reservation_keys = array_keys($reservation_info);
		$reservation_info = $reservation_info_raw;
		
		$allowed_keys = array('no_of_children', 'no_of_oldage', 'confirm_via_email', 'email_address', 'phone_no', 'created', 'no_of_guest', 'time', 'event_id', 'floor_id', 'table_id');
		
		$pieces = explode(",", $reservation_info['table_id']);
		$table_data = array();
		
		foreach ($pieces as $key=>$tables){
			
			$pieces_1 = explode("T", $tables);
			
			if($pieces_1[0] != 'M'){
				$table_data[$key]['floor_id'] = 1;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} elseif ($pieces_1[0] != 'G') {
				$table_data[$key]['floor_id'] = 2;
				$table_data[$key]['table_id'] = $pieces_1[1];
			} else {
				$table_data[$key]['floor_id'] = 0;
				$table_data[$key]['table_id'] = $pieces_1[1];
			}
				
		}
		
		if($reservation_info['event_id'] == 0){
			$reservation_info['event_id'] =12;
		}
		
		
		
		
		
		
		$this->_controller->Reservation->set($reservation_info);
		if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_children')))) {
			if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_oldage')))) {
				if ($this->_controller->Reservation->validates(array('fieldList' => array('confirm_via_email')))) {
					if ($this->_controller->Reservation->validates(array('fieldList' => array('email_address')))) {
						if ($this->_controller->Reservation->validates(array('fieldList' => array('phone_no')))) {
							if ($this->_controller->Reservation->validates(array('fieldList' => array('created')))) {
								if ($this->_controller->Reservation->validates(array('fieldList' => array('no_of_guest')))) {
									if ($this->_controller->Reservation->validates(array('fieldList' => array('time')))) {
										if ($this->_controller->Reservation->validates(array('fieldList' => array('event_id')))) {
											if ($this->_controller->Reservation->validates(array('fieldList' => array('floor_id')))) {
												if ($this->_controller->Reservation->validates(array('fieldList' => array('table_id')))) {
													$this->_controller->Reservation->ReservationTable->set($reservation_info);
												
	
													
													
															/**
															 * END: validation
															 * authenticating reservations
															 * weather the table is also reserved or not
															 */
															//		if (!empty($reservation_info['table_id']) && !empty($reservation_info['floor_id']) && !empty($reservation_info['time'])) {
	
															$is_already_reserved = $this->_controller->Reservation->verify_reservation($reservation_info['floor_id'], $reservation_info['table_id'], $reservation_info['time']);
	
															
															
															if ($is_already_reserved) {
	
																// display error code
																$this->ApiResponse->base = self::BASE_CODE;
																$this->ApiResponse->offset = '14';
																$this->ApiResponse->msg = "Already Reserved";
																$this->ApiResponse->format = ApiResponseComponent::JSON;
																return $this->ApiResponse->get();
															} else {
																$time_span = self::get_reservation_time_span($reservation_info['time']);
																$reservation_info['time_from'] = $time_span['time_from'];
																$reservation_info['time_to'] = $time_span['time_to'];
																
																if (!empty($reservation_info)) {	
	
																	$reservation_info = $this->_controller->Reservation->edit_reservation($reservation_info,$table_data);
																
																	
																	/* Wrap data with rendering logic */
																	//sending user data to the user class
	
																	//$obj_reservation = new DtReservation($reservation_info['Reservation']);
																	//$obj_reservation->add_reservation_table($reservation_info['ReservationTable']);
	
	
																	/* END: Wrap data with rendering logic */
																	/* Retrieve data from rendering logic */
	
	
																	$data = array();
																	//$data['Reservation'] = $obj_reservation->get_field();
																	//$data['ReservationTable'] = $obj_reservation->ReservationTable->get_field();
	
	
																	/* END: Retrieve data from rendering logic */
	
																}
																	$this->ApiResponse->base = self::BASE_CODE;
																	$this->ApiResponse->offset = '0';
																	$this->ApiResponse->msg = "Record Inserted";
																	$this->ApiResponse->format = ApiResponseComponent::JSON;	
																	$this->ApiResponse->body = $data;
																	return $this->ApiResponse->get();
																}
																}
																	
														
												} else {
												$this->ApiResponse->base = self::BASE_CODE;
												$this->ApiResponse->offset = '4';
												$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
												$this->ApiResponse->format = ApiResponseComponent::JSON;
												return $this->ApiResponse->get();
												}
											} else {
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '5';
											$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											return $this->ApiResponse->get();
											}
										} else {
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '6';
										$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										return $this->ApiResponse->get();
										}
									} else {
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '7';
										$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										return $this->ApiResponse->get();
									}
								} else {
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '8';
									$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									return $this->ApiResponse->get();
								}
							} else {
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '9';
								$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								return $this->ApiResponse->get();
							}
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '10';
							$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							return $this->ApiResponse->get();
						}
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '11';
						$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '12';
					$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '13';
			$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
		//} else {
		//$this->ApiResponse->base = self::BASE_CODE;
		//$this->ApiResponse->offset = '13';
		//$this->ApiResponse->msg = $this->App->get_formatted_cake_error_msg($this->_controller->Reservation->validationErrors);
		//$this->ApiResponse->format = ApiResponseComponent::JSON;
		//return $this->ApiResponse->get();
		//}	
		//}
	}
	
	function get_reservation(){
		
		$result = $this->_controller->Reservation->get_reservation();
		
		
		
		
			
		
		
		
		$data = array();		
		
		if (!empty($result)) {
				
			
			$i=0;
			foreach ($result as $reservation){
				
				//print_r($reservation);
				
				$reservations = $reservation;
				//print_r($reservations);
				//print_r($reservations);
				$obj_reservation = new DtReservation($reservations['Reservation']);
				
				
				
				//foreach ($reservations['ReservationTable'] as $key=>$reservation_table){
					//$obj_reservation->add_reservation_table($reservations['Reservation']);
					//$data[$i]['ReservationTable'][$key] = $obj_reservation->ReservationTable->get_field();
					//$data[$i]['ReservationTable'] = $reservation_table;
					//print_r($reservation_table);
					
				//}
				//exit;
				
				
				$data[$i]['Reservation'] = $obj_reservation->get_field();
				$data[$i]['ReservationTable'] = $reservations['ReservationTable'];
				
				//$data[$i]['ReservationTable'] = $obj_reservation->ReservationTable->get_field();
				
				/*foreach (){
					
				}*/
				
			$i++;	
			}
			//print_r($data);
			
			
			
			
			
			//exit;
				
				
		
			//print_r($data);
			//exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		}
				
	}


static function get_reservation_time_span($time) {

return array(
	'time_from' => date('Y-m-d H:i:s', strtotime($time . ' - ' . INTERVAL_TIME_DIFF_TO_MINUTES . ' minute')),
	'time_to' => date('Y-m-d H:i:s', strtotime($time . ' + ' . INTERVAL_TIME_DIFF_FROM_MINUTES . ' minute')+ 2*60*60)
);
}

}
