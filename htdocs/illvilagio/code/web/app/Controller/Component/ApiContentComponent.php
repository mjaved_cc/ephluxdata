<?php

class ApiContentComponent extends Component {

	const BASE_CODE = '5000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */

	function get_content_by_key($lang_name,$modified_date) {
						
		$data = array();
		$result = $this->_controller->Content->get_content_by_key($lang_name,$modified_date);	
		
		//$data['status'] = 1;
		if($result != 1){
		$data['latitude']= 1.23456789;
		$data['longitude'] = 2.2456;
		foreach ($result as $key=>$results)
		{
		//$data[$key] = $results;
		foreach ($results as $content){		
			$data['content'][$key] = $content;
		//print_r($content);
		}		
		}
		
		$this->ApiResponse->base = self::BASE_CODE;
		$this->ApiResponse->offset = '0';
		$this->ApiResponse->msg = "Result found";
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		$this->ApiResponse->body = $data;
		return $this->ApiResponse->get();
		
		} else {
			
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		}		
		//return $result;	
	}
	
	
	function add_content($add_content_info){

		//print_r($add_content_info);
		//exit;
		
		
		if (!empty($add_content_info)) {
			$this->_controller->Content->set($add_content_info);
			if(!empty($add_content_info['title'])){
				if(!empty($add_content_info['description'])){
					if(!empty($add_content_info['title_arabic'])){
						if(!empty($add_content_info['description_arabic'])){
							
							$add_info = $this->_controller->Content->add_content($add_content_info);
							
							$obj_content = new DtContent($add_info);
							
							$data = array();
							$data['content'] = $obj_content->get_field();
							
							
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '0';
							$this->ApiResponse->msg = "Record Inserted";
							$this->ApiResponse->format = ApiResponseComponent::JSON;			
							$this->ApiResponse->body = $data;
							return $this->ApiResponse->get();
						
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '1';
							$this->ApiResponse->msg = "Arabic description field is missing";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}											
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '2';
						$this->ApiResponse->msg = "Arabic title field is missing";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
			
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '3';
					$this->ApiResponse->msg = "Description field is missing";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
				
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '4';
				$this->ApiResponse->msg = "Title field is missing---------------";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '5';
			$this->ApiResponse->msg = "Sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();			
			
		}	
	}
	
	function edit_content($edit_content_info){
		
		
		if (!empty($edit_content_info)) {
			$this->_controller->Content->set($edit_content_info);
			if(!empty($edit_content_info['id'])){
			if(!empty($edit_content_info['title'])){
				if(!empty($edit_content_info['description'])){
					if(!empty($edit_content_info['title_arabic'])){
						if(!empty($edit_content_info['description_arabic'])){
								
							$edit_info = $this->_controller->Content->edit_content($edit_content_info);
								
							$obj_content = new DtContent($edit_info);
								
							$data = array();
							$data['content'] = $obj_content->get_field();
								
								
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '0';
							$this->ApiResponse->msg = "Record Inserted";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = $data;
							return $this->ApiResponse->get();
		
						} else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '1';
							$this->ApiResponse->msg = "Arabic description field is missing";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = "Record not Inserted";
							return $this->ApiResponse->get();
						}
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '2';
						$this->ApiResponse->msg = "Arabic title field is missing";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "Record not Inserted";
						return $this->ApiResponse->get();
					}
						
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '3';
					$this->ApiResponse->msg = "Description field is missing";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "Record not Inserted";
					return $this->ApiResponse->get();
				}
		
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '4';
				$this->ApiResponse->msg = "Title field is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
			
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '5';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '6';
			$this->ApiResponse->msg = "Sorry we did not found any parameter";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
				
		}
		
		
		
		
	}
	
	
	
	function delete_content($content_id){
	
	
		if (!empty($content_id)) {
			$this->_controller->Content->set($content_id);
			if(!empty($content_id['id'])){
				
				$edit_info = $this->_controller->Content->delete_content($content_id);
								
				$obj_content = new DtContent($edit_info);
								
				$data = array();
				$data['content'] = $obj_content->get_field();
								
							
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Record Inserted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = $data;
				return $this->ApiResponse->get();
					
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "id is missing";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "Record not Inserted";
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "can not find id to remove record";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
	
		}
		
	
	}
	
	
	
	
	
}



?>
