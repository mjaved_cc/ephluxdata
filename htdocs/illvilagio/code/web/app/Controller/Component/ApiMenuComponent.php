<?php

class ApiMenuComponent extends Component {

	const BASE_CODE = '1000';

	public $components = array('ApiResponse', 'App','ApiResponseMenu');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all menus 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
//	function get_all_menus() {
//		$result = $this->_controller->Menu->get_all_menus();
//
//		$data = array();
//		//$obj_menu = new DtMenu($result[0]['Menu']);
//		//$temp = $obj_menu->get_field();
//		//debug ($temp);exit;
// 
//		if (!empty($result)) {
//			foreach ($result as $key => $value){
//				
//				$obj_menu = new DtMenu($value['Menu']);
//				$data[$key]['Menu'] = $obj_menu->get_field();
//			}
//			
//			$this->ApiResponse->base = self::BASE_CODE;
//			$this->ApiResponse->offset = '0';
//			$this->ApiResponse->msg = "Result found";
//			$this->ApiResponse->format = ApiResponseComponent::JSON;
//			$this->ApiResponse->body = $data;
//			return $this->ApiResponse->get();
//		} else {
//			$this->ApiResponse->base = self::BASE_CODE;
//			$this->ApiResponse->offset = '1';
//			$this->ApiResponse->msg = "Result not found";
//			$this->ApiResponse->format = ApiResponseComponent::JSON;
//			return $this->ApiResponse->get();
//		}
//	}

	/**
	 * Get all menus
	 */
	function get_all_menus() {
		$result = $this->_controller->Menu->get_all_menus();
		debug($result); 
		$data = array();
		//$obj_menu = new DtMenu($result[0]['Menu']);
		//$temp = $obj_menu->get_field();
		//debug ($temp);exit;

		if (!empty($result)) {
			$formatted_data = $this->_get_formatted_menu($result);
			debug($formatted_data); exit;
			$obj_menu_lists = array();
			foreach ($formatted_data['Menu'] as $key => $value) {
				
				$obj_menu_lists[$key] = new DtMenu($value);
				$data[$key]['Menu'] = $obj_menu_lists[$key]->get_field();
			}
			//debug($obj_menu_lists); exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	function edit_menu($menu_info,$file_info){
	
		
		
			if(!empty($menu_info['id'])){
				if(!empty($menu_info['name'])){
					if(!empty($menu_info['extra'])){
						if(!empty($menu_info['name_arabic'])){						
								if(!empty($menu_info['price'])){												
									if(!empty($menu_info['description'])){
										if(!empty($menu_info['description_arabic'])){
										
											$image_path = $this->saveToFile($file_info, 'menu');
											$result = $this->_controller->Menu->edit_menu($menu_info,$file_info);
											
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '0';
											$this->ApiResponse->msg = "Result found";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = $result;
											return $this->ApiResponse->get();
											}
											else {
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '0';
											$this->ApiResponse->msg = "description_arabic not found";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = 'arabic description not found';
											return $this->ApiResponse->get();
										}
									}
									else{
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '0';
										$this->ApiResponse->msg = "description not found";
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										$this->ApiResponse->body = '';
										return $this->ApiResponse->get();
									}
								}  
								else{
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '0';
									$this->ApiResponse->msg = "price not found";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = '';
									return $this->ApiResponse->get();
								}
							
						}
						else {
							$this->ApiResponse->base = self::BASE_CODE;
							$this->ApiResponse->offset = '0';
							$this->ApiResponse->msg = "name_arabic not found";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							$this->ApiResponse->body = '';
							return $this->ApiResponse->get();
						}
					}
					else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "extra field not found";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = '';
						return $this->ApiResponse->get();
					}
				
				} else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '0';
					$this->ApiResponse->msg = "name field not found";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = '';
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "id field not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = '';
				return $this->ApiResponse->get();
			}		
			
		//return $result;
	}
	
	function add_menu($menu_info,$file_info){
		
		
		
			if(!empty($menu_info['parent_id'])){
				if(!empty($menu_info['name'])){
					if(!empty($menu_info['extra'])){
						if(!empty($menu_info['name_arabic'])){							
								if(!empty($menu_info['price'])){
									if(!empty($menu_info['description'])){
										if(!empty($menu_info['description_arabic'])){
											
											$image_path = $this->saveToFile($file_info, 'menu');
											$result = $this->_controller->Menu->add_menu($menu_info,$file_info);
											
											
											$this->ApiResponse->base = self::BASE_CODE;
											$this->ApiResponse->offset = '0';
											$this->ApiResponse->msg = "Result found";
											$this->ApiResponse->format = ApiResponseComponent::JSON;
											$this->ApiResponse->body = $result;
											return $this->ApiResponse->get();
											}
											else {
												$this->ApiResponse->base = self::BASE_CODE;
												$this->ApiResponse->offset = '0';
												$this->ApiResponse->msg = "description_arabic not found";
												$this->ApiResponse->format = ApiResponseComponent::JSON;
												$this->ApiResponse->body = "description_arabic not found";
												return $this->ApiResponse->get();
											}
										}
									else{
										$this->ApiResponse->base = self::BASE_CODE;
										$this->ApiResponse->offset = '0';
										$this->ApiResponse->msg = "description not found";
										$this->ApiResponse->format = ApiResponseComponent::JSON;
										$this->ApiResponse->body = "description not found";
										return $this->ApiResponse->get();
									}
								}else{
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '0';
									$this->ApiResponse->msg = "price not found";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = "price not found";
									return $this->ApiResponse->get();
								}
							
						}
						else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "name_arabic not found";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = "name_arabic not found";
						return $this->ApiResponse->get();
						}
					}
					else {
					$this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '0';
					$this->ApiResponse->msg = "extra field not found";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "extra field not found";
					return $this->ApiResponse->get();
					}
											
				} else {
				    $this->ApiResponse->base = self::BASE_CODE;
					$this->ApiResponse->offset = '0';
					$this->ApiResponse->msg = " not found";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					$this->ApiResponse->body = "name field not found";
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "parent id field not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = "parent id field not found";
				return $this->ApiResponse->get();
			}
									
			
	}
		
	function delete_menu($menu_info){
	
		$result = $this->_controller->Menu->delete_menu($menu_info);
		return $result;
	}
		
	function get_menu($time){
		
		
		$result['menu'] = $this->_controller->Menu->get_menu($time);		
		
		
		if(!empty($result)){
		$this->ApiResponseMenu->base = self::BASE_CODE;
		$this->ApiResponseMenu->offset = '0';
		$this->ApiResponseMenu->msg = "Result found";
		$this->ApiResponseMenu->format = ApiResponseMenuComponent::JSON;
		$this->ApiResponseMenu->body = $result;
		return $this->ApiResponseMenu->get();
		}
		
	}
	
	
	
	/**
	 * Format menu according to requirement.
	 * 
	 * @param array $data Menu Details
	 * @return array Result
	 */
	function _get_formatted_menu($data) {
		$return_data = array();

		foreach ($data as $value) {

			if (
					empty($return_data) ||
					!array_key_exists($value['Menu']['id'], $return_data['Menu'])
			) {
				$return_data['Menu'][$value['Menu']['id']] = $value['Menu'];
			}

			if (
					!isset($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['id']) ||
					!array_key_exists($return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['id'], $return_data['Menu'][$value['Menu']['id']]['Category'])
			) {
				$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']] = $value['Category'];
			}

			$return_data['Menu'][$value['Menu']['id']]['Category'][$value['Category']['id']]['CategoryItem'][$value['CategoryItem']['id']] = $value['CategoryItem'];
		}
		
		return $return_data;
	}

	
	function saveToFile($file, $path) {
		$info = pathinfo($file['name']); // split filename and extension
		$saveName = $info['basename'] ;
		$savePath = WWW_ROOT . $path . DS . $saveName;
	
		move_uploaded_file($file['tmp_name'], $savePath);
		$path = FULL_BASE_URL . Router::url('/') .'app/webroot' . DS . $path  . DS . $saveName;
		return $path;
	}
	

}


?>
