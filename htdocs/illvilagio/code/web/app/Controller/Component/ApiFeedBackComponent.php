<?php

class ApiFeedBackComponent extends Component {

	const BASE_CODE = '7000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller)  {

		$this->_controller = $controller;
	}

	/**
	 * Get all galleries 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function add_feedback($feedback) {
			
		//print_r($result);
		//exit;
		$data = array();

		if (!empty($feedback)) {
			if(!empty($feedback['name'])){
				
					if(!empty($feedback['email'])){
						
							if(!empty($feedback['feedbacktext'])) {
							
								$result = $this->_controller->FeedBack->add_feedback($feedback);
								
								foreach ($result as $key => $value){								
									
									$obj_feedback = new DtFeedBack($result[0]['FeedBack']);
									$data[$key] = $obj_feedback->get_field();
									
									$this->ApiResponse->base = self::BASE_CODE;
									$this->ApiResponse->offset = '0';
									$this->ApiResponse->msg = "Result found";
									$this->ApiResponse->format = ApiResponseComponent::JSON;
									$this->ApiResponse->body = $data;
									return $this->ApiResponse->get();						
								}								
								
							} else  {
								$this->ApiResponse->base = self::BASE_CODE;
								$this->ApiResponse->offset = '0';
								$this->ApiResponse->msg = "feed back text is required";
								$this->ApiResponse->format = ApiResponseComponent::JSON;
								$this->ApiResponse->body = 'Record not Inserted';
								return $this->ApiResponse->get();
							}					
						
					
					} else {
						$this->ApiResponse->base = self::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "email field is required";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						$this->ApiResponse->body = 'Record not Inserted';
						return $this->ApiResponse->get();
					}
				
			} else {
				$this->ApiResponse->base = self::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Name field is required";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				$this->ApiResponse->body = 'Record not Inserted';
				return $this->ApiResponse->get();
			}
			
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}
	
	function get_feedback() {
		
		$result = $this->_controller->FeedBack->get_feedback();
		
		
		//print_r($result);
		//exit;
		$data = array();
		
		
		if (!empty($result)) {
			
			$i=0;
			foreach ($result as $feedbacks){
			$data['feedbacks'][$i] = $feedbacks['feedbacks'];
			//print_r($data);
			$i++;			
			}
		//	exit;
			
				
			//print_r($data);
			//exit;
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = "Result not found";
			return $this->ApiResponse->get();
		}
		
		
		//print_r($result);
		//exit;
		//return $result;
	}
	

}

?>
