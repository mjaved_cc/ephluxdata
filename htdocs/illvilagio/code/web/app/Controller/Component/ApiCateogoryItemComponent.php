<?php

class ApiMenuComponent extends Component {

	const BASE_CODE = '3000';

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get all menus 
	 * 
	 * Error Range 0 - 30
	 * 
	 */
	function get_cat_item_selected() {
		$result = $this->_controller->CategoryItem->get_cat_item_selected();

		$data = array();
		//$obj_menu = new DtMenu($result[0]['Menu']);
		//$temp = $obj_menu->get_field();
		//debug ($temp);exit;
 
		if (!empty($result)) {
			foreach ($result as $key => $value){
				
				$obj_cat_item = new DtCategoryItem($value['CategoryItem']);
				$data[$key]['CategoryItem'] = $obj_cat_item->get_field();
				debug ($data);
			}
			
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = self::BASE_CODE;
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Result not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}


	
}

?>
