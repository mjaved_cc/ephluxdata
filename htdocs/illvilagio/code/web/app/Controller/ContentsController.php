<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class ContentsController extends AppController {

	public $components = array('Facebook', 'Twitter', 'ApiUser');
	public $uses = array('Content');
	

	function beforeFilter() {
		parent::beforeFilter();
		$this->layout = 'admin';
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function about_content() {
		
		$key = 'about_us';
		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			$con_en = $this->request->data['Content']['About us'];
			$con_ar = $this->request->data['Content']['Arabic'];
			
			$this->Content->save_content($con_en, $con_ar, $key);
		}
		
		

		$content = $this->Content->get_content($key);

		$this->set('content', $content[0]);
	}
	
	public function description_content() {
		
		$key = 'des';
		
		if ($this->request->is('post') || $this->request->is('put')) {			
			
			$con_en = $this->request->data['Content']['Description'];
			$con_ar = $this->request->data['Content']['Arabic'];
			
			$this->Content->save_content($con_en, $con_ar, $key);
		}
		
		

		$content = $this->Content->get_content($key);

		$this->set('content', $content[0]);
	}

	

}

