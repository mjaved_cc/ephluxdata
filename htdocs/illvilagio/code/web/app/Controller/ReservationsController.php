<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ReservationsController extends AppController {

	public $name = 'Reservations';
	public $components = array('ApiReservation');

	function success() {
		die('Success');
	}

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allowedActions = array('admin_attach');
	}

	function add_reservation() {

//		$post_array = array(
//			'no_of_children' => '3',
//			'no_of_oldage' => '4',
//			'confirm_via_email' => '0',
//			'email_address' => 'sadieee@hotmail.com',
//			'phone_no' => '123456',
//			'created' => '2013-06-14 17:36:08',
//			'no_of_guest' => '45',
//			'time' => '2013-06-14 17:36:08',
//			'event_id' => '1',
//			'floor_id' => '1',
//			'table_id' => '1'
//		);

		if ($this->request->is('post') && !empty($this->data['reservation_info'])) {
			$reservation_posted_info = $this->data['reservation_info'];

			//$json_reservation_info = $this->App->encoding_json($reservation_array);
			$result = $this->ApiReservation->add_reservation($reservation_posted_info);
			$this->set(compact('result'));
			$this->render('/Event/json/get_event');
		}
	}

	function index() {
		$this->layout = 'admin';
		$this->set('reservation', $this->Reservation->get_reservation_list());
//		$this->paginate = array('limit' => 10,);
//		$this->set('reservation', $this->paginate());
	}

	function view($id) {
		$this->layout = 'admin';
		$this->set('reservation', $this->Reservation->findById($id));
		$result = $this->Reservation->get_table_floor($id);

		$this->set('floor', $this->Reservation->get_table_floor($id));
	}

	function add() {
		$this->layout = 'admin';
		if ($this->request->is('post')) {
			$result = $this->request->data;

			$array = array($result['Reservation']['table_id'], $result['Reservation']['table_id1'], $result['Reservation']['table_id2'], $result['Reservation']['table_id3'], $result['Reservation']['table_id4'], $result['Reservation']['table_id5'], $result['Reservation']['table_id6'], $result['Reservation']['table_id7'], $result['Reservation']['table_id8'], $result['Reservation']['table_id9'], $result['Reservation']['table_id10'], $result['Reservation']['table_id11'], $result['Reservation']['table_id12'], $result['Reservation']['table_id13'], $result['Reservation']['table_id14'], $result['Reservation']['table_id15'], $result['Reservation']['table_id16'], $result['Reservation']['table_id17'], $result['Reservation']['table_id18'], $result['Reservation']['table_id19'],);
			$comma_separated = implode(",", $array);

			$arr = array_diff($array, array("0"));
			$comma_separated = implode(",", $arr);

			//pr($table_id = $comma_separated);

			$this->request->data['Reservation']['table_id'] = $comma_separated;
			$this->request->data['Reservation']['is_notification'] = 0;
//			pr($this->request->data['Reservation']['email_address']);
//			exit;
			//$result_reservation = $this->ApiReservation->add_reservation($this->request->data['Reservation']);
			$present_date = date("m/d/Y h:i");
			$time_from = $this->request->data['Reservation']['time_from'];

			if (strtotime($time_from) > strtotime($present_date)) {

				$data = $this->ApiReservation->add_reservation($this->request->data['Reservation']);
				echo '<span class="mesg-inserted">Record has been inserted!</span>';
			} else {
				echo '<span class="mesg-inserted">Time from is not valid. Record not added!</span>';
				$this->set('invalid_time','invalid');
				
			}
		}

//		$result = $this->ApiReservation->add_reservation($json_reservation_info);
//		$this->set('result',$result);
	}

	function edit($id) {

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Reservation']['id'] = $id;
			//pr($this->request->data);
			//exit();
			//pr($this->request->data['Reservation']['status']);

			$comma_separated = implode(",", $this->request->data['Reservation']['Reservation_tables']);
			$this->request->data['Reservation']['table_id'] = $comma_separated;
			echo $email = ($this->request->data['Reservation']['email_address'] );
			$data = $this->ApiReservation->edit_reservation($this->request->data['Reservation']);

			//$this->Reservation->edit_reservation($this->request->data['Reservation'],$this->request->data['Reservation_tables']);
			echo 'Reservation has been edited!';
			if ($this->request->data['Reservation']['status'] == 'Approved' && $this->request->data['Reservation']['udid'] != '0') {
				App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
				PushNotificationIOS::send($this->request->data['Reservation']['udid'], "Your reservation has been approved!");
				App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail();
				$Email->from(array('shahshobo@gmail.com' => 'Illvillagio.com'))
						->to($email)
						->subject('Reservation')
						->send('Dear customer, Your Order has been taken');
//				$Email->from(array('shahshobo@gmail.com' => 'Illvillagio.com'))
//						->to('shahshobo@gmail.com')
//						->subject('Reservation')
//						->send('Dear customer, Your Reservation has been successfully Approved');
//		$this->redirect(array('controller' => 'reservations', 'action' => 'index'));
			}
//		//pr($data);
//		}
			if ($this->request->data['Reservation']['status'] = 'Approved') {
				App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
				print_r(PushNotificationIOS::send('b38ca7ea75ffc8fe6e800846f47c9c3fbb3f1da48c35175e8185bfdc7a70bed6', "Han bhai, Penalty Lunch ??"));
			}
//		elseif($this->request->data['Reservation']['status'] = 'Rejected')
//		{ 
//		App::uses('PushNotificationIOS', 'Lib/PushNotification/iOS');
//		
//		print_r(PushNotificationIOS::send('b38ca7ea75ffc8fe6e800846f47c9c3fbb3f1da48c35175e8185bfdc7a70bed6', "Han bhai, Penalty Lunch ??"));	
		}


		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->request->data = $this->Reservation->findById($id);
		$this->request->data['Reservation']['time'] = $this->request->data['Reservation']['time_to'];
		//pr($this->request->data);
		$this->set('udid', $this->request->data['Reservation']['udid']);
		$order_items = $this->Reservation->get_reservation_tables($id);

		$tables = array();
		foreach ($order_items as $o_items) {

			if ($o_items['reservation_tables']['floor_id'] == 1) {
				$tables[] = 'MT' . $o_items['reservation_tables']['table_id'];
			} elseif ($o_items['reservation_tables']['floor_id'] == 2) {
				$tables[] = 'GT' . $o_items['reservation_tables']['table_id'];
			}
		}

		$this->set('tables_floor', $tables);
		//pr($tables);
		if (!$this->request->data) {
			throw new NotFoundException(__('Invalid post'));
		}


		$this->layout = 'admin';
//			$this->redirect(array('controller' => 'reservations', 'action' => 'index'));
	}

	function delete($id) {
		$this->layout = 'admin';
		$this->Reservation->delete_reservation($id);
		$this->Session->setFlash('The post with id: ' . $id . ' has been deleted.');
		$this->redirect(array('action' => 'index'));
	}

}

?>