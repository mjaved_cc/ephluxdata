<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CategoryController extends AppController {

	public $name = 'Category';
	public $uses = array('Category', 'Menu');

	function beforeFilter() {
		
		parent::beforeFilter('add_category');
		$this->Auth->allow();
	}

	function index() {



		$menuArr = array();
		$menuItems = $this->Menu->find('all', array(
			'fields' => array('Menu.id', 'Menu.name')
				));

		foreach ($menuItems as $key => $items) {
			$menuArr[$items['Menu']['id']] = $items['Menu']['name'];
			//pr($items['Menu']['id']);
		}

		$this->set(compact('menuArr'));
		//pr($menuArr);
		//die('Welcome');
	}

	function success() {
		die('Success');
	}

	function add_category() {

		$data = array();
		/* Getting menu items from menus table to make combo box */

		$menuArr = array();
		$menuItems = $this->Menu->find('all', array(
			'fields' => array('Menu.id', 'Menu.name')
				));

		foreach ($menuItems as $key => $items) {
			$menuArr[$items['Menu']['id']] = $items['Menu']['name'];
		}

		$this->set(compact('menuArr'));

		/* End menu items */

		/* Getting category ids from category table to make combo box */


		$categoryItems = $this->Menu->find('all', array(
			'fields' => array('Menu.id', 'Menu.name')
				));
		$this->JCManager->add_js('common');
		/* End category */

		if ($this->request->data) {


			$this->Category->set($this->request->data);
			if ($this->Category->validates(array('fieldList' => array('name', 'displayOrder', 'status')))) {

				if (is_uploaded_file($this->request->params['form']['imageUrl'])) {

					$data['imageUrl'] = $this->saveToFile($this->request->params['form']['imageUrl'], IMAGE_MENU); //file url

					$data['name'] = $this->request->data['name'];
					$data['displayOrder'] = $this->request->data['displayOrder'];
					$data['status'] = $this->request->data['status'];
					$data['created'] = $this->App->get_current_datetime();



					if ($this->Category->create($data)) {

						//	pr($this->Category->create($data));
						$this->Session->setFlash(__('The Category has been saved'));
						//$this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The Category could not been saved'));
					}
				} else {

					$this->Session->setFlash(__('Image uploading failed'));
				}
			}



			/* 	$this->Menu->create();
			  $this->Menu->set('name', $this->request->data['name']);
			  $this->Menu->set('imageURL', $this->request->data['imageUrl']);
			  $this->Menu->set('displayOrder', $this->request->data['displayOrder']);
			  $this->Menu->set('status', $this->request->data['status']);
			  if($this->Menu->save()){

			  echo 'data inserted successfully';

			  } */
		} else {
			$this->Session->setFlash(__('Nothing posted'));
			//pr($this->request->data);
		}
	}
	
	
	
	function ajax_get_category() {
		
	}

}

