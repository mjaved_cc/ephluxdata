var App = function (){}

/**
 * Is given data is loopable or not
 * 
 * @param object|array data
 */
App.isForeachable = function (data){
	return (data !== null && data.length > 0) ? true : false ; 
}

/**
 * Convert & -> &amp; < -> &lt; and > -> &gt;
 *
 *
 * @return string
 */
String.prototype.clean = function (quote_style, charset, double_encode) {
	// http://kevin.vanzonneveld.net
	// +   original by: Mirek Slugen
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Nathan
	// +   bugfixed by: Arno
	// +    revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +    bugfixed by: Brett Zamir (http://brett-zamir.me)
	// +      input by: Ratheous
	// +      input by: Mailfaker (http://www.weedem.fr/)
	// +      reimplemented by: Brett Zamir (http://brett-zamir.me)
	// +      input by: felix
	// +    bugfixed by: Brett Zamir (http://brett-zamir.me)
	// %        note 1: charset argument not supported
	// *     example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
	// *     returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
	// *     example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
	// *     returns 2: 'ab"c&#039;d'
	// *     example 3: htmlspecialchars("my "&entity;" is still here", null, null, false);
	// *     returns 3: 'my &quot;&entity;&quot; is still here'
  
	var string = this + '';

	var optTemp = 0,
	i = 0,
	noquotes = false;
	if (typeof quote_style === 'undefined' || quote_style === null) {
		quote_style = 2;
	}
	string = string.toString();
	if (double_encode !== false) { // Put this first to avoid double-encoding
		string = string.replace(/&/g, '&amp;');
	}
	string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

	var OPTS = {
		'ENT_NOQUOTES': 0,
		'ENT_HTML_QUOTE_SINGLE': 1,
		'ENT_HTML_QUOTE_DOUBLE': 2,
		'ENT_COMPAT': 2,
		'ENT_QUOTES': 3,
		'ENT_IGNORE': 4
	};
	if (quote_style === 0) {
		noquotes = true;
	}
	if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
		quote_style = [].concat(quote_style);
		for (i = 0; i < quote_style.length; i++) {
			// Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
			if (OPTS[quote_style[i]] === 0) {
				noquotes = true;
			}
			else if (OPTS[quote_style[i]]) {
				optTemp = optTemp | OPTS[quote_style[i]];
			}
		}
		quote_style = optTemp;
	}
	if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
		string = string.replace(/'/g, '&#039;');
	}
	if (!noquotes) {
		string = string.replace(/"/g, '&quot;');
	}

	return string;
}; // String.prototype.clean

/**
 * Get ajax indication URL
 */
App.baseURL = function (){
	var url = location.href;  // entire url including querystring - also: window.location.href;
	var baseURL = url.substring(0, url.indexOf('/', 14));

	if (baseURL.indexOf('http://localhost') != -1) {
		// Base Url for localhost
		var url = location.href;  // window.location.href;
		var pathname = location.pathname;  // window.location.pathname;
		var index1 = url.indexOf(pathname);
		var index2 = url.indexOf("/", index1 + 1);
		var baseLocalUrl = url.substr(0, index2);

		return baseLocalUrl + "/";
	}
	else {
		// Root Url for domain name
		return baseURL + "/";
	}
}

/**
 * Attach more parameters to URL
 *
 * extend_with can be object or a serialized string
 *
 * @param string url
 * @param mixed extend_with
 */
App.extendUrl = function(url, extend_with) {
	if(!url || !extend_with) {
		return url;
	} // if
  
	var extended_url = url.indexOf('?') < 0 ? url + '?' : url + '&';
  
	// Extend with array
	if(typeof(extend_with) == 'object') {
		var parameters = [];
    
		for(var i in extend_with) {
			if(typeof(extend_with[i]) == 'object') {
				for(var j in extend_with[i]) {
					parameters.push(i + '[' + j + ']' + '=' + extend_with[i][j]);
				} // for
			} else {
				parameters.push(i + '=' + extend_with[i]);
			} // if
		} // for
    
		return extended_url + parameters.join('&');
    
	// Extend with string (serialized?)
	} else {
		return extended_url + extend_with;
	} // if
};

/**
 * Get url following cake convention
 *
 * extend_with can be object or a serialized string
 * 
 * @param mixed extend_with
 */

App.getUrl = function(extend_with){
	var url = App.baseURL();
	if(!extend_with) {
		return url;
	} // if
	
	// Extend with array
	if(typeof(extend_with) == 'object') {		    
		return url + extend_with.controller + '/' + extend_with.action ;
	} // if
};