$(document).ready(function () {
	

// Highlight current page in menu
        var loc = window.location.toString().split("/")
        loc = loc[loc.length - 1]
        $("#main_nav li a[href=\"" + loc + "\"]").parents('li:not(ul ul li)').children('a').addClass("current");


// Create Graph using FLOT and Graph Table
    $('table.graph').each(function () {
        
        /**
			Options for Flot will go here. For more details see:
			http://flot.googlecode.com/svn/trunk/README.txt
			http://flot.googlecode.com/svn/trunk/API.txt
        **/
        graph_type = ($(this).attr('data-type'));

	   if (graph_type=="pie") {
            var FlotOptions = {
                series: {
                pie: {
                    show: true,
					tilt:0.5,
					innerRadius: 0.5
                    }
                },
 				grid:{
					color:'#808080',
					hoverable: true
					},
				legend: {
					show: true,
					margin: 10,
					backgroundOpacity: 0.8
				}
            };
        }



        else if (graph_type=="line") {
            var FlotOptions = {
                series: {
                    lines: { 
					  show: true,
					  fill: false
					},
                    points: {
						show:true,
						radius: 4
					},
                    shadowSize: 5
                },
 				grid:{
					show: true,
					color:'#808080',
					hoverable: true
					},
				legend: {
					show: true,
					margin: 10,
					backgroundOpacity: 0.8
				}

            };
        }
		
        else if (graph_type=="area") {
            var FlotOptions = {
                series: {
                    lines: { 
					  show: true,
					  fill: true
					},
                    points: {
						show:true,
						radius: 2
					},
                    shadowSize: 5
                },
				grid:{
					show: true,
					color:'#808080',
					hoverable: true
					},
				legend: {
					show: true,
					margin: 10,
					backgroundOpacity: 0.8
				}

            };
        }
	
        else if (graph_type=="bar") {
            var FlotOptions = {
                series: {
                    bars: {
					barWidth: 0.2,
					show: true,
					align:"left"
				}
                },
				grid:{
					show: true,
					color:'#808080',
					hoverable: true
					},
				legend: {
					show: true,
					margin: 10,
					backgroundColor:'#fff',
					backgroundOpacity: 0
				}
            };
        }
        
        
        
        /**Options for GraphTable will go here. For more details see:
        http://blog.rebeccamurphey.com/2007/12/17/graph-table-data-jquery-flot
        **/
        var TableOptions = {
            series: 'columns'
        };
        
     
        /**Initialize the graph and hide the table**/
        $(this).graphTable(TableOptions,FlotOptions).hide();
       $(".flot-graph").width("100%").height(250);
    });
	


// Create Graph using just FLOT
		//Data for Bar Graph
		$("#flot_bar").each(function () {
		
		var data1 = [[0, 5], [2, 7], [4, 11], [6, 1], [8, 8], [10, 7], [12, 9], [14, 3], [16, 5], [18, 7], [20, 11], [22, 1], [24, 8], [26, 7], [28, 9], [30, 3]];
		var data2 = [[1, 3], [3, 8], [5, 5], [7, 13], [9, 8], [11, 5], [13, 8], [15, 5], [17, 3], [19, 8], [21, 5], [23, 13], [25, 8], [27, 5], [29, 8], [31, 5]];
		$.plot($(this), 
		[
			{	
	
				data: data1,
				color:'#88BCFF',
				bars: { 
					label:'Data 1',
					show: true,
					fill: true,
					lineWidth: 0,
					border:false
					}
			},		
			{	
	
				color:'#9440ed',
				data: data2,
				bars: {
					label:'Data 2',
					show: true,
					fill: true,
					lineWidth: 0,
					border:false
					}
			}
		],  
			{
				grid:{
					show: true,
					color:'#808080',
					hoverable: true
					},
				legend: {
					show: true,
					margin: 10,
					backgroundOpacity: 0.5
				}
			}
		);
		});
	


//Calendar

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();



        var calendar = $('#calendar').fullCalendar({
                header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                        var title = prompt('Event Title:');
                        if (title) {
                                calendar.fullCalendar('renderEvent', {
                                        title: title,
                                        start: start,
                                        end: end,
                                        allDay: allDay
                                }, true // make the event "stick"
                                );

                        }
                        calendar.fullCalendar('unselect');
                },
                events: [{
                        title: 'All Day Event',
                        start: new Date(y, m, 1)
                }, {
                        title: 'Long Event',
                        start: new Date(y, m, d - 5),
                        end: new Date(y, m, d - 2)
                }, {
                        id: 999,
                        title: 'Repeating Event',
                        start: new Date(y, m, d - 3, 16, 0),
                        allDay: false
                },

                {
                        id: 999,
                        title: 'Repeating Event',
                        start: new Date(y, m, d + 4, 16, 0),
                        allDay: false
                },

                {
                        title: 'Meeting',
                        start: new Date(y, m, d, 10, 30),
                        allDay: false
                }, {
                        title: 'Lunch',
                        start: new Date(y, m, d, 12, 0),
                        end: new Date(y, m, d, 14, 0),
                        allDay: false
                }, {
                        title: 'Birthday Party',
                        start: new Date(y, m, d + 1, 19, 0),
                        end: new Date(y, m, d + 1, 22, 30),
                        allDay: false
                }, {
                        title: 'Click for Google',
                        start: new Date(y, m, 28),
                        end: new Date(y, m, 29),
                        url: 'http://google.com/'
                }]
        });


//Show/Hide Navigation for iPhone
        $('.nav_toggle').each(function () {
                $(this).toggle(function () {
                        $(this).toggleClass("swap");
                        $('#main_nav').slideToggle();
                }, function () {
                        $(this).toggleClass("swap");
                        $('#main_nav').slideToggle();
                });
        });



// user menu
        var user_menu = $("#user_menu .menu");
        var user_menu_btn = $("#user_menu .menu_btn");



        $(user_menu_btn).click(function () {
                if ($(user_menu).is(':hidden')) $(user_menu).stop(true, true).slideDown(200); //Slide down menu
                else {
                        $(user_menu).fadeOut();
                }
                return false;
        });

        $(user_menu).click(function (e) {
                e.stopPropagation();
        });


        $(document).click(function () {
                $(user_menu).fadeOut();
        });


//Tabs
       $('#user_menu ul.tabs').each(function () {
                var pane = $(this).closest(".menu").find(".pane");
                $(this).tabs(pane, {
                        effect: 'fade'
                });
        });



//Expandable Tables 
        $('.expandable').each(function () {
                $(this).find("tr:odd").addClass("odd");
                $(this).find("tr:not(.odd)").hide().addClass("grid_dropdown");
                $(this).find("thead tr").show();
                $(this).find("tr.odd").click(function () {
                        $(this).toggleClass("active");
                        $(this).next("tr").toggle();
                        $(this).find(".toggle").toggleClass("collapse");
                });
        });



// Data Table
        $('.dataTable').dataTable({
                "sPaginationType": "full_numbers",
                "bAutoWidth": false //setting width to 100%
        });


// Data tables that can be exported and printed									
        $('.dataTable_exportable').dataTable({
                "sPaginationType": "full_numbers",
                "sDom": 'Tlfrtip',
                "bAutoWidth": false,
                "oTableTools": {
                        "sSwfPath": "js/libs/copy_cvs_xls_pdf.swf"
                }
        });


//Add Title and Tooltips to individual Buttons
        $('.DTTT_button_copy').addClass('enable_tip').attr("title", "Copy Table to Clipboard");
        $('.DTTT_button_csv').addClass('enable_tip').attr("title", "Export to CSV");
        $('.DTTT_button_xls').addClass('enable_tip').attr("title", "Export to Excel");
        $('.DTTT_button_pdf').addClass('enable_tip').attr("title", "Export to PDF");
        $('.DTTT_button_print').addClass('enable_tip').attr("title", "Print Table");


//Close button:
        $(".close_notification").click(

        function () {
                $(this).hide();
                $(this).parent().fadeTo('fast', 0, function () {
                        $(this).slideUp('fast');
                });

        });



//Date Picker
        $(".dateinput").dateinput();



//Range Input
        $(".rangeinput").rangeinput({
                progress: true
        });



//Toggle Buttons

        $('.on_off').each(function () {
                $(this).ezMark({
                        checkboxCls: 'itoggle',
                        checkedCls: 'itoggle_off'
                });
        });

//Expose on checkbox
        $('.expose input.on_off').change(function () {
                var box = $(this).closest(".box");
                if ($(this).is(':checked')) {
                        // checked  
                        $(box).expose({
                                closeOnEsc: false,
                                closeOnClick: false
                        });
                } else {
                        // un-checked  
                        $.mask.close();
                }
        });
		
		
		
//CL Editor 
        $('.wysiwyg').each(function () {
                $(this).cleditor({
                        width:"100%",
						height:"100%"
                });
        });
		
//Enable tooltip for Editor Buttons

        $('.cleditorButton').addClass('enable_tip')

// Modal on click
        $(".attach_modal[rel]").overlay({
                // disable this for modal dialog-type of overlays
                closeOnClick: true,
                mask: {
                        loadSpeed: 200,
                        opacity: 0.8
                }
        });



//Wizard
        $(".wizard").formToWizard({
                submitButton: 'SaveAccount'
        });


//Tabs in box header 
        $('.sub_nav').each(function () {
                var pane = $(this).closest(".box").find(".pane");
                $(this).tabs(pane, {
                        effect: 'fade'
                });
        });



//Vetrtical Navigation in sidebar
        $('ul.vertical_nav').each(function () {
                var pane = $(this).closest(".box").find(".pane");
                $(this).tabs(pane, {
                        effect: 'fade'
                });
                $('.current').parent().addClass("selected");
        });


//Horizontal Tabs 
        $('.box ul.tabs').each(function () {
                var pane = $(this).closest(".box").find(".pane");
                $(this).tabs(pane, {
                        effect: 'fade'
                });
        });
		
		
//Accordion
        $('.accordion').each(function () {
                var pane = $(this).find(".pane");
                $(this).tabs(pane, {
                        tabs: 'h2',
                        effect: 'slide'
                });
        });


//Show/Hide boxes on click 
        $('.toggle_visibility').each(function () {
                var body = $(this).closest(".box").find(".body");
                if ($(body).hasClass('collapsed')) {
                        $(body).hide();
                        $(this).addClass("swap");
                }
                $(this).toggle(function () {
                        $(this).toggleClass("swap");
                        $(body).slideToggle()
                }, function () {
                        $(this).toggleClass("swap");
                        $(body).slideToggle()
                });
        });




//Main Navigation
        $("ul.sf-menu").supersubs({
                minWidth: 12,
                // minimum width of sub-menus in em units 
                maxWidth: 27,
                // maximum width of sub-menus in em units 
                extraWidth: 1 // extra width can ensure lines don't sometimes turn over 
        }).superfish({
				delay: 200,
				// one second delay on mouseout 
				animation: {
					  opacity: 'show',
					  height: 'show'
				},
				// fade-in and slide-down animation 
				speed: 200,
				// faster animation speed 
				dropShadows: false // disable drop shadows 
        });
		
		
//Tooltip -- Show only for desktops 
if (!Modernizr.touch){                   //Test if browser is touch enabled
			  $('.enable_tip').tooltip({ 
					  opacity: 1.0,
					  effect: 'slide',
					  predelay: 200,
					  delay: 10,
					  position: 'top center',
					  layout: '<div><span class="arrow"></span></div>',
					  offset: [0, 0]
			  }).dynamic({
					  bottom: {
							  direction: 'down',
							  bounce: true
					  } //made it dynamic so it will show on bottom if there isnt space on the top
			  });
}  



//Validator
        $("#validate").validator({
                speed: 1000,
                offset: [0, 10],
                position: 'bottom left',
				relative: true,
                messageClass: 'validator',
                message: '<div><span class="arrow"/></div>'
        }).attr('novalidate', 'novalidate'); //Disable Browser Validation since we validate with jquery tools
        
		
//Pretty Photo Gallery
        $("a[rel^='prettyPhoto']").prettyPhoto({
                animation_speed: 'slow',
                slideshow: false,
                theme: 'facebook',
                //  theme: 'dark_rounded',    Uncomment this is if you are using the dark layout
                overlay_gallery: false,
                social_tools: false,
                deeplinking: false
        });
		
//Code Viewer
        $("pre").each(function () {
				codeType = $(this).attr('data-type');
				$(this).snippet(codeType,{style:"pablo" });
		});

});




$(window).load(function () { 
//Setup Media Queries and load configurations for different plugins
function setupMQ() {
	
	
		//For Tablet
		if (Modernizr.mq('screen and (min-width: 768px) and (max-width: 1024px)')) {


				
				$('.scrollable').jcarousel({
						visible: 8,
						scroll: 8
				});
				
				//Show menu and remove swap class
				$('#main_nav').show();
				$('.nav_toggle').removeClass("swap");
		}

		// For Phone
		else if (Modernizr.mq('screen and  (max-width: 767px)')) {
				//Scrollable
				$('.scrollable').jcarousel({
						visible: 3,
						scroll: 3
				});
				
				//Show menu and remove swap class
				$('#main_nav').hide();
				$('.nav_toggle').addClass("swap");

		}

		// For Desktops and Everything Else
		else {
				//Scrollable
				$('.scrollable').jcarousel({
						visible: 8,
						scroll: 8
				});

				//Show menu and remove swap class
			  $('#main_nav').show();
			  $('.nav_toggle').removeClass("swap");
		}

}
// Apply the MediaQuery function above
		setupMQ();

// Change MediaQuery on resize
        $(window).resize(function () {
			 setupMQ();
        });



//Preload CSS and Images
		$.preloadCssImages(); 

});