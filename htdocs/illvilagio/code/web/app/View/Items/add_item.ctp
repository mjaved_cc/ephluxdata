
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<?PHP echo $this->Html->link('Back to Menu', '/items/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 

			<!--<button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'divisions',
				action : 'index'
			})">Back To Event Listing<?php //echo __('Back to ' . $page_heading . ' Listing')      ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Item', array('id' => 'validate')); ?>
			<fieldset>
				<legend>Add Item<?php // echo __($page_heading)    ?></legend>
					<?php //pr(array_keys($item));exit();?>
				<p><?php echo $this->Form->input('Category', array('type' => 'select','options' => $item,'required' => 'required')); ?></p>	
				
				<p><?php echo $this->Form->input('Name', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('Arabic Name', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('Display Order', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('Price', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('Description', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->create('Upload', array('type' => 'file')); ?></p>
                <p><?php echo $this->Form->input('file', array('type' => 'file','id'=>'file')); ?></p>
			</fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit','value' =>'Upload','onClick'=>'return img_path()','escape' => true, 'class' => 'button green small','onclick'=>'imageValidate()'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>