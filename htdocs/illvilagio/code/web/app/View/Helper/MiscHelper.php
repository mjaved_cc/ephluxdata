<?php

class MiscHelper extends AppHelper {

	public function make_current_tab($controller) {
		
		return ($this->request->params['controller'] == $controller) ? 'class = "current"' : '';
	}

}