<div class="<?php echo $class; ?>">
	<strong>
		<img src="<?php echo $this->webroot; ?>img/iinfo_icon.png" alt="Information" width="28" height="29" class="icon" />
	</strong>

	<?php
	if (isset($message)) {

		echo $message;
	}
	?>

	<a href="#" class="close_notification" title="Click to Close">
		<img src="<?php echo $this->webroot; ?>img/close_icon.gif" width="6" height="6" alt="Close" /></a>

</div>
