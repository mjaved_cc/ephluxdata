<!--================== admin panel date time picker===========-------->
<?php echo $this->Html->script('admin/ImageTypeValidation'); ?>
<?php // echo $this->Html->script('libs/jquery-1.10.1.min') ?>
<?php //pr($tables_floor); ?>
<?php echo $this->Html->script('libs/jquery-ui.min'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-timepicker-addon'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-sliderAccess'); ?>
<?php echo $this->Html->script('admin/timepicker'); ?>
<!---------------===================== END ================------------------------>
<?php //pr($udid);?>


<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Edit') ?></h2>
			<?PHP echo $this->Html->link('Back to View', '/reservations/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
			<!--			<button class="button dark" data-target="register" onclick="App.redirect({
							controller : 'divisions',
							action : 'index'
						})"> Edit Event<?php // echo __('Back to ' . $page_heading . ' Listing')   ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Reservation', array('id' => 'validate')); ?>
			<fieldset>
				<legend>Reservation<?php // echo __($page_heading)     ?></legend>
				<p><?php echo $this->Form->input('no_of_children', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('no_of_oldage', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('email_address', array('required' => 'required', 'div' => false)); ?>*</p>
				<?php echo $this->Form->input('confirm_via_email', array('value' => '1', 'type' => 'hidden')); ?>
				<?php echo $this->Form->input('udid', array('value' => $udid, 'type' => 'hidden')); ?>
				<p><?php echo $this->Form->input('phone_no', array('required' => 'required', 'div' => false)); ?>*</p>
				<p><?php echo $this->Form->input('no_of_guest', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('time', array('required' => 'required', 'class' => 'datetime', 'type' => 'timestamp', 'div' => false)); ?>*</p>
				<p><?php //echo $this->Form->input('time_from', array('required' => 'required', 'div' => false,'class' => 'datetime','type' => 'timestamp'));  ?></p>
				<p><?php echo $this->Form->input('event_id', array('required' => 'required', 'div' => false, 'type' => 'text')); ?>*</p>

				<?php //echo $this->Form->checkbox('Model.field',array( 'type' => 'select', 'multiple' => 'checkbox','options' => array('Approved' => 'Approved') )); ?>
				<p><?php echo $this->Form->input('floor_id', array('required' => 'required', 'div' => false,'value'=>'0' , 'type' => 'hidden')); ?></p>
				<p><?php //echo $this->Form->input('table_id', array('required' => 'required', 'div' => false, 'type' => 'text')); ?></p>

				<p>	 <!-- <label>Ground Floor</label>-->
 Change Status </p>
 <?php 
 $options = array('Pending' => 'Pending', 'Approved' => 'Approved', 'Rejected' => 'Rejected');
$attributes = array('legend' => false);
echo $this->Form->radio('status', $options, $attributes); ?>
					<?php //echo $this->Form->checkbox('status', array('value' => 'reserved', 'type'=>'radio')); ?>  
					
					<?php echo $this->Form->input('Reservation_tables', array(
						  'type' => 'select',
						  'multiple' => 'checkbox',
						  'options' => array('MT1' => 'MT1', 'MT2' => 'MT2', 'MT3' => 'MT3', 'MT4' => 'MT4', 'MT5' => 'MT5', 'MT6' => 'MT6', 'MT7' => 'MT7', 'MT8' => 'MT8', 'MT9' => 'MT9', 'MT10' => 'MT10', 'GT1' => 'GT1', 'GT2' => 'GT2', 'GT3' => 'GT3', 'GT4' => 'GT4', 'GT5' => 'GT5', 'GT6' => 'GT6', 'GT7' => 'GT7', 'GT8' => 'GT8', 'GT9' => 'GT9', 'GT10' => 'GT10'), // array ( (int)id => string(label), ... )
						  'selected' => $tables_floor
					));	?>



<?php
//echo $this->Form->checkbox('status', array('value' => 'reserved','multiple' => 'checkbox'));?><?php


				
					/*
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT1','multiple' => 'checkbox'));?> MT1<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT2','multiple' => 'checkbox'));?> MT2<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT3','multiple' => 'checkbox','checked'));?> MT3<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT4','multiple' => 'checkbox','checked'));?> MT4<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT5','multiple' => 'checkbox'));?> MT5<?php
					 */
					?>

				<p>	<?php
					/*
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT6','multiple' => 'checkbox'));?> MT6<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT7','multiple' => 'checkbox'));?> MT7<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT8','multiple' => 'checkbox'));?> MT8<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT9','multiple' => 'checkbox','checked'));?> MT9<?php
					  echo $this->Form->checkbox('Reservation', array('value' => 'MT10','multiple' => 'checkbox'));?> MT10<?php
					 */
					?></p>



			</fieldset>
<?php
echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
?>
			<?php echo $this->Form->end(); 
			//$this->redirect(array('controller' => 'reservations', 'action' => 'index'));
?>
		</div>
	</div>
</section>