<!--================== admin panel date time picker===========-------->
<?php echo $this->Html->script('admin/ImageTypeValidation'); ?>
<?php //echo $this->Html->script('libs/jquery-1.10.1.min') ?>



<?php echo $this->Html->script('libs/jquery-ui.min'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-timepicker-addon'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-sliderAccess'); ?>
<?php echo $this->Html->script('admin/timepicker'); ?>
<!---------------===================== END ================------------------------>




<?php if (isset($invalid_time)) { ?>

<script>
$(document).ready(function(){
 
    $("#timefrom").css("display","inline");

});
</script>


	<?php
}
?> 


<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
<?PHP echo $this->Html->link('Back to Reservation', '/reservations/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 

			<!--<button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'divisions',
				action : 'index'
			})">Back To Event Listing<?php //echo __('Back to ' . $page_heading . ' Listing')       ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

<?php echo $this->Form->create('Reservation', array('id' => 'validate')); ?>
			<fieldset>
				<legend>Reservation<?php // echo __($page_heading)    ?></legend>
				<p><?php echo $this->Form->input('no_of_children', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('no_of_oldage', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('email_address', array('required' => 'required', 'div' => false)); ?>*</p>
<?php echo $this->Form->input('confirm_via_email', array('value' => '1', 'type' => 'hidden')); ?>
				<p><?php echo $this->Form->input('phone_no', array('required' => 'required', 'div' => false)); ?>*</p>
				<p><?php echo $this->Form->input('no_of_guest', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('time', array('class' => 'datetime', 'type' => 'timestamp', 'div' => false)); ?>*</p>
				<p><?php echo $this->Form->input('time_from', array('div' => false, 'class' => 'datetime', 'type' => 'timestamp')); ?>*<span id="timefrom" style="display: none; color:red;">Time is not valid</span></p>
				<p><?php echo $this->Form->input('event_id', array('required' => 'required', 'div' => false, 'type' => 'text')); ?>*</p>
				<p><?php echo $this->Form->input('floor_id', array('required' => 'required', 'div' => false, 'value' => '0', 'type' => 'hidden')); ?>*</p>
				<p>	<label>Ground Floor</label>

<?php echo $this->Form->checkbox('table_id', array('value' => 'GT1', 'multiple' => 'checkbox')); ?> GT1<?php echo $this->Form->checkbox('table_id1', array('value' => 'GT2', 'multiple' => 'checkbox')); ?> GT2<?php echo $this->Form->checkbox('table_id2', array('value' => 'GT3', 'multiple' => 'checkbox')); ?> GT3<?php echo $this->Form->checkbox('table_id3', array('value' => 'GT4', 'multiple' => 'checkbox')); ?> GT4<?php echo $this->Form->checkbox('table_id4', array('value' => 'GT5', 'multiple' => 'checkbox')); ?> GT5<?php
?></p>

				<p>	<?php echo $this->Form->checkbox('table_id5', array('value' => 'GT6', 'multiple' => 'checkbox')); ?> GT6<?php echo $this->Form->checkbox('table_id6', array('value' => 'GT7', 'multiple' => 'checkbox')); ?> GT7<?php echo $this->Form->checkbox('table_id7', array('value' => 'GT8', 'multiple' => 'checkbox')); ?> GT8<?php echo $this->Form->checkbox('table_id8', array('value' => 'GT9', 'multiple' => 'checkbox')); ?> GT9<?php echo $this->Form->checkbox('table_id9', array('value' => 'GT10', 'multiple' => 'checkbox')); ?> GT10<?php
?></p>	<label>Mezzanine Floor</label>

				<p>	<?php echo $this->Form->checkbox('table_id10', array('value' => 'MT1', 'multiple' => 'checkbox')); ?> MT1<?php echo $this->Form->checkbox('table_id11', array('value' => 'MT2', 'multiple' => 'checkbox')); ?> MT2<?php echo $this->Form->checkbox('table_id12', array('value' => 'MT3', 'multiple' => 'checkbox')); ?> MT3<?php echo $this->Form->checkbox('table_id13', array('value' => 'MT4', 'multiple' => 'checkbox')); ?> MT4<?php echo $this->Form->checkbox('table_id14', array('value' => 'MT5', 'multiple' => 'checkbox')); ?> MT5<?php
?></p>

				<p>	<?php echo $this->Form->checkbox('table_id15', array('value' => 'MT6', 'multiple' => 'checkbox')); ?> MT6<?php echo $this->Form->checkbox('table_id16', array('value' => 'MT7', 'multiple' => 'checkbox')); ?> MT7<?php echo $this->Form->checkbox('table_id17', array('value' => 'MT8', 'multiple' => 'checkbox')); ?> MT8<?php echo $this->Form->checkbox('table_id18', array('value' => 'MT9', 'multiple' => 'checkbox')); ?> MT9<?php echo $this->Form->checkbox('table_id19', array('value' => 'MT10', 'multiple' => 'checkbox')); ?> MT10<?php ?></p>

			</fieldset>
					<?php
					echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
					?>
					<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>
