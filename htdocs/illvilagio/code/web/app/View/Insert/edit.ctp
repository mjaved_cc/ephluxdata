<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Edit') ?></h2>
			<?PHP echo $this->Html->link('Back to View', '/inserts/view', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
			<!--			<button class="button dark" data-target="register" onclick="App.redirect({
							controller : 'divisions',
							action : 'index'
						})"> Edit Event<?php // echo __('Back to ' . $page_heading . ' Listing')  ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Division', array('id' => 'validate')); ?>
			<fieldset>
				<legend>Event<?php // echo __($page_heading)  ?></legend>
				<p><?php echo $this->Form->input('id'); ?></p>
				<p><?php echo $this->Form->input('name', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('description', array('div' => false)); ?></p>
			</fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>