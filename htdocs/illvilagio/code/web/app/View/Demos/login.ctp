<?php
if (isset($api_response)) {

	$response = json_decode($api_response);

	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}

	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>
<!--========= Begin Forms  =========-->

<!-- Begin Box -->
<div id="login_container" class="box" style="width: 350px; height: 325px;">
	<!-- FORM FOR LOGIN -->   
		<?php echo $this->Form->create('User',array('class'=>'login wf active validate')); ?>
	<!--< class="login wf active validate" >-->
		<!-- Begin Header -->
		<div class="header clearfix">
			<h2>Login</h2>
			<?PHP echo $this->Html->link('Create A New Account', '/demos/signup', array('controller' => 'demos', 'action' => 'signup', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
		</div>

		<!-- End Header -->

		<div class="body"><!-- Begin Body-->


			<!--========= Form Fields  =========-->
		

			<p> <?php echo $this->Form->input('username', array('label' => 'Email','type'=>'email','class' => 'large', 'div' => false, 'required' => 'required ')); ?> </p>
			<p> <?php echo $this->Form->input('password', array('class' => ' large', 'div' => false, 'required' => 'required')); ?> </P>
			<p> </p>

			<div class="remember_password clearfix">
				<input type="checkbox" class="on_off"/> 
				Remember Password
			</div>
				<div class="clear"></div>


		</div> <!-- End Body -->

		<!-- Begin Footer -->
		<div class="footer">
			<?php
		//	 echo $this->Form->submit('Create Account', array('class' => 'button green large', 'div' => false));
			echo $this->Form->submit('Login', array('class' => 'button green small', 'div' => false));
//				. '  ' . 
//		$this->Html->image("login_button_facebook.png", array(
//			"alt" => "facebook",
//			'id' => 'facebook',
//			'url' => array('controller' => 'demos', 'action' => 'login_with_facebook') 
//		))
//		. '  ' .
//		$this->Html->image("login_button_twitter.png", array(
//			"alt" => "facebook",
//			'id' => 'facebook',
//			'url' => array('controller' => 'demos', 'action' => 'login_with_twitter')
//		));
			?>
			<!--<button type="submit"  class="button green large">Login</button>-->
			<?PHP echo $this->Html->link('Forgot Password?', '/demos/forgot_password', array('controller' => 'demos', 'action' => 'signup', 'class' => 'linkform fr', 'data-target' => 'register')); ?> 
			<!--//<a href="#" data-target="forgot_password" class="linkform fr"></a>-->
		</div>
		<!-- End Footer -->                


