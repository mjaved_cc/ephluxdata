<?php
if (!isset($request_variable)) {
	$request_variable = '';
}
if (isset($api_response)) {
	$response = json_decode($api_response);

	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}

	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>


<div id="login_container" class="box" style="width: 400px; height: 310px;">
	<!-- FORM FOR FORGOT PASSWORD -->
	<form class="forgot_password validate active">

		<!-- Begin Header -->
		<div class="header clearfix">
			<h2>Reset Password</h2>
			<?PHP echo $this->Html->link('Create A New Account', '/demos/signup', array('controller' => 'demos', 'action' => 'signup', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
		</div>
		<!-- End Header -->

		<div class="body"><!-- Begin Body-->

			<!--========= Form Fields  =========-->
			<?php echo $this->Form->create('User'); ?>
			<p> <?php echo $this->Form->input('password', array('class' => 'textfield large', 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'class' => 'textfield large', 'label' => __('Confirm Password'), 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('hidden', array('value' => $request_variable, 'type' => 'hidden', 'div' => false)); ?> </p>
			<div class="clear"></div>


		</div> <!-- End Body -->

		<!-- Begin Footer -->
		<div class="footer">
			<?php echo $this->Form->submit('Reset Password', array('class' => 'button green large', 'div' => false)); ?> </p>
			
			<?PHP echo $this->Html->link('Back to Login', '/demos/login', array('controller' => 'demos', 'action' => 'login', 'class' => 'linkform fr', 'data-target' => 'register')); ?> 
		</div>
		<!-- End Footer -->                

	



