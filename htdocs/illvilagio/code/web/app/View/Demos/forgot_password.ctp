
<?php
if (isset($api_response)) {
	
	$response = json_decode($api_response);
	
	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}
	
	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>

<div id="login_container" class="box" style="width: 400px; height: 314px;">
<!-- FORM FOR FORGOT PASSWORD -->
            <form class="forgot_password validate active">
      
				<!-- Begin Header -->
				<div class="header clearfix">
                  <h2>Forgot Password</h2>
                  <?PHP echo $this->Html->link('Create A New Account', '/demos/signup', array('controller' => 'demos', 'action' => 'signup', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
				</div>
				<!-- End Header -->
					
					<div class="body"><!-- Begin Body-->
						
					<!--========= Form Fields  =========-->
                    <fieldset>
						<legend>Please Tell us your email address</legend>

						<p>
						  <label>Email Address:</label>
                          <small>We will send you a new password at this email Address</small>
						  <?php echo $this->Form->create('User'); ?>
		<p> <?php echo $this->Form->input('Email', array( 'class' => 'textfield large', 'div' => false)); ?> </p>
						</p>

                       </fieldset>
<div class="clear"></div>


				</div> <!-- End Body -->
                
                <!-- Begin Footer -->
                <div class="footer">
                  <button name="button" type="submit" class="button dark" >Reset Password</button>
                  <?PHP echo $this->Html->link('Back to Login', '/demos/login', array('controller' => 'demos', 'action' => 'login', 'class' => 'linkform fr', 'data-target' => 'register')); ?> 
                </div>
                <!-- End Footer -->                
                
	  </form>                
			</div><!-- End Box -->



