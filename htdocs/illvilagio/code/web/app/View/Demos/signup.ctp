<?php
if (isset($api_response)) {

	$response = json_decode($api_response);

	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}

	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
}
?>
<!--========= Begin Forms  =========-->

<!-- Begin Box -->
<div id="login_container" class="box" style="width: 400px; height: 550px;">
	<form class="register validate active">

		<!-- Begin Header -->
		<div class="header clearfix">
			<h2>Register Here</h2>
		</div>
		<!-- End Header -->

		<div class="body"><!-- Begin Body-->
			<!--========= Form Fields  =========-->
			<?php echo $this->Form->create('User'); ?>

			<p> <?php echo $this->Form->input('first_name', array('class' => 'textfield large', 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('last_name', array('class' => 'textfield large', 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('username', array('label' => __('Email'), 'class' => 'textfield large', 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('password', array('class' => 'textfield large', 'div' => false)); ?> </p>
			<p> <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'class' => 'textfield large', 'div' => false)); ?> </p>

			<div class="clear"></div>


		</div> <!-- End Body -->

		<!-- Begin Footer -->
		<div class="footer">
			<p> <?php echo $this->Form->submit('Create Account', array('class' => 'button green large', 'div' => false)); ?> </p>

			<?PHP echo $this->Html->link('Login here', '/demos/login', array('controller' => 'demos', 'action' => 'login', 'class' => 'linkform fr', 'data-target' => 'register')); ?> 

		</div>
		<!-- End Footer -->                
	</form>

