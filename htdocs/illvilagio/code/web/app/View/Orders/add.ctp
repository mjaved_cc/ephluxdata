
<?php
//echo $this->Form->end('Submit');       
//pr($item);
?>


<?php echo $this->Html->script('admin/jquery.sheepItPlugin'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />





		<script type="text/javascript">


			$(document).ready(function() {
     
				var sheepItForm = $('#sheepItForm').sheepIt({
					separator: '',
					allowRemoveLast: true,
					allowRemoveCurrent: true,
					allowRemoveAll: true,
					allowAdd: true,
					allowAddN: true,
					maxFormsCount: 10,
					minFormsCount: 0,
					iniFormsCount: 0,
					pregeneratedForms: ['pregenerated_form_1'] // Ids
				});
	
				$(".button").click(function(){
									
					
					var telno = $('#customerDetailTelephoneNo').val();
					var qty = $('#sheepItForm_0_phone').val(); 
					var orderbypeople = $('#OrderNoSeat').val();
					
					
					//var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
					var pattern =/[0-9,]+/;
					if(pattern.test(telno)==false){
			
						alert('Please enter valid phone no');
						return false;
					}
					if(pattern.test(qty)==false){
			
						alert('Quantity not valid');
						return false;
					}
					if(pattern.test(orderbypeople)==false){
			
						alert('Order By People not valid');
						return false;
					}
					
					
					
				});
 
			});



		</script>
		<style>



			a {
				text-decoration:underline;
				color:#00F;
				cursor:pointer;
			}



			#sheepItForm_controls div, #sheepItForm_controls div input {
				float:left;    
				margin-right: 10px;
			}


			a {
				text-decoration:underline;
				color:#00F;
				cursor:pointer;
			}


		</style>


	</head>


	<body>


		<section class="grid_12">
			<!-- begin box -->
			<div class="box">
				<!-- Box Header -->
				<div class="header clearfix">
					<h2><?php //echo __('Add')   ?></h2>

					<?PHP //echo// $this->Html->link('Back to Order', '/orders/view', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 

					<!--<button class="button dark" data-target="register" onclick="App.redirect({
						controller : 'divisions',
						action : 'index'
					})">Back To Event Listing<?php //echo __('Back to ' . $page_heading . ' Listing')             ?></button>-->
				</div>
				<!-- End Header -->

				<div class="body">

					<?php echo $this->Form->create('', array('id' => 'validate', 'action' => 'add', 'value' => 'Post')); ?>
					<table  style="float:left; width:350px; align:'left'">


						<fieldset>
							<legend>Order<?php // echo __($page_heading)           ?></legend>
							<th> Order Details</th>
							<tr><td><label><p>Order Type</p></td></label>
								<td><?php
					$options = array('Dine In' => 'Dine In', 'Take Away' => 'Take Away', 'Delivery' => 'Delivery');
					$attributes = array('legend' => false, 'div' => false, 'name' => 'order_type');
					echo $this->Form->radio('gender', $options, $attributes);
					?></td></tr>
							<tr><td><label>Order By People</label></td>
								<td><?php echo $this->Form->input('no_seat', array('required' => 'required', 'name' => 'no_seats', 'div' => false, 'label' => false)); ?>*</td></tr>
							<tr>
<!--								<td><label>Status</label></td>-->
								<td><?php echo $this->Form->input('status', array('type'=>'hidden', 'value'=>'1','name' => 'status', 'div' => false, 'label' => false)); ?></td></tr>



							<?php echo $this->Form->input('order_table_id', array('required' => 'required', 'type' => 'hidden', 'value' => '1', 'name' => 'order_table_id', 'class' => 'datetime', 'div' => false, 'label' => false)); ?>


							<?php echo $this->Form->input('FloorId', array('required' => 'required', 'type' => 'hidden', 'value' => '1', 'name' => 'FloorId', 'div' => false, 'label' => false)); ?>
							<td><?php echo $this->Form->input('FloorId', array('required' => 'required', 'type' => 'hidden', 'name' => 'entryDateTime', 'value' => date('Y-m-d'), 'div' => false, 'label' => false)); ?>
								<?php echo $this->Form->input('FloorId', array('required' => 'required', 'type' => 'hidden', 'name' => 'udid', 'value' => '123456789', 'div' => false, 'label' => false)); ?>
								</table>



								<table  style="float:left; width:350px">
									<th> Customer Details</th>
									<tr><td><label>Name</label></td>
										<td><?php echo $this->Form->input('customerDetail.name', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
									<tr><td><label>Telephone No</label></td>
										<td><?php echo $this->Form->input('customerDetail.telephoneNo', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
									<tr><td><label>Email Address</label></td>
										<td><?php echo $this->Form->input('customerDetail.email', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
									<tr><td><label>Address</label></td>
										<td><?php echo $this->Form->input('customerDetail.shippingAddress', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>




								</table>



								<?php echo $this->Form->checkbox('table_id', array('value' => 'GT1', 'multiple' => 'checkbox')); ?> GT1<?php echo $this->Form->checkbox('table_id1', array('value' => 'GT2', 'multiple' => 'checkbox')); ?> GT2<?php echo $this->Form->checkbox('table_id2', array('value' => 'GT3', 'multiple' => 'checkbox')); ?> GT3<?php echo $this->Form->checkbox('table_id3', array('value' => 'GT4', 'multiple' => 'checkbox')); ?> GT4<?php echo $this->Form->checkbox('table_id4', array('value' => 'GT5', 'multiple' => 'checkbox')); ?> GT5<?php
								?></p>



								<p>	<?php echo $this->Form->checkbox('table_id5', array('value' => 'GT6', 'multiple' => 'checkbox')); ?> GT6<?php echo $this->Form->checkbox('table_id6', array('value' => 'GT7', 'multiple' => 'checkbox')); ?> GT7<?php echo $this->Form->checkbox('table_id7', array('value' => 'GT8', 'multiple' => 'checkbox')); ?> GT8<?php echo $this->Form->checkbox('table_id8', array('value' => 'GT9', 'multiple' => 'checkbox')); ?> GT9<?php echo $this->Form->checkbox('table_id9', array('value' => 'GT10', 'multiple' => 'checkbox')); ?> GT10<?php
								?></p>	<label>Mezzanine Floor</label>



								<p>	<?php echo $this->Form->checkbox('table_id10', array('value' => 'MT1', 'multiple' => 'checkbox')); ?> MT1<?php echo $this->Form->checkbox('table_id11', array('value' => 'MT2', 'multiple' => 'checkbox')); ?> MT2<?php echo $this->Form->checkbox('table_id12', array('value' => 'MT3', 'multiple' => 'checkbox')); ?> MT3<?php echo $this->Form->checkbox('table_id13', array('value' => 'MT4', 'multiple' => 'checkbox')); ?> MT4<?php echo $this->Form->checkbox('table_id14', array('value' => 'MT5', 'multiple' => 'checkbox')); ?> MT5<?php ?></p>



								<p>	<?php echo $this->Form->checkbox('table_id15', array('value' => 'MT6', 'multiple' => 'checkbox')); ?> MT6<?php echo $this->Form->checkbox('table_id16', array('value' => 'MT7', 'multiple' => 'checkbox')); ?> MT7<?php echo $this->Form->checkbox('table_id17', array('value' => 'MT8', 'multiple' => 'checkbox')); ?> MT8<?php echo $this->Form->checkbox('table_id18', array('value' => 'MT9', 'multiple' => 'checkbox')); ?> MT9<?php echo $this->Form->checkbox('table_id19', array('value' => 'MT10', 'multiple' => 'checkbox')); ?> MT10<?php
								?></p>




								<!-- sheepIt Form -->
								<div id="sheepItForm">



									<!-- Form template-->
									<div id="sheepItForm_template">     
										<label for="sheepItForm_#index#_phone">Item <span id="sheepItForm_label"></span></label>
										<?php echo $this->Form->input('', array('id' => "sheepItForm_#index#_phone", 'name' => "items[#index#][itemId]", 'type' => 'select', 'options' => $item, 'required' => 'required', 'empty' => 'Select Item')); ?>
									   <!--<input id="sheepItForm_#index#_phone" name="person[phones][#index#][item]" type="text" size="15" maxlength="10" />-->
										<label for="sheepItForm_#index#_phone">Comment<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][comment]" type="text" size="15" maxlength="10" />
										<label for="sheepItForm_#index#_phone">Quantity<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][quantity]" type="text" size="15" maxlength="10" />
										<label for="sheepItForm_#index#_phone">Seat No.<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][seatNo]" type="text" size="15" maxlength="10" />
										<a id="sheepItForm_remove_current"><img class="delete" src="<?php echo $this->webroot; ?>img/cross.png" width="16" height="16" border="0"></a>
									</div>
									<!-- /Form template-->

									<!-- Pre-generated form -->
									<div id="pregenerated_form_1">
										<label for="sheepItForm_#index#_phone">Item <span id="sheepItForm_label"></span></label>
								<!--		<select id="sheepItForm_#index#_phone" name="items[#index#][item]" >
								   <option   ><?php $item ?></option>
								  <option value="saab">Saab</option>
								  <option value="mercedes">Mercedes</option>
								  <option value="audi">Audi</option>
								</select> -->

										<?php echo $this->Form->input('', array('id' => "sheepItForm_#index#_phone", 'name' => "items[#index#][itemId]", 'type' => 'select', 'options' => $item, 'required' => 'required', 'empty' => 'Select Item')); ?>
<!--<input id="sheepItForm_#index#_phone" name="person[phones][#index#][item]" type="text" size="15" maxlength="10" />-->
										<label for="sheepItForm_#index#_phone">Comment<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][comment]" type="text" size="15" maxlength="10" />
										<label for="sheepItForm_#index#_phone">Quantity<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][quantity]" type="text" size="15" maxlength="10" />
										<label for="sheepItForm_#index#_phone">Seat No.<span id="sheepItForm_label"></span></label>
										<input id="sheepItForm_#index#_phone" required ="required" name="items[#index#][seatNo]" type="text" size="15" maxlength="10" />

										<a id="sheepItForm_remove_current"><img class="delete" src="<?php echo $this->webroot; ?>img/cross.png" width="16" height="16" border="0"></a>
									</div>
									<!-- /Pre-generated form -->
									<!-- No forms template -->
									<div id="sheepItForm_noforms_template">No Item</div>
									<!-- /No forms template-->

									<!-- Controls -->
									<div id="sheepItForm_controls">
										<div id="sheepItForm_add"><a><span>Add Item</span></a></div>
										<div id="sheepItForm_remove_last"><a><span>Remove</span></a></div>
								<!--        <div id="sheepItForm_remove_all"><a><span>Remove all</span></a></div>-->
										<!--        <div id="sheepItForm_add_n">
												  <input id="sheepItForm_add_n_input" type="text" size="4" />
												  <div id="sheepItForm_add_n_button"><a><span>Add</span></a></div></div>-->
									</div>
									<!-- /Controls -->

								</div>
								<!-- /sheepIt Form -->

						</fieldset>

						<br clear="all"></br>
				</div>
				<table  style="float:right ; padding-right:50px" >
					<?php
					echo $this->Form->button('Submit', array('type' => 'submit', 'onClick' => 'return img_path()', 'escape' => true, 'class' => 'button green small', 'onclick' => 'imageValidate()'));
					echo $this->Form->end();
					?>
				</table>
			</div>
		</section>








	</body>

</html>
