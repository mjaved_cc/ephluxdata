<?php
//pr ($order);
//pr ($info);
//pr($orders);
//pr($floor);
//$pagination_params = $this->Paginator->params(); 
?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php // echo __($page_heading)      ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Order Details </h2> 

			<?PHP echo $this->Html->link('New Order', '/orders/add', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?>      <!-- <button class="button dark" data-target="register" onclick='history.back()'
  
  //App.redirect({
//				controller : 'Inserts',
//				action : 'add'
//			})">--> <?php //echo __('New ' . $page_heading)     ?></button>

		</div>
		<!-- End Header -->
		<div class="body">

			<?php echo $this->Form->create('Division', array('id' => 'validate')); ?>
			<table  style="float:left; width:270px; align:'left'">

				<fieldset>
					<legend>Order<?php // echo __($page_heading)           ?></legend>
					<th> Order Details</th>

					<tr><td><label>Order Time</label></td>
						<td><?php echo ($order['Order']['order_taketime']); ?></td></tr>
					<tr><td><label>Order Type</label></td>
						<td><?php echo ($order['Order']['ordertype']); ?></td></tr>
<!--					<tr><td><label>Status</label></td>
						<td><?php //echo ($order['Order']['order_status']);      ?></td></tr>-->
					<tr><td><label>Tabbed Amount</label></td>
						<td><?php echo ($order['Order']['order_tabbed_amount']); ?></td></tr>
					<tr><td><label>Amount Paid </label></td>
						<td><?php echo ($order['Order']['order_amount_paid']); ?></td></tr>
			</table>

			<table  style="float:left; width:270px">
				<th> Order Items</th>

				<?php foreach ($orders as $ordername) { ?>
					<tr><td><label>Order Item</label></td>
						<td><?php echo ($ordername['items']['name']); ?></td></tr>
					<tr><td><label>Quantity</label></td>
						<td><?php echo ($ordername['orders']['quantity']); ?></td></tr>

					<?php
				}
				?>
			</table>
			<table  style="float:left; width:270px">
				<th> Table </th>

				<?php foreach ($floor as $floors) { ?>
					<tr><td><label>Floor</label></td>
						<?php
						$floor_id = $floors['order_tables']['floor_id'];
						if ($floor_id == 1) {
							$floor = 'Ground';
						} elseif ($floor_id == 2) {
							$floor = ' Mezzanine';
						}
						?>			
						<td><?php echo $floor; ?></td></tr>
					<tr><td><label>Table No.</label></td>
						<td><?php echo ($floors['order_tables']['table_id']); ?></td></tr>

					<?php
				}
				?>
			</table>

			<table  style="float:left; width:270px">
				<th> Customer Details</th>
				<tr><td><label>Name</label></td>
					<td><?php echo ($info['CustomerInformation']['name']); ?></td></tr>
				<tr><td><label>Telephone No</label></td>
					<td><?php echo ($info['CustomerInformation']['telephone_no']); ?></td></tr>
				<tr><td><label>Email</label></td>
					<td><?php echo ($info['CustomerInformation']['email']); ?></td></tr>
				<tr><td><label>Address</label></td>
					<td><?php echo ($info['CustomerInformation']['address']); ?></td></tr>


			</table>
			</fieldset>

			<br clear="all"></br>
		</div>

	</div>
	<!-- End box body -->

	<!-- End box -->
</section>


<section id="facebox" class="grid_6"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Order Invoice</h2> 
		</div>
		<div class="body">

			<table class="display " id="new" >

				<thead>
					<tr>
						<th>Date</th></tr>







				</thead>
				<tbody>

					<tr><td><label> Date</label>
							<?php echo ($order['Order']['order_taketime']); ?></td></tr>
					<tr><td><label>Customer Name</label> 
						<?php echo ($info['CustomerInformation']['name']); ?></td></tr>	

									
					<tr><td><label> Order Id</label>
							<?php echo ($order['Order']['id']); ?></td></tr>	
					<tr><td><label> Order Type</label>
							<?php echo ($order['Order']['ordertype']); ?></td></tr>	
					<?php
							
							foreach ($orders as $orderi) {
								?>
					<tr><td><label> Order Items</label>
							
						<?php		echo ($orderi['items']['name']); ?></td></tr>	
					<tr><td><label> Quantity</label>
							<?php ($orderi['orders']['quantity']); ?></td></tr>	
					<tr><td><label> Price</label>
							<?php $price = ($orderi['items']['price']); 
					echo $price  ?></td></tr>
					<?php
	$total = $price + $price; 
						
					}
					?>
					<tr><td><label> Amount</label>
							<?php echo $total;  ?></td></tr>	
					</td>


				</tbody>
				<?php ?>
			</table>
			<?php //}
			?>

			<button class="close button blue"> Close </button>

			<!-- Close class on any element inside the modal box will close the modal. -->
		</div>
	</div> 
</section>

<section class="grid_12 clearfix">
	<!-- begin box -->
	<div class="box">
		<div class="body tac">
			<a href="#" class="button attach_modal light large" data-modal="#facebox"> Invoice</a>
		</div>
	</div>
</section>
