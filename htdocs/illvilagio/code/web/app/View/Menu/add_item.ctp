
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<?PHP echo $this->Html->link('Back to Menu', '/items/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 

			<!--<button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'divisions',
				action : 'index'
			})">Back To Event Listing<?php //echo __('Back to ' . $page_heading . ' Listing')      ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Menu', array('id' => 'validate')); ?>
			<fieldset>
				<legend>Add Item<?php // echo __($page_heading)    ?></legend>
					<?php //pr(array_keys($item));exit();?>
				<p><?php echo $this->Form->input('parent_id', array('type' => 'select','options' => $item,'required' => 'required')); ?></p>	
				
				<p><?php echo $this->Form->input('name', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('extra', array('required' => 'required', 'type' => 'hidden','value' => 'extra' , 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('price', array('required' => 'required', 'type' => 'text' , 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('is_item', array('required' => 'required', 'type' => 'hidden','value' => '1' , 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('name_arabic', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('Display_Order', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('description', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('description_arabic', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('side_line', array('div' => false, 'type'=>'checkbox')); ?></p>
				<p><?php echo $this->Form->input('filename',array('type' => 'file')); ?></p>
			</fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit','value' =>'Upload','onClick'=>'return img_path()','escape' => true, 'class' => 'button green small','onclick'=>'imageValidate()'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>