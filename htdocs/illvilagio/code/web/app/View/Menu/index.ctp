<!-- File: /app/View/Posts/index.ctp -->
<!--
<table>

	<tr>
	
	<td>  <?php // $url=$this->params['url']; print_r($url);   //print_r($test);  ?>  </td>
	</tr>

	<tr>
	<td>Users</td>
	</tr>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
    </tr>

     Here is where we loop through our $posts array, printing out post info 
	
<?php //foreach ($users as $post): ?>
    <tr>
        <td><?php //echo $post['User']['first_name'];  ?></td>
        <td>
<?php //echo $this->Html->link($post['User']['last_name'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?>
        </td>
        <td><?php //echo $post['User']['email'];  ?></td>
    </tr>
	
	
	
<?php //endforeach; ?>
<?php //unset($user); ?>

<?php //echo $this->Html->link('Add Post', array('controller' => 'users', 'action' => 'add')); ?>
</table>-->

<?php
//pr ($menu);
//$pagination_params = $this->Paginator->params(); 
?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php // echo __($page_heading)    ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Category Listings </h2> 

			<?PHP echo $this->Html->link('New Item', '/menus/add', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?>      <!-- <button class="button dark" data-target="register" onclick='history.back()'
  
  //App.redirect({
//				controller : 'Inserts',
//				action : 'add'
//			})">--> <?php //echo __('New ' . $page_heading)   ?></button>

		</div>
		<!-- End Header -->
		<div class="body">

			<table class="display dataTable">
				<thead>
					<tr>
						<th class="sorting">S.No<?php //echo $this->Paginator->sort('name');     ?></th>
						<th class="sorting">Name<?php //echo $this->Paginator->sort('name');     ?></th>
						<th class="sorting">Arabic Name<?php //echo $this->Paginator->sort('name');     ?></th>
						<th class="sorting">Image<?php //echo $this->Paginator->sort('name');     ?></th>
						<th class="sorting">Category<?php //echo $this->Paginator->sort('name');     ?></th>

						<th >Action<?php // echo __('Action');     ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$counter =1;
//					$current_page = $this->Paginator->counter(array('format' => ' %page%'));
//					$counter = ($current_page * 10) - 9;
					if (!empty($menu)) {
						foreach ($menu as $results) {
							
							?>
							<tr class="odd_gradeX">
								<td class="center"><?php echo $counter; ?>&nbsp;</td>
								<td class="center"><?php echo ($results['items']['name']); ?>&nbsp;</td>
								<td class="center"><?php echo ($results['items']['nameArabic']); ?>&nbsp;</td>
								<td class="center"><img src="<?php echo ($results['items']['imageUrl']); ?>" alt="Image Not Found" width="60" height="60" /></td>
								<td class="center"><?php echo ($results['Category']['name']); ?>&nbsp;</td>


								<td class="actions" align="center">
		<?php echo $this->Html->link(__('Details'), array('controller' => 'menus', 'action' => 'view_category', $results['items']['id'])); ?> |
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'menus', 'action' => 'edit', $results['items']['id'])); ?> |
									<?php
									echo $this->Html->link('Delete', array('action' => 'delete', $results['items']['id']), null, 'Are you sure ');
									$counter++;
									?>
								</td>
							</tr>

							<?php
						}
					} else {
						?>
						<tr>
							<td colspan="6">No records found</td>
						</tr>
						<?php
					}
					?>
			</table>
					<?php
//					if (!empty($menu)) {
//						echo '<table class="display"  >';
//						echo '<tr><td align="center">';
//						echo $this->paginator->first();
//						echo '&nbsp;';
//						echo $this->paginator->prev();
//						echo '&nbsp;';
//						echo $this->paginator->numbers(array('modulus' => '2'));
//						echo '&nbsp;';
//						echo $this->paginator->next();
//						echo '&nbsp;';
//						echo $this->paginator->last();
//						echo '<br />';
//						echo $this->paginator->counter(array('format' => 'Page %page% of %pages%'));
//
//						echo '</td></tr>';
//						echo '</table>';
//					}
					?>



			</tbody>
			</table>
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>