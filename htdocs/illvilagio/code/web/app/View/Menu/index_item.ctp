<?php
echo $this->Session->flash();
pr ($menus);
//$pagination_params = $this->Paginator->params(); 
?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php // echo __($page_heading)   ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Items Listings </h2> 

			<?PHP echo $this->Html->link('New Item', '/menus/add_item', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?>      <!-- <button class="button dark" data-target="register" onclick='history.back()'
  
  //App.redirect({
//				controller : 'Inserts',
//				action : 'add'
//			})">--> <?php //echo __('New ' . $page_heading)  ?></button>

		</div>
		<!-- End Header -->
		<div class="body">

			<table class="display dataTable">
				<thead>
					<tr>
						<th class="sorting">S No.<?php //echo $this->Paginator->sort('name');    ?></th>
						<th class="sorting">Name<?php //echo $this->Paginator->sort('name');    ?></th>
						<th class="sorting">Arabic Name<?php //echo $this->Paginator->sort('name');    ?></th>
						<th class="sorting">Image<?php //echo $this->Paginator->sort('name');    ?></th>
						<th class="sorting">Category<?php //echo $this->Paginator->sort('name');    ?></th>
						<th class="sorting">Price<?php //echo $this->Paginator->sort('name');    ?></th>

						<th >Action<?php // echo __('Action');    ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $counter = 1;
					
					foreach ($menus as $results): ?>
					<tr class="odd_gradeX">
						<td class="center"><?php echo $counter; ?>&nbsp;</td>
						<td class="center"><?php echo ($results['items']['name']); ?>&nbsp;</td>
						<td class="center"><?php echo ($results['items']['nameArabic']); ?>&nbsp;</td>
						<td class="center"><img src="<?php echo FULL_BASE_URL . Router::url('/') . 'app/webroot/gallery/'.$results['items']['imageUrl']  ?>" alt="Image Not Found" width="60" height="60" />
							
							</td>
						<td class="center"><?php echo ($results['Category']['name']); ?>&nbsp;</td>
						<td class="center"><?php echo ($results['items']['price']); ?>&nbsp;</td>
						

						<td class="actions" align="center">
							<?php echo $this->Html->link(__('Details'), array('controller' => 'menus', 'action' => 'view_item', $results['items']['id']));  ?> |
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit_item',$results['items']['id'])); ?> |
							<?php echo $this->Html->link('Delete', array('action' => 'delete_category', $results['items']['id']), null, 'Are you sure ' ) ?>
						</td>
					</tr>
					
					

					<?php  $counter++;
					endforeach;  ?>
				</tbody>
			</table>
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
