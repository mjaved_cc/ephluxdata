<!doctype html>

<!-- Coniditional CSS Hacks for IE. -->
<!--[if lt IE 8 ]> <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie"> <![endif]-->
<html lang="en" class="no-js"><!-- InstanceBegin template="/Templates/Template.dwt" codeOutsideHTMLIsLocked="false" -->
	<!--<![endif]-->

	<!--============================ HEAD ============================-->
	<head>
		<meta charset="utf-8">

		<!--  Title & meta tags -->
		<title>illvillagio Admin</title>


		<meta name="description" content="Mriya: Responsive Admin Theme for Web Applications and Backend Interfaces">
		<meta name="author" content="Cosmive">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta name="robots" content="noindex,nofollow" />

		<!--  Mobile viewport optimized -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!--  Favicons -->
		<link rel="shortcut icon"  href="favicon.ico" type="image/ico">
		<link rel="shortcut icon"  href="favicon.png" type="image/png">


		<!--  Icons for iPhone/iPad -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />



		<!--========= Fonts  =========-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600,400,700' rel='stylesheet' type='text/css'>
		
		<!--=========  CONFIG =========-->
			<?php echo $this->Html->script('script'); ?>		

		<!--========= CSS  =========-->



		<?php echo $this->Html->css('themes/jquery-ui-timepicker-addon') ?>
		<?php echo $this->Html->css('themes/jquery-ui') ?>
		<?php echo $this->Html->css('themes/core') ?>
		<?php echo $this->Html->css('themes/light/light') ?>
		<?php echo $this->Html->css('admin/uploadify'); ?>

		<?php //echo $this->Html->css('themes/dark/dark')?>
		<?php // echo $this->Minify->css($css_list); ?>

		<!--<link class="css" rel="stylesheet" href=" ">
		<link class="css" rel="stylesheet" href=" " > <!-- " title="light" Change your default stylesheet here-->
		<!--<link class="css" rel="alternate stylesheet" href="" title="dark" >
		
		
		<!--========= JS  =========-->
		<!-- All JavaScript at the bottom, except for Modernizr and style switcher -->
		<?php //echo $this->Html->script('libs/jquery-1.10.1.min') ?>

		<?php echo $this->Html->script('libs/jquery-1.6.4.min'); ?> 
		<?php // echo $this->Html->script('libs/jquery-1.10.1.min') ?>

		<?php echo $this->Html->script('admin/jquery.validate'); ?>

		<?php echo $this->Html->script('admin/additional-methods'); ?>
		<?php echo $this->Html->script('libs/modernizr-latest.min') ?>
		<?php echo $this->Html->script('libs/styleswitcher.min') ?>
	</head>

	<!--============================ BODY ============================-->
	<body>

		<!--============================ MAIN WRAPPER - required for sticky footer ============================-->
		<div class="wrapper">


			<!--============================ HEADER ============================-->
			<header id="header">
				<div class="container_12"> 


					<!--========= Logo =========--> 
					<a href="JavaScript:void(0);" class="logo ir">Logo Name</a> 

					<!--========= User menu =========-->
					<div id="user_menu">
						<a href="JavaScript:void(0);" class="menu_btn"><span class="user_id">Admin</span><span class=" ir arrow">Show/Hide Menu</span></a>
						<div class="menu">

							<!--Search Field-->
							<!--Search box-->

							<!--							<div class="search_container">
															<div class="search">
																<input name="" type="text">
																<a href="JavaScript:void(0);" class="search_icon ir enable_tip" title="Search">Search</a>
							
															</div>
														</div>-->

<!--							Logout Button
														<a href="JavaScript:void(0);" class="logout button red enable_tip" title="End this Session">LOGOUT</a>
							
														 Tabs 
														<ul class="tabs">
															<li><a class="enable_tip settings" title="Settings" ><span>Settings</span></a></li>
															<li><a class="enable_tip emails" title="Emails"><span>Emails</span></a></li>
															<li><a class="enable_tip themes" title="Theme"><span>Messages</span></a></li>
														</ul>-->

							<!-- Settings-->
							<!--							<div class="pane settings">
															<ul>
																<li><a href="JavaScript:void(0);"> <span class="icon password ir">Icon</span>Change Password</a></li>
																<li><a href="JavaScript:void(0);"> <span class="icon account ir">Icon</span>Account Settings</a></li>
																<li><a href="JavaScript:void(0);"> <span class="icon profile ir">Icon</span>Profile Settings</a></li>
																<li><a href="JavaScript:void(0);"> <span class="icon notification ir">Icon</span>Notification Settings</a></li>
															</ul>
															<div class="footer"><a href="JavaScript:void(0);">Go to Control Panel</a></div>
														</div>-->

							<!--Emails-->
							<!--							<div class="pane emails"><ul>
																<li><a href="JavaScript:void(0);">New Feature: I want to request a new feature <span class="icon ir">Icon</span><small>From Stacy, Today at 3:12 PM</small></a></li>
																<li><a href="JavaScript:void(0);" class="read">Bug Report: I came across this issue in the Nav. <span class="icon ir">Icon</span><small>From Mike on 25th October</small></a></li>
																<li><a href="JavaScript:void(0);" class="read">Status?: What is the staus on the new build?<span class="icon ir">Icon</span><small>From Gary on 22th October</small> </a></li>
																<li><a href="JavaScript:void(0);">Database Report: Database maintance schedule<span class="icon ir">Icon</span><small>From Brad on 21th October</small></a></li>
																<li><a href="JavaScript:void(0);">Account Credentials: Please reset my password<span class="icon ir">Icon</span><small>From Lucy on 15th October</small></a></li>
															</ul>
															<div class="footer"><a href="JavaScript:void(0);">Read All Emails</a></div>
														</div>-->

							<!-- Theme Switcher-->
							<div class="pane themes">
								   <ul id="css_switcher">
							<li><?php echo $this->Html->link('Logout', '/demos/logout',array('controller' => 'demos', 'action' => 'index')); ?>
							</li>	
								   </ul>
							</div>			


						</div>
					</div>

					<!--========= Toggle Button for Navigation =========-->
					<a href="JavaScript:void(0);" class="nav_toggle ir">Show/Hide Menu</a>



					<!--========= Navigation =========-->
					<nav id="main_nav" class="clearfix">
						<ul class="sf-menu">
							<li><?php echo $this->Html->link('Dashboard', '/dashboards/index',array('controller' => 'dashboards', 'action' => 'index')); ?>
							</li>				

							<li><a href="javascript: void(0)">Menu</a>
								<ul>
									<li><?php echo $this->Html->link('Category','/menus/index',array('controller' => 'menus') ); ?>
									 <ul>
                                <li><?php echo $this->Html->link('Add New','/menus/add', array('controller' => 'menus', 'action' => 'add')); ?></li>
                                <li><?php echo $this->Html->link('List','/menus/index', array('controller' => 'menus', 'action' => 'index')); ?></li>
                                
                            </ul>
									
									</li>
									<li><?php echo $this->Html->link('Item ','/menus/index_item', array('controller' => 'menus', )); ?>
									 <ul>
                                <li><?php echo $this->Html->link('Add New','/menus/add_item', array('controller' => 'menus', 'action' => 'add_item')); ?></li>
                                <li><?php echo $this->Html->link('List','/menus/index_item', array('controller' => 'menus', 'action' => 'index_item')); ?></li>
                                
                            </ul></li>
									
								</ul>
							</li>


							<li><a href="javascript: void(0)">Manage</a>
								<ul>
									<li><?php echo $this->Html->link('Event ','/events/index', array('controller' => 'events', )); ?>
									 <ul>
                                <li><?php echo $this->Html->link('Add New','/events/add', array('controller' => 'events', 'action' => 'add_item')); ?></li>
                                <li><?php echo $this->Html->link('List','/events/index', array('controller' => 'events', 'action' => 'index')); ?></li>
                                
                            </ul></li>
							<li><?php echo $this->Html->link('Gallery ', array('controller' => 'galleries', )); ?>
									 <ul>
                                <li><?php echo $this->Html->link('Add New','/galleries/add', array('controller' => 'galleries', 'action' => 'add_item')); ?></li>
                                <li><?php echo $this->Html->link('List','/galleries/index', array('controller' => 'galleries', 'action' => 'index')); ?></li>
                                
                            </ul></li>

								</ul>
							</li>


							<li><a href="javascript: void(0)">Reservation</a>
								<ul>
									<li><?php echo $this->Html->link('Add New ','/reservations/add', array('controller' => 'reservations', 'action' => 'add')); ?></li>
									<li><?php //echo $this->Html->link('Edit ','/reservations/edit', array('controller' => 'reservations', 'action' => 'edit')); ?></li>
									<li><?php //echo $this->Html->link('View ','/reservations/view', array('controller' => 'reservations', 'action' => 'view')); ?></li>
									<li><?php echo $this->Html->link('List ','/reservations/index', array('controller' => 'reservations', 'action' => 'index')); ?></li>
								</ul>
							</li>



							<li><?php echo $this->Html->link('Feedbacks ','/feedbacks/index', array('controller' => 'feedbacks', 'action' => 'index')); ?></a>
								<ul>

									
									<li></li>
								</ul>
							</li>

							
							<li><?php echo $this->Html->link('Customer ','/customer_informations/index', array('controller' => 'customer_informations', 'action' => 'index')); ?></a>
								<ul>
						<li><?php //echo $this->Html->link('Add ','/customer_informations/add', array('controller' => 'customer_informations', 'action' => 'add')); ?></li>
						
						<li></li>
								</ul>
							</li>
							<li><a href="javascript: void(0)">Order </a>
								<ul>
									<li><?php echo $this->Html->link('Add New ','/orders/add', array('controller' => 'orders', 'action' => 'add')); ?></li>
									<li><?php //echo $this->Html->link('Edit ','/orders/edit', array('controller' => 'orders', 'action' => 'edit')); ?></li>
									<li><?php //echo $this->Html->link('View ','/orders/view', array('controller' => 'orders', 'action' => 'view')); ?></li>
									<li><?php echo $this->Html->link('List ', '/orders/index',array('controller' => 'orders', 'action' => 'index')); ?></li>
								</ul>
							</li>
							<li><a href="javascript: void(0)">Content </a>
								<ul>
									<li><?php echo $this->Html->link('About ','/contents/about_content', array('controller' => 'contents', 'action' => 'about_content')); ?></li>
									<li><?php echo $this->Html->link('Description ','/contents/description_content', array('controller' => 'contents', 'action' => 'description_content')); ?></li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</header>
			<!-- End of Header --> 
			<div id="main">
				<div class="container_12 clearfix">
					<?php echo $this->fetch('content'); ?>

				</div>
			</div>




			<!--============================ FOOTER ============================-->
			<footer id="footer">
				<p class="tac">
					© Copyright 2011  Your Company. All Rights Reserved. Powered by <a href="#">Mriya Admin.</a>
				</p>

				<div class="push"></div>
			</footer>
			<!-- End of Footer --> 



			<!--============================ JAVASCRIPTS ============================-->



			<!--=========  BASE UI PLUGINS | These plugins are required for basic functionality to work such as tabs, menus etc =========-->
			<!-- jQuery --> 


			<!-- Different plugins from jQuery Tools library --> 
			<!-- Tabs --> 

			<?php echo $this->Html->script('libs/tabs.min'); ?>

			<!-- Modal --> 
			<?php echo $this->Html->script('libs/overlay.min'); ?>

			<!-- Expose --> 
			<?php echo $this->Html->script('libs/toolbox.expose'); ?>

			<!-- Tooltip --> 
			<?php echo $this->Html->script('libs/tooltip.min'); ?>  
			<?php echo $this->Html->script('libs/tooltip.slide.min'); ?> 
			<?php echo $this->Html->script('libs/tooltip.dynamic.min'); ?>

			<!-- Validator, HTML5 range and date control --> 
			<?php echo $this->Html->script('libs/validator.min'); ?>
			<?php echo $this->Html->script('libs/rangeinput.min'); ?>
			<?php echo $this->Html->script('libs/dateinput.min'); ?>        


			<!-- Super fish Menu --> 
			<?php echo $this->Html->script('libs/superfish.min'); ?>

			<!-- Toggle Buttons --> 
			<?php echo $this->Html->script('libs/jquery.ezmark.min'); ?>

			<!-- CSS & Images Preoloader --> 
			<?php echo $this->Html->script('libs/preload.css.images.min'); ?>
			<!--=========  END BASE UI PLUGINS =========-->




			<!--=========  OPTIONAL SCRIPTS | These plugins should be loaded as per use basis =========-->

			<!-- Snippet Code Highlighter --> 
			<?php echo $this->Html->script('libs/jquery.snippet.min'); ?>

			<!-- Carousel --> 
			<?php echo $this->Html->script('libs/jquery.jcarousel.min'); ?>

			<!-- Pretty Photo Gallery --> 

			<?php echo $this->Html->script('libs/jquery.prettyPhoto'); ?>
			<?php echo $this->Html->css('themes/pretty_photo'); ?>

			<!-- Flot and Graph table-->
			<!-- Canvas Plugin for IE8-->
			<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="libs/excanvas.min.js"></script><![endif]-->
			<?php echo $this->Html->script('libs/jquery.flot.min'); ?>
			<?php echo $this->Html->script('libs/jquery.flot.pie.min'); ?>
			<?php echo $this->Html->script('libs/jquery.flot.resize.min'); ?>
			<?php echo $this->Html->script('libs/jquery.graphTable-0.2.min'); ?>

			<!-- Data Tables -->
			<?php echo $this->Html->script('libs/jquery.dataTables.min'); ?>
			<?php echo $this->Html->script('libs/TableTools.min'); ?>
			<?php echo $this->Html->script('libs/ZeroClipboard.min'); ?>
			<!-- Wizard  --> 
			<?php echo $this->Html->script('libs/formToWizard.min'); ?>

			<!-- WYSIWYG Editor --> 
			<?php echo $this->Html->script('libs/jquery.cleditor'); ?>

			<!-- Calendar --> 
			<?php echo $this->Html->script('libs/fullcalendar.min'); ?>

			<?php //echo $this->Html->script('libs/jquery-ui-1.8.6.custom.min'); ?>
			<!--=========  END OPTIONAL SCRIPTS =========-->        


			<!--=========  CONFIG =========-->
			<?php echo $this->Html->script('libs/script'); ?>		

	</body>
	<!-- InstanceEnd --></html>


<!--================== Master Tree View Jquery  ===========-------->
<?php echo $this->Html->script('admin/jquery.cookie'); ?>	
<?php echo $this->Html->script('admin/jquery.treeview'); ?>	
<?php echo $this->Html->script('admin/demo'); ?>	
<?php echo $this->Html->css('admin/jquery.treeview'); ?>	
<?php //echo $this->Html->script('admin/demo'); ?>	