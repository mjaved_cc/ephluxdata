<!doctype html>

<!-- Coniditional CSS Hacks for IE. -->
<!--[if lt IE 8 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie"> <![endif]-->
<html lang="en" class="no-js">
<!--<![endif]-->

<!--============================ HEAD ============================-->
<head>
<meta charset="utf-8">

<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="robots" content="noindex,nofollow" /> 

<!--  Mobile viewport optimized -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--  Title & meta tags -->
<title>Mriya Admin</title>


<meta name="description" content="Mriya: Responsive Admin Theme for Web Applications and Backend Interfaces">
<meta name="author" content="Cosmive">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="robots" content="noindex,nofollow" /> 


<!--  Favicons -->
<link rel="shortcut icon"  href="favicon.ico" type="image/ico">
<link rel="shortcut icon"  href="favicon.png" type="image/png">


<!--  Icons for iPhone/iPad -->
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />

<!--========= Fonts  =========-->
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold' rel='stylesheet' type='text/css'>

<!--========= CSS  =========-->
<?php echo $this->Html->css('themes/core') ?>
		<?php echo $this->Html->css('themes/light/light') ?>
		<?php //echo $this->Html->css('themes/dark/dark')?>

<!-- All JavaScript at the bottom, except for Modernizr and style switcher -->
<?php echo $this->Html->script('js/libs/modernizr-latest.min') ?>
		<?php echo $this->Html->script('js/libs/styleswitcher.min') ?>


</head>
<!--============================ BODY ============================-->

<body>

<!--============================ MAIN WRAPPER - required for sticky footer ============================-->
<div class="wrapper">
	
<!--============================ CONTAINER ============================-->
<div id="main">


<!--========= LOGO  =========-->
      <div class="logo_login">
            <a href="#" class="ir">Shift Admin</a>
      </div>


	
	<?php echo $this->fetch('content'); ?>
	
	
	 <!-- Begin Footer -->
          <!--      <div class="footer">
                  <button name="button" type="submit" class="button dark" >Reset Password</button>
                  <a href="#" data-target="login" class="linkform fr"> Back to Login</a>-->
              <!--  </div>
                <!-- End Footer -->                
                
	  </form>                
			</div><!-- End Box -->


</div>
<!-- end of main container --> 

</div>
<!-- end of main wrapper --> 
