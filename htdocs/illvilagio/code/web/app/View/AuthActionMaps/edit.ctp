<div class="authActionMaps form">
	<?php echo $this->Form->create('AuthActionMap'); ?>
	<fieldset>
		<legend><?php echo __('Edit Auth Action Map'); ?></legend>		
		<div class="input text">
			<label><?php echo __('Controller'); ?></label>
			<label><?php echo $this->request->data['Controller']['alias']; ?></label>			
		</div>
		<div class="input text">
			<label><?php echo __('Action'); ?></label>
			<label><?php echo $this->request->data['Action']['alias']; ?></label>			
		</div>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
		echo $this->Form->input('feature_id');
//		echo $this->Form->input('crud', array(
//			'options' => $crud_actions,
//			'label' => __('Treated as'),
//			'selected' => $this->Form->value('AuthActionMap.crud')
//		));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

<!--		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AuthActionMap.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AuthActionMap.id')));
	?></li>-->
		<li><?php echo $this->Html->link(__('List Auth Action Maps'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Acos'), array('controller' => 'acos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('controller' => 'acos', 'action' => 'add')); ?> </li>
	</ul>
</div>
