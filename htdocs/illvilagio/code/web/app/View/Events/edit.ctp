<!--================== admin panel date time picker===========-------->
<?php echo $this->Html->script('admin/ImageTypeValidationEdit'); ?>
<?php // echo $this->Html->script('libs/jquery-1.10.1.min') ?>

<?php echo $this->Html->script('libs/jquery-ui.min'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-timepicker-addon'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-sliderAccess'); ?>
<?php echo $this->Html->script('admin/timepicker'); ?>
<!---------------===================== END ================------------------------>
<!---------------===================== MULTIPLE IMAGE UPLOAD ================------------------------>
<?php echo $this->Html->script('admin/jquery.uploadify.min'); ?>
<?php echo $this->Html->script('admin/jquery.uploadify'); ?>

<!---------------===================== END   MULTIPLE IMAGE UPLOAD ================------------------------>

<?php //pr($id); ?>


<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Edit') ?></h2>
			<?PHP echo $this->Html->link('Back to Event', '/events/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
			<!--			<button class="button dark" data-target="register" onclick="App.redirect({
							controller : 'divisions',
							action : 'index'
						})"> Edit Event<?php // echo __('Back to ' . $page_heading . ' Listing')    ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Event', array('id' => 'validate')); ?>
			<table  style="float:left; width:500px; align:'left'">

				<fieldset>
					<legend>Event<?php // echo __($page_heading)       ?></legend>

					<tr><td><label><p>Title</p></label></td>
						<td><?php echo $this->Form->input('title', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</p></td></tr>
					<tr><td><label>Description</label></td>
						<td><?php echo $this->Form->input('description', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>How To Join ?</label></td>
						<td><?php echo $this->Form->input('participate_description', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>Time</label></td>
						<td><?php echo $this->Form->input('event_time', array('required' => 'required', 'type'=>'datetime', 'class' => 'datetime', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>Arabic Title</label></td>
						<td><?php echo $this->Form->input('title_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
					<tr><td><label>Arabic Description</label></td>
						<td><?php echo $this->Form->input('description_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
					<tr><td><label>How To Join ? (Arabic) </label></td>
						<td><?php echo $this->Form->input('participate_description_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
					<tr><td><label>Is promoted Home </label></td>
						<td><?php echo $this->Form->input('promotedhome', array('div' => false, 'label' => false, 'type' => 'checkbox')); ?></td></tr>
			</table>

			<table  style="float:left; width:500px">

				<!-- <tr><td><label>Name</label></td>
					<td><?php echo $this->Form->input('Name', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</p></td></tr>
				<tr><td><label>Arabic Name</label></td>
					<td><?php echo $this->Form->input('Arabic Name', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>-->
				<tr><td><label>Upload Images</label></td>
					<td><?php echo $this->Form->create('Upload', array('type' => 'file', 'label' => false)); ?>                
						<?php echo $this->Form->input('file', array('type' => 'file', 'id' => 'file_upload','name'=>'file_upload', 'label' => false)); ?></td></tr>		

			</table>
			<div id="image_list"></div>
			</fieldset>
			<div class="old_images"></div>
			<?php foreach($image as $key=>$images){	?>
			<!-- <input type="text" class="image<?php echo $key; ?>" name="images_old[<?php $key; ?>]"  value="<?php echo $images['EventImage']['image_url'] ?>"/>-->
			<?php } ?>
			<br clear="all"></br>
		</div>
		

			<div class="body clearfix">

					<!-- Begin Gallery -->
					<ul class="gallery">


					<?php foreach($image as $key1=>$images){?>
						<!--Single Gallery image-->
						
						<?php //pr($images); ?>
						<li class="remove">
							<!--Thumbnail-->
							<input type="hidden"  value="<?php echo $images['EventImage']['image_url'] ?>"/>
							<img src="<?php echo $this->webroot; ?>events/<?php echo $images['EventImage']['event_id'].'/'.$images['EventImage']['image_url']; ?>" width="128" height="85" alt="<?php echo $images['EventImage']['title_image'] ?>">
							<!--Controls for view, edit, delete-->
							<ul class="controls">
								<li title="View Image" class="enable_tip"><a href="<?php echo $this->webroot; ?>gallery/example.jpg" data-gallery="prettyPhoto[gallery1]" class="view ir">View</a></li>
								<li title="Edit Image" class="enable_tip"><a href="JavaScript:void(0);" class="edit ir">Edit</a></li>
								<li title="Delete Image" id="<?php echo $images['EventImage']['id']; ?>" class="delete_image"><a href="JavaScript:void(0);" class="delete ir">Delete</a></li>
							</ul>
						</li>
						
						<?php } ?>
					
					</ul>
					<!-- End Gallery -->


				</div>
				<!-- End box body -->


		<table  style="float:right ; padding-right:50px" >
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit','value' =>'Upload','onClick'=>'return img_path()','escape' => true, 'class' => 'button green small','onclick'=>'imageValidate()'));
			echo $this->Form->end();
			?>
		</table>
	</div>
</section>
<script type="text/javascript">
<?php $timestamp = time(); ?>
	$(function() {
	var i=1;
		$('#file_upload').uploadify({
			
			'method'   : 'post',
			'swf': '<?php echo $this->webroot ?>js/admin/uploadify.swf',
			'uploader': '<?php echo Router::url('/', true) ?>events/upload',
			  "auto"      : true,
			'preventCaching' : false,
			'onUploadSuccess': function(file, data, response) {
				//alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
			$('#image_list').append('<div><input placeholder="English Title" type="text" name="Event[image]['+i+'][text]"><input type="text" placeholder="Arabic Title" name="Event[image]['+i+'][text_arabic]"><input type="hidden" value="'+file.name+'" name="Event[images]['+i+'][name]">'+file.name+'</div>');
			//console.log(data);	
			i++;		
			},
			
			'onUploadComplete' : function(file) {
				//$('#image_list').append('<div><input type="text" name="Events[image]['+i+'][text]"><input type="text" name="Events[image]['+i+'][text_arabic]"><input type="hidden" value="'+file.name+'" name="Events[images]['+i+'][name]">'+file.name+'</div>');
				//console.log(data);	
					//i++;

				//alert(file);
			//$('#image_list').append('<div><?php echo $this->Form->input('image_upload_name[]', array('type' => 'hidden')); ?> '+file.name+'</div>')
            //alert('The file ' + file.name + ' finished processing.');
        } 
		});
	});



	$( document ).ready(function() {
		var i=1;
		$(".delete_image").click(function() {
			//alert($(this).attr('id'));
			//var clas = $(this).attr('id');
		    //$('.'+clas).remove();
		$(".old_images").append("<input name='Event[images_old]["+i+"]' type='text' value='"+$(this).attr('id')+"'>");
		i++;
		});
		

	
			
	});

</script>
