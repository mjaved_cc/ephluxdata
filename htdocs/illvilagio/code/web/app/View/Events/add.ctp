<!--================== admin panel date time picker===========-------->
<?php echo $this->Html->script('admin/ImageTypeValidationEdit'); ?>
<?php //echo $this->Html->script('libs/jquery-1.10.1.min') ?>

<?php echo $this->Html->script('libs/jquery-ui.min'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-timepicker-addon'); ?> 
<?php echo $this->Html->script('admin/jquery-ui-sliderAccess'); ?>
<?php echo $this->Html->script('admin/timepicker'); ?>
<!---------------===================== END ================------------------------>
<!---------------===================== MULTIPLE IMAGE UPLOAD ================------------------------>
<?php echo $this->Html->script('admin/jquery.uploadify.min'); ?>
<?php echo $this->Html->script('admin/jquery.uploadify'); ?>

<!---------------===================== END   MULTIPLE IMAGE UPLOAD ================------------------------>
<!-- <a href="<?php echo $this->webroot.'events/uploadify'; ?>">hit me </a>-->

<?php //echo $_SERVER['DOCUMENT_ROOT'].''.$this->webroot  ?>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<?php //echo time(); ?>
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<?PHP echo $this->Html->link('Back to Event', '/events/index', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 
			<!--			<button class="button dark" data-target="register" onclick="App.redirect({
							controller : 'divisions',
							action : 'index'
						})"> Edit Event<?php // echo __('Back to ' . $page_heading . ' Listing')      ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Events', array('id' => 'validate')); ?>
			<table  style="float:left; width:500px; align:'left'">

				<fieldset>
					<legend>Event<?php // echo __($page_heading)       ?></legend>
					
					<tr><td><label><p>Title</p></label></td>
						<td><?php echo $this->Form->input('title', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</p></td></tr>
					<tr><td><label>Description</label></td>
						<td><?php echo $this->Form->input('description', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>How To Join ?</label></td>
						<td><?php echo $this->Form->input('participate_description', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>Time</label></td>
						<td><?php echo $this->Form->input('event_time', array( 'class' => 'datetime', 'div' => false, 'label' => false)); ?>*</td></tr>
					<tr><td><label>Arabic Title</label></td>
						<td><?php echo $this->Form->input('title_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
					<tr><td><label>Arabic Description</label></td>
						<td><?php echo $this->Form->input('description_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
					<tr><td><label>How To Join ? (Arabic) </label></td>
						<td><?php echo $this->Form->input('participate_description_arabic', array('required' => 'required', 'div' => false, 'label' => false)); ?></td></tr>
			</table>

			<table  style="float:left; width:500px">

				<!--<tr><td><label>Name</label></td>
					<td><?php echo $this->Form->input('Name', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</p></td></tr>
				<tr><td><label>Arabic Name</label></td>
					<td><?php echo $this->Form->input('Arabic Name', array('required' => 'required', 'div' => false, 'label' => false)); ?>*</td></tr>-->

				<tr><td><label>Upload Images</label></td>
					<td><?php echo $this->Form->create('Upload', array('type' => 'file', 'label' => false)); ?>                
						<?php echo $this->Form->input('file', array('type' => 'file', 'id' => 'file_upload','name'=>'file_upload', 'label' => false)); ?></td></tr>
						
							
			</table>
			<div id="image_list"></div>
			
			</fieldset>

			<br clear="all"></br>
		</div>
		<table  style="float:right ; padding-right:50px" >
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit','value' =>'Upload','onClick'=>'return img_path()','escape' => true, 'class' => 'button green small','onclick'=>'imageValidate()'));
			echo $this->Form->end();
			?>
		</table>
	</div>
</section>
<script type="text/javascript">
<?php $timestamp = time(); ?>
	$(function() {
	var i=1;
		$('#file_upload').uploadify({
			
			'method'   : 'post',
			'swf': '<?php echo $this->webroot ?>js/admin/uploadify.swf',
			'uploader': 'upload',
			  "auto"      : true,
			'preventCaching' : false,
			'onUploadSuccess': function(file, data, response) {
				//alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
			$('#image_list').append('<div><input placeholder="English Title" type="text" name="Events[image]['+i+'][text]"><input type="text" placeholder="Arabic Title" name="Events[image]['+i+'][text_arabic]"><input type="hidden" value="'+file.name+'" name="Events[images]['+i+'][name]">'+file.name+'</div>');
			console.log(data);	
			i++;		
			},
			
			'onUploadComplete' : function(file) {
				//$('#image_list').append('<div><input type="text" name="Events[image]['+i+'][text]"><input type="text" name="Events[image]['+i+'][text_arabic]"><input type="hidden" value="'+file.name+'" name="Events[images]['+i+'][name]">'+file.name+'</div>');
				//console.log(data);	
					//i++;

				//alert(file);
			//$('#image_list').append('<div><?php echo $this->Form->input('image_upload_name[]', array('type' => 'hidden')); ?> '+file.name+'</div>')
            //alert('The file ' + file.name + ' finished processing.');
        } 
		});
	});
</script>

