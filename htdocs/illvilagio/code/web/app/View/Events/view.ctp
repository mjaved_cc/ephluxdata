<?php
//pr($order);
//$this->Paginator->params(); 
?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php // echo __($page_heading)  ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Event Listings </h2> 

<?PHP echo $this->Html->link('Add Event', '/events/add', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?>      <!-- <button class="button dark" data-target="register" onclick='history.back()'
  
  //App.redirect({
//				controller : 'Inserts',
//				action : 'add'
//			})">--> <?php //echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
			<?php
//			echo $this->Form->create("Insert", array('action' => 'index', 'id' => 'validate'));
//			echo $this->Form->input("q", array('label' => 'Search for', 'required' => 'required', 'div' => false, 'class' => 'med float-left-margin'));
//			echo $this->Form->button('Search', array('type' => 'submit', 'escape' => true, 'class' => 'button green small float-left-margin'));
//			echo $this->Form->end();
			?> 
            <table >
				<thead>
					<tr>
						<th align="left">Detail<?php  //echo $this->Paginator->sort('name');  ?></th>
						
					</tr>
				</thead>
				<tbody>
				
					<?php foreach ($event as $events): ?>
				
				<?php //pr($events); ?>
				<tr>					
					<td >Event Name &nbsp; </td>
					<td ><?php echo $events['title'];  ?></td>									
				</tr>
				<tr> 					
					<td >Description &nbsp; </td>
					<td ><?php echo $events['description'];  ?></td>						
				</tr>
				
				<tr> 					
					<td >Participate Description &nbsp; </td>
					<td ><?php echo $events['participate_description'];  ?></td>
				</tr>
					
				<tr> 					
					<td >Event Name Arabic &nbsp; </td>
					<td ><?php echo $events['title_arabic'];  ?></td>
				</tr>	
					
				<tr> 					
					<td >Event Description Arabic &nbsp; </td>
					<td ><?php echo $events['description_arabic'];  ?></td>
				</tr>
				
				<tr> 					
					<td >Particapte Description Arabic &nbsp; </td>
					<td ><?php echo $events['particapte_description_arabic'];  ?></td>
				</tr>	
								
				<tr>
					<td class="grid_dropdown"> </td>
					<td colspan="3"><?php //echo h($result['Array']['description']);  ?></td>
				</tr>
				<?php endforeach;  ?>
				</tbody>
            </table>
			<?php //echo $this->element('pagination',array('pagination_params' => $pagination_params));  ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>


<!--============================ GALLERY ============================-->
<section class="grid_12">
			<!-- Begin box -->
		<div class="box">
				
				<!-- Begin box header -->
				<div class="header clearfix">
					<h2>Gallery</h2>

					<!-- Dot pagination -->
					<ul class="dot_pagination fr">
						<li><a href="JavaScript:void(0);">1</a></li>
						<li class="active"><a href="JavaScript:void(0);">2</a></li>
						<li><a href="JavaScript:void(0);">3</a></li>
						<li><a href="JavaScript:void(0);">4</a></li>
						<li><a href="JavaScript:void(0);">5</a></li>
					</ul>

					<!-- Separator -->
					<span class="seperator"></span>

				</div>
				<!-- End header -->

				<!-- Begin box body -->
				<div class="body clearfix">

					<!-- Begin Gallery -->
					<ul class="gallery">


					<?php foreach($image as $images){?>
						<!--Single Gallery image-->
						
						<?php //pr($images); ?>
						<li>
							<!--Thumbnail-->
							<img src="<?php echo $this->webroot; ?>events/<?php echo $images['EventImage']['event_id'].'/'.$images['EventImage']['image_url']; ?>" width="128" height="85" alt="<?php echo $images['EventImage']['title_image'] ?>">
							<!--Controls for view, edit, delete-->
							<ul class="controls">
								<li title="View Image" class="enable_tip"><a href="<?php echo $this->webroot; ?>gallery/example.jpg" data-gallery="prettyPhoto[gallery1]" class="view ir">View</a></li>
								<li title="Edit Image" class="enable_tip"><a href="JavaScript:void(0);" class="edit ir">Edit</a></li>
								<li title="Delete Image" class="enable_tip"><a href="JavaScript:void(0);" class="delete ir">Delete</a></li>
							</ul>
						</li>

						<?php } ?>
					
					</ul>
					<!-- End Gallery -->


				</div>
				<!-- End box body -->

		
		</div>
		<!-- End box -->
		</section>


	
	<!-- InstanceEndEditable -->

