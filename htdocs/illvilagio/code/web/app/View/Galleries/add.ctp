<?php echo $this->Html->script('admin/ImageTypeValidation'); ?>
		

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<?PHP echo $this->Html->link('Back to Gallery', '/galleries/view', array('type' => 'button', 'class' => 'button light linkform', 'data-target' => 'register')); ?> 

			<!--<button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'divisions',
				action : 'index'
			})">Back To Event Listing<?php //echo __('Back to ' . $page_heading . ' Listing')      ?></button>-->
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Gallery', array('id' => 'validate','type' => 'file')); ?>
			<fieldset>
				<legend>Gallery<?php // echo __($page_heading)    ?></legend>
				<p><?php echo $this->Form->input('name', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('name_arabic', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('caption', array('required' => 'required', 'div' => false)); ?></p>
				<p><?php echo $this->Form->input('image_caption_arabic', array('required' => 'required', 'div' => false)); ?></p>
				   <?php echo $this->Form->input('filename',array('type' => 'file')); ?>
			</fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit','value' =>'Upload','onClick'=>'return img_path()','escape' => true, 'class' => 'button green small'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>
