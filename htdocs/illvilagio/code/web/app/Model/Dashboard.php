<?php

class Dashboard extends AppModel {
	public $useTable = 'events'; 
	
	function get_upcoming_events() {
		$result = $this->query('call get_upcoming_events()');
		return $result;
	}
	
		function get_pending_reservations() {
		$result = $this->query('call get_pending_reservations()');
		return $result;
	}
		function get_today_order() {
		$result = $this->query('call get_today_orders()');
		return $result;
	}
		function get_tomorrow_order() {
		$result = $this->query('call get_tomorrow_orders()');
		return $result;
	}
		function get_yesterday_order() {
		$result = $this->query('call get_yesterday_orders()');
		return $result;
	}
		function get_today_reservation() {
		$result = $this->query('call get_today_reservation()');
		return $result;
	}
		function get_tomorrow_reservation() {
		$result = $this->query('call get_tomorrow_reservation()');
		return $result;
	}
		function get_upcoming_reservations() {
		$result = $this->query('call get_upcoming_reservations()');
		return $result;
	}
		function get_today_dinein() {
		$result = $this->query('call get_today_dinein()');
		return $result;
	}
		function get_today_takeaway() {
		$result = $this->query('call get_today_takeaways()');
		return $result;
	}
		function get_today_delivery() {
		$result = $this->query('call get_today_deliveries()');
		return $result;
	}
}

?>
