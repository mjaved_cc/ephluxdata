<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class Content extends AppModel {
	
	public $validate = array(
			
		    'title' => array(
		        'rule'       => 'ruleName', // or: array('ruleName', 'param1', 'param2' ...)
		        'required'   => true,
		        'allowEmpty' => false,
		        'on'         => 'create', // or: 'update'
		        'message'    => 'Your Error Message'
		    ),		
	);
	
	/**
	 * function to get contents
	 * @param By language English(en) & arabic(ar) $lang_name
	 * @param unknown_type $modified_date
	 * @return mixed
	 */
	
	
	function get_content_by_key($lang_name,$modified_date)
	{
		//print 'here it is Content Model!!!!!';
		//die();
		
		$result = $this->query('call get_content_by_key("'.$lang_name.'","'.$modified_date.'")');
				
		//debug($result);
		//exit;
		return $result;
	}
	
	function get_content( $key ) {
		
		$result = $this->query('call get_content("'.$key.'")');
		return $result;
	}
	
	function save_content( $con_en, $con_ar, $key ) {
		$this->query('call save_content("'.$con_en.'", "'.$con_ar.'", "'.$key.'")');
	}


	/*
	 * Function to add contents
	 * 
	 */
	
	function add_content($content_fields){
		
		//print_r($content_fields);
		//exit;
		
		$result = $this->query('call add_content("'.$content_fields['title'].'","'.$content_fields['description'].'", "'.$content_fields['description_arabic'].'" ,"'.$content_fields['title_arabic'].'","'.date('Y-m-d').'","'.date('Y-m-d').'")');
		//print_r($result);
		//exit;
		
		return $result[0]['contents']; 
	}
	
	/*
	 * Function to edit content.
	 */
	function edit_content($content_fields){
	
		$result = $this->query('call edit_content("'.$content_fields['id'].'","'.$content_fields['title'].'","'.$content_fields['description'].'", "'.$content_fields['description_arabic'].'" ,"'.$content_fields['title_arabic'].'","'.date('Y-m-d').'","'.date('Y-m-d').'")');
		//print_r($result);
		//exit;
	
		return $result[0]['contents'];
	}
	
	/* 
	 * funtion to delete content by id.
	 */
	
	function delete_content($content_id){
	
		//print_r($content_id);
		//exit;
		$result = $this->query('call delete_content("'.$content_id['id'].'")');
		//print_r($result);
		//exit;
	
		//print_r($result);
		//exit;
		return $result[0]['contents'];
	}
	
	
	function contents_count(){
		//count_galleries
		$result = $this->query('call content_count()');
		return $result;
	}
	
	
}
?>
