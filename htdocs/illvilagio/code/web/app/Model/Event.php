<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class Event extends AppModel {
	/*
	 * GET lISTING OF EVENTS
	 * @params lang
	 * @param modified_date  
	 */

	function get_event($lang_name, $modified_date, $is_deal) {
		if ($is_deal == 1) {
			$result = $this->query('call deal_events("' . $is_deal . '","' . $lang_name . '")');
		} elseif ($is_deal == 2) {
			$result = $this->query('call deal_events("' . $is_deal . '","' . $lang_name . '")');
		} else {
			$result = $this->query('call get_event("' . $lang_name . '","' . $modified_date . '")');
		}
		return $result;
	}

	function get_event_image_id($id) {

		$result = $this->query('call get_event_image_id("' . $id . '")');
		return $result;
	}

	function add_event($add_event_info) {

		
		//$date = explode('/', $add_event_info['event_time']);
		//$new_date = $date[2].'-'.$date[1].'-'.$date[0];
		
		$time = explode(' ', $add_event_info['event_time']);
		$date = explode('/', $time[0]);
		$new_date = $date[2].'-'.$date[0].'-'.$date[1].' '.$time[1];
		
		//pr($new_date);
		//pr($time);
		//exit;
		//print_r($add_event_info);
		///exit;
		//$data = array();
		$result = $this->query('call add_event("' . $add_event_info['title'] . '","' . $add_event_info['description'] . '","' . $add_event_info['participate_description'] . '","' . $new_date . ':00","' . date("Y-m-d H:i:s", time()) . '","' . date("Y-m-d H:i:s", time()) . '","' . $add_event_info['title_arabic'] . '","' . $add_event_info['description_arabic'] . '","' . $add_event_info['participate_description_arabic'] . '")');

		//pr($add_event_info);
		//exit;
		$data = array();

		$i = 1;
		if(isset($add_event_info['image'])){
		foreach ($add_event_info['image'] as $key1 => $_images1) {

			$data[$i] = $_images1;

			//print_r($data);
			$i++;
		}
		}
		//exit;
		$i = 1;
		
		if(isset($add_event_info['images'])){
		foreach ($add_event_info['images'] as $key => $_event_images) {
			//print_r($data[$i]['text']);
			//debug($images_name[$key]['text']);
			//die();

			$this->query('call add_event_images("' . $_event_images['name'] . '","' . $result[0]['events']['id'] . '","' . date('Y-m-d') . '","' . date('Y-m-d') . '","' . $data[$i]['text'] . '","' . $data[$i]['text_arabic'] . '")');
			$i++;
		}
		}
		//exit;		
		//$data = $this->query('call add_event_images("'.$add_event_info['title'].'",)');
		//print_r($result);
		//die();
		return $result[0]['events'];
	}
	
	function get_promoted_home() {
		$result = $this->query('call get_event_promoted_home()');
		return $result[0][0]['promoted_home'];
	}

	function edit_event($edit_event_info) {

		//pr($edit_event_info);
		//exit;
		$result = $this->query('call edit_events("' . $edit_event_info['id'] . '","' . $edit_event_info['title'] . '","' . $edit_event_info['description'] . '","' . $edit_event_info['participate_description'] . '","' . $edit_event_info['event_time']['year'] . '-'.$edit_event_info['event_time']['month'].'-'.$edit_event_info['event_time']['day'].' '.$edit_event_info['event_time']['hour'].':'.$edit_event_info['event_time']['min'].':00","' . date("Y-m-d H:i:s", time()) . '","' . date("Y-m-d H:i:s", time()) . '","' . $edit_event_info['title_arabic'] . '","' . $edit_event_info['description_arabic'] . '","' . $edit_event_info['participate_description_arabic'] . '", "' . $edit_event_info['promotedhome'] . '")');


		$data = array();

		$i = 1;
		if(isset($edit_event_info['image'])){
		foreach ($edit_event_info['image'] as $key1 => $_images1) {

			$data[$i] = $_images1;

			//print_r($data);
			$i++;
		}
		}
		//exit;				
		//print_r($result);
		//exit;
		
		if(isset($edit_event_info['images_old'])){
		foreach ($edit_event_info['images_old'] as $old_images){
			
			//pr($old_images);
			$this->query('DELETE FROM event_images WHERE `id` = "'.$old_images.'"');
		}
		}
		
		//$this->query('call delete_event("' . $delete_event . '")');
		
		$i = 1;
		
		if(isset($edit_event_info['images'])){
		foreach ($edit_event_info['images'] as $key => $_event_images) {
			//print_r( $_event_images['name']);
			//exit;
			$this->query('call edit_event_images("' . $_event_images['name'] . '","' . $result[0]['events']['id'] . '","' . date('Y-m-d') . '","' . date('Y-m-d') . '","' . $data[$i]['text'] . '","' . $data[$i]['text_arabic'] . '")');
			$i++;
		}
		}

		//$data = $this->query('call add_event_images("'.$add_event_info['title'].'",)');
		//print_r($result);
		//die();
		return $result[0]['events'];
	}

	function delete_event($delete_event) {

		//print_r($delete_event);
		//exit;
		$result = $this->query('call delete_event("' . $delete_event . '")');

		return $result[0]['events'];
	}

	function get_image_event_id($id) {
		$result = $this->query('call get_event_image_id("' . $id . '")');
		return $result;
	}

	function get_event_list() {


		$result = $this->query('call get_event_list()');
		return $result;
	}

	function events_count() {
		//count_galleries
		$result = $this->query('call count_events()');
		return $result;
	}

	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$recursive = -1;
		//$group = $fields = array('a_party');
		//return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
		$sql = "SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time,Event.event_name AS NAME, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id AND EventImage.status = 1
WHERE Event.status = 1 
GROUP BY Event.id
ORDER BY Event.event_time DESC
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
		$results = $this->query($sql);
		return $results;
	}

	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time,Event.event_name AS NAME, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id AND EventImage.status = 1
WHERE Event.status = 1 
GROUP BY Event.id
ORDER BY Event.event_time DESC;";

		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}

	function get_udid() {
		$result = $this->query('call get_udid_list()');
		
		return $result;
	}
	
	function get_email() {
		$result = $this->query('call get_customer_email()');
		
		return $result;
	}
	
	function event_image($id){
		$result = $this->query('SELECT image_url FROM event_images WHERE `id` = "'.$id.'";');
		pr($result);
		exit;
		return $result;
	}
	
}

?>
