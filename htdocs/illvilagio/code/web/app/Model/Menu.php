<?php

class Menu extends AppModel {

	public $actsAs = array('Tree');
	public $useTable = 'items';
	//public $name = 'Menu';
	public $count = 0;
	public $data_recursion;
	public $data_menu_item;
	public $last_count = 0;
	public $validate = array(
		'name' => array('notempty' => array('rule' => array('notempty'), 'message' => 'This is requried field',),),
		'imageUrl' => array('notempty' => array('rule' => array('notempty'), 'message' => 'This is requried field',),),
		'displayOrder' => array('notempty' => array('rule' => array('numeric'), 'message' => 'This is requried field',),),
		'status' => array('notempty' => array('rule' => array('numeric'), 'message' => 'This is requried field',),),
	);

	function insert_menu($data) {
		die('inside menu model ');

		/*
		  if (!empty($type) && !empty($data)) {


		  if ($type == 'HAS_LIKED') {
		  $uid = $data['user_id'];
		  $pid = $data['post_id'];
		  $tid = $data['target_id'];

		  $this->UserNotification->create();
		  $this->UserNotification->set('source_id', $uid);
		  $this->UserNotification->set('target_id', $tid);
		  $this->UserNotification->set('user_asset_id', $pid);
		  $this->UserNotification->set('action', $type);
		  $this->UserNotification->save();
		  }

		  if ($type == 'HAS_FOLLOWED') {
		  $uid = $data['user_id'];
		  $tid = $data['target_id'];

		  $this->UserNotification->create();
		  $this->UserNotification->set('source_id', $uid);
		  $this->UserNotification->set('target_id', $tid);
		  $this->UserNotification->set('action', $type);
		  $this->UserNotification->save();
		  }

		  if ($type == 'REQUEST_FOLLOW') {
		  $uid = $data['user_id'];
		  $tid = $data['target_id'];

		  $this->UserNotification->create();
		  $this->UserNotification->set('source_id', $uid);
		  $this->UserNotification->set('target_id', $tid);
		  $this->UserNotification->set('action', $type);
		  $this->UserNotification->save();
		  }

		  if ($type == 'HAS_MENTIONED') {

		  //pr($data);
		  $this->UserNotification->custom_saveAll($data);
		  }


		  if ($type == 'HAS_COMMENTED') {
		  $uid = $data['user_id'];
		  $tid = $data['target_id'];
		  $pid = $data['post_id'];

		  $this->UserNotification->create();
		  $this->UserNotification->set('source_id', $uid);
		  $this->UserNotification->set('target_id', $tid);
		  $this->UserNotification->set('user_asset_id', $pid);
		  $this->UserNotification->set('action', $type);
		  $this->UserNotification->save();
		  }
		  } */
	}

	/**
	 * Create feature
	 * 
	 * @param array $data
	 */
	
	function create($data) {

		$this->query('call create_menu(
			"' . $data['name'] . '",
			"' . $data['imageUrl'] . '",
			"' . $data['displayOrder'] . '",
			"' . $data['status'] . '",
			"' . $data['created'] . '",
				@result)');
		return $this->get_status();
	}

//	function get_all_menus()
//	{
//		$result = $this->query('call GetAllMenus()');
//		return $result;
//	}
//	
//	function get_status_active_menus()
//	{
//	
//	$result = $this->query('call GetStatusActiveMenus()');
//		return $result;
//	}


	function add_menu($menu_info, $file_info) {  //pr($menu_info['side_line']); exit;

		$data['Menu']['parentId'] = $menu_info['parent_id'];
		$data['Menu']['name'] = $menu_info['name'];
		$data['Menu']['extra'] = $menu_info['extra'];
		$data['Menu']['nameArabic'] = $menu_info['name_arabic'];
//		$data['Menu']['imageUrl'] = FULL_BASE_URL . Router::url('/') . 'app/webroot/menu/' . $file_info['name'];
	
		$data['Menu']['imageUrl'] = $file_info; 
		$data['Menu']['price'] = $menu_info['price'];
		$data['Menu']['created'] = date('Y-m-d h:i:s');
		$data['Menu']['description'] = $menu_info['description'];
		$data['Menu']['is_item'] = $menu_info['is_item'];
		$data['Menu']['descriptionArabic'] = $menu_info['description_arabic'];
		$data['Menu']['displayOrder'] = $menu_info['Display_Order'];
		$data['Menu']['lastUpdate'] = date('Y-m-d h:i:s');
		$data['Menu']['description_arabic'] = $menu_info['description_arabic'];
		$data['Menu']['allow_sideline'] = $menu_info['side_line'];

		$this->save($data);

		$msg = 'Menu added now!';
		return $msg;
	}

	function delete_menu($menu_info) {

		//$this->query('call delete_menu("'.$menu_info['id'].'")');	
		$this->query('call delete_menu("' . $menu_info . '")');

		$msg = 'Menu deleted now!';
		return $msg;
	}

	function edit_menu($menu_info, $file_info) {
		//pr($menu_info);
		//exit;
		$data['Menu']['id'] = $menu_info['id'];

		//$data['Menu']['parent_id'] = $menu_info['parent_id'];
		$data['Menu']['name'] = $menu_info['name'];
		$data['Menu']['extra'] = $menu_info['extra'];
		$data['Menu']['nameArabic'] = $menu_info['name_arabic'];
		$data['Menu']['displayOrder'] = $menu_info['Display_Order'];
		
		if (!empty( $file_info['name'] )) {
			$data['Menu']['imageUrl'] = FULL_BASE_URL . Router::url('/') . 'app/webroot/menu/' . $file_info['name'];
		}

		$data['Menu']['price'] = $menu_info['price'];

		$data['Menu']['lastUpdate'] = date('Y-m-d h:i:s');
		$data['Menu']['description'] = $menu_info['description'];
		$data['Menu']['descriptionArabic'] = $menu_info['description_arabic'];
		$data['Menu']['allow_sideline'] = $menu_info['side_line'];
		$this->save($data);

		$msg = 'Menu added now!';
		return $msg;
	}

	function get_menu($time) {
	
		//$parent_category = $this->find('all',array('conditions' => array('Menu.parent_id' => '0')));

		$data = array();

		$category = $this->query('CALL get_all_categories_subcategories("' . $time . '")');
		if (empty($category)) {
			$data = $category;
		}
		//$category = $this->find('all',array('conditions' => array('Menu.is_item' => '0','Menu.parent_id' => $category_parents['id'])));

		foreach ($category as $key => $categories) {

			foreach ($categories as $menus) {



				$obj_cat = new DtMenu($menus);
				$obj_field = $obj_cat->get_field();

				$data['category'][$key] = $obj_field;

				$child = array();
				$child = $this->find('all', array('conditions' => array('Menu.parentId' => $menus['id'], 'Menu.is_item' => '1')));

				if (!empty($child)) {

					foreach ($child as $key1 => $childs) {
						foreach ($childs as $child_menu) {

							$obj_cat_child = new DtMenu($child_menu);
							$obj_field_child = $obj_cat_child->get_field();
							$data['category'][$key]['categoryItems'][$key1] = $obj_field_child;
						}
					}
				}
			}
		}


		return $data;
	}

	
	function menu_parent_date_modify($id){
		//pr($id);
		$result = $this->query('call menu_parent_date_modify("'.$id.'","'.date('Y-m-d h:i:s').'")');
		return $result;
	}
		
	function recursive_records($id, $count) {

		$data = array();
		//$data = $this->children($id,true);

		$data = $this->find('all', array('conditions' => array('Menu.parent_id' => $id, 'Menu.is_item' => '0')));

		if (!empty($data)) {

			foreach ($data as $key34 => $menu) {
				//print_r($menu['Menu']['id']);
				$this->recursive_records($menu['Menu']['id'], $count);
			}
		}


		$recursion = array();
		$temp = array();
		foreach ($data as $key12 => $value) {

			foreach ($value as $keys => $recur) {

				if (!is_null($value)) {
					//$id=$recur['name'];

					$this->data_recursion[$this->count] = $recur;
					// $this->data_recursion;
					$this->count++;
				}
			}
			//print_r(end($this->data_recursion));
		}

		return $this->data_recursion;
	}

	function get_all_menus() {
		$result = $this->query('call get_all_menus()');
		return $result;
	}

	function _get_formatted_menu() {
		$result = $this->query('call get_all_menus()');
		return $result;
	}

	function get_menu_treeview($time) {

		//$parent_category = $this->find('all',array('conditions' => array('Menu.parent_id' => '0')));

		$data = array();

		//$category = $this->query('CALL get_all_categories_subcategories("'.$time.'")');
		$category = $this->find('all', array('conditions' => array('Menu.parentId' => 0, 'Menu.is_item' => '0')));
		if (empty($category)) {
			$data['menu'] = $category;
		} //pr($category);exit();		
		//$category = $this->find('all',array('conditions' => array('Menu.is_item' => '0','Menu.parent_id' => $category_parents['id'])));

		foreach ($category as $key => $categories) {

			//foreach ($categories['Menu'] as $menus){				
			//pr($menus);exit();

			$obj_cat = new DtMenu($categories['Menu']);
			$obj_field = $obj_cat->get_field();

			$data['Menu'][$key] = $obj_field;

			$child = array();
			$child = $this->find('all', array('conditions' => array('Menu.parentId' => $data['Menu'][$key]['id'], 'Menu.is_item' => '0')));
			//pr($child);exit();
			if (!empty($child)) {

				foreach ($child as $key1 => $childs) {
					foreach ($childs as $child_menu) {

						$obj_cat_child = new DtMenu($child_menu);
						$obj_field_child = $obj_cat_child->get_field();
						$data['Menu'][$key]['Category'][$key1] = $obj_field_child;

						$child1 = array();
						$child1 = $this->find('all', array('conditions' => array('Menu.parentId' => $data['Menu'][$key]['Category'][$key1]['id'])));
						//pr($child);exit();
						if (!empty($child1)) {

							foreach ($child1 as $key2 => $childs) {
								foreach ($childs as $child_menu) {

									$obj_cat_child = new DtMenu($child_menu);
									$obj_field_child = $obj_cat_child->get_field();
									$data['Menu'][$key]['Category'][$key1]['CategoryItem'][$key2] = $obj_field_child;

									$child2 = array();
									$child2 = $this->find('all', array('conditions' => array('Menu.parentId' => $data['Menu'][$key]['Category'][$key1]['CategoryItem'][$key2]['id'], 'Menu.is_item' => '1')));
									//pr($child);exit();
									if (!empty($child2)) {

										foreach ($child2 as $key3 => $childs) {
											foreach ($childs as $child_menu) {

												$obj_cat_child = new DtMenu($child_menu);
												$obj_field_child = $obj_cat_child->get_field();
												$data['Menu'][$key]['Category'][$key1]['CategoryItem'][$key2]['Item'][$key3] = $obj_field_child;
											}
										}
									}
								}
							}
						}
					}
				}

				// }
			}
		}


		return $data;
	}

	function get_item_list() {
		$result = $this->query('call get_item_list()');
		return $result;
	}

	function get_menu_category_list() {
		$result = $this->query('call get_menu_category_list()');
		return $result;
	}

	function get_menu_item_by_id($id) {
		$result = $this->query('call get_menu_item_by_id("' . $id . '")');
		return $result;
	}

	function get_menuCategoryList_by__id($id) {
		$result = $this->query('call get_menuCategoryList_by__id("' . $id . '")');
		return $result;
	}

	/**
	 * Overridden paginate method - group by week, away_team_id and home_team_id
	 */
	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$recursive = -1;
		//$group = $fields = array('a_party');
		//return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
		$sql = "SELECT items.id,items.name,items.nameArabic,items.imageUrl, Category.name 
FROM items
INNER JOIN items Category ON (items.parentId= Category.id)
WHERE items.is_item=0 AND items.is_sideline_category =0 AND items.status =1
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
		$results = $this->query($sql);
		return $results;
	}

	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT items.id,items.name,items.nameArabic,items.imageUrl, Category.name 
FROM items
INNER JOIN items Category ON (items.parentId= Category.id)
WHERE items.is_item=0 AND items.is_sideline_category =0 AND items.status =1";
//    if ($conditions['Blacklist.b_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.b_party = \'' . $conditions['Blacklist.b_party'] . '\'';
//    } else if ($conditions['Blacklist.a_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.a_party = \'' . $conditions['Blacklist.a_party'] . '\'';
//    }
		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}

	/**
	 * Overridden paginate method - group by week, away_team_id and home_team_id
	 */
	public function paginate2($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$recursive = -1;
		//$group = $fields = array('a_party');
		//return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
		$sql = "SELECT items.id,items.name,items.nameArabic ,items.imageUrl ,Category.name ,items.price,items.displayOrder,
	items.description,items.descriptionArabic
 FROM items 
INNER JOIN items Category ON (items.parentId = Category.id )
WHERE items.is_item =1 AND items.status =1
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
		$results = $this->query($sql);
		return $results;
	}

	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount2($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT items.id,items.name,items.nameArabic ,items.imageUrl ,Category.name ,items.price,items.displayOrder,
	items.description,items.descriptionArabic
 FROM items 
INNER JOIN items Category ON (items.parentId = Category.id )
WHERE items.is_item =1 AND items.status =1";
//    if ($conditions['Blacklist.b_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.b_party = \'' . $conditions['Blacklist.b_party'] . '\'';
//    } else if ($conditions['Blacklist.a_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.a_party = \'' . $conditions['Blacklist.a_party'] . '\'';
//    }
		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}
	function get_menu_item_names() {
		$result = $this->query('call get_menu_item_names()');
		return $result;
	}

}

?>