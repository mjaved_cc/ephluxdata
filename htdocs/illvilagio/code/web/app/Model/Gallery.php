<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Gallery extends AppModel {
    
	function getGalleryInfo($lang_name,$modified_date)
	{
		$result = $this->query('call get_gallery("'.$lang_name.'","'.$modified_date.'")');
		
		//print_r($result[0]['galleries']);
		//exit;
		//print_r($result[0]['galleries']);
		///exit;
		return $result;
	
	}
    
    /*function _getGalleryInfo()
    {
	$result = $this->query('call get_gallery()');
	return $result;
		
    }*/
    
    function add_galleryInfo($add_images_info,$file_info){

    	//print_r($add_images_info);
    	//print_r($file_info);
    	//exit;
    	
        	$result = $this->query('call add_gallery("'.$add_images_info['name'].'","'.$file_info['name'].'","1","'.$add_images_info['name_arabic'].'","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'" ,"'.$add_images_info['caption'].'","'.$add_images_info['image_caption_arabic'].'" )');
            
        	
        	//print_r($result);
        	//exit;
        	return $result[0]['galleries'];
    

        //pr($parameters_all);
    }
    
    function edit_galleryInfo($add_images_info,$file_info){
    
    	//print_r($add_images_info);
    	//print_r($file_info);
    	//exit;
				
    	$result = $this->query('call edit_gallery("'.$add_images_info['id'].'","'.$add_images_info['name'].'","'.$file_info['name'].'","1","'.$add_images_info['name_arabic'].'","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'" ,"'.$add_images_info['caption'].'","'.$add_images_info['image_caption_arabic'].'" )');   
    	    
    	return $result[0]['galleries'];    
    
    	//pr($parameters_all);
    }
    
    function delete_galleryInfo($add_images_info){
    
    	//print_r($add_images_info);
    	//print_r($file_info);
    	//exit;
    
    	//$result = $this->query('call delete_gallery("'.$add_images_info['id'].'")');
    	$result = $this->query('call delete_gallery("'.$add_images_info.'")');
    
    	//print_r($result);
    	//exit;
    	return $result[0]['galleries'];
    
    
    	//pr($parameters_all);
    }
    
    function get_image_gallery_id($id){
		$result = $this->query('call get_gallery_id("'.$id.'")');		
		return $result;
	}
    
	function galleries_count(){
		//count_galleries
		$result = $this->query('call count_galleries()');		
		return $result;
	}
	
    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
    $recursive = -1;
    //$group = $fields = array('a_party');
    //return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
   $sql = "SELECT 
		Gallery.id,
		Gallery.name ,
		Gallery.image_name,
		Gallery.status,
		Gallery.name_arabic ,
		Gallery.modified,
		Gallery.created,
		Gallery.image_caption,
		Gallery.image_caption_arabic
		
		
	FROM galleries AS gallery
	WHERE Gallery.status = 1 
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
    $results = $this->query($sql);
    return $results;
}

/**
 * Overridden paginateCount method
 */
   public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
    $sql = "SELECT 
		Gallery.id,
		Gallery.name ,
		Gallery.image_name,
		Gallery.status,
		Gallery.name_arabic ,
		Gallery.modified,
		Gallery.created,
		Gallery.image_caption,
		Gallery.image_caption_arabic
		
		
	FROM galleries AS gallery
	WHERE Gallery.status = 1 ;";
//    if ($conditions['Blacklist.b_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.b_party = \'' . $conditions['Blacklist.b_party'] . '\'';
//    } else if ($conditions['Blacklist.a_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.a_party = \'' . $conditions['Blacklist.a_party'] . '\'';
//    }
    $this->recursive = $recursive;
    $results = $this->query($sql);
    return count($results);
}
}

?>
