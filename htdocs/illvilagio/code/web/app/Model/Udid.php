<?php

class Udid extends AppModel {

	public $name = 'Udid';

	/**
	 * Check this email already exist in tmp table or not
	 * @param email $email Email address
	 * @return int no of records
	 */
	function is_id_exists( $id) { 

//        $is_unverified_exist = $this->find('count', array(
//			'conditions' => array('type' => 'registration', 'target_id' => $email)
//				));		

		$is_udid = $this->query('CALL get_udid("' . $id . '")');  

		return $is_udid;
	}
	
	function save_udid( $id, $version ) {
		
		$this->query('CALL save_udid("' . $id . '", "' . $version . '")');
	}
	
}

	