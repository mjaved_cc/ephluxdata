<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ReservationTable extends AppModel {

	//public $useTable = 'reservations';
	public $validate = array(
		'floor_id' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'enter must be in numeric digits '
			)
		),
		'table_id' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'enter must be in numeric digits '
			)
		)
	);

}

?>