<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Order extends AppModel {
	//public $useTable = 'orders';
	
	const PRICE = '12';
	
	public $validate = array(
		'order_type' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			)
		),
		'order_item_id' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		'order_item_quantity' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		'order_table_id' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		'order_seat_no' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		'order_amount' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		'order_amount_to_be_paid' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'Only Numbers Allowed.',
				'last' => false
			)
		),
		
//		'radioField' => array(
//        'rule' => 'notEmpty'
//     )
	);
	
    

	    
    function makeOrder($order,$table_data)
	{
		//pr($order);
		//exit;
				
		$result_makeOrderNew = $this->query('call makeOrderNew("'. $order['order_type']. '",
															   ' . $order['order_table_id'] . ',
															   "' . date('Y-m-d h:i:s') .'",  
															   "' . $order['customerDetail']['name'] . '",
															   "' . $order['customerDetail']['email'] . '",
															   ' . $order['customerDetail']['telephoneNo']. ',
															   "' . $order['customerDetail']['shippingAddress']. '",
																"' . $order['no_seats']. '",
																"' . $this->get_total_price($order) . '",
																"' . $order['udid']. '"
																)');
		//$get_total = $this->get_total_price($order['items']);
		//print_r($get_total);
		//exit;
		
		//pr($this->order_items[0]);die;
		
		foreach( $order['items'] as $key => $value ){
			
			//print_r($this->get_price($value['itemId']));		
			
			$result = $this->query('call addItemsToOrder('. $result_makeOrderNew[0][0]['order_id'] . ',
														"' . date('Y-m-d') . '",
														' . $value['itemId'] . ',
														"' . $this->get_price($value['itemId']) .'",
														' . $value['quantity']  .',
														"' . $value['comment'] .'",
														"1",
														' . $order['order_table_id'] . ',
														' . $value['seatNo'] . '
														)');		
			
		}
		
		if($table_data != ''){
		foreach ($table_data as $key=>$tables){
			//print_r($tables);
				
			$result_tables = $this->query('call create_order_table(
					"' . $tables['floor_id'] . '",
					"' . $tables['table_id'] . '",
					"' . $result_makeOrderNew[0][0]['order_id'] . '")
					');
		}
		}
		
	}
	
		function editOrder($order)
		{
			
			
			$pieces = explode(",", $order['Order']['table_ids']);

			$table_data = array();
			
			foreach ($pieces as $key=>$tables){

				
				$pieces_1 = explode("T", $tables);
				//pr($pieces_1);
				if($pieces_1[0] != 'M'){
					$table_data[$key]['floor_id'] = 2;
					$table_data[$key]['table_id'] = $pieces_1[1];
				} elseif ($pieces_1[0] != 'G') {
					$table_data[$key]['floor_id'] = 1;
					$table_data[$key]['table_id'] = $pieces_1[1];
				} else {
					$table_data[$key]['floor_id'] = 0;
					$table_data[$key]['table_id'] = $pieces_1[1];
				}
			
			}
			
						
			//exit;
			if($order['order_status'] == ''){
				$order['order_status'] = 'pending';
			}
			
		
			$result_makeOrderNew = $this->query('call edit_order("'. $order['id']. '",
					"'. $order['order_type']. '",
					' . $order['order_table_id'] . ',
					"' . $order['entryDateTime'] .'",
					"' . $order['customerDetail']['name'] . '",
					"' . $order['customerDetail']['email'] . '",
					' . $order['customerDetail']['telephoneNo']. ',
					"' . $order['customerDetail']['shippingAddress']. '",
					"' . $order['no_seats']. '",
					"' . $this->get_total_price($order) . '",
					"' . $order['udid']. '",
					"' . $order['order_status']. '"
			)');
			//$get_total = $this->get_total_price($order['items']);
			//print_r($result_makeOrderNew[0]['orders']['id']);
			//exit;
		
			//pr($this->order_items[0]);die;
		
			$result_tables_del = $this->query('call order_items_delete_by_id(
					"' . $result_makeOrderNew[0]['orders']['id'] . '")
					');
			
			foreach( $order['items'] as $key => $value ){
					
				//print_r($this->get_price($value['itemId']));
					
					
					
					
				$result = $this->query('call addItemsToOrder('. $result_makeOrderNew[0]['orders']['id'] . ',
						"' . date('Y-m-d') . '",
						' . $value['itemId'] . ',
						"' . $this->get_price($value['itemId']) .'",
						' . $value['quantity']  .',
						"' . $value['comment'] .'",
						"1",
						"1",
						' . $value['seatNo'] . '
				)');
					
			}
		
			//pr($table_data);
			
			$result_tables_del = $this->query('call order_tables_delete_id(
					"' . $result_makeOrderNew[0]['orders']['id']. '")
					');
			
		if($table_data != ''){
		foreach ($table_data as $key=>$tables){
			//print_r($tables);
				
			$result_tables = $this->query('call create_order_table(
					"' . $tables['floor_id'] . '",
					"' . $tables['table_id'] . '",
					"' . $result_makeOrderNew[0]['orders']['id'] . '")
					');
		}
		}
			
			
			
		/*foreach ($order['sidelines'] as $key=>$sidelines){
			
			$result = $this->query('call addItemsToOrder('. $result_makeOrderNew[0][0]['order_id'] . ',
														 "' . date('Y-m-d') . '",
														 ' . $value['itemId'] . ',
														 "' . $this->PRICE .'",
														 ' . $value['quantity']  .',
														 "' . $value['comment'] .'",
													 	 ' . $value['active'] .',
														 ' . $order['TableId'] . ',
														 ' . $value['seatNo'] . '
			)');
			
		}*/
		
		//exit;
		//print_r($value);
		//exit;
		return $result;
		
	}

	function delete_order($order_id){
		
		//$result = $this->query('call delete_order("'.$order_id['id'].'")');	
		$result = $this->query('call delete_order("'.$order_id.'")');	
		//print_r($result);
		//exit;
		return $result;
		
	}
	
	function get_total_price($order){
		//print_r($order['items']);
		//exit;
		$item_total = array();
		foreach( $order['items'] as $key => $value ){
			//print_r($this->get_price($value['itemId']));
			//print_r($value['quantity']);
			//exit;
			 
			$item_total[$key] = $value['quantity'] * $this->get_price($value['itemId']); 
				
			
		}
		//print_r($item_total);
		//print_r(array_sum($item_total));
		//exit;
		return array_sum($item_total);
		
	}
	
	function get_price($id){
		
		//print_r($id);
		//exit;
		$price_raw = $this->query('call get_item_price('. $id . ')');
		foreach ($price_raw as $item){
			return $item['items']['price'];
		}
		
	}
	
	function get_order(){
		
		$result = $this->query('call get_orders()');	
				return $result;
		
	}
		

	function view_order($id){
		
		$result = $this->query('call get_order_by_id('. $id . ')');	
				return $result;		
	}
	
	function view_order_items($id){
		
		$result = $this->query('call get_order_items('. $id . ')');	
				return $result;
		
	}
	

	
	function get_tables_floors($id){
		$result = $this->query('call get_tables_floor('. $id . ')');
		return $result;
	}
	

	function view_table_floor($id){
		
		$result = $this->query('call get_tables_floor('. $id . ')');	
		
				return $result;
		
	}
	

	/**
 * Overridden paginate method - group by week, away_team_id and home_team_id
 */
public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
    $recursive = -1;
    //$group = $fields = array('a_party');
    //return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
   $sql = "SELECT orders.id,orders.order_taketime,orders.order_servetime,orders.order_checkout_time,orders.table_id,orders.ordertype,
orders.order_status,orders.order_tabbed_amount,orders.order_amount_paid,orders.customer_id,
info.name,orders.no_of_seats,orders.status,
items.id
 FROM orders AS orders
 LEFT JOIN customer_information AS info ON info.id =orders.customer_id 
 LEFT JOIN order_item AS items ON items.order_id = orders.id 
WHERE orders.status = 1  
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
    $results = $this->query($sql);
    return $results;
}

/**
 * Overridden paginateCount method
 */
   public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
    $sql = "SELECT orders.id,orders.order_taketime,orders.order_servetime,orders.order_checkout_time,orders.table_id,orders.ordertype,
orders.order_status,orders.order_tabbed_amount,orders.order_amount_paid,orders.customer_id,
info.name,orders.no_of_seats,orders.status,
items.id
 FROM orders AS orders
 LEFT JOIN customer_information AS info ON info.id =orders.customer_id 
 LEFT JOIN order_item AS items ON items.order_id = orders.id 
WHERE orders.status = 1";  
//    if ($conditions['Blacklist.b_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.b_party = \'' . $conditions['Blacklist.b_party'] . '\'';
//    } else if ($conditions['Blacklist.a_party'] <> null) {
//        $sql = $sql . ' WHERE Blacklist.a_party = \'' . $conditions['Blacklist.a_party'] . '\'';
//    }
    $this->recursive = $recursive;
    $results = $this->query($sql);
    return count($results);
}
	
	
}

?>
