<?php

class Category extends AppModel {

	public $name = 'Category';
	

	public $validate = array(
		'name' => array('notempty' => array('rule' => array('notempty'), 'message' => 'This is requried field',),),
		'imageUrl' => array('notempty' => array('rule' => array('notempty'), 'message' => 'This is requried field',),),
		'displayOrder' => array('notempty' => array('rule' => array('numeric'), 'message' => 'This is requried field',),),
		'status' => array('notempty' => array('rule' => array('numeric'), 'message' => 'This is requried field',),),
	);

		/**
	 * Create feature
	 * 
	 * @param array $data
	 */
	
	
	
	function create($data) {

		$this->query('call create_category(
			"' . $data['name'] . '",
			"' . $data['imageUrl'] . '",
			"' . $data['displayOrder'] . '",
			"' . $data['status'] . '",
			"' . $data['created'] . '",
				@result)');
		return $this->get_status();
	}
	
	
	

}