<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CustomerInformation extends AppModel {

	public $useTable = 'customer_information';

	function get_customer_by_id($id) {
		$result = $this->query('call get_customer_by_id("' . $id . '")');
		return $result;
	}

	function delete_customer($id) {
		$result = $this->query('call delete_customer("' . $id . '")');
		return $result;
	}

	/**
	 * Overridden paginate method - group by week, away_team_id and home_team_id
	 */
	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$recursive = -1;
		//$group = $fields = array('a_party');
		//return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
	$sql = "SELECT customer_information.id,customer_information.name, customer_information.telephone_no,customer_information.email,customer_information.address,customer_information.status
FROM feedbacks 
LEFT JOIN customer_information ON feedbacks.customer_id = customer_information.id
WHERE feedbacks.status =1 AND customer_information.status =1
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
		$results = $this->query($sql);
		return $results;
	}

	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT customer_information.id,customer_information.name, customer_information.telephone_no,customer_information.email,customer_information.address,customer_information.status
FROM feedbacks 
LEFT JOIN customer_information ON feedbacks.customer_id = customer_information.id
WHERE feedbacks.status =1 AND customer_information.status =1";

		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}

}
