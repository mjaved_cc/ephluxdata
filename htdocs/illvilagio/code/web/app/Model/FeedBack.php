<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Feedback extends AppModel {

	public $useTable = 'feedbacks';
	//public $customerID;
	public $customerName;
	public $telephoneNo;
	public $email;
	public $create_date_time;
	public $Address;
	public $feedBackText;
	public $validate = array(
		'name' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			)
		),
		'telephone_no' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			)
		),
		'address' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			)
		),
		'feedback_text' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field.',
				'last' => false
			)
		)
	);

	function add_feedback($feedback) {

		//print_r($feedback);
		//exit;

		$result = $this->query('call add_feedback("' . $feedback['name'] . '",
												   ' . $feedback['telephoneno'] . ',
												   "' . $feedback['email'] . '",
												   "' . date('Y-m-d h:i:s') . '",
												  "' . $feedback['address'] . '",
												  "' . $feedback['feedbacktext'] . '"
												)');
		return $result;
	}

	function get_feedback() {

		//print_r($feedback);
		//exit;

		$result = $this->query('call get_feedback()');
		return $result;
	}

	function get_customerinfo() {
		$result = $this->query('call get_customer_by_feedback_id()');
		return $result;
	}

	function delete_feedback($id) {

		$this->query('call delete_feedback("' . $id . '")');

		$msg = 'Feedback deleted now!';
		return $msg;
	}

	/**
	 * Overridden paginate method - group by week, away_team_id and home_team_id
	 */
	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$recursive = -1;
		//$group = $fields = array('a_party');
		//return $this->find('all', compact('conditions', 'fields', 'order', 'limit', 'page', 'recursive', 'group'));
		$sql = "SELECT * FROM feedbacks WHERE feedbacks.status=1
LIMIT " . (($page - 1) * $limit) . ', ' . $limit;
		$results = $this->query($sql);
		return $results;
	}

	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		$sql = "SELECT * FROM feedbacks WHERE feedbacks.status=1";

		$this->recursive = $recursive;
		$results = $this->query($sql);
		return count($results);
	}

}

?>
