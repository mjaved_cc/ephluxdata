<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Reservation extends AppModel {

	//public $useTable = 'reservations';
	public $validate = array(
		'no_of_children' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field'
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'must be in numeric digits'
			)
		),
		'no_of_oldage' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field'
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'must be in numeric digits'
			)
		),
		'confirm_via_email' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'boolean',
				'message' => 'incorrect value'
			),
		),
		'email_address' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please supply a valid email address.',
				'last' => false
			),
		),
		'phone_no' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'must be in numeric digits'
			),
		),
		'no_of_guest' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'numeric',
				'message' => 'must be in numeric digits'
			),
		),
		'time_to' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'datetime',
				'message' => 'enter proper datetime'
			)
		),
		'time_from' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'datetime',
				'message' => 'enter proper datetime'
			)
		)
	);
	public $hasOne = array('ReservationTable');
	
	/*
	 * Reservation 
	 * add 
	 * edit
	 * get
	 */
	
	function add_reservation($data,$table_data) {
		//SApiReservationComponent::get_reservation_time_span($reservation_info['time']);
		//print_r($data);
		//exit;
		
		
		if($data['udid'] == ''){
			$data['udid'] = 0;
		}
		
		//print_r($data);
		//exit;
		
		
		//$data['time_to'] = $data['time'];
		//$data['time_from'] = $data['time'];
		
		$result = $this->query('call create_reservation(
														"' . $data['no_of_children'] . '",
														"' . $data['no_of_oldage'] . '",
														"' . $data['confirm_via_email'] . '",
														"' . $data['email_address'] . '",
														"' . $data['phone_no'] . '",
														"' . date('Y-m-d h:i:s') . '",
														"' . $data['no_of_guest'] . '",
														"' . $data['time'] . '",
														"' . $data['time_from'] . '",														
														"' . $data['event_id'] . '",
														"' . $data['floor_id'] . '",														
														"' . $data['udid'] . '",
														"' . $data['is_notification'] .'")														
														');
		
		//print_r($result);
		//exit;
		
		foreach ($table_data as $key=>$tables){
			//print_r($tables);		
			
				$result_tables = $this->query('call create_reservation_table(
											"' . $tables['floor_id'] . '",
											"' . $tables['table_id'] . '",
											"' . $result[0]['reservations']['id'] . '")
											');
		}
		//exit;
		return $result_tables;
	}
	
	function edit_reservation($data,$table_data) {
//		print_r($data);
//		exit;
		//SApiReservationComponent::get_reservation_time_span($reservation_info['time']);
		
//		if($data['status'] != 'Pending'){
//			$data['status']= 'Approved';
//		}
		
		
		$result = $this->query('call edit_reservation(
															"' . $data['id'] . '",
															"' . $data['no_of_children'] . '",
															"' . $data['no_of_oldage'] . '",
															"' . $data['confirm_via_email'] . '",
															"' . $data['email_address'] . '",
															"' . $data['phone_no'] . '",
															"' . date('Y-m-d h:i:s') . '",
															"' . $data['no_of_guest'] . '",
															"' . $data['time_to'] . '",
															"' . $data['time_from'] . '",
															"' . $data['event_id'] . '",
															"' . $data['floor_id'] . '",
															"' . $data['table_id'] . '",
															"' . $data['status'] . '")
														');
		
		
		
		$result_tables_del = $this->query('call reservation_table_delete_by_reservation_id(
								"' . $result[0]['reservations']['id'] . '")
								');
		
		
		foreach ($table_data as $key=>$tables){
			//print_r($tables);				
				
			$result_tables = $this->query('call edit_reservation_table(
					"' . $tables['floor_id'] . '",
					"' . $tables['table_id'] . '",
					"' . $result[0]['reservations']['id'] . '")
					');
		}
		
	//print_r($result_tables);
	//exit;
		return $result[0];
	}
	
	function get_reservation(){
		$result=array();
		$result = $this->query('call get_reservation()');

		$data=array();
		//print_r($result_table);
		foreach ($result as $key=>$results){
			//print_r($results);
			$data[$key]['Reservation']=$results['Reservation'];
			$data[$key]['ReservationTable'] = $this->query('call get_reservation_table("'.$results['Reservation']['id'].'")');
			//print_r($results['Reservation']['id']);			
		}
		//print_r($data);
		//exit;
		return $data;
	}
		
	function delete_reservation($delete_reservation){		
		
		$result = $this->query('call delete_reservation("'.$delete_reservation.'")');		
		
		//print_r($result);
		//exit;
		return $result;
		
	}
	
	function verify_reservation($floor_id, $table_id, $time) {
		
		$result = $this->query('call verify_reservation("' . $floor_id . '","' . $table_id . '","' . $time . '")');
		//print_r($result);
		//exit;
		return $result[0][0]['is_allowed_reservation'];
	}
	
	
	function get_reservation_tables($id){
		$data = $this->query('call get_reservation_table("'.$id.'")');
		return $data;
	}

	function get_reservation_list(){
		$data = $this->query('call get_reservation()');
		return $data;
	}
	function get_table_floor($id){
		$data = $this->query('call get_reservation_table_floor("'.$id.'")');
		
		return $data;
	}
}

?>