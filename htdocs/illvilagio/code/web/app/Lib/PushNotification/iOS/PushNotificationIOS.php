<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PushNotificationIOS
 *
 * @author Shan
 */
class PushNotificationIOS {
	const PASS_PHRASE	= "1234";
	const REMOTE_SOCKET = 'ssl://gateway.sandbox.push.apple.com:2195';
	
	/** 
	 *
	 * @param String $message
	 * @param String $sound
	 * @return Int or Array, Returns 0-Zero on success
	 * @author Shan-e-Raza
	 * @example $result = PushNotificationIOS::send("This is a notification", "default")
	 */
	
	public static function send ($deviceToken, $message, $sound='default') {
		$socketClient = ""; 
		
		if (is_array(($socketClient = self::init()))) {
			CakeLog::write('debug', 'Couldn\'t connect to socket client' . PHP_EOL . print_r($socketClient));
			return $socketClient; 
		}
		
		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => $sound
		);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($socketClient, $msg, 8192);
		self::abort($socketClient);

		if (!$result) {
			CakeLog::write('debug', 'Error Code 2: Message not delivered' . PHP_EOL);
			return 2;
		}
		else {
			CakeLog::write('debug', 'Message successfully delivered' . PHP_EOL);
			return 0;
		}
	}

	private static function init() {
		$certificateFilename =env('DOCUMENT_ROOT').''. Router::url('/') . "app/Lib/PushNotification/iOS/ck.pem";
		
		//echo $certificateFilename;
		//exit;
		
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $certificateFilename);
		stream_context_set_option($ctx, 'ssl', 'passphrase', self::PASS_PHRASE);

		// Open a connection to the APNS server
		$fp = stream_socket_client(self::REMOTE_SOCKET, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp) return (array("code"=>1, "message"=>"Failed to connect: $err $errstr" . PHP_EOL));
		
		return $fp;
	}

	private static function abort($socketClient) {
		fclose($socketClient);
	}
}
