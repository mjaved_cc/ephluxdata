<?php

class DtGallery {

	private $_fields = array();
	private $_allowed_keys = array('id', 'name', 'image_name', 'status', 'name_arabic', 'modified', 'created', 'image_caption', 'image_caption_arabic');

	public function __construct($data = null) {
//print_r($data);
//Array ( [id] => 1 [name] => Picture 1 [image_name] => 1.png [status] => 1 [title_arabic] => رواق [modified] => 2013-06-13 16:09:48 [created] => 2013-06-13 16:09:48 [image_caption] => this is image description. [image_caption_arabic] => صالة عرضصالة عرض صال ) 
//exit;
		$this->populate_data($data);
		
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		$this->_fields[$key] = $value;
	}

	function __get($key) {
		if ($key == 'name')
			return $this->_ucfirst_name();
		else
			return $this->_fields[$key];
	}

	function get_field($lang_name) {
		if ($lang_name == 'ar'){	

			return array(
				'id' => $this->id,
				'Title' => $this->name_arabic,			
				'imageUrl' => FULL_BASE_URL . Router::url('/') . 'app/webroot/gallery/' . $this->image_name,
				'status' => $this->status,
				'caption' => $this->image_caption_arabic
			);
			}
			
			 elseif ($lang_name == 'en'){
				return array(
						'id' => $this->id,
						'Title' => $this->name,						
						'imageUrl' => FULL_BASE_URL . Router::url('/') . 'app/webroot/gallery/' . $this->image_name,
						'status' => $this->status,
						'caption' => $this->image_caption,
						
				);
			}
			
			else {
				return array(
						'id' => $this->id,
						'Title' => $this->name,
						'Title_arabic' => $this->name_arabic,
						'imageUrl' => FULL_BASE_URL . Router::url('/') . 'app/webroot/gallery/' . $this->image_name,
						'status' => $this->status,
						'caption' => $this->image_caption,
						'caption_arabic' => $this->image_caption_arabic
				);
			}
		}
	
	/* Upper Case Each Word of Title */
	private function _ucfirst_name() {
		return ucfirst($this->_fields['name']);
	}

}

?>
