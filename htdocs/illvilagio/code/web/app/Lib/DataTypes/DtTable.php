<?php

class DtTable {

	private $_fields = array();
	private $_allowed_keys = array( 'status', 'state' );

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		$this->_fields[$key] = $value;
	}

	function __get($key) {
		return $this->_fields[$key];
	}

	function get_field() {
		return array(
			'status' => $this->status,
			'state' => $this->state
		);
	}
}

?>
