<?php

class DtEvent {

	private $_fields = array();
	private $_allowed_keys = array('id', 'title', 'description', 'participate_description', 'event_time','name', 'status', 'created', 'modified','title_arabic','description_arabic','particapte_description_arabic','is_promoted_home','is_deal');

	public function __construct($data = null) {
				        
		//print_r($data);
		//die();
		$this->populate_data($data);
		
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}

	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
			
			else if ($key == 'name') {
				return $this->_ucfirst_name();
			}
			else				
				return $this->_fields[$key];
			
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'title' => $this->_ucfirst_name(),
			'description' => $this->description,
			'particapte_description' => $this->participate_description,
			'event_time' => $this->event_time,
			'name' => $this->name,
			'status' => $this->status,
			'created' => $this->created,
			'modified' => $this->modified,
			
		);
	}

	/**
	 * Add single event_image  to object
	 * 
	 * @param array $event_image event images of resturant
	 */
	function add_event_image($event_image) {
		if (isset($this->_fields["EventImage"]) && !is_array($this->_fields["EventImage"])) {
			$prev_value = $this->EventImage; // restore previous value
			unset($this->_fields["EventImage"]); // now unset it so that we can convert object to array
			$this->_fields["EventImage"][$prev_value->id] = $prev_value;
			$this->_fields["EventImage"][$event_image['id']] = new DtEventImage($event_image);
		} else if (isset($this->_fields["EventImage"]) && is_array($this->_fields["EventImage"]))
			$this->_fields["EventImage"][$event_image['id']] = new DtEventImage($event_image);
		else
			$this->_fields["EventImage"] = new DtEventImage($event_image);
	}

	/**
	 * Add multiple event_image to object
	 * 
	 * @param array $event_images images of resturant
	 */
	function add_event_images($event_images) {
		if (!empty($event_images)) {
			foreach ($event_images as $value)
				$this->add_event_image($value);
		}
	}

	/**
	 * Get total number of  event_image object at given row 
	 * 
	 * @return object
	 */
	function get_event_image_obj_at_row($key) {

		if (is_array($this->_fields['EventImage']) &&
				$this->_fields['EventImage'][$key] instanceof DtEventImage) {
			return $this->_fields['EventImage'][$key];
		} else if ($this->_fields['EventImage'] instanceof DtEventImage) {
			return $this->_fields['EventImage'];
		}
	}

	/* Upper Case Each Word of Title */

	private function _ucfirst_name() {
		return ucfirst($this->_fields['title']);
	}
		
	

}

?>
