<?php

class DtReservationTable {

	private $_fields = array();
	private $_allowed_keys = array('id', 'floor_id','table_id', 'reservation_id', 'status');

		
public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}

	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
									
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'floor_id' => $this->floor_id,
			'table_id' => $this->table_id,
			'reservation_id' => $this->reservation_id,
			'status' => $this->status,
			
		);
	}
	
}
	?>