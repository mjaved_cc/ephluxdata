<?php

class DtMenu {

	private $_fields = array();
	private $_allowed_keys = array('id', 'name', 'image_url', 'display_order', 'status', 'created', 'modified');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'name' => $this->name,
			'image_url' => $this->image_url,
			'display_order' => $this->display_order,
			'status' => $this->status,
			'created' => $this->created,
			'modified' => $this->modified,
		);
	}

}

?>
