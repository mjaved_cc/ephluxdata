<?php

class DtMenu {

	private $_fields = array();
	private $_allowed_keys = array('id','parentId','lft','rght', 'name', 'imageUrl', 'displayOrder', 'status', 'created', 'lastUpdate','extra','nameArabic','price','description','descriptionArabic','is_item','is_sideline_category','allow_sideline');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'lastUpdate';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'lastUpdate') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {
		
		return array(
			'id' => $this->id,
			'name' => $this->name,
			'nameArabic' => $this->nameArabic,
			'imageUrl' => $this->imageUrl,
			'description' => $this->description,
			'descriptionArabic' => $this->descriptionArabic,
			'price' => $this->price,
			'displayOrder' => $this->displayOrder,
			'status' => $this->status,
			'created' => $this->created,
			'lastUpdate' => $this->lastUpdate,
			'parentId'	=> $this->parentId,
			'isSidelineCategory'	=> $this->is_sideline_category,
			'allowSideline'	=> $this->allow_sideline
		);
	}
	
	
	function add_category(){
		if (isset($this->_fields["UserSocialAccount"]) && !is_array($this->_fields["UserSocialAccount"])) {
			$prev_value = $this->UserSocialAccount; // restore previous value
			unset($this->_fields["UserSocialAccount"]); // now unset it so that we can convert object to array
			$this->_fields["UserSocialAccount"][] = $prev_value;
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		} else if (isset($this->_fields["UserSocialAccount"]) && is_array($this->_fields["UserSocialAccount"]))
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		else
			$this->_fields["UserSocialAccount"] = new DtUserSocialAccount($user_social_data);
	}

}

?>
