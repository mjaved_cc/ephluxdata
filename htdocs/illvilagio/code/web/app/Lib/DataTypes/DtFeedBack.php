<?php

class DtFeedBack {

	private $_fields = array();
	private $_allowed_keys = array('id', 'feedback_text', 'customer_id', 'created', 'modified', 'status');

	public function __construct($data = null) {

		//print_r($data);
		//exit;
		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		$this->_fields[$key] = $value;
	}

	function __get($key) {
		return $this->_fields[$key];
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'feedback_text' => $this->feedback_text,
			'customer_id' => $this->customer_id,
			'created' => $this->created,	
			'modified' => $this->modified,	
			'status' => $this->status
		);
	}
}

?>
