<?php

class DtCategoryItem {

	private $_fields = array();
	private $_allowed_keys = array('id','cat_id', 'name','description','price', 'image_url', 'display_order', 'status','FroceModifier','istab','ParentItemId','ChildItems', 'created', 'modified','item_name','isforce_modifier','is_side_line');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'cat_id' => $this->cat_id,
			'name' => $this->name,
			'description' => $this->description,
			'price' => $this->price,
			'image_url' => $this->id,
			'display_order' => $this->id,
			'status' => $this->id,
			'ForceModifier' => $this->id,
			'istab' => $this->id,
			'ParentItemId' => $this->ParentItemId,
			'ChildItems' => $this->ChildItems,
			'created' => $this->created,
			'modified' => $this->modified,
			'item_name' => $this->item_name,
			'isforce_modifier' => $this->isforce_modifier,
			'is_side_line' => $this->is_side_line,
		);
	}

}

?>
