<?php

class DtCustomer {

	private $_fields = array();
	private $_allowed_keys = array('id', 'name', 'telephone_no', 'email', 'address');

	public function __construct($data = null) {
				        
		//print_r($data);
		//die();
		$this->populate_data($data);
		
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}

	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
			
			else if ($key == 'name') {
				return $this->_ucfirst_name();
			}
			else				
				return $this->_fields[$key];
			
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'name' => $this->_ucfirst_name(),
			'telephone_no' => $this->telephone_no,
			'email' => $this->email,
			'address' => $this->address		
			
		);
	}
	/* Upper Case Each Word of Title */

	private function _ucfirst_name() {
		return ucfirst($this->_fields['name']);
	}
		
	

}

?>
