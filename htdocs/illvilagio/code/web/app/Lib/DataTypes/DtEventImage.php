<?php

class DtEventImage{

	private $_fields = array();
	private $_allowed_keys = array('id', 'image_url','event_id','created','status','title_image');

		
	public function __construct($data = null) {
	
		//print_r($data);
		//exit;
		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {
		
		return array(
			'id' => $this->id,
			'image_url' => $this->get_image_url(),
			'name_image' => $this->title_image,	
			'status' => $this->status,
			'created' => $this->created,
			'modified' => $this->modified,
			
		);
	}
	
	function get_image_url(){
		//return URL_EVENT_IMAGES . "/" . $this->id . "/" . $this->image_name;
		return FULL_BASE_URL . Router::url('/') . 'app/webroot/events/' . $this->event_id . "/" . $this->image_url;
	}
	
}