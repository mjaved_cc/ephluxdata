<?php

require_once 'DtReservationTable.php';

class DtReservation {

	private $_fields = array();
	private $_allowed_keys = array('id', 'no_of_children', 'no_of_oldage', 'confirm_via_email', 'email_address', 'phone_no', 'created',
		'no_of_guest', 'time_to', 'time_from', 'event_id');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {

			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'no_of_children' => $this->no_of_children,
			'no_of_oldage' => $this->no_of_oldage,
			'confirm_via_email' => $this->confirm_via_email,
			'email_address' => $this->email_address,
			'phone_no' => $this->phone_no,
			'created' => $this->created,
			'no_of_guest' => $this->no_of_guest,
			'time_to' => $this->time_to,
			'time_from' => $this->time_from,
			'event_id' => $this->event_id,
		);
	}

	function add_reservation_table($reservation_info) {

		if (isset($this->_fields["ReservationTable"]) && !is_array($this->_fields["ReservationTable"])) {
			$prev_value = $this->ReservationTable; // restore previous value
			unset($this->_fields["ReservationTable"]); // now unset it so that we can convert object to array
			$this->_fields["ReservationTable"][$prev_value->id] = $prev_value;
			$this->_fields["ReservationTable"][$reservation_info['id']] = new DtReservationTable($reservation_info);
		} else if (isset($this->_fields["ReservationTable"]) && is_array($this->_fields["ReservationTable"]))
			$this->_fields["ReservationTable"][$reservation_info['id']] = new DtReservationTable($reservation_info);
		else
			$this->_fields["ReservationTable"] = new DtReservationTable($reservation_info);
	}

}

?>