<?php

class DtDate{
	
	const FORMAT_MYSQL = 1;
	const FORMAT_TIMESPAN = 2;
	public $date;
	
	function __construct($mySqlDate){
		$this->date = $this->formatToMySql(strtotime($mySqlDate));
	}
	
	public function getDate(){
		return $this->date;
	}
	
	public function addDays($days, $flag = self::FORMAT_MYSQL){
		$newDate = strtotime('+'.$days, $this->date);
		
		if ($flag == self::FORMAT_MYSQL )
			$newDate = $this->formatToMySql($newDate);
		
		return $newDate;
	}
	
	public function formatToMySql($date){
		return date('Y-m-d-H-i-s', $date);
	}
	
	private function getDaysDiff($endDate){
		$days = round( ($endDate - $this->date)/86400 , 0);
		//$days = ($days < 0)? ceil($days): floor($days);
		//$days = ($days == 1)? $days.' day': $days.' days';
		return $days;
	}
	
	private function getMonthsDiff($endDate){
		$days 	= $this->getDaysDiff($endDate);
		$months = round( $days / 30 , 0);
		//$months = ($months < 0)? ceil($months): floor($months);
		//$months = ($months == 1)? $months.' month': $months.' months';
		return $months;
	}
	
	private function getYearsDiff($endDate){
		$months	= $this->getMonthsDiff($endDate);
		$years  = round( $months / 12 , 0);
		//$years = ($years < 0)? ceil($years): floor($years);
		//$years  = ($years == 1)? $years.' year': $years.' years';
		return $years;
	}
	
	/*private function getHoursDiff($endDate){
		$days 	= $this->getDaysDiff($endDate);
		$hours	= $days * 24;
		return $hours;
	}
	
	private function getMinutesDiff($endDate){
		$hours = $this->getHoursDiff($endDate);
		$minutes = $hours * 60;
		return $minutes;
	}
	
	private function getSecondsDiff($endDate){
		$minutes = $this->getMinutesDiff($endDate);
		$seconds = $minutes * 60;
		return $seconds;
	}*/
	
	public function getDifference($endDate){
		$days = $this->getDaysDiff($endDate);
		if($days >= 30 || $days <= -30){
			$months = $this->getMonthsDiff($endDate);
			if($months >= 12 || $months <= -12){
				$years = $this->getYearsDiff($endDate);
				return ($years == 1 || $years == -1)? $years.' year': $years.' years';
			}else{
				return ($months == 1 || $months == -1)? $months.' month': $months.' months';
			}
		}elseif ($days == 0){
			return 'today';
		}else {
			return ($days == 1 || $days == -1)? $days.' day': $days.' days';
		}
		
	}
	
	protected function getServerTimeDifference($endDate){
		$todays_time = strtotime(date('Y-m-d H:i:s'));
		$days = ($todays_time - $endDate)/86400;
		if($days < 0)
		{
			$interval = ' remaining';
			$days = abs($days);
		}
		else
		{
			$interval = ' ago';
		}
		// if less than 1 day difference, then calculate time difference
		if(round($days, 0) < 1)
		{
			$seconds = $days*24*60*60;
			if(round($seconds, 0) > 59)
			{
				$minutes = $days*24*60;
				if(round($minutes, 0) > 59)
				{
					$hours	= $days * 24;
					$returnVal	= (round($hours, 0) == 1)? round($hours, 0).' hour': round($hours, 0).' hours';
				}
				else
					$returnVal	= (round($minutes, 0) == 1)? round($minutes, 0).' minute': round($minutes, 0).' minutes';
			}
			else
			{
				$returnVal	= (round($seconds, 0) == 1)? round($seconds, 0).' second': round($seconds, 0).' seconds';
				//$returnVal	= ' moments';
			}
		} 
		elseif(round($days, 0) > 364)
		{ // calculate years
			$years	= $days / 365;
			$returnVal	= (round($years, 0) == 1)? round($years, 0).' year': round($years, 0).' years';
		} 
		elseif (round($days, 0) > 30)
		{ // calculate months
			$months	= $days / 30;
			$returnVal	= (round($months, 0) == 1)? round($months, 0).' month': round($months, 0).' months'; 
		} 
		elseif (round($days, 0) > 6)
		{ // calculate weeks
			$weeks 	= $days / 7;
			if(round($weeks, 0) == 4)
				$returnVal = '1 month';
			else
				$returnVal = (round($weeks, 0) == 1)? round($weeks, 0).' week': round($weeks, 0).' weeks';
		}
		else
		{
			$returnVal	= (round($days, 0) == 1)? round($days, 0).' day': round($days, 0).' days';
		}
		
		return $returnVal.$interval;
	}
	
	public function formatToLongDate($date = null){
		return date('F-j-Y', $date);
	}
	
	public function getFormattedElapsedTime(){
		return $this->getServerTimeDifference($this->date);
	}
	
	public static function getServerTimeElapsed($mySqlDate){
		return self::getServerTimeDifference(strtotime($mySqlDate));
	}
}