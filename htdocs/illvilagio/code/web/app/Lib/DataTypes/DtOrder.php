<?php

class DtOrder {

	private $_fields = array();
	private $_allowed_keys = array('id', 'order_taketime', 'order_servetime', 'order_checkout_time', 'table_id', 'ordertype', 'order_status', 'order_tabbed_amount', 'status','customer_id');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
		//pr($this->$key);die;
	}

	function __set($key, $value) {
		$this->_fields[$key] = $value;
	}

	function __get($key) {
		return $this->_fields[$key];
	}

	function get_field() {
		 return array(
			'id' => $this->id,
			'ordertype' => $this->ordertype,
			'table_id' => $this->table_id
		);
		
	}
}

?>
