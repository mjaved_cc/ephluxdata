<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class DtItem{

	private $_fields = array();
	private $_allowed_keys = array('id','parent_id','lft','rght', 'name', 'image_name', 'display_order', 'status', 'created', 'modified','extra','name_arabic','price','description');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if (($key == 'created' || $key == 'modified') && isset($this->_fields[$key])) {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {
		
		return array(
			'id' => $this->id,
			'parent_id' => $this->parent_id,
			'lft' => $this->lft,
			'rght' => $this->rght,
			'name' => $this->name,
			'image_name' => $this->image_name,
			'display_order' => $this->display_order,
			'status' => $this->status,
			'created' => $this->created,
			'extra' => $this->extra,
			'name_arabic' => $this->name_arabic,
			'price' => $this->price,
			'description' => $this->description,
					);
	}
}
?>
