<?php
App::uses('Sanitize', 'Utility');
class DtContent {
	public $content;
	private $_fields = array();
	private $_allowed_keys = array('id', 'title', 'description','status', 'key' ,'title_arabic' , 'description_arabic', 'modified', 'created');
	
	
	function __construct($data){
		
		
		$this->populate_data($data);
	}	
	
	function populate_data($data) {
		
		foreach ($data as $key => $value) {
			
			if (in_array($key, $this->_allowed_keys))
				
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}
	
	function get_field() {
	
		return array(
				'id' => $this->id,
				'title' => $this->title,
				'description' => $this->description,
				'status' => $this->status,
				'key' => $this->key,
				'title_arabic' => $this->title_arabic,
				'description_arabic' => $this->description_arabic,
				'modified' => $this->modified,
				'created' => $this->created,
		);
	}
	
	function htmlContent() {
		return $this->content;	
	}
	
	function textConent() {
		return Sanitize::html($this->content,array('remove' => true));
	}
}
?>