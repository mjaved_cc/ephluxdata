//
//  CenterViewController.h
//  SideViews
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 salman.ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView   *homeScrollView;
    IBOutlet UIButton    *btnTodayDeal;
    IBOutlet UIButton    *btnToNightDeal;
    IBOutlet UIButton    *btnSpecialDeal;
    IBOutlet UIImageView    *imageEventFirst;
    IBOutlet UIImageView    *imageEventSecond;
    IBOutlet UIImageView    *imageToNightDeal;
    IBOutlet UIImageView    *imageSpecialDeal;
    IBOutlet UILabel    *lblImageOneDish;
    IBOutlet UILabel    *lblImageSecondDate;
    
    NSMutableArray* dealArray;
    NSMutableArray* eventArray;
    
    
}
@end
