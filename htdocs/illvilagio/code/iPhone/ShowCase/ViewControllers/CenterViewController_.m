//
//  CenterViewController.m
//  SideViews
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 salman.ahmed. All rights reserved.
//

#import "CenterViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "MMExampleDrawerVisualStateManager.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "EventsViewController.h"
#import "iphone_HomeDetailVC.h"




typedef NS_ENUM(NSInteger, MMCenterViewControllerSection){
    MMCenterViewControllerSectionLeftViewState,
    MMCenterViewControllerSectionLeftDrawerAnimation,
    MMCenterViewControllerSectionRightViewState,
    MMCenterViewControllerSectionRightDrawerAnimation,
};

@interface CenterViewController ()

@end

@implementation CenterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    UITapGestureRecognizer * doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    //    [doubleTap setNumberOfTapsRequired:2];
    //    [self.view addGestureRecognizer:doubleTap];
    //
    //    UITapGestureRecognizer * twoFingerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerDoubleTap:)];
    //    [twoFingerDoubleTap setNumberOfTapsRequired:2];
    //    [twoFingerDoubleTap setNumberOfTouchesRequired:2];
    //    [self.view addGestureRecognizer:twoFingerDoubleTap];
    
    
    [self setupLeftMenuButton];
    [self setupRightMenuButton];
    [homeScrollView setBackgroundColor:[UIColor clearColor]];
    if ([Functions isIPad]){
        
    }else{
        if(IS_IPHONE5){
            homeScrollView.frame=CGRectMake(0, 150, 320, 480);
        }else{
            homeScrollView.frame=CGRectMake(0, 230, 320, 480);
        }
    }
    homeScrollView.contentSize = CGSizeMake(320, 600);
    
    UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"iphone_top_bar_logo.png"]];
    imageView.frame=CGRectMake(150, 0, 31, 44);
    self.navigationItem.titleView = imageView;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.view.layer setCornerRadius:10.0f];
    
    
}
-(IBAction)OnClick:(id)sender{
    if([sender tag]==0){
        iphone_HomeDetailVC *homeDetailVC=[[iphone_HomeDetailVC alloc]init];
        [self.navigationController pushViewController:homeDetailVC animated:YES];
        
    }else if([sender tag]==1){
        iphone_HomeDetailVC *homeDetailVC=[[iphone_HomeDetailVC alloc]init];
        [self.navigationController pushViewController:homeDetailVC animated:YES];
    }else if([sender tag]==2){
        iphone_HomeDetailVC *homeDetailVC=[[iphone_HomeDetailVC alloc]init];
        [self.navigationController pushViewController:homeDetailVC animated:YES];
    }else if([sender tag]==3){
        iphone_HomeDetailVC *homeDetailVC=[[iphone_HomeDetailVC alloc]init];
        [self.navigationController pushViewController:homeDetailVC animated:YES];
    }
}
-(IBAction) buttonAction:(id)sender{
    EventsViewController * center = [[EventsViewController alloc] init];
    [self.navigationController pushViewController:center animated:YES];
    
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
