//
//  AlertViewController.h
//   IL VILLAGGIO
//
//  Created by salman ahmed on 7/15/13.
//
//

#import <UIKit/UIKit.h>

@interface AlertViewController : UIViewController
{

    IBOutlet UILabel* lblNoOfGuest;
    IBOutlet UILabel* lblPopupTitle;
    IBOutlet UISlider* sliderPopup;
    IBOutlet UILabel* lblNumber;
    IBOutlet UILabel* lbDineIn;
    IBOutlet UIButton *btnAlertSwitch;
    IBOutlet UIButton *btnSearch;
    IBOutlet UILabel* lblBelowDineIn;
    IBOutlet UIView *addOrderView;
    IBOutlet UIView *innerOrderView;
    IBOutlet UIButton *btnSwitch;
    IBOutlet UIButton *btnOk;
}
@property(nonatomic,assign)BOOL *isOrderView;
@property(nonatomic, strong) NSString* notifyString;


-(IBAction) OkPopup:(id)sender;
-(IBAction) addOrderPopup:(id)sender;
-(IBAction)actionSwitch:(id)sender;
-(IBAction) sliderAction:(id)sender;

@end
