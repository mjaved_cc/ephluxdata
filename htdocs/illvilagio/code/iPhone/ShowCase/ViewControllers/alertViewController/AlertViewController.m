//
//  AlertViewController.m
//   IL VILLAGGIO
//
//  Created by salman ahmed on 7/15/13.
//
//

#import "AlertViewController.h"
#import "DataStoreManager + DataSpecialized.h"

@interface AlertViewController ()

@end

@implementation AlertViewController
@synthesize isOrderView,notifyString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:self.notifyString object:nil];
    [self addOrderView];
    // Do any additional setup after loading the view from its nib.
}
-(void)addOrderView{
     [Functions setOrderType:0];
    //set font of labels of popup view

    [lbDineIn setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    [lblNoOfGuest setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    [lblPopupTitle setFont:[UIFont fontWithName:@"Script MT bold" size:22.0f]];
    lbDineIn.textColor = [UIColor darkGrayColor];
    lblNoOfGuest.textColor = [UIColor darkGrayColor];
    lblPopupTitle.textColor=UIColorFromRGB(0x5b1900);
    lblNumber.textColor=UIColorFromRGB(0x5b1900);
    
    lbDineIn.text=NSLocalizedString(@"Dine In / TakeAway", @"Dine In / TakeAway");
    lblNoOfGuest.text=NSLocalizedString(@"No Of People", @"No Of People");
    lblBelowDineIn.text=NSLocalizedString(@"On for 'DineIn' , Off for 'TakeAway'", @"On for 'DineIn' , Off for 'TakeAway'");
    lblPopupTitle.text=NSLocalizedString(@"Add Order!", @"Add Order!");
     lblNumber.text = [Functions getLocalizedNumber:@"1"];
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateNormal];
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateHighlighted];
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateSelected];

    [self setPopupSlider];
    
    innerOrderView.layer.cornerRadius = 10;
    innerOrderView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    innerOrderView.layer.borderWidth = .5;
    if([Functions isDeliveryTarget]){
        
        [self setupDeliveryView];
        
    }

    //self.isOrderView = TRUE;
    
    //[self.view addSubview:addOrderView];
    //[self.view bringSubviewToFront:addOrderView];
    // NSLog(@"subview %@",[self.view subviews]);
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setBool:YES forKey:kaddOrderPopup];
    //[defaults synchronize];
    
    //}
    
}

-(void)setupDeliveryView{
    
    btnSwitch.alpha = 0;
    lbDineIn.text =NSLocalizedString( @"Delivery",@"Delivery");
    lblBelowDineIn.text =NSLocalizedString(  @"Order will be taken for DELIVERY only.",@"Order will be taken for DELIVERY only.");
    
}
-(void)setPopupSlider{
    
    sliderPopup.backgroundColor = [UIColor clearColor];
	
	UIImage *stetchRightTrack = [UIImage imageNamed:@"count_slide_bg.png"];
	
    UIImage *sliderLeftTrackImage = [[UIImage imageNamed: @"count_slide_bg_full.png"] stretchableImageWithLeftCapWidth: 9 topCapHeight: 0];
    
	[sliderPopup setThumbImage: [UIImage imageNamed:@"btn_count_slide_drag_normal.png"] forState:UIControlStateNormal];
	[sliderPopup setThumbImage: [UIImage imageNamed:@"btn_count_slide_drag_pressed.png"] forState:UIControlStateHighlighted];
	
    [sliderPopup setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
	[sliderPopup setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    
	sliderPopup.minimumValue = 1.0;
	sliderPopup.maximumValue = 4.0;
	sliderPopup.continuous = YES;
	sliderPopup.value = 2.0;
	
	[sliderPopup addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
}

-(IBAction) sliderAction:(id)sender
{
	UISlider *slider = (UISlider *)sender;
    lblNumber.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",(int)slider.value]];
    [[SingletonClass sharedInstance] setNumberOfPeople:(int)slider.value];
    
}
#pragma mark add order popup

-(IBAction) OkPopup:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:kaddOrderPopup];
    [defaults synchronize];
    [self.view removeFromSuperview];
    [[NSNotificationCenter defaultCenter] postNotificationName:self.notifyString object:nil];
}
-(IBAction) addOrderPopup:(id)sender{
   Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
    int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
    
    if(qtyTotal>0){
        [[SingletonClass sharedInstance] setNumberOfPeople:[lblNumber.text intValue]];
        NSString* alertText =NSLocalizedString( @"Do you want to discard previous order.",@"Do you want to discard previous order.");
        NSString* cancletxt=NSLocalizedString(@"Caution", @"Caution");
        [Functions showAlertCancel:cancletxt message:alertText:self];
        alertText=nil;
    }else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:NO forKey:kaddOrderPopup];
        [defaults synchronize];
        [self OkPopup:nil];
    }
    
    cashTotal = nil;
    
}
#pragma mark alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:kaddOrderPopup];
    [defaults synchronize];
    
    if(buttonIndex == 0){
       [[DataStoreManager manager] clearOrder];
        [[DataStoreManager manager] saveData];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrdersItemAdded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                                      [NSNumber numberWithBool:YES], keyStatus,nil]];
        
        [UIView animateWithDuration:1
                              delay:0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             addOrderView.alpha =0;
                         }
                         completion:^(BOOL finished){
                             [addOrderView removeFromSuperview];
                             addOrderView.alpha=1;
                             
                             
                         }];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:self.notifyString object:nil];
        
    }else{
        
        [self OkPopup:nil];
    }
    
    NSLog(@"%d",buttonIndex);
    
}

#pragma mark delivery / dine in switch button
-(IBAction)actionSwitch:(id)sender{
    
    if(btnSwitch.selected){
        btnSwitch.selected = NO;
        [Functions setOrderType:0];
        
    }
    else{
        btnSwitch.selected = YES;
         [Functions setOrderType:1];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
