//
//  FeedBackViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import "FeedBackViewController.h"
#import "MBProgressHUD.h"
#import "CenterViewController.h"

@interface FeedBackViewController ()

@end

@implementation FeedBackViewController
@synthesize progressHud;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.title = @"Feedback";
        self.title=NSLocalizedString(@"Feedback", @"Feedback");
        [Functions setNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedBackNotification:) name:kNotificationFeedBack object:nil];
    self.progressHud = [[MBProgressHUD alloc] initWithView:self.view] ;
    self.progressHud.labelText =NSLocalizedString(@"Loading...",@"Loading...");
    [self.view addSubview:self.progressHud];
    
    NSString* btnReserveTitle=NSLocalizedString(@"Send Feedback",@"Send Feedback");
    [btnFeedback setTitle:btnReserveTitle forState:UIControlStateNormal];
    NSString* btnOkTitle=NSLocalizedString(@"Ok",@"Ok");
    [btnOk setTitle:btnOkTitle forState:UIControlStateNormal];
    
    [lblName setText:NSLocalizedString(@"Name", @"Name")];
    [lblEmailAddress setText:NSLocalizedString(@"Email", @"Email")];
    [lblMidTitle setText:NSLocalizedString(@"We Welcome your Comments & Suggestions", @"We Welcome your Comments & Suggestions")];
    [lblAlertTitle setText:NSLocalizedString(@"Thanks for sending your feedback.", @"Thanks for sending your feedback.")];
    [lblFeedbackSent setText:NSLocalizedString(@"Feedback Sent!", @"Feedback Sent!")];
    [txtName setPlaceholder:NSLocalizedString(@"Name", @"Name")];
    [txtEmail setPlaceholder:NSLocalizedString(@"Email", @"Email")];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                           nil];
    [numberToolbar sizeToFit];
    txtComment.inputAccessoryView = numberToolbar;
    
    [lblFeedbackSent setFont:[UIFont fontWithName:@"Script MT bold" size:22.0f]];
    lblFeedbackSent.textColor = UIColorFromRGB(0x5b1900);
    [lblAlertTitle setFont:[UIFont fontWithName:@"Oxygen-Regular" size:18.0f]];
    //[lblEmailAddress setFont:[UIFont fontWithName:@"segoeuib_0" size:15.0f]];
    //[lblName setFont:[UIFont fontWithName:@"segoeuib_0" size:15.0f]];
    
    detailView.layer.cornerRadius = 5;
    detailView.layer.masksToBounds = YES;
    detailView.layer.borderWidth = 0.5f;
    detailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    if([Functions isIPad]){
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
    }else{
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];
        if(IS_IPHONE5){
            
            
        }
    }
}
#pragma mark FeedBack Notifire
-(void) feedBackNotification:(NSNotification*)notification{
    [self.progressHud hide:YES];
    @try {
        NSDictionary* obj=notification.userInfo;
        //if(![[obj objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:0]]){
            [self.view addSubview:popupView];
//        }else{
//            NSString* alertTitletxt=NSLocalizedString(@"Warning",@"Warning");
//            NSString* alertMsg=NSLocalizedString(@"Server Error",@"Server Error");
//            [Functions showAlert:alertTitletxt message:alertMsg];
//        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark Submit Feedback
-(IBAction)SubmitFeedback:(id)sender{
    
    if([self validateFome]){
        BOOL isConnectedWithInternet = [InternetCheck IsConnected];
        
        if (isConnectedWithInternet) {
            [ self.progressHud show:YES];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            NSString *feedbackDateString = [dateFormatter stringFromDate:[NSDate date]];
            
            NSMutableDictionary *feedback = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              txtName.text, @"name",
                                              txtEmail.text, @"email",
                                              @"0333373075",@"telephoneno",
                                             @"Suit 414",@"address",
                                             txtComment.text,@"feedbacktext",nil];
            //                                         feedbackDateString,@"create_date_time",
            //                                         nil];
            //name, telephone_no, address, feedback_text
            [[DataStoreManager manager] addFeedback:feedback];
            
        }else{
            NSString* alertTitle=NSLocalizedString(@"Not Connected to wifi",@"Not Connected to wifi");
            NSString* alertMsg=NSLocalizedString(@"Thank You",@"Thank You");
            [Functions showAlert:alertTitle message:alertMsg];
            
        }
        
    }
    
}
-(BOOL) validateFome{
    NSString* alertTitle=NSLocalizedString(@"Sorry",@"Sorry");
    
    if([txtName.text isEqualToString:@""] || [txtComment.text isEqualToString:@""]){
        NSString* alertMsg=NSLocalizedString(@"Please fill all the fields",@"Please fill all the fields");
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }else if(![self validateEmailWithString:txtEmail.text]){
        NSString* alertMsg=NSLocalizedString(@"Please enter correct email format",@"Please enter correct email format");
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }
    return TRUE;
}
#pragma mark Keyboard Hide
-(void)keyboardWillHide
{
    //call when key board hides
    [self animateTextField: Nil up: NO distance:0];
}
-(void)cancelNumberPad:(id)sender{
    [txtComment resignFirstResponder];
    [self animateTextField: sender up: NO distance:0];
    
}
#pragma mark TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    txtTempField=textField;
    //if( txtEmail==textField){
    [self animateTextField: textField up: YES distance:-100];
    return YES;
    //}
    
    // return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    [textField resignFirstResponder];
    [self animateTextField: textField up: NO distance:0];
    
    
    return YES;
}
#pragma mark TextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self animateTextField:nil up:YES distance:-200];
    
    return YES;
}
#pragma mark email validate
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark keyboard UpView
- (void) animateTextField: (UITextField*) textField up: (BOOL) up distance:(int) distance
{
    const int movementDistance =distance; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectMake(0, movementDistance,self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    [UIView commitAnimations];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        [self animateTextField: nil up: NO distance:0];
        
    }
    
    return YES;
}
-(IBAction) OkPopup:(id)sender{
    [self.progressHud hide:YES];
    [popupView removeFromSuperview];
    txtComment.text=@"";
    txtEmail.text=@"";
    txtName.text=@"";
    if([Functions isIPad]){
        NSIndexPath * ndxPath= [NSIndexPath indexPathForRow:0 inSection:0];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuClick object:ndxPath];
    }else{
        CenterViewController * homeViewController = [[CenterViewController alloc] init];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:homeViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
        
    }
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self animateTextField: nil up: NO distance:0];
    [txtTempField resignFirstResponder];
    [txtComment resignFirstResponder];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self animateTextField: nil up: NO distance:0];
    [txtTempField resignFirstResponder];
    [txtComment resignFirstResponder];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
