//
//  FeedBackViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import <UIKit/UIKit.h>
@class MBProgressHUD;
@interface FeedBackViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    IBOutlet UIView         *detailView;
    IBOutlet UITextField    *txtEmail;
    IBOutlet UITextField    *txtName;
    IBOutlet UITextView     *txtComment;
    IBOutlet UIView         *popupView;
    IBOutlet UILabel        *lblFeedbackSent;
    IBOutlet UILabel        *lblAlertTitle;
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblEmailAddress;
    IBOutlet UIButton       *btnFeedback;
    IBOutlet UIButton       *btnOk;
    IBOutlet UILabel        *lblMidTitle;
    UITextField    *txtTempField;
}
@property (nonatomic, retain) MBProgressHUD *progressHud;
-(void) feedBackNotification:(NSNotification*)notification;
-(IBAction)SubmitFeedbacks:(id)sender;
-(IBAction)SubmitFeedback:(id)sender;
-(IBAction) OkPopup:(id)sender;
@end
