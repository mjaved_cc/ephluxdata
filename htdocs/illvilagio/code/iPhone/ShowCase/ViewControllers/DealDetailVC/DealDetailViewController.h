//
//  DealDetailViewController.h
//  ShowCase
//
//  Created by salman ahmed on 6/11/13.
//
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
@interface DealDetailViewController : UIViewController<UIScrollViewDelegate,PageControlDelegate>
{
    PageControl             *pageControl;
    IBOutlet UIButton       *btnReserver;
    IBOutlet UIView         *imageBackgroundView;
    IBOutlet UIScrollView   *imageScrollView;
    IBOutlet UIScrollView   *mainScrollView;
    IBOutlet UILabel        *lbltitle;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblDis;
    IBOutlet UILabel        *lblArchive;
    IBOutlet UILabel        *lblHowTo;
    IBOutlet UILabel        *lblparticapte_description;
    NSMutableArray*         imagesArray;
    
}
@property (nonatomic,strong)NSMutableDictionary* detailDic;
@property (nonatomic,assign)BOOL isCurrent;

@end
