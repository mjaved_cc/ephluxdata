//
//  DealDetailViewController.m
//  ShowCase
//
//  Created by salman ahmed on 6/11/13.
//
//

#import "DealDetailViewController.h"
#import "AsyncImageView.h"
@interface DealDetailViewController ()

@end

@implementation DealDetailViewController
@synthesize detailDic,isCurrent;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [Functions setNavigationBar];
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lblHowTo.text=NSLocalizedString(@"How to Participate", @"How to Participate");
    [self setBackBarButton];
    @try {
        
        lbltitle.text=[detailDic objectForKey:@"name"];
        lblDate.text=[Functions getLocalizedDate:[Functions dateFormate:[detailDic objectForKey:@"event_time"]]];
        lblDis.text=[detailDic objectForKey:@"description"];
        lblparticapte_description.text=[detailDic objectForKey:@"particapte_description"];
        [lblparticapte_description sizeToFit];
        
        imageBackgroundView.layer.cornerRadius = 5;
        imageBackgroundView.layer.masksToBounds = YES;
        imageBackgroundView.layer.borderWidth = 0.5f;
        imageBackgroundView.layer.borderColor = [UIColor grayColor].CGColor;
        
        if(isCurrent){
            self.title =NSLocalizedString( @"Tonight Deal", @"Tonight Deal");
        }else{
            self.title =NSLocalizedString( @"Special Deal", @"Special Deal");
        }
        if(self.isCurrent){
            [btnReserver setHidden:NO];
            [lblArchive setHidden:YES];
        }else{
            
            [btnReserver setHidden:YES];
            [lblArchive setHidden:NO];
        }
        
        imagesArray=[[NSMutableArray alloc]init];
        imagesArray=[detailDic objectForKey:@"images"];
        
        [self CreateImageView];
        
        if([Functions isIPad]){
            
        }else{
            mainScrollView.contentSize=CGSizeMake(320, 850);
        }if(IS_IPHONE5 ){
            
            
        }else{
            
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    
}
- (void)CreateImageView {
    
    int numberOfViews=[imagesArray count];
    //imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0,280,200)];
    imageScrollView.delegate = self;
    [imageScrollView setBackgroundColor:[UIColor clearColor]];
	imageScrollView.pagingEnabled = YES;
    UIView *viewFrame=nil;
	for (int i = 0; i <[imagesArray count]; i++) {
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        viewFrame = [[UIView alloc] initWithFrame:CGRectMake(imageScrollView.frame.size.width * i, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=[[imagesArray objectAtIndex:i] objectForKey:@"image_url"];
        
        //NSString *stImageURL=@"http://playtimeandparty.com/wp-content/uploads/2013/03/breakfast-pizza.png";
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame.backgroundColor = [UIColor clearColor];
		[imageScrollView addSubview:viewFrame];
    }
    [imageScrollView setBounces:NO];
    imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width * numberOfViews, imageScrollView.frame.size.height);
    [mainScrollView addSubview:imageScrollView];
    
    pageControl = [[PageControl alloc] init ];
    if([Functions isIPad]){
        pageControl.frame = CGRectMake(130,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }else{
        pageControl.frame = CGRectMake(0,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }
    
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.delegate = self;
    pageControl.numberOfPages = numberOfViews;
    //pageControl.currentPage = myViewTag;
    if([Functions isIPad]){
        
        [self.view addSubview:pageControl];
        [self.view bringSubviewToFront:pageControl];
        
    }else{
        [mainScrollView addSubview:pageControl];
    }
    
    
    
}
- (void)old_CreateImageView {
    
    int numberOfViews=3;
    //imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0,280,200)];
    imageScrollView.delegate = self;
    [imageScrollView setBackgroundColor:[UIColor redColor]];
	imageScrollView.pagingEnabled = YES;
    UIView *viewFrame=nil;
	for (int i = 0; i <numberOfViews; i++) {
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        viewFrame = [[UIView alloc] initWithFrame:CGRectMake(imageScrollView.frame.size.width * i, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=@"http://playtimeandparty.com/wp-content/uploads/2013/03/breakfast-pizza.png";
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame.backgroundColor = [UIColor clearColor];
		[imageScrollView addSubview:viewFrame];
    }
    [imageScrollView setBounces:NO];
    imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width * numberOfViews, imageScrollView.frame.size.height);
    [mainScrollView addSubview:imageScrollView];
    
    pageControl = [[PageControl alloc] init ];
    if([Functions isIPad]){
        pageControl.frame = CGRectMake(130,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }else{
        pageControl.frame = CGRectMake(0,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }
    
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.delegate = self;
    pageControl.numberOfPages = numberOfViews;
    //pageControl.currentPage = myViewTag;
    if([Functions isIPad]){
        
        [self.view addSubview:pageControl];
        [self.view bringSubviewToFront:pageControl];
        
    }else{
        [mainScrollView addSubview:pageControl];
    }
    
    
    
}

#pragma mark Scrollview and PageControll Delegate
- (void) scrollViewDidScroll: (UIScrollView *) aScrollView
{
	CGPoint offset = aScrollView.contentOffset;
    
	pageControl.currentPage = offset.x / imageScrollView.frame.size.width;
    //NSLog(@"%d ",pageControl.currentPage);
    //[self setTitle_:pageControl.currentPage];
}
-(void) setTitle_:(int)pageNo{
    //NSLog(@"Last %d , page no %d ",lastPageNo,pageNo);
    
    //
    //    if(pageNo==0 ||pageNo==1 ){
    //        lblTitle.text=@";
    //      }
}
- (void)pageControlPageDidChange:(PageControl *)pageControler{
    int page = pageControler.currentPage;
    
    CGRect frame = imageScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [imageScrollView scrollRectToVisible:frame animated:YES];
}


-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) viewDidDisappear:(BOOL)animated{
    btnReserver.selected=NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end