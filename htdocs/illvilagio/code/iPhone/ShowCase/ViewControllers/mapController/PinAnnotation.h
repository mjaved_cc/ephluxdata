//
//  PinAnnotation.h
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface PinAnnotation : NSObject <MKAnnotation> {
	
	CLLocationCoordinate2D	coordinate;
	NSString*				title;
	NSString*				subtitle;
}

@property (nonatomic, assign)	CLLocationCoordinate2D	coordinate;
@property (nonatomic, copy)		NSString*				title;
@property (nonatomic, copy)		NSString*				subtitle;

-(id) initWithCoordinate:(CLLocationCoordinate2D)_coordinate title:(NSString*)_title subtitle:(NSString *)_subTitle;

@end
