//
//  GoogleMapViewController.m
//  ShowCase
//
//  Created by salman ahmed on 6/6/13.
//
//

#import "GoogleMapViewController.h"

@interface GoogleMapViewController ()

@end

@implementation GoogleMapViewController
@synthesize urlString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Distance";
        [Functions setNavigationBar];

    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if([Functions isIPad]){
        
    }else{
        
        [self setBackBarButton];
        
               
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:urlString];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [webView loadRequest:requestObj];

        
    }if(IS_IPHONE5 ){
      
        
    }else{
       
    }

}
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
