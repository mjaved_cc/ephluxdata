//
//  LocationGetter.m
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import "LocationGetter.h"

static LocationGetter *manager = nil;
@implementation LocationGetter
@synthesize delegateLocationGetter;

#pragma mark -
#pragma mark Static Call for object creation
+(id)manager {
	@synchronized(self) {
		if (manager == nil) {
			manager = [[self alloc] init];
		}
	}
	return manager;
}

#pragma mark -
#pragma mark Life Cycle
-(id)init {
	self = [super init];
	if(self != nil) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
	}
	
	return self;
}

- (void)startUpdates{
    if (locationManager == nil)
        locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)stopUpdates{
    if (locationManager != nil) {
        [locationManager stopUpdatingLocation];
    }
}

#pragma mark -
#pragma mark CLLocationManager delegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
	[delegateLocationGetter didFailUpdateLocation:manager err:error];
}

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manage didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	//    NSLog(@"didUpdateToLocation");
    [delegateLocationGetter didUpdateLocation:newLocation];
}
@end

