//
//  PinAnnotation.m
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import "PinAnnotation.h"

@implementation PinAnnotation
@synthesize title;
@synthesize subtitle;
@synthesize coordinate;

-(id) initWithCoordinate:(CLLocationCoordinate2D)_coordinate title:(NSString*)_title subtitle:(NSString *)_subTitle
{
	self = [super init];
	coordinate = _coordinate;
	title      = _title;
    subtitle   = _subTitle;
	return self;
}

@end
