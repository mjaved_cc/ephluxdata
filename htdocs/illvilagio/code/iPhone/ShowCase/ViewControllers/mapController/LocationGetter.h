//
//  LocationGetter.h
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "MapKit/MapKit.h"

//@protocol LocationGetterDelegate <NSObject>
@protocol LocationGetterDelegate

- (void)didUpdateLocation:(CLLocation *)location;
//- (void)didFailUpdateLocation;
- (void)didFailUpdateLocation:(CLLocationManager *)manager err:(NSError *)error;

@end

@interface LocationGetter : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
	
    id delegateLocationGetter;
	//	id <LocationGetterDelegate> delegateLocationGetter;
}

//@property (nonatomic, assign) id <LocationGetterDelegate> delegateLocationGetter;

@property(nonatomic,retain)id delegateLocationGetter;

+(id)manager;

- (void)startUpdates;
- (void)stopUpdates;

@end
