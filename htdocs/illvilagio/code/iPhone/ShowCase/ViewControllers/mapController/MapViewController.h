//
//  MapViewController.h
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LocationGetter.h"

@interface MapViewController : UIViewController<MKMapViewDelegate, LocationGetterDelegate>
{
	IBOutlet MKMapView *mvLocation;
	CLLocation *currentLocation;
    
    UIButton *btnDirections;
	
}
@end
