//
//  MapViewController.m
//  ShowCaseMaps
//
//  Created by Ammad on 5/23/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//

#import "MapViewController.h"
#import "PinAnnotation.h"
#import "GoogleMapViewController.h"
@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      //self.title=@"Map";
        self.title=NSLocalizedString(@"Map",@"Map");
       [Functions setNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	
    LocationGetter *locationGetter = [LocationGetter manager];
    locationGetter.delegateLocationGetter = self;
    [locationGetter startUpdates];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = -26.0446763554;
    coordinate.longitude = 27.9922105726;
    mvLocation.showsUserLocation = YES;
    NSString* titletxt=NSLocalizedString(@"IL VILLAGGIO",@"IL VILLAGGIO");
    PinAnnotation *targetAnnotation = [[PinAnnotation alloc]initWithCoordinate:coordinate title:titletxt subtitle:@""];
    [mvLocation addAnnotation:targetAnnotation];
    
    
    btnDirections = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnDirections setTitle:@"Distance" forState:UIControlStateNormal];
    btnDirections.frame = CGRectMake(100.0, 10.0, 100.0, 40.0);
    [btnDirections addTarget:self action:@selector(showDirections:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:btnDirections];
   
    if([Functions isIPad]){
        
    }else{
        
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];
        
                

        if(IS_IPHONE5 ){
           
        }else{
           
        }
    }
	
}

-(void)showDirections:(id)sender
{
	UIApplication *app = [UIApplication sharedApplication];
    
    //[mvLocation setHidden:YES];	//for google maps.
    NSString *coordinates = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%f,%f&saddr=%f,%f", 24.852017, 67.033939, currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];

	//for apple maps. directions not supported in many regions of the world
//	NSString *coordinates = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%f,%f&saddr=%f,%f", 24.852017, 67.033939, currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
	/*
    [app openURL:[NSURL URLWithString: coordinates]];
    */
    
GoogleMapViewController* googleVC=[[GoogleMapViewController alloc]initWithNibName:@"GoogleMapViewController" bundle:nil];
googleVC.urlString=coordinates;
[self.navigationController pushViewController:googleVC animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - map kit delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
 
	if (annotation == mapView.userLocation)
	{
       
        MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
        MKCoordinateRegion region = MKCoordinateRegionMake(mapView.userLocation.coordinate, span);
        [mvLocation setRegion:region animated:YES];
        [mvLocation regionThatFits:region];
       
		//if (annotation==mapView.userLocation)
		//	return nil;
	}
    
    static NSString *myAnnotationIdentifier = @"annot";
	
    MKPinAnnotationView *pv = (MKPinAnnotationView *)[mapView
													  dequeueReusableAnnotationViewWithIdentifier:myAnnotationIdentifier];
    if (!pv)
    {
        pv = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
											  reuseIdentifier:myAnnotationIdentifier];
        [pv setPinColor:MKPinAnnotationColorRed];
        [pv setCanShowCallout:YES];
    }
    else
    {
        //we're re-using an annotation view
        //update annotation property in case re-used view was for another
        pv.annotation = annotation;
    }
	
    return pv;
    
}

#pragma mark - Location Getter Delegate

//- (void)didFailUpdateLocation
- (void)didFailUpdateLocation:(CLLocationManager *)manager err:(NSError *)error
{
	NSLog(@"fail update location");
   
}

- (void)didUpdateLocation:(CLLocation *)location
{
	NSLog(@"location updated");
	mvLocation.showsUserLocation = YES;
    currentLocation = [location copy];
    
    
    LocationGetter *locationGetter = [LocationGetter manager];
    locationGetter.delegateLocationGetter = self;
    [locationGetter stopUpdates];
    
   
}

- (void)applicationDidFinishLaunching:(UIApplication *)application {
//    self.wvTutorial = [[WebViewController alloc] initWithNibName:@”WebView” bundle:[NSBundle mainBundle]];
//    
//    [window addSubview:[wvTutorial view]];
//    
//    // Override point for customization after app launch
//    [window makeKeyAndVisible];
}
#pragma mark - Button Handlers
-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

@end
