//
// Copyright (c) 2013 Syed Sami Ul AHbab
//

//
#import <QuartzCore/QuartzCore.h>

#import "AnyViewController.h"
#import "Defines.h"
#import "CategoryItem.h"
#import "CategoryItem+Image.h"
#import "RootViewController.h"
#import "MenuManager.h"
#import "OrderListController.h"
#import "PageControl.h"
#import "AsyncImageView.h"


@implementation AnyViewController
@synthesize detailArray;
@synthesize currentPageIndex;
@synthesize holderImage;
@synthesize category;
@synthesize selectedIndex;
@synthesize currentCategoryItem;

-(IBAction)actionBack:(id)sender{
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(IBAction)change:(id)sender{
    
    
    NSString* value=[NSString stringWithFormat:@"%d",[sender tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeMenu" object:value];
    NSArray *viewControllers = [self.navigationController viewControllers];
    for(UIViewController *viewController in viewControllers) {
        if([viewController isKindOfClass:[RootViewController class]]) {
            /* CATransition *animation=[CATransition animation];
             [animation setDelegate:self];
             [animation setDuration:0.75];UIlabe
             [animation setType:@"genieEffect"];
             [animation setFillMode:kCAFillModeRemoved];
             
             animation.endProgress=0.99;
             [animation setRemovedOnCompletion:YES];
             [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
            [self.navigationController popToViewController:viewController animated:YES];
            break;
        }
    }
}
-(void)callingMenu{
    
    @try {
        
        menuManager = [MenuManager menuManager];
        [menuManager loadMenu];
        
        /*******************************************UISCROLLVIEW************************************************/
        
        int x_axis=431;
        int width=620;
        if ([[menuManager menus] count]==1) {
            x_axis=826;
            width=200;
        }
        if ([[menuManager menus] count]==2) {
            x_axis=626;
            width=400;
        }
        UIScrollView* imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(x_axis, 53, width, 50)];
        [imageScroll setScrollEnabled:YES];
        imageScroll.backgroundColor = [UIColor clearColor];
        [imageScroll setShowsHorizontalScrollIndicator:NO];
        imageScroll.pagingEnabled = YES;
        
        int menus=[[menuManager menus] count];
        for (int j=0; j < menus; j++) {
            /*******************************************Menu****************************************************************/
            Menu *menuItem=[[menuManager menus] objectAtIndex:j];
            
            UIButton* btn=[UIButton buttonWithType:UIButtonTypeCustom];
            [btn setFrame:CGRectMake((j*199), 2, 198, 45)];
            btn.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:24.0f];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [btn setTitle:langIsArabic?menuItem.name_arabic:menuItem.name forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor darkGrayColor]];
            [btn setTag:j];
            [btn addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundImage:[UIImage imageNamed:@"Tabbtn_UnSelectedx"] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"Tabbtn_Selected"] forState:UIControlStateSelected];
            
            [imageScroll addSubview:btn];
        }
        imageScroll.contentSize = CGSizeMake(199*menus, 50);
        imageScroll.userInteractionEnabled=YES;
        imageScroll.delaysContentTouches=YES;
        [imageScroll setExclusiveTouch:YES];
        [self.view addSubview:imageScroll];
        scrollView.alpha=1;
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"Calling Menu At Detail");
    }
}


-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    @try {

        
        [self setBackBarButton];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddOrderNotification:) name:@"NotifyAnyView" object:nil];
        lblNotAvailable.text=NSLocalizedString(@"Not Available", @"Not Available");
        [btnDetails setTitle:NSLocalizedString(@"Details", @"Details") forState:UIControlStateNormal];
        [btnDetails setTitle:NSLocalizedString(@"Details", @"Details") forState:UIControlStateHighlighted];
        [btnDetails setTitle:NSLocalizedString(@"Details", @"Details") forState:UIControlStateSelected];
        
        [btnSidelines setTitle:NSLocalizedString(@"Sidelines", @"Sidelines") forState:UIControlStateNormal];
        [btnSidelines setTitle:NSLocalizedString(@"Sidelines", @"Sidelines") forState:UIControlStateHighlighted];
        [btnSidelines setTitle:NSLocalizedString(@"Sidelines", @"Sidelines") forState:UIControlStateSelected];
        
        forEveryOneValue=0.0;
        dicSeatsQuantities=[[NSMutableDictionary alloc]init];
        
        UIButton* someButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 92, 26)];
        if([lang isEqualToString:@"ar"]){
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_normal_ar.png"] forState:UIControlStateNormal];
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_pressed_ar.png"] forState:UIControlStateSelected];
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_pressed_ar.png"] forState:UIControlStateHighlighted];
            
        }else{
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_normal.png"] forState:UIControlStateNormal];
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_pressed.png"] forState:UIControlStateSelected];
            [someButton setBackgroundImage:[UIImage imageNamed:@"btn_place_order_pressed.png"] forState:UIControlStateHighlighted];
        }
        [someButton addTarget:self action:@selector(actionOrderAdd:) forControlEvents:UIControlEventTouchUpInside];
        CategoryItem *categoryItem;
        
        if(currentPageIndex == -1){
            categoryItem = currentCategoryItem;
        }else{
            categoryItem=[detailArray objectAtIndex:currentPageIndex];
        }
        
        someButton.tag=[categoryItem.backendItemID intValue];
        
        UIBarButtonItem* rightbutton = [[UIBarButtonItem alloc] initWithCustomView:someButton];
        [self.navigationItem setRightBarButtonItem:rightbutton];
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        [scrollView setScrollEnabled:NO];
        [scrollView setDelegate:self];
        scrollView.alpha=0;
        scrollView.backgroundColor = [UIColor clearColor];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        scrollView.pagingEnabled = YES;
        
        for (UIView *v in scrollView.subviews) {
            [v removeFromSuperview];
        }
        
        UIView* detailView=[[UIView alloc] initWithFrame:CGRectMake(17,20,283,176)];
        detailView=[self addItemDetailView:detailView:NO];
        [self.view addSubview:detailView];
        //[self createImageView];
        [self addDetailAndSideLine];
        
        //set first detail View
        [itemDetailOneView setHidden:NO];
        [itemDetailTwoView setHidden:YES];
        btnDetails.selected=YES;
        btnSidelines.selected=NO;
        
        [itemDetailOneView.layer setBorderWidth:0.5];
        [itemDetailOneView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        
        [itemDetailTwoView.layer setBorderWidth:0.5];
        [itemDetailTwoView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        
        [self getSideLine];
        
        
        [super viewDidLoad];
        
    }
    @catch (NSException *exception) {
        NSLog(@"TEST");
    }
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)getSideLine{
    
    BOOL isSideLineAvailable = [category.allowedSideLine intValue];
    
    if(isSideLineAvailable){
        
        NSArray* sideLineCatArray=[[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] getSideLineCategory]];
        
        
        if([sideLineCatArray count] > 0){
            
            
            Category *sideLineCat  = [sideLineCatArray objectAtIndex:0];
            
            sideLineArray=[[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] categoryItemsFor:sideLineCat]];
            if(![self.view.subviews containsObject:bgView]){
                if([sideLineArray count]>0)
                    [self createSideLineView];
            }
        }
        
        
        
    }
    
    
}


- (void)createImageView {
    
    int numberOfViews=2;
    
    CategoryItem *categoryItem;
    
    if(currentPageIndex == -1){
        categoryItem = currentCategoryItem;
    }else{
        categoryItem=[detailArray objectAtIndex:currentPageIndex];
    }
    
    imageScrollView = [[UIScrollView alloc] initWithFrame:holderImage.frame];
    imageScrollView.delegate = self;
    [imageScrollView setBackgroundColor:[UIColor clearColor]];
	imageScrollView.pagingEnabled = YES;
    UIView *viewFrame=nil;
	for (int i = 0; i <numberOfViews; i++) {
        
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        viewFrame = [[UIView alloc] initWithFrame:CGRectMake(imageScrollView.frame.size.width * i, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=categoryItem.imageURL;
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame = [self addItemDetailView:viewFrame:YES];
        
        viewFrame.backgroundColor = [UIColor clearColor];
		[imageScrollView addSubview:viewFrame];
    }
    
    [imageScrollView setBounces:NO];
    imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width * numberOfViews, imageScrollView.frame.size.height);
    [self.view addSubview:imageScrollView];
    
    /*
     pageControl = [[PageControl alloc] init ];
     pageControl.frame = CGRectMake(0.0,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
     pageControl.backgroundColor = [UIColor clearColor];
     pageControl.delegate = self;
     pageControl.numberOfPages = numberOfViews;
     //pageControl.currentPage = myViewTag;
     [self.view addSubview:pageControl];
     */
    
}


- (void)createSideLineView {
    
    float noPages = (float)[sideLineArray count]/2;
    numberOfViews=[sideLineArray count]/2;
    
    if(noPages > numberOfViews){
        
        numberOfViews = numberOfViews +1 ;
    }
    
    imageScrollView = [[UIScrollView alloc] initWithFrame:holderImage.frame];
    imageScrollView.frame=CGRectMake(30,holderImage.frame.origin.y-10, 270, holderImage.frame.size.height);
    if([Functions isIPad]){
        imageScrollView.frame=CGRectMake(45,itemDetailTwoView.frame.origin.y, 480, 200);
    }
    imageScrollView.delegate = self;
    [imageScrollView setBackgroundColor:[UIColor clearColor]];
	imageScrollView.pagingEnabled = YES;
    
    
	for (int i = 0; i <[sideLineArray count]; i++) {
        
        CategoryItem *categoryItem1=[sideLineArray objectAtIndex:i];
        
        [self addItemChildSideline:categoryItem1 :i:0];
        
        
    }
    
    [imageScrollView setBounces:NO];
    imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width * numberOfViews, imageScrollView.frame.size.height);
    [itemDetailTwoView addSubview:imageScrollView];
    
    pageControlSideLine = [[PageControl alloc] init ];
    pageControlSideLine.frame = CGRectMake(0.0,itemDetailTwoView.frame.size.height-30,320.0,15.0);
    
    if([Functions isIPad]){
        pageControlSideLine.frame = CGRectMake(0.0,itemDetailTwoView.frame.size.height,500,200.0);
    }
    
    pageControlSideLine.backgroundColor = [UIColor clearColor];
    pageControlSideLine.delegate = self;
    pageControlSideLine.numberOfPages = numberOfViews;
    pageControlSideLine.currentPage = 0;
    btnPrevious.enabled=NO;
    
    if( [sideLineArray count]>0){
        
        [btnSidelines setEnabled:YES];
        btnNext.alpha =1;
        btnPrevious.alpha=1;
        // [itemDetailTwoView addSubview:pageControlSideLine];
        [lblNotAvailable setHidden:YES];
        
    }else{
        
        btnNext.alpha = 0;
        btnPrevious.alpha= 0;
        
        //[btnSidelines setEnabled:NO];
    }
}


-(void)addItemChildSideline:(CategoryItem*)categoryItem : (int)i :(int)index{
    
    AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,130,95)];
    UIView* viewFrame = [[UIView alloc] initWithFrame:CGRectMake(135 * i,30, 118,95)];
    if([Functions isIPad]){
        asynImageView.frame=CGRectMake(0,0,225,150);
        viewFrame.frame=CGRectMake(240 * i,30, 250,95);
    }
    
    
    asynImageView.backgroundColor = [UIColor clearColor];
    asynImageView.contentMode = UIViewContentModeScaleAspectFit;
    NSString *stImageURL=categoryItem.imageURL;
    
    if( ![stImageURL isEqualToString:@""] )
    {
        NSURL *imageURL = [NSURL URLWithString:stImageURL];
        [asynImageView loadImageFromURL:imageURL];
        
    }
    else
    {
        //TODO: show default image.
    }
    
    [viewFrame addSubview:asynImageView];
    viewFrame.backgroundColor = [UIColor clearColor];
    
    // add label
    CGSize txtSz;
    UILabel *scoreLabel = [ [UILabel alloc ] init];
    scoreLabel.textAlignment =  UITextAlignmentLeft;
    scoreLabel.textColor = [UIColor darkGrayColor];
    scoreLabel.numberOfLines=0;
    //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
    scoreLabel.backgroundColor = [UIColor whiteColor];
    scoreLabel.alpha = 0.8;
    [scoreLabel setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    scoreLabel.text = [NSString stringWithFormat:@"  %@  ",langIsArabic?category.name_arabic:category.name];
    txtSz= [scoreLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
    
    [scoreLabel setFrame:CGRectMake(0, CGRectGetMaxY(viewFrame.frame)-txtSz.height-30, txtSz.width, txtSz.height)];
    
    if([Functions isIPad]){
        
        [scoreLabel setFrame:CGRectMake(0,asynImageView.frame.size.height-20, txtSz.width, txtSz.height)];
        
    }
    
    [viewFrame addSubview:scoreLabel];
    [viewFrame bringSubviewToFront:scoreLabel];
    scoreLabel = nil;
    
    
    UIButton *addOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addOrderBtn setUserInteractionEnabled:YES];
    [addOrderBtn addTarget:self action:@selector(actionOrderAdd:) forControlEvents:UIControlEventTouchUpInside];
    [addOrderBtn setImage:[UIImage imageNamed:@"btn_add_normal.png"] forState:UIControlStateNormal];
    [addOrderBtn setImage:[UIImage imageNamed:@"btn_add_pressed.png"] forState:UIControlStateSelected];
    addOrderBtn.alpha=0.9;
    [addOrderBtn setShowsTouchWhenHighlighted:YES];
    //[addOrderBtn setReversesTitleShadowWhenHighlighted:YES];
    [addOrderBtn setTag:[categoryItem.backendItemID intValue]];
    [addOrderBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [addOrderBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    //  [addOrderBtn setFrame:CGRectMake(135 * i, 0, 30,30)];
    [addOrderBtn setFrame:CGRectMake(viewFrame.frame.size.width-18,0, 30, 30)];
    
    if([Functions isIPad]){
        
        [addOrderBtn setFrame:CGRectMake(viewFrame.frame.size.width-54, 0, 30, 30)];
        
        
    }
    
    [viewFrame addSubview:addOrderBtn];
    [viewFrame bringSubviewToFront:addOrderBtn];
    
    addOrderBtn=nil;
    
    //addOrderBtn=nil;
    [imageScrollView addSubview:viewFrame];
    
}

-(void)addDetailAndSideLine{
    
    CategoryItem *categoryItem;
    
    if(currentPageIndex == -1){
        categoryItem = currentCategoryItem;
    }else{
        categoryItem=[detailArray objectAtIndex:currentPageIndex];
    }
    
    
    txtDetailView.text = langIsArabic?categoryItem.desc_arabic:categoryItem.desc;
    
    [txtDetailView setFont:[UIFont fontWithName:@"Segoe UI" size:14]];
    txtDetailView.textColor = UIColorFromRGB(0x939393);
    
}

-(UIView*)addItemDetailView:(UIView*)view : (BOOL)isCategoryName{
    
    CategoryItem *categoryItem;
    
    if(currentPageIndex == -1){
        categoryItem = currentCategoryItem;
    }else{
        categoryItem=[detailArray objectAtIndex:currentPageIndex];
    }
    
    
    self.title = langIsArabic?categoryItem.name_arabic:categoryItem.name;
    UIImage *imageName=[categoryItem thumbImage];
    holderImage.image = imageName;
    NSString *fileName = [Functions localImageThumbName:categoryItem.imageURL backendID:[categoryItem.backendID intValue]];
    NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
        
        [categoryItem startDownloadThumbImage:@""];
    }
    
    if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
        
        UIImage *imageName=[categoryItem thumbImage];
        
        holderImage.image = imageName;
        
        //NSLog(@"image  %@",[tempFrame subviews]);
    }else{
        
        AsyncImageView* asynImageView=[[AsyncImageView alloc]init];
        asynImageView.frame= CGRectMake(0,0, holderImage.frame.size.width, holderImage.frame.size.height);
        
        NSString *stImageURL=categoryItem.imageURL;
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            [holderImage addSubview:asynImageView];
            
        }
        else
        {
            //TODO: show default image.
        }
    }
    
    
    CGSize txtSz;
    UILabel *scoreLabel = [ [UILabel alloc ] init];
    scoreLabel.textAlignment =  UITextAlignmentLeft;
    scoreLabel.textColor = [UIColor darkGrayColor];
    scoreLabel.numberOfLines=0;
    //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
    scoreLabel.backgroundColor = [UIColor whiteColor];
    scoreLabel.alpha = 0.8;
    [scoreLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
    scoreLabel.text = [NSString stringWithFormat:@"  %@  ",langIsArabic?categoryItem.name_arabic:categoryItem.name] ;//[NSString stringWithFormat: @"%d", score];
    txtSz= [scoreLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 18]];
    [scoreLabel setFrame:CGRectMake(2, CGRectGetMaxY(view.frame)-50, txtSz.width, txtSz.height)];
    if([Functions isIPad]){
        [scoreLabel setFrame:CGRectMake(10, holderImage.frame.size.height+15, txtSz.width, txtSz.height)];
        
    }
    [view addSubview:scoreLabel];
    [view bringSubviewToFront:scoreLabel];
    
    scoreLabel = nil;
    
    
    if(!isCategoryName){
        UILabel *cateLabel = [ [UILabel alloc ] init];
        cateLabel.textAlignment =  UITextAlignmentRight;
        cateLabel.textColor = [UIColor darkGrayColor];
        cateLabel.numberOfLines=0;
        //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
        cateLabel.backgroundColor = [UIColor whiteColor];
        cateLabel.alpha = 0.8;
        [cateLabel setFont:[UIFont fontWithName:@"Segoe UI" size:16]];
        cateLabel.text = [NSString stringWithFormat:@"  %@  ",langIsArabic?categoryItem.name_arabic:categoryItem.name] ;//[NSString
        txtSz= [cateLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
        [cateLabel setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [cateLabel setFrame:CGRectMake(view.frame.size.width-txtSz.width, 0, txtSz.width, 30)];
        if([Functions isIPad]){
            
            [cateLabel setFrame:CGRectMake(holderImage.frame.size.width-txtSz.width+20, 40, txtSz.width, 30)];
            
        }
        
        
        [view addSubview:cateLabel];
        [view bringSubviewToFront:cateLabel];
        cateLabel = nil;
    }
    
    UILabel *priceLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame)-85, view.frame.size.width/2.5, 30)];
    priceLabel.textAlignment =  UITextAlignmentLeft;
    priceLabel.textColor = UIColorFromRGB(0x5b1900);
    priceLabel.alpha = 0.8;
    priceLabel.backgroundColor = [UIColor whiteColor];
    [priceLabel setFont:[UIFont fontWithName:@"Segoe UI" size:16]];
    
    priceLabel.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@" %d %@",[categoryItem.price intValue],[[NSUserDefaults standardUserDefaults] objectForKey:keyCurrencySymbol]]];
    
    txtSz= [priceLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 16]];
    [priceLabel setFrame:CGRectMake(0, CGRectGetMaxY(view.frame)-80, txtSz.width, txtSz.height+7)];
    if([Functions isIPad]){
        [priceLabel setFrame:CGRectMake(10, holderImage.frame.size.height-15, txtSz.width, txtSz.height+7)];
        
    }
    priceLabel.numberOfLines=0;
    [view addSubview:priceLabel];
    [view bringSubviewToFront:priceLabel];
    
    if(!isCategoryName){
        self.orderLabel = [ [UILabel alloc ] init];
        self.orderLabel.textAlignment =  UITextAlignmentCenter;
        self.orderLabel.textColor = [UIColor darkGrayColor];
        self.orderLabel.numberOfLines=0;
        //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
        self.orderLabel.backgroundColor = [UIColor whiteColor];
        self.orderLabel.alpha = 0.8;
        self.orderLabel.text = [NSString stringWithFormat:@"%d",[self getTotalQuantity]] ;
        txtSz= [self.orderLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
        [self.orderLabel setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [self.orderLabel setFrame:CGRectMake(holderImage.frame.size.width-25, holderImage.frame.size.height-25, 30, 30)];
        
        if([Functions isIPad]){
            [self.orderLabel setFrame:CGRectMake(holderImage.frame.size.width-16, holderImage.frame.size.height+10, 30, 30)];
        }
        
        [view addSubview:self.orderLabel];
        [view bringSubviewToFront:self.orderLabel];
    }
    return view;
    
    [self.view addSubview:view];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    @try {
        [self callingMenu];
        [self setOrderQuantity];
    }@catch (NSException *exception) {
        NSLog(@"Exception");
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
	
}

- (BOOL)shouldAutorotate{
    
    return FALSE;
    
}



-(IBAction)goHome:(id)sender{
    
    
    NSArray *viewControllers = [self.navigationController viewControllers];
    
    for(UIViewController *viewController in viewControllers) {
        if([viewController isKindOfClass:[RootViewController class]]) {
            
            /*   CATransition *animation=[CATransition animation];
             [animation setDelegate:self];
             [animation setDuration:0.75];
             [animation setType:@"genieEffect"];
             [animation setFillMode:kCAFillModeRemoved];
             
             animation.endProgress=0.99;
             [animation setRemovedOnCompletion:YES];
             [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
            [self.navigationController popToViewController:viewController animated:YES];
            break;
        }
    }
    
    
}

-(IBAction)actionOrderlist:(id)sender{
    @try {
        OrderListController *controller=[[OrderListController alloc] initWithNibName:@"OrderListController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"OrderList");
    }
    
    
}

#pragma mark-
#pragma mark-Setting Order Quantity
-(void)setOrderQuantity{
    @try {
        Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
        int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
        [btnOrderQty setTitle: [Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",qtyTotal]] forState:UIControlStateNormal];
        
        self.orderLabel.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",qtyTotal]];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}

-(int)getTotalQuantity{
    
    Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
    int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
    return qtyTotal;
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}

#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pageControlPageDidChange:(PageControl *)pageControler{
    
    int page = pageControler.currentPage;
    
    CGRect frame = imageScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [imageScrollView scrollRectToVisible:frame animated:YES];
}

- (void) scrollViewDidScroll: (UIScrollView *) aScrollView
{
	
    CGPoint offset = aScrollView.contentOffset;
	pageControlSideLine.currentPage = offset.x / imageScrollView.frame.size.width;
    
    int page = pageControlSideLine.currentPage;
    
    if(page==0){
        
        btnPrevious.enabled = NO;
        btnNext.enabled = YES;
        
    }else if(page == numberOfViews-1){
        
        btnPrevious.enabled = YES;
        btnNext.enabled = NO;
        
    }else{
        
        btnNext.enabled = YES;
        btnNext.enabled = YES;
    }
    
}



-(IBAction)detailOptionClick:(id)sender{
    if([sender tag]==0){
        [itemDetailOneView setHidden:NO];
        [itemDetailTwoView setHidden:YES];
        btnDetails.selected=YES;
        btnSidelines.selected=NO;
        
    }else if([sender tag]==1){
        [itemDetailOneView setHidden:YES];
        [itemDetailTwoView setHidden:NO];
        btnDetails.selected=NO;
        btnSidelines.selected=YES;
    }
}

-(IBAction)old_actionOrderAdd:(id)sender{
    @try {
        
        UIButton* btn=(UIButton*)sender;
        
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:btn.tag];
        
        if (orderItem!=nil) {
            int qty=[orderItem.quantity intValue]+1;
            orderItem.quantity=[NSNumber numberWithInt:qty];
            orderItem.entryDateTime=[NSDate date];
            [[DataStoreManager manager] saveData];
        }else{
            Order *newOrder=[[DataStoreManager manager] createOrder];
            newOrder.entryDateTime=[NSDate date];
            newOrder.itemBackendID=[NSNumber numberWithInt:btn.tag];
            newOrder.quantity=[NSNumber numberWithInt:1];
            newOrder.comment=@"Click Here For Comments.";
            [[DataStoreManager manager] saveData];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrdersItemAdded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus,nil]];
        [self setOrderQuantity];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
}

-(IBAction)actionOrderAdd:(id)sender{
    @try {
        
        btnID=(UIButton*)sender;
        
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:btnID.tag];
        
        backEndID=orderItem.itemBackendID;
        
        CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:btnID.tag];
        
        pricePer=[categoryItem.price intValue];
        
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:kaddOrderPopup] integerValue]==1){
            [self AddAlertView];
            
        }else{
            if(![self.view.subviews containsObject:bgView]){
                [self createSeats:[[SingletonClass sharedInstance] numberOfPeople]];
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
}

-(IBAction)NextPreviousAction:(id)sender{
    
    int page = pageControlSideLine.currentPage;//selectedIndex++;
    
    if([sender tag]==0 && pageControlSideLine.currentPage<numberOfViews && pageControlSideLine.currentPage>0){
        
        CGRect frame = imageScrollView.frame;
        page = page-1;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        [imageScrollView scrollRectToVisible:frame animated:YES];
        
    }else if([sender tag]==1 && pageControlSideLine.currentPage<numberOfViews-1 ){
        
        CGRect frame = imageScrollView.frame;
        page = page + 1;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        [imageScrollView scrollRectToVisible:frame animated:YES];
        
    }
    
    if(page==0){
        
        btnPrevious.enabled = NO;
        btnNext.enabled = YES;
        
    }else if(page == numberOfViews-1){
        
        btnPrevious.enabled = YES;
        btnNext.enabled = NO;
        
    }else{
        
        btnNext.enabled = YES;
        btnNext.enabled = YES;
    }
}
#pragma mark Creat Order View
-(void) createSeats:(int) noOfseats{
    
    multipalOrderValue=[NSString stringWithFormat:@"0"];
    singleOrderValue=[NSString stringWithFormat:@"0"];
    CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:btnID.tag];
    
    [dicSeatsQuantities removeAllObjects];
    [dicSeatsQuantities setValue:@"0" forKey:@"1"];
    isForEveryOne=TRUE;
    UIView* bgFrame=[[UIView alloc]initWithFrame:CGRectMake(90, 350, 395,317 )];//395 × 317 pixels
    UIImageView* imageBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 395, 317)];
    [imageBg setImage:[UIImage imageNamed:@"add_order_popup_bg.png"]];
    
    int tempHeight=0;
    float screenHeight= [[UIScreen mainScreen] bounds].size.height;
    float screenWidth= [[UIScreen mainScreen] bounds].size.width;
    
    if([Functions isIPad]){
        bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 552, screenWidth)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        bgFrame.frame=CGRectMake(130, screenWidth-410, 276,344 );
        imageBg.frame=CGRectMake(10, 0, 276,344 );
        [imageBg setImage:[UIImage imageNamed:@"iphone_add_order_popup_bg.png"]];
        deviceName=@"iphone_";
        tempHeight=45;
    }else{
        
        
        bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,320,screenHeight)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        bgFrame.frame=CGRectMake(10, screenHeight-410, 276,344 );
        imageBg.frame=CGRectMake(10, 0, 276,344 );
        [imageBg setImage:[UIImage imageNamed:@"iphone_add_order_popup_bg.png"]];
        deviceName=@"iphone_";
        tempHeight=45;
    }
    
    UIImageView* lightGrayImageView=[[UIImageView alloc]
                                     init];
    [lightGrayImageView setBackgroundColor:[UIColor blackColor]];
    [lightGrayImageView setAlpha:0.8];
    lightGrayImageView.frame=bgView.frame;
    [bgView addSubview:lightGrayImageView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(RemoveView:)
     forControlEvents:UIControlEventTouchUpInside];
    if([lang isEqualToString:@"ar"]){
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_normal_ar.png"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed_ar.png"] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed_ar.png"] forState:UIControlStateHighlighted];
    }else{
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_normal@2x.png"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed.png"] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed.png"] forState:UIControlStateHighlighted];
        
    }
    button.frame = CGRectMake(bgFrame.frame.origin.x+20,bgFrame.frame.origin.y+13, 66, 25);
    
    btnSwitch = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSwitch addTarget:self
                  action:@selector(onSwitch:)
        forControlEvents:UIControlEventTouchUpInside];
    [btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_off.png"] forState:UIControlStateNormal];
    [btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_on.png"] forState:UIControlStateSelected];
    //[btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_off.png"] forState:UIControlStateHighlighted];
    btnSwitch.frame = CGRectMake(bgFrame.frame.origin.x+bgFrame.frame.size.width-60,bgFrame.frame.origin.y+10, 55, 29.0);
    
    if(noOfseats==1) {
        btnSwitch.selected=TRUE;
        isForEveryOne=FALSE;
    }
    UILabel* lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+button.frame.size.width+20, bgFrame.frame.origin.y+15, bgFrame.frame.size.width-140, 20.0)];
    [lblTitle setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
    lblTitle.textColor = UIColorFromRGB(0x5b1900);
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    lblTitle.text=NSLocalizedString(@"Place to Order",@"Place to Order");
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    
    UILabel* lblPrice=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+20,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 100.0, 12.0)];
    [lblPrice setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblPrice.textColor = [UIColor grayColor];
    [lblPrice setBackgroundColor:[UIColor clearColor]];
    lblPrice.text=NSLocalizedString( @"Price per Serving", @"Price per Serving");
    UILabel* lblTotal=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+ bgFrame.frame.size.width-80,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 100.0, 12.0)];
    [lblTotal setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblTotal.textColor = [UIColor grayColor];
    [lblTotal setBackgroundColor:[UIColor clearColor]];
    lblTotal.text=NSLocalizedString(@"Total", @"Total");
    
    lblTotalPerItem=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+ bgFrame.frame.size.width-50,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 560.0, 12.0)];
    [lblTotalPerItem setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblTotalPerItem.textColor = [UIColor grayColor];
    [lblTotalPerItem setBackgroundColor:[UIColor clearColor]];
    lblTotalPerItem.text=[Functions getLocalizedNumber:@"0x0"];
    
    lblPriceValue=[[UILabel alloc]initWithFrame:CGRectMake(lblPrice.frame.origin.x,bgFrame.frame.origin.y+ bgFrame.frame.size.height-30, 100.0, 20.0)];
    [lblPriceValue setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    lblPriceValue.textColor = [UIColor brownColor];
    [lblPriceValue setBackgroundColor:[UIColor clearColor]];
    lblPriceValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",pricePer]];
    
    lblTotalValue=[[UILabel alloc]initWithFrame:CGRectMake(lblTotal.frame.origin.x,bgFrame.frame.origin.y+ bgFrame.frame.size.height-30, 100.0, 20.0)];
    [lblTotalValue setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    lblTotalValue.textColor = [UIColor brownColor];
    [lblTotalValue setBackgroundColor:[UIColor clearColor]];
    lblTotalValue.text=[Functions getLocalizedNumber: @"0 SAR"];
    //[lblTotalValue sizeToFit];
    
    btnAddOrder = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnAddOrder = [[UIButton alloc] initWithFrame:CGRectMake(bgFrame.frame.origin.x+25, bgFrame.frame.origin.y+250, 242, 40)];
    [btnAddOrder setTitleColor:UIColorFromRGB(0x5b1900) forState:UIControlStateNormal];
    NSString* btnTitletxt=NSLocalizedString(@"Add to Order",@"Add to Order");
    [btnAddOrder setTitle:btnTitletxt forState:UIControlStateNormal];
    
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_normal",deviceName]] forState:UIControlStateNormal];
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_pressed.png",deviceName]] forState:UIControlStateSelected];
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_normal",deviceName]] forState:UIControlStateHighlighted];
    [btnAddOrder addTarget:self
                    action:@selector(DoneOrder:)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView* lblImage=[[UIImageView alloc]initWithFrame:CGRectMake(40,10,17.0,17.0)];
    [lblImage setImage:[UIImage imageNamed:@"icon_grill.png"]];
    
    [btnAddOrder addSubview:lblImage];
    
    [bgFrame addSubview:imageBg];
    [bgView addSubview:bgFrame];
    [bgView addSubview:btnAddOrder];
    [bgView addSubview:button];
    [bgView addSubview:btnSwitch];
    
    if([categoryItem.isSideLine intValue] == 0){
        
        [bgView addSubview:lblTitle];
        [bgView addSubview:lblPrice];
        [bgView addSubview:lblTotal];
        [bgView addSubview:lblTotalPerItem];
        [bgView addSubview:lblPriceValue];
        [bgView addSubview:lblTotalValue];
        
    }else{
        
        lblTotalValue.text = @"It is SideLine Item";
        lblTotalValue.frame = CGRectMake(0, lblTotalValue.frame.origin.y-10, 250, 44);
        lblTotalValue.textAlignment = UITextAlignmentCenter;
    }
    
    
    int yAxis=15;
    int startingHeight=55;
    for(int i=1; i<=noOfseats; i++){
        
        UIImageView* tempView;
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(25, startingHeight, 241, 47)];
        view1.tag=i;
        view1.backgroundColor = [UIColor clearColor];
        tempView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, view1.frame.size.width, 47)];
        [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_top.png",deviceName]]];
        tempView.tag=4000+i;
        [view1 addSubview:tempView];
        
        UILabel* lblSeatTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, yAxis, 100, 20)];
        [lblSeatTitle setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [lblSeatTitle setBackgroundColor:[UIColor clearColor]];
        [lblSeatTitle setTextColor:[UIColor lightGrayColor]];
        [lblSeatTitle setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Seat", @"Seat"),[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",i]]]];
        lblSeatTitle.tag=5000+i;
        if(noOfseats==1){
            lblSeatTitle.text=NSLocalizedString(@"Order Quantity",@"Order Quantity");
        }
        [view1 addSubview:lblSeatTitle];
        
        
        UIButton* btnSubtract = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnSubtract = [[UIButton alloc] initWithFrame:CGRectMake(view1.frame.origin.x+130, yAxis-5, 25, 30)];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_normal.png",deviceName]] forState:UIControlStateNormal];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_pressed.png",deviceName]] forState:UIControlStateSelected];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_normal.png",deviceName]] forState:UIControlStateHighlighted];
        btnSubtract.tag=1000+i;
        [btnSubtract addTarget:self
                        action:@selector(OrderBtnOnclick:)
              forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnSubtract];
        
        UIImageView* lblImage=[[UIImageView alloc]initWithFrame:CGRectMake(btnSubtract.frame.origin.x+btnSubtract.frame.size.width,yAxis-4, 33.0, 30.0)];
        [lblImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@quantity_text-field.png",deviceName]]];
        [view1 addSubview:lblImage];
        
        UILabel* lblScrolValue=[[UILabel alloc]initWithFrame:CGRectMake(btnSubtract.frame.origin.x+btnSubtract.frame.size.width+6,yAxis-3, 30.0, 20.0)];
        [lblScrolValue setFont:[UIFont fontWithName:@"Segoe UI" size:20]];
        lblScrolValue.textColor = [UIColor lightGrayColor];
        [lblScrolValue setBackgroundColor:[UIColor clearColor]];
        [lblSeatTitle setTextAlignment:UITextAlignmentCenter];
        lblScrolValue.tag=3000+i;
        lblScrolValue.text=[Functions getLocalizedNumber:@"0"];
        [view1 addSubview:lblScrolValue];
        
        UIButton* btnPlus = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnPlus = [[UIButton alloc] initWithFrame:CGRectMake(lblImage.frame.origin.x+lblImage.frame.size.width, yAxis-5, 25, 30)];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_normal.png",deviceName]] forState:UIControlStateNormal];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_pressed.png",deviceName]] forState:UIControlStateSelected];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_normal.png",deviceName]] forState:UIControlStateHighlighted];
        btnPlus.tag=2000+i;
        [btnPlus addTarget:self
                    action:@selector(OrderBtnOnclick:)
          forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnPlus];
        
        if(noOfseats==1 && i==noOfseats){
            [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_single",deviceName]]];
        }else
            if(( noOfseats==2 || noOfseats==3 || noOfseats==4) && i==noOfseats){
                
                [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_bot",deviceName]]];
                
            }else if(i!=1){
                [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_mid.png",deviceName]]];
            }
        startingHeight=startingHeight+view1.frame.size.height;
        [bgFrame addSubview:view1];
    }
    
    [self.view addSubview:bgView];
    
}

-(void)DoneOrder:(id)sender{
    @try {
        int seatNo;
        
        UIButton* btn=(UIButton*)sender;
        
        btn.selected=TRUE;
        if(isForEveryOne==TRUE){
            for(NSString* key in [dicSeatsQuantities allKeys]){
                seatNo=[key intValue];
                int orderQty=[[dicSeatsQuantities objectForKey:key] intValue];
                if(orderQty>0){
                    [[DataStoreManager manager] AddOrderFunction:btnID Seat:seatNo Qty:orderQty];
                }
            }
            
        }else {
            seatNo=0;
            int orderQty=[[dicSeatsQuantities objectForKey:@"1"] intValue];
            if(orderQty>0){
                
                [[DataStoreManager manager] AddOrderFunction:btnID Seat:seatNo Qty:orderQty];
            }else{
                NSString* alertTitletxt=NSLocalizedString(@"Warning",@"Warning");
                NSString* alertMsg=NSLocalizedString(@"Quantity wouldn't be Zero",@"Quantity wouldn't be Zero");
                
                [Functions showAlert:alertTitletxt message:alertMsg];
            }
            
        }
        multipalOrderValue=@"0";
        singleOrderValue=@"0";
        [bgView removeFromSuperview];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}

#pragma mark AddOrderNotification

- (void)AddOrderNotification:(NSNotification *)notification{
    if(![self.view.subviews containsObject:bgView]){
        [self createSeats:[[SingletonClass sharedInstance] numberOfPeople]];
    }
    
}
#pragma mark AddAlertView
-(void) AddAlertView{
    AlertViewController *alertVC;
    if([Functions isIPad]){
        alertVC=[[AlertViewController alloc]initWithNibName:@"ipad_AlertViewController" bundle:nil];
        
        
    }else{
        alertVC=[[AlertViewController alloc]initWithNibName:@"AlertViewController" bundle:nil];
    }
    alertVC.notifyString=@"NotifyAnyView";
    [self addChildViewController:alertVC];
    [self.view addSubview:alertVC.view];
}
#pragma mark Switch For Everyone/Single
-(void) onSwitch:(id)sender{
   
    @try {
        btnSwitch=(UIButton*) sender;
        if(btnSwitch.selected==TRUE){
            if([[SingletonClass sharedInstance] numberOfPeople]>1){
                btnSwitch.selected=FALSE;
                isForEveryOne=TRUE;
                for(int i=2; i<=[[SingletonClass sharedInstance] numberOfPeople];i++){
                    UIView* tempView=(UIView*)[self.view viewWithTag:i];
                    [tempView setHidden:NO];
                    
                }
                //set cell image to top
                UIImageView* temp=(UIImageView*) [self.view viewWithTag:4001];
                [temp setImage:[UIImage imageNamed:@"iphone_order_quantity_list_top.png"]];
                UILabel* lblTitle=(UILabel*) [self.view viewWithTag:5001];
                lblTitle.text=NSLocalizedString(@"Seat 1",@"Seat 1");
                
            }
            
            UILabel* templable=(UILabel*)[self.view viewWithTag:3001];
            [templable setText:multipalOrderValue];
            singleOrderValue=[NSString stringWithFormat:@"%@",[dicSeatsQuantities objectForKey:@"1"]];
            [dicSeatsQuantities setValue:multipalOrderValue forKey:@"1"];
            
            [self setTotalValue:[[dicSeatsQuantities allKeys] count]];
            
        }else{
            for(int i=2; i<=[[SingletonClass sharedInstance] numberOfPeople];i++){
                UIView* tempView=(UIView*)[self.view viewWithTag:i];
                [tempView setHidden:YES];
            }
            //set cell image to Single
            UIImageView* temp=(UIImageView*) [self.view viewWithTag:4001];
            [temp setImage:[UIImage imageNamed:@"iphone_order_quantity_list_single.png"]];
            UILabel* lblTitle=(UILabel*) [self.view viewWithTag:5001];
            lblTitle.text=NSLocalizedString(@"Order Quantity",@"Order Quantity");
            
            isForEveryOne=FALSE;
            btnSwitch.selected=TRUE;
            
            UILabel* templable=(UILabel*)[self.view viewWithTag:3001];
            [templable setText:singleOrderValue];
            multipalOrderValue=[NSString stringWithFormat:@"%@",[dicSeatsQuantities objectForKey:@"1"]];
            
            [dicSeatsQuantities setValue:singleOrderValue forKey:@"1"];
            
            noOfQut=0;
            noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:@"1"] intValue];
            lblTotalPerItem.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]];
            lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

#pragma mark OrderButton OnClieck
-(void)OrderBtnOnclick:(id)sender{
    int tagValue=[sender tag];
    
    if(tagValue<2000){
        UILabel* templable=(UILabel*)[self.view viewWithTag:tagValue+2000];
        if(tagValue+2000==3001){
            if(btnSwitch.selected==TRUE ){
                [templable setText:singleOrderValue];
            }else{
                [templable setText:multipalOrderValue];
            }
        }
        int value=[templable.text intValue];
        if(value>0){
            [templable setText:[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",--value]]];
        }
        UIView* tempView=(UIView*)[self.view viewWithTag:tagValue-1000];
        [dicSeatsQuantities setValue:templable.text forKey:[NSString stringWithFormat:@"%d",tempView.tag]];
        if(tagValue+2000==3001){
           
            if(btnSwitch.selected==TRUE ){
                singleOrderValue=templable.text;
            }else{
                multipalOrderValue=templable.text;
            }
        }
    }else if (tagValue>2000){
        UILabel* templable=(UILabel*)[self.view viewWithTag:tagValue+1000];
        if(tagValue+1000==3001){
            if(btnSwitch.selected==TRUE){
                [templable setText:singleOrderValue];
            }else{
                [templable setText:multipalOrderValue];
            }
        }
        int value=[templable.text intValue];
        [templable setText:[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",++value]]];
        UIView* tempView=(UIView*)[self.view viewWithTag:tagValue-2000];
        [dicSeatsQuantities setValue:templable.text forKey:[NSString stringWithFormat:@"%d",tempView.tag]];
        if(tagValue+1000==3001){
            if(btnSwitch.selected==TRUE ){
                singleOrderValue=templable.text;
            }else{
                multipalOrderValue=templable.text;
            }
        }
    }
    if(isForEveryOne==FALSE){
        noOfQut=0;
        noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:@"1"] intValue];
        lblTotalPerItem.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]];
        lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
    }else{
        
        [self setTotalValue:[[dicSeatsQuantities allKeys] count]];
    }
    
}

#pragma mark totle Prices set
-(void) setTotalValue:(int) items{
    
    noOfQut=0;
    int i=1;
    for (NSString *key in [dicSeatsQuantities allKeys])
    {
        noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:key] intValue];
        
        if(i==items) break;
        i++;
    }
    
    lblTotalPerItem.text=[Functions getLocalizedNumber:[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]]];
    
    CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:btnID.tag];
    if([categoryItem.isSideLine intValue] == 0)
        lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
    
}
-(void) RemoveView:(id)sender{
    
    [bgView removeFromSuperview];
}
@end
