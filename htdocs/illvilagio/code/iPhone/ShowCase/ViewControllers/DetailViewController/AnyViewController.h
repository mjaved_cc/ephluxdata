//
// Copyright (c) 2013 Syed Sami Ul AHbab
//


#import <UIKit/UIKit.h>
#import "NoteView.h"
#import "MenuManager.h"
#import "PageControl.h"

@interface AnyViewController : UIViewController<UITextViewDelegate,UIScrollViewDelegate,PageControlDelegate> {
    MenuManager *menuManager;
    PageControl *pageControlSideLine;
    
    IBOutlet    UIView  *itemDetailOneView;
    IBOutlet    UIView  *itemDetailTwoView;
    IBOutlet    UIButton    *btnDetails;
    IBOutlet    UIButton    *btnSidelines;
    IBOutlet    UILabel     *lblNotAvailable;
    IBOutlet    UIButton       *btnNext;
    IBOutlet    UIButton       *btnPrevious;
    IBOutlet    UITextView *txtDetailView;
    IBOutlet    UIScrollView   *imageScrollView;
    IBOutlet    UIButton *btnOrderQty;
    IBOutlet    NoteView *noteView;
    int         numberOfViews;
    int         backEndID;
    int         forEveryOneValue;
    int         pricePer;
    int         noOfQut;
    BOOL        isForEveryOne;

    UIScrollView* scrollView ;
    UIView* bgView;
    NSString* deviceName;
    NSMutableDictionary* dicSeatsQuantities;
    UILabel* lblPriceValue;
    UILabel* lblTotalValue;
    UILabel* lblTotalPerItem;
    UIButton* btnSwitch;
    UIButton* btnID;
    UIButton* btnAddOrder;
    NSArray *sideLineArray;
    NSString* singleOrderValue;
    NSString* multipalOrderValue;
    

}

@property(nonatomic,retain)Category *category;
@property(nonatomic,retain) NSArray *detailArray;
@property(nonatomic,assign) NSInteger currentPageIndex;
@property(nonatomic,assign) CategoryItem *currentCategoryItem;
@property(nonatomic,retain) IBOutlet UIImageView *holderImage;
@property(nonatomic,retain) UILabel *orderLabel;
@property (nonatomic,assign) int selectedIndex;

-(UIView*)addItemDetailView:(UIView*)view : (BOOL)isCategoryName;

-(IBAction)detailOptionClick:(id)sender;
-(IBAction)actionBack:(id)sender;
-(IBAction)goHome:(id)sender;
-(IBAction)actionOrderlist:(id)sender;
-(void)setOrderQuantity;
-(IBAction)NextPreviousAction:(id)sender;

@end
