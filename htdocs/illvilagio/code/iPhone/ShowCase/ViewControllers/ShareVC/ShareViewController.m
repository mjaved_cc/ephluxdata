//
//  ShareViewController.m
//  ShowCase
//
//  Created by salman ahmed on 6/6/13.
//
//

#import "ShareViewController.h"
#import <Twitter/TWTweetComposeViewController.h>
#import <Social/Social.h>
#import "MBProgressHUD.h"
@interface ShareViewController ()

@end

@implementation ShareViewController

@synthesize detailDic;
@synthesize fbGraph,socialWrapper,progressHud;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Share",@"Share");
        [Functions setNavigationBar];
        UIBarButtonItem *backButton = [Functions setBackBarButton:self];
        [self.navigationItem setLeftBarButtonItem:backButton];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postTwitter:) name:kNotificationIsLogin object:nil];
    
    [btnFb setTitle:NSLocalizedString(@"Facebook",@"Facebook") forState:UIControlStateNormal];
    [btnFb setTitle:NSLocalizedString(@"Facebook",@"Facebook") forState:UIControlStateHighlighted];
    [btnFb setTitle:NSLocalizedString(@"Facebook",@"Facebook") forState:UIControlStateSelected];
    
    [btnTwitter setTitle:NSLocalizedString(@"Twitter",@"Twitter") forState:UIControlStateNormal];
    [btnTwitter setTitle:NSLocalizedString(@"Twitter",@"Twitter") forState:UIControlStateHighlighted];
    [btnTwitter setTitle:NSLocalizedString(@"Twitter",@"Twitter") forState:UIControlStateSelected];
    
    txtArea.text=[NSString stringWithFormat:@"Check this out %@, Date: %@",[self.detailDic objectForKey:@"title"],[Functions dateFormate:[self.detailDic objectForKey:@"event_time"]]];
    
    self.progressHud = [[MBProgressHUD alloc] initWithView:self.view] ;
    self.progressHud.labelText =NSLocalizedString(@"Posting data to social media",@"Posting data to social media");
    [self.view addSubview:self.progressHud];
    
    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:kOAuthConsumerKey andSecret:kOAuthConsumerSecret];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    
    detailView.layer.cornerRadius = 5;
    detailView.layer.masksToBounds = YES;
    detailView.layer.borderWidth = 0.5f;
    detailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    [[FHSTwitterEngine sharedEngine]loadAccessToken];
    NSString *username = [[FHSTwitterEngine sharedEngine]loggedInUsername];// self.engine.loggedInUsername;
    if (username.length > 0) {
        NSLog(@"%@",[NSString stringWithFormat:@"Logged in as %@",username]);
        [self LoginCheck];
    } else {
        NSLog(@"%@",@"You are not logged in.");
    }
    
}


#pragma mark Twitter V 1.1
- (void)storeAccessToken:(NSString *)accessToken {
    NSLog(@"%@",accessToken);
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"authData"];
}

- (NSString *)loadAccessToken {
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"authData"];
}

- (IBAction)showLoginWindow:(id)sender {
    [[FHSTwitterEngine sharedEngine] showOAuthLoginControllerFromViewController:self];
    
}
-(void) LoginCheck{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"]);
    BOOL isAlreadyLogin=[[[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"] integerValue];
    
    if(isAlreadyLogin==FALSE){
        //[[FHSTwitterEngine sharedEngine] showOAuthLoginControllerFromViewController:self];
        //[self userInfo:nil];
        
    }else {
        //[self userInfo:nil];
    }
    
    
}
-(void) postTwitter:(id)sender{
    [ self.progressHud show:YES];
    if([[self.detailDic objectForKey:@"images"] count]>0){
        [self PostImageAndText:nil];
    }else{
        [self postTweet:nil];
    }
    
}
- (IBAction)PostImageAndText:(id)sender {
    NSString* tempTxt=txtArea.text;
    @try {
        
        
        dispatch_async(GCDBackgroundThread, ^{
            @autoreleasepool {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                NSLog(@"self.detailDic %@", self.detailDic);
               // NSError *returnCode= [[FHSTwitterEngine sharedEngine] postTweet:txtArea.text withImageData: [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[self.detailDic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"]]]];
                NSError *returnCode= [[FHSTwitterEngine sharedEngine] postTweet:tempTxt withImageData: [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[self.detailDic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"]]]];
               
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                NSString *title = nil;
                NSString *message = nil;
                
                if (returnCode) {
                    title = [NSString stringWithFormat:@"Error %d",returnCode.code];
                    message = returnCode.domain;
                } else {
                    title =NSLocalizedString(@"Success",@"Success");
                    message =NSLocalizedString(@"Your social posts shared successfully.",@"Your social posts shared successfully.");
                    
                }
                
                dispatch_sync(GCDMainThread, ^{
                    @autoreleasepool {
                        [self.progressHud hide:YES];
                        NSString* alertBtnTitle=NSLocalizedString(@"OK!",@"OK!");
                        UIAlertView *av = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:alertBtnTitle otherButtonTitles:nil];
                        [av show];
                    }
                });
            }
            
        });
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)postTweet:(id)sender {
    NSString* tempTxt=txtArea.text;
    @try {
         
        dispatch_async(GCDBackgroundThread, ^{
            @autoreleasepool {
                
                [ self.progressHud show:YES];
                NSLog(@"self.detailDic %@", self.detailDic);
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                //NSError *returnCode = [[FHSTwitterEngine sharedEngine] postTweet:txtArea.text];
                NSError *returnCode = [[FHSTwitterEngine sharedEngine] postTweet:tempTxt];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                NSString *title = nil;
                NSString *message = nil;
                
                if (returnCode) {
                    title = [NSString stringWithFormat:@"Error %d",returnCode.code];
                    message = returnCode.domain;
                } else {
                    title =NSLocalizedString(@"Success",@"Success");
                    message =NSLocalizedString(@"Your social posts shared successfully.",@"Your social posts shared successfully.");
                    
                }
                
                dispatch_sync(GCDMainThread, ^{
                    @autoreleasepool {
                        [self.progressHud hide:YES];
                        NSString* alertBtnTitle=NSLocalizedString(@"OK!",@"OK!");
                        UIAlertView *av = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:alertBtnTitle otherButtonTitles:nil];
                        [av show];
                    }
                });
            }
        });
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark Facebook Login
-(IBAction)facebookConnect
{
    self.socialWrapper = [[SocialNetworkingWrapper alloc]inits];
    self.socialWrapper.delegate = self;
    [ self.socialWrapper IsFacebookInitlizeAndAuthorize];
}

#pragma mark Social Networking Facebook Delegate
- (void)didGetFacebookRequest:(NSMutableDictionary *)getRecords
{
    //NSLog(@"request -> %@", getRecords);
    @try {
        
        
        if([self.socialWrapper.requestName isEqualToString:@"me"]){
            
            [self.progressHud show:YES];
            
            if([[self.detailDic objectForKey:@"images"] count]>0){
                [self.socialWrapper postToFbTitle:@"" Description:txtArea.text ImageURL:[[[self.detailDic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"]];
            }else{
                [self.socialWrapper postToFbTitle:@"" Description:txtArea.text];
            }
        }else if([self.socialWrapper.requestName isEqualToString:@"PostImage"]){
            [self.progressHud hide:YES];
            NSString* aletTitle=NSLocalizedString(@"Success",@"Success");
            NSString* alertMsg=NSLocalizedString(@"Your social posts shared successfully.",@"Your social posts shared successfully.");
            NSString* alertBtnTitle=NSLocalizedString(@"OK!",@"OK!");
            // Show the result in an alert
            [[[UIAlertView alloc] initWithTitle:aletTitle
                                        message:alertMsg
                                       delegate:nil
                              cancelButtonTitle:alertBtnTitle
                              otherButtonTitles:nil]
             show];
            
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

- (void)didFailedFacebookRequest:(NSError *)error
{
    NSLog(@"eror comes -> %@", [error localizedDescription]);
}




#pragma mark Twitter for IOS 6
- (IBAction)postToTwitter:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:[NSString stringWithFormat:@"Check this out %@, Date:%@",[self.detailDic objectForKey:@"title"],[self.detailDic objectForKey:@"event_time"]]];
        //[tweetSheet addImage:[UIImage imageNamed:@"Default-Portrait.png"]];
        [tweetSheet addURL:[NSURL URLWithString:[[self.detailDic objectForKey:@"image"] objectForKey:@"image_url"]]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
        [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output = @"";
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Post Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    twitterShared = YES;
                    output = @"Post Successfull";
                    break;
                }
                default:
                    break;
            }
            [self dismissViewControllerAnimated:YES completion:^{
                
                [self resetSocialVariables];
                
            }];
            
            
        }];
        
    }
}
- (IBAction)postToFacebook:(id)sender {
    
    [self fbGraphShare];
}
-(void)resetSocialVariables {
    
    if ( twitterShared ) {
        NSString* aletTitle=NSLocalizedString(@"Success",@"Success");
        NSString* alertMsg=NSLocalizedString(@"Your social posts shared successfully.",@"Your social posts shared successfully.");
        NSString* alertBtnTitle=NSLocalizedString(@"OK!",@"OK!");
        // Show the result in an alert
        [[[UIAlertView alloc] initWithTitle:aletTitle
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:alertBtnTitle
                          otherButtonTitles:nil]
         show];
    }
    
    
    twitterShared = NO;
    
}



//Facebook Pic Sharing
-(void)fbGraphShare
{
	/*Facebook Application ID*/
	NSString *client_id = @"528899480458572";//@"130902823636657";
	
	//alloc and initalize our FbGraph instance
	self.fbGraph = [[FbGraph alloc] initWithFbClientID:client_id];
	
	//begin the authentication process.....
	[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback)
						 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
	
}

//fb//
#pragma mark -
#pragma mark FbGraph Callback Function
/**
 * This function is called by FbGraph after it's finished the authentication process
 **/
- (void)fbGraphCallback {
	
	if ( (fbGraph.accessToken == nil) || ([fbGraph.accessToken length] == 0) ) {
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[fbGraph authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback)
							 andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		
	} else {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Wait..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        [alert show];
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        indicator.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height - 50);
        [indicator startAnimating];
        [alert addSubview:indicator];
        
		
		NSLog(@"------------>CONGRATULATIONS<------------, You're logged into Facebook...  Your oAuth token is:  %@", fbGraph.accessToken);
        
        [fbGraph removeButton];
        timer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                 target:self
                                               selector:@selector(targetMethod)
                                               userInfo:nil
                                                repeats:NO];
        
        
        
	}
	
}
-(void)targetMethod
{
    [timer invalidate];
    [self sndPicPost];
    [self postSuccessfully];
    
}
-(void)sndPicPost
{
    
	NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:2];
	
	//create a UIImage (you could use the picture album or camera too)
    
    UIImage *picture  = [UIImage imageNamed:@"Default-Portrait.png"];;
    
	//create a FbGraphFile object insance and set the picture we wish to publish on it
	FbGraphFile *graph_file = [[FbGraphFile alloc] initWithImage:picture];
	
	//finally, set the FbGraphFileobject onto our variables dictionary....
	[variables setObject:graph_file forKey:@"file"];
	
	[variables setObject:@"testing" forKey:@"caption"];
	
	//the fbGraph object is smart enough to recognize the binary image data inside the FbGraphFile
	//object and treat that is such.....
	FbGraphResponse *fb_graph_response = [fbGraph doGraphPost:@"me/photos" withPostVars:variables];
	NSLog(@"postPictureButtonPressed:  %@", fb_graph_response.htmlResponse);
	
	NSLog(@"Now log into Facebook and look at your profile & photo albums...");
	
}

-(void)postSuccessfully
{
	UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"FB Posting" message:@"Your Pic has been Succesfully Posted"
                                                    delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
	[alert1 show];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    
}
//Facebook Pic Sharing
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        
        
    }
    
    return YES;
}

#pragma mark - Twitter Share/Tweet

-(BOOL)isTwitterAvailable {
    return NSClassFromString(@"TWTweetComposeViewController") != nil;
}

-(BOOL)isSocialAvailable {
    return NSClassFromString(@"SLComposeViewController") != nil;
}
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
