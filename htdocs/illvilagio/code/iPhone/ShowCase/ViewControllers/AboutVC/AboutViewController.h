//
//  AboutViewController.h
//  ShowCase
//
//  Created by salman ahmed on 6/5/13.
//
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController{

    NSMutableArray* contentArray;
    
    IBOutlet    UILabel *lblh1;
    IBOutlet    UILabel *lblh2;
    IBOutlet    UILabel *lblDetail1;
    IBOutlet    UILabel *lblDetail2;
}

@end
