//
//  AboutViewController.m
//  ShowCase
//
//  Created by salman ahmed on 6/5/13.
//
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.title=@"About";
        self.title = NSLocalizedString(@"About",@"About");
        [Functions setNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    contentArray=[[NSMutableArray alloc]init];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contentNotificationFunction:) name:kNotificationContent object:nil];
    //    [[DataStoreManager manager] fetchContent];
    @try {
        //contentArray=[[[SingletonClass sharedInstance] allContentDic] objectForKey:@"content"];
        contentArray=(NSMutableArray*)[Functions getDataFromNSUserDefault_Key:@"content"];
        lblh1.text=[[contentArray objectAtIndex:0] objectForKey:@"title"];
        lblDetail1.text=[[contentArray objectAtIndex:0] objectForKey:@"description"];
        lblh2.text=[[contentArray objectAtIndex:1] objectForKey:@"title"];
        lblDetail2.text=[[contentArray objectAtIndex:1] objectForKey:@"description"];
        lblDetail1.numberOfLines=0;
         lblDetail2.numberOfLines=0;
        [lblDetail1 sizeToFit];
        [lblDetail2 sizeToFit];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    if([Functions isIPad]){
        
    }else{
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];
        
        //lblFeedbackSent.textColor = UIColorFromRGB(0x5b1900);
        [lblDetail1 setFont:[UIFont fontWithName:@"Oxygen-Regular" size:14.0f]];
        [lblDetail2 setFont:[UIFont fontWithName:@"Oxygen-Regular" size:14.0f]];
        [lblh1 setFont:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]];
        [lblh2 setFont:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]];
        if(IS_IPHONE5){
            
        }else{
            
        }
    }
}
-(void) contentNotificationFunction:(NSNotification*)notification{
    @try {
        NSDictionary *dict = notification.userInfo;
        contentArray=[[[dict objectForKey:@"status"] objectForKey:@"body"] objectForKey:@"content"];
        lblh1.text=[[contentArray objectAtIndex:0] objectForKey:@"title"];
        lblDetail1.text=[[contentArray objectAtIndex:0] objectForKey:@"description"];
        lblh2.text=[[contentArray objectAtIndex:1] objectForKey:@"title"];
        lblDetail2.text=[[contentArray objectAtIndex:1] objectForKey:@"description"];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}
#pragma mark - Button Handlers
-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
