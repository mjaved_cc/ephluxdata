//
//  NIDropDown.h
//  NIDropDown
//
//  Copyright (c) 2012 Sami. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OptionsDropDown;
@protocol OptionsDropDownDelegate
- (void) OptionsDropDownDelegateMethod: (OptionsDropDown *) sender  tag:(int)indexPath;
@end 

@interface OptionsDropDown : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) id <OptionsDropDownDelegate> delegate;

-(void)OptionshideDropDown:(UIButton *)b;
- (id)OptionsshowDropDown:(UIButton *)b:(CGFloat *)height:(NSArray *)arr;
@end
