//
//  HomeViewController.h
//  ShowCase
//
//  Created by Syed Sami on 5/21/13.
//
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController{

    UIScrollView *scrollView;
    
}

@property (nonatomic,retain) UIScrollView *scrollView;
@property (nonatomic,retain) UIView *containerView;

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame;
-(id)initWithFrame:(CGRect)frame;
@end
