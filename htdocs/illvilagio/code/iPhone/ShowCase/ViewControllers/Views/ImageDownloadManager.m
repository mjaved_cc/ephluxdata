//
//  ImageDownloadManager.m
//  POS
//
//  Created by admin on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ImageDownloadManager.h"
#import "Defines.h"
#import "Category+Image.h"
#import "CategoryItem+Image.h"

@implementation ImageDownloadManager


static ImageDownloadManager *sharedInstance = nil;


@synthesize operationQueue = _operationQueue;

+(ImageDownloadManager*)sharedImageDownalodManager {
	if (sharedInstance == nil) {
		sharedInstance = [[self alloc] init];
	}
    return sharedInstance;
}

+(id)allocWithZone:(NSZone*)zone {
	if (sharedInstance == nil) {
		sharedInstance = [super allocWithZone:zone];
		return sharedInstance;
	}
    return nil; //on subsequent allocation attempts return nil
}

-(id) copyWithZone:(NSZone*)zone {
    return self;
}


-(id)init {
    
    self = [super init];
	if(!self) return nil;
        
    
	//if(![super init]) return nil;
	queue = [[NSMutableArray alloc] init];
	alreadyStartedProcess = NO;
    
    _operationQueue = [[NSOperationQueue alloc] init];
    _operationQueue.maxConcurrentOperationCount = 1;

	return self;
}


#pragma mark -
#pragma mark Adding Image URLs in the Queue
-(void)addToQueue:(id)object {
	if(!object) return;
	[queue addObject:object];
}

-(void)startDownloads {
	if(thread == nil && alreadyStartedProcess == NO) {
		alreadyStartedProcess = YES;
		thread = [[NSThread alloc] initWithTarget:self selector:@selector(getImagesUsingObjects) object:nil];
		[thread start];
	}
}

-(void)getImagesUsingObjects {
	
    //NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{

        for(int i = 0; i < [queue count]; i++) {
            id object = [queue objectAtIndex:i];
            [object startDownloadThumbImage];
        }
        
    [self performSelectorOnMainThread:@selector(getImagesDidFinish) withObject:nil waitUntilDone:NO];

	
}

-(void)getImagesDidFinish{
	[thread cancel];
	thread = nil;
}

-(void)clearImages{
	[thread cancel];;
	thread = nil;
	[queue removeAllObjects];
}


@end
