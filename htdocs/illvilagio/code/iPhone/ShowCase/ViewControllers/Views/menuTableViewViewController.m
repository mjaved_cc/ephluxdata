//
//  menuTableViewViewController.m
//  ShowCase
//
//  Created by Syed Sami on 5/17/13.
//
//

#import "menuTableViewViewController.h"
#import "Defines.h"
#import "GalleryViewController.h"
#import "CenterViewController.h"
#import "RootViewController.h"
#import "RootViewController_iPhone.h"
#import "ReservationViewController.h"
#import "EventsViewController.h"
#import "FeedBackViewController.h"
#import "MapViewController.h"
#import "AboutViewController.h"


@interface menuTableViewViewController ()

@end

@implementation menuTableViewViewController

@synthesize menuListingArray;
@synthesize btnOrder;
@synthesize tb;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.view.frame = frame;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.menuListingArray = [NSArray arrayWithObjects:@"Home",@"Menu",@"Reservations",@"Events",@"Gallery",@"About",@"Feedback",@"Location",nil];
    
    self.menuListingArray = [NSArray arrayWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"Home",@"name",NSLocalizedString(@"Home",@"Home") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Menu",@"name",NSLocalizedString(@"Menu",@"Menu") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Reservations",@"name",NSLocalizedString(@"Reservations",@"Reservations") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Events",@"name",NSLocalizedString(@"Events",@"Events") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Gallery",@"name",NSLocalizedString(@"Gallery",@"Gallery") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"About",@"name",NSLocalizedString(@"About",@"About") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Feedback",@"name",NSLocalizedString(@"Feedback",@"Feedback") ,@"LocalName", nil],[[NSDictionary alloc] initWithObjectsAndKeys:@"Location",@"name",NSLocalizedString(@"Location",@"Location") ,@"LocalName", nil],nil];
    

    
     //self.menuListingArray = [NSArray arrayWithObjects:NSLocalizedString(@"Home",@"Home"),NSLocalizedString(@"Menu",@"Menu"),NSLocalizedString(@"Reservations",@"Reservations"),NSLocalizedString(@"Events",@"Events"),NSLocalizedString(@"Gallery",@"Gallery"),NSLocalizedString(@"About",@"About"),NSLocalizedString(@"Feedback",@"Feedback"),NSLocalizedString(@"Location",@"Location"),nil];
    
    self.btnOrder.titleLabel.textColor = UIColorFromRGB(0x673102);
    self.btnOrder.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [self.btnOrder.titleLabel setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15]];
    
    btnOrder.titleLabel.textColor = UIColorFromRGB(0x673102);
    [self.btnOrder.titleLabel setText:NSLocalizedString(@"New Order",@"New Order")];
     [self.btnOrder setTitle:NSLocalizedString(@"New Order",@"New Order") forState:UIControlStateNormal];
     [self.btnOrder setTitle:NSLocalizedString(@"New Order",@"New Order") forState:UIControlStateHighlighted];
     [self.btnOrder setTitle:NSLocalizedString(@"New Order",@"New Order") forState:UIControlStateSelected];
    
    self.tb.delegate=self;
    self.tb.dataSource=self;
    // Do any additional setup after loading the view from its nib.
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    return [self.menuListingArray count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45.0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CellMenu";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    
    //NSString *menuString = [self.menuListingArray objectAtIndex:indexPath.row];
    NSDictionary* tempDic = [self.menuListingArray objectAtIndex:indexPath.row];
    
    // BACKGROUND SELECTED VIEW
    UIImageView *avSelected = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 236, 45)];
    avSelected.backgroundColor = [UIColor clearColor];
    avSelected.opaque = NO;
    avSelected.image = [UIImage imageNamed:[NSString stringWithFormat:@"left_nav_list_%@_active.png",[[tempDic objectForKey:@"name"]  lowercaseString]]];
    
    UILabel* lblSelected = [[UILabel alloc] initWithFrame:CGRectMake(54, 0, 150, 45)];
    lblSelected.text=[tempDic objectForKey:@"LocalName"];
    lblSelected.textColor = UIColorFromRGB(0x9e7a48);
    lblSelected.backgroundColor = [UIColor clearColor];
    
    [lblSelected setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    [avSelected addSubview:lblSelected];
    
    cell.selectedBackgroundView = avSelected;
    
    cell.textLabel.text= @"";
    //BACKGROUND VIEW
    UIImageView *av = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 236, 45)];
    av.backgroundColor = [UIColor clearColor];
    av.opaque = NO;
    av.image = [UIImage imageNamed:[NSString stringWithFormat:@"left_nav_list_%@.png",[[tempDic objectForKey:@"name" ]  lowercaseString]]];
    UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(54, 0, 150, 45)];
    lbl.text=[tempDic objectForKey:@"LocalName"];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = UIColorFromRGB(0x939393);
    [lbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    [av addSubview:lbl];
    cell.backgroundView = av;
    
    cell.textLabel.textColor = UIColorFromRGB(0x939393);
    cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    cell.imageView.image = [UIImage imageNamed:@"btn_white_small_normal@2x.png"];
    cell.imageView.alpha = 0;
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([Functions isIPad]){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuClick object:indexPath];
        
    }else{
        if(IS_IPHONE5){
            [self TabOnSelectedIndex:indexPath.row];
        }else{
            [self TabOnSelectedIndex:indexPath.row];
        }
    }
}


-(void) TabOnSelectedIndex:(NSInteger) index{
    if(index==0){
        CenterViewController * homeViewController = [[CenterViewController alloc] init];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:homeViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==1){
        RootViewController_iPhone *rootViewController=[[RootViewController_iPhone alloc] initWithNibName:@"RootViewController_iPhone" bundle:nil];
        rootViewController.isOrderView=FALSE;//true
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:rootViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==2){
        ReservationViewController *reservationViewController=[[ReservationViewController alloc]initWithNibName:@"ReservationViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:reservationViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==3){
        EventsViewController    *eventViewController=[[EventsViewController alloc]initWithNibName:@"EventsViewController" bundle:nil];
        UINavigationController  *nav=[[UINavigationController alloc]initWithRootViewController:eventViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==4){
        GalleryViewController * galleryVC = [[GalleryViewController alloc] initWithNibName:@"GalleryViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:galleryVC];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==5){
        AboutViewController  *aboutVC=[[AboutViewController alloc]initWithNibName:@"AboutViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:aboutVC];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==6){
        FeedBackViewController  *feedbackVC=[[FeedBackViewController alloc]initWithNibName:@"FeedBackViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:feedbackVC];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }else if(index==7){
        MapViewController  *mapView=[[MapViewController alloc]initWithNibName:@"MapViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:mapView];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }
    
}

-(void)viewDidUnload{
    
    self.menuListingArray = nil;
    
}

-(IBAction)addOrder:(id)sender{
    
    
    if([Functions isIPad]){
        
        NSIndexPath *myindexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:YES], @"is_orderView",
                                  nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuClick object:myindexPath userInfo:userInfo];
        
    }else{
        [Functions setOrderType:0];
        RootViewController_iPhone *rootViewController=[[RootViewController_iPhone alloc] initWithNibName:@"RootViewController_iPhone" bundle:nil];
        rootViewController.isOrderView = TRUE;
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:rootViewController];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
    }
}


@end
