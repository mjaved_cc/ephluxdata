//
//  AddOrderIViewClass.m
//   IL VILLAGGIO
//
//  Created by salman ahmed on 6/25/13.
//
//

#import "AddOrderIViewClass.h"
#define kOFFSET_FOR_KEYBOARD 50.0

@implementation AddOrderIViewClass



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor=[UIColor lightGrayColor];
        self.alpha=5.0;
        flag1=TRUE;
        orderView=[[UIView alloc]initWithFrame:CGRectMake(50, 50, 320, 600)];
        [orderView setBackgroundColor:[UIColor yellowColor]];
        
        btnCancle= [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnCancle addTarget:self action:@selector(RemoveView:)
                   forControlEvents:UIControlEventTouchDown];
        [btnCancle setTitle:@"X" forState:UIControlStateNormal];
        btnCancle.frame = CGRectMake(0.0, 10.0, 20.0, 20.0);
        [orderView addSubview:btnCancle];

        
        btnOrderEveryOne= [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnOrderEveryOne addTarget:self action:@selector(onClick:)
           forControlEvents:UIControlEventTouchDown];
        [btnOrderEveryOne setTitle:@"Show" forState:UIControlStateNormal];
        btnOrderEveryOne.frame = CGRectMake(0.0, 50.0, 200.0, 50.0);
        btnOrderEveryOne.tag=0;
        [orderView addSubview:btnOrderEveryOne];
       
        sliderOrderEveryOne=[[UISlider alloc]initWithFrame:CGRectMake(0, 100, 300, 50)];//100
        sliderOrderEveryOne.tag=0;
        [orderView addSubview:sliderOrderEveryOne];
        
        btnSeat1= [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnSeat1 addTarget:self action:@selector(onClick:)
            forControlEvents:UIControlEventTouchDown];
        [btnSeat1 setTitle:@"X" forState:UIControlStateNormal];
        btnSeat1.frame = CGRectMake(0.0, 100.0, 200.0, 50.0);//150
        btnSeat1.tag=1;
        [orderView addSubview:btnSeat1];
        
        slider1=[[UISlider alloc]initWithFrame:CGRectMake(0, 200, 300, 50)];//200
        slider1.tag=1;
        [orderView addSubview:slider1];
        [orderView addSubview:btnSeat1];
        
        btnSeat2= [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnSeat2 addTarget:self action:@selector(onClick:)
           forControlEvents:UIControlEventTouchDown];
        [btnSeat2 setTitle:@"X" forState:UIControlStateNormal];
        btnSeat2.frame = CGRectMake(0.0, 200.0, 200.0, 50.0);//250
        btnSeat2.tag=2;
        [orderView addSubview:btnSeat2];
        
        slider2=[[UISlider alloc]initWithFrame:CGRectMake(0, 300, 300, 50)];//300
        slider2.tag=1;
        [orderView addSubview:slider2];
        [orderView addSubview:btnSeat2];

        
        
        [self addSubview:orderView];
        
        [self hiddenAllSlider];
    }
    return self;
}

-(void) onClick:(id)sender{
    
    if([sender tag]==0){
        if(flag1){
            [self hiddenSlider:0];
            flag1=FALSE;
        [self setViewMovedUp:NO view:btnSeat1];
            [self setViewMovedUp:NO view:btnSeat2];
        }else{
             [self hiddenAllSlider];
            flag1=TRUE;
            [self setViewMovedUp:YES view:btnSeat1];
            [self setViewMovedUp:YES view:btnSeat2];
        }
    }else if([sender tag]==1){
        
    }else if([sender tag]==2){
        
    }else if([sender tag]==3){
        
    }
}
-(void) hiddenAllSlider{
    [sliderOrderEveryOne setHidden:YES];
    [slider1 setHidden:YES];
    [slider2 setHidden:YES];
    [slider3 setHidden:YES];
    [slider4 setHidden:YES];
}
-(void) hiddenSlider:(id)sender{
    [sliderOrderEveryOne setHidden:YES];
    [slider1 setHidden:YES];
    [slider2 setHidden:YES];
    [slider3 setHidden:YES];
    [slider4 setHidden:YES];
    
    if([sender tag]==0){
        [sliderOrderEveryOne setHidden:NO];
    }else if([sender tag]==1){
        [slider1 setHidden:NO];
    }else if([sender tag]==2){
        [slider2 setHidden:NO];
    }
}
-(void) RemoveView:(id)sender{
    [self removeFromSuperview];
}
-(void)setViewMovedUp:(BOOL)movedUp view:(UIButton*)btnView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = btnView.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    btnView.frame = rect;
    
    [UIView commitAnimations];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
