//
//  HomeViewController.m
//  ShowCase
//
//  Created by Syed Sami on 5/21/13.
//
//

#import "HomeViewController.h"
#import "OrderListController.h"
#import "HomeViewDetailController.h"
#import "UIViewController+Container.h"
#import "Defines.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    
    self.view.frame=frame;
    //self.view.backgroundColor = [UIColor blackColor];
    self.title = NSLocalizedString(@"Home", @"Home"); 
    
    return self;
    
}

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.view.frame=frame;
    }
    
    return self;
    
    
}


-(void)showNextView {
    
    HomeViewDetailController *childViewController = [[HomeViewDetailController alloc] initWithFrame:CGRectMake(0, 0, 550, 700)];
    
    //    [self.view addSubview:childViewController.view];
    
    [self.navigationController pushViewController:childViewController animated:YES];
    //    [self addChildViewController:childViewController];
    //
    //    CGRect retainFrame;
    //    CGRect myFrame = childViewController.view.frame;
    //    retainFrame=myFrame;
    //    myFrame.origin.x = myFrame.origin.x + 550;
    //    childViewController.view.frame = myFrame;
    //
    //    [UIView transitionWithView:self.view
    //                      duration:0.4
    //                       options:UIViewAnimationOptionCurveEaseIn
    //                    animations:^{
    //
    //                        [self.view addSubview:childViewController.view];
    //                        childViewController.view.frame = retainFrame;
    //
    //                    }
    //
    //                    completion:nil];
    //
    //
    //    [childViewController didMoveToParentViewController:self];
    //
    //    //[self.navigationController pushViewController:self animated:YES];
    [self performSelector:@selector(removeView:) withObject:childViewController afterDelay:3];
    
}

-(void)removeView:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    //    NSArray *children = [self childViewControllers];
    //    NSLog(@"Before %d",[children count]);
    //
    //    [self containerRemoveChildViewController:[children objectAtIndex:0]];
    //
    //    NSLog(@"After %d",[[self childViewControllers] count]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //    [self.navigationController setNavigationBarHidden:YES];
    
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    
    [self addDealView];
    [self addSpecial];
    [self addUpComingEventView];
    
    [self.view addSubview:scrollView];
}

-(void)addDealView{
    
    
    UIImageView *dealImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, 510, 201)];
    
    dealImageView.image = [UIImage imageNamed:@"img_holder_full-width.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(dealMethod:)
     forControlEvents:UIControlEventTouchDown];
    button.backgroundColor = [UIColor clearColor];
    //[button setTitle:@"Show View" forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 30, 510, 201);
    [scrollView addSubview:button];
    
    
    [scrollView addSubview:dealImageView];
    
    
}

-(void)dealMethod:(id)sender{
    
    NSLog(@"button Deal Method");
    
    [self performSelector:@selector(showNextView) withObject:nil afterDelay:0];
    
    
}


-(void)addUpComingEventView{
    
    
    UIImageView *upcomingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 470, 510, 201)];
    
    upcomingImageView.image = [UIImage imageNamed:@"img_holder_full-width.png"];
    
    [scrollView addSubview:upcomingImageView];
    
    upcomingImageView=nil;
    
}

-(void)addSpecial{
    
    UIImageView *special1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 255, 247, 196)];
    
    special1.image = [UIImage imageNamed:@"img_holder_half-width.png"];
    
    [scrollView addSubview:special1];
    
    special1=nil;
    
    UIImageView *special2 = [[UIImageView alloc] initWithFrame:CGRectMake(260, 255, 247, 196)];
    
    special2.image = [UIImage imageNamed:@"img_holder_half-width.png"];
    
    [scrollView addSubview:special2];
    
    special2=nil;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
