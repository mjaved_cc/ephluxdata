//
// Copyright (c) 2010-2011 René Sprotte, Provideal GmbH
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
#import <QuartzCore/QuartzCore.h>
#import "MMGridViewDefaultCell.h"
#import "Defines.h"

@implementation MMGridViewDefaultCell

@synthesize textLabel;
@synthesize textLabelBackgroundView;
@synthesize backgroundView;
@synthesize imageView;
@synthesize priceLabel;
@synthesize orderBtn;


- (id)initWithFrame:(CGRect)frame 
{
    @try {
        
       if ((self = [super initWithFrame:frame])) {
        // Background view
        self.layer.borderWidth=0.5f;
        self.layer.borderColor=[UIColor clearColor].CGColor;
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectNull];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backgroundView];
        
        // Image view
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectNull];
        [self addSubview:self.imageView];
        
        // Label
        self.textLabelBackgroundView = [[UIView alloc] initWithFrame:CGRectNull];
           self.textLabelBackgroundView.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:255 green:255 blue:255 alpha:0.7];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectNull];
        self.textLabel.textAlignment = UITextAlignmentLeft;
           self.textLabel.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];//[UIColor clearColor];

        self.textLabel.textColor = [UIColor darkGrayColor];//UIColorFromRGB(0x939393);
            //self.textLabel.font=[UIFont boldSystemFontOfSize:14.0f];
          
        [self.textLabel setFont:[UIFont fontWithName:@"Segoe UI" size:16]];

        //[ self.textLabel setFont:[UIFont fontWithName:@"ProximaNova-Bold" size:16]];
        self.textLabel.numberOfLines=0;
            //self.textLabel.lineBreakMode=UILineBreakModeTailTruncation;
        [self.textLabel sizeToFit];
        
        //NSLog(@"%lf",self.frame.size.width);
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectNull];
        self.priceLabel.textAlignment = UITextAlignmentLeft;
        self.priceLabel.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];//[UIColor clearColor];
        
        self.priceLabel.textColor = UIColorFromRGB(0x5b1900);
            //self.textLabel.font=[UIFont boldSystemFontOfSize:14.0f];
        [self.priceLabel setFont:[UIFont fontWithName:@"Segoe UI" size:14]];
        self.priceLabel.numberOfLines=0;
        [self.priceLabel sizeToFit];
            //  self.textLabel.minimumFontSize =18.0f;
        [self.textLabelBackgroundView addSubview:self.priceLabel];
        [self.textLabelBackgroundView addSubview:self.textLabel];
        
        self.orderBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        [self.orderBtn setUserInteractionEnabled:YES];
         //  [self.orderBtn addTarget:self action:@selector(actionOrderAdd:) forControlEvents:UIControlEventTouchUpInside];
        [self.orderBtn setImage:[UIImage imageNamed:@"btn_add_normal.png"] forState:UIControlStateNormal];
        [self.orderBtn setImage:[UIImage imageNamed:@"btn_add_pressed.png"] forState:UIControlStateSelected];
        
        self.orderBtn.alpha=0.9;
       

        [self addSubview:self.orderBtn];
        [self addSubview:self.textLabelBackgroundView];
    }
    
    return self;
    }
    @catch (NSException *exception) {
        NSLog(@"Self Exception");
    }
   
}


- (void)layoutSubviews
{
    @try {
        [super layoutSubviews];
        
        int labelHeight = 50;
        int inset = 0;
        self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, 156, 108);
        self.backgroundColor=[UIColor whiteColor];
            // Background view
        self.backgroundView.frame = self.bounds;
        self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
           // Image view
        self.imageView.frame = CGRectMake(self.bounds.origin.x+3, 3+self.bounds.origin.y, 146, 96);
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        if([Functions isIPad]){
            self.bounds=CGRectMake(self.bounds.origin.x, self.bounds.origin.y, 250,150);
            self.imageView.frame = CGRectMake(self.bounds.origin.x+3, 3+self.bounds.origin.y, 238, 138);
        }
            // Layout label
        self.textLabelBackgroundView.frame = CGRectMake(0, 
                                                        self.bounds.size.height - labelHeight - inset-5,
                                                        self.bounds.size.width-10,
                                                        labelHeight);
        self.textLabelBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
            // Layout label background
        self.priceLabel.frame =CGRectMake(0,0,self.bounds.size.width/2,30);// CGRectInset(f, inset, 0);
        self.priceLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        
        // Layout label background
        //CGRect g = CGRectMake(0,CGRectGetMaxY( self.textLabel.frame),100,30);
    
        self.textLabel.frame = CGRectMake(0,CGRectGetMaxY( self.priceLabel.frame),self.bounds.size.width-10,30);//CGRectInset(g, inset,0);
        self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        
        // setting textlabel width dynamically
        CGSize txtSz = [self.textLabel.text sizeWithFont:[UIFont fontWithName:@"Segoe UI" size:16]];
        
        txtSz.width = txtSz.width>self.bounds.size.width ? self.bounds.size.width:txtSz.width;
        
        self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x,self.textLabel.frame.origin.y-3, txtSz.width, txtSz.height);
        
        // setting priceLabel width dynamically
        txtSz = [self.priceLabel.text sizeWithFont:[UIFont fontWithName:@"Segoe UI" size:14]];
        self.priceLabel.frame = CGRectMake(self.priceLabel.frame.origin.x,self.priceLabel.frame.origin.y+7, txtSz.width, txtSz.height);
        
        self.orderBtn.frame = CGRectMake(self.bounds.size.width-35,5,30,30);//CGRectInset(g, inset,0);
        
        //[self.orderBtn setFrame:CGRectMake(3+(aantal*200)+195-30, 2, 30, 30)];
        
        
    }
    
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
}

@end
