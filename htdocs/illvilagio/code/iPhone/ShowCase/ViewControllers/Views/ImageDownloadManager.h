//
//  ImageDownloadManager.h
//  POS
//
//  Created by admin on 11/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloadManager : NSObject {
	NSMutableArray *queue;
	NSThread *thread;
	
    
	BOOL alreadyStartedProcess;
}
@property (strong) NSOperationQueue *operationQueue;

+(ImageDownloadManager*)sharedImageDownalodManager;

-(void)addToQueue:(id)object;
-(void)startDownloads;
-(void)clearImages;

@end
