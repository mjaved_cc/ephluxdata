//
//  menuTableViewViewController.h
//  ShowCase
//
//  Created by Syed Sami on 5/17/13.
//
//

#import <UIKit/UIKit.h>

@interface menuTableViewViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *tb;
    NSArray *menuListingArray;
    IBOutlet UIButton *btnOrder;

}

@property(nonatomic, retain) NSArray *menuListingArray;
@property(nonatomic, retain) IBOutlet UIButton *btnOrder;
@property(nonatomic, retain)IBOutlet UITableView *tb;

-(IBAction)addOrder:(id)sender;

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame;

@end
