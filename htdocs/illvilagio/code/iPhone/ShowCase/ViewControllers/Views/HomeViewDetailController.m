//
//  HomeViewController.m
//  ShowCase
//
//  Created by Syed Sami on 5/21/13.
//
//

#import "HomeViewDetailController.h"
#import "OrderListController.h"
#import "UIViewController+Container.h"

@interface HomeViewDetailController ()

@end

@implementation HomeViewDetailController

@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.title = NSLocalizedString(@"Deal Detail", @"Deal Detail");
        // Custom initialization
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    
    self.view.frame=frame;
    self.title = NSLocalizedString(@"Deal Detail", @"Deal Detail");
    
    //self.view.backgroundColor = [UIColor blackColor];
    return self;
    
}

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.view.backgroundColor = [UIColor blackColor];
        self.view.frame=frame;
    }
    
    return self;
    
    
}


-(void)showNextView {
    
    //OrderListController *childViewController=[[OrderListController alloc] initWithNibName:@"OrderListController" bundle:nil];
    
    OrderListController *childViewController=[[OrderListController alloc] initWithNibNameAndFrame:@"OrderListController" bundle:nil frame:self.view.frame];
    
    [self addChildViewController:childViewController];
    
    CGRect retainFrame;
    CGRect myFrame = childViewController.view.frame;
    retainFrame=myFrame;
    myFrame.origin.x = myFrame.origin.x + 500;
    childViewController.view.frame = myFrame;
    
    [UIView transitionWithView:self.view
                      duration:0.4
                       options:UIViewAnimationOptionCurveEaseIn
                    animations:^{
                        
                        [self.view addSubview:childViewController.view];
                        childViewController.view.frame = retainFrame;
                        
                    }
     
                    completion:nil];
    
    
    [childViewController didMoveToParentViewController:self];
    
    [self performSelector:@selector(removeView:) withObject:childViewController afterDelay:3];
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    
    [self addDealView];
    //[self addSpecial];
    [self addUpComingEventView];
    
    [self addBackground];
    [self.view addSubview:scrollView];
}


-(void)addBackground{
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    
    bgImageView.image = [UIImage imageNamed:@"body_bg.png"];
    
    [self.view addSubview:bgImageView];
    
    
}



-(void)addDealView{
    
    UIImageView *dealImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, 510, 401)];
    
    dealImageView.image = [UIImage imageNamed:@"img_holder_full-width.png"];
    
    [scrollView addSubview:dealImageView];
    
    
}

-(void)addUpComingEventView{
    
    
    UIImageView *upcomingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 470, 510, 201)];
    
    upcomingImageView.image = [UIImage imageNamed:@"img_holder_full-width.png"];
    
    [scrollView addSubview:upcomingImageView];
    
    upcomingImageView=nil;
    
}

-(void)addSpecial{
    
    UIImageView *special1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 255, 247, 196)];
    
    special1.image = [UIImage imageNamed:@"img_holder_half-width.png"];
    
    [scrollView addSubview:special1];
    
    special1=nil;
    
    UIImageView *special2 = [[UIImageView alloc] initWithFrame:CGRectMake(260, 255, 247, 196)];
    
    special2.image = [UIImage imageNamed:@"img_holder_half-width.png"];
    
    [scrollView addSubview:special2];
    
    special2=nil;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
