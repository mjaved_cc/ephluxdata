//
//  MyLeftViewController.m
//  MMDrawerControllerKitchenSink
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "MyLeftViewController.h"
//#import "MMExampleCenterTableViewController.h"
//#import "MMSideDrawerSectionHeaderView.h"
#import "GalleryViewController.h"
#import "CenterViewController.h"

@interface MyLeftViewController ()

@end

@implementation MyLeftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton* gallertBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 100, 50, 50)];
    gallertBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [gallertBtn setTitle:@"click" forState:UIControlStateNormal];
    
    [gallertBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:gallertBtn];
    
}
-(IBAction) buttonAction:(id)sender{
    GalleryViewController * center = [[GalleryViewController alloc] init];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:center];
        [self.mm_drawerController
         setCenterViewController:nav
         withCloseAnimation:YES
         completion:nil];
  

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
