//
//  OrderListController.m
//  ShowCase
//
//  Created by USER on 5/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "OrderListController.h"
#import "DataStoreManager + DataSpecialized.h"
#import "RootViewController.h"
#import "Defines.h"
#import "Order.h"
#import "OrderListControllerCell.h"
#import "DDAlertPrompt.h"
#import "OrderCustomerViewController.h"

@implementation OrderListController

@synthesize btnDeleteAll;
@synthesize arrayData;
@synthesize switchBtn;
@synthesize lblNetTotal,lblOrderDelivery,lblOrderTax,lblOrderTotal;
@synthesize btnPlaceOrder;
@synthesize lblMyOrder;
@synthesize tableViewOrder;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.view.frame = frame;
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.btnDeleteAll.titleLabel.textColor = UIColorFromRGB(0x5b1900);
    self.btnDeleteAll.titleLabel.font = [UIFont boldSystemFontOfSize:155.0];
    self.btnDeleteAll.titleLabel.text = @"Delete All Order";
    self.btnDeleteAll.alpha=0;
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderItemAdded) name:kNotificationOrdersItemAdded object:nil];
 
    
    
    totalOrder=0;
    tax=10;
    netTotalOrder=0;
    deliveryCharges=0;
    lblOrderTax.text=@"10 %";
    lblOrderDelivery.text=@"0 SAR";
    
    if([Functions isIPad]){
    
    [self.lblMyOrder setFont:[UIFont fontWithName:@"Script MT bold" size:20]];
    
    [self.btnPlaceOrder.titleLabel setFont:[UIFont fontWithName:@"Oxygen Bold" size:16]];

    [self.lblNetTotal setFont:[UIFont fontWithName:@"Segoe UI semibold" size:15]];
    
    }else{
    
    if(IS_IPHONE5){
    
        imageBackGroundView.image = [UIImage imageNamed:@"right_panel_i5@2x.png"];
        imageBackGroundView.frame=CGRectMake(imageBackGroundView.frame.origin.x, imageBackGroundView.frame.origin.y, imageBackGroundView.frame.size.width, self.view.frame.size.height);
    
    }
        
    [self.lblMyOrder setFont:[UIFont fontWithName:@"Script MT bold" size:20]];
        
    [self.btnPlaceOrder.titleLabel setFont:[UIFont fontWithName:@"Oxygen Bold" size:16]];
        
    [self.lblNetTotal setFont:[UIFont fontWithName:@"Segoe UI semibold" size:15]];
        
    }
    
    [self.tableViewOrder setDelegate:self];
    [self.tableViewOrder setDataSource:self];
    
       //self.lblMyOrder.font self
}

-(void)viewWillAppear:(BOOL)animated{
    @try {
        [super viewWillAppear:animated];
        
        if (arrayData!=nil) {
            arrayData=nil;
        }
        
        arrayData=[[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] getAllOrderItem]];
        
    
    }
    @catch (NSException *exception) {
        NSLog(@"viewWillAppear Exception");
    }
  

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
	
}


#pragma mark-
#pragma mark-UIACTION
-(IBAction)goHome:(id)sender{
    
    NSArray *viewControllers = [self.navigationController viewControllers];
    
    for(UIViewController *viewController in viewControllers) {
        if([viewController isKindOfClass:[RootViewController class]]) {
      
            [self.navigationController popToViewController:viewController animated:YES];
            break;
        }	
    }
    
    
}
-(IBAction)actionBack:(id)sender{     
    

   /* CATransition *animation=[CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.75];
    [animation setType:@"genieEffect"];
    [animation setFillMode:kCAFillModeRemoved];
    
    animation.endProgress=0.99;
    [animation setRemovedOnCompletion:YES];
    [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark-UItableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (arrayData!=nil) {
        arrayData=nil;
    }
    
    arrayData=[[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] getAllOrderItem]];
   
    totalOrder=0;
    netTotalOrder=0;
    
    if ([arrayData count]>0) {
        
        for(int i = 0 ; i<[arrayData count]; i++){
        
        int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:i] intValue];
        
        NSString * quantity=[[[arrayData valueForKey:@"quantity"] objectAtIndex:i] stringValue];
        
        
        NSString * comment=@"Click Here For Comments.";
        
        if ([NSNull null]!=(NSNull*) [[arrayData valueForKey:@"comment"] objectAtIndex:i])
            comment=[[arrayData valueForKey:@"comment"] objectAtIndex:i];
        
        CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:itemID];
        
        int price =[categoryItem.price intValue];
        int total = (price * ([quantity intValue]));
        
        totalOrder+=total;
        }
        
        
        lblOrderTotal.text=[NSString stringWithFormat:@"%d SAR",totalOrder];
        
        lblNetTotal.text = [NSString stringWithFormat:@"%d SAR",totalOrder + totalOrder*(tax/100)];
    }
    

   
    return 1;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    @try {
        //NSLog(@"Array count %d",[arrayData count]);
        if([arrayData count] == 0 ){
        
            lblNetTotal.text =@"0 SAR";
            lblOrderTotal.text =@"0 SAR";
            lblOrderDelivery.text=@"0 SAR";
            self.btnDeleteAll.alpha=0;
            self.tableViewOrder.alpha=0;
            infoView.alpha=1;
        
            
        }else{
            
            infoView.alpha=0;
            self.tableViewOrder.alpha=1;
            self.btnDeleteAll.alpha=0;

        }

        return [arrayData count];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Lookup %@",exception);
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
       /* UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }*/
        OrderListControllerCell *cell = (OrderListControllerCell*)[tableView dequeueReusableCellWithIdentifier:[OrderListControllerCell cellIdentifier]];
        if (cell == nil) {
            cell = [OrderListControllerCell createCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.btnAddItem addTarget:self action:@selector(actionAddItem:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnMinusItem addTarget:self action:@selector(actionMinusItem:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnComment addTarget:self action:@selector(actionComment:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        
        
        if ([arrayData count]>0) {

            int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:indexPath.row] intValue];
            
            NSString * quantity=[[[arrayData valueForKey:@"quantity"] objectAtIndex:indexPath.row] stringValue];
 
          
            NSString * comment=@"Click Here For Comments.";
            
            if ([NSNull null]!=(NSNull*) [[arrayData valueForKey:@"comment"] objectAtIndex:indexPath.row])
                comment=[[arrayData valueForKey:@"comment"] objectAtIndex:indexPath.row];

            CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:itemID];
    
            int price =[categoryItem.price intValue];
            int total = (price * ([quantity intValue]));
            
    
            cell.labelItemName.text=categoryItem.name;

            cell.labelQty.text= [NSString stringWithFormat:@"%d x %@",price,quantity];
            
            cell.labelTotal.text=[NSString stringWithFormat:@"%d SAR",total];
            
            cell.labelSNo.text=[NSString stringWithFormat:@"%d",indexPath.row+1];
            [cell.btnMinusItem setTag:indexPath.row];
            [cell.btnAddItem setTag:indexPath.row];
            [cell.btnComment setTag:indexPath.row];
            
            [cell.labelQty setFont:[UIFont fontWithName:@"Segoe UI" size:12]];
        
            [cell.labelTotal setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
            
            [cell.labelSNo setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
            
            [cell.labelItemName setFont:[UIFont fontWithName:@"Segoe UI" size:16]];

        }
        
        

        return cell;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   /* @try {
        int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:indexPath.row] intValue];
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:itemID];

        if ([orderItem.quantity intValue]>0) {
            int qty=[orderItem.quantity intValue]-1;
            orderItem.quantity=[NSNumber numberWithInt:qty];
            [[DataStoreManager manager] saveData];
        }
        
        [self viewWillAppear:YES];
        [tableViewOrder reloadData];
    }@catch (NSException *exception) {
        NSLog(@"didSelectRowAtIndexPath Exception");
    }
   */
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
   // UIImage *headerImage=[UIImage imageNamed:@"List_HeaderTab"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setUserInteractionEnabled:NO];
    //[button setBackgroundImage:headerImage forState:UIControlStateNormal];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    return button;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}
#pragma mark-
#pragma mark-
-(IBAction)actionMinus:(id)sender{
   
    totalOrder=0;
    netTotalOrder=0;
    
    [[DataStoreManager manager] clearOrder];
    [[DataStoreManager manager] saveData];
    [self viewWillAppear:YES];
    [tableViewOrder reloadData];

}

-(IBAction)actionMinusItem:(id)sender{
    @try {
        totalOrder=0;
        netTotalOrder=0;

        UIButton *btnSender=(UIButton*)sender;
        int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:btnSender.tag] intValue];
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:itemID];
        if ([orderItem.quantity intValue]>0) {
            int qty=0;
            orderItem.quantity=[NSNumber numberWithInt:qty];
            [[DataStoreManager manager] saveData];
        }
        [self viewWillAppear:YES];
        [tableViewOrder reloadData];
    }@catch (NSException *exception) {
        NSLog(@"didSelectRowAtIndexPath Exception");
    }
}
-(IBAction)actionAddItem:(id)sender{
    @try {
        

        UIButton *btnSender=(UIButton*)sender;
        int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:btnSender.tag] intValue];
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:itemID];
        if ([orderItem.quantity intValue]>0) {
            int qty=[orderItem.quantity intValue]+1;
            orderItem.quantity=[NSNumber numberWithInt:qty];
            [[DataStoreManager manager] saveData];
        }
        [self viewWillAppear:YES];
        [tableViewOrder reloadData];
    }@catch (NSException *exception) {
        NSLog(@"didSelectRowAtIndexPath Exception");
    }
    
}

-(IBAction)actionComment:(id)sender{
    @try {
        
        UIButton *btnSender=(UIButton*)sender;
        commentID=btnSender.tag;
        DDAlertPrompt *loginPrompt = [[DDAlertPrompt alloc] initWithTitle:@"Comments" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitle:@"Add"];	
        loginPrompt.tag=110;
        UIImage *alertImage = [UIImage imageNamed:@"iphone_popup_bg"];
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:alertImage];
        backgroundImageView.frame = CGRectMake(0, 0, 282, 175);
        backgroundImageView.contentMode = UIViewContentModeScaleToFill;
        [loginPrompt addSubview:backgroundImageView];
        [loginPrompt sendSubviewToBack:backgroundImageView]; 
        [loginPrompt show];

    }@catch (NSException *exception) {
        NSLog(@"didSelectRowAtIndexPath Exception");
    }
    
}

- (void)didPresentAlertView:(UIAlertView *)alertView {
	if ([alertView isKindOfClass:[DDAlertPrompt class]]) {
		DDAlertPrompt *loginPrompt = (DDAlertPrompt *)alertView;
		[loginPrompt.plainTextField becomeFirstResponder];		
		[loginPrompt setNeedsLayout];
	}
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == [alertView cancelButtonIndex]) {
	} else {
        

        if ([alertView isKindOfClass:[DDAlertPrompt class]]) {
			DDAlertPrompt *loginPrompt = (DDAlertPrompt *)alertView;
                // NSLog(@"commentID %d",commentID);
            int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:commentID] intValue];
            Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:itemID];
            if ([orderItem.quantity intValue]>0) {
                orderItem.comment=loginPrompt.plainTextField.text;
                [[DataStoreManager manager] saveData];
            }
            [self viewWillAppear:YES];
            [tableViewOrder reloadData];
            
        }
    }
}        

-(IBAction)actionSwitch:(id)sender{

    if(!self.switchBtn.selected)
    self.switchBtn.selected = YES;
    else
    self.switchBtn.selected = NO;

}

-(void)orderItemAdded{

    [self.tableViewOrder reloadData];

}

- (NSMutableDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
    id objectInstance;
    NSUInteger indexKey = 0;
    
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    for (objectInstance in array)
        [mutableDictionary setObject:objectInstance forKey:[NSNumber numberWithUnsignedInt:indexKey++]];
    
    return mutableDictionary;
}

-(IBAction)placeOrder:(id)sender{
    
    OrderCustomerViewController *rootViewController=[[OrderCustomerViewController alloc] initWithNibName:@"OrderCustomerViewController" bundle:nil];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    [self.mm_drawerController
     setCenterViewController:nav
     withCloseAnimation:YES
     completion:nil];
    
}



@end
