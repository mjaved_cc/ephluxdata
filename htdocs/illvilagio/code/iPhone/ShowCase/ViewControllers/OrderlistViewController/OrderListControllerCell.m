//
//  OrderListControllerCell.m
//  ShowCase
//
//  Created by USER on 5/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "OrderListControllerCell.h"
#import "Functions.h"
@implementation OrderListControllerCell

@synthesize labelSNo;
@synthesize labelQty;
@synthesize labelTotal;
@synthesize btnAddItem;
@synthesize btnMinusItem;
@synthesize labelItemName;
@synthesize btnComment;

+(NSString*)cellIdentifier {
	static NSString *cellIdentifier = @"OrderListControllerCellIdentifier";
	return cellIdentifier;
}

+(id)createCell {

    if([Functions isIPad]){
    
        return [Functions loadUniversalNibNamed:@"OrderListControllerCell" owner:self];
    
    }else{
   
        return [Functions loadUniversalNibNamed:@"iPhone_OrderListControllerCell" owner:self];

    }
}



@end
