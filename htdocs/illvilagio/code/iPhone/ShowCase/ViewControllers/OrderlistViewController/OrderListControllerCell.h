//
//  OrderListControllerCell.h
//  ShowCase
//
//  Created by USER on 5/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderListControllerCell : UITableViewCell


@property(nonatomic,strong) IBOutlet UILabel *labelSNo;
@property(nonatomic,strong) IBOutlet UILabel *labelItemName;
@property(nonatomic,strong) IBOutlet UILabel *labelQty;
@property(nonatomic,strong) IBOutlet UILabel *labelTotal;
@property(nonatomic,strong) IBOutlet UIButton *btnAddItem;
@property(nonatomic,strong) IBOutlet UIButton *btnMinusItem;
@property(nonatomic,strong) IBOutlet UIButton *btnComment;
@property(nonatomic,strong) IBOutlet UIImageView *itemImage;
@property(nonatomic,strong) IBOutlet UIImageView *backgroundItemImage;


+(NSString*)cellIdentifier;
+(id)createCell;
@end
