//
//  MyRightViewController.m
//  MMDrawerControllerKitchenSink
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "EventsViewController.h"
#import "AsyncImageView.h"
#import "CenterViewController.h"
#import "EventDetailViewController.h"


#define imageHeight 0


@interface EventsViewController ()

@end

@implementation EventsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.title = @"Event";
        self.title=NSLocalizedString(@"Events", @"Events");
        
        [Functions setNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    eventArray=[[NSMutableArray alloc]init];
    currentEventArray=[[NSMutableArray alloc]init];
    archiveEventArray=[[NSMutableArray alloc]init];
    //eventArray=[[[SingletonClass sharedInstance] allContentDic] objectForKey:@"events"];
   // NSData *myEncodedObject2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"allinformation"];
    //NSDictionary *obj = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject2];
    
    
    
    lblArchive.text=NSLocalizedString(@"Archived Events",@"Archived Events");
    [lblCurrentEventNotAvilable setText:NSLocalizedString(@"Not Available",@"Not Available")];
    [lblArchiveEventNotAvilable setText:NSLocalizedString(@"Not Available",@"Not Available")];
    
    eventArray=(NSMutableArray*)[Functions  getDataFromNSUserDefault_Key:@"events"];
    [self ComapreDate];
    
    if([Functions isIPad]){
        // viewHeight=150;
        
        //currentScrollView.frame=CGRectMake(0, 20, 320, viewHeight+20);
        //archiveScrollView.frame=CGRectMake(0, 230, 320, viewHeight+20);
    }else{
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];
        
        if(IS_IPHONE5 ){
            currentScrollView.frame=CGRectMake(0, 20, 320, currentScrollView.frame.size.height);
            archiveScrollView.frame=CGRectMake(0, 230, 320, archiveScrollView.frame.size.height);
        }else{
            currentScrollView.frame=CGRectMake(0,20, 320, currentScrollView.frame.size.height);
            archiveScrollView.frame=CGRectMake(0, 230, 320, archiveScrollView.frame.size.height);
        }
        
    }
    
}
-(void) eventViewNotificationFunction:(NSNotification*)notification{
    NSDictionary *dict = notification.userInfo;
    eventArray=[[dict objectForKey:@"status"]  objectForKey:@"Event"] ;
    numberOfViews=[eventArray count];
    [[NSUserDefaults standardUserDefaults] setValue:@"2013-02-02" forKey:@"EventLastDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"EventLastDate"]);
    [[SingletonClass sharedInstance] setEventArray:eventArray];
    
    [self ComapreDate];
    
}

#pragma mark iphone
#pragma mark CreateGalleryView
-(void) CreateCurrentView{
    @try {
        viewHeight=117;
        viewWidth=130;
        imageWidth=155;
        distanceBetweenImages=13;
        
        int nextHeight=0;
        currentIndex=0;
        if([currentEventArray count]>0){
            [lblCurrentEventNotAvilable setHidden:YES];
          for(int i=0; i<[currentEventArray count];i++){
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5,5,viewWidth, viewHeight)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(distanceBetweenImages+(i*imageWidth),0,viewWidth, viewHeight)];
                UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"iphone_gallery_image_holder.png"]];
                imageView.frame=CGRectMake(0, 0, viewWidth+10, viewHeight+10);
                [viewFrame addSubview:imageView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                //
                
                NSString *stImageURL;
                if([[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"images"] count]>0){
                    stImageURL=[[[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"];
                }
                
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                }
                [viewFrame addSubview:asynImageView];
                UILabel*    lblDate=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, 72, 110, 20)];
                lblDate.text= [Functions getLocalizedDate:[Functions dateFormate:[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"event_time"]]];
                
                [lblDate setBackgroundColor:[UIColor lightGrayColor]];
                
                [lblDate setFont:[UIFont fontWithName:@"Segoe UI" size:13]];
                lblDate.textColor = UIColorFromRGB(0x5b1900);
                lblDate.backgroundColor = [UIColor whiteColor];
                lblDate.alpha = 0.8;
                lblDate.textAlignment = UITextAlignmentLeft;
                CGSize txtSz1 = [lblDate.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 13]];
                lblDate.frame = CGRectMake(lblDate.frame.origin.x, lblDate.frame.origin.y, txtSz1.width+10, lblDate.frame.size.height);
                
                [viewFrame addSubview:lblDate];
                
                UILabel*    lblDishName=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, lblDate.frame.origin.y+lblDate.frame.size.height, 120, 30)];
                lblDishName.text=[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"title"];
                [lblDishName setBackgroundColor:[UIColor lightGrayColor]];
                [lblDishName setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDishName.textColor = [UIColor grayColor];//UIColorFromRGB(0x5b1900);
                lblDishName.backgroundColor = [UIColor whiteColor];
                lblDishName.alpha = 0.8;
                lblDishName.textAlignment = UITextAlignmentCenter;
                CGSize txtSz = [lblDishName.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
              
              if(txtSz.width+10>130.0){
               lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, 130.0, lblDishName.frame.size.height);
              }else{
                lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
              }
                [viewFrame addSubview:lblDishName];
                
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=100+currentIndex;
                viewFrame.backgroundColor = [UIColor greenColor];
                
                [currentScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
                //}
                nextHeight=distanceBetweenImages+(i*imageWidth);
                distanceBetweenImages=13+distanceBetweenImages;
            }
            currentScrollView.contentSize = CGSizeMake(nextHeight+imageWidth, viewHeight);
            currentScrollView.tag=0;
            [self.view addSubview:currentScrollView];
        }else{
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

#pragma mark CreateArchiveView
-(void) CreateArchiveView{
    @try {
        viewHeight=117;
        viewWidth=130;
        imageWidth=155;
        distanceBetweenImages=13;
        
        int nextHeight=0;
        currentIndex=0;
        if([archiveEventArray count]>0){
            [lblArchiveEventNotAvilable setHidden:YES];
            
            for(int k=0; k<([archiveEventArray count]);k++){
                
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5,5,viewWidth, viewHeight)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(distanceBetweenImages+(k*imageWidth),0,viewWidth, viewHeight)];
                UIImageView* imageFrameView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, viewWidth+10, viewHeight+10)];
                imageFrameView.image=[UIImage imageNamed:@"iphone_gallery_image_holder.png"];
                [viewFrame addSubview:imageFrameView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                NSString *stImageURL;
                if([[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"images"] count]>0){
                    stImageURL=[[[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"];
                }
                
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                }
                [viewFrame addSubview:asynImageView];
                
                UILabel*    lblDate=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, 73, 110, 20)];
                lblDate.text=[Functions getLocalizedDate:[Functions dateFormate:[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"event_time"]]] ;
                [lblDate setBackgroundColor:[UIColor lightGrayColor]];
                [lblDate setFont:[UIFont fontWithName:@"Segoe UI" size:13]];
                lblDate.textColor = UIColorFromRGB(0x5b1900);
                lblDate.backgroundColor = [UIColor whiteColor];
                lblDate.alpha = 0.8;
                lblDate.textAlignment = UITextAlignmentLeft;
                CGSize txtSz1 = [lblDate.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 13]];
                lblDate.frame = CGRectMake(lblDate.frame.origin.x, lblDate.frame.origin.y, txtSz1.width+10, lblDate.frame.size.height);
                
                [viewFrame addSubview:lblDate];
                
                UILabel*    lblDishName=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, lblDate.frame.origin.y+lblDate.frame.size.height, 120, 30)];
                lblDishName.text=[[archiveEventArray objectAtIndex:currentIndex]  objectForKey:@"title"];
                [lblDishName setBackgroundColor:[UIColor lightGrayColor]];
                [lblDishName setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDishName.textColor = [UIColor grayColor];//UIColorFromRGB(0x5b1900);
                lblDishName.backgroundColor = [UIColor whiteColor];
                lblDishName.alpha = 0.8;
                lblDishName.textAlignment = UITextAlignmentCenter;
                CGSize txtSz = [lblDishName.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
                if(txtSz.width+10>130.0){
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, 130.0, lblDishName.frame.size.height);
                }else{
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                }

                //lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                [viewFrame addSubview:lblDishName];
                
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=200+currentIndex;
                viewFrame.backgroundColor = [UIColor clearColor];
                
                [archiveScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
                // }
                
                nextHeight=distanceBetweenImages+(k*imageWidth);
                distanceBetweenImages=13+distanceBetweenImages;
                //NSLog(@"%d",nextHeight);
            }
            
            archiveScrollView.contentSize = CGSizeMake(nextHeight+imageWidth, viewHeight);
            archiveScrollView.tag=1;
            [self.view addSubview:archiveScrollView];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

#pragma mark ipad
#pragma mark CreateGalleryView
-(void) ipad_CreateCurrentView{
    @try {
        viewHeight=191;
        viewWidth=246;
        
        distanceBetweenImages=5;
        int nextHeight=0;
        currentIndex=0;
        if([currentEventArray count]>0){
            [lblCurrentEventNotAvilable setHidden:YES];
            for(int k=0; k<([currentEventArray count]);k++){
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(7,7,viewWidth-15, viewHeight-15)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(distanceBetweenImages+(k*viewWidth),0,viewWidth, viewHeight)];
                
                UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"img_holder_half-width.png"]];
                imageView.frame=CGRectMake(0, 0, viewWidth, viewHeight);
                [viewFrame addSubview:imageView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                
                NSString *stImageURL;
                if([[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"images"] count]>0){
                    stImageURL=[[[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"];
                }
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                }
                [viewFrame addSubview:asynImageView];
                UILabel*    lblDate=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, 135, 110, 20)];
                lblDate.text=[Functions getLocalizedDate:[Functions dateFormate:[[currentEventArray objectAtIndex:currentIndex] objectForKey:@"event_time"] ]];
                [lblDate setBackgroundColor:[UIColor lightGrayColor]];
                [lblDate setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDate.textColor = UIColorFromRGB(0x5b1900);
                lblDate.backgroundColor = [UIColor whiteColor];
                lblDate.alpha = 0.8;
                lblDate.textAlignment = UITextAlignmentCenter;
                CGSize txtSz1 = [lblDate.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
                lblDate.frame = CGRectMake(lblDate.frame.origin.x, lblDate.frame.origin.y, txtSz1.width+10, lblDate.frame.size.height);
                
                [viewFrame addSubview:lblDate];
                
                UILabel*    lblDishName=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, lblDate.frame.origin.y+lblDate.frame.size.height, 120, 30)];
                lblDishName.text=[[currentEventArray objectAtIndex:currentIndex]  objectForKey:@"title"];
                [lblDishName setBackgroundColor:[UIColor lightGrayColor]];
                [lblDishName setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDishName.textColor = [UIColor grayColor];//UIColorFromRGB(0x5b1900);
                lblDishName.backgroundColor = [UIColor whiteColor];
                lblDishName.alpha = 0.8;
                lblDishName.textAlignment = UITextAlignmentCenter;
                CGSize txtSz = [lblDishName.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
                if(txtSz.width+10>130.0){
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, 130.0, lblDishName.frame.size.height);
                }else{
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                }

                //lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                [viewFrame addSubview:lblDishName];
                
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=100+currentIndex;
                viewFrame.backgroundColor = [UIColor clearColor];
                
                [currentScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
                distanceBetweenImages=20+distanceBetweenImages;
                nextHeight=nextHeight+viewWidth;
            }
            currentScrollView.contentSize = CGSizeMake(nextHeight+distanceBetweenImages, viewHeight);
            currentScrollView.tag=0;
            [self.view addSubview:currentScrollView];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

#pragma mark CreateArchiveView
-(void) ipad_CreateArchiveView{
    @try {
        viewHeight=191;
        viewWidth=246;
        distanceBetweenImages=5;
        
        int nextHeight=0;
        currentIndex=0;
        if([archiveEventArray count]>0){
            [lblArchiveEventNotAvilable setHidden:YES];
            for(int k=0; k<([archiveEventArray count]);k++){
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(7,7,viewWidth-15, viewHeight-15)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(distanceBetweenImages+(k*viewWidth),0,viewWidth, viewHeight)];
                UIImageView* imageFrameView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,viewWidth,viewHeight)];
                imageFrameView.image=[UIImage imageNamed:@"img_holder_half-width.png"];
                [viewFrame addSubview:imageFrameView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                
                NSString *stImageURL;
                if([[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"images"] count]>0){
                    stImageURL=[[[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"images"] objectAtIndex:0] objectForKey:@"image_url"];
                }
                
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                    
                }
                [viewFrame addSubview:asynImageView];
                
                UILabel*    lblDate=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, 135, 110, 20)];
                lblDate.text=[Functions getLocalizedDate:[Functions dateFormate:[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"event_time"]]];
                //[[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"event_time"] substringWithRange:NSMakeRange(0, 10)] ;
                [lblDate setBackgroundColor:[UIColor lightGrayColor]];
                [lblDate setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDate.textColor = UIColorFromRGB(0x5b1900);
                lblDate.backgroundColor = [UIColor whiteColor];
                lblDate.alpha = 0.8;
                lblDate.textAlignment = UITextAlignmentCenter;
                CGSize txtSz1 = [lblDate.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
                lblDate.frame = CGRectMake(lblDate.frame.origin.x, lblDate.frame.origin.y, txtSz1.width+10, lblDate.frame.size.height);
                
                [viewFrame addSubview:lblDate];
                
                UILabel*    lblDishName=[[UILabel alloc]initWithFrame:CGRectMake(asynImageView.frame.origin.x, lblDate.frame.origin.y+lblDate.frame.size.height, 120, 30)];
                lblDishName.text=[[archiveEventArray objectAtIndex:currentIndex] objectForKey:@"title"];
                [lblDishName setBackgroundColor:[UIColor lightGrayColor]];
                [lblDishName setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                lblDishName.textColor = [UIColor grayColor];//UIColorFromRGB(0x5b1900);
                lblDishName.backgroundColor = [UIColor whiteColor];
                lblDishName.alpha = 0.8;
                lblDishName.textAlignment = UITextAlignmentCenter;
                CGSize txtSz = [lblDishName.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 15]];
                if(txtSz.width+10>130.0){
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, 130.0, lblDishName.frame.size.height);
                }else{
                    lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                }

               // lblDishName.frame = CGRectMake(lblDishName.frame.origin.x, lblDishName.frame.origin.y, txtSz.width+10, lblDishName.frame.size.height);
                [viewFrame addSubview:lblDishName];
                
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=200+currentIndex;
                viewFrame.backgroundColor = [UIColor clearColor];
                
                [archiveScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
                distanceBetweenImages=20+distanceBetweenImages;
                nextHeight=nextHeight+viewWidth;
                //NSLog(@"%d",nextHeight);
            }
            
            archiveScrollView.contentSize = CGSizeMake(nextHeight+distanceBetweenImages, viewHeight);
            archiveScrollView.tag=1;
            [self.view addSubview:archiveScrollView];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
    }
    
}

#pragma mark Find Current n Archive Event
-(void) ComapreDate{
    @try {
        for (int i=0; i<[eventArray count]; i++) {
            NSString* tempDate=[[eventArray objectAtIndex:i] objectForKey:@"event_time"];
            
            NSArray *divideArray = [tempDate componentsSeparatedByString:@" "];
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            
            NSDate *dt1 = [[NSDate alloc] init];
            NSDate *dt2 = [[NSDate alloc] init];
            dt1 = [df dateFromString:[divideArray objectAtIndex:0]];
            dt2 =[NSDate date];
            //NSLog(@"my date -> %@   system date -> %@",dt1,dt2);
            NSString* temp=[df stringFromDate:dt2];
            dt2=[df dateFromString:temp];
            
            NSComparisonResult result = [dt1 compare:dt2];
            if(result==NSOrderedDescending || result==NSOrderedSame  ){
                [currentEventArray addObject:[eventArray objectAtIndex:i]];
            }else if(result==NSOrderedAscending ){
                [archiveEventArray addObject:[eventArray objectAtIndex:i]];
            }
        }
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
        NSArray *sortedCurrentArray = [currentEventArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        [currentEventArray removeAllObjects];
        currentEventArray=[sortedCurrentArray copy];
        
        NSArray *sortedArchiveArray = [archiveEventArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        [archiveEventArray removeAllObjects];
        archiveEventArray=[sortedArchiveArray copy];
        if([Functions isIPad]){
            [self ipad_CreateArchiveView];
            [self ipad_CreateCurrentView];
        }else{
            [self CreateCurrentView];
            [self CreateArchiveView];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


- (void)oneTap:(UIGestureRecognizer *)gesture {
    
    int myViewTag = gesture.view.tag;
    NSLog(@"%d",myViewTag);
    EventDetailViewController   *eventDetailVC;
    if([Functions isIPad]){
        eventDetailVC=[[EventDetailViewController alloc]initWithNibName:@"ipad_DetailViewController" bundle:nil];
    }else{
        eventDetailVC=[[EventDetailViewController alloc]initWithNibName:@"EventDetailViewController" bundle:nil];
    }
    
    if (myViewTag<200) {
        myViewTag=myViewTag-100;
        eventDetailVC.detailDic=[currentEventArray objectAtIndex:myViewTag];
        eventDetailVC.isCurrent=TRUE;
        
    }else if (myViewTag>=200){
        myViewTag=myViewTag-200;
        eventDetailVC.detailDic=[archiveEventArray objectAtIndex:myViewTag];
        eventDetailVC.isCurrent=FALSE;
    }
    
    [self.navigationController pushViewController:eventDetailVC animated:YES];
}

#pragma mark - Button Handlers
-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
}
-(IBAction) buttonAction:(id)sender{
    CenterViewController * center = [[CenterViewController alloc] init];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:center];
    
    
    [self.mm_drawerController
     setCenterViewController:nav
     withCloseAnimation:YES
     completion:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
