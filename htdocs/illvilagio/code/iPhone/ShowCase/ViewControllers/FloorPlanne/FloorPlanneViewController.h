//
//  FloorPlanneViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import <UIKit/UIKit.h>

@interface FloorPlanneViewController : UIViewController<UIAlertViewDelegate>{

    NSMutableArray* tempTableArray;

    IBOutlet    UIView  *floorOneView;
    IBOutlet    UIView  *floorTwoView;
    IBOutlet    UIScrollView    *groudViewScroll;
    IBOutlet    UIScrollView    *mezzViewScroll;
    IBOutlet    UIButton    *btnGround;
    IBOutlet    UIButton    *btnMezznine;
    IBOutlet    UIButton    *btnGTable1;
    IBOutlet    UIButton    *btnGTable2;
    IBOutlet    UIButton    *btnGTable3;
    IBOutlet    UIButton    *btnGTable4;
    IBOutlet    UIButton    *btnGTable5;
    IBOutlet    UIButton    *btnGTable6;
    IBOutlet    UIButton    *btnGTable7;
    IBOutlet    UIButton    *btnMTable1;
    IBOutlet    UIButton    *btnMTable2;
    IBOutlet    UIButton    *btnMTable3;
    IBOutlet    UIButton    *btnMTable4;
    IBOutlet    UIButton    *btnMTable5;
    IBOutlet    UIButton    *btnMTable6;
    IBOutlet    UIButton    *btnMTable7;
    IBOutlet    UIButton    *btnMTable8;
    IBOutlet    UIButton    *btnMTable9;
    IBOutlet    UIButton    *btnMTable10;
    NSString* strFloors;
    NSString* strFloorName;
  
}
@property (nonatomic,strong) NSString*  tableName;
@property (nonatomic) BOOL  fromReservation;
-(IBAction)FloorClick:(id)sender;
-(IBAction)onClickGroundFloor:(id)sender;
-(IBAction)onClickMezzFloor:(id)sender;

@end
