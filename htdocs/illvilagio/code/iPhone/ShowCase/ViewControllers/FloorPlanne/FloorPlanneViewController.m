//
//  FloorPlanneViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import "FloorPlanneViewController.h"
#import "OrderCustomerConfirmViewController.h"

#define GROUND @"G"
#define MEZZ @"M"
@interface FloorPlanneViewController ()

@end

@implementation FloorPlanneViewController
@synthesize tableName,fromReservation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.title = NSLocalizedString(@"Choose Table",@"Choose Table");
        [Functions setNavigationBar];
        
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [floorOneView setHidden:NO];
    [floorTwoView setHidden:YES];
    
    [btnGround setTitle:NSLocalizedString(@"Ground", @"Ground") forState:UIControlStateNormal];
    [btnGround setTitle:NSLocalizedString(@"Ground", @"Ground") forState:UIControlStateHighlighted];
    [btnGround setTitle:NSLocalizedString(@"Ground", @"Ground") forState:UIControlStateSelected];
    btnGround.selected=TRUE;
    
    [btnMezznine setTitle:NSLocalizedString(@"Mezzenine", @"Mezzenine") forState:UIControlStateNormal];
    [btnMezznine setTitle:NSLocalizedString(@"Mezzenine", @"Mezzenine") forState:UIControlStateHighlighted];
    [btnMezznine setTitle:NSLocalizedString(@"Mezzenine", @"Mezzenine") forState:UIControlStateSelected];
    
    UIButton* someButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 61, 26)];
      if([lang isEqualToString:@"ar"]){
    [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_normal_ar.png"] forState:UIControlStateNormal];
    [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_pressed_ar.png"] forState:UIControlStateSelected];
     [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_pressed_ar.png"] forState:UIControlStateHighlighted];
      }else{
          [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_normal.png"] forState:UIControlStateNormal];
          [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_pressed.png"] forState:UIControlStateSelected];
          [someButton setBackgroundImage:[UIImage imageNamed:@"btn_done_pressed.png"] forState:UIControlStateHighlighted];
      }
    [someButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    someButton.tag=1;
    UIBarButtonItem* rightbutton = [[UIBarButtonItem alloc] initWithCustomView:someButton];
    [self.navigationItem setRightBarButtonItem:rightbutton];
    
    tempTableArray=[[NSMutableArray alloc]init];
    
    
    if([Functions isIPad]){
        if( self.fromReservation==TRUE){
            [self setBackBarButton];
            [self CheckSelectedTable];
            
        }
    }else{
        if( self.fromReservation==TRUE){
            [self setBackBarButton];
            [self CheckSelectedTable];
            
        }else{
            [self setupLeftMenuButton];
        }
        groudViewScroll.contentSize = CGSizeMake(850, 368);
        mezzViewScroll.contentSize = CGSizeMake(700, 368);
        
        //btnGTable1.enabled=NO;
        
        
        if(IS_IPHONE5 ){
            
        }else{
            
        }
    }
    
    
}
#pragma mark Ground Floor Tables Selection
-(IBAction)onClickGroundFloor:(id)sender{
    if([sender tag]==1){
        if(btnGTable1.selected ){
            btnGTable1.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable1.titleLabel.text]];
        }else {
            btnGTable1.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable1.titleLabel.text]];
        }
    }else if([sender tag]==2){
        if(btnGTable2.selected ){
            btnGTable2.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable2.titleLabel.text]];
        }else {
            btnGTable2.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable2.titleLabel.text]];
        }
    }else if([sender tag]==3){
        if(btnGTable3.selected ){
            btnGTable3.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable3.titleLabel.text]];
        }else {
            btnGTable3.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable3.titleLabel.text]];
        }
    }else if([sender tag]==4){
        if(btnGTable4.selected){
            btnGTable4.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable4.titleLabel.text]];
        }else {
            btnGTable4.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable4.titleLabel.text]];
        }
    }else if([sender tag]==5){
        if(btnGTable5.selected ){
            btnGTable5.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable5.titleLabel.text]];
        }else {
            btnGTable5.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable5.titleLabel.text]];
        }
    }else if([sender tag]==6){
        if(btnGTable6.selected ){
            btnGTable6.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable6.titleLabel.text]];
        }else {
            btnGTable6.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable6.titleLabel.text]];
        }
    }else if([sender tag]==7){
        if(btnGTable7.selected ){
            btnGTable7.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable7.titleLabel.text]];
        }else {
            btnGTable7.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",GROUND,btnGTable7.titleLabel.text]];
        }
    }
    
}
#pragma mark Mezznine Floor Tables Selection
-(IBAction)onClickMezzFloor:(id)sender{
    if([sender tag]==1){
        if(btnMTable1.selected ){
            btnMTable1.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable1.titleLabel.text]];
        }else {
            btnMTable1.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable1.titleLabel.text]];
        }
    }else if([sender tag]==2){
        if(btnMTable2.selected ){
            btnMTable2.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable2.titleLabel.text]];
        }else {
            btnMTable2.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable2.titleLabel.text]];
        }
    }else if([sender tag]==3){
        if(btnMTable3.selected ){
            btnMTable3.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable3.titleLabel.text]];
        }else {
            btnMTable3.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable3.titleLabel.text]];
        }
    }else if([sender tag]==4){
        if(btnMTable4.selected){
            btnMTable4.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable4.titleLabel.text]];
        }else {
            btnMTable4.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable4.titleLabel.text]];
        }
    }else if([sender tag]==5){
        if(btnMTable5.selected ){
            btnMTable5.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable5.titleLabel.text]];
        }else {
            btnMTable5.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable5.titleLabel.text]];
        }
    }else if([sender tag]==6){
        if(btnMTable6.selected ){
            btnMTable6.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable6.titleLabel.text]];
        }else {
            btnMTable6.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable6.titleLabel.text]];
        }
    }else if([sender tag]==7){
        if(btnMTable7.selected ){
            btnMTable7.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable7.titleLabel.text]];
        }else {
            btnMTable7.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable7.titleLabel.text]];
        }
    }else if([sender tag]==8){
        if(btnMTable8.selected ){
            btnMTable8.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable8.titleLabel.text]];
        }else {
            btnMTable8.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable8.titleLabel.text]];
        }
    }else if([sender tag]==9){
        if(btnMTable9.selected ){
            btnMTable9.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable9.titleLabel.text]];
        }else {
            btnMTable9.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable9.titleLabel.text]];
        }
    }else if([sender tag]==10){
        if(btnMTable10.selected ){
            btnMTable10.selected=NO;
            [tempTableArray removeObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable10.titleLabel.text]];
        }else {
            btnMTable10.selected=YES;
            [tempTableArray addObject:[NSString stringWithFormat:@"%@%@",MEZZ,btnMTable10.titleLabel.text]];
        }
    }
}

-(void) CheckSelectedTable{
    NSArray* tempArray=[tableName componentsSeparatedByString:@","];
    for (int i=0; i<[tempArray count];  i++) {
        NSString* tempstr=[tempArray objectAtIndex:i];
        [tempTableArray addObject:tempstr];
        if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T1"]]){
            btnMTable1.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T2"]]){
            btnMTable2.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T3"]]){
            btnMTable3.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T4"]]){
            btnMTable4.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T5"]]){
            btnMTable5.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T6"]]){
            btnMTable6.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T7"]]){
            btnMTable7.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T8"]]){
            btnMTable8.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T9"]]){
            btnMTable9.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",MEZZ,@"T10"]]){
            btnMTable10.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T1"]]){
            btnGTable1.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T2"]]){
            btnGTable2.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T3"]]){
            btnGTable3.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T4"]]){
            btnGTable4.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T5"]]){
            btnGTable5.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T6"]]){
            btnGTable6.selected=YES;
        }else if([tempstr isEqualToString:[NSString stringWithFormat:@"%@%@",GROUND,@"T7"]]){
            btnGTable7.selected=YES;
        }
    }
    
}
-(IBAction)FloorClick:(id)sender{
    if([sender tag]==0){
        [floorOneView setHidden:NO];
        [floorTwoView setHidden:YES];
        btnGround.selected=YES;
        btnMezznine.selected=NO;
        
    }else if([sender tag]==1){
        [floorOneView setHidden:YES];
        [floorTwoView setHidden:NO];
        btnGround.selected=NO;
        btnMezznine.selected=YES;
    }
}

-(void)back:(id)sender{
    if(fromReservation==FALSE){
        if([tempTableArray count]>0){
            [self SetTableFunction];
            NSDictionary* tempDicRecord=[[NSDictionary alloc]initWithObjectsAndKeys:[[SingletonClass sharedInstance] tableName],@"Tables",[self FloorIdes],@"Floor", nil];
            OrderCustomerConfirmViewController* orderController;
        if([Functions isIPad]){
           orderController=[[OrderCustomerConfirmViewController alloc]initWithNibName:@"ipad_OrderCustomerConfirmViewController" bundle:Nil];
        }else{
            orderController=[[OrderCustomerConfirmViewController alloc]initWithNibName:@"OrderCustomerConfirmViewController" bundle:Nil];
        }
            orderController.tableRecord =[[NSMutableDictionary alloc] initWithDictionary:tempDicRecord];
            orderController.nameString=NSLocalizedString(@"Your reserved tables",@"Your reserved tables");
            orderController.phoneString=[[SingletonClass sharedInstance] tableName];
            orderController.addressString=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Selected floor",@"Selected floor"),strFloorName];
            orderController.fromFloor=TRUE;
            
            [[SingletonClass sharedInstance] setTableName:NSLocalizedString(@"Select Table",@"Select Table")];
            [self.navigationController pushViewController:orderController animated:YES];
            
        }else{
            NSString* alertTitle=NSLocalizedString(@"Sorry",@"Sorry");
            NSString* alertMsg=NSLocalizedString(@"Please select table",@"Please select table");
            [Functions showAlert:alertTitle message:alertMsg];            
        }
        
    }else{
        [self SetTableFunction];
    if([sender tag]==0){//back button
        //        if([tempTableArray count]>0){
        //            UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Your reserve t" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancle", nil];
        //            [alert show];
        //
        //        }else{
        //[[SingletonClass sharedInstance] setTableName:NSLocalizedString(@"Select Table",@"Select Table")];
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    }
}
-(NSString*) FloorIdes{
    strFloors=@"";
    strFloorName=@"";
    NSRange rangeValue = [[[SingletonClass sharedInstance] tableName] rangeOfString:@"M" options:NSCaseInsensitiveSearch];
    if (rangeValue.length>0){
        strFloors=@"2";
        strFloorName=NSLocalizedString(@"Mezzenine",@"Mezzenine");
    }
    NSRange rangeValue2 = [[[SingletonClass sharedInstance] tableName] rangeOfString:@"G" options:NSCaseInsensitiveSearch];
    if (rangeValue2.length>0){
        if([strFloors isEqualToString:@""]){
            strFloors=@"1";
            strFloorName=NSLocalizedString(@"Ground",@"Ground");
        }else{
            strFloors=[NSString stringWithFormat:@"%@,1",strFloors];
            strFloorName=[NSString stringWithFormat:@"%@, %@",NSLocalizedString(@"Ground",@"Ground"),strFloorName];
        }
    }
    NSLog(@"found %@",strFloors);
    return strFloors;
}

-(void) SetTableFunction{
    for(int i=0; i<[tempTableArray count]; i++){
        if (i==0) {
            [[SingletonClass sharedInstance] setTableName:[NSString stringWithFormat:@"%@",[tempTableArray objectAtIndex:i]]];
        }else{
            [[SingletonClass sharedInstance] setTableName:[NSString stringWithFormat:@"%@,%@",[[SingletonClass sharedInstance] tableName],[tempTableArray objectAtIndex:i]]];
        }
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex==0){
        [self SetTableFunction];
        [self.navigationController popViewControllerAnimated:YES];
    }else if(buttonIndex==1){
        NSString* selectTable=NSLocalizedString(@"Select Table", @"Select Table");
        [[SingletonClass sharedInstance] setTableName:selectTable];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
