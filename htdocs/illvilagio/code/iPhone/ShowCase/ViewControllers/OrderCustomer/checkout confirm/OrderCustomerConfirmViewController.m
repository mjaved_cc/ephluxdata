//
//  FeedBackViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import "OrderCustomerConfirmViewController.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Defines.h"
#import "Order.h"
#import "DDAlertPrompt.h"
#import "CheckOutSuccessController.h"
#import "RootViewController_iPhone.h"

@interface OrderCustomerConfirmViewController ()

@end

@implementation OrderCustomerConfirmViewController
@synthesize nameString,phoneString,emailString,addressString;
@synthesize fromFloor;
@synthesize progressHud;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Checkout",@"Checkout");
        
        [Functions setNavigationBar];
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.progressHud = [[MBProgressHUD alloc] initWithView:self.view] ;
    self.progressHud.labelText =NSLocalizedString(@"Loading...",@"Loading...");
    [self.view addSubview:self.progressHud];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(OrderNotification:) name:kNotificationOrderDone object:nil];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                           nil];
    [numberToolbar sizeToFit];
    UIFont *lblFont = [UIFont fontWithName:@"Segoe UI" size:13.0f];
    [lblEmailAddress setFont:lblFont];
    textViewAddress.textColor=lblTelephone.textColor=lblEmailAddress.textColor=lblName.textColor=UIColorFromRGB(0x939393);
    
    [lblTelephone setFont:lblFont];
    [lblName setFont:lblFont];
    [textViewAddress setFont:lblFont];
    
    
    [lblOrderTotalValue setFont:[UIFont fontWithName:@"Segoe UI" size:15.0f]];
    [lblOrderDeliveryValue setFont:[UIFont fontWithName:@"Segoe UI" size:15.0f]];
    [btnChangeAddress.titleLabel setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [btnChangeMenu.titleLabel setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [btnCheckOut.titleLabel setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [lblOrderAmountTitle setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [lblOrderDeliveryTitle setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [lblOrderTypeTitle setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [lblOrderName setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    [lblOrderTax setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
    
    [self setLocalizeView];
    
    lblTelephone.text= self.phoneString;
    lblName.text=self.nameString;
    lblEmailAddress.text=self.emailString;
    textViewAddress.text=self.addressString;
    [self setOrderType];
    [self setTotalLabels];
    
    [self setBackBarButton];
    
    if([Functions isIPad]){
        
        if([@"ar" isEqualToString:[Functions getLocalLang] ]&& fromFloor==FALSE){
            [headerImageView setImage:[UIImage imageNamed:@"ipad_order_breadcrumb_2_ar.png"]];
        }else
            if(![@"ar" isEqualToString:[Functions getLocalLang] ]&& fromFloor==TRUE){
            [headerImageView setImage:[UIImage imageNamed:@"en_order_breadcrumb_dinein_1.png"]];
        }else
            if([@"ar" isEqualToString:[Functions getLocalLang]]&& fromFloor==TRUE){
                [headerImageView setImage:[UIImage imageNamed:@"ar_order_breadcrumb_dinein_1.png"]];
        
        }else
         if(![@"ar" isEqualToString:[Functions getLocalLang]] && fromFloor==FALSE){
            [headerImageView setImage:[UIImage imageNamed:@"order_breadcrumb_2.png"]];
        }
        
    
    }else{
        if([@"ar" isEqualToString:[Functions getLocalLang] ] && fromFloor==FALSE){
            [headerImageView setImage:[UIImage imageNamed:@"iphone_order_breadcrumb_2_ar.png"]];
        }else
            if(![@"ar" isEqualToString:[Functions getLocalLang]] && fromFloor==TRUE){
                [headerImageView setImage:[UIImage imageNamed:@"en_iphone_order_breadcrumb_dinein_1.png"]];
            }else
                if([@"ar" isEqualToString:[Functions getLocalLang]] && fromFloor==TRUE){
                    [headerImageView setImage:[UIImage imageNamed:@"ar_iphone_order_breadcrumb_dinein_1.png"]];
                }else
               if(![@"ar" isEqualToString:[Functions getLocalLang]] && fromFloor==FALSE) {
                    [headerImageView setImage:[UIImage imageNamed:@"iphone_order_breadcrumb_2.png"]];
                }
//        
//        if([@"ar" isEqualToString:[Functions getLocalLang]]){
//            [headerImageView setImage:[UIImage imageNamed:@"iphone_order_breadcrumb_2_ar.png"]];
//        }else{
//            [headerImageView setImage:[UIImage imageNamed:@"iphone_order_breadcrumb_3.png"]];
//        }
        
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];
        
        if(IS_IPHONE5){
            
        }else{
            
            btnChangeAddress.frame = CGRectMake(btnChangeAddress.frame.origin.x, btnChangeAddress.frame.origin.y-10, btnChangeAddress.frame.size.width, btnChangeAddress.frame.size.height);
            
            btnChangeMenu.frame = CGRectMake(btnChangeMenu.frame.origin.x, btnChangeMenu.frame.origin.y-10, btnChangeMenu.frame.size.width, btnChangeMenu.frame.size.height);
            
            orderTotalView.frame= CGRectMake(orderTotalView.frame.origin.x, orderTotalView.frame.origin.y+10, orderTotalView.frame.size.width, orderTotalView.frame.size.height);
            
        }
        
    }
    
    
}
-(void) setLocalizeView{
    
    [lblOrderTypeTitle setText:NSLocalizedString(@"Order Type", @"Order Type")];
    [lblOrderTypeValue setText:NSLocalizedString(@"Dine-In", @"Dine-In")];
    [lblOrderTotalTitle setText:NSLocalizedString(@"Order Total:", @"Order Total:")];
    [lblOrderDeliveryTitle setText:NSLocalizedString(@"Delivery Charges:", @"Delivery Charges:")];
    [lblOrderName setText:NSLocalizedString(@"Name", @"Name")];
    [lblOrderAmountTitle setText:NSLocalizedString(@"Net Amount", @"Net Amount")];
    [lblHeaderTxt setText:NSLocalizedString(@"Please confirm your order & checkout!", @"Please confirm your order & checkout!")];
    
    [btnChangeMenu setTitle:NSLocalizedString(@"Change Order", @"Change Order") forState:UIControlStateNormal];
    if(self.fromFloor==TRUE){
        [btnChangeAddress setTitle:NSLocalizedString(@"Change Tables", @"Change Tables") forState:UIControlStateNormal];
    }else{
        [btnChangeAddress setTitle:NSLocalizedString(@"Change Address", @"Change Address") forState:UIControlStateNormal];
    }
    
    [btnCheckOut setTitle:NSLocalizedString(@"CheckOut", @"CheckOut") forState:UIControlStateNormal];
    
    
    
}
#pragma mark FeedBack Notifire
-(void) OrderNotification:(NSNotification*)notification{
    [self.progressHud hide:YES];
    @try {
        
        
        
        NSDictionary* obj=notification.userInfo;
       // if(![[obj objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:0]]){
            CheckOutSuccessController *rootViewController;
            if([Functions isIPad]){
                rootViewController=[[CheckOutSuccessController alloc] initWithNibName:@"ipad_CheckOutSuccessVC" bundle:nil];
            }else{
                rootViewController=[[CheckOutSuccessController alloc] initWithNibName:@"iphone_CheckOutSuccessVC" bundle:nil];
            }
        if(fromFloor==TRUE){
         rootViewController.fromFloor=TRUE;
        }else{
        rootViewController.fromFloor=FALSE;
        }
            [self.navigationController pushViewController:rootViewController animated:YES];
            
//        }else{
//            NSString* alertTitletxt=NSLocalizedString(@"Warning",@"Warning");
//            NSString* alertMsg=NSLocalizedString(@"Server Error",@"Server Error");
//            [Functions showAlert:alertTitletxt message:alertMsg];
//            
//        }
    }
    @catch (NSException *exception) {
        
    }
}

-(void)setOrderType{
    
    
    NSString *orderTypeTitle;
    NSInteger orderTypeValue = [Functions getOrderType];
    
    if([Functions isDeliveryTarget]){
        orderTypeValue = 2;
    }
    
    switch (orderTypeValue) {
        case 0:
            orderTypeTitle=NSLocalizedString(@"TakeAway",@"TakeAway");
            strOrderType=@"TakeAway";
            break;
            
        case 1:
            orderTypeTitle=NSLocalizedString(@"Dine-In",@"Dine-In");
            strOrderType=@"Dine-In";
            break;
            
        case 2:
            orderTypeTitle=NSLocalizedString(@"Delivery",@"Delivery");
            strOrderType=@"Delivery";
            break;
            
            
        default:
            orderTypeTitle=NSLocalizedString(@"TakeAway",@"TakeAway");
            strOrderType=@"TakeAway";
            break;
    }
    
    lblOrderTypeValue.text =orderTypeTitle;
}

-(IBAction)changeAddress:(id)sender{
    
    [self back:nil];
}

-(IBAction)changeMenu:(id)sender{
    
    RootViewController_iPhone *rootViewController;
    
    if([Functions isIPad]){
        
        rootViewController=[[RootViewController_iPhone alloc] initWithNibName:@"ipad_RootViewController" bundle:nil];
        
        rootViewController.isChangeOrder = YES;
        
    }else{
        
        rootViewController=[[RootViewController_iPhone alloc] initWithNibName:@"RootViewController_iPhone" bundle:nil];
        
        rootViewController.isChangeOrder = NO;
    }
    
    [self.navigationController pushViewController:rootViewController animated:YES];
    
}

#pragma mark checkOutOrder
-(IBAction)checkOutOrder:(id)sender{
    
    
    BOOL isConnectedWithInternet = [InternetCheck IsConnected];
    
    if (isConnectedWithInternet) {
        
        [self sendCheckOut];
        
    }else{
        NSString* alertTitle=NSLocalizedString(@"Not Connected to wifi",@"Not Connected to wifi");
        NSString* alertMsg=NSLocalizedString(@"Thank You",@"Thank You");
        [Functions showAlert:alertTitle message:alertMsg];
        
    }
    
    
}


-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)placeOrderToServer{
    BOOL isConnectedWithInternet = [InternetCheck IsConnected];
    @try {
        
        
        if (isConnectedWithInternet) {
            [self.progressHud show:YES];
            
            NSMutableDictionary* orderData = [[NSMutableDictionary alloc] init];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd  hh:mm:ss"];
            NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
            
            
            // NSArray *itemIdArr = [NSArray arrayWithObjects:@"11",@"12",@"13",@"15",@"17", nil];
            NSMutableArray* tempItem=[[NSMutableArray alloc]initWithCapacity:[arrayData count]];
            for (int i=0; i<[arrayData count]; i++) {
                [tempItem addObject:@"11"];
            }
            
            
            for(int i=0;i<[arrayData count];i++){
                
                
                Order *orderK = (Order*)[arrayData objectAtIndex:i];
                
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                //NSString *orderDateString = [dateFormatter stringFromDate:orderK.entryDateTime];
                
                NSDictionary *singleOrder = [NSDictionary dictionaryWithObjectsAndKeys:
                                             //  orderK.itemBackendID, @"itemId",
                                             orderK.itemBackendID, @"itemId",
                                             //[NSNumber numberWithInt:22], @"price",
                                             orderK.seatNo,@"seatNo", // to be come from order table
                                             orderK.quantity,@"quantity",
                                             @"",@"comment",
                                             //[NSNumber numberWithInt:1],@"active",
                                             //orderDateString,@"entryDateTime",
                                             nil];
                
                [orderData setObject:singleOrder forKey:[NSString stringWithFormat:@"%d",i]];
                
            }
            
            //NSLog(@"%@ %@ %@",self.nameString,self.emailString,self.phoneString);
            NSDictionary *orderNameDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                             lblName.text, @"name",
                                             lblEmailAddress.text, @"email",
                                             lblTelephone.text,@"telephoneNo",
                                             textViewAddress.text,@"shippingAddress",
                                             nil];
            NSDictionary* tables=nil;
            NSDictionary *data=nil;
            if(self.fromFloor==TRUE){
                orderNameDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"ilvillaggio", @"name",
                                   @"http://www.ilvillaggio.com", @"email",
                                   @"201-935-7733",@"telephoneNo",
                                   @"651 Route 17 N Carlstadt, NJ 07072",@"shippingAddress",
                                   nil];
                
                tables=[NSDictionary dictionaryWithObject:[self.tableRecord objectForKey:@"Tables"] forKey:@"table_id"];
                data = [NSDictionary dictionaryWithObjectsAndKeys:
                        orderData, @"items",
                        orderNameDetail,@"customerDetail",
                        strOrderType, @"order_type",
                        @"1",@"status",
                        tables,@"Order",
                        @"0",@"order_table_id",
                        [self.tableRecord objectForKey:@"Floor"],@"FloorId",
                        dateString,@"entryDateTime",
                        [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] ,@"udid",
                        [NSString stringWithFormat:@"%d",[[SingletonClass sharedInstance] numberOfPeople]] ,@"no_seats",
                        //dateString,@"entryDateTime",
                        nil];
            }else{
                tables=[NSDictionary dictionaryWithObject:@"" forKey:@"table_id"];
                data = [NSDictionary dictionaryWithObjectsAndKeys:
                        orderData, @"items",
                        orderNameDetail,@"customerDetail",
                        strOrderType, @"order_type",
                        @"1",@"status",
                        tables,@"Order",
                        @"1",@"order_table_id",
                        @"1",@"FloorId",
                        dateString,@"entryDateTime",
                        [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] ,@"udid",
                        [NSString stringWithFormat:@"%d",[[SingletonClass sharedInstance] numberOfPeople]] ,@"no_seats",
                        //dateString,@"entryDateTime",
                        nil];
            }
            //    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
            //                          orderData, @"items",
            //                          orderNameDetail,@"customerDetail",
            //                          @"dinein", @"orderType",
            //                          @"1",@"status",
            //                          @"1",@"TableId",
            //                          @"1",@"FloorId",
            //                          dateString,@"entryDateTime",
            //                          nil];
            NSLog(@"%@",data);
            orderItems = [[NSMutableDictionary alloc] initWithDictionary:data];
            [[DataStoreManager manager] sendOrderCheckoutToServer:orderItems];
        }else{
            NSString* alertTitle=NSLocalizedString(@"Not Connected to wifi",@"Not Connected to wifi");
            NSString* alertMsg=NSLocalizedString(@"Thank You",@"Thank You");
            [Functions showAlert:alertTitle message:alertMsg];
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)setTotalLabels{
    
    if (arrayData!=nil) {
        arrayData=nil;
    }
    
    NSFetchedResultsController *a = [[DataStoreManager manager] getAllOrderItems];
    
    
    arrayData=[[NSMutableArray alloc] initWithArray:[a fetchedObjects]];
    
    int totalOrder=0;
    int tax=0;
    
    if ([arrayData count]>0) {
        
        for(int i = 0 ; i<[arrayData count]; i++){
            
            int itemID=[[[arrayData valueForKey:@"itemBackendID"] objectAtIndex:i] intValue];
            
            NSString * quantity=[[[arrayData valueForKey:@"quantity"] objectAtIndex:i] stringValue];
            
            
            NSString * comment=@"Click Here For Comments.";
            
            if ([NSNull null]!=(NSNull*) [[arrayData valueForKey:@"comment"] objectAtIndex:i])
                comment=[[arrayData valueForKey:@"comment"] objectAtIndex:i];
            
            CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:itemID];
            
            int price =[categoryItem.price intValue];
            int total = (price * ([quantity intValue]));
            
            totalOrder+=total;
        }
        
        
        lblOrderTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",totalOrder]];
        
        lblOrderAmountValue.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",totalOrder + totalOrder*(tax/100)]];
    }
    
}

-(void)sendCheckOut{
    BOOL isConnectedWithInternet = [InternetCheck IsConnected];
    
    if (isConnectedWithInternet) {
        
        [self placeOrderToServer];
        
        //        CheckOutSuccessController *rootViewController;
        //        if([Functions isIPad]){
        //            rootViewController=[[CheckOutSuccessController alloc] initWithNibName:@"ipad_CheckOutSuccessVC" bundle:nil];
        //        }else{
        //            rootViewController=[[CheckOutSuccessController alloc] initWithNibName:@"iphone_CheckOutSuccessVC" bundle:nil];
        //        }
        //
        //
        //        [self.navigationController pushViewController:rootViewController animated:YES];
        
        NSDictionary *orderInformation = [NSDictionary dictionaryWithObjectsAndKeys:
                                          lblName.text, @"name",
                                          lblEmailAddress.text, @"email",
                                          lblTelephone.text,@"telephoneNo",
                                          textViewAddress.text,@"shippingAddress",
                                          nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:orderInformation forKey:kOrderInformation];
    }else{
        NSString* alertTitle=NSLocalizedString(@"Not Connected to wifi",@"Not Connected to wifi");
        NSString* alertMsg=NSLocalizedString(@"Thank You",@"Thank You");
        [Functions showAlert:alertTitle message:alertMsg];
        
    }
}




-(void)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
