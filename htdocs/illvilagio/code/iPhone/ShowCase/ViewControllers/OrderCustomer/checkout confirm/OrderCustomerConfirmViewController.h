//
//  FeedBackViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface OrderCustomerConfirmViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblTelephone;
    IBOutlet UILabel        *lblEmailAddress;
    IBOutlet UILabel        *lblOrderTypeTitle;
    IBOutlet UILabel        *lblOrderTypeValue;
    IBOutlet UILabel        *lblOrderTotalTitle;
    IBOutlet UILabel        *lblOrderTotalValue;
    IBOutlet UILabel        *lblOrderDeliveryTitle;
    IBOutlet UILabel        *lblOrderDeliveryValue;
    IBOutlet UILabel        *lblOrderAmountTitle;
    IBOutlet UILabel        *lblOrderAmountValue;
    IBOutlet UILabel        *lblOrderName;
    IBOutlet UILabel        *lblOrderTax;
    IBOutlet UIButton       *btnCheckOut;
    IBOutlet UIButton       *btnChangeAddress;
    IBOutlet UIButton       *btnChangeMenu;
    IBOutlet UIView         *orderTotalView;
    IBOutlet UITextView     *textViewAddress;
    IBOutlet UIImageView    *headerImageView;
    IBOutlet UILabel        *lblHeaderTxt;
    NSMutableDictionary *orderItems;
    NSMutableArray* arrayData;
    NSString* strOrderType;
    
}

@property (nonatomic,assign) NSString *emailString;
@property (nonatomic,assign) NSString *nameString;
@property (nonatomic,assign) NSString *phoneString;
@property (nonatomic,assign) NSString *addressString;
@property (nonatomic,strong) NSMutableDictionary *tableRecord;
@property (nonatomic) BOOL  fromFloor;
@property (nonatomic, retain) MBProgressHUD *progressHud;

-(IBAction)checkOutOrder:(id)sender;
-(IBAction)changeAddress:(id)sender;
-(IBAction)changeMenu:(id)sender;


@end
