//
//  FeedBackViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import <UIKit/UIKit.h>

@interface OrderCustomerViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    IBOutlet UIView         *detailView;
    IBOutlet UITextField    *txtEmail;
    IBOutlet UITextField    *txtName;
    IBOutlet UITextField    *txtPhoneNo;
    IBOutlet UITextView     *txtShippingAddress;
    IBOutlet UIView         *popupView;
    IBOutlet UILabel        *lblFeedbackSent;
    IBOutlet UILabel        *lblAlertTitle;
    IBOutlet UILabel        *lblName;
    IBOutlet UILabel        *lblTelephone;
    IBOutlet UILabel        *lblEmailAddress;
    IBOutlet UIScrollView   *scrollView;
    IBOutlet UIButton       *btnOrderNow;
    
}


-(IBAction)SubmitFeedbacks:(id)sender;
-(IBAction)SubmitFeedback:(id)sender;
-(IBAction) OkPopup:(id)sender;
@end
