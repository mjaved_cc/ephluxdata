//
//  OrderListController.h
//  ShowCase
//
//  Created by USER on 5/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckOutSuccessController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    IBOutlet UITableView *tableViewOrder;
    int commentID;
    IBOutlet UIButton *btnDeleteAll;
    IBOutlet UIButton *switchBtn;
    IBOutlet UILabel *lblOrderTotal;
    IBOutlet UILabel *lblOrderTax;
    IBOutlet UILabel *lblOrderDelivery;
    IBOutlet UILabel *lblNetTotal;
    IBOutlet UIButton *btnPlaceOrder;
    IBOutlet UIView *infoView;
    IBOutlet UILabel *lblMsg;
    int totalOrder;
    int deliveryCharges;
    int tax; // in percentage
    int netTotalOrder;
    IBOutlet UIImageView* imageBackGroundView;
    
    IBOutlet UILabel        *lblOrderTotalTitle;
    IBOutlet UILabel        *lblOrderDeliveryTitle;
    IBOutlet UILabel        *lblOrderAmountTitle;
    IBOutlet UILabel        *lblYourOrder;
    IBOutlet UILabel        *lblHeaderTxt;
    IBOutlet UIImageView    *headerImageView;
    

}

@property(nonatomic,retain) NSMutableArray *arrayData;
@property(nonatomic,retain) IBOutlet UITableView *tableViewOrder;
@property(nonatomic,retain) IBOutlet UIButton *btnDeleteAll;
@property(nonatomic,retain)  IBOutlet UIButton *switchBtn;
@property(nonatomic,retain) IBOutlet UILabel *lblOrderTotal;
@property(nonatomic,retain) IBOutlet UILabel *lblOrderTax;
@property(nonatomic,retain) IBOutlet UILabel *lblOrderDelivery;
@property(nonatomic,retain) IBOutlet UILabel *lblNetTotal;
@property(nonatomic,retain) IBOutlet UIButton *btnPlaceOrder;
@property(nonatomic,retain) IBOutlet UILabel *lblMyOrder;
@property (nonatomic) BOOL  fromFloor;

-(id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame;
-(IBAction)actionBack:(id)sender;
-(IBAction)goHome:(id)sender;
-(IBAction)actionMinus:(id)sender;
-(IBAction)actionSwitch:(id)sender;
-(void)reloadTable;
-(IBAction)placeOrder:(id)sender;

@end
