//
//  FeedBackViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/31/13.
//
//

#import "OrderCustomerViewController.h"
#import "OrderCustomerConfirmViewController.h"

@interface OrderCustomerViewController ()

@end

@implementation OrderCustomerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Checkout",@"Checkout");
        [Functions setNavigationBar];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
  
    txtName.text =[[Functions getOrderInformation] valueForKey:@"name"];
    txtEmail.text =[[Functions getOrderInformation] valueForKey:@"emailaddress"];
    txtPhoneNo.text =[[Functions getOrderInformation] valueForKey:@"telephone_no"];
    txtShippingAddress.text =[[Functions getOrderInformation] valueForKey:@"address"];
    
//    txtPhoneNo.text = @"123456";
//    txtEmail.text = @"greatahbab@gmail";
//    txtName.text = @"Syed sami";
//    txtShippingAddress.text = @"12 123 132 ";

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                           nil];
    [numberToolbar sizeToFit];
    txtPhoneNo.inputAccessoryView = numberToolbar;
    [btnOrderNow.titleLabel setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15]];

    [lblFeedbackSent setFont:[UIFont fontWithName:@"Script MT bold" size:22.0f]];
    lblFeedbackSent.textColor = UIColorFromRGB(0x5b1900);
    [lblAlertTitle setFont:[UIFont fontWithName:@"Oxygen-Regular" size:18.0f]];
    //[lblEmailAddress setFont:[UIFont fontWithName:@"segoeuib_0" size:15.0f]];
    //[lblName setFont:[UIFont fontWithName:@"segoeuib_0" size:15.0f]];
    
    detailView.layer.cornerRadius = 5;
    detailView.layer.masksToBounds = YES;
    detailView.layer.borderWidth = 0.5f;
    detailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    // delete before going live
//    txtPhoneNo.text = @"123456789";
//    txtName.text = @"Syed Sami";
//    txtEmail.text = @"greatahbab@gmail.com";
//    txtShippingAddress.text = @"A -12 Sunny view gulistan e johar karachi";;
    
    
    if([Functions isIPad]){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }else{
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"iphone_top_bar_bg.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.view.layer setCornerRadius:10.0f];

        scrollView.contentSize = CGSizeMake(320,650);

     if(IS_IPHONE5){
    
         scrollView.contentSize = CGSizeMake(320,550);
    
     }
    }
}
#pragma mark Keyboard Hide
-(void)keyboardWillHide
{
    //call when key board hides
    [self animateTextField: Nil up: NO distance:0];
}
#pragma mark Submit Feedback
-(IBAction)SubmitFeedback:(id)sender{
   
    if([self validateFome]){
        OrderCustomerConfirmViewController *rootViewController;
        if([Functions isIPad]){
        rootViewController=[[OrderCustomerConfirmViewController alloc] initWithNibName:@"ipad_OrderCustomerConfirmViewController" bundle:nil];    
        
        }else{
        rootViewController=[[OrderCustomerConfirmViewController alloc] initWithNibName:@"OrderCustomerConfirmViewController" bundle:nil];
        }
        
     
        rootViewController.emailString = txtEmail.text;
        rootViewController.nameString = txtName.text;
        rootViewController.phoneString=txtPhoneNo.text;
        rootViewController.addressString=txtShippingAddress.text;
        
        NSDictionary *orderInformation = [NSDictionary dictionaryWithObjectsAndKeys:
                                         txtName.text, @"name",
                                         txtEmail.text, @"emailaddress",
                                         txtPhoneNo.text,@"telephone_no",
                                         txtShippingAddress.text,@"address",
                                        nil];

        [[NSUserDefaults standardUserDefaults] setObject:orderInformation forKey:kOrderInformation];
        
        [self.navigationController pushViewController:rootViewController animated:YES];
    
    }
 
}
-(BOOL) validateFome{
    NSString* alertTitle=NSLocalizedString(@"Sorry",@"Sorry");
     if([txtName.text isEqualToString:@""] || [txtShippingAddress.text isEqualToString:@""] || [txtPhoneNo.text isEqualToString:@""]){
        
         NSString* alertMsg=NSLocalizedString(@"Please fill all the fields",@"Please fill all the fields");
         
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }else if(![self validateEmailWithString:txtEmail.text]){
         NSString* alertMsg=NSLocalizedString(@"Please enter correct email format",@"Please enter correct email format");
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }
    return TRUE;
}

-(void)cancelNumberPad:(id)sender{
    [txtPhoneNo resignFirstResponder];
    [self animateTextField: sender up: NO distance:0];
    
}
#pragma mark TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    //if( txtEmail==textField){
        [self animateTextField: textField up: YES distance:-100];
        return YES;
    //}
    
   // return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    [textField resignFirstResponder];
    [self animateTextField: textField up: NO distance:0];
    
    
    return YES;
}
#pragma mark TextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self animateTextField:nil up:YES distance:-200];
    
    return YES;
}
#pragma mark email validate
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark keyboard UpView
- (void) animateTextField: (UITextField*) textField up: (BOOL) up distance:(int) distance
{
    const int movementDistance =distance; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectMake(0, movementDistance,self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    [UIView commitAnimations];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
         [self animateTextField: nil up: NO distance:0];
        
    }
    
    return YES;
}
-(IBAction) OkPopup:(id)sender{
    [popupView removeFromSuperview];
    txtShippingAddress.text=@"";
    txtEmail.text=@"";
    txtName.text=@"";
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:) ];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
