//
//  BaseViewController.h
//  POS
//
//  Created by admin on 28/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataStoreManager.h"
#import "DataStoreManager_WebServices.h"

@interface BaseViewController : UIViewController {
	DataStoreManager *dataStoreManager;
}


-(void)showNextView;

@end
