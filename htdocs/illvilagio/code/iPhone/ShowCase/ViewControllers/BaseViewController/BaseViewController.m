//
//  BaseViewController.m
//  POS
//
//  Created by admin on 28/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BaseViewController.h"
#import "Defines.h"

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self.navigationController setNavigationBarHidden:YES animated:NO];
	
	dataStoreManager = [DataStoreManager manager];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(datas) name:kNotificationUserRequireReLogin object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


-(void)showNextView {
}

#pragma mark -
#pragma mark Private Helper Functions
-(void)userNeedRelogin {
    
	[self.navigationController popToRootViewControllerAnimated:YES];
}
@end
