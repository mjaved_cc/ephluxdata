//
//  EventDetailViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/29/13.
//
//

#import <UIKit/UIKit.h>
#import "PageControl.h"

@interface EventDetailViewController : UIViewController<UIScrollViewDelegate,PageControlDelegate>
{
    PageControl             *pageControl;
    NSMutableArray*         imagesArray;
    
    IBOutlet UIButton       *btnReserver;
    IBOutlet UIView         *imageBackgroundView;
    IBOutlet UIScrollView   *imageScrollView;
    IBOutlet UIScrollView   *mainScrollView;
    IBOutlet UILabel        *lbltitle;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblDis;
    IBOutlet UILabel        *lblArchive;
    IBOutlet UILabel        *lblParticapte_Desc;
    IBOutlet UIView         *shareView;
    IBOutlet UILabel        *lblHowTo;
    IBOutlet UILabel       *lblNotAvilable;
     BOOL popupTrue;
    
}
@property (nonatomic,strong)NSMutableDictionary* detailDic;
@property (nonatomic,assign)BOOL isCurrent;
-(IBAction)OnReserverClick:(id)sender;
@end
