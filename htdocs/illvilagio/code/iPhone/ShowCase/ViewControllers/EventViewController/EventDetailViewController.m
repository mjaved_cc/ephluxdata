//
//  EventDetailViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/29/13.
//
//

#import "EventDetailViewController.h"
#import "ReservationFormVC.h"
#import "AsyncImageView.h"
#import "ShareViewController.h"
@interface EventDetailViewController ()

@end

@implementation EventDetailViewController
@synthesize detailDic,isCurrent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Event Detail",@"Event Detail");
        [Functions setNavigationBar];
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:shareView];
    [shareView setHidden:YES];
    popupTrue = TRUE;
    lblHowTo.text=NSLocalizedString(@"How to Participate", @"How to Participate");
    [lblArchive setText:NSLocalizedString(@"Archive Event", @"Archive Event")];
    [lblNotAvilable setText:NSLocalizedString(@"Not Available", @"Not Available")];
    [self setBackBarButton];
    
    // someButton=nil;
    UIButton *shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 46, 44)];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"btn_share_normal.png"] forState:UIControlStateNormal];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"btn_share_pressed.png"] forState:UIControlStateSelected];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"btn_share_pressed.png"] forState:UIControlStateHighlighted];
    
    [shareButton addTarget:self action:@selector(OnShareClick:) forControlEvents:UIControlEventTouchUpInside];
    shareButton.tag=1;
    UIBarButtonItem* rightbutton = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    [self.navigationItem setRightBarButtonItem:rightbutton];
    if([lang isEqualToString:@"ar"]){
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_normal_ar.png"] forState:UIControlStateNormal];
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_active_ar.png"] forState:UIControlStateSelected];
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_active_ar.png"] forState:UIControlStateHighlighted];

    }else{
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_normal.png"] forState:UIControlStateNormal];
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_active.png"] forState:UIControlStateSelected];
        [btnReserver setBackgroundImage:[UIImage imageNamed:@"btn_reserve_active.png"] forState:UIControlStateHighlighted];

        
    }
    imagesArray=[[NSMutableArray alloc]init];
    imagesArray=[detailDic objectForKey:@"images"];
    
    lbltitle.text=[detailDic objectForKey:@"title"];
    lblDate.text= [Functions getLocalizedDate:[Functions dateFormate:[detailDic objectForKey:@"event_time"]]];
    lblDis.text=[detailDic objectForKey:@"description"];
    lblParticapte_Desc.text=[detailDic objectForKey:@"particapte_description"];
    [lblParticapte_Desc sizeToFit];
    
    if(self.isCurrent){
        [btnReserver setHidden:NO];
        [lblArchive setHidden:YES];
    }else{
        
        [btnReserver setHidden:YES];
        [lblArchive setHidden:NO];
    }
    [self CreateImageView];
    imageBackgroundView.layer.cornerRadius = 5;
    imageBackgroundView.layer.masksToBounds = YES;
    imageBackgroundView.layer.borderWidth = 0.5f;
    imageBackgroundView.layer.borderColor = [UIColor grayColor].CGColor;
    if([Functions isIPad]){
        
    }else{
        
        
        
    }if(IS_IPHONE5 ){
        mainScrollView.contentSize=CGSizeMake(320, 850);
        
    }else{
        mainScrollView.contentSize=CGSizeMake(320, 900);
    }
}
-(void)viewDidAppear:(BOOL)animated{
    
}
-(IBAction)showPopup
{
    
    
    if(popupTrue == TRUE)
    {
        popupTrue = FALSE;
        [shareView setHidden:NO];
        [self.view bringSubviewToFront:shareView];
        [self fadeIn:@"id" finished:NO target:shareView];
        
    }
    else
    {
        // [viewForTable setHidden:YES];
        //  [self.view sendSubviewToBack:viewForTable];
        popupTrue = TRUE;
        [self fadeOut:@"id" finished:YES target:shareView];
        
    }
    
}

-(IBAction)OnReserverClick:(id)sender{
    ReservationFormVC* reserverVC;
    if([Functions isIPad]){
        btnReserver.selected=YES;
        reserverVC=[[ReservationFormVC alloc]initWithNibName:@"ipad_ReservationFormVC" bundle:nil];
        reserverVC.eventDate=detailDic;
        [self.navigationController pushViewController:reserverVC animated:YES];
    }else{
        btnReserver.selected=YES;
        reserverVC=[[ReservationFormVC alloc]initWithNibName:@"ReservationFormVC" bundle:nil];
        reserverVC.eventDate=detailDic;
        [self.navigationController pushViewController:reserverVC animated:YES];
    }
    
}
-(void) OnShareClick:(id)sender{
    ShareViewController* shareVC;
    if([Functions isIPad]){
        shareVC=[[ShareViewController alloc]initWithNibName:@"ipad_ShareViewController" bundle:nil];
    }else{
    shareVC=[[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
    }
    shareVC.detailDic=detailDic;
    [self.navigationController pushViewController:shareVC animated:YES];
    
}
- (void)CreateImageView {
    
    int numberOfViews=[imagesArray count];
    //imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0,280,200)];
    imageScrollView.delegate = self;
    [imageScrollView setBackgroundColor:[UIColor clearColor]];
	imageScrollView.pagingEnabled = YES;
    UIView *viewFrame=nil;
	for (int i = 0; i <[imagesArray count]; i++) {
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        viewFrame = [[UIView alloc] initWithFrame:CGRectMake(imageScrollView.frame.size.width * i, 0, imageScrollView.frame.size.width, imageScrollView.frame.size.height)];
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=[[imagesArray objectAtIndex:i] objectForKey:@"image_url"];
        
        //NSString *stImageURL=@"http://playtimeandparty.com/wp-content/uploads/2013/03/breakfast-pizza.png";
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame.backgroundColor = [UIColor clearColor];
		[imageScrollView addSubview:viewFrame];
    }
    [imageScrollView setBounces:NO];
    imageScrollView.contentSize = CGSizeMake(imageScrollView.frame.size.width * numberOfViews, imageScrollView.frame.size.height);
    [mainScrollView addSubview:imageScrollView];
    
    pageControl = [[PageControl alloc] init ];
    if([Functions isIPad]){
        pageControl.frame = CGRectMake(130,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }else{
        pageControl.frame = CGRectMake(0,imageScrollView.frame.origin.y+viewFrame.frame.size.height,320.0,15.0);
    }
    
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.delegate = self;
    pageControl.numberOfPages = numberOfViews;
    //pageControl.currentPage = myViewTag;
    if([Functions isIPad]){
        
        [self.view addSubview:pageControl];
        [self.view bringSubviewToFront:pageControl];
        
    }else{
        [mainScrollView addSubview:pageControl];
    }
    
    
    
}

#pragma mark Scrollview and PageControll Delegate
- (void) scrollViewDidScroll: (UIScrollView *) aScrollView
{
	CGPoint offset = aScrollView.contentOffset;
    
	pageControl.currentPage = offset.x / imageScrollView.frame.size.width;
    //NSLog(@"%d ",pageControl.currentPage);
    //[self setTitle_:pageControl.currentPage];
}
-(void) setTitle_:(int)pageNo{
    //NSLog(@"Last %d , page no %d ",lastPageNo,pageNo);
    
    //
    //    if(pageNo==0 ||pageNo==1 ){
    //        lblTitle.text=@";
    //      }
}
- (void)pageControlPageDidChange:(PageControl *)pageControler{
    int page = pageControler.currentPage;
    
    CGRect frame = imageScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [imageScrollView scrollRectToVisible:frame animated:YES];
}
#pragma mark ANIMATION

//-------fade in

- (void)fadeIn:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    [target setAlpha:1.0f];
    [UIView commitAnimations];
}

- (void)fadeOut:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    if ([target alpha] == 1.0f)
        [target setAlpha:0.0f];
    else
        [target setAlpha:1.0f];
    [UIView commitAnimations];
}

- (void)fadeOutSecond:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    [target setAlpha:0.0f];
    
    [UIView commitAnimations];
}



-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) viewDidDisappear:(BOOL)animated{
    btnReserver.selected=NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
