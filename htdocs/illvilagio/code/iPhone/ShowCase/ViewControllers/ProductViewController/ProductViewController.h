//
// Copyright (c) 2010-2011 René Sprotte, Provideal GmbH
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "MMGridView.h"
#import "Category.h"
#import "OptionsDropDown.h"
#import "PageControl.h"
@class MBProgressHUD;

@interface ProductViewController : UIViewController<MMGridViewDataSource, MMGridViewDelegate,OptionsDropDownDelegate,PageControlDelegate>

{
    OptionsDropDown *dropDown;
    IBOutlet UIButton *btnSorting;
     Category *catArrayDidSelect;
    NSString *currencySymbol;
    IBOutlet MMGridView *gridView;
    //IBOutlet UIPageControl *pageControl;

    IBOutlet UIButton *btnOrderQty;
    IBOutlet UILabel *lblheading;
    
    IBOutlet UIImageView *preDesignImage;
    IBOutlet UIImageView *postDesignImage;
    PageControl             *pageControl;
    
    UIView* bgView;
    NSString* deviceName;
    NSMutableDictionary* dicSeatsQuantities;
    UILabel* lblPriceValue;
    UILabel* lblTotalValue;
    UILabel* lblTotalPerItem;
    UIButton* btnSwitch;
    UIButton* btnID;
    UIButton* btnAddOrder;
    int backEndID;
    int forEveryOneValue;
    int pricePer;
    int noOfQut;
    BOOL isForEveryOne;
    NSString* singleOrderValue;
    NSString* multipalOrderValue;

}
@property (nonatomic, retain) MBProgressHUD *progressHud;
@property(nonatomic,retain)Category *category;
@property (nonatomic,retain) MMGridView *gridView;
@property(nonatomic,retain)NSMutableArray *categoryItems;
@property (nonatomic,retain) IBOutlet UILabel *lblheading;
@property (nonatomic,assign) NSString *navTitle;

- (id)initWithNibNameAndFrame:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil frame:(CGRect)frame;

-(IBAction)actionBack:(id)sender;
-(IBAction)goHome:(id)sender;
-(IBAction)actionSorting:(id)sender;
-(IBAction)actionRefresh:(id)sender;

-(IBAction)actionOrderlist:(id)sender;
-(void)setOrderQuantity;

@end
