//
//  MyRightViewController.m
//  MMDrawerControllerKitchenSink
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "MyRightViewController.h"
//#import "MMSideDrawerTableViewCell.h"
//#import "MMSideDrawerSectionHeaderView.h"
#import "CenterViewController.h"


@interface MyRightViewController ()

@end

@implementation MyRightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton* leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 100, 50, 50)];
    leftBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [leftBtn setTitle:@"click" forState:UIControlStateNormal];
    
    [leftBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:leftBtn];
    
    [self setupLeftMenuButton];
    [self setupRightMenuButton];
    
    [self.navigationController.navigationBar setTintColor:[UIColor
                                                           colorWithRed:78.0/255.0
                                                           green:156.0/255.0
                                                           blue:206.0/255.0
                                                           alpha:1.0]];
    
    [self.navigationController.view.layer setCornerRadius:10.0f];

       

}
-(void)setupLeftMenuButton{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
}
-(IBAction) buttonAction:(id)sender{
   CenterViewController * center = [[CenterViewController alloc] init];
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:center];
    
    
    [self.mm_drawerController
     setCenterViewController:nav
     withCloseAnimation:YES
     completion:nil];
    
}
-(IBAction)NextViewController:(id)sender
{
//    NextViewController* VC=[[NextViewController alloc]init];
//    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:VC];
//    [self.mm_drawerController
//     setCenterViewController:nav
//     withCloseAnimation:YES
//     completion:nil];
//
   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
