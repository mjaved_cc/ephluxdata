//
//  GalleryViewController.h
//  SideViews
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 salman.ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageControl.h"
@interface GalleryViewController : UIViewController<UIScrollViewDelegate,PageControlDelegate,UIGestureRecognizerDelegate>
{
    PageControl             *pageControl;
    UIScrollView            *galleryScrollView;
    NSMutableArray          *imageURLArray;
    UIScrollView            *scrollView;
    NSInteger               numberOfViews;
    int                     currentIndex;
    IBOutlet    UIView      *largeGalleryView;
    IBOutlet    UIButton    *closeBtn;
    IBOutlet    UILabel     *lblNoGalleryAvailable;
}
@end
