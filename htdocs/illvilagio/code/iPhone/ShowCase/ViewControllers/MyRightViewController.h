//
//  MyRightViewController.h
//  MMDrawerControllerKitchenSink
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
@interface MyRightViewController : UIViewController

@end
