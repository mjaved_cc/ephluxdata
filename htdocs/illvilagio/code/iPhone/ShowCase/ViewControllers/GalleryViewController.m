//
//  GalleryViewController.m
//  SideViews
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 salman.ahmed. All rights reserved.
//

#import "GalleryViewController.h"
#import "AsyncImageView.h"
#import "GalleryDetailViewController.h"

#define imageWidth 155
#define viewHeight 117
#define viewWidth  130
#define ipad_ImageHeight 136
#define ipad_ImageWidth 157

@interface GalleryViewController ()

@end

@implementation GalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        //self.title = @"gallery";
        self.title = NSLocalizedString(@"gallery", @"gallery");
        [Functions setNavigationBar];
        imageURLArray=[[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    @try {
        [lblNoGalleryAvailable setText:NSLocalizedString(@"No Images Available", @"No Images Available")];
        
        //NSData *myEncodedObject2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"allinformation"];
        //NSDictionary *obj = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject2];
        imageURLArray=(NSMutableArray*)[Functions getDataFromNSUserDefault_Key:@"gallery"];
        //imageURLArray=[[[SingletonClass sharedInstance] allContentDic] objectForKey:@"gallery"];
        numberOfViews=[imageURLArray count];
        if(numberOfViews>0){
            [lblNoGalleryAvailable setHidden:YES];
            if([Functions isIPad]){
                [self ipad_CreateGalleryView];
            }else{
                [self CreateGalleryView];
            }
        }else{
            
        }
        
        if([Functions isIPad]){
            //[self ipad_CreateGalleryView];
        }else{
            //[self CreateGalleryView];
            [self setupLeftMenuButton];
            [self setupRightMenuButton];
            
        }
    }
    @catch (NSException *exception) {
        
    }
   
    
}
-(void)viewWillAppear:(BOOL)animated{
   // [largeGalleryView removeFromSuperview];
    
    
}
-(void) galleryViewNotificationFunction:(NSNotification*)notification{
    @try {
        
   
    NSDictionary *dict = notification.userInfo;
    imageURLArray=[[dict objectForKey:@"status"] objectForKey:@"gallery"];
    numberOfViews=[imageURLArray count];
    if(numberOfViews>0){
        [lblNoGalleryAvailable setHidden:YES];
        if([Functions isIPad]){
            [self ipad_CreateGalleryView];
        }else{
            [self CreateGalleryView];
        }
    }else{
        
    }
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma mark CreateGalleryView
-(void) CreateGalleryView{
    @try {
     galleryScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height)];
        
        [galleryScrollView setBackgroundColor:[UIColor clearColor]];
        
        int nextHeight=0;
        currentIndex=0;
        
        for(int k=0; k<((numberOfViews/2) +(numberOfViews%2));k++){
            
            for (int i = 0; ((i<2 && (k!=(numberOfViews/2))) || (i<1 && numberOfViews%2!=0 && k==(numberOfViews/2))) ; i++){
                
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5,5,viewWidth, viewHeight)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(13+(i*imageWidth),nextHeight,viewWidth, viewHeight)];
                UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"iphone_gallery_image_holder.png"]];
                imageView.frame=CGRectMake(0, 0, 140, 127);
                [viewFrame addSubview:imageView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                
                NSString *stImageURL=[[imageURLArray objectAtIndex:currentIndex] objectForKey:@"imageUrl"];
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                }
                [viewFrame addSubview:asynImageView];
                
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=currentIndex;
                viewFrame.backgroundColor = [UIColor greenColor];
                
                [galleryScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
            }
            nextHeight+=140;
        }
        
        galleryScrollView.contentSize = CGSizeMake(galleryScrollView.frame.size.width, nextHeight+150);
        [self.view addSubview:galleryScrollView];
    }@catch (NSException *exception) {
        
    }
    
    
}
#pragma mark ipad_CreateGalleryView
-(void) ipad_CreateGalleryView{
    @try {
        galleryScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,35, self.view.frame.size.width, self.view.frame.size.height)];
        [galleryScrollView setBackgroundColor:[UIColor clearColor]];
        
        int nextHeight=0;
        currentIndex=0;
        int distance=20;
        numberOfViews=[imageURLArray count];
        //NSLog(@"div %d  mode %d",numberOfViews/3, numberOfViews%3);
        for(int k=0; ((k<(numberOfViews/3) && numberOfViews%3==0) || (k<((numberOfViews/3)+1) && numberOfViews%3!=0));k++){
            for (int i = 0; ((i<3 && (k!=(numberOfViews/3))) || (i<numberOfViews%3 && numberOfViews%3!=0 && k==(numberOfViews/3))) ; i++){
                AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(5,5,ipad_ImageWidth-10, ipad_ImageHeight-11)];
                UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(distance+(i*ipad_ImageWidth),nextHeight,ipad_ImageWidth, ipad_ImageHeight)];
                UIImageView* imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"gallery_image_holder.png"]];
                imageView.frame=CGRectMake(0, 0, ipad_ImageWidth, ipad_ImageHeight);
                [viewFrame addSubview:imageView];
                asynImageView.backgroundColor = [UIColor clearColor];
                asynImageView.contentMode = UIViewContentModeScaleAspectFit;
                //NSLog(@" index  %d",currentIndex);
                NSString *stImageURL=[[imageURLArray objectAtIndex:currentIndex] objectForKey:@"imageUrl"];
                if( ![stImageURL isEqualToString:@""] )
                {
                    NSURL *imageURL = [NSURL URLWithString:stImageURL];
                    [asynImageView loadImageFromURL:imageURL];
                    //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                    
                }
                else
                {
                    //TODO: show default image.
                }
                [viewFrame addSubview:asynImageView];
                UITapGestureRecognizer * onTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
                [onTap setDelegate:self];
                [onTap setNumberOfTapsRequired:1];
                [viewFrame addGestureRecognizer:onTap];
                viewFrame.userInteractionEnabled = YES;
                viewFrame.tag=currentIndex;
                viewFrame.backgroundColor = [UIColor clearColor];
                
                [galleryScrollView addSubview:viewFrame];
                asynImageView=nil;
                viewFrame=nil;
                currentIndex++;
                distance=distance+20;
            }
            distance=20;
            nextHeight+=160;
        }
        
        galleryScrollView.contentSize = CGSizeMake(galleryScrollView.frame.size.width, nextHeight+110);
        [self.view addSubview:galleryScrollView];
    }@catch (NSException *exception) {
        
    }
}
- (void)oneTap:(UIGestureRecognizer *)gesture {
    @try {
        GalleryDetailViewController *galleryDetailVC;
        int myViewTag = gesture.view.tag;
        if([Functions isIPad]){
            galleryDetailVC=[[GalleryDetailViewController alloc]initWithNibName:@"ipad_GalleryDetailVC" bundle:nil];
            
        }else{
            galleryDetailVC=[[GalleryDetailViewController alloc]initWithNibName:@"GalleryDetailViewController" bundle:nil];
            
        }
        galleryDetailVC.tempDic=[imageURLArray objectAtIndex:myViewTag];
        galleryDetailVC.galleryArray=imageURLArray;
        galleryDetailVC.selectedIndex=myViewTag;
        [self.navigationController pushViewController:galleryDetailVC animated:YES];
        
    }@catch (NSException *exception) {
        
    }
}

- (void)Old_oneTap:(UIGestureRecognizer *)gesture {
    
    int myViewTag = gesture.view.tag;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 9.0,320,416)];
    scrollView.delegate = self;
    [scrollView setBackgroundColor:[UIColor clearColor]];
	scrollView.pagingEnabled = YES;
    
	for (int i = 0; i <numberOfViews; i++) {
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(10,10,300, 350)];
        UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(scrollView.frame.size.width * i, 0, 320, 370)];
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=[[imageURLArray objectAtIndex:i] objectForKey:@"image"];
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame.backgroundColor = [UIColor greenColor];
		[scrollView addSubview:viewFrame];
    }
    [scrollView setBounces:NO];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * numberOfViews, 416);
    [largeGalleryView addSubview:scrollView];
    
    pageControl = [[PageControl alloc] init ];
    pageControl.frame = CGRectMake(10.0,380.0,320.0,15.0);
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.delegate = self;
    pageControl.numberOfPages = numberOfViews;
    pageControl.currentPage = myViewTag;
    [largeGalleryView addSubview:pageControl];
    [self.view addSubview:largeGalleryView];
    [largeGalleryView bringSubviewToFront:closeBtn];
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * myViewTag;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    
}
#pragma mark RemoveLArgeGallery
-(IBAction)RemoveFromSuperView:(id)sender{
    [largeGalleryView removeFromSuperview];
}

#pragma mark Scrollview and PageControll Delegate
- (void) scrollViewDidScroll: (UIScrollView *) aScrollView
{
	CGPoint offset = aScrollView.contentOffset;
    
	pageControl.currentPage = offset.x / 320.0f;
    //[self setTitle_:pageControl.currentPage];
}
-(void) setTitle_:(int)pageNo{
    //NSLog(@"Last %d , page no %d ",lastPageNo,pageNo);
    
    //
    //    if(pageNo==0 ||pageNo==1 ){
    //        lblTitle.text=@";
    //      }
}
- (void)pageControlPageDidChange:(PageControl *)pageControler{
    int page = pageControler.currentPage;
    
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
