//
//  RootViewController.h
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuManager.h"
#import "AccordionView.h"
@interface RootViewControllerChild_iPhone : UIViewController
{
    MenuManager *menuManger;
    IBOutlet UIButton *btnOrderQty;
    UIImageView *thumbImage;
    NSArray* childArray;
    
    
    UIView* bgView;
    NSString* deviceName;
    NSMutableDictionary* dicSeatsQuantities;
    UILabel* lblPriceValue;
    UILabel* lblTotalValue;
    UILabel* lblTotalPerItem;
    UIButton* btnSwitch;
    UIButton* btnID;
    UIButton* btnAddOrder;
    int backEndID;
    int forEveryOneValue;
    int pricePer;
    int noOfQut;
    BOOL isForEveryOne;
    
    IBOutlet UILabel* lblNoOfGuest;
    IBOutlet UILabel* lblPopupTitle;
    IBOutlet UISlider* sliderPopup;
    IBOutlet UILabel* lblNumber;
    IBOutlet UILabel* lbDineIn;
    IBOutlet UIButton *btnAlertSwitch;
    IBOutlet UIButton *btnSearch;
    IBOutlet UILabel* lblBelowDineIn;
    IBOutlet UIView *addOrderView;
    IBOutlet UIView *innerOrderView;
    NSString* singleOrderValue;
    NSString* multipalOrderValue;
}

@property(nonatomic,retain)Category *category;
@property(nonatomic) int menuId;
@property(nonatomic) int catId;
@property(nonatomic,retain) NSArray* childArray;
@property(nonatomic,assign)BOOL *isOrderView;
//-(void)menuChanged:(id)sender ;
-(IBAction)actionBack:(id)sender;
-(UIScrollView*)menuChanged:(id)sender;
-(IBAction)actionOrderlist:(id)sender;
-(void)setOrderQuantity;
-(UIView*)productListinChanged:(id)sender;
-(void)OrderBtnOnclick:(id)sender;
-(void) setTotalValue:(int) items;


-(IBAction) OkPopup:(id)sender;
-(IBAction) addOrderPopup:(id)sender;
@end
