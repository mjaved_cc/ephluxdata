//
//  RootViewController.h
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuManager.h"

@interface RootViewController : UIViewController
{
    MenuManager *menuManger;
    IBOutlet UIButton *btnOrderQty;
}
-(void)menuChanged:(id)sender ;
-(IBAction)actionBack:(id)sender;

-(IBAction)actionOrderlist:(id)sender;
-(void)setOrderQuantity;

@end
