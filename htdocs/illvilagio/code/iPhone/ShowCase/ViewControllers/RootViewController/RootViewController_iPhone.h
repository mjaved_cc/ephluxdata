//
//  RootViewController.h
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuManager.h"
#import "AsyncImageView.h"
@interface RootViewController_iPhone : UIViewController<UISearchBarDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    MenuManager *menuManger;
     NSArray* catParentArray;
    UIButton *btnOrderQty;
    UIImageView *thumbImage;
    NSFetchedResultsController *searchResultController;
    IBOutlet UIView *viewForTable;
    IBOutlet UITableView *tblMenu;
    BOOL popupTrue;
    IBOutlet UIView *addOrderView;
    IBOutlet UIView *innerOrderView;
    IBOutlet UIScrollView *imageScroll;
    BOOL searchingCheck;
    NSArray *filteredArray;;
    NSMutableArray *filteredCOntent;
    UISearchBar *mySearch;
    int catorID;
    Category *catArrayDidSelect;
    NSArray *catArrayLocal;
    IBOutlet UILabel *lblcateMenu;
    NSIndexPath* checkedIndexPath;
    NSMutableArray *tickImage;
    IBOutlet UITextField *txtSearch;
    IBOutlet UITableView *tblSearch;
    
    IBOutlet UILabel* lblNoOfGuest;
    IBOutlet UILabel* lblPopupTitle;
    IBOutlet UISlider* sliderPopup;
    IBOutlet UILabel* lblNumber;
    IBOutlet UILabel* lbDineIn;
    IBOutlet UIButton *btnSwitch;
    IBOutlet UIButton *btnSearch;
    IBOutlet UILabel* lblBelowDineIn;
     IBOutlet UIButton *btnOk;
    UIView* layOutView;
    IBOutlet UIView* searchBarView;
}
@property(nonatomic,retain) NSMutableArray *filteredCOntent;
@property (nonatomic,retain) NSIndexPath* checkedIndexPath;
@property(nonatomic,retain) NSMutableArray *tickImage;
@property(nonatomic,retain)NSArray *filteredArray;
@property(nonatomic,assign)BOOL *isOrderView;
@property(nonatomic,assign)BOOL *isChangeOrder;

- (NSArray *) contactsMatchingName: (NSString *) fname SectionNo:(int)sec;
-(IBAction)back:(id)sender;
-(UIScrollView*)menuChanged:(id)sender;
-(IBAction)actionOrderlist:(id)sender;
-(void)setOrderQuantity;
- (void)fadeIn:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target;
-(IBAction)showPopup;
-(IBAction) OkPopup:(id)sender;
-(IBAction) addOrderPopup:(id)sender;
- (IBAction)fieldEditingBegan:(UITextField *)sender;
-(IBAction)actionSwitch:(id)sender;

@end
