//
//  RootViewController.m
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "RootViewControllerChild_iPhone.h"
#import "ProductViewController.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Category+Image.h"
#import "Functions.h"
#import "Defines.h"
#import "OrderListController.h"
#import "CategoryItem+Image.h"
#import "AnyViewController.h"
#import "AsyncImageView.h"
#import "AccordionView.h"


@implementation RootViewControllerChild_iPhone

@synthesize category;
@synthesize childArray;
@synthesize isOrderView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = langIsArabic?self.category.name_arabic:self.category.name;
        
        [Functions setNavigationBar];
        
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)meunTapped:(UITapGestureRecognizer *)gesture
{
    //
    
    UIImageView *theTappedImageView = (UIImageView *)gesture.view;
    NSInteger tag = theTappedImageView.tag;
}

-(void)imageTapped:(UITapGestureRecognizer *)gesture
{
    @try {
        //
        
        
        UILabel *theTappedLabel = (UILabel *)gesture.view;
        
        NSString *split = theTappedLabel.text;
        
        NSArray *arr = [split componentsSeparatedByString:@"||"];
        
        int menuId=0, backendID=0;
        
        if([arr count]>1){
            menuId = [[arr objectAtIndex:1] intValue];
            backendID = [[arr objectAtIndex:0] intValue];
        }else{
            
            menuId = [[arr objectAtIndex:0] intValue];
            
        }
        
        gesture.view.backgroundColor = [UIColor clearColor];
        
        CategoryItem *cItem = [[DataStoreManager manager] getCategoryItem:backendID];
        
        NSLog(@"%@",cItem);
        
        AnyViewController *c ;
        if([Functions isIPad]){
            c = [[AnyViewController alloc] initWithNibName:@"ipad_AnyViewController" bundle:nil];
            
        }else{
            c = [[AnyViewController alloc] initWithNibName:@"AnyViewController" bundle:nil];
            
        }
        c.detailArray=[[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] categoryItemsFor:[self.childArray objectAtIndex:menuId]]];
        c.category = [cItem valueForKey:@"category"];
        c.currentPageIndex=-1;
        c.currentCategoryItem=cItem;
        
        [self.navigationController pushViewController:c animated:YES];
        return;
        
        
        
        
    }@catch (NSException *exception) {
        
        NSLog(@"Exception %@",exception);
    }
    
    
}
-(void)refreshController:(NSNotification*)notification{
    
    NSLog(@"Notification %@",notification.userInfo);
}

-(void)setBackBarButton{
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    @try {
        
        
        [super viewDidLoad];
        forEveryOneValue=0.0;
        dicSeatsQuantities=[[NSMutableDictionary alloc]init];
        self.isOrderView=TRUE;

        self.title = langIsArabic?self.category.name_arabic:self.category.name;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshController:) name:@"refreshCategory" object:nil];
       
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuNotification:) name:@"changeMenu" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadedNotification:) name:kNotificationCategoryItemImageDownloaded object:nil];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddOrderNotification:) name:@"childVC" object:nil];
        
        [self setBackBarButton];
        
        //setup iphone button
        
        if(![Functions isIPad]){
            
            [self setupRightMenuButton];
        }
        
        
        /*******************************************UISCROLLVIEW*************************************************************/
        menuManger = [MenuManager menuManager];
        [menuManger loadMenu];
        
        /*******************************************UISCROLLVIEW*************************************************************/
        
        UIScrollView* imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        if([Functions isIPad ]){
            imageScroll.frame=CGRectMake(0, 0, 552, 786);
        }
        
        [imageScroll setScrollEnabled:YES];
        imageScroll.backgroundColor = [UIColor clearColor];
        [imageScroll setShowsHorizontalScrollIndicator:NO];
        //imageScroll.pagingEnabled = YES;
        
        for (UIView *v in imageScroll.subviews) {
            [v removeFromSuperview];
        }
        
        int menus=[childArray count];
        for (int j=0; j < menus; j++) {
            /*******************************************Menu***************************************************************/
            Category *childCategory=[childArray objectAtIndex:j];
            
            UIImageView *headerbarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2 + (53*j)+ (j*140), 320, 53)];
            
            headerbarView.image=[UIImage imageNamed:@"sub-heading-bar_small.png"];
            
            [headerbarView setBackgroundColor:[UIColor clearColor]];
            
            UILabel *headerbarViewLabel= [[ UILabel alloc] initWithFrame:CGRectMake(0, (53*j)+ (j*140), 320, 53)];
            
            if([Functions isIPad ]){
                headerbarView.frame=CGRectMake(0, 2 + (53*j)+ (j*170), 552, 53);
                headerbarViewLabel.frame=CGRectMake(0, (53*j)+ (j*170), 552, 53);
            }
            
            headerbarViewLabel.text = langIsArabic?childCategory.name_arabic:childCategory.name;
            
            CGSize txtSz = [headerbarViewLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 18]];
            
            headerbarViewLabel.frame = CGRectMake(headerbarViewLabel.frame.size.width/2 - txtSz.width/2, headerbarViewLabel.frame.origin.y, txtSz.width+10, headerbarViewLabel.frame.size.height);
            
            [headerbarViewLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
            headerbarViewLabel.textColor = UIColorFromRGB(0x5b1900);
            headerbarViewLabel.backgroundColor = [UIColor clearColor];
            headerbarViewLabel.textAlignment = UITextAlignmentCenter;
            
            
            UIImageView *preDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
            
            UIImageView *postDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
            
            postDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x+headerbarViewLabel.frame.size.width+10,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
            
            
            [imageScroll addSubview:headerbarView];
            [imageScroll addSubview:headerbarViewLabel];
            [imageScroll addSubview:postDesignImage];
            
            preDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x-20,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
            
            [imageScroll addSubview:preDesignImage];
            
            [imageScroll addSubview:[self productListinChanged:[NSNumber numberWithInt:j]]];
            
        }
        
        
        if([Functions isIPad]){
            
            imageScroll.contentSize = CGSizeMake(320, 250*menus);
            
        }else{
            if(IS_IPHONE5){
                imageScroll.contentSize = CGSizeMake(320, 195*menus);
                
            }else{
                imageScroll.contentSize = CGSizeMake(320, 210*menus);
                
            }
            
        }
        imageScroll.userInteractionEnabled=YES;
        imageScroll.delaysContentTouches=YES;
        [imageScroll setExclusiveTouch:YES];
        [self.view addSubview:imageScroll];
        
    }
    
    @catch (NSException *exception) {
        NSLog(@"ViewDidLoad %@",exception);
    }
    
    // Do any additional setup after loading the view from its nib.
}


-(void)imageDownloadedNotification:(NSNotification *) sender{
    
    
    //
    //    NSInteger tag=[[sender.userInfo valueForKey:@"tag"] intValue];
    //
    //    UIImageView *originalImageView = (UIImageView *)[self.view viewWithTag:tag];
    //
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //
    //    NSMutableString* str = [[NSMutableString alloc] initWithCapacity:300];
    //    [str appendString:documentsDirectory];
    //    [str appendString:@"/CategoryItems/"];
    //    [str appendString:[[sender.userInfo valueForKey:@"object"] valueForKey:@"imageLocalPath"]];
    //
    //
    //    NSFileManager *fileManager = [NSFileManager defaultManager];
    //    BOOL success = [fileManager fileExistsAtPath:str];
    //
    //    UIImage* image = nil;
    //
    //    if(!success)
    //    {
    //
    //    }
    //    else
    //    {
    //
    //    }
    
}

-(UIScrollView*)menuChanged:(id)sender {
    @try {
        
        NSArray *catArray=nil;
        
        if ([sender isKindOfClass:[NSString class]]){
            catArray=[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:[sender intValue]]:0];
        }else{
            catArray=[[DataStoreManager manager] categoriesFor:[[menuManger menus] objectAtIndex:[sender tag]]];
        }
        
        menuManger.categories=catArray;
        
        int count=1;//[catArray count];
        if (count>0) {
            /*******************************************UIScrollView****************************************************************/
            UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (53*([sender intValue]+1))+([sender intValue]*140) , 320, 200)];
            [scrollView setScrollEnabled:YES];
            
            scrollView.backgroundColor = [UIColor clearColor];
            
            [scrollView setShowsHorizontalScrollIndicator:NO];
            for (UIView *v in scrollView.subviews) {
                [v removeFromSuperview];
            }
            for (int aantal=0; aantal < count; aantal++) {
                /*******************************************Category****************************************************************/
                Category *categ=[catArray objectAtIndex:aantal];
                
                //Either Download Image Here or In CategoryParser.Which One Suits You.
                
                NSString *fileName = [Functions localImageThumbName:categ.imageURL backendID:[categ.backendID intValue]];
                NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    [categ startDownloadThumbImage:[[NSString stringWithFormat:@"%d000%d",aantal+1,[sender intValue]] intValue]];
                }
                
                
                thumbImage = [[UIImageView alloc] initWithFrame:CGRectMake(3+(aantal*200), 2, 195, 120)];
                thumbImage.backgroundColor = [UIColor clearColor];
                thumbImage.image=[categ thumbImage];
                
                thumbImage.tag= [[NSString stringWithFormat:@"%d000%d",aantal+1,[sender intValue]] intValue];
                
                //NSLog(@"%d",thumbImage.tag);
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
                tap.numberOfTapsRequired = 1;
                tap.cancelsTouchesInView=YES;
                thumbImage.userInteractionEnabled = YES;
                [thumbImage addGestureRecognizer:tap];
                [scrollView addSubview:thumbImage];
                
                /*******************************************UITapGestureRecognizer****************************************************/
                
                UILabel *scoreLabel = [ [UILabel alloc ] init];
                [scoreLabel setFrame:CGRectMake(3+(aantal*200), CGRectGetMaxY(thumbImage.frame)-40, 195, 30)];
                scoreLabel.textAlignment =  UITextAlignmentCenter;
                scoreLabel.textColor = [UIColor darkGrayColor];
                scoreLabel.numberOfLines=0;
                scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
                scoreLabel.backgroundColor = [UIColor whiteColor];
                scoreLabel.alpha = 0.8;
                [scoreLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];

                scoreLabel.text = langIsArabic?categ.name_arabic:categ.name;
                
                [scrollView addSubview:scoreLabel];
                [scrollView bringSubviewToFront:scoreLabel];
                //thumbImage=nil;
            }
            scrollView.contentSize = CGSizeMake(195*count, 130);
            scrollView.userInteractionEnabled=YES;
            scrollView.delaysContentTouches=NO;
            //[self.view addSubview:scrollView];
            return scrollView;
            
            
        }else{
            NSString* alertTitle=NSLocalizedString(@"No Items Found", @"No Items Found");
            NSString* alertMsg=NSLocalizedString(@"There is no items attached to this menu", @"There is no items attached to this menu");
            
            [Functions showAlert:alertTitle message:alertMsg];
            return nil;
        }
        
    }
    @catch (NSException *exception) {
        NSString* alertTitle=NSLocalizedString(@"No Items", @"No Items");
        NSString* alertMsg=NSLocalizedString(@"Sorry no items are there in this category",@"Sorry no items are there in this category");
        [Functions showAlert:alertTitle message:alertMsg];
    }
}

-(UIView*)productListinChanged:(id)sender {
    @try {
        UIView* tempFrame;
        NSArray *cItems;
        
        if ([[self.childArray objectAtIndex:[sender intValue]] isKindOfClass:[Category class]]){
            
            cItems = [[NSMutableArray alloc] initWithArray:[[DataStoreManager manager] categoryItemsFor:[self.childArray objectAtIndex:[sender intValue]]]];
            
        }
        
        
        
        int count=[cItems count];
        if (count>0) {
            menuManger.categories=cItems;
            /*******************************************UIScrollView****************************************************************/
            UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (53*([sender intValue]+1))+([sender intValue]*140) , 320, 200)];
            if([Functions isIPad]){
                scrollView.frame=CGRectMake(0,20+(53*([sender intValue]+1))+([sender intValue]*170), 552, 200);
            }
            [scrollView setScrollEnabled:YES];
            
            scrollView.backgroundColor = [UIColor clearColor];
            
            [scrollView setShowsHorizontalScrollIndicator:NO];
            for (UIView *v in scrollView.subviews) {
                [v removeFromSuperview];
            }
            for (int aantal=0; aantal < count; aantal++) {
                /*******************************************Category****************************************************************/
                CategoryItem *citem=[cItems objectAtIndex:aantal];
                
                //Either Download Image Here or In CategoryParser.Which One Suits You.
                
                NSString *fileName = [Functions localImageThumbName:citem.imageURL backendID:[citem.backendID intValue]];
                NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    
                    //[categoryItem startDownloadThumbImage]
                    
                    [citem startDownloadThumbImage:[[NSString stringWithFormat:@"%d000%d",aantal+1,[sender intValue]] intValue]];
                }
                
                
                UIImageView *imagHolder = [[UIImageView alloc] initWithFrame:CGRectMake(3+(aantal*200), 2, 195, 122)];
                imagHolder.image = [UIImage imageNamed:@"menu_image_holder.png"];
                
                tempFrame=[[UIView alloc]init];
                tempFrame.frame=CGRectMake(0+(aantal*200), 2, 195, 122);
                imagHolder.frame= CGRectMake(0, 0, 195, 122);
                if([Functions isIPad]){
                    tempFrame.frame=CGRectMake(0+(aantal*200), 2, 195, 122);
                    imagHolder.frame= CGRectMake(0, 0, 195, 122);
                }
                
                [tempFrame setBackgroundColor:[UIColor yellowColor]];
                [tempFrame addSubview:imagHolder];
                
                
                if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    
                    thumbImage = [[UIImageView alloc] initWithFrame:CGRectMake(5,5, 184, 112)];
                    
                    if([Functions isIPad]){
                        thumbImage.frame= CGRectMake(5,5, 184, 112);
                    }
                    thumbImage.image=[citem thumbImage];
                    [tempFrame addSubview:thumbImage];
                    //NSLog(@"image  %@",[tempFrame subviews]);
                }else{
                    
                    AsyncImageView* asynImageView=[[AsyncImageView alloc]init];
                    asynImageView.frame= CGRectMake(5,5, 184, 112);
                    if([Functions isIPad]){
                        imagHolder.frame= CGRectMake(0, 0, 195, 122);
                        asynImageView.frame= CGRectMake(5,5, 185, 112);
                    }
                    NSString *stImageURL=citem.imageURL;
                    
                    if( ![stImageURL isEqualToString:@""] )
                    {
                        NSURL *imageURL = [NSURL URLWithString:stImageURL];
                        [asynImageView loadImageFromURL:imageURL];
                        //[thumbImage addSubview:asynImageView];
                        [tempFrame addSubview:asynImageView];
                        
                        
                    }
                    else
                    {
                        //TODO: show default image.
                    }
                }
                
                
                UILabel *labelValue = [[UILabel alloc] init];
                labelValue.alpha=1;
                labelValue.textColor = [UIColor clearColor];
                labelValue.text=[NSString stringWithFormat:@"%@||%d",[citem backendItemID],[sender intValue]];
                labelValue.backgroundColor = [UIColor clearColor];
                labelValue.frame = tempFrame.frame;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
                tap.numberOfTapsRequired = 1;
                tap.cancelsTouchesInView=YES;
                labelValue.userInteractionEnabled = YES;
                [labelValue addGestureRecognizer:tap];
                
                
                UILabel *scoreLabel = [ [UILabel alloc ] init];
                
                NSString *scoreLabelText= langIsArabic?citem.name_arabic:citem.name;
                scoreLabel.text = [NSString stringWithFormat:@"  %@ ",scoreLabelText];
                CGSize txtSz = [scoreLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 16]];
                
                scoreLabel.textAlignment =  UITextAlignmentLeft;
                scoreLabel.textColor = [UIColor darkGrayColor];
                scoreLabel.numberOfLines=0;
                //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
                scoreLabel.backgroundColor = [UIColor whiteColor];
                scoreLabel.alpha = 0.8;
                [scoreLabel setFont:[UIFont fontWithName:@"Segoe UI" size:16]];
                
                UILabel *priceLabel = [[UILabel alloc ] init];
                priceLabel.text =[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",[citem.price intValue]]];
                
                CGSize txtSzPrice = [priceLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 14]];
                
                priceLabel.textAlignment =  UITextAlignmentCenter;
                priceLabel.textColor = UIColorFromRGB(0x5b1900);
                priceLabel.numberOfLines=0;
                //scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
                priceLabel.backgroundColor = [UIColor whiteColor];
                priceLabel.alpha = 0.8;
                [priceLabel setFont:[UIFont fontWithName:@"Segoe UI" size:14]];
                
                UIButton *addOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [addOrderBtn setUserInteractionEnabled:YES];
                [addOrderBtn addTarget:self action:@selector(actionOrderAdd:) forControlEvents:UIControlEventTouchUpInside];
                
                [addOrderBtn setImage:[UIImage imageNamed:@"btn_add_normal.png"] forState:UIControlStateNormal];
                [addOrderBtn setImage:[UIImage imageNamed:@"btn_add_pressed.png"] forState:UIControlStateSelected];
                addOrderBtn.alpha=0.9;
                [addOrderBtn setShowsTouchWhenHighlighted:YES];
                //[addOrderBtn setReversesTitleShadowWhenHighlighted:YES];
                [addOrderBtn setTag:[citem.backendItemID intValue]];
                [addOrderBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
                [addOrderBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
                
                scoreLabel.frame = CGRectMake(2,tempFrame.frame.size.height-48, txtSz.width, txtSz.height);
                priceLabel.frame = CGRectMake(2,tempFrame.frame.size.height-25, txtSzPrice.width, txtSzPrice.height);
                [addOrderBtn setFrame:CGRectMake(tempFrame.frame.origin.x+tempFrame.frame.size.width-35, 5, 30, 30)];
                
                if([Functions isIPad]){
                    
                    scoreLabel.frame = CGRectMake(2,tempFrame.frame.size.height-48, txtSz.width, txtSz.height);
                    priceLabel.frame = CGRectMake(2,tempFrame.frame.size.height-25, txtSzPrice.width, txtSzPrice.height);
                    [addOrderBtn setFrame:CGRectMake(tempFrame.frame.origin.x+tempFrame.frame.size.width-35, 5, 30, 30)];
                    
                }
                
                [tempFrame addSubview:scoreLabel];
                [tempFrame bringSubviewToFront:scoreLabel];
                [tempFrame addSubview:priceLabel];
                [tempFrame bringSubviewToFront:priceLabel];
                //[tempFrame addSubview:addOrderBtn];
                
                
                [scrollView addSubview:tempFrame];
                [scrollView addSubview:labelValue];
                [scrollView addSubview:addOrderBtn];
                scoreLabel = nil;
                priceLabel = nil;
                addOrderBtn=nil;
                
            }
            
            scrollView.contentSize = CGSizeMake(200*count, 130);
            scrollView.userInteractionEnabled=YES;
            scrollView.delaysContentTouches=NO;
            //[self.view addSubview:scrollView];
            return scrollView;
            
        }else{
          
            
            UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, (53*([sender intValue]+1))+([sender intValue]*140) , 320, 200)];
            if([Functions isIPad]){
                view.frame=CGRectMake(0,20+(53*([sender intValue]+1))+([sender intValue]*170), 552, 200);
            }
            view.backgroundColor = [UIColor clearColor];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
            [lbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
            lbl.text = NSLocalizedString(@"No Items Available",@"No Items Available");
            lbl.textColor = [UIColor grayColor];
            lbl.backgroundColor=[UIColor clearColor];
            lbl.textAlignment =UITextAlignmentCenter;
            [view addSubview:lbl];
            return view;
        }
        
    }
    @catch (NSException *exception) {
        NSString* alertTitle=NSLocalizedString(@"No Items", @"No Items");
        NSString* alertMsg=NSLocalizedString(@"Sorry no items are there in this category",@"Sorry no items are there in this category");
        [Functions showAlert:alertTitle message:alertMsg];
    }
}

#pragma mark AddOrderNotification

- (void)AddOrderNotification:(NSNotification *)notification{

    [self createSeats:[[SingletonClass sharedInstance] numberOfPeople]];
    
}
- (void)menuNotification:(NSNotification *)notification{
    
    [self menuChanged:notification.object];
    
}
- (void)viewDidUnload
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCategoryImageDownloaded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeMenu" object:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [self setOrderQuantity];
    
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    //self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
	
}

-(IBAction)actionBack:(id)sender{
    
    
    /* CATransition *animation=[CATransition animation];
     [animation setDelegate:self];
     [animation setDuration:0.75];
     [animation setType:@"genieEffect"];
     [animation setFillMode:kCAFillModeRemoved];
     
     animation.endProgress=0.99;
     [animation setRemovedOnCompletion:YES];
     [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    [[self navigationController] popViewControllerAnimated:YES];
}


-(IBAction)actionOrderlist:(id)sender{
    @try {
       
        OrderListController *controller=[[OrderListController alloc] initWithNibName:@"OrderListController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"OrderList");
    }
    
    
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}


#pragma mark-
#pragma mark-Setting Order Quantity
-(void)setOrderQuantity{
    @try {
        Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
        int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
        [btnOrderQty setTitle:[NSString stringWithFormat:@"%d",qtyTotal] forState:UIControlStateNormal];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}

-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) RemoveView:(id)sender{
    [self fadeOut:bgView withDuration:1];
    //[bgView removeFromSuperview];
}
- (void)fadeOut:(UIView*)view withDuration:(CFTimeInterval)duration {
	[UIView beginAnimations:kFadeKey context:nil];
	[UIView setAnimationDuration:duration];
	view.alpha = 0.0;
	[UIView commitAnimations];
    [bgView removeFromSuperview];
}
#pragma mark Creat Order View
-(void) createSeats:(int) noOfseats{
    multipalOrderValue=[NSString stringWithFormat:@"0"];
    singleOrderValue=[NSString stringWithFormat:@"0"];
   
    [dicSeatsQuantities removeAllObjects];
     [dicSeatsQuantities setValue:@"0" forKey:@"1"];
    isForEveryOne=TRUE;
    UIView* bgFrame=[[UIView alloc]initWithFrame:CGRectMake(90, 350, 395,317 )];//395 × 317 pixels
    UIImageView* imageBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 395, 317)];
    [imageBg setImage:[UIImage imageNamed:@"add_order_popup_bg.png"]];
    
    int tempHeight=0;
    float screenHeight= [[UIScreen mainScreen] bounds].size.height;
    float screenWidth= [[UIScreen mainScreen] bounds].size.width;
    
    if([Functions isIPad]){
        bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 552, screenWidth)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        bgFrame.frame=CGRectMake(130, screenWidth-410, 276,344 );
        imageBg.frame=CGRectMake(10, 0, 276,344 );
        [imageBg setImage:[UIImage imageNamed:@"iphone_add_order_popup_bg.png"]];
        deviceName=@"iphone_";
        tempHeight=45;
    }else{
        
        
        bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,320,screenHeight)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        bgFrame.frame=CGRectMake(10, screenHeight-410, 276,344 );
        imageBg.frame=CGRectMake(10, 0, 276,344 );
        [imageBg setImage:[UIImage imageNamed:@"iphone_add_order_popup_bg.png"]];
        deviceName=@"iphone_";
        tempHeight=45;
    }
    
    UIImageView* lightGrayImageView=[[UIImageView alloc]
                                     init];
    [lightGrayImageView setBackgroundColor:[UIColor blackColor]];
    [lightGrayImageView setAlpha:0.8];
    lightGrayImageView.frame=bgView.frame;
    [bgView addSubview:lightGrayImageView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(RemoveView:)
     forControlEvents:UIControlEventTouchUpInside];
    if([lang isEqualToString:@"ar"]){
       [ button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_normal_ar.png"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed_ar.png"] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed_ar.png"] forState:UIControlStateHighlighted];

    }else{
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_normal@2x.png"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed.png"] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageNamed:@"btn_popup_cancel_pressed.png"] forState:UIControlStateHighlighted];
        
    }

   
    button.frame = CGRectMake(bgFrame.frame.origin.x+20,bgFrame.frame.origin.y+13, 66, 25);
    
    btnSwitch = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSwitch addTarget:self
                  action:@selector(onSwitch:)
        forControlEvents:UIControlEventTouchUpInside];
    [btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_off.png"] forState:UIControlStateNormal];
    [btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_on.png"] forState:UIControlStateSelected];
    //[btnSwitch setBackgroundImage:[UIImage imageNamed:@"switch_people_off.png"] forState:UIControlStateHighlighted];
    btnSwitch.frame = CGRectMake(bgFrame.frame.origin.x+bgFrame.frame.size.width-60,bgFrame.frame.origin.y+10, 55, 29.0);
    
    if(noOfseats==1) {
        btnSwitch.selected=TRUE;
        isForEveryOne=FALSE;
    }
    UILabel* lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+button.frame.size.width+20, bgFrame.frame.origin.y+15, bgFrame.frame.size.width-140, 20.0)];
    [lblTitle setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
    lblTitle.textColor = UIColorFromRGB(0x5b1900);
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    lblTitle.text=NSLocalizedString(@"Place to Order",@"Place to Order");
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    
    UILabel* lblPrice=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+20,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 100.0, 12.0)];
    [lblPrice setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblPrice.textColor = [UIColor grayColor];
    [lblPrice setBackgroundColor:[UIColor clearColor]];
    lblPrice.text=NSLocalizedString( @"Price per Serving", @"Price per Serving");    
    UILabel* lblTotal=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+ bgFrame.frame.size.width-80,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 100.0, 12.0)];
    [lblTotal setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblTotal.textColor = [UIColor grayColor];
    [lblTotal setBackgroundColor:[UIColor clearColor]];
    lblTotal.text=NSLocalizedString(@"Total", @"Total");
    
    lblTotalPerItem=[[UILabel alloc]initWithFrame:CGRectMake(bgFrame.frame.origin.x+ bgFrame.frame.size.width-50,bgFrame.frame.origin.y+ bgFrame.frame.size.height-tempHeight, 560.0, 12.0)];
    [lblTotalPerItem setFont:[UIFont fontWithName:@"Segoe UI" size:10]];
    lblTotalPerItem.textColor = [UIColor grayColor];
    [lblTotalPerItem setBackgroundColor:[UIColor clearColor]];
    lblTotalPerItem.text=[Functions getLocalizedNumber:@"0x0"];
    
    lblPriceValue=[[UILabel alloc]initWithFrame:CGRectMake(lblPrice.frame.origin.x,bgFrame.frame.origin.y+ bgFrame.frame.size.height-30, 100.0, 20.0)];
    [lblPriceValue setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    lblPriceValue.textColor = [UIColor brownColor];
    [lblPriceValue setBackgroundColor:[UIColor clearColor]];
    lblPriceValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",pricePer]];
    
    lblTotalValue=[[UILabel alloc]initWithFrame:CGRectMake(lblTotal.frame.origin.x,bgFrame.frame.origin.y+ bgFrame.frame.size.height-30, 100.0, 20.0)];
    [lblTotalValue setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
    lblTotalValue.textColor = [UIColor brownColor];
    [lblTotalValue setBackgroundColor:[UIColor clearColor]];
    lblTotalValue.text=[Functions getLocalizedNumber:@"0 SAR"];
    //[lblTotalValue sizeToFit];
    
    btnAddOrder = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnAddOrder = [[UIButton alloc] initWithFrame:CGRectMake(bgFrame.frame.origin.x+25, bgFrame.frame.origin.y+250, 242, 40)];
    [btnAddOrder setTitleColor:UIColorFromRGB(0x5b1900) forState:UIControlStateNormal];
    NSString* btnTitletxt=NSLocalizedString(@"Add to Order",@"Add to Order");
    [btnAddOrder setTitle:btnTitletxt forState:UIControlStateNormal];
    
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_normal",deviceName]] forState:UIControlStateNormal];
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_pressed.png",deviceName]] forState:UIControlStateSelected];
    [btnAddOrder setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_order_detail_popup_normal",deviceName]] forState:UIControlStateHighlighted];
    [btnAddOrder addTarget:self
                    action:@selector(DoneOrder:)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView* lblImage=[[UIImageView alloc]initWithFrame:CGRectMake(40,10,17.0,17.0)];
    [lblImage setImage:[UIImage imageNamed:@"icon_grill.png"]];
    
    [btnAddOrder addSubview:lblImage];
    
    [bgFrame addSubview:imageBg];
    [bgView addSubview:bgFrame];
    [bgView addSubview:btnAddOrder];
    [bgView addSubview:button];
    [bgView addSubview:btnSwitch];
    [bgView addSubview:lblTitle];
    [bgView addSubview:lblPrice];
    [bgView addSubview:lblTotal];
    [bgView addSubview:lblTotalPerItem];
    [bgView addSubview:lblPriceValue];
    [bgView addSubview:lblTotalValue];
    
    int yAxis=15;
    int startingHeight=55;
    for(int i=1; i<=noOfseats; i++){
        
        UIImageView* tempView;
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(25, startingHeight, 241, 47)];
        view1.tag=i;
        view1.backgroundColor = [UIColor clearColor];
        tempView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, view1.frame.size.width, 47)];
        [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_top.png",deviceName]]];
        tempView.tag=4000+i;
        [view1 addSubview:tempView];
        
        UILabel* lblSeatTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, yAxis, 100, 20)];
        [lblSeatTitle setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [lblSeatTitle setBackgroundColor:[UIColor clearColor]];
        [lblSeatTitle setTextColor:[UIColor lightGrayColor]];
        [lblSeatTitle setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Seat", @"Seat"),[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",i]]]];
        lblSeatTitle.tag=5000+i;
        if(noOfseats==1){
            lblSeatTitle.text=NSLocalizedString(@"Order Quantity",@"Order Quantity");
        }
        [view1 addSubview:lblSeatTitle];
        
        
        UIButton* btnSubtract = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnSubtract = [[UIButton alloc] initWithFrame:CGRectMake(view1.frame.origin.x+130, yAxis-5, 25, 30)];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_normal.png",deviceName]] forState:UIControlStateNormal];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_pressed.png",deviceName]] forState:UIControlStateSelected];
        [btnSubtract setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_minus_normal.png",deviceName]] forState:UIControlStateHighlighted];
        btnSubtract.tag=1000+i;
        [btnSubtract addTarget:self
                        action:@selector(OrderBtnOnclick:)
              forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnSubtract];
        
        UIImageView* lblImage=[[UIImageView alloc]initWithFrame:CGRectMake(btnSubtract.frame.origin.x+btnSubtract.frame.size.width,yAxis-4, 33.0, 30.0)];
        [lblImage setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@quantity_text-field.png",deviceName]]];
        [view1 addSubview:lblImage];
        
        UILabel* lblScrolValue=[[UILabel alloc]initWithFrame:CGRectMake(btnSubtract.frame.origin.x+btnSubtract.frame.size.width+6,yAxis-3, 30.0, 20.0)];
        [lblScrolValue setFont:[UIFont fontWithName:@"Segoe UI" size:20]];
        lblScrolValue.textColor = [UIColor lightGrayColor];
        [lblScrolValue setBackgroundColor:[UIColor clearColor]];
        [lblSeatTitle setTextAlignment:UITextAlignmentCenter];
        lblScrolValue.tag=3000+i;
        lblScrolValue.text=[Functions getLocalizedNumber:@"0"];
        [view1 addSubview:lblScrolValue];
        
        UIButton* btnPlus = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnPlus = [[UIButton alloc] initWithFrame:CGRectMake(lblImage.frame.origin.x+lblImage.frame.size.width, yAxis-5, 25, 30)];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_normal.png",deviceName]] forState:UIControlStateNormal];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_pressed.png",deviceName]] forState:UIControlStateSelected];
        [btnPlus setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@btn_quantity_add_normal.png",deviceName]] forState:UIControlStateHighlighted];
        btnPlus.tag=2000+i;
        [btnPlus addTarget:self
                    action:@selector(OrderBtnOnclick:)
          forControlEvents:UIControlEventTouchUpInside];
        [view1 addSubview:btnPlus];
        
        if(noOfseats==1 && i==noOfseats){
            [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_single",deviceName]]];
        }else
            if(( noOfseats==2 || noOfseats==3 || noOfseats==4) && i==noOfseats){
                
                [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_bot",deviceName]]];
                
            }else if(i!=1){
                [tempView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@order_quantity_list_mid.png",deviceName]]];
            }
        startingHeight=startingHeight+view1.frame.size.height;
        [bgFrame addSubview:view1];
    }
    
    [self.view addSubview:bgView];
    
}
#pragma mark Switch For Everyone/Single
-(void) onSwitch:(id)sender{
    @try {
      btnSwitch=(UIButton*) sender;
    if(btnSwitch.selected==TRUE){
        if([[SingletonClass sharedInstance] numberOfPeople]>1){
            btnSwitch.selected=FALSE;
            isForEveryOne=TRUE;
            for(int i=2; i<=[[SingletonClass sharedInstance] numberOfPeople];i++){
                UIView* tempView=(UIView*)[self.view viewWithTag:i];
                [tempView setHidden:NO];
                
            }
            //set cell image to top
            UIImageView* temp=(UIImageView*) [self.view viewWithTag:4001];
            [temp setImage:[UIImage imageNamed:@"iphone_order_quantity_list_top.png"]];
            UILabel* lblTitle=(UILabel*) [self.view viewWithTag:5001];
            lblTitle.text=NSLocalizedString(@"Seat 1",@"Seat 1");
            
        }
        UILabel* templable=(UILabel*)[self.view viewWithTag:3001];
        [templable setText:multipalOrderValue];
        singleOrderValue=[NSString stringWithFormat:@"%@",[dicSeatsQuantities objectForKey:@"1"]];
        [dicSeatsQuantities setValue:multipalOrderValue forKey:@"1"];
        
        [self setTotalValue:[[dicSeatsQuantities allKeys] count]];
    }else{
       
        for(int i=2; i<=[[SingletonClass sharedInstance] numberOfPeople];i++){
            UIView* tempView=(UIView*)[self.view viewWithTag:i];
            [tempView setHidden:YES];
        }
        //set cell image to Single
        UIImageView* temp=(UIImageView*) [self.view viewWithTag:4001];
        [temp setImage:[UIImage imageNamed:@"iphone_order_quantity_list_single.png"]];
        UILabel* lblTitle=(UILabel*) [self.view viewWithTag:5001];
        lblTitle.text=NSLocalizedString(@"Order Quantity",@"Order Quantity");
       isForEveryOne=FALSE;
        btnSwitch.selected=TRUE;
        
        UILabel* templable=(UILabel*)[self.view viewWithTag:3001];
        [templable setText:singleOrderValue];
        multipalOrderValue=[NSString stringWithFormat:@"%@",[dicSeatsQuantities objectForKey:@"1"]];
        
        [dicSeatsQuantities setValue:singleOrderValue forKey:@"1"];

        noOfQut=0;
        noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:@"1"] intValue];
        lblTotalPerItem.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]];
        lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
    }
       
    }
    @catch (NSException *exception) {
       
    }
    @finally {
        
    }

}
#pragma mark OrderButton OnClieck
-(void)OrderBtnOnclick:(id)sender{
    int tagValue=[sender tag];
    
    if(tagValue<2000){
        UILabel* templable=(UILabel*)[self.view viewWithTag:tagValue+2000];
        if(tagValue+2000==3001){
            if(btnSwitch.selected==TRUE ){
                [templable setText:singleOrderValue];
            }else{
                [templable setText:multipalOrderValue];
            }
        }
        
        int value=[templable.text intValue];
        if(value>0){
            [templable setText:[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",--value]]];
        }
        UIView* tempView=(UIView*)[self.view viewWithTag:tagValue-1000];
        [dicSeatsQuantities setValue:templable.text forKey:[NSString stringWithFormat:@"%d",tempView.tag]];
        if(tagValue+2000==3001){
            
            if(btnSwitch.selected==TRUE ){
                singleOrderValue=templable.text;
            }else{
                multipalOrderValue=templable.text;
            }
        }
        
    }else if (tagValue>2000){
        UILabel* templable=(UILabel*)[self.view viewWithTag:tagValue+1000];
        if(tagValue+1000==3001){
            if(btnSwitch.selected==TRUE){
                [templable setText:singleOrderValue];
            }else{
                [templable setText:multipalOrderValue];
            }
        }
        int value=[templable.text intValue];
       
        [templable setText:[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",++value]]];
        UIView* tempView=(UIView*)[self.view viewWithTag:tagValue-2000];
        [dicSeatsQuantities setValue:templable.text forKey:[NSString stringWithFormat:@"%d",tempView.tag]];
        if(tagValue+1000==3001){
            if(btnSwitch.selected==TRUE ){
                singleOrderValue=templable.text;
            }else{
                multipalOrderValue=templable.text;
            }
        }
    }
    if(isForEveryOne==FALSE){
        noOfQut=0;
        noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:@"1"] intValue];
        lblTotalPerItem.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]];
        lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
    }else{
        
        [self setTotalValue:[[dicSeatsQuantities allKeys] count]];
    }
    
}
#pragma mark totle Prices set
-(void) setTotalValue:(int) items{
    noOfQut=0;
    int i=1;
    for (NSString *key in [dicSeatsQuantities allKeys])
    {
        noOfQut=noOfQut+[[dicSeatsQuantities objectForKey:key] intValue];
        
        if(i==items) break;
        i++;
    }
    lblTotalPerItem.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%dx%d",noOfQut,pricePer]];
    lblTotalValue.text=[Functions getLocalizedNumber:[NSString stringWithFormat:@"%d SAR",noOfQut*pricePer]];
    
}
-(IBAction)actionOrderAdd:(id)sender{
    @try {
        
        //NSLog(@"%@",dicSeatsQuantities);
        btnID=(UIButton*)sender;
        
        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:btnID.tag];
        backEndID=orderItem.itemBackendID;
        CategoryItem *categoryItem= [[DataStoreManager manager] getCategoryItem:btnID.tag];
        pricePer=[categoryItem.price intValue];
       
        NSLog(@"true %d",[[[NSUserDefaults standardUserDefaults] objectForKey:kaddOrderPopup]integerValue]);
        
        if([[[NSUserDefaults standardUserDefaults] objectForKey:kaddOrderPopup] integerValue]==1){
            [self AddAlertView];
            
        }else{
            [self createSeats:[[SingletonClass sharedInstance] numberOfPeople]];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
}
-(void) AddAlertView{
    AlertViewController *alertVC;
    if([Functions isIPad]){
        alertVC=[[AlertViewController alloc]initWithNibName:@"ipad_AlertViewController" bundle:nil];
        
    }else{
     alertVC=[[AlertViewController alloc]initWithNibName:@"AlertViewController" bundle:nil];
    }
    alertVC.notifyString=@"childVC";
    [self addChildViewController:alertVC];
    [self.view addSubview:alertVC.view];
}
-(void) DoneOrder:(id)sender{
    @try {
        int seatNo;
        UIButton* btn=(UIButton*)sender;
        //NSLog(@"On Add order %d  btn add order %d",btnID.tag,[btnAddOrder tag]);
        //NSLog(@"%@",dicSeatsQuantities);
        btn.selected=TRUE;
        if(isForEveryOne==TRUE){
            for(NSString* key in [dicSeatsQuantities allKeys]){
                seatNo=[key intValue];
                int orderQty=[[dicSeatsQuantities objectForKey:key] intValue];
                if(orderQty>0){
                    [[DataStoreManager manager] AddOrderFunction:btnID Seat:seatNo Qty:orderQty];
                }
            }
            
        }else {
            seatNo=0;
            int orderQty=[[dicSeatsQuantities objectForKey:@"1"] intValue];
            if(orderQty>0){
                [[DataStoreManager manager] AddOrderFunction:btnID Seat:seatNo Qty:orderQty];
            }else{
                NSString* alertTitletxt=NSLocalizedString(@"Warning",@"Warning");
                NSString* alertMsg=NSLocalizedString(@"Quantity wouldn't be Zero",@"Quantity wouldn't be Zero");

                [Functions showAlert:alertTitletxt message:alertMsg];
            }
            
        }
        [bgView removeFromSuperview];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}

@end
