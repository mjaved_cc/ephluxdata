//
//  RootViewController.m
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "RootViewController.h"
#import "ProductViewController.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Category+Image.h"
#import "Functions.h"
#import "Defines.h"
#import "OrderListController.h"

@implementation RootViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Menu",@"Menu");
        
        [Functions setNavigationBar];

        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)meunTapped:(UITapGestureRecognizer *)gesture
{
    //
    
    UIImageView *theTappedImageView = (UIImageView *)gesture.view;
    NSInteger tag = theTappedImageView.tag;
   // NSLog(@"tapped!%d",tag);
}

-(void)imageTapped:(UITapGestureRecognizer *)gesture
{
    @try {
        //
        
        UIImageView *theTappedImageView = (UIImageView *)gesture.view;
        NSInteger tag = theTappedImageView.tag;
        gesture.view.backgroundColor = [UIColor grayColor];
        ProductViewController *viewController=[[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
        viewController.category=(Category*)[[menuManger categories] objectAtIndex:tag];
        [self.navigationController pushViewController:viewController animated:YES];
        
        
        /*CATransition *animation=[CATransition animation];
         [animation setDelegate:self];
         [animation setDuration:0.85];
         [animation setType:@"genieEffect"];
         [animation setFillMode:kCAFillModeForwards];
         animation.endProgress=0.99;
         [animation setRemovedOnCompletion:YES];
         [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    }@catch (NSException *exception) {
        NSLog(@"Exception %@",exception);
    }
    
    
}
-(void)refreshController:(NSNotification*)notification{
    
    NSLog(@"Notification %@",notification.userInfo);
}
- (void)viewDidLoad
{
    @try {
        
        
        
        [super viewDidLoad];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshController:) name:@"refreshCategory" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuNotification:) name:@"changeMenu" object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadedNotification:) name:kNotificationCategoryImageDownloaded object:nil];
        
        
        //setup iphone button
        
        if(![Functions isIPad]){
        
            [self setupLeftMenuButton];
            [self setupRightMenuButton];
        }
        
        
        /*******************************************UISCROLLVIEW*************************************************************/
        menuManger = [MenuManager menuManager];
        [menuManger loadMenu];
        
        /*******************************************UISCROLLVIEW*************************************************************/
        
        int x_axis=420;
        int width=620;
        if ([[menuManger menus] count]==1) {
            x_axis=826;
            width=200;
        }
        if ([[menuManger menus] count]==2) {
            x_axis=626;
            width=400;
        }
        UIScrollView* imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(x_axis, 53, width, 50)];
        [imageScroll setScrollEnabled:YES];
        imageScroll.backgroundColor = [UIColor clearColor];
        [imageScroll setShowsHorizontalScrollIndicator:NO];
        imageScroll.pagingEnabled = YES;
        
        for (UIView *v in imageScroll.subviews) {
            [v removeFromSuperview];
        }
        
        int menus=[[menuManger menus] count];
        for (int j=0; j < menus; j++) {
            /*******************************************Menu***************************************************************/
            Menu *menuItem=[[menuManger menus] objectAtIndex:j];
            
            UIButton* btn=[UIButton buttonWithType:UIButtonTypeCustom];
            [btn setFrame:CGRectMake((j*199), 2, 198, 45)];
            btn.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:24.0f];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitle:menuItem.name forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor grayColor]];
            [btn setTag:j];
            [btn addTarget:self action:@selector(menuChanged:) forControlEvents:UIControlEventTouchUpInside];
            [btn setBackgroundImage:[UIImage imageNamed:@"Tabbtn_UnSelectedx"] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"Tabbtn_Selected"] forState:UIControlStateSelected];
            
            [imageScroll addSubview:btn];
        }
        [self menuChanged:nil];
        imageScroll.contentSize = CGSizeMake(199*menus, 50);
        imageScroll.userInteractionEnabled=YES;
        imageScroll.delaysContentTouches=YES;
        [imageScroll setExclusiveTouch:YES];
        [self.view addSubview:imageScroll];
    }
    @catch (NSException *exception) {
        NSLog(@"ViewDidLoad %@",exception);
    }
    
    // Do any additional setup after loading the view from its nib.
}


-(void)imageDownloadedNotification:(id)sender{
    
   
    
}

-(void)menuChanged:(id)sender {
    @try {
        //
        
        NSArray *catArray=nil;
        
        if ([sender isKindOfClass:[NSString class]]){
            catArray=[[DataStoreManager manager] categoriesFor:[[menuManger menus] objectAtIndex:[sender intValue]]];
        }else{
            catArray=[[DataStoreManager manager] categoriesFor:[[menuManger menus] objectAtIndex:[sender tag]]];
        }
        
        menuManger.categories=catArray;
        
        int count=[catArray count];
        if (count>0) {
            /*******************************************UIScrollView****************************************************************/
            UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, 550, 200)];
            [scrollView setScrollEnabled:YES];
            scrollView.backgroundColor = [UIColor whiteColor];
            [scrollView setShowsHorizontalScrollIndicator:NO];
            for (UIView *v in scrollView.subviews) {
                [v removeFromSuperview];
            }
            for (int aantal=0; aantal < count; aantal++) {
                /*******************************************Category****************************************************************/
                Category *category=[catArray objectAtIndex:aantal];
                
                //Either Download Image Here or In CategoryParser.Which One Suits You.
                
                NSString *fileName = [Functions localImageThumbName:category.imageURL backendID:[category.backendID intValue]];
                NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    [category startDownloadThumbImage];
                }
                
                
                UIImageView *item = [[UIImageView alloc] initWithFrame:CGRectMake(3+(aantal*256), 2, 250, 190)];
                item.backgroundColor = [UIColor grayColor];
                item.image=[category thumbImage];
                item.tag=aantal;
                
                
                
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
                tap.numberOfTapsRequired = 1;
                tap.cancelsTouchesInView=YES;
                item.userInteractionEnabled = YES;
                [item addGestureRecognizer:tap];
                [scrollView addSubview:item];
                
                /*******************************************UITapGestureRecognizer****************************************************/
                
                UILabel *scoreLabel = [ [UILabel alloc ] init];
                [scoreLabel setFrame:CGRectMake(3+(aantal*256), CGRectGetMaxY(item.frame)-50, 256, 40)];
                scoreLabel.textAlignment =  UITextAlignmentCenter;
                scoreLabel.textColor = [UIColor grayColor];
                scoreLabel.numberOfLines=0;
                scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
                scoreLabel.backgroundColor = [UIColor whiteColor];
                scoreLabel.font = [UIFont fontWithName:@"ProximaNova-Light" size:(28.0)];
                scoreLabel.text = category.name ;//[NSString stringWithFormat: @"%d", score];
                [scrollView addSubview:scoreLabel];
                [scrollView bringSubviewToFront:scoreLabel];
            }
            scrollView.contentSize = CGSizeMake(256*count, 200);
            scrollView.userInteractionEnabled=YES;
            scrollView.delaysContentTouches=NO;
            [self.view addSubview:scrollView];
            
            
            
        }else{
            NSString* alertTitle=NSLocalizedString(@"No Items Found", @"No Items Found");
            NSString* alertMsg=NSLocalizedString(@"There is no items attached to this menu", @"There is no items attached to this menu");
            
            [Functions showAlert:alertTitle message:alertMsg];

            return;
        }
        
        
        CATransition *animation=[CATransition animation];
        [animation setDelegate:self];
        [animation setDuration:0.75];
        [animation setType:@"genieEffect"];
        [animation setFillMode:kCAFillModeRemoved];
        animation.endProgress=0.99;
        [animation setRemovedOnCompletion:YES];
        [self.view.layer addAnimation:animation forKey:nil];
    }
    @catch (NSException *exception) {
        NSString* alertTitle=NSLocalizedString(@"No Items", @"No Items");
        NSString* alertMsg=NSLocalizedString(@"Sorry no items are there in this category",@"Sorry no items are there in this category");
        [Functions showAlert:alertTitle message:alertMsg];
    }
    
    
    
}
- (void)menuNotification:(NSNotification *)notification{
    
    [self menuChanged:notification.object];
    
}
- (void)viewDidUnload
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCategoryImageDownloaded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeMenu" object:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [self setOrderQuantity];
    
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    //self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
	
}

-(IBAction)actionBack:(id)sender{
    
    
    /* CATransition *animation=[CATransition animation];
     [animation setDelegate:self];
     [animation setDuration:0.75];
     [animation setType:@"genieEffect"];
     [animation setFillMode:kCAFillModeRemoved];
     
     animation.endProgress=0.99;
     [animation setRemovedOnCompletion:YES];
     [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    [[self navigationController] popViewControllerAnimated:YES];
}


-(IBAction)actionOrderlist:(id)sender{
    @try {
        
        
        OrderListController *controller=[[OrderListController alloc] initWithNibName:@"OrderListController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"OrderList");
    }
    
    
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}


#pragma mark-
#pragma mark-Setting Order Quantity
-(void)setOrderQuantity{
    @try {
        Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
        int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
        [btnOrderQty setTitle:[NSString stringWithFormat:@"%d",qtyTotal] forState:UIControlStateNormal];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}

@end
