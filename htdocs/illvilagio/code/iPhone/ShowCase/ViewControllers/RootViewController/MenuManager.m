//
//  MenuManager.m
//  POS
//
//  Created by admin on 19/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MenuManager.h"

static MenuManager *menuManager = nil;

@implementation MenuManager

@synthesize currentMenu, currentCategory, menus, categories, categoryItems;

#pragma mark -
#pragma mark Static Call for object creation
+(id)menuManager {
	@synchronized(self) {
		if (menuManager == nil) {
			menuManager = [[self alloc] init];
		}
	}
	
	return menuManager;
}

#pragma mark -
#pragma mark Life Cycle
-(id)init {
	self = [super init];
	if(self != nil) {
		dataStoreManager = [DataStoreManager manager];
	}

	return self;
}



#pragma mark -
#pragma mark Public function
/*-(void)loadMenu {
    @try {
        self.menus = [dataStoreManager allMenues];
        if ([self.menus count]>0){
            self.currentMenu = [menus objectAtIndex:0];
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Load menu Exception");
    }
	
}
*/
-(void)loadMenu {
    @try {
        self.menus = [dataStoreManager allMenues];
        if ([self.menus count]>0){
            
            int count=[self.menus count]-1;
            
            if (count==0) {
                count=1;
            }else{
                count=count+1;
            }
            for (int i=0; i<count; i++) {
                Menu *mainMenu=(Menu*)[menus objectAtIndex:i];
                if ([mainMenu.categories count]>0) {
                    self.currentMenu = [menus objectAtIndex:i];
                    //return;
                }
            }
        }
    }@catch (NSException *exception) {
        NSLog(@"Load menu Exception");
    }
}

-(NSString*)idInString {
	return [NSString stringWithFormat:@"%d", self.currentMenu.backendID];
}

#pragma mark -
#pragma mark Overrided Properties
-(void)setCurrentMenu:(Menu*)menu {
    @try {
        if(currentMenu != menu) {
            currentMenu = menu;
            
            self.categories = [dataStoreManager categoriesFor:currentMenu];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"setCurrentMenu Exception");
    }

}

-(void)setCategories:(NSArray*)_categories {
    @try {
        if(categories != _categories) {
            categories = _categories ;
            
            self.currentCategory = (Category*)[categories objectAtIndex:0];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception SetCategories");
    }
   

}

-(void)setCurrentCategory:(Category*)category {
    @try {
        if(currentCategory != category) {
            currentCategory = category ;
            
            self.categoryItems = [dataStoreManager categoryItemsFor:currentCategory];
                //TODO:
                //self.categoryItems = currentCategory.categoryItems;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"SetCurrentCategory Exception");
    }

}

@end
