//
//  MenuManager.h
//  POS
//
//  Created by admin on 19/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataStoreManager + DataSpecialized.h"

@interface MenuManager : NSObject {
	DataStoreManager *dataStoreManager;
	Menu *currentMenu;
	Category *currentCategory;
	
	NSArray *menus;
	NSArray *categories;
	NSArray *categoryItems;
}

@property(nonatomic, retain) Menu *currentMenu;
@property(nonatomic, retain) Category *currentCategory;

@property(nonatomic, retain) NSArray *menus;
@property(nonatomic, retain) NSArray *categories;
@property(nonatomic, retain) NSArray *categoryItems;

-(void)loadMenu;
-(NSString*)idInString;

+(id)menuManager;

@end
