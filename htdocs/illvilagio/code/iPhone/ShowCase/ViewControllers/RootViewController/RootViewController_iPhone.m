//
//  RootViewController.m
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "RootViewController_iPhone.h"
#import "ProductViewController.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Category+Image.h"
#import "Functions.h"
#import "Defines.h"
#import "OrderListController.h"
#import "RootViewControllerChild_iPhone.h"
#import "AnyViewController.h"

#define kOFFSET_FOR_KEYBOARD 0.0
@implementation RootViewController_iPhone

@synthesize  filteredArray;;;
@synthesize checkedIndexPath;
@synthesize tickImage,filteredCOntent;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Menu", @"Menu");
        self.isChangeOrder = NO;
        [Functions setNavigationBar];
    }
    
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)meunTapped:(UITapGestureRecognizer *)gesture
{
    //
    
    UIImageView *theTappedImageView = (UIImageView *)gesture.view;
    NSInteger tag = theTappedImageView.tag;
    // NSLog(@"tapped!%d",tag);
}


-(void)highlightLabelOff:(UILabel*)sender{
    
    sender.backgroundColor = [UIColor clearColor];
    sender.alpha = 1;
    
    
}

-(void)imageTapped_old:(UITapGestureRecognizer *)gesture
{
    @try {
        //
        
        UILabel *theTappedLabel = (UILabel *)gesture.view;
        // NSInteger tag = theTappedImageView.tag;
        theTappedLabel.backgroundColor = [UIColor whiteColor];
        theTappedLabel.alpha = 0.5;
        
        [self performSelector:@selector(highlightLabelOff:) withObject:theTappedLabel afterDelay:0.1];
        
        NSString *split = theTappedLabel.text;
        
        NSArray *arr = [split componentsSeparatedByString:@"||"];
        
        int menuId=0, catId=0;
        
        if([arr count]>1){
            menuId = [[arr objectAtIndex:1] intValue];
            catId = [[arr objectAtIndex:0] intValue]-1;
        }else{
            
            menuId = [[arr objectAtIndex:0] intValue];
            
        }
        
        
        NSArray* catArray=[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:menuId]:0];
        
        NSArray* childArray=[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:menuId]:[[[catArray objectAtIndex:catId] valueForKey:@"backendID"] intValue]];
        
        if([childArray count]>0){
            
            RootViewControllerChild_iPhone *viewController;
            if([Functions isIPad]){
                viewController=[[RootViewControllerChild_iPhone alloc] initWithNibName:@"ipad_RootChildVC" bundle:nil];
            }else{
                viewController=[[RootViewControllerChild_iPhone alloc] initWithNibName:@"RootViewControllerChild_iPhone" bundle:nil];
            }
            viewController.category= [catArray objectAtIndex:catId];
            viewController.menuId = menuId;
            viewController.catId = catId;
            viewController.childArray=childArray;
            [self.navigationController pushViewController:viewController animated:YES];
            
            return;
        }
        
        ProductViewController *viewController;
        if([Functions isIPad]){
            viewController=[[ProductViewController alloc] initWithNibName:@"ipad_ProductViewController" bundle:nil];
        }else{
            viewController=[[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
        }
        
        viewController.category= [catArray objectAtIndex:catId];
        
        if(langIsArabic==1){
            viewController.navTitle = [[[menuManger menus] objectAtIndex:menuId] valueForKey:@"name_arabic"];
        }else{
            viewController.navTitle = [[[menuManger menus] objectAtIndex:menuId] valueForKey:@"name"];
        }
        
        [self.navigationController pushViewController:viewController animated:YES];
        
    }@catch (NSException *exception) {
        
        NSLog(@"Exception %@",exception);
    }
    
    
}
-(void)imageTapped:(UITapGestureRecognizer *)gesture
{
    @try {
        //
        
        UILabel *theTappedLabel = (UILabel *)gesture.view;
        // NSInteger tag = theTappedImageView.tag;
        theTappedLabel.backgroundColor = [UIColor whiteColor];
        theTappedLabel.alpha = 0.5;
        
        [self performSelector:@selector(highlightLabelOff:) withObject:theTappedLabel afterDelay:0.1];
        
        NSString *split = theTappedLabel.text;
        
        int catId=[split intValue];
        
        Category *categoryClicked = [[DataStoreManager manager] getCategory:catId];
        
        Category *parentCategoryClicked = [[DataStoreManager manager] getCategory:[categoryClicked.parentID intValue]];
        
        NSArray* childArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:[categoryClicked.backendID intValue]];
        
        
        if([childArray count]>0){
            
            RootViewControllerChild_iPhone *viewController;
            if([Functions isIPad]){
                viewController=[[RootViewControllerChild_iPhone alloc] initWithNibName:@"ipad_RootChildVC" bundle:nil];
            }else{
                viewController=[[RootViewControllerChild_iPhone alloc] initWithNibName:@"RootViewControllerChild_iPhone" bundle:nil];
            }
            viewController.category= categoryClicked;
            viewController.catId = catId;
            viewController.childArray=childArray;
            [self.navigationController pushViewController:viewController animated:YES];
            
            return;
        }
        
        ProductViewController *viewController;
        if([Functions isIPad]){
            viewController=[[ProductViewController alloc] initWithNibName:@"ipad_ProductViewController" bundle:nil];
        }else{
            viewController=[[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil];
        }
        
        viewController.category= categoryClicked;
        viewController.navTitle = langIsArabic?parentCategoryClicked.name_arabic:parentCategoryClicked.name;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }@catch (NSException *exception) {
        
        NSLog(@"Exception %@",exception);
    }
    
    
}
-(void)refreshController:(NSNotification*)notification{
    
    NSLog(@"Notification %@",notification.userInfo);
}


-(void)showDetail:(NSIndexPath*)sender
{
    @try {
        //
        int menuId=0, catId=0;
        
        menuId = sender.section;
        catId = sender.row;
        
        NSArray *detailArray = [[DataStoreManager manager] categoryItemsFor:[[searchResultController objectAtIndexPath:sender] valueForKey:@"category"]];
        
        if([detailArray count]>0){
            
            AnyViewController *c;
            if([Functions isIPad]){
                c = [[AnyViewController alloc] initWithNibName:@"ipad_AnyViewController" bundle:nil];
                
            }else{
                c = [[AnyViewController alloc] initWithNibName:@"AnyViewController" bundle:nil];
                
            }
            c.detailArray=detailArray;
            c.category = [[searchResultController objectAtIndexPath:sender] valueForKey:@"category"];
            c.currentPageIndex=-1;
            c.currentCategoryItem = [searchResultController objectAtIndexPath:sender];
            [self.navigationController pushViewController:c animated:YES];
            
            return;
        }
        
    }@catch (NSException *exception) {
        
        NSLog(@"Exception is %@",exception);
    }
}

#pragma mark Friend Contact matching
- (NSArray *) contactsMatchingName: (NSString *) fname SectionNo:(int)sec
{
    NSArray *catArray = nil;
    /*
     NSPredicate *pred;
     // 	pred = [NSPredicate predicateWithFormat:@"firstname contains[cd] %@ OR lastname contains[cd] %@ OR nickname contains[cd] %@ OR middlename contains[cd] %@", fname, fname, fname, fname];
     pred = [NSPredicate predicateWithFormat:@"name contains[cd] %@", fname];
     
     //  pred = [NSPredicate predicateWithFormat:@"name contains[cd] %@ OR name == %@", fname,fname];
     return [catArray filteredArrayUsingPredicate:pred];
     */
    return catArray;
}

#pragma mark -
#pragma mark UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBarLocal textDidChange:(NSString *)searchText
{
    searchBarLocal.frame = CGRectMake(0, 0, 258, 48);
    //NSLog(@"tag %d",searchBarLocal.tag);
    //258
}


-(void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    controller.searchBar.frame = CGRectMake(0, 0, 258, 48);
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.frame = CGRectMake(0, 0, 100, 48);
    
	[searchBar resignFirstResponder];
	[mySearch setShowsCancelButton:NO animated:YES];
	searchBar.text=@"";
    
	[tblMenu reloadData];
}

-(void)SearchResult:(NSString *)searchvalue;
{
	
}
// called when Search (in our case ‚ÄúDone‚Äù) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
	[searchBar resignFirstResponder];
	[tblMenu reloadData];
    
}

- (void)viewDidLoad
{
    layOutView=[[UIView alloc]init];
    popupTrue = TRUE;
    [viewForTable setHidden:YES];
    catorID = -33;
    catArrayLocal = [[NSArray alloc]init];
    filteredCOntent = [[NSMutableArray alloc]init];
    tickImage = [[NSMutableArray alloc]init];
    [tblSearch removeFromSuperview];
    [lblcateMenu setFont:[UIFont fontWithName:@"Segoe UI" size:20]];
    lblcateMenu.textColor = UIColorFromRGB(0x5b1900);
    lblcateMenu.text=NSLocalizedString(@"Menu Category", @"Menu Category");
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateNormal];
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateHighlighted];
    [btnOk setTitle:NSLocalizedString(@"Ok",@"Ok") forState:UIControlStateSelected];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iphone_body_bg.png"]];
    [tblSearch setBackgroundView:bg];
    
    @try {
        
        [super viewDidLoad];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshController:) name:@"refreshCategory" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuNotification:) name:@"changeMenu" object:nil];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDownloadedNotification:) name:kNotificationCategoryImageDownloaded object:nil];
                //setup iphone button
        
        //*******************************************UISCROLLVIEW*************************************************************/
        
        menuManger = [MenuManager menuManager];
        [menuManger loadMenu];
        catParentArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:0];
        
        if([[menuManger menus] count] == 0){
            
            txtSearch.enabled = NO;
            btnSearch.enabled = NO;
        }
        
        /*******************************************UISCROLLVIEW*************************************************************/
        
        //imageScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 46, 320, 460)];
        [imageScroll setScrollEnabled:YES];
        imageScroll.backgroundColor = [UIColor clearColor];
        [imageScroll setShowsHorizontalScrollIndicator:NO];
        //imageScroll.pagingEnabled = YES;
        
        for (UIView *v in imageScroll.subviews) {
            [v removeFromSuperview];
        }
        
        //int menus=[[menuManger menus] count];
        int menus = [catParentArray count];
        NSLog(@"menus data count %d",menus);
        if([Functions isIPad]){
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(keyboardWillHide)
                                                         name:UIKeyboardWillHideNotification
                                                       object:nil];
                if(self.isChangeOrder){
                [self setBackBarButton];
            }
            
            for (int j=0; j < menus; j++) {
                /*******************************************Menu***************************************************************/
                Category* menuItem = [catParentArray objectAtIndex:j];
                
                UIImageView *headerbarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 + (53*j)+ (j*140),552, 53)];
                
                headerbarView.image=[UIImage imageNamed:@"sub-heading-bar_small.png"];
                [headerbarView setBackgroundColor:[UIColor clearColor]];
                
                UILabel *headerbarViewLabel= [[ UILabel alloc] initWithFrame:CGRectMake(0, 2+(53*j)+ (j*140), 552, 53)];
                
                if(j!=0){
                    headerbarView.frame=CGRectMake(0, 70 + (53*j)+ (j*140),552, 53) ;
                    headerbarViewLabel.frame=CGRectMake(0, 68+(53*j)+ (j*140), 552, 53);
                }
                
                //[Functions getLocalLang];
                
                
                
                headerbarViewLabel.text = langIsArabic?menuItem.name_arabic:menuItem.name;
                
                CGSize txtSz = [headerbarViewLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 18]];
                
                headerbarViewLabel.frame = CGRectMake(headerbarViewLabel.frame.size.width/2 - txtSz.width/2, headerbarViewLabel.frame.origin.y, txtSz.width+10, headerbarViewLabel.frame.size.height);
                
                [headerbarViewLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
                headerbarViewLabel.textColor = UIColorFromRGB(0x5b1900);
                headerbarViewLabel.backgroundColor = [UIColor clearColor];
                headerbarViewLabel.textAlignment = UITextAlignmentCenter;
                
                UIImageView *preDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
                
                UIImageView *postDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
                
                postDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x+headerbarViewLabel.frame.size.width+10,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
                
                
                [imageScroll addSubview:headerbarView];
                [imageScroll addSubview:headerbarViewLabel];
                [imageScroll addSubview:postDesignImage];
                
                preDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x-20,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
                
                [imageScroll addSubview:preDesignImage];
                
                
                [imageScroll addSubview:[self menuChanged:[NSString stringWithFormat:@"%d",j]]];
                
            }
            
            imageScroll.multipleTouchEnabled = NO;
            imageScroll.contentSize = CGSizeMake(320, 250*menus);
            imageScroll.userInteractionEnabled=YES;
            imageScroll.delaysContentTouches=YES;
            [imageScroll setExclusiveTouch:YES];
            // [self.view addSubview:imageScroll];
            
            
        }else{
            
            [self setupLeftMenuButton];
            [self setupRightMenuButton];
            for (int j=0; j < menus; j++) {
                /*******************************************Menu***************************************************************/
                
                //Menu *menuItem=[[menuManger menus] objectAtIndex:j];
                
                Category* menuItem = [catParentArray objectAtIndex:j];
                UIImageView *headerbarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2 + (53*j)+ (j*140), 320, 53)];
                headerbarView.image=[UIImage imageNamed:@"sub-heading-bar_small.png"];
                [headerbarView setBackgroundColor:[UIColor clearColor]];
                
                UILabel *headerbarViewLabel= [[ UILabel alloc] initWithFrame:CGRectMake(0, (53*j)+ (j*140), 320, 53)];
                headerbarViewLabel.text = langIsArabic?menuItem.name_arabic:menuItem.name;
                
                CGSize txtSz = [headerbarViewLabel.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 18]];
                
                headerbarViewLabel.frame = CGRectMake(headerbarViewLabel.frame.size.width/2 - txtSz.width/2, headerbarViewLabel.frame.origin.y, txtSz.width+10, headerbarViewLabel.frame.size.height);
                
                [headerbarViewLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
                headerbarViewLabel.textColor = UIColorFromRGB(0x5b1900);
                headerbarViewLabel.backgroundColor = [UIColor clearColor];
                headerbarViewLabel.textAlignment = UITextAlignmentCenter;
                
                
                UIImageView *preDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
                
                UIImageView *postDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
                
                postDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x+headerbarViewLabel.frame.size.width+10,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
                
                
                [imageScroll addSubview:headerbarView];
                [imageScroll addSubview:headerbarViewLabel];
                [imageScroll addSubview:postDesignImage];
                
                preDesignImage.frame = CGRectMake(headerbarViewLabel.frame.origin.x-20,headerbarView.frame.origin.y+headerbarView.frame.size.height/2-2, 12, 3);
                
                [imageScroll addSubview:preDesignImage];
                
                
                [imageScroll addSubview:[self menuChanged:[NSString stringWithFormat:@"%d",j]]];
                
            }
            imageScroll.contentSize = CGSizeMake(320, 250*menus);
            imageScroll.userInteractionEnabled=YES;
            imageScroll.delaysContentTouches=YES;
            [imageScroll setExclusiveTouch:YES];
            // [self.view addSubview:imageScroll];
        }
        
        [self addOrderView];
        //[self AddAlertView];
        if([Functions isDeliveryTarget]){
            
            [self setupDeliveryView];
            
        }
    }
    
    @catch (NSException *exception) {
        NSLog(@"ViewDidLoad %@",exception);
    }
    
    
    // Do any additional setup after loading the view from its nib.
}



-(void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint touchPoint = [touch locationInView:self.view ]; //location is relative to the current view
//    // do something with the touched point
//    NSLog(@"current view");
//}
-(void)setupDeliveryView{
    
    btnSwitch.alpha = 0;
    lbDineIn.text =NSLocalizedString(@"Delivery", @"Delivery");
    lblBelowDineIn.text = NSLocalizedString(@"Order will be taken for DELIVERY only.", @"Order will be taken for DELIVERY only.");
    
}
-(void)setPopupSlider{
    
    sliderPopup.backgroundColor = [UIColor clearColor];
	
	UIImage *stetchRightTrack = [UIImage imageNamed:@"count_slide_bg.png"];
	
    UIImage *sliderLeftTrackImage = [[UIImage imageNamed: @"count_slide_bg_full.png"] stretchableImageWithLeftCapWidth: 9 topCapHeight: 0];
    
	[sliderPopup setThumbImage: [UIImage imageNamed:@"btn_count_slide_drag_normal.png"] forState:UIControlStateNormal];
	[sliderPopup setThumbImage: [UIImage imageNamed:@"btn_count_slide_drag_pressed.png"] forState:UIControlStateHighlighted];
	
    [sliderPopup setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
	[sliderPopup setMinimumTrackImage:sliderLeftTrackImage forState:UIControlStateNormal];
    
	sliderPopup.minimumValue = 1.0;
	sliderPopup.maximumValue = 4.0;
	sliderPopup.continuous = YES;
	sliderPopup.value = 2.0;
	
	[sliderPopup addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark AddAlertView
-(void) AddAlertView{
    AlertViewController *alertVC;
    if([Functions isIPad]){
        alertVC=[[AlertViewController alloc]initWithNibName:@"ipad_AlertViewController" bundle:nil];
        
    }else{
        alertVC=[[AlertViewController alloc]initWithNibName:@"AlertViewController" bundle:nil];
    }
    //alertVC.notifyString=@"NotifyProduct";
    [self addChildViewController:alertVC];
    [self.view addSubview:alertVC.view];
}

-(void)addOrderView{
    
    //set font of labels of popup view
    
    if(self.isOrderView)
        // if(![[NSUserDefaults standardUserDefaults] objectForKey:kaddOrderPopup])
    {
        [lbDineIn setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [lblNoOfGuest setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        [lblPopupTitle setFont:[UIFont fontWithName:@"Script MT bold" size:22.0f]];
        lbDineIn.textColor = [UIColor darkGrayColor];
        lblNoOfGuest.textColor = [UIColor darkGrayColor];
        lblPopupTitle.textColor=UIColorFromRGB(0x5b1900);
        lblNumber.textColor=UIColorFromRGB(0x5b1900);
        lblNumber.text = [Functions getLocalizedNumber:@"1"];
        
        lbDineIn.text=NSLocalizedString(@"Dine In / TakeAway", @"Dine In / TakeAway");
        lblNoOfGuest.text=NSLocalizedString(@"No Of People", @"No Of People");
        lblBelowDineIn.text=NSLocalizedString(@"On for 'DineIn' , Off for 'TakeAway'", @"On for 'DineIn' , Off for 'TakeAway'");
        lblPopupTitle.text=NSLocalizedString(@"Add Order!", @"Add Order!");
        
        [self setPopupSlider];
        
        innerOrderView.layer.cornerRadius = 10;
        innerOrderView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        innerOrderView.layer.borderWidth = .5;
        //self.isOrderView = TRUE;
        
        [self.view addSubview:addOrderView];
        [self.view bringSubviewToFront:addOrderView];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:FALSE forKey:kaddOrderPopup];
        
    }
    
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setBool:NO forKey:kaddOrderPopup];
    //[defaults synchronize];
}


-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

-(void)sliderAction:(id)sender
{
	UISlider *slider = (UISlider *)sender;
    lblNumber.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%d",(int)slider.value]];
    [[SingletonClass sharedInstance] setNumberOfPeople:(int)slider.value];
    
}

-(IBAction)showPopup
{
    
    [txtSearch resignFirstResponder];
    if(popupTrue == TRUE)
    {
        popupTrue = FALSE;
        [viewForTable setHidden:NO];
        [self.view bringSubviewToFront:viewForTable];
        [self fadeIn:@"id" finished:NO target:viewForTable];
        
    }
    else
    {
        // [viewForTable setHidden:YES];
        //  [self.view sendSubviewToBack:viewForTable];
        popupTrue = TRUE;
        [self fadeOut:@"id" finished:YES target:viewForTable];
        
    }
    
}
#pragma mark - Text Field Began Editing
- (IBAction)fieldEditingBegan:(UITextField *)sender
{
    
    //NSLog(@"All -->  %d",catorID);
    @try {
        
        if(catorID == -33){
            
            searchResultController=[[DataStoreManager manager] searchText:sender.text:0];
            
        }else{
            
            searchResultController=[[DataStoreManager manager] searchText:sender.text:catorID];
            
        }
        
        self.filteredArray = [searchResultController fetchedObjects];
        
        if([self.filteredArray count]>0){
            [layOutView removeFromSuperview];//old
            [tblSearch setHidden:NO];
            [tblSearch reloadData];
            [self.view addSubview:tblSearch];
            [self.view bringSubviewToFront:tblSearch];
            
        }else{
            [tblSearch setHidden:YES];
            [tblSearch removeFromSuperview];
        }
        
        }
    @catch (NSException *exception) {
        NSLog(@"Exception Occur File Editing began %@",exception);
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([textField.text isEqualToString:@""]|| [textField.text isEqualToString:@" "])
    {
        
        [tblSearch removeFromSuperview];
    }
    else
    {
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField.text isEqualToString:@""]|| [textField.text isEqualToString:@" "])
    {
        [tblSearch removeFromSuperview];
    }else{
        [tblSearch setHidden:NO];
        [self fieldEditingBegan:txtSearch];
        
    }
    if([[self.view subviews] containsObject:tblSearch]){
        
    }else{
        [self setViewMovedUp:NO view:searchBarView];
    }
    
    [layOutView removeFromSuperview];
     [txtSearch resignFirstResponder];
    
    return YES;
}
#pragma mark Keyboard Hide
-(void)keyboardWillHide
{
//    if([txtSearch.text isEqualToString:@""]|| [txtSearch.text isEqualToString:@" "])
//    {
//        
//    }else{
//        [tblSearch setHidden:NO];
//        [self fieldEditingBegan:txtSearch];
//        
//    }
[tblSearch removeFromSuperview];
    if([[self.view subviews] containsObject:tblSearch]){
        
    }else{
        [self setViewMovedUp:NO view:searchBarView];
    }
    
    [layOutView removeFromSuperview];
    [txtSearch resignFirstResponder];
    //call when key board hides
    //[self animateTextField: Nil up: NO distance:0];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
   // if([textField clearButtonMode]){
        //NSLog(@"clearButtonMode");
   // }else{
    [self addLayer];
   // }
}
-(void) addLayer{
    [self fadeOutSecond:@"id" finished:YES target:viewForTable];
    
    layOutView.frame=CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+44, self.view.frame.size.width, self.view.frame.size.height);
    [layOutView setBackgroundColor:[UIColor blackColor]];
    layOutView.alpha=0.5;
    UITapGestureRecognizer * onTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap:)];
    [onTap1 setDelegate:self];
    [onTap1 setNumberOfTapsRequired:1];
    [layOutView addGestureRecognizer:onTap1];
    
    //NSArray* subViewsArray= [self.view subviews];
    // NSLog(@"%@",[self.view subviews]);
    if([[self.view subviews] containsObject:layOutView]){
        
    }else if([filteredArray count]==0 ){
        [self.view addSubview:layOutView];
    }
    [self setViewMovedUp:YES view:searchBarView];

}
#pragma mark On Deal Tap
- (void)oneTap:(UIGestureRecognizer *)gesture {
     [self setViewMovedUp:NO view:searchBarView];
    [txtSearch resignFirstResponder];
    [layOutView removeFromSuperview];
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    [tblSearch setHidden:YES];
    [self setViewMovedUp:NO view:searchBarView];
    [layOutView removeFromSuperview];
    //[txtSearch setText:@""];
   // [self textFieldShouldReturn:textField];
    return YES;
}
#pragma mark -
#pragma mark Table view data source

// Customize the number of rows in the table view.

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 42;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 47.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    searchingCheck = YES;
	if (tableView == tblMenu) {
		searchingCheck = NO;
        
        if (section == 0) {
            return 1;
        }else{
            
            Category *cat=[catParentArray objectAtIndex:section-1];
            
            NSArray* catArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:[cat.backendID intValue]];
            
            //[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:section-1]:0];
            int count=[catArray count];
            NSLog(@"section %d cou %d",section-1,count);
            return [catArray count];
        }
    }
    
    return [[[searchResultController sections] objectAtIndex:section] numberOfObjects];
    
    //   return [[searchResultController objectAtIndexPath:section] count];
    //   return self.filteredArray.count;
  	
    
}
#pragma mark - table View Delegate
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *imageViews;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 42 )];
    if([Functions isIPad]){
        headerView.frame=CGRectMake(0, 0, 552, 42 );
    }else{
        
    }
    [headerView setBackgroundColor:[UIColor clearColor]];
    UILabel *mylbl;
    
    
    if(tblMenu == tableView)
    {
        headerView.layer.cornerRadius = 5;
        headerView.layer.masksToBounds = YES;
        headerView.layer.borderWidth = 0.5f;
        headerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 120, 25)];
        mylbl.backgroundColor = [UIColor clearColor];
        
        [mylbl setFont:[UIFont fontWithName:@"Oxygen-Regular" size:15.0f]];
        mylbl.textColor = UIColorFromRGB(0x5b1900);
        
        imageViews.frame = CGRectMake(0,0,320,42);
        if([Functions isIPad]){
            imageViews.frame=CGRectMake(0, 0, 551, 42);
        }else{
            
        }
        imageViews = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"category_header.png"]];
        // imageViews = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub-heading-bar_small.png"]];
        imageViews.layer.cornerRadius=2.0;
        if (section == 0) {
            mylbl.text = NSLocalizedString(@"All", @"All");
        }
        else{
            //Menu *menuItem=[[menuManger menus] objectAtIndex:section-1];
            Category *menuItem=[catParentArray objectAtIndex:section-1];
            if (section == 1) {
                mylbl.text = [NSString stringWithFormat:@"%@",langIsArabic?menuItem.name_arabic:menuItem.name];
            }if (section == 2) {
                mylbl.text = [NSString stringWithFormat:@"%@",langIsArabic?menuItem.name_arabic:menuItem.name];
            }
            
            
        }
        
    }
    else
    {
        mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 42 )];
        mylbl.backgroundColor = [UIColor clearColor];
        mylbl.textAlignment = UITextAlignmentCenter;
        [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
        mylbl.textColor = UIColorFromRGB(0x5b1900);
        
        imageViews.frame = CGRectMake(0,0,320,42 );
        if([self.filteredArray count]>0)
        {
            
            NSIndexPath *index=[NSIndexPath indexPathForRow:0 inSection:section];
            
            if([[searchResultController fetchedObjects] count]){
                
                if(langIsArabic){
                    mylbl.text = [[[searchResultController objectAtIndexPath:index] valueForKey:@"category"] valueForKey:@"name_arabic"];
                }else{
                    mylbl.text = [[[searchResultController objectAtIndexPath:index] valueForKey:@"category"] valueForKey:@"name"];
                }
                
            }
            
        }
        else
        {
            mylbl.text =  NSLocalizedString(@"Item Not Found", @"Item Not Found");
        }
        
        
        
        //imageViews = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub-heading-bar_small.png"]];
        
        if([Functions isIPad]){
            imageViews = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub-heading-bar.png"]];
            imageViews.frame=CGRectMake(0, 0, 551, 42);
        }else{
            imageViews = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sub-heading-bar_small.png"]];
        }
        
        
    }
    
    CGSize txtSz = [mylbl.text sizeWithFont:[UIFont fontWithName: @"Segoe UI" size: 18]];
    
    mylbl.frame = CGRectMake(imageViews.frame.size.width/2 - txtSz.width/2, mylbl.frame.origin.y, txtSz.width+10, mylbl.frame.size.height);
    
    UIImageView *preDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
    
    UIImageView *postDesignImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"heading_decoration.png"]];
    
    postDesignImage.frame = CGRectMake(mylbl.frame.origin.x+mylbl.frame.size.width+0,imageViews.frame.origin.y+imageViews.frame.size.height/2-2, 12, 3);
    [headerView addSubview:imageViews];
    [headerView addSubview:mylbl];
    [headerView addSubview:postDesignImage];
    
    preDesignImage.frame = CGRectMake(mylbl.frame.origin.x-20,imageViews.frame.origin.y+imageViews.frame.size.height/2-2, 12, 3);
    
    [headerView addSubview:preDesignImage];
    
    return headerView;
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == tblMenu){
        
        int countMenu = [catParentArray count]+1;
        return countMenu;
    }else
    {
        int countsAre = 0;
        
        if (searchResultController){
            
            return [[searchResultController sections] count];
            
        }
        
        return 1;
        
        return countsAre;;
    }
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView  Old_cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",indexPath.row,indexPath.section];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(tableView == tblMenu)
    {
        if (cell==nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cell.backgroundView =[[UIImageView alloc] init];
            cell.selectedBackgroundView =[[UIImageView alloc] init];
            UIImage *unselectedBackground = [UIImage imageNamed:@"iphone_top_bar_bg.png"];
            //((UIImageView *)cell.backgroundView).image = unselectedBackground;
            if(indexPath.section==0){
                UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
                [mylbl setNumberOfLines:0];
                mylbl.numberOfLines = 2;
                [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                
                mylbl.text = NSLocalizedString(@"All",@"All");
                [cell.contentView addSubview:mylbl];
                mylbl.textColor = [UIColor darkGrayColor];
                mylbl.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:mylbl];
                
                
                UIButton *myAcceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                myAcceptBtn.tag = -33;
                [tickImage addObject:[NSString stringWithFormat:@"%d",myAcceptBtn.tag]];
                [myAcceptBtn addTarget:self action:@selector(chckMark:) forControlEvents:UIControlEventTouchUpInside];
                [myAcceptBtn setFrame:CGRectMake(188, 20, 18, 19)];
                [myAcceptBtn setImage:[UIImage imageNamed:@"icon_tick_mark.png"] forState:UIControlStateNormal];
                [cell.contentView addSubview:myAcceptBtn];
                
                
            }else
                if(indexPath.section==1){
                    @try {
                        
                        Category *cat=[catParentArray objectAtIndex:indexPath.section-1];
                        
                        NSArray* catArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:[cat.backendID intValue]];
                        UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
                        [mylbl setNumberOfLines:0];
                        mylbl.numberOfLines = 2;
                        [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                        
                        if(langIsArabic){
                            
                            mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name_arabic"]];
                        }else{
                            
                            mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
                            
                        }
                        
                        [cell.contentView addSubview:mylbl];
                        mylbl.textColor = [UIColor darkGrayColor];
                        mylbl.backgroundColor = [UIColor clearColor];
                        [cell.contentView addSubview:mylbl];
                        
                        
                        UIButton *myAcceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                        myAcceptBtn.tag = [[[catArray objectAtIndex:indexPath.row] valueForKey:@"backendID"] intValue];
                        [tickImage addObject:[NSString stringWithFormat:@"%d",myAcceptBtn.tag]];
                        [myAcceptBtn addTarget:self action:@selector(chckMark:) forControlEvents:UIControlEventTouchUpInside];
                        [myAcceptBtn setFrame:CGRectMake(188, 20, 18, 19)];
                        [myAcceptBtn setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
                        [cell.contentView addSubview:myAcceptBtn];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"");
                    }
                    @finally {
                        
                    }
                }else if(indexPath.section==2){
                    @try {
                        
                        NSArray *catArray=nil;
                        catArray=[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:1]:0];
                        // NSLog(@"No of rows  %d    %@",[catArray count],[[catArray objectAtIndex:indexPath.row] valueForKey:@"name"]);
                        UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
                        [mylbl setNumberOfLines:0];
                        mylbl.numberOfLines = 2;
                        [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                        
                        if(langIsArabic){
                            mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name_arabic"]];
                        }else{
                            mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
                        }
                        
                        
                        [cell.contentView addSubview:mylbl];
                        mylbl.textColor = [UIColor darkGrayColor];
                        mylbl.backgroundColor = [UIColor clearColor];
                        [cell.contentView addSubview:mylbl];
                        
                        
                        UIButton *myAcceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                        myAcceptBtn.tag = [[[catArray objectAtIndex:indexPath.row] valueForKey:@"backendID"] intValue];
                        [tickImage addObject:[NSString stringWithFormat:@"%d",myAcceptBtn.tag]];
                        [myAcceptBtn addTarget:self action:@selector(chckMark:) forControlEvents:UIControlEventTouchUpInside];
                        [myAcceptBtn setFrame:CGRectMake(188, 20, 18, 19)];
                        [myAcceptBtn setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
                        [cell.contentView addSubview:myAcceptBtn];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    @finally {
                        
                    }
                    
                }
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else
    {
        
        CategoryItem *categoryss = (CategoryItem*)[searchResultController objectAtIndexPath:indexPath];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView =[[UIImageView alloc] init];
        cell.selectedBackgroundView =[[UIImageView alloc] init];
        UIImage *unselectedBackground = [UIImage imageNamed:@"iphone_top_bar_bg.png"];
        //((UIImageView *)cell.backgroundView).image = unselectedBackground;
        
        UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(55, 0, 190, 42)];
        
        mylbl.numberOfLines = 2;
        mylbl.text = [NSString stringWithFormat:@"%@",langIsArabic?categoryss.name_arabic:categoryss.name];
        [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        
        mylbl.textColor = [UIColor darkGrayColor];
        mylbl.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mylbl];
        
        UILabel *lblPrice =  [[UILabel alloc] initWithFrame:CGRectMake(250, 10, 300, 25)];
        lblPrice.numberOfLines = 2;
        lblPrice.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%@ SAR",categoryss.price]];
        [lblPrice setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
        lblPrice.textColor = UIColorFromRGB(0x5b1900);
        lblPrice.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:lblPrice];
        
        AsyncImageView *asyncImage = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 2, 40, 40)];
        if ([Functions isIPad]) {
            lblPrice.frame=CGRectMake(450, 10, 300, 25);
            mylbl.frame=CGRectMake(80, 0, 190, 42);
            asyncImage.frame=CGRectMake(20, 0, 40, 40);
        }else{
            
        }
        
        asyncImage.backgroundColor = [UIColor clearColor];
        [asyncImage loadImageFromURL:[NSURL URLWithString:[[self.filteredArray objectAtIndex:indexPath.row]valueForKey:@"imageURL"]]];
        [cell.contentView addSubview:asyncImage];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",indexPath.row,indexPath.section];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(tableView == tblMenu)
    {
        if (cell==nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cell.backgroundView =[[UIImageView alloc] init];
            cell.selectedBackgroundView =[[UIImageView alloc] init];
            UIImage *unselectedBackground = [UIImage imageNamed:@"iphone_top_bar_bg.png"];
            //((UIImageView *)cell.backgroundView).image = unselectedBackground;
            if(indexPath.section==0){
                UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
                [mylbl setNumberOfLines:0];
                mylbl.numberOfLines = 2;
                [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                
                mylbl.text = NSLocalizedString(@"All",@"All");
                [cell.contentView addSubview:mylbl];
                mylbl.textColor = [UIColor darkGrayColor];
                mylbl.backgroundColor = [UIColor clearColor];
                [cell.contentView addSubview:mylbl];
                
                
                UIButton *myAcceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                myAcceptBtn.tag = -33;
                [tickImage addObject:[NSString stringWithFormat:@"%d",myAcceptBtn.tag]];
                [myAcceptBtn addTarget:self action:@selector(chckMark:) forControlEvents:UIControlEventTouchUpInside];
                [myAcceptBtn setFrame:CGRectMake(188, 20, 18, 19)];
                [myAcceptBtn setImage:[UIImage imageNamed:@"icon_tick_mark.png"] forState:UIControlStateNormal];
                [cell.contentView addSubview:myAcceptBtn];
                
                
            }else
            {
                @try {
                    
                    
                    Category *cat=[catParentArray objectAtIndex:indexPath.section-1];
                    
                    NSArray* catArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:[cat.backendID intValue]];
                    
                    
                    UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
                    [mylbl setNumberOfLines:0];
                    mylbl.numberOfLines = 2;
                    [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
                    
                    if(langIsArabic){
                        mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name_arabic"]];
                    }else{
                        mylbl.text = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
                    }
                    
                    [cell.contentView addSubview:mylbl];
                    mylbl.textColor = [UIColor darkGrayColor];
                    mylbl.backgroundColor = [UIColor clearColor];
                    [cell.contentView addSubview:mylbl];
                    
                    
                    UIButton *myAcceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    myAcceptBtn.tag = [[[catArray objectAtIndex:indexPath.row] valueForKey:@"backendID"] intValue];
                    [tickImage addObject:[NSString stringWithFormat:@"%d",myAcceptBtn.tag]];
                    [myAcceptBtn addTarget:self action:@selector(chckMark:) forControlEvents:UIControlEventTouchUpInside];
                    [myAcceptBtn setFrame:CGRectMake(188, 20, 18, 19)];
                    [myAcceptBtn setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
                    [cell.contentView addSubview:myAcceptBtn];
                }
                @catch (NSException *exception) {
                    NSLog(@"");
                }
                @finally {
                    
                }
            }
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else
    {
        
        CategoryItem *categoryss = (CategoryItem*)[searchResultController objectAtIndexPath:indexPath];
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView =[[UIImageView alloc] init];
        cell.selectedBackgroundView =[[UIImageView alloc] init];
        UIImage *unselectedBackground = [UIImage imageNamed:@"iphone_top_bar_bg.png"];
        //((UIImageView *)cell.backgroundView).image = unselectedBackground;
        
        UILabel *mylbl =  [[UILabel alloc] initWithFrame:CGRectMake(55, 0, 190, 42)];
        
        mylbl.numberOfLines = 2;
        mylbl.text = [NSString stringWithFormat:@"%@",langIsArabic?categoryss.name_arabic:categoryss.name];
        [mylbl setFont:[UIFont fontWithName:@"Segoe UI" size:15]];
        
        mylbl.textColor = [UIColor darkGrayColor];
        mylbl.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mylbl];
        
        UILabel *lblPrice =  [[UILabel alloc] initWithFrame:CGRectMake(250, 10, 300, 25)];
        lblPrice.numberOfLines = 2;
        lblPrice.text = [Functions getLocalizedNumber:[NSString stringWithFormat:@"%@ SAR",categoryss.price]];
        [lblPrice setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
        lblPrice.textColor = UIColorFromRGB(0x5b1900);
        lblPrice.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:lblPrice];
        
        AsyncImageView *asyncImage = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 2, 40, 40)];
        if ([Functions isIPad]) {
            lblPrice.frame=CGRectMake(450, 10, 300, 25);
            mylbl.frame=CGRectMake(80, 0, 190, 42);
            asyncImage.frame=CGRectMake(20, 0, 40, 40);
        }else{
            
        }
        
        asyncImage.backgroundColor = [UIColor clearColor];
        [asyncImage loadImageFromURL:[NSURL URLWithString:[[self.filteredArray objectAtIndex:indexPath.row]valueForKey:@"imageURL"]]];
        [cell.contentView addSubview:asyncImage];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
}
-(void)addThis:(id)sender
{
}

-(void)chckMark:(id)sender
{
    for (int cou = 0; cou < [tickImage count];cou++) {
        UIButton *btns = (UIButton *) [self.view viewWithTag:[[tickImage objectAtIndex:cou]intValue]];
        [btns setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
    }
    
    [self fadeOut:@"id" finished:YES target:viewForTable];
    popupTrue = TRUE;
    
    /////////////////// *********************** ///////////////////
    int senderTag = [sender tag];
    //NSLog(@"checkMark %d",senderTag);
    
    
    UIButton *btns = (UIButton *) [self.view viewWithTag:senderTag];
    if(btns.imageView.image == [UIImage imageNamed:@"icon_tick_mark.png"])
    {
        [btns setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btns setImage:[UIImage imageNamed:@"icon_tick_mark.png"] forState:UIControlStateNormal];
        NSArray *catArray=nil;
        catArray=[[DataStoreManager manager] categoriesForParent:[[menuManger menus] objectAtIndex:0]];
        
        
        catorID = senderTag;
        if([catArray count] > 0)
            catArrayLocal = catArray;
        
    }
    /////////////////// *********************** ///////////////////
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tblMenu == tableView)
    {
        /////////////////////////////************ Old cell ************************/
        
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:checkedIndexPath];
        
        for (UIButton *viewsss in [oldCell.contentView subviews])
		{
            if([viewsss isKindOfClass:[UIButton class]])
            {
                [viewsss setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
            }
        }
        /////////////////////////////************ Old cell ************************/
        
        for (int cou = 0; cou < [tickImage count];cou++) {
            UIButton *btns = (UIButton *) [self.view viewWithTag:[[tickImage objectAtIndex:cou]intValue]];
            [btns setImage:[UIImage imageNamed:@"icon_tick_mark_huge.png"] forState:UIControlStateNormal];
        }
        
        
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
        //        if(indexPath.section==0){
        //            catorID=-33;
        //        }else{
        for (UIButton *viewsss in [newCell.contentView subviews])
		{
            if([viewsss isKindOfClass:[UIButton class]])
            {
                //NSLog(@"viewss %d",viewsss.tag);
                
                [viewsss setImage:[UIImage imageNamed:@"icon_tick_mark.png"] forState:UIControlStateNormal];
                
                if(indexPath.section==0){
                    catorID=-33;
                }else{
                    
                    NSArray *catArray=nil;
                    catArray=[[DataStoreManager manager] categoriesForParent:[[menuManger menus] objectAtIndex:0]];
                    @try {
                        NSString *catorIDStr = [NSString stringWithFormat:@"%@",[[catArray objectAtIndex:indexPath.row] valueForKey:@"backendID"]];
                        
                        catorID = viewsss.tag;
                        //NSLog(@"categ %d",catorID);
                        if([catArray count] > 0)
                            catArrayLocal = catArray;
                        
                    }
                    @catch (NSException *exception) {
                        NSLog(@"cath this ");
                    }
                }
            }
        }
        //}
        
        checkedIndexPath = indexPath;
        
        //[viewForTable setHidden:YES];
        //    /[self.view sendSubviewToBack:viewForTable];
        popupTrue = TRUE;
        
        [self fadeOut:@"id" finished:YES target:viewForTable];
        [tblMenu reloadData];
    }
    else
    {
        [self showDetail:indexPath];
        
    }
    
    
}

-(void)imageDownloadedNotification:(NSNotification *) sender{
    
    
}

-(UIScrollView*)menuChanged:(id)sender {
    @try {
        UIView* tempFrame;
        NSArray *catArray=nil;
        
        if ([sender isKindOfClass:[NSString class]]){
            Category* menuItem = [catParentArray objectAtIndex:[sender intValue]];
            catArray=[[DataStoreManager manager] categoriesForWithParentWithoutMenu:[menuItem.backendID intValue]];
            
            // catArray=[[DataStoreManager manager] categoriesForWithParent:[[menuManger menus] objectAtIndex:[sender intValue]]:0];
        }else{
            catArray=[[DataStoreManager manager] categoriesFor:[[menuManger menus] objectAtIndex:[sender tag]]];
        }
        
        int count=[catArray count];
        if (count>0) {
            menuManger.categories=catArray;
            
            /*******************************************UIScrollView****************************************************************/
            UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, (53*([sender intValue]+1))+([sender intValue]*140) , 320, 200)];
            [scrollView setScrollEnabled:YES];
            if([Functions isIPad]){
                scrollView.frame=CGRectMake(0,10+ (53*([sender intValue]+1))+([sender intValue]*175), 552, 230);
            }else{
                
            }
            scrollView.backgroundColor = [UIColor clearColor];
            
            [scrollView setShowsHorizontalScrollIndicator:NO];
            for (UIView *v in scrollView.subviews) {
                [v removeFromSuperview];
            }
            for (int aantal=0; aantal < count; aantal++) {
                /*******************************************Category****************************************************************/
                Category *category=[catArray objectAtIndex:aantal];
                
                //Either Download Image Here or In CategoryParser.Which One Suits You.
                
                NSString *fileName = [Functions localImageThumbName:category.imageURL backendID:[category.backendID intValue]];
                NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
                
                if(![[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    [category startDownloadThumbImage:[[NSString stringWithFormat:@"%d000%d",aantal+1,[sender intValue]] intValue]];
                }
                
                UILabel *labelValue = [[UILabel alloc] init];
                labelValue.alpha=1;
                labelValue.textColor = [UIColor clearColor];
                labelValue.text=[NSString stringWithFormat:@"%@",category.backendID];
                labelValue.backgroundColor = [UIColor clearColor];
                tempFrame=[[UIView alloc]init];
                UIImageView* frameImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"img_holder_half-width.png"]];
                [tempFrame setBackgroundColor:[UIColor clearColor]];
                
                if([Functions isIPad]){
                    frameImage.frame=CGRectMake(0, 0, 235, 170);
                    if([sender intValue]==0){
                        tempFrame.frame=CGRectMake(10+(aantal*250), 10,235, 170);
                    }else{
                        tempFrame.frame=CGRectMake(10+(aantal*250),50,235, 170);
                    }
                    
                }else{
                    frameImage.frame=CGRectMake(0, 0, 200, 120);
                    tempFrame.frame=CGRectMake(3+(aantal*220), 0, 195, 120);
                }
                [tempFrame addSubview:frameImage];
                //tempFrame.frame=thumbImage.frame;
                if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                    thumbImage = [[UIImageView alloc] initWithFrame:CGRectMake(6, 4, 188, 110)];
                    if([Functions isIPad]){
                        thumbImage.frame= CGRectMake(7, 6, 222, 156);
                    }
                    thumbImage.image=[category thumbImage];
                    [tempFrame addSubview:thumbImage];
                    [tempFrame bringSubviewToFront:thumbImage];
                    
                }else{
                    AsyncImageView* asynImageView=[[AsyncImageView alloc]init];
                    [asynImageView setBackgroundColor:[UIColor grayColor]];
                    asynImageView.frame= CGRectMake(6, 4, 188, 110);
                    if([Functions isIPad]){
                        asynImageView.frame= CGRectMake(7, 6, 222, 158);
                    }
                    NSString *stImageURL=category.imageURL;
                    if( ![stImageURL isEqualToString:@""] )
                    {
                        NSURL *imageURL = [NSURL URLWithString:stImageURL];
                        [asynImageView loadImageFromURL:imageURL];
                        //[thumbImage addSubview:asynImageView];
                        [tempFrame addSubview:asynImageView];
                        //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
                        
                    }
                    else
                    {
                        //TODO: show default image.
                    }
                }
                
                tempFrame.tag= [[NSString stringWithFormat:@"%d000%d",aantal+1,[sender intValue]] intValue];
                labelValue.frame=tempFrame.frame;
                
                //NSLog(@"%d",thumbImage.tag);
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
                tap.numberOfTapsRequired = 1;
                tap.cancelsTouchesInView=YES;
                labelValue.userInteractionEnabled = YES;
                [labelValue addGestureRecognizer:tap];
                
                [scrollView addSubview:tempFrame];
                [scrollView addSubview:labelValue];
                
                /*******************************************UITapGestureRecognizer****************************************************/
                
                UILabel *scoreLabel = [ [UILabel alloc ] init];
                [scoreLabel setFrame:CGRectMake(tempFrame.frame.origin.x, CGRectGetMaxY(tempFrame.frame)-35, tempFrame.frame.size.width, 30)];
                if([Functions isIPad]){
                    [scoreLabel setFrame:CGRectMake(tempFrame.frame.origin.x, CGRectGetMaxY(tempFrame.frame)-35, 230, 30)];
                }
                scoreLabel.textAlignment =  UITextAlignmentCenter;
                scoreLabel.textColor = [UIColor grayColor];
                scoreLabel.numberOfLines=0;
                scoreLabel.lineBreakMode=UILineBreakModeWordWrap;
                scoreLabel.backgroundColor = [UIColor whiteColor];
                scoreLabel.alpha = 0.8;
                [scoreLabel setFont:[UIFont fontWithName:@"Segoe UI" size:18]];
                
                scoreLabel.text = langIsArabic?category.name_arabic:category.name;
                //[NSString stringWithFormat: @"%d", score];
                [scrollView addSubview:scoreLabel];
                [scrollView bringSubviewToFront:scoreLabel];
                //thumbImage=nil;
            }
            
            scrollView.multipleTouchEnabled = NO;
            scrollView.contentSize = CGSizeMake((count*25)+tempFrame.frame.size.width*count, 200);
            scrollView.userInteractionEnabled=YES;
            scrollView.delaysContentTouches=NO;
            //[self.view addSubview:scrollView];
            return scrollView;
            
            
        }else{
            
            return nil;
        }
        
        
        /*  CATransition *animation=[CATransition animation];
         [animation setDelegate:self];
         [animation setDuration:0.75];
         [animation setType:@"genieEffect"];
         [animation setFillMode:kCAFillModeRemoved];
         animation.endProgress=0.99;
         [animation setRemovedOnCompletion:YES];
         [self.view.layer addAnimation:animation forKey:nil];*/
    }
    @catch (NSException *exception) {
        NSLog(@"bhandaa occur ");
        //[Functions showAlert:@"No Items" message:@"Sorry no items are there in this category"];
    }
    
    
    
}

- (void)menuNotification:(NSNotification *)notification{
    
    [self menuChanged:notification.object];
    
}
- (void)viewDidUnload
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCategoryImageDownloaded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeMenu" object:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewWillAppear:(BOOL)animated{
        
    
    if([self.filteredArray count]>0){
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [layOutView removeFromSuperview];
        
    }
    [self setOrderQuantity];
    
    //self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
	
}

-(IBAction)back:(id)sender{
    
    
    /* CATransition *animation=[CATransition animation];
     [animation setDelegate:self];
     [animation setDuration:0.75];
     [animation setType:@"genieEffect"];
     [animation setFillMode:kCAFillModeRemoved];
     
     animation.endProgress=0.99;
     [animation setRemovedOnCompletion:YES];
     [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    [[self navigationController] popViewControllerAnimated:YES];
}


-(IBAction)actionOrderlist:(id)sender{
    @try {
        
        
        OrderListController *controller=[[OrderListController alloc] initWithNibName:@"OrderListController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"OrderList");
    }
    
    
}

-(void)setupLeftMenuButton{
   
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [txtSearch resignFirstResponder];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [txtSearch resignFirstResponder];
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}


#pragma mark-
#pragma mark-Setting Order Quantity
-(void)setOrderQuantity{
    @try {
        Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
        int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
        [btnOrderQty setTitle:[NSString stringWithFormat:@"%d",qtyTotal] forState:UIControlStateNormal];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
}


#pragma mark ANIMATION

//-------fade in

- (void)fadeIn:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    [target setAlpha:1.0f];
    [UIView commitAnimations];
}

- (void)fadeOut:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    if ([target alpha] == 1.0f)
        [target setAlpha:0.0f];
    else
        [target setAlpha:1.0f];
    [UIView commitAnimations];
}

- (void)fadeOutSecond:(NSString *)animationId finished:(BOOL)finished target:(UIView *)target
{
    [UIView beginAnimations:animationId context:(__bridge void *)(target)];
    [UIView setAnimationDuration:0.9f];
    [UIView setAnimationDelegate:self];
    [target setAlpha:0.0f];
    
    [UIView commitAnimations];
}



#pragma mark add order popup

-(IBAction) OkPopup:(id)sender{
    
    
    [UIView animateWithDuration:1
                          delay:0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         addOrderView.alpha =0;
                         //
                         //self.basketTop.frame = basketTopFrame;
                         //self.basketBottom.frame = basketBottomFrame;
                     }
                     completion:^(BOOL finished){
                         [addOrderView removeFromSuperview];
                         addOrderView.alpha=1;
                         
                         NSLog(@"Done!");
                     }];
    
    
}


-(IBAction) addOrderPopup:(id)sender{
    
    
    Order *cashTotal=[[DataStoreManager manager] getTotalQuantity];
    int qtyTotal = [[cashTotal valueForKeyPath:@"@sum.quantity"] intValue] ;
    
    if(qtyTotal>0){
        [[SingletonClass sharedInstance] setNumberOfPeople:[lblNumber.text intValue]];
        NSString* alertText = NSLocalizedString(@"Do you want to discard previous order.",@"Do you want to discard previous order.");
        NSString* alertCancleBtn=NSLocalizedString(@"Caution", @"Caution");
        [Functions showAlertCancel:alertCancleBtn message:alertText:self];
        alertText=nil;
    }else{
        
        [self OkPopup:nil];
    }
    
    cashTotal = nil;
    
}

#pragma mark delivery / dine in switch button
-(IBAction)actionSwitch:(id)sender{
    
    if(btnSwitch.selected){
        btnSwitch.selected = NO;
        [Functions setOrderType:0];
        
    }
    else{
        btnSwitch.selected = YES;
        [Functions setOrderType:1];
    }
}
-(void)setViewMovedUp:(BOOL)movedUp view:(UIView*)senderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = senderView.frame;
    if (movedUp)
    {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y = -kOFFSET_FOR_KEYBOARD;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        // revert back to the normal state.
        rect.origin.y = kOFFSET_FOR_KEYBOARD;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    senderView.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0){
        
        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //[defaults setBool:YES forKey:kaddOrderPopup];
        
        [[DataStoreManager manager] clearOrder];
        [[DataStoreManager manager] saveData];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrdersItemAdded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                                      [NSNumber numberWithBool:YES], keyStatus,nil]];
        
        [UIView animateWithDuration:1
                              delay:0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             addOrderView.alpha =0;
                         }
                         completion:^(BOOL finished){
                             [addOrderView removeFromSuperview];
                             addOrderView.alpha=1;
                             
                             
                         }];
        
    }else{
        
        [self OkPopup:nil];
    }
    
    NSLog(@"%d",buttonIndex);
    
}

@end
