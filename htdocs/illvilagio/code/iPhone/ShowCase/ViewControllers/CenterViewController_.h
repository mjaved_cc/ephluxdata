//
//  CenterViewController.h
//  SideViews
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 salman.ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *homeScrollView;
}
@end
