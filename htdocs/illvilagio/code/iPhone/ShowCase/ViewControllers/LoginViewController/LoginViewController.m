//
//  LoginViewController.m
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LoginViewController.h"
#import "RootViewController.h"
#import "menuTableViewViewController.h"
#import "Defines.h"
#import "DataStoreManager + DataSpecialized.h"
//#import "Configure.h"
#import "MBProgressHUD.h"
#import "OrderListController.h"
#import "UIViewController+Container.h"
#import "HomeViewController.h"
#import "AboutViewController.h"
#import "EventsViewController.h"
#import "ReservationViewController.h"
#import "FeedBackViewController.h"
#import "GalleryViewController.h"
#import "MapViewController.h"
#import "CenterViewController.h"
#import "RootViewController_iPhone.h"
#import "OrderCustomerViewController.h"
#import "OrderCustomerConfirmViewController.h"
#import "FloorPlanneViewController.h"
@implementation LoginViewController

@synthesize notifierUpdateTimer;
@synthesize progressHud;
@synthesize lblHeaderBar;
@synthesize orderController;

-(IBAction)actionLogin:(id)sender{
    
    self.navigationController.navigationBarHidden=NO;
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    //[[DataStoreManager manager] fetchMenu];
    
    //
    
    /*  NSArray *array=[[DataStoreManager manager] getConfigurationData];
     [self.progressHud show:YES];
     
     NSLog(@"%@",array);
     
     if ([array count]>0) {
     //pincode =[[array valueForKey:@"passCode"] objectAtIndex:0];
     [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
     [[DataStoreManager manager] doLogin:@"0" password:pincode];
     }else{
     
     }*/
    
}


-(void)addProressView:(id)sender{
    
    [self.view addSubview:self.progressHud];
    
}

-(IBAction)configuration:(id)sender{
    
    BOOL isConnectedWithInternet = [InternetCheck IsConnected];
    
    if (isConnectedWithInternet) {
        /* Configure *anotherViewController=[[Configure alloc] initWithNibName:@"CheckinViewController" bundle:nil];
         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:anotherViewController] ;
         navController.navigationBarHidden=YES;
         navController.navigationBar.tintColor=[UIColor blackColor];
         navController.modalPresentationStyle = UIModalPresentationFormSheet;
         [navController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
         [self presentModalViewController:navController animated:YES];
         if([Functions isIPad])
         {
         navController.view.superview.frame=CGRectMake(0, 0, 320, 460);
         navController.view.superview.center=self.view.center;
         }
         */
    }else{
        [Functions showAlert:@"Not Connected to wifi" message:@"Thank You"];
        
    }
    /*CATransition *animation=[CATransition animation];
     [animation setDelegate:self];
     [animation setDuration:0.75];
     [animation setType:@"genieEffect"];
     [animation setFillMode:kCAFillModeRemoved];
     
     animation.endProgress=0.99;
     [animation setRemovedOnCompletion:YES];
     [self.navigationController.view.layer addAnimation:animation forKey:nil];*/
    
}

-(IBAction)gotoMenuView:(id)sender{
    
    menuTableViewViewController *rootViewController=[[menuTableViewViewController alloc] initWithNibName:@"menuTableViewViewController" bundle:nil];
    [[self navigationController] pushViewController:rootViewController animated:YES];
    
}


-(void)refreshChildViewTable{
    
    if([orderController isViewLoaded]){
        
        [orderController.tableViewOrder reloadData];
        
    }
    
}




-(IBAction)addHomeMenuView:(id)sender{
    
    menuTableViewViewController *rootViewController=[[menuTableViewViewController alloc] initWithNibNameAndFrame:@"menuTableViewViewController" bundle:nil frame:CGRectMake(0, 0, 236, 760)];
    
    [self addChildViewController:rootViewController];//NEWLY
    [self.view addSubview:rootViewController.view];
    [rootViewController didMoveToParentViewController:self];
    
}

-(void)addOrderTableView:(id)sender{
    
    orderController=[[OrderListController alloc] initWithNibNameAndFrame:@"OrderListController" bundle:nil frame:CGRectMake(793, 0, 240, 768)];
    
    [self addChildViewController:orderController];
    [self.view addSubview:orderController.view];
    
    
}
#pragma mark removeChildViewController
-(void) removeChildViewController{
    if([self.childViewControllers count]>2){
        //NSLog(@"Child VC in childVC Befor remove array: %d", [self.childViewControllers count]);
        [[self.childViewControllers lastObject] willMoveToParentViewController:nil];
        [[[self.childViewControllers lastObject] view] removeFromSuperview];
        [[self.childViewControllers lastObject] removeFromParentViewController];
        //NSLog(@"Child VC in childVC array: %d", [self.childViewControllers count]);
        
    }
}

-(void)addHomeView:(id)sender{
    
    [Functions setNavigationBar];
    
    
    switch (selectedMenuIndex) {
        case 1:
        {
            [self removeChildViewController];
            CenterViewController *vC = [[CenterViewController alloc] initWithNibName:@"ipad_CenterViewController" bundle:nil];
            contentController=vC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:vC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
            
        case 2:
        {
            
            [self removeChildViewController];
            RootViewController_iPhone *rootViewController=[[RootViewController_iPhone alloc] initWithNibName:@"ipad_RootViewController" bundle:nil];
            rootViewController.isOrderView = FALSE;
            contentController=rootViewController;
            
            if([sender boolValue]){
                rootViewController.isOrderView = TRUE;
            }
            
            UINavigationController *ncMenu = [[UINavigationController alloc] initWithRootViewController:rootViewController];
            ncMenu.view.frame = CGRectMake(240, 0, 550, 768);
            
            [ncMenu setNavigationBarHidden:NO];
            [self addChildViewController:ncMenu];
            [self.view addSubview:ncMenu.view];
            NSLog(@"cese 2");
        }
            break;
            
        case 3:
        {
            [self removeChildViewController];
            ReservationViewController *vC = [[ReservationViewController alloc] initWithNibName:@"ipad_RevervationVC" bundle:nil];
            contentController=vC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:vC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
        case 4:
        {
            [self removeChildViewController];
            EventsViewController *vC = [[EventsViewController alloc] initWithNibName:@"ipad_EventViewController" bundle:nil];
            contentController=vC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:vC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
        case 5:
        {
            [self removeChildViewController];
            GalleryViewController *vC = [[GalleryViewController alloc] initWithNibName:@"ipad_GalleryVC" bundle:nil];
            contentController=vC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:vC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
        case 6:
        {
            [self removeChildViewController];
            AboutViewController  *aboutVC=[[AboutViewController alloc]initWithNibName:@"ipad_AboutViewController" bundle:nil];
            
            //HomeViewController *vC = [[HomeViewController alloc] initWithFrame:CGRectMake(0, 0, 560, 768)];
            contentController=aboutVC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:aboutVC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
        case 7:
        {
            [self removeChildViewController];
            FeedBackViewController  *aboutVC=[[FeedBackViewController alloc]initWithNibName:@"ipad_FeedbackVC" bundle:nil];
            
            //HomeViewController *vC = [[HomeViewController alloc] initWithFrame:CGRectMake(0, 0, 560, 768)];
            contentController=aboutVC;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:aboutVC];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
            
        case 8:
        {
            [self removeChildViewController];
            MapViewController  *mapView=[[MapViewController alloc]initWithNibName:@"ipad_MapViewController" bundle:nil];
            
            contentController=mapView;
            UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:mapView];
            ncHome.view.frame = CGRectMake(240,0, 552, 768);
            [ncHome setNavigationBarHidden:NO];
            [self addChildViewController:ncHome];
            
            [self.view addSubview:ncHome.view];
            
            
            
        }
            break;
            
            
        default:
            break;
    }
    
    
    
    
}
-(void) orderPlace:(id) sender{
    [self removeChildViewController];
    if([Functions isDeliveryTarget]==FALSE){
        FloorPlanneViewController  *temp=[[FloorPlanneViewController alloc]initWithNibName:@"ipad_FloorPlanneVC" bundle:nil];        
        contentController=temp;
    }else{
        
        OrderCustomerViewController  *temp=[[OrderCustomerViewController alloc]initWithNibName:@"ipad_OrderCustomerViewController" bundle:nil];
        contentController=temp;
    }
    
    UINavigationController *ncHome = [[UINavigationController alloc] initWithRootViewController:contentController];
    ncHome.view.frame = CGRectMake(240,0, 552, 768);
    [ncHome setNavigationBarHidden:NO];
    [self addChildViewController:ncHome];
    [self.view addSubview:ncHome.view];
}

-(void)menuDidSelect:(NSNotification*)notification {
    
    
    NSDictionary *userInfo = notification.userInfo;
    
    if([userInfo count]){
        
        NSLog(@"coming from new order");
        
        selectedMenuIndex = [notification.object row]+1;
        
        contentController.view =nil;
        
        [self addHomeView:[NSNumber numberWithBool:YES]];
        
    }else{
        
        selectedMenuIndex = [notification.object row]+1;
        
        contentController.view =nil;
        
        [self addHomeView:NO];
    }
    
    
    
}


-(void)loginDelegateHandler:(NSNotification*)notification {
    
	[[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    NSDictionary *dict = notification.userInfo;
    if([[dict objectForKey:keyStatus] boolValue] == NO) {
        //return;
    }
    [self.progressHud hide:YES];
    
}


-(IBAction)btnActionTapped:(id)sender{
    
    CATransition *animation=[CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:1.75];
    [animation setType:@"genieEffect"];
    [animation setFillMode:kCAFillModeRemoved];
    animation.endProgress=0.99;
    [animation setRemovedOnCompletion:YES];
    [self.view.layer addAnimation:animation forKey:nil];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)rippleEffect{
    CATransition *animation=[CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:1.75];
    [animation setType:@"genieEffect"];
    
    [animation setFillMode:kCAFillModeRemoved];
    animation.endProgress=0.99;
    [animation setRemovedOnCompletion:NO];
    
    [self.view.layer addAnimation:animation forKey:nil];
    
    
}
-(IBAction)actionCloseSplash:(id)sender {
	[buttonSplash removeFromSuperview];
}
-(void)splash_hide{
    buttonSplash.alpha = 0;
    splash_view.hidden = YES;
}

- (void)viewDidLoad
{
    self.navigationController.navigationBarHidden=YES;
	
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDelegateHandler:) name:kNotificationUserLogin object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuDidSelect:) name:kNotificationMenuClick object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderPlace:) name:kNotificationPlaceOrderClick object:nil];
    
    [self.view addSubview:splash_view];
    splash_view.hidden = NO;
    
	[AnimationUtility fadeOut:splash_view withDuration:1];
    
    self.progressHud = [[MBProgressHUD alloc] initWithView:self.view] ;
    self.progressHud.labelText = @"Menu Syncing";
    
    
    selectedMenuIndex=1;
    
    [self addViews];
    
    // set header Bar
    self.lblHeaderBar.textColor = UIColorFromRGB(0x5b1900);
    self.lblHeaderBar.font = [UIFont boldSystemFontOfSize:23.0];
    [self.lblHeaderBar setFont:[UIFont fontWithName:@"Script MT bold" size:23]];
    
    //[self actionLogin:nil];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

-(void)testMethod:(id)sender{
    
    NSLog(@"button testMethod");
    [self.progressHud show:YES];
    //[self.progressHud hide:NO];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    //[[DataStoreManager manager] fetchMenu];
    
}

-(void)addViews{
    
    
    [self performSelector:@selector(addHomeMenuView:) withObject:nil afterDelay:1];
    
    [self performSelector:@selector(addOrderTableView:) withObject:nil afterDelay:1];
    
    [self performSelector:@selector(addHomeView:) withObject:nil afterDelay:1];
    [self performSelector:@selector(addProressView:) withObject:nil afterDelay:1.1];
    
}




-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    @try {
        
        [self performSelector:@selector(refreshChildViewTable) withObject:nil afterDelay:0];
        
        self.navigationController.navigationBarHidden=YES;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:YES forKey:kSettingEnableSound];
        
        
    }
    
    @catch (NSException *exception) {
        NSLog(@"Exception ");
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    //On 8th Feb
    //if (appDelegate.global.inKitchen==YES) {
    @try {
        self.navigationController.navigationBarHidden=NO;
        
        /* if ([self.notifierUpdateTimer isValid]) {
         [self.notifierUpdateTimer invalidate];
         self.notifierUpdateTimer=nil;
         }*/
    }
    @catch (NSException *exception) {
        NSLog(@"Timer Exception");
    }
    
}
- (void)viewDidUnload
{
    if (![self.progressHud isHidden])
        [self.progressHud hide:NO];
    self.progressHud = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
-(IBAction)goToMenu:(id)sender{
    
    [self performSelector:@selector(actionLogin:) withObject:nil afterDelay:1];
    
}


@end
