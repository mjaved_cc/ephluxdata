//
//  LoginViewController.h
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MBProgressHUD;
@class OrderListController;

@interface LoginViewController : UIViewController{

	IBOutlet UIButton *buttonSplash;
    IBOutlet UIView* splash_view;
    //IBOutlet UIView* menuView;
    //IBOutlet UIView* orderView;
    IBOutlet UILabel* lblHeaderBar;
    OrderListController *orderController;
    NSString * pincode ;//=@"4897";
    UIViewController *contentController;
    int selectedMenuIndex;
}

-(IBAction)goToMenu:(id)sender;
-(IBAction)actionLogin:(id)sender;
-(IBAction)configuration:(id)sender;
-(IBAction)gotoMenuView:(id)sender;
-(IBAction)actionCloseSplash:(id)sender;

@property (nonatomic, assign) NSTimer *notifierUpdateTimer;//On 8th Feb
@property (nonatomic, retain) MBProgressHUD *progressHud;
@property (nonatomic, retain) OrderListController *orderController;
//@property (nonatomic, retain) IBOutlet UIView * menuView;
//@property (nonatomic, retain) IBOutlet UIView * orderView;

@property (nonatomic, retain) IBOutlet UILabel* lblHeaderBar;

@end
