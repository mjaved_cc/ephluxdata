//
//  GalleryDetailViewController.h
//  ShowCase
//
//  Created by salman ahmed on 6/3/13.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "PageControl.h"
@interface GalleryDetailViewController : UIViewController<UIScrollViewDelegate,PageControlDelegate>
{
    PageControl             *pageControl;
    IBOutlet UIScrollView            *scrollView;
    IBOutlet UIButton       *btnNext;
    IBOutlet UIButton       *btnPrevious;
    
}
@property (nonatomic,strong) NSMutableDictionary    *tempDic;
@property (nonatomic,strong) NSMutableArray         *galleryArray;
@property (nonatomic,assign) int                    selectedIndex;

-(IBAction)NextPreviousAction:(id)sender;
@end
