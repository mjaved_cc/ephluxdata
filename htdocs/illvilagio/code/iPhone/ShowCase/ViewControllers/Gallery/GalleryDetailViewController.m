//
//  GalleryDetailViewController.m
//  ShowCase
//
//  Created by salman ahmed on 6/3/13.
//
//

#import "GalleryDetailViewController.h"

@interface GalleryDetailViewController ()

@end

@implementation GalleryDetailViewController
@synthesize tempDic,galleryArray,selectedIndex;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self createImages];
    [self setBackBarButton];
    if([Functions isIPad]){
        
    }else{
        
    }if(IS_IPHONE5 ){
        
        
    }else{
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
}
- (void)createImages {
    if(selectedIndex==0){
        [btnNext setEnabled:YES];
        [btnPrevious setEnabled:NO];
    }else if(selectedIndex ==[galleryArray count]-1 ){
        [btnNext setEnabled:NO];
        [btnPrevious setEnabled:YES];
    }
    //scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 9.0,320,416)];
    scrollView.delegate = self;
    [scrollView setBackgroundColor:[UIColor clearColor]];
	scrollView.pagingEnabled = YES;
    int numberOfViews=[galleryArray count];
	for (int i = 0; i <numberOfViews; i++) {
        AsyncImageView *asynImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0,0,scrollView.frame.size.width, scrollView.frame.size.height)];
        UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(scrollView.frame.size.width * i, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        asynImageView.backgroundColor = [UIColor clearColor];
        asynImageView.contentMode = UIViewContentModeScaleAspectFit;
        NSString *stImageURL=[[galleryArray objectAtIndex:i] objectForKey:@"imageUrl"];
        
        if( ![stImageURL isEqualToString:@""] )
        {
            NSURL *imageURL = [NSURL URLWithString:stImageURL];
            [asynImageView loadImageFromURL:imageURL];
            //[aView loadImageFromURL:imageURL withSize:CGSizeMake(92, 92)];
            
        }
        else
        {
            //TODO: show default image.
        }
        [viewFrame addSubview:asynImageView];
        viewFrame.backgroundColor = [UIColor clearColor];
		[scrollView addSubview:viewFrame];
    }
    [scrollView setBounces:NO];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * numberOfViews, scrollView.frame.size.height);
    
    pageControl = [[PageControl alloc] init ];
    pageControl.frame = CGRectMake(10.0,380.0,320.0,15.0);
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.delegate = self;
    pageControl.numberOfPages = numberOfViews;
    pageControl.currentPage = selectedIndex;
    //[self.view addSubview:pageControl];
    //[self.view addSubview:largeGalleryView];
    //[largeGalleryView bringSubviewToFront:closeBtn];
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * selectedIndex;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    [self.view bringSubviewToFront:btnNext];
    [self.view bringSubviewToFront:btnPrevious];
    self.title=[[galleryArray objectAtIndex:selectedIndex]objectForKey:@"Title"];
    
}
#pragma mark Scrollview and PageControll Delegate
- (void) scrollViewDidScroll: (UIScrollView *) aScrollView
{
	CGPoint offset = aScrollView.contentOffset;
    
	pageControl.currentPage = offset.x / 320.0f;
    //selectedIndex=pageControl.currentPage;
    //self.title=[[galleryArray objectAtIndex:selectedIndex]objectForKey:@"Title"];
}
-(void) setTitle_:(int)pageNo{
    //NSLog(@"Last %d , page no %d ",lastPageNo,pageNo);
    
    //
    //    if(pageNo==0 ||pageNo==1 ){
    //        lblTitle.text=@";
    //      }
}
- (void)pageControlPageDidChange:(PageControl *)pageControler{
    int page = pageControler.currentPage;
    
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
}
-(IBAction)NextPreviousAction:(id)sender{
    [btnNext setEnabled:YES];
    [btnPrevious setEnabled:YES];
    //NSLog(@"current index %d",selectedIndex);
    if([sender tag]==0 && selectedIndex<[galleryArray count] && selectedIndex>0){
        selectedIndex--;
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * selectedIndex;
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
        self.title=[[galleryArray objectAtIndex:selectedIndex]objectForKey:@"Title"];
        
    }else if([sender tag]==1 && selectedIndex<[galleryArray count]-1 ){
        selectedIndex++;
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * selectedIndex;
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
        self.title=[[galleryArray objectAtIndex:selectedIndex]objectForKey:@"Title"];
        
    }
    
    if(selectedIndex==0){
        [btnNext setEnabled:YES];
        [btnPrevious setEnabled:NO];
    }else if(selectedIndex ==[galleryArray count]-1 ){
        [btnNext setEnabled:NO];
        [btnPrevious setEnabled:YES];
    }
    
}
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
