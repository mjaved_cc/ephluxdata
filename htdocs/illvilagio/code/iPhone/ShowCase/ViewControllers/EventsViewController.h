//
//  MyRightViewController.h
//  MMDrawerControllerKitchenSink
//
//  Created by salman ahmed on 5/20/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MMDrawerController.h"
@interface EventsViewController : UIViewController<UIGestureRecognizerDelegate>{
    
    NSMutableArray          *eventArray;
    NSInteger               numberOfViews;   
    NSMutableArray          *currentEventArray;
    NSMutableArray          *archiveEventArray;
    int viewHeight;
    int viewWidth;
    int distanceBetweenImages;
    int imageWidth;
    int currentIndex;
    
    IBOutlet UIScrollView  *currentScrollView;
    IBOutlet UIScrollView  *archiveScrollView;
    IBOutlet UILabel       *lblCurrentEventNotAvilable;
    IBOutlet UILabel       *lblArchiveEventNotAvilable;
    IBOutlet UILabel       *lblArchive;
    
}

-(void) ComapreDate:(NSString*) Date;
@end
