//
//  ReservationViewController.m
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import "ReservationViewController.h"
#import "ReservationFormVC.h"
@interface ReservationViewController ()

@end

@implementation ReservationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString( @"Reservations",@"Reservations");
        [Functions setNavigationBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //setup iphone button
    lblReservation.text=NSLocalizedString(@"You currently don't have a reservation", @"You currently don't have a reservation");
    [btnReserver setTitle:NSLocalizedString(@"Create One", @"Create One") forState:UIControlStateNormal];
    [btnReserver setTitle:NSLocalizedString(@"Create One", @"Create One") forState:UIControlStateHighlighted];
    [btnReserver setTitle:NSLocalizedString(@"Create One", @"Create One") forState:UIControlStateSelected];
    
    if([Functions isIPad]){
         [lblReservation setFont:[UIFont fontWithName:@"Segoe UI" size:24.0f]];
      
    }else{
        [self setupLeftMenuButton];
        [self setupRightMenuButton];
        [lblReservation setFont:[UIFont fontWithName:@"Segoe UI" size:20.0f]];
    }
    
}
-(void) viewDidAppear:(BOOL)animated{
    btnReserver.selected=NO;
}
-(IBAction)MoveOnReservation:(id)sender{
    if([Functions isIPad]){
        btnReserver.selected=YES;
       ReservationFormVC* reservationVC=[[ReservationFormVC alloc]initWithNibName:@"ipad_ReservationFormVC" bundle:nil];
        [self.navigationController pushViewController:reservationVC animated:YES];
    }else{
        btnReserver.selected=YES;
        ReservationFormVC *reservationVC=[[ReservationFormVC alloc]initWithNibName:@"ReservationFormVC" bundle:nil];
        [self.navigationController pushViewController:reservationVC animated:YES];
    }
   
}

-(void)setupLeftMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:1];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    [[SingletonClass sharedInstance] setButtonTag:0];
    MMDrawerBarButtonItem * rightDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightDrawerButton animated:YES];
}
#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
