//
//  ReservationViewController.h
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import <UIKit/UIKit.h>

@interface ReservationViewController : UIViewController
{
    IBOutlet UIButton   *btnReserver;
    IBOutlet UILabel    *lblReservation;
}

@end
