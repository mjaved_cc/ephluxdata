//
//  ReservationFormVC.m
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import "ReservationFormVC.h"
#import "FloorPlanneViewController.h"
#import "CenterViewController.h"

@interface ReservationFormVC ()

@end

@implementation ReservationFormVC
@synthesize eventDate,progressHud;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString( @"Reservation Form",@"Reservation Form");
        [Functions setNavigationBar];
    }
    return self;
}

-(void)setBackBarButton{
    
    UIBarButtonItem *backButton = [Functions setBackBarButton:self];
    [self.navigationItem setLeftBarButtonItem:backButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.progressHud = [[MBProgressHUD alloc] initWithView:self.view] ;
    self.progressHud.labelText =NSLocalizedString(@"Loading...",@"Loading...");
    [self.view addSubview:self.progressHud];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reservationNotification:) name:kNotificationReservation object:nil];
    
    
    [self setLocalizeView];
    [self setBackBarButton];
    if(self.eventDate!=Nil){
        [btnDate setTitle:[Functions getLocalizedDate:[Functions dateFormate:[self.eventDate objectForKey:@"event_time"]]] forState:UIControlStateNormal];
        eventId=[self.eventDate objectForKey:@"id"];
    }else{
        eventId=@"0";
    }
    [[SingletonClass sharedInstance] setTableName:NSLocalizedString(@"Select Table",@"Select Table")];
    
    if([Functions isIPad]){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
        scrollableForm.contentSize = CGSizeMake(500,800);
    }else{
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:)],
                               nil];
        [numberToolbar sizeToFit];
        txtNoOfChildren.inputAccessoryView = numberToolbar;
        txtOldAge.inputAccessoryView = numberToolbar;
        txtPhoneNo.inputAccessoryView = numberToolbar;
        txtGuest.inputAccessoryView = numberToolbar;
       if(IS_IPHONE5 ){
            scrollableForm.contentSize = CGSizeMake(320,950);
        }else{
            scrollableForm.contentSize = CGSizeMake(320,1000);
        }
    }
    
}
#pragma mark Keyboard Hide
-(void)keyboardWillHide
{
    //call when key board hides
    [self animateTextField: Nil up: NO distance:0];
}
-(void) setLocalizeView{
    NSString* btnSelectDate=NSLocalizedString(@"Select Date",@"Select Date");
    NSString* btnSelectTime=NSLocalizedString(@"Select Time",@"Select Time");
    NSString* btnSelectTable=NSLocalizedString(@"Select Table",@"Select Table");
    NSString* btnReserveTitle=NSLocalizedString(@"Reserve",@"Reserve");
    [btnDate setTitle:btnSelectDate forState:UIControlStateNormal];
    [btnTime setTitle:btnSelectTime forState:UIControlStateNormal];
    [btnTableSelect setTitle:btnSelectTable forState:UIControlStateNormal];
    [btnReserve setTitle:btnReserveTitle forState:UIControlStateNormal];
    
    [txtGuest setPlaceholder:NSLocalizedString(@"No of guest", @"No of guest")];
    [txtEmail setPlaceholder:NSLocalizedString(@"Email", @"Email")];
    [txtNoOfChildren setPlaceholder:NSLocalizedString(@"No. of Child", @"No. of Child")];
    [txtOldAge setPlaceholder:NSLocalizedString(@"No. of Oldage", @"No. of Oldage")];
    [txtPhoneNo setPlaceholder:NSLocalizedString(@"Phone no", @"Phone no")];
    [lblDate setText:NSLocalizedString(@"Date", @"Date")];
    [lblTime setText:NSLocalizedString(@"Time", @"Time")];
    [lblTable setText:NSLocalizedString(@"Table", @"Table")];
    [lblGuest setText:NSLocalizedString(@"Guest", @"Guest")];
    [lblChild setText:NSLocalizedString(@"Child", @"Child")];
    [lblAge setText:NSLocalizedString(@"Old age/Disabled", @"Old age/Disabled")];
    [lblEmail setText:NSLocalizedString(@"Email", @"Email")];
    [lblConfrimView setText:NSLocalizedString(@"Confrim Via", @"Confrim Via")];
    [lblNotificationEmail setText:NSLocalizedString(@"Email", @"Email")];
    [lblPhoneNo setText:NSLocalizedString(@"Phone Number", @"Phone Number")];
    [lblNotification setText:NSLocalizedString(@"Notification ", @"Notification ")];
    [lblMidTitle setText:NSLocalizedString(@"Fill in the form to reserve the Table for yourself.", @"Fill in the form to reserve the Table for yourself.")];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    btnReserve.selected=NO;
    [btnTableSelect setTitle:[[SingletonClass sharedInstance] tableName] forState:UIControlStateNormal];
    [[SingletonClass sharedInstance] setTableName:NSLocalizedString(@"Select Table",@"Select Table")];
    
}
#pragma mark reservation Notifire
-(void) reservationNotification:(NSNotification*)notification{
    @try {
        [self.progressHud hide:YES];
        NSDictionary* resultJSON=notification.userInfo;
        if([[[resultJSON objectForKey:@"status"] objectForKey:@"status"]  intValue]!=1){
            NSString* alertTitletxt=NSLocalizedString(@"Reservetion",@"Reservetion");
            NSString* alertMsg=NSLocalizedString(@"Fail submitted",@"Fail submitted");
            [Functions showAlert:alertTitletxt message:alertMsg];
        }else{
            [self ClearFome];
            NSString* alertTitletxt=NSLocalizedString(@"Reservetion",@"Reservetion");
            NSString* alertMsg=NSLocalizedString(@"successfully submitted",@"successfully submitted");
            [Functions showAlert:alertTitletxt message:alertMsg];
            if([Functions isIPad]){
                NSIndexPath * ndxPath= [NSIndexPath indexPathForRow:0 inSection:0];
               [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuClick object:ndxPath];
            }else{
            CenterViewController * homeViewController = [[CenterViewController alloc] init];
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:homeViewController];
            [self.mm_drawerController
             setCenterViewController:nav
             withCloseAnimation:YES
             completion:nil];
            }

            
        }
    
}
@catch (NSException *exception) {
    
}
@finally {
    
}

}
-(IBAction)onClick:(id)sender{
    
    if([Functions isIPad]){
        if([sender tag]==0){
            btnTag=0;
            [self ipad_datePickerSheet:sender];
        }else if([sender tag]==1){
            btnTag=1;
            [self ipad_datePickerSheet:sender];
        }else
            if([sender tag]==2){
                FloorPlanneViewController* floorplaneVC=[[FloorPlanneViewController alloc]initWithNibName:@"ipad_FloorPlanneVC" bundle:nil];
                floorplaneVC.fromReservation=TRUE;
                if(![btnTableSelect.titleLabel.text isEqualToString:NSLocalizedString(@"Select Table",@"Select Table")])
                    floorplaneVC.tableName=btnTableSelect.titleLabel.text;
                [self.navigationController pushViewController:floorplaneVC animated:YES];
            }
    }else{
        if([sender tag]==0){
            btnTag=0;
            [self datePickerSheet:sender];
        }else if([sender tag]==1){
            btnTag=1;
            [self datePickerSheet:sender];
        }else if([sender tag]==2){
            FloorPlanneViewController*  floorplaneVC=[[FloorPlanneViewController alloc]initWithNibName:@"FloorPlanneViewController" bundle:nil];
             floorplaneVC.fromReservation=TRUE;
            if(![btnTableSelect.titleLabel.text isEqualToString:NSLocalizedString(@"Select Table",@"Select Table")])
                floorplaneVC.tableName=btnTableSelect.titleLabel.text;
            
            [self.navigationController pushViewController:floorplaneVC animated:YES];
        }
    }
    
}
-(NSString*) FloorIdes{
    strFloors=@"";
    NSRange rangeValue = [btnTableSelect.titleLabel.text rangeOfString:@"M" options:NSCaseInsensitiveSearch];
    if (rangeValue.length>0){
        strFloors=@"2";
    }
    NSRange rangeValue2 = [btnTableSelect.titleLabel.text rangeOfString:@"G" options:NSCaseInsensitiveSearch];
    if (rangeValue2.length>0){
        if([strFloors isEqualToString:@""]){
            strFloors=@"1";
        }else
            strFloors=[NSString stringWithFormat:@"%@,1",strFloors];
    }
    NSLog(@"found %@",strFloors);
    return strFloors;
}
#pragma mark Reservation post
-(IBAction)postReservationRequest:(id)sender{
    //TODO: webservice work
    if([self validateFome]){
        BOOL isConnectedWithInternet = [InternetCheck IsConnected];
        
        if (isConnectedWithInternet) {
            [self.progressHud show:YES];
            
            btnReserve.selected=YES;
            BOOL emailFlag=btnSwitch.selected;
            BOOL notificationFlag=btnNotificationSwitch.selected;
            
            //NSLog(@"%@ %d",fomeArray,btnSwitch.selected);
            NSMutableDictionary *reservation = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                txtNoOfChildren.text, @"no_of_children",
                                                txtOldAge.text, @"no_of_oldage",
                                                [NSNumber numberWithInt:emailFlag],@"confirm_via_email",
                                                [NSNumber numberWithInt:notificationFlag],@"is_notification",
                                                txtEmail.text,@"email_address",
                                                txtPhoneNo.text,@"phone_no",
                                                btnDate.titleLabel.text,@"created",
                                                txtGuest.text,@"no_of_guest",
                                                [Functions DateFormateFunctionDate:selectedData Time:selectedTime],@"time",// @"2013-07-25 15:11:45"
                                                eventId,@"event_id",
                                                [self FloorIdes],@"floor_id",
                                                btnTableSelect.titleLabel.text,@"table_id",
                                                [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] ,@"udid",nil];
            NSLog(@" reservation data %@",reservation);
            [[DataStoreManager manager] addReservation:reservation];
            
        }else{
            [self.progressHud hide:YES];
            NSString* alertTitle=NSLocalizedString(@"Not Connected to wifi",@"Not Connected to wifi");
            NSString* alertMsg=NSLocalizedString(@"Thank You",@"Thank You");
            [Functions showAlert:alertTitle message:alertMsg];
            
        }
    }
    
}
-(void) ClearFome{
    NSString* btnSelectDate=NSLocalizedString(@"Select Date",@"Select Date");
    NSString* btnSelectTime=NSLocalizedString(@"Select Time",@"Select Time");
    NSString* btnSelectTable=NSLocalizedString(@"Select Table",@"Select Table");
    [btnDate setTitle:btnSelectDate forState:UIControlStateNormal];
    [btnTime setTitle:btnSelectTime forState:UIControlStateNormal];
    [btnTableSelect setTitle:btnSelectTable forState:UIControlStateNormal];
    txtNoOfChildren.text=@"";
    txtOldAge.text=@"";
    txtPhoneNo.text=@"";
    txtEmail.text=@"";
    txtGuest.text=@"";
    
    
}
-(BOOL) validateFome{
    NSString* alertTitle=NSLocalizedString(@"Sorry",@"Sorry");
    if([btnDate.titleLabel.text isEqualToString:NSLocalizedString(@"Select Date",@"Select Date")] || [btnTime.titleLabel.text isEqualToString:NSLocalizedString(@"Select Time",@"Select Time")] || [btnTableSelect.titleLabel.text isEqualToString:NSLocalizedString(@"Select Table",@"Select Table")] || [txtNoOfChildren.text isEqualToString:@""]
       || [txtOldAge.text isEqualToString:@""] || [txtPhoneNo.text isEqualToString:@""] || [txtGuest.text isEqualToString:@""])
    {
        NSString* alertMsg=NSLocalizedString(@"Please fill all the field",@"Please fill all the field");
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }else if(![self validateEmailWithString:txtEmail.text]){
        NSString* alertMsg=NSLocalizedString(@"Please enter correct email format",@"Please enter correct email format");
        [Functions showAlert:alertTitle message:alertMsg];
        
        return FALSE;
    }
    
    if([txtGuest.text integerValue]<=0){
        NSString* alertMsg=NSLocalizedString(@"Number of guest should not be Zero", @"Number ofguest should not be Zero");
        [Functions showAlert:alertTitle message:alertMsg];
        return FALSE;
    }
    return TRUE;
}
#pragma mark Email switch button
-(IBAction)actionSwitch:(id)sender{
    
    if(btnSwitch.selected)
        btnSwitch.selected = NO;
    else
        btnSwitch.selected = YES;
}
#pragma mark Notification switch button
-(IBAction)NotificationSwitch:(id)sender{
    
    if(btnNotificationSwitch.selected)
        btnNotificationSwitch.selected = NO;
    else
        btnNotificationSwitch.selected = YES;
    
}
#pragma mark iphone Action sheet DatePicker
-(void)datePickerSheet:(id)sender{
    int yaxis=[Functions screenSize].height;
    [datePicker removeFromSuperview];
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake ( 0.0, yaxis-280, 320, 216)];
    [datePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:lang]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:lang]];
    //format datePicker mode. in this example time is used
    if([sender tag]==0){
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.tag = 0;
        datePicker.minimumDate=[NSDate date];
        
        if(![btnDate.titleLabel.text isEqualToString:NSLocalizedString(@"Select Date",@"Select Date")]){
            NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
            [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:lang]];
            [FormatDate setDateFormat:@"MMMM dd, yyyy"];
            NSDate *tempdate = [[NSDate alloc] init];
            tempdate=[FormatDate dateFromString:btnDate.titleLabel.text];
            [datePicker setDate:tempdate animated:NO];
            
        }
        
    }else if([sender tag]==1){
        
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.tag = 1;
        [self setMiniMaxValue];
    }
    
    
    [dateFormatter setDateFormat:@"hh:mm a"];
    //calls dateChanged when value of picker is changed
    //[datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    UIToolbar* toolbarPicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbarPicker.barStyle=UIBarStyleBlackOpaque;
    [toolbarPicker sizeToFit];
    NSMutableArray *itemsBar = [[NSMutableArray alloc] init];
    //calls DoneClicked
    UIBarButtonItem *bbitem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneClicked)];
    [itemsBar addObject:bbitem];
    [toolbarPicker setItems:itemsBar animated:YES];
    
    [datePicker addSubview:toolbarPicker];
    [self.view addSubview:datePicker];
    
    //[pickerAction addSubview:toolbarPicker];
    //[pickerAction addSubview:datePicker];
    //[pickerAction showInView:self.view];
    //[pickerAction setBounds:CGRectMake(0,0,320, 464)];
    
}
#pragma mark ipad DatePicker
-(void)ipad_datePickerSheet:(id)sender{
    [datePicker removeFromSuperview];
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake ( 0.0, 490, 552, 400.0)];
    [datePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:lang]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    // [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:lang]];
    //format datePicker mode. in this example time is used
    if([sender tag]==0){
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.tag = 0;
        datePicker.minimumDate=[NSDate date];
        
        if(![btnDate.titleLabel.text isEqualToString:NSLocalizedString(@"Select Date",@"Select Date")]){
            NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
            [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:lang] ];
            [FormatDate setDateFormat:@"MMMM dd, yyyy"];
            NSDate *tempdate = [[NSDate alloc] init];
            tempdate=[FormatDate dateFromString:btnDate.titleLabel.text];
            [datePicker setDate:tempdate animated:NO];
            
        }
        
    }else if([sender tag]==1){
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.tag = 1;
        [self setMiniMaxValue];
    }
    
    
    [dateFormatter setDateFormat:@"hh:mm a"];
    //calls dateChanged when value of picker is changed
    //[datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    UIToolbar* toolbarPicker = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbarPicker.barStyle=UIBarStyleBlackOpaque;
    [toolbarPicker sizeToFit];
    NSMutableArray *itemsBar = [[NSMutableArray alloc] init];
    //calls DoneClicked
    UIBarButtonItem *bbitem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneClicked)];
    [itemsBar addObject:bbitem];
    [toolbarPicker setItems:itemsBar animated:YES];
    
    [datePicker addSubview:toolbarPicker];
    
    //[pickerAction addSubview:toolbarPicker];
    //[pickerAction addSubview:datePicker];
    //[pickerAction showInView:self.view];
    //[pickerAction setBounds:CGRectMake(0,0,320, 464)];
    
    [self.view addSubview:datePicker];
}

-(void) setMiniMaxValue{
    int startHour = 11;
    int endHour = 23;
    
    NSDate *date1 = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date1];
    [components setHour: startHour];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *startDate = [gregorian dateFromComponents: components];
    
    [components setHour: endHour];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *endDate = [gregorian dateFromComponents: components];
    [datePicker setMinimumDate:startDate];
    [datePicker setMaximumDate:endDate];
    [datePicker setDate:startDate animated:YES];
    datePicker.tag = 1;
    
}
-(void)dateChanged:(id)sender{
    /*
     NSLog(@"asdfasdf %d",[sender tag]);
     //format date
     NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
     [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] ];
     if([sender tag] ==0){
     btnTag=0;
     //set date format
     [FormatDate setDateFormat:@"dd MMMM YYYY"];
     //update date textfield
     [btnDate setTitle:[FormatDate stringFromDate:[datePicker date]] forState:UIControlStateNormal];
     }else if([sender tag]==1){
     btnTag=1;
     
     //set date format
     [FormatDate setDateFormat:@"hh:mm a"];
     //update date textfield
     [btnTime setTitle:[FormatDate stringFromDate:[datePicker date]] forState:UIControlStateNormal];
     
     }
     */
}

-(BOOL)closeDatePicker:(id)sender{
    if([Functions isIPad]){
        [datePicker removeFromSuperview];
    }else{
        //        [pickerAction dismissWithClickedButtonIndex:0 animated:YES];
        [datePicker removeFromSuperview];
    }
    
    
    return YES;
}

//action when done button is clicked
-(IBAction)DoneClicked{
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    
    if(btnTag ==0){
        [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en"] ];
        //set date format
        [FormatDate setDateFormat:@"MMMM dd, YYYY"];
        selectedData=[FormatDate stringFromDate:[datePicker date]];
        
        [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:lang] ];
        //update date textfield
        [btnDate setTitle:[FormatDate stringFromDate:[datePicker date]] forState:UIControlStateNormal];
        
        
    }else if(btnTag==1){
        [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en"] ];
        //set date format
        [FormatDate setDateFormat:@"hh:mm a"];
        selectedTime=[FormatDate stringFromDate:[datePicker date]];
        [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:lang] ];
        //update date textfield
        [btnTime setTitle:[FormatDate stringFromDate:[datePicker date]] forState:UIControlStateNormal];
        
    }
    
    [self closeDatePicker:self];
    datePicker.frame=CGRectMake(0, 44, 320, 416);
    
}

//returns number of components in pickerview
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

//returns number of rows in date picker
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 40;
}

#pragma mark TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(txtOldAge==textField || txtNoOfChildren==textField){
        [self animateTextField: textField up: YES distance:-230];
        
    }else{
        [self animateTextField: textField up: YES distance:-220];
    }
    
    return YES;
}
-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length >=3 && range.length == 0 && (textField==txtOldAge || textField==txtNoOfChildren || textField==txtGuest)  ) {
        return NO;
        
    }
    if(textField==txtPhoneNo && textField.text.length>=16 && range.length == 0){
        return NO;
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        [self animateTextField: nil up: NO distance:0];
        
    }
    
    return YES;
}

-(void)cancelNumberPad:(id)sender{
    [txtNoOfChildren resignFirstResponder];
    [txtOldAge resignFirstResponder];
    [txtPhoneNo resignFirstResponder];
    [txtGuest resignFirstResponder];
    [self animateTextField: sender up: NO distance:0];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    [textField resignFirstResponder];
    [self animateTextField: textField up: NO distance:0];
    
    
    return YES;
}
#pragma mark email validate
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark keyboard UpView
- (void) animateTextField: (UITextField*) textField up: (BOOL) up distance:(int) distance
{
    const int movementDistance =distance; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectMake(0, movementDistance,self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    [UIView commitAnimations];
}

-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
