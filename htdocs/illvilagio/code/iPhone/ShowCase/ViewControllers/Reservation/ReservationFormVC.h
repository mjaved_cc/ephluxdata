//
//  ReservationFormVC.h
//  ShowCase
//
//  Created by salman ahmed on 5/27/13.
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ReservationFormVC : UIViewController<UIScrollViewDelegate,UIActionSheetDelegate,UITextFieldDelegate>{
    
    
    IBOutlet UIScrollView   *scrollableForm;
    IBOutlet UIDatePicker   *datePicker;
    IBOutlet UITextField    *txtNoOfChildren;
    IBOutlet UITextField    *txtOldAge;
    IBOutlet UITextField    *txtPhoneNo;
    IBOutlet UITextField    *txtEmail;
    IBOutlet UITextField    *txtGuest;
    IBOutlet UIButton       *btnDate;
    IBOutlet UIButton       *btnTime;
    IBOutlet UIButton       *btnSwitch;
    IBOutlet UIButton       *btnTableSelect;
    IBOutlet UIButton       *btnReserve;
    IBOutlet UIButton       *btnNotificationSwitch;
    IBOutlet UILabel        *lblDate;
    IBOutlet UILabel        *lblTime;
    IBOutlet UILabel        *lblTable;
    IBOutlet UILabel        *lblEmail;
    IBOutlet UILabel        *lblGuest;
    IBOutlet UILabel        *lblAge;
    IBOutlet UILabel        *lblChild;
    IBOutlet UILabel        *lblPhoneNo;
    IBOutlet UILabel        *lblConfrimView;
    IBOutlet UILabel        *lblNotificationEmail;
    IBOutlet UILabel        *lblNotification;
    IBOutlet UILabel        *lblMidTitle;
    int btnTag;
    UIActionSheet *pickerAction ;
    NSString* strFloors;
    NSString* eventId;
    NSString* selectedData;
    NSString* selectedTime;
    
    MBProgressHUD *progressHud;
}
@property (nonatomic,strong) NSDictionary*  eventDate;
-(IBAction)onClick:(id)sender;
-(IBAction)postReservationRequest:(id)sender;
@property (nonatomic, retain) MBProgressHUD *progressHud;
@end
