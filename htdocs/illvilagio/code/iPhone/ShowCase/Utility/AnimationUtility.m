//
//  AnimationUtility.m
//  
//
//  Created by  on 28/08/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "AnimationUtility.h"

@implementation AnimationUtility

+ (void)transition:(UIView*)aView fromDirection:(NSString*)direction withDuration:(CFTimeInterval)duration delegateTarget:(id)target {
	// Chose transition type and direction at random from the arrays of supported transitions and directions
	NSArray *transitions = [NSArray arrayWithObjects:kCATransitionPush, nil];
	NSString *transition = [transitions objectAtIndex:0];
	
	// Set up the animation
	CATransition *animation = [CATransition animation];
	
	if (target)
		[animation setDelegate:target];
	
	// Set the type and if appropriate direction of the transition, 
	[animation setType:transition];
	[animation setSubtype:direction];
	
	// Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
	[animation setDuration:duration];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[aView layer] addAnimation:animation forKey:kTransitionKey];
}

+ (void)fadeOut:(UIView*)view withDuration:(CFTimeInterval)duration {
	[UIView beginAnimations:kFadeKey context:nil];
	[UIView setAnimationDuration:duration];
	view.alpha = 0.0;
	[UIView commitAnimations];
}

+ (void)flipViews:(UIView*)mainView showView:(UIView*)viewHot hideView:(UIView*)viewCold flipTransition:(UIViewAnimationTransition)transition withDuration:(CFTimeInterval)duration {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];  
	[mainView addSubview:viewHot];
	[viewCold removeFromSuperview];
	[UIView setAnimationTransition:transition forView:viewHot cache:YES];	
    [UIView commitAnimations];
}

@end
