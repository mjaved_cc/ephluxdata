//
//  InternetCheck.m
//  
//
//  Created by  on 12/13/10.
//  Copyright 2010. All rights reserved.
//

#import "InternetCheck.h"

static BOOL internetConnectivityChecked = NO;
static BOOL gotInternet = NO; 

#define kNetworkError @"No connection found – Updated will not be available unless connected to the internet."
#define kServerDownError @"Server is temporarily unavailable, please try after few minutes."

@implementation InternetCheck

+(BOOL)IsConnected {
        //if(internetConnectivityChecked)
        //	return gotInternet;
	
	internetConnectivityChecked = YES;
    
    //reachabilityWithHostName
	Reachability *r = [Reachability reachabilityWithHostname:@"www.google.com"];//finance.yahoo.com
	NetworkStatus internetStatus = [r currentReachabilityStatus];
	
	if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) {
		gotInternet = NO;
	} else {
		gotInternet = YES;
	}

	return gotInternet;
}

+(NSString*)errorMessageForNetworkOrServerDown {
	//Check for internet or our backend server availabilty and provide approperiate error!
	BOOL isConnectedWithInternet = [InternetCheck IsConnected];
	
	if (isConnectedWithInternet) {
		return kServerDownError;
	}

	return kNetworkError;
}

@end
