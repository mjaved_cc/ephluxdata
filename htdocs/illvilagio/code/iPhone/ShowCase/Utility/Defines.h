/*
 *  Defines.h
 *  
 *
 *  Created by  on 1/3/11.
 *  Copyright 2010. All rights reserved.
 *
 */

#import "UIViewAdditions.h"
#import "NSDateAdditions.h"
#import "Functions.h"
#import "InternetCheck.h"
#import "AnimationUtility.h"
#import "Sounds.h"

#import "BaseViewController.h"



//
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
//#define  ScreenSize [[UIScreen mainScreen] bounds].size;
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define kNotificationGallery @"galleryService"
#define kOrderInformation @"orderInfo"
#define kNumberOfPeople @"numberOfPeople"
#define kaddOrderPopup @"addOrderPopup"
#define kSettingVersion @"SettingsVersion"
#define kSettingBaseURL @"SettingsBaseURL"
#define kSettingNotificationDelay @"SettingsNotificationDelay"
#define kSettingEnableSound @"SettingsEnableSound"
#define kNotificationEvents @"eventService"
#define kNotificationFeedBack @"feedBackService"
#define kNotificationContent @"contentService"
#define kNotificatonAllContent @"allContent"
#define kNotificatonAllHome @"HomeContent"
#define kNotificationReservation @"ReservationService"
#define kNotificationOrderDone @"OrderDoneService"

#define kServicePostOrderCheckout @"Order/makeOrder.json"
#define kServiceAddFeedback @"FeedBack/add_feedback.json"
#define kOrderType @"OrderType"


#define LATITUDE @"CurrentLatitude"
#define LONGITUDE @"CurrentLongitude"
#define ZIP @"CurrentZip"

#define keyCurrencySymbol @"CurrencySymbol"
#define keyTaxPercentage @"TaxPercentage"
#define keyDeliveryAmount @"deliverycharges"

#define keyTableLayoutImage @"TableLayoutImage"

#define kPassword @"password"
#define keyTableLayoutImageStatus @"tableLayoutImageStatus"
#define kFloorLevel @"floorLevel"
#define kHostField @"hostField"
#define kCustomerName @"customerName"
#define kDeliveryAddress @"deliveryAddress"
#define kCustomerID @"customerID"
#define kCustomerSearch @"customerLookUp"
#define kNotificationInventory @"Inventory"

////////////////////DELIVERY AGENT INFORMATION///////////////////////////
#define kDeliveryAgentID @"deliveryAgentID"
#define kDeliveryAgentName @"deliveryAgentName"
#define kDeliveryTime @"deliveryTime"

////////////////////DELIVERY AGENT INFORMATION///////////////////////////
///////Sallu Twitter second after "2S" Application
#define kOAuthConsumerKey	 @"zt49DFFFsN8NfQ0AZnFruA"
#define kOAuthConsumerSecret @"fJIpQIvA5hlX63osovHc42RhZU3a6pakLpjcbL0o8r4"

#define kNotificationIsLogin @"islogin"
////////////////////TABBED AMOUNT/////////////////////////
#define kTabAmount @"orderTabAmountEntered"
#define kTabBool @"orderTabAmount"


#define kCustomerListData @"customerListData"
#define kDeliveryAgentsData @"deliveryAgentsData"


#define keyStatus @"status"
#define keyStatusMessage @"statusDesc"


#define kCustomerTypeID @"CustomerTypeID"
#define kCustomerTypeDesc @"CustomerTypeDesc"


#define kNotificationTypeDelivery @"TypeDelivery"


#define kNotificationAttendance @"Attendance"
#define kNotificationBreakList @"breakList"




#define kNotificationSubmitOrderCheckout @"SubmitOrderCheckout"
#define kNotificationUpdateGuest @"UpdateGuest"
#define kNotificationSyncApp @"SyncApp"

#define kNotificationMenuClick @"MenuClickStatus"
#define kNotificationPlaceOrderClick @"PlaceOrderClickStatus"

#define kNotificationUserRequireReLogin @"UserRequiredReLogin"
#define kNotificationUserLogin @"UserLoginStatus"
#define kNotificationUserLogout @"UserLogoutStatus"
#define kNotificationLast12HoursOrder @"Last12HoursOrder"
#define kNotificationOrderDetail @"OrderDetail"
#define kNotificationSubmitOrder @"SubmitOrder"
#define kNotificationSubmitOrderIsReadyStatus @"SubmitOrderIsReadyStatus"
#define kNotificationSubmitOrderServedStatus @"SubmitOrderServedStatus"
#define kNotificationSubmitOrderCheckout @"SubmitOrderCheckout"
#define kNotificationTables @"Tables"
#define kNotificationSubmitTableStatus @"SubmitTableStatus"
#define kNotificationOfNotifier @"Notifier"
#define kNotificationNotifierNewOrders @"NotifierNewOrders"
#define kNotificationNotifierOrdersUpdated @"NotifierOrdersUpdated"
#define kNotificationNotifierTableStatusUpdated @"NotifierTableStatusUpdated"
#define kNotificationItemDetail @"ReceivedItemDetail"
#define kNotificationCustomerLook @"CustomerLookUp"
#define kNotificationCustomerDetail @"DetailOfCustomer"

#define kNotificationAddTables @"AddTables"

#define kNotificationCustomerHistory @"CustomerHistory"

#define kNotificationOrdersReport @"OrdersReoprt"
#define kNotificationOrdersItemAdded @"OrdersItemAdded"

#define kNotificationSubmitTableState @"SubmitTableState"
#define kNotificationSubmitTableSwapTo @"SubmitTableSwapTo"

#define kNotificationCustomerOrderDetail @"CustomerOrderDetail"
#define kNotificationGetRegister @"GetRegister"


#define kNotificationTableReservation @"TableReservation"


#define kNotificationNotifierNewReservation @"NotifierNewReservation"
#define kNotificationNotifierListUpdated @"NotifierListUpdated"
#define kNotificationLast24HoursReservation @"Last24HoursReservation"
#define kNotificationListDetail @"ListDetail"
#define kNotificationUpdateReservation @"UpdateTableReservation"

//#define kNotificationSubmitTableSwap @"Last12HoursOrder"
#define kListBackendID @"listBackendID"
#define kNotificationSearchMenu @"SubmitSearchMenu"
#define kNotificationSubmitOrderCheckoutiPad @"SubmitOrderCheckoutiPad"

/***************************/

#define kNotificationUserImageDownloaded @"UserImageDownloaded"
#define kNotificationMenuImageDownloaded @"MenuImageDownloaded"
#define kNotificationCategoryImageDownloaded @"CategoryImageDownloaded"
#define kNotificationCategoryItemImageDownloaded @"CategoryItemImageDownloaded"
#define kNotificationSubItemImageDownloaded @"SubItemImageDownloaded"
#define kNotificationSideItemImageDownloaded @"SideItemImageDownloaded"
#define kNotificationTableTypeImageDownloaded @"TableTypeImageDownloaded"

#define kNotificationMenuHiResImageDownloaded @"MenuHiResImageDownloaded"
#define kNotificationCategoryHiResImageDownloaded @"CategoryHiResImageDownloaded"
#define kNotificationCategoryItemHiResImageDownloaded @"CategoryItemHiResImageDownloaded"
#define kNotificationSubItemHiResImageDownloaded @"SubItemHiResImageDownloaded"
#define kNotificationSideItemHiResImageDownloaded @"SideItemHiResImageDownloaded"


#define kNotificationUpdateTableTabItem @"updateTableTab"
#define kNotificationUpdateComplimentaryItem @"compliementaryItem"


#define kNotificationNewCustomer @"NewCustomer"

#define kNotificationCustomerList @"CustomerList"
#define kNotificationDeliveryAgent @"DeliveryAgent"
#define kNotificationUpdateDeliveryAgent @"UpdateDeliveryAgent"


#define kNotificationSearchMenuController @"SearchMenu"
#define kNotificationNewOrderReports @"NewReport"

#define kTableStatusNormal @"Normal"
#define kTableStatusOccupied @"Occupied"
#define kTableStatusReserved @"Reserved"
#define kTableStatusBlocked @"Blocked"
#define kModifiedDate @"modified_date"
//#define LocalServerURL @"http://apptestonline.com/illvillagio/"
//#define LocalServerURL @"http://demo.apptestonline.com/ilvillaggio/"
#define LocalServerURL @"http://192.175.0.81/restaurant/development/code/web/"
#define d_FacebookAppId @"547166328654259"

typedef enum {
	TableStatusNormal = 201,
	TableStatusReserved,
	TableStatusOccupied,
	TableStatusBlocked,
    TableStatusRecipetGiven,

} TableStatus;


typedef enum {
	Added,
	Updated,
	Deleted,
} OrderItemState;

