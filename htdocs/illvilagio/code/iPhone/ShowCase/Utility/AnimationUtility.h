//
//  AnimationUtility.h
//  
//
//  Created by  on 28/08/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#define kTransitionKey @"transitionViewAnimation"
#define kFadeKey @"fadeViewAnimation"

@interface AnimationUtility : NSObject {

}

+ (void)transition:(UIView*)view fromDirection:(NSString*)direction withDuration:(CFTimeInterval)duration delegateTarget:(id)target;
+ (void)fadeOut:(UIView*)view withDuration:(CFTimeInterval)duration;
+ (void)flipViews:(UIView*)mainView showView:(UIView*)viewHot hideView:(UIView*)viewCold flipTransition:(UIViewAnimationTransition)transition withDuration:(CFTimeInterval)duration;

@end
