//
//  ASIHTTPWrapper.h
//  Depiction
//
//  Created by Ammad on 6/19/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "Facebook.h"

// Protocol for the importer to communicate with its delegate.
@protocol SocialNetworkingWrapperDelegate <NSObject>

- (void)didGetFacebookRequest:(NSMutableDictionary *)getRecords;
- (void)didFailedFacebookRequest:(NSError *)error;

@end

@interface SocialNetworkingWrapper : NSObject <SocialNetworkingWrapperDelegate,FBSessionDelegate, FBRequestDelegate>
{
	id <SocialNetworkingWrapperDelegate> delegate;
    NSDictionary *postParams;
     Facebook *facebookPost;
    NSString *fbaccessToken;
}
@property (nonatomic, retain) Facebook *facebookPost;
@property(nonatomic,retain)NSString *fbaccessToken;
@property(nonatomic,strong) NSString *requestName;

-(id)inits;
@property (nonatomic, assign) id <SocialNetworkingWrapperDelegate> delegate;
@property (nonatomic, retain) NSDictionary *postParams;
-(void)IsFacebookInitlizeAndAuthorize;
-(void)getUserProfile:(NSString *)graphpath;
-(void) postToFbTitle:(NSString*) title  Description:(NSString*) description ImageURL:(NSString*) yourpathToImage;
-(void) postToFbTitle:(NSString*) title  Description:(NSString*) description;

@end
