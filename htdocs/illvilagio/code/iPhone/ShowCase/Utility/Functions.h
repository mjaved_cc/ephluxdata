//
//  Functions.h
//
//
//  Created by  on 2/24/11.
//  Copyright . All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *lang;
extern NSInteger langIsArabic;

@interface Functions : NSObject {
    
}
+(BOOL)isIPad;
+(UIViewController *)allocUniversalViewController:(NSString*)controllerName;
//+(id)allocUniversalViewController:(NSString*)controllerName;
+(NSString*)getUniversalImageName:(NSString*)imageName;
+(id)loadUniversalNibNamed:(NSString*)nibName owner:(id)owner;
+(int)getOrderType;
+(void)setOrderType:(int)value ;
+(int)getNumberOfPeople;
+(BOOL)isDeliveryTarget;

+(NSDictionary*)getOrderInformation;
+(void)setNavigationBar;
+(UIButton*)setNavigationBackBarButton;
+(UIBarButtonItem*)setBackBarButton:(id)object;

+(NSString*)trim:(NSString *)value;
+(NSString*)deviceInString;
+(NSString*)localLanguage;
+(NSString*)deviceIdentifier;
+(NSString*)applicationVersion;
+(NSDate*)dateForStoreReceivedFromBackend:(NSString*)receivedDate;
+(void)showAlert:(NSString*)title message:(NSString*)message;
+(void)showAlert:(NSString*)title message:(NSString*)message tag:(NSInteger)tag;
+(void)showAlertCancel:(NSString*)title message:(NSString*)message : (UIViewController*)controller;
+(NSString*)stateEnumToString:(int)state; //Change from OrderState to int, to get rid of Compilation Errors
+(NSString*)orderStatusEnumToString:(int)status;

+(NSString*)localImageThumbName:(NSString*)url backendID:(int)backendID;
+(NSString*)localImageHiResName:(NSString*)url backendID:(int)backendID;
+(NSString*)highResImageURL:(NSString*)path;
+(NSString*)localImageTableType:(NSString*)url state:(NSString*)state backendID:(int)backendID;
+(NSString*) getLocalizedNumber:(NSString*)num;
+(NSString*) getLocalizedDate:(NSString*)date;
+(CGSize) screenSize;

+(NSString*)filePathForMenuImage;
+(NSString*)filePathForCategoryImage;
+(NSString*)filePathForCategoryItemImage;
+(NSString*)filePathForSubItemImage;
+(NSString*)filePathForSideItemImage;
+(NSString*)filePathForTableTypeImage;
+(NSString*)filePathForOrderAndInvoiceImage;
+(NSString* ) dateFormate:(NSString* ) mydate;
+(NSString*)currentDateTimeInString;
+(NSString* ) DateFormateFunctionDate:(NSString*)date Time:(NSString*) time;
+(NSString *) getLocalLang;
+(NSMutableArray*) getDateFromJSON:(NSString*) key;
+(NSMutableArray*) getDataFromNSUserDefault_Key: (NSString*) key;
+(void)useResourceJSONFile:(BOOL)flag;
+(BOOL)isResourceJSONFile;

//Settings
+(NSString*)settingBaseURL;
+(double)settingNotificationDelay;
+(BOOL)settingSoundEnabled;
+(NSString *) replaceNullFromString:(NSString *)original;
+(NSString*)dateForReservation:(NSString*)receivedDate;
+ (UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize ;
@end

static NSString *hexToString(NSString * label, void *data, int length) {
	const char HEX[]="0123456789ABCDEF";
	char s[2000];
	for(int i=0;i<length;i++) {
		s[i*3]=HEX[((uint8_t *)data)[i]>>4];
		s[i*3+1]=HEX[((uint8_t *)data)[i]&0x0f];
		s[i*3+2]=' ';
	}
	s[length*3]=0;
	
    if(label)
        return [NSString stringWithFormat:@"%@(%d): %s",label,length,s];
    else
        return [NSString stringWithCString:s encoding:NSASCIIStringEncoding];
}
