    //
    //  Sounds.m
    //  POS
    //
    //  Created by admin on 17/10/11.
    //  Copyright 2011 __MyCompanyName__. All rights reserved.
    //

#import "Sounds.h"
#import "Functions.h"

@implementation Sounds

#pragma mark -
#pragma mark Static Call for object creation
+(id)soundsManager {
	static Sounds *soundManager = nil;
	
	@synchronized(self) {
		if (soundManager == nil) {
			soundManager = [[self alloc] init];
		}
	}
	
	return soundManager;
}

#pragma mark -
#pragma mark Life Cycle


-(void)keySound{
    
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("beep-29"), CFSTR("wav"), NULL);
    AudioServicesCreateSystemSoundID(soundFileURLRef, &keyStroke);
    CFRelease(soundFileURLRef);
    AudioServicesPlaySystemSound(keyStroke);
    
}
-(void)orderSound{
    
  /*  CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("orderReady"), CFSTR("wav"), NULL);
    AudioServicesCreateSystemSoundID(soundFileURLRef, &orderReady);
    CFRelease(soundFileURLRef);*/
        //AudioServicesPlaySystemSound(orderReady);
    
}
-(void)cashSound{
    
    /*CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef;
    soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("cashRegister"), CFSTR("wav"), NULL);
    AudioServicesCreateSystemSoundID(soundFileURLRef, &cashRegister);
    CFRelease(soundFileURLRef);
        //AudioServicesPlaySystemSound(cashRegister);
    */
}
-(id)init {
	self = [super init];
	if(self != nil) {
        
            //[self performSelector:@selector(keySound) withObject:nil afterDelay:0];
            //[self performSelector:@selector(orderSound) withObject:nil afterDelay:0];
            //[self performSelector:@selector(cashSound) withObject:nil afterDelay:0];
        [self keySound];
       
        
            //	AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("keyStroke"), CFSTR("wav"), NULL), &keyStroke);
            //	AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("orderReady"), CFSTR("wav"), NULL), &orderReady);
            //	AudioServicesCreateSystemSoundID(CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("cashRegister"), CFSTR("wav"), NULL), &cashRegister);
	}
	
	return self;
}

-(void)dealloc {
        //AudioServicesDisposeSystemSoundID(keyStroke);
        //AudioServicesDisposeSystemSoundID(cashRegister);
        //AudioServicesDisposeSystemSoundID(orderReady);
}

-(void)playKeyStroke {
	if([Functions settingSoundEnabled] == NO) return;
	
	AudioServicesPlaySystemSound(keyStroke);
}

-(void)playCashRegister {
	if([Functions settingSoundEnabled] == NO) return;
    
	AudioServicesPlaySystemSound(cashRegister);
}

-(void)playOrderReadyAndVibrate {
	if([Functions settingSoundEnabled] == NO) return;
    
	AudioServicesPlaySystemSound(orderReady);
	AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
}

@end
