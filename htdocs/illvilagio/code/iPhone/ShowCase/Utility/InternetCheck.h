//
//  InternetCheck.h
//  
//
//  Created by  on 12/13/10.
//  Copyright 2010. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Reachability.h"

@interface InternetCheck : NSObject {
}

+(BOOL)IsConnected;
+(NSString*)errorMessageForNetworkOrServerDown;

@end
