//
//  SingletonClass.m
//  Depiction
//
//  Created by Muhammad.uzair on 11/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SingletonClass.h"

@implementation SingletonClass

@synthesize buttonTag,tableName,eventArray,numberOfPeople,allContentDic,dealArray;
@synthesize applicationBecomeActive;
static SingletonClass *sharedInstance = nil;

+ (SingletonClass *)sharedInstance
{
    @synchronized(self)
    {
        if( sharedInstance == nil )
        {
            sharedInstance = [[SingletonClass alloc] init];
        }
    }
    return sharedInstance;
}
//-(void) setEvetArrayObject{
//    self.eventArray=[[NSMutableArray alloc]init];
//    for(int i=0; i<20;i++){
//        NSMutableDictionary* tempDic=[[NSMutableDictionary alloc]init];
//        if(i==3){
//            [tempDic setObject:@"http://playtimeandparty.com/wp-content/uploads/2013/03/breakfast-pizza.png" forKey:@"image"];
//            [tempDic setObject:@"July 15 2013" forKey:@"date"];
//            [tempDic setObject:@"Rock the World" forKey:@"dish"];
//            [tempDic setObject:@"orem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor" forKey:@"discription"];
//            [tempDic setObject:[NSNumber numberWithBool:YES] forKey:@"isActive"];
//            
//            [self.eventArray addObject:tempDic];
//            
//        }else if(i==6){
//            [tempDic setObject:@"http://www.leanonlife.com/wp-content/uploads/139736048-300x200.jpg"forKey:@"image"];
//            [tempDic setObject:@"July 26 2013" forKey:@"date"];
//            [tempDic setObject:@"Rock the World" forKey:@"dish"];
//            [tempDic setObject:@"orem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor" forKey:@"discription"];
//            [tempDic setObject:[NSNumber numberWithBool:YES] forKey:@"isActive"];
//            [self.eventArray addObject:tempDic];
//            
//        }else
//            if(i%2){
//                [tempDic setObject:@"http://playtimeandparty.com/wp-content/uploads/2013/03/breakfast-pizza.png" forKey:@"image"];
//                [tempDic setObject:@"June 24 2013" forKey:@"date"];
//                [tempDic setObject:@"Rock the World" forKey:@"dish"];
//                [tempDic setObject:@"Experience the delectable Arabic Cuisine and relish the royal tastes without comprising on your budget.Dealtoday brings you this delectable cuisine at 50% off price. Enjoy 1 Farrouj Mishwi Behari, 1 Shish Taouk, Parathas/Pita Bread and 2 Mocktails for Rs 620/- instead of Rs 1,240/- [50% off] at Marrakech Lounge." forKey:@"discription"];
//                [tempDic setObject:[NSNumber numberWithBool:NO] forKey:@"isActive"];
//                [self.eventArray addObject:tempDic];
//            }else{
//                [tempDic setObject:@"http://www.hcplive.com/_media/_upload_image/_thumbnails/upclose-fast_food2.jpg" forKey:@"image"];
//                [tempDic setObject:@"May 22 2013" forKey:@"date"];
//                [tempDic setObject:@"Elisa" forKey:@"dish"];
//                [tempDic setObject:@"Experience the delectable Arabic Cuisine and relish the royal tastes without comprising on your budget.Dealtoday brings you this delectable cuisine at 50% off price. Enjoy 1 Farrouj Mishwi Behari, 1 Shish Taouk, Parathas/Pita Bread and 2 Mocktails for Rs 620/- instead of Rs 1,240/- [50% off] at Marrakech Lounge." forKey:@"discription"];
//                [tempDic setObject:[NSNumber numberWithBool:NO] forKey:@"isActive"];
//                [self.eventArray addObject:tempDic];
//            }
//        tempDic=nil;
//    }
//
//}
@end
