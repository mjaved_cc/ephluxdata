//
//  ASIHTTPWrapper.m
//  Depiction
//
//  Created by Ammad on 6/19/13.
//  Copyright (c) 2013 Ammad. All rights reserved.
//


#import "SocialNetworkingWrapper.h"

@implementation SocialNetworkingWrapper

@synthesize delegate, postParams;
@synthesize facebookPost,fbaccessToken;
@synthesize requestName;


-(id)inits
{
	if (self = [super init])
	{
		self = [[SocialNetworkingWrapper alloc] init];
	}
	return self;
}

#pragma mark For Facebook
-(void)IsFacebookInitlizeAndAuthorize
{
    facebookPost = [[Facebook alloc] initWithAppId:d_FacebookAppId];
    NSArray* permissions = [[NSArray alloc] initWithObjects:
                            @"publish_stream", @"email",nil];
    [facebookPost authorize:permissions delegate:self];
    [permissions release];
}

#pragma mark Facebook Get All My Details
// example [facebookPost getAllMyFeeds];
-(void)getAllMyFeeds
{
   
    [facebookPost requestWithGraphPath:@"https://graph.facebook.com/me" andParams:nil andDelegate:self];

}
#pragma mark Post Twitter
-(void) postToFbTitle:(NSString*) title  Description:(NSString*) description ImageURL:(NSString*) yourpathToImage{
    self.requestName=@"PostImage";
    //NSString *filePath =yourpathToImage;
    //@"http://d1xzuxjlafny7l.cloudfront.net/wp-content/uploads/2011/08/HUDTutorial.jpg"
    UIImage *img  = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:yourpathToImage]]];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   description,@"message",img, @"source",
                                   nil];
    
    [facebookPost requestWithGraphPath:@"/me/photos"
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];

}
#pragma mark Post Twitter
-(void) postToFbTitle:(NSString*) title  Description:(NSString*) description{
    self.requestName=@"PostImage";
    
       NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   description,@"message",
                                   nil];
    
    [facebookPost requestWithGraphPath:@"me/feed"
                             andParams:params
                         andHttpMethod:@"POST"
                           andDelegate:self];
    
}

#pragma mark Faebook get Facebook own User Details
// example [facebookPost getUserProfile];
-(void)getUserProfile:(NSString *)graphpath
{
    [facebookPost requestWithGraphPath:graphpath andDelegate:self];
}

#pragma mark Facebook Delegate
- (void)fbDidLogin {
    NSLog(@"login ho gya");
    
    self.fbaccessToken = [facebookPost accessToken];
   // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // [[NSUserDefaults standardUserDefaults]synchronize];
    //[defaults setObject:[NSString stringWithFormat:@"%@",[facebookPost accessToken]] forKey:d_FacebookToken];
     self.requestName=@"me";
    [self getUserProfile:@"me"];
}

-(void)fbDidNotLogin:(BOOL)cancelled {
	NSLog(@"did not login");
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    if ([result isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary* json = result;
//        NSLog(@"-> %@", json);
        [self didGetFacebookRequest:json];
    }
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed with error: %@", [error localizedDescription]);
    [self didFailedFacebookRequest:error];

}


- (void)didGetFacebookRequest:(NSMutableDictionary *)getRecords
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[delegate didGetFacebookRequest:getRecords];
}

- (void)didFailedFacebookRequest:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[delegate didFailedFacebookRequest:error];
}


@end
