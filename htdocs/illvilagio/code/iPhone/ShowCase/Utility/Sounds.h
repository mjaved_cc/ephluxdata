//
//  Sounds.h
//  POS
//
//  Created by admin on 17/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>

@interface Sounds : NSObject {
	SystemSoundID keyStroke;
	SystemSoundID orderReady;
	SystemSoundID cashRegister;
}

+(id)soundsManager;

-(void)playKeyStroke;
-(void)playCashRegister;
-(void)playOrderReadyAndVibrate;

@end
