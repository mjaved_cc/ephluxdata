//
//  SingletonClass.h
//  Depiction
//
//  Created by Muhammad.uzair on 11/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonClass : NSObject
{
   }
@property (nonatomic,assign) NSInteger      buttonTag;
@property (nonatomic,strong) NSString       *tableName;
@property (nonatomic,strong) NSMutableArray *eventArray;
@property (nonatomic,strong) NSMutableArray *dealArray;
@property (nonatomic,assign) NSInteger       numberOfPeople;
@property (nonatomic,strong) NSMutableDictionary *allContentDic;
@property (nonatomic) bool applicationBecomeActive;


+ (id)sharedInstance;
-(void) setEvetArrayObject;
@end
