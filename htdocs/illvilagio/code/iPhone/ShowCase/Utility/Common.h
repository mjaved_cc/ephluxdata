//
//  Common.h
//  Depiction
//
//  Created by Muhammad.uzair on 10/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject

+ (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize image:(UIImage *)image;

@end
