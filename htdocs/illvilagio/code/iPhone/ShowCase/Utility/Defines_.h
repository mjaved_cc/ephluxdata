/*
 *  Defines.h
 *  
 *
 *  Created by  on 1/3/11.
 *  Copyright 2010. All rights reserved.
 *
 */

#import "UIViewAdditions.h"
#import "NSDateAdditions.h"
#import "Functions.h"
#import "InternetCheck.h"
#import "AnimationUtility.h"
#import "Sounds.h"
#import "BaseViewController.h"


#define kSettingVersion @"SettingsVersion"
#define kSettingBaseURL @"SettingsBaseURL"
#define kSettingNotificationDelay @"SettingsNotificationDelay"
#define kSettingEnableSound @"SettingsEnableSound"

#define LATITUDE @"CurrentLatitude"
#define LONGITUDE @"CurrentLongitude"
#define ZIP @"CurrentZip"

#define keyCurrencySymbol @"CurrencySymbol"
#define keyTaxPercentage @"TaxPercentage"
#define keyDeliveryAmount @"deliverycharges"

//abduls
#define keyTableLayoutImage @"TableLayoutImage"

//abdule

//KhalilG
#define kPassword @"password"
#define keyTableLayoutImageStatus @"tableLayoutImageStatus"
#define kFloorLevel @"floorLevel"
#define kHostField @"hostField"
#define kCustomerName @"customerName"
#define kDeliveryAddress @"deliveryAddress"
#define kCustomerID @"customerID"
#define kCustomerSearch @"customerLookUp"
#define kNotificationInventory @"Inventory"

////////////////////DELIVERY AGENT INFORMATION///////////////////////////
#define kDeliveryAgentID @"deliveryAgentID"
#define kDeliveryAgentName @"deliveryAgentName"
#define kDeliveryTime @"deliveryTime"

////////////////////DELIVERY AGENT INFORMATION///////////////////////////

////////////////////TABBED AMOUNT/////////////////////////
#define kTabAmount @"orderTabAmountEntered"
#define kTabBool @"orderTabAmount"

///////////////////////////////////////////////////////////////////////////
//KhalilG

//Farhan Ghouri
#define kCustomerListData @"customerListData"
#define kDeliveryAgentsData @"deliveryAgentsData"


#define keyStatus @"status"
#define keyStatusMessage @"statusDesc"


#define kCustomerTypeID @"CustomerTypeID"
#define kCustomerTypeDesc @"CustomerTypeDesc"


#define kNotificationTypeDelivery @"TypeDelivery"


#define kNotificationAttendance @"Attendance"
#define kNotificationBreakList @"breakList"




#define kNotificationSubmitOrderCheckout @"SubmitOrderCheckout"
#define kNotificationUpdateGuest @"UpdateGuest"
#define kNotificationSyncApp @"SyncApp"

#define kNotificationUserRequireReLogin @"UserRequiredReLogin"
#define kNotificationUserLogin @"UserLoginStatus"
#define kNotificationUserLogout @"UserLogoutStatus"
#define kNotificationLast12HoursOrder @"Last12HoursOrder"
#define kNotificationOrderDetail @"OrderDetail"
#define kNotificationSubmitOrder @"SubmitOrder"
#define kNotificationSubmitOrderIsReadyStatus @"SubmitOrderIsReadyStatus"
#define kNotificationSubmitOrderServedStatus @"SubmitOrderServedStatus"
#define kNotificationSubmitOrderCheckout @"SubmitOrderCheckout"
#define kNotificationTables @"Tables"
#define kNotificationSubmitTableStatus @"SubmitTableStatus"
#define kNotificationOfNotifier @"Notifier"
#define kNotificationNotifierNewOrders @"NotifierNewOrders"
#define kNotificationNotifierOrdersUpdated @"NotifierOrdersUpdated"
#define kNotificationNotifierTableStatusUpdated @"NotifierTableStatusUpdated"
#define kNotificationItemDetail @"ReceivedItemDetail"
#define kNotificationCustomerLook @"CustomerLookUp"
#define kNotificationCustomerDetail @"DetailOfCustomer"

#define kNotificationAddTables @"AddTables"

#define kNotificationCustomerHistory @"CustomerHistory"

#define kNotificationOrdersReport @"OrdersReoprt"
//abduls
#define kNotificationSubmitTableState @"SubmitTableState"
#define kNotificationSubmitTableSwapTo @"SubmitTableSwapTo"

#define kNotificationCustomerOrderDetail @"CustomerOrderDetail"
#define kNotificationGetRegister @"GetRegister"


#define kNotificationTableReservation @"TableReservation"


#define kNotificationNotifierNewReservation @"NotifierNewReservation"
#define kNotificationNotifierListUpdated @"NotifierListUpdated"
#define kNotificationLast24HoursReservation @"Last24HoursReservation"
#define kNotificationListDetail @"ListDetail"
#define kNotificationUpdateReservation @"UpdateTableReservation"


//#define kNotificationSubmitTableSwap @"Last12HoursOrder"
//abdule
#define kListBackendID @"listBackendID"
//saad start

#define kNotificationSearchMenu @"SubmitSearchMenu"

// saad end
#define kNotificationSubmitOrderCheckoutiPad @"SubmitOrderCheckoutiPad"

/***************************/

#define kNotificationUserImageDownloaded @"UserImageDownloaded"
#define kNotificationMenuImageDownloaded @"MenuImageDownloaded"
#define kNotificationCategoryImageDownloaded @"CategoryImageDownloaded"
#define kNotificationCategoryItemImageDownloaded @"CategoryItemImageDownloaded"
#define kNotificationSubItemImageDownloaded @"SubItemImageDownloaded"
#define kNotificationSideItemImageDownloaded @"SideItemImageDownloaded"
#define kNotificationTableTypeImageDownloaded @"TableTypeImageDownloaded"

#define kNotificationMenuHiResImageDownloaded @"MenuHiResImageDownloaded"
#define kNotificationCategoryHiResImageDownloaded @"CategoryHiResImageDownloaded"
#define kNotificationCategoryItemHiResImageDownloaded @"CategoryItemHiResImageDownloaded"
#define kNotificationSubItemHiResImageDownloaded @"SubItemHiResImageDownloaded"
#define kNotificationSideItemHiResImageDownloaded @"SideItemHiResImageDownloaded"


#define kNotificationUpdateTableTabItem @"updateTableTab"
#define kNotificationUpdateComplimentaryItem @"compliementaryItem"


#define kNotificationNewCustomer @"NewCustomer"

#define kNotificationCustomerList @"CustomerList"
#define kNotificationDeliveryAgent @"DeliveryAgent"
#define kNotificationUpdateDeliveryAgent @"UpdateDeliveryAgent"


#define kNotificationSearchMenuController @"SearchMenu"
#define kNotificationNewOrderReports @"NewReport"

#define kNotificationCardSwiped @"CardSwiped"


#define kNotificationCustomerTypeList @"CustomerTypeList"


#define keyCardReadStatus @"CardReadStatus"

#define kWaiter @"Waiter"
#define kKitchen @"Kitchen"
#define kManager @"Manager"

#define kTableStatusNormal @"Normal"
#define kTableStatusOccupied @"Occupied"
#define kTableStatusReserved @"Reserved"
#define kTableStatusBlocked @"Blocked"


typedef enum {
	UserTypeWaiter,
	UserTypeKitchen,
	UserTypeManager,
} UserType;


typedef enum {
	TableStatusNormal = 201,
	TableStatusReserved,
	TableStatusOccupied,
	TableStatusBlocked,
    TableStatusRecipetGiven,

} TableStatus;

typedef enum {
	OrderStatusInProcess, //Order taking in process by waiter
	OrderStatusInKitchen = 301,
	OrderStatusServed,
	OrderStatusCheckedOut,
	OrderStatusCancelled,
	OrderStatusIsReady,
    
} OrderStatus;

typedef enum {
	Added,
	Updated,
	Deleted,
} OrderItemState;

typedef enum {
	Cash = 1,
	Card,
	Void,
}PaymentMode;