/* 
 * Copyright (c) 2009 Keith Lazuka
 * License: http://www.opensource.org/licenses/mit-license.html
 */

@interface NSDate (NSDateAdditions)
//Minutes
- (NSDate *)cc_dateByMovingToMinutes:(NSInteger) minutes;

// Day
- (NSDate *)cc_dateByMovingToLast12Hours;
- (NSDate *)cc_dateByMovingToBeginningOfDay;
- (NSString*)dateForBackendRequestFormat;

@end