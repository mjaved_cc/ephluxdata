/* 
 * Copyright (c) 2009 Keith Lazuka
 * License: http://www.opensource.org/licenses/mit-license.html
 */

#import "NSDateAdditions.h"

@implementation NSDate (NSDateAdditions)

- (NSDate *)cc_dateByMovingToMinutes:(NSInteger) minutes
{
	unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents* parts = [[NSCalendar currentCalendar] components:flags fromDate:self];
	[parts setMinute:[parts minute] - minutes];
	return [[NSCalendar currentCalendar] dateFromComponents:parts];
}

- (NSDate *)cc_dateByMovingToLast12Hours 
{
	unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDate *startDate = [self cc_dateByMovingToBeginningOfDay];
	NSDateComponents* parts = [[NSCalendar currentCalendar] components:flags fromDate:startDate];
	[parts setHour:[parts hour] - 12];
	return [[NSCalendar currentCalendar] dateFromComponents:parts];
}

- (NSDate *)cc_dateByMovingToBeginningOfDay
{
	unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents* parts = [[NSCalendar currentCalendar] components:flags fromDate:self];
	[parts setHour:0];
	[parts setMinute:0];
	[parts setSecond:0];
	return [[NSCalendar currentCalendar] dateFromComponents:parts];
}

- (NSString*)dateForBackendRequestFormat 
{
	NSLocale *POSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] ;
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:POSIXLocale];  
	[dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"]; //.SSSS 
	
	NSString *formattedDateString = [dateFormatter stringFromDate:self];
	
	return formattedDateString;	
}


@end