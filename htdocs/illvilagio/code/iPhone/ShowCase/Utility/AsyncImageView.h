//
//  AsyncImageView.h
//  YellowJacket
//
//  Created by Wayne Cochran on 7/26/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//


//
// Code heavily lifted from here:
// 
//

#import <UIKit/UIKit.h>

@interface AsyncImageView : UIView {
    NSURLConnection *connection;
    NSMutableData *data;
    NSString *urlString; // key for image cache dictionary
}

@property (nonatomic,retain) NSMutableData *data;

-(void)loadImageFromURL:(NSURL*)url;
-(void)loadImageFromURL:(NSURL*)url withSize:(CGSize)size;
-(void)loadImageFromDirectry:(NSString *)url;
-(void)loadImageFromNSBundle:(NSString *)aImageName;
@end
