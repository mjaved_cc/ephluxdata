//
//  Functions.m
//
//
//  Created by  on 2/24/11.
//  Copyright . All rights reserved.
//

#import "Functions.h"
#import "Defines.h"

@implementation Functions

NSString *lang=@"en";
NSInteger langIsArabic=0;

+(BOOL)isIPad {
	static int device = -1;
	if(device == -1) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
		if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
			device = YES;
		}
		else
#endif
			device = NO;
    }
	return device;
}
//+(id)allocUniversalViewController:(NSString*)controllerName {
+(UIViewController *)allocUniversalViewController:(NSString*)controllerName {
	id controller = nil;
	Class viewControllerClass = (NSClassFromString(controllerName));
	if (viewControllerClass != nil) {
		if ([Functions isIPad]) {
			controller = [[viewControllerClass alloc] initWithNibName:[NSString stringWithFormat:@"%@-iPad", controllerName] bundle:nil];
		}
		else {
			controller = [[viewControllerClass alloc] initWithNibName:controllerName bundle:nil];
		}
	}
	return controller;
}
+(NSString*)getUniversalImageName:(NSString*)imageName {
	if ([Functions isIPad])
		return [imageName stringByReplacingOccurrencesOfString:@"." withString:@"-iPad."];
	
	return imageName;
}
+(NSMutableArray*) getDateFromJSON:(NSString*) key{
    @try {
        NSMutableData *myEncodedObject2=[NSMutableData data];;
        
        if(langIsArabic){
            myEncodedObject2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"allinformation_Arabic"];
            //NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:myEncodedObject2];
            //NSDictionary *obj = (NSDictionary *)archiver;
            NSDictionary *obj = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject2];
            
            if(obj!=Nil){
            if([[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:[NSString stringWithFormat:@"%@_count",key]] integerValue]>0 || ([key isEqualToString:@"deals"] || [key isEqualToString:@"homeEvents"])){
                
                NSLog(@"Default Set %@",[NSString stringWithFormat:@"%@_arabic",key]);
                
                if([[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key] count]>0){
                    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key]];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:myEncodedObject forKey:[NSString stringWithFormat:@"%@_arabic",key]];
                    [defaults synchronize];
                    return [[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key];
                }else{
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSData *myEncodedObject = [defaults objectForKey:[NSString stringWithFormat:@"%@_arabic",key]];
                    NSMutableArray *obj = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
                    
                    return obj;
                }
            }
            }
            
        }else{
            myEncodedObject2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"allinformation_English"];
            //NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:myEncodedObject2];
            //NSDictionary *obj = (NSDictionary *)archiver;
            //NSDictionary *obj = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject2];
            
            NSDictionary *obj = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject2];
             if(obj!=Nil){
            if([[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:[NSString stringWithFormat:@"%@_count",key]] integerValue]>0 || ([key isEqualToString:@"deals"] || [key isEqualToString:@"homeEvents"])){
                
                
                if([[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key] count]>0){
                    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key]];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:myEncodedObject forKey:key];
                    [defaults synchronize];
                    return [[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key];
                }else{
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSData *myEncodedObject = [defaults objectForKey:key];
                    NSMutableArray *obj = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
                    
                    return obj;
                }
            }
        }
        }
    }
    //return [[[obj objectForKey:@"status"]objectForKey:@"body"] objectForKey:key];
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return 0;
    
}
+(NSMutableArray*) getDataFromNSUserDefault_Key: (NSString*) key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject;
    if(langIsArabic){
        NSLog(@"%@",[NSString stringWithFormat:@"%@_arabic",key]);
        myEncodedObject = [defaults objectForKey:[NSString stringWithFormat:@"%@_arabic",key]];
        
    }else{
        myEncodedObject = [defaults objectForKey:key];
    }
    NSMutableArray *obj = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    
    return obj;
    
    
}
+(id)loadUniversalNibNamed:(NSString*)nibName owner:(id)owner {
    
    //if ([Functions isIPad])
    ///	nibName = [NSString stringWithFormat:@"%@-iPad", nibName];
	
	NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibName owner:owner options:nil];
	return [nib lastObject];
}
+(NSString*)deviceInString {
	UIDevice *device = [UIDevice currentDevice];
	NSString *deviceType = [device model];
	NSLog(@"Devie Type: %@", deviceType);
	return deviceType;
}
+(NSString*)localLanguage {
	NSLocale* curentLocale = [NSLocale currentLocale];
	NSString *localIdentifier = [curentLocale localeIdentifier];
	NSLog(@"Local Identifier: %@", localIdentifier);
	return localIdentifier;
}
+(NSString*)applicationVersion {
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}
+(NSString*)deviceIdentifier {
	return nil;//[[UIDevice currentDevice] uniqueIdentifier];
}
+(NSDate*)dateForStoreReceivedFromBackend:(NSString*)receivedDate {
	//Date received form backend is in the formast "YYYY-MM-DD-HH-MM-SS"
	NSArray *array = [receivedDate componentsSeparatedByString:@"-"];
	
	if(array == nil || [array count] != 6) return [NSDate date];
	
	unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
	NSDateComponents* parts = [[NSCalendar currentCalendar] components:flags fromDate:[NSDate date]];
	
    int hours =[[array objectAtIndex:3] intValue];
    [parts setYear:[[array objectAtIndex:0] intValue]];
	[parts setMonth:[[array objectAtIndex:1] intValue]];
	[parts setDay:[[array objectAtIndex:2] intValue]];
	[parts setHour:hours];
	[parts setMinute:[[array objectAtIndex:4] intValue]];
	[parts setSecond:[[array objectAtIndex:5] intValue]];
	return [[NSCalendar currentCalendar] dateFromComponents:parts];
}
+(void)showAlert:(NSString*)title message:(NSString*)message {
    NSString* alertBtnOk=NSLocalizedString(@"OK" ,@"OK" );
    
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:alertBtnOk otherButtonTitles:nil];
    
    UIImage *alertImage = [UIImage imageNamed:@""];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:alertImage];
    backgroundImageView.contentMode = UIViewContentModeScaleToFill;
    backgroundImageView.backgroundColor = [UIColor darkGrayColor];
    [backgroundImageView.layer setCornerRadius:10.0];
    backgroundImageView.layer.borderWidth=0.5;
    backgroundImageView.layer.borderColor= [[UIColor whiteColor] CGColor];
    
    
    [alertView addSubview:backgroundImageView];
    [alertView sendSubviewToBack:backgroundImageView];
    
    [alertView show];
    
    backgroundImageView.frame = CGRectMake(0, 0, 282, alertView.frame.size.height-18);
    
}


+(void)showAlertCancel:(NSString*)title message:(NSString*)message : (UIViewController*)controller {
    
    NSString* alertBtnOk=NSLocalizedString(@"OK" ,@"OK" );
    NSString* alertBtnCancle=NSLocalizedString(@"Cancel" ,@"Cancel" );
    
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:controller cancelButtonTitle:alertBtnOk otherButtonTitles:alertBtnCancle, nil];
    
    
    
    UIImage *alertImage = [UIImage imageNamed:@""];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:alertImage];
    backgroundImageView.contentMode = UIViewContentModeScaleToFill;
    backgroundImageView.backgroundColor = [UIColor darkGrayColor];
    [backgroundImageView.layer setCornerRadius:10.0];
    backgroundImageView.layer.borderWidth=0.5;
    backgroundImageView.layer.borderColor= [[UIColor whiteColor] CGColor];
    
    
    [alertView addSubview:backgroundImageView];
    [alertView sendSubviewToBack:backgroundImageView];
    [alertView show];
    backgroundImageView.frame = CGRectMake(0, 0, 282, alertView.frame.size.height-18);
    
    backgroundImageView =nil;
    
}

+(void)showAlert:(NSString*)title message:(NSString*)message tag:(NSInteger)tag {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK" ,@"OK") otherButtonTitles:nil];
	alert.tag = tag;
	[alert show];
}

+(NSString*)trim:(NSString *)value {
	return [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
+(NSString*)stateEnumToString:(int)state {
    // NSLog(@"stateEnumToString %d",state);
	switch (state) {
		case Added:
			return @"Add";
		case Updated:
			return @"Update";
		case Deleted:
			return @"Delete";
	}
	return @"";
}
//CHANGE OrderStatus to int on 12t
#pragma mark -
#pragma mark Folder Paths for Different Types of Images
+(NSString*)filePathForMenuImage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"Menu"];
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
	
	return fullPath;
}
+(NSString*)filePathForCategoryImage {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"Categories"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
	
    return fullPath;
}
+(NSString*)filePathForCategoryItemImage {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"CategoryItems"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:fullPath]) {
        NSError *error=nil;
        @try {
            [fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:&error];
        }
        @catch (NSException *exception) {
            NSLog(@"Error %@", [error description]);
        }
        
    }
	return fullPath;
}
+(NSString*)filePathForSubItemImage {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"SubItems"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
	
	return fullPath;
}
+(NSString*)filePathForSideItemImage {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"SideItems"];
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
    
	return fullPath;
}

+(NSString*)filePathForTableTypeImage {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"TableTypes"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
	
	return fullPath;
}
+(NSString*)filePathForOrderAndInvoiceImage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"OrderAndInvoice"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	[fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:nil];
	
	return fullPath;
}

#pragma mark -
#pragma mark Image
+(NSString*)localImageThumbName:(NSString*)url backendID:(int)backendID {
	NSString *extension = [url pathExtension];
	NSString *fileName = [NSString stringWithFormat:@"%d-Thumb.png", backendID, extension];
	return fileName;
}
+(NSString*)localImageHiResName:(NSString*)url backendID:(int)backendID {
	NSString *extension = [url pathExtension];
	NSString *fileName = [NSString stringWithFormat:@"%d.%@",backendID, extension];
	return fileName;
}
+(NSString*)highResImageURL:(NSString*)path {
	NSString *highResImageURLPath = [path stringByReplacingOccurrencesOfString:@"-Thumb" withString:@""];
	return highResImageURLPath;
}

+(NSString*)localImageTableType:(NSString*)url state:(NSString*)state backendID:(int)backendID {
	NSString *extension = [url pathExtension];
	NSString *fileName = [NSString stringWithFormat:@"%d-%@.%@",backendID, state, extension];
	return fileName;
}
+(NSString*)currentDateTimeInString {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyyMMddhhmmss"];
	NSString *date = [dateFormatter stringFromDate:[NSDate date]];
	
	return date;
}

static BOOL useResourceJSONFile = NO;
+(void)useResourceJSONFile:(BOOL)flag {
	useResourceJSONFile = flag;
}
+(BOOL)isResourceJSONFile {
	return useResourceJSONFile;
}


#pragma mark add order popup

+(int)getOrderType_old{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults integerForKey:kaddOrderPopup];
    
}
+(int)getOrderType{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults integerForKey:kOrderType];
    
}
+(void)setOrderType:(int)value {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:kOrderType];
    [defaults synchronize];
}

+(BOOL)isDeliveryTarget{
    
#if defined(DELIVERY)
    
    return TRUE;
    
#else
    
    return FALSE;
    
#endif
    
}

+(int)getNumberOfPeople{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults integerForKey:kNumberOfPeople];
    
}

+(NSDictionary*)getOrderInformation{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults objectForKey:kOrderInformation];
    
}


#pragma mark -
#pragma mark Setting Related Functions
+(NSString*)settingBaseURL {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults objectForKey:kSettingBaseURL];
}
+(double)settingNotificationDelay {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults doubleForKey:kSettingNotificationDelay];
}
+(BOOL)settingSoundEnabled {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults boolForKey:kSettingEnableSound];
}

+(NSString *) replaceNullFromString:(NSString *)original
{
    NSMutableString * newString = [NSMutableString stringWithString:original];
    
    NSRange foundRange = [original rangeOfString:@"(null)"];
    if (foundRange.location != NSNotFound)
    {
        [newString replaceCharactersInRange:foundRange
                                 withString:@""];
    }
    
    return newString;// [[newString retain] autorelease];
}
#pragma mark dateForReservation


+(NSString*)dateForReservation:(NSString*)receivedDate {
    NSDate *lastUpdatedDate = [self dateForStoreReceivedFromBackend:receivedDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE, dd MMM yyyy"];
    //conversion of NSString to NSDate
    NSString* dateFromString = [formatter stringFromDate:lastUpdatedDate];
    return dateFromString;
}

#pragma mark-
#pragma mark-Resize Image
- (UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

#pragma Navigation Bar

+(void)setNavigationBar{
    UIImage *navBarImage;
    if([self isIPad]){
        navBarImage = [UIImage imageNamed:@"header_bar.png"];
    }else{
        navBarImage = [UIImage imageNamed:@"iphone_top_bar_bg.png"];
    }
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor:  UIColorFromRGB(0x5b1900),
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: [UIFont fontWithName:@"Script MT bold" size:22.0f]
     }];
    
    [[UINavigationBar appearance] setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
    
}


+(UIBarButtonItem*)setBackBarButton:(id)object{
    
    UIImage* image = [UIImage imageNamed:@"iphone_btn_back_normal.png"];
    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);
    UIButton* someButton = [[UIButton alloc] initWithFrame:frame];
    someButton.tag=0;
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    [someButton setBackgroundImage:[UIImage imageNamed:@"iphone_btn_back_pressed.png"] forState:UIControlStateSelected];
    [someButton addTarget:object action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:someButton];
    return someBarButtonItem;
    
}

#pragma mark DAteFormate in July 29, 2013
+(NSString* ) dateFormate:(NSString* ) mydate{
    
    NSString *myString = mydate;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"MMMM dd, yyyy";
    return [dateFormatter stringFromDate:yourDate];
}
#pragma mark DateFormate in 2013-07-29 11:00:00
+(NSString* ) DateFormateFunctionDate:(NSString*)date Time:(NSString*) time{
    NSString *myString =[NSString stringWithFormat:@"%@ %@",date,time];
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setAMSymbol:@"AM"];
    [FormatDate setPMSymbol:@"PM"];
    FormatDate.dateFormat = @"MMMM dd, yyyy hh:mm a";
    NSDate *yourDate = [FormatDate dateFromString:myString];
    FormatDate.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [FormatDate stringFromDate:yourDate];
}

#pragma mark NavigationBtn
+(UIButton*)setNavigationBackBarButton{
    
    UIImage* image = [UIImage imageNamed:@"iphone_btn_back_normal.png"];
    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);
    UIButton* someButton = [[UIButton alloc] initWithFrame:frame];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    [someButton setBackgroundImage:[UIImage imageNamed:@"iphone_btn_back_pressed.png"] forState:UIControlStateSelected];
    return someButton;
    
    
}
#pragma mark Local Lang
+(NSString *) getLocalLang{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    return currentLanguage;
    
}

+(NSString*) getLocalizedNumber:(NSString*)num{
    
    NSString *lang = [self getLocalLang]; // get from function
    
    if ([lang isEqualToString:@"ar"] ) {
        
        num=[num stringByReplacingOccurrencesOfString:@"0" withString:@"٠"];
        
        num=[num stringByReplacingOccurrencesOfString:@"1" withString:@"١"];
        
        num=[num stringByReplacingOccurrencesOfString:@"2" withString:@"٢"];
        
        num=[num stringByReplacingOccurrencesOfString:@"3" withString:@"٣"];
        
        num=[num stringByReplacingOccurrencesOfString:@"4" withString:@"٤"];
        
        num=[num stringByReplacingOccurrencesOfString:@"5" withString:@"٥"];
        
        num=[num stringByReplacingOccurrencesOfString:@"6" withString:@"٦"];
        
        num=[num stringByReplacingOccurrencesOfString:@"7" withString:@"٧"];
        
        num=[num stringByReplacingOccurrencesOfString:@"8" withString:@"٨"];
        
        num=[num stringByReplacingOccurrencesOfString:@"9" withString:@"٩"];
        
        num=[num stringByReplacingOccurrencesOfString:@"GMT" withString:@"جي أم تي"];
        
    }
    
    
    return num;
    
}
+(NSString*) getLocalizedDate:(NSString*)date{
   
    NSString *lang = [self getLocalLang]; // get from function
    
    if ([lang isEqualToString:@"ar"] ) {
        
        date=[date stringByReplacingOccurrencesOfString:@"January" withString:@"يناير"];
        
        date=[date stringByReplacingOccurrencesOfString:@"February" withString:@"فبراير"];
        
        date=[date stringByReplacingOccurrencesOfString:@"March" withString:@"مارس"];
        
        date=[date stringByReplacingOccurrencesOfString:@"April" withString:@"أبريل"];
        
        date=[date stringByReplacingOccurrencesOfString:@"May" withString:@"مايو"];
        
        date=[date stringByReplacingOccurrencesOfString:@"June" withString:@"يونيو"];
        
        date=[date stringByReplacingOccurrencesOfString:@"July" withString:@"يوليو"];
        
        date=[date stringByReplacingOccurrencesOfString:@"August" withString:@"أغسطس"];
        
        date=[date stringByReplacingOccurrencesOfString:@"September" withString:@"سبتمبر"];
        
        date=[date stringByReplacingOccurrencesOfString:@"October" withString:@"أكتوبر"];
        
        date=[date stringByReplacingOccurrencesOfString:@"November" withString:@"نوفمبر"];
        
        date=[date stringByReplacingOccurrencesOfString:@"December" withString:@"ديسمبر"];
        
        
    }
    
    
    return [self getLocalizedNumber:date];
    
}
+(CGSize) screenSize{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    return result;
}
@end
