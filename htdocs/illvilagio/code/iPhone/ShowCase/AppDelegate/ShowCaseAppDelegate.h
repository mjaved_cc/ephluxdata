//
//  ShowCaseAppDelegate.h
//  ShowCase
//
//  Created by USER on 3/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MBProgressHUD;



@interface ShowCaseAppDelegate : UIResponder <UIApplicationDelegate>{

    MBProgressHUD *progressHud;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) MBProgressHUD *progressHud;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)timerTicked:(NSTimer*)theTimer;


@end
