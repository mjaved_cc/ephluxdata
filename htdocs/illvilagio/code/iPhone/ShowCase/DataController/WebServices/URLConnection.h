//
//  URLConnection.h
//
//  Created by  on 11/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    URLConnectionTypeSync,
	URLConnectionTypeLogin,
    URLConnectionTypeLogout,
    URLConnectionTypeGallery,
    URLConnectionTypeSubmitOrderStatusCheckout,
    URLConnectionTypeSubmitOrder,
    URLConnectionTypeAddFeedback,
    URLConnectionTypeAddResevation,
    URLConnectionTypeEvents,
    URLConnectionTypeContent,
    URLConnectionTypeAllContent,
    URLConnectionTypeHomeContent,
    URLConnectionTypeAddDeviceID,
} URLConnectionType;

@interface URLConnection : NSURLConnection {
	URLConnectionType connectionType;
	id param;
	NSMutableData *data;
}

@property (nonatomic, assign) URLConnectionType connectionType;
@property (nonatomic, retain) id param;
@property (nonatomic, retain) NSMutableData *data;

@end
