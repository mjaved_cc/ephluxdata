//
//  MenuParser.h
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GalleryParser : NSObject {

}

+(void)parseData:(NSArray*)data;
-(void)parseData:(NSArray *)data parent:(NSManagedObject*)obj ;
@end
