//
//  CategoryItemsParser.m
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryItemsParser.h"

#import "Defines.h"
#import "DataStoreManager.h"
#import "DataStoreManager + DataSpecialized.h"
#import "CategoryItem+Image.h"
#import "ImageDownloadManager.h"

@implementation CategoryItemsParser

+(void)parseData:(NSArray *)data parent:(NSManagedObject*)obj {
	NSArray *userData = data ;
	
	DataStoreManager *manager = [DataStoreManager manager];
    
    for(NSDictionary *menuData in userData) {
        BOOL updated = NO;
        int backendID = [[menuData objectForKey:@"id"] intValue];
        NSDate *lastUpdatedDate = [Functions dateForStoreReceivedFromBackend:[menuData objectForKey:@"lastUpdate"]];
        
        CategoryItem *categoryItem = [manager getCategoryItem:backendID];
        
        if(categoryItem == nil) {
            updated = YES;
            categoryItem = [manager createCategoryItem];
        }
        
        if(categoryItem.lastUpdatedDate == nil || ([categoryItem.lastUpdatedDate compare:lastUpdatedDate] != NSOrderedSame))
            updated = YES;
        
        if(updated) {
            @try {
                            
                categoryItem.active = [NSNumber numberWithInt:[[menuData objectForKey:@"status"] boolValue]];
                categoryItem.lastUpdatedDate = lastUpdatedDate;
                categoryItem.backendItemID = [NSNumber numberWithInt:[[menuData objectForKey:@"id"] intValue]];
                
                categoryItem.backendID = [NSNumber numberWithInt:backendID];
                categoryItem.desc = [menuData objectForKey:@"description"];
                categoryItem.desc_arabic = [menuData objectForKey:@"descriptionArabic"];
                categoryItem.price = [NSNumber numberWithFloat:[[menuData objectForKey:@"price"] floatValue]];

                categoryItem.displayOrder = [NSNumber numberWithInt:[[menuData objectForKey:@"displayOrder"] intValue]];
                
                if([menuData objectForKey:@"isSidelineCategory"]!=nil){
                    
                categoryItem.isSideLine = [NSNumber numberWithInt:[[menuData objectForKey:@"isSidelineCategory"] intValue]];
                    
                }else{
                    
                    categoryItem.isSideLine = [NSNumber numberWithInt:0];
                    
                }
                
                
                categoryItem.imageURL = [menuData objectForKey:@"imageUrl"];
                categoryItem.name = [menuData objectForKey:@"name"];
                categoryItem.name_arabic = [menuData objectForKey:@"nameArabic"];
                categoryItem.unitType=@"";
                categoryItem.forcedItem= [NSNumber numberWithInt:0];
                categoryItem.category = (Category*)obj;
                
                if ([NSNull null]!=(NSNull*) [menuData objectForKey:@"imageUrl"]){
                    NSString *fileName = [Functions localImageThumbName:[menuData objectForKey:@"imageUrl"] backendID:[[menuData objectForKey:@"id"] intValue]];
                    NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
                    if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) {
                        [[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
                    }
                }
             
            }
            @catch (NSException *exception) {
                NSLog(@"CategoryItemParser Exception  %@",exception.userInfo);
            }
        }
        else {
            @try {
                
                //[categoryItem startDownloadThumbImage];
            }
            @catch (NSException *exception) {
                NSLog(@"No Images %@",exception);
                
            }
        }
        
        
    }
    
    
    
	
}


@end
