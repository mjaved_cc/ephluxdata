//
//  MenuParser.m
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MenuParser.h"


#import "Functions.h"
#import "DataStoreManager.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Menu+Image.h"

@implementation GalleryParser

+(void)parseData:(NSArray *)data {
	NSArray *userData = data;
    //NSLog(@"Menu Parser %@",userData);
    DataStoreManager *manager = [DataStoreManager manager];
	
    for(NSDictionary *galleryData in userData) {
		//NSLog(@"M<enu Data %@",menuData);
		BOOL updated = NO;
		
        int backendID = [[galleryData objectForKey:@"id"] intValue];
		
        //NSDate *lastUpdatedDate = [Functions dateForStoreReceivedFromBackend:[menuData objectForKey:@"lastUpdate"]];
		
		Gallery *gallery = [manager getGallery:backendID];
		
        if(gallery == nil) {
			updated = YES;
			gallery = [manager createGallery];
		}
        
		if(updated) {
            
			gallery.galleryID = [NSNumber numberWithInt:[[galleryData objectForKey:@"id"] intValue] ];
			gallery.title =[galleryData objectForKey:@"Title"];
			gallery.imageURL = [galleryData objectForKey:@"imageUrl"];
            gallery.Title_arabic =[galleryData objectForKey:@"Title_arabic"];
			gallery.caption = [galleryData objectForKey:@"caption"];
            gallery.caption_arabic =[galleryData objectForKey:@"caption_arabic"];
                    
		}
        
        @try {
            
            
        }
        @catch (NSException *exception) {
            NSLog(@"Categoreise Exception ");
        }
        
		
	}
    
}

@end
