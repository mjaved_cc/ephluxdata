//
//  MenuParser.m
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MenuParser.h"
#import "CategoryParser.h"

#import "Functions.h"
#import "DataStoreManager.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Menu+Image.h"

@implementation MenuParser

+(void)parseData:(NSArray *)data {
	NSArray *userData = data;
    //NSLog(@"Menu Parser %@",userData);
    DataStoreManager *manager = [DataStoreManager manager];
	
    for(NSDictionary *menuData in userData) {
		//NSLog(@"M<enu Data %@",menuData);
		BOOL updated = NO;
		
        int backendID = 1;
		
        //NSDate *lastUpdatedDate = [Functions dateForStoreReceivedFromBackend:[menuData objectForKey:@"lastUpdate"]];
		
		Menu *menu = [manager getMenu:backendID];
		
        if(menu == nil) {
			updated = YES;
			menu = [manager createMenu];
		}
        
		if(updated) {
            
			menu.backendID = [NSNumber numberWithInt:1];
			menu.active = [NSNumber numberWithInt:1];
			menu.displayOrder = [NSNumber numberWithInt:1];
			menu.imageURL = @"";
			menu.name = @"IllVilagio Menu";
			menu.lastUpdatedDate = [NSDate date];
		}
        
        @try {
            
            [CategoryParser parseData:[userData valueForKey:@"category"] parent:menu];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Categoreise Exception ");
        }
        
		
	}
    
}

@end
