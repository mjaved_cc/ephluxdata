//
//  CategoryParser.m
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryParser.h"
#import "CategoryItemsParser.h"


#import "Defines.h"
#import "DataStoreManager.h"
#import "DataStoreManager + DataSpecialized.h"
#import "Category+Image.h"


#import "ImageDownloadManager.h"

@implementation CategoryParser

+(void)parseData:(NSArray *)data parent:(NSManagedObject*)obj {
	
    NSLog(@"CategoryParser");
    
    NSArray *userData = data;
    DataStoreManager *manager = [DataStoreManager manager];
    @try {
        for(NSDictionary *menuData in userData) {
            BOOL updated = NO;
            int backendID = [[menuData objectForKey:@"id"] intValue];
            
            NSDate *lastUpdatedDate = [Functions dateForStoreReceivedFromBackend:[menuData objectForKey:@"lastUpdate"]];
            
            
            Category *category = [manager getCategory:backendID];
            
            if(category == nil) {
                updated = YES;
                category = [manager createCategory];
            }
            
            
            if(category.lastUpdatedDate != nil || ([category.lastUpdatedDate compare:lastUpdatedDate] != NSOrderedSame))
                updated = YES;
            
            updated = YES;
            if(updated) {
                @try {
                    category.active = [NSNumber numberWithInt:[[menuData objectForKey:@"status"] boolValue]];
                    category.allowedSideLine = [NSNumber numberWithInt:[[menuData objectForKey:@"allowSideline"] boolValue]];
                    category.isSidelineCategory = [NSNumber numberWithInt:[[menuData objectForKey:@"isSidelineCategory"] boolValue]];
                    category.imageURL = [menuData objectForKey:@"imageUrl"];
                    category.displayOrder = [NSNumber numberWithInt:[[menuData objectForKey:@"displayOrder"] intValue]];
                    category.backendID = [NSNumber numberWithInt:backendID];
                    category.name = [menuData objectForKey:@"name"];
                    category.name_arabic = [menuData objectForKey:@"nameArabic"];
                    category.lastUpdatedDate = lastUpdatedDate;
                    category.parentID = [NSNumber numberWithInt:[[menuData objectForKey:@"parentId"] intValue]];
                    category.menu = (Menu*)obj;
                    
                    /*@try {
                     [category startDownloadThumbImage];
                     }
                     @catch (NSException *exception) {
                     NSLog(@"Image Exception");
                     }
                     */
                    @try {
                        if([[menuData objectForKey:@"categoryItems"] count]>1)
                            [CategoryItemsParser parseData:[menuData objectForKey:@"categoryItems"] parent:category];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"IMAGE Exception");
                    }
                    
                }
                @catch (NSException *exception) {
                    //NSLog(@"CategoryParser Exception %@",exception);
                }
                
            }else {
                /* @try {
                 [category startDownloadThumbImage];
                 }
                 @catch (NSException *exception) {
                 NSLog(@"No Images %@",exception);
                 
                 }*/
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Main Categroy Exception %@",exception);
    }
    
	
}

@end
