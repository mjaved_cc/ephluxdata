//
//  CategoryParser.h
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CategoryParser : NSObject {

}

+(void)parseData:(NSArray*)data parent:(NSManagedObject*)obj;

@end
