//
//  DataStoreManager + DataSpecialized.h
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/NSFetchedResultsController.h>

#import "DataStoreManager.h"

#import "Menu.h"
#import "Category.h"
#import "CategoryItem.h"
#import "Configuration.h"
#import "Gallery.h"
//#import "InvoiceFormat.h"
#import "Order.h"

@interface DataStoreManager (DataSpecialized)

#pragma mark-
#pragma mark-Configuration

-(Configuration*)saveConfiguration;
-(NSArray*)getConfigurationData ;
-(void)deleteAllConfiguration ;

#pragma mark-
#pragma mark-Menu

-(Menu*)createMenu ;
-(Gallery*)createGallery ;
-(Menu*)getMenu:(int)backendID;
-(NSArray*)allMenues;
-(void)deleteAllMenu;


#pragma mark-
#pragma mark-Category

-(Category*)createCategory ;
-(NSArray*)getCategoryCount:(int)backendId ;

-(void)updateCategory:(int)backendId;
-(Category*)getCategory:(int)backendID ;

-(void)deleteAllCategories;

-(NSArray*)getDistinctCategoriesForPrinting ;
-(NSArray*)categoriesFor:(Menu*)menu ;
-(NSArray*)categoriesForWithParent : (Menu*)menu : (int)parentId;
-(NSArray*)categoriesForWithParentWithoutMenu :(int)parentId;
#pragma mark-
#pragma mark-CategoryItem 
-(NSArray *)getALlCategories;

-(CategoryItem*)createCategoryItem;
-(CategoryItem*)getCategoryItemByBackendID:(NSString*)backendID;
-(CategoryItem*)getCategoryItem:(int)backendID ;
-(Gallery*)getGallery:(int)backendID;

-(NSArray*)categoryItemsFor:(Category*)category ;
-(NSArray*)categoryItemsWithSideLineFor:(Category*)category : (BOOL)isSideLine;
-(NSArray*)getSideLineCategory;

-(NSArray*)getCatItems:(int)backendItemId ;
-(NSArray*)categoriesForParent : (Menu*)menu;

-(void)updateCategoryItem:(int)backendItemId ;
-(void)deleteAllCategoriesItems;

-(NSArray*)sortItemsByPrice:(Category*)category ;
-(NSArray*)sortItemsByName:(Category*)category ;
-(NSArray*)categoriesForWithChilds:(int)parentId;

-(NSFetchedResultsController*)searchText:(NSString *)searchText :(int)backEndId;
//-(NSFetchedResultsController*)searchText:(NSString *)backendID ;
#pragma mark-
#pragma mark-InvoiceFormat

//-(InvoiceFormat*)createInvoiceFormat ;
//-(NSArray*)invoiceArray;

//-(InvoiceFormat*)getInvoiceFormat;
//-(void)deleteAllInvoiceFormat;



#pragma mark-
#pragma mark-Order
-(Order*)createOrder;
-(Order*)getCategoryItemInOrder:(int)backendID;
-(Order*)getCategoryItemInOrder : (int)backendID :(int)seatNo;
-(NSArray*)getCategoryItemsInOrder : (int)backendID :(int)seatNo;
-(NSFetchedResultsController*)getAllOrderItems;
-(NSArray*)getAllOrderItem;
-(void)clearOrder;
-(Order*)getTotalQuantity;
-(void)deleteItemWithNoQuantity:(int)backendID;
-(void) AddOrderFunction:(id) sender Seat:(int)seatNo Qty:(int)orderQty;
-(void)deleteItemWithSeatNoID:(int)backendID SeatNo:(int) seatNo;
@end
