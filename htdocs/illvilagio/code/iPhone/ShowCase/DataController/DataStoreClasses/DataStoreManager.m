    //
//  DataStoreManager.m
//
//  Created by  on 30/01/10.
//  Copyright 2010. All rights reserved.
//

#import "DataStoreManager.h"
#import "Defines.h"
#define kDBName @"DataStore.sqlite"

static DataStoreManager *manager = NULL; 

@interface DataStoreManager (Hide)

-(NSString*)applicationDocumentsDirectory;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(void)copyDatabaseFileIfNeeded;

@end

@implementation DataStoreManager

+(DataStoreManager*)manager {
	
	@synchronized (self) {
		if (manager == NULL) {
			manager = [[self alloc] init];
			
			//TODO: Should be removed from final release
//			NSURL *storeUrl = [NSURL fileURLWithPath: [[manager applicationDocumentsDirectory] stringByAppendingPathComponent:kDBName]];
//			[[NSFileManager defaultManager] removeItemAtURL:storeUrl error:nil];
		}
	}

	return manager;
}

#pragma mark -
#pragma mark === Memory management  ===
#pragma mark



#pragma mark -
#pragma mark === Public Functions  ===
#pragma mark

-(void)saveData {
    NSError *error = nil;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			/*
			 Replace this implementation with code to handle the error appropriately.
			 
			 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, 
			 although it may be useful during development. If it is not possible to recover from the error, 
			 display an alert panel that instructs the user to quit the application by pressing the Home button.
			 */
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
        }
    }
}

-(void)deleteObject:(NSManagedObject*)deleteThisEntity {
	
	[managedObjectContext deleteObject:deleteThisEntity];

	// Commit the change.
	NSError *error; 
	if (![managedObjectContext save:&error]) { 
		// Handle the error. 
	}	
}

#pragma mark
#pragma mark-Delete The Files From InvoiceOrder Folders
- (void)deleteFilesFromDocuments {
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/OrderAndInvoice"];
	NSFileManager* fm = [[NSFileManager alloc] init];
	NSDirectoryEnumerator* en = [fm enumeratorAtPath:path];    
	NSError* err = nil;
	BOOL res;
	NSString* file;
	while (file = [en nextObject]) {
		
		if ([file rangeOfString:@".sqlite"].location == NSNotFound) {
			res = [fm removeItemAtPath:[path stringByAppendingPathComponent:file] error:&err];
			if (!res && err) {
				NSLog(@"oops: %@", err);
			}
		}
	}
}

- (void)deleteFilesFromDocumentsDocument:(NSString*)pathName {
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",pathName]];
	NSFileManager* fm = [[NSFileManager alloc] init];
	NSDirectoryEnumerator* en = [fm enumeratorAtPath:path];    
	NSError* err = nil;
	BOOL res;
	NSString* file;
	while (file = [en nextObject]) {
		
		if ([file rangeOfString:@".sqlite"].location == NSNotFound) {
			res = [fm removeItemAtPath:[path stringByAppendingPathComponent:file] error:&err];
			if (!res && err) {
				NSLog(@"oops: %@", err);
			}
		}
	}
}

#pragma mark -
#pragma mark === Public Utility Functions  ===
#pragma mark


#pragma mark -
#pragma mark === Private Helper Functions  ===
#pragma mark

#pragma mark -
#pragma mark Application's Documents directory
/**
 Returns the path to the application's Documents directory.
 */
-(NSString*)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark -
#pragma mark === Core Data stack  ===
#pragma mark
/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
-(NSManagedObjectContext*)managedObjectContext {
	
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
-(NSManagedObjectModel*)managedObjectModel {
	
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;    
    return managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
-(NSPersistentStoreCoordinator*)persistentStoreCoordinator {
	
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
	
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent:kDBName]];
	
	NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
		 
		 Typical reasons for an error here include:
		 * The persistent store is not accessible
		 * The schema for the persistent store is incompatible with current managed object model
		 Check the error message to determine what the actual problem was.
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }    
	
    return persistentStoreCoordinator;
}

@end