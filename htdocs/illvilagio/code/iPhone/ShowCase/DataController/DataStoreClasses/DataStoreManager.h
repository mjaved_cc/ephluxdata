//
//  DataStoreManager.h
//
//  Created by  on 30/01/10.
//  Copyright 2010. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataStoreManager : NSObject {
	NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;	    
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;

-(void)deleteObject:(NSManagedObject*)deleteThisEntity;
-(void)saveData;
- (void)deleteFilesFromDocuments;
- (void)deleteFilesFromDocumentsDocument:(NSString*)pathName;
+ (DataStoreManager*)manager;

@end
