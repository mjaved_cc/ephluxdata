//
//  DataStoreManager + DataSpecialized.m
//  POS
//
//  Created by admin on 27/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataStoreManager + DataSpecialized.h"
#import "NSDateAdditions.h"
#import "Defines.h"

@implementation DataStoreManager (DataSpecialized)

#pragma mark
#pragma mark-BillSplit Function



#pragma mark-
#pragma mark-Configuration

-(Configuration*)saveConfiguration{
    Configuration *obj=(Configuration*)[NSEntityDescription 
                                        insertNewObjectForEntityForName:NSStringFromClass([Configuration class])
                                        inManagedObjectContext:[self managedObjectContext]];
    
    return obj;
    
}
-(NSArray*)categoriesForWithParentWithoutMenu :(int)parentId {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active==1 AND parentID == %d AND isSidelineCategory == 0",parentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	NSLog(@"Result Count query %d",[results count]);
	return results;
}

-(NSArray*)getConfigurationData {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Configuration class]) inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
    NSError *error = nil; 
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    return results;
}

-(void)deleteAllConfiguration {
	NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([Configuration class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}
#pragma mark-
#pragma mark-Menu

-(Menu*)createMenu {
	Menu *obj = (Menu *)[NSEntityDescription
                         insertNewObjectForEntityForName:NSStringFromClass([Menu class])
                         inManagedObjectContext:[self managedObjectContext]];
	return obj;
}
-(Gallery*)createGallery {
	Gallery *obj = (Gallery *)[NSEntityDescription
                         insertNewObjectForEntityForName:NSStringFromClass([Gallery class])
                         inManagedObjectContext:[self managedObjectContext]];
	return obj;
}


-(Menu*)getMenu:(int)backendID {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Menu class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendID];
	[request setPredicate:predicate];
    
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	if(results == nil || [results count] == 0)
		return nil;
    
	return [results objectAtIndex:0];
}
-(Gallery*)getGallery:(int)backendID {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Gallery class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"galleryID == %d", backendID];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	if(results == nil || [results count] == 0)
		return nil;
    
	return [results objectAtIndex:0];
}

-(NSArray *)getALlCategories
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
//	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];

	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active==1"];

	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	return results;
}

-(NSArray*)allMenues {
	NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Menu class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1"];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	return results;
}


-(void)deleteAllMenu{
	NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([Menu class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}


#pragma mark-
#pragma mark-Category

-(Category*)createCategory {
	Category *obj = (Category *)[NSEntityDescription
                                 insertNewObjectForEntityForName:NSStringFromClass([Category class])
                                 inManagedObjectContext:[self managedObjectContext]];
	return obj;
}

-(NSArray*)getCategoryCount:(int)backendId {
    
    NSFetchRequest *request=[[NSFetchRequest alloc] init];
    [request setFetchBatchSize:20];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendId];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	return results;
}

-(void)updateCategory:(int)backendId {
    
    NSFetchRequest *request=[[NSFetchRequest alloc] init];
    [request setFetchBatchSize:20];
    //Replace CategoryItem with Category Class as app was crashing;
    //12th July 2012 3:49 PM
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendId];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
    
}

-(Category*)getCategory:(int)backendID {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendID];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	if(results == nil || [results count] == 0)
		return nil;
	
	return [results objectAtIndex:0];
}

-(void)deleteAllCategories{
	NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}

-(NSArray*)getDistinctCategoriesForPrinting {
	NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	
	// Following line has issues on hardware. When objects returned by this  
	// function (getDistinctCategoriesForPrinting) are accessed it causes 
	// crash on Hadrware, but works fine on Simulator.
	// Dont know why? People on internet suggested to comment this.
	// One reason could be, it (setFetchBatchSize) does not work properly 
	// when distinct is used to get managed objects.
	
	// In all above 'get' methods it works fine.
	//[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	[request setResultType:NSDictionaryResultType];
	[request setReturnsDistinctResults:YES];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1"];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
		
	return results;
}
-(NSArray*)categoriesFor:(Menu*)menu {
	NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"menu == %@ AND active == 1", menu];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
		
	return results;
}

//categoriesForParent
-(NSArray*)categoriesForParent : (Menu*)menu {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active==1 AND menu == %@",menu];
    
    //    //	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"menu == %@ AND active==1 AND parentID == %d", menu,parentId];

	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	return results;
}

#pragma mark get Parent Id
-(NSArray*)categoriesForWithChilds:(int)parentId
{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active==1 AND parentID == %d",parentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	return results;
}

-(NSArray*)categoriesForWithParent : (Menu*)menu : (int)parentId {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"menu == %@ AND active==1 AND parentID == %d", menu,parentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	return results;
}

#pragma mark-
#pragma mark-CategoryItem 


-(CategoryItem*)createCategoryItem {
	CategoryItem *obj = (CategoryItem *)[NSEntityDescription
                                         insertNewObjectForEntityForName:NSStringFromClass([CategoryItem class])
                                         inManagedObjectContext:[self managedObjectContext]];
	return obj;
}

-(CategoryItem*)getCategoryItemByBackendID:(NSString*)backendID{
    
    //NSLog(@"getCategoryItemByBackendID %@", backendID);
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendItemID = %@", backendID];
    
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	request = nil;
	entity=nil;
    request=nil;
    if(results == nil || [results count] == 0)
		return nil;
	
	return [results objectAtIndex:0];
}
-(CategoryItem*)getCategoryItem:(int)backendID {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendItemID == %d", backendID];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	
	if(results == nil || [results count] == 0)
		return nil;
	
	return [results objectAtIndex:0];
}


////////////////////////////////////
-(NSFetchedResultsController*)searchText:(NSString *)searchText :(int)backEndId{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
    BOOL isChildExist=FALSE;
    Category *matchCategory;
    NSArray *MoreCate;
   
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	
    [request setEntity:entity];
    request.fetchBatchSize = 20;
    
    NSString *searchString = searchText;
    NSArray *words = [searchString componentsSeparatedByString:@" "];
    NSMutableArray *predicateList = [NSMutableArray array];
    
        NSMutableString *stringToReturn = [[NSMutableString alloc] init];

    if(backEndId>0){
        
        matchCategory = [self getCategory:backEndId];
        MoreCate = [self categoriesForWithChilds:backEndId];
        if([MoreCate count]){
        
            isChildExist = TRUE;
            
        }
        
    }

    NSString *name;
    if(langIsArabic){
        
        name = @"name_arabic";
        
    }else{
        
        name= @"name";
    
    }
    
    for (NSString *word in words) {
        if ([word length] > 0) {
            NSPredicate *pred;
            
            if(backEndId >0)
            {
                if(isChildExist){
                
                    for(int child=0;child<[MoreCate count];child++)
                    {
                 
                        if(langIsArabic)
                            pred =  [NSPredicate predicateWithFormat:@"category == %@ and name_arabic contains[c] %@",[MoreCate objectAtIndex:child],name, word];
                            else
                            pred =  [NSPredicate predicateWithFormat:@"category == %@ and name contains[c] %@",[MoreCate objectAtIndex:child],name, word];
                        
                        [predicateList addObject:pred];
                    }
                    
                }else{
                    if(langIsArabic)
                    
                    pred =  [NSPredicate predicateWithFormat:@"category == %@ and name_arabic contains[c] %@",matchCategory,word];
                    
                    else
                        
                    pred =  [NSPredicate predicateWithFormat:@"category == %@ and name contains[c] %@",matchCategory,word];
                
                }
            }
            else{
                if(langIsArabic)
                    pred = [NSPredicate predicateWithFormat:@"name_arabic CONTAINS[c] %@", word];
                else
                    pred = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@", word];
                
            }
            
           if(!isChildExist)
               [predicateList addObject:pred];
        
        }
    }
    
    
    matchCategory=nil;
    
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicateList];
    
	[request setPredicate:predicate];
	
	NSError *error = nil;
  
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES];
    
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
   NSFetchedResultsController *controller=  [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                        managedObjectContext:[self managedObjectContext]
                                          sectionNameKeyPath:@"category"
                                                   cacheName:@"CategotyCache"];
   // fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:sectionNameKeyPath cacheName:@"SongsCache"];
    if (![controller performFetch:&error]) {
        
    }

    
    NSArray *results = [controller fetchedObjects];
    
    if([results count]>0){
        return controller;
    }else{
        return nil;
    }
    
	if(results == nil || [results count] == 0)
		return nil;
    
//	return [results objectAtIndex:0];
    return results;
}
//////////////////////////////////




-(NSArray*)categoryItemsFor:(Category*)category {
    
    
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"menu == %@ AND active==1 AND parentID == %d", menu,parentId];

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@ AND active == 1 ", category];

//	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@ AND active == 1 AND isSideLine==0", category];
    
    
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
		
	return results;
}

-(NSArray*)getSideLineCategory{

	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Category class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
    
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1 AND isSidelineCategory=1"];
    
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    //NSLog(@"SideLine Category%@",results);
    
	return results;


}

-(NSArray*)categoryItemsWithSideLineFor:(Category*)category : (BOOL)isSideLine {
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
    
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@ AND active == 1", category ,isSideLine];
    
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	return results;
}

#pragma mark-
#pragma mark-Sort By Price
-(NSArray*)sortItemsByPrice:(Category*)category {
	NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"price" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@ AND active == 1", category];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	return results;
}
#pragma mark-
#pragma mark-Sort By Price
-(NSArray*)sortItemsByName:(Category*)category {
	NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@ AND active == 1", category];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	return results;
}
-(NSArray*)getCatItems:(int)backendItemId {
    
    NSFetchRequest *request=[[NSFetchRequest alloc] init];
    [request setFetchBatchSize:20];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendItemId];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
		
	return results;
}

-(void)updateCategoryItem:(int)backendItemId {
    NSFetchRequest *request=[[NSFetchRequest alloc] init];
    [request setFetchBatchSize:20];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]];
    [request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"displayOrder" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"backendID == %d", backendItemId];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
    //25th july 2012
    
    
}
-(void)deleteAllCategoriesItems{
	NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([CategoryItem class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}
#pragma mark-
#pragma mark-InvoiceFormat
/*
-(InvoiceFormat*)createInvoiceFormat {
	InvoiceFormat *obj = (InvoiceFormat *)[NSEntityDescription
                                           insertNewObjectForEntityForName:NSStringFromClass([InvoiceFormat class])
                                           inManagedObjectContext:[self managedObjectContext]];
	return obj;
}
-(InvoiceFormat*)getInvoiceFormat{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([InvoiceFormat class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
    NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	
	if(results == nil || [results count] == 0)
		return nil;
	return [results objectAtIndex:0];
}
-(NSArray*)invoiceArray{
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([InvoiceFormat class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"companyAddress" ascending:NO];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];

	
    NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	return results;
}
-(void)deleteAllInvoiceFormat {
	NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([InvoiceFormat class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}
*/
#pragma mark-
#pragma mark-Order
-(Order*)createOrder{
    Order *obj = (Order *)[NSEntityDescription
                         insertNewObjectForEntityForName:NSStringFromClass([Order class])
                         inManagedObjectContext:[self managedObjectContext]];
	return obj;
}


-(Order*)getCategoryItemInOrder:(int)backendID{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d AND quantity >0", backendID];
	[request setPredicate:predicate];
    
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	if(results == nil || [results count] == 0)
		return nil;
    
	return [results objectAtIndex:0];
}


-(Order*)getCategoryItemInOrder : (int)backendID :(int)seatNo{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d AND quantity >0 and seatNo == %d", backendID,seatNo];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	if(results == nil || [results count] == 0)
		return nil;
    
	return [results objectAtIndex:0];
}

-(NSArray*)getCategoryItemsInOrder : (int)backendID :(int)seatNo{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
    NSPredicate *predicate;
    
    if(seatNo>0){
	
        predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d AND quantity >0 and seatNo == %d", backendID,seatNo];
    
    }else{
  
        predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d AND quantity >0", backendID];
        
    }
	
    [request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	if(results == nil || [results count] == 0)
		return nil;
    
	return results;
}



-(NSFetchedResultsController*)getAllOrderItems{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
    
    NSSortDescriptor *sortDescriptorDateTime = [[NSSortDescriptor alloc] initWithKey:@"seatNo" ascending:YES];

    
	//NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"seatNo" ascending:YES];
	
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDateTime,nil];
    
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1 AND quantity > 0"];
	[request setPredicate:predicate];
	
	NSError *error = nil;
    
    NSFetchedResultsController *controller=  [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                                 managedObjectContext:[self managedObjectContext]
                                                                                   sectionNameKeyPath:@"seatNo"
                                                                                            cacheName:nil];
    
    if (![controller performFetch:&error]) {
        
    }
    
    
    NSArray *results = [controller fetchedObjects];
   
	return controller;
    
}


-(NSArray*)getAllOrderItemGroupBy{

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setFetchBatchSize:20];

    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
    
	[fetchRequest setEntity:entity];
    
    NSExpressionDescription* ex = [[NSExpressionDescription alloc] init];
    [ex setName:@"quantity"];
    [ex setExpression:[NSExpression expressionWithFormat:@"@sum.quantity"]];
    [ex setExpressionResultType:NSDecimalAttributeType];
        
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"itemBackendID", ex, nil]];
    [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObject:@"itemBackendID"]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1 AND quantity !=0"];
    [fetchRequest setPredicate:predicate];

    [fetchRequest setResultType:NSDictionaryResultType];
    NSError *error = nil;

	NSArray *results = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
 
    return results;
}

-(NSArray*)getAllOrderItem{
    
    return [self getAllOrderItemGroupBy];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"entryDateTime" ascending:NO];
    
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];

    
	[request setSortDescriptors:sortDescriptors];
    // [request setResultType:NSDictionaryResultType];
    //[request setPropertiesToGroupBy:[NSArray arrayWithObject:@"itemBackendID"]];

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1 AND quantity !=0"];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
	return results;

}


-(Order*)getTotalQuantity{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init]; 
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
	[request setEntity:entity];
	
	NSSortDescriptor *sortDescriptorDisplayOrder = [[NSSortDescriptor alloc] initWithKey:@"entryDateTime" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptorDisplayOrder, nil];
	[request setSortDescriptors:sortDescriptors];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active == 1"];
	[request setPredicate:predicate];
	
	NSError *error = nil; 
	NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	
	return results;
    
}

-(void)clearOrder{
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]]];
	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}

}

#pragma mark Delete With Respect to BackendID
-(void)deleteItemWithNoQuantity:(int)backendID{
  
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setFetchBatchSize:20];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]];
    
	[request setEntity:entity];
	
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d AND quantity >0", backendID];

    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemBackendID = %d", backendID];
	[request setPredicate:predicate];
    
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}


#pragma mark Delete With Respect to BackendID, SeatNO
-(void)deleteItemWithSeatNoID:(int)backendID SeatNo:(int) seatNo{
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
	[request setEntity:[NSEntityDescription entityForName:NSStringFromClass([Order class]) inManagedObjectContext:[self managedObjectContext]]];
    [request setFetchBatchSize:20];

	[request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
  //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ITEMBACKENDID  == %d AND SEATNO == %d", backendID,seatNo];
    NSPredicate *  predicate = [NSPredicate predicateWithFormat:@"itemBackendID == %d and seatNo == %d", backendID,seatNo];
	[request setPredicate:predicate];
    
	NSError * error = nil;
	NSArray * results = [[self managedObjectContext] executeFetchRequest:request error:&error];
	for (NSManagedObject * table in results) {
		[[self managedObjectContext] deleteObject:table];
	}
}

#pragma mark AddOrder Final
-(void) AddOrderFunction:(id) sender Seat:(int)seatNo Qty:(int)orderQty{
    @try {

        Order* orderItem=[[DataStoreManager manager] getCategoryItemInOrder:[sender tag]:seatNo];
    
        if (orderItem!=nil) {
            
            int qty=[orderItem.quantity intValue]+orderQty;
            if(qty>0){
                orderItem.quantity=[NSNumber numberWithInt:qty];
            }
            orderItem.entryDateTime=[NSDate date];
            [[DataStoreManager manager] saveData];
        }else{
            Order *newOrder=[[DataStoreManager manager] createOrder];
            newOrder.entryDateTime=[NSDate date];
            newOrder.itemBackendID=[NSNumber numberWithInt:[sender tag]];
            newOrder.quantity=[NSNumber numberWithInt:orderQty];
            newOrder.comment=@"Click Here For Comments.";
            newOrder.seatNo=[NSNumber numberWithInt:seatNo];
            [[DataStoreManager manager] saveData];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrdersItemAdded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus,nil]];
        //[self setOrderQuantity];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception");
    }
    
    
    
}

@end
