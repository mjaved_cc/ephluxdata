//
//  ImageDownloader.m
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Category+Image.h"

#import "Defines.h"
#define DEFAULT_TIMEOUT 120.0f

@implementation Category  (Image)


-(BOOL)isThumbImageExists {
	if(self.imageURL == nil) return NO;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	return [[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete];
}

-(UIImage*)thumbImage {
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];

	if(image == nil) {
		image = [UIImage imageNamed:@"noImage.jpg"];
	}

	return image;
}

-(UIImage*)hiResImage {
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];
	
	//TODO: - DefaultMenuHiResImage.png add it in resource
	if(image == nil)
		image = [UIImage imageNamed:@"DefaultCategoryHiResImage.jpg"];
	
	return image;
}

-(void)startDownloadHiResImage {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) return;
	
	[self performSelectorInBackground:@selector(startDownloadProcessHiResImage) withObject:nil];
}

-(void)startDownloadThumbImage {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) 
		[[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
	
	[self performSelectorInBackground:@selector(startDownloadProcess) withObject:nil];
}

-(void)startDownloadThumbImage:(int)tag {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete])
		[[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
	
	[self performSelectorInBackground:@selector(startDownloadProcess:) withObject:[NSString stringWithFormat:@"%d",tag]];
}

#pragma mark -
#pragma mark Another Thread For DownloadProcess

-(void)startDownloadProcess:(id)sender {
    
   NSString *str=self.imageURL;
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:str];//self.imageURL];
    
    
    // NSURL *url = [NSURL URLWithString:self.imageURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] >0 && error == nil)
         {
             
             @try {
                 
                 NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
                 NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
                 [data writeToFile:imagePathComplete atomically:YES];
                 self.imageLocalPath = fileName;
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoryImageDownloaded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithBool:YES], keyStatus,
                                self, @"object",sender,@"tag",nil]];
                 
             }
             @catch (NSException *exception) {
                 
                 /* UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Failed To DOWNLOAD CATEGORY" message:@"Please! try after some time" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
                  [alertView show];
                  [alertView release];*/
             }
             
             
             
         }
         else if ([data length] == 0 && error == nil)
         {
             ///NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             //NSLog(@"Error = %@", error);
             // [self startDownloadProcess];
         }
         
         
     }];
    
    
}


-(void)startDownloadProcessHiResImage {
    
	
	NSURL *url = [NSURL URLWithString:[Functions highResImageURL:self.imageURL]];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	

	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	[receivedData writeToFile:imagePathComplete atomically:YES];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoryHiResImageDownloaded object:self 
													  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];
	
    
}

@end
