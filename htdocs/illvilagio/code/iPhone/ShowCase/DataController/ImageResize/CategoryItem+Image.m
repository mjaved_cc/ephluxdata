////
//  CategoryItem+Image.m
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryItem+Image.h"

#import "Defines.h"

@implementation CategoryItem  (Image)

-(BOOL)isThumbImageExists {
	if(self.imageURL == nil) return NO;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryImage] stringByAppendingPathComponent:fileName];
	return [[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete];
}

-(UIImage*)thumbImage {
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];

	if(image == nil)
		image = [UIImage imageNamed:@"noImage.jpg"];
	
	return image;
}

-(UIImage*)hiResImage {
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];
	
	//TODO: - DefaultMenuHiResImage.png add it in resource
	if(image == nil)
		image = [UIImage imageNamed:@"DefaultCategoryItemHiResImage.jpg"];
	
	return image;
}

-(void)startDownloadHiResImage {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) return;
	
	[self performSelectorInBackground:@selector(startDownloadProcessHiResImage) withObject:nil];
}

-(void)startDownloadThumbImage {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) 
		[[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
	
	[self performSelectorInBackground:@selector(startDownloadProcess) withObject:nil];
}

-(void)startDownloadThumbImage:(int)tag {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete])
		[[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
	
	[self performSelectorInBackground:@selector(startDownloadProcess:) withObject:[NSString stringWithFormat:@"%d",tag]];
}

-(void)startDownloadProcess:(id)sender {
    
    NSString *str=self.imageURL;
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:str];//self.imageURL];
    
    
    // NSURL *url = [NSURL URLWithString:self.imageURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] >0 && error == nil)
         {
             
             @try {
                 //                        NSLog(@"File Name %@",str);
                 
                 NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
                 NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
                 [data writeToFile:imagePathComplete atomically:YES];
                 self.imageLocalPath = fileName;
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoryItemImageDownloaded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                                                       [NSNumber numberWithBool:YES], keyStatus,
                                                                                                                                       self, @"object",sender,@"tag",nil]];
                 
             }
             @catch (NSException *exception) {
                 
                 /* UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Failed To DOWNLOAD CATEGORY" message:@"Please! try after some time" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
                  [alertView show];
                  [alertView release];*/
             }
             
             
             
         }
         else if ([data length] == 0 && error == nil)
         {
             ///NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             //NSLog(@"Error = %@", error);
             // [self startDownloadProcess];
         }
         
         
     }];
    
    
}


#pragma mark-
#pragma mark-Resize Image
- (UIImage*) scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 34.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    
    
    return scaledImage;
}
#pragma mark -
#pragma mark Another Thread For DownloadProcess
-(void)startDownloadProcess {
    
    NSString *str=self.imageURL;
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:str];//self.imageURL];

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] >0 && error == nil)
         {
             // DO YOUR WORK HERE
             
             @try {                 

                 
                 /* 
                  [data writeToFile:imagePathComplete atomically:YES];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"DataSaved" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];*/
                 
                 NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
                 NSString* imagePathComplete=[[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
                 self.imageLocalPath = fileName;
                 

                 UIImage* image = [[UIImage alloc] initWithData:data];
                 UIImage* newImage =[self scaleImage:image toSize:CGSizeMake(500, 590)]; 
                 [UIImageJPEGRepresentation(newImage, 0) writeToFile:imagePathComplete atomically:YES];

                     // resize image
                 /* CGSize newSize = CGSizeMake(500, 590);
                  UIGraphicsBeginImageContext( newSize );// a CGSize that has the size you want
                  [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
                  
                  //image is the original UIImage
                  UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                  UIGraphicsEndImageContext();		
                  
                  UIGraphicsBeginImageContext(newSize);
                  [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
                  */
                 
                     //  NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"resizedImage.jpg"];
                     //[UIImageJPGRepresentation(newImage, 1.0) writeToFile:path atomically:YES];
                 
                 
                
                 

             }@catch (NSException *exception) {
                // NSLog(@"Item Error %@",exception.userInfo);
                 
             }
                 [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoryItemImageDownloaded object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];
         }else if ([data length] == 0 && error == nil)
         {
             NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             //    NSLog(@"Error = %@", error);
             // [self startDownloadProcess];
         }            
         
         
     }];
    
    
}

-(void)startDownloadProcessHiResImage {
    
	
	NSURL *url = [NSURL URLWithString:[Functions highResImageURL:self.imageURL]];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	

	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForCategoryItemImage] stringByAppendingPathComponent:fileName];
	[receivedData writeToFile:imagePathComplete atomically:YES];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoryItemHiResImageDownloaded object:self 
													  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];
	
    
}
@end