//
//  CategoryItem+Image.h
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryItem.h"

@interface CategoryItem  (Image)

-(BOOL)isThumbImageExists;

-(UIImage*)thumbImage;
-(UIImage*)hiResImage;

-(void)startDownloadThumbImage;
-(void)startDownloadHiResImage;
-(void)startDownloadThumbImage:(int)tag;
-(void)startDownloadProcess:(id)sender;

@end
