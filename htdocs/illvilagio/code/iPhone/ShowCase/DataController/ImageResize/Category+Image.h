//
//  ImageDownloader.h
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Category.h"

@interface Category  (Image)
-(BOOL)isThumbImageExists;

-(UIImage*)thumbImage;
-(UIImage*)hiResImage;

-(void)startDownloadThumbImage;
-(void)startDownloadHiResImage;
-(void)startDownloadProcess:(id)tag;
-(void)startDownloadThumbImage:(int)tag;
@end
