//
//  Menu+Image.h
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Menu.h"

@interface Menu  (Image)

-(UIImage*)thumbImage;
-(UIImage*)hiResImage;

-(void)startDownloadThumbImage;
-(void)startDownloadHiResImage;

@end