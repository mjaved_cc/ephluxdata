//
//  Menu+Image.m
//  POS
//
//  Created by admin on 11/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Menu+Image.h"

#import "Defines.h"

@implementation Menu  (Image)

-(UIImage*)thumbImage {
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];

	if(image == nil)
		image = [UIImage imageNamed:@"DefaultMenuImage.jpg"];

	return image;
}

-(UIImage*)hiResImage {
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];
	UIImage *image = [UIImage imageWithContentsOfFile:imagePathComplete];
	
	//TODO: - DefaultMenuHiResImage.png add it in resource
	if(image == nil)
		image = [UIImage imageNamed:@"DefaultMenuHiResImage.jpg"];

	return image;
}

-(void)startDownloadHiResImage {
	if(self.imageURL == nil) return;

	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) return;

	[self performSelectorInBackground:@selector(startDownloadProcessHiResImage) withObject:nil];
}

-(void)startDownloadThumbImage {
	if(self.imageURL == nil) return;
	
	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];
	if([[NSFileManager defaultManager] fileExistsAtPath:imagePathComplete]) 
		[[NSFileManager defaultManager] removeItemAtPath:imagePathComplete error:nil];
	
	[self performSelectorInBackground:@selector(startDownloadProcess) withObject:nil];
}

#pragma mark -
#pragma mark Another Thread For DownloadProcess
-(void)startDownloadProcess {
    

	NSString *fileName = [Functions localImageThumbName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];

	NSURL *url = [NSURL URLWithString:self.imageURL];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];	
	NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	

	[receivedData writeToFile:imagePathComplete atomically:YES];

	self.imageLocalPath = fileName;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuImageDownloaded object:self 
													  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];
	
    
}

-(void)startDownloadProcessHiResImage {
    

	NSURL *url = [NSURL URLWithString:[Functions highResImageURL:self.imageURL]];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	
	NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	
	NSString *fileName = [Functions localImageHiResName:self.imageURL backendID:[self.backendID intValue]];
	NSString* imagePathComplete = [[Functions filePathForMenuImage] stringByAppendingPathComponent:fileName];
	[receivedData writeToFile:imagePathComplete atomically:YES];

	[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMenuHiResImageDownloaded object:self 
													  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], keyStatus, self, @"object", nil]];
	
            //25th JULY 2012

    
}


@end
