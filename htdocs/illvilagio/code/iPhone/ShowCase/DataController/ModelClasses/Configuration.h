//
//  Configuration.h
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Configuration : NSManagedObject

@property (nonatomic, retain) NSNumber * floor;
@property (nonatomic, retain) NSString * host;
@property (nonatomic, retain) NSString * passCode;

@end
