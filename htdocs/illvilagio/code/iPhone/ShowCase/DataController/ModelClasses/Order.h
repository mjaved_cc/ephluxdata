//
//  Order.h
//  ShowCase
//
//  Created by USER on 5/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSManagedObject

@property (nonatomic, retain) NSNumber * itemBackendID;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, retain) NSDate   * entryDateTime;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSNumber * seatNo;

@end
