//
//  Category.h
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>



@interface Gallery : NSManagedObject

@property (nonatomic, retain) NSNumber * galleryID;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * Title_arabic;
@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSString * caption_arabic;




@end
