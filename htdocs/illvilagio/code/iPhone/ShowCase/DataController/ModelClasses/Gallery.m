//
//  Category.m
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Gallery.h"


@implementation Gallery

@dynamic galleryID;
@dynamic imageURL;
@dynamic title;
@dynamic Title_arabic;
@dynamic caption;
@dynamic caption_arabic;


@end
