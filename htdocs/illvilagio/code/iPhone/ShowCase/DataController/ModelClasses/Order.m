//
//  Order.m
//  ShowCase
//
//  Created by USER on 5/7/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Order.h"


@implementation Order

@dynamic itemBackendID;
@dynamic quantity;
@dynamic active;
@dynamic entryDateTime;
@dynamic comment;
@dynamic seatNo;
@end
