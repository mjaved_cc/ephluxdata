//
//  Category.h
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Menu;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSNumber * isSidelineCategory;
@property (nonatomic, retain) NSNumber * allowedSideLine;
@property (nonatomic, retain) NSNumber * backendID;
@property (nonatomic, retain) NSNumber * parentID;
@property (nonatomic, retain) NSNumber * displayOrder;
@property (nonatomic, retain) NSString * imageLocalPath;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSDate * lastUpdatedDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_arabic;
@property (nonatomic, retain) Menu *menu;
@property (nonatomic, retain) NSSet *categoryItems;

@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addCategoryItemsObject:(NSManagedObject *)value;
- (void)removeCategoryItemsObject:(NSManagedObject *)value;
- (void)addCategoryItems:(NSSet *)values;
- (void)removeCategoryItems:(NSSet *)values;
@end
