//
//  CategoryItem.h
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

@interface CategoryItem : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSNumber * backendID;
@property (nonatomic, retain) NSNumber * backendItemID;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * desc_arabic;
@property (nonatomic, retain) NSNumber * displayOrder;
@property (nonatomic, retain) NSNumber * forcedItem;
@property (nonatomic, retain) NSString * imageLocalPath;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSDate * lastUpdatedDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_arabic;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSNumber * isSideLine;
@property (nonatomic, retain) NSString * unitType;
@property (nonatomic, retain) Category *category;


@end
