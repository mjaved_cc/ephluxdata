//
//  CategoryItem.m
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CategoryItem.h"
#import "Category.h"


@implementation CategoryItem

@dynamic active;
@dynamic backendID;
@dynamic backendItemID;
@dynamic desc;
@dynamic desc_arabic;
@dynamic displayOrder;
@dynamic forcedItem;
@dynamic imageLocalPath;
@dynamic imageURL;
@dynamic lastUpdatedDate;
@dynamic name;
@dynamic name_arabic;
@dynamic price;
@dynamic unitType;
@dynamic category;
@dynamic isSideLine;

@end
