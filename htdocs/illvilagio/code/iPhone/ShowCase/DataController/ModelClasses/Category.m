//
//  Category.m
//  ShowCase
//
//  Created by USER on 3/18/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Category.h"
#import "Menu.h"


@implementation Category

@dynamic active;
@dynamic backendID;
@dynamic displayOrder;
@dynamic imageLocalPath;
@dynamic imageURL;
@dynamic lastUpdatedDate;
@dynamic name;
@dynamic name_arabic;
@dynamic menu;
@dynamic categoryItems;
@dynamic parentID;
@dynamic allowedSideLine;
@dynamic isSidelineCategory;
@end
