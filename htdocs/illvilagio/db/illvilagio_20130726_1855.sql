CREATE DATABASE  IF NOT EXISTS `illvillagio` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `illvillagio`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 192.175.0.1    Database: illvillagio
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,164),(2,1,NULL,NULL,'AuthActionMaps',2,19),(3,2,NULL,NULL,'index',3,4),(4,2,NULL,NULL,'view',5,6),(5,2,NULL,NULL,'add',7,8),(6,2,NULL,NULL,'edit',9,10),(7,2,NULL,NULL,'delete',11,12),(8,2,NULL,NULL,'assign_permission',13,14),(16,2,NULL,NULL,'sync_action_maps',15,16),(17,1,NULL,NULL,'Demos',20,49),(21,17,NULL,NULL,'facebook_connect',21,22),(25,17,NULL,NULL,'twitter_connect',23,24),(27,17,NULL,NULL,'signup',25,26),(28,17,NULL,NULL,'verify_account',27,28),(30,17,NULL,NULL,'forgot_password',29,30),(31,17,NULL,NULL,'reset_account_password',31,32),(40,1,NULL,NULL,'Groups',50,63),(41,40,NULL,NULL,'index',51,52),(42,40,NULL,NULL,'view',53,54),(43,40,NULL,NULL,'add',55,56),(44,40,NULL,NULL,'edit',57,58),(45,40,NULL,NULL,'delete',59,60),(54,1,NULL,NULL,'Users',64,77),(55,54,NULL,NULL,'index',65,66),(56,54,NULL,NULL,'view',67,68),(57,54,NULL,NULL,'add',69,70),(58,54,NULL,NULL,'edit',71,72),(59,54,NULL,NULL,'delete',73,74),(82,1,NULL,NULL,'AclExtras',78,79),(83,1,NULL,NULL,'Minify',80,85),(84,83,NULL,NULL,'Minify',81,84),(85,84,NULL,NULL,'index',82,83),(86,17,NULL,NULL,'index',33,34),(87,17,NULL,NULL,'secure',35,36),(88,2,NULL,NULL,'isAuthorized',17,18),(89,17,NULL,NULL,'isAuthorized',37,38),(90,40,NULL,NULL,'isAuthorized',61,62),(91,54,NULL,NULL,'isAuthorized',75,76),(92,1,NULL,NULL,'Conversations',86,143),(93,92,NULL,NULL,'ConversationMessages',87,92),(94,93,NULL,NULL,'add',88,89),(102,93,NULL,NULL,'isAuthorized',90,91),(103,92,NULL,NULL,'ConversationUsers',93,116),(104,103,NULL,NULL,'index',94,95),(105,103,NULL,NULL,'view',96,97),(106,103,NULL,NULL,'add',98,99),(107,103,NULL,NULL,'edit',100,101),(108,103,NULL,NULL,'delete',102,103),(109,103,NULL,NULL,'admin_index',104,105),(110,103,NULL,NULL,'admin_view',106,107),(111,103,NULL,NULL,'admin_add',108,109),(112,103,NULL,NULL,'admin_edit',110,111),(113,103,NULL,NULL,'admin_delete',112,113),(121,103,NULL,NULL,'isAuthorized',114,115),(122,92,NULL,NULL,'Conversations',117,142),(123,122,NULL,NULL,'index',118,119),(124,122,NULL,NULL,'sent',120,121),(125,122,NULL,NULL,'view',122,123),(126,122,NULL,NULL,'add',124,125),(127,122,NULL,NULL,'edit',126,127),(128,122,NULL,NULL,'delete',128,129),(129,122,NULL,NULL,'admin_index',130,131),(130,122,NULL,NULL,'admin_view',132,133),(131,122,NULL,NULL,'admin_add',134,135),(132,122,NULL,NULL,'admin_edit',136,137),(133,122,NULL,NULL,'admin_delete',138,139),(141,122,NULL,NULL,'isAuthorized',140,141),(157,17,NULL,NULL,'login',39,40),(161,17,NULL,NULL,'logout',41,42),(165,17,NULL,NULL,'login_with_facebook',43,44),(166,17,NULL,NULL,'login_with_twitter',45,46),(173,17,NULL,NULL,'test',47,48),(174,1,NULL,NULL,'Features',144,157),(175,174,NULL,NULL,'index',145,146),(176,174,NULL,NULL,'view',147,148),(177,174,NULL,NULL,'add',149,150),(178,174,NULL,NULL,'edit',151,152),(179,174,NULL,NULL,'delete',153,154),(180,174,NULL,NULL,'isAuthorized',155,156),(181,1,NULL,NULL,'conversations',158,163),(182,181,NULL,NULL,'ConversationsApp',159,162),(183,182,NULL,NULL,'isAuthorized',160,161);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,'group-1',1,2),(2,NULL,'Group',2,'group-2',3,4),(3,NULL,'Group',4,'group-4',5,6);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL auto_increment,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL default '0',
  `_read` varchar(2) NOT NULL default '0',
  `_update` varchar(2) NOT NULL default '0',
  `_delete` varchar(2) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (110,2,21,'1','1','1','1'),(111,2,25,'1','1','1','1'),(112,2,27,'1','1','1','1'),(113,2,28,'1','1','1','1'),(114,2,30,'1','1','1','1'),(115,2,31,'1','1','1','1'),(116,2,55,'1','1','1','1'),(117,2,56,'1','1','1','1'),(118,2,57,'1','1','1','1'),(119,2,58,'1','1','1','1'),(120,2,59,'1','1','1','1'),(121,2,157,'1','1','1','1'),(122,2,161,'1','1','1','1'),(123,2,165,'1','1','1','1'),(124,2,166,'1','1','1','1'),(135,3,21,'1','1','1','1'),(136,3,25,'1','1','1','1'),(137,3,27,'1','1','1','1'),(138,3,28,'1','1','1','1'),(139,3,30,'1','1','1','1'),(140,3,31,'1','1','1','1'),(141,3,157,'1','1','1','1'),(142,3,161,'1','1','1','1'),(143,3,165,'1','1','1','1'),(144,3,166,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_action_maps`
--

DROP TABLE IF EXISTS `auth_action_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_action_maps` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL default '5',
  `crud` enum('create','read','update','delete') NOT NULL default 'read',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_action_maps`
--

LOCK TABLES `auth_action_maps` WRITE;
/*!40000 ALTER TABLE `auth_action_maps` DISABLE KEYS */;
INSERT INTO `auth_action_maps` VALUES (2,'Display mapping information',4,2,'read','2013-04-10 20:21:58','2013-04-19 21:18:37'),(3,'Adding mapping information',5,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:06'),(4,'Editing mapping information',6,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:23'),(5,'Deleting mapping information',7,2,'read','2013-04-10 20:21:58','2013-04-22 22:05:17'),(6,'Assign permission',8,2,'read','2013-04-10 20:21:58','2013-04-19 21:20:56'),(14,'Add/Synchronise all acos with auth action mapper',16,2,'read','2013-04-10 20:21:58','2013-04-19 21:21:52'),(17,'callback action for facebook',21,1,'read','2013-04-10 20:21:58','2013-04-19 21:14:23'),(21,'callback action for twitter',25,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:11'),(23,'Register user',27,1,'create','2013-04-10 20:21:58','2013-04-22 22:05:47'),(24,'Email verification',28,1,'read','2013-04-10 20:21:58','2013-04-22 22:06:07'),(26,'Forgot password',30,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:18'),(27,'Reset password',31,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:23'),(35,'List all groups',41,3,'read','2013-04-10 20:21:58','2013-04-19 21:22:45'),(36,'Display group',42,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:01'),(37,'Add group',43,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:15'),(38,'Edit group',44,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:36'),(39,'Delete group',45,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:52'),(47,'List all users',55,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:37'),(48,'Display user',56,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:48'),(49,'Add user',57,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:57'),(50,'Edit user',58,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:26'),(51,'Delete user',59,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:36'),(73,'Minify plugin',84,4,'read','2013-04-10 20:21:58','2013-04-19 21:27:01'),(74,'Minify plugin',85,4,'read','2013-04-10 20:21:58','2013-04-19 21:29:09'),(75,'Dummy index action',86,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:04'),(76,'Action after successful login',87,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:42'),(77,'',88,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(78,'',89,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(79,'',90,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(80,'',91,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(81,'ConversationMessages',93,4,'read','2013-04-10 20:21:58','2013-04-22 22:10:27'),(82,'ConversationMessages -> add',94,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:12'),(90,'',102,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(91,'Conversations -> ConversationUsers',103,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:42'),(92,'ConversationUsers -> index',104,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:55'),(93,'ConversationUsers -> view',105,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:12'),(94,'ConversationUsers -> add',106,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:31'),(95,'ConversationUsers -> edit',107,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:42'),(96,'ConversationUsers -> delete',108,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:53'),(97,'ConversationUsers -> admin_index',109,4,'read','2013-04-10 20:21:58','2013-04-22 22:13:10'),(98,'ConversationUsers -> admin_view',110,4,'read','2013-04-10 20:21:58','2013-04-22 22:17:48'),(99,'ConversationUsers -> admin_add',111,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:00'),(100,'ConversationUsers -> admin_edit',112,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:10'),(101,'ConversationUsers -> admin_delete',113,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:20'),(109,'',121,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(110,'Conversations -> Conversations',122,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:37'),(111,'Conversations -> index',123,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:00'),(112,'Conversations -> sent',124,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:13'),(113,'Conversations -> view',125,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:27'),(114,'Conversations -> add',126,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:37'),(115,'Conversations -> edit',127,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:49'),(116,'ConversationUsers -> delete',128,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:01'),(117,'Conversations -> admin_index',129,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:14'),(118,'Conversations -> admin_view',130,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:33'),(119,'Conversations -> admin_add',131,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:51'),(120,'Conversations -> admin_edit',132,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:06'),(121,'Conversations -> admin_delete',133,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:26'),(129,'',141,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(141,'Login',157,1,'read','2013-04-12 21:53:51','2013-04-19 21:30:07'),(144,'Logout',161,1,'read','2013-04-15 20:01:29','2013-04-19 21:30:12'),(147,'Login action for facebook',165,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:19'),(148,'Login action for twitter',166,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:25'),(153,'List all auth action maps',3,2,'read','2013-04-19 20:03:52','2013-04-22 16:11:29'),(154,'',173,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05'),(155,'Display features',175,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:00'),(156,'View Feature',176,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:14'),(157,'Add new Feature',177,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:23'),(158,'Edit Feature',178,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:33'),(159,'Delete Feature',179,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:43'),(160,'',180,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05'),(161,'',182,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05'),(162,'',183,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05');
/*!40000 ALTER TABLE `auth_action_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cash_infos`
--

DROP TABLE IF EXISTS `cash_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cash_infos` (
  `id` bigint(20) NOT NULL,
  `cash_amount` int(10) default NULL,
  `entry_date` datetime default NULL,
  `order_id` bigint(20) default NULL,
  `payment_mode` int(3) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cash_infos`
--

LOCK TABLES `cash_infos` WRITE;
/*!40000 ALTER TABLE `cash_infos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cash_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `menu_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) default NULL,
  `name` varchar(250) default NULL,
  `image_name` text,
  `status` tinyint(1) NOT NULL default '1',
  `display_order` int(3) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `name_arabic` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,1,0,'egg',NULL,1,1,NULL,NULL,NULL),(2,1,1,'boiled egg',NULL,1,2,NULL,NULL,NULL),(3,1,0,'sandwich',NULL,1,NULL,NULL,NULL,NULL),(4,2,0,'roti',NULL,1,NULL,NULL,NULL,NULL),(5,2,0,'daal',NULL,1,NULL,NULL,NULL,NULL),(6,3,0,'bbq',NULL,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_items`
--

DROP TABLE IF EXISTS `category_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_items` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `category_id` bigint(20) unsigned NOT NULL,
  `name` varchar(250) default NULL,
  `description` varchar(500) default NULL,
  `price` int(10) default NULL,
  `image_name` text,
  `display_order` int(3) default NULL,
  `status` tinyint(1) NOT NULL default '1',
  `force_modifier` int(1) default NULL,
  `is_tab` tinyint(1) default '0',
  `parent_item_id` bigint(20) default NULL,
  `child_items` bigint(20) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `is_force_modifier` tinyint(1) default NULL,
  `is_side_line` tinyint(1) default NULL,
  `name_arabic` varchar(20) default NULL,
  `description_arabic` varchar(500) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_category_items` (`category_id`),
  CONSTRAINT `FK_category_items` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_items`
--

LOCK TABLES `category_items` WRITE;
/*!40000 ALTER TABLE `category_items` DISABLE KEYS */;
INSERT INTO `category_items` VALUES (1,1,'half boil egg',NULL,NULL,NULL,NULL,1,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'full boil egg',NULL,NULL,NULL,NULL,1,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,2,'fried egg',NULL,NULL,NULL,NULL,1,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'stakes',NULL,NULL,NULL,NULL,1,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,'grill fish',NULL,NULL,NULL,NULL,1,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `category_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(45) default NULL,
  `description` varchar(150) default NULL,
  `status` varchar(45) default NULL,
  `key` varchar(45) NOT NULL,
  `title_arabic` varchar(45) character set utf8 default NULL,
  `description_arabic` varchar(45) character set utf8 default NULL,
  `modified` datetime default NULL,
  `created` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,'About us','About us description ','1','about_us','من نحن','من نحن',NULL,NULL),(2,'Description','This is description text','1','des','من نحن','حول لنا الوصف','2013-07-04 00:00:00','2013-07-04 00:00:00');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_groups`
--

DROP TABLE IF EXISTS `conversation_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_groups` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_groups`
--

LOCK TABLES `conversation_groups` WRITE;
/*!40000 ALTER TABLE `conversation_groups` DISABLE KEYS */;
INSERT INTO `conversation_groups` VALUES (1,'doctors'),(2,'reception');
/*!40000 ALTER TABLE `conversation_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_messages`
--

DROP TABLE IF EXISTS `conversation_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_messages` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `conversation_id` int(11) NOT NULL,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `attachment_id` int(10) unsigned NOT NULL default '0',
  `message` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_messages`
--

LOCK TABLES `conversation_messages` WRITE;
/*!40000 ALTER TABLE `conversation_messages` DISABLE KEYS */;
INSERT INTO `conversation_messages` VALUES (1,1,'20',0,'<p>losem iprem ASASSAs</p>','2013-04-10 18:50:03'),(2,2,'20',0,'<p>javed</p>','2013-04-10 18:52:55'),(6,2,'21',0,'<p>reply from javed</p>','2013-04-11 12:31:54'),(7,2,'21',0,'<p>reply from javed</p>','2013-04-11 12:35:34'),(8,2,'21',0,'<p>afda</p>','2013-04-11 12:39:41'),(9,3,'20',0,'<p>aaaaa</p>','2013-04-11 14:46:06'),(10,4,'21',0,'THis is sampe content','2013-04-11 19:59:06'),(11,4,'20',0,'<p>afdaf</p>','2013-04-11 15:04:59'),(12,4,'20',0,'<p>testssss</p>','2013-04-11 15:05:28'),(13,4,'20',0,'Well done','2013-04-11 20:08:23'),(14,4,'21',0,'werewrwrw','2013-04-11 20:08:48'),(15,5,'21',0,'1st msg','2013-04-11 20:14:27'),(16,5,'20',0,'2nd msg','2013-04-11 20:14:40');
/*!40000 ALTER TABLE `conversation_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_users`
--

DROP TABLE IF EXISTS `conversation_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `conversation_id` int(11) NOT NULL,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `status` tinyint(2) unsigned NOT NULL default '0' COMMENT '0=ok,\n1=deleted,2=removed',
  `last_view` int(10) unsigned NOT NULL default '0',
  `created` datetime NOT NULL,
  `folder` enum('inbox','sent') default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_users`
--

LOCK TABLES `conversation_users` WRITE;
/*!40000 ALTER TABLE `conversation_users` DISABLE KEYS */;
INSERT INTO `conversation_users` VALUES (1,1,'14',0,0,'2013-04-10 18:50:03','inbox'),(2,1,'20',0,0,'2013-04-10 18:50:03','sent'),(3,2,'14',0,0,'2013-04-10 18:52:55','inbox'),(4,2,'20',0,0,'2013-04-10 18:52:55','sent'),(5,3,'22',0,0,'2013-04-11 14:46:06','inbox'),(6,3,'20',0,0,'2013-04-11 14:46:06','sent'),(7,4,'20',0,0,'2013-04-11 19:59:06','inbox'),(8,4,'21',0,0,'2013-04-11 19:59:06','sent'),(9,5,'20',0,0,'2013-04-11 20:14:27','inbox'),(10,5,'21',0,0,'2013-04-11 20:14:27','sent');
/*!40000 ALTER TABLE `conversation_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `title` varchar(60) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `created` datetime NOT NULL,
  `last_message_id` int(10) unsigned NOT NULL default '0',
  `allow_add` tinyint(1) unsigned NOT NULL default '0',
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversations`
--

LOCK TABLES `conversations` WRITE;
/*!40000 ALTER TABLE `conversations` DISABLE KEYS */;
INSERT INTO `conversations` VALUES (1,'20','This is test','2013-04-10 18:50:03',1,0,0),(2,'20','javed','2013-04-10 18:52:55',8,0,0),(3,'20','ree','2013-04-11 14:46:06',9,0,0),(4,'21','User to Admin','2013-04-11 19:59:06',14,0,0),(5,'21','User to Admin 1','2013-04-11 20:14:27',16,0,0);
/*!40000 ALTER TABLE `conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_information`
--

DROP TABLE IF EXISTS `customer_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_information` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(250) default NULL,
  `telephone_no` int(20) default NULL,
  `email` varchar(250) default NULL,
  `address` varchar(250) default NULL,
  `status` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_information`
--

LOCK TABLES `customer_information` WRITE;
/*!40000 ALTER TABLE `customer_information` DISABLE KEYS */;
INSERT INTO `customer_information` VALUES (1,'Customer Name',123,NULL,'Customer Address',1),(2,'customer Name',123456789,NULL,'',1),(3,'name',123,NULL,'address',1),(4,'name1',123,NULL,'address',1),(5,'syed sami',123,NULL,'address',1),(6,'Syed Sami',333373075,NULL,'Suit 414',1),(7,'Syed Sami',333373075,'syed.sami@ephlux.com',NULL,1),(8,'Syed Farhan',333373075,'farhan.chand@ephlux.',NULL,1),(9,'Syed Sami',333373075,'','Suit 414',1),(10,'value1111',123456789,'demo@email.com','bla bla bla ',1),(11,'rizwan',123456789,'demo@email.com','bla bla bla ',1),(12,'rizwan',123456789,'abc@demo.com','abc address ',1),(13,'Salman',123456789,'salman@demo.com','bla bla bla adressss',1),(14,'nmae11111',123456789,'abc@demo.com','abc address 12345678',1),(15,'salman',333373075,'asdasd@sadasd.com','Suit 414',1),(16,'rizwan',12321312,'demo@demo.com','',1),(18,'asdfs',333373075,'as@as.com','Suit 414',1),(19,'salman',333373075,'salman@salman.com','Suit 414',1),(20,'hf',333373075,'gf@gf.com','Suit 414',1),(21,'salman',23432434,'salman.ahmed@ephlux.',NULL,1);
/*!40000 ALTER TABLE `customer_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_information`
--

DROP TABLE IF EXISTS `delivery_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_information` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_id` bigint(20) NOT NULL,
  `delivery_address` varchar(250) default NULL,
  `agent_id` int(3) default NULL,
  `agent_name` varchar(200) default NULL,
  `delivery_time` datetime default NULL,
  `email_address` varchar(2501) default NULL,
  `phone_no` int(20) default NULL,
  `delivery_dispatch_time` datetime default NULL,
  `delivery_chargers` int(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_delivery_information` (`order_id`),
  CONSTRAINT `FK_delivery_information` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_information`
--

LOCK TABLES `delivery_information` WRITE;
/*!40000 ALTER TABLE `delivery_information` DISABLE KEYS */;
INSERT INTO `delivery_information` VALUES (1,1,'Delivery Address',1,'Agent Name',NULL,'test@test.com',123456,NULL,50);
/*!40000 ALTER TABLE `delivery_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_images`
--

DROP TABLE IF EXISTS `event_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_images` (
  `id` bigint(20) NOT NULL auto_increment,
  `image_url` varchar(45) NOT NULL,
  `event_id` bigint(20) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `status` int(3) NOT NULL default '1',
  `title_image` varchar(45) default NULL,
  `title_image_arabic` varchar(45) character set utf8 default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_event_images` (`event_id`),
  CONSTRAINT `FK_event_images` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_images`
--

LOCK TABLES `event_images` WRITE;
/*!40000 ALTER TABLE `event_images` DISABLE KEYS */;
INSERT INTO `event_images` VALUES (1,'a.jpg',1,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'image 1','إ ح س ا ن'),(2,'b.jpg',1,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'iamge 2','إ ح س ا ن'),(3,'d.jpg',2,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'test text2','إحسان'),(4,'d.jpg',2,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'test text2','إحسان'),(5,'d.jpg',3,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'image 5','إحسان'),(6,'b.jpg',3,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'image 6','إحسان'),(9,'d.jpg',2,'2013-06-13 16:09:48','2013-06-13 16:09:48',1,'test text2','إ ح س ا ن'),(16,'flower-game-screenshot-2.jpg',18,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(17,'flower-game-screenshot-2.jpg',18,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(18,'Screenshot from 2013-07-01 14:37:55.png',19,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(19,'Screenshot from 2013-07-01 14:37:55.png',19,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(20,'flower-game-screenshot-2.jpg',20,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(21,'flower-game-screenshot-2.jpg',20,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(22,'Screenshot from 2013-07-01 14:37:55.png',21,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(23,'flower-game-screenshot-2.jpg',21,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(24,'Screenshot from 2013-07-01 14:37:55.png',22,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(25,'Screenshot from 2013-07-01 14:37:55.png',22,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(26,'Screenshot from 2013-07-01 14:37:55.png',23,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(27,'Screenshot from 2013-07-01 14:37:55.png',23,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(28,'Screenshot from 2013-07-01 14:37:55.png',24,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(29,'flower-game-screenshot-2.jpg',24,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(30,'flower-game-screenshot-2.jpg',25,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(31,'flower-game-screenshot-2.jpg',25,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(32,'flower-game-screenshot-2.jpg',26,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(33,'flower-game-screenshot-2.jpg',26,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(34,'Screenshot from 2013-07-01 14:37:55.png',27,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(35,'flower-game-screenshot-2.jpg',27,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(36,'Screenshot from 2013-07-01 14:37:55.png',28,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'iamge 2','إ ح س ا ن'),(37,'flower-game-screenshot-2.jpg',28,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(38,'Screenshot from 2013-07-01 14:37:55.png',29,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(39,'flower-game-screenshot-2.jpg',29,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(40,'Screenshot from 2013-07-01 14:37:55.png',30,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(41,'flower-game-screenshot-2.jpg',30,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(42,'Screenshot from 2013-07-01 14:37:55.png',31,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(43,'flower-game-screenshot-2.jpg',31,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(44,'Screenshot from 2013-07-01 14:37:55.png',32,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(45,'flower-game-screenshot-2.jpg',32,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(46,'Screenshot from 2013-07-01 14:37:55.png',33,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(47,'flower-game-screenshot-2.jpg',33,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(48,'Screenshot from 2013-07-01 14:37:55.png',34,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(49,'flower-game-screenshot-2.jpg',34,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(50,'Screenshot from 2013-07-01 14:37:55.png',35,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(51,'flower-game-screenshot-2.jpg',35,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(52,'Screenshot from 2013-07-01 14:37:55.png',36,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(53,'flower-game-screenshot-2.jpg',36,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(54,'Screenshot from 2013-07-01 14:37:55.png',37,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(55,'flower-game-screenshot-2.jpg',37,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(56,'Screenshot from 2013-07-01 14:37:55.png',38,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(57,'flower-game-screenshot-2.jpg',38,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(58,'Screenshot from 2013-07-01 14:37:55.png',39,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(59,'flower-game-screenshot-2.jpg',39,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(60,'Screenshot from 2013-07-01 14:37:55.png',40,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(61,'flower-game-screenshot-2.jpg',40,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(62,'Screenshot from 2013-07-01 14:37:55.png',41,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(63,'flower-game-screenshot-2.jpg',41,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(64,'Screenshot from 2013-07-01 14:37:55.png',42,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(65,'flower-game-screenshot-2.jpg',42,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(66,'Screenshot from 2013-07-01 14:37:55.png',45,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(67,'flower-game-screenshot-2.jpg',45,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(68,'Screenshot from 2013-07-01 14:37:55.png',46,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(69,'flower-game-screenshot-2.jpg',46,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(70,'Screenshot from 2013-07-01 14:37:55.png',47,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(71,'flower-game-screenshot-2.jpg',47,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(72,'Screenshot from 2013-07-01 14:37:55.png',48,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(73,'flower-game-screenshot-2.jpg',48,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(74,'Screenshot from 2013-07-01 14:37:55.png',49,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(75,'flower-game-screenshot-2.jpg',49,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(76,'Screenshot from 2013-07-01 14:37:55.png',50,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(77,'flower-game-screenshot-2.jpg',50,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(78,'Screenshot from 2013-07-01 14:37:55.png',51,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(79,'flower-game-screenshot-2.jpg',51,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(80,'flower-game-screenshot-2.jpg',52,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(81,'Screenshot from 2013-07-01 14:37:55.png',52,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(82,'flower-game-screenshot-2.jpg',53,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(83,'Screenshot from 2013-07-01 14:37:55.png',53,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(84,'Screenshot from 2013-07-01 14:37:55.png',19,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(85,'Screenshot from 2013-07-01 14:37:55.png',19,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(86,'flower-game-screenshot-2.jpg',54,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(87,'Screenshot from 2013-07-01 14:37:55.png',54,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(88,'flower-game-screenshot-2.jpg',55,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(89,'Screenshot from 2013-07-01 14:37:55.png',55,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(93,'flower-game-screenshot-2.jpg',59,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(94,'Screenshot from 2013-07-01 14:37:55.png',59,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(95,'flower-game-screenshot-2.jpg',67,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(96,'Screenshot from 2013-07-01 14:37:55.png',67,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(97,'flower-game-screenshot-2.jpg',67,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(98,'Screenshot from 2013-07-01 14:37:55.png',67,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(99,'flower-game-screenshot-2.jpg',68,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text1','إ ح س ا ن'),(100,'Screenshot from 2013-07-01 14:37:55.png',68,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text1','إ ح س ا ن'),(101,'flower-game-screenshot-2.jpg',68,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(102,'Screenshot from 2013-07-01 14:37:55.png',68,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(103,'flower-game-screenshot-2.jpg',74,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text1','إ ح س ا ن'),(104,'Screenshot from 2013-07-01 14:37:55.png',74,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن'),(105,'flower-game-screenshot-2.jpg',75,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text1','إ ح س ا ن'),(106,'Screenshot from 2013-07-01 14:37:55.png',75,'2013-07-09 00:00:00','2013-07-09 00:00:00',1,'test text2','إ ح س ا ن');
/*!40000 ALTER TABLE `event_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(250) character set latin1 default NULL,
  `description` varchar(500) character set latin1 default NULL,
  `participate_description` varchar(500) default NULL,
  `event_time` datetime default NULL,
  `status` tinyint(1) NOT NULL default '1',
  `modified` datetime default NULL,
  `created` datetime default NULL,
  `title_arabic` varchar(250) default NULL,
  `description_arabic` varchar(500) default NULL,
  `particapte_description_arabic` varchar(45) default NULL,
  `is_promoted_home` varchar(45) NOT NULL default '0',
  `is_deal` varchar(45) NOT NULL default '0',
  `event_name` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'charity','charity','charity for orphans','2013-06-13 16:09:48',1,'2013-06-13 16:09:48',NULL,'إحسان','إحسان','مشاركات موقع خيرية','1','0','ILVILAGIO'),(2,'charity','Losem iprem','charity for orphans','2013-06-13 16:09:48',1,'2013-09-13 16:09:48',NULL,'إحسان','إحس انإ حسان','مرحبا بكم في موقع جمعية خيرية','1','0','ILVILAGIO 1'),(3,'charity','lorem ipsum','charity for orphans','2013-07-02 17:09:48',1,'2013-06-13 16:09:48','0000-00-00 00:00:00','إحسان ','إ ح س ا ن','صدقة آخر اختبار موقع','0','0','ILVILAGIO 2'),(4,'charity','lorem ipsum','charity for orphans','2013-08-12 16:09:48',1,'2013-06-13 16:09:48','0000-00-00 00:00:00','إحسان ','إ ح س ا ن','صدقة آخر اختبار موقع','0','0',NULL),(5,'test event title','test description','test participate description','2013-06-13 16:09:48',1,'2013-09-13 16:09:47','2013-09-13 16:09:47','إحسان','إحسان','صالة عرضصالة عرض صالة عرض','0','0',NULL),(6,'event title ','this is test description','This is test participate description','2013-09-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 21:05:01','إحسان','إحس ','مشاركات موقع خيرية','0','0',NULL),(7,'event title ','this is test description','This is test participate description','2013-09-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 21:05:43','إحسان','إحس ','مشاركات موقع خيرية','0','0',NULL),(8,'event title ','this is test description','This is test participate description','2013-09-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 21:06:09','إحسان','إحس ','مشاركات موقع خيرية','0','0',NULL),(9,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 21:18:31','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(10,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 22:19:58','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(11,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 22:20:43','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(12,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-08 22:21:17','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(13,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:37:43','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(14,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:38:39','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(15,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:40:33','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(16,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:41:58','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(17,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:44:05','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(18,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 12:45:14','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(19,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 22:00:16','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(20,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:42:10','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(21,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:43:37','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(22,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 19:01:29','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(23,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 19:04:09','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(24,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:47:32','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(25,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:51:11','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(26,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:52:28','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(27,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:56:52','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(28,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:58:41','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(29,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 13:59:59','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(30,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:00:48','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(31,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:02:38','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(32,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'2013-06-13 16:09:48','2013-06-13 16:09:48','إحسان','مرحبا بكم في موقع جمعية خيرية','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(33,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:04:11','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(34,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:06:34','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(35,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:07:44','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(36,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:08:36','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(37,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:10:31','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(38,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:12:10','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(39,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:25:05','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(40,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:28:13','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(41,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:29:18','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(42,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:29:53','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(43,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:37:27','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(44,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:38:01','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(45,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:40:08','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(46,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:40:36','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(47,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:41:59','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(48,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:44:40','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(49,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 14:46:07','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(50,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 15:23:21','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(51,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 15:23:53','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(52,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 15:24:19','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(53,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 15:27:21','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(54,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 17:30:32','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(55,'test title of event','test event description','test participate description','2013-06-13 16:09:48',0,'0000-00-00 00:00:00','2013-07-09 17:35:00','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(56,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 17:56:52','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(57,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 17:59:48','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(58,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:01:44','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(59,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:05:08','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(60,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:07:22','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(61,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:09:16','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(62,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:13:25','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(63,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:16:44','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(64,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:18:47','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(65,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:20:01','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(66,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:20:43','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(67,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:29:39','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','1',NULL),(68,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:32:09','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','1',NULL),(69,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:34:10','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(70,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:34:47','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(71,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:35:06','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(72,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:36:52','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(73,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:37:21','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(74,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:42:05','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL),(75,'test title of event','test event description','test participate description','2013-06-13 16:09:48',1,'0000-00-00 00:00:00','2013-07-09 18:42:19','إحسان','إ ح س ا ن','مرحبا بكم في موقع جمعية خيرية','0','0',NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `features`
--

LOCK TABLES `features` WRITE;
/*!40000 ALTER TABLE `features` DISABLE KEYS */;
INSERT INTO `features` VALUES (1,'User Management',2130706433,1,'2013-04-19 20:33:14','2013-04-24 14:44:06'),(2,'Acl Management',2130706433,1,'2013-04-19 21:18:13','2013-04-24 14:44:20'),(3,'Group Management',2130706433,1,'2013-04-19 21:22:18','2013-04-24 14:44:23'),(4,'Plugins',2130706433,1,'2013-04-19 21:26:44','2013-04-24 14:44:26'),(5,'Others',2130706433,1,'2013-04-22 16:13:27','2013-04-24 14:44:29'),(7,'Test Feature',2130706433,0,'2013-04-24 16:05:54','2013-04-24 16:11:26'),(8,'Feature Management',2130706433,1,'2013-04-24 21:54:03','2013-04-24 21:54:03');
/*!40000 ALTER TABLE `features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedbacks`
--

DROP TABLE IF EXISTS `feedbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedbacks` (
  `id` bigint(20) NOT NULL auto_increment,
  `feedback_text` text character set utf8,
  `customer_id` bigint(20) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `status` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `FK_feedbacks` (`customer_id`),
  CONSTRAINT `FK_feedbacks` FOREIGN KEY (`customer_id`) REFERENCES `customer_information` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedbacks`
--

LOCK TABLES `feedbacks` WRITE;
/*!40000 ALTER TABLE `feedbacks` DISABLE KEYS */;
INSERT INTO `feedbacks` VALUES (1,'feed Back Text goes here',1,'2013-06-18 12:27:16',NULL,1),(2,'feedback_text',1,'2013-06-18 12:52:21',NULL,1),(3,'feedback_text',1,'2013-06-18 14:04:22',NULL,1),(4,'feedback_text',1,'2013-06-18 14:04:41',NULL,1),(5,'feedback_text',1,'2013-06-18 15:19:40',NULL,1),(6,'feed back text',1,'2013-06-18 04:39:09',NULL,1),(7,'feed back text text',1,'2013-06-18 05:06:24',NULL,1),(8,'feed back text text text',1,'2013-06-18 05:52:01',NULL,1),(9,'feed back text text text',4,'2013-06-18 06:44:41',NULL,1),(10,'feed back text text text',4,'2013-06-18 06:50:41',NULL,1),(11,'feed back text text text',5,'2013-06-18 06:51:17',NULL,1),(12,'hello how are you',6,'2013-06-19 07:19:01',NULL,1),(13,'hello how are you',9,'0000-00-00 00:00:00',NULL,1),(14,'feed back text goes here',7,'2013-06-17 15:19:28',NULL,1),(15,'hello how are you',7,'2013-06-19 07:47:17',NULL,1),(16,'this is feedback text...',10,'2013-06-18 12:52:21',NULL,1),(17,'this is feedback text...',10,'2013-06-18 12:52:21',NULL,1),(18,'this is feedback text...',10,'2013-06-18 12:52:21',NULL,1),(19,'this is feedback text...',10,'2013-06-28 06:47:34',NULL,1),(20,'this is feedback text...',10,'2013-06-28 07:14:26',NULL,1),(21,'this is feedback text...',10,'2013-06-28 07:14:58',NULL,1),(22,'this is feedback text...',11,'2013-06-28 07:16:06',NULL,1),(23,'this is feedback text...',11,'2013-07-01 01:06:53',NULL,1),(24,'this is feedback text...',11,'2013-07-01 01:07:15',NULL,1),(25,'this is feedback text...',11,'2013-07-01 01:07:57',NULL,1),(26,'this is feedback text...',11,'2013-07-01 01:09:06',NULL,1),(27,'this is feedback text...',11,'2013-07-01 01:09:38',NULL,1),(28,'this is feed back text',11,'2013-07-01 01:10:44',NULL,1),(29,'this is feed back text',11,'2013-07-01 02:20:18',NULL,1),(30,'this is feed back text',11,'2013-07-01 02:48:02',NULL,1),(31,'this is feed back text',11,'2013-07-01 03:06:01',NULL,1),(32,'this is feed back text',11,'2013-07-01 06:59:56',NULL,1),(33,'this is feed back text',11,'2013-07-01 07:00:30',NULL,1),(34,'this is feed back text',11,'2013-07-01 07:07:35',NULL,1),(35,'this is feed back text',11,'2013-07-01 07:08:28',NULL,1),(36,'this is feed back text',11,'2013-07-01 10:05:10',NULL,1),(37,'this is feed back text......',12,'2013-07-02 01:05:34',NULL,1),(38,'test feedback text..',13,'2013-07-02 05:23:48',NULL,1),(39,'this is feedback text.....',14,'2013-07-09 07:28:08',NULL,1),(40,'this is feedback text.....',14,'2013-07-09 07:29:46',NULL,1),(41,'this is feedback text.....',14,'2013-07-09 07:30:07',NULL,1),(42,'this is feedback text.....',14,'2013-07-09 07:30:28',NULL,1),(43,'this is feedback text.....',14,'2013-07-09 07:31:01',NULL,1),(44,'this is feedback text.....',14,'2013-07-09 07:32:20',NULL,1),(45,'this is feedback text.....',14,'2013-07-09 07:32:31',NULL,1),(46,'this is feedback text.....',14,'2013-07-09 07:33:08',NULL,1),(47,'this is feedback text.....',14,'2013-07-09 07:33:24',NULL,1),(48,'this is feedback text.....',14,'2013-07-09 07:33:34',NULL,1),(49,'this is feedback text.....',14,'2013-07-09 07:43:38',NULL,1),(50,'this is feedback text.....',14,'2013-07-09 07:45:07',NULL,1),(51,'',14,'2013-07-09 07:59:34',NULL,1),(52,'this is feed back text!!!!!!',14,'2013-07-09 08:01:07',NULL,1),(53,'this is feed back text!!!!!!',14,'2013-07-09 08:03:28',NULL,1),(54,'this is feed back text!!!!!!',14,'2013-07-09 08:34:41',NULL,1),(55,'this is feed back text!!!!!!',14,'2013-07-09 08:55:08',NULL,1),(56,'this is feed back text!!!!!!',14,'2013-07-09 08:55:48',NULL,1),(57,'this is feed back text!!!!!!',14,'2013-07-09 08:56:12',NULL,1),(58,'????? ??? ?? ???? ????? ?????',14,'2013-07-09 08:56:18',NULL,1),(59,'????? ??? ?? ???? ????? ?????',14,'2013-07-09 08:56:51',NULL,1),(60,'مرحبا بكم في موقع جمعية خيرية',14,'2013-07-09 08:57:40',NULL,1),(61,'مرحبا بكم في موقع جمعية خيرية',14,'2013-07-09 08:58:10',NULL,1),(62,'this is feed back text...',14,'2013-07-09 08:58:22',NULL,1),(63,'مرحبا بكم في موقع جمعية خيرية',14,'2013-07-09 08:58:54',NULL,1),(64,'مرحبا بكم في موقع جمعية خيرية',14,'2013-07-09 09:51:55',NULL,1),(65,'Hello world ',15,'2013-07-10 01:34:08',NULL,1),(66,'safd',18,'2013-07-23 11:51:19',NULL,1),(67,'Dsf',19,'2013-07-24 10:12:08',NULL,1),(68,'Gd',20,'2013-07-25 10:40:54',NULL,1);
/*!40000 ALTER TABLE `feedbacks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floors`
--

DROP TABLE IF EXISTS `floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floors` (
  `id` bigint(20) NOT NULL auto_increment,
  `floor_no` int(3) default NULL,
  `floor_name` varchar(250) default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `modified` datetime default NULL,
  `created` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floors`
--

LOCK TABLES `floors` WRITE;
/*!40000 ALTER TABLE `floors` DISABLE KEYS */;
INSERT INTO `floors` VALUES (1,0,'ground',1,NULL,NULL),(2,1,'meznine',1,NULL,NULL);
/*!40000 ALTER TABLE `floors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `force_modifiers`
--

DROP TABLE IF EXISTS `force_modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `force_modifiers` (
  `id` bigint(20) NOT NULL auto_increment,
  `item_id` bigint(20) unsigned NOT NULL,
  `size` text,
  `price` bigint(20) default NULL,
  `name` varchar(250) default NULL,
  `description` varchar(500) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `name_arabic` varchar(250) default NULL,
  `description_arabic` varchar(500) default NULL,
  `status` tinyint(3) unsigned default '1',
  PRIMARY KEY  (`id`),
  KEY `FK_force_modifiers` (`item_id`),
  CONSTRAINT `FK_force_modifiers` FOREIGN KEY (`item_id`) REFERENCES `category_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `force_modifiers`
--

LOCK TABLES `force_modifiers` WRITE;
/*!40000 ALTER TABLE `force_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `force_modifiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(250) default NULL,
  `image_name` varchar(50) default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `name_arabic` varchar(250) character set utf8 default NULL,
  `modified` datetime default NULL,
  `created` datetime default NULL,
  `image_caption` varchar(45) character set utf8 default NULL,
  `image_caption_arabic` varchar(45) character set utf8 default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Picture 1','1.png',1,'رواق','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(2,'Picture 2','2.png',1,'صالة عرض','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(3,'Picture 3','3.png',1,'دهليز','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(4,'Picture 4','4.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(5,'Picture 5','5.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(6,'Picture 6','6.png',1,'معرض رسوم','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(7,'Picture 7','7.png',1,'رواق','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(8,'Picture 8','8.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(9,'Picture 9','9.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(10,'Picture 10','10.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(11,'Picture 11','11.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(12,'Picture 12','12.png',0,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(13,'Picture 13','13.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(14,'Picture 14','14.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(15,'Picture 15','15.png',1,'منصة','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(19,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:09:48','2013-06-13 16:09:48','this is image description.','صالة عرضصالة عرض صال'),(20,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:09:48','2013-06-13 16:10:48','this is image description.','صالة عرضصالة عرض صال'),(21,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:09:48','2013-06-13 16:15:48','this is image description.','صالة عرضصالة عرض صال'),(22,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:09:48','2013-06-13 16:20:48','this is image description.','صالة عرضصالة عرض صال'),(23,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:27:48','2013-06-13 16:25:48','this is image description.','صالة عرضصالة عرض صال'),(24,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:25:48','2013-06-13 16:27:48','this is image description.','صالة عرضصالة عرض صال'),(25,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:29:48','2013-06-13 16:29:48','this is image description.','صالة عرضصالة عرض صال'),(26,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:32:48','2013-06-13 16:32:48','this is image description.','صالة عرضصالة عرض صال'),(27,'wqeqweqw','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:35:48','2013-06-13 16:35:48','this is image description.','صالة عرضصالة عرض صال'),(28,'this is name','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:40:48','2013-06-13 16:40:48','this is image description.','صالة عرضصالة عرض صال'),(29,'this is name','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:42:48','2013-06-13 16:42:48','this is image description.','صالة عرضصالة عرض صال'),(30,'blue','Screenshot from 2013-07-01 14:37:55.png',1,'منصة','2013-06-13 16:45:48','2013-06-13 16:45:48','this is image description.','صالة عرضصالة عرض صال'),(31,'blue','flower-game-screenshot-2.jpg',1,'صالة ع ر ض','2013-06-13 16:48:48','2013-06-13 16:48:48','this is image description.','صالة عرضصالة عرض صال'),(32,'blue','flower-game-screenshot-2.jpg',1,'ص الة ع ر ض','2013-06-13 16:50:48','2013-06-13 16:50:48','this is image description.','صالة عرضصالة عرض صال'),(33,'blue','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:52:48','2013-06-13 16:52:48','this is image description.','صالة عرضصالة عرض صال'),(34,'blue','flower-game-screenshot-2.jpg',1,'صالة عرض','2013-06-13 16:54:48','2013-06-13 16:54:48','this is image description.','صالة عرضصالة عرض صال');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Super Admin',2130706433,1,'2013-04-04 15:59:31','2013-04-24 20:15:22'),(2,'Admin',2130706433,1,'2013-04-04 15:59:41','2013-04-24 20:15:35'),(4,'User',2130706433,1,'2013-04-22 22:34:21','2013-04-24 20:15:39'),(5,'testing',2130706433,0,'2013-04-24 19:52:50','2013-04-24 20:18:44');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inserts`
--

DROP TABLE IF EXISTS `inserts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inserts` (
  `id` int(23) NOT NULL auto_increment,
  `name` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inserts`
--

LOCK TABLES `inserts` WRITE;
/*!40000 ALTER TABLE `inserts` DISABLE KEYS */;
/*!40000 ALTER TABLE `inserts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `parentId` int(10) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  `name` varchar(250) NOT NULL,
  `imageUrl` varchar(150) NOT NULL,
  `displayOrder` int(3) NOT NULL,
  `status` tinyint(3) NOT NULL default '1',
  `created` datetime NOT NULL,
  `lastUpdate` datetime NOT NULL COMMENT 'lastUpdate changed to modified because of cake php default modication featured ',
  `extra` varchar(500) default NULL,
  `nameArabic` varchar(250) default NULL,
  `price` int(10) default NULL,
  `description` varchar(500) default NULL,
  `descriptionArabic` varchar(500) default NULL,
  `is_item` int(5) NOT NULL default '0',
  `is_sideline_category` int(5) default NULL,
  `allow_sideline` int(5) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1 COMMENT='menus table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,0,NULL,NULL,'Food','',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(135,1,NULL,NULL,'Appetizer','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,400,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(136,1,NULL,NULL,'Sandwich','http://apptestonline.com/resturantapp/images/sandwich/sandwich1.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,500,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(137,1,NULL,NULL,'Pasta','http://apptestonline.com/resturantapp/images/pasta/pasta1.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,600,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,1),(138,1,NULL,NULL,'Rissoto','http://apptestonline.com/resturantapp/images/risotto/risotto1.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,700,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(139,1,NULL,NULL,'Pizza','http://apptestonline.com/resturantapp/images/pizza/pizza1.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,250,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(140,1,NULL,NULL,'Maincourse','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,369,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(141,1,NULL,NULL,'Side Dish','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-14 08:51:04',NULL,NULL,258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(142,1,NULL,NULL,'Dessert','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,147,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(143,1,NULL,NULL,'Ice Cream','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,789,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(144,1,NULL,NULL,'Kids Menu','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,589,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(145,135,NULL,NULL,'Hot Appetizer','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(146,135,NULL,NULL,'Cold Appetizer','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(147,135,NULL,NULL,'Salad','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,255,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(148,135,NULL,NULL,'Soup','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,5698,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(150,140,NULL,NULL,'Meat','http://apptestonline.com/resturantapp/images/main-course/meat/meat1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(151,140,NULL,NULL,'Fish','http://apptestonline.com/resturantapp/images/main-course/fish/fish1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(152,140,NULL,NULL,'SeaFood','http://apptestonline.com/resturantapp/images/main-course/sea-food/seafood1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,563,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(153,136,NULL,NULL,'Club Sanwich','http://apptestonline.com/resturantapp/images/sandwich/sandwich4.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,254,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,0,0),(154,136,NULL,NULL,'Chicken Sandwich','http://apptestonline.com/resturantapp/images/sandwich/sandwich3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,456,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,0,0),(155,136,NULL,NULL,'Cheese Sandwich','http://apptestonline.com/resturantapp/images/sandwich/sandwich1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,569,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,0,0),(156,151,NULL,NULL,'Grilled Fish','http://apptestonline.com/resturantapp/images/main-course/fish/fish1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,569,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,0,0),(157,151,NULL,NULL,'Baked Fish','http://apptestonline.com/resturantapp/images/main-course/fish/fish2.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,0,0),(158,151,NULL,NULL,'Fish and Chips','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,369,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,0,0),(159,1,NULL,NULL,'Student Menu','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,145,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(160,1,NULL,NULL,'Banquet Menu','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(161,1,NULL,NULL,'Special Menu','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,265,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(162,1,NULL,NULL,'Take Away','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,236,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(163,1,NULL,NULL,'Delivery','http://apptestonline.com/resturantapp/images/main-course/fish/fish3.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,254,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(164,0,NULL,NULL,'Beverages','',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,1258,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(165,164,NULL,NULL,'Cold Beverages','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,NULL,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(166,164,NULL,NULL,'Hot Beverages','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,456,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(167,165,NULL,NULL,'Softdrinks','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-16 08:51:04',NULL,NULL,589,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,0,0),(168,165,NULL,NULL,'Juices','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-18 08:51:04',NULL,NULL,658,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,0,0,0),(169,165,NULL,NULL,'Cocktails','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,569,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(170,165,NULL,NULL,'Milk Shakes','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,475,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(171,165,NULL,NULL,'Water','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,587,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(172,165,NULL,NULL,'Specials','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,568,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(173,166,NULL,NULL,'Coffee','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(174,166,NULL,NULL,'Tea','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,321,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(175,166,NULL,NULL,'Specials','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,987,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,0,0,0),(176,167,NULL,NULL,'Pepsi','http://apptestonline.com/resturantapp/images/food3.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,70,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,1,0,0),(177,167,NULL,NULL,'Coca cola','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,70,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,1,0,0),(178,167,NULL,NULL,'Mirinda','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,70,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,1,0,0),(179,167,NULL,NULL,'Sprite','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,70,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',NULL,1,0,0),(180,176,NULL,NULL,'Pepsi Small','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,32,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(181,176,NULL,NULL,'Pepsi Medium','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,80,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(182,176,NULL,NULL,'Pepsi Large','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-06-14 08:51:04',NULL,NULL,120,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(183,225,0,0,'Baked Potato','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-20 08:51:04',NULL,NULL,50,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(184,225,NULL,0,'Mashed Potato','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-20 08:51:04',NULL,NULL,100,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(185,225,NULL,NULL,'Scrumbled Potato','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-20 08:51:04',NULL,NULL,100,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non num',NULL,1,1,0),(188,145,NULL,NULL,'Appitizer 1 ','http://apptestonline.com/resturantapp/images/appetizers/apetizer3.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,147,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(189,145,NULL,NULL,'Appitizer 2','http://apptestonline.com/resturantapp/images/appetizers/apetizer1.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(190,145,NULL,NULL,'Appiizer 3','http://apptestonline.com/resturantapp/images/appetizers/apetizer2.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,369,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(191,146,NULL,NULL,'Cold Appitizer 1','http://apptestonline.com/resturantapp/images/appetizers/apetizer2.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,987,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(192,146,NULL,NULL,'Cold Appitizer2','http://apptestonline.com/resturantapp/images/appetizers/apetizer1.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,456,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(193,146,NULL,NULL,'Cold Appitizer 3','http://apptestonline.com/resturantapp/images/appetizers/apetizer4.jpg\"',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,321,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(194,147,NULL,NULL,'Salad 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,148,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(195,147,NULL,NULL,'Salad 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,256,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(196,148,NULL,NULL,'Soup 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,263,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(197,148,NULL,NULL,'Soup 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,259,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(198,148,NULL,NULL,'Soup 3','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(199,150,NULL,NULL,'Meat 1','http://apptestonline.com/resturantapp/images/main-course/meat/meat1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(200,150,NULL,NULL,'Meat 2','http://apptestonline.com/resturantapp/images/main-course/meat/meat2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,265,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(201,150,NULL,NULL,'Meat 3','http://apptestonline.com/resturantapp/images/main-course/meat/meat3.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,2369,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(202,152,NULL,NULL,'SeaFood 1','http://apptestonline.com/resturantapp/images/main-course/sea-food/seafood1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,236,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(203,152,NULL,NULL,'SeaFood 2','http://apptestonline.com/resturantapp/images/main-course/sea-food/seafood2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(204,152,NULL,NULL,'Sea Food 3','http://apptestonline.com/resturantapp/images/main-course/sea-food/seafood3.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,147,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(205,159,NULL,NULL,'Student Menu 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,147,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(206,159,NULL,NULL,'Student Menu 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,147,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(207,160,NULL,NULL,'Banquet Menu 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,125,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(208,160,NULL,NULL,'Banquet Menu 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(209,161,NULL,NULL,' Special Menu 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,258,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(210,161,NULL,NULL,'Special Menu 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,963,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(211,162,NULL,NULL,'Take Away 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(212,162,NULL,NULL,'Take Away 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,74,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(213,162,NULL,NULL,'Take Away 3','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,185,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(214,163,NULL,NULL,'Delivery 1','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(215,163,NULL,NULL,'Delivery 2','',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(216,137,NULL,NULL,'Pasta 1','http://apptestonline.com/resturantapp/images/pasta/pasta1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,963,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(217,137,NULL,NULL,'Pasta 2','http://apptestonline.com/resturantapp/images/pasta/pasta2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,741,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(218,137,NULL,NULL,'Pasta 3','http://apptestonline.com/resturantapp/images/pasta/pasta3.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(219,138,NULL,NULL,'Rissoto 1','http://apptestonline.com/resturantapp/images/risotto/risotto3.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,963,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(220,138,NULL,NULL,'Rissoto 2','http://apptestonline.com/resturantapp/images/risotto/risotto2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(221,138,NULL,NULL,'Rissoto 3','http://apptestonline.com/resturantapp/images/risotto/risotto1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,74,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(222,139,NULL,NULL,'Pizza 1','http://apptestonline.com/resturantapp/images/pizza/pizza1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,471,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(223,139,NULL,NULL,'Pizza 2','http://apptestonline.com/resturantapp/images/pizza/pizza2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(224,139,NULL,NULL,'Pizza 3','http://apptestonline.com/resturantapp/images/pizza/pizza3.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,963,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(225,141,NULL,NULL,'Side Dish 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,1,0),(226,141,NULL,NULL,'Side Dish 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,741,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,NULL,NULL),(227,141,NULL,NULL,'Side Dish 3','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,8552,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,NULL,NULL),(228,142,NULL,NULL,'Dessert 1','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,852,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(229,142,NULL,NULL,'Dessert 2','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,963,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(230,142,NULL,NULL,'Dessert 3','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,874,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(231,143,NULL,NULL,'Ice Cream 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,856,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(232,143,NULL,NULL,'Ice Cream 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,895,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(233,143,NULL,NULL,'Ice Cream 3','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(234,144,NULL,NULL,'Kids Menu 1','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,9654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(235,144,NULL,NULL,'Kids Menu 2','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(236,144,NULL,NULL,'Kids Menu 3','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,965,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(237,168,NULL,NULL,'Juice 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(238,168,NULL,NULL,'Juice 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,9654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(239,169,NULL,NULL,'cocktail 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(240,169,NULL,NULL,'Cocktail 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,9654,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(241,170,NULL,NULL,'Milk shake 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(242,170,NULL,NULL,'Milk shake 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,965,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(243,173,NULL,NULL,'Coffee 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(244,173,NULL,NULL,'Coffee 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL),(245,174,NULL,NULL,'Tea 1','http://apptestonline.com/resturantapp/images/food2.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,0,NULL,NULL),(246,174,NULL,NULL,'Tea 2','http://apptestonline.com/resturantapp/images/food1.jpg',0,1,'0000-00-00 00:00:00','2013-07-24 12:51:04',NULL,NULL,854,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_copy`
--

DROP TABLE IF EXISTS `items_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_copy` (
  `id` bigint(20) unsigned NOT NULL default '0',
  `parent_id` int(10) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  `name` varchar(250) NOT NULL,
  `image_name` text NOT NULL,
  `display_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL COMMENT 'lastUpdate changed to modified because of cake php default modication featured ',
  `extra` varchar(500) default NULL,
  `name_arabic` varchar(250) default NULL,
  `price` int(10) default NULL,
  `description` varchar(500) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='menus table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_copy`
--

LOCK TABLES `items_copy` WRITE;
/*!40000 ALTER TABLE `items_copy` DISABLE KEYS */;
INSERT INTO `items_copy` VALUES (1,14,2,49,'Food','aaa',1,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','d','dd',NULL,NULL),(2,1,3,32,'Appetizer','dfd',2,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','ww','ww',NULL,NULL),(3,1,33,42,'Sandwich','dd',3,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','jhk','jh',NULL,NULL),(4,1,43,44,'Pasta','nh\r\n',4,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','jhk','kjh',NULL,NULL),(5,1,45,46,'Rissoto','hjkjl',5,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','jk','hj',NULL,NULL),(6,1,47,48,'Pizza',';k;k',6,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','lkj','klj',NULL,NULL),(7,2,4,11,'Hot Appetizer','kkk',7,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','k','k',NULL,NULL),(8,2,12,17,'cold Appetizer','ddd',22,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','2','2',NULL,NULL),(9,2,18,23,'Salad','jk',22,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','k','k',NULL,NULL),(10,2,24,31,'soup','kjj',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','j','j',NULL,NULL),(11,7,5,6,'chichen wings','ljd',22,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','j','j',NULL,NULL),(12,7,7,8,'Roasted Squash and Sweet Potato Flatbread','rrr',212,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','dd','dd',NULL,NULL),(13,10,25,26,'Roast carrot soup with pancetta croutons ','fff',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','f','f',NULL,NULL),(14,NULL,1,52,'items','dd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','ss','s',NULL,NULL),(15,7,9,10,'Grilled Veggie and Steak Appetizer','ddd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','dd','dd',NULL,NULL),(16,8,13,14,'Chicken Puffs','dsd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','d','d',NULL,NULL),(17,8,15,16,'Parmesan Cheese Ball','ddd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','d','d',NULL,NULL),(18,9,19,20,'Broccoli Spaghetti','ddd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','d','d',NULL,NULL),(19,10,27,28,'Spagetti','kk	',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','k','k',NULL,NULL),(20,9,21,22,'Pasta Salad','dd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','l','l',NULL,NULL),(21,10,29,30,'Creamy tomato soup ','ddd',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','d','d',NULL,NULL),(22,3,34,37,'club sandwich','hlo',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','lk','k',NULL,NULL),(23,22,35,36,'mayo club sandwich','kj',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','j','j',NULL,NULL),(24,3,38,39,'chicken sandwich','l',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:17','j','j',NULL,NULL),(25,3,40,41,'cheese sandwich','j',0,1,'0000-00-00 00:00:00','2013-06-11 08:55:18','k','kk',NULL,NULL),(26,14,50,51,'Beverages','lk',11,1,'2013-06-11 13:59:22','2013-06-11 08:55:18','j','j',NULL,NULL);
/*!40000 ALTER TABLE `items_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(250) NOT NULL,
  `image_name` text NOT NULL,
  `display_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL COMMENT 'lastUpdate changed to modified because of cake php default modication featured ',
  `extra` varchar(500) default NULL,
  `name_arabic` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='menus table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'breakfast','http://localhost/resturant/uploads/images_menu/6334328fc19393608cc8b4800a5ba8c5.JPG',1,1,'2013-05-31 12:59:16','2013-05-31 12:59:16',NULL,NULL),(2,'lunch','http://localhost/resturant/uploads/images_menu/5609f6c30d4adb9f375be1cc18e8d566.JPG',1,1,'2013-06-03 09:06:02','2013-06-03 09:06:02',NULL,NULL),(3,'dinner','ssss',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `seat_no` bigint(20) default NULL,
  `item_id` bigint(20) default NULL,
  `quantity` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `item_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(3) default NULL,
  `iscomplimentary` tinyint(1) default NULL,
  `created` datetime default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `table_id` bigint(20) default NULL,
  `seat_no` int(10) default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_order_item` (`item_id`),
  KEY `FK_order_item_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,1,1,15,NULL,'2013-06-17 16:19:47',1,1,5,'2013-06-17 16:49:48'),(2,15,3,1,NULL,'2013-06-19 05:10:17',1,1,3,NULL),(3,12,3,1,NULL,'2013-06-18 03:22:04',0,1,1,NULL),(4,17,3,1,NULL,'2013-06-19 05:10:19',1,1,4,NULL),(5,13,3,4,NULL,'2013-06-19 05:10:16',1,1,2,NULL),(6,11,3,1,NULL,'2013-06-18 03:22:03',1,1,0,NULL),(7,15,4,1,NULL,'2013-06-19 05:10:17',0,1,3,NULL),(8,12,4,1,NULL,'2013-06-18 03:22:04',1,1,1,NULL),(9,17,4,1,NULL,'2013-06-19 05:10:19',1,1,4,NULL),(10,13,4,4,NULL,'2013-06-19 05:10:16',1,1,2,NULL),(11,11,4,1,NULL,'2013-06-18 03:22:03',1,1,0,NULL),(12,1,7,40,NULL,'0000-00-00 00:00:00',0,12,12,NULL),(13,1,12,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(14,2,12,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(15,3,12,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(16,1,13,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(17,2,13,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(18,3,13,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(19,1,14,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(20,2,14,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(21,3,14,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(22,1,15,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(23,2,15,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(24,3,15,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(25,1,16,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(26,2,16,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(27,3,16,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(28,1,17,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(29,2,17,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(30,3,17,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(31,1,18,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(32,2,18,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(33,3,18,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(34,1,22,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(35,2,22,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(36,3,22,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(37,1,23,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(38,2,23,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(39,3,23,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(40,1,25,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(41,2,25,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(42,3,25,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(43,1,26,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(44,2,26,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(45,3,26,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(46,1,27,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(47,2,27,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(48,3,27,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(49,1,28,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(50,1,28,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(51,1,29,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(52,1,29,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(53,1,39,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(54,1,39,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(55,1,49,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(56,1,49,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(57,1,50,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(58,2,50,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(59,2,50,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(60,1,52,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(61,81,52,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(62,1,53,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(63,81,53,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(64,81,53,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(65,1,54,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(66,1,54,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(67,1,54,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(68,153,55,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(69,153,55,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(70,153,55,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(71,153,56,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(72,154,56,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(73,154,56,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(74,153,57,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(75,154,57,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(76,154,57,40,NULL,'0000-00-00 00:00:00',1,12,12,NULL),(77,153,59,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(78,2,59,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(79,153,60,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(80,2,60,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(81,153,61,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(82,154,61,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(83,153,62,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(84,154,62,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(85,153,63,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(86,154,63,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(87,153,64,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(88,154,64,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(89,153,65,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(90,154,65,4,NULL,'2013-07-24 00:00:00',1,12,12,NULL),(91,153,66,40,NULL,'2013-07-25 00:00:00',1,12,12,NULL),(92,154,66,40,NULL,'2013-07-25 00:00:00',1,12,12,NULL),(93,229,67,1,NULL,'2013-07-25 00:00:00',1,1,2,NULL),(94,240,67,3,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(95,229,67,1,NULL,'2013-07-25 00:00:00',1,1,3,NULL),(96,229,67,1,NULL,'2013-07-25 00:00:00',1,1,1,NULL),(97,179,67,5,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(98,5,68,4,NULL,'2013-07-25 00:00:00',1,11,1,NULL),(99,5,68,5,NULL,'2013-07-25 00:00:00',1,11,6,NULL),(100,229,69,1,NULL,'2013-07-25 00:00:00',1,1,2,NULL),(101,240,69,3,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(102,229,69,1,NULL,'2013-07-25 00:00:00',1,1,3,NULL),(103,229,69,1,NULL,'2013-07-25 00:00:00',1,1,1,NULL),(104,179,69,5,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(105,229,70,1,NULL,'2013-07-25 00:00:00',1,1,2,NULL),(106,240,70,3,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(107,229,70,1,NULL,'2013-07-25 00:00:00',1,1,3,NULL),(108,229,70,1,NULL,'2013-07-25 00:00:00',1,1,1,NULL),(109,179,70,5,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(110,229,71,1,NULL,'2013-07-25 00:00:00',1,1,2,NULL),(111,240,71,3,NULL,'2013-07-25 00:00:00',1,1,0,NULL),(112,229,71,1,NULL,'2013-07-25 00:00:00',1,1,3,NULL),(113,229,71,1,NULL,'2013-07-25 00:00:00',1,1,1,NULL),(114,179,71,5,NULL,'2013-07-25 00:00:00',1,1,0,NULL);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_taketime` datetime default NULL,
  `order_servetime` datetime default NULL,
  `order_checkout_time` datetime default NULL,
  `table_id` bigint(20) default NULL,
  `ordertype` varchar(100) default NULL,
  `order_status` text,
  `order_tabbed_amount` int(10) default NULL,
  `order_amount_paid` int(10) default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `customer_id` bigint(20) default NULL,
  `no_of_seats` varchar(45) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_orders` (`customer_id`),
  CONSTRAINT `FK_orders` FOREIGN KEY (`customer_id`) REFERENCES `customer_information` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2013-06-17 15:19:28',NULL,NULL,1,'dineIn',NULL,150,250,1,1,NULL),(2,'2013-06-19 07:26:25',NULL,NULL,1,'dinein',NULL,NULL,NULL,1,7,NULL),(3,'2013-06-19 07:30:49',NULL,NULL,1,'dinein',NULL,NULL,NULL,0,7,NULL),(4,'2013-06-19 07:36:15',NULL,NULL,1,'dinein',NULL,NULL,NULL,1,8,NULL),(5,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,0,16,NULL),(6,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(7,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,0,16,NULL),(8,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(9,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(10,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(11,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(12,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,0,16,NULL),(13,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(14,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(15,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(16,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(17,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,NULL),(18,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(19,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(20,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(21,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(22,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(23,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(24,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(25,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(26,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(27,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(28,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(29,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(30,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(31,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(32,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(33,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(34,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(35,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(36,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(37,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(38,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(39,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(40,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(41,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(42,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(43,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(44,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(45,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(46,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(47,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(48,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,NULL,1,16,'5'),(49,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,50,1,16,'5'),(50,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,50,1,16,'5'),(51,'0000-00-00 00:00:00',NULL,NULL,12,'','active',NULL,50,1,16,'5'),(52,'0000-00-00 00:00:00',NULL,NULL,12,'','active',NULL,50,1,16,'5'),(53,'0000-00-00 00:00:00',NULL,NULL,12,'','active',NULL,0,1,16,'5'),(54,'0000-00-00 00:00:00',NULL,NULL,12,'','active',NULL,0,1,16,'5'),(55,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,254,1,16,'5'),(56,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,254,1,16,'5'),(57,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,710,1,16,'5'),(58,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,254,1,16,'5'),(59,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,254,1,16,'5'),(60,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,1016,1,16,'5'),(61,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,2840,1,16,'5'),(62,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,2840,1,16,'5'),(63,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,2840,1,16,'5'),(64,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,2840,1,16,'5'),(65,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,2840,1,16,'5'),(66,'0000-00-00 00:00:00',NULL,NULL,12,'DineInn','active',NULL,28400,1,16,'5'),(67,'0000-00-00 00:00:00',NULL,NULL,1,'dinein','active',NULL,32201,1,21,'1'),(68,'0000-00-00 00:00:00',NULL,NULL,11,'DINEinn','active',NULL,0,1,19,'2'),(69,'2013-07-25 03:45:39',NULL,NULL,1,'dinein','active',NULL,32201,1,21,'1'),(70,'2013-07-25 03:53:24',NULL,NULL,1,'dinein','active',NULL,32201,1,21,'1'),(71,'2013-07-25 05:04:08',NULL,NULL,1,'dinein','active',NULL,32201,1,21,'1');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_tables`
--

DROP TABLE IF EXISTS `reservation_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_tables` (
  `id` bigint(20) NOT NULL auto_increment,
  `floor_id` bigint(20) NOT NULL,
  `table_id` bigint(20) NOT NULL,
  `reservation_id` bigint(20) NOT NULL,
  `status` tinyint(3) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `FK_reservation_tables` (`floor_id`),
  KEY `FK_reservation_tables_tables` (`table_id`),
  KEY `FK_reservation_tables_reservations` (`reservation_id`),
  CONSTRAINT `FK_reservation_tables` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`),
  CONSTRAINT `FK_reservation_tables_reservations` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`),
  CONSTRAINT `FK_reservation_tables_tables` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=392 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_tables`
--

LOCK TABLES `reservation_tables` WRITE;
/*!40000 ALTER TABLE `reservation_tables` DISABLE KEYS */;
INSERT INTO `reservation_tables` VALUES (299,1,1,374,1),(300,1,2,375,1),(301,1,2,376,1),(302,1,2,377,1),(303,1,1,383,1),(304,1,2,384,1),(305,1,3,386,1),(306,1,4,387,1),(307,1,5,388,1),(308,1,6,389,1),(309,1,7,390,1),(310,1,8,391,1),(311,1,9,392,1),(312,1,10,393,1),(313,1,10,394,1),(314,1,10,395,1),(315,1,10,396,1),(316,1,10,397,1),(317,1,10,398,1),(318,1,10,399,1),(319,2,10,400,1),(320,2,10,401,1),(321,1,10,402,1),(322,1,10,403,1),(323,1,10,404,1),(324,1,10,405,1),(325,1,2,406,1),(326,1,2,407,1),(327,1,10,408,1),(328,1,2,409,1),(329,1,10,410,1),(330,1,2,411,1),(331,1,2,412,1),(332,1,2,413,1),(334,2,2,415,1),(335,2,2,428,1),(336,2,2,429,1),(337,2,2,430,1),(338,2,2,444,1),(339,2,3,444,1),(340,2,5,444,1),(341,1,1,444,1),(342,1,2,444,1),(343,1,2,445,1),(344,1,2,445,1),(345,1,2,445,1),(346,1,2,445,1),(347,1,2,445,1),(389,2,2,414,0),(390,2,3,414,0),(391,2,5,414,0);
/*!40000 ALTER TABLE `reservation_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `id` bigint(20) NOT NULL auto_increment,
  `no_of_children` bigint(20) default NULL,
  `no_of_oldage` bigint(20) default NULL,
  `confirm_via_email` tinyint(1) default NULL,
  `email_address` varchar(500) default NULL,
  `phone_no` int(200) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `no_of_guest` bigint(20) default NULL,
  `time_to` datetime default NULL,
  `time_from` datetime default NULL,
  `event_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_reservations` (`event_id`),
  CONSTRAINT `FK_reservations` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (47,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(48,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 20:06:08',1),(49,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(50,1,2,1,'demo@demo.com',12345678,'2013-06-14 17:36:08',NULL,1,5,'2013-07-10 16:04:00','2013-07-10 20:04:00',6),(51,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(52,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,0,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(53,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(54,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,0,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(55,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,0,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(56,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(57,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(58,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(59,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(60,3,4,0,'sadie@hotmail.com',123456,'2013-06-14 17:36:08',NULL,0,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(61,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,0,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(62,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(63,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(64,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(65,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(66,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(67,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(68,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(69,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(70,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(71,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(72,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(73,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(74,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(75,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(76,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(77,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(78,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(79,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(80,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(81,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(82,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(83,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(84,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(85,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(86,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(87,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(88,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(89,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(90,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(91,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(92,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(93,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(94,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(95,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(96,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(97,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(98,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(99,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(100,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(101,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(102,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(103,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(104,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(105,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(106,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(107,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(108,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(109,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(110,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(111,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(112,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(113,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(114,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(115,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(116,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(117,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(118,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(119,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(120,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(121,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(122,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(123,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(124,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(125,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(126,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(127,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(128,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(129,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(130,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(131,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(132,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(133,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(134,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(135,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(136,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(137,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(138,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(139,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(140,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(141,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(142,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(143,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(144,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(145,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(146,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(147,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(148,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(149,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(150,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(151,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(152,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(153,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(154,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(155,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(156,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(157,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(158,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(159,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(160,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(161,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(162,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(163,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(164,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(165,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(166,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(167,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(168,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(169,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(170,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(171,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(172,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(173,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(174,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(175,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(176,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(177,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(178,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(179,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(180,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(181,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(182,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(183,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(184,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(185,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(186,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(187,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(188,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(189,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(190,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(191,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(192,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(193,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(194,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(195,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(196,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(197,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(198,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(199,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(200,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(201,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(202,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(203,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(204,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(205,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(206,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(207,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(208,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(209,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(210,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(211,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(212,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(213,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(214,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(215,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(216,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(217,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(218,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(219,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(220,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(221,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(222,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(223,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(224,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(225,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(226,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(227,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(228,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(229,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(230,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(231,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(232,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(233,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(234,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(235,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(236,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(237,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(238,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(239,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(240,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(241,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(242,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(243,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(244,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(245,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(246,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(247,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(248,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(249,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(250,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(251,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(252,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(253,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(254,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(255,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(256,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(257,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(258,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(259,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(260,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(261,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(262,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(263,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(264,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(265,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(266,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(267,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(268,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(269,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(270,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(271,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(272,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(273,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(274,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(275,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(276,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(277,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(278,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(279,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(280,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(281,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(282,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(283,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(284,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(285,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(286,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(287,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(288,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(289,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(290,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(291,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(292,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,3,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(293,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(294,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(295,4,4444,0,'sadaaaaaa@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(296,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(297,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(298,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(299,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(300,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(301,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(302,4,4444,0,'sadaaaaaa@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(303,4,4444,0,'sadaaaaaa@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(304,4,4444,0,'sadaaaaaa@hotmail.com',1234111156,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(305,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(306,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(307,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(308,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(309,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(310,3,4,0,'sadieee@hotmail.com',123456,'0000-00-00 00:00:00',NULL,1,45,'1970-01-01 00:00:00','1970-01-01 00:00:00',1),(311,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(312,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(313,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(314,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(315,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(316,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(317,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(318,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(319,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(320,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(321,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(322,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(323,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(324,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(325,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(326,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(327,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(328,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(329,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(330,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(331,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(332,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(333,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(334,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(335,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(336,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(337,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(338,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(339,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(340,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(341,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(342,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(343,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(344,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(345,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(346,3,4,0,'sadieee@hotmail.com',123465,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(347,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(348,3,4,0,'sadieee@hotmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(349,3,4,1,'demo@gmail.com',123456,'2013-06-14 17:36:08',NULL,1,45,'2013-06-14 18:36:08','2013-06-14 17:06:08',1),(350,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(351,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(352,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(353,1,2,0,'demo@demo.com',123456,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(354,1,2,0,'demo@demo.com',123456789,'2013-06-14 17:36:08',NULL,1,4,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(355,2,3,1,'demo@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(356,2,3,1,'new@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(357,2,3,1,'new@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(358,2,3,1,'new@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(359,2,3,1,'new@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(360,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(361,77,76,1,'yy@ho.com',766,'0000-00-00 00:00:00',NULL,1,87,'2013-07-25 12:00:00','2013-07-25 10:30:00',6),(362,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(363,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(364,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(365,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(366,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(367,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(368,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(369,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(370,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(371,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(372,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(373,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(374,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(375,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(376,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(377,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(378,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(379,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(380,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(381,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(382,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(383,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(384,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(385,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(386,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(387,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(388,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(389,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(390,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(391,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(392,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(393,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(394,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(395,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(396,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(397,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(398,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(399,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(400,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(401,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(402,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(403,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(404,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(405,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(406,8,8,1,'ii@cc.com',8373,'0000-00-00 00:00:00',NULL,1,7,'2013-07-25 16:11:45','2013-07-25 14:41:45',4),(407,77,777,0,'ii@dd.com',877,'0000-00-00 00:00:00',NULL,1,7,'2013-07-25 16:11:45','2013-07-25 14:41:45',4),(408,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(409,7,7,1,'iu@hg.com',76,'0000-00-00 00:00:00',NULL,1,7,'2013-07-25 16:11:45','2013-07-25 14:41:45',12),(410,2,3,1,'denome@demo.com',123456789,'2013-07-14 08:51:04',NULL,1,5,'2013-07-14 09:51:04','2013-07-14 08:21:04',12),(411,1,2,1,'shashlik@gmail.com',123456,'0000-00-00 00:00:00',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',2),(412,1,2,1,'shashlik@gmail.com',123456,'0000-00-00 00:00:00',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(413,1,2,1,'shashlik@gmail.com',123456,'0000-00-00 00:00:00',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(414,1,2,1,'shashlik_edit@gmail.com',123456,'2013-07-26 10:20:24','2013-07-26 03:52:17',0,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(415,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:23:19',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(416,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:44:34',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(417,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:45:31',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(418,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:47:16',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(419,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:47:35',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(420,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:49:40',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(421,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:53:16',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(422,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:55:31',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(423,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:56:04',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(424,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:56:43',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(425,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:57:08',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(426,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 10:58:31',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(427,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:00:46',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(428,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:08:50',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(429,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:09:23',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(430,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:13:30',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(431,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:52:24',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(432,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 11:53:13',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(433,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:12:28',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(434,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:13:04',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(435,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:13:19',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(436,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:13:34',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(437,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:14:16',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(438,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:15:10',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(439,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:16:09',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(440,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:18:36',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(441,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:18:53',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(442,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:20:08',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(443,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:21:28',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(444,1,2,1,'shashlik@gmail.com',123456,'2013-07-26 12:28:50',NULL,1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12),(445,1,2,1,'shashlik@gmail.com',123456,'0000-00-00 00:00:00','2013-07-26 03:08:10',1,5,'2013-06-14 18:36:08','2013-06-14 17:06:08',12);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables` (
  `id` bigint(20) NOT NULL auto_increment,
  `table_no` int(20) default NULL,
  `name` varchar(250) default NULL,
  `status` tinyint(3) NOT NULL default '1',
  `modified` datetime default NULL,
  `created` datetime default NULL,
  `floor_id` bigint(20) NOT NULL,
  `state` text,
  `capacity` int(20) default NULL,
  `name_arabic` varchar(250) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_tables` (`floor_id`),
  CONSTRAINT `FK_tables` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (1,1,'Table 1',1,'2013-06-14 19:43:26',NULL,1,'Reservered',5,'Arabic Name'),(2,2,'Table2',1,'2013-06-24 17:01:43','2013-06-24 17:01:43',1,'1',5,''),(3,3,'Table',1,NULL,NULL,1,'1',5,NULL),(4,4,'Table2',1,'0000-00-00 00:00:00',NULL,1,'1',5,NULL),(5,5,'Table2',1,NULL,NULL,1,'1',5,NULL),(6,6,'Table2',1,NULL,NULL,1,'1',5,NULL),(7,7,'Table2',1,NULL,NULL,1,'1',1,NULL),(8,8,'Table2',1,NULL,NULL,1,'1',1,NULL),(9,9,'Table2',1,NULL,NULL,1,'1',1,NULL),(10,10,'Table2',1,NULL,NULL,1,'1',1,NULL);
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmps`
--

DROP TABLE IF EXISTS `tmps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmps` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `target_id` varchar(255) NOT NULL COMMENT 'string field holding the unique ID of a record for e.g registration',
  `type` enum('registration','forgot password') NOT NULL,
  `data` mediumtext NOT NULL,
  `expire_at` date NOT NULL COMMENT 'Date on which the row will be expired / Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmps`
--

LOCK TABLES `tmps` WRITE;
/*!40000 ALTER TABLE `tmps` DISABLE KEYS */;
INSERT INTO `tmps` VALUES (21,'abc@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"abc@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:31:16','0000-00-00 00:00:00'),(23,'def@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"def@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:32:50','0000-00-00 00:00:00'),(24,'dsf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"dsf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:36:38','0000-00-00 00:00:00'),(25,'sdf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:38:36','0000-00-00 00:00:00'),(26,'sdd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:38:45','0000-00-00 00:00:00'),(27,'sdfsd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfsd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:39:30','0000-00-00 00:00:00'),(28,'dsfd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"dsfd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:43:48','0000-00-00 00:00:00'),(29,'sdfsad@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfsad@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:44:37','0000-00-00 00:00:00'),(30,'sadfsdf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sadfsdf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:47:33','0000-00-00 00:00:00'),(31,'sdfsad@yahoo.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdf\",\"username\":\"sdfsad@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 19:17:19','0000-00-00 00:00:00'),(32,'dsfsdaf@yahoo.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdf\",\"username\":\"dsfsdaf@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 19:19:59','0000-00-00 00:00:00'),(33,'sdfasdfsda@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfasdfsda@sdkfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"aksdfj\"}','2013-07-12','2013-04-12 19:20:55','0000-00-00 00:00:00'),(34,'asdfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"asdfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:23:56','0000-00-00 00:00:00'),(35,'sdfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:26:36','0000-00-00 00:00:00'),(36,'ssadfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"ssadfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:28:30','0000-00-00 00:00:00'),(37,'sdfsadf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfsadf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:29:23','0000-00-00 00:00:00'),(38,'sdfsadf@sddkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfsadf@sddkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:35:45','0000-00-00 00:00:00'),(39,'sdfjsadf@gkasdfj.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkasdfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-15','2013-04-15 12:51:09','0000-00-00 00:00:00'),(40,'sdfjsadf@gkadfj.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-15','2013-04-15 12:52:12','0000-00-00 00:00:00'),(41,'sdfjsadf@gkadf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadf.com\",\"password\":\"fd7db3c4491f86d551d7512440b64be9\",\"confirm_password\":\"ashraf123\"}','2013-07-15','2013-04-15 12:52:45','0000-00-00 00:00:00'),(42,'sdfjsadf@gkadsfdf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadsfdf.com\",\"password\":\"45e60bbcc2fb9461ceb909c1536d304a\",\"confirm_password\":\"shoaib124\"}','2013-07-15','2013-04-15 12:53:23','0000-00-00 00:00:00'),(43,'sdfjsadf@gkadsdfsfdf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadsdfsfdf.com\",\"password\":\"45e60bbcc2fb9461ceb909c1536d304a\",\"confirm_password\":\"shoaib124\"}','2013-07-15','2013-04-15 13:44:54','0000-00-00 00:00:00'),(44,'','registration','{\"confirm_password\":\"\",\"password\":\"2c6269575b291018dc91ed7ed4158603\"}','2013-07-15','2013-04-15 17:11:55','0000-00-00 00:00:00'),(45,'tr.ephlux@gmail.com','registration','{\"first_name\":\"Tahir\",\"last_name\":\"Rasheed\",\"username\":\"tr.ephlux@gmail.com\",\"password\":\"68c91a594329677d6041c1c899c85272\"}','2013-07-17','2013-04-17 08:19:56','0000-00-00 00:00:00'),(46,'tahir_uf@hotmail.com','registration','{\"first_name\":\"Tahir\",\"last_name\":\"Rasheed\",\"username\":\"tahir_uf@hotmail.com\",\"password\":\"68c91a594329677d6041c1c899c85272\"}','2013-07-17','2013-04-17 08:24:07','0000-00-00 00:00:00'),(47,'javeed.nedian@hotmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"javeed.nedian@hotmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-17','2013-04-17 16:17:39','0000-00-00 00:00:00'),(48,'javeeed_neduet@yahoo.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"javeeed_neduet@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-17','2013-04-17 16:23:59','0000-00-00 00:00:00'),(49,'rizwan.khan@ephlux.com','registration','{\"first_name\":\"rizwan\",\"last_name\":\"khan\",\"username\":\"rizwan.khan@ephlux.com\",\"password\":\"68c91a594329677d6041c1c899c85272\",\"confirm_password\":\"12345678\"}','2013-09-26','2013-06-26 17:59:43','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tmps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_social_accounts`
--

DROP TABLE IF EXISTS `user_social_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_social_accounts` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `user_id` bigint(11) unsigned NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) default NULL,
  `image_url` varchar(255) default NULL,
  `type_id` enum('facebook','twitter','linkedin') NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `access_token_secret` varchar(255) default NULL,
  `is_valid_token` tinyint(1) NOT NULL default '1',
  `screen_name` varchar(255) NOT NULL,
  `extra` mediumtext COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') NOT NULL default 'active',
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_social_accounts`
--

LOCK TABLES `user_social_accounts` WRITE;
/*!40000 ALTER TABLE `user_social_accounts` DISABLE KEYS */;
INSERT INTO `user_social_accounts` VALUES (1,35,100001124133650,'yousuf.qureshi@ephlux.com','http://profile.ak.fbcdn.net/hprofile-ak-prn1/274101_100001124133650_382477_q.jpg','facebook','BAAHWj2l5sVQBAB1Mbp5Sn9pUIX9FZCZBBpCopPOWBUsinuGdny76MCmxwFWMraYf0SFCUbgwRtToE9lRQORn09F1WTLkIYCDhvhZBvOeqbmmJmL09663MnsJsQItdFDLKWaPQHX3jmLCwQmEQdZBnjF6GCWgEKx2ACAQaYUtlyELbnhaJbNrUrdJI8VyAJ8ZD',NULL,1,'ephlux.pwm',NULL,'active',2130706433,'2013-04-11 20:53:11','2013-04-11 20:53:11'),(2,35,84262925,NULL,'http://a0.twimg.com/profile_images/713544938/lala_peshowri_normal.jpg','twitter','84262925-3oNKnXAW0mocvtOZQHn2FEToBZAuLEdbXcHGm5TY','fIc7oNmGU1HBzLKPWJCy1JEchhgJRKhbK1Cu5hqUjVU',1,'ephlux_shahzad',NULL,'active',2130706433,'2013-04-11 21:00:09','2013-04-11 21:00:09'),(9,53,100001124133650,'yousuf.qureshi@ephlux.com','http://profile.ak.fbcdn.net/hprofile-ak-prn1/274101_100001124133650_382477_q.jpg','facebook','AAAHWj2l5sVQBACMDwvQwhq88LMEs3CkPGsx8gEgPEAdZCOEL8AZBJwyCB274pZBzP4Hkm8wo3MqUFPFIzIyiLp0tY3rT07eTUgStHzzLQZDZD',NULL,1,'ephlux.pwm',NULL,'active',2130706433,'2013-04-17 21:01:18','2013-04-17 21:01:18'),(13,55,225699082,'a6@test.com','http://a0.twimg.com/profile_images/3442078187/79033acbc1832b385b76f77b40383c70_bigger.jpeg','twitter','225699082-USF4rxtWTiWTknSBMGo3zbFefWeqznVF9aHjTr8g',NULL,1,'anj_kh3',NULL,'active',2130706433,'2013-04-18 20:23:17','2013-04-18 20:23:17');
/*!40000 ALTER TABLE `user_social_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `username` varchar(50) NOT NULL COMMENT 'username is basically email address',
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `status` enum('active','disabled','deleted','suspended') NOT NULL default 'active',
  `conversation_group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (17,'azhar','javed','javed.nedian@hotmail.com','b5ae6faf8e81146538383bd8e1be90f2',4,2130706433,'2013-03-22 19:59:10','2013-04-16 18:19:08','active',0),(20,'Shakeel','Uddin','shakeel@test.com','68c91a594329677d6041c1c899c85272',1,2130706433,'2013-04-01 12:46:52','2013-04-09 16:41:50','active',0),(21,'ahmed',NULL,'user@test.com','68c91a594329677d6041c1c899c85272',4,2130706433,'2013-04-08 15:00:07','2013-04-08 15:00:07','active',0),(31,'javed','javed','javed_neduet@yahoo.com','54b4e5b5ff0b9c146b5c696a3587ad82',4,2130706433,'2013-04-12 16:45:23','2013-04-12 16:45:23','active',0),(35,'Jason','Borne','anasanjaria@gmail.com','604a62677a8d14ad85b7bbf703b535e1',4,2130706433,'2013-04-12 17:17:32','2013-04-12 17:17:32','active',0),(53,'Ephlux','Pwm','yousuf.qureshi@ephlux.com','0212ba62093c988a2a7af82ed35bf2a5',4,2130706433,'2013-04-17 21:01:18','2013-04-17 21:01:18','active',0),(55,'John','Mike','a6@test.com','61c209e62381891077d1782e179f2230',4,2130706433,'2013-04-18 20:23:17','2013-04-18 20:23:17','active',0),(57,'Shakeel','Uddin','zuhair@test.com','68c91a594329677d6041c1c899c85272',2,2130706433,'2013-04-01 12:46:52','2013-04-09 16:41:50','active',0),(58,'rizwan','khan','rizwan.khan@ephlux.com','68c91a594329677d6041c1c899c85272',3,2130706433,'2013-06-26 18:05:10','2013-06-26 18:05:10','active',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'illvillagio'
--
/*!50003 DROP PROCEDURE IF EXISTS `addItemsToOrder` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `addItemsToOrder`( orderId INT(20),
				item_entryDateTime TEXT,
				itemId INT(20), 
				price INT(20),
				item_quantity INT(20),
				`comment` TEXT,
				itemStatus INT(1),
				tableId INT(20),
				seatNo INT(20) )
BEGIN
	INSERT INTO order_item SET
	item_id = itemId,
	order_id = orderId,
	quantity = item_quantity,
	created = item_entryDateTime,
	`status` = itemStatus,
	table_id = tableId,
	seat_no = seatNo;
	
	SELECT * FROM orders WHERE id = orderId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_content` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_content`(
										IN title VARCHAR(20),
										IN description VARCHAR(40),
										IN title_arabic nvarchar(20),
										IN description_arabic nvarchar(40),
										IN created DATETIME,
										IN modified DATETIME
										)
BEGIN

INSERT INTO `contents`
            (
             `title`,
             `description`,
             `title_arabic`,
             `description_arabic`,
             `created`,
             `modified`,
             `status`)
VALUES (
        title,
        description,
        title_arabic,
        description_arabic,
        created,
        modified,
        '1'
        );

SELECT * FROM contents WHERE contents.status = 1 AND contents.id = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_event` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_event`( 
												IN title VARCHAR(20),
												IN description VARCHAR(500),
												IN participate_description VARCHAR(500),
												IN event_time DATETIME,
												IN created DATETIME,
												IN modified DATETIME,
												IN title_arabic nvarchar(40),
												IN description_arabic nvarchar(500),
												IN participate_description_arabic nvarchar(500)												
												)
BEGIN


INSERT INTO `illvillagio`.`events`
            (
             `title`,
             `description`,
             `participate_description`,
             `event_time`,
			 `status`,
             `modified`,
             `created`,             
			 `title_arabic`,
             `description_arabic`,
             `particapte_description_arabic`
			)

VALUES (
        title,
        description,
        participate_description,
        event_time,
		'1',
        created,
        modified,
		title_arabic,
        description_arabic,
        participate_description_arabic
        );

SELECT * FROM events WHERE events.status = 1 AND events.id = LAST_INSERT_ID();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_event_images` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_event_images`(
														IN image_url VARCHAR(100),
														IN event_id VARCHAR(40),
														IN created DATETIME,
														IN modified DATETIME,
														IN title_image VARCHAR(100),
														IN title_image_arabic nvarchar(100)
														
														)
BEGIN


INSERT INTO `event_images`
            (
             `image_url`,
             `event_id`,
             `created`,
             `modified`,
			 `status`,
             `title_image`,             
             `title_image_arabic`
			)

VALUES (
        image_url,
        event_id,
        created,
        modified,
		'1',
        title_image,
        title_image_arabic
        );

SELECT * FROM event_images WHERE event_images.status = 1 AND event_images.id = LAST_INSERT_ID();






END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_feedback` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_feedback`(customerName VARCHAR(250), 
						     telephoneNo INT(20),
						     customerEmail VARCHAR(250),
						     createDateTime VARCHAR(250),
						     Address VARCHAR(250), 
						     feedBackText nvarchar(100)							 
							)
BEGIN
DECLARE ID2 INT;
DECLARE customerID INT;
DECLARE customerID_new INT;
SELECT COUNT(*) INTO ID2 FROM customer_information WHERE `name` = customerName AND `email` = customerEmail;
SELECT id INTO customerID FROM customer_information WHERE `name` = customerName AND `email` = customerEmail;
IF (ID2 > 0 ) THEN 
	INSERT INTO feedbacks SET 
	feedback_text = feedBackText, 
	customer_id = customerID, 
	created = createDateTime;
 
ELSE 
	INSERT INTO customer_information SET 
	`name` = customerName, 
	telephone_no = telephoneNo,
	`email` = customerEmail, 
	address = Address, 
	`status` = 1;
	SELECT id INTO customerID_new FROM customer_information WHERE id = LAST_INSERT_ID() AND `name` = customerName AND telephone_no = telephoneNo AND `address` = Address;
	
	INSERT INTO feedbacks SET 
	feedback_text = feedBackText,	
	customer_id = customerID_new, 
	created = createDateTime;
 
END IF;
SELECT * FROM feedbacks FeedBack WHERE FeedBack.status = 1 AND FeedBack.id = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_gallery` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_gallery`(
												IN name VARCHAR(20),
												IN image_name VARCHAR(40),
												IN status INT(20),
												IN name_arabic nvarchar(40),
												IN created DATETIME,
												IN modified DATETIME,
												IN image_caption VARCHAR(40),
												IN image_caption_arabic nvarchar(20)
												)
BEGIN

INSERT INTO `galleries`
            (
             `name`,
             `image_name`,
             `status`,
             `name_arabic`,
             `created`,
             `modified`,
             `image_caption`,
			 `image_caption_arabic`)
VALUES (
        name,
        image_name,
        status,
        name_arabic,
        created,
        modified,
        image_caption,
        image_caption_arabic
        );

SELECT * FROM galleries WHERE galleries.status = 1 AND galleries.id = LAST_INSERT_ID();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `add_reservation`(
						id bigint(20),
						NoofChildren bigint(20) ,
						no_of_oldage bigint(20) ,
						ConfirmViaEmail tinyint(1),
						email_address varchar(500) ,
						PhoneNo int(200),
						created datetime,
						modified datetime,
						`status` tinyint(3) ,						
						no_of_guest bigint(20) ,
						time_to datetime ,
						time_from datetime, 
						event_id bigint(20)
						)
BEGIN
    DECLARE ID2 INT;
    
    SELECT COUNT(*) INTO ID2 FROM reservation WHERE `id` = id AND phone_no = PhoneNo AND `email_address` = email_address;
IF (ID2 > 0 ) THEN 
INSERT INTO `reservations`
            (
             `no_of_children`,
             `no_of_oldage`,
             `confirm_via_email`,
             `email_address`,
             `phone_no`,
             `created`,
             `modified`,
             `status`,
             `no_of_guest`,
             `time_to`,
             `time_from`,
             `event_id`)
VALUES (
        no_of_children,
        no_of_oldage,
        ConfirmViaEmail,
        email_address,
        PhoneNo,
        created,
        modified,
        'status',
        no_of_guest,
        time_to,
        time_from,
        event_id);
 ELSE 
 INSERT INTO `reservations`
            (
             `no_of_children`,
             `no_of_oldage`,
             `confirm_via_email`,
             `email_address`,
             `phone_no`,
             `created`,
             `modified`,
             `status`,
             `no_of_guest`,
             `time_to`,
             `time_from`,
             `event_id`)
VALUES (
        no_of_children,
        no_of_oldage,
        confirm_via_email,
        email_address,
        phone_no,
        created,
        modified,
        'status',
        no_of_guest,
        time_to,
        time_from,
        event_id);
        
INSERT INTO `reservation_tables`
            (`id`,
             `floor_id`,
             `table_id`,
             `reservation_id`,
             `status`)
VALUES (
        floor_id,
        table_id,
        LAST_INSERT_ID(),
        `status`);
        
        
 END IF;
SELECT * FROM reservations WHERE reservations.status = 1 AND reservations.id = LAST_INSERT_ID();
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_feature` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_feature`(IN description VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
INSERT INTO features(
	features.description,
	features.ip_address,
	features.created,
	features.modified
) VALUES (
	description,
	ip_address,
	created,
	created -- created & modified at the time of insertion is same
);
SET `status` = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_group` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_group`(IN name VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into groups(
	groups.name,
	groups.ip_address,
	groups.created,
	groups.modified
) values (
	name,
	ip_address,
	created,
	created -- created & modified at the time of insertion is same
);
SET `status` = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_menu` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_menu`(IN p_name VARCHAR(250) ,IN p_imageUrl VARCHAR(500),IN p_displayOrder INT(3),IN p_status INT(3),IN p_created DATETIME, OUT `status` INT)
BEGIN
    INSERT INTO menus(
	menus.name,
	menus.image_url,
	menus.display_order,
	menus.status,
	menus.created,
	menus.modified
) VALUES (
	p_name,
	p_imageUrl,
	p_displayOrder,
	p_status,
	p_created,
	p_created
	-- created & modified at the time of insertion is same
);
SET `status` = LAST_INSERT_ID();
    
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_reservation`(
						in no_of_children BIGINT(20),
						in no_of_oldage BIGINT(20),
						in confirm_via_email TINYINT(1),
						in email_address VARCHAR(500) ,
						in phone_no INT(200),
						in created DATETIME,
						in no_of_guest BIGINT(20) ,
						in time_to DATETIME ,
						in time_from DATETIME, 
						in event_id VARCHAR(20),
						in floor_id VARCHAR(20),
						in table_id VARCHAR(20)
						)
BEGIN

INSERT INTO `reservations`
            (`no_of_children`,
             `no_of_oldage`,
             `confirm_via_email`,
             `email_address`,
             `phone_no`,
             `created`,
             `no_of_guest`,
             `time_to`,
             `time_from`, 
             `event_id`
             )
VALUES (
        no_of_children,
        no_of_oldage,
        confirm_via_email,
        email_address,
        phone_no,
        created,
       no_of_guest,
        time_to,
        time_from,
        event_id );

SELECT reservations.id FROM reservations WHERE reservations.status = 1 AND reservations.id = LAST_INSERT_ID();
  
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_reservation_table` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_reservation_table`( 
							IN `floor_id` VARCHAR(20),
							IN `table_id` VARCHAR(20),
							IN `reservation_id` BIGINT(20)
							)
BEGIN
INSERT INTO `reservation_tables`
            (
             `floor_id`,
             `table_id`,
             `reservation_id`,
			 `status`	
             )
VALUES (
        floor_id,
        table_id,
        reservation_id,
		'1'
        );
        

 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `create_user`(IN `first_name` VARCHAR(255), IN `last_name` VARCHAR(255), IN `username` VARCHAR(50), IN `password` VARCHAR(255), IN `group_id` INT(11), IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
INSERT INTO users
(
	users.first_name                  , 
	users.last_name                   , 
	users.username                    , 
	users.password                    ,
	users.group_id                    ,
	users.ip_address                  ,
	users.created	             ,
	users.modified
)
VALUES 
( 
	first_name                       , 
	last_name                        , 
	username                         ,  
	password                         ,
	group_id                         ,
	ip_address                       ,
	created                          ,
	created
) ; 
SET `status` = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deal_events` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `deal_events`(
													IN is_deal INT(10),
													IN LANG_KEY VARCHAR(10)
													)
BEGIN

IF (is_deal = 1) THEN 

IF ( LANG_KEY = 'ar' ) THEN 

SELECT Event.id,Event.title_arabic AS title ,Event.description_arabic AS description, Event.particapte_description_arabic AS participate_description,Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image_arabic AS title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.is_deal = is_deal ;

ELSE


SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status

FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.is_deal = is_deal ;

END IF;

ELSE 


IF ( LANG_KEY = 'ar' ) THEN 


SELECT Event.id,Event.title_arabic AS title ,Event.description_arabic AS description, Event.particapte_description_arabic AS participate_description,Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image_arabic AS title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.is_deal = 0 AND Event.is_promoted_home = 1 ;

ELSE

SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.is_deal = 0 AND Event.is_promoted_home = 1 ;

END IF;



END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_content` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_content`(
										IN id INT(20)		
										)
BEGIN



UPDATE contents SET contents.status='0'
WHERE contents.id = id ;


SELECT * FROM contents WHERE contents.id = id ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_event` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_event`(
													IN id VARCHAR(20)
												)
BEGIN

UPDATE events SET events.status='0'
WHERE events.id = id ;


SELECT * FROM events WHERE events.id = id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_gallery` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_gallery`( IN id INT(20) )
BEGIN


UPDATE galleries SET galleries.status='0'
WHERE galleries.id = id ;


SELECT * FROM galleries WHERE galleries.id = id ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_items` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_items`(
												IN order_id BIGINT
												)
BEGIN


UPDATE order_item SET order_item.status='0'
WHERE order_item.id = order_id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_menu` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_menu`(
												IN id BIGINT
												)
BEGIN

UPDATE items SET items.status='0'
WHERE items.id = id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_order` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_order`(
												 IN order_id BIGINT
												)
BEGIN
DECLARE OrderId  BIGINT(20);

UPDATE orders SET orders.status='0'
WHERE orders.id = order_id ;

SET OrderId = order_id;

CALL delete_items(OrderId);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_reservation`(
														IN `delete_id` bigint(20)
														)
BEGIN

DECLARE ReservationId  BIGINT(20);

UPDATE reservations SET reservations.status='0'
WHERE reservations.id = delete_id ;

SET ReservationId = delete_id;

CALL delete_reservation_table(ReservationId);
call get_reservation_by_reservation_id(ReservationId);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_reservation_table` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_reservation_table`(
															in reservation_id BIGINT(20)
															)
BEGIN

UPDATE reservation_tables SET reservation_tables.status='0'
WHERE reservation_tables.reservation_id = reservation_id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_tmp_record` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `delete_tmp_record`(IN `id` INT(5),OUT `status` INT)
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.id = id ; 
SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_content` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_content`(
										IN id INT(20),
										IN title VARCHAR(20),
										IN description VARCHAR(40),
										IN title_arabic nvarchar(20),
										IN description_arabic nvarchar(40),
										IN created DATETIME,
										IN modified DATETIME
										)
BEGIN



UPDATE contents SET contents.title=title, contents.description=description , contents.title_arabic = title_arabic , contents.description_arabic = description_arabic , contents.created = created , contents.modified = modified 
WHERE contents.id = id ;


SELECT * FROM contents WHERE contents.id = id ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_events` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_events`(
												IN id BIGINT(20),
												IN title VARCHAR(20),
												IN description VARCHAR(500),
												IN participate_description VARCHAR(500),
												IN event_time DATETIME,
												IN created DATETIME,
												IN modified DATETIME,
												IN title_arabic nvarchar(40),
												IN description_arabic nvarchar(500),
												IN participate_description_arabic nvarchar(500)
												)
BEGIN


UPDATE events SET events.id=id, events.title=title , events.description = description , events.participate_description = participate_description , events.event_time = event_time,events.status = '1' , events.created = created , events.modified = modified , events.title_arabic = title_arabic , events.description_arabic = description_arabic , events.particapte_description_arabic = participate_description_arabic 
WHERE events.id = id ;


SELECT * FROM events WHERE events.id = id ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_event_images` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_event_images`(
														
														IN image_url VARCHAR(100),
														IN event_id VARCHAR(50),
														IN created DATETIME,
														IN modified DATETIME,
														IN title_image VARCHAR(100),
														IN title_image_arabic nvarchar(100)
														
														)
BEGIN


UPDATE event_images SET event_images.id=id, event_images.image_url=image_url , event_images.event_id = event_id, event_images.status = '1' , event_images.created = created , event_images.modified = modified,event_images.title_image = title_image , event_images.title_image_arabic = title_image_arabic
WHERE event_images.event_id = event_id ;


SELECT * FROM event_images WHERE event_images.event_id = event_id ;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_gallery` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_gallery`(
												IN id VARCHAR(20),
												IN name VARCHAR(20),
												IN image_name VARCHAR(40),
												IN status INT(20),
												IN name_arabic nvarchar(40),
												IN created DATETIME,
												IN modified DATETIME,
												IN image_caption VARCHAR(40),
												IN image_caption_arabic nvarchar(20)
												
												)
BEGIN

UPDATE galleries SET galleries.id=id, galleries.name=name , galleries.image_name = image_name , galleries.status = status , galleries.name_arabic = name_arabic , galleries.created = created , galleries.modified = modified , galleries.image_caption = image_caption , galleries.image_caption_arabic = image_caption_arabic 
WHERE galleries.id = id ;


SELECT * FROM galleries WHERE galleries.id = id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_reservation`(
													   in id BIGINT(20),
													   in no_of_children BIGINT(20),
													   in no_of_oldage BIGINT(20),
													   in confirm_via_email TINYINT(1),
													   in email_address VARCHAR(500) ,
													   in phone_no INT(200),
													   in modified DATETIME,
													   in no_of_guest BIGINT(20) ,
													   in time_to DATETIME ,
													   in time_from DATETIME, 
													   in event_id BIGINT(20),
													   in floor_id BIGINT(20),
													   in table_id BIGINT(20)			
														)
BEGIN
DECLARE ReservationId  BIGINT(20);
UPDATE reservations SET reservations.no_of_children=no_of_children, reservations.no_of_oldage=no_of_oldage , reservations.confirm_via_email = confirm_via_email , reservations.email_address = email_address , reservations.phone_no = phone_no , reservations.created = created, reservations.modified = modified, reservations.status = status , reservations.no_of_guest = no_of_guest , reservations.time_to = time_to , reservations.time_from = time_from , reservations.event_id = event_id ,reservations.modified = modified
WHERE reservations.id = id ;

SELECT reservations.id FROM reservations WHERE reservations.status = 1 AND reservations.id = id ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `edit_reservation_table` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `edit_reservation_table`(
							
							IN floor_id BIGINT(20),
							IN table_id BIGINT(20),
							IN reservation_id BIGINT(20)							
							)
BEGIN

INSERT INTO `illvillagio`.`reservation_tables`
            (
             `floor_id`,
             `table_id`,
             `reservation_id`,
			 `status`	
             )
VALUES (
        floor_id,
        table_id,
        reservation_id,
		'1'
        );

SELECT reservation_tables.id FROM reservation_tables WHERE reservation_tables.status = 1 AND reservation_tables.id = reservation_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_arosacos_by_group_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_all_arosacos_by_group_id`(IN group_id INT(10))
BEGIN
SELECT 	ArosAco.id,ArosAco.aco_id
FROM groups AS `Group`
LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
LEFT JOIN `aros_acos` AS ArosAco ON ArosAco.aro_id = Aro.id
WHERE Group.id = group_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_categories_subcategories` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_all_categories_subcategories`(
													IN time DATETIME
													)
BEGIN
SELECT *  FROM items AS menu WHERE menu.is_item = 0 AND menu.lastUpdate >= time; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_menus` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_all_menus`()
BEGIN
SELECT 
	Menu.id,Menu.name,Category.id,Category.name,
	SubCategory.id,SubCategory.name
	,CategoryItem.id,CategoryItem.name
	
FROM items AS Item
LEFT JOIN items AS Menu ON (Item.lft < Menu.lft AND Item.rght > Menu.rght 
		AND Item.parent_id IS NULL AND Menu.parent_id = Item.id)
LEFT JOIN items AS Category ON (Menu.lft <= Category.lft AND Menu.rght > Category.rght 
		AND Menu.parent_id IS NOT NULL AND Category.parent_id = Menu.id)
LEFT JOIN items AS SubCategory ON (Category.lft < SubCategory.lft AND Category.rght > SubCategory.rght 
		AND SubCategory.parent_id = Category.id)
LEFT JOIN items AS CategoryItem ON (SubCategory.lft < CategoryItem.lft AND SubCategory.rght > CategoryItem.rght 
		AND SubCategory.parent_id IS NOT NULL AND CategoryItem.parent_id = SubCategory.id)
 
 
WHERE Menu.status = 1;
   END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_permissions` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_all_permissions`()
BEGIN
	SELECT 
		Child.id,Child.alias,
		AuthActionMap.description,AuthActionMap.id,
		Feature.id,Feature.description,
		Parent.id,Parent.alias
	FROM `acos` AS Child
	LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.aco_id = Child.id
	LEFT JOIN features AS Feature ON Feature.id = AuthActionMap.feature_id AND Feature.is_active = 1
	WHERE `Parent`.`parent_id` IS NOT NULL;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_content_by_key` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_content_by_key`( IN lang_key VARCHAR(20),IN modifiedDateTime DATETIME)
BEGIN	


DECLARE ID2 INT;
	SELECT COUNT(*) INTO ID2 FROM contents WHERE `modified` > modifiedDateTime;

IF (ID2 > 0) THEN 


	IF ( lang_key = 'ar' ) THEN 	
			SELECT contents.id , contents.title_arabic AS title , contents.description_arabic AS description ,contents.created ,contents.modified , contents.status, contents.key   FROM contents; 
			#SELECT * FROM content;
	ELSEIF (lang_key = 'en') THEN 

	SELECT contents.id , contents.title AS title , contents.description AS description , contents.status,contents.created ,contents.modified , contents.key   FROM contents;
		
	ELSE 

	SELECT contents.id , contents.title AS title , contents.description AS description , contents.status,contents.created ,contents.modified , contents.key   FROM contents;

	END IF;



END IF;




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_customers` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_customers`()
BEGIN

SELECT customer_information.id, customer_information.name , customer_information.telephone_no ,customer_information.email ,customer_information.address FROM customer_information;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_event` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_event`(
												IN LANG_KEY VARCHAR(20),
												IN modifiedDateTime DATETIME
												)
BEGIN
    


DECLARE ID2 INT;
	SELECT COUNT(*) INTO ID2 FROM events WHERE `modified` > modifiedDateTime;

IF (ID2 > 0) THEN 

IF ( LANG_KEY = 'ar' ) THEN 
    
    SELECT Event.id,Event.title_arabic AS title ,Event.description_arabic AS description, Event.particapte_description_arabic AS participate_description,Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image_arabic AS title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.status = 1 AND Event.is_deal = 0;

ELSEIF (LANG_KEY = 'en') THEN

 
    SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.status = 1 AND Event.is_deal = 0;


ELSE 
 
    SELECT Event.id, Event.title, Event.description, Event.participate_description,
	Event.event_time, Event.status, Event.modified, Event.event_time,Event.event_name AS name, Event.status,
	Event.modified,Event.created,
	EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status
	
FROM `events` AS Event
LEFT JOIN event_images AS EventImage ON Event.id = EventImage.event_id And EventImage.status = 1
 	
 	
WHERE Event.status = 1 AND Event.is_deal = 0;

END IF;

END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_event_image_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_event_image_id`(
													IN id BIGINT
														)
BEGIN

SELECT EventImage.id,EventImage.image_url,EventImage.title_image,EventImage.event_id,
	EventImage.created,EventImage.modified,EventImage.status 
	FROM event_images AS EventImage
	WHERE EventImage.event_id = id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_feature_by_feature_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_feature_by_feature_id`(IN feature_id INT(11))
BEGIN
select 	Feature.id, Feature.description, Feature.created
from `features` AS Feature
WHERE Feature.id = feature_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_feature_details_by_feature_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_feature_details_by_feature_id`(IN feature_id INT(11))
BEGIN
	SELECT 	
		Feature.id,Feature.description,Feature.created,
		AuthActionMap.id,AuthActionMap.description,AuthActionMap.created
	FROM features AS Feature
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.feature_id = Feature.id
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_feedback` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_feedback`()
BEGIN

SELECT * FROM feedbacks Where feedbacks.status=1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_gallery` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_gallery`(IN LANG_KEY VARCHAR(20),IN modifiedDateTime DATETIME)
BEGIN


DECLARE ID2 INT;
	SELECT COUNT(*) INTO ID2 FROM galleries WHERE `modified` > modifiedDateTime;

IF (ID2 > 0) THEN 
IF ( LANG_KEY = 'ar' ) THEN 
	SELECT 
		Gallery.id,
		Gallery.name ,
		Gallery.image_name,
		Gallery.status,
		Gallery.name_arabic ,
		Gallery.modified,
		Gallery.created,
		Gallery.image_caption,
		Gallery.image_caption_arabic
		
		
	FROM galleries AS gallery
	WHERE Gallery.status = 1 ;

ELSEIF (LANG_KEY = 'en') THEN

	SELECT 
		Gallery.id,
		Gallery.name ,
		Gallery.image_name,
		Gallery.status,
		Gallery.name_arabic ,
		Gallery.modified,
		Gallery.created,
		Gallery.image_caption,
		Gallery.image_caption_arabic
	FROM galleries AS gallery
	WHERE Gallery.status = 1 ;


ELSE

	SELECT 
		Gallery.id,
		Gallery.name ,
		Gallery.image_name,
		Gallery.status,
		Gallery.name_arabic ,
		Gallery.modified,
		Gallery.created,
		Gallery.image_caption,
		Gallery.image_caption_arabic
	FROM galleries AS gallery
	WHERE Gallery.status = 1 ;


END IF;
END IF;

    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_group_by_group_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_group_by_group_id`(IN group_id INT(11))
BEGIN
	select 	Group.id, Group.name, Group.created
	from `groups` AS `Group`
	WHERE Group.id = group_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_group_details_by_group_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_group_details_by_group_id`(IN group_id INT(11))
BEGIN
	SELECT 	
		Group.id,Group.name,Group.created,
		User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
	FROM groups AS `Group`
	LEFT JOIN users AS `User` ON User.group_id = Group.id AND User.status = 'active'
	WHERE Group.id = group_id AND Group.is_active = 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_item_price` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_item_price`(
													IN id BIGINT
													)
BEGIN

SELECT items.price FROM items WHERE items.id = id;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_menu` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_menu`()
BEGIN
select * from menus;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_reservation`()
BEGIN

SELECT Reservation.id,
	     Reservation.no_of_children,
	     Reservation.no_of_oldage,           
	     Reservation.confirm_via_email,
	     Reservation.email_address,
	     Reservation.phone_no,
	     Reservation.created,
	     Reservation.no_of_guest,
	     Reservation.time_to,           
	     Reservation.time_from, 
	     Reservation.event_id,
		 ReservationTable.id,
	     ReservationTable.floor_id,
	     ReservationTable.table_id,
	     ReservationTable.reservation_id,
	     ReservationTable.status
	         
	FROM reservations AS `Reservation`
	left join reservation_tables as ReservationTable 
		on Reservation.id=ReservationTable.reservation_id 

WHERE Reservation.status = 1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reservation_by_reservation_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_reservation_by_reservation_id`(IN ReservationId bigint(20))
BEGIN
	SELECT Reservation.id,
	     Reservation.no_of_children,
	     Reservation.no_of_oldage,           
	     Reservation.confirm_via_email,
	     Reservation.email_address,
	     Reservation.phone_no,
	     Reservation.created,
	     Reservation.no_of_guest,
	     Reservation.time_to,           
	     Reservation.time_from, 
	     Reservation.event_id,
	     ReservationTable.id,
	     ReservationTable.floor_id,
	     ReservationTable.table_id,
	     ReservationTable.reservation_id,
	     ReservationTable.status
             
	FROM reservations AS `Reservation`
	left join reservation_tables as ReservationTable 
		on Reservation.id=ReservationTable.reservation_id
	
	WHERE Reservation.id = ReservationId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reservation_table` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_reservation_table`(
															IN id BIGINT
															)
BEGIN

SELECT  reservation_tables.id, reservation_tables.floor_id, reservation_tables.table_id, reservation_tables.reservation_id, reservation_tables.status
FROM reservation_tables 
WHERE reservation_tables.reservation_id=id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_by_email` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_user_by_email`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
FROM users AS `User` 
WHERE User.username = email AND User.status = 'active'; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_details_by_user_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_user_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status , 
		
		UserSocialAccount.id, UserSocialAccount.link_id, UserSocialAccount.email_address, UserSocialAccount.image_url, 
		UserSocialAccount.type_id,UserSocialAccount.is_valid_token, UserSocialAccount.screen_name, UserSocialAccount.status, 
		UserSocialAccount.created,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN user_social_accounts AS UserSocialAccount ON UserSocialAccount.user_id = `User`.id AND UserSocialAccount.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_tmp_data` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `get_user_tmp_data`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.*
FROM tmps AS Tmp 
WHERE Tmp.target_id = target_id AND Tmp.type = type; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_test` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `insert_test`()
BEGIN
SELECT * FROM events;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `is_unverified_email_exists` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.target_id
FROM tmps AS Tmp
WHERE Tmp.target_id = email AND Tmp.type = 'registration';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `makeOrder` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `makeOrder`( order_type text, order_item_id int(20), order_item_quantity int(20), order_table_id int(20), order_seat_no INT(20), order_amount int(20), order_amount_to_be_paid int(20) )
BEGIN
declare orderId INT;
DECLARE tax INT;
set tax = 10;
	
	INSERT INTO orders SET
	order_taketime = NOW(),
	table_id = order_table_id,
	ordertype = order_type,
	order_tabbed_amount = order_amount,
	order_amount_paid = order_amount_to_be_paid;
	SELECT id INTO orderId FROM orders ORDER BY id DESC LIMIT 0,1;
	
	INSERT INTO order_item SET
	item_id = order_item_id,
	order_id = orderId,
	quantity = order_item_quantity,
	created = NOW(),
	table_id = order_table_id,
	seat_no = order_seat_no;
	
	IF ( order_type = 'delivery' ) THEN 
	
	/*SELECT 'added';*/
	
		
	INSERT INTO delivery_information SET
		order_id = orderId,
		delivery_address = 'Delivery Address',
		agent_id = 1,
		agent_name = 'Agent Name';
	/*SELECT 
		O.id, O.ordertype, O.order_taketime, O.table_id,
		OrderItem.quantity,
		Item.id, Item.name, Item.price,
		Delivery.agent_name, Delivery.delivery_address, Delivery.delivery_time, 
		(OrderItem.quantity*Item.price),
		(OrderItem.quantity*Item.price + (OrderItem.quantity*Item.price * tax)/100) AS totalamount
		
	FROM orders O
	LEFT JOIN order_item AS OrderItem ON O.id = OrderItem.order_id
	LEFT JOIN items AS Item ON Item.id = OrderItem.item_id
	LEFT JOIN delivery_information AS Delivery ON Delivery.order_id = O.id
	WHERE O.id = LAST_INSERT_ID();		
		
	ELSE
	
	SELECT 'not added';
	
	
	SELECT 
		O.id, O.ordertype, O.order_taketime, O.table_id,
		OrderItem.quantity,
		Item.id, Item.name, Item.price,
		(OrderItem.quantity*Item.price),
		(OrderItem.quantity*Item.price + (OrderItem.quantity*Item.price * 10)/100) AS totalamount
		
	FROM orders O
	LEFT JOIN order_item AS OrderItem ON O.id = OrderItem.order_id
	LEFT JOIN items AS Item ON Item.id = OrderItem.item_id
	WHERE O.id = LAST_INSERT_ID();*/
	
	END IF;	
	
SELECT * FROM orders WHERE id = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `makeOrderNew` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `makeOrderNew`( order_type TEXT, 
				 order_table_id INT(20),
				 order_entryDateTime TEXT,
				 customerName TEXT,
				 customerEmail VARCHAR(20),
				 customerTelephoneNo INT(20),
				 customerShippingAddress TEXT,
				 no_seats INT(20),
				 total_price INT(50)	
				)
BEGIN
DECLARE orderId INT;
DECLARE customerId INT;
DECLARE ID2 INT;
	SELECT COUNT(*) INTO ID2 FROM customer_information WHERE `name` = customerName AND `email` = customerEmail;
	
	IF (ID2 > 0 ) THEN 
	SELECT id INTO customerId FROM customer_information WHERE `name` = customerName AND `email` = customerEmail;
	
	ELSE
	INSERT INTO customer_information SET `name` = customerName, telephone_no = customerTelephoneNo, email = customerEmail, address = Address, `status` = 1;
	SELECT id INTO customerId FROM customer_information WHERE id = LAST_INSERT_ID();	
	
	END IF;
	INSERT INTO orders SET
	order_taketime = order_entryDateTime,
	table_id = order_table_id,
	ordertype = order_type,
	customer_id = customerId,
	no_of_seats = no_seats,
	order_amount_paid = total_price,
	order_status = 'active';
	
	SELECT LAST_INSERT_ID() AS order_id;
	
	/*SELECT orderId;*/
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remove_arosacos_by_group_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `remove_arosacos_by_group_id`(IN group_id INT(11) , OUT `status` INT)
BEGIN
	DELETE FROM `aros_acos` 
	WHERE aros_acos. aro_id = (
		SELECT 	Aro.id
		FROM groups AS `Group`
		LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
		WHERE Group.id = group_id
	);
SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remove_feature` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `remove_feature`(
	IN feature_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `features` AS `Feature` WHERE `Feature`.`id` = feature_id AND Feature.is_active = 1) THEN	
		UPDATE `features` AS Feature
		SET Feature.is_active = 0,
		Feature.modified = modified,
		Feature.ip_address = ip_address
		WHERE Feature.id = feature_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remove_group` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `remove_group`(
	IN group_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `groups` AS `Group` WHERE `Group`.`id` = group_id AND Group.is_active = 1) THEN	
		UPDATE `groups` AS `Group`
		SET Group.is_active = 0,
		Group.modified = modified,
		Group.ip_address = ip_address
		WHERE Group.id = group_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reservation_table_delete_by_reservation_id` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `reservation_table_delete_by_reservation_id`(
													IN reservation_id BIGINT
													)
BEGIN

DELETE FROM reservation_tables WHERE reservation_tables.reservation_id=reservation_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reset_password` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5), OUT `status` INT)
    NO SQL
BEGIN
    UPDATE users
    SET    
           users.password = `password`                    
    WHERE  users.id = id;
SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `save_tmp_record` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 
    INSERT INTO tmps
         (
           tmps.target_id                  , 
           tmps.type                       , 
           tmps.data                       , 
           tmps.expire_at                  ,
           tmps.created
         )
    VALUES 
         ( 
           target_id                    , 
           type                         , 
           data                         ,  
           expire_at                    ,
           created
          
         ) ; 
SET `status` = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setTableStatus` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `setTableStatus`(tableId INT, floorId INT, tableStatus TEXT)
BEGIN
	update TABLES SET 
	table_no = tableId, 
	modified = NOW(), 
	floor_id = floorId, 
	state = tableStatus
	where table_no = tableId;
	
	SELECT `status`, state FROM TABLES WHERE table_no = tableId and floor_id = floorId and state = tableStatus;	
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `signup_with_social_account` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `signup_with_social_account`(IN xml_data varchar(1000), OUT `status` INT)
BEGIN
DECLARE user_id BIGINT(11);
DECLARE first_name VARCHAR(255);
DECLARE last_name VARCHAR(255);
DECLARE username VARCHAR(50); -- username is basically email so this field can be used in user social account
DECLARE pwd VARCHAR(255);
DECLARE group_id INT(11);
DECLARE ip_address  INT(10); -- this field can be used in user social account
DECLARE created DATETIME; -- this field can be used for modified too & also in user social account
DECLARE link_id BIGINT(11); -- social account id
DECLARE image_url VARCHAR(255);
DECLARE type_id VARCHAR(20);
DECLARE access_token VARCHAR(255);
DECLARE screen_name VARCHAR(255);
SELECT ExtractValue(xml_data, '/response/User/first_name') INTO first_name;
SELECT ExtractValue(xml_data, '/response/User/last_name') INTO last_name;
SELECT ExtractValue(xml_data, '/response/User/username') INTO username;
SELECT ExtractValue(xml_data, '/response/User/password') INTO pwd;
SELECT ExtractValue(xml_data, '/response/User/group_id') INTO group_id;
SELECT ExtractValue(xml_data, '/response/User/ip_address') INTO ip_address;
SELECT ExtractValue(xml_data, '/response/User/created') INTO created;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/link_id') INTO link_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/image_url') INTO image_url;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/type_id') INTO type_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/access_token') INTO access_token;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/screen_name') INTO screen_name;
call `create_user`(first_name,last_name,username,pwd,group_id,created,ip_address,@status);
SELECT @status INTO user_id;
INSERT INTO `user_social_accounts` 
(	
	user_social_accounts.user_id,
	user_social_accounts.link_id,
	user_social_accounts.email_address,
	user_social_accounts.image_url,
	user_social_accounts.type_id,
	user_social_accounts.access_token,
	user_social_accounts.screen_name,
	user_social_accounts.ip_address,
	user_social_accounts.created,
	user_social_accounts.modified
)values (
	user_id,
	link_id,
	username,
	image_url,
	type_id,
	access_token,
	screen_name,
	ip_address,
	created,
	created
);
SET `status` = LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sync_aam_aco` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `sync_aam_aco`(OUT `status` INT)
BEGIN
DELETE FROM auth_action_maps WHERE NOT aco_id IN ( SELECT Aco.id FROM `acos` AS Aco );
SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `test_multi_sets` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateOrder` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `updateOrder`( orderId INT(20), orderItemId INT(20), orderItemQuantity INT(20), orderAmount INT(20), orderAmountToBePaid INT(20) )
BEGIN
	
	update orders set 
	order_tabbed_amount = orderAmount,
	order_amount_paid = orderAmountToBePaid
	where id = orderId;
	
	UPDATE order_item SET 
	item_id = orderItemId,
	quantity = orderItemQuantity,
	modified = NOW()
	WHERE order_id = orderId;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_feature` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `update_feature`(
	IN feature_id INT(11),
	IN description VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `features` AS Feature
	SET Feature.description = description,
	Feature.modified = modified,
	Feature.ip_address = ip_address
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
	SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_group` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `update_group`(
	IN group_id INT(11),
	IN name VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `groups` AS `Group`
	SET Group.name = name,
	Group.modified = modified,
	Group.ip_address = ip_address
	WHERE Group.id = group_id AND Group.is_active = 1;
	SET `status` = ROW_COUNT();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `verify_login` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id
FROM users AS User 
WHERE User.username = email AND User.password = pass AND User.status = 'active'; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 DROP PROCEDURE IF EXISTS `verify_reservation` */;
--
-- WARNING: old server version. The following dump may be incomplete.
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`%`*/ /*!50003 PROCEDURE `verify_reservation`( 
						 IN `floor_id` bigint(20),
						 IN `table_id` BIGINT(20),
						 in  `time_to` datetime )
BEGIN
	SELECT count(Reservation.id) AS is_allowed_reservation
		
	FROM reservations AS Reservation 
	LEFT JOIN reservation_tables AS ReservationTable 
		ON Reservation.id = ReservationTable.reservation_id
			
	WHERE time_to  between Reservation.time_to and Reservation.time_from 
		and ReservationTable.table_id = table_id 
		AND ReservationTable.floor_id = floor_id
		AND Reservation.status =1
		AND ReservationTable.status =1;
    
    
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-26 18:56:26
