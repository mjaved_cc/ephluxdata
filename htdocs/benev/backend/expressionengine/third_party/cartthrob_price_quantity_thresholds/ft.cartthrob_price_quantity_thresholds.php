<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'cartthrob/fieldtypes/ft.cartthrob_matrix'.EXT;

/**
 * @property EE_EE $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_price_quantity_thresholds_ft extends Cartthrob_matrix_ft
{
	public $info = array(
		'name' => 'CartThrob Price - Quantity',
		'version' => '1.0.0'
	);
	
	public $default_row = array(
		'from_quantity' => '',
		'up_to_quantity' => '',
		'price' => '',
	);
	
	public function pre_process($data)
	{
		$data = parent::pre_process($data);
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->load->library('number');
		
		foreach ($data as &$row)
		{
			if (isset($row['price']) && $row['price'] !== '')
			{
				if ($this->EE->cartthrob->store->config('tax_inclusive_price'))
				{
					if ($plugin = $this->EE->cartthrob->store->plugin($this->EE->cartthrob->store->config('tax_plugin')))
					{
						$row['price'] = $plugin->get_tax($row['price']) + $row['price'];
					}
				}
				
				$row['price_numeric'] = $row['price'];
				
				$row['price'] = $this->EE->number->format($row['price']);
			}
		}
		
		return $data;
	}
}

/* End of file ft.cartthrob_discount.php */
/* Location: ./system/expressionengine/third_party/cartthrob_discount/ft.cartthrob_discount.php */