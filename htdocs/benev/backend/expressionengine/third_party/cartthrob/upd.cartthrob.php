<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 */
class Cartthrob_upd
{ 
	public $version;
	public $current; 
	private $mod_actions = array(
		'delete_from_cart_action',
		'cart_action',
		'download_file_action',
		'add_to_cart_action',
		'update_cart_action',
		'add_coupon_action',
		'multi_add_to_cart_action',
		'update_live_rates_action',
		'save_customer_info_action',
		'update_item_action',
		'checkout_action',
		'payment_return_action',
	);
	
	private $mcp_actions = array(
		'save_price_modifier_presets_action',
	);
	
	private $tables = array(
		'cartthrob_sessions' => array(
			'session_id' => array(
				'type' => 'varchar',
				'constraint' => 32,
				'null' => FALSE,
				'primary_key' => TRUE,
			),
			'user_data' => array(
				'type' => 'text',
			),
			'sess_key' => array(
				'type' => 'varchar',
				'constraint' => 40,
				'default' => '',
			),
			'sess_expiration' => array(
				'type' => 'int',
				'constraint' => 11,
				'default' => 0,
			),
		),
		'cartthrob_settings' => array(
			'site_id' => array(
				'type' => 'int',
				'constraint' => 4,
				'default' => 1,
			),
			'`key`' => array(
				'type' => 'varchar',
				'constraint' => 255,
			),
			'value' => array(
				'type' => 'text',
				'null' => TRUE,
			),
			'serialized' => array(
				'type' => 'int',
				'constraint' => 1,
				'null' => TRUE,
			),
		),
		'cartthrob_order_items' => array(
			'row_id' => array(
				'type' => 'int',
				'constraint' => 10,
				'auto_increment' => TRUE,
				'primary_key' => TRUE,
			),
			'row_order' => array(
				'type' => 'int',
				'constraint' => 10,
			),
			'order_id' => array(
				'type' => 'int',
				'constraint' => 10,
			),
			'entry_id' => array(
				'type' => 'int',
				'constraint' => 10,
				'null' => TRUE,
			),
			'title' => array(
				'type' => 'varchar',
				'constraint' => 255,
				'null' => TRUE,
			),
			'quantity' => array(
				'type' => 'varchar',
				'constraint' => 10,
				'null' => TRUE,
			),
			'price' => array(
				'type' => 'varchar',
				'constraint' => 100,
				'null' => TRUE,
			),
			'weight' => array(
				'type' => 'varchar',
				'constraint' => 100,
				'null' => TRUE,
			),
			'shipping' => array(
				'type' => 'varchar',
				'constraint' => 100,
				'null' => TRUE,
			),
			'no_tax' => array(
				'type' => 'tinyint',
				'constraint' => 1,
				'null' => TRUE,
				'default' => 0,
			),
			'no_shipping' => array(
				'type' => 'tinyint',
				'constraint' => 1,
				'null' => TRUE,
				'default' => 0,
			),
			'extra' => array(
				'type' => 'text',
				'null' => TRUE,
			),
		),
	);
	
	private $fieldtypes = array(
		'cartthrob_discount',
		'cartthrob_order_items',
		'cartthrob_price_modifiers',
		'cartthrob_price_quantity_thresholds',
		'cartthrob_price_simple'
	);
     
	public function __construct()
	{ 
		$this->EE =& get_instance();
		
		include PATH_THIRD.'cartthrob/config'.EXT;
		
		$this->version = $config['version'];
	}
	
	public function install()
	{
		//install module to exp_modules
		$data = array(
			'module_name' => 'Cartthrob' ,
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);

		$this->EE->db->insert('modules', $data);
		
		//create tables from $this->tables array
		$this->EE->load->dbforge();
		
		foreach ($this->tables as $table => $fields)
		{
			$primary_key = FALSE;
			
			foreach ($fields as $field => $attributes)
			{
				if ( ! empty($attributes['primary_key']))
				{
					unset($fields[$field]['primary_key']);
					
					$primary_key = $field;
				}
			}
			
			$this->EE->dbforge->add_field($fields);
			
			if ($primary_key !== FALSE)
			{
				$this->EE->dbforge->add_key($primary_key, TRUE);
			}
			
			$this->EE->dbforge->create_table($table, TRUE);
		}
		
		//check for CartThrob actions in the database
		//so we don't get duplicates
		$this->EE->db->select('method')
				->from('actions')
				->like('class', 'Cartthrob', 'after');
		
		$existing_methods = array();
		
		foreach ($this->EE->db->get()->result() as $row)
		{
			$existing_methods[] = $row->method;
		}
		
		//install the module actions from $this->mod_actions
		foreach ($this->mod_actions as $method)
		{
			if ( ! in_array($method, $existing_methods))
			{
				$this->EE->db->insert('actions', array('class' => 'Cartthrob', 'method' => $method));
			}
		}
		
		//install the module actions from $this->mcp_actions
		foreach ($this->mcp_actions as $method)
		{
			if ( ! in_array($method, $existing_methods))
			{
				$this->EE->db->insert('actions', array('class' => 'Cartthrob_mcp', 'method' => $method));
			}
		}
		
		//install the fieldtypes
		require_once APPPATH.'fieldtypes/EE_Fieldtype'.EXT;
		
		foreach ($this->fieldtypes as $fieldtype)
		{
			require_once PATH_THIRD.$fieldtype.'/ft.'.$fieldtype.EXT;
			
			$ft = get_class_vars(ucwords($fieldtype.'_ft'));
			
			$this->EE->db->insert('fieldtypes', array(
				'name' => $fieldtype,
				'version' => $ft['info']['version'],
				'settings' => base64_encode(serialize(array())),
				'has_global_settings' => method_exists($fieldtype, 'display_global_settings') ? 'y' : 'n'
			));
		}
		
		return TRUE;
	}
	
	public function update($current = '')
	{
		$this->current = $current;
		
		if ($this->current == $this->version)
		{
			return FALSE;
		}
		
		//remove the member_member_login hook
		//update sessions database
		if ($this->older_than('2.0271')) 
		{
			$this->EE->db->delete('extensions', array('method' => 'member_member_login'));
			
			$this->EE->load->dbforge();
			
			$this->EE->dbforge->drop_column('cartthrob_sessions', 'last_activity');
			$this->EE->dbforge->drop_column('cartthrob_sessions', 'ip_address');
			$this->EE->dbforge->drop_column('cartthrob_sessions', 'member_id');
			$this->EE->dbforge->drop_column('cartthrob_sessions', 'user_agent');
			
			$this->EE->db->query("ALTER TABLE ".$this->EE->db->dbprefix('cartthrob_sessions')." MODIFY session_id varchar(32) NOT NULL");
			$this->EE->db->query("ALTER TABLE ".$this->EE->db->dbprefix('cartthrob_sessions')." ADD PRIMARY KEY (session_id)");
			
			$fields = array(
				'sess_key' => array(
					'type' => 'varchar',
					'constraint' => 40,
					'default' => '',
				),
				'sess_expiration' => array(
					'type' => 'int',
					'constraint' => 11,
					'default' => 0,
				),
			);
			
			$this->EE->dbforge->add_column('cartthrob_sessions', $fields);
		}
		
		if ($this->older_than('2.0318'))
		{
			$this->EE->load->dbforge();
			
			$this->EE->dbforge->add_field($this->tables['cartthrob_order_items']);
			
			$this->EE->dbforge->add_key('row_id', TRUE);
			
			$this->EE->dbforge->create_table('cartthrob_order_items', TRUE);
			
			$fields = $this->EE->db->select('field_id, group_id')
					       ->where('field_type', 'cartthrob_order_items')
					       ->get('channel_fields')
					       ->result();
			
			$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
			
			$this->EE->load->helper('data_formatting');
			
			foreach ($fields as $field)
			{
				$entries = $this->EE->db->select('entry_id, field_id_'.$field->field_id)
						      ->join('channels', 'channels.channel_id = channel_data.channel_id')
						      ->where('field_group', $field->group_id)
						      ->where('field_id_'.$field->field_id.' !=', '')
						      ->get('channel_data')
						      ->result();
				
				foreach ($entries as $entry)
				{
					$data = _unserialize($entry->{'field_id_'.$field->field_id}, TRUE);
					
					foreach ($data as $row_id => $row)
					{
						$insert = array(
							'order_id' => $entry->entry_id,
							'row_order' => $row_id,
						);
						
						foreach (array('entry_id', 'title', 'quantity', 'price') as $key)
						{
							$insert[$key] = (isset($row[$key])) ? $row[$key] : '';
							unset($row[$key]);
						}
						
						$insert['extra'] = (count($row) > 0) ? base64_encode(serialize($row)) : '';
						
						$this->EE->db->insert('cartthrob_order_items', $insert);
					}
					
					$this->EE->db->update('channel_data', array('field_id_'.$field->field_id => 1), array('entry_id' => $entry->entry_id));
				}
			}
		}
		
		if ($this->older_than('2.0323'))
		{
			$field = ($this->EE->db->field_exists('order_id', 'cartthrob_order_items')) ? 'order_id' : 'parent_id';
			
			$parents = $this->EE->db->select($field)
					      ->distinct()
					      ->get('cartthrob_order_items')
					      ->result();
			
			$updated_channels = array();
			
			$order_items_fields = $this->EE->db->select('site_id, value')
							  ->where('`key`', 'orders_items_field')
							  ->get('cartthrob_settings')
							  ->result();
			
			foreach ($parents as $parent)
			{
				$site_id = $this->EE->db->select('site_id')
							   ->where('entry_id', $parent->{$field})
							   ->get('channel_titles')
							   ->row('site_id');
				
				foreach ($order_items_fields as $row)
				{
					if ($site_id == $row->site_id && $row->value)
					{
						$this->EE->db->update('channel_data', array('field_id_'.$row->value => 1), array('entry_id' => $parent->{$field}));
						
						break;
					}
				}
			}
		}
		
		if ($this->older_than('2.0325'))
		{
			$this->EE->load->dbforge();
			
			if ($this->EE->db->field_exists('parent_id', 'cartthrob_order_items'))
			{
				$this->EE->dbforge->modify_column(
					'cartthrob_order_items',
					array(
						'parent_id' => array(
							'name' => 'order_id',
							'type' => 'int',
							'constraint' => 10,
						),
					)
				);
 			}
		}
		
		if ($this->older_than('2.0378'))
		{
			$this->EE->db->insert('extensions', array(
				'class' => 'Cartthrob_ext', 
				'method' => 'cp_menu_array',
				'hook' => 'cp_menu_array', 
				'settings' => '', 
				'priority' => 10, 
				'version' => $this->version,
				'enabled' => 'y',
			));
		}

		return TRUE;
	}
	
	public function uninstall()
	{
		$this->EE->db->delete('modules', array('module_name' => 'Cartthrob'));
		
		$this->EE->db->like('class', 'Cartthrob', 'after')->delete('actions');
		
		//should we do this?
		//nah, do it yourself if you really want to
		/*
		foreach (array_keys($this->tables) as $table)
		{
			$this->EE->dbforge->drop_table($table);
		}
		*/
		
		return TRUE;
	}
	
	private function older_than($version)
	{
		return version_compare($this->current, $version, '<');
	}
}

/* End of file upd.cartthrob.php */
/* Location: ./system/expressionengine/third_party/cartthrob/upd.cartthrob.php */