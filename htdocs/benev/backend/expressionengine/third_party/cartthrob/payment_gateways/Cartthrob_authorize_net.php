<?php 
class Cartthrob_authorize_net extends Cartthrob_payment_gateway
{
	public $title = 'authorize_net_title';
	public $affiliate = 'authorize_net_affiliate'; 
	public $overview = 'authorize_net_overview';
	public $language_file = TRUE;
	public $settings = array(
		array(
			'name' => 'authorize_net_settings_api_login',
			'short_name' => 'api_login',
			'type' => 'text'
		),
		array(
			'name' => 'authorize_net_settings_trans_key',
			'short_name' => 'transaction_key',
			'type' => 'text'
		),
		array(
			'name' => 'authorize_net_settings_email_customer',
			'short_name' => 'email_customer',
			'type' => 'radio',
			'default' => "no",
			'options' => array(
				"no"	=> "no",
				"yes"	=> "yes"
			)
		),
		array(
			'name' => "mode",
			'short_name' => 'mode',
			'type' => 'radio',
			'default' => "test",
			'options' => array(
				"test"	=> "test",
				"live"	=> "live",
				"developer" => "developer"
			)
		),
		array(
			'name' => 'authorize_net_settings_dev_api_login',
			'short_name' => 'dev_api_login',
			'type' => 'text'
		),
		array(
			'name' => 'authorize_net_settings_dev_trans_key',
			'short_name' => 'dev_transaction_key',
			'type' => 'text'
		)
	);
	
	public $required_fields = array(
		'credit_card_number',
		'expiration_year',
		'expiration_month'
	);
	
	public $fields = array(
		'first_name',
		'last_name',
		'address',
		'address2',
		'city',
		'state',
		'zip',
		'phone',
		'email_address',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_address',
		'shipping_address2',
		'shipping_city',
		'shipping_state',
		'shipping_zip',
		'card_type',
		'credit_card_number',
		'CVV2',
		'expiration_year',
		'expiration_month'
	);
	
	public $hidden = array();
	public $card_types = NULL;
	
	/**
	 * process_payment
	 *
 	 * @param string $credit_card_number 
	 * @return mixed | array | bool An array of error / success messages  is returned, or FALSE if all fails.
	 * @author Chris Newton
	 * @access public
	 * @since 1.0.0
	 */
	public function process_payment($credit_card_number)
	{
		$this->_x_type 			=	'AUTH_CAPTURE';
 		$this->_x_test_request         	= "TRUE";
		
		$api_login = $this->plugin_settings('api_login');
		$transaction_key = $this->plugin_settings('transaction_key');
		
		if ($this->plugin_settings('mode') == 'developer') 
		{
			$this->_host					= "https://test.authorize.net/gateway/transact.dll";
			$api_login = $this->plugin_settings('dev_api_login');
			$transaction_key = $this->plugin_settings('dev_transaction_key');
		}
		elseif ($this->plugin_settings('mode') == "test") 
		{
			$this->_host					= "https://secure.authorize.net/gateway/transact.dll";
 		}
		else
		{
			$this->_host					= "https://secure.authorize.net/gateway/transact.dll";
			$this->_x_test_request         	= "FALSE";
			
		}
 		
 
 		$post_array = array(
			"x_login"         				=> $api_login,
			"x_tran_key"           			=> $transaction_key,
			"x_version"           	 		=> "3.1",
			"x_test_request"    		   	=> $this->_x_test_request,
			"x_delim_data"    	    	 	=> "TRUE",
			"x_relay_response"				=> "FALSE",
			"x_first_name"       	     	=> $this->order('first_name'),
			"x_last_name"       	     	=> $this->order('last_name'),
			"x_address"      		      	=> $this->order('address')." ".$this->order('address2'),
			"x_city"            	    	=> $this->order('city'),
			"x_state"              		  	=> $this->order('state'),
			"x_description"					=> $this->order('description'),
			"x_zip"            		    	=> $this->order('zip'),
			"x_country"            		   	=> $this->alpha2_country_code(($this->order('country_code') ? $this->order('country_code') : 'USA')),
			'x_ship_to_first_name'			=> ($this->order('shipping_first_name')) ? $this->order('shipping_first_name') : $this->order('first_name'),
			'x_ship_to_last_name'			=> ($this->order('shipping_last_name')) ? $this->order('shipping_last_name') : $this->order('last_name'),
			'x_ship_to_address'				=> ($this->order('shipping_address')) ? $this->order('shipping_address').' '.$this->order('shipping_address2') : $this->order('address').' '.$this->order('address2'),
			'x_ship_to_city'				=> ($this->order('shipping_city')) ? $this->order('shipping_city') : $this->order('city'),
			'x_ship_to_state'				=> ($this->order('shipping_state')) ? $this->order('shipping_state') : $this->order('state'),
			'x_ship_to_zip'					=> ($this->order('shipping_zip')) ? $this->order('shipping_zip') : $this->order('zip'),
			"x_phone"          		      	=> $this->order('phone'),
			"x_email"          		      	=> $this->order('email_address'),
			"x_cust_id"          		   	=> $this->order('member_id'),
			"x_invoice_num"					=> time().strtoupper(substr($this->order('last_name'), 0, 3)),
			"x_company"						=> $this->order('company'),
			"x_email_customer"    		 	=> ($this->plugin_settings('email_customer') == "yes") ? "TRUE" : "FALSE",
			"x_amount"               	 	=> number_format($this->total(),2,'.',''),
			"x_method"               	 	=> "CC",
			"x_type"                 		=> $this->_x_type,  // set to AUTH_CAPTURE for money capturing transactions
			"x_card_num"             		=> $credit_card_number,
			"x_card_code"             		=> $this->order('CVV2'),
			"x_exp_date"             		=> str_pad($this->order('expiration_month'), 2, '0', STR_PAD_LEFT).'/'.$this->year_2($this->order('expiration_year')),
			"x_tax"							=> $this->order('tax'),
			"x_freight"						=> $this->order('shipping'),
		);
	
		reset($post_array);
		$data='';
		while (list ($key, $val) = each($post_array)) 
		{
			$data .= $key . "=" . urlencode($val) . "&";
		}
		
		// SENDING ORDER DATA TO AUTHORIZE.NET
		$line_item = array();
		
		if ($this->order('items'))
		{
			foreach ($this->order('items') as $row_id => $item)
			{
				$basket = ""; 
				
				if (!isset($count))
				{
					$count=1;
				}
				$count++;
				if ($count > 30)
				{
					continue; 
				}
	
				$title = substr($item['title'], 0, 30); 
				
				while (strlen(urlencode(htmlspecialchars($title))) > 30)
				{
					$title = substr($title, 0, -1); 
				}
				if (empty($item['entry_id']))
				{
					$item['entry_id'] = "000";
				}
				$basket .= $item['entry_id']."<|>"; 
				$basket .= urlencode(htmlspecialchars($title))."<|>";
				$basket .= $item['entry_id']."<|>";
				$basket .= abs($item['quantity'])."<|>";
				$basket .= number_format(abs($item['price']),2,'.','')."<|>"; 
				$basket .="Y";
				$line_item[] = $basket; 
			}
		}
		
		// ADDING TO EXISTING DATA STRING. 
		while (list($key, $val) = each($line_item)) 
		{
			$data .= 'x_line_item=' .$val.'&';
		}
		
		$data .= 'x_duty=0';
		
		$auth['authorized']	 	= FALSE; 
		$auth['declined'] 		= FALSE; 
		$auth['transaction_id']	= NULL;
		$auth['failed']			= TRUE; 
		$auth['error_message']	= "";
		
 		$ch = curl_init($this->_host);
		curl_setopt($ch, CURLOPT_HEADER, 0); 		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		$connect = curl_exec($ch); 
 		
  		if (!$connect)
		{
 			$auth['error_message'] = $this->lang('curl_gateway_failure')." ".  curl_error($ch);
 
			return $auth; 
		}
		
		curl_close ($ch);
		
		$response = explode(",",$connect);
		
		
		switch($response[0])
		{
			case 1: 
				$auth['authorized'] 	= TRUE; 
				$auth['failed']			= FALSE; 
				$auth['transaction_id'] = @$response[6];
			break;
			case 2:
				$auth['authorized']	 	= FALSE; 
				$auth['declined'] 		= TRUE; 
				$auth['transaction_id']	= NULL;
				$auth['failed']			= FALSE; 
				$auth['error_message']	= @$response[3];
			break;
			case 3: 
			case 4: 
				$auth['authorized']	 	= FALSE; 
				$auth['declined'] 		= FALSE; 
				$auth['transaction_id']	= NULL;
				$auth['failed']			= TRUE; 
				$auth['error_message']	= @$response[3];
			break;
			default:
				$auth['failed']			= TRUE; 
				$auth['error_message']	= $this->lang('authorize_net_error_1') . $response[0];
		}
		return $auth;
	}
	// END Auth
}
// END Class