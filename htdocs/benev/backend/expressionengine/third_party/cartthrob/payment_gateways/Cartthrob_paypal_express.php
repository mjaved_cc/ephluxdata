<?php 
class Cartthrob_paypal_express extends Cartthrob_payment_gateway
{
	public $title = 'paypal_express_title';
	public $affiliate = 'paypal_express_affiliate'; 
	public $overview = "paypal_express_overview"; 
	public $settings = array(
		array(
			'name' =>  'paypal_express_api_username',
			'short_name' => 'api_username', 
			'type' => 'text', 
			'default' => '', 
		),
		array(
			'name' =>  'paypal_express_api_password',
			'short_name' => 'api_password', 
			'type' => 'text', 
			'default' => '', 
		),
		array(
			'name' =>  'paypal_express_signature',
			'short_name' => 'api_signature', 
			'type' => 'text', 
			'default' => '', 
		),
		
		array(
			'name' => "paypal_express_sandbox_api_username",
			'short_name' => 'test_username', 
			'type' => 'text', 
			'default' => '', 
		),
		array(
			'name' => "paypal_express_sandbox_api_password",
			'short_name' => 'test_password', 
			'type' => 'text', 
			'default' => '', 
		),
		array(
			'name' => "paypal_express_sandbox_signature",
			'short_name' => 'test_signature', 
			'type' => 'text', 
			'default' => '', 
		),
		array(
			'name' => "paypal_express_payment_action",
			'short_name' => 'payment_action', 
			'type' => 'radio', 
			'default' => 'Sale',
			'options'	=> array(
				'Sale' => 'sale',
				'Authorization' => 'authorization'
				) 
		),
		
		array(
			'name' =>  'mode',
			'short_name' => 'mode', 
			'type' => 'radio', 
			'default' => 'test',
			'options' => array(
					'test'=> "sandbox",
					'live'=> "live"
				),
		),
 	);
	
	public $required_fields = array(
	);
	
	public $fields = array(
		'first_name',
		'last_name',
		'address',
		'address2',
		'city',
		'state',
		'zip',
		'country_code',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_address',
		'shipping_address2',
		'shipping_city',
		'shipping_state',
		'shipping_zip',
		'shipping_country_code',
		'phone',
		'email_address',
		);
	public $hidden = array('currency_code'); 
 	public $paypal_server; 
	public $API_UserName; 
	public $API_Password; 
	public $API_Signature; 
	public $paypal_offsite; 

	public function initialize()
	{
		if ($this->plugin_settings('mode') == "test") 
		{
			// Sandbox server for use with API signatures;use for testing your API
			//$this->_paypal_server = "https://api-3t.sandbox.paypal.com/nvp"; 
			$this->paypal_server = "https://api.sandbox.paypal.com/nvp"; 
			$this->API_UserName = urlencode($this->plugin_settings('test_username'));
			$this->API_Password = urlencode($this->plugin_settings('test_password'));
			$this->API_Signature = urlencode($this->plugin_settings('test_signature'));
			$this->paypal_offsite = "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=";
			
		}
		else
		{
			// PayPal "live" production server for usewith API signatures
			$this->paypal_server = "https://api-3t.paypal.com/nvp"; 
			$this->paypal_offsite = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";
			$this->API_UserName = urlencode($this->plugin_settings('api_username'));
			$this->API_Password = urlencode($this->plugin_settings('api_password'));
			$this->API_Signature = urlencode($this->plugin_settings('api_signature'));
		}
	}
 	/**
	 * process_payment
	 *
	 * @param string $credit_card_number 
	 * @return mixed | array | bool An array of error / success messages  is returned, or FALSE if all fails.
	 * @author Chris Newton
	 * @access private
	 * @since 1.0.0
	 */
	public function process_payment($credit_card_number)
	{
		$resp['authorized'] 	= 	FALSE; 
		$resp['declined']		=	FALSE; 
		$resp['failed']			=	FALSE; 
		$resp['error_message']	= 	NULL; 
		$resp['transaction_id']	=	NULL;
		
		$post_array = array(
			'METHOD'								=> 'SetExpressCheckout',
			'VERSION'								=> urlencode("63.0"),
			'PWD'  									=> $this->API_Password,
			'USER' 									=> $this->API_UserName,
			'SIGNATURE'								=> $this->API_Signature,
			'PAYMENTREQUEST_0_AMT'					=> $this->order('total'), 
			'PAYMENTREQUEST_0_PAYMENTACTION'		=> $this->plugin_settings('payment_action'),
			'RETURNURL'								=> $this->get_notify_url(ucfirst(get_class($this)),'confirm_payment'),
			'CANCELURL'								=> $this->get_notify_url(ucfirst(get_class($this)),'cancel_payment') ,
			'PAYMENTREQUEST_0_CURRENCYCODE'			=> ($this->order('currency_code') ? $this->order('currency_code') : "USD"),
			'ADDROVERRIDE'							=> 1,
			'PAYMENTREQUEST_0_SHIPTONAME'			=> ($this->order('shipping_first_name') 		? $this->order('shipping_first_name') . " ". $this->order('shipping_last_name') : $this->order('first_name') ." ". $this->order('last_name')),
			'PAYMENTREQUEST_0_SHIPTOSTREET'			=> ($this->order('shipping_address') 			? $this->order('shipping_address') : $this->order('address')),
			'PAYMENTREQUEST_0_SHIPTOSTREET2'		=> ($this->order('shipping_address2') 			? $this->order('shipping_address2') : $this->order('address2')),
			'PAYMENTREQUEST_0_SHIPTOCITY'			=> ($this->order('shipping_city') 				? $this->order('shipping_city') : $this->order('city')),
			'PAYMENTREQUEST_0_SHIPTOSTATE'			=> ($this->order('shipping_state')				? strtoupper($this->order('shipping_state')) : strtoupper($this->order('state'))),
			'PAYMENTREQUEST_0_SHIPTOZIP'			=> ($this->order('shipping_zip') 				? $this->order('shipping_zip') : $this->order('zip')),                                                                           
			'PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE'	=> ($this->order('shipping_country_code') 		? $this->alpha2_country_code($this->order('shipping_country_code')) : $this->alpha2_country_code($this->order('country_code'))),
			'ALLOWNOTE'								=> 0,
			'NOSHIPPING'							=> 1
			
		);

		$data = 	$this->data_array_to_string($post_array);
		$connect = 	$this->curl_post($this->paypal_server,$data); 
		if (!$connect)
		{
			exit($this->lang('curl_gateway_failure'));
		}
		$transaction =  $this->split_url_string($connect);
 		$token =""; 
 
		if (is_array($transaction))
		{
			if ("SUCCESS" == strtoupper($transaction['ACK']) || "SUCCESSWITHWARNING" == strtoupper($transaction["ACK"])) 
			{
				$token = urldecode($transaction["TOKEN"]);
					
			} 
			else  
			{  
				if (!empty($transaction['L_LONGMESSAGE0']))
				{
					$auth['error_message']	=$transaction['L_LONGMESSAGE0']; 
				}
				return $auth; 
			}
		}
		else
		{
			$auth['error_message']	= $this->lang('paypal_express_did_not_respond') ;
			return $auth;
		}
		
		$this->gateway_exit_offsite(NULL, $this->paypal_offsite.$token.'&useraction=commit'); exit;
	}// END
	function cancel_payment($post)
	{
		$auth = array(
			'authorized' 	=> FALSE,
			'error_message'	=> $this->lang('paypal_express_you_cancelled'),  
			'failed'		=> TRUE,
			'declined'		=> FALSE,
			'transaction_id'=> NULL, 
			);
		$this->gateway_order_update($auth, $this->order('entry_id'), $this->order('return')); 	
		exit;
	}
	
	function confirm_payment($post)
	{
		$auth = array(
			'authorized' 	=> FALSE,
			'error_message'	=> NULL,
			'failed'		=> TRUE,
			'declined'		=> FALSE,
			'transaction_id'=> NULL, 
			);
		
		$post_array = array(
			'METHOD'	=> 'GetExpressCheckoutDetails',
			'VERSION'	=> urlencode("63.0"),
			'PWD'		=> $this->API_Password,
			'USER'		=> $this->API_UserName,
			'SIGNATURE'	=> $this->API_Signature,
			'TOKEN'		=> $post['token'],
			);
		
		$data = 	$this->data_array_to_string($post_array);
		$connect = 	$this->curl_transaction($this->paypal_server,$data); 
		if (!$connect)
		{
			exit( $this->lang('curl_gateway_failure'));
		}
		
		$transaction =  $this->split_url_string($connect);
 		$payer_id = NULL; 
		if (is_array($transaction))
		{
			if ("SUCCESS" == strtoupper($transaction['ACK']) || "SUCCESSWITHWARNING" == strtoupper($transaction["ACK"])) 
			{
				$payer_id =	$transaction['PAYERID'];
			} 
			else  
			{
				if (!empty($transaction['L_LONGMESSAGE0']))
				{
					$auth['error_message']	=$transaction['L_LONGMESSAGE0']; 
				}
				$this->gateway_order_update($auth, $this->order('entry_id'),$this->order('return')); 	
				exit;
			}
		}
		else
		{
			$auth['error_message']	= $this->lang('paypal_express_did_not_respond') ;
			$this->gateway_order_update($auth, $this->order('entry_id'),$this->order('return')); 	
			exit;
		}
		
		
		$post_array = array(
			'METHOD'					=> 'DoExpressCheckoutPayment',
			'VERSION'					=> urlencode("63.0"),
			'PWD'						=> $this->API_Password,
			'USER'						=> $this->API_UserName,
			'SIGNATURE'					=> $this->API_Signature,
			'PAYMENTREQUEST_0_AMT'		=> $this->order('total'), 
			'PAYMENTREQUEST_0_PAYMENTACTION'	=> $this->plugin_settings('payment_action'),
			'PAYMENTREQUEST_0_CURRENCYCODE'	=> ($this->order('currency_code') ? $this->order('currency_code'): "USD"),
			'TOKEN'						=> $post['token'], 
			'PAYERID'					=> $payer_id,
			'IPADDRESS'					=> $_SERVER['SERVER_NAME']
			);
		
		$data = 	$this->data_array_to_string($post_array);
		$connect = 	$this->curl_transaction($this->paypal_server,$data); 
		
		if (!$connect)
		{
			exit( $this->lang('curl_gateway_failure'));
		}
		$transaction =  $this->split_url_string($connect);
		
		if (is_array($transaction))
		{
			if ("SUCCESS" == strtoupper($transaction['ACK']) || "SUCCESSWITHWARNING" == strtoupper($transaction["ACK"])) 
			{
				$auth = array(
					'authorized' 	=> TRUE,
					'error_message'	=> NULL,
					'failed'		=> FALSE,
					'declined'		=> FALSE,
					'transaction_id'=> $transaction['PAYMENTINFO_0_TRANSACTIONID'], 
	 				);
			} 
			else  
			{
				if (!empty($transaction['L_LONGMESSAGE0']))
				{
					$auth['error_message']	=$transaction['L_LONGMESSAGE0']; 
				}
			}
		}
		else
		{
			$auth['error_message']	= $this->lang('paypal_express_did_not_respond') ;
		}
		$this->gateway_order_update($auth, $this->order('entry_id'), $this->order('return')); 	
		exit;
	}
	
}// END Class