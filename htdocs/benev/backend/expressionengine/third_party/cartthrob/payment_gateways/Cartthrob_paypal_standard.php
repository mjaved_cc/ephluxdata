<?php 
class Cartthrob_paypal_standard extends Cartthrob_payment_gateway
{
	public $title = 'paypal_title';
	public $affiliate = 'paypal_affiliate'; 
	public $overview = 'paypal_standard_overview';
	public $language_file = TRUE;
 	public $settings = array(
		array(
			'name' => 'paypal_settings_id', 
			'short_name' => 'paypal_id', 
			'type' => 'text' 
		),
		array(
			'name' =>  'paypal_sandbox_settings_id', 
			'short_name' => 'paypal_sandbox_id', 
			'type' => 'text'
		),
		array(
			'name' => 'mode', 
			'short_name' => 'test_mode', 
			'type' => 'radio',
			'default' => 'sandbox',
			'options' => array(
				'sandbox' => 'sandbox',
				'live' => 'live'
			)
		),
		array(
			'name' => "paypal_address_override", 
			'short_name' => 'address_override', 
			'type' => 'radio',
			'default' => 'no', 
			'options' => array(
				'no' => 'no', 
				'yes' => 'yes'
			)
		),
		array(
			'name' => "paypal_order_completion", 
			'short_name' => 'completion_event', 
			'type' => 'radio',
			'default' => 'immediate',
			'options' => array(
				'leaves'	=> 'paypal_customer_leaves',
				'return' => 'paypal_customer_returns',
				'immediate' => 'paypal_with_trans_id', 
				'pending'	=> 'paypal_with_ipn_pending_update',
				'ipn' => 'paypal_with_ipn_update'
			)
		), 
		array(
			'name' => "Log Errors to File", 
			'short_name' => 'log_errors', 
			'type' => 'radio',
			'default' => 'no',
			'options' => array('no' => 'no', 'yes' => 'yes') 
		)
	);
	
	public $required_fields = array();
	
	public $fields = array(
		'first_name',
		'last_name',
		'address',
		'address2',
		'city',
		'state',
		'zip',
		'country_code',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_address',
		'shipping_address2',
		'shipping_city',
		'shipping_state',
		'shipping_zip',
		'shipping_country_code',
		'phone',
		'email_address',
	);

	public $hidden = array('currency_code'); 
	
	public function initialize()
	{
		if ($this->plugin_settings('test_mode') == 'live')
		{
			$this->_host = 'https://www.paypal.com/cgi-bin/webscr';
			$this->_id = $this->plugin_settings('paypal_id'); 
		}
		else
		{
			$this->_host = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
			$this->_id = $this->plugin_settings('paypal_sandbox_id');
		}
	}
	/**
	 * process_payment
	 *
 	 * @param string $credit_card_number 
	 * @return mixed | array | bool An array of error / success messages  is returned, or FALSE if all fails.
	 * @author Chris Newton
	 * @access public
	 * @since 1.0.0
	 */
	public function process_payment($credit_card_number)
	{
		// Formats phone number into chunks. 
		$phone = $this->get_formatted_phone($this->order('phone'));
		
		$currency_code = ($this->order('currency_code')) ? $this->order('currency_code') : $this->config('number_format_defaults_currency_code');
		
		$post_array = array(
			'cmd'					=> '_cart',
			'address_override'		=> ($this->plugin_settings('address_override')=="no" ? 1 : 0),  
			'upload'				=> '1', // so we can send all items at once.
			'business'				=> $this->_id,
			'weight_unit'			=> ($this->order('weight_unit') ? $this->order('weight_unit') : "lbs" ), 
			'currency_code'			=> $this->order('currency_code'), //PAYPAL's default is USD.  
			'discount_amount_cart'	=> $this->order('discount'),
			'address1'				=> $this->order('address'),
			'address2'				=> $this->order('address2'),
			'city'					=> $this->order('city'),
			'country'				=> $this->alpha2_country_code($this->order('country_code')),
			'email'					=> $this->order('email_address'),
			'first_name'			=> $this->order('first_name'),
			'last_name'				=> $this->order('last_name'),
			'lc'					=> $this->get_language_abbrev($this->order('language')),
			'night_phone_a'			=> $phone['area_code'],
			'night_phone_b'			=> $phone['prefix'],
			'night_phone_c'			=> $phone['suffix'],
			'state'					=> $this->order('state'),
			'zip'					=> $this->order('zip'),
			'rm'					=> '2',
			'invoice'				=> $this->order('entry_id'),   
			'custom'				=> session_id()
			//'shipping_1'			=> $this->order('shipping'),
			//'tax_cart'			=> $this->order('tax'),
			);
		$this->log($post_array['invoice']); 
		
			
		if(empty($post_array['country']))
		{
			$post_array['country'] = "US"; 
		}
		if(empty($post_array['currency_code']))
		{
			$post_array['currency_code'] = "USD"; 
		}
		
		// removing ampersands... because paypal will send them back in the IPN. This isn't 
		// the only place we pay attention to &, but it's a good start
		foreach ($post_array as $key=>$item)
		{
			if (strstr($item, '&'))
			{
				$post_array[$key] = str_replace("&" , "and", $item);
			}
		}
		// setting these separately because they need to maintain their ampersands... otherwise stripped out above
		$post_array['notify_url']		= $this->get_notify_url(ucfirst(get_class($this)),'paypal_incoming_payment'); 
		$post_array['cancel_return']	= $this->get_notify_url(ucfirst(get_class($this)),'paypal_success'); 
		$post_array['return']			= $this->get_notify_url(ucfirst(get_class($this)),'paypal_success'); 
		
		// removing any blank array keys
		foreach ($post_array as $key=>$item)
		{
			if ($item=="")
			{
				unset ($post_array[$key]);
			}
		}
 	

		/*
		$post_array["item_name_1"]	= $this->lang('paypal_cart_default_title');
		
		$post_array["amount_1"]		= $this->order('subtotal');
		
		// Paypal doesn't allow discount amounts greater than the total of items (tax and shipping aren't included)
		// so we just won't send tax and shipping
		if ($this->order('discount') > $this->order('subtotal'))
		{
			unset($post_array['tax_cart']); 
			unset($post_array['shipping_1']); 
			unset($post_array['discount_amount_cart']); 
			$post_array['amount_1']  = $this->order('total');
 		}
		*/
		
		
		// Paypal doesn't allow discount amounts greater than the total of items (tax and shipping aren't included)
		// so we just won't send tax and shipping
		if ($this->order('discount') > $this->order('subtotal'))
		{
			$post_array['amount_1']  = $this->order('total');
 		}
		else
		{
			foreach ($this->order('items') as $row_id => $item)
			{
				if (!isset($count))
				{
					$count=0;
				}
				$count++;
				$post_array["weight_".$count] 		= $item['weight']; 
				$post_array["item_name_".$count]	= $item['title']; 
				$post_array["quantity_".$count]	 	= $item['quantity']; 
				$post_array["amount_".$count]		= $item['price'];
			}
 
			if ($this->order('shipping'))
			{
				$count ++; 
				$post_array["weight_".$count] 		= 0; 
				$post_array["item_name_".$count]	= "Shipping"; 
				$post_array["quantity_".$count]	 	= 1; 
				$post_array["amount_".$count]		= $this->order('shipping');
			}

			if ($this->order('tax'))
			{
				$count ++; 
				$post_array["weight_".$count] 		= 0; 
				$post_array["item_name_".$count]	= "Taxes"; 
				$post_array["quantity_".$count]	 	= 1; 
				$post_array["amount_".$count]		= $this->order('tax');
			}
			
		}
 		
		
		// **************************************************************************************** // 
		if ($this->plugin_settings('completion_event') == "leaves")
		{
			$auth = array(
				'authorized' 	=> TRUE,
				'error_message'	=> NULL,
				'failed'		=> FALSE,
				'declined'		=> FALSE,
				'transaction_id'	=> $this->order('entry_id')
				);
			$this->gateway_order_update($auth, $this->order('entry_id')); 	
		}
		
		$this->gateway_exit_offsite($post_array, $this->_host);
		exit;
	}
	// END
	/**
	 * paypal_success
	 *
	 * This is a maintenance and redirection function for paypal
	 * Order status is not updated until the process_incoming_payment function is fired 
	 * This function primarily does temporary cleanup in anticipation for a successful 
	 * response from the Payapl IPN. 
	 * 
	 * @param array $post 
	 * @return void
	 * @author Chris Newton
	 */
	public function paypal_success($post)
	{
		//@TODO need to check for whether this has already had an email sent for it.
		//@TODO need to check whether this has been updated yet... and not update it again. 
		// the problem is that there IS a chance that Paypal can update via IPN BEFORE the customer returns.
		// impossible I know... but seriously it happens. 
		
		// immediate has a transaction id
		if ($this->plugin_settings('completion_event') == "immediate")
		{
			if (!empty($post['tx']))
			{
				$auth  = array(
					'authorized' 	=> TRUE,
					'error_message'	=> NULL,
					'failed'		=> FALSE,
					'declined'		=> FALSE,
					'transaction_id'=> $post['tx'] 
					);
					
					$this->clear_cart();
			}
			else
			{
				$auth = array(
					'authorized' 	=> FALSE,
					'error_message'	=>  $this->lang('paypal_incomplete'),
					'failed'		=> TRUE,
					'declined'		=> FALSE,
					'transaction_id'=> NULL
					);
			}
		}
		// this is nearly indescriminate. If the customer returns... SUCCESS!
		elseif ($this->plugin_settings('completion_event') == "return")
		{
			$auth = array(
				'authorized' 	=> TRUE,
				'error_message'	=> NULL,
				'failed'		=> FALSE,
				'declined'		=> FALSE,
				);
			if (!empty($post['tx']))
			{
				$auth['transaction_id'] = $post['tx']; 
			}
			$this->clear_cart();
		}
		// waiting for the IPN
		else
		{
			if (!empty($post['tx']))
			{
				$auth = array(
					'authorized' 	=> FALSE,
					'error_message'	=> "",
					'failed'		=> FALSE,
					'processing' 	=> TRUE,
					'declined'		=> FALSE,
					'transaction_id'=> $post['tx'] 
					);
				
			}
			else
			{
				$auth  = array(
					'authorized' 	=> FALSE,
					'error_message'	=>  $this->lang('paypal_incomplete_ipn'),
					'failed'		=> FALSE,
					'processing' 	=> TRUE,
					'declined'		=> FALSE,
					'transaction_id'=> NULL
					);	
			}
			
			$this->clear_cart();
			
		}
 		$this->gateway_order_update($auth, $this->order('entry_id'), $this->order('return')); 	

		// we don't want to go through the normal checkout_complete function, so we exit here. 
		exit; 
	} // END
	
	public function paypal_incoming_payment($post)
	{
		if (empty($post))
		{
			@header("HTTP/1.0 404 Not Found");
	        @header("HTTP/1.1 404 Not Found");
	        exit('No Data Sent');
		}

		if (!array_key_exists('custom', $post) OR !$post['custom'])
		{
			$this->log("information was not successfully transmitted from paypal ");
			exit; 
		}
		else
		{
			$this->relaunch_session($post['custom']);
		}
		if ($this->order('entry_id'))
		{
			$order_id = $this->order('entry_id'); 
		}
		else
		{
			$order_id = $post['invoice']; 
		}
			

		$auth = array(
			'authorized' 	=> FALSE,
			'error_message'	=> NULL,
			'failed'		=> TRUE,
			'declined'		=> FALSE,
			'transaction_id'=> NULL, 
			'processing'	=> FALSE
			); 
		
		// The return response to Paypal must contain all of the data of the original
		// with the addition of the notify-validate command
		$post['cmd'] = '_notify-validate';
		
		$data = $this->data_array_to_string($post);

		// Fix for multi-line data
		// Thanks to Dom.S for finding a cure for this multi-line issue.  
		// and to me for finally figuring out I needed to move this AFTER I urlencoded the data
		// ***** facepalm ******
		$data = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',  $data);
		
		// RESULT will either contain VERIFIED or INVALID
		$result = $this->curl_transaction($this->_host,$data);
 		
 		$this->log("paypal result: {$result}");
		$this->log("order: ". $this->order('order_id')); 
 		
		if (stristr($result, 'VERIFIED'))
    	{
			$this->log("paypal: valid");
	
			// we don't want paypal info for ID's that we're not using, and we don't want information about unfinished transactions
			if ($this->plugin_settings('paypal_id') != trim($post['receiver_email']))
    		{
				$auth['error_message']	.= $this->lang('paypal_incorrect_id'); 
    		}


			switch( strtolower(trim($post['payment_status'])) )
			{
				case "created": 
				case "completed":
				case "processed": //masspay
					$auth = array(
						'authorized' 	=> TRUE,
						'error_message'	=> NULL,
						'failed'		=> FALSE,
						'declined'		=> FALSE,
						'transaction_id'=> $post['txn_id'], 
						);
					break;
				case "canceled_reversal": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "cancelled reversal ". $post['reason_code'],
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL
						);
					break;
				case "denied": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "denied",
						'failed'		=> FALSE,
						'declined'		=> TRUE,
						'transaction_id'=> NULL
						);
					break;
				case "failed": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "failed",
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL
						);
					break;
 				case "expired": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "expired",
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL
						);
					break;
				case "voided":
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "voided",
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL,
						);
					break;
				case "refunded": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "refunded". $post['reason_code'],
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL,
						);
					break;
				case "reversed": 
					$auth = array(
						'authorized' 	=> FALSE,
						'error_message'	=> "reversal " . $post['reason_code'],
						'failed'		=> TRUE,
						'declined'		=> FALSE,
						'transaction_id'=> NULL,
						);
					break;
				default: 
					if( $this->plugin_settings('completion_event') == "pending" )
					{
						$this->log("payment_status: pending"); 
						$auth = array(
							'authorized' 	=> TRUE,
							'error_message'	=> "Pending reason: ".$post['pending_reason'],
							'failed'		=> FALSE,
							'declined'		=> FALSE,
							'processing'	=> FALSE,
							'transaction_id'=> $post['txn_id'], 
							);
					}
					else
					{
						$auth = array(
							'authorized' 	=> FALSE,
							'error_message'	=> "Pending reason: ".$post['pending_reason'],
							'failed'		=> FALSE,
							'declined'		=> FALSE,
							'processing'	=> TRUE,
							'transaction_id'=> $post['txn_id'], 
							);
 
						$this->log("payment_status". $post['payment_status']); 
						$this->log("paypal: status incomplete");
					}
					
			}
 			
		}
		elseif (stristr($result, 'INVALID'))
		{	
			$this->log("paypal: invalid");
			
			if ($this->plugin_settings('log_errors')== "yes")
			{
				// LOGGING ///////////////////////////////////
				// this folder has to be writeable
				// the default is one folder above index.php
				if ($logfile = fopen("../paypal_problems.log", "a"))
				{
					fwrite($logfile, sprintf("\r%s:- %s",date("D M j G:i:s T Y"), $_SERVER["REQUEST_URI"] ));
					foreach ($_POST as $key=>$item)
					{
						$this->log("paypal post-{$key}: {$item}");
						
						fwrite($logfile, sprintf("\r%s:- %s",date("D M j G:i:s T Y"), "pp". $key ." : " . $item ));
					}
					fwrite($logfile, sprintf("\r%s:- %s",date("D M j G:i:s T Y"), $data ));
				}
			}
			
			$auth = array(
				'authorized' 	=> FALSE,
				'error_message'	=> $this->lang('paypal_not_verified'),
				'failed'		=> TRUE,
				'declined'		=> FALSE,
				'transaction_id'=> NULL, 
				);
		}
		else
		{
			$this->log("paypal: unknown response");
			
			$auth = array(
				'authorized' 	=> FALSE,
				'error_message'	=> $this->lang('paypal_unknown_response'),
				'failed'		=> TRUE,
				'declined'		=> FALSE,
				'transaction_id'=> NULL, 
				);
		}
		$this->gateway_order_update($auth, $order_id); 	
		// we don't want to go through the normal checkout_complete function, so we exit here. 
		exit;
	} // END
	
}
// END Class