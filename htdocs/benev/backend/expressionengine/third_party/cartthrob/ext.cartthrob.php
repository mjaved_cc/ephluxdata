<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_ext
{
	public $settings = array();
	public $name = 'CartThrob';
	public $version;
	public $description = 'CartThrob Shopping Cart';
	public $settings_exist = 'y';
	public $docs_url = 'http://cartthrob.com/docs';

	/**
	* Cartthrob_ext
	*/
	public function __construct($settings='')
	{
		$this->EE =& get_instance();
		
		include PATH_THIRD.'cartthrob/config'.EXT;
		
		$this->version = $config['version'];
	}
	
	public function settings_form()
	{
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=cartthrob');
	}
	
	/**
	 * Activates Extension
	 *
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function activate_extension()
	{
		$this->activate_hooks(array(
			array('member_member_logout'),
//			array('member_member_login', 'member_member_login_multi'),
//			array('member_member_login', 'member_member_login_single'),
//			array('submit_new_entry_start'),
//			array('show_full_control_panel_end'),
//			array('channel_entries_tagdata'),
			array('cp_menu_array'),
		));
		
		return TRUE;
	}
	// END
	
	// --------------------------------
	//  Activate Hooks
	// --------------------------------
	/**
	 * Activates Hooks
	 * 
	 * Registers an array of hooks in the exp_extensions database 
	 *
	 * @access private
	 * @param array $hooks
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	private function activate_hooks($hooks)
	{	
		foreach ($hooks as $row)
		{
			$this->EE->db->insert(
				'extensions',
				array(
					'class' => __CLASS__,
					'method' => $row[0],
					'hook' => ( ! isset($row[1])) ? $row[0] : $row[1],
					'settings' => ( ! isset($row[2])) ? '' : $row[2],
					'priority' => ( ! isset($row[3])) ? 10 : $row[3],
					'version' => $this->version,
					'enabled' => 'y'
				)
			);
		}
	}
	// END
	
	
	// --------------------------------
	//  Update Extension
	// --------------------------------  
	/**
	 * Updates Extension
	 *
	 * @access public
	 * @param string
	 * @return void|BOOLEAN False if the extension is current
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function update_extension($current='')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
		
		$this->EE->db->update('extensions', array('version' => $this->version), array('class' => __CLASS__));
		
		return TRUE;
	}
	// END
	
	// --------------------------------
	//  Disable Extension
	// --------------------------------
	/**
	 * Disables Extension
	 * 
	 * Deletes mention of this extension from the exp_extensions database table
	 *
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function disable_extension()
	{
		$this->EE->db->delete('extensions', array('class' => __CLASS__));
	}
	// END
	
	// --------------------------------
	//  Settings Function
	// --------------------------------
	/**
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function settings()
	{
	}
	// END
	
	public function sessions_start(&$session)
	{
		$this->EE->session = $session;
		
		$this->EE->load->library('cartthrob_loader');
	}
	
	// --------------------------------
	//  Member Logout Hook Access
	// --------------------------------
	/**
	 * Perform additional actions after logout
	 *
	 * @access public
	 * @param NULL
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 * @see http://expressionengine.com/developers/extension_hooks/member_member_logout/
	 */
	public function member_member_logout()
	{
		$this->EE->load->library('cartthrob_loader');
		
		if ($this->EE->cartthrob->store->config('clear_session_on_logout'))
		{
			session_start();
			session_unset();
			session_destroy();
		}
		else if ($this->EE->cartthrob->store->config('clear_cart_on_logout') && ! $this->EE->cartthrob->cart->is_empty())
		{
			$this->EE->cartthrob->cart->clear()->save();
		}
	}
	// END
	
	//not currently in use
	public function member_member_login($row)
	{
		//@TODO test this, does it work?
	
		//we set this so that the cartthrob_session class
		//knows to load the session from session_id, not member_id
		$this->EE->session->cache['cartthrob']['just_logged_in'] = TRUE;
		
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->db->update('cartthrob_sessions', array('member_id' => $row->member_id), array('session_id' => $this->EE->cartthrob_session->userdata('session_id')));
	}
	
	
	//@TODO finish this and add it as a setting
	public function cp_menu_array($menu)
	{
		if ($this->EE->extensions->last_call !== FALSE)
		{
			$menu = $this->EE->extensions->last_call;
		}
		
		$channels = array();
		
		if (is_array($menu['content']['publish']))
		{
			//we've got a perfectly good list of channels right here in menu, let's grab it
			foreach ($menu['content']['publish'] as $channel_name => $url)
			{
				if (preg_match('/channel_id=(\d+)$/', $url, $match))
				{
					$channels[$match[1]] = $channel_name;
				}
			}
		}
		else if (is_string($menu['content']['publish']) && preg_match('/channel_id=(\d+)$/', $menu['content']['publish'], $match))
		{
			$channels[$match[1]] = '';
		}
		
		$this->EE->lang->loadfile('cartthrob', 'cartthrob');
		
		$query = $this->EE->db->where('site_id', $this->EE->config->item('site_id'))
				      ->where_in('`key`', array('product_channels', 'orders_channel', 'coupon_code_channel', 'discount_channel', 'purchased_items_channel', 'cp_menu_label'))
				      ->get('cartthrob_settings');
		
		$settings = array();
		
		foreach ($query->result() as $row)
		{
			$settings[$row->key] = ($row->serialized) ? @unserialize($row->value) : $row->value;
		}
		
		$label = 'cartthrob';
		
		if ( ! empty($settings['cp_menu_label']))
		{
			$this->EE->lang->language['nav_'.$settings['cp_menu_label']] = $settings['cp_menu_label'];
			
			$label = $settings['cp_menu_label'];
		}
		
		$menu[$label] = array();
		
		$has_channel = FALSE;
		
		if ( ! empty($settings['product_channels']))
		{
			if (count($settings['product_channels']) > 1)
			{
				foreach ($settings['product_channels'] as $channel_id)
				{
					if (isset($channels[$channel_id]))
					{
						$has_channel = TRUE;
						
						$this->EE->lang->language['nav_'.$channels[$channel_id]] = $channels[$channel_id];
						$menu[$label]['products'][$channels[$channel_id]] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$channel_id;
					}
				}
			}
			else
			{
				$channel_id = current($settings['product_channels']);
				
				if (isset($channels[$channel_id]))
				{
					$has_channel = TRUE;
					
					$menu[$label]['products'] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$channel_id;
				}
			}
		}
		
		if (isset($settings['orders_channel']))
		{
			if (isset($channels[$settings['orders_channel']]))
			{
				$has_channel = TRUE;
				
				$menu[$label]['orders'] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$settings['orders_channel'];
			}
		}
		
		if (isset($settings['purchased_items_channel']))
		{
			if (isset($channels[$settings['purchased_items_channel']]))
			{
				$has_channel = TRUE;
				
				$menu[$label]['purchased_items'] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$settings['purchased_items_channel'];
			}
		}
		
		if (isset($settings['discount_channel']))
		{
			if (isset($channels[$settings['discount_channel']]))
			{
				$has_channel = TRUE;
				
				$menu[$label]['discounts'] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$settings['discount_channel'];
			}
		}
		
		if (isset($settings['coupon_code_channel']))
		{
			if (isset($channels[$settings['coupon_code_channel']]))
			{
				$has_channel = TRUE;
				
				$menu[$label]['coupon_codes'] = BASE.AMP.'C=content_edit'.AMP.'channel_id='.$settings['coupon_code_channel'];
			}
		}
		
		require_once PATH_THIRD.'cartthrob/mcp.cartthrob.php';
		
		$settings = array();
		
		foreach (array_keys(Cartthrob_mcp::$nav) as $nav)
		{
			$settings[$nav] = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=cartthrob'.AMP.'method='.$nav;
		}
		
		if ($has_channel)
		{
			$menu[$label][] = '----';
			
			$menu[$label]['settings'] = $settings;
		}
		else
		{
			$menu[$label] = $settings;
		}
		
		return $menu;
	}
}
// END CLASS
/* End of file ext.cartthrob_ext.php */
/* Location: ./system/extension/ext.cartthrob_ext.php */