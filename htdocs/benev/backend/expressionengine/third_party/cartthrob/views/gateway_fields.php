<?php echo $hidden; ?>
<?php foreach ($sections as $section => $fields) : ?>
<?=form_fieldset(lang($section), array('class' => $section, 'id' => $section))?>
<?php foreach ($fields as $field) : ?>

	<?=form_label(lang($field), $field, (in_array($field, $required_fields)) ? array('class' => 'required') : array())?>

<?php
	$attributes = array('id' => $field);
	if (in_array($field, $required_fields))
	{
		$attributes['class'] = 'required';
	}

	switch ($field)
	{
		case 'card_type': 
			$field = form_dropdown($field, $card_types, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes)); 
			break;
		case 'expiration_month':
		case 'begin_month':
			$field = form_dropdown($field, $months, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes)); 
			break;
		case 'expiration_year':
			$field =form_dropdown($field, $exp_years, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes)); 
			break;
		case 'begin_year':
			$field = form_dropdown($field, $begin_years, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes)); 
			break;
		case 'state': 
		case 'shipping_state':
			$field = form_dropdown($field, $states, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes)); 
			break;
		case 'country': 
		case 'shipping_country': 
		case 'country_code': 
		case 'shipping_country_code':
			$field = form_dropdown($field, $countries, $this->cartthrob->cart->customer_info($field), _parse_attributes($attributes));
			break;
		default:
			$attributes['name'] = $field;
			$attributes['value'] = $this->cartthrob->cart->customer_info($field);
			$field = form_input($attributes);
	}
	
	$field = str_replace('<option', str_repeat("\t", 2).'<option', $field);
	$field = str_replace('</select', str_repeat("\t", 1).'</select', $field);
?>
	<?=$field?>

<?php endforeach; ?>
<?=form_fieldset_close()?>

<?php endforeach; ?>
