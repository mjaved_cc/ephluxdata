<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('security_settings_header')?></strong>
				</th>
 			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td>
					<label><?=lang('security_settings_allow_gateway_selection')?></label>
					<div class="subtext"><?=lang('security_settings_allow_gateway_selection_description')?></div>
				</td>
				<td style='width:50%;'>
					<input class='radio' type='radio' name='allow_gateway_selection' value='1' <?php if ($settings['allow_gateway_selection']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('yes')?>
					<input class='radio' type='radio' name='allow_gateway_selection' value='0' <?php if ( ! $settings['allow_gateway_selection']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('no')?>
				</td>
			</tr>
			<tr class="odd">
				<td>
					<label><?=lang('security_settings_cc_modulus_checking')?></label>
					<div class="subtext"><?=lang('security_settings_cc_modulus_description')?></div>
				</td>
				<td style='width:50%;'>
					<input class='radio' type='radio' name='modulus_10_checking' value='1' <?php if ($settings['modulus_10_checking']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('yes')?>
					<input class='radio' type='radio' name='modulus_10_checking' value='0' <?php if ( ! $settings['modulus_10_checking']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('no')?>
				</td>
			</tr>
		</tbody>
	</table>
