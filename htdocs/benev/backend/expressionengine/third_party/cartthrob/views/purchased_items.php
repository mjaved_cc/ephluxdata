<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('purchased_items_headers')?></strong><br />
					<?=lang('purchased_items_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td>
					<?=lang('save_purchased_items')?>
 				</td>
				<td style='width:50%;'>
					<input class='radio' type='radio' name='save_purchased_items' value='1' <?php if ($settings['save_purchased_items']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('yes')?>
					<input class='radio' type='radio' name='save_purchased_items' value='0' <?php if ( ! $settings['save_purchased_items']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('no')?>
 				</td>
			</tr>
			<tr class="odd">
				<td>
					<label><?=lang('purchased_items_channel')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_channel' class="channels" id='select_purchased_items' >
						<option value=''></option>
						<?php foreach ($channels as $channel) : ?>
							<option value="<?=$channel['channel_id']?>" <?php if ($settings['purchased_items_channel'] == $channel['channel_id']) : ?>selected="selected"<?php endif; ?>>
								<?=$channel['channel_title']?>
							</option>
						<?php endforeach; ?>
					</select>
 				</td>
			</tr>
			<tr class="even">
				<td>
					<label><?=lang('purchased_items_default_status')?></label>
					<div class="subtext"><?=lang('purchased_items_set_status')?> </div>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_default_status' class='select status_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($statuses[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($statuses[$settings['purchased_items_channel']] as $status) : ?>
								<option value="<?=$status['status']?>" <?php if ($settings['purchased_items_default_status'] == $status['status']) : ?>selected="selected"<?php endif; ?>>
									<?=$status['status']?>
								</option>
							<?php endforeach; ?>  
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="odd">
				<td>
					<label><?=lang('purchased_items_processing_status')?></label>
  				</td>
				<td style='width:50%;'>
					<select name='purchased_items_processing_status' class='select status_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($statuses[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($statuses[$settings['purchased_items_channel']] as $status) : ?>
								<option value="<?=$status['status']?>" <?php if ($settings['purchased_items_processing_status'] == $status['status']) : ?>selected="selected"<?php endif; ?>>
									<?=$status['status']?>
								</option>
							<?php endforeach; ?>  
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="even">
				<td>
					<label><?=lang('purchased_items_declined_status')?></label>
  				</td>
				<td style='width:50%;'>
					<select name='purchased_items_declined_status' class='select status_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($statuses[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($statuses[$settings['purchased_items_channel']] as $status) : ?>
								<option value="<?=$status['status']?>" <?php if ($settings['purchased_items_declined_status'] == $status['status']) : ?>selected="selected"<?php endif; ?>>
									<?=$status['status']?>
								</option>
							<?php endforeach; ?>  
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="odd">
				<td>
					<label><?=lang('purchased_items_failed_status')?></label>
  				</td>
				<td style='width:50%;'>
					<select name='purchased_items_failed_status' class='select status_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($statuses[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($statuses[$settings['purchased_items_channel']] as $status) : ?>
								<option value="<?=$status['status']?>" <?php if ($settings['purchased_items_failed_status'] == $status['status']) : ?>selected="selected"<?php endif; ?>>
									<?=$status['status']?>
								</option>
							<?php endforeach; ?>  
						<?php endif; ?>
					</select>
 				</td>
			</tr>
		</tbody>	
	</table>
</div> 

<h3 class="accordion"><?=lang('purchased_items_data_fields')?></h3>
<div style="padding: 5px 1px;">
 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th><strong><?=lang('purchased_items_data_type')?></strong><br />
				<?=lang('purchased_items_data_type_description')?>
				</th>
				<th><strong><?=lang('purchased_items_channel_field')?></strong><br />
					<?=lang('purchased_items_channel_field_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">			
 				<td>
					<label><?=lang('purchased_items_title_prefix')?></label>
 				</td>
				<td style='width:50%;'>
					<input type="text" name="purchased_items_title_prefix" value="<?=$settings['purchased_items_title_prefix']?>" />
 				</td>
			</tr>
			<tr class="odd">			
 				<td>
					<label><?=lang('purchased_items_id_field')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_id_field' class='select field_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($fields[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($fields[$settings['purchased_items_channel']] as $field) : ?>
								<option value="<?=$field['field_id']?>" <?php if ($settings['purchased_items_id_field'] == $field['field_id']) : ?>selected="selected"<?php endif; ?>>
									<?=$field['field_label']?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="even">			
 				<td>
					<label><?=lang('purchased_items_quantity_field')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_quantity_field' class='select field_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($fields[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($fields[$settings['purchased_items_channel']] as $field) : ?>
								<option value="<?=$field['field_id']?>" <?php if ($settings['purchased_items_quantity_field'] == $field['field_id']) : ?>selected="selected"<?php endif; ?>>
									<?=$field['field_label']?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="odd">			
 				<td>
					<label><?=lang('purchased_items_price_field')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_price_field' class='select field_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($fields[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($fields[$settings['purchased_items_channel']] as $field) : ?>
								<option value="<?=$field['field_id']?>" <?php if ($settings['purchased_items_price_field'] == $field['field_id']) : ?>selected="selected"<?php endif; ?>>
									<?=$field['field_label']?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="even">			
 				<td>
					<label><?=lang('purchased_items_order_id_field')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_order_id_field' class='select field_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($fields[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($fields[$settings['purchased_items_channel']] as $field) : ?>
								<option value="<?=$field['field_id']?>" <?php if ($settings['purchased_items_order_id_field'] == $field['field_id']) : ?>selected="selected"<?php endif; ?>>
									<?=$field['field_label']?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="odd">			
 				<td>
					<label><?=lang('purchased_items_license_number_field')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_license_number_field' class='select field_purchased_items' >
						<option value='' class="blank" ></option>
						<?php if ($settings['purchased_items_channel'] && isset($fields[$settings['purchased_items_channel']])) : ?>
							<?php foreach ($fields[$settings['purchased_items_channel']] as $field) : ?>
								<option value="<?=$field['field_id']?>" <?php if ($settings['purchased_items_license_number_field'] == $field['field_id']) : ?>selected="selected"<?php endif; ?>>
									<?=$field['field_label']?>
								</option>
							<?php endforeach; ?>
						<?php endif; ?>
					</select>
 				</td>
			</tr>
			<tr class="even">			
 				<td>
					<label><?=lang('purchased_items_license_number_type')?></label>
 				</td>
				<td style='width:50%;'>
					<select name='purchased_items_license_number_type' class='select field_purchased_items' >
						<option value='uuid' class="blank" ><?=lang('license_number_uuid')?></option>
					</select>
 				</td>
			</tr>
		</tbody>
	</table>
