<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable js_hide" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('tax_header')?></strong><br />
					<?=lang('tax_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('tax_settings')?></strong><br />
					<?=lang('tax_global_options')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td>
					<label><?=lang('tax_use_shipping_address')?></label>
				</td>
				<td style='width:50%;'>
					<input class='radio' type='radio' name='tax_use_shipping_address' value='1' <?php if ($settings['tax_use_shipping_address']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('yes')?>
					<input class='radio' type='radio' name='tax_use_shipping_address' value='0' <?php if ( ! $settings['tax_use_shipping_address']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('no')?>
				</td>
			</tr>
			<tr class="odd">
				<td>
 					<label><?=lang('tax_options_inclusive_price')?></label>
					<div class="subtext"><?=lang('tax_options_inclusive_price_description')?></div>
				</td>
				<td style='width:50%;'>
					<input class='radio' type='radio' name='tax_inclusive_price' value='1' <?php if ($settings['tax_inclusive_price']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('yes')?>
					<input class='radio' type='radio' name='tax_inclusive_price' value='0' <?php if ( ! $settings['tax_inclusive_price']) : ?>checked='checked'<?php endif; ?> /> 
					<?=lang('no')?>
				</td>
			</tr>
			<tr class="even">
				<td>
				    <label><?=lang('tax_choose_a_plugin')?></label>
 				</td>
				<td style='width:50%;'>
				    <select name='tax_plugin' class='plugins' id="select_tax_plugin">
						<?php foreach ($tax_plugins as $plugin) : ?>
							<option value="<?=$plugin['classname']?>" <?php if ($settings['tax_plugin'] == $plugin['classname']) : ?>selected="selected"<?php endif; ?>>
								<?=$plugin['title']?>
							</option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	
<?=$this->load->view('plugin_settings', array('settings'=>$settings, 'plugins'=>$tax_plugins, 'plugin_type'=>'tax_plugin'))?>
