<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('get_started_header')?></strong><br />
					<?=lang('get_started_description')?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/sub_pages/choose_channels_to_store_product_data_orders_and_coupons" target="_blank">
						<?=lang('get_started_view_video')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/sub_pages/backend_configuration_settings" target="_blank">
						<?=lang('get_started_view_video')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/tags/" target="_blank">
						<?=lang('get_started_view_tags_list')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/" target="_blank">
						<?=lang('get_started_view_user_guide')?> &raquo;
					</a>
				</td>
			</tr>
 		</tbody>
	</table>
