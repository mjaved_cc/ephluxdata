<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="">
			<tr>
				<th colspan="2">
					<strong><?=lang('nav_support')?></strong><br />
 				</th>
			</tr>
		</thead>
		<tbody>	
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/docs" target="_blank">
						<?=lang('nav_http://cartthrob.com/docs')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">
					<a href="http://cartthrob.com/bug_tracker" target="_blank">
						<?=lang('nav_http://cartthrob.com/bug_tracker')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/forums" target="_blank">
						<?=lang('nav_http://cartthrob.com/forums')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">
					<a href="http://cartthrob.com/site/contact_us" target="_blank">
						<?=lang('nav_http://cartthrob.com/site/contact_us')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/sub_pages/shipping/" target="_blank">
						<?=lang('nav_http://cartthrob.com/docs/sub_pages/shipping/')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="odd">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/sub_pages/payments/" target="_blank">
						<?=lang('nav_http://cartthrob.com/docs/sub_pages/payments/')?> &raquo;
					</a>
				</td>
			</tr>
			<tr class="even">
				<td colspan="2">
					<a href="http://cartthrob.com/docs/sub_pages/coupons_overview/" target="_blank">
						<?=lang('nav_http://cartthrob.com/docs/sub_pages/coupons_overview/')?> &raquo;
					</a>
				</td>
			</tr>
 		</tbody>
	</table>
