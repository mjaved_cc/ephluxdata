<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php if (count($template_errors) || count($templates_installed)) : ?>
	<div class="box">
		<?php if (count($templates_installed)) : ?>
			<span class="highlight_alt_bold"><?=lang('installed')?>:</span><br />
			<ul>
				<?php foreach ($templates_installed as $installed) : ?>
					<li><?=$installed?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<?php if (count($templates_installed) && count($template_errors)) : ?>
			<br />
		<?php endif; ?>
		<?php if (count($template_errors)) : ?>
			<span class="alert"><?=lang('errors')?>:</span><br />
			<ul>
				<?php foreach ($template_errors as $error) : ?>
					<li>
						<span class="alert"><?=$error?></span>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
<?php endif; ?>


 	<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
		<thead class="visualEscapism">
			<tr>
				<th><?=lang('preference')?></th><th><?=lang('setting')?></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($install_channels)) : ?>
				<tr class="<?=alternator('odd', 'even')?>">
					<td>
						<label style="height:100%;"><?=lang('section')?></label>
					</td>
					<td style='width:50%;'>
						<ul>
						<?php foreach ($install_channels as $index => $name): ?>
							<li>
								<label class="radio">
									<input type="checkbox" checked="checked" name="templates[]" class="templates" value="<?=$index?>" />
									<?=$name?>
								</label>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			<?php endif; ?>
			<?php if (count($install_template_groups)) : ?>
			<?php $checked = array('cart', 'cart_examples', 'cart_multi_page_checkout', 'cart_includes'); ?>
				<tr class="<?=alternator('odd', 'even')?>">
					<td>
						<label style="height:100%;"><?=lang('templates')?></label>
					</td>
					<td style='width:50%;'>
						<ul>
						<?php foreach ($install_template_groups as $index => $name): ?>
						<li>
							<label class="radio">
								<input type="checkbox" <?php if (in_array($name, $checked)) : ?>checked="checked" <?php endif; ?>name="templates[]" class="templates" value="<?=$index?>" />
								<?=$name?>
							</label>
						</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>	
			<?php endif; ?>
			<?php if (count($install_member_groups)) : ?>
			
 			<tr class="<?=alternator('odd', 'even')?>">
				<td>
					<label style="height:100%;"><?=lang('section')?></label>
				</td>
				<td style='width:50%;'>
					<ul>
					<?php foreach ($install_member_groups as $index => $name): ?>
						<li>
							<label class="radio">
								<input type="checkbox" checked="checked" name="templates[]" class="templates" value="<?=$index?>" />
								<?=$name?>
							</label>
						</li>
					<?php endforeach; ?>
					</ul>
				</td>
			</tr>
			<?php endif; ?>
		</tbody>	
	</table>
