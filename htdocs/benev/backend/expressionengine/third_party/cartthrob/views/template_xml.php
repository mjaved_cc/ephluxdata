<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<xml>
	<channel channel_name='products' channel_title='Store - Products' channel_url='' channel_description='' channel_lang='en' total_comments='0' last_comment_date='0' cat_group='' status_group='0' deft_status='open' search_excerpt='0' deft_category='' deft_comments='y' channel_require_membership='y' channel_max_chars='0' channel_html_formatting='all' channel_allow_img_urls='y' channel_auto_link_urls='n' channel_notify='n' channel_notify_emails='' comment_url='' comment_system_enabled='y' comment_require_membership='n' comment_use_captcha='n' comment_moderate='n' comment_max_chars='0' comment_timelock='0' comment_require_email='y' comment_text_formatting='xhtml' comment_html_formatting='safe' comment_allow_img_urls='n' comment_auto_link_urls='y' comment_notify='n' comment_notify_authors='n' comment_notify_emails='' comment_expiration='0' search_results_url='' ping_return_url='' show_button_cluster='y' rss_url='' enable_versioning='n' max_revisions='10' default_entry_title='' url_title_prefix='' live_look_template='0'>
		<field_group group_name='Products'>
			<field field_name='product_description' field_label='Description' field_instructions='' field_type='textarea' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_price' field_label='Price' field_instructions='The current product price (regular or sale price)' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='2' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_original_price' field_label='Original Price' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='3' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_cost' field_label='Store Cost' field_instructions='This is the wholesale cost' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='y' field_fmt='none' field_show_fmt='n' field_order='4' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_shipping' field_label='Shipping' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='2' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='5' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_weight' field_label='Weight' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='5' field_content_type='numeric' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_inventory' field_label='Inventory' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='6' field_content_type='integer' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_sku' field_label='SKU' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='10' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_taxable' field_label='Taxable' field_instructions='' field_type='select' field_list_items='Yes\nNo' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='11' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_shippable' field_label='Shippable' field_instructions='' field_type='select' field_list_items='Yes\nNo' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='12' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_thumbnail' field_label='Product Thumbnail' field_instructions='' field_type='file' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='15' field_content_type='image' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_detail_image' field_label='Product Detail Image' field_instructions='' field_type='file' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='16' field_content_type='image' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_size' field_label='Options - Size' field_instructions='' field_type='cartthrob_price_modifiers' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='20' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_options_other' field_label='Options - Other' field_instructions='' field_type='cartthrob_price_modifiers' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='21' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_color' field_label='Options - Color' field_instructions='' field_type='cartthrob_price_modifiers' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='22' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_related_1' field_label='Related Product 1' field_instructions='' field_type='rel' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='4' field_related_orderby='title' field_related_sort='asc' field_related_max='250' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='30' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_related_2' field_label='Related Product 2' field_instructions='' field_type='rel' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='4' field_related_orderby='title' field_related_sort='asc' field_related_max='250' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='32' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='related_product_3' field_label='Related Product 3' field_instructions='' field_type='rel' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='4' field_related_orderby='title' field_related_sort='asc' field_related_max='250' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='33' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='product_download_url' field_label='Download URL' field_instructions='If the product has an associated download URL, add it here. ' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='300' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='40' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
		</field_group>
	</channel>
	<channel channel_name='coupon_codes' channel_title='Store - Coupon Codes' channel_url='' channel_description='' channel_lang='en' total_comments='0' last_comment_date='0' cat_group='' status_group='0' deft_status='open' search_excerpt='0' deft_category='' deft_comments='y' channel_require_membership='y' channel_max_chars='0' channel_html_formatting='all' channel_allow_img_urls='y' channel_auto_link_urls='y' channel_notify='n' channel_notify_emails='' comment_url='' comment_system_enabled='y' comment_require_membership='n' comment_use_captcha='n' comment_moderate='n' comment_max_chars='0' comment_timelock='0' comment_require_email='y' comment_text_formatting='xhtml' comment_html_formatting='safe' comment_allow_img_urls='n' comment_auto_link_urls='y' comment_notify='n' comment_notify_authors='n' comment_notify_emails='' comment_expiration='0' search_results_url='' ping_return_url='' show_button_cluster='y' rss_url='' enable_versioning='n' max_revisions='10' default_entry_title='' url_title_prefix='' live_look_template='0'>
		<field_group group_name='Coupon Codes'>
			<field field_name='coupon_code_type' field_label='Coupon Type' field_instructions='' field_type='cartthrob_discount' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='26' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='xhtml' field_show_fmt='y' field_order='1' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
		</field_group>
	</channel>
	<channel channel_name='orders' channel_title='Store - Orders' channel_url='' channel_description='' channel_lang='en' total_comments='0' last_comment_date='0' cat_group='' status_group='1' deft_status='open' search_excerpt='0' deft_category='' deft_comments='y' channel_require_membership='y' channel_max_chars='0' channel_html_formatting='all' channel_allow_img_urls='y' channel_auto_link_urls='n' channel_notify='n' channel_notify_emails='' comment_url='' comment_system_enabled='y' comment_require_membership='n' comment_use_captcha='n' comment_moderate='n' comment_max_chars='0' comment_timelock='0' comment_require_email='y' comment_text_formatting='xhtml' comment_html_formatting='safe' comment_allow_img_urls='n' comment_auto_link_urls='y' comment_notify='n' comment_notify_authors='n' comment_notify_emails='' comment_expiration='0' search_results_url='' ping_return_url='' show_button_cluster='y' rss_url='' enable_versioning='n' max_revisions='10' default_entry_title='' url_title_prefix='' live_look_template='0'>
		<field_group group_name='Orders'>
			<field field_name='order_items' field_label='Order Items' field_instructions='' field_type='cartthrob_order_items' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='0' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_total' field_label='Order Total' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='2' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_subtotal' field_label='Order Subtotal' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='3' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping' field_label='Order Shipping Cost' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='4' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_tax' field_label='Order Tax' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='5' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_coupons' field_label='Order Coupon Codes' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='6' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_option' field_label='Order Shipping Method' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='7' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_discount' field_label='Order Discount' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='7' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_customer_full_name' field_label='Customer Full Name' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='8' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_customer_phone' field_label='Customer Phone' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='9' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_customer_email' field_label='Customer Email' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='10' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_language' field_label='Customer Language Code' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='11' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_full_billing_address' field_label='Full Billing Address' field_instructions='' field_type='textarea' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='4' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='13' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_full_shipping_address' field_label='Full Shipping Address' field_instructions='' field_type='textarea' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='4' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='14' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_first_name' field_label='Billing First Name' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='15' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_address' field_label='Billing Address' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='16' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_address2' field_label='Billing Address 2' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='17' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_state' field_label='Billing State' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='18' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_zip' field_label='Billing Zip' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='19' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_city' field_label='Billing City' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='20' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_last_name' field_label='Billing Last Name' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='21' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_company' field_label='Billing Company' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='y' field_order='22' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_billing_country' field_label='Billing Country' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='y' field_order='23' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_country_code' field_label='Billing Country Code' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='3' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='y' field_order='24' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_first_name' field_label='Shipping First Name' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='25' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_last_name' field_label='Shipping Last Name' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='26' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_address' field_label='Shipping Address' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='27' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_address2' field_label='Shipping Address 2' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='28' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_city' field_label='Shipping City' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='29' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_state' field_label='Shipping State' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='30' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_zip' field_label='Shipping Zip' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='31' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_company' field_label='Shipping Company' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='32' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_country' field_label='Shipping Country' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='33' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_shipping_country_code' field_label='Shipping Country Code' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='34' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_error_message' field_label='Payment: Error Message' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='255' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='35' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_transaction_id' field_label='Payment: Transaction ID' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='date' field_related_sort='desc' field_related_max='0' field_ta_rows='8' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='36' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_last_four' field_label='Payment: CC Last Four Digits' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='4' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='37' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='order_ip_address' field_label='Payment: IP Address' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='38' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
		</field_group>
	</channel>
	<channel channel_name='purchased_items' channel_title='Store - Purchased Items' channel_url='' channel_description='' channel_lang='en' total_comments='0' last_comment_date='0' cat_group='1' status_group='1' deft_status='open' search_excerpt='0' deft_category='' deft_comments='y' channel_require_membership='y' channel_max_chars='0' channel_html_formatting='all' channel_allow_img_urls='y' channel_auto_link_urls='n' channel_notify='n' channel_notify_emails='' comment_url='' comment_system_enabled='y' comment_require_membership='n' comment_use_captcha='n' comment_moderate='n' comment_max_chars='0' comment_timelock='0' comment_require_email='y' comment_text_formatting='xhtml' comment_html_formatting='safe' comment_allow_img_urls='n' comment_auto_link_urls='y' comment_notify='n' comment_notify_authors='n' comment_notify_emails='' comment_expiration='0' search_results_url='' ping_return_url='' show_button_cluster='y' rss_url='' enable_versioning='n' max_revisions='10' default_entry_title='' url_title_prefix='' live_look_template='0'>
		<field_group group_name='Purchased Items'>
			<field field_name='purchased_id' field_label='ID' field_instructions='' field_type='text' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='26' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='purchased_quantity' field_label='Quantity' field_instructions='' field_type='text' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='purchased_price' field_label='Price' field_instructions='' field_type='cartthrob_price_simple' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='0' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='purchased_order_id' field_label='Order Id' field_instructions='' field_type='text' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='purchased_product_download_url' field_label='Product Download URL' field_instructions='This is the filename of the downloadable product. ' field_type='text' field_list_items='' field_pre_populate='n' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='8' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
			<field field_name='purchased_license_number' field_label='License Number' field_instructions='' field_type='text' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='5' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='y' field_order='6' field_content_type='any' field_settings='YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
		</field_group>
	</channel>
	<channel channel_name='discounts' channel_title='Store - Discounts' channel_url='' channel_description='' channel_lang='en' total_comments='0' last_comment_date='0' cat_group='' status_group='1' deft_status='open' search_excerpt='0' deft_category='' deft_comments='y' channel_require_membership='y' channel_max_chars='0' channel_html_formatting='all' channel_allow_img_urls='y' channel_auto_link_urls='y' channel_notify='n' channel_notify_emails='' comment_url='' comment_system_enabled='y' comment_require_membership='n' comment_use_captcha='n' comment_moderate='n' comment_max_chars='0' comment_timelock='0' comment_require_email='y' comment_text_formatting='xhtml' comment_html_formatting='safe' comment_allow_img_urls='n' comment_auto_link_urls='y' comment_notify='n' comment_notify_authors='n' comment_notify_emails='' comment_expiration='0' search_results_url='' ping_return_url='' show_button_cluster='y' rss_url='' enable_versioning='n' max_revisions='10' default_entry_title='' url_title_prefix='' live_look_template='0'>
		<field_group group_name='Discounts'>
			<field field_name='discount_type' field_label='Discount Settings' field_instructions='' field_type='cartthrob_discount' field_list_items='' field_pre_populate='0' field_pre_channel_id='0' field_pre_field_id='0' field_related_to='channel' field_related_id='2' field_related_orderby='title' field_related_sort='desc' field_related_max='0' field_ta_rows='6' field_maxl='128' field_required='n' field_text_direction='ltr' field_search='n' field_is_hidden='n' field_fmt='none' field_show_fmt='n' field_order='1' field_content_type='any' field_settings='YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=' />
		</field_group>
	</channel>
	<template_group group_name='cart' group_order='0' is_site_default='y'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299768384' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='185'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="index"}
 
{embed="{template_group}/.header" title="Shopping Cart" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}

					<div class="left_column">
						<h1>Available Products</h1>
 
							{exp:channel:entries channel="products" limit="30"  dynamic="no"}
  								
								<div class="product">
									<h3>{title}</h3>
									{if product_thumbnail}
											<a href="{entry_id_path='{template_group}/product_detail'}"><img src="{product_thumbnail}" /></a>
									{/if}
									<p>
									{if product_original_price && product_original_price > product_price}
										Regular Price: {product_original_price}<br />
											<span class="sale_price">Sale Price: {product_price}</span> 
									{if:else}
										Regular Price: {product_price} 
									{/if}
									<br /><a href="{entry_id_path={template_group}/product_detail}">Learn more &raquo;</a></p>

								</div>
  							{/exp:channel:entries}
					</div>
					<div class="right_column">
 						{embed="{template_group}/.right_column" template_group="{template_group}" template="{template}"}
					</div>
					<div class="clear_both"></div>

 				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299769889' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='57'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="checkout"}
 
{embed="{template_group}/.header" title="Checkout" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}
 					<div class="left_column">
						<h1>Checkout</h1>
						{if "{exp:cartthrob:total_items_count}" > 0}
						
						<div id="checkout_header">

							<div id="checkout_totals">
								<table>
									<tr>
										<td><span class="sale_price">Total</span></td>
										<td><span class="sale_price">{exp:cartthrob:cart_total}</span></td>
									</tr>							
									<tr>
										<td>Subtotal</td>
										<td>{exp:cartthrob:cart_subtotal}</td>
									</tr>
									<tr>
										<td>Shipping</td>
										<td>{exp:cartthrob:cart_shipping}</td>
									</tr>
									<tr>
										<td>Tax</td>
										<td>{exp:cartthrob:cart_tax}</td>
									</tr>
									<tr>
										<td>-Discount</td>
										<td>{exp:cartthrob:cart_discount}</td>
									</tr>
								</table>
 							</div>
							<div id="checkout_notes">
								<p>Expected shipping time 
								is 3-7 days<br /><br />

								*All fields are required</p>
							</div>
							<div class="clear_both"></div>
						</div>
						{exp:cartthrob:checkout_form 
							return="{template_group}/order_complete"}
							
							{gateway_fields}
							
						    <input type="submit" value="Complete Checkout" />
						{/exp:cartthrob:checkout_form}


						{if:else}
							<p>You have no items in your cart. <a href="{path=cart/index}">Continue shopping</a></p>
						{/if}
					</div>
					<div class="right_column">
					{exp:cartthrob:cart_items_info limit="1" order_by="entry_id" sort="desc"}
 						{embed="{template_group}/.right_column" entry_id="{entry_id}" template_group="{template_group}" template="{template}"}
					{/exp:cartthrob:cart_items_info} 						
						
					</div>
					<div class="clear_both"></div>
  				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='order_complete' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299646117' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='28'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="order_complete"}
{embed="{template_group}/.header" title="Order Results" template_group="{template_group}" template="{template}" }
</head>
<body>
<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}
	<h1>Store: Order Results</h1>
	<p></p>
 
<div class="store_block">
    <h2>The results of your order</h2>
	
	{exp:cartthrob:submitted_order_info}
	    <div class="store_block">
	        {if authorized}
	            Your transaction is complete!<br />
	            Transaction ID: {transaction_id}<br />
	            Your total: {cart_total}<br />
	            Your order ID: {order_id}
	        {if:elseif processing}
	            Your Order is Currently being processed: {error_message}<br />
	            Transaction ID: {transaction_id}<br />
	            <br />Order processing is generally completed within 48 hours depending on your payment method. 
	        {if:elseif declined}
	            Your credit card was declined: {error_message}
	            <a href="{path={template_group}/checkout}">Try checking out again &raquo;</a>
	        {if:elseif failed}
	            Your payment failed: {error_message}
	            <a href="{path={template_group}/checkout}">Try checking out again &raquo;</a>
	        {/if}
	    </div>
	{/exp:cartthrob:submitted_order_info}
</div>

  
	{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}		]]>
		</template>
		<template template_name='shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299769864' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='78'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="shipping"}
 
{embed="{template_group}/.header" title="Shipping" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}
				<div class="product">

					<div class="left_column">
						<h1>Set Shipping</h1>
 						
						<div id="checkout_header">
							<div id="checkout_totals">
								<table>
									<tr>
										<td><span class="sale_price">Total</span></td>
										<td><span class="sale_price"><span class="cart_total">{exp:cartthrob:cart_total}</span></span></td>
									</tr>							
									<tr>
										<td>Subtotal</td>
										<td><span class="cart_subtotal">{exp:cartthrob:cart_subtotal}</span></td>
									</tr>
									<tr>
										<td>Shipping</td>
										<td><span class="cart_shipping">{exp:cartthrob:cart_shipping}</span></td>
									</tr>
									<tr>
										<td>Tax</td>
										<td><span class="cart_tax">{exp:cartthrob:cart_tax}</span></td>
									</tr>
									<tr>
										<td>-Discount</td>
										<td><span class="cart_discount">{exp:cartthrob:cart_discount}</span></td>
									</tr>
								</table>
 							</div>
							<div id="checkout_notes">
								<p>Expected shipping time 
								is 3-7 days<br /><br />

								*All fields are required</p>
							</div>
							<div class="clear_both"></div>
						</div>
						
						{exp:cartthrob:save_customer_info_form 
					        id="myform_id" 
					        return="{template_group}/checkout" 
					        }
							{exp:cartthrob:customer_info}
								<fieldset>
								<label for="first_name">First Name: </label>
								<input type="text" id="first_name" name="first_name" value="{customer_first_name}" /><br />
								
								<label for="last_name">Last Name: </label>
								<input type="text" id="last_name" name="last_name" value="{customer_last_name}" /><br />
								
								<label for="email_address">Email Address:</label>
								<input type="text" id="email_address" name="email_address" value="{customer_email_address}" /><br />
								
								<label for="state">State:</label>
								{exp:cartthrob:state_select name="state" id="state" selected="{customer_state}"}<br />

								<label for="country_code">Country:</label>
								{exp:cartthrob:country_select name="country_code" id="country_code" selected="{customer_country_code}"}<br />
								
								<label for="zip">Zip:</label>
								<input type="text" name="zip" id="zip" value="{customer_zip}" /><br />
								</fieldset>
							{/exp:cartthrob:customer_info}

							{exp:cartthrob:get_shipping_options}

					        <br />
					        <input type="submit" value="Checkout &raquo; " />
					    {/exp:cartthrob:save_customer_info_form}
					

					</div>
					<div class="right_column">
					{exp:cartthrob:cart_items_info limit="1" order_by="entry_id" sort="desc"}
 						{embed="{template_group}/.right_column" entry_id="{entry_id}" template_group="{template_group}" template="{template}"}
					{/exp:cartthrob:cart_items_info}
					</div>
					<div class="clear_both"></div>
				</div>
 				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='view_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299769874' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='287'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="view_cart"}
 
{embed="{template_group}/.header" title="Shopping Cart" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}
 					<div class="left_column">
						<h1>Shopping Cart</h1>
						{if "{exp:cartthrob:total_items_count}"==0}You have no items in your cart. <a href="{path=cart/}">Continue shopping.</a>{/if}
						{exp:cartthrob:cart_items_info}
							{if first_row}
					    		{exp:cartthrob:update_cart_form 
								id="update_cart_form"
					        		return="{template_group}/{template}"}
								<input type="hidden" name="return" id="return_url" value="{template_group}/{template}" />
							{/if}
									<div class="product">
										<h3>{title}</h3>
										{if product_thumbnail}
										<a href="{entry_id_path='{template_group}/product_detail'}"><img src="{product_thumbnail}" /></a>
 										{/if}										
 										<p>Price: {item_price} x {quantity} = {item_subtotal}</p>
										{embed="{template_group}/.item_options" entry_id="{entry_id}" row_id="{row_id}"}
										<p>
											<label for="product_quantity_{row_id}">Quantity</label>
											<input type="text" id="product_quantity_{row_id}" name="quantity[{row_id}]" size="8"  value="{quantity}" /> 
										</p>
										<p>
											
											<label for="delete_this_{row_id}">Delete this?</label> 
											<input type="checkbox" id="delete_this_{row_id}" name="delete[{row_id}]">
										</p>
 									</div>
 							
							{if last_row}
	
							     <input type="submit" id="update_button" value="Update" /> 
							     <input type="submit" id="checkout_button" value="Proceed to Checkout" /> 

								{/exp:cartthrob:update_cart_form}
							{/if}
						{/exp:cartthrob:cart_items_info}
							
					</div>
					<div class="right_column">
					{exp:cartthrob:cart_items_info limit="1" order_by="entry_id" sort="desc"}
 						{embed="{template_group}/.right_column" entry_id="{entry_id}" template_group="{template_group}" template="{template}"}
					{/exp:cartthrob:cart_items_info}
					</div>
					<div class="clear_both"></div>
  				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='x_total' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299106011' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='32'>
		<![CDATA[
{exp:cartthrob:cart_total}		]]>
		</template>
		<template template_name='x_shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105969' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='31'>
		<![CDATA[
{exp:cartthrob:cart_shipping}		]]>
		</template>
		<template template_name='x_subtotal' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105980' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='31'>
		<![CDATA[
{exp:cartthrob:cart_subtotal}		]]>
		</template>
		<template template_name='x_discount' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105957' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='33'>
		<![CDATA[
{exp:cartthrob:cart_discount}		]]>
		</template>
		<template template_name='x_tax' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299377170' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='33'>
		<![CDATA[
{exp:cartthrob:cart_tax}		]]>
		</template>
		<template template_name='.header' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784013' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>{embed:title}</title>
	{exp:jquery:script_tag}
	<link rel="stylesheet" type="text/css" media="all" href="{stylesheet={embed:template_group}/main.css}" />
	<script type="text/javascript" src="{site_url}themes/third_party/cartthrob/scripts/jquery.form.js" ></script>
	
	<script type="text/javascript">
		jQuery(document).ready(function($){

			// writing a save customer info form to every page for ajax updating.
			$.get('{path=cart/x_save_customer_info_form}', function(data) {
 				$('body').append(data); 
				$("#hidden_save_customer_info_form").attr("style", "display:none"); 
			}); 

			// when billing and shipping data is changed, this is fired. 
			$("#state, #shipping_state, #country_code, #shipping_country_code, #zip, #shipping_zip, #region, #shipping_region").live('change',function(){
				// display a message indicating that we're updating fields cost
				update_fields(); 

				$('#hidden_save_customer_info_form form').ajaxSubmit({
					success: function(data) {
 						
						load_fields(); 
					}
				});
			});
			
			function update_fields()
			{
				// update the value of the hidden form field
				$('#hidden_country_code')	.val( $("#country_code").val() );
				$('#hidden_state')			.val( $("#state").val() );
				$('#hidden_zip')			.val( $("#zip").val() );
				$('#hidden_region')			.val( $("#region").val() );

				$('#hidden_shipping_country_code')	.val( $("#shipping_country_code").val() );
				$('#hidden_shipping_state')			.val( $("#shipping_state").val() );
				$('#hidden_shipping_zip')			.val( $("#shipping_zip").val() );
				$('#hidden_shipping_region')		.val( $("#shipping_region").val() );
			}
			function load_fields()
			{
				$('.cart_tax')			.html( 'updating...' );
				$('.cart_total')		.html( 'updating...' );
				$('.cart_shipping')		.html( 'updating...' );
				$('.cart_subtotal')		.html( 'updating...' );
				$('.cart_discount')		.html( 'updating...' );
				
				$('.cart_tax').	load('{path=cart/x_tax}');
				$('.cart_total').load('{path=cart/x_total}');
				$('.cart_shipping').load('{path=cart/x_shipping}');
				$('.cart_discount').load('{path=cart/x_discount}');
				$('.cart_subtotal').load('{path=cart/x_subtotal}');
			}
			function set_config(act, param)
			{
				$.get('{path=cart/x_set_config/}'+act+"/"+param, function(data) {
					load_fields(); 
				});
			}
			$("#checkout_button").click(function(){
				$("#return_url").val("cart/shipping"); 
				$("#update_cart_form").submit(); 
				return false; 
			}); 
			$("#update_button").click(function(){
				$("#return_url").val("cart/view_cart"); 
				$("#update_cart_form").submit(); 
				return false;
			});
 		}); 
	</script>
		]]>
		</template>
		<template template_name='.footer' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299694317' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
 </body>
</html>		]]>
		</template>
		<template template_name='x_save_customer_info_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299600228' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='864'>
		<![CDATA[
{exp:cartthrob:customer_info}
<div id="hidden_save_customer_info_form">
	{exp:cartthrob:save_customer_info_form 
 		return="cart/x_total"}
		<input type="text" value="{customer_shipping_country_code}" id="hidden_shipping_country_code" name="shipping_country_code">
		<input type="text" value="{customer_shipping_zip}" id="hidden_shipping_zip" name="shipping_zip">
		<input type="text" value="{customer_shipping_region}" id="hidden_shipping_region" name="shipping_region">
		<input type="text" value="{customer_shipping_state}" id="hidden_shipping_state" name="shipping_state">
		<input type="text" value="{customer_country_code}" id="hidden_country_code" name="country_code">
		<input type="text" value="{customer_zip}" id="hidden_zip" name="zip">
		<input type="text" value="{customer_state}" id="hidden_state" name="state">
		<input type="text" value="{customer_region}" id="hidden_region" name="region">
		<input type="submit" /> 
	{/exp:cartthrob:save_customer_info_form}
</div>
{/exp:cartthrob:customer_info}		]]>
		</template>
		<template template_name='main.css' save_template_file='n' template_type='css' template_notes='' edit_date='1299566942' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
* {
	margin:0;
	padding:0;
}
body {
	font-family:Verdana, Geneva, sans-serif;
	font-size:11px;
	color:#384a68;
	background:#d7d7d7; 
	width:100%;
}
body a {
	text-decoration:none;
	color:#384a68;
}
#container {
	margin-left:auto; 
	margin-right:auto;
	text-align:left; 
	position:relative;
	left:0px;
	top:0px;
	width:650px;
	min-height: 845px;
	background:#fff; 
}
#main {
	position:relative;
	left:15px;
	top:0px;
	width:610px;
	min-height:843px;
	z-index:1;
}
#header {
	position:relative;
}
#header_logo {
	width:365px;
	position:relative;
	float:left;
}
#header_account_info {
	text-align:right;
	position:relative;
	float:right;
}
#header_account_info p {
	margin:0;
	padding:0;
	margin-top:8px;
}
#login_form{
	padding:5px 10px;
	background-color:#3D506F;
	color:#FFFFFF;
}
#login_form a{
	color:#FFFFFF;
	text-decoration:underline;
}
.clear_both {
	clear:both;
}
#cart_icon {
	vertical-align:middle;
}
#top_nav {
	padding-bottom:12px;
}
#top_nav a {
	color:#c00000;
}
.product {
	padding-bottom:10px;
	border-bottom: 1px solid #d7d7d7;
	margin-bottom:18px;
}
.product:last-child{
	border-bottom: none; 
}
.left_column {
	float:left;
	width:345px;
}
.left_column p {
	line-height:18px;
	color:#8392B0;
	padding-bottom:18px;
}
.left_column form p {
	line-height:18px;
	color:#6a778e;
	padding-bottom:10px;
}
.right_column {
	text-align:left;
	float:right;
	width: 245px;
}
.right_column ul {
	list-style-position: inside;
}
.right_column ul li, .right_column ul li a {
	color:#8392B0;
	padding-bottom:6px;
}
.sale_price {
	font-size:14px;
	color:#526078;
	font-weight:bold;
}
.related_products {
	padding-top:18px;
 	padding-bottom:18px;

}

.related_item {
	float:left;
	padding-right:22px;
}
.related_item img {
	display:block;
	padding-bottom:12px
}

.cart_item{
	display: block;
	
}
#footer_nav{
 	border-top: 1px solid #d7d7d7;
	margin-top: 48px;
	padding-top: 38px;
	padding-bottom:10px;
	text-align:center;	
}
#footer_nav a, #footer_nav p, #footer_logo p, #footer_logo a{
	color:#8392B0;

}
#footer_logo{
	text-align:center;
	padding-bottom:8px;
}
#footer_logo img{
	vertical-align:middle;
}
#checkout_totals{
	width:140px;
	padding-right:25px;
	float:left;
}
#checkout_totals table tr td{
	padding-bottom: 4px; 
	padding-right: 6px;
}
#checkout_totals table{
	width: 100%; 
}
#checkout_notes{
	width:175px;
	float:right;
}
#checkout_header{
	padding-bottom: 12px;
}
h1 {
	color:#c00000;
	font-size:14px;
	padding-bottom:15px;
}
h2 {
	color:#384a68;
	font-size:13px;
}
h3 {
	font-size:13px;
	color:#a3a2a2;
	padding-bottom:12px
}
a img {
	border:none;
}
fieldset legend{
	display:none; 
}
fieldset {
	border:none; 
}
fieldset label{
	display:block;
	padding-bottom:4px;
}
fieldset input{
	width:300px; 
	display:block;
	margin-bottom:14px;
}
fieldset select, fieldset textarea{
	display:block;
	margin-bottom:14px;
}
.doc_block ul{
	margin:5px 20px;
}
#contact_fields{
	padding:15px;
	background-color:#3D506F;
	color:#FFFFFF;
}
#contact_action input{
	width:auto;
}		]]>
		</template>
		<template template_name='product_detail' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784233' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='229'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="product_detail"}

{exp:channel:entries 
	channel="products" 
	limit="1"}
{embed="{template_group}/.header" title="{title} - Product Detail" template_group="{template_group}" template="{template}" }
{/exp:channel:entries}
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}
			{exp:channel:entries 
				channel="products" 
				limit="1"}
				<div class="product">
					<div class="left_column">
						<h1>{title}</h1>
						#{product_sku}
						{product_description}
						{if product_original_price && product_original_price > product_price}
							<p>Regular Price: {product_original_price}<br />
								<span class="sale_price">Sale Price: {product_price}</span> </p>
						{if:else}
							<p>Regular Price: {product_price}</p>
						{/if}
						{if group_id ==1}<p>Our store cost: {product_cost}</p>{/if}

						{embed="{template_group}/.add_to_cart_form" 
								entry_id="{entry_id}" 
								inventory = "{product_inventory}"
								template_group="{template_group}"
								no_tax="{if '{product_taxable}' == 'Yes'}0{if:else}1{/if}" 
								no_shipping="{if '{product_shippable}' == 'Yes'}0{if:else}1{/if}" }
					</div>
					<div class="right_column">
						{if product_detail_image}
							<img src="{product_detail_image}" />
						{if:else}
							<img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/main_pic.jpg" />
						{/if}
					</div>
					<div class="clear_both"></div>
				</div>
				<div class="related_products">
					<div class="left_column">
						{if product_related_1 OR product_related_2 OR product_related_3}
							<h3>You Might Also Like</h3>
						{/if}
						{related_entries id="product_related_1"}
							<div class="related_item">
								<a href="{entry_id_path='{template_group}/{template}'}">
									<img src="{product_thumbnail}" />
									{title}<br />
									{product_price} &raquo;
								</a>
							</div>
						{/related_entries}
						
						{related_entries id="product_related_2"}
							<div class="related_item">
								<a href="{entry_id_path='{template_group}/{template}'}">
									<img src="{product_thumbnail}" />
									{title}<br />
									{product_price} &raquo;
								</a>
							</div>
						{/related_entries}
						
						{related_entries id="product_related_3"}
							<div class="related_item">
								<a href="{entry_id_path='{template_group}/{template}'}">
									<img src="{product_thumbnail}" />
									{title}<br />
									{product_price} &raquo;
								</a>
							</div>
						{/related_entries}
						
						<div class="clear_both"></div>
					</div>
					<div class="right_column">
						<h3>Customers Also Purchased</h3>
						<ul>
 							{embed="{template_group}/.also_purchased" entry_id="{entry_id}" template_group="{template_group}" template="{template}"} 
 						</ul>
					</div>
					<div class="clear_both"></div>
				</div>
				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}

			{/exp:channel:entries}
		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='.also_purchased' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299389218' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:also_purchased entry_id="{embed:entry_id}" limit="7"}
	{if no_results}

	{/if}
	{exp:channel:entries channel="products" entry_id="{entry_id}"}
		<li><a href="{entry_id_path="{embed:template_group}/{embed:template}"}">{title} <strong>{product_price} &raquo; </strong></a></li>
	{/exp:channel:entries}
{/exp:cartthrob:also_purchased}

		]]>
		</template>
		<template template_name='.top_nav' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784070' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<div id="top_shadow"><img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/top_shadow.gif" /></div>
<div id="top_nav"> 
	<a href="{site_url}">Home</a> 
	&bull; <a href="{path='cart/'}">Store</a>
	&bull; <a href="{path='cart/about'}">About Us</a> 
	&bull; <a href="{path='cart/contact'}">Contact Us</a> 
	&bull; <a href="{path='cart/donate'}">Donate</a>
</div>
<div id="top_spacer"><img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/top_spacer.gif" /></div>
		]]>
		</template>
		<template template_name='.store_header' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784056' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<div id="header">
{embed="cart/.login_form"}
	<div id="header_logo"><a href="{site_url}"><img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/logo.gif" /></a></div>
	<div id="header_account_info">
		{if logged_in}
		<p>
			<a href="{path='cart/account'}">welcome {screen_name} &raquo;</a><br />
			<a href="{path='LOGOUT'}">logout &raquo;</a>
		</p>
		{/if}
		{if logged_out}
		<p>
			<a href="{path='member/register'}">Register &raquo;</a><br />
			{!-- <a href="{path='member/login'}">Login &raquo;</a> --}
			<a href="javascript:void(0);" id="login_bttn">Login &raquo;</a>
			<script type="text/javascript">
        			$(function() {
					$("#login_bttn").click(function(){
						$("#login_form").show();
						return false;
					});
           			 });
			</script>
		</p>
		{/if}
		<p>
			<img id="cart_icon" src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/cart_icon.gif"/> 
			<a href="{path="{embed:template_group}/view_cart"}">cart ({exp:cartthrob:total_items_count}) {exp:cartthrob:cart_total} &raquo;</a> <a href="{path="{embed:template_group}/shipping"}">checkout &raquo;</a>
		</p>
	</div>
	<div class="clear_both"></div>
</div>		]]>
		</template>
		<template template_name='.footer_nav' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299783967' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<div id="footer_nav">
<p>
	<a href="{site_url}">Home</a> 
	&bull; <a href="{path='cart'}">Store</a>
	&bull; <a href="{path='cart/about'}">About Us</a> 
	&bull; <a href="{path='cart/contact'}">Contact Us</a> 
	&bull; <a href="{path='cart/donate'}">Donate</a>	
	&bull; <a href="{path='cart/privacy_policy'}">Privacy Policy</a>
	&bull; <a href="{path='cart/returns'}">Return Policy</a>
	&bull; &copy;{current_time format="%Y"}
</p>
</div>
<div id="footer_logo">
<p><img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/small_store_logo.gif" /> powered by <a href="http://cartthrob.com">CartThrob eCommerce</a></p>
</div>		]]>
		</template>
		<template template_name='.item_options' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299605631' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:item_options entry_id="{embed:entry_id}" row_id="{embed:row_id}" type=}

	{item_options:list}
		{if option_first_row}
			<p> 
			<label>{option_label}</label>
		{/if}  
	{/item_options:list}

	{if dynamic} 
		{item_options:input}
	{if:else}
	  	{item_options:select}		
			<option {selected} value="{option_value}">{option_name} {price}</option>
		{/item_options:select}
	{/if}
	 {item_options:list}
		{if option_last_row}
			</p> 
 		{/if}  
	{/item_options:list}

 {/exp:cartthrob:item_options}		]]>
		</template>
		<template template_name='.right_column' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299769797' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
	{if embed:entry_id}
	<h3>Customers Also Purchased</h3>
	<ul>
	 	{embed="{embed:template_group}/.also_purchased" entry_id="{entry_id}" template_group="{embed:template_group}" template="product_detail"} 
	</ul>
	<br /><br />
	{exp:channel:entries channel="products" limit="1" entry_id="{embed:entry_id}" }
		<!-- {entry_id}  {title}-->
			{if product_related_1 OR product_related_2 OR product_related_3}
				<h3>You Might Also Like</h3>
			{/if}
			{related_entries id="product_related_1"}
				<div class="related_item">
					<a href="{entry_id_path='{embed:template_group}/product_detail'}">
						<img src="{product_thumbnail}" />
						{title}<br />
						{product_price} &raquo;
					</a>
				</div>
			{/related_entries}

			{related_entries id="product_related_2"}
				<div class="related_item">
					<a href="{entry_id_path='{embed:template_group}/product_detail'}">
						<img src="{product_thumbnail}" />
						{title}<br />
						{product_price} &raquo;
					</a>
				</div>
			{/related_entries}

			{related_entries id="product_related_3"}
				<div class="related_item">
					<a href="{entry_id_path='{embed:template_group}/product_detail'}">
						<img src="{product_thumbnail}" />
						{title}<br />
						{product_price} &raquo;
					</a>
				</div>
			{/related_entries}
 	{/exp:channel:entries}
	{/if}
 		]]>
		</template>
		<template template_name='.login_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299525730' last_author_id='902' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{if logged_out}
<div id="login_form" style="display:none;">
<p>
{exp:member:login_form return="{site_url}"}
	<label>Username</label> <input type="text" name="username" value=""  maxlength="32" class="input" size="25" />
	<label>Password</label> <input type="password" name="password" value="" maxlength="32" class="input" size="25" />
	<input type="submit" name="submit" value="Submit" />
{/exp:member:login_form}
</p>
<p>Not a member? <a href="{path=member/register}">Create an Account</a></p>
</div>
{/if}		]]>
		</template>
		<template template_name='contact' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299543978' last_author_id='902' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='27'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="index"}
 
{embed="{template_group}/.header" title="Contact Us" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}	
    <div class="product">
	<div class="left_column">
	<h1>Contact Us</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat .</p>
	<!-- This uses the Email Module's Contact Form: http://expressionengine.com/docs/modules/email/contact_form.html -->
			{exp:email:contact_form user_recipients="false" recipients="info@mightybigrobot.com" charset="utf-8"}
			<fieldset id="contact_fields">
			<label for="from">
				<span>Your Email:</span>
				<input type="text" id="from" name="from" value="{member_email}" />
			</label>

			<label for="subject">
				<span>Subject:</span>
				<input type="text" id="subject" name="subject" size="40" value="Contact Form" />
			</label>

			<label for="message">
				<span>Message:</span>
				<textarea id="message" name="message" rows="18" cols="40">Email from: {member_name}, Sent at: {current_time format="%Y %m %d"}</textarea>
			</label>
			</fieldset>

			<fieldset id="contact_action">
				<p>We will never pass on your details to third parties.</p>
				<input name="submit" type='submit' value='Submit' id='contactSubmit' />
			</fieldset>
			{/exp:email:contact_form}
	</div>
	<div class="right_column">
<h1>Address</h1>
		 <p>
			12345 Fake Street<br />
			Anytown<br />
			Missouri<br />
			55555
			 </p>
	</div>
	<div class="clear_both"></div>
</div>

	
{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}		]]>
		</template>
		<template template_name='about' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784080' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='13'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="index"}
 
{embed="{template_group}/.header" title="About Us" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}	

	<div class="left_column">
	<h1>About Us</h1>
	<p>Putamus parum claram anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima eodem. Tempor cum soluta nobis eleifend, option congue nihil. At vero eros et accumsan et iusto odio dignissim qui blandit praesent.
</p>
	<p>Nisl ut aliquip ex ea commodo consequat duis autem vel eum. Ut laoreet dolore magna aliquam erat volutpat ut wisi enim ad minim veniam. Formas humanitatis per, seacula quarta decima et quinta.
</p>
	<p>Per seacula quarta decima et quinta decima eodem modo typi qui nunc nobis? Cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim? Adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat ut. Claritas est etiam processus dynamicus qui sequitur mutationem consuetudium lectorum mirum est notare quam. Ea commodo consequat duis autem vel eum iriure dolor in hendrerit in?
</p>
	</div>
	<div class="right_column">
		<img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/main_pic.jpg" />
		<p>A sweet caption for that sweet placeholder pic.</p>
	</div>
	<div class="clear_both"></div>


	
{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}		]]>
		</template>
		<template template_name='donate' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784090' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='24'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="donate"}
 
{embed="{template_group}/.header" title="Donate" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}	

	<div class="left_column">
	<h1>Make a Donation</h1>
	<p>I would like to donate to your cause.</p>
 
	{exp:cartthrob:add_to_cart_form 
	    return="{template_group}/view_cart" 
		allow_user_price="yes"
		title="Donation"
		no_shipping="yes"
		no_tax="yes"
	    on_the_fly="true"  

		}
		<p>Donation Amount:  <input type="text" maxlength="7" size="5" name="price"></p>
 
		<p>Donation Note: {item_options:input:personal_message value="" }</p>
		</p>
	    <input type="submit" value="Submit" />
	{/exp:cartthrob:add_to_cart_form}
	</div>
	<div class="right_column">
		<img src="{site_url}themes/third_party/cartthrob/store_themes/basic_white/images/main_pic.jpg" />
	</div>
	<div class="clear_both"></div>

	
{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}		]]>
		</template>
		<template template_name='.purchased_product' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299568388' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:channel:entries channel="products" entry_id="{embed:entry_id}"}
<div class="product">
	<h3>{title}</h3>
	{if product_thumbnail}
		<a href="{entry_id_path='{template_group}/product_detail'}"><img src="{product_thumbnail}" /></a>
	{/if}
	<p>
		Purchase Price: {embed:price} 
	<br /><a href="{entry_id_path={template_group}/product_detail}">Review &raquo;</a></p>
	
	{if product_download_url}
		{exp:cartthrob:download_file_form file="{product_download_url}"  }
			<input type="submit" value="Download Now!" /> 
		{/exp:cartthrob:download_file_form}
 	{/if}
</div>
{/exp:channel:entries}		]]>
		</template>
		<template template_name='account' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299769899' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='9'>
		<![CDATA[
{preload_replace:template_group="cart"}
{preload_replace:template="account"}
 
{embed="{template_group}/.header" title="Account / Purchased Items" template_group="{template_group}" template="{template}" }
 
</head>
<body>
	<div id="container">
		<div id="main">
			{embed="{template_group}/.store_header" template_group="{template_group}" template="{template}"}
			{embed="{template_group}/.top_nav" template_group="{template_group}" template="{template}"}

					<div class="left_column">
						<h1>Previous Purchases</h1>
 
							{exp:channel:entries channel="purchased_items" limit="30"  dynamic="off" author_id="CURRENT_USER"}
								{embed="{template_group}/.purchased_product" entry_id="{purchased_id}" price="{purchased_price}"}
								{if purchased_license_number}
									<p>License number: {purchased_license_number}</p>
								{/if}
  							{/exp:channel:entries}
					</div>
					<div class="right_column">
					{exp:cartthrob:cart_items_info limit="1" order_by="entry_id" sort="desc"}
 						{embed="{template_group}/.right_column" entry_id="{entry_id}" template_group="{template_group}" template="{template}"}
					{/exp:cartthrob:cart_items_info}
					</div>
					<div class="clear_both"></div>

 				{embed=cart/.footer_nav template="{template}" template_group="{template_group}"}
  		</div>
	</div>
		
{embed=cart/.footer}
		]]>
		</template>
		<template template_name='.add_to_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299767783' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:add_to_cart_form 
	entry_id="{embed:entry_id}" 
	no_tax="{embed:no_tax}"
	no_shipping = "{embed:no_shipping}"
	return="{embed:template_group}/view_cart"} 
	<p>
		{embed="cart/.item_options" entry_id="{entry_id}"}

	</p>
	<p>
	{if "{embed:inventory}" > 0}
		<label for="product_quantity">Quantity</label>
		<input type="text" id="product_quantity" name="quantity" size="8"  />
	{if:else}
		<strong>This item is out of stock</strong>
	{/if}
	</p>
	<input type="submit" value="Add to Cart" />

{/exp:cartthrob:add_to_cart_form}		]]>
		</template>
	</template_group>
	<template_group group_name='cart_examples' group_order='1' is_site_default='n'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295726059' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='41'>
		<![CDATA[
{embed="cart_includes/.header"}
{embed="cart_includes/.footer"}		]]>
		</template>
		<template template_name='donations' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295542076' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='31'>
		<![CDATA[
{preload_replace:template_group="cart_examples"}
{preload_replace:template="donations"}
{embed=cart_includes/.header title="Taking Donations" }
</head>
<body>
	
	<h1>Taking Donations</h1>
	<p>This page shows an example of how to take donations of any amount</p>

	{!-- ADD A PRODUCT --}
    <div class="store_block">
	<h2>Make a Donation</h2>
{!-- 
	This "product" is created on the fly. The parameters below all relate to 
	adding products to the cart that aren't stored in a channel
	
	parameters
	return: the redirect location
	allow_user_price: allows the user to set a price. If not set, user price is ignored for security's sake 
	title: adds a title to this dynamic product
	no_shipping: controls whether shipping is applied to this product
	no_tax: controls whether tax is applied
	on_the_fly: creates a dynamic product 
--}
	{exp:cartthrob:add_to_cart_form 
	    return="{template_group}/{template}" 
		allow_user_price="yes"
		title="Donation"
		no_shipping="yes"
		no_tax="yes"
	    on_the_fly="true"  
		}
		<p>
			Donation Amount:  <input type="text" maxlength="7" size="5" name="price"> 
			
			{!-- Adding a personal_message to the donation. No field called personal_message exists,
				but if you are using the "Cartthrob Order Items" custom field type in your Orders channel... 
				this message will still be dynamically added to the order data. 
				See the add_to_cart_form documentation for more details
				 --}
			Donation Note: {item_options:input:personal_message value="" }<br />
		</p>
	    <input type="submit" value="Submit" />
	{/exp:cartthrob:add_to_cart_form}
	</div>

	{embed="cart_includes/view_cart" template_group="{template_group}" template="{template}" }

	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_to_cart_form">add_to_cart_form</a></li>
		</ul>
		
		<h2>Concepts used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/sub_pages/purchased_items_overview">Purchased Items channel</a></li>
			<li><a href="http://cartthrob.com/docs/sub_pages/orders_overview">Orders channel</a></li>
		</ul>
	</div>
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>

</body>
</html>		]]>
		</template>
		<template template_name='register_at_checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299093787' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='7'>
		<![CDATA[
{preload_replace:template_group="cart_examples"}
{preload_replace:template="register_at_checkout"}
{embed=cart_includes/.header title="Create Member During Checkout" }
</head>
<body>
	<h1>Create Member At Checkout</h1>
	<p>Use this form to create a member during checkout. 
		Please review the standard checkout_form first for additional details</p>
	{if segment_3 !="order_complete"}
		{!-- CHECKOUT --}
		<div class="store_block">
		<h2>Checkout</h2>
		{!-- 
			parameters
			
			group_id: the group_id you would like to add your customer to.
			Any group_ids lower than 5 are automatically changed to 5 for security purposes. 
			Leaving this value blank in conjunction with create_user, will make new users part of the "member" group (5).
			create_user: whether a user should be created or not. 
			--}
		{if logged_out}
			{exp:cartthrob:checkout_form 
				group_id="6"
				create_user="TRUE"
				return="{template_group}/{template}/order_complete"}

				{gateway_fields}
			
				{!-- In addition to the standard gateway fields, 
					you will need the following fields to create a user during checkout.
					--}
					<p>	
						Username: 								
						<input type="text" name="username" />									
					</p>
					<p>									
						Screen Name: <input type="text" name="screen_name" />
					</p>									
					<p>
						Password: 
						<input type="password" name="password" />
						<input type="password" name="password_confirm" />
					</p>									
					<p>
						Email: 
						<input type="text" name="email_address" />
					</p>	
				<input type="submit" value="Checkout" />
			{/exp:cartthrob:checkout_form}
		{if:else}
			You are logged in as {screen_name}
			{exp:cartthrob:checkout_form 
				return="{template_group}/{template}/order_complete"}

				{gateway_fields}
			
				<input type="submit" value="Checkout" />
			{/exp:cartthrob:checkout_form}
		{/if}
		</div>
	{if:else}

		{exp:cartthrob:submitted_order_info}
		    <div class="store_block">
				{if authorized}
					Your transaction is complete!<br />
			        Transaction ID: {transaction_id}<br />
			        Your total: {cart_total}<br />
			        Your order ID: {order_id}
			    {if:elseif processing}
					Your transaction is being processed!<br />
			        Transaction ID: {transaction_id}<br />
			        Your total: {cart_total}<br />
			        Your order ID: {order_id}
			    {if:elseif declined}
			        Your credit card was declined: {error_message}
			    {if:elseif failed}
			        Your payment failed: {error_message}
			    {if:else}
			        Your payment failed: {error_message}
			    {/if}
			</div>
		{/exp:cartthrob:submitted_order_info}
	{/if}

	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/checkout_form">checkout_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/submitted_order_info">submitted_order_info</a></li>
		</ul>
	</div>
	
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='single_page_checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299270092' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='548'>
		<![CDATA[
{preload_replace:template_group="cart_examples"}
{preload_replace:template="single_page_checkout"}
{embed=cart_includes/.header title="Single Page Store" }
</head>
<body>
 
	<h1>Single Page Store</h1>
	<p>This single page is an example of how you can use one page to add, update, and delete items, as well as checkout. 
		This is the most in depth single page sample 
	
	</p>
 	
	{!-- ORDER COMPLETE MESSAGES --}
	{!-- the submitted_order_info tag outputs information from the last attempted order.
		Even if the customer leaves this page and returns, the information from the customer's 
		last attempted purchase will be output. 
		 --}
	{exp:cartthrob:submitted_order_info}
	    <div class="store_block">
			{if authorized}
			{!-- This content displays if the purchase was successful--}
				Your transaction is complete!<br />
		        Transaction ID: {transaction_id}<br />
		        Your total: {cart_total}<br />
		        Your order ID: {order_id}
			{!-- This content displays if the purchase is only partly completed--}
		    {if:elseif processing}
				Your transaction is being processed!<br />
		        Transaction ID: {transaction_id}<br />
		        Your total: {cart_total}<br />
		        Your order ID: {order_id}
		    {if:elseif declined}
			{!-- This content displays if the purchase was declined--}
		        Your credit card was declined: {error_message}
		    {if:elseif failed}
			{!-- This content displays if the purchase completely fails--}
		        Your payment failed: {error_message}
		    {/if}
		</div>
	{/exp:cartthrob:submitted_order_info}

	{!-- ADD A PRODUCT --}
    <div class="store_block">
	<h2>Add Products</h2>
	{!-- outputting products stored in one of the "products" channels. 
		Product channels are no different than standard channels. --}
	{exp:channel:entries channel="products" limit="10"}
 		{!-- 
			 The add_to_cart_form adds 1 or more of a product to the cart

			parameters
			return: the page the customer will be redirected to after adding this item to the cart
			entry_id: the product's entry_id
		--}
		
		{exp:cartthrob:add_to_cart_form 
			entry_id="{entry_id}" 
			return="{template_group}/{template}"}
			<p>
				Product name: {title} Price: {product_price}<br />
				Quantity: <input type="text" name="quantity" size="5" value="" /> 
				<br />
				<input type="submit" value="Add to Cart">
			</p>
		{/exp:cartthrob:add_to_cart_form}
	{/exp:channel:entries}
	</div>

	{!-- VIEW CART CONTENTS / UPDATE QUANTITIES --}
	<div class="store_block">
	<h2>Cart Contents</h2>
 
	{!-- cart_items_info outputs information about your current cart, including products in the cart, weight, and prices. 
		you can combine this with update_cart_form, to output cart item data, and update cart info
		--}
		{exp:cartthrob:cart_items_info}
 		
			{if no_results}
			    <p>There is nothing in your cart</p>
			{/if}
		{!-- outputting content that's only applicable for the first item. --}
		{if first_row}
			{!-- adding an update_cart_form to the first row. --}
			{!-- update_cart_form allows you to edit the information of one or more items in the cart at the same time
				as well as save customer information, and shipping options. --}
			{exp:cartthrob:update_cart_form 
				return="{template_group}/{template}"}
		
			<h3>Customer Info</h3>
			{!-- customer_info outputs customer info that is current stored in session.
				update_cart_form, checkout_form, save_customer_info form and other forms
				can all save customer data.
				 --}
				
			{exp:cartthrob:customer_info}
				First Name: <input type="text" name="first_name" value="{customer_first_name}" /><br />
				Last Name: <input type="text" name="last_name" value="{customer_last_name}" /><br />
				Email Address:	<input type="text" name="email_address" value="{customer_email_address}" /><br />
				State: <input type="text" name="state" value="{customer_state}" /><br />
				Country: <input type="text" name="country_code" value="{customer_country_code}" /><br />
				Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
			{/exp:cartthrob:customer_info}
		    <table>
		        <thead>
		            <tr>
		                <td>Item</td>
		                <td colspan="2">Quantity</td>
		            </tr>
		        </thead>
		        <tbody>
				{/if}
		        <tr>
	                <td>{title}</td>
	                <td>
						{!-- you can reference products by entry_id and row_id. If you sell configurable 
							items (like t-shirts with multiple sizes) you should use row_id to edit and 
							delete items, otherwise, all items with that entry id
							are affected, regardless of configuration --}
                       	<input type="text" name="quantity[{row_id}]" size="2" value="{quantity}" />
	                </td>
	                <td>
						{!-- This deletes one item (row_id) at a time--}
						<input type="checkbox" name="delete[{row_id}]"> Delete this item
	                </td>
	            </tr>
				{if last_row}
				{!-- outputting content that's only applicable for the last item. --}
		            <tr>
		                <td>
							{!-- these are just some of the variables available within the cart_items_info tag --}
		                    <p>
								Subtotal: {cart_subtotal}<br />
			                    Shipping: {cart_shipping}<br />
			                    Tax: {cart_tax}<br /> 
								{!--tax is updated based on user's location. To create a default tax price, set a default tax region in the backend --}
								Shipping Option: {shipping_option}<br />
								Tax Name: {cart_tax_name}<br />
								Tax %: {cart_tax_rate}<br />
 								Discount: {cart_discount}<br />
			                    <strong>Total: {cart_total}</strong>
							</p>
							<p>
								{!-- total quantity of all items in cart --}
								Total Items: {exp:cartthrob:total_items_count}<br />
								{!-- total items in cart --}
								Total Unique Items: {exp:cartthrob:unique_items_count}
							</p>
		                </td>
		                <td colspan="2">&nbsp;</td>
		            </tr>
		        </tbody>
		    </table>
			<input type="submit" value="Update Cart" />
			{/exp:cartthrob:update_cart_form}
		{/if}
	{/exp:cartthrob:cart_items_info}
	</div>

	{!-- ADD COUPON --}
	<div class="store_block">
	<h2>Add Coupon</h2>
	{!--  add_coupon_form tag outputs an add_coupon form--}
	{exp:cartthrob:add_coupon_form 
		return="{template_group}/{template}"}
		<input type="text" name="coupon_code" /> use code 5_off if you're demoing this on CartThrob.net<br />
		<input type="submit" value="Add" />
	{/exp:cartthrob:add_coupon_form}
	</div>
	
	{!-- CHECKOUT --}
	<div class="store_block">
	<h2>Checkout</h2>
	{!-- checkout_form tag outputs a checkout form--}
	{!-- There are many parameters available for the checkout form. 
			gateway: you can manually set the gateway. If this parameter isn't set, the default gateway will be used
		  --}
	{exp:cartthrob:checkout_form 
 		return="{template_group}/{template}/order_complete"}
		
		{!-- The gateway_fields template variable to output fields required by your currently selected gateway 
			what you see on the front end changes based on the gateway's requirements.--}
		{gateway_fields}
		<br />
		<input type="submit" value="Checkout" />
	{/exp:cartthrob:checkout_form}
	</div>
 	
	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_to_cart_form">add_to_cart_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_coupon_form">add_coupon_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/checkout_form">checkout_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/save_customer_info_form">save_customer_info_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/submitted_order_info">submitted_order_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/update_cart_form">update_cart_form</a></li>
		</ul>
	</div>
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='software' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784244' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='8'>
		<![CDATA[
{preload_replace:template_group="cart_examples"}
{preload_replace:template="software"}
{embed=cart_includes/.header title="Software Store" }

</head>
<body>
 
	<h1>Software Store</h1>
	<p>This page shows an example of how you can protect your downloadable products</p>
 
	<div class="store_block">
		<h2>Your Previous Software Purchases</h2>
		{exp:channel:entries channel="purchased_items" author="CURRENT_USER" search:purchased_product_download_url="not IS_EMPTY"}
			{if no_results}
				<h3>You have not purchased any software, but here's a link anyway</h3>
				{!-- this is where you would add a link to the file the customer just purchased 
				to keep things simple, i've just added a link to a file that ships with 
				CartThrob so you can test the download & encryption. 
				--}
				{exp:cartthrob:download_file_form 
					file="{site_url}themes/third_party/cartthrob/images/cartthrob_logo_bg.jpg" }
					<input type="submit" value="submit" /> 
				{/exp:cartthrob:download_file_form}
 			{/if}
			{!-- 
				There are 2 methods for providing downloads: download_file_form, and get_download_link. 
				Download_file_form has the advantage of hiding the link. 
				get_download_link will allow you to output an encrypted link that can be tied to a group or member id.
				 --}
			<a href="{exp:cartthrob:get_download_link 
						file='{site_url}themes/third_party/cartthrob/images/cartthrob_logo_bg.jpg'
						member_id ='{logged_in_member_id}'}"> download {title}</a>
			<br />
		{/exp:channel:entries}
	</div>	
 
	
	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/download_file_form">download_file_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/get_download_link">get_download_link</a></li>
		</ul>
		<h2>Concepts used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/sub_pages/purchased_items_overview">Purchased Items channel</a></li>
		</ul>
	</div>
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>

</body>
</html>		]]>
		</template>
		<template template_name='tshirt' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299097354' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='40'>
		<![CDATA[
{preload_replace:template_group="cart_examples"}
{preload_replace:template="tshirt"}
{embed=cart_includes/.header title="T-Shirt Store" }
</head>
<body>
	<h1>T-Shirt Store</h1>
	<p>This example shows how you can add options that your customers can select, some options affect the price, and some do not.</p>

	{!-- ADD A PRODUCT --}
    <div class="store_block">
		<h2>Add T-Shirts</h2>

		{!-- outputting products stored in one of the "products" channels. These are exactly the same as normal 
			product channels, so the channel names may be different from what is listed below --}
		{exp:channel:entries channel="products" limit="10"}
			{!-- The add_to_cart_form adds 1 or more of a product to the cart --}
			{exp:cartthrob:add_to_cart_form 
				entry_id="{entry_id}" 
				return="{template_group}/{template}"}
				<p>
					T-Shirt name: {title} Tee  -- Price: {product_price}<br />
					Quantity: <input type="text" name="quantity" size="5" value="" /> 
					<input type="submit" value="Add to Cart">
					<br />
					{item_options:select:product_size}
						<option value="{option_value}">{option_name} {price}</option>
					{/item_options:select:product_size}
					{item_options:select:product_color}
						<option value="{option_value}">{option_name} {price}</option>
					{/item_options:select:product_color}
					{item_options:select:product_options}
						<option value="{option_value}">{option_name} {price}</option>
					{/item_options:select:product_options}
					
					{!-- Some major magic happens here. This is the item_options variable.
						It can be used in conjunction with a "Cartthrob Price Modifiers" field from your channel, 
						and can automatically create and populate input and select fields with the data from that custom field. 
						
						A. 
						It can be used singly like this: 
						{item_options:select:YOUR_FIELD_NAME}
						and a select dropdown with your values will be output

						B. 
						You can use it as a variable pair like this: 
						 {item_options:select:YOUR_FIELD_NAME}
							<option value="{option_value}">{option_name} $ {price}</option>
						{/item_options:select:YOUR_FIELD_NAME}
						option, option_name, and price are variables associated with the Cartthrob Price Modifiers custom field type.
						Associated prices are automatically figured. 
						
						C.
						OR, you can add optoions on the fly like this: 
						<select name="item_options[whatevs]">
							<option value="S">Small</option>
							<option value="M">Medium</option>
							<option value="L">Large</option>
						</select>
						
						D. 
						OR This:  
						{item_options:select:size class="size_box" values="S:Small|M:Medium|L:Large" attr:rel="external"}
						In both option C and D above, prices aren't modified dynamically. 
						
						There are lots of ways to use the item_options variable. It's one of the most powerful features of CartThrob, 
						but possibly a bit complicated to grasp at first. Please feel free to post questions in the CartThrob forums
						--}
				</p>
			{/exp:cartthrob:add_to_cart_form}
		{/exp:channel:entries}
	</div>

	{!-- VIEW CART CONTENTS / UPDATE QUANTITIES --}
	<div class="store_block">
	<h2>Cart Contents</h2>
	{!-- cart_items_info outputs information about your current cart, including products in the cart, weight, and prices. --}
	{exp:cartthrob:cart_items_info}
		{if no_results}
		    <p>There is nothing in your cart</p>
		{/if}
		{!-- outputting data that's only applicable for the first item. --}
		{if first_row}
			{!-- update_cart_form allows you to edit the information of one or more items in the cart at the same time
				as well as save customer information, and shipping options. --}
    		{exp:cartthrob:update_cart_form 
				return="{template_group}/{template}"
				}
 		{/if}
			<p>
				{if item_options:product_color  } Color: {item_options:product_color}{/if} 
				{if item_options:product_size  } Size: {item_options:product_size}{/if}
{if item_options:product_options}{item_options:product_options:label}{/if}
			</p>
			<strong>{title}</strong> -- {item_price}
			{!-- The following will generate a item_options select boxes. With the parameter row_id="true" 
				the select boxes will automatically add the all-important row_id to the output. The final select box will look
				something like this: 
				<select name="item_options[1][product_size]">
				--}
{!--
			{item_options:select:product_size row_id="{row_id}"}
			{item_options:select:product_color row_id="{row_id}"}
--}

			{!-- you can reference products by entry_id and row_id. If you sell configurable 
				items (like t-shirts with multiple sizes) you should use row_id to edit and 
				delete items, otherwise, all items with that entry id
				are affected, regardless of configuration --}
				<input type="text" name="quantity[{row_id}]" size="2" value="{quantity}" />
			{!-- This deletes one item (row_id) at a time--}
				<input type="checkbox" name="delete[{row_id}]"> Delete this item
			{!-- outputting data that's only applicable for the last item. --}
			
		{if last_row}
			{!-- a clear_cart input can be used to remove all items in the cart --}
	    	<input type="submit" name="clear_cart" value="Empty Cart" />
	    	<input type="submit" value="Update Cart" />
			{/exp:cartthrob:update_cart_form}
		{/if}
	{/exp:cartthrob:cart_items_info}
	</div>

 	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_to_cart_form">add_to_cart_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_to_cart_form/#var_item_options:select:your_option_name">add_to_cart_form: item_options</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info/#var_item_options:your_option">cart_items_info: item_options</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/submitted_order_info">submitted_order_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/update_cart_form">update_cart_form</a></li>
		</ul>
		<h2>Concepts used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/sub_pages/price_modifiers/">Price Modifiers</a></li>
		</ul>
	</div>
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>

</body>
</html>		]]>
		</template>
		<template template_name='dynamic_product' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298325505' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='46'>
		<![CDATA[
{exp:cartthrob:add_to_cart_form 
entry_id="123"
quantity="2"
on_the_fly="y"
title="Red T-Shirt"
price="9.95"
shipping="4.95"
weight="20"
no_tax="yes"
no_shipping="yes"
allow_user_price="yes"
allow_user_shipping="yes"
allow_user_weight="yes"
license_number="yes"
item_options:shirt_size="XXL"
item_options:shirt_color="red"
expiration_date="365"
return="cart_examples/dynamic_product"
}

<input type="submit" value="Add dynamic product to cart" /> 

{/exp:cartthrob:add_to_cart_form}


{exp:cartthrob:add_to_cart_form 
on_the_fly="y"
allow_user_price="yes"
allow_user_shipping="yes"
allow_user_weight="yes"
license_number="yes"
expiration_date="365"
	return="cart_examples/dynamic_product"
}

<input type="text" name="quantity" value="2" /> 
<input type="text" name="title" value="Red T-Shirt" />
<input type="text" name="price" value="9.95"/>
<input type="text" name="shipping" value="4.95"/>
<input type="text" name="weight" value="20"/>
<input type="text" name="item_options[shirt_size]" value="XXL"/>
<input type="text" name="item_options[shirt_color]" value="red"/>


<input type="submit" value="Add donation type product to cart" /> 

{/exp:cartthrob:add_to_cart_form}



<h2>Cart Items</h2>
{exp:cartthrob:cart_items_info}
entry_id                {entry_id}                <br />
title                   {title}                   <br />
url_title               {url_title}               <br />
quantity                {quantity}                <br />
row_id                  {row_id}                  <br />
item_price              {item_price}              <br />
item_subtotal           {item_subtotal}           <br />
item_shipping           {item_shipping}           <br />
item_weight             {item_weight}             <br />
total_items             {total_items}             <br />
total_unique_items      {total_unique_items}      <br />
cart_total              {cart_total}              <br />
cart_subtotal           {cart_subtotal}           <br />
cart_tax                {cart_tax}                <br />
cart_shipping           {cart_shipping}           <br />
cart_tax_name           {cart_tax_name}           <br />
cart_discount           {cart_discount}           <br />
cart_count              {cart_count}              <br />
entry_id_path           {entry_id_path}           <br />
row_id_path             {row_id_path}             <br />
item_options:shirt_size {item_options:shirt_size} <br />
item_options:shirt_color {item_options:shirt_color}<br />
switch                  {switch}                  <br />



<hr />
{/exp:cartthrob:cart_items_info}		]]>
		</template>
		<template template_name='shipping_test' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299270125' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='85'>
		<![CDATA[
{embed=cart_includes/.header title="Single Page Store" }
</head>
<body>
	<h1>Single Page Store</h1>
	<p>This single page is an example of how you can use one page to add, update, and delete items, as well as checkout</p>

	{!-- ORDER COMPLETE MESSAGES --}
	{!-- The "return" paramater of the checkout form below is set back to this page with "order_complete" in the URL. 
		This saves creating a template specifically to handle order info. --}
	{if segment_2=="order_complete"}
		{!-- the submitted_order_info tag returns information from the last attempted order. --}
		{exp:cartthrob:submitted_order_info}
		    <div class="store_block">
				{if authorized}
					Your transaction is complete!<br />
			        Transaction ID: {transaction_id}<br />
			        Your total: {cart_total}<br />
			        Your order ID: {order_id}
			    {if:elseif processing}
					Your transaction is being processed!<br />
			        Transaction ID: {transaction_id}<br />
			        Your total: {cart_total}<br />
			        Your order ID: {order_id}
				{if:elseif declined}
			        Your credit card was declined: {error_message}
			    {if:elseif failed}
			        Your payment failed: {error_message}
			    {if:else}
			        Your payment failed: {error_message}
			    {/if}
			</div>
		{/exp:cartthrob:submitted_order_info}
	{/if}

	{!-- ADD A PRODUCT --}
    <div class="store_block">
	<h2>Add Products</h2>
	{!-- outputting products stored in one of the "products" channels. These are exactly the same as normal 
		product channels, so the channel names may be different from what is listed below --}
	{exp:channel:entries channel="products" limit="10"}
		{!-- The add_to_cart_form adds 1 or more of a product to the cart --}
		{exp:cartthrob:add_to_cart_form 
			entry_id="{entry_id}" 
			return="cart_examples/shipping_test"}
				<p>Product name: {title} <br />
				Quantity: <input type="text" name="quantity" size="5" value="" /> <input type="submit" value="Add to Cart">
				<br />
				Price: ${product_price}<br />
				
				</p>
		{/exp:cartthrob:add_to_cart_form}
	{/exp:channel:entries}
	</div>

	{!-- VIEW CART CONTENTS / UPDATE QUANTITIES --}

	<div class="store_block">
	<h2>Cart Contents</h2>
	{!-- cart_items_info outputs information about your current cart, including products in the cart, weight, and prices. --}
		{exp:cartthrob:cart_items_info}
		{if no_results}
		    <p>There is nothing in your cart</p>
		{/if}
		{!-- outputting data that's only applicable for the first item. --}
		{if first_row}
			{exp:cartthrob:update_cart_form 
				return="cart_examples/shipping_test"}
		
			<h3>Customer Info</h3>
				{exp:cartthrob:customer_info}
					First Name: <input type="text" name="first_name" value="{customer_first_name}" /><br />
					Last Name: <input type="text" name="last_name" value="{customer_last_name}" /><br />
					Email Address:	<input type="text" name="email_address" value="{customer_email_address}" /><br />
					State: {exp:cartthrob:state_select name="state" selected="{customer_state}"}<br />
					Country: {exp:cartthrob:country_select name="country_code" selected="{customer_country_code}"}<br />
					Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
				{/exp:cartthrob:customer_info}

			{!-- update_cart_form allows you to edit the information of one or more items in the cart at the same time
				as well as save customer information, and shipping options. --}


				
			    <table>
			        <thead>
			            <tr>
			                <td>Item</td>
			                <td colspan="2">Quantity</td>
			            </tr>
			        </thead>
			        <tbody>
		{/if}
			        <tr>
		                <td>{title}</td>
		                <td>
								{!-- you can reference products by entry_id and row_id. If you sell configurable 
									items (like t-shirts with multiple sizes) you should use row_id to edit and 
									delete items, otherwise, all items with that entry id
									are affected, regardless of configuration --}

	                        	<input type="text" name="quantity[{row_id}]" size="2" value="{quantity}" />
		                </td>
		                <td>
							{!-- This deletes one item (row_id) at a time--}
								<input type="checkbox" name="delete[{row_id}]"> Delete this item
		                </td>
		            </tr>
		{if last_row}
		{!-- outputting data that's only applicable for the last item. --}
			            <tr>
			                <td>
								{!-- these are just some of the variables available within the cart_items_info tag --}
			                    <p>Subtotal: {cart_subtotal}<br />
			                    Shipping: {cart_shipping}<br />
			                    Tax: {cart_tax}<br /> 
								{!--tax is updated based on user's location. To create a default tax price, set a default tax region in the backend --}

								Shipping Option: {shipping_option}<br />
								Tax Name: {cart_tax_name}<br />
								Tax %: {cart_tax_rate}<br />
 								Discount: {cart_discount}<br />
			
			                    <strong>Total: {cart_total}</strong></p>
								<p>
								{!-- total quantity of all items in cart --}
								Total Items: {exp:cartthrob:total_items_count}<br />
								{!-- total items in cart --}
								Total Unique Items: {exp:cartthrob:unique_items_count}</p>

			                </td>
			                <td colspan="2">&nbsp;</td>
			
			            </tr>
			        </tbody>
			    </table>
	<input type="submit" value="Update Cart" />

				{/exp:cartthrob:update_cart_form}
			
			
		{/if}
	{/exp:cartthrob:cart_items_info}
    
	
	</div>

	{!-- ADD COUPON --}
	<div class="store_block">
	<h2>Add Coupon</h2>
	{!--  add_coupon_form tag outputs an add_coupon form--}
	{exp:cartthrob:add_coupon_form 
		return="cart_examples/shipping_test"}
		<input type="text" name="coupon_code" /> use code 5_off if you're demoing this on CartThrob.net<br />
		<input type="submit" value="Add" />
	{/exp:cartthrob:add_coupon_form}
	</div>

	{!-- SAVE CUSTOMER INFO --}
	<div class="store_block">
	<h2>Save Customer Info</h2>
	
	{exp:cartthrob:save_customer_info_form 
		id="myform_id" 
		name="myform_name" 
		class="myform_class" 
		return="cart_examples/shipping_test"}
 		{exp:cartthrob:customer_info}
			First Name: <input type="text" name="first_name" value="{customer_first_name}" /><br />
			Last Name: <input type="text" name="last_name" value="{customer_last_name}" /><br />
			Email Address:	<input type="text" name="email_address" value="{customer_email_address}" /><br />
			State: {exp:cartthrob:state_select name="state" selected="{customer_state}"}<br />
			Country: {exp:cartthrob:country_select name="country_code" selected="{customer_country_code}"}<br />
			Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
		{/exp:cartthrob:customer_info}
		
		<br />
		<input type="submit" value="Save" />
	{/exp:cartthrob:save_customer_info_form}
	
	</div>

	
	{!-- CHECKOUT --}
	<div class="store_block">
	<h2>Checkout</h2>
	{!--  checkout_form tag outputs a checkout form--}
	{!--- There are many parameters available for the checkout form. You may want to note: cart_empty_redirect 
		this parameter will redirect customer if there are no products in their cart.  --}
	{exp:cartthrob:checkout_form 
		gateway="dev_template"
		return="cart_examples/shipping_test"}
		{!-- The gateway_fields template variable to output fields required by your currently selected gateway 
			what you see on the front end changes based on the gateway's requirements.--}
		{gateway_fields}
		<br />
		{!-- you can add a coupon code using the "add_coupon_form" or you can add a code right here in the checkout_form --}
		Add a coupon code: <input type="text" name="coupon_code" /> <br />
		<input type="submit" value="Checkout" />
	{/exp:cartthrob:checkout_form}
	</div>
	<div class="store_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_to_cart_form">add_to_cart_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/add_coupon_form">add_coupon_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/checkout_form">checkout_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/save_customer_info_form">save_customer_info_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/submitted_order_info">submitted_order_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/update_cart_form">update_cart_form</a></li>
		</ul>
	</div>
	<div class="store_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
	</template_group>
	<template_group group_name='cart_multi_page_checkout' group_order='2' is_site_default='n'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105703' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='10'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="index"}
{embed=cart_includes/.header title="Multi-Page Store - View Cart" }

</head>
<body>
	<h1>Store: Add to Cart Page</h1>
	<p></p>
	
	
<a href="{path={template_group}/view_cart}">Next: View Cart &raquo;</a> 

<div class="store_block">
	<h2>Add Products</h2>
	{!-- outputting products stored in one of the "products" channels. 
		Product channels are no different than standard channels. --}
	{exp:channel:entries channel="products" limit="10"}
		{!-- 
			 The add_to_cart_form adds 1 or more of a product to the cart

			parameters
			return: the page the customer will be redirected to after adding this item to the cart
			entry_id: the product's entry_id
		--}
		
		{exp:cartthrob:add_to_cart_form 
			entry_id="{entry_id}" 
			return="{template_group}/{template}"}
			<p>
				Product name: {title} Price: {product_price}<br />
				Quantity: <input type="text" name="quantity" size="5" value="" /> 
				<br />
				<input type="submit" value="Add to Cart">
			</p>
		{/exp:cartthrob:add_to_cart_form}
	{/exp:channel:entries}
	</div>

	<a href="{path={template_group}/view_cart}">Next: View Cart &raquo;</a> 

	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/update_cart_form">update_cart_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/unique_items_count">unique_items_count</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/total_items_count">total_items_count</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/arithmetic">arithmetic</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			
		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105690' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="checkout"}
{embed=cart_includes/.header title="Multi-Page Store - Checkout" }

</head>
<body>
	<h1>Store: Checkout</h1>
	<p></p>
<a href="{path={template_group}/order_complete}">Next: Order Complete &raquo;</a> 

<div class="store_block">
    <h2>Checkout</h2>
    {!--  checkout_form tag outputs a checkout form--}
    {!--- There are many parameters available for the checkout form. You may want to note: cart_empty_redirect 
        this parameter will redirect customer if there are no products in their cart.  --}
    {exp:cartthrob:checkout_form 
        gateway="dev_template"
        return="{template_group}/order_complete"}

 

	{!--	
		You can use {gateway_fields} variable to output all suggested fields for this particular gateway.
		Or add the customer fields manually.
 		Add the fields show in the settings page for your selected payment gateway. 
		If you chose authorize.net, add the gateway fields that are listed with that gateway. 
		In the gateway settings page, it will also list the "required" fields for that gateway. You must include those at a minimum. 
		--}
	{gateway_fields}
	
		{!--
		{exp:cartthrob:customer_info}
	    <fieldset class="billing" id="billing_info">
	        <legend>Billing info</legend>
	        <p>        
	            <label for="first_name" class="required" >First Name</label>
	            <input type="text" id="first_name" name="first_name" value="{customer_first_name}" />
	        </p>
	        <p>        
	            <label for="last_name" class="required" >Last Name</label>
	            <input type="text" id="last_name" name="last_name" value="{customer_last_name}" />
	        </p>
	     </fieldset>
	    {/exp:cartthrob:customer_info}
		--}

    <input type="submit" value="Complete Checkout" />
    {/exp:cartthrob:checkout_form}
</div>

	<a href="{path={template_group}/order_complete}">Next: Order Complete &raquo;</a> 

	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/checkout_form">checkout_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='order_complete' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105715' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="order_complete"}
{embed=cart_includes/.header title="Multi-Page Store - Order Results" }

</head>
<body>
	<h1>Store: Order Results</h1>
	<p></p>
 
<div class="store_block">
    <h2>The results of your order</h2>
	
	{exp:cartthrob:submitted_order_info}
	    <div class="store_block">
	        {if authorized}
	            Your transaction is complete!<br />
	            Transaction ID: {transaction_id}<br />
	            Your total: {cart_total}<br />
	            Your order ID: {order_id}
		    {if:elseif processing}
				Your transaction is being processed!<br />
		        Transaction ID: {transaction_id}<br />
		        Your total: {cart_total}<br />
		        Your order ID: {order_id}
		    {if:elseif declined}
	            Your credit card was declined: {error_message}
	            <a href="{path={template_group}/checkout}">Try checking out again &raquo;</a>
	        {if:elseif failed}
	            Your payment failed: {error_message}
	            <a href="{path={template_group}/checkout}">Try checking out again &raquo;</a>
	        {/if}
	    </div>
	{/exp:cartthrob:submitted_order_info}
</div>

 
	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/submitted_order_info">submitted_order_info</a></li>
		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='review_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299270133' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="review_cart"}
{embed=cart_includes/.header title="Multi-Page Store - Review Cart" }

</head>
<body>
	<h1>Store: Review Cart Page</h1>
	<p></p>
<a href="{path={template_group}/checkout}">Next: Checkout &raquo;</a> 

<div class="store_block">
    <h2>Review Your Cart (then proceed to checkout)</h2>
 	{exp:cartthrob:cart_items_info}
	    {if no_results}
	        <p>There is nothing in your cart</p>
	    {/if}
	    {if first_row}
	        <h3>Customer Info</h3>
	        {exp:cartthrob:customer_info}
	            First Name: <input type="text" name="first_name" value="{customer_first_name}" /><br />
	            Last Name: <input type="text" name="last_name" value="{customer_last_name}" /><br />
	            Email Address:    <input type="text" name="email_address" value="{customer_email_address}" /><br />
	            State: {exp:cartthrob:state_select name="state" selected="{customer_state}"}<br />
	            Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
	            {!-- ADD ADDITIONAL FIELDS AS NECESSARY --}
	        {/exp:cartthrob:customer_info}

	    <h3>Items in Cart</h3>
	    {/if}


	    <strong>{title}</strong> @{item_price}<br />
	    {if last_row}
	        <p>Subtotal: {cart_subtotal}<br />
	        Shipping: {cart_shipping}<br />
	        Tax: {cart_tax}<br /> 
	        Shipping Option: {shipping_option}<br />
	        Tax Name: {cart_tax_name}<br />
	        Tax %: {cart_tax_rate}<br />
 	        Discount: {cart_discount}<br />
	        <strong>Total: {cart_total}</strong></p>

	        <p>
	        {!-- total quantity of all items in cart --}
	        Total Items: {exp:cartthrob:total_items_count}<br />
	        {!-- total items in cart --}
	        Total Unique Items: {exp:cartthrob:unique_items_count}</p>


	    {/if}
	{/exp:cartthrob:cart_items_info}

    </div>

	<a href="{path={template_group}/checkout}">Next: Checkout &raquo;</a> 

	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/arithmetic">arithmetic</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/total_items_count">total_items_count</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/unique_items_count">unique_items_count</a></li>
			
		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299105915' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="shipping"}
{embed=cart_includes/.header title="Multi-Page Store - Shipping" }

</head>
<body>
	<h1>Store: Shipping Page</h1>
	<p></p>
	
<a href="{path={template_group}/review_cart}">Next: Review Cart &raquo;</a> 

{!-- SAVE CUSTOMER INFO --}
    <div class="store_block">
    <h2>Save Customer Info / Set Shipping</h2>
    
    {exp:cartthrob:save_customer_info_form 
        id="myform_id" 
        name="myform_name" 
        class="myform_class" 
        return="{template_group}/review_cart" 
        }
            {exp:cartthrob:customer_info}
                First Name: <input type="text" name="first_name" value="{customer_first_name}" /><br />
                Last Name: <input type="text" name="last_name" value="{customer_last_name}" /><br />
                Email Address:    <input type="text" name="email_address" value="{customer_email_address}" /><br />
	            State: {exp:cartthrob:state_select name="state" selected="{customer_state}"}<br />
                Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
                {!-- ADD YOUR FIELDS AS NECESSARY --}
            {/exp:cartthrob:customer_info}
        
        {exp:cartthrob:get_shipping_options}
        <br />
        <input type="submit" value="Review Cart &raquo; " />
    {/exp:cartthrob:save_customer_info_form}
    
    </div>

	<a href="{path={template_group}/review_cart}">Next: Review Cart &raquo;</a> 

	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/save_customer_info_form">save_customer_info_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/state_select">state_select</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/country_select">country_select</a></li>

		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
		<template template_name='view_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299270140' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='6'>
		<![CDATA[
{preload_replace:template_group="cart_multi_page_checkout"}
{preload_replace:template="view_cart"}
{embed=cart_includes/.header title="Multi-Page Store - View Cart" }

</head>
<body>
	<h1>Store: View Cart Page</h1>
	<p></p>
	
	
<a href="{path={template_group}/shipping}">Next: Enter Shipping Details &raquo;</a> 

<div class="store_block">
    <h2>View / Edit Cart Contents</h2>
    {!-- cart_items_info outputs information about your current cart, including products in the cart, weight, and prices. --}
        {exp:cartthrob:cart_items_info}
        {if no_results}
            <p>There is nothing in your cart</p>
        {/if}
        {!-- outputting data that is only applicable for the first item. --}
        {if first_row}
            {exp:cartthrob:update_cart_form 
                return="cart/view_cart"}
 
            {!-- update_cart_form allows you to edit the information of one or more items in the cart at the same time
                as well as save customer information, and shipping options. --}
                <table>
                    <thead>
                        <tr>
                            <td>Item</td>
                            <td colspan="2">Quantity</td>
                        </tr>
                    </thead>
                    <tbody>
        {/if}
                    <tr>
                        <td>{title}</td>
                        <td>
                                {!-- you can reference products by entry_id and row_id. If you sell configurable 
                                    items (like t-shirts with multiple sizes) you should use row_id to edit and 
                                    delete items, otherwise, all items with that entry id
                                    are affected, regardless of configuration --}

                                <input type="text" name="quantity[{row_id}]" size="2" value="{quantity}" />
                        </td>
                        <td>
                            {!-- This deletes one item (row_id) at a time--}
                                <input type="checkbox" name="delete[{row_id}]"> Delete this item
                        </td>
                    </tr>
        {if last_row}
        {!-- outputting data that's only applicable for the last item. --}
                        <tr>
                            <td>
                                {!-- these are just some of the variables available within the cart_items_info tag --}
                                <p>Subtotal: {cart_subtotal}<br />
                                Shipping: {cart_shipping}<br />
                                Tax: {cart_tax}<br /> 
                                {!--tax is updated based on user's location. To create a default tax price, set a default tax region in the backend --}

                                Shipping Option: {shipping_option}<br />
                                Tax Name: {cart_tax_name}<br />
                                Tax %: {cart_tax_rate}<br />
                                 Discount: {cart_discount}<br />
            
                                <strong>Total: {cart_total}</strong></p>
                                <p>
                                {!-- total quantity of all items in cart --}
                                Total Items: {exp:cartthrob:total_items_count}<br />
                                {!-- total items in cart --}
                                Total Unique Items: {exp:cartthrob:unique_items_count}</p>

                            </td>
                            <td colspan="2"> </td>
            
                        </tr>
                    </tbody>
                </table>
    			<input type="submit" value="Update Cart" />
                {/exp:cartthrob:update_cart_form}
        {/if}
    {/exp:cartthrob:cart_items_info}
 
		
		
    </div>

	<a href="{path={template_group}/shipping}">Next: Enter Shipping Details &raquo;</a> 

	<div class="doc_block">
		<h2>Tags used in this template</h2>
		<ul>
			<li><a href="http://cartthrob.com/docs/tags_detail/update_cart_form">update_cart_form</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/cart_items_info">cart_items_info</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/unique_items_count">unique_items_count</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/total_items_count">total_items_count</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/arithmetic">arithmetic</a></li>
			<li><a href="http://cartthrob.com/docs/tags_detail/customer_info">customer_info</a></li>
			
		</ul>
	</div>
	<div class="doc_block">
		{embed=cart_includes/.footer}
	</div>
</body>
</html>		]]>
		</template>
	</template_group>
	<template_group group_name='cart_includes' group_order='3' is_site_default='n'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540427' last_author_id='0' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0' />
		<template template_name='add_to_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540706' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:channel:entries channel="products" limit="10" dynamic="off" }
	{if no_results}
		<p>Please add some items to your products channel</p>
	{/if}
	{exp:cartthrob:add_to_cart_form 
		entry_id="{entry_id}" 
		return="{embed:return_template_group}/{embed:return_template}"}
		<p>Product name: {title} -- {product_price}<br />
			Quantity: <input type="text" name="quantity" size="5" value="" /> 
			
			<input type="submit" value="Add to Cart">
		</p>
	{/exp:cartthrob:add_to_cart_form}
{/exp:channel:entries}		]]>
		</template>
		<template template_name='cart_shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540726' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_shipping}		]]>
		</template>
		<template template_name='cart_tax' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540739' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_tax}		]]>
		</template>
		<template template_name='cart_total' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540752' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_total}		]]>
		</template>
		<template template_name='checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540760' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
	{!-- CHECKOUT --}
	<div class="store_block">
	<h2>Checkout</h2>
	{!-- the checkout_form  outputs a checkout form--}
	{!-- overriding the chosen gateway with the the dev_template gateway here --}
	{exp:cartthrob:checkout_form gateway="dev_template" return="{embed:template_group}/{embed:template}/order_complete"}
		{!-- The gateway_fields template variable to output fields required by your currently selected gateway 
			what you see on the front end changes based on the gateway's requirements.--}
		{gateway_fields}
		<input type="submit" value="Checkout" />
	{/exp:cartthrob:checkout_form}
	</div>		]]>
		</template>
		<template template_name='items_in_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540770' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_items_info}
	{title} {item_price} x {quantity}<br />
	discount: {cart_discount}
{/exp:cartthrob:cart_items_info}		]]>
		</template>
		<template template_name='view_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297457867' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
	{!-- VIEW CART CONTENTS / UPDATE QUANTITIES --}
	<div class="store_block">
		<h2>Cart Contents</h2>
		
	{!-- cart_items_info outputs information about your current cart, including products in the cart, weight, and prices. --}
	{exp:cartthrob:cart_items_info}
		{if no_results}
		<p>Your cart is empty</p>
		{/if}
		{!-- outputting data that's only applicable for the first item. --}
		{if first_row}
			<h2>Thank You.</h2>
			<p>Thank you for your donation commitment, please pay for your donation now.</p>
		{/if}
		<p>Title: {title} <br />

			Personal Message: {item_options:personal_message}<br />
			{!-- The delete URL links back to this page. 
			The segments activate the delete_from_cart tag at the top of this template.--}
			<a href="{path=cart_includes/delete_from_cart/{row_id}/{embed:template_group}/{embed:template}}">Delete</a><br />
			</p>
		
		{if last_row}
			{!-- these are just some of the variables available within the cart_items_info tag --}
			<p><strong>Total: {cart_total}</strong></p>
		{/if}
	{/exp:cartthrob:cart_items_info}
	</div>		]]>
		</template>
		<template template_name='.footer' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295726024' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<h2>Basic Store Templates for CartThrob 2</h2>
<ul>
<li><a href="{path=cart_examples/single_page_checkout}">Single Page Checkout</a></li>
<li><a href="{path=cart_examples/donations}">Taking Donations</a></li>
<li><a href="{path=cart_examples/software}">Selling Software</a></li>
<li><a href="{path=cart_examples/tshirt}">Selling Configurable Products (t-shirts)</a></li>
<li><a href="{path=cart_multi_page_checkout}">Multi-Page Checkout</a></li>
</ul>

<h2>Ajax Examples</h2>
<ul>
<li><a href="{path=cart_ajax_examples}">Selecting Gateway Dynamically</a></li>
<li><a href="{path=cart_ajax_examples}">Update checkout fields with gateway change</a></li>
<li><a href="{path=cart_ajax_examples}">Update shipping with location change</a></li>
<li><a href="{path=cart_ajax_examples}">Update tax with location change</a></li>
</ul>


<h2>Administrative Templates</h2>
<ul>
<li><a href="{path=cart_orders}">Admin Reports</a></li>
<li><a href="{path=cart_gateway_test}">Gateway Test</a></li>
</ul>		

 


{!-- uncomment to activate 
{exp:cartthrob:debug_info}
--}		]]>
		</template>
		<template template_name='.header' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295726320' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<head>
	<title>{embed:title}</title>
  	<style type="text/css">
	body{
			font: 11px "Lucida Grande", Lucida, Verdana, sans-serif;
		}
		h1{
			font-size: 18px;
			font-weight: bold;
		}
		h2{
			font-size: 16px;
			font-weight: bold;
		}
		h3{
			font-size: 13px;
			font-weight: bold;
		}
		h4{
			font-size: 12px;
			font-weight: bold;
		}
		.store_block{
			padding:12px;
			margin-top: 12px;
			margin-bottom:12px;
		background-color: #e1fefd;
	}
	</style>				]]>
		</template>
		<template template_name='delete_from_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297457882' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:delete_from_cart row_id="{segment_3}" return="{segment_4}/{segment_5}"}		]]>
		</template>
	</template_group>
	<template_group group_name='cart_ajax_examples' group_order='5' is_site_default='n'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295540505' last_author_id='0' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3' />
		<template template_name='dynamic_gateway' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295541188' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
<html>
<head>
	<title>Ajax Add to Cart Example</title>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){

    // bind to the change event of the state select in the checkout form
    $('#payment_method').change(function(){

        // display a message indicating that we're updating the tax cost
        $('#checkout_form_content').html( 'Updating...' );
        $('#checkout_form_content').load('{path=cart_ajax_examples/include_dynamic_checkout}'+$(this).val());
    });
});
</script>
</head>
<body>
	<h1>Dynamic Payment Method</h1>

<select id="payment_method" name="payment_method">
<option value="authorize_net" selected>Authorize.net</option>
<option value="dev_template">Developer Template</option> 
<option value="paypal_standard">Paypal Standard</option>
</select>
<div id="checkout_form_content">
{embed="cart_ajax_examples/include_dynamic_checkout/"}
</div>



	{exp:channel:entries channel="products" limit="1"}
		<p>Product name: {title} <br />
		Quantity: <input type="text" name="add_quantity" id="add_quantity" size="5" value="1" /> 
		<input type="hidden" value="{entry_id}" id="add_entry_id" name="add_entry_id">
		<input type="submit" id="add_submit" value="Add to Cart">
		Price: ${product_price}</p>
	{/exp:channel:entries}

	Cart Total with Tax & Shipping: <div id="cart_total">{exp:cartthrob:cart_total}</div>

</body>
</html>		]]>
		</template>
		<template template_name='include_dynamic_checkout' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295541176' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:checkout_form gateway="{segment_3}"}
{gateway_fields}
{/exp:cartthrob:checkout_form}		]]>
		</template>
		<template template_name='per_location_shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295541166' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:set_config}
{set_shipping_plugin value="shipping_by_location_price_threshold"}
{/exp:cartthrob:set_config}


{exp:cartthrob:cart_items_info}
{if no_results}
	{exp:cartthrob:add_to_cart entry_id="3" return="cart_ajax_examples/per_location_shipping"}
{/if}
{/exp:cartthrob:cart_items_info}
<html>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.form.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){

    // bind to the change event of the state select in the checkout form
    $('#shipping_country_code,#country_code').change(function(){

        // display a message indicating that we're updating the tax cost
        $('#cart_tax').html( 'Updating...' );
        $('#cart_total').html( 'Updating...' );
        $('#cart_shipping').html( 'Updating...' );

        // update the value of the hidden form field
        $('#hidden_country').val( $("#country_code").val() );
        $('#hidden_shipping_country').val( $("#shipping_country_code").val() );

        // use the jquery form plugin method ajaxSubmit to send the form
        $('#save_customer_info_form').ajaxSubmit({
            success: function(data) {
                // load the updated tax value back into the page
                $('#cart_tax').load('{path=cart_includes/cart_tax}');
                $('#cart_total').load('{path=cart_includes/cart_total}');
				$('#cart_shipping').load('{path=cart_includes/cart_shipping}');
                
            }
        });
    });
});
</script>
</head>
<body>
	<!-- somewhere in your page, have this hidden form, which will be used to update the customers state-->
	<div style="display:none;">
		{exp:cartthrob:save_customer_info_form id="save_customer_info_form"}
		<input type="hidden" name="country_code" id="hidden_country" />
		<input type="hidden" name="shipping_country_code" id="hidden_shipping_country" />
		{/exp:cartthrob:save_customer_info_form}
	</div>
	{exp:cartthrob:cart_items_info}
		{title} {item_price} x {quantity}<br />
	{/exp:cartthrob:cart_items_info}
	
	{exp:cartthrob:checkout_form}
		Shipping Country: <select name="shipping_country_code" id="shipping_country_code">
		    <option value="">--</option>
		    <option value="UK">UK</option>
		    <option value="US">US</option>
		</select>
<br /> 
		Country <select name="country_code" id="country_code">
			<option value="">--</option>
		    <option value="UK">UK</option>
		    <option value="US">US</option>
		</select>
	{/exp:cartthrob:checkout_form}
	<!-- this is your display of the cart tax -->
	{exp:cartthrob:customer_info}
		Cart Shipping: <div id="cart_shipping">{exp:cartthrob:cart_shipping}</div><br />
		Cart Subtotal: <div id="cart_sub_total">{exp:cartthrob:cart_subtotal}</div><br />
		Cart Total: <div id="cart_total">{exp:cartthrob:cart_total}</div>
		
	{/exp:cartthrob:customer_info}
	<br />


	<p>&nbsp;</p>
</body>
</html>		]]>
		</template>
		<template template_name='tax' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295541155' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_items_info}
{if no_results}
	{exp:cartthrob:add_to_cart entry_id="3" return="cart_ajax_examples/tax"}
{/if}
{/exp:cartthrob:cart_items_info}
<html>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.form.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){

    // bind to the change event of the state select in the checkout form
    $('#checkout_state').change(function(){

        // display a message indicating that we're updating the tax cost
        $('#cart_tax').html( 'Updating...' );
        $('#cart_total').html( 'Updating...' );

        // update the value of the hidden form field
        $('#hidden_state_field').val( $(this).val() );

        // use the jquery form plugin method ajaxSubmit to send the form
        $('#save_customer_info_form').ajaxSubmit({
            success: function(data) {
                // load the updated tax value back into the page
                $('#cart_tax').load('{path=cart_includes/cart_tax}');
                $('#cart_total').load('{path=cart_includes/cart_total}');

            }
        });
    });
});
</script>
</head>
<body>
	<!-- somewhere in your page, have this hidden form, which will be used to update the customers state-->
	<div style="display:none;">
		{exp:cartthrob:save_customer_info_form id="save_customer_info_form"}
			<input type="hidden" name="shipping_state" id="hidden_state_field" />
		{/exp:cartthrob:save_customer_info_form}
	</div>
	{exp:cartthrob:cart_items_info}
		{title} {item_price} x {quantity}<br />
	{/exp:cartthrob:cart_items_info}
	
	{exp:cartthrob:checkout_form}
		<select name="shipping_state" id="checkout_state">
		    <option value="NH">New Hampshire (No Sales Tax)</option>
		    <option value="AR">Arkansas (Tax)</option>
		    <option value="MO">Missouri (Tax)</option>
		</select>

	{/exp:cartthrob:checkout_form}
	<!-- this is your display of the cart tax -->
	{exp:cartthrob:customer_info}
		Tax:<div id="cart_tax">
			<!-- using a conditional, we can display a message if they haven't already selected a state -->
			{if '{customer_state}' == ''}
				You must first select your state to calculate tax.
			{if:else}
				{exp:cartthrob:cart_tax}
			{/if}
		</div>
		<br />
		Cart Shipping: <div id="cart_shipping">{exp:cartthrob:cart_shipping}</div><br />
		Cart Subtotal: <div id="cart_sub_total">{exp:cartthrob:cart_subtotal}</div><br />
		Cart Total: <div id="cart_total">{exp:cartthrob:cart_total}</div>
		
	{/exp:cartthrob:customer_info}
	<br />


	<p>&nbsp;</p>
</body>
</html>		]]>
		</template>
	</template_group>
	<template_group group_name='cart_functions' group_order='8' is_site_default='n'>
		<template template_name='index' save_template_file='n' template_type='webpage' template_notes='' edit_date='1295830597' last_author_id='0' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1' />
		<template template_name='add_coupon_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297976186' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[

{exp:cartthrob:add_coupon_form return="cart_examples/single_page_checkout"}
    <input type="text" name="coupon_code" />
    <input type="submit" name="Add Coupon" />
{/exp:cartthrob:add_coupon_form}


<br />Creates a form where coupons codes can be entered by customer
<br />http://cartthrob.com/docs/tags_detail/add_coupon_form/		]]>
		</template>
		<template template_name='add_to_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298927633' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='9'>
		<![CDATA[
{exp:channel:entries channel="products" limit="1"}
Product Name: {title} {product_price}
    {exp:cartthrob:add_to_cart_form return="cart_examples/single_page_checkout" entry_id="{entry_id}"}
	Quantity <input type="text" name="quantity"/>
	<input type="submit" /> 
    {/exp:cartthrob:add_to_cart_form}
{/exp:channel:entries}

<br />Creates a form for adding products to the cart
<br />NOTE: example requires "products" channel and entries
<br />http://cartthrob.com/docs/tags_detail/add_to_cart_form/		]]>
		</template>
		<template template_name='add_to_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298331640' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:cartthrob:add_to_cart entry_id="1" quantity="1" return="cart_examples/single_page_checkout"}

<br />This tag will add a product to the cart just automatically. 
<br />NOTE: example requires product with entry_id:1
<br />NOTE: This will redirect you to another page. 
<br />http://cartthrob.com/docs/tags_detail/add_to_cart/		]]>
		</template>
		<template template_name='also_purchased' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297976655' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:cartthrob:also_purchased entry_id="1" limit="3"}
	You might also like:  {entry_id}<br /> 
{/exp:cartthrob:also_purchased}

<br />Outputs entry_ids based on customer's past purchases.
<br />NOTE: example requires product with entry_id:1, and an existing order history
<br />http://cartthrob.com/docs/tags_detail/also_purchased/		]]>
		</template>
		<template template_name='arithmetic' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298330721' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
Item Subtotal: {exp:cartthrob:arithmetic operator="*" num1="123" num2="1.3"} (should equal 159.90)


<br />Very simple calculations tag. For more advanced calculations, we suggest using 3rd party math plugins
<br />http://cartthrob.com/docs/tags_detail/arithmetic/		]]>
		</template>
		<template template_name='cart_discount' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297976794' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:cart_discount}

<br />Outputs current discount value
<br />NOTE: example requires that a discount/coupon be active. Refer to add_coupon_code_form
<br />http://cartthrob.com/docs/tags_detail/cart_discount/		]]>
		</template>
		<template template_name='cart_empty_redirect' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298331626' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='9'>
		<![CDATA[
{exp:cartthrob:cart_empty_redirect return="cart_examples/single_page_checkout"}
If you're reading this... your cart has items in it
<a href="{path=cart_examples}">Go delete some items here &raquo; </a>

<br />Redirects customer to an alternate page if cart is empty
<br />NOTE: this will redirect to another page if the cart is empty.
<br />http://cartthrob.com/docs/tags_detail/cart_empty_redirect/
		]]>
		</template>
		<template template_name='cart_entry_ids' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297976878' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='8'>
		<![CDATA[
{exp:cartthrob:cart_entry_ids}

<br />Outputs a pipe-delimited list of entry_ids currently in the cart. 
<br />http://cartthrob.com/docs/tags_detail/cart_entry_ids/		]]>
		</template>
		<template template_name='cart_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297988080' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='220'>
		<![CDATA[
{exp:cartthrob:cart_info}
	Total Items: {total_items} <br />
	Total Unique Items: {total_unique_items} <br />
	Cart Entry IDs: {cart_entry_ids} <br />
	Cart Shipping: {cart_shipping} <br />
	Cart Tax Rate: {cart_tax_rate} <br />
	Cart Tax: {cart_tax} <br />
	Cart Subtotal: {cart_subtotal} <br />
	Cart Total: {cart_total} <br />
{/exp:cartthrob:cart_info}

<br />Outputs general information about the cart. For item specific information, use cart_items_info
<br />NOTE: example requires items in cart.
<br />http://cartthrob.com/docs/tags_detail/cart_info/		]]>
		</template>
		<template template_name='cart_items_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297988072' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='10'>
		<![CDATA[
<pre>
{exp:cartthrob:cart_items_info}
{if no_results}There's nothing in your cart{/if}
	{entry_id}
	{title}
	{url_title}
	{quantity}
	{row_id}
	{item_price}
	{item_subtotal}
	{item_shipping}
	{item_weight}
	{total_items}
	{total_unique_items}
	{cart_total}
	{cart_subtotal}
	{cart_tax}
	{cart_shipping}
	{cart_tax_name}
	{cart_discount}
	{cart_count}
	{entry_id_path=product/detail}
	{row_id_path=cart/delete_from_cart}
	{item_options:your_option}
	{item_options:your_option:option_name}
	{item_options:your_option:price}
	{switch="odd|even"}
{/exp:cartthrob:cart_items_info}
<pre>

<br />Outputs information about items in the cart
<br />NOTE: example requires items in cart
<br />http://cartthrob.com/docs/tags_detail/cart_items_info/		]]>
		</template>
		<template template_name='cart_shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297977670' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:cart_shipping}

<br />Outputs cart shipping cost
<br />http://cartthrob.com/docs/tags_detail/cart_shipping/		]]>
		</template>
		<template template_name='cart_subtotal' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297977683' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:cart_subtotal}

<br />Outputs cart subtotal
<br />http://cartthrob.com/docs/tags_detail/cart_subtotal/		]]>
		</template>
		<template template_name='cart_total' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297977718' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='95'>
		<![CDATA[
{exp:cartthrob:cart_total}

<br />Outputs cart total
<br />http://cartthrob.com/docs/tags_detail/cart_total/		]]>
		</template>
		<template template_name='duplicate_item' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978673' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='11'>
		<![CDATA[
{exp:cartthrob:duplicate_item entry_id="1"}

<br />Duplicates an item in the cart
<br />NOTE: example requires that entry_id:1 be an item in the cart.
<br />http://cartthrob.com/docs/tags_detail/duplicate_item/		]]>
		</template>
		<template template_name='multi_add_to_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297979414' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='6'>
		<![CDATA[
{exp:channel:entries channel="products" limit="5"}
	{if count == 1}
		{exp:cartthrob:multi_add_to_cart_form return="cart_examples/single_page_checkout"}
	{/if}

		Product Name: {title} {product_price}
		<input type="hidden" name="entry_id[{count}]" value="{entry_id}" />
		<input type="text" name="item_options[{count}][notes]"  />
		<input type="text" name="quantity[{count}]"  />

	{if count == total_results}
			<input type="submit" /> 
		{/exp:cartthrob:multi_add_to_cart_form}
	{/if}
{/exp:channel:entries}

<br />Outputs a form that allows you to add multiple items to the cart at the same time.
<br />NOTE: example requires "products" channel. 
<br />http://cartthrob.com/docs/tags_detail/multi_add_to_cart_form/		]]>
		</template>
		<template template_name='request_shipping_quote_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980318' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:request_shipping_quote_form
	return = "cart_functions/shipping_quote_info"
	shipping_mode="shop"
	}	
	{shipping_fields}
	<input type="submit" value="Get Costs">
{/exp:cartthrob:request_shipping_quote_form}

<br />Outputs a form for requesting shipping quotes. Only used by "Live Rates" plugins like UPS or FedEx. 
<br />http://cartthrob.com/docs/tags_detail/request_shipping_quote_form/
<br />
<br />Use in conjunction with shipping_quote_info
<br />http://cartthrob.com/docs/tags_detail/shipping_quote_info/		]]>
		</template>
		<template template_name='shipping_quote_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980672' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:shipping_quote_info}
	{if error_message}
		
You can't select a shipping method until you fix the following error: {error_message}

	{if:else}
		{!-- // display quote request if there are no errors --}
		<h2>Select one of the available shipping methods.</h2> 
		{exp:cartthrob:request_shipping_quote_form
			return = "cart_examples/single_page_checkout"
			apply_costs="TRUE"   
			shipping_mode="rate"
			}	
	 		{exp:cartthrob:customer_info}
				Zip: <input type="text" name="zip" value="{customer_zip}" /><br />
			{/exp:cartthrob:customer_info}

			{shipping_methods}
				{if item:first_item}
					<select name="shipping[product]">
				{/if}
						<option name="{item:option_value}">{item:option_title} - {item:option_price}</option>
				{if item:last_item}
					</select>
				{/if}
			{/shipping_methods}

			<input type="submit" value="Apply Costs">
		{/exp:cartthrob:request_shipping_quote_form}
	{/if}
{/exp:cartthrob:shipping_quote_info}

<br />Outputs information regarding a shipping quote
<br />Only relevant for "Live Rates" Plugins like UPS and FedEx. 
<br />http://cartthrob.com/docs/tags_detail/shipping_quote_info/		]]>
		</template>
		<template template_name='clear_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298331645' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:cartthrob:clear_cart return="cart_examples/single_page_checkout"}

<br />Clears entire cart contents
<br />NOTE: example requires items to be in cart. 
<br />NOTE: This will redirect you to another page when viewing.  
<br />http://cartthrob.com/docs/tags_detail/clear_cart/		]]>
		</template>
		<template template_name='clear_coupon_codes' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978295' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[
{exp:cartthrob:clear_coupon_codes return="cart_examples/single_page_checkout"}

<br />Clears coupon codes
<br />http://cartthrob.com/docs/tags_detail/clear_coupon_codes/		]]>
		</template>
		<template template_name='delete_from_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298331606' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[
{exp:cartthrob:delete_from_cart entry_id="1" return="cart_examples/single_page_checkout"}

<br />Deletes items from the cart
<br />NOTE: example requires that item with entry_id:1 be in cart
<br />NOTE: This will redirect you to a different page when this template is viewed
<br />http://cartthrob.com/docs/tags_detail/delete_from_cart/		]]>
		</template>
		<template template_name='cart_tax' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297977710' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:cart_tax}

<br />Outputs cart tax
<br />http://cartthrob.com/docs/tags_detail/cart_tax/		]]>
		</template>
		<template template_name='countries' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978342' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
<select name="country_code">
	{exp:cartthrob:countries}
		<option value="{country_code}">{country}</option>
	{/exp:cartthrob:countries}
</select>

<br />Outputs list of countries using standard 3 character country codes
<br />http://cartthrob.com/docs/tags_detail/countries/		]]>
		</template>
		<template template_name='country_select' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978392' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:country_select name="country_code" selected="USA"}

<br />Generates a country select
<br />http://cartthrob.com/docs/tags_detail/country_select/		]]>
		</template>
		<template template_name='cart_subtotal_plus_shipping' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297977700' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:cart_subtotal_plus_shipping}

<br />Outputs cart subtotal plus shipping cost
<br />http://cartthrob.com/docs/tags_detail/cart_subtotal_plus_shipping/		]]>
		</template>
		<template template_name='checkout_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298331003' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
<h2>Standard Checkout</h2>
{exp:cartthrob:checkout_form  
	gateway="dev_template"
	return="cart/order_info" 
	}
 	
	{gateway_fields}

	<input type="submit" value="Submit" />
{/exp:cartthrob:checkout_form}

<h2>Create User During Checkout</h2>
{exp:cartthrob:checkout_form 
    	create_user="yes"
	gateway="dev_template"
	group_id="6"}
            Username: <input type="text" class="required" name="username" value="" />
            Email Address: <input type="text" class="required" name="email_address" value="" />
            Screen Name: <input type="text" name="screen_name" value="" />
            Password: <input type="password" name="password"  value="" />
            Confirm Password: <input type="password" name="password_confirm" value="" /> 
 	
	{gateway_fields}

	<input type="submit" value="Submit" />
{/exp:cartthrob:checkout_form}

<br />Generates a checkout form
<br />http://cartthrob.com/docs/tags_detail/checkout_form/
		]]>
		</template>
		<template template_name='customer_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299707300' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='9'>
		<![CDATA[
{exp:cartthrob:customer_info}
first_name           	{first_name}
<br />last_name            	{last_name}
<br />address              	{address}
<br />address2             	{address2}
<br />city                 	{city}
<br />state                	{state}
<br />zip                  	{zip}
<br />company              	{company}
<br />country              	{country}
<br />country_code         	{country_code}
<br />region               	{region}
<br />phone                	{phone}
<br />email_address        	{email_address}
<br />shipping_first_name  	{shipping_first_name}
<br />shipping_last_name   	{shipping_last_name}
<br />shipping_address     	{shipping_address}
<br />shipping_address2    	{shipping_address2}
<br />shipping_city        	{shipping_city}
<br />shipping_state       	{shipping_state}
<br />shipping_zip         	{shipping_zip}
<br />shipping_company     	{shipping_company}
<br />shipping_country     	{shipping_country}
<br />shipping_country_code	{shipping_country_code}
<br />shipping_company     	{shipping_company}
<br />ip_address           	{ip_address}
<br />weight_unit          	{weight_unit}
<br />language             	{language}
<br />use_billing_info     	{use_billing_info}
<br />description          	{description}
<br />card_type            	{card_type}
<br />expiration_month     	{expiration_month}
<br />expiration_year      	{expiration_year}
<br />po_number            	{po_number}
<br />card_code            	{card_code}
<br />CVV2                 	{CVV2}
<br />issue_number         	{issue_number}
<br />transaction_type     	{transaction_type}
<br />bank_account_number  	{bank_account_number}
<br />check_type           	{check_type}
<br />account_type         	{account_type}
<br />routing_number       	{routing_number}
<br />begin_month          	{begin_month}
<br />begin_year           	{begin_year}
<br />bday_month           	{bday_month}
<br />bday_day             	{bday_day}
<br />bday_year            	{bday_year}
<br />currency_code        	{currency_code}
<br />shipping_option      	{shipping_option}
<br />success_return       	{success_return}
<br />cancel_return        	{cancel_return}
<br />username             	{username}
<br />screen_name          	{screen_name}
             
	you need to save this data first using save_customer_info_form 	{custom_data:your_data_field_name} 
{/exp:cartthrob:customer_info}

<br />Outputs saved customer info
<br />NOTE: example requires that some customer data be available see save_customer_info_form 
<br />http://cartthrob.com/docs/tags_detail/customer_info/		]]>
		</template>
		<template template_name='debug_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978502' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[
{exp:cartthrob:debug_info view_all="yes"}

<br />Outputs cart/customer information from current cart, and optionally ALL cartthrob settings
<br />View all parameter will show EVERYTHING in CartThrob's settings... not recommended during normal testing. 
<br />http://cartthrob.com/docs/tags_detail/debug_info/		]]>
		</template>
		<template template_name='download_file_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784251' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='21'>
		<![CDATA[
{exp:cartthrob:download_file_form file="{site_url}themes/third_party/cartthrob/images/cartthrob_logo_bg.jpg" }
	<input type="submit" value="submit" /> 
{/exp:cartthrob:download_file_form}

<br />Outputs a form used for downloading files in a more secure fashion than a standard link
<br />http://cartthrob.com/docs/tags_detail/download_file_form/		]]>
		</template>
		<template template_name='get_cartthrob_logo' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297978985' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:get_cartthrob_logo}

<br />Outputs the cartthrob logo. Show us some love!
<br />http://cartthrob.com/docs/tags_detail/get_cartthrob_logo/		]]>
		</template>
		<template template_name='get_download_link' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299784262' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='22'>
		<![CDATA[
<a href="{exp:cartthrob:get_download_link  free_file="{site_url}themes/third_party/cartthrob/images/cartthrob_logo_bg.jpg"}" }"> download free file</a>
<br />
<a href="{exp:cartthrob:get_download_link group_id="1" file="{site_url}themes/third_party/cartthrob/images/cartthrob_logo_bg.jpg"}"> download file restricted to group 1</a>


<br />Generates an encrypted download link
<br />NOTE: Standard downloads require at least a member id OR a group id. If you want no restriction use free_file parameter. 
<br />http://cartthrob.com/docs/tags_detail/get_download_link/		]]>
		</template>
		<template template_name='get_items_in_range' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298927651' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:cartthrob:get_items_in_range price_min="10" price_max="55"}

<br />Searches all product channels for items within the price range and outputs as a pipe-delimited list.
<br />http://cartthrob.com/docs/tags_detail/get_items_in_range/		]]>
		</template>
		<template template_name='get_shipping_options' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298402424' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:get_shipping_options}

<br />Gets available shipping_options from selected shipping plugin. If no options are available, nothing is returned.
<br />http://cartthrob.com/docs/tags_detail/get_shipping_options/		]]>
		</template>
		<template template_name='is_in_cart' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298402569' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{if {exp:cartthrob:is_in_cart entry_id="1"}}
	This item is already in your cart!
{/if}

<br />Outputs "1" if a specific item is in the cart
<br />NOTE: example requires item with entry_id:1 be in cart 
<br />http://cartthrob.com/docs/tags_detail/is_in_cart/		]]>
		</template>
		<template template_name='is_purchased_item' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297979186' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{if '{exp:cartthrob:is_purchased_item entry_id="1"}' }
 You have already purchased this item!
{/if}

<br />Outputs "1" if a specific item is in the customer's history
<br />NOTE: example requires item with entry_id:1 be customer's history. Requires that customer be logged in. 
<br />http://cartthrob.com/docs/tags_detail/is_purchased_item/		]]>
		</template>
		<template template_name='order_totals' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297979447' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='7'>
		<![CDATA[
{exp:cartthrob:order_totals}

<br />Outputs order_totals
<br />NOTE: example requires that previous orders have been made.
<br />http://cartthrob.com/docs/tags_detail/order_totals/		]]>
		</template>
		<template template_name='selected_shipping_option' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980557' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:selected_shipping_option}

<br />Outputs the selected shipping option
<br />NOTE: will not display if no shipping option has been set
<br />http://cartthrob.com/docs/tags_detail/selected_shipping_option/		]]>
		</template>
		<template template_name='selected_gateway_fields' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980495' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:selected_gateway_fields gateway="authorize_net"}

<br />Outputs the fields connected with a specific gateway
<br />http://cartthrob.com/docs/tags_detail/selected_gateway_fields/		]]>
		</template>
		<template template_name='save_customer_info_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299707309' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='19'>
		<![CDATA[
{exp:cartthrob:save_customer_info_form return="cart_functions/customer_info"} 
first_name                        	<input type="text" name="first_name" value="" /> <br />
last_name                         	<input type="text" name="last_name" value="" /> <br />
address                           	<input type="text" name="address" value="" /> <br />
address2                          	<input type="text" name="address2" value="" /> <br />
city                              	<input type="text" name="city" value="" /> <br />
state                             	<input type="text" name="state" value="" /> <br />
zip                               	<input type="text" name="zip" value="" /> <br />
country                           	<input type="text" name="country" value="" /> <br />
country_code                      	<input type="text" name="country_code" value="" /> <br />
company                           	<input type="text" name="company" value="" /> <br />
phone                             	<input type="text" name="phone" value="" /> <br />
email_address                     	<input type="text" name="email_address" value="" /> <br />
ip_address                        	<input type="text" name="ip_address" value="" /> <br />
description                       	<input type="text" name="description" value="" /> <br />
use_billing_info                  	<input type="text" name="use_billing_info" value="" /> <br />
shipping_first_name               	<input type="text" name="shipping_first_name" value="" /> <br />
shipping_last_name                	<input type="text" name="shipping_last_name" value="" /> <br />
shipping_address                  	<input type="text" name="shipping_address" value="" /> <br />
shipping_address2                 	<input type="text" name="shipping_address2" value="" /> <br />
shipping_city                     	<input type="text" name="shipping_city" value="" /> <br />
shipping_state                    	<input type="text" name="shipping_state" value="" /> <br />
shipping_zip                      	<input type="text" name="shipping_zip" value="" /> <br />
shipping_country                  	<input type="text" name="shipping_country" value="" /> <br />
shipping_country_code             	<input type="text" name="shipping_country_code" value="" /> <br />
shipping_company                  	<input type="text" name="shipping_company" value="" /> <br />
CVV2                              	<input type="text" name="CVV2" value="" /> <br />
card_type                         	<input type="text" name="card_type" value="" /> <br />
expiration_month                  	<input type="text" name="expiration_month" value="" /> <br />
expiration_year                   	<input type="text" name="expiration_year" value="" /> <br />
po_number                         	<input type="text" name="po_number" value="" /> <br />
card_code                         	<input type="text" name="card_code" value="" /> <br />
issue_number                      	<input type="text" name="issue_number" value="" /> <br />
transaction_type                  	<input type="text" name="transaction_type" value="" /> <br />
bank_account_number               	<input type="text" name="bank_account_number" value="" /> <br />
check_type                        	<input type="text" name="check_type" value="" /> <br />
account_type                      	<input type="text" name="account_type" value="" /> <br />
routing_number                    	<input type="text" name="routing_number" value="" /> <br />
begin_month                       	<input type="text" name="begin_month" value="" /> <br />
begin_year                        	<input type="text" name="begin_year" value="" /> <br />
bday_month                        	<input type="text" name="bday_month" value="" /> <br />
bday_day                          	<input type="text" name="bday_day" value="" /> <br />
bday_year                         	<input type="text" name="bday_year" value="" /> <br />
currency_code                     	<input type="text" name="currency_code" value="" /> <br />
language                          	<input type="text" name="language" value="" /> <br />
shipping_option                   	<input type="text" name="shipping_option" value="" /> <br />
weight_unit                       	<input type="text" name="weight_unit" value="" /> <br />
region                            	<input type="text" name="region" value="" /> <br />
success_return                    	<input type="text" name="success_return" value="" /> <br />
cancel_return                     	<input type="text" name="cancel_return" value="" /> <br />
username                          	<input type="text" name="username" value="" /> <br />
screen_name                       	<input type="text" name="screen_name" value="" /> <br />
custom_data[your_custom_data_name]	<input type="text" name="custom_data[your_custom_data_name]" value=""/><br />
	<input type="submit" value="submit" />
{/exp:cartthrob:save_customer_info_form}
<br />Outputs a form to capture customer data in the cart<br />
Backend settings allow you to save this data into custom member fields<br />
http://cartthrob.com/docs/tags_detail/save_customer_info_form/<br />
 		]]>
		</template>
		<template template_name='set_config' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298927658' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:set_config}
	{if group_id == 7}
		{set_price_field channel="products" field="product_discounted_price"}
	{if:else}
		{set_price_field channel="products" field="product_price"}
	{/if}
{/exp:cartthrob:set_config}

<br />Allows you to override some cart settings on the fly
<br />NOTE: example requires that you have a group_id:7, a "products" channel, "product_price and "product_discounted_price" fields
<br />http://cartthrob.com/docs/tags_detail/set_config/		]]>
		</template>
		<template template_name='state_select' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980751' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:state_select name="state" selected="MO"}

<br />Outputs a select containing all US states
<br />http://cartthrob.com/docs/tags_detail/state_select/		]]>
		</template>
		<template template_name='states' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980721' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
<select name="state">
{exp:cartthrob:states}
    <option value="{abbrev}">{state}</option>
{/exp:cartthrob:states}
</select>

<br />Outputs a list of US States
<br />http://cartthrob.com/docs/tags_detail/states/
		]]>
		</template>
		<template template_name='total_items_count' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980796' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:total_items_count}

<br />Outputs total number of all items in cart 
<br />http://cartthrob.com/docs/tags_detail/total_items_count/		]]>
		</template>
		<template template_name='unique_items_count' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980823' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:unique_items_count}

<br />Outputs total number of unique items in the cart
<br />http://cartthrob.com/docs/tags_detail/unique_items_count/		]]>
		</template>
		<template template_name='update_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297980902' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='4'>
		<![CDATA[
{exp:cartthrob:cart_items_info}
	{if first_row}
		{exp:cartthrob:update_cart_form  return="cart/index"}
	{/if}
	{title}
	<input type="text" name="quantity[{row_id}]" size="2" value="{quantity}" />
	{!-- This deletes lets you select multiple row_ids --}
	<input type="checkbox" name="delete[{row_id}]"> Delete this item
	{if last_row}
    	<input type="submit" value="Update Cart" />
		{/exp:cartthrob:update_cart_form}
		
	{/if}
{/exp:cartthrob:cart_items_info}

<br />Outputs a form to update items in cart
<br />NOTE: example requires that items be in cart
<br />http://cartthrob.com/docs/tags_detail/update_cart_form/		]]>
		</template>
		<template template_name='view_converted_currency' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981126' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:view_converted_currency price="10" currency_code="USD" new_currency_code="GBP" api_key="YOUR_KEY_HERE"}

<br />Uses xurrency.com to convert prices on the fly
<br />NOTE: requires an API key to use
<br />http://cartthrob.com/docs/tags_detail/view_converted_currency/		]]>
		</template>
		<template template_name='view_country_name' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981147' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:view_country_name country_code="USA"}

<br />Converts a country code to a country name
<br />http://cartthrob.com/docs/tags_detail/view_country_name/		]]>
		</template>
		<template template_name='view_decrypted_string' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981177' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:view_decrypted_string string="ZE83M2FBVndWanhSVitkRWxsckNBbko1Qm81Rzh0RUpnNE1hUG1Qak14STlZY20wSy9OdHhUQ1QwYVhhNG1RSm5ZYkVCL2l3MGQ3SFBJV3l1MHdhQWc9PQ%3D%3D" key="10asl15bajkls8bb"}

<br />Will decrypt content encrypted with the specified key
<br />http://cartthrob.com/docs/tags_detail/view_decrypted_string/		]]>
		</template>
		<template template_name='view_encrypted_string' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981196' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='2'>
		<![CDATA[
{exp:cartthrob:view_encrypted_string string="hello world" key="10asl15bajkls8bb"}

<br />Will encrypt content using the specified key
<br />http://cartthrob.com/docs/tags_detail/view_encrypted_string/		]]>
		</template>
		<template template_name='view_formatted_number' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981210' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:view_formatted_number number="12.1234" prefix="$" decimals="2" }

<br />Formats a number
<br />http://cartthrob.com/docs/tags_detail/view_formatted_number/		]]>
		</template>
		<template template_name='view_setting' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981237' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:view_setting prefix="yes"}10

<br />Outputs stored settings values
<br />http://cartthrob.com/docs/tags_detail/view_setting/		]]>
		</template>
		<template template_name='fieldtype_order_items' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298927642' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:channel:entries channel="orders"}
	{order_items}
		Entry Id: {item:entry_id}<br />
		Title: {item:title}<br />
		Quantity: {item:quantity}<br /><br />
	{/order_items}
{/exp:channel:entries}

<br />Outputs content from a CartThrob Order Items fieldtype
<br />NOTE: example requires an "orders" channel, "order_items" field, and previous orders
<br />http://cartthrob.com/docs/tags_detail/order_items/
		]]>
		</template>
		<template template_name='fieldtype_price_modifiers' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298927646' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='16'>
		<![CDATA[
{exp:channel:entries channel="products" limit="10" }
title: {title}<br />
	{product_size}
		{option_name}-{option_value}-{price}<br />
	{/product_size}
<hr />
{/exp:channel:entries}


<br />Generates a select box based on the contents of a CartThrob Price Modifiers custom fieldtype
<br />NOTE: example requires "products" channel and "product_color" price modifier field
http://cartthrob.com/docs/tags_detail/price_modifiers/		]]>
		</template>
		<template template_name='year_select' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981330' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='3'>
		<![CDATA[
{exp:cartthrob:year_select years="6"}

<br />Outputs a select list of years
<br />http://cartthrob.com/docs/tags_detail/year_select/		]]>
		</template>
		<template template_name='years' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981314' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
<select name="expiration_year">
{exp:cartthrob:years years="6"}
    <option value="{year}">{year}</option>
{/exp:cartthrob:years}
</select>

<br />Outputs a list of years
<br />http://cartthrob.com/docs/tags_detail/years/		]]>
		</template>
		<template template_name='.README' save_template_file='n' template_type='webpage' template_notes='' edit_date='1297981023' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
The templates contained in this template group are purely instructional. 
They are the simplest possible examples available, and do not reference
every variable and parameter. Each template contains basic notes about
what the tag is for, and a link to the full online documentation		]]>
		</template>
		<template template_name='submitted_order_info' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298040562' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='1'>
		<![CDATA[
{exp:cartthrob:submitted_order_info}
	{if authorized}
		Your payment is a success!
		Transaction ID: {transaction_id}
		Your total: {cart_total}
		Your order ID: {order_id}
    {if:elseif processing}
		Your transaction is being processed!<br />
        Transaction ID: {transaction_id}<br />
        Your total: {cart_total}<br />
        Your order ID: {order_id}
	{if:elseif declined}
		Your credit card was declined: {error_message}
	{if:elseif failed}
		Your payment failed: {error_message}
	{/if}
{/exp:cartthrob:submitted_order_info}


<br />Outputs the results of a checkout_form submission. 
<br />NOTE: example requires that a checkout be attempted before any results will display
<br />http://cartthrob.com/docs/tags_detail/submitted_order_info/		]]>
		</template>
		<template template_name='delete_from_cart_form' save_template_file='n' template_type='webpage' template_notes='' edit_date='1298399585' last_author_id='901' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='6'>
		<![CDATA[
{exp:cartthrob:cart_items_info}
		{exp:cartthrob:delete_from_cart_form
			return="cart_functions/delete_from_cart_form" 
			}
	Title: {title}<input type="hidden" name="row_id" value="{row_id}"> 
    	<input type="submit" value="Delete Items" />
		{/exp:cartthrob:delete_from_cart_form}
{/exp:cartthrob:cart_items_info}

<br />Outputs a form used to delete items. You can also delete items using update_cart_form, so this is a bit redundant
<br />NOTE: Example requires that items be added to the cart
<br />http://cartthrob.com/docs/tags_detail/delete_from_cart_form/		]]>
		</template>
		<template template_name='cart_weight' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299269906' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='0'>
		<![CDATA[
{exp:cartthrob:cart_weight}

<br />Outputs cart weight
<br />http://cartthrob.com/docs/tags_detail/cart_weight/		]]>
		</template>
		<template template_name='item_options' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299604026' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='y' php_parse_location='o' hits='6'>
		<![CDATA[
{exp:cartthrob:cart_items_info}
	{if no_results}Your cart is empty{/if}
	 <div><h2>Item: {entry_id}</h2>
	{exp:cartthrob:item_options entry_id = "{entry_id}" row_id="{row_id}" }
		{if no_results}
			There are no options for this item
		{/if}
		Option {item_options:option_label}
		<br />
		{item_options:list}
			{if dynamic}dynamically added options!{/if}
			LIST ITEM: {if option_selected}selected{/if} {option} {option_name}  price: {price}<br />
		{/item_options:list}
		<br />
		{item_options:select row_id="{row_id}" id="{option_field_name}"}
			<option {selected} value="{option}">{option_name} {price}</option>
		{/item_options:select}
		<br /><br /><br />
	{/exp:cartthrob:item_options}
	</div>
{/exp:cartthrob:cart_items_info}


<br />Will pull up all item options for a specific entry. Can be used singly, or in combination with cart items info to pull up information about a specific item in the basket
<br />NOTE: example requires "products" channel and "product_color" price modifier field
http://cartthrob.com/docs/tags_detail/price_modifiers/		]]>
		</template>
		<template template_name='fieldtype_price_with_quantity' save_template_file='n' template_type='webpage' template_notes='' edit_date='1299279568' last_author_id='1' cache='n' refresh='0' no_auth_bounce='' enable_http_auth='n' allow_php='n' php_parse_location='o' hits='10'>
		<![CDATA[
{exp:channel:entries channel="products" limit="10" }
	title: {title}<br />
	{price_with_quantity}
    		{if "{row_count}" == "1"}Total Rows {total_rows}<br />{/if}
     		   <span style="{row_switch="inline|none|none"}">{from_quantity} - {up_to_quantity} {price} Row count {row_count}<br /></span>
    		<br />
	{/price_with_quantity} 
<hr />
{/exp:channel:entries}


<br />The price with quantity fieldtype allows you to set price breaks when purchasing multiples of one item. This tag outputs the prices & their related quantities
<br />NOTE: example requires "products" channel, a field called price_with_quantity mapped as a price field, and data in that field
http://cartthrob.com/docs/tags_detail/price_with_quantity/		]]>
		</template>
	</template_group>
</xml>
