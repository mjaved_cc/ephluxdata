<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_core_ee $cartthrob;
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_matrix_ft extends EE_Fieldtype
{
	public $info = array(
		'name' => 'CartThrob Matrix',
		'version' => '1.0.0'
	);
	
	public $has_array_data = TRUE;
	
	public $default_row = array();
	
	public $buttons = array(
		'Add Row' => '$.cartthrobMatrix.addRow($(this).parents(\'div.cartthrobMatrixControls\').prev(\'table.cartthrobMatrix\'));',
		'Add Column' => '$.cartthrobMatrix.addColumn($(this).parents(\'div.cartthrobMatrixControls\').prev(\'table.cartthrobMatrix\'));',
	);
	
	public $hidden_columns = array();
	
	public $additional_controls = '';
	
	public $variable_prefix = '';
	
	public $row_nomenclature = 'row';
	
	public function __construct()
	{
		parent::EE_Fieldtype();
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
	}
	
	public function pre_process($data)
	{
		$this->EE->load->helper('data_formatting');
		
		return _unserialize($data, TRUE);
	}
	
	public function display_field($data, $replace_tag = FALSE)
	{
		$this->EE->load->helper(array('url', 'html'));
		
		$this->EE->load->library('table');
		
		$this->EE->table->clear();
		
		$this->EE->load->helper('data_formatting');
		
		if ( ! is_array($data))
		{
			$data = _unserialize($data, TRUE);
		}
		
		if ( ! $data)
		{
			$data = array($this->default_row);
		}
		
		$output = '';
		
		reset($data);
		
		$headers = array();
		
		$header_row = array();
		
		foreach ($data as $row)
		{
			foreach ($row as $key => $value)
			{
				if ( ! in_array($key, $headers) && ! in_array($key, $this->hidden_columns))
				{
					$headers[] = $key;
				}
			}
		}
		
		foreach ($this->default_row as $header => $default)
		{
			if ( ! in_array($header, $headers))
			{
				$headers[] = $header;
			}
		}
		
		$table_rows = array();
		
		foreach ($data as $row_id => $row)
		{
			$new_row = array();
			
			if ( ! $replace_tag)
			{
				$new_row[] = array(
					'data' => img(array('border' => '0', 'src' => $this->EE->config->item('theme_folder_url').'third_party/cartthrob/images/ct_drag_handle.gif', 'width' => 10, 'height' => 17)),
					'class' => 'drag_handle',
				);
			}
			
			foreach ($headers as $index => $header)
			{
				$index += 1;
				
				$new_row[$index] = (isset($row[$header])) ? $row[$header] : '';
				
				if ( ! $replace_tag)
				{
					if ( ! preg_match('/[\r\n]/', $new_row[$index]))
					{
						$new_row[$index] = form_input($this->field_name.'['.$row_id.']['.$header.']', $new_row[$index]);
					}
					else
					{
						$new_row[$index] = form_textarea(array('name' => $this->field_name.'['.$row_id.']['.$header.']', 'value' => $new_row[$index], 'rows' => 3));
					}
				}
			}
			
			if ( ! $replace_tag)
			{
				$last_col = $this->js_anchor(
					img(array(
						'border' => '0',
						'src' => $this->EE->config->item('theme_folder_url').'cp_themes/default/images/content_custom_tab_delete.png'
					)),
					'$.cartthrobMatrix.removeRow(this);'
				);
				
				foreach ($this->hidden_columns as $col)
				{
					$last_col .= form_hidden($this->field_name.'['.$row_id.']['.$col.']', (isset($row[$col])) ? $row[$col] : '');
				}
				
				$new_row[] = array(
					'data' => $last_col,
					'class' => 'remove',
				);
			}
			
			$table_rows[$row_id] = $new_row;
		}
		
		if ( ! $replace_tag)
		{
			if (empty($this->EE->session->cache['cartthrob_matrix']['head']))
			{
				$this->EE->cp->add_to_head('<link rel="stylesheet" href="'.$this->EE->config->item('theme_folder_url').'third_party/cartthrob/css/cartthrob_matrix.css" type="text/css" media="screen" />');
				
				$this->EE->cp->add_js_script(array('ui' => array('sortable')));
				
				if (REQ != 'CP')
				{
					$this->EE->cp->add_to_head('<link rel="stylesheet" href="'.$this->EE->config->item('theme_folder_url').'third_party/cartthrob/css/cartthrob_matrix_table.css" type="text/css" media="screen" />');
				}
				
				$this->EE->cp->add_to_head('
				<script type="text/javascript">
				$.cartthrobMatrix = {
					blankRows: {},
					hiddenRows: {},
					handleButton: \''.img(array('border' => '0', 'src' => $this->EE->config->item('theme_folder_url').'third_party/cartthrob/images/ct_drag_handle.gif', 'width' => 10, 'height' => 17)).'\',
					removeRowButton: \''.$this->js_anchor(img(array('border' => '0', 'src' => $this->EE->config->item('theme_folder_url').'cp_themes/default/images/content_custom_tab_delete.png')), '$.cartthrobMatrix.removeRow(this);').'\',
					removeColumnButton: \''.$this->js_anchor(img(array('border' => '0', 'src' => $this->EE->config->item('theme_folder_url').'cp_themes/default/images/content_custom_tab_delete.png')), '$.cartthrobMatrix.removeColumn(this);', array('class' => 'remove_column')).'\',
					removeRow: function(e) {
						var table = $(e).parents("table");
						if (confirm("'.$this->EE->lang->line('remove_row_confirm').'"))
						{
							$(e).parents("tr").eq(0).remove();
						}
						$.cartthrobMatrix.resetRows(table);
					},
					clearRows: function(e) {
						$(e).find("tbody tr").remove();
					},
					addRow: function(e) {
						var row = "<tr><td class=\'drag_handle\'>"+$.cartthrobMatrix.handleButton+"</td>";
						var fieldName = $(e).attr("id");
						$.each($.cartthrobMatrix.blankRows[fieldName], function(i,value){
							row += "<td>"+value+"</td>";
						});
						row += "<td class=\'remove\'>"+$.cartthrobMatrix.removeRowButton;
						$.each($.cartthrobMatrix.hiddenRows[fieldName], function(i,value){
							row += value;
						});
						row += "</td></tr>";
						$(e).children("tbody").append(row);
						$(e).children("tbody").children("tr:last").find(":input").attr("disabled", false);
						$(e).children("tbody").children("tr:last").find(":input").eq(0).focus();
						$.cartthrobMatrix.resetRows($(e));
					},
					removeColumn: function(e) {
						if (confirm("'.$this->EE->lang->line('remove_column_confirm').'"))
						{
							var index = $(e).parent().index();
							var fieldName = $(e).parents("table").attr("id");
							$(e).parents("table").find("tbody tr").each(function(){
								$(this).children("td").eq(index).remove();
							});
							$.cartthrobMatrix.blankRows[fieldName].splice(index, 1);
							$(e).parent().remove();
						}
					},
					resetRows: function(e) {
						var index = 0;
						$(e).find("tbody tr").each(function(){
							var mod = (index % 2) ? "odd" : "even";
							$(this).removeClass("even").removeClass("odd");
							$(this).addClass(mod);
							$(this).find(":input").each(function(){
								$(this).attr("name", $(this).attr("name").replace(/^(.*?)\[.*?\](.*?)$/, "$1["+index+"]$2"));
							});
							index++;
						});
					},
					addColumn: function(e, name) {
						var fieldName = $(e).attr("id");
						if ( ! name) {
							name = $.trim(prompt("'.$this->EE->lang->line('name_column_prompt').'")).toLowerCase().replace(/\s/g, "_").replace(/[^a-z0-9_]/g, "");
						}
						if ( ! name) {
							return;
						}
						$(e).find("thead tr th:last").before("<th>"+name+$.cartthrobMatrix.removeColumnButton+"</th>");
						$(e).find("tbody tr").each(function(){
							$(this).children("td:last").before("<td><input type=\'text\' name=\'"+fieldName+"[INDEX]["+name+"]\'></td>");
						});
						$.cartthrobMatrix.blankRows[fieldName].push("<input type=\'text\' name=\'"+fieldName+"[INDEX]["+name+"]\'>");
						$(e).find("tbody tr:first :input:last").focus();
						$.cartthrobMatrix.resetRows($(e));
					},
					serialize: function(e) {
						var data = [];
						$(e).find(":input").each(function(){
							var match = $(this).attr("name").match(/.*\[(\d+)\]\[(.*)\]/);
							if (match) {
								if (data[match[1]] == undefined) {
									data[match[1]] = {};
								}
								data[match[1]][match[2]] = $(this).val();
							}
						});
						return data;
					},
					unserialize: function(e, data) {
						this.clearRows(e);
						for (i in data) {
							this.addRow(e);
							for (j in data[i]) {
								if ( ! this.hasColumn(e, j)) {
									this.addColumn(e, j);
								}
								$(e).find(":input").each(function(){
									if ($(this).attr("name").match(new RegExp(".*\\\["+i+"\\\]\\\["+j+"\\\]"))) {
										$(this).val(data[i][j]);
										return false;
									}
								});
							}
						}
						$(e).find("tbody tr:first :input:first").focus();
					},
					hasColumn: function(e, column) {
						var hasColumn = false;
						$(e).find("thead th").each(function(){
							if ($(this).text() == column) {
								hasColumn = true;
								return false;
							}
						});
						return hasColumn;
					}
				}
				</script>
				');
				
				$this->EE->javascript->output('
				$("table.cartthrobMatrix tbody").sortable({
					handle: ".drag_handle",
					stop: function(e, ui) {
						$.cartthrobMatrix.resetRows($(ui.item).parents("table"));
					}
				});
				');
				
				$this->EE->session->cache['cartthrob_matrix']['head'] = TRUE;
			}
			
			foreach ($headers as $header)
			{
				$blank_row[] = form_input(array('name' => $this->field_name.'[INDEX]['.$header.']', 'disabled' => 'disabled'));
			}
			
			$hidden = array();
			
			foreach ($this->hidden_columns as $col)
			{
				$hidden[] = '<input type="hidden" name="'.$this->field_name.'[INDEX]['.$col.']" disabled="disabled" />';
			}
			
			$this->EE->javascript->output("$.cartthrobMatrix.blankRows['{$this->field_name}'] = ".$this->EE->javascript->generate_json($blank_row, TRUE).';');
			$this->EE->javascript->output("$.cartthrobMatrix.hiddenRows['{$this->field_name}'] = ".$this->EE->javascript->generate_json($hidden, TRUE).';');
			
			$headers[] = '';
		}
		
		foreach ($headers as &$header)
		{
			if ($replace_tag || ! $header || in_array($header, array_keys($this->default_row)))
			{
				continue;
			}
			
			$header .= $this->js_anchor(img(array('border' => '0', 'src' => $this->EE->config->item('theme_folder_url').'cp_themes/default/images/content_custom_tab_delete.png')), '$.cartthrobMatrix.removeColumn(this);', array('class' => 'remove_column'));
		}
		
		array_unshift($headers, '');
		
		array_unshift($table_rows, $headers);
		
		$this->EE->table->set_template(array(
			'table_open' => '<table class="mainTable cartthrobMatrix" id="'.$this->field_name.'" border="0" cellspacing="0" cellpadding="0">',
			'row_start' => '<tr class="even">',
			'row_alt_start' => '<tr class="odd">'
		));
		
		$output .= $this->EE->table->generate($table_rows);
		
		if ( ! $replace_tag)
		{
			$output .= '<div class="cartthrobMatrixControls">';
			
			if ($this->buttons)
			{
				$buttons = array();
				
				foreach ($this->buttons as $name => $onclick)
				{
					$buttons[] = $this->js_anchor($this->EE->lang->line($name), $onclick);
				}
				
				$output .= ul($buttons, array('class' => 'cartthrobMatrixButtons'));
			}
			
			$output .= $this->additional_controls;
			
			$output .= '</div>';
		}
		
		return $output;
	}
	
	public function save($data)
	{
		if (is_array($data))
		{
			//if there's just one empty row
			if (count($data) === 1 && count(array_filter(current($data))) === 0)
			{
				return '';
			}
			
			return base64_encode(serialize($data));
		}
		
		return '';
	}
	
	public function display_settings()
	{
		$prefix = strtolower(preg_replace('/_ft$/', '', get_class($this))).'_';
		
		return form_hidden($prefix.'field_fmt', 'none');
	}
	
	/**
	 * Replace tag
	 *
	 * @access	public
	 * @param	field contents
	 * @return	replacement text
	 *
	 */
	public function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		if (count($data) === 0 && preg_match('/'.LD.'if '.$this->variable_prefix.'no_results'.RD.'(.*?)'.LD.'\/if'.RD.'/s', $tagdata, $match))
		{
			$this->EE->TMPL->tagdata = str_replace($match[0], '', $this->EE->TMPL->tagdata);
			
			$this->EE->TMPL->no_results = $match[1];
		}
		
		if ( ! $data)
		{
			return $this->EE->TMPL->no_results();
		}
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->helper('data_formatting');
		
		$total_results = count($data);
		
		$this->EE->load->library('data_filter');
		
		$this->EE->data_filter->sort($data, (isset($params['orderby'])) ? $params['orderby'] : FALSE, (isset($params['sort'])) ? $params['sort'] : FALSE);
		
		$count = 1;
		
		foreach ($data as &$row)
		{
			$row['count'] = $count;
			
			$row['total_results'] = $total_results;
			
			$row['first_'.$this->row_nomenclature] = (int) ($count === 1);
			
			$row['last_'.$this->row_nomenclature] = (int) ($count === $total_results);
			
			$row = array_merge($row, array_key_prefix($row, $this->variable_prefix));
			
			$row[$this->row_nomenclature.'_count'] = $row['count'];
			
			$row['total_'.$this->row_nomenclature.'s'] = $row['total_results'];
		
			if (preg_match_all('/'.LD.'('.preg_quote($this->variable_prefix).'|row_)?'.'switch=([\042\047])(.+)\\2'.RD.'/', $tagdata, $matches))
			{
				foreach ($matches[0] as $i => $v)
				{
					$values = explode('|', $matches[3][$i]);
					
					$row[substr($matches[0][$i], 1, -1)] = $values[($count - 1) % count($values)];
				}
			}
			
			$count++;
		}
		
		return $this->EE->TMPL->parse_variables($tagdata, $data);
	}
	
	public function replace_table($data, $params = array(), $tagdata = FALSE)
	{
		return $this->display_field($data, TRUE);
	}
	
	public function replace_total_results($data, $params = array(), $tagdata = FALSE)
	{
		return count($data);
	}
	
	//deprecated, alias for total_results
	public function replace_option_count($data, $params = array(), $tagdata = FALSE)
	{
		return $this->replace_total_results($data, $params, $tagdata);
	}
	
	public function replace_label($data, $params = array(), $tagdata = FALSE)
	{
		$this->EE->load->model('cartthrob_field_model');
		
		return $this->EE->cartthrob_field_model->get_field_label($this->field_id);
	}
	
	protected function js_anchor($title, $onclick = '', $attributes = '')
	{
		if ($onclick)
		{
			$attributes['onclick'] = $onclick;
		}
		
		if ($attributes)
		{
			$attributes = _parse_attributes($attributes);
		}
	    
		return '<a href="javascript:void(0);"'.$attributes.'>'.$title.'</a>';
	}
}

/* End of file ft.cartthrob_discount.php */
/* Location: ./system/expressionengine/third_party/cartthrob_discount/ft.cartthrob_discount.php */