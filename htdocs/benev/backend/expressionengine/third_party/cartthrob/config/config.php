<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cartthrob_default_settings'] = array(
	'admin_email' => '',
	'low_stock_email'	=> '',
	'license_number' => '',
	'logged_in' => 0,
	'default_member_id' => '',
	'clear_cart_on_logout' => 1,
	'session_expire' => 18000,
	'allow_gateway_selection' => 1,
	'encode_gateway_selection' => 1,
	'encrypted_sessions'	=> 1, 
	'allow_products_more_than_once' => 0,
	'allow_empty_cart_checkout' => 0,
	'product_split_items_by_quantity' => 0,
	'product_channels' => array(),
	'product_channel_fields' => array(),
	'save_orders' => 0,
	'orders_channel' => '',
	'orders_sequential_order_numbers' => 1,
	'orders_items_field' => '',
	'orders_subtotal_field' => '',
	'orders_tax_field' => '',
	'orders_shipping_field' => '',
	'orders_discount_field' => '',
	'orders_total_field' => '',
	'orders_status_field' => '',
	'orders_status'	=> array(   
		'completed',    	// standard finished transaction
		'processing',		// currently being processed by third parties
		'declined',			// credit agency declined transaction
		'failed',			// failure when attempting to complete a transaction
		//'canceled',			// order cancelled by customer
		//'expired',			// credit agency reports that the transaction has expired
		//'voided',		
		//'refunded',
		//'reversed',			// reversed
		//'pending_payment'	// on hold
	),
	'orders_default_status' => '',
	'orders_processing_status' => '',
	'orders_declined_status' => '',
	'orders_failed_status' => '',
	'orders_transaction_id' => '',
	'orders_last_four_digits' => '',
	'orders_convert_country_code' => 1,
	'orders_coupon_codes' => '',
	'orders_customer_name' => '',
	'orders_customer_email' => '',
	'orders_customer_ip_address' => '',
	'orders_customer_phone' => '',
	'orders_full_billing_address' => '',
	'orders_billing_first_name' => '',
	'orders_billing_last_name' => '',
	'orders_billing_company' => '',
	'orders_billing_address' => '',
	'orders_billing_address2' => '',
	'orders_billing_city' => '',
	'orders_billing_state' => '',
	'orders_billing_zip' => '',
	'orders_billing_country' => '',
	'orders_country_code' => '',
	'orders_full_shipping_address' => '',
	'orders_shipping_first_name' => '',
	'orders_shipping_last_name' => '',
	'orders_shipping_company' => '',
	'orders_shipping_address' => '',
	'orders_shipping_address2' => '',
	'orders_shipping_city' => '',
	'orders_shipping_state' => '',
	'orders_shipping_zip' => '',
	'orders_shipping_country' => '',
	'orders_shipping_country_code' => '',
	'orders_shipping_option' => '',
	'orders_license_number_field' => '',
	'orders_license_number_type' => 'uuid',
	'orders_error_message_field' => '',
	'orders_language_field' => '',
	'orders_title_prefix' => 'Order #',
	'orders_title_suffix' => '',
	'orders_url_title_prefix' => 'order_',
	'orders_url_title_suffix' => '',
	'save_purchased_items' => 0,
	'purchased_items_channel' => '',
	'purchased_items_default_status' => '',
	'purchased_items_processing_status' => '',
	'purchased_items_declined_status' => '',
	'purchased_items_failed_status' => '',
	'purchased_items_id_field' => '',
	'purchased_items_quantity_field' => '',
	'purchased_items_order_id_field' => '',
	'purchased_items_license_number_field' => '',
	'purchased_items_price_field' => '',
	'purchased_items_title_prefix' => '',
	'purchased_items_license_number_type' => 'uuid',
	'approve_orders' => FALSE,
	'rounding_default' => 'standard',
	'global_item_limit' => 0,
	'global_coupon_limit' => 1,
	'tax_settings' => '',
	'tax_use_shipping_address' => 0,
	'coupon_code_channel' => '',
	'enable_logging' => FALSE,
	'cp_menu' => TRUE,
	'cp_menu_label' => '',
	'coupon_code_field' => '',
	'coupon_code_type' => '',
	'discount_channel' => '',
	'discount_type' => '',
	'payment_gateway' => 'Cartthrob_dev_template',
	'low_stock_level'	=> 5,
	'send_email' => 1,
	'send_confirmation_email' => 1,
	'send_inventory_email' => 0,
	
	'send_customer_declined_email'	=> 0,
	'send_customer_processing_email'	=> 0, 
	'send_customer_failed_email'	=> 0, 
	
	'send_admin_declined_email'	=> 0,
	'send_admin_failed_email'	=> 0, 
	'send_admin_processing_email'	=> 0, 
	
	'email_order_confirmation' => '{preload_replace:headline="THANK YOU FOR YOUR ORDER"}
 {if group_id=="1" OR "ORDER_ID" != ""}
	{exp:cartthrob:submitted_order_info}
		<table width="600" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" align="left" style="font-size:12px;color:#000000;font-family:arial, sans-serif;"><br>
					<p> <span style="font-size:16px;font-weight:bold;">{headline}</span> </p>
					<table cellspacing="0" cellpadding="2" bgcolor="#000000" width="100%">
						<tr>
							<td><span style="color:#ffffff;font-size:14px;">Order Data</span></td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="2" width="100%">
						<tr>
							<td valign="top">
								<span style="font-size:12px;font-weight:bold;">Order Date: </span> 
								<span style="font-size:12px;">{entry_date format="%M %D %Y"}</span>
							</td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td valign="top">
								<span style="font-size:12px; font-weight:bold;">Order ID: </span> 
								<span style="font-size:12px;">{title}</span>
							</td>
						</tr>
					</table>
					<hr>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="250" valign="top">
								<span style="font-size:14px; font-weight:bold; ">Billing</span><br>
								<span style="font-size:12px; ">   
									{order_billing_first_name} {order_billing_last_name}<br>
									{order_billing_address}<br>
									{if order_billing_address2}{order_billing_address2}<br>{/if}
									{order_billing_city}, {order_billing_state} {order_billing_zip}<br>
									{if order_country_code}{order_country_code}<br>{/if}
									{order_customer_email}<br>
									{order_customer_phone}
								</span>
							</td>
							<td valign="top">
								<span style="font-size:14px; font-weight:bold;">Shipping</span><br>
								<span style="font-size:12px; ">   
									{if order_shipping_address}
										{order_shipping_first_name} {order_shipping_last_name}<br>
										{order_shipping_address}<br>
										{if order_shipping_address2}{order_shipping_address2}<br>{/if}
										{order_shipping_city}, {order_shipping_state} {order_shipping_zip}
										{if order_shipping_country_code}{order_shipping_country_code}{/if}
									{if:else}
										{order_billing_first_name} {order_billing_last_name}<br>
										{order_billing_address}<br>
										{if order_billing_address2}{order_billing_address2}<br>{/if}
										{order_billing_city}, {order_billing_state} {order_billing_zip}<br>
										{if order_country_code}{order_country_code}<br>{/if}
										{order_customer_email}<br>
										{order_customer_phone}
									{/if}
								</span>
							</td>
						</tr>
					</table>
					<hr>

					Total number of purchased items: {order_items:total_results}.
					<table cellspacing="0" cellpadding="2" width="100%">
						<thead>
							<tr>
								<td><span style="font-size:12px;font-weight:bold;">ID</span></td>
								<td><span style="font-size:12px;font-weight:bold;">Description</span></td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Price</span></td>
								<td align="center">&nbsp;</td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Qty</span></td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Item Total</span></td>
							</tr>
						</thead>
						<tbody>
							{order_items}
								<tr class="{item:switch="odd|even"}">
									<td><span style="font-size:12px;">{item:entry_id}</span></td>
									<td><span style="font-size:12px;">{item:title}</span></td>
									<td align="right"><span style="font-size:12px;">{item:quantity}</span></td>
									<td align="center">&nbsp;</td>
									<td align="right"><span style="font-size:12px;">{item:price}</span></td>
									<td align="right">
										<span style="font-size:12px;">
										{exp:cartthrob:arithmetic num1="{item:price_numeric}" num2="{item:quantity}" operator="*"}
										</span>
									</td>
								</tr>
							{/order_items}
							<tr>
								<td><span style="font-size:12px;">&nbsp;</span></td>
								<td colspan="3">&nbsp;</td>
								<td><span style="font-size:12px;">&nbsp;</span></td>
								<td><span style="font-size:12px;">&nbsp;</span></td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td align="right">
								<table cellspacing="0" cellpadding="2">
									<tr>
										<td valign="top" align="right"><span style="font-size:12px;">Shipping:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;">{order_shipping}</span></td>
									</tr>
									<tr>
										<td valign="top" align="right"><span style="font-size:12px;">Tax:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;">{order_tax}</span></td>
									</tr>
									<tr>
										<td valign="top" align="right">&nbsp;</td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;"></span></td>
									</tr>
									<tr>
										<td valign="top" align="right"><span style="font-size:14px;font-weight:bold;">Total:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:14px;font-weight:bold;">{order_total}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	{/exp:cartthrob:submitted_order_info}
{/if}',
	'email_admin_notification' => '{preload_replace:headline="ORDER INFORMATION"}
 {if group_id=="1" OR "ORDER_ID" != ""}
	{exp:cartthrob:submitted_order_info}
		<table width="600" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" align="left" style="font-size:12px;color:#000000;font-family:arial, sans-serif;"><br>
					<p> <span style="font-size:16px;font-weight:bold;">{headline}</span> </p>
					<table cellspacing="0" cellpadding="2" bgcolor="#000000" width="100%">
						<tr>
							<td><span style="color:#ffffff;font-size:14px;">Order Data</span></td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="2" width="100%">
						<tr>
							<td valign="top">
								<span style="font-size:12px;font-weight:bold;">Order Date: </span> 
								<span style="font-size:12px;">{entry_date format="%M %D %Y"}</span>
							</td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td valign="top">
								<span style="font-size:12px; font-weight:bold;">Order ID: </span> 
								<span style="font-size:12px;">{title}</span>
							</td>
						</tr>
					</table>
					<hr>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="250" valign="top">
								<span style="font-size:14px; font-weight:bold; ">Billing</span><br>
								<span style="font-size:12px; ">   
									{order_billing_first_name} {order_billing_last_name}<br>
									{order_billing_address}<br>
									{if order_billing_address2}{order_billing_address2}<br>{/if}
									{order_billing_city}, {order_billing_state} {order_billing_zip}<br>
									{if order_country_code}{order_country_code}<br>{/if}
									{order_customer_email}<br>
									{order_customer_phone}
								</span>
							</td>
							<td valign="top">
								<span style="font-size:14px; font-weight:bold;">Shipping</span><br>
								<span style="font-size:12px; ">   
									{if order_shipping_address}
										{order_shipping_first_name} {order_shipping_last_name}<br>
										{order_shipping_address}<br>
										{if order_shipping_address2}{order_shipping_address2}<br>{/if}
										{order_shipping_city}, {order_shipping_state} {order_shipping_zip}
										{if order_shipping_country_code}{order_shipping_country_code}{/if}
									{if:else}
										{order_billing_first_name} {order_billing_last_name}<br>
										{order_billing_address}<br>
										{if order_billing_address2}{order_billing_address2}<br>{/if}
										{order_billing_city}, {order_billing_state} {order_billing_zip}<br>
										{if order_country_code}{order_country_code}<br>{/if}
										{order_customer_email}<br>
										{order_customer_phone}
									{/if}
								</span>
							</td>
						</tr>
					</table>
					<hr>

					Total number of purchased items: {order_items:total_results}.
					<table cellspacing="0" cellpadding="2" width="100%">
						<thead>
							<tr>
								<td><span style="font-size:12px;font-weight:bold;">ID</span></td>
								<td><span style="font-size:12px;font-weight:bold;">Description</span></td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Price</span></td>
								<td align="center">&nbsp;</td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Qty</span></td>
								<td align="right"><span style="font-size:12px;font-weight:bold;">Item Total</span></td>
							</tr>
						</thead>
						<tbody>
							{order_items}
								<tr class="{item:switch="odd|even"}">
									<td><span style="font-size:12px;">{item:entry_id}</span></td>
									<td><span style="font-size:12px;">{item:title}</span></td>
									<td align="right"><span style="font-size:12px;">{item:quantity}</span></td>
									<td align="center">&nbsp;</td>
									<td align="right"><span style="font-size:12px;">{item:price}</span></td>
									<td align="right">
										<span style="font-size:12px;">
										{exp:cartthrob:arithmetic num1="{item:price_numeric}" num2="{item:quantity}" operator="*"}
										</span>
									</td>
								</tr>
							{/order_items}
							<tr>
								<td><span style="font-size:12px;">&nbsp;</span></td>
								<td colspan="3">&nbsp;</td>
								<td><span style="font-size:12px;">&nbsp;</span></td>
								<td><span style="font-size:12px;">&nbsp;</span></td>
							</tr>
						</tbody>
					</table>
					<hr>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td align="right">
								<table cellspacing="0" cellpadding="2">
									<tr>
										<td valign="top" align="right"><span style="font-size:12px;">Shipping:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;">{order_shipping}</span></td>
									</tr>
									<tr>
										<td valign="top" align="right"><span style="font-size:12px;">Tax:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;">{order_tax}</span></td>
									</tr>
									<tr>
										<td valign="top" align="right">&nbsp;</td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:12px;"></span></td>
									</tr>
									<tr>
										<td valign="top" align="right"><span style="font-size:14px;font-weight:bold;">Total:</span></td>
										<td valign="top" align="right"></td>
										<td valign="top" align="right"><span style="font-size:14px;font-weight:bold;">{order_total}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	{/exp:cartthrob:submitted_order_info}
{/if}',
	'email_order_confirmation_subject' => 'Thank you for your order.',
	'email_admin_notification_subject' => 'An order has been placed',
	'email_order_confirmation_from' => '',
	'email_admin_notification_from' => '{customer_email}',
	'email_order_confirmation_from_name' => 'Web store',
	'email_admin_notification_from_name' => '{customer_name}',
	
	'email_inventory_notification_from_name' => 'Inventory Alert',
	'email_inventory_notification_from'	=> '',
	'email_inventory_notification_subject'=> 'Low stock inventory notification',
	'email_inventory_notification'	=> 'Low stock notification for item number ENTRY_ID. Stock level currently at STOCK_LEVEL',
	
	'email_admin_notification_plaintext' => 0,
	'email_low_stock_notification_plaintext' => 0,
	'email_order_confirmation_plaintext' => 0,
	'tax_plugin' => 'Cartthrob_tax_default',
	'shipping_plugin' => '',
	'auto_force_https' => FALSE,
	'force_https_domain' => '',
	'number_format_defaults_decimals' => 2,
	'number_format_defaults_dec_point' => '.',
	'number_format_defaults_thousands_sep' => ',',
	'number_format_defaults_prefix' => '$',
	'number_format_defaults_space_after_prefix' => FALSE,
	'number_format_defaults_currency_code' => 'USD',
	'save_member_data' => 1,
	'modulus_10_checking' => 0,
	'tax_inclusive_price' => 0,
	'checkout_registration_options' => 'auto-login',
	'member_first_name_field' => '',
	'member_last_name_field' => '',
	'member_address_field' => '',
	'member_address2_field' => '',
	'member_city_field' => '',
	'member_state_field' => '',
	'member_zip_field' => '',
	'member_country_field' => '',
	'member_country_code_field' => '',
	'member_company_field' => '',
	'member_phone_field' => '',
	'member_email_address_field' => '',
	'member_use_billing_info_field' => '',
	'member_shipping_first_name_field' => '',
	'member_shipping_last_name_field' => '',
	'member_shipping_address_field' => '',
	'member_shipping_address2_field' => '',
	'member_shipping_city_field' => '',
	'member_shipping_state_field' => '',
	'member_shipping_zip_field' => '',
	'member_shipping_country_field' => '',
	'member_shipping_country_code_field' => '',
	'member_shipping_company_field' => '',
	'member_language_field' => '',
	'member_shipping_option_field' => '',
	'member_region_field' => '',
	'default_location' => array(
		'state' => 'NY',
		'zip' => 10020,
		'country_code' => 'USA',
		'region' => '',
	),
	'price_modifier_presets' => array(
		'Small/Medium/Large' => array(
			array(
				'option_value' => 'S',
				'option_name' => 'Small',
				'price' => '',
			),
			array(
				'option_value' => 'M',
				'option_name' => 'Medium',
				'price' => '',
			),
			array(
				'option_value' => 'L',
				'option_name' => 'Large',
				'price' => '',
			),
		),
	),
	'Cartthrob_dev_template_settings' => array(
		'mode' => 'always_succeed',
		'gateway_fields_template' => '',
	),
	'clear_session_on_logout' => 0,
	'use_session_start_hook'	=> 0,
);