<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

abstract class Cartthrob_discount extends Cartthrob_child
{
	public $title = '';
	public $settings = array();
	public $plugin_settings = array();
	
	protected $error;
	protected $coupon_code;
	
	public static $global_settings = array(
		array(
			'type' => 'textarea',
			'short_name' => 'used_by',
			'name' => 'Redeemed By',
			'note' => 'A pipe delimited list of member_id\'s of users who have used this coupon code.',
		),
		array(
			'type' => 'text',
			'short_name' => 'per_user_limit',
			'name' => 'Per User Limit',
			'note' => 'How many times can this be used per customer? Leave blank for no limit.',
			'size' => '50px',
		),
		array(
			'type' => 'text',
			'short_name' => 'discount_limit',
			'name' => 'Global Limit',
			'note' => 'How many times can this be used overall? Leave blank for no limit.',
			'size' => '50px',
		),
		array(
			'type' => 'text',
			'short_name' => 'member_groups',
			'name' => 'Limit By Member Group',
			'note' => 'Which member groups can use this? Enter a list of list of group_id\'s, separated by comma. Leave blank for no limit.',
		),
		array(
			'type' => 'text',
			'short_name' => 'member_ids',
			'name' => 'Limit By Member ID',
			'note' => 'Which members can use this? Enter a list of member_id\'s, separated by comma. Leave blank for no limit.',
		)
	);
	
	public function initialize($plugin_settings = array())
	{
		if (is_array($plugin_settings))
		{
			$this->plugin_settings = $plugin_settings;
		}
		
		$this->type = Cartthrob_core::get_class($this);
		
		return $this;
	}
	
	public function plugin_settings($key, $default = FALSE)
	{
		if ($key === FALSE)
		{
			return $this->plugin_settings;
		}
		
		return (isset($this->plugin_settings[$key])) ? $this->plugin_settings[$key] : $default;
	}
	
	public function set_plugin_settings($plugin_settings)
	{
		$this->plugin_settings = $plugin_settings;
		
		return $this;
	}
	
	public function set_coupon_code($coupon_code)
	{
		if (is_string($coupon_code))
		{
			$this->coupon_code = $coupon_code;
		}
		
		return $this;
	}
	
	public function set_error($error)
	{
		if (is_string($error))
		{
			$this->error = $error;
		}
		
		return $this;
	}
	
	public function error()
	{
		return $this->error;
	}
	
	abstract function get_discount();
}
