<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

class Cartthrob_item_package extends Cartthrob_item
{
	protected $items;
	
	protected $defaults = array(
		'row_id' => NULL,
		'quantity' => 1,
		'product_id' => NULL,
		'price' => 0,
		'weight' => 0,
		'shipping' => 0,
		'title' => '',
		'no_tax' => FALSE,
		'no_shipping' => FALSE,
		'item_options' => array(),
		'items' => array()
	);
	
	private function items()
	{
		return $this->items;
	}
	
	public function price()
	{
		$price = 0;
		
		foreach ($this->items() as $row_id => $item)
		{
			$price += $item->price($item->item_options()) * $item->quantity();
		}
		
		return $price;
	}
	
	public function taxed_price()
	{
		$price = 0;
		
		foreach ($this->items() as $item)
		{
			$price += $item->price($item->item_options()) * $item->quantity() * (1 + $this->store->tax_rate());
		}
		
		return $price;
	}
	
	public function inventory()
	{
		$inventory = FALSE;
		
		foreach ($this->items() as $row_id => $item)
		{
			if ( ! $item->product_id())
			{
				continue;
			}
			
			$_inventory = floor($item->inventory($item->item_options()) / $item->quantity());
			
			if ($inventory === FALSE || $_inventory < $inventory)
			{
				$inventory = $_inventory;
			}
		}
		
		return (int) $inventory;
	}
}