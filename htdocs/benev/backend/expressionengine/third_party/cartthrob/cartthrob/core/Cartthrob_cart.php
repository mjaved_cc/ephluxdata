<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

/**
 * CartThrob Shopping Cart Class
 *
 * @package CartThrob
 * @subpackage Core
 */
class Cartthrob_cart extends Cartthrob_child
{
	protected $items, $total, $subtotal, $shippable_subtotal, $taxable_subtotal, $tax, $shipping, $discount, $customer_info, $shipping_info, $custom_data, $coupon_codes, $order, $meta, $config;
	
	protected $defaults = array(
		'items' => array(),
		//'total' => 0,
		//'subtotal' => 0,
		//'tax' => 0,
		//'shipping' => 0,
		//'customer_info' => array(),
		'shipping_info' => array(),
		'custom_data' => array(),
		'coupon_codes' => array(),
		'order' => array(),
		'config' => array(),
		'meta' => array()
	);
	
	private $calculation_caching = TRUE;
	
	/**
	 * Retrive the meta array or a meta key value
	 *
	 * @access public
	 * @param string|boolean $key
	 * @return mixed
	 */
	public function meta($key = FALSE)
	{
		if ($key === FALSE)
		{
			return $this->meta;
		}
		
		return (isset($this->meta[$key])) ? $this->meta[$key] : FALSE;
	}
	
	/**
	 * Set one or more meta array values
	 *
	 * @access public
	 * @param string|array $key
	 * @param mixed $value
	 * @return Cartthrob_cart
	 */
	public function set_meta($key, $value = FALSE)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->set_meta($k, $v);
			}
		}
		else
		{
			$this->meta[$key] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Set a global config value to be overridden for this cart only
	 *
	 * @access public
	 * @param string|array $key
	 * @param mixed $value
	 * @return Cartthrob_cart
	 */
	public function set_config($key, $value = FALSE)
	{
		$this->core->store->set_config($key, $value);
		
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->config[$k] = $v;
			}
		}
		else
		{
			$this->config[$key] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Turn calculation caching on and off
	 *
	 * @access public
	 * @param bool $calculation_caching
	 * @return Cartthrob_cart
	 */
	public function set_calculation_caching($calculation_caching = TRUE)
	{
		$this->calculation_caching = (bool) $calculation_caching;
		
		return $this;
	}
	
	/**
	 * Set the order data array
	 *
	 * @access public
	 * @param array $order
	 * @return Cartthrob_cart
	 */
	public function set_order($order)
	{
		if (is_array($order))
		{
			$this->order = $order;
		}
		
		return $this;
	}
	
	/**
	 * Update the order data array
	 *
	 * @access public
	 * @param array $order
	 * @return Cartthrob_cart
	 */
	public function update_order($order)
	{
		if (is_array($order))
		{
			$this->order = array_merge($this->order, $order);
		}
		
		return $this;
	}
	
	/**
	 * Retrieve the entire order data array or just a key's value
	 *
	 * @access public
	 * @param string|bool $key
	 * @return array
	 */
	public function order($key = FALSE)
	{
		if ($key !== FALSE)
		{
			return (isset($this->order[$key])) ? $this->order[$key] : FALSE;
		}
		
		return $this->order;
	}
	
	/**
	 * Add an item to cart
	 *
	 * @access public
	 * @param array $params
	 * @return Cartthrob_item
	 */
	public function add_item($params = array())
	{
		$this->clear_errors();
		
		$item_options = (isset($params['item_options'])) ? $params['item_options'] : array();
		
		//check if row_id exists,
		//if so, update it, and move on
		//or remove that paramter if no row_id isset
		if (isset($params['row_id']))
		{
			if ($item = $this->item($params['row_id']))
			{
				return $item->update($params);
			}
			
			//unset($params['row_id']);
		}
		
		if ( ! isset($params['class']))
		{
			$params['class'] = 'default';
		}
		
		if ($this->core->hooks->set_hook('cart_add_item_start')->run($params) && $this->core->hooks->end())
		{
			$params = $this->core->hooks->value();
		}
		
		$find_params = $params;
		unset($find_params['quantity']);//, $find_params['row_id']);
		
		$item = $this->find_item($find_params);
		
		if ( ! isset($params['row_id']))
		{
			$params['row_id'] = (count($this->items) > 0) ? end(array_keys($this->items)) + 1 : 0;
		}
		
		if ( ! isset($params['quantity']))
		{
			$params['quantity'] = 1;
		}
		
		if ($item !== FALSE && ! $this->core->store->config('allow_products_more_than_once'))
		{
			if ( ! $item->in_stock($item_options))
			{
				$this->core->set_error(sprintf($this->core->lang('item_not_in_stock_add_to_cart'), $item->title()));
			}
			
			$final_quantity = $item->quantity() + $params['quantity'];

			if ($this->core->store->config('global_item_limit') && $params['quantity'] + $this->count_all(array('product_id' => $params['product_id'])) > $this->core->store->config('global_item_limit'))
			{
				$final_quantity = $this->core->store->config('global_item_limit');
			}

			if ($item->inventory($item_options) !== FALSE && $params['quantity'] + $this->count_all(array('product_id' => $params['product_id'])) > $item->inventory())
			{
				$msg = ($item->inventory($item_options) == 1) ? $this->core->lang('item_quantity_greater_than_stock_add_to_cart_one') : $this->core->lang('item_quantity_greater_than_stock_add_to_cart');
				
				$this->core->set_error(sprintf($msg, $item->inventory($item_options), $item->title(), $item->inventory($item_options)));
			}

			$item->set_quantity($final_quantity);
		}
		else
		{
			$product = (isset($params['product_id'])) ? $this->core->store->product($params['product_id']) : FALSE;
			
			if ($product && ! $product->in_stock($item_options))
			{
				$this->core->set_error(sprintf($this->core->lang('item_not_in_stock_add_to_cart'), $product->title(), $product->inventory($item_options)));
			}
			else
			{
				if ($this->core->store->config('product_split_items_by_quantity') && $params['quantity'] > 1)
				{
					$quantity = $params['quantity'];
					
					$params['quantity'] = 1;
					
					for ($i = 1; $i <= $quantity; $i++)
					{
						$this->items[$params['row_id']] = Cartthrob_core::create_child($this->core, 'item_'.$params['class'], $params, $this->core->item_defaults);
						
						$params['row_id']++;
					}
				}
				else
				{
					$this->items[$params['row_id']] = Cartthrob_core::create_child($this->core, 'item_'.$params['class'], $params, $this->core->item_defaults);
				}
			}
		}
		
		//this hook call doesn't return a value
		$this->core->hooks->set_hook('cart_add_item_end')->run($this->item($params['row_id']), $params);
		
		return $this->item($params['row_id']);
	}
	
	/**
	 * Update an item in cart, identified by row_id
	 *
	 * @access public
	 * @param int $row_id
	 * @param array $params
	 * @return Cartthrob_cart
	 */
	public function update_item($row_id, $params = array())
	{
		if (FALSE !== ($item = $this->item($row_id)))
		{
			$item->update($params);
		}
		
		return $this;
	}
	
	/**
	 * Retrieve the saved coupon codes for this cart
	 *
	 * @access public
	 * @return array
	 */
	public function coupon_codes()
	{
		return $this->coupon_codes;
	}
	
	/**
	 * Clear this cart's coupon codes
	 *
	 * @access public
	 * @return Cartthrob_cart
	 */
	public function clear_coupon_codes()
	{
		$this->coupon_codes = array();
		
		return $this;
	}
	
	/**
	 * Add a coupon code to this cart, with validation
	 *
	 * @access public
	 * @param string $coupon_code
	 * @return Cartthrob_cart
	 */
	public function add_coupon_code($coupon_code)
	{
		if ($this->core->validate_coupon_code($coupon_code))
		{
			//in the case of a coupon limit of 1, we'll overwrite the coupon code
			if ($this->core->store->config('global_coupon_limit') == 1 && count($this->coupon_codes()) >= 1)
			{
				$this->coupon_codes = array($coupon_code);
			}
			else
			{
				if ( ! in_array($coupon_code, $this->coupon_codes))
				{
					$this->coupon_codes[] = $coupon_code;
				}
			}
		}
		
		return $this;
	}
	
	/**
	 * Remove an item from cart, indentified by row_id
	 *
	 * @access public
	 * @param int $row_id
	 * @return Cartthrob_cart
	 */
	public function remove_item($row_id)
	{
		if ($this->item($row_id))
		{
			unset($this->items[$row_id]);
		}
		
		return $this;
	}
	
	/**
	 * Duplicate an item from cart, indentified by row_id
	 *
	 * @access public
	 * @param array $params
	 * @return Cartthrob_cart
	 */
	public function duplicate_item($row_id)
	{
		if (is_object($row_id) && $row_id instanceof Cartthrob_item)
		{
			$item = $row_id;
		}
		else
		{
			$item = $this->item($row_id);
		}
		
		if ($item)
		{
			$params = $item->to_array();
			
			$params['row_id'] = (count($this->items) > 0) ? end(array_keys($this->items)) + 1 : 0;
			
			$this->add_item($params);
		}
		
		return $this;
	}
	
	/**
	 * Check the inventory status of each item in cart
	 *
	 * errors are added to $this->errors
	 *
	 * @access public
	 * @return bool
	 */
	public function check_inventory()
	{
		$this->clear_errors();
		
		foreach ($this->items() as $row_id => $item)
		{
			if ( ! $item->product_id())
			{
				continue;
			}
			
			if ($item->inventory() !== FALSE && $item->quantity() > $item->inventory())
			{
				if ($item->inventory() == 0)
				{
					$this->core->set_error(sprintf($this->core->lang('item_not_in_stock'), $item->title()));
				}
				else
				{
					$msg = ($item->inventory() == 1) ? $this->core->lang('item_quantity_greater_than_stock_one') : $this->core->lang('item_quantity_greater_than_stock');
					
					$this->core->set_error(sprintf($msg, $item->inventory(), $item->title(), $item->quantity() - $item->inventory()));
				}
			}
		}
		
		return (count($this->errors) <= 0);
	}
	
	/**
	 * Retrieve an item from the cart, or return false
	 *
	 * @access public
	 * @param string $row_id
	 * @return Cartthrob_item|false
	 */
	public function item($row_id)
	{
		return ($row_id !== FALSE && isset($this->items[$row_id])) ? $this->items[$row_id] : FALSE;
	}
	
	/**
	 * Find the first item in cart that matches all the data provided
	 *
	 * @access public
	 * @param array $data
	 * @return Cartthrob_item|false
	 */
	public function find_item(array $data)
	{
		foreach ($this->items as $item)
		{
			$array = $item->to_array();
			
			foreach ($data as $key => $value)
			{
				if (isset($array[$key]) && $value !== $array[$key])
				{
					continue 2;
				}
			}
			
			return $item;
		}
		
		return FALSE;
	}
	
	/**
	 * Retrieve all the items in the cart that match the provided data
	 *
	 * @access public
	 * @param array $data
	 * @return array
	 */
	public function filter_items($data = FALSE)
	{
		if ( ! $data || ! is_array($data))
		{
			return $this->items;
		}
		
		$items = array();
		
		foreach ($this->items as $item)
		{
			$array = $item->to_array();
			
			$match = TRUE;
			
			foreach ($data as $key => $value)
			{
				if (isset($array[$key]) && $value !== $array[$key])
				{
					$match = FALSE;
				}
			}
			
			if ($match)
			{
				$items[$item->row_id()] = $item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Retrieve all the items in the cart
	 *
	 * @access public
	 * @return array
	 */
	public function items()
	{
		return $this->items;
	}
	
	/**
	 * Retrieve all the items in the cart in array form
	 *
	 * @access public
	 * @return array
	 */
	public function items_array()
	{
		$items = array();
		
		foreach ($this->items as $row_id => $item)
		{
			$items[$row_id] = $item->data();
		}
		
		return $items;
	}
	
	/**
	 * Set a value in the customer info array,
	 * or set many values by providing an array.
	 *
	 * @param   array|string $key
	 * @param   mixed $value
	 * @return  Cartthrob_cart
	 */
	public function set_customer_info($key, $value = FALSE)
	{
		if ( ! is_array($key))
		{
			$key = array($key => $value);
		}
		
		$this->customer_info = array_merge($this->customer_info, $key);
		
		return $this;
	}
	
	/**
	 * Get a value from the customer info array, or
	 * get the whole array by not specifying a key
	 *
	 * @param   string|false $key
	 * @return  mixed|false
	 */
	public function customer_info($key = FALSE)
	{
		if ($key === FALSE)
		{
			return $this->customer_info;
		}
		
		return (isset($this->customer_info[$key])) ? $this->customer_info[$key] : FALSE;
	}
	
	/**
	 * Reset all default customer info values to empty
	 *
	 * @return  Cartthrob_cart
	 */
	public function clear_customer_info()
	{
		foreach ($this->core->customer_info_defaults as $key => $value)
		{
			$this->customer_info[$key] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Set a value in the shipping info array,
	 * or set many values by providing an array.
	 *
	 * @param   array|string $key
	 * @param   mixed $value
	 * @return  Cartthrob_cart
	 */
	public function set_shipping_info($key, $value = FALSE)
	{
		if ( ! is_array($key))
		{
			$key = array($key => $value);
		}
		
		$this->shipping_info = array_merge($this->shipping_info, $key);
		
		return $this;
	}
	
	/**
	 * Get a value from the shipping info array, or
	 * get the whole array by not specifying a key
	 *
	 * @param   string|false $key
	 * @return  mixed|false
	 */
	public function shipping_info($key = FALSE)
	{
		if ($key === FALSE)
		{
			return $this->shipping_info;
		}
		
		return (isset($this->shipping_info[$key])) ? $this->shipping_info[$key] : FALSE;
	}
	
	/**
	 * Set a value in the custom data array,
	 * or set many values by providing an array.
	 *
	 * @param   array|string $key
	 * @param   mixed $value
	 * @return  Cartthrob_cart
	 */
	public function set_custom_data($key, $value = FALSE)
	{
		if ( ! is_array($key))
		{
			$key = array($key => $value);
		}
		
		$this->custom_data = array_merge($this->custom_data, $key);
		
		return $this;
	}
	
	/**
	 * Get a value from the custom data array, or
	 * get the whole array by not specifying a key
	 *
	 * @param   string|false $key
	 * @return  mixed|false
	 */
	public function custom_data($key = FALSE)
	{
		if ($key === FALSE)
		{
			return $this->custom_data;
		}
		
		return (isset($this->custom_data[$key])) ? $this->custom_data[$key] : FALSE;
	}
	
	/**
	 * Get the number of items in the cart,
	 * W/ optional filter array
	 *
	 * @param   array|false $key
	 * @return  int
	 */
	public function count($filter = FALSE)
	{
		return count($this->filter_items($filter));
	}
	
	/**
	 * True if no items in cart
	 *
	 * @param   array|false $key
	 * @return  int
	 */
	public function is_empty()
	{
		return $this->count() == 0;
	}
	
	/**
	 * Get all of the unique product ids in the cart
	 *
	 * @return  array
	 */
	public function product_ids()
	{
		$product_ids = array();
		
		foreach ($this->items as $item)
		{
			if ($item->product_id())
			{
				$product_ids[] = $item->product_id();
			}
		}
		
		return array_unique($product_ids);
	}
	
	/**
	 * Get the sum of the quantity of items in the cart
	 * Optional filter array
	 *
	 * @param array|false $key
	 * @return int
	 */
	public function count_all($filter = FALSE)
	{
		$count = 0;
		
		foreach ($this->filter_items($filter) as $item)
		{
			$count += $item->quantity();
		}
		
		return $count;
	}
	
	/**
	 * Remove all items from the cart
	 *
	 * @return Cartthrob_cart
	 */
	public function clear()
	{
		$this->items = array();
		
		return $this;
	}
	
	/**
	 * Remove all the shipping info values
	 *
	 * @return  Cartthrob_cart
	 */
	public function clear_shipping_info()
	{
		$this->shipping_info = array();
		
		return $this;
	}
	
	/**
	 * Get the amount of discount associated with this cart
	 *
	 * @return string|float
	 */
	public function discount()
	{
		if ($this->calculation_caching === FALSE || is_null($this->discount))
		{
			if ($this->core->hooks->set_hook('cart_discount_start')->run() && $this->core->hooks->end())
			{
				$this->discount = $this->core->hooks->value();
			}
			else
			{
				$this->discount = 0;
		
				foreach ($this->coupon_codes() as $coupon_code)
				{
					$data = $this->core->get_coupon_code_data($coupon_code);
					
					if ($this->core->validate_coupon_code($coupon_code) && ! empty($data['type']))
					{
						$plugin = $this->core->create_child($this->core, $data['type'], $data);
						
						if (method_exists($plugin, 'get_discount'))
						{
							$this->discount += $this->core->round($plugin->get_discount());
						}
					}
				}
				
				foreach ($this->core->get_discount_data() as $data)
				{
					if (empty($data['type']))
					{
						continue;
					}
					
					$plugin = $this->core->create_child($this->core, $data['type'], $data);
						
					if (method_exists($plugin, 'get_discount'))
					{
						$this->discount += $this->core->round($plugin->get_discount());
					}
				}
		
				$this->discount = ($this->discount > 0) ? $this->core->round($this->discount) : 0;
				
				if ($this->core->hooks->set_hook('cart_discount_end')->run($this->discount) && $this->core->hooks->end())
				{
					$this->discount = $this->core->hooks->value();
				}
			}
		}
		
		return  $this->core->round($this->discount);
	}
	
	/**
	 * Get the total cost associated with this cart
	 *
	 * @return string|float
	 */
	public function total()
	{
		if ($this->calculation_caching === FALSE || is_null($this->total))
		{
			if ($this->core->hooks->set_hook('cart_total_start')->run() && $this->core->hooks->end())
			{
				$this->total = $this->core->hooks->value();
			}
			else
			{
				$this->total = $this->subtotal() + $this->tax() + $this->shipping() - $this->discount();
				
				if ($this->core->hooks->set_hook('cart_total_end')->run($this->total) && $this->core->hooks->end())
				{
					$this->total = $this->core->hooks->value();
				}
			}
		}
		
		if ($this->total < 0)
		{
			$this->total = 0;
		}
		
		return  $this->core->round($this->total);
	}
	
	/**
	 * Get the subtotal cost associated with this cart
	 *
	 * @return string|float
	 */
	public function subtotal()
	{
		if ($this->calculation_caching === FALSE || is_null($this->subtotal))
		{
			$this->subtotal = 0;
			
			foreach ($this->items() as $item)
			{
				$this->subtotal += $item->price() * $item->quantity();
			}
		}
		
		return  $this->core->round($this->subtotal);
	}
	
	/**
	 * Get the amount of tax associated with this cart
	 *
	 * @return string|float
	 */
	public function tax()
	{
		if ($this->calculation_caching === FALSE || is_null($this->tax))
		{
			if ( ! $this->items)
			{
				$this->shipping = 0;
			}
			else if ($this->core->hooks->set_hook('cart_tax_start')->run() && $this->core->hooks->end())
			{
				$this->shipping = $this->core->hooks->value();
			}
			else
			{
				$this->tax = 0;
				
				if ($this->core->store->config('tax_plugin'))
				{
					$plugin = $this->core->create_child($this->core, $this->core->store->config('tax_plugin'));
					
					if (method_exists($plugin, 'get_tax'))
					{
						$taxable_subtotal = $this->core->cart->taxable_subtotal() - $this->core->cart->discount();
						
						if ($plugin->tax_shipping())
						{
							$taxable_subtotal += $this->core->cart->shipping();
						}
						
						$this->tax = $this->core->round($plugin->get_tax($taxable_subtotal));
					}
				}
				
				if ($this->core->hooks->set_hook('cart_tax_end')->run($this->tax) && $this->core->hooks->end())
				{
					$this->tax = $this->core->hooks->value();
				}
			}
		}
		
		return  $this->core->round($this->tax);
	}
	
	/**
	 * Set the cart shipping cost
	 * 
	 * @param string|int|float $shipping
	 * 
	 * @return Cartthrob_cart
	 */
	public function set_shipping($shipping)
	{
		$this->shipping = $shipping;
		
		return $this;
	}
	
	/**
	 * Get the shipping cost associated with this cart
	 *
	 * @return string|float
	 */
	public function shipping()
	{
		if ($this->calculation_caching === FALSE || is_null($this->shipping))
		{
			if ( ! $this->items)
			{
				$this->shipping = 0;
			}
			else if ($this->core->hooks->set_hook('cart_shipping_start')->run() && $this->core->hooks->end())
			{
				$this->shipping = $this->core->hooks->value();
			}
			else
			{
				$this->shipping = 0;
				
				if ($this->core->store->config('shipping_plugin'))
				{
					$plugin = $this->core->create_child($this->core, $this->core->store->config('shipping_plugin'));
					
					if (method_exists($plugin, 'get_shipping'))
					{
						$this->shipping = $this->core->round($plugin->get_shipping());
					}
				}
				else
				{
					foreach ($this->core->cart->items() as $item)
					{
						$this->shipping += $item->shipping();
					}
				}
				
				if ($this->core->hooks->set_hook('cart_shipping_end')->run($this->shipping) && $this->core->hooks->end())
				{
					$this->shipping = $this->core->hooks->value();
				}
			}
		}
		
		return  $this->core->round($this->shipping);
	}
	
	/**
	 * Get the customer's selected shipping option
	 *
	 * @return string|false
	 */
	public function shipping_option()
	{
		return $this->shipping_info('shipping_option');
	}
	
	/**
	 * Reset the shipping cost cache, to recalculate
	 *
	 * @return Cartthrob_cart
	 */
	public function reset_shipping()
	{
		$this->shipping = NULL;
		
		return $this;
	}
	
	/**
	 * Reset the tax amount cache, to recalculate
	 *
	 * @return Cartthrob_cart
	 */
	public function reset_tax()
	{
		$this->tax = NULL;
		
		return $this;
	}
	
	/**
	 * Reset the total cost cache, to recalculate
	 *
	 * @return Cartthrob_cart
	 */
	public function reset_total()
	{
		$this->total = NULL;
		
		return $this;
	}
	
	/**
	 * Reset the subtotal cost cache, to recalculate
	 *
	 * @return Cartthrob_cart
	 */
	public function reset_subtotal()
	{
		$this->subtotal = NULL;
		$this->shippable_subtotal = NULL;
		$this->taxable_subtotal = NULL;
		
		return $this;
	}
	
	/**
	 * Reset the discount amount cache, to recalculate
	 *
	 * @return Cartthrob_cart
	 */
	public function reset_discount()
	{
		$this->discount = NULL;
		
		return $this;
	}
	
	/**
	 * Save the serialized cart to session
	 * using core driver's save_cart method
	 *
	 * @return Cartthrob_cart
	 */
	public function save()
	{
		$this->core->save_cart();
		
		return $this;
	}
	
	/**
	 * Reset the total weight of the items in cart
	 *
	 * @return int|float
	 */
	public function weight()
	{
		$weight = 0;
		
		foreach ($this->items() as $item)
		{
			$weight += $item->quantity() * $item->weight();
		}
		
		return $weight;
	}
	
	/**
	 * Get an array of items that are not marked no_shipping
	 *
	 * @return array
	 */
	public function shippable_items()
	{
		$items = array();
		
		foreach ($this->items as $item)
		{
			if ($item->is_shippable())
			{
				$items[$item->row_id()] = $item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Get the subtotal cost of items not marked no_shipping
	 *
	 * @return int|float
	 */
	public function shippable_subtotal()
	{
		if (is_null($this->shippable_subtotal))
		{
			$this->shippable_subtotal = 0;
			
			foreach ($this->shippable_items() as $item)
			{
				$this->shippable_subtotal += $item->price() * $item->quantity();
			}
		}
		
		return  $this->core->round($this->shippable_subtotal);
	}
	
	/**
	 * Get an array of items that are not marked no_tax
	 *
	 * @return array
	 */
	public function taxable_items()
	{
		$items = array();
		
		foreach ($this->items as $item)
		{
			if ($item->is_taxable())
			{
				$items[$item->row_id()] = $item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Get the subtotal cost of items not marked no_tax
	 *
	 * @return int|float
	 */
	public function taxable_subtotal()
	{
		if (is_null($this->taxable_subtotal))
		{
			$this->taxable_subtotal = 0;
			
			foreach ($this->taxable_items() as $item)
			{
				$this->taxable_subtotal += $item->price() * $item->quantity();
			}
		}
		
		return  $this->core->round($this->taxable_subtotal);
	}
	
	/* Cartthrob_child */
	
	public function to_array()
	{
		$data = parent::to_array();
		
		$data['items'] = $this->items_array();
		/*
		$data['items'] = array();
		
		foreach ($this->items as $row_id => $item)
		{
			$data['items'][$row_id] = $item->to_array();
		}
		*/
		
		return $data;
	}
	
	public function initialize($params = array())
	{
		$this->defaults['customer_info'] = $this->core->customer_info_defaults;
		
		$items = (isset($params['items'])) ? $params['items'] : array();
		
		unset($params['items']);
		
		if (is_array($this->core->store->config('default_location')))
		{
			foreach ($this->core->store->config('default_location') as $key => $value)
			{
				$this->defaults['customer_info'][$key] = $value;
			}
		}
		
		$this->defaults['customer_info']['currency_code'] = (string) $this->core->store->config('number_format_defaults_currency_code');
		
		parent::initialize($params);
		
		foreach ($items as $item)
		{
			$class = (isset($item['class'])) ? $item['class'] : 'default';
			
			$this->items[$item['row_id']] = Cartthrob_core::create_child($this->core, 'item_'.$class, $item, $this->core->item_defaults);
			//$this->add_item($item);
		}
		
		$this->core->store->override_config($this->config);
		
		return $this;
	}
}