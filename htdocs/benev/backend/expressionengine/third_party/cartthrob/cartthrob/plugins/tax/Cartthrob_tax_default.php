<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

class Cartthrob_tax_default extends Cartthrob_tax
{
	public $title = 'tax_by_location';
	public $settings = array(
		array(
			'name' => 'tax_by_location_settings',
			'short_name' => 'tax_settings',
			'type' => 'matrix',
			'settings' => array(
				array(
					'name' => 'name',
					'short_name' => 'name',
					'type' =>'text',	
				),
				array(
					'name' => 'tax_percent',
					'short_name' => 'rate',
					'type' => 'text'
				),
				array(
					'name' => 'state_country',
					'short_name' => 'state',
					'type' => 'select',
					'attributes' => array(
						'class' => 'states_and_countries',
					),
					'options' => array(),
				),
				array(
					'name' => 'zip_region',
					'short_name' => 'zip',
					'type' => 'text',
				),
				array(
					'name' => 'tax_shipping',
					'short_name' => 'tax_shipping',
					'type' => 'checkbox',
				),
			)
		)
	);
	
	protected $tax_data;
	
	public function get_tax($price)
	{
		return $this->core->round($price * $this->tax_rate());
	}
	
	public function tax_name()
	{
		return $this->tax_data('name');
	}
	
	public function tax_rate()
	{
		return $this->core->sanitize_number($this->tax_data('rate'))/100;
	}
	
	public function tax_shipping()
	{
		return (bool) $this->tax_data('tax_shipping');
	}
	
	public function tax_data($key = FALSE)
	{
		if (is_null($this->tax_data))
		{
			$this->tax_data = array();
			
			$prefix = ($this->core->store->config('tax_use_shipping_address')) ? 'shipping_' : '';
		
			foreach ($this->plugin_settings('tax_settings', array()) as $tax_data)
			{	
				//zip code first
				if ($this->core->cart->customer_info($prefix.'zip') && $tax_data['zip'] == $this->core->cart->customer_info($prefix.'zip'))
				{
					$this->tax_data = $tax_data;
					break;
				}
				elseif ($this->core->cart->customer_info($prefix.'region') && $tax_data['zip'] == $this->core->cart->customer_info($prefix.'region'))
				{
					$this->tax_data = $tax_data;
					break;
				}
				elseif ($this->core->cart->customer_info($prefix.'state') && $tax_data['state'] == $this->core->cart->customer_info($prefix.'state'))
				{
					$this->tax_data = $tax_data;
					break;
				}
				elseif ($this->core->cart->customer_info($prefix.'country_code') && $tax_data['state'] == $this->core->cart->customer_info($prefix.'country_code'))
				{
					$this->tax_data = $tax_data;
					break;
				}
				//elseif (array_key_exists('global', $tax_data))
				// 'global' is set in the state dropdown so it's not an array key, it's a value of $tax_data['state']
				elseif (in_array('global', $tax_data))
				{
					$this->tax_data = $tax_data;
					break;
				}
			}
			
		}
		
		if ($key === FALSE)
		{
			return $this->tax_data;
		}
		
		return (isset($this->tax_data[$key])) ? $this->tax_data[$key] : FALSE;
	}
	
	/*
	public function tax_rate()
	{
		$percentage = FALSE;
		
		$prefix = ($this->plugin_settings('use_shipping_address')) ? 'shipping_' : '';
		
		$tax_settings = $this->tax_settings();

		$customer_info = $this->core->cart->customer_info();

		$tax_rate = 0;

		//zip code first
		if ( ! empty($customer_info[$prefix.'zip']) && ! empty($tax_settings[$customer_info[$prefix.'zip']]))
		{
			$tax_rate = $tax_settings[$customer_info[$prefix.'zip']];
		}
		elseif ( ! empty($customer_info[$prefix.'region']) && ! empty($tax_settings[$customer_info[$prefix.'region']]))
		{
			$tax_rate = $tax_settings[$customer_info[$prefix.'region']];
		}
		elseif ( ! empty($customer_info[$prefix.'state']) && ! empty($tax_settings[$customer_info[$prefix.'state']]))
		{
			$tax_rate = $tax_settings[$customer_info[$prefix.'state']];
		}
		elseif ( ! empty($customer_info[$prefix.'country_code']) && ! empty($tax_settings[$customer_info[$prefix.'country_code']]))
		{
			$tax_rate = $tax_settings[$customer_info[$prefix.'country_code']];
		}
		elseif (array_key_exists('global', $tax_settings))
		{
			$tax_rate = $tax_settings['global'];
		}

		return ($percentage) ? $tax_rate : ($tax_rate/100);
	}

	public function tax_name()
	{
		$tax_names = $this->tax_names();

		$customer_info = $this->core->cart->customer_info();

		$tax_name = '';

		//zip code first
		if ( ! empty($customer_info['zip']) && ! empty($tax_names[$customer_info['zip']]))
		{
			$tax_name = $tax_names[$customer_info['zip']];
		}
		elseif ( ! empty($customer_info['region']) &&  ! empty($tax_names[$customer_info['region']]))
		{
			$tax_name = $tax_names[$customer_info['region']];
		}
		elseif ( ! empty($customer_info['state']) &&  ! empty($tax_names[$customer_info['state']]))
		{
			$tax_name = $tax_names[$customer_info['state']];
		}
		elseif ( ! empty($customer_info['country_code']) &&  ! empty($tax_names[$customer_info['country_code']]))
		{
			$tax_name = $tax_names[$customer_info['country_code']];
		}
		elseif (array_key_exists('global', $tax_names))
		{
			$tax_name = $tax_names['global'];
		}

		return $tax_name;
	}

	protected function tax_shipping()
	{
		$customer_info = $this->core->cart->customer_info();

		$tax_shipping = array();

		foreach ($this->plugin_settings('tax_settings', array()) as $setting)
		{
			if ( ! empty($setting['tax_shipping']))
			{
				$locale = ($setting['state']) ? $setting['state'] : $setting['zip'];

				if ($locale)
				{
					$tax_shipping[] = $locale;
				}
			}
		}

		if ( ! count($tax_shipping))
		{
			return FALSE;
		}

		//zip code first
		if ($customer_info['zip'] && in_array($customer_info['zip'], $tax_shipping))
		{
			return TRUE;
		}
		elseif ($customer_info['region'] && in_array($customer_info['region'], $tax_shipping))
		{
			return TRUE;
		}
		elseif ($customer_info['state'] && in_array($customer_info['state'], $tax_shipping))
		{
			return TRUE;
		}
		elseif ($customer_info['country_code'] && in_array($customer_info['country_code'], $tax_shipping))
		{
			return TRUE;
		}
		elseif (in_array('global', $tax_shipping))
		{
			return TRUE;
		}

		return FALSE;
	}

	protected function tax_settings()
	{
		$tax_settings = array();
		
		foreach ($this->plugin_settings('tax_settings', array()) as $setting)
		{
			$locale = ($setting['state']) ? $setting['state'] : $setting['zip'];

			if ($locale)
			{
				//determine whether user put in actual rate, or percentage
				//ie. 8.75 vs. .0875
				$setting['rate'] = ($setting['rate'] < 1) ? ($setting['rate']*100) : $setting['rate'];

				$tax_settings[$locale] = $setting['rate'];
			}
		}

		return $tax_settings;
	}

	protected function tax_names()
	{
		$tax_names = array();

		foreach ($this->plugin_settings('tax_settings', array()) as $setting)
		{
			$locale = ($setting['state']) ? $setting['state'] : $setting['zip'];

			if ($locale)
			{
				$tax_names[$locale] = $setting['name'];
			}
		}

		return $tax_names;
	}
	*/
}