<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

class Cartthrob_shipping_by_price_threshold extends Cartthrob_shipping
{
	public $title = 'title_price_threshold';
	public $classname = __CLASS__;
	public $note = 'price_threshold_overview';
	public $settings = array(
		array(
			'name' => 'set_shipping_cost_by',
			'short_name' => 'mode',
			'type' => 'radio',
			'default' => 'price',
			'options' => array(
				'price' => 'rate_amount',
				'rate' => 'rate_amount_times_cart_total'
			)
		),
		array(
			'name' => 'thresholds',
			'short_name' => 'thresholds',
			'type' => 'matrix',
			'settings' => array(
				array(
					'name' => 'rate',
					'short_name' => 'rate',
					'note' => 'rate_example',
					'type' => 'text'
				),
				array(
					'name' => 'price_threshold',
					'short_name' => 'threshold',
					'note' => 'price_threshold_example',
					'type' => 'text'
				)
			)
		)
	);
	
	protected $thresholds = array();
	
	public function initialize()
	{
		foreach ($this->plugin_settings('thresholds', array()) as $threshold)
		{
			$this->thresholds[$threshold['threshold']] = $threshold['rate'];
		}
		
		ksort($this->thresholds);
	}

	public function get_shipping()
	{
		$shipping = 0;
	
		$price = $this->core->cart->shippable_subtotal();

		$priced = FALSE;
	
		foreach ($this->thresholds as $threshold => $rate)
		{
			if ($price > $threshold)
			{
				continue;
			}
			else
			{
				$shipping = ($this->plugin_settings('mode') == 'rate') ? $price * $rate : $rate;
			
				$priced = TRUE;
			
				break;
			}
		}
	
		if ( ! $priced)
		{
			$shipping = ($this->plugin_settings('mode') == 'rate') ? $price * end($this->thresholds) : end($this->thresholds);
		}
	
		return $shipping;
	}
}