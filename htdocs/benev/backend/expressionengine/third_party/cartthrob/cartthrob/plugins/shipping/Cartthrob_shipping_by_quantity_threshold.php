<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

class Cartthrob_shipping_by_quantity_threshold extends Cartthrob_shipping
{
	public $title = 'title_by_quantity_threshold';
	public $classname = __CLASS__;
	public $note = 'costs_are_set_at';
	public $settings = array(   
		array(
			'name' => 'calculate_costs',
			'short_name' => 'mode',
			'type' => 'radio',
			'default' => 'price',
			'options' => array(
				'price' => 'use_rate_as_shipping_cost',
				'rate' => 'multiply_rate_by_quantity'
			)
		),
		array(
			'name' => 'thresholds',
			'short_name' => 'thresholds',
			'type' => 'matrix',
			'settings' => array(
				array(
					'name' => 'rate',
					'short_name' => 'rate',
					'note' => 'rate_example',
					'type' => 'text'
				),
				array(
					'name' => 'quantity_threshold',
					'short_name' => 'threshold',
					'type' => 'text'
				)
			)
		)
	);
	
	protected $thresholds = array();
	
	public function initialize()
	{
		foreach ($this->plugin_settings('thresholds', array()) as $threshold)
		{
			$this->thresholds[$threshold['threshold']] = $threshold['rate'];
		}
		
		ksort($this->thresholds);
	}

	public function get_shipping()
	{
		$shipping = 0;

		$total_items = $this->core->cart->count_all(array('no_shipping' => FALSE));
		$priced = FALSE;
	
		foreach ($this->thresholds as $threshold => $rate)
		{
			if ($total_items > $threshold)
			{
				continue;
			}
			else
			{
				$shipping = ($this->plugin_settings('mode') == 'rate') ? $total_items * $rate : $rate;
			
				$priced = TRUE;
			
				break;
			}
		}
	
		if ( ! $priced)
		{
			$shipping = ($this->plugin_settings('mode') == 'rate') ? $total_items * end($this->thresholds) : end($this->thresholds);
		}
	
		return $shipping;
	}
}