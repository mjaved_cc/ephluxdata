<?php if ( ! defined('CARTTHROB_PATH')) Cartthrob_core::core_error('No direct script access allowed');

class Cartthrob_core_ee extends Cartthrob_core
{
	public $item_defaults = array(
		'entry_id' => NULL,
		//'expiration_date' => NULL,
		//'license_number' => NULL
	);
	public $product_defaults = array(
		'entry_id' => NULL,
		'url_title' => NULL
	);
	public $customer_info_defaults = array(
		'first_name' => '',
		'last_name' => '',
		'address' => '',
		'address2' => '',
		'city' => '',
		'state' => '',
		'zip' => '',
		'country' => '',
		'country_code' => '',
		'company' => '',

		'phone' => '',
		'email_address' => '',
		'ip_address' => '',
		'description' => '',
		'use_billing_info' => '',

		'shipping_first_name' => '',
		'shipping_last_name' => '',
		'shipping_address' => '',
		'shipping_address2' => '',
		'shipping_city' => '',
		'shipping_state' => '',
		'shipping_zip' => '',
		'shipping_country' => '',
		'shipping_country_code' => '',
		'shipping_company' => '',
		'shipping_region' => '',

		'CVV2' => '',
		'card_type' => '',
		'expiration_month' => '',
		'expiration_year' => '',
		'begin_month' => '',
		'begin_year' => '',
		'bday_month' => '',
		'bday_day' => '',
		'bday_year' => '',

		'currency_code' => '',
		'language' => '',

		'shipping_option' => '',
		'weight_unit' => '',

		'region' => '',

		'success_return' => '',
		'cancel_return' => '',

		'po_number' => '',
		'card_code' => '',
		'issue_number' => '',
		'transaction_type' => '',
		'bank_account_number' => '',
		'check_type' => '',
		'account_type' => '',
		'routing_number' => '',

		// MEMBER CREATION
		//'username' => '', 
		//'screen_name' => '',
		//'password' => '',
		//'password_confirm' => '', 
		//'create_member' => '',
		//'group_id' => '',
		
		// RECURRENT BILLING
		'subscription_name' => '',
		'subscription_total_occurrences' => '',
		'subscription_trial_price' => '',
		'subscription_trial_occurrences' => '',
		'subscription_start_date' => '',
		'subscription_end_date' => '',
		'subscription_interval' => '', // pay every X 
		'subscription_interval_units' => '', // D, W, M, Y
		'subscription_allow_modification' => '', // can subscribers change subscription
	);
	
	private $cart_hash;
	
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->config->load('../third_party/cartthrob/config/config');
		$this->EE->lang->loadfile('cartthrob_errors', 'cartthrob');
		$this->EE->lang->loadfile('cartthrob', 'cartthrob');
		self::add_plugin_path('shipping', PATH_THIRD.'cartthrob/third_party/shipping_plugins/');
		self::add_plugin_path('discount', PATH_THIRD.'cartthrob/third_party/discount_plugins/');
		self::add_plugin_path('tax', PATH_THIRD.'cartthrob/third_party/tax_plugins/');
		self::add_plugin_path('price', PATH_THIRD.'cartthrob/third_party/price_plugins/');
	}
	
	/* core */
	public function log($msg)
	{
		//log_message('debug', $msg);
		$this->EE->load->library('logger');
		$this->EE->logger->log_action($msg);
	}
	
	public function lang($key)
	{
		return $this->EE->lang->line($key);
	}
	
	public function get_hooks()
	{
		return array(
			'cart_total_start',
			'cart_total_end',
			'cart_discount_start',
			'cart_tax_end',
			'cart_shipping_end',
			'product_reduce_inventory',
			'product_meta',
			'product_price',
			'product_inventory',
		);
	}
	
	public function get_settings()
	{
		if (isset($this->EE->session->cache['cartthrob']['settings'][$this->EE->config->item('site_id')]))
		{
			return $this->EE->session->cache['cartthrob']['settings'][$this->EE->config->item('site_id')];
		}
		
		$settings = $this->EE->config->item('cartthrob_default_settings');
		
		foreach ($this->EE->db->where('site_id', $this->EE->config->item('site_id'))->get('cartthrob_settings')->result() as $row)
		{
			if ($row->serialized)
			{
				$row->value = unserialize($row->value);
			}
			
			$settings[$row->key] = $row->value;
		}
		
		$this->EE->session->cache['cartthrob']['settings'][$this->EE->config->item('site_id')] = $settings;
		
		return $settings;
	}
	
	public function get_cart()
	{
		$this->EE->load->library('cartthrob_session', array($this));
		
		$cart = $this->EE->cartthrob_session->custom_userdata();
		
		if (is_array($this->store->config('default_location')))
		{
			foreach ($this->store->config('default_location') as $key => $value)
			{
				$this->customer_info_defaults[$key] = $value;
			}
		}
		
		if ( ! isset($cart['customer_info']))
		{
			$cart['customer_info'] = $this->customer_info_defaults;
		}
		
		$cart['customer_info']['currency_code'] = (string) $this->store->config('number_format_defaults_currency_code');
		
		if ($this->EE->session->userdata('member_id') && $this->store->config('save_member_data'))
		{
			$this->EE->load->model('member_model');
			
			$member_data = $this->EE->member_model->get_all_member_data($this->EE->session->userdata('member_id'))->row_array();
			
			foreach ($this->customer_info_defaults as $key => $value)
			{
				$member_field = $this->store->config('member_'.$key.'_field');
				
				if ($member_field && isset($member_data['m_field_id_'.$member_field]))
				{
					$cart['customer_info'][$key] = $member_data['m_field_id_'.$member_field];
				}
			}
		}
		
		return $cart;
	}
	
	public function get_product($entry_id)
	{
		$this->EE->load->model('product_model');
		
		$product = self::create_child($this, 'product', $this->EE->product_model->get_product($entry_id), $this->product_defaults);
		
		$product->set_item_options($this->EE->product_model->get_all_price_modifiers($entry_id));
		
		return $product;
	}
	
	public function get_categories()
	{
		$this->EE->load->model('product_model');
		
		$categories = array();
		
		foreach ($this->EE->product_model->get_categories() as $category)
		{
			$categories[$category['category_id']] = $category['category_name'];
		}
		
		return $categories;
	}
	
	public function save_cart()
	{
		$cart = $this->cart->to_array();
		
		//let's strip the array of data that matches the default data
		//to minimize the size of the array before we save it
		foreach ($cart as $key => $value)
		{
			if ($value === $this->cart->defaults($key))
			{
				unset($this->EE->cartthrob_session->userdata[$key]);
				unset($cart[$key]);
			}
		}
		
		if (isset($cart['items']))
		{
			foreach ($cart['items'] as $row_id => $item)
			{
				foreach ($item as $key => $value)
				{
					if ($value === $this->cart->item($row_id)->defaults($key))
					{
						unset($cart['items'][$row_id][$key]);
					}
				}
			}
		}
		
		$this->EE->cartthrob_session->set_userdata($cart);
	}
	
	public function process_inventory()
	{
		$inventory_reduce = array();

		foreach ($this->cart->items() as $row_id => $item)
		{
			if ($item->product_id() && $product = $this->store->product($item->product_id()))
			{
				$product->reduce_inventory($item->quantity(), $item->item_options());
			}

			if (is_array($item->meta('inventory_reduce')))
			{
				foreach ($item->meta('inventory_reduce') as $entry_id => $quantity)
				{
					$inventory_reduce[] = array(
						'entry_id' => $entry_id,
						'quantity' => $quantity
					);
				}
			}
		}

		foreach ($inventory_reduce as $row)
		{
			if ($product = $this->store->product($row['entry_id']))
			{
				$product->reduce_inventory($row['quantity']);
			}
		}
		
		return $this;
	}
	
	//formerly process_coupon_codes
	public function process_discounts()
	{
		$this->EE->load->model('discount_model');
		
		$this->EE->load->model('coupon_code_model');
		
		$this->EE->discount_model->process_discounts();
		
		$this->EE->coupon_code_model->process_coupon_codes();
		
		return $this;
	}
	
	public function validate_coupon_code($coupon_code)
	{
		$this->EE->load->model('coupon_code_model');
		
		return $this->EE->coupon_code_model->validate_coupon_code($coupon_code);
	}
	
	public function get_coupon_code_data($coupon_code)
	{
		$this->EE->load->model('coupon_code_model');
		
		return $this->EE->coupon_code_model->get_coupon_code_data($coupon_code);
	}
	
	public function get_discount_data()
	{
		$this->EE->load->model('discount_model');
		
		return $this->EE->discount_model->get_valid_discounts();
	}
	
	public function set_config_customer_info($params)
	{
		if ( ! empty($params['field']) && isset($params['value']))
		{
			if (preg_match('/^customer_(.*)/', $params['field'], $match))
			{
				$params['field'] = $match[1];
			}
			
			$this->cart->set_customer_info($params['field'], $params['value']);
		}
	}
	
	/**
	 * _set_config_shipping_plugin
	 *
	 * sets the selected shipping plugin
	 * 
	 * @param string $params shipping parameter short_name (ie. by_weight_ups_xml)
	 * @return void
	 * @author Chris Newton
	 * @since 1.0
	 */
	public function set_config_shipping_plugin($params)
	{
		if (isset($params['value']))
		{
			if (strpos($params['value'], 'shipping_') !== 0)
			{
				$params['value'] = 'shipping_'.$params['value'];
			}
			
			$this->cart->set_config('shipping_plugin', 'Cartthrob_'.$params['value']);
			
			$this->cart->reset_shipping();
		}
	}
	
	public function set_config_price_field($params)
	{
		if (empty($params['field']))
		{
			if (empty($params['value']))
			{
				return;
			}
			else
			{
				$params['field'] = $params['value'];
			}
		}
		
		if (empty($params['channel_id']) && empty($params['channel']))
		{
			return;
		}

		$this->EE->load->model('cartthrob_field_model');
		
		if ( ! ($field_id = $this->EE->cartthrob_field_model->get_field_id($params['field'])))
		{
			return;
		}
		if (!empty($params['channel']))
		{
			$params['channel_id'] = $this->EE->db->select('channel_id')->where('channel_name', $params['channel'])->get('channel_titles')->row('channel_id');
 		}		
		$product_channel_fields = ($this->store->config('product_channel_fields')) ? $this->store->config('product_channel_fields') : array();
		
		$product_channel_fields[$params['channel_id']]['price'] = $field_id;
		
		$this->cart->set_config('product_channel_fields', $product_channel_fields);
	}
	
	/* non-core utilities */
	
	public function save_customer_info()
	{
		$this->EE->load->library('locales');
	
		if ( ! isset($_POST['country_code']))
		{
			if ($this->EE->input->post('country') && $country_code = $this->EE->locales->country_code($this->EE->input->post('country')))
			{
				$_POST['country_code'] = $country_code;
			}
		}
		
		foreach (array_keys($this->cart->customer_info()) as $field)
		{
			if ($this->EE->input->post($field) !== FALSE)
			{
				$this->cart->set_customer_info($field, $this->EE->input->post($field, TRUE));
			
				if ($this->store->config('save_member_data') && $this->store->config('member_'.$field.'_field'))
				{
					$member_data['m_field_id_'.$this->store->config('member_'.$field.'_field')] = $this->cart->customer_info($field);
				}
			}
		}

		if ($this->cart->customer_info('use_billing_info'))
		{
			$billing_fields = array(
				'first_name',
				'last_name',
				'address',
				'address2',
				'city',
				'state',
				'zip',
				'region',
				'country',
				'country_code',
				'company'
			);

			foreach ($billing_fields as $field)
			{
				$this->cart->set_customer_info('shipping_'.$field, $this->cart->customer_info($field));
			
				if ($this->store->config('save_member_data') && $this->store->config('member_shipping_'.$field.'_field'))
				{
					$member_data['m_field_id_'.$this->store->config('member_shipping_'.$field.'_field')] = $this->cart->customer_info($field);
				}
			}

			unset($billing_fields);
		}
		
		if ( ! empty($member_data) && $this->EE->session->userdata('member_id'))
		{
			$this->EE->load->model('member_model');
			
			$this->EE->member_model->update_member_data($this->EE->session->userdata('member_id'), $member_data);
		}
		
		$this->EE->load->library('languages');
		
		$this->EE->languages->set_language($this->EE->input->post('language', TRUE));
		
		if (($data = $this->EE->input->post('custom_data', TRUE)) && is_array($data))
		{
			foreach ($data as $key => $value)
			{
				$this->cart->set_custom_data($key, $value);
			}
		}
		
		if ($this->EE->input->post('shipping_option'))
		{
			$this->cart->set_shipping_info('shipping_option', $this->EE->input->post('shipping_option', TRUE));
		}
		
		if ($data = $this->EE->input->post('shipping', TRUE) && is_array($data))
		{
			foreach ($data as $key => $value)
			{
				$this->cart->set_shipping_info($key, $value);
			}
		}
	}
	
	/**
	 * Hooks
	 *
	 * To use the hooks found in the Cartthrob_child objects, create a method
	 * by prefixing the class short name and the hook name. For example, to
	 * use the Cartthrob_cart class' add_item_end hook, add a method here
	 * called cart_add_item_end.
	 * 
	 */
	/*
	function cart_add_item_end($item, $params)
	{
		if (is_null($item->product_id()) && isset($params['entry_id']))
		{
			$item->set_product_id($params['entry_id']);
		}
	}
	*/
	
	function cart_total_start()
	{
		// cartthrob_calculate_total hook
		if ($this->EE->extensions->active_hook('cartthrob_calculate_total') === TRUE)
		{
			if (($total = $this->EE->extensions->call('cartthrob_calculate_total')) !== FALSE)
			{
				$this->hooks->set_end();
				
				return $total;
			}
		}
	}
	
	public function cart_discount_start()
	{
		if ($this->EE->extensions->active_hook('cartthrob_calculate_discount') === TRUE)
		{
			if (($discount = $this->EE->extensions->call('cartthrob_calculate_discount')) !== FALSE)
			{
				$this->hooks->set_end();
				
				return $discount;
			}
		}
	}
	
	public function cart_shipping_end($shipping)
	{
		if ($this->EE->extensions->active_hook('cartthrob_calculate_shipping') === TRUE)
		{
			$this->hooks->set_end();
			
			return $this->EE->extensions->call('cartthrob_calculate_shipping', $shipping);
		}
	}
	
	public function cart_tax_end($tax)
	{
		if ($this->EE->extensions->active_hook('cartthrob_calculate_tax') === TRUE)
		{
			$this->hooks->set_end();
			
			return $this->EE->extensions->call('cartthrob_calculate_tax', $tax);
		}
	}
	
	public function item_shipping_end($shipping)
	{
		if ($this->EE->extensions->active_hook('cartthrob_calculate_item_shipping') === TRUE)
		{
			$this->hooks->set_end();
			
			return $this->EE->extensions->call('cartthrob_calculate_item_shipping', $shipping);
		}
	}
	
	public function product_meta(Cartthrob_product $product, $key)
	{
		$this->EE->load->model(array('cartthrob_field_model', 'product_model'));
		
		$data = $this->EE->product_model->get_product($product->product_id());
		
		if ($key === FALSE)
		{
			$this->hooks->set_end();
			return $data;
		}
		
		if (isset($data[$key]))
		{
			$this->hooks->set_end();
			return $data[$key];
		}
		
		if ($field_id = $this->EE->cartthrob_field_model->get_field_id($key) && isset($data['field_id_'.$field_id]))
		{
			$this->hooks->set_end();
			return $data['field_id_'.$field_id];
		}
	}
	
	public function product_price(Cartthrob_product $product, $item = FALSE)
	{
		$this->EE->load->model(array('cartthrob_field_model', 'product_model'));
		
		$data = $this->EE->product_model->get_product($product->product_id());
		
		if ($channel_id = element('channel_id', $data))
		{
			$global_price = $this->store->config('product_channel_fields', $channel_id, 'global_price');
			
			if ($global_price !== FALSE && $global_price !== '')
			{
				$this->hooks->set_end();
				
				return $global_price;
			}
			
			if ($item instanceof Cartthrob_item)
			{
				$field_id = $this->store->config('product_channel_fields', $channel_id, 'price');
				
				if ($field_id && $this->EE->cartthrob_field_model->get_field_type($field_id) === 'cartthrob_price_quantity_thresholds')
				{
					$data = _unserialize($data['field_id_'.$field_id], TRUE);
					
					while(($row = current($data)) !== FALSE)
					{
						// if quantity is within the thresholds
						// OR if we get to the end of the array
						// the last row will set the price, no matter what
						if (next($data) === FALSE || ($item->quantity() >= $row['from_quantity'] && $item->quantity() <= $row['up_to_quantity']))
						{
							/*
							if ($item->is_taxable() && $this->config('tax_inclusive_price'))
							{
								// @TODO notax
								return $this->_calculate_price_with_tax( $data[$i]['price'] ); 
							}
							*/
							
							return $row['price'];
						}
					}
	
					return 0;
				}
			}
		}
	}
	
	public function product_inventory(Cartthrob_product $product, $item_options)
	{
		$this->hooks->set_end();
		
		$hash = md5($product->product_id().serialize($item_options));
		
		if (FALSE !== ($inventory = $this->cache($hash)))
		{
			return $inventory;
		}
		
		$inventory = PHP_INT_MAX;
		
		$this->EE->load->model(array('cartthrob_field_model', 'product_model'));
		
		$data = $this->EE->product_model->get_product($product->product_id());
		
		$channel_id = element('channel_id', $data);
		
		if ($channel_id && $field_id = $this->store->config('product_channel_fields', $channel_id, 'inventory'))
		{
			$field_name = $this->EE->cartthrob_field_model->get_field_name($field_id);
			
			if (in_array($this->EE->cartthrob_field_model->get_field_type($field_id), array('cartthrob_price_modifiers', 'matrix')))
			{
				$price_modifiers = $this->EE->product_model->get_price_modifiers($product->product_id(), $field_id);
				
				if (isset($item_options[$field_name]))
				{
					foreach ($price_modifiers as $row)
					{
						if ($item_options[$field_name] == $row['option_value'])
						{
							$inventory = element('inventory', $row);
							continue;
						}
					}
				}
			}
			else
			{
				$inventory = element('field_id_'.$field_id, $data);
				
				if ($inventory === FALSE || $inventory === '')
				{
					$inventory = PHP_INT_MAX;
				}
			}
		}
		
		$this->set_cache($hash, $inventory);
		
		return $inventory;
	}
	
	public function product_reduce_inventory(Cartthrob_product $product, $quantity, $args)
	{
		//because of the way hooks work,
		//and how we call this in the process_inventory method above
		//item_options are the first arg in args
		$item_options = (isset($args[0])) ? $args[0] : array();
		
		$this->EE->load->model('product_model');
		
		$inventory = $this->EE->product_model->reduce_inventory($product->product_id(), $quantity, $item_options);
		if ($inventory!== FALSE && $this->store->config('send_inventory_email'))
		{
			if ($inventory <= $this->store->config('low_stock_level'))
			{
				$this->EE->load->library('cartthrob_emails');
				$this->EE->cartthrob_emails->send_low_inventory_email($product->product_id(), $inventory);				
			}
		}
		$this->hooks->set_end();
	}
	
	/**
	 * Custom Methods
	 *
	 * Create custom methods for the Cartthrob_child objects by prefixing the
	 * class short name. For example, to create an Cartthrob_item::entry_id()
	 * method, add a method here called item_entry_id().
	 *
	 * Please use sparingly as __call and call_user_func_array are expensive.
	 * 
	 */
	public function item_entry_id(Cartthrob_item $item)
	{
		return $item->product_id();
	}
	
	public function item_product_entry_id(Cartthrob_item $item)
	{
		return $item->product_id();
	}
	
	public function cart_info($number_format = TRUE)
	{
		$this->EE->load->library('api/api_cartthrob_tax_plugins');
		
		$cart_info = array(
			'total_unique_items'	=> $this->cart->count(),
			'cart_tax_name'			=> $this->EE->api_cartthrob_tax_plugins->tax_name(),
			'total_items'			=> $this->cart->count_all(),
			'cart_tax'				=> $this->cart->tax(),
			'cart_total'			=> $this->cart->total(),
			'cart_subtotal'			=> $this->cart->subtotal(),
			'cart_tax_rate'			=> $this->EE->api_cartthrob_tax_plugins->tax_rate(),
			'cart_shipping'			=> $this->cart->shipping(),
			'cart_entry_ids'		=> implode('|', $this->cart->product_ids()),
			'cart_discount'			=> $this->cart->discount(),	
			//'cart_subtotal_plus_shipping' 	=> $this->cart->subtotal() + $this->cart->shipping(),
			'shipping_option'		=> $this->cart->shipping_info('shipping_option')
		);
		
		if ($number_format)
		{
			$this->EE->load->library('number');
			
			foreach (array('cart_tax', 'cart_total', 'cart_subtotal', 'cart_shipping', 'cart_discount') as $key)
			{
				$cart_info[$key.'_numeric'] = $cart_info[$key];
				$cart_info[$key] = $this->EE->number->format($cart_info[$key]);
			}
		}
		
		return $cart_info;
	}
}