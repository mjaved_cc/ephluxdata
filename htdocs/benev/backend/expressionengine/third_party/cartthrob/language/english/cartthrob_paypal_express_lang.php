<?php   
$lang = array(
		

		//PAYPAL EXPRESS ERRORS & CONTENT
		'paypal_express_title' => 'PayPal Express',
		'paypal_express_sandbox_mode' => 'Sandbox Mode?',
		'paypal_express_signature' => 'Signature',
		'paypal_express_api_password' => 'API Password',
		'paypal_express_api_username' => 'API Username',
		'paypal_express_sandbox_signature' => 'Sandbox Signature',
		'paypal_express_sandbox_api_password' => 'Sandbox API Password',
		'paypal_express_sandbox_api_username' => 'Sandbox API Username',
		
		'paypal_express_overview' => '<p>Unlike PayPal standard, PayPal Express allows you to take PayPal payments and receive instant notifcation of success or failure, making it a much more attractive option for selling digital and downloadable goods, or merchandise that needs to be immediately shipped. This particular implementation of PayPal express requires that you collect all of your customer\'s data before being sent to PayPal. While on PayPal the customer\'s saved shipping addresses are not displayed, and shipping information can not be edited. This ensures that the shipping calculations made on your site are honored.</p> ',
		'paypal_express_affiliate' => '<p><A HREF="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A" target="_blank"><IMG  SRC="http://images.paypal.com/en_US/i/bnr/paypal_mrb_banner.gif" BORDER="0" ALT="Sign up for PayPal and start accepting credit card payments instantly."></A></p><p><a href="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A">Click here to sign up now!</a></p>', 
		'paypal_express_payment_action'	=> 'Payment Action Type',
		'paypal_express_did_not_respond'	=> 'PayPal did not respond',
		'paypal_express_you_cancelled'	=> 'You cancelled your payment at PayPal',
);