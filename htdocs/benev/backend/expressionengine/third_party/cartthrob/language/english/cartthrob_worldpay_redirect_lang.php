<?php   
$lang = array(
	
	// RBS WORLDPAY ERRORS & CONTENT
	'worldpay_redirect_title'			=> 'RBS Worldpay Redirect Payment Gateway (Experimental)',
	'worldpay_redirect_overview'			=> "<p>In the WorldPay Merchant Interface > Installations settings, you will need to set your Payment Response URL to the following url: <pre>http://&lt;WPDISPLAY ITEM=MC_callback&gt;</pre> You must also check 'Payment Response Enabled' and 'Enable Shopper Response'.</p>
		<p>Please note: WorldPay does not redirect customers back to your site when they're done processing the transaction. WorldPay pulls in content from your order complete template, so you'll have to use full URL paths to to include any images or navigation. You'll also need to make sure the image URLs use https:// links. Since Worldpay's page is secured, it will balk if your images are not also secured. Javascript and Flash or other web objects will not be shown by WorldPay on their success page. It's best to keep your template simple.</p>",
	'worldpay_redirect_installation_id'	=> 'Installation ID',
	'worldpay_transaction_failure'		=> 'Worldpay data was not successfully transmitted. Please contact site owner to make sure your transaction went through successfully.',
	'worldpay_default_template'	=> 'Default Order Complete Template (used if return parameter is not set)',

);