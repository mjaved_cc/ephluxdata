<?php   
$lang = array(
		'ct_pay_by_phone_title' => 'Pay By Phone',
 		'ct_pay_by_phone_transaction_id'	=> 'Payment by phone',
		'ct_pay_by_phone_overview'	=> 'This payment method is intended for use when you will be collecting payment from the customer via the phone. 
				Orders placed with this gateway will be assigned your default processing status', 
		'ct_pay_by_phone_processing_call_in'	=> 'Call us to complete this order',
		'ct_pay_by_phone_error_message'	=> 'There has been an error with this order. Call us to complete this order.'
);