<?php   
$lang = array(
		'ct_pay_by_check_title' => 'Pay By Check',
 		'ct_pay_by_check_transaction_id'	=> 'Payment by check',
		'ct_pay_by_check_overview'	=> 'This payment method is intended for use when you will be collecting a check from the customer at a later date. 
				Orders placed with this gateway will be assigned your default processing status', 
		'ct_pay_by_check_processing_send_in_check'	=> 'To complete this transaction, please submit a check for full purchase price',
		'ct_pay_by_check_error_message'	=> 'There has been an error with this order. Please contact us to complete this order'
);