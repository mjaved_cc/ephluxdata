<?php   
$lang = array(  
	 //COUPON_ERROR_MESSAGES
	'coupon_valid_msg' =>  'Your code is valid and your cart total has been updated.',
	'coupon_invalid_msg'=>"The code you entered is invalid.",
	'coupon_inactive_msg'=>"The code you entered is not yet active.", //Inactive Coupon (used before coupon start date)
	'coupon_expired_msg'=>"The code you entered is expired.", //Expired Coupon (used after coupon end date)
	'coupon_global_limit_msg'=>"You are only allowed %d coupon code per order.", //Coupon Limit Reached
	'coupon_user_limit_msg'=>"You have already used this coupon code.", //If coupons have a limit per customer,
	'coupon_coupon_limit_msg'=>"The code you entered is no longer valid.", //If coupons have a limit per coupon,
	'coupon_default_error_msg'=>'The coupon codes channel has not been properly set up.',
	
	// VIEW: VALIDATION ERROR MESSAGES
	'validation_missing_fields' => 'The following required fields are missing',
	'validation_customer_name' => 'Customer Name',
	'validation_first_name' => 'First Name',
	'validation_last_name' => 'Last Name',
	'validation_address' => 'Address',
	'validation_address2' => 'Address (line 2)',
	'validation_city' => 'City',
	'validation_state' => 'State',
	'validation_zip' => 'Zip',
	'validation_country' => 'Country',
	'validation_phone' => 'Phone',
	'validation_email_address' => 'Email Address',
	'validation_company' => 'Company',
	'validation_shipping_first_name' => 'Shipping First Name',
	'validation_shipping_last_name' => 'Shipping Last Name',
	'validation_shipping_address'  => 'Shipping Address',
	'validation_shipping_address2'=> 'Shipping Address2',
	'validation_shipping_city' => 'Shipping City',
	'validation_shipping_state' => 'Shipping State',
	'validation_shipping_zip' => 'Shipping Zip',
	'validation_shipping_option' => 'Shipping Option',
	'validation_credit_card_number' => 'Credit Card Number',
	'validation_expiration_month' => 'Expiration Month',
	'validation_expiration_year' => 'Expiration Year',
	'validation_card_code' => 'Card Code (3 or 4 digit number on back)',
	'validation_card_modulus_10'	=> 'The credit card number appears to be invalid',
	
	//INVENTORY ERROR MESSAGES
	'item_not_in_stock' => '%s in your cart is out of stock, please remove it from your cart before checking out.',
	'item_quantity_greater_than_stock' => 'There are just %d remaining of item "%s" in your cart. Please remove "%s" or reduce its quantity before checking out.',
	'item_quantity_greater_than_stock_one' => 'There is just %d remaining of item "%s" in your cart. Please remove this item from your cart or reduce its quantity before checking out.',
	'item_quantity_greater_than_stock_zero' => 'Item "%s" is currently out of stuck. Please remove this item from your cart before checking out.',
	'item_not_in_stock_add_to_cart' => '%s is currently out of stock.',
	'item_quantity_greater_than_stock_add_to_cart' => 'There are just %d remaining of item "%s" in stock. Please reduce the quantity before adding to your cart.',
	'item_quantity_greater_than_stock_add_to_cart_one' => 'There is just %d remaining of item "%s" in stock. Please reduce the quantity before adding to your cart.',
	'item_quantity_greater_than_stock_add_to_cart_zero' => 'Item "%s" is currently out of stock.',
	
	//CHECKOUT ERRORS
	'empty_cart' => 'Your cart is empty.',

	//GENERAL PAYMENT GATEWAY ERRORS & CONTENT
	"gateway_function_does_not_exist" => "Payment gateway is not configured to process incoming payments from third parties.",
	"payment_action_missing" => "CartThrob is not correctly installed. 1. Go to the EE modules page and make sure that the CartThrob module says 'installed'. 2. Go to the EE extensions page, and make sure the CartThrob extension is listed as 'enabled'. 3. As a last resort you may need to go to the modules page and select 'remove' and then click to 'install' CartThrob. Resetting the CartThrob module in this way will not affect your templates or settings.",
	"unknown_error"	=> 'An unknown error occurred',
	'live'	=> 'Live',
	'test'	=> 'Test',
	'mode'	=> 'Mode',
	'sandbox'	=> 'Sandbox',
	
	//PAYMENT GATEWAY: DEV_TEMPLATE & CONTENT
	'dev_template_title' => 'Developer Payment Gateway Sample',
	'dev_template_affiliate' => 'insert affiliate link here',
	'dev_template_overview' => 'Overview content goes here ',
	'dev_template_settings_example' => 'Settings Example',
	'dev_template_error_1' => "CartThrob development payment gateway error: It's not you, it's me.", 
	'dev_template_error_2' => "CartThrob development payment gateway error: Let's see other people.", 
	'dev_template_error_3' => "CartThrob development payment gateway error: I just need some space.",
	'dev_template_error_4' => "CartThrob development payment gateway error: Let's just be friends.",
	
	// AUTHORIZE NET ERRORS & CONTENT
	'authorize_net_affiliate'	=> '<p><a href="http://reseller.authorize.net/application/?id=5553676"><img src="http://www.authorize.net/images/authorizenet_logo.gif" border="0" alt="AUTHORIZE.NET and the Authorize.Net logo are trademarks of CyberSource Corporation." width="150" height="42"></a></p><p><a href="http://reseller.authorize.net/application/?id=5553676">Click here to sign up now!</a></p>',
	'authorize_net_overview'	=> '<p>Authorize.Net manages the submission of payment transactions to the processing networks on behalf of its 284,000 merchant customers. Since 1996, Authorize.Net has been a leading provider of Internet Protocol (IP) based payment gateway services, enabling merchants to accept credit card and electronic check payments from Web sites, retail stores, mail order/telephone order (MOTO) centers or mobile devices. Authorize.net handles international sales, but the merchant bank account must be based in the United States.  </p> ',
	'authorize_net_settings_api_login' => 'API Login',
	'authorize_net_settings_trans_key' => 'Transaction Key',
	'authorize_net_settings_email_customer' => 'Email Customer?',
	'authorize_net_settings_test_mode' => 'Test Mode?',
	'authorize_net_settings_dev_mode' => 'Developer Mode?',
	'authorize_net_settings_dev_api_login' => 'Developer API Login',
	'authorize_net_settings_dev_trans_key' => 'Developer Transaction Key',
	
	'authorize_net_cant_connect' => "Can not connect using using fsockopen or cURL",
	'authorize_net_error_1' => "There Was A Problem Connecting To Authorize.net. Error ",
	'authorize_net_error_2' => "There Was A Problem Connecting To Authorize.net. Error 2",
	'authorize_net_no_response' => " no response",
	
	// OFFLINE PAYMENTS ERRORS & CONTENT
	'offline_title' => 'Offline Payments',
	'offline_overview' => '<p>This is intended to be used on transactions where you will be taking payments in person or over the phone. This will automatically complete an order. It does not contain any monetary processess.</p>',
	'offline_transaction_id' => 'OFFLINE PAYMENT',
	
	// EWAY ERRORS & CONTENT
	'eway_title' => 'eWay.au Payment Gateway',
	'eway_overview' => "<p>eWAY provides a leading payment gateway solution for processing payments in real time. eWAY’s high level of standards in technology, innovations and customer care has helped them become Australia’s award winning payment gateway.</p>",
	'eway_customer_id'	=> 'Customer ID',
	'eway_payment_method' => 'Payment Method',
	'eway_test_mode'	=> 'Test Mode?',
	'eway_cant_connect' => "Can't connect using using cURL",
	'eway_transaction_error' => "Transaction Error: ",
	'eway_invalid_response' => "Error: An invalid response was recieved from the payment gateway.",
	
	// LINKPOINT ERRORS & CONTENT
	'linkpoint_cant_connect' => "Can't connect using using fsockopen or cURL",
	'linkpoint_execute_curl' => "<r_error>Could not execute curl.</r_error>",
	'linkpoint_connect_problem' => "There Was A Problem Connecting To Linkpoint Payment Gateway. Please contact site administrator and tell them about this problem",
	'linkpoint_no_response' => " no response",
	
	//PAYPAL STANDARD ERRORS & CONTENT
	'paypal_title' 	=> 'Paypal Website Payments Standard',
	'paypal_affiliate'	=> '<p><A HREF="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A" target="_blank"><IMG  SRC="http://images.paypal.com/en_US/i/bnr/paypal_mrb_banner.gif" BORDER="0" ALT="Sign up for PayPal and start accepting credit card payments instantly."></A></p><p><a href="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A">Click here to sign up now!</a></p>', 
		'overview' => '<p>Paypal Website Payments Standard is a gateway used for basic integration with paypal. Payments are handled off site by Paypal. To control the look and feel of paypal\'s cart, set the page_style in your PayPal account profile. </p>',
	'paypal_settings_id' => "Paypal ID",
	'paypal_sandbox_settings_id' => "Paypal Sandbox ID",
		
	'paypal_incorrect_id' => 'Payment for an incorrect paypal_id was sent. ',
	'paypal_status_incomplete' => 'Payment Status shows payment incomplete. ',
	'paypal_not_verified' => 'Could not verify that the payment notification coming from Paypal was received correctly. ',
	'paypal_unknown_response' => 'An unknown response was returned from Paypal.',

	
	//PAYPAL PRO ERRORS & CONTENT
	'paypal_pro_title' => 'Paypal Website Payments Pro',
	'paypal_pro_sandbox_mode' => 'Sandbox Mode?',
	'paypal_pro_signature' => 'Signature',
	'paypal_pro_api_password' => 'API Password',
	'paypal_pro_api_username' => 'API Username',
	'paypal_pro_overview' => '<p>Paypal Website Payments Pro is a fully integrated payment processing solution. It works in much the same manner as a typical merchant account and payment gateway. All transactions are handled directly on your site, the customer is never transferred away.</p><p>To get started with Website Payments Pro, you will need to upgrade your Paypal account. Once you have your account set up, you will need to <a href="https://www.x.com/docs/DOC-1316#id084E30I30RO">generate API credentials</a> and add them below.</p> ',
	'paypal_pro_affiliate' => '<p><A HREF="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A" target="_blank"><IMG  SRC="http://images.paypal.com/en_US/i/bnr/paypal_mrb_banner.gif" BORDER="0" ALT="Sign up for PayPal and start accepting credit card payments instantly."></A></p><p><a href="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A">Click here to sign up now!</a></p>', 
	'paypal_pro_cant_connect' => "Can't connect using using cURL",
	'paypal_pro_contact_admin' => "Could not connect to payment gateway. Please contact administrator",
	
	// QUANTUM ERRORS & CONTENT
	'quantum_email_customer' => 'Email Customer?',
	'quantum_restrict_key'=>'RestrictKey',
	'quantum_gateway_login'=>'Gateway Login',
	'quantum_overview'=>'<p>As a CDGcommerce merchant, you will be able to substantially reduce your business costs by virtue of their exclusive FREE Payment Gateway program. There are absolutely no gateway setup, monthly or per transaction fees.</p>
	<p>They also do not "pass through" gateway costs in the form of higher merchant account rates like some competitors have done. Their program is the industry\'s first TRUE 100% FREE Payment Gateway Program and is designed to save merchants thousands of dollars in unnecessary gateway fees.</p>',
	'quantum_title'=>'CDG Commerce Quantum',
	'quantum_cant_connect' => "Can't connect using using cURL",
	'quantum_problem_connecting' => "There Was A Problem Connecting To CDG Commerce Quantum Paymet Gateway.",
	
	
	// SAGE ERRORS & CONTENT
	'sage_title'=> 'Sage Payment Gateway',
	'sage_affiliate'=> '<p><a href="https://support.sagepay.com/apply/default.aspx?PartnerID=2A93C68A-1EB6-453E-BED0-2DB1429164B8"><img src="http://cartthrob.com/images/affiliate_logos/sage_pay_partner_logo.gif" border="0" alt="Sage Pay Logo" ></a></p><p><a href="https://support.sagepay.com/apply/default.aspx?PartnerID=2A93C68A-1EB6-453E-BED0-2DB1429164B8">Click here to sign up now!</a></p>', 
	'sage_overview'=> "<p>Sage Pay is the UK and Ireland's leading independent payment service provider and makes accepting payments online, simple, fast, secure and profitable.</p><p>To accept payments online, over the phone or via mail order you will need a merchant bank account. Setting one up can be difficult and time-consuming, which is why Sage Pay has made the process much easier and has teamed up one of our banking partners to provide merchant accounts at extremely competitive rates.</p><p>So whether you are new to accepting card payments or are an established business we can offer a simple pricing package with the added benefit of no set-up fees.</p>",
	'sage_vps'=> 'VPS Protocol',
	'sage_vendor_name'=> 'Vendor Name',
	'sage_mode'=> 'Mode',
	
	'sage_failed' => "Can't connect using using fsockopen or cURL",
	'sage_malformed' => "The data sent to the payment processor was missing fields or badly formatted. This error typically only occurs during development, integration and testing.",
	'sage_invalid' => "The transaction was not registered because although the data sent to the payment gateway was formatted correctly, some information supplied was invalid. E.g. an incorrect vendor name or currency code was sent.",
	'sage_notauthed' => "The transaction was not authorized by the acquiring bank. No funds could be taken from the card.",
	'sage_rejected' => "The payment gateway rejected the transaction because of rules set on the vendor's account.",
	'sage_3dauth' => "3D-Authentication is required for this transaction.",
	'sage_ppredirect' => "Paypal payments have not been properly configured for this account.",
	'sage_authenticated' => "Security checks were performed successfully and the card details secured at Sage Pay, but no money has been taken from the card.",
	'sage_registered' => "3D-Secure checks failed or were not performed, but the card details are still secured at Sage Pay. No money has been taken from the card.",
	'sage_error' => "An error occurred at the payment gateway which means the transaction could not be completed successfully. No money has been taken from the card.",
	'sage_default' => "An unknown error occurred which means the transaction could not be completed successfully. No money has been taken from the card.",
	'sage_3dsecure' => "An unknown error occurred which means the transaction could not be completed successfully. No money has been taken from the card.",
	'sage_contact_admin' => "Could not connect to payment gateway. Please contact administrator",
	'sage_aborted'		=> "You chose to Cancel your order on the payment pages.",
	
	'sage_form_title'	=> 'Sage Form Payment Gateway',
	'sage_form_send_email'	=> 'Send Email?',
	'sage_form_encryption_password' => 'Encryption Password',
	
	// TRANSACTION CENTRAL ERRORS & CONTENT
	'trans_central_title'=>'Transaction Central',
	'trans_central_overview'=>'<p>Transaction Central, offered by TransFirst is an inexpensive alternative to the Authorize.net payment gateway, which uses almost all of the same settings as Authorize.net, making it easy to adapt to any project. </p>',
	'trans_central_merchant_id'=>'Merchant ID',
	'trans_central_reg_key'=>'RegKey',
	'trans_central_test_mode'=>'Test Mode?',
	'trans_central_tax_type'=>'Tax Type',
	'trans_central_charge_url'=>'Charge URL',
	'trans_central_unexpected' => "An unexpected response was returned from the payment gateway processor.",
	'trans_central_no_response' => "No response was returned from the payment gateway processor.",

	'trans_central_ach_title'=>'Transaction Central ACH Payments',
	'trans_central_ach_overview'=>'<p>Transaction Central ACH gateway collects payment data for storage with TransFirst for recurrent or post-transaction offline billing. </p>',

	'trans_central_ach_title'=>'Transaction Central Recurring ACH Payments',
	'trans_central_recurring_id' => 'RecurringId',

);