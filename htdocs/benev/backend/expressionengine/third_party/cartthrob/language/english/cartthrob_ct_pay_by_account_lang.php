<?php   
$lang = array(
		'ct_pay_by_account_title' => 'Bill to Credit Account',
 		'ct_pay_by_account_transaction_id'	=> 'Apply cost to credit account',
		'ct_pay_by_account_overview'	=> 'This payment method is intended for use when you will be applying a charge to the customer\'s existing credit account. 
				Orders placed with this gateway will be assigned your default processing status', 
		'ct_pay_by_account_note'	=> 'The order total will be applied to your credit account'
);