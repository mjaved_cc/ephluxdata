<?php   
$lang = array(
		'ct_save_order_title' => 'Save Order',
 		'ct_save_order_transaction_id'	=> 'Saved Order',
		'ct_save_order_overview'	=> 'This payment method is intended for use when you simply want customers to be able to save an order without any concern for payment. 
				Orders placed with this gateway will not be billed, and will look like very similar to other completed orders, so be careful when using this.', 
		'ct_save_order_error_message'	=> 'There has been an error with this order.',
		'ct_save_order_processing_message'	=> 'Your order is being processed. Thank you. '
);