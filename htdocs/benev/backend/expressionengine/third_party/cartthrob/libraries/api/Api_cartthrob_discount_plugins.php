<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'cartthrob/libraries/api/Api_cartthrob_plugins'.EXT;

class Api_cartthrob_discount_plugins extends Api_cartthrob_plugins
{
	public function get_plugins()
	{
		$this->EE->load->helper('file');
		
		$plugins = array();
	
		include_once PATH_THIRD.'cartthrob/cartthrob/Cartthrob.php';
		require_once CARTTHROB_CORE_PATH.'Cartthrob_child.php';
		require_once CARTTHROB_CORE_PATH.'Cartthrob_discount.php';
	
		$paths[] = CARTTHROB_DISCOUNT_PLUGIN_PATH;
		$paths[] = PATH_THIRD.'cartthrob/third_party/discount_plugins/';
		
		foreach ($paths as $path)
		{
			if ( ! is_dir($path))
			{
				continue;
			}
			
			foreach (get_filenames($path, TRUE) as $file)
			{
				$class = basename($file, EXT);
				
				if (strpos($class, 'Cartthrob_') !== 0)
				{
					continue;
				}
				
				$plugin = $this->EE->cartthrob->create_child($this->EE->cartthrob, $this->EE->cartthrob->get_class($class));
				
				$plugins[$class] = get_object_vars($plugin);
			}
		}
		
		return $plugins;
	}
	
	public function set_plugin_settings($plugin_settings)
	{
		if ($this->plugin)
		{
			$this->plugin->plugin_settings = $plugin_settings;
		}
		
		return $this;
	}
	
	public function global_settings($key = FALSE)
	{
		if ($key === FALSE)
		{
			return Cartthrob_discount::$global_settings;
		}
		
		return (isset(Cartthrob_discount::$global_settings[$key])) ? Cartthrob_discount::$global_settings[$key] : FALSE;
	}
}