<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 * @property Template $TMPL
 */
class Cartthrob_emails
{
	public $cartthrob, $store, $cart;
	
	public function __construct()
	{
		$this->EE =& get_instance();
		
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->cartthrob_loader->setup($this);
		
		if ( ! isset($this->EE->TMPL))
		{
			$this->EE->load->library('template', NULL, 'TMPL');
		}
	}
	
	/**
	 * Utility function, sends an email using the EE Core email class.
	 *
	 * @access public
	 * @param string $from
	 * @param string $from_name
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 * @return void
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function send_email($from, $from_name, $to, $subject, $message, $plaintext = FALSE)
	{
		$this->EE->load->library('email');
		
		$mailtype = ($plaintext) ? 'text' : 'html';

		//$this->EE->email->EE_initialize();
		
		$this->EE->email->validate = TRUE;
		
		if ( ! $from)
		{
			$from = $this->EE->config->item('webmaster_email');
		}
		
		if ( ! $from_name)
		{
			$from_name = $this->EE->config->item('webmaster_name');
		}
		
		$this->EE->email->set_mailtype($mailtype)
				->from(addslashes($from), addslashes($from_name))
				->to(addslashes($to))
				->subject(addslashes($subject))
				->message($message);
		
		$this->EE->email->send();
		
		//echo $this->EE->email->print_debugger();
	}
	
	public function parse($template, $variables = array(), $constants = array(), $run_template_engine = FALSE)
	{
		foreach ($constants as $key => $value)
		{
			$template = str_replace($key, $value, $template);
		}
		
		if ($variables)
		{
			$template = $this->EE->TMPL->parse_variables($template, array($variables));
		}
		
		if ($run_template_engine)
		{
			$this->EE->TMPL->parse($template);
			
			//restore package path
			$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
			
			$template = $this->EE->TMPL->final_template;
		}
		
		return $template;
	}

	/**
	 * Send the member order confirmation email
	 *
	 * @access private
	 * @param array $order_data
	 * @return void
	 */
	public function send_confirmation_email($to, $order_data)
	{
		$order_data['order_id'] = $order_data['entry_id'];

		unset($order_data['entry_id']);

		$subject = $this->parse($this->store->config('email_order_confirmation_subject'), $order_data, array('ORDER_ID' => $order_data['order_id']));
		
		$message = $this->parse($this->store->config('email_order_confirmation'), $order_data, array('ORDER_ID' => $order_data['order_id']), TRUE);
		
		$this->send_email(
			$this->store->config('email_order_confirmation_from'),
			$this->store->config('email_order_confirmation_from_name'),
			$to,
			$subject,
			$message,
			$this->store->config('email_order_confirmation_plaintext')
		);
	}
	
	
	public function send_customer_declined_email($to, $order_data){}
	public function send_admin_declined_email($order_data){}

	public function send_customer_processing_email($to, $order_data){}
	public function send_admin_processing_email($order_data){}

	public function send_customer_failed_email($to, $order_data){}
	public function send_admin_failed_email($order_data){}
	
	public function send_low_inventory_email($entry_id, $stock_level)
	{
		$variable_array['entry_id'] = $entry_id; 
 
		$subject = $this->parse($this->store->config('email_inventory_notification_subject'), $variable_array, array('ENTRY_ID' => $entry_id, 'STOCK_LEVEL' => $stock_level));
		
		$message = $this->parse($this->store->config('email_inventory_notification'), $variable_array, array('ENTRY_ID' =>  $entry_id, 'STOCK_LEVEL' => $stock_level), TRUE);
		
		$from = $this->parse($this->store->config('email_inventory_notification_from'), $variable_array);
		
		$from_name = $this->parse($this->store->config('email_inventory_notification_from_name'), $variable_array);
		
		$this->send_email(
			$from,
			$from_name,
			$this->store->config('low_stock_email'),
			$subject,
			$message,
			$this->store->config('email_inventory_notification_plaintext')
		);
		
	}
	
	/**
	 * Send the admin order notification email
	 *
	 * @access private
	 * @param array $order_data
	 * @return void
	 */
	public function send_admin_notification_email($order_data)
	{
		$order_data['order_id'] = $order_data['entry_id'];

		unset($order_data['entry_id']);
		
		$subject = $this->parse($this->store->config('email_admin_notification_subject'), $order_data, array('ORDER_ID' => $order_data['order_id']));
		
		$message = $this->parse($this->store->config('email_admin_notification'), $order_data, array('ORDER_ID' => $order_data['order_id']), TRUE);
		
		$from = $this->parse($this->store->config('email_admin_notification_from'), $order_data);
		
		$from_name = $this->parse($this->store->config('email_admin_notification_from_name'), $order_data);
		
		$this->send_email(
			$from,
			$from_name,
			$this->store->config('admin_email'),
			$subject,
			$message,
			$this->store->config('email_admin_notification_plaintext')
		);
	}
}
