<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paths
{
	public function __construct()
	{
		$this->EE =& get_instance();
	}
	
	public function parse_path($path)
	{
		if ( ! $path)
		{
			return '';
		}
		
		if (strpos($path, '{site_url}') !== FALSE)
		{
			$path = str_replace('{site_url}', $this->EE->functions->fetch_site_index(1), $path);
		}
		
		if (strpos($path, LD.'path=') !== FALSE)
		{
			$path = preg_replace_callback('/'.LD.'path=[\042\047]?(.*?)[\042\047]?'.RD.'/', array($this->EE->functions, 'create_url'), $path);
		}
		
		if (! preg_match("#^(http:\/\/|https:\/\/|www\.|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})#i", $path))
		{
			if (strpos($path, '/') !== 0)
			{
				$path = $this->EE->functions->create_url($path);
			}
		}
		
		return $path;
	}
	
	public function get_server_path($path)
	{
		if ( ! $path)
		{
			return FALSE;
		}
		// check for {variables}
		if (preg_match("/".LD."[^;\n]+?".RD."/", $path))
		{
			// parse the variables
			$path = $this->parse_path($path);
 		}
		
		// check to see if this is local
		if (strpos($path, $this->EE->config->item('base_url') )!== FALSE)
		{
			$path = str_replace($this->EE->config->item('base_url'), "", $path); 
			// replace the sys dir from the application path to get the server path. add the relative path. 
			$path = str_replace(SYSDIR."/expressionengine/", "", APPPATH).$path;
			return $path;
		}
		// one last check for files on this server that aren't in the current directory
		elseif (preg_match("#^(http:\/\/|https:\/\/|www\.|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})#i", $path))
		{
			$url_info = parse_url($path);
			// we parsed it, check it against the server information to see if it's local
 			if (isset($url_info['host']) && strpos($_SERVER['SERVER_NAME'], $url_info['host'] )!== FALSE)
			{
				
				$path = $_SERVER['DOCUMENT_ROOT'].$url_info['path'];
				return $path; 
			}
			else
			{
				return FALSE; 
			}
		}
		else
		{
 			if ( strpos($_SERVER['DOCUMENT_ROOT'], $path )!== FALSE)
			{
 				return $path; 
			}
			else
			{
				return FALSE; 
			}
		}
 	}
}