<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_payments
{
	private $gateway;
	private $paths = array();
	private $errors = array();
	private $total;
	
	public $cartthrob, $store, $cart;
	
	public function __construct($params = array())
	{
		$this->EE =& get_instance();
		$this->EE->load->library('cartthrob_loader');
		$this->EE->cartthrob_loader->setup($this);
		
		require_once PATH_THIRD.'cartthrob/payment_gateways/Cartthrob_payment_gateway'.EXT;
		
		$this->paths[] = PATH_THIRD.'cartthrob/third_party/payment_gateways/';
		$this->paths[] = PATH_THIRD.'cartthrob/payment_gateways/';
	}
	
	public function add_error($key, $value = FALSE)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->add_error($k, $v);
			}
		}
		else
		{
			if ($value === FALSE)
			{
				$this->errors[] = $key;
			}
			else
			{
				$this->errors[$key] = $value;
			}
		}
		
		return $this;
	}
	
	public function set_total($total)
	{
		$this->total = $total;
		return $this;
	}
	
	public function total()
	{
		return $this->total;
	}
	
	public function config($key = FALSE)
	{
		return $this->store->config($key);
	}
	
	public function order($order = FALSE)
	{
		return $this->cart->order($order);
	}
	
	public function payment_url()
	{
		return $this->EE->functions->fetch_site_index(0, 0).QUERY_MARKER.'ACT='.$this->EE->functions->fetch_action_id('Cartthrob', 'checkout_action');
	}
	
	public function paths()
	{
		return $this->paths;
	}
	
	public function process_payment($credit_card_number)
	{
		if ($this->total <= 0)
		{
			return array(
				'processing' => FALSE,
				'authorized' => TRUE,
				'declined' => FALSE,
				'failed' => FALSE,
				'error_message' => '',
				'transaction_id' => time()
			);
		}
		
		if ( ! $this->gateway)
		{
			return array(
				'processing' => FALSE,
				'authorized' => FALSE,
				'declined' => FALSE,
				'failed' => TRUE,
				'error_message' => $this->lang('invalid_payment_gateway'),
				'transaction_id' => ''
			);
		}
		
		return $this->gateway->process_payment($credit_card_number);
	}
	
	public function required_fields()
	{
		return ($this->gateway) ? $this->gateway->required_fields : array();
	}
	
	public function set_gateway($gateway)
	{
		if (strpos($gateway, 'Cartthrob_') !== 0)
		{
			$gateway = 'Cartthrob_'.$gateway;
		}
		
		if ( ! is_object($this->gateway) || get_class($this->gateway) != $gateway)
		{
			$this->gateway = NULL;
			
			foreach ($this->paths as $path)
			{
				if (file_exists($path.$gateway.EXT))
				{
					require_once $path.$gateway.EXT;
					
					$this->gateway = new $gateway;
					$this->gateway->set_core($this);
					$this->gateway->initialize();
					
					$this->EE->lang->loadfile(strtolower($gateway), 'cartthrob');
				}
			}
		}
		
		return $this;
	}
	
	public function gateway()
	{
		return $this->gateway;
	}
	
	/* utilities for the payment gateways */
	
	public function log($msg)
	{
		$this->cartthrob->log($msg);
	}
	
	public function create_url($path)
	{
		return $this->EE->functions->create_url($path);
	}
	
	public function fetch_template($template)
	{
		$template = explode('/', $template);
		
		$template_group = $template[0];
		
		$template_name = (isset($template[1])) ? $template[1] : 'index';
		
		return $this->EE->db->select('template_data')
				    ->join('template_groups', 'templates.group_id = template_groups.group_id')
				    ->where('group_name', $template_group)
				    ->where('template_name', $template_name)
				    ->get('templates')
				    ->row('template_data');
	}
	
	public function parse_template($template, $vars = array())
	{
		if ( ! isset($this->EE->TMPL))
		{
			$this->EE->load->library('template', NULL, 'TMPL');
		}
		
		if ($vars)
		{
			$template = $this->EE->TMPL->parse_variables($template, array($vars));
		}
		
		$this->EE->TMPL->parse($template);
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		return $this->EE->TMPL->final_template;
	}
	
	public function lang($key)
	{
		return $this->EE->lang->line($key);
	}
	
	public function year_2($year)
	{
		if (strlen($year > 2))
		{
			return substr($year, -2);
		}
		
		return str_pad($year, 2, '0', STR_PAD_LEFT);
	}
	
	public function year_4($year)
	{
		$length = strlen($year);
		
		switch($length)
		{
			case 3:
				return '2'.$year;
			case 2:
				return '20'.$year;
			case 1:
				return '200'.$year;
			case ($length > 4):
				return substr($year, -4);
		}
		
		return $year;
	}
	
	public function alpha2_country_code($country_code)
	{
		$this->EE->load->library('locales');
		
		return $this->EE->locales->alpha2_country_code($country_code);
	}
	
	public function alpha3_country_code($country_code)
	{
		$this->EE->load->library('locales');
		
		return $this->EE->locales->alpha3_country_code($country_code);
	}
	
	public function curl_transaction($url, $data = FALSE, $header = FALSE, $mode = 'POST', $suppress_errors = FALSE, $options = NULL)
	{
		if ( ! function_exists('curl_exec'))
		{
			return show_error(lang('curl_not_installed'));
		}
		
		// CURL Data to institution
		$curl = curl_init($url);
		
		if ($header)
		{
			curl_setopt($curl, CURLOPT_HEADER, 1);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array($header));
		}
		else
		{
			// set to 0 to eliminate header info from response
			curl_setopt($curl, CURLOPT_HEADER, 0);
		}
		
		// Returns response data instead of TRUE(1)
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		if ($data)
		{
			if ($mode === 'POST')
			{
				// use HTTP POST to send form data
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}
			else
			{
				// check for query  string
				if (strrpos($url, "?") === FALSE)
				{
					curl_setopt($curl, CURLOPT_URL, $url.'?'.$data); 
				}
				else
				{
					curl_setopt($curl, CURLOPT_URL, $url.$data);
				}
				
				curl_setopt($curl, CURLOPT_HTTPGET, 1);
			}
		}
		else
		{
			// if there's no data passed in, then it's a GET
			curl_setopt($curl, CURLOPT_HTTPGET, 1);
		}
		
		// curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0');
		// Turn off the server and peer verification (PayPal TrustManager Concept).
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		
		if (is_array($options))
		{
			foreach ($options as $key => $value)
			{
				curl_setopt($curl, $key, $value);
			}
		}
		// execute post and get results
		$response = curl_exec($curl);

		if ( ! $response)
		{
			$error = curl_error($curl).' ('.curl_errno($curl).')';
		}

		curl_close($curl);
		
		if ( ! $suppress_errors && ! empty($error))
		{
			return show_error($error);
		}
		
		return $response; 
	}
	
	/**
	 * curl_post
	 *
	 * @param array $data (string:url, array:params, array:options)
	 * @return string
	 * @author Rob Sanchez
	 * 
	 * $this->EE->curl->simple_post('http://example.com', array('foo'=>'bar'), array(CURLOPT_BUFFERSIZE => 10));  
	 * @see http://codeigniter.com/wiki/Curl_library/
	 * @see http://uk3.php.net/manual/en/function.curl-setopt.php
	 */
	public function curl_post($url, $params = array(), $options = array())
	{
		if (is_array($url))
		{
			$options = (isset($url[2])) ? $url[2] : array();
			$params = (isset($url[1])) ? $url[1] : array();
			$url = $url[0];
		}
		
		$this->EE->load->library('curl');
		
		return $this->EE->curl->simple_post($url, $params, $options);
	}
	/**
	 * curl_get
	 *
	 * @param array $data (string:url, array:options)
	 * @return string
	 * @author Rob Sanchez
	 * 
	 * $this->curl->simple_get('http://example.com', array(CURLOPT_PORT => 8080)); 
	 * @see http://codeigniter.com/wiki/Curl_library/
	 * @see http://uk3.php.net/manual/en/function.curl-setopt.php
	 */
	public function curl_get($url, $options = array())
	{
		if (is_array($url))
		{
			$options = (isset($url[1])) ? $url[1] : array();
			$url = $url[0];
		}
		
		$this->EE->load->library('curl');
		
		return $this->EE->curl->simple_get($url, $options);
	}
	
	public function curl_error_message()
	{
		$this->EE->load->library('curl');
		
		return $this->EE->curl->error_string;
	}
	
	public function curl_error_code()
	{
		$this->EE->load->library('curl');
		
		return $this->EE->curl->error_code;
	}
	
	public function input_post($key, $xss_clean = FALSE)
	{
		return $this->EE->input->post($key, $xss_clean);
	}
	
	public function input_get($key, $xss_clean = FALSE)
	{
		return $this->EE->input->get($key, $xss_clean);
	}
	
	public function input_get_post($key, $xss_clean = FALSE)
	{
		return $this->EE->input->get_post($key, $xss_clean);
	}
	
	public function input_cookie($key, $xss_clean = FALSE)
	{
		return $this->EE->input->cookie($key, $xss_clean);
	}
	
	public function xss_clean($data)
	{
		return $this->EE->security->xss_clean($data);
	}
	
	public function split_url_string($data,  $split_character = '&')
	{
		$this->EE->load->helper('data_formatting');
		return split_url_string($data, $split_character);
	}
	
	public function convert_response_xml($xml)
	{
		$this->EE->load->helper('data_formatting');
		return convert_response_xml($xml);
	}
	
	public function get_formatted_phone($phone)
	{
		$this->EE->load->helper('data_formatting');
		return get_formatted_phone($phone);
	}
	
	public function data_array_to_string($data)
	{
		if (function_exists('http_build_query'))
		{
			return http_build_query($data, '', '&');
		}

		$string = '';
		
		while (list($key, $val) = each($data)) 
		{
			$string .= $key."=".urlencode(stripslashes(str_replace("\n", "\r\n", $val))).'&';
		}
		
		if ($string)
		{
			$string = substr($data, 0, -1);
		}
		
		return $string;
	}
	
	public function clear_cart()
	{
		$this->cart->clear();
	}
	
	public function update_order($data)
	{
		$this->cart->update_order($data);
	}
	public function update_order_by_id($entry_id, $order_data)
	{
		$this->EE->load->model("order_model");
		return $this->EE->order_model->update_order($entry_id, $order_data); 
		
	}
	public function get_language_abbrev($language)
	{
		$this->EE->load->library('languages');
		
		return $this->EE->languages->get_language_abbrev($language);
	}
	public function relaunch_session_full($session_id)
	{
		if ($session_id != @session_id())
		{
			session_destroy(); 
			session_id($session_id);
			session_start();
		}
	}
	public function relaunch_session($session_id)
	{
		$params = $this->EE->config->item('session_config');
		
		$params['session_id'] = $session_id;
		if ( !$this->store->config('encrypted_sessions'))
		{
			$params['encrypted_sessions'] = FALSE; 
			if ($session_id != @session_id())
			{
				@session_destroy(); 
				session_id($session_id);
				session_start();
			}
 		}		
		$this->EE->load->library('cartthrob_session_relaxed', $params, 'cartthrob_session');

		$this->EE->cartthrob->cart = Cartthrob_core::create_child($this->EE->cartthrob, 'cart', $this->EE->cartthrob->get_cart());
		
		$this->EE->cartthrob_loader->setup_all('cart');
	}
	
	public function get_notify_url($gateway, $method = FALSE)
	{
		$this->EE->load->library('encrypt');
		
		if (substr($gateway, 0, 10) == 'Cartthrob_')
		{
			$gateway = substr($gateway, 10);
		}

		$notify_url = $this->EE->functions->fetch_site_index(0, 0).QUERY_MARKER.'ACT='.$this->EE->functions->insert_action_ids($this->EE->functions->fetch_action_id('Cartthrob', 'payment_return_action')).'&G='.urlencode($this->EE->encrypt->encode($gateway));

		if ($method)
		{
			$notify_url .= "&M=".urlencode($this->EE->encrypt->encode($method));
		}
		
		return $notify_url; 
	}
	
	/**
	 * gateway_exit_offsite
	 *
	 * sends a customer offsite to finish a payment transaction
	 * 
	 * @param array $post_array 
	 * @param string $url 
	 * @return void
	 * @author Chris Newton 
	 * @since 1.0
	 * @access public 
	 */
	function gateway_exit_offsite($post_array=NULL, $url=FALSE)
	{
		if ($this->store->config('save_purchased_items') && $this->cart->order('items'))
		{
			$this->EE->load->model('purchased_items_model');
			
			$purchased_items = array();
			
			foreach ($this->cart->order('items') as $item)
			{
				$purchased_items[] = $this->EE->purchased_items_model->create_purchased_item($item, $this->order('order_id'), $this->store->config('purchased_items_default_status'));
			}
			
			$this->cart->update_order(array('purchased_items' => $purchased_items));
			
			$this->cart->save();
		}
		if ( !$this->store->config('encrypted_sessions'))
		{
 			$this->EE->load->library('cartthrob_session_relaxed', array('session_id'=> @session_id()), 'cartthrob_session');
			$this->EE->cartthrob_session->set_userdata($_SESSION['cartthrob'] ); 
		}
		
		
		if(FALSE === $url)
		{
			return;
		}

		if ($post_array)
		{
			$url .= '?'.$this->data_array_to_string($post_array);	
		}

		$this->EE->functions->redirect($url);
	}

	/**
	 * gateway_order_update
	 *
	 * @param array $auth_array 
	 * @param string $order_id 
	 * @return void
	 * @author Chris Newton
	 * @since 1.0.0
	 */
	public function gateway_order_update($auth, $order_id, $return_url = NULL, $send_email=NULL)
	{
		$auth = array_merge(
			array(
				'processing' => FALSE,
				'authorized' => FALSE,
				'declined' => FALSE,
				'failed' => TRUE,
				'error_message' => '',
				'transaction_id' => '', 
			),
			$auth
		);
		
		$this->cart->update_order(array('auth' => $auth));
		$customer_info = $this->cart->customer_info();
		
		/////// AUTHORIZED
		if ($auth['authorized'])
		{
			if ($this->store->config('save_orders'))
			{
				$this->EE->load->model('order_model');
				
				$this->EE->order_model->update_order($order_id, array(
					'status' => ($this->store->config('orders_default_status')) ? $this->store->config('orders_default_status') : 'open',
					'transaction_id' => element('transaction_id', $auth),
				));
			}
		
			if ($this->store->config('save_purchased_items') && $this->order('purchased_items'))
			{
				$this->EE->load->model('purchased_items_model');
				
				foreach ($this->order('purchased_items') as $entry_id)
				{
					$this->EE->purchased_items_model->update_purchased_item($entry_id, array(
						'status' => $this->store->config('purchased_items_default_status')
					));
				}
			}

			// PROCESS 
			$this->cartthrob->process_discounts()
					->process_inventory();

			$this->cart->clear()
				   ->clear_coupon_codes()
				   ->save();
			
			if ($this->EE->extensions->active_hook('cartthrob_on_authorize') === TRUE)
			{
				$edata = $this->EE->extensions->call('cartthrob_on_authorize');
				if ($this->EE->extensions->end_script === TRUE) return;
			}
			// SEND EMAIL
			$this->EE->load->library('cartthrob_emails');

			if ($this->store->config('send_confirmation_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_confirmation_email($customer_info['email_address'], $this->cart->order());
			}

			if ($this->store->config('send_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_admin_notification_email($this->cart->order());
			}
 		}
		///////// DECLINED
		elseif ($auth['declined'])
		{
			if ($this->store->config('save_orders'))
			{
				$this->EE->load->model('order_model');
				
				$this->EE->order_model->update_order($order_id, array(
					'error_message' => 'DECLINED: '.element('error_message', $auth),
					'status' => ($this->store->config('orders_declined_status')) ? $this->store->config('orders_declined_status') : 'closed',
				));
			}
		
			if ($this->store->config('save_purchased_items') && $this->order('purchased_items'))
			{
				$this->EE->load->model('purchased_items_model');
				
				foreach ($this->order('purchased_items') as $entry_id)
				{
					$this->EE->purchased_items_model->update_purchased_item($entry_id, array(
						'status' => ($this->store->config('purchased_items_declined_status')) ? $this->store->config('purchased_items_declined_status') : 'closed'
					));
				}
			}
			
			
			// SEND EMAIL
			/*$this->EE->load->library('cartthrob_emails');

			if ($this->store->config('send_customer_declined_email') && $send_email !==FALSE )
			{
				$this->EE->cartthrob_emails->send_customer_declined_email($customer_info['email_address'], $this->cart->order());
			}

			if ($this->store->config('send_admin_declined_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_admin_declined_email($this->cart->order());
			}*/

			
			if ($this->EE->extensions->active_hook('cartthrob_on_decline') === TRUE)
			{
				$this->EE->extensions->call('cartthrob_on_decline');
				if ($this->EE->extensions->end_script === TRUE) return;
			}
 
			
		}
		////////// PROCESSING
		elseif ($auth['processing'])
		{
			if ($this->store->config('save_orders'))
			{
				$this->EE->load->model('order_model');
				
				$this->EE->order_model->update_order($order_id, array(
					'status' => ($this->store->config('orders_processing_status')) ? $this->store->config('orders_processing_status') : 'closed',
					'transaction_id' => element('transaction_id', $auth),
				));
			}
		
			if ($this->store->config('save_purchased_items') && $this->order('purchased_items'))
			{
				$this->EE->load->model('purchased_items_model');
				
				foreach ($this->order('purchased_items') as $entry_id)
				{
					$this->EE->purchased_items_model->update_purchased_item($entry_id, array(
						'status' => ($this->store->config('purchased_items_processing_status')) ? $this->store->config('purchased_items_processing_status') : 'closed'
					));
				}
			}

			// SEND EMAIL
			/*$this->EE->load->library('cartthrob_emails');

			if ($this->store->config('send_customer_processing_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_customer_processing_email($customer_info['email_address'], $this->cart->order());
			}

			if ($this->store->config('send_admin_processing_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_admin_processing_email($this->cart->order());
			}*/
			
			if ($this->EE->extensions->active_hook('cartthrob_on_processing') === TRUE)
			{
				$this->EE->extensions->call('cartthrob_on_processing');
				if ($this->EE->extensions->end_script === TRUE) return;
			}
 
		}
		////////// FAILED
		else
		{
			if (empty($auth['error_message']))
			{
				$auth['error_message'] = 'Unknown Failure.';
			}
			
			if ($this->store->config('save_orders'))
			{
				$this->EE->load->model('order_model');
				
				$this->EE->order_model->update_order($order_id, array(
					'error_message' => 'FAILED: '.element('error_message', $auth),
					'status' => ($this->store->config('orders_failed_status')) ? $this->store->config('orders_failed_status') : 'closed',
				));
			}
		
			if ($this->store->config('save_purchased_items') && $this->order('purchased_items'))
			{
				$this->EE->load->model('purchased_items_model');
				
				foreach ($this->order('purchased_items') as $entry_id)
				{
					$this->EE->purchased_items_model->update_purchased_item($entry_id, array(
						'status' => ($this->store->config('purchased_items_failed_status')) ? $this->store->config('purchased_items_failed_status') : 'closed'
					));
				}
			}
			// SEND EMAIL
			/*$this->EE->load->library('cartthrob_emails');

			if ($this->store->config('send_customer_failed_email') && $send_email !==FALSE)
			{
				$this->EE->cartthrob_emails->send_customer_failed_email($customer_info['email_address'], $this->cart->order());
			}

			if ($this->store->config('send_admin_failed_email') && $send_email !==FALSE )
			{
				$this->EE->cartthrob_emails->send_admin_failed_email($this->cart->order());
			}*/
			
			if ($this->EE->extensions->active_hook('cartthrob_on_fail') === TRUE)
			{
				$this->EE->extensions->call('cartthrob_on_fail');
				if ($this->EE->extensions->end_script === TRUE) return;
			}

		}
		
		$this->cart->save();
		// REDIRECT
		if ($return_url)
		{
			$return_url = $this->EE->functions->create_url($return_url);
			
			$this->EE->functions->redirect($return_url);
		}
		else
		{
			return TRUE; 
		}
	}
	
	/**
	 * gateway_cleanup_after_redirect
	 *
	 * processes inventory and coupons, updates session order data and sends emails. 
	 * @param string $auth_array 
	 * @param string $return_url 
	 * @return void
	 * @author Chris Newton
	 */
	public function return_processing($auth, $return_url = NULL)
	{
		$auth = array_merge(
			array(
				'processing' => FALSE,
				'authorized' => FALSE,
				'declined' => FALSE,
				'failed' => TRUE,
				'error_message' => '',
				'transaction_id' => '', 
			),
			$auth
		);

		$this->cart->update_order(array('auth' => $auth));

		if ($this->EE->extensions->active_hook('cartthrob_on_authorize') === TRUE)
		{
			$this->EE->extensions->call('cartthrob_on_authorize');
			if ($this->EE->extensions->end_script === TRUE) return;
		}

		// send emails
		
		$this->EE->load->library('cartthrob_emails');
		
		if ($this->store->config('send_confirmation_email'))
		{
			$this->EE->cartthrob_emails->send_confirmation_email( $this->cart->customer_info('email_address'), $this->cart->order());
		}

		if ($this->store->config('send_email'))
		{
			$this->EE->cartthrob_emails->send_admin_notification_email($this->cart->order());
		}

		$this->cartthrob->process_discounts()
				->process_inventory();

		$this->cart->clear()
			   ->clear_coupon_codes()
			   ->save();
		
		if ($return_url)
		{
			$return_url = $this->EE->functions->create_url($return_url);
			
			$this->EE->functions->redirect($return_url);
		}
		
		exit;
	}
	
	public function get_default_order_status()
	{
		return ($this->store->config('orders_default_status')) ? $this->store->config('orders_default_status') : 'open';
	}
	
	public function get_default_purchased_items_status()
	{
		return ($this->store->config('purchased_items_default_status')) ? $this->store->config('purchased_items_default_status') : 'open';
	}
	// END
	
	/**
	 * update_customer_info
	 *
	 * send an array with keys that correspond to CT standard fields, and the update data as the values, and this will update the database 
	 * @param array $data_array 
	 * @return void
	 * @author Chris Newton
	 */
	/*
	public function update_customer_info($data_array, $order_id)
	{
		global $DB; 
		
		$exp_weblog_data = array(); 
		
		foreach ($data_array as $key=> $item)
		{
			if ($item !="" && !empty($this->settings[$key]))
			{
				$exp_weblog_data[$this->_get_field_id($this->settings[$key], 'field_id')] = $item;
			}
		}
		
		if (count($exp_weblog_data))
		{
			$DB->query($DB->update_string('exp_weblog_data', $exp_weblog_data, array('entry_id' => $order_id)));
		}
		
		return NULL; 
		
	}
	*/
	
	/**
	 * xml_to_array
	 *
	 * @param string $xml 
	 * @param string $build_type 
	 * @return array
	 * @author Chris Newton
	 * @since 1.0
	 */
	public function xml_to_array($xml, $build_type = 'basic')
	{
		$this->EE->load->helper('data_formatting');
		return xml_to_array($xml, $build_type);
	}
	
}