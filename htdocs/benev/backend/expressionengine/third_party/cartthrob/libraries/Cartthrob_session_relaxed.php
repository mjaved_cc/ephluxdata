<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cartthrob_session_relaxed
{
	private $now; 
	private $_SESSION = array();//the full session data
	public $userdata = array();//the cartthrob key of the full session data
	private $session_id = NULL;
	private $sess_encrypt_userdata = TRUE;
	private $sess_table_name = 'cartthrob_sessions';
	private $time_reference = NULL;
	
	public function __construct($params = array())
	{
		if (isset($params['session_id']))
		{
			$this->session_id = $params['session_id'];
 		}
		if ( ! $this->session_id)
		{
			return;
		}
		$this->now = $this->time();
		
		$this->EE =& get_instance();

		$query = $this->EE->db->select('user_data')
				      ->where('session_id', $this->session_id)
				      ->get('cartthrob_sessions');

		if ($query->num_rows() === 0)
		{
			return;
		}

		if ($this->sess_encrypt_userdata)
		{
			$this->EE->load->library('encrypt');

			$this->_SESSION = $this->unserialize($this->EE->encrypt->decode($query->row('user_data')));
		}
		else
		{
			$this->_SESSION = $this->unserialize($query->row('user_data'));
		}
		
		if ( ! isset($this->_SESSION['cartthrob']))
		{
			$this->_SESSION['cartthrob'] = array();
		}
		if (isset($params['encrypted_sessions']) && !$params['encrypted_sessions'])
		{
			/*
			if ( $this->session_id!= @session_id())
			{
				session_destroy(); 
				session_id( $this->session_id);
				session_start();
			}
			*/
			$_SESSION['cartthrob'] = $this->_SESSION['cartthrob'];
		}

		$this->userdata =& $this->_SESSION['cartthrob'];
	}
	
	public function custom_userdata()
	{
		return $this->userdata;
	}
	
	public function set_userdata($data)
	{
		if ( ! $this->session_id)
		{
			return;
		}
		
		$this->userdata = $data;
		
		$user_data = '';
		// if nothing has been saved to the database yet, _SESSION won't be set. so we have to get the default session data
		if (empty($this->_SESSION))
		{
			$this->_SESSION=$_SESSION; 
		}
		
		foreach ($this->_SESSION as $key => $value)
		{
			$user_data .= $key.'|'.serialize($value);
		}
		
		$this->sess_write($this->session_id, $user_data); 
	}
	public function sess_write($session_id, $user_data)
	{
		$this->EE->load->library('encrypt');
		
		if ($this->EE->db->where('session_id', $session_id)->count_all_results($this->sess_table_name) == 0)
		{
			$this->EE->db->insert(
				$this->sess_table_name,
				array(
					'session_id' => $session_id,
					'sess_key' => $this->sess_key(),
					'sess_expiration' => $this->now + ini_get('session.gc_maxlifetime'),
					'user_data' => $this->EE->encrypt->encode($user_data),
				)
			);
		}
		else
		{
			$this->EE->db->update(
				$this->sess_table_name,
				array(
					'sess_expiration' => $this->now + ini_get('session.gc_maxlifetime'),
					'user_data' => $this->EE->encrypt->encode($user_data),
				),
				array('session_id' => $session_id)
			);
		}
		
		return ($this->EE->db->affected_rows() > 0);
	}
	private function sess_key()
	{
		return sha1($this->EE->config->item('encryption_key').$this->EE->input->server('HTTP_ACCEPT_LANGUAGE').$this->EE->input->server('HTTP_ACCEPT_CHARSET').$this->EE->input->server('HTTP_ACCEPT_ENCODING'));  
	}
	/**
	 * Get the "now" time
	 *
	 * @access	private
	 * @return	string
	 */
	private function time()
	{
		if (strtolower($this->time_reference) == 'gmt')
		{
			$now = time();
			$time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));
		}
		else
		{
			$time = time();
		}

		return $time;
	}
	
	//from http://us.php.net/manual/en/function.session-decode.php#101687
	private function unserialize($data)
	{
		if (strlen($data) === 0)
		{
			return array();
		}
		
		// match all the session keys and offsets
		preg_match_all('/(^|;|\})([a-zA-Z0-9_]+)\|/i', $data, $matchesarray, PREG_OFFSET_CAPTURE);
	    
		$returnArray = array();
	    
		$lastOffset = NULL;
		
		$currentKey = '';
		
		foreach ($matchesarray[2] as $value)
		{
		    $offset = $value[1];
		    
		    if ( ! is_null($lastOffset))
		    {
			$valueText = substr($data, $lastOffset, $offset - $lastOffset);
			
			$returnArray[$currentKey] = unserialize($valueText);
		    }
		    
		    $currentKey = $value[0];
	    
		    $lastOffset = $offset + strlen($currentKey) + 1;
		}
	    
		$valueText = substr($data, $lastOffset);
		
		if ($currentKey === '')
		{
			$returnArray = unserialize($valueText);
		}
		else
		{
			$returnArray[$currentKey] = unserialize($valueText);
		}
		
		return $returnArray;
	}
}