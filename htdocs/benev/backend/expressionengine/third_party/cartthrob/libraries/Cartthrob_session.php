<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2010, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Session Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Sessions
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/sessions.html
 */

/**
 * CartThrob Session Class
 *
 * @package		CartThrob
 * @link		http://codeigniter.com/user_guide/libraries/sessions.html
 */

class Cartthrob_session
{
	private $now;
	private $sess_table_name = 'cartthrob_sessions';
	private $sess_use_database = TRUE;
	private $sess_expiration = 7200;
	private $sess_gc_probability = 5;
	private $time_reference = NULL;
	
	public $userdata;
	
	public function __construct($params = array())
	{
		$this->EE =& get_instance();
		
		//core should be the one and only array value
		$core = current($params);
		
		// Set the "now" time.  Can either be GMT or server time, based on the
		// config prefs.  We use this to set the "last activity" time
		$this->now = $this->time();
		
		if ($core->store->config('session_expire'))
		{
			$this->sess_expiration = $core->store->config('session_expire');
		}
		else if ($this->EE->config->item('sess_expiration'))
		{
			$this->sess_expiration =  $this->EE->config->item('sess_expiration');
		}
		
		if ( ! is_numeric($this->sess_expiration))
		{
			$this->sess_expiration = ini_get('sesssion.cookie_lifetime');
		}
		
		// 500 errors are thrown on some servers if ini_set is called. 
		// so we need more than @ to silence the errors. 
		if(is_callable('ini_set'))
		{
			@ini_set('session.gc_maxlifetime', $this->sess_expiration);
			
			if ($this->EE->config->item('sess_gc_probability'))
			{
				@ini_set('session.gc_probability', $this->EE->config->item('sess_gc_probability'));
			}

			if ($this->EE->config->item('sess_gc_divisor'))
			{
				@ini_set('session.gc_divisor', $this->EE->config->item('sess_gc_divisor'));
			}

			if ($this->EE->config->item('sess_cookie_name'))
			{
				@ini_set('session.name', $this->EE->config->item('sess_cookie_name'));
			}
		}
 		

		if ($core->store->config('encrypted_sessions'))
		{
			session_set_save_handler(
				array(&$this, 'sess_open'),
				array(&$this, 'sess_close'),
				array(&$this, 'sess_read'),
				array(&$this, 'sess_write'),
				array(&$this, 'sess_destroy'),
				array(&$this, 'sess_gc')
			);
			register_shutdown_function('session_write_close');
		}
		
		$path = ($this->EE->config->item('cookie_path')) ? $this->EE->config->item('cookie_path') : ini_get('session.cookie_path');
		$domain = ($this->EE->config->item('cookie_domain')) ? $this->EE->config->item('cookie_domain') : ini_get('sesssion.cookie_domain');
		
		session_set_cookie_params($this->sess_expiration, $path, $domain);
		
		@session_start();
		
		if ( ! isset($_SESSION['cartthrob']))
		{
			$_SESSION['cartthrob'] = array();
		}
		
		$this->userdata =& $_SESSION['cartthrob'];
	}
	
	public function custom_userdata()
	{
		return $_SESSION['cartthrob'];
	}
	
	public function set_userdata($user_data)
	{
		$_SESSION['cartthrob'] = $user_data;
	}
       
 
	public function sess_destroy($session_id = FALSE)
	{
		if ( ! $session_id)
		{
			$session_id = session_id(); 
		}
		
		session_regenerate_id();

		if ($this->sess_use_database === TRUE)
		{
			$this->EE->db->delete($this->sess_table_name, array('session_id' => $session_id)); 
		}
		
		session_unset();

		return ($this->EE->db->affected_rows() > 0);
	}
	
	public function sess_open()
	{
		return TRUE; 
	}
	
	public function sess_close()
	{
		return TRUE; 
	}
      
	private function sess_key()
	{
		return sha1($this->EE->config->item('encryption_key').$this->EE->input->server('HTTP_ACCEPT_LANGUAGE').$this->EE->input->server('HTTP_ACCEPT_CHARSET').$this->EE->input->server('HTTP_ACCEPT_ENCODING'));  
	}
 
	public function sess_read($session_id)
	{
		$query = $this->EE->db->select('user_data')
				      ->where('session_id', $session_id)
				      ->where('sess_key', $this->sess_key())
				      ->where('sess_expiration >', $this->now)
				      ->limit(1)
				      ->get($this->sess_table_name);
		
		if ($query->num_rows() === 0)
		{
			return '';
		}
		
		$this->EE->load->library('encrypt');
		
		return $this->EE->encrypt->decode($query->row('user_data'));
	}
 
	public function sess_write($session_id, $user_data)
	{
		$this->EE->load->library('encrypt');
		
		if ($this->EE->db->where('session_id', $session_id)->count_all_results($this->sess_table_name) == 0)
		{
			$this->EE->db->insert(
				$this->sess_table_name,
				array(
					'session_id' => $session_id,
					'sess_key' => $this->sess_key(),
					'sess_expiration' => $this->now + ini_get('session.gc_maxlifetime'),
					'user_data' => $this->EE->encrypt->encode($user_data),
				)
			);
		}
		else
		{
			$this->EE->db->update(
				$this->sess_table_name,
				array(
					'sess_expiration' => $this->now + ini_get('session.gc_maxlifetime'),
					'user_data' => $this->EE->encrypt->encode($user_data),
				),
				array('session_id' => $session_id)
			);
		}
		
		return ($this->EE->db->affected_rows() > 0);
	}
 

	public function sess_gc($max_life = NULL)
	{
		if ($this->sess_use_database)
		{
			$this->EE->db->delete($this->sess_table_name, array('sess_expiration <' => $this->now - $max_life));
		}
		
		return;
 	}
	/**
	 * Get the "now" time
	 *
	 * @access	private
	 * @return	string
	 */
	private function time()
	{
		if (strtolower($this->time_reference) == 'gmt')
		{
			$now = time();
			$time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));
		}
		else
		{
			$time = time();
		}

		return $time;
	}
}