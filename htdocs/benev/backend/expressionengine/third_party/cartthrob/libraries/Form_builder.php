<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (class_exists('Form_builder')) return;

class Form_builder
{
	protected $classname;
	protected $method;
	protected $action = '';
	protected $form_data = array(
		'error_handling',
		'return',
		'secure_return',
	);
	protected $hidden = array();
	protected $attributes = array('id', 'class', 'name', 'onsubmit');
	protected $encoded_bools = array(
		'show_errors' => array('ERR', TRUE),
	);
	protected $encoded_form_data = array(
		'required' => 'REQ',
	);
	protected $encoded_numbers = array();
	protected $params = array();
	protected $content = '';
	protected $array_form_data = array();
	protected $secure_action = FALSE;
	
	protected $errors = array();
	protected $success_callback;
	protected $error_callback;
	protected $show_errors = TRUE;
	protected $error_header = FALSE;
	protected $return;
	
	protected $required;
	protected $rules;
	
	protected $global_form_hashes = array();
	protected $global_errors;
	
	/**
	 * Constructor
	 * 
	 * @param array $params
	 * 
	 * @return void
	 */
	public function __construct($params = array())
	{
		$this->EE =& get_instance();
		$this->EE->load->library(array('encrypt', 'form_validation'));
		$this->reset($params);
	}
	
	public function set_errors(array $errors)
	{
		$this->errors = $errors;
		return $this;
	}
	
	public function clear_errors()
	{
		return $this->set_errors(array());
	}
	
	public function set_success_callback($callback)
	{
		$this->success_callback = $callback;
		return $this;
	}
	
	public function set_error_callback($callback)
	{
		$this->error_callback = $callback;
		return $this;
	}
	
	public function set_rules($rules)
	{
		//@TODO
		return $this;
	}
	
	public function set_required($required)
	{
		if (is_array($required))
		{
			$this->required = $required;
		}
		
		return $this;
	}
	
	public function set_return($return)
	{
		$this->return = $return;
		return $this;
	}
	
	public function set_show_errors($show_errors = TRUE)
	{
		$this->show_errors = $show_errors;
		return $this;
	}
	
	public function set_error_header($error_header)
	{
		$this->error_header = $error_header;
		return $this;
	}
	
	public function add_error($key, $value = NULL)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				if (is_numeric($k) && isset($this->errors[$k]))
				{
					$this->errors[] = $v;
				}
				else
				{
					$this->errors[$k] = $v;
				}
			}
		}
		else if ($value !== NULL)
		{
			$this->errors[$key] = $value;
		}
		else
		{
			$this->errors[] = $key;
		}
		
		return $this;
	}
	
	public function set_form_data($data)
	{
		if (is_array($data))
		{
			$this->form_data = array_merge($this->form_data, $data);
		}
		else
		{
			$this->form_data[] = $data;
		}
		
		return $this;
	}
	
	public function set_array_form_data($data)
	{
		if (is_array($data))
		{
			$this->array_form_data = $data;
		}
		else
		{
			$this->array_form_data[] = $data;
		}
		
		return $this;
	}
	
	public function set_encoded_form_data($key, $value = FALSE)
	{
		if (is_array($key))
		{
			$this->encoded_form_data = array_merge($this->encoded_form_data, $key);
		}
		else
		{
			$this->encoded_form_data[$key] = $value;
		}
		
		return $this;
	}
	
	public function set_encoded_bools($key, $value = FALSE)
	{
		if (is_array($key))
		{
			$this->encoded_bools = array_merge($this->encoded_bools, $key);
		}
		else
		{
			$this->encoded_bools[$key] = $value;
		}
		
		return $this;
	}
	
	public function set_encoded_numbers($key, $value = FALSE)
	{
		if (is_array($key))
		{
			$this->encoded_numbers = $key;
		}
		else
		{
			$this->encoded_numbers[$key] = $value;
		}
		
		return $this;
	}
	
	public function set_classname($classname)
	{
		if ($classname)
		{
			$this->classname = $classname;
		}
		
		return $this;
	}
	
	public function set_method($method)
	{
		if ($method)
		{
			$this->method = $method;
		}
		
		return $this;
	}
	
	public function set_action($action)
	{
		$this->action = $action;
		
		return $this;
	}
	
	public function set_attributes($key, $value = NULL)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->set_attributes($k, $v);
			}
		}
		else
		{
			$this->attributes[$key] = $value;
		}
		
		return $this;
	}
	
	public function set_hidden($key, $value = NULL)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->set_hidden($k, $v);
			}
		}
		else
		{
			$this->hidden[$key] = $value;
		}
		
		return $this;
	}
	
	public function initialize($params = array())
	{
		$this->reset();
		
		foreach (get_class_vars(__CLASS__) as $key => $value)
		{
			if (isset($params[$key]))
			{
				if (method_exists($this, "set_$key"))
				{
					$this->{"set_$key"}($params[$key]);
				}
				else
				{
					$this->{$key} = $params[$key];
				}
			}
			else
			{
				$this->{$key} = $value;
			}
		}
		
		return $this;
	}
	
	public function reset($reset = array())
	{
		if (empty($reset))
		{
			$reset = array_keys(get_class_vars(__CLASS__));
		}
		
		foreach (get_class_vars(__CLASS__) as $key => $value)
		{
			if (substr($key, 0, 7) !== 'global_' && in_array($key, $reset))
			{
				$this->{$key} = $value;
			}
		}
		
		return $this;
	}
	
	public function set_secure_action($secure_action = TRUE)
	{
		$this->secure_action = $secure_action;
		
		return $this;
	}
	
	public function set_params($params)
	{
		if ( ! $params)
		{
			return $this;
		}
		
		//set ALL encoded bools
		foreach ($this->encoded_bools as $key => $value)
		{
			$default = FALSE;
			
			if (is_array($value))
			{
				$default = (bool) @$value[1];
			}
			
			if ( ! isset($params[$key]))
			{
				$params[$key] = $default;
			}
		}
		
		foreach ($params as $param => $value)
		{
			switch($param)
			{
				case (in_array($param, $this->attributes)):
					$this->set_attributes($param, $value);
					break;
				case (in_array($param, $this->form_data)):
					$this->set_hidden($param, $value);
					break;
				case (array_key_exists($param, $this->encoded_form_data)):
					$this->set_hidden($this->encoded_form_data[$param], $this->EE->encrypt->encode($value));
					break;
				case (array_key_exists($param, $this->encoded_bools)):
					$key = (is_array($this->encoded_bools[$param])) ? $this->encoded_bools[$param][0] : $this->encoded_bools[$param];
					$this->set_hidden($key, $this->EE->encrypt->encode(create_bool_string(bool_string($value))));
					break;
				case (array_key_exists($param, $this->encoded_numbers)):
					$this->set_hidden($this->encoded_numbers[$param], $this->EE->encrypt->encode(sanitize_number($value)));
					break;
				case (substr($param, 0, 6) === 'rules:'):
					$name = substr($param, 6);
					if (preg_match('/^([^\[]+)(\[.*\])$/', $name, $match))
					{
						$this->set_hidden('rules['.$match[1].']'.$match[2], $this->EE->encrypt->encode($value));
					}
					else
					{
						$this->set_hidden('rules['.$name.']', $this->EE->encrypt->encode($value));
					}
					break;
				case (strpos($param, ':') !== FALSE):
					foreach ($this->array_form_data as $name)
					{
						if (preg_match("/^$name:(.+)$/", $param, $match))
						{
							$this->set_hidden($name.'['.$match[1].']', $value);
						}
					}
					break;
				case 'secure_action':
					$this->set_secure_action(bool_string($value));					
					break;
			}
		}
		
		return $this;
	}
	
	public function set_content($content)
	{
		$this->content = $content;
		
		return $this;
	}
	
	public function error($key)
	{
		return (isset($this->errors[$key])) ? $this->errors[$key] : FALSE;
	}
	
	public function errors()
	{
		//@TODO refactor using form_hash
		return $this->errors;
		//return (isset($this->errors[$this->form_hash()])) ? $this->errors[$this->form_hash()] : array();
	}
	
	protected function form_hash()
	{
		if ( ! isset($this->global_form_hashes[$this->EE->TMPL->tagproper]))
		{
			$this->global_form_hashes[$this->EE->TMPL->tagproper] = md5($this->EE->TMPL->tagproper);
		}
		
		return $this->global_form_hashes[$this->EE->TMPL->tagproper];
	}
	
	public function form()
	{
		if ( ! $this->action)
		{
			// .283 Changed from using config->site_url because it uses CI's base url, making
			// it impossible to change the site's url from the CP
			$this->action = $this->EE->functions->create_url($this->EE->uri->uri_string());
		}
		
		$this->EE->load->helper(array('form', 'data_formatting', 'https'));
		
		if (is_secure())
		{
			$this->secure_action = TRUE;
		}
		
		if ($this->secure_action)
		{
			$this->action = str_replace('http://', 'https://', $this->action);
		}
		
		$data = $this->attributes;
		
		$data['action'] = $this->action;
		
		if ( ! empty($this->classname) && ! empty($this->method))
		{
			$data['hidden_fields']['ACT'] = $this->EE->functions->fetch_action_id($this->classname, $this->method);
		}
		
		$data['hidden_fields']['RET'] = $this->EE->functions->fetch_current_uri();
		$data['hidden_fields']['URI'] = $this->EE->uri->uri_string();
		$data['hidden_fields']['form_hash'] = $this->form_hash();
		$data['hidden_fields'] = array_merge($data['hidden_fields'], $this->hidden);
		
		$return = $this->EE->functions->form_declaration($data).$this->content.form_close();
	
		$this->reset();
		
		return $return;
	}
	
	public function action_complete()
	{
		$this->EE->load->helper(array('data_formatting', 'https'));
		
		$this->EE->load->library('javascript');
		
		$this->EE->session->set_flashdata(array(
			'success' => ! (bool) $this->errors,
			'errors' => $this->errors,
		));
		
		if ($this->EE->input->post('ERR'))
		{
			$this->set_show_errors(bool_string($this->EE->encrypt->decode($this->EE->input->post('ERR')), TRUE));
		}
		
		if ($this->errors && $this->error_callback && is_callable($this->error_callback))
		{
			call_user_func($this->error_callback);
		}
		
		if ($this->show_errors && $this->errors && ! AJAX_REQUEST)
		{
			if ($this->EE->input->post('error_handling') === 'inline')
			{
				$method = (version_compare(APP_VER, '2.1.3', '>')) ? 'generate_page' : '_generate_page';
				
				$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
				
				return $this->EE->core->$method();
			}
			
			return $this->EE->output->show_user_error('general', $this->errors, $this->error_header);
		}
		
		if ( ! $this->EE->security->secure_forms_check($this->EE->input->post('XID')))
		{
			$this->EE->functions->redirect(stripslashes($this->EE->input->post('RET')));		
		}
		
		if (AJAX_REQUEST && $this->EE->config->item('secure_forms') === 'y')
		{
			$this->EE->session->set_flashdata('XID', $this->EE->functions->add_form_security_hash('{XID_HASH}'));
		}
		
		if ( ! $this->errors && $this->success_callback && is_callable($this->success_callback))
		{
			call_user_func($this->success_callback);
		}
		
		if ( ! $this->return)
		{
			$this->return = ($this->EE->input->post('return')) ? $this->EE->input->post('return', TRUE) : $this->EE->uri->uri_string();
		}
		
		$url = $this->EE->functions->create_url($this->return);
		
		if (is_secure() || bool_string($this->EE->input->post('secure_return')))
		{
			$url = str_replace('http://', 'https://', $url);
		}
		
		$this->EE->session->set_flashdata('return', $url);
		
		$this->EE->functions->redirect($url);
	}
	
	protected function process_rules($rules, $key = FALSE, $output = array())
	{
		if ( ! is_array($rules))
		{
			return FALSE;
		}
		
		foreach ($rules as $i => $rule)
		{
			$full_key = ($key === FALSE) ? $i : $key.'['.$i.']';
			
			if (is_array($rule))
			{
				$this->process_rules($rule, $full_key, $output);
			}
			else
			{
				$output[$full_key] = xss_clean($this->EE->encrypt->decode($rule));
			}
		}
		
		return $output;
	}

	public function validate()
	{
		//grab from POST if not already set
		if (is_null($this->required) && $this->EE->input->post('REQ'))
		{
			$this->required = explode('|', xss_clean($this->EE->encrypt->decode($this->EE->input->post('REQ'))));
		}
		
		if ( ! is_array($this->rules))//meaning, someone has already done this processing
		{
			$this->rules = $this->process_rules($this->EE->input->post('rules'));
		}
		
		if (is_array($this->required))
		{
			foreach ($this->required as $key)
			{
				if (preg_match('/^custom_data\[(.*)\]$/', $key, $match))
				{
					$label = sprintf($this->EE->lang->line('validation_custom_data'), $match[1]);
				}
				else
				{
					$label = $this->EE->lang->line('validation_'.$key);
				}
				
				if (isset($this->rules[$key]))
				{
					if (strpos($this->rules[$key], 'required') === FALSE)
					{
						$this->rules[$key] = 'required|'.$this->rules[$key];
					}
				}
				else
				{
					$this->rules[$key] = 'required';
				}
			}
		}
		
		if ( ! $this->rules)
		{
			return TRUE;
		}
		
		foreach ($this->rules as $key => $rules)
		{
			if (preg_match('/^custom_data\[(.*)\]$/', $key, $match))
			{
				$label = sprintf($this->EE->lang->line('validation_custom_data'), $match[1]);
			}
			else
			{
				$label = $this->EE->lang->line('validation_'.$key);
			}
			
			$this->EE->form_validation->set_rules($key, $label, $rules);
		}
		
		if ( ! $valid = $this->EE->form_validation->run())
		{
			$this->add_error($this->EE->form_validation->_error_array);
			//$this->set_error_header($this->EE->lang->line('validation_missing_fields'));
		}
		
		return $valid;
	}

	public function error_variables()
	{
		$variables = array();
		
		if (count($this->errors()) > 0)
		{
			foreach ($this->errors() as $key => $value)
			{
				$variables['error:'.$key] = $value;
				
				$variables['errors'][]['error'] = $value;
			}
			
			$variables['errors_exist'] = 1;
		}
		else
		{
			$variables['errors'] = 0;
			
			$variables['errors_exist'] = 0;
		}
		
		foreach ($this->EE->TMPL->var_single as $key)
		{
			if (strpos($key, 'error:') === 0 && ! isset($variables[$key]))
			{
				$variables[$key] = '';
			}
		}
		
		return $variables;
	}
}