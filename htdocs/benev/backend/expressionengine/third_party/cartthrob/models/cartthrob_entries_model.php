<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cartthrob_entries_model extends CI_Model
{
	protected $entries = array();
	protected $last_entries = array();
	
	public $errors = array();
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('array');
	}
	
	public function entry($entry_id)
	{
		$this->load_entries_by_entry_id(array($entry_id));
		
		return element($entry_id, $this->entries);
	}
	
	public function last_entries()
	{
		return $this->last_entries;
	}
	
	public function find_entries($data = array())
	{
		$this->load_entries($data);
		
		$entries = array();
		
		foreach ($this->last_entries as $entry_id)
		{
			if (isset($this->entries[$entry_id]))
			{
				$entries[$entry_id] = $this->entries[$entry_id];
			}
		}
		
		return $entries;
	}
	
	public function load_entries_by_entry_id($entry_ids)
	{
		foreach ($entry_ids as $i => $entry_id)
		{
			if (isset($this->entries[$entry_id]))
			{
				unset($entry_ids[$i]);
			}
		}
		
		if ($entry_ids)
		{
			return $this->load_entries(array('channel_titles.entry_id' => $entry_ids));
		}
		
		return $this;
	}
	
	public function load_entries($data = array())
	{
		$this->load->model('cartthrob_field_model');
		
		foreach ($data as $key => $value)
		{
			if (is_array($value))
			{
				$this->db->where_in($key, $value);
			}
			else
			{
				if (substr($value, 0, 1) === '%' || substr($value, -1, 1) === '%')
				{
					$this->db->like($key, $value);
				}
				else
				{
					$this->db->where($key, $value);
				}
			}
		}
		
		$this->db->select('channel_titles.*, channel_data.*, channels.*')
			->from('channel_titles')
			->join('channel_data', 'channel_titles.entry_id = channel_data.entry_id')
			->join('channels', 'channels.channel_id = channel_titles.channel_id');
		
		$this->last_entries = array();
		
		foreach ($this->db->get()->result_array() as $row)
		{
			foreach ($this->cartthrob_field_model->get_fields(array('group_id' => $row['field_group'])) as $field)
			{
				$row[$field['field_name']] = $row['field_id_'.$field['field_id']];
			}
			
			$this->last_entries[] = $row['entry_id'];
			
			$this->entries[$row['entry_id']] = $row;
		}
		
		return $this;
	}
	
	public function create_entry($data)
	{
		$this->load->model('cartthrob_field_model');
		
		if ( ! isset($data['channel_id']))
		{
			$this->errors[] = 'no_channel_id';
			return FALSE;
		}
		
		$fields = $this->cartthrob_field_model->get_fields_by_channel($data['channel_id']);
		
		$title_fields = $this->db->list_fields('channel_titles');
		
		$title_defaults = array(
			'author_id' => $this->session->userdata('member_id'),
			'site_id' => $this->config->item('site_id'),
			'ip_address' => $this->input->ip_address(),
			'entry_date' => $this->localize->now - 60, // subtracting a minute to keep this entry from accidentally being a "future" entry
			'edit_date' => date("YmdHis"),
			'versioning_enabled' => 'y',
			'status' => 'open',
			'forum_topic_id' => 0,
		);
		
		$channel_titles = array();
		
		foreach ($title_fields as $key)
		{
			if (isset($data[$key]))
			{
				$channel_titles[$key] = $data[$key];
			}
			else if (isset($title_defaults[$key]))
			{
				$channel_titles[$key] = $title_defaults[$key];
			}
		}
		
		$channel_titles['year'] = date('Y', $channel_titles['entry_date']);
		$channel_titles['month'] = date('m', $channel_titles['entry_date']);
		$channel_titles['day'] = date('d', $channel_titles['entry_date']);
		
		if (empty($data['author_id']))
		{
			$this->errors[] = 'no_author_id';
			return FALSE;
		}

		$this->db->insert('channel_titles', $channel_titles);
		
		unset($channel_titles, $title_fields, $title_defaults);
		
		$entry_id = $this->db->insert_id();

		$channel_data = array(
			'entry_id' => $entry_id,
			'channel_id' => $data['channel_id'],
			'site_id' => $this->config->item('site_id')
		);
		
		foreach ($fields as $field)
		{
			if (isset($data['field_id_'.$field['field_id']]))
			{
				$channel_data['field_id_'.$field['field_id']] = $data['field_id_'.$field['field_id']];
			}
			else
			{
				$channel_data['field_id_'.$field['field_id']] = (isset($data[$field['field_name']])) ? $data[$field['field_name']] : '';
			}
			
			if (isset($data['field_ft_'.$field['field_id']]))
			{
				$channel_data['field_ft_'.$field['field_id']] = $data['field_ft_'.$field['field_id']];
			}
			else
			{
				$channel_data['field_ft_'.$field['field_id']] = $field['field_fmt'];
			}
		}

		$this->db->insert('channel_data', $channel_data);
		
		unset($channel_data);
		
		$this->db->set('total_entries', 'total_entries + 1', FALSE)
			->where('member_id', $data['author_id'])
			->update('members');

		$this->stats->update_channel_stats($data['channel_id']);

		if ($this->config->item('new_posts_clear_caches') == 'y')
		{
			$this->functions->clear_caching('all');
		}
		else
		{
			$this->functions->clear_caching('sql');
		}

		return $entry_id;
	}
	
	public function update_entry($entry_id, $data)
	{
		$this->load->model('cartthrob_field_model');
		
		$channel_id = (isset($data['channel_id'])) ? $data['channel_id'] : $this->db->select('channel_id')->where('entry_id', $entry_id)->get('channel_titles')->row('channel_id');
		
		$fields = ($channel_id) ? $this->cartthrob_field_model->get_fields_by_channel($channel_id) : array();
		
		$title_fields = $this->db->list_fields('channel_titles');
		
		$channel_titles = array();
		
		foreach ($title_fields as $key)
		{
			if (isset($data[$key]))
			{
				$channel_titles[$key] = $data[$key];
			}
		}
		
		$channel_titles['edit_date'] = date("YmdHis");
		
		$this->db->update('channel_titles', $channel_titles, array('entry_id' => $entry_id));
		
		$channel_data = array();
		
		foreach ($fields as $field)
		{
			if (isset($data['field_id_'.$field['field_id']]))
			{
				$channel_data['field_id_'.$field['field_id']] = $data['field_id_'.$field['field_id']];
			}
			else if (isset($data[$field['field_name']]))
			{
				$channel_data['field_id_'.$field['field_id']] = $data[$field['field_name']];
			}
		}
		
		if ($channel_data)
		{
			$this->db->update('channel_data', $channel_data, array('entry_id' => $entry_id));
		}
		
		return $entry_id;
	}
	
	public function clear_cache($entry_ids)
	{
		if ( ! is_array($entry_ids))
		{
			$entry_ids = array($entry_ids);
		}
		
		foreach ($entry_ids as $entry_id)
		{
			unset($this->entries[$entry_id]);
		}
	}
	
	public function channel_entries($params = array(), $return_query = FALSE)
	{
		require_once PATH_MOD.'channel/mod.channel'.EXT;
		
		$channel = new Channel;
		
		if (isset($params['channel_id']) && ! isset($params['channel']))
		{
			if ( ! isset($this->session->cache['cartthrob']['channel_names'][$params['channel_id']]))
			{
				$this->load->model('channel_model');
				
				$query = $this->channel_model->get_channel_info($params['channel_id'], array('channel_name'));
				
				$params['channel'] = $this->session->cache['cartthrob']['channel_names'][$params['channel_id']] = $query->row('channel_name');
				
				unset($query);
			}
		}
		
		if (is_array($this->TMPL->tagparams))
		{
 			$this->TMPL->tagparams = array_merge( $this->TMPL->tagparams, $params);
		}
		else
		{
			$this->TMPL->tagparams =  $params; 
		}
		
		if ( ! $return_query)
		{
			$this->TMPL->tagdata = $this->TMPL->assign_relationship_data($this->TMPL->tagdata);
			
			if (count($this->TMPL->related_markers) > 0)
			{
				foreach ($this->TMPL->related_markers as $marker)
				{
					if ( ! isset($this->TMPL->var_single[$marker]))
					{
						$this->TMPL->var_single[$marker] = $marker;
					}
				}
			}
	
			if ($this->TMPL->related_id)
			{
				$this->TMPL->var_single[$this->TMPL->related_id] = $this->TMPL->related_id;
				
				$this->TMPL->related_id = '';
			}
			
			return $channel->entries();
		}
	
		$channel->uri = ($channel->query_string) ? $channel->query_string : 'index.php';
		
		$save_cache = ($this->config->item('enable_sql_caching') === 'y' && ! ($channel->sql = $channel->fetch_cache()));
		
		if ( ! $channel->sql)
		{
			$channel->build_sql_query();
		}
		
		if ($channel->sql)
		{
			if ($save_cache)
			{
				$channel->save_cache($channel->sql);
			}
			
			return $this->db->query($channel->sql);
		}
		
		return FALSE;
	}
	
	public function entry_vars($entry, $tagdata = FALSE, $var_single = FALSE, $var_pair = FALSE)
	{
		static $channels = array();
		
		if ($tagdata === FALSE)
		{
			$tagdata = $this->TMPL->tagdata;
		}
		
		if ($var_single === FALSE)
		{
			$var_single = $this->TMPL->var_single;
		}
		
		if ($var_pair === FALSE)
		{
			$var_pair = $this->TMPL->var_pair;
		}
		
		$this->load->library('api');
		
		$this->api->instantiate('channel_fields');
		
		$this->load->library('typography');
		
		$this->load->model('cartthrob_field_model');
		
		$row = array();
		
		if (($site_pages = $this->config->item('site_pages')) !== FALSE && isset($site_pages[$entry['site_id']]['uris'][$entry['entry_id']]))
		{
			$row['page_uri'] = $site_pages[$entry['site_id']]['uris'][$entry['entry_id']];
			$row['page_url'] = $this->functions->create_page_url($site_pages[$entry['site_id']]['url'], $site_pages[$entry['site_id']]['uris'][$entry['entry_id']]);
		}
		else
		{
			$row['page_uri'] = '';
			$row['page_url'] = '';
		}
		
		//set up all the fieldtypes in api_channel_fields
		if ( ! in_array($entry['channel_id'], $channels))
		{
			$channels[] = $entry['channel_id'];
			
			foreach ($this->cartthrob_field_model->get_fields_by_channel($entry['channel_id']) as $field)
			{
				$this->api_channel_fields->set_settings(
					$field['field_id'],
					array_merge(
						$field,
						$this->cartthrob_field_model->get_field_settings($field['field_id'])
					)
				);
			}
		}
		
		foreach ($var_pair as $var_full => $var_params)
		{
			$var_name = $var_full;
			
			if (($pos = strpos($var_full, ' ')) !== FALSE)
			{
				$var_name = substr($var_full, 0, $pos);
			}
			
			if ($field_id = $this->cartthrob_field_model->get_field_id($var_name))
			{
				if (preg_match_all('#'.LD.preg_quote($var_full).RD.'(.*)'.LD.'/'.preg_quote($var_name).RD.'#s', $tagdata, $matches))
				{
					foreach ($matches[1] as $i => $match)
					{
						$var_key = substr($matches[0][$i], 1, -1);
						
						if (isset($row[$var_key]))
						{
							continue;
						}
					
						if (empty($entry['field_id_'.$field_id]))
						{
							$row[$var_key] = '';
						}
						else if ($this->api_channel_fields->setup_handler($field_id))
						{
							$this->api_channel_fields->apply('_init', array(array('row' => $entry)));
							
							$var_data = $this->api_channel_fields->apply('pre_process', array($entry['field_id_'.$field_id]));
							
							$row[$var_key] = $this->api_channel_fields->apply('replace_tag', array($var_data, $var_params, $match));
						}
						else
						{
							$row[$var_key] = $entry['field_id_'.$field_id];
						}
					}
				}
			}
		}
		
		foreach ($var_single as $var_full)
		{
			$var_params = array();
			$var_name = $var_full;
			
			if (($pos = strpos($var_name, ' ') !== FALSE))
			{
				$var_params = $this->functions->assign_parameters(substr($var_full, $pos));
				$var_name = substr($var_full, 0, $pos);
			}
			
			$method = 'replace_tag';
			
			if (($pos = strpos($var_name, ':')) !== FALSE)
			{
				$method = 'replace_'.substr($var_name, $pos + 1);
				$var_name = substr($var_name, 0, $pos);
			}
			
			if ($field_id = $this->cartthrob_field_model->get_field_id($var_name))
			{
				if (empty($entry['field_id_'.$field_id]))
				{
					$row[$var_full] = '';
				}
				else if ($this->api_channel_fields->setup_handler($field_id))
				{
					$this->api_channel_fields->apply('_init', array(array('row' => $entry)));
					
					$var_data = $this->api_channel_fields->apply('pre_process', array($entry['field_id_'.$field_id]));
					
					$row[$var_full] = $this->api_channel_fields->apply($method, array($var_data, $var_params, FALSE));
				}
				else
				{
					$row[$var_full] = $entry['field_id_'.$field_id];
				}
			}
		}
		
		foreach ($entry as $key => $value)
		{
			if ( ! isset($row[$key]))
			{
				$row[$key] = $value;
			}
		}
		
		//@TODO fix this
		$row['categories'] = '';
		
		return $row;
	}
}
