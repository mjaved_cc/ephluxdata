<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchased_items_model extends CI_Model
{
	public $cartthrob, $store, $cart;
	
	public function __construct()
	{
		$this->load->library('cartthrob_loader');
		$this->cartthrob_loader->setup($this);
		$this->load->model('cartthrob_entries_model');
	}
	public function purchased_entry_ids()
	{
		$query = $this->db->select('field_id_'.$this->store->config('purchased_items_id_field')." AS entry_id")
				  	->distinct()
	    			->where('channel_id', $this->store->config('purchased_items_channel'))
				  	->get('channel_data');
		
		$entry_ids = array(); 
		foreach ($query->result() as $row)
		{
			$entry_ids[] = $row->entry_id; 
		}
		return $entry_ids; 
	}
	//returns an array
	//  entry_id => count
	public function also_purchased($entry_ids, $limit = FALSE)
	{
		$purchased = array();
		
		if ( ! $entry_ids || ! $this->store->config('purchased_items_channel') || ! $this->store->config('purchased_items_id_field'))
		{
			return $purchased;
		}
		
		if ( ! is_array($entry_ids))
		{
			$entry_ids = array($entry_ids);
		}
		
		$author_ids = array();
		
		$query = $this->db->select('author_id')
				  ->distinct()
				  ->where('channel_titles.channel_id', $this->store->config('purchased_items_channel'))
				  ->where_in('field_id_'.$this->store->config('purchased_items_id_field'), $entry_ids)
				  ->join('channel_data', 'channel_titles.entry_id = channel_data.entry_id')
				  ->get('channel_titles');
		
		foreach ($query->result() as $row)
		{
			$author_ids[] = $row->author_id;
		}
		
		if (count($author_ids) !== 0)
		{
			$query = $this->db->select('field_id_'.$this->store->config('purchased_items_id_field'))
					  ->where('channel_titles.channel_id', $this->store->config('purchased_items_channel'))
					  ->where_in('author_id', $author_ids)
					  ->where_not_in('field_id_'.$this->store->config('purchased_items_id_field'), $entry_ids)
					  ->join('channel_data', 'channel_titles.entry_id = channel_data.entry_id')
					  ->get('channel_titles');
			
			foreach ($query->result_array() as $row)
			{
				if (isset($purchased[$row['field_id_'.$this->store->config('purchased_items_id_field')]]))
				{
					$purchased[$row['field_id_'.$this->store->config('purchased_items_id_field')]]++;
				}
				else
				{
					$purchased[$row['field_id_'.$this->store->config('purchased_items_id_field')]] = 1;
				}
			}
		}
		
		if ( ! $limit)
		{
			$limit = 20;
		}
		
		arsort($purchased);
		
		$purchased = array_slice($purchased, 0, $limit, TRUE);
		
		return $purchased;
	}
	
	public function has_purchased($entry_id)
	{
		if ( ! $this->store->config('purchased_items_channel') || ! $this->store->config('purchased_items_id_field'))
		{
			return FALSE;
		}
		
		$this->db->from('channel_titles, channel_data')
				->where('author_id', $this->session->userdata('member_id'))
				->where('field_id_'.$this->store->config('purchased_items_id_field'), $entry_id);
		
		return ($this->db->count_all_results() > 0);
	}
	
	public function update_purchased_item($entry_id, $data)
	{
		return $this->cartthrob_entries_model->update_entry($entry_id, $data);
	}
	
	public function create_purchased_item($item_data, $order_id, $status)
	{
		$this->load->model('cartthrob_members_model');
		$this->load->helper('url');
		
		if ( ! $channel_id = $this->store->config('purchased_items_channel'))
		{
			return 0;
		}
		
		$product = $this->store->product($item_data['product_id']);
		
		$title = element('title', $item_data);
		
		if ($product && ! $title)
		{
			$title = $product->title();
		}
		
		$word_separator = ($this->config->item('word_separator') === 'underscore') ? '_' : $this->config->item('word_separator');
		
		$data = array(
			'title' => $this->store->config('purchased_items_title_prefix').$title,
			'url_title' => url_title($title, $word_separator, TRUE).$word_separator.uniqid(NULL, TRUE),
			'author_id' => $this->cartthrob_members_model->get_member_id(),
			'channel_id' => $channel_id,
			'status' => ($status) ? $status : 'closed',
		);
		
		if ( ! empty($item_data['meta']['expiration_date']))
		{
			$data['expiration_date'] = $this->localize->now + ($item_data['meta']['expiration_date']*24*60*60);
		}

		if ($this->store->config('purchased_items_id_field') && isset($item_data['product_id']))
		{
			$data['field_id_'.$this->store->config('purchased_items_id_field')] = $item_data['product_id'];
		}
		
		if ($this->store->config('purchased_items_quantity_field') && isset($item_data['quantity']))
		{
			$data['field_id_'.$this->store->config('purchased_items_quantity_field')] = $item_data['quantity'];
		}
		
		if ($this->store->config('purchased_items_price_field'))
		{
			if ( ! empty($item_data['price']))
			{
				$data['field_id_'.$this->store->config('purchased_items_price_field')] = $item_data['price'];
			}
			else if ($product)
			{
				$data['field_id_'.$this->store->config('purchased_items_price_field')] = $product->price();
			}
		}
		
		if ($this->store->config('purchased_items_order_id_field') && $order_id)
		{
			$data['field_id_'.$this->store->config('purchased_items_order_id_field')] = $order_id;
		}
		
		if ($this->store->config('purchased_items_license_number_field') && ! empty($item_data['meta']['license_number']))
		{
			$limit = 25;

			$license_number = '';

			$this->load->helper('license_number');
			
			do
			{
				$license_number = generate_license_number($this->store->config('purchased_items_license_number_type'));

				$this->db->from('channel_data')
						->where('field_id_'.$this->store->config('purchased_items_license_number_field'), $license_number);

				$limit --;

			} while($this->db->count_all_results() > 0 && $limit >= 0);

			if ($limit >= 0 && $license_number)
			{
				$data['field_id_'.$this->store->config('purchased_items_license_number_field')] = $license_number;
			}
		}
		
		foreach ($this->cartthrob_field_model->get_fields_by_channel($channel_id) as $field)
		{
			if ($this->input->post($field['field_name']) !== FALSE)
			{
				$data['field_id_'.$field['field_id']] = $this->input->post($field['field_name'], TRUE);
			}

			if (isset($item['item_options'][$field['field_name']]))
			{
				$data['field_id_'.$field['field_id']] = $item_data['item_options'][$field['field_name']];
			}
			
			if (preg_match('/^purchased_(.*)/', $field['field_name'], $match) && isset($item_data['item_options'][$match[1]]))
			{
				$data['field_id_'.$field['field_id']] = $item_data['item_options'][$match[1]];
			}
		}
		
		return $this->cartthrob_entries_model->create_entry($data);
	}
}
