<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cartthrob_field_model extends CI_Model
{
	protected $fields;
	protected $channels;
	protected $matrix_cols;
	protected $matrix_rows;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('array');
		
		$this->db->select('field_id, field_name, field_label, group_id, field_type, field_settings, field_fmt')
			->from('channel_fields')
			->where('site_id', $this->config->item('site_id'))
			->order_by('group_id, field_order');
		
		foreach ($this->db->get()->result_array() as $row)
		{
			$this->fields[$row['field_id']] = $row;
		}
	}
	
	public function channel_has_fieldtype($channel_id, $fieldtype, $return_field_id = FALSE)
	{
		return $this->group_has_fieldtype($this->get_field_group($channel_id), $fieldtype, $return_field_id);
	}
	
	public function group_has_fieldtype($group_id, $fieldtype, $return_field_id = FALSE)
	{
		$fields = $this->get_fields_by_group($group_id);
		
		$this->load->library('data_filter');
		
		$this->data_filter->filter($fields, 'field_type', $fieldtype);
		
		if ($return_field_id === TRUE)
		{
			$field = current($fields);
			
			return ($field) ? $field['field_id'] : FALSE;
		}
		
		return (count($fields) > 0);
	}
	
	public function get_matrix_cols($field_id)
	{
		if ( ! $field_id)
		{
			return array();
		}
		
		$settings = $this->get_field_settings($field_id);
		
		if ( ! isset($this->matrix_cols[$field_id]))
		{
			$this->matrix_cols[$field_id] = (empty($settings['col_ids']))
							? array()
							: $this->db->where_in('col_id', $settings['col_ids'])
								->get('matrix_cols')
								->result_array();
		}
		
		return $this->matrix_cols[$field_id];
	}
	
	public function get_matrix_rows($entry_id, $field_id)
	{
		if ( ! $entry_id || ! $field_id)
		{
			return array();
		}
		
		if ( ! isset($this->matrix_rows[$entry_id][$field_id]))
		{
			$this->matrix_rows[$entry_id][$field_id] = $this->db->where('entry_id', $entry_id)
										->where('field_id', $field_id)
										->get('matrix_data')
										->result_array();
		}
		
		return $this->matrix_rows[$entry_id][$field_id];
	}
	
	public function get_fields($params = array(), $limit = FALSE)
	{
		$this->load->library('data_filter');
		
		$fields = $this->fields;
		
		foreach ($params as $key => $value)
		{
			$this->data_filter->filter($fields, $key, $value);
		}
		
		if ($limit !== FALSE)
		{
			$this->data_filter->limit($fields, $limit);
		}

		return $fields;
	}

	public function get_field_by_id($field_id)
	{
		return element($field_id, $this->fields);
	}
	
	public function get_field_by_name($field_name)
	{
		return current($this->get_fields(array('field_name' => $field_name), 1));
	}

	public function get_field_id($field_name)
	{
		return element('field_id', $this->get_field_by_name($field_name));
	}

	public function get_field_name($field_id)
	{
		return element('field_name', $this->get_field_by_id($field_id));
	}

	public function get_field_label($field_id)
	{
		return element('field_label', $this->get_field_by_id($field_id));
	}

	public function get_field_fmt($field_id)
	{
		return element('field_fmt', $this->get_field_by_id($field_id));
	}

	function get_field_group($channel_id)
	{
		if (is_null($this->channels))
		{
			$this->db->select('field_group, channel_id')
				->from('channels')
				->where('site_id', $this->config->item('site_id'));
				
			foreach ($this->db->get()->result() as $row)
			{
				$this->channels[$row->channel_id] = $row->field_group;
			}
		}

		return element($channel_id, $this->channels);
	}

	public function get_fields_by_group($group_id)
	{
		return $this->get_fields(array('group_id' => $group_id));
	}

	public function get_fields_by_channel($channel_id)
	{
		return $this->get_fields_by_group($this->get_field_group($channel_id));
	}

	public function get_field_type($field_id)
	{
		return element('field_type', $this->get_field_by_id($field_id));
	}

	public function get_field_settings($field_id)
	{
		if ( ! isset($this->fields[$field_id]))
		{
			return FALSE;
		}
		
		if ($this->fields[$field_id]['field_settings'] !== FALSE || ! is_array($this->fields[$field_id]['field_settings']))
		{
			$this->fields[$field_id]['field_settings'] = _unserialize($this->fields[$field_id]['field_settings'], TRUE);
		}
		
		return $this->fields[$field_id]['field_settings'];
	}
}

/* End of file field_model.php */
/* Location: ./system/expressionengine/models/field_model.php */