<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Log model
 *
 * @package CartThrob
 * @author Chris Newton
 * @since 2.0
 * 
 * used to read and write log entries
 */
class Log_model extends CI_Model
{
	public $cartthrob, $store, $cart;
	
	public function __construct()
	{
		$this->load->library('cartthrob_loader');
		$this->cartthrob_loader->setup($this);
 	}
	
	/**
	 * log
	 *
	 * writes an entry to EE's console log
	 * 
	 * @param string|array $action 
	 * @param string $method 
	 * @return void
	 * @author Chris Newton
	 */
	public function log($action, $method="standard")
	{
		
		if ($method="js")
		{
			if (is_array($action))
			{
				foreach ($action as $act)
				{
					echo "<script type=\"text/javascript\">if (window.console){ console.log('{$act}'); }</script>";
				}
			}
			else
			{
				echo "<script type=\"text/javascript\">if (window.console){ console.log('{$action}'); }</script>";
			}
		}
		else
		{
			$this->logger->log_action($action); 
		}
 	}
	// END
}
// END CLASS