<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'cartthrob/fieldtypes/ft.cartthrob_matrix'.EXT;

/**
 * @property CI_Controller $EE
 * @property Cartthrob_core_ee $cartthrob;
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_price_modifiers_ft extends Cartthrob_matrix_ft
{
	public $info = array(
		'name' => 'CartThrob Price Modifiers',
		'version' => '1.0.0'
	);
	
	public $default_row = array(
		'option_value' => '',
		'option_name' => '',
		'price' => '',
	);
	
	public function pre_process($data)
	{
		$data = parent::pre_process($data);
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->load->library('number');
		
		foreach ($data as &$row)
		{
			if (isset($row['price']) && $row['price'] !== '')
			{
				if ($this->EE->cartthrob->store->config('tax_inclusive_price'))
				{
					if ($plugin = $this->EE->cartthrob->store->plugin($this->EE->cartthrob->store->config('tax_plugin')))
					{
						$row['price'] = $plugin->get_tax($row['price']) + $row['price'];
					}
				}
				
				$row['price_numeric'] = $row['price'];
				
				$row['price'] = $this->EE->number->format($row['price']);
			}
		}
		
		return $data;
	}
	
	public function display_field($data, $replace_tag = FALSE)
	{
		$this->EE->lang->loadfile('cartthrob', 'cartthrob');
		
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->load->helper('html');
		
		if ( ! $presets = $this->EE->cartthrob->store->config('price_modifier_presets'))
		{
			$presets = array();
		}
		
		$options = array('' => $this->EE->lang->line('select_preset'));
		
		$json_presets = array();
		
		foreach ($presets as $key => $preset)
		{
			$json_presets[] = array(
				'name' => $key,
				'values' => $preset,
			);
			
			$options[] = $key;
		}
		
		$this->additional_controls = ul(
			array(
				form_dropdown('', $options),
				form_submit('', $this->EE->lang->line('load_preset'), 'onclick="$.cartthrobPriceModifiers.loadPreset($(this).parents(\'div.cartthrobMatrixControls\').prev(\'table.cartthrobMatrix\')); return false;"'),
				form_submit('', $this->EE->lang->line('delete_preset'), 'onclick="$.cartthrobPriceModifiers.deletePreset($(this).parents(\'div.cartthrobMatrixControls\').prev(\'table.cartthrobMatrix\')); return false;"'),
				form_submit('', $this->EE->lang->line('save_preset'), 'onclick="$.cartthrobPriceModifiers.savePreset($(this).parents(\'div.cartthrobMatrixControls\').prev(\'table.cartthrobMatrix\')); return false;"'),
			),
			array('class' => 'cartthrobMatrixPresets')
		);
		
		if ($this->EE->input->get('channel_id') && $this->field_id == $this->EE->cartthrob->store->config('product_channel_fields', $this->EE->input->get('channel_id'), 'inventory'))
		{
			$this->default_row['inventory'] = '';
		}
		
		if (empty($this->EE->session->cache['cartthrob_price_modifiers']['head']))
		{
			//always use action
			$url = (REQ === 'CP') ? 'EE.BASE+"&C=addons_modules&M=show_module_cp&module=cartthrob&method=save_price_modifier_presets_action"'
					     : 'EE.BASE+"ACT="+'.$this->EE->functions->fetch_action_id('Cartthrob_mcp', 'save_price_modifier_presets_action');
			
			$this->EE->cp->add_to_head('
			<script type="text/javascript">
			$.cartthrobPriceModifiers = {
				currentPreset: function(e) {
					return $(e).next("div.cartthrobMatrixControls").find("ul.cartthrobMatrixPresets select").val() || "";
				},
				presets: '.$this->EE->javascript->generate_json($json_presets, TRUE).',
				savePreset: function(e) {
					var currentValue = (this.presets[this.currentPreset(e)] !== undefined) ? this.presets[this.currentPreset(e)].name : "";
					var name = prompt("'.$this->EE->lang->line('name_preset_prompt').'", currentValue);
					if (name)
					{
						this.presets.push({"name": name, "values": $.cartthrobMatrix.serialize(e)});
						this.updatePresets();
					}
				},
				updatePresets: function() {
					var select = "<select>";
					select += "<option value=\'\'>'.$this->EE->lang->line('select_preset').'</option>";
					for (i in this.presets) {
						select += "<option value=\'"+i+"\'>"+this.presets[i].name+"</option>";
					}
					select += "</select>";
					$("div.cartthrobMatrixControls ul.cartthrobMatrixPresets select").replaceWith(select);
					$.post(
						'.$url.',
						{
							"XID": EE.XID,
							"price_modifier_presets": this.presets
						},
						function(data){
							EE.XID = data.XID;
						},
						"json"
					);
				},
				loadPreset: function(e) {
					var which = this.currentPreset(e);
					if (this.presets[which] != undefined && confirm("'.$this->EE->lang->line('load_preset_confirm').'")) {
						$.cartthrobMatrix.unserialize(e, this.presets[which].values);
					}
				},
				deletePreset: function(e) {
					var which = this.currentPreset(e);
					if (which && this.presets[which] != undefined && confirm("'.$this->EE->lang->line('delete_preset_confirm').'")) {
						delete this.presets[which];
						this.updatePresets();
					}
				}
			};
			</script>
			');
			
			$this->EE->session->cache['cartthrob_price_modifiers']['head'] = TRUE;
		}
		
		return parent::display_field($data, $replace_tag);
	}
}

/* End of file ft.cartthrob_discount.php */
/* Location: ./system/expressionengine/third_party/cartthrob_discount/ft.cartthrob_discount.php */