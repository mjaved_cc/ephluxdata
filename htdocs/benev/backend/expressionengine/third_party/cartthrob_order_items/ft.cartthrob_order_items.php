<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'cartthrob/fieldtypes/ft.cartthrob_matrix'.EXT;

/**
 * @property CI_Controller $EE
 * @property Cartthrob_core_ee $cartthrob;
 */
class Cartthrob_order_items_ft extends Cartthrob_matrix_ft
{
	public $EE, $cartthrob;
	
	public $info = array(
		'name' => 'CartThrob Order Items',
		'version' => '1.0.1'
	);
	
	public $variable_prefix = 'item:';
	
	public $row_nomenclature = 'item';
	
	public $default_row = array(
		'entry_id' => '',
		'title' => '',
		'quantity' => '',
		'price' => ''
	);
	
	public $default_columns = array(
		'row_id',
		'row_order',
		'order_id',
		'entry_id',
		'title',
		'quantity',
		'price',
		'weight',
		'shipping',
		'no_tax',
		'no_shipping',
		'extra',
	);
	
	public function pre_process($data)
	{
		$this->EE->load->library('cartthrob_loader');
		
		$this->EE->load->library('number');
		
		$this->EE->load->model('order_model');
		
		$data = $this->EE->order_model->get_order_items($this->row['entry_id']);
		/*
		foreach ($data as &$row)
		{
			if (isset($row['price']) && $row['price'] !== '')
			{
				$row['price_numeric'] = $row['price'];
				$row['price'] = $this->EE->number->format($row['price']);
			}
		}
		*/
		
		//this is to set blank columns that arent in all rows
		if ( ! isset($this->EE->session->cache['cartthrob_order_items']['extra_columns']))
		{
			$this->EE->session->cache['cartthrob_order_items']['extra_columns'] = array();
		}
		
		foreach ($data as &$row)
		{
			foreach (array_keys($row) as $key)
			{
				if (in_array($key, $this->default_columns))
				{
					continue;
				}
				
				if ( ! in_array($key, $this->EE->session->cache['cartthrob_order_items']['extra_columns']))
				{
					$this->EE->session->cache['cartthrob_order_items']['extra_columns'][] = $key;
				}
			}
			
			foreach ($this->EE->session->cache['cartthrob_order_items']['extra_columns'] as $key)
			{
				if ( ! isset($row[$key]))
				{
					$row[$key] = '';
				}
			}
		}
		
		return $data;
	}
	
	public function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		//@TODO fix this fast.
		if (count($data) === 0 && preg_match('/'.LD.'if '.$this->variable_prefix.'no_results'.RD.'(.*?)'.LD.'\/if'.RD.'/s', $tagdata, $match))
		{
			$tagdata = str_replace($match[0], '', $tagdata);
			
			$this->EE->TMPL->no_results = $match[1];
		}
		
		if ( ! $data)
		{
			return $this->EE->TMPL->no_results();
		}
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->helper('data_formatting');
		
		$total_results = count($data);
		
		$this->EE->load->library('data_filter');
		
		$this->EE->load->model(array('product_model', 'cartthrob_entries_model'));
		
		$this->EE->data_filter->sort($data, (isset($params['orderby'])) ? $params['orderby'] : FALSE, (isset($params['sort'])) ? $params['sort'] : FALSE);
		
		$count = 1;
		
		foreach ($data as &$row)
		{
			$row['count'] = $count;
			
			$row['total_results'] = $total_results;
			
			$row['first_'.$this->row_nomenclature] = (int) ($count === 1);
			
			$row['last_'.$this->row_nomenclature] = (int) ($count === $total_results);
			
			$row[$this->row_nomenclature.'_count'] = $row['count'];
			
			$row['total_'.$this->row_nomenclature.'s'] = $row['total_results'];
		
			if (preg_match_all('/'.LD.'('.preg_quote($this->variable_prefix).'|row_)?'.'switch=([\042\047])(.+)\\2'.RD.'/', $tagdata, $matches))
			{
				foreach ($matches[0] as $i => $v)
				{
					$values = explode('|', $matches[3][$i]);
					
					$row[substr($matches[0][$i], 1, -1)] = $values[($count - 1) % count($values)];
				}
			}
			
			if (isset($row['price']) && $row['price'] !== '')
			{
				$row['price_numeric'] = $row['price'];
				$row['price'] = $this->EE->number->format($row['price']);
			}
			
			if (isset($row['entry_id']) && $product = $this->EE->product_model->get_product($row['entry_id']))
			{
				$vars = $this->EE->functions->assign_variables($tagdata);
				
				$row = array_merge($this->EE->cartthrob_entries_model->entry_vars($product, $tagdata, $vars['var_single'], $vars['var_pair']), $row);
			}
			
			$row = array_merge($row, array_key_prefix($row, $this->variable_prefix));
			
			$count++;
		}
		
		return $this->EE->TMPL->parse_variables($tagdata, $data);
	}
	
	public function display_field($data, $replace_tag = FALSE)
	{
		$this->EE->load->model('order_model');
		
		if ( ! $replace_tag)
		{
			$data = ($this->EE->input->get_post('entry_id'))
				? $this->EE->order_model->get_order_items($this->EE->input->get_post('entry_id', TRUE))
				: array();
		}
		
		$hide_fields = array(
			'weight',
			'shipping',
			'no_tax',
			'no_shipping',
		);
		
		foreach ($data as &$row)
		{
			unset($row['row_order'], $row['order_id'], $row['extra']);
			
			foreach ($hide_fields as $key)
			{
				if ( ! empty($row[$key]) && $row[$key] != 0)
				{
					unset($hide_fields[array_search($key, $hide_fields)]);
				}
			}
		}
		
		$this->hidden_columns = $hide_fields;
		$this->hidden_columns[] = 'row_id';
		
		return parent::display_field($data, $replace_tag);
	}
	
	public function save($data)
	{
		$this->EE->session->cache['cartthrob_order_items'][$this->field_id] = NULL;
		
		if (is_array($data))
		{
			//if there's just one empty row
			if (count($data) === 1 && count(array_filter(current($data))) === 0)
			{
				return '';
			}
			
			$this->EE->session->cache['cartthrob_order_items'][$this->field_id] = $data;
			
			return 1;
		}
		
		return '';
	}
	
	public function post_save($data)
	{
		if ($data)
		{
			$data = $this->EE->session->cache['cartthrob_order_items'][$this->field_id];
			
			//if there's just one empty row
			if ( ! is_array($data) || (count($data) === 1 && count(array_filter(current($data))) === 0))
			{
				return;
			}
			
			$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
			
			$this->EE->load->model('order_model');
			
			$this->EE->order_model->update_order_items($this->settings['entry_id'], $data);
		}
	}
	
	public function delete($entry_ids)
	{
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->model('order_model');
		
		$this->EE->order_model->delete_order_items($entry_ids);
	}
}

/* End of file ft.cartthrob_discount.php */
/* Location: ./system/expressionengine/third_party/cartthrob_discount/ft.cartthrob_discount.php */