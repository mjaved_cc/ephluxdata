<?php
$name = 'UKAD default settings';

// this is contents of a javascript object, format as such
$editor_config = "

height: '400px'

,forcePasteAsPlainText: true
,startupOutlineBlocks:true
,scayt_autoStartup: true
,scayt_sLang: 'en_GB'

,disableNativeSpellChecker: false


,extraPlugins: 'eepages,video'
,templates_replaceContent: false

,toolbar:
[
    ['Maximize','Source','ShowBlocks'],['Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
    ['Scayt','Undo','Redo','-','Find','Replace','-','SelectAll'],
    ['Format'],
    '/',
    ['Bold','Italic','Underline','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent'],
    ['JustifyLeft','JustifyCenter','JustifyRight','Blockquote'],
    ['Link','eepages','Unlink','Image','Anchor'],
    ['HorizontalRule','SpecialChar','Table'],
    '/',
]

,format_tags: 'p;div;h2;h3'
	
// add styling to the editor content to match your site
,contentsCss: '/assets/ckstyles.css'

// add styles like so, see http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Styles
,stylesCombo_stylesSet: 'my_styles:/assets/ckstyles.js'

// add templates like so, see http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Templates
,templates_files: [ '/assets/cktemplates.js' ]
";