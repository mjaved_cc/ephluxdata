<?php
//require_once "Games.php";

/**
 * MuzeGames
 */
 
class MuzeGames{
	

	/** MuzeGames dir path
	 *
	 */
	const DATAFILES = "muze_game_data/Export/Games/ASCII/Incrementals/";

	/**
	 * @var string
	 */
	private $delimiter = '|';

	public function __construct()
	{
	
	}

	public function readFile($filename)
	{
		$data = file($filename); 
		return $data;		
	}

	public function getGames()
	{
		$resource = self::DATAFILES.'Product.txt';
		
		$games = array_slice($this->readFile($resource), 1); 
       
		$data = array();
		$count = 1;
		foreach($games as $records)
	 	{
			$game = new Games();
			$attributes = explode($this->delimiter,$records);
			$game->api   =  (int) $attributes[0];
			$game->title = (string) $this->getTitle($attributes[2]);
			$game->titleID = (string) $attributes[2];
			$game->desc = (string) $this->getDescription($attributes[0]);
			$game->console = (string) $attributes[1];
            if (count($this->getProductGenre($attributes[0]))>1)
			$game->genre = (string) implode(',',(string)$this->getProductGenre($attributes[0]));
            else
            {$game->genre = $this->getProductGenre($attributes[0]);
            $game->genre=$game->genre[0];
            }
			$game->screenshot = (string) $this->getScreenshot($attributes[0]);
			$game->publisher =  (string) $this->getCompany($this->getPublisher($attributes[0]));
			$game->developers =  (string) $this->getCompany($this->getDeveloper($attributes[0]));
			$game->barcode = (string) $attributes[4];
			$game->price = (string) $attributes[7];
			$game->image = $this->getCoverImage($attributes[0]);
			$game->releaseDate = strtotime($attributes[8]);
			$data[] = $game;
			//if($count == 25): break; else: $count++; endif;
		}
			return $data;
		
	}
	
	public function getTitle($titleID)
	{
		$resource = self::DATAFILES.'Title.txt';
		
		$titles = array_slice($this->readFile($resource), 1); 
		
		foreach($titles as $records)
	 	{
			$attributes = explode($this->delimiter,$records);

			$pattern = '/^' . preg_quote($attributes[0], '/') . '$/';
			if (preg_match($pattern, $titleID))
 			{
				return $attributes[2];
 				break;
 			}
			
		}

	}

	public function getDescription($gameID)
	{
		$resource = self::DATAFILES.'ProductAnnotation.txt';
		
		$description = array_slice($this->readFile($resource), 1); 
		
		foreach($description as $records)
	 	{
			$attributes = explode($this->delimiter,$records);

			$pattern = '/^' . preg_quote($attributes[0], '/') . '$/';

			if (preg_match($pattern, $gameID))
 			{
				return $attributes[3];
 				break;
 			}
		}
	}

	public function getCoverImage($gameID)
	{
		$resource = self::DATAFILES.'ProductImage.txt';
		
		$coverImages = array_slice($this->readFile($resource), 1); 
		
		foreach($coverImages as $records)
	 	{
			$image_attributes = explode($this->delimiter,$records);

			$pattern1 = '/^' . preg_quote($image_attributes[0], '/') . '$/';
			$pattern2 = '/^' . preg_quote($image_attributes[3], '/') . '$/';

			if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Cover'))
 			{
				return $image_attributes[2];
 				break;
 			}
		}
	}

	public function getScreenshot($gameID)
	{
		$resource = self::DATAFILES.'ProductImage.txt';
		
		$Screenshots = array_slice($this->readFile($resource), 1); 
		
		foreach($Screenshots as $records)
	 	{
			$attributes = explode($this->delimiter,$records);

			$pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
			$pattern2 = '/^' . preg_quote($attributes[3], '/') . '$/';

			if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Screenshot'))
 			{
				//print 'find';
				$screenshot[] = $attributes[2];
 			}
		}
		
		$screenshot = (!empty($screenshot)) ? implode(',', $screenshot) : '';  
		return $screenshot;
	}

	public function getProductGenre($gameID)
	{
		$resource = self::DATAFILES.'ProductGenre.txt';
		
		$productGenre = array_slice($this->readFile($resource), 1); 
		
		foreach($productGenre as $records)
	 	{
			$genre_attributes = explode($this->delimiter,$records);

			$pattern = '/^' . preg_quote($genre_attributes[1], '/') . '$/';

			if (preg_match($pattern, $gameID))
 			{
				$genres[] = $genre_attributes[2];
 			}
		}
		
		return $genres;
	}

	public function getPublisher($gameID)
	{
		$resource = self::DATAFILES.'ProductCompany.txt';
		
		$ProductCompany = array_slice($this->readFile($resource), 1); 
		
		foreach($ProductCompany as $records)
	 	{
			$attributes = explode($this->delimiter,$records);
			$publisher = trim($attributes[2]);
			$pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
			$pattern2 = '/^' . preg_quote($publisher, '/') . '$/';

			if ( preg_match($pattern1, $gameID) && preg_match($pattern2, 'Publisher') )
 			{
				return $attributes[1];
				break;
 			}
		}
	}

	public function getDeveloper($gameID)
	{
		$resource = self::DATAFILES.'ProductCompany.txt';
		
		$ProductCompany = array_slice($this->readFile($resource), 1); 
		
		foreach($ProductCompany as $records)
	 	{
			$attributes = explode($this->delimiter,$records);
			$developer = trim($attributes[2]);
			$pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
			$pattern2 = '/^' . preg_quote($developer, '/') . '$/';

			if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Developer'))
 			{
				return $attributes[1];
				break;
 			}
		}
	}

	public function getCompany($companyID)
	{
		//print $companyID;
		$resource = self::DATAFILES.'Company.txt';
		
		$Company = array_slice($this->readFile($resource), 1); 
		
		foreach($Company as $records)
	 	{
			$attributes = explode($this->delimiter,$records);

			$pattern = '/^' . preg_quote($attributes[0], '/') . '$/';

			if (preg_match($pattern, $companyID))
 			{
				return $attributes[1];
				break;
 			}
		}
	}

	public function getAllConsole()
	{
		$resource = self::DATAFILES.'Product.txt';
		$games = array_slice($this->readFile($resource), 1); 
		$data = array();
		foreach($games as $records)
	 	{
			$attributes = explode($this->delimiter,$records);
			$data[]	= $attributes[1];
		}
			return array_unique($data); 
	}

	public function getAllGenre()
	{
		$resource = self::DATAFILES.'ProductGenre.txt';
		$genres = array_slice($this->readFile($resource), 1); 
		$data = array();
		foreach($genres as $records)
	 	{
			$attributes = explode($this->delimiter,$records);
			$data[]	= $attributes[2];
		}
			return array_unique($data); 
	}
	
	public function getSimilarGames()
	{
		$resource = self::DATAFILES.'Similar.txt';
		$games = array_slice($this->readFile($resource), 1); 
		$data = array();
		$count = 0;
		foreach($games as $records)
	 	{
			$attributes = explode($this->delimiter,$records); 
			$data[$count]['TitleID']	= $attributes[0];
			$data[$count]['SimilarTitleID']	= $attributes[1]; 
			$count++;
			
		}
			return $data; 
	}
	
}
 