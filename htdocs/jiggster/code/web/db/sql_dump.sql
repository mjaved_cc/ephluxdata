/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.0.45-community-nt : Database - jiggster
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jiggster` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `jiggster`;

/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acos` */

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `aros` */

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL auto_increment,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL default '0',
  `_read` varchar(2) NOT NULL default '0',
  `_update` varchar(2) NOT NULL default '0',
  `_delete` varchar(2) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `aros_acos` */

/*Table structure for table `auth_action_maps` */

DROP TABLE IF EXISTS `auth_action_maps`;

CREATE TABLE `auth_action_maps` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL default '5',
  `crud` enum('create','read','update','delete') NOT NULL default 'read',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_action_maps` */

/*Table structure for table `chatlogs` */

DROP TABLE IF EXISTS `chatlogs`;

CREATE TABLE `chatlogs` (
  `id` int(11) NOT NULL auto_increment,
  `status` tinyint(1) unsigned NOT NULL default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `chatlogs` */

insert  into `chatlogs`(`id`,`status`,`created`) values (1,1,'2013-11-21 02:23:12'),(2,1,'2013-11-28 00:36:00');

/*Table structure for table `consoles` */

DROP TABLE IF EXISTS `consoles`;

CREATE TABLE `consoles` (
  `id` int(11) NOT NULL auto_increment,
  `console` varchar(99) default NULL,
  `status` tinyint(4) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `consoles` */

insert  into `consoles`(`id`,`console`,`status`,`created`) values (1,'Nintendo Wii',1,'2013-11-20 18:23:26'),(2,'Nintendo Dual Screen',1,'2013-11-20 18:23:26'),(3,'Personal Computer',1,'2013-11-20 18:23:26'),(4,'Sony PlayStation Portable',1,'2013-11-20 18:23:26'),(5,'Sony PlayStation 2',1,'2013-11-20 18:23:26'),(6,'Sony PlayStation 3',1,'2013-11-20 18:23:26'),(7,'Microsoft Xbox 360',1,'2013-11-20 18:23:26'),(8,'Apple Macintosh',1,'2013-11-20 18:23:27'),(9,'Nintendo Game Boy Advanced',1,'2013-11-20 18:23:27'),(10,'PC and Mac compatible game',1,'2013-11-20 18:23:27'),(11,'Microsoft XBox',1,'2013-11-20 18:23:27'),(12,'Nintendo Game Cube',1,'2013-11-20 18:23:27'),(13,'Nintendo Game Boy Color',1,'2013-11-20 18:23:27'),(14,'Nintendo DSi',1,'2013-11-20 18:23:27'),(15,'Nintendo 3DS',1,'2013-11-20 18:23:27'),(16,'PlayStation Vita',1,'2013-11-20 18:23:27'),(17,'Nintendo Wii U',1,'2013-11-20 18:23:27'),(18,'PlayStation 4',1,'2013-11-20 18:23:27'),(19,'Microsoft Xbox One',1,'2013-11-20 18:23:27');

/*Table structure for table `deals` */

DROP TABLE IF EXISTS `deals`;

CREATE TABLE `deals` (
  `id` int(11) NOT NULL auto_increment,
  `offered_by` int(11) NOT NULL,
  `amount` int(11) default NULL,
  `comment` text,
  `last_action_by` int(11) default NULL,
  `last_action_date` timestamp NULL default NULL,
  `status` int(1) default '0',
  `created` timestamp NULL default CURRENT_TIMESTAMP,
  `modified` timestamp NULL default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `deals` */

insert  into `deals`(`id`,`offered_by`,`amount`,`comment`,`last_action_by`,`last_action_date`,`status`,`created`,`modified`) values (1,1,20,'this is testing',NULL,NULL,0,'2013-11-21 23:46:18','2013-11-22 00:24:37'),(2,1,20,'this is testing',NULL,NULL,0,'2013-11-22 00:28:38',NULL),(3,1,0,'this is testing',NULL,NULL,1,'2013-11-22 00:31:23',NULL),(4,1,0,'this is testing',NULL,NULL,1,'2013-11-22 00:34:53',NULL),(5,1,0,'this is testing',NULL,NULL,1,'2013-11-22 00:35:59',NULL),(6,1,0,'this is testing',NULL,NULL,1,'2013-11-22 00:37:50',NULL),(7,65,0,'',66,'2013-11-25 21:49:26',1,'2013-11-23 02:51:51','2013-11-25 21:49:26'),(8,28,10,'',68,'2013-11-27 22:45:12',0,'2013-11-27 14:23:26','2013-11-27 22:45:12'),(9,72,10,'',72,'2013-11-27 22:30:01',1,'2013-11-27 22:30:01',NULL),(10,28,10,'',28,'2013-11-27 22:42:10',1,'2013-11-27 22:42:10',NULL),(11,28,10,'',28,'2013-11-27 22:42:32',1,'2013-11-27 22:42:32',NULL),(12,28,10,'',28,'2013-11-27 22:43:54',1,'2013-11-27 22:43:54',NULL),(13,72,25,'',72,'2013-11-28 12:49:31',1,'2013-11-27 23:08:46','2013-11-28 12:49:31'),(14,72,0,'',72,'2013-11-28 12:46:03',1,'2013-11-27 23:22:39','2013-11-28 12:46:03'),(15,68,25,'',38,'2013-11-27 23:43:15',1,'2013-11-27 23:41:43','2013-11-27 23:43:15'),(16,68,2,'',68,'2013-11-28 00:17:54',1,'2013-11-28 00:17:54',NULL),(17,25,0,'pp',25,'2013-11-28 00:36:21',0,'2013-11-28 00:36:21',NULL),(18,68,5,'',38,'2013-11-28 00:39:29',0,'2013-11-28 00:37:10','2013-11-28 00:39:29');

/*Table structure for table `deals_chatlogs` */

DROP TABLE IF EXISTS `deals_chatlogs`;

CREATE TABLE `deals_chatlogs` (
  `id` int(11) NOT NULL auto_increment,
  `user_deal_id` int(11) default NULL,
  `id_sender` int(11) default NULL,
  `id_receiver` int(11) default NULL,
  `message` text character set latin1,
  `status` enum('read','unread') default 'unread',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_user_deals_reference` (`user_deal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `deals_chatlogs` */

insert  into `deals_chatlogs`(`id`,`user_deal_id`,`id_sender`,`id_receiver`,`message`,`status`,`created`) values (1,1,2,1,'This is deal message','read','2013-11-26 17:16:52'),(2,1,2,1,'This is deal message','read','2013-11-26 17:16:52'),(3,1,2,1,'This is deal message','read','2013-11-26 17:16:52');

/*Table structure for table `deals_proposals` */

DROP TABLE IF EXISTS `deals_proposals`;

CREATE TABLE `deals_proposals` (
  `id` int(11) NOT NULL auto_increment,
  `for_user` int(11) NOT NULL,
  `with_user` int(11) NOT NULL,
  `trade` enum('sell','trade','sell or trade') character set latin1 default NULL,
  `amount` int(11) default NULL,
  `send_games` varchar(255) character set latin1 default NULL,
  `receive_games` varchar(255) character set latin1 default NULL,
  `status` int(1) default '0',
  `created` timestamp NULL default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `deals_proposals` */

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert  into `features`(`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'User Management',2130706433,1,'2013-04-19 20:33:14','2013-04-24 14:44:06'),(2,'Acl Management',2130706433,1,'2013-04-19 21:18:13','2013-04-24 14:44:20'),(3,'Group Management',2130706433,1,'2013-04-19 21:22:18','2013-04-24 14:44:23'),(4,'Plugins',2130706433,1,'2013-04-19 21:26:44','2013-04-24 14:44:26'),(5,'Others',2130706433,1,'2013-04-22 16:13:27','2013-04-24 14:44:29'),(7,'Test Feature',2130706433,0,'2013-04-24 16:05:54','2013-04-24 16:11:26'),(8,'Feature Management',2130706433,1,'2013-04-24 21:54:03','2013-04-24 21:54:03');

/*Table structure for table `games` */

DROP TABLE IF EXISTS `games`;

CREATE TABLE `games` (
  `id` int(11) NOT NULL auto_increment,
  `api` int(11) default NULL,
  `title` tinytext character set latin1,
  `desc` text character set latin1,
  `console_id` int(11) default NULL,
  `genre_id` int(11) default NULL,
  `publisher` varchar(255) character set latin1 default NULL,
  `developers` varchar(255) default NULL,
  `barcode` bigint(11) NOT NULL,
  `buying` bigint(11) default '0',
  `selling` bigint(11) default '0',
  `trading` bigint(11) default '0',
  `ongoing_deals` int(11) default '0',
  `rating` int(11) default '0',
  `image_url` varchar(255) default NULL,
  `screenshot` varchar(999) default NULL,
  `jiggster_pick` tinyint(1) default '0',
  `status` int(1) default '0',
  `release_date` timestamp NULL default NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `modified` timestamp NULL default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `games` */

insert  into `games`(`id`,`api`,`title`,`desc`,`console_id`,`genre_id`,`publisher`,`developers`,`barcode`,`buying`,`selling`,`trading`,`ongoing_deals`,`rating`,`image_url`,`screenshot`,`jiggster_pick`,`status`,`release_date`,`created`,`modified`) values (1,1,'Wii Chess','Taking chess kicking and screaming into the 21st century...<BR><BR>You can now test your Chess skills against the Wii, with the latest Touch Generations title from Wii. Wii chess is considered one of the most powerful chess computers. Wii chess uses loop ',1,1,'Nintendo','',45496364878,1,0,0,12,10,'128014.jpg','128015.jpg,128016.jpg',0,1,'2008-01-18 00:01:00','2013-11-27 16:42:30',NULL),(2,2,'Zack & Wiki - Quest for Barbaro\'s Treasure','Discover the legendary treasure that was hidden away by the great pirate Barbaros.<BR><BR>Enter a world of puzzles and piracy in Wii adventure Zack and Wiki: Quest for Barbaros’ Treasure. rafted exclusively for Wii by Capcom, this point-and-click puzzle-s',1,2,'Nintendo','',45496364397,4,4,7,4,10,'34.jpg','102196.jpg,102197.jpg',0,1,'2008-01-18 00:01:00','2013-11-29 17:23:08',NULL),(3,3,'Donkey Kong - Jet Race','Donkey Kong is back...and he brought friends!<BR><BR>Donkey Kong: Jet Blast invites you to drum the Wii Remote and Nunchuk to zoom Donkey Kong through a series of races and be the first to cross the finish line. You\'ll collect bananas and power-ups to com',1,3,'Nintendo','',45496900403,1,2,4,7,10,'35.jpg','102914.jpg,102915.jpg',0,1,'2008-01-25 00:01:00','2013-11-28 21:31:05',NULL),(4,4,'Advance Wars - Dark Conflict','Advance to victory.<BR><BR>Meteor storms, earthquakes and tidal waves devastate the Earth, causing <BR><BR>90 percent of the world’s population to perish. The sky has been blocked out by ash, yielding little sunlight. In a world ravaged by a domino effect',2,4,'Nintendo','',45496739447,2,0,3,14,10,'36.jpg','103457.jpg,103458.jpg',0,1,'2008-01-25 00:01:00','2013-11-29 17:21:33',NULL),(5,5,'Battalion Wars 2','Cry \'Havoc!\' and let slip the commanding officers of Battalion Wars 2.<BR><BR>Tricked into believing that the Solar Empire is developing a devastating superweapon, Commander Pierce and Colonel Windsor of the Anglo Isles launch a pre-emptive strike. <BR><B',1,4,'Nintendo','',45496900250,0,0,1,14,10,'37.jpg','104409.jpg,104410.jpg',0,1,'2008-02-15 00:02:00','2013-11-22 02:19:24',NULL),(6,6,'Professor Kageyama\'s Maths Training','Improve your calculus with DS!<BR><BR>The foundation of Dr Kageyama’s Maths Training is 100 square calculations a method conceived by Kageyama Hidea, a Japanese elementary school principal. The DS is held side ways like a book and all answers are written ',2,2,'Nintendo','',45496466176,3,3,6,16,10,'38.jpg','105304.jpg,105305.jpg',0,1,'2008-02-08 00:02:00','2013-11-29 19:32:31',NULL),(7,7,'Naruto - Ninja Destiny','The latest action packed adventure from the Naruto series.<BR><BR>Feel the impact as the Naruto franchise brings 3D fighting action to Nintendo DS! Unleash your ultimate jutsu in Naruto: Ninja Destiny, a pure fighting adventure that will blow you away! <B',2,5,'Nintendo','',45496466299,0,0,0,7,10,'4411.jpg','107092.jpg,107093.jpg',0,1,'2008-02-15 00:02:00','2013-11-22 02:20:47',NULL),(8,8,'Magic Made Fun','Begin as a disciple...and end up as the Master!<BR><BR>Did you know Nintendo DS was magic? It is now, as making magic first teaches users how to perform incredible magic and card tricks, then acts as an assistant as they perform for others. Once users lea',2,1,'Nintendo','',45496739294,1,0,5,11,10,'39.jpg','108141.jpg,108142.jpg',0,1,'2008-03-14 00:03:00','2013-11-28 21:24:16',NULL),(9,9,'Naruto - Clash of Ninja Revolution','Naruto makes its Wii debut.<BR><BR>Naruto Wii takes full advantage of the unique Wii control’s as this game main control style uses both the Wii Remote and Nunchuck attachment. The Nunchuck\'s analog stick is used for movement while its triggers will make ',1,5,'Nintendo','',45496365677,0,0,3,13,10,'40.jpg','108988.jpg,108989.jpg',0,1,'2008-12-05 00:12:00','2013-11-22 02:21:50',NULL),(10,10,'Harvest Moon - Magical Melody','Find a partner, get married and raise a family!<BR><BR>There\'s an array of livestock to choose from:  Cows, horses, sheep, chickens and many more!<BR><BR>Take pride in beating your farming rivals in the fields and at the festivals.<BR><BR>Interact with to',1,2,'Rising Star','',45496363710,3,0,3,1,10,'41.jpg','97155.jpg,97156.jpg',0,1,'2007-10-12 00:10:00','2013-11-28 20:19:09',NULL),(11,11,'Fire Emblem - Radiant Dawn','Plan the perfect battle and fight for a country’s freedom as Fire Emblem: Radiant Dawn heads to Wii! Using strategic planning and skill, mixed with a little bit of luck and magic, players must prepare to execute the perfect attack and defeat the enemies o',1,5,'Nintendo','',45496900410,0,1,1,3,10,'42.jpg','114651.jpg,114652.jpg',0,1,'2008-03-14 00:03:00','2013-11-26 18:54:18',NULL),(12,12,'Mario Kart Wii','The multiplayer racing of the Mario Kart series makes its way to Wii with online play and a fresh control scheme.\r\n',1,3,'Nintendo','',45496901004,0,1,0,0,10,'43.jpg','99995.jpg,99996.jpg',0,1,'2008-04-11 00:04:00','2013-11-27 21:04:45',NULL),(13,13,'Wii Fit','Wii Fit helps everybody in the household to exercise and manage their health in an interactive and entertaining manner. The software comes bundled with the Wii Balance Board which can measure <BR><BR>weight and balance, which allows for the body movement ',1,6,'Nintendo','',45496901080,0,0,1,0,10,'44.jpg','101047.jpg,101048.jpg',0,1,'2008-04-25 00:04:00','2013-11-22 02:22:54',NULL),(14,14,'Ace Attorney - Apollo Justice','Prepare to become champion of the courtroom, dazzling onlookers with persuasive wit and a keen sense of intuition as Apollo Justice™ Ace Attorney™ comes to the Nintendo DS. Succeeding the highly popular Phoenix Wright™ Ace Attorney™ series, this title see',2,7,'Nintendo','',45496466640,0,2,1,0,10,'45.jpg','101961.jpg,101962.jpg',0,1,'2008-05-09 00:05:00','2013-11-29 18:36:11',NULL),(15,15,'American Champion Aircraft','',3,7,'Flight1','',5060094400914,1,0,1,0,10,'112894.jpg','112896.jpg,112897.jpg',0,1,'2008-01-11 00:01:00','2013-11-28 20:19:09',NULL),(16,16,'Ultimate Terrain X - USA','',3,7,'Flight1','',5060094400860,0,0,1,0,10,'114229.jpg','Ultimate_Terrain_X_USA_Screenshot1.jpg, Ultimate_Terrain_X_USA_Screenshot2.jpg',0,1,'2008-01-18 00:01:00','2013-11-22 16:58:16',NULL),(17,17,'Ultimate Terrain X - Canada','',3,7,'Flight1','',5060094401003,0,0,0,0,10,'114227.jpg','Ultimate_Terrain_X_Canada_Screenshot1.jpg, Ultimate_Terrain_X_Canada_Screenshot2.jpg',0,1,'2008-01-18 00:01:00','2013-11-22 16:58:43',NULL),(18,18,'Messerschmitt BF109','The Messerschmitt BF109 was flown by the three highest scoring fighter aces of WWII, outperforming both Spitfires and Hurricanes in vertical climbs and dives. The iconic foe of the Allied air forces in WWII, the Messerschmitt BF109 was the staple fighter ',3,7,'Flight1','',5060094400839,1,1,2,0,10,'112895.jpg','112905.jpg,112906.jpg',0,1,'2008-03-21 00:03:00','2013-11-22 02:23:34',NULL),(19,19,'Level-D 767-300ER','',3,7,'Flight1','',5060094400617,0,0,0,4,10,'118581.jpg','118472.jpg,118473.jpg',0,1,'2008-08-01 00:08:00','2013-11-22 02:23:44',NULL),(20,20,'Ultimate Terrain X - Europe','',3,7,'Flight1','',5060094400884,0,0,0,0,10,'114228.jpg','Ultimate_Terrain_X_Europe_Screenshot1.jpg, Ultimate_Terrain_X_Europe_Screenshot2.jpg',0,1,'2008-05-09 00:05:00','2013-11-22 17:00:30',NULL),(21,21,'Democracy','Democracy thrusts you into the hot-seat as the Prime Minister (or President) of the world\'s most powerful countries. The country is run as a simple democracy where you need to secure over 50% of the vote in each election in order to remain in power. The o',3,4,'Ascaron','',4014935240130,0,0,0,4,10,'114320.jpg','Democracy_Screenshot1.jpg, Democracy_Screenshot2.jpg',0,1,'2008-02-15 00:02:00','2013-11-25 21:58:34',NULL),(22,22,'CDV Strategy Collection','Cossacks II: Napoleonic Wars: Cossacks II propels you into the historical battles of the Napoleonic Wars. Prove yourself a triumphant commander in grandiose massive battles and lead your troops victoriously from the bloody battle fields.<BR><BR>Blitzkrieg',3,4,'Ascaron','',4015756114051,0,0,0,0,10,'114323.jpg','CDV_Strategy_Collection_Screenshot1.jpg, CDV_Strategy_Collection_Screenshot2.jpg',0,1,'2008-02-08 00:02:00','2013-11-22 17:02:26',NULL),(23,23,'Speedball 2','',3,8,'Ascaron','',0,0,0,1,2,10,'speedball_2.jpg','Speedball_2_Screenshot1.jpg, Speedball_2_Screenshot2.jpg',0,1,'2008-02-08 00:02:00','2013-11-28 21:24:03',NULL),(24,24,'Agatha Christie - And Then There Were None','The first Wii® video game ever created based on the masterpiece of the world\'s best known mystery author: Agatha Christie!<BR><BR>Breathtaking third-person graphics that truly immerse the player into this thrilling mystery.<BR><BR> <BR><BR>A challenging m',1,4,'JoWooD','',9006113150206,0,1,0,0,10,'agatha_christie.jpg','102426.jpg,102427.jpg',0,1,'2008-02-08 00:02:00','2013-11-28 20:21:49',NULL),(25,25,'Final Fantasy','Remastered for the PSP, Final Fantasy lets players experience the first game in this iconic series. \r\n',4,5,'Square Enix','',5060121822498,0,1,0,5,10,'117984.jpg','102433.jpg,102434.jpg',0,1,'2008-02-08 00:02:00','2013-11-28 00:36:21',NULL),(26,20871,'Grand Theft Auto V',NULL,3,NULL,NULL,NULL,0,5,6,4,1,10,'GTA_5.jpg','GTA_5_Screenshot1.jpg, GTA_5_Screenshot2.jpg',0,1,NULL,'2013-11-28 21:31:18',NULL);

/*Table structure for table `games_genres` */

DROP TABLE IF EXISTS `games_genres`;

CREATE TABLE `games_genres` (
  `id` int(11) NOT NULL auto_increment,
  `game_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_genre_reference_to_games` (`genre_id`),
  KEY `FK_game_reference_to_genres` (`game_id`),
  CONSTRAINT `FK_game_reference_to_genres` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_genre_reference_to_games` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `games_genres` */

/*Table structure for table `genres` */

DROP TABLE IF EXISTS `genres`;

CREATE TABLE `genres` (
  `id` int(11) NOT NULL auto_increment,
  `genre` varchar(99) character set latin1 default NULL,
  `status` tinyint(1) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `genres` */

insert  into `genres`(`id`,`genre`,`status`,`created`) values (1,'Party & Games',1,'2013-11-20 18:23:52'),(2,'Learning',1,'2013-11-20 18:23:52'),(3,'Driving/Racing',1,'2013-11-20 18:23:52'),(4,'Strategy',1,'2013-11-20 18:23:52'),(5,'Action/Adventure',1,'2013-11-20 18:23:52'),(6,'Sport',1,'2013-11-20 18:23:52'),(7,'Simulation',1,'2013-11-20 18:23:52'),(8,'Fighting',1,'2013-11-20 18:23:53'),(9,'Arcade/Retro',1,'2013-11-20 18:23:53'),(10,'Music/Rhythm',1,'2013-11-20 18:23:53');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) character set latin1 NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Super Admin',2130706433,1,'2013-04-04 15:59:31','2013-04-24 20:15:22'),(2,'Admin',2130706433,1,'2013-04-04 15:59:41','2013-04-24 20:15:35'),(4,'User',2130706433,1,'2013-04-22 22:34:21','2013-04-24 20:15:39'),(5,'testing',2130706433,0,'2013-04-24 19:52:50','2013-04-24 20:18:44');

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(99) character set latin1 default NULL,
  `desc` text character set latin1,
  `status` int(1) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

insert  into `pages`(`id`,`title`,`desc`,`status`,`created`) values (1,'About Us','This is about us page',1,'2013-11-12 05:28:40'),(2,'Contact','Arcade tower\r\nShahrefaisal, \r\nKarrachi, Pakistan \r\n0210456789',1,'2013-10-26 03:06:08');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(90) default NULL,
  `nickname` varchar(90) default NULL,
  `last_name` varchar(90) default NULL,
  `username` varchar(40) character set latin1 default NULL,
  `email` varchar(40) character set latin1 default NULL,
  `password` varchar(40) character set latin1 default NULL,
  `dob` varchar(40) default NULL,
  `group_id` int(11) default NULL,
  `ip_address` int(10) default NULL,
  `status` int(11) default '0',
  `login` enum('1','2','3','4') NOT NULL,
  `gaming_id` text,
  `shipping_address` varchar(300) default NULL,
  `location` varchar(50) default NULL,
  `paypal_id` varchar(20) default NULL,
  `contact` varchar(40) default NULL,
  `image_url` varchar(256) default 'img/user_profile/default.png',
  `profile_text` text,
  `trusted_user` tinyint(1) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified` timestamp NULL default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`nickname`,`last_name`,`username`,`email`,`password`,`dob`,`group_id`,`ip_address`,`status`,`login`,`gaming_id`,`shipping_address`,`location`,`paypal_id`,`contact`,`image_url`,`profile_text`,`trusted_user`,`created`,`modified`) values (1,NULL,'azhar',NULL,NULL,'azhar.javeed@ephlux.com','25d55ad283aa400af464c76d713c07ad','1383737466408',NULL,NULL,1,'1','azhar','khiephlux','dehati','90076801','15','img/user_profile/default.png','i am a gamer',1,'2013-10-22 03:07:27','2013-11-29 12:30:22'),(2,NULL,'umer',NULL,NULL,'m.umer@test.com','25d55ad283aa400af464c76d713c07ad','1383737466408',NULL,NULL,1,'1','azhar','khiephlux','dehati','90076801','15','img/user_profile/default.png','I am not a full time gammer and not designer and asdk',1,'2013-10-22 03:09:00','2013-11-29 12:31:26'),(3,NULL,'salik',NULL,NULL,'salik.chughtai@ephlux.com','25d55ad283aa400af464c76d713c07ad','1383737466408',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-10-22 03:10:58',NULL),(4,NULL,'admin',NULL,NULL,'admin@jiggster.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-10-23 08:14:30',NULL),(7,NULL,'ks',NULL,NULL,'ll@hotmzil.com','bf083d4ab960620b645557217dd59a49','1382627580727',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-10-24 20:14:21',NULL),(8,NULL,'j',NULL,NULL,'hfjdjdj@hdjfk.fjck','8277e0910d750195b448797616e091ad','1382627913238',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-24 20:19:14',NULL),(9,NULL,'s',NULL,NULL,'jdhdi@hfj.xjj','8277e0910d750195b448797616e091ad','1382628014036',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-24 20:20:50',NULL),(10,NULL,'kkk',NULL,NULL,'jfjxkbdi@hjjk.cjl','acd2b09d39705a84bff035c18c9faea9','1382628809099',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-24 20:34:12',NULL),(11,NULL,'ab',NULL,NULL,'a@jigg.com','187ef4436122d1cc2f40dc2b92f0eba0','1382629044422',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-24 20:38:05',NULL),(12,NULL,'bb',NULL,NULL,'b@jigg.com','21ad0bd836b90d08f4cf640b4c298e7c','1382629141838',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-10-24 20:39:44',NULL),(13,NULL,'numair',NULL,NULL,'numair.qadir@gmail.com','25d55ad283aa400af464c76d713c07ad','1383053607218',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-29 18:34:39',NULL),(14,NULL,'sa',NULL,NULL,'sa@a.com','c12e01f2a13ff5587e1e9e4aedb8242d','1383053909910',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-29 18:39:23',NULL),(16,NULL,'Syed Muhammad Umair',NULL,NULL,'umairfriends11@gmail.com',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-31 03:34:16',NULL),(17,NULL,'askdf',NULL,NULL,'abc@gamil.com',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-31 04:43:34',NULL),(18,NULL,'Qa Qa',NULL,NULL,'sm.umair@ephlux.com','25d55ad283aa400af464c76d713c07ad',NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-31 04:51:54',NULL),(19,NULL,'Syed_M_Umair',NULL,NULL,'Syed_M_Umair',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-10-31 07:59:18',NULL),(20,NULL,'salik',NULL,NULL,'salik@salik.com','654872630fbd3428d615cdea3f1894be','1383643433128',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-05 21:26:31',NULL),(23,NULL,'sa',NULL,NULL,'sa@sa.com','c12e01f2a13ff5587e1e9e4aedb8242d','1383726608967',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-06 20:45:18',NULL),(25,NULL,'numair',NULL,NULL,'numair.qadir@ephlux.com','25d55ad283aa400af464c76d713c07ad','1383737466408',NULL,NULL,1,'1','123','khiephlux','dehati','90076801','15','img/user_profile/default.png','this is testvhfb',0,'2013-11-06 23:31:27','2013-11-29 12:33:53'),(26,NULL,'ammad',NULL,NULL,'ammad@apppli.com','3dbe00a167653a1aaee01d93e77e730e','500133878464',NULL,NULL,0,'1','0','None','None','0','0','img/user_profile/default.png','Lorem Ipsum is simply dummy text of the printing and typesetting industry.',0,'2013-11-07 02:05:20','2013-11-07 07:10:44'),(27,NULL,'sa\'s',NULL,NULL,'sad@sa.com','c12e01f2a13ff5587e1e9e4aedb8242d','1383726608967',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-07 03:40:58',NULL),(28,NULL,'Testid Jigg',NULL,NULL,'ephluxqa87@gmail.com',NULL,NULL,NULL,NULL,0,'1','0','-','-','0','0','img/user_profile/default.png',NULL,0,'2013-11-08 07:56:26','2013-11-27 15:56:12'),(29,NULL,'Syed Muhammad Umair',NULL,NULL,'syed.m.umair.71',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-11-09 04:53:22',NULL),(30,NULL,'Salik Chughtai',NULL,NULL,'Anti.Geniuns',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-09 05:11:32',NULL),(31,NULL,'salikChughtai',NULL,NULL,'salikChughtai',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-09 05:22:11',NULL),(32,NULL,'sa',NULL,NULL,'sa2@sa.com','c12e01f2a13ff5587e1e9e4aedb8242d','1384172486226',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-12 05:21:21',NULL),(33,NULL,'umair',NULL,NULL,'umair_friends11@hotmail.com','aafd4fb9adee64d2fce872647245e083','597848395196',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-12 06:01:52',NULL),(34,NULL,'umair',NULL,NULL,'sm_umair88@yahoo.com','aafd4fb9adee64d2fce872647245e083','597849350211',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-12 06:17:02',NULL),(38,NULL,'mashraf',NULL,NULL,'m.umer@ephlux.com','25d55ad283aa400af464c76d713c07ad','1384956602721',NULL,NULL,1,'1','0','-','-','0','0','img/user_profile/default.png',NULL,0,'2013-11-20 19:11:37','2013-11-27 21:53:51'),(52,NULL,'chhv',NULL,NULL,'salikaer5@salik.com','654872630fbd3428d615cdea3f1894be','469811571563',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-20 20:15:41',NULL),(53,NULL,'chhv',NULL,NULL,'salik12@salik.com','654872630fbd3428d615cdea3f1894be','469811571563',NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-20 20:17:52',NULL),(59,NULL,'Salik Chughtai',NULL,NULL,'salik456@gmail.com',NULL,NULL,NULL,NULL,0,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-20 22:42:31',NULL),(60,NULL,'salik chughtai',NULL,NULL,'salik.chughtai@hotmail.com','c12e01f2a13ff5587e1e9e4aedb8242d','375559046034',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-20 22:57:39',NULL),(61,NULL,'salik',NULL,NULL,'salik.123@salik.com','c12e01f2a13ff5587e1e9e4aedb8242d','312025758814',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,1,'2013-11-21 14:49:19',NULL),(62,NULL,'Noman noman',NULL,NULL,'noman@noman.com','c12e01f2a13ff5587e1e9e4aedb8242d','248953779554',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-21 14:49:50',NULL),(63,NULL,'samsung',NULL,NULL,'samsumg@samsung.com','c12e01f2a13ff5587e1e9e4aedb8242d','1385070962052',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-22 02:55:53',NULL),(64,NULL,'HTC drvicddde',NULL,NULL,'htc@htc.com','c12e01f2a13ff5587e1e9e4aedb8242d','1385071022711',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-22 02:57:19',NULL),(65,NULL,'test cycle 4',NULL,NULL,'testcycle4@gmaik.com','c12e01f2a13ff5587e1e9e4aedb8242d','312153985346',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-23 02:26:29',NULL),(66,NULL,'test cycle 3',NULL,NULL,'testcycle3@gmail.com','c12e01f2a13ff5587e1e9e4aedb8242d','343776527063',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-23 02:29:29',NULL),(67,NULL,'test 3',NULL,NULL,'test3@test.com','c12e01f2a13ff5587e1e9e4aedb8242d','312156135947',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-23 03:02:28',NULL),(68,NULL,'local 1',NULL,NULL,'local1@test.com','c12e01f2a13ff5587e1e9e4aedb8242d','1385542473687',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-27 13:54:27',NULL),(69,NULL,'test gmail',NULL,NULL,'test@gmail.com','c12e01f2a13ff5587e1e9e4aedb8242d','1385554011421',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-27 17:06:57',NULL),(70,NULL,'jiggster jiggster',NULL,NULL,'jiggsterjiggster@gmail.com','a45f8702297474d9a80ff36cb6a48200','1385569214446',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-27 21:20:24',NULL),(71,NULL,'siraj salim',NULL,NULL,'sirajsalim@gmail.com','798e00567616e4b41e4050d6f04e4232','1385569200067',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-27 21:21:03',NULL),(72,NULL,'salik 123',NULL,NULL,'salik123@gmail.com','c12e01f2a13ff5587e1e9e4aedb8242d','1385571014070',NULL,NULL,1,'1',NULL,NULL,NULL,NULL,NULL,'img/user_profile/default.png',NULL,0,'2013-11-27 21:50:02',NULL);

/*Table structure for table `users_chatlogs` */

DROP TABLE IF EXISTS `users_chatlogs`;

CREATE TABLE `users_chatlogs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `chatlog_id` int(11) NOT NULL,
  `sender_id` int(11) default NULL,
  `receiver_id` int(11) default NULL,
  `message` text,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status` enum('read','unread') default 'unread',
  PRIMARY KEY  (`id`),
  KEY `conversation_id` (`chatlog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users_chatlogs` */

insert  into `users_chatlogs`(`id`,`chatlog_id`,`sender_id`,`receiver_id`,`message`,`created`,`status`) values (1,1,60,1,'iaisiududu','2013-11-21 02:23:12','unread'),(2,2,25,1,'hi','2013-11-28 00:36:00','unread');

/*Table structure for table `users_consoles` */

DROP TABLE IF EXISTS `users_consoles`;

CREATE TABLE `users_consoles` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `console_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_user_reference_to_consoles` (`user_id`),
  KEY `FK_console_reference_to_users` (`console_id`),
  CONSTRAINT `FK_console_reference_to_users` FOREIGN KEY (`console_id`) REFERENCES `consoles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_reference_to_consoles` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=451 DEFAULT CHARSET=utf8;

/*Data for the table `users_consoles` */

insert  into `users_consoles`(`id`,`user_id`,`console_id`) values (21,4,1),(22,4,2),(23,4,3),(24,4,4),(25,4,5),(26,4,6),(27,4,7),(28,4,8),(29,4,9),(30,4,10),(33,25,3),(34,25,4),(35,1,1),(36,1,2),(37,1,3),(38,1,4),(39,1,5),(40,1,6),(41,1,7),(42,1,8),(43,1,9),(44,1,10),(48,8,2),(49,8,3),(50,8,4),(51,9,3),(54,60,2),(87,61,6),(88,62,6),(89,66,6),(90,65,6),(91,67,6),(92,68,6),(370,59,16),(371,59,1),(372,59,19),(373,59,18),(380,70,6),(381,71,6),(409,38,1),(410,38,2),(411,38,3),(412,38,4),(413,38,5),(414,38,6),(415,38,7),(416,38,8),(417,38,9),(418,38,10),(419,38,11),(420,38,12),(421,38,13),(422,38,14),(423,38,15),(424,38,17),(425,38,16),(426,38,19),(427,38,18),(428,72,1),(429,72,2),(430,72,3),(431,72,4),(432,72,5),(433,72,6),(434,72,7),(435,72,8),(436,72,9),(437,72,10),(438,72,11),(439,72,12),(440,72,13),(441,72,14),(442,72,15),(443,72,17),(444,72,16),(445,72,19),(446,72,18),(447,28,17),(448,28,19),(449,28,14),(450,28,15);

/*Table structure for table `users_deals` */

DROP TABLE IF EXISTS `users_deals`;

CREATE TABLE `users_deals` (
  `id` int(11) NOT NULL auto_increment,
  `deal_id` int(11) default NULL,
  `user_id` int(11) NOT NULL,
  `game` varchar(255) default NULL,
  `action_taken` enum('created','pending','accepted','counter','rejected','cancelled') NOT NULL default 'pending',
  `is_ensured` tinyint(1) NOT NULL default '0',
  `trade` enum('exchange','exchange and pay','exchange and charge','sell only','purchase only') default NULL,
  `send_confirm` tinyint(1) default '0',
  `receive_confirm` tinyint(1) default '0',
  `rating` int(11) default '0',
  PRIMARY KEY  (`id`),
  KEY `FK_deals_chat_log` (`deal_id`),
  CONSTRAINT `FK_deal_reference` FOREIGN KEY (`deal_id`) REFERENCES `deals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

/*Data for the table `users_deals` */

insert  into `users_deals`(`id`,`deal_id`,`user_id`,`game`,`action_taken`,`is_ensured`,`trade`,`send_confirm`,`receive_confirm`,`rating`) values (57,1,1,'2','accepted',1,'sell only',1,1,2),(58,1,2,'1,4','accepted',0,'purchase only',1,1,0),(59,2,1,'2','accepted',0,'purchase only',1,1,2),(60,2,3,'1,4','accepted',0,'sell only',1,1,0),(61,3,1,'1,8','accepted',1,'exchange',1,1,0),(62,3,4,'1,4','accepted',0,'exchange',1,1,0),(63,4,1,'1','accepted',0,'sell only',1,1,0),(64,4,5,'5,4','accepted',0,'purchase only',1,1,0),(65,5,1,'2','accepted',0,'sell only',1,1,0),(66,5,6,'5,4','accepted',0,'purchase only',1,1,0),(67,6,1,'1','accepted',0,'exchange',1,1,0),(68,6,7,'5,4','accepted',0,'exchange and pay',0,0,0),(69,7,65,'1','accepted',0,'exchange and charge',0,0,0),(70,7,66,'26','accepted',0,'exchange and pay',0,0,0),(71,8,1,'4','accepted',0,'exchange and pay',1,1,4),(72,8,68,'2','accepted',0,'exchange and charge',1,1,0),(73,9,72,'1','accepted',0,'sell only',0,0,0),(74,9,38,'26','accepted',0,'purchase only',0,0,0),(75,10,28,'1','accepted',0,'sell only',0,0,0),(76,10,68,'2','accepted',0,'purchase only',0,0,0),(77,11,28,'1','accepted',0,'sell only',0,0,0),(78,11,68,'2','accepted',0,'purchase only',0,0,0),(79,12,28,'2','accepted',0,'sell only',0,0,0),(80,12,68,'2','accepted',0,'purchase only',0,0,0),(81,13,72,'2','accepted',0,'exchange and pay',0,0,0),(82,13,38,'1','accepted',0,'exchange and charge',0,0,0),(83,14,72,'26','accepted',0,'exchange',0,0,0),(84,14,38,'2','accepted',0,'exchange',0,0,0),(85,15,68,'2','accepted',0,'exchange',0,0,0),(86,15,38,'1','accepted',0,'exchange',0,0,0),(87,16,68,'26','accepted',0,'exchange and charge',0,0,0),(88,16,38,'2','accepted',0,'exchange and pay',0,0,0),(89,17,25,'8','accepted',0,'exchange',1,1,0),(90,17,1,'25','accepted',0,'exchange',1,1,0),(91,18,68,'26,3','accepted',0,'exchange and pay',1,1,0),(92,18,38,'2','accepted',0,'exchange and charge',1,1,0);

/*Table structure for table `users_deals_insure` */

DROP TABLE IF EXISTS `users_deals_insure`;

CREATE TABLE `users_deals_insure` (
  `id` int(11) NOT NULL auto_increment,
  `deal_id` int(11) NOT NULL,
  `txn_id` varchar(50) character set latin1 NOT NULL default '',
  `receiver_email` varchar(50) character set latin1 NOT NULL,
  `receiver_id` varchar(50) character set latin1 NOT NULL,
  `residence_country` varchar(50) character set latin1 NOT NULL,
  `test_ipn` varchar(50) character set latin1 NOT NULL,
  `transaction_subject` varchar(1000) character set latin1 NOT NULL,
  `txn_type` varchar(50) character set latin1 NOT NULL,
  `payer_email` varchar(50) character set latin1 NOT NULL,
  `payer_id` varchar(50) character set latin1 NOT NULL,
  `payer_status` varchar(50) character set latin1 NOT NULL,
  `first_name` varchar(50) character set latin1 NOT NULL,
  `last_name` varchar(50) character set latin1 NOT NULL,
  `address_city` varchar(50) character set latin1 NOT NULL,
  `address_country` varchar(50) character set latin1 NOT NULL,
  `address_country_code` varchar(50) character set latin1 NOT NULL,
  `address_name` varchar(50) character set latin1 NOT NULL,
  `address_state` varchar(50) character set latin1 NOT NULL,
  `address_status` varchar(50) character set latin1 NOT NULL,
  `address_street` varchar(50) character set latin1 NOT NULL,
  `address_zip` varchar(50) character set latin1 NOT NULL,
  `handling_amount` double NOT NULL,
  `item_name` varchar(1000) character set latin1 NOT NULL,
  `item_number` varchar(50) character set latin1 NOT NULL,
  `mc_currency` varchar(50) character set latin1 NOT NULL,
  `mc_fee` double NOT NULL,
  `mc_gross` double NOT NULL,
  `payment_date` varchar(50) character set latin1 NOT NULL,
  `payment_fee` double NOT NULL,
  `payment_gross` double NOT NULL,
  `payment_status` varchar(50) character set latin1 NOT NULL,
  `payment_type` varchar(50) character set latin1 NOT NULL,
  `protection_eligibility` varchar(50) character set latin1 NOT NULL,
  `quantity` int(11) NOT NULL,
  `shipping` double NOT NULL,
  `tax` double NOT NULL,
  `notify_version` varchar(50) character set latin1 NOT NULL,
  `charset` varchar(50) character set latin1 NOT NULL,
  `verify_sign` varchar(50) character set latin1 NOT NULL,
  `normalized_payment_date` varchar(50) character set latin1 NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_deal_reference_id` (`deal_id`),
  CONSTRAINT `FK_deal_reference_id` FOREIGN KEY (`deal_id`) REFERENCES `deals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_deals_insure` */

/*Table structure for table `users_games` */

DROP TABLE IF EXISTS `users_games`;

CREATE TABLE `users_games` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `console_id` int(11) default NULL,
  `trade` enum('sell','trade','sell or trade','keep offline') character set latin1 default NULL,
  `condition` enum('new','mint','good','acceptable','poor') character set latin1 default NULL,
  `comes_with` enum('box + dvd','box + manual + dvd','dvd only','manual + dvd') default NULL,
  `amount` varchar(20) default '0',
  `comment` text character set latin1,
  `status` int(11) default '0',
  `created` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_users_games_gid` (`game_id`),
  KEY `FK_users_games_uid` (`user_id`),
  CONSTRAINT `FK_game_reference_to_users` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_reference_to_games` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

/*Data for the table `users_games` */

insert  into `users_games`(`id`,`user_id`,`game_id`,`console_id`,`trade`,`condition`,`comes_with`,`amount`,`comment`,`status`,`created`) values (47,1,2,5,'sell','mint','box + manual + dvd','0','',1,'2013-11-20 19:20:08'),(48,1,11,1,'sell or trade','new','dvd only','0','abc',1,'2013-11-20 19:20:35'),(49,1,25,1,'trade','new','box + dvd','0','',1,'2013-11-20 19:27:29'),(50,8,6,1,'sell or trade','new','dvd only','0','abc',1,'2013-11-20 19:32:57'),(51,8,18,33,'sell or trade','acceptable','dvd only','0','bfn',1,'2013-11-20 19:36:34'),(53,1,13,1,'trade','new','box + dvd','0','',1,'2013-11-20 21:41:39'),(54,59,6,1,'sell','new','box + dvd','0','',1,'2013-11-20 22:43:32'),(55,59,7,1,'sell or trade','acceptable','box + dvd','0',NULL,1,'2013-11-20 22:43:32'),(56,59,16,1,'sell or trade','new','box + dvd','0','',1,'2013-11-20 19:20:35'),(57,60,4,1,'sell','new','box + dvd','0','',1,'2013-11-20 22:52:38'),(58,62,6,6,'sell','good','box + dvd','0','',1,'2013-11-21 14:45:01'),(60,65,26,7,'sell','new','box + dvd','0','',1,'2013-11-23 02:21:46'),(61,67,26,6,'sell','new','box + dvd','0','',1,'2013-11-23 02:57:55'),(62,68,26,NULL,'sell','new','box + dvd','10.0','',0,'2013-11-27 14:04:38'),(63,28,2,NULL,'sell or trade','new','box + dvd','10','',0,'2013-11-27 14:22:29'),(64,68,3,NULL,'sell','new','box + dvd','100.0','',0,'2013-11-27 17:17:50'),(69,70,3,NULL,'sell','new','box + dvd','25.0','great',1,'2013-11-27 21:19:24'),(70,71,26,NULL,'sell or trade','new','box + dvd','10.0','',1,'2013-11-27 21:20:59'),(71,72,26,NULL,'sell or trade','new','box + dvd','10.0','',1,'2013-11-27 21:46:10'),(72,38,2,NULL,'sell','new','box + dvd','25.0','',0,'2013-11-27 21:55:09'),(73,72,6,NULL,'sell','new','box + dvd','1.99','',1,'2013-11-28 00:33:41'),(75,25,6,NULL,'sell','poor','dvd only','88','',1,'2013-11-28 21:23:11'),(76,25,23,NULL,'trade','poor','dvd only','0','p',1,'2013-11-28 21:24:03'),(77,25,2,NULL,'trade','new','box + dvd','0','',1,'2013-11-29 17:23:08'),(78,25,14,NULL,'sell or trade','new','box + dvd','96','',1,'2013-11-29 17:24:16');

/*Table structure for table `users_genres` */

DROP TABLE IF EXISTS `users_genres`;

CREATE TABLE `users_genres` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `genre_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_genre_reference_to_users` (`genre_id`),
  KEY `FK_user_reference_to_genres` (`user_id`),
  CONSTRAINT `FK_genre_reference_to_users` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_reference_to_genres` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

/*Data for the table `users_genres` */

insert  into `users_genres`(`id`,`user_id`,`genre_id`) values (26,4,1),(27,4,2),(28,4,3),(29,4,4),(30,4,5),(31,4,6),(32,4,7),(33,4,8),(34,4,9),(35,4,10),(36,25,3),(37,25,6),(38,1,1),(39,1,2),(40,1,3),(41,1,4),(42,1,5),(43,1,6),(44,1,7),(45,1,8),(46,1,9),(47,1,10),(50,8,3),(51,8,4),(52,9,3),(56,60,4),(57,60,7),(79,62,6),(80,61,4),(82,66,5),(83,65,5),(84,67,7),(85,68,3),(102,59,10),(103,59,2),(104,70,8),(105,70,5),(106,71,8),(107,71,5),(136,72,1),(137,72,2),(138,72,3),(139,72,4),(140,72,5),(141,72,6),(142,72,7),(143,72,8),(144,72,9),(145,72,10),(146,38,1),(147,38,2),(148,38,3),(149,38,4),(150,38,5),(151,38,6),(152,38,7),(153,38,8),(154,28,4);

/*Table structure for table `users_social_accounts` */

DROP TABLE IF EXISTS `users_social_accounts`;

CREATE TABLE `users_social_accounts` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) character set latin1 default NULL,
  `image_url` varchar(255) character set latin1 default NULL,
  `type_id` enum('facebook','twitter','google_plus') character set latin1 NOT NULL,
  `access_token` varchar(255) character set latin1 NOT NULL,
  `access_token_secret` varchar(255) character set latin1 default NULL,
  `is_valid_token` tinyint(1) NOT NULL default '1',
  `screen_name` varchar(255) character set latin1 NOT NULL,
  `extra` mediumtext character set latin1 COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') character set latin1 NOT NULL default 'active',
  `ip_address` int(10) unsigned default NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `modified` timestamp NULL default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_user_SA_reference` (`user_id`),
  CONSTRAINT `FK_user_SA_reference` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `users_social_accounts` */

insert  into `users_social_accounts`(`id`,`user_id`,`link_id`,`email_address`,`image_url`,`type_id`,`access_token`,`access_token_secret`,`is_valid_token`,`screen_name`,`extra`,`status`,`ip_address`,`created`,`modified`) values (1,16,9223372036854775807,'umairfriends11@gmail.com','{\"url\":\"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50\"}','twitter','ya29.AHES6ZRK9IyL5aHzOqk_feoe6QqCFDkMPhqOC4q_sXUVVsr59Gfeex0L-w','',1,'Syed Muhammad Umair',NULL,'active',192175,'2013-10-31 03:34:16',NULL),(2,17,1223,'abc@gamil.com','sadkfas','facebook','sdfasdkfj;askf','',1,'askdf',NULL,'active',1270,'2013-10-31 04:43:34',NULL),(3,18,100005564561605,'sm.umair@ephlux.com','http://graph.facebook.com/100005564561605/picture','facebook','CAADG9VZBBuX4BAF1SzjDrxREGJZAytDQPg3HN2bMzUwguB8AUD1Nf2w3uBoLxEfaQ2ZC4Pa9mBcIEuciJMEqnbbI4eJ31OnyZAMwQSYjufcZCrQAzb5zYDS0IJMKs47BF8FTNRzs9DRMJPs8xs7U0w0KjPRkTgWpuN5PBVrKodet48M4xT0MU','CAADG9VZBBuX4BAF1SzjDrxREGJZAytDQPg3HN2bMzUwguB8AUD1Nf2w3uBoLxEfaQ2ZC4Pa9mBcIEuciJMEqnbbI4eJ31OnyZAMwQSYjufcZCrQAzb5zYDS0IJMKs47BF8FTNRzs9DRMJPs8xs7U0w0KjPRkTgWpuN5PBVrKodet48M4xT0MU',1,'Qa Qa',NULL,'active',192175,'2013-10-31 04:51:54',NULL),(4,19,1239784807,'Syed_M_Umair','http://pbs.twimg.com/profile_images/3345198220/a3e2bd0bde74b87faf6f7ea844495252_normal.jpeg','twitter','1239784807-IuPu95Qe2uIq3J2nOBNy0W2kedeZ11hdFjw4ZX3','WcExNSSPMIsfjShbikh4wdFl72Z0s27brMmLGTWNrsg96',1,'Syed_M_Umair',NULL,'active',192175,'2013-10-31 07:59:18',NULL),(5,28,9223372036854775807,'ephluxqa87@gmail.com','{\"url\":\"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50\"}','google_plus','ya29.AHES6ZQxEJN3VHX2giWhikrHxcgvsA_IHDW9BNWCLwsr1mwauxgY','ya29.AHES6ZQxEJN3VHX2giWhikrHxcgvsA_IHDW9BNWCLwsr1mwauxgY',1,'Testid Jigg',NULL,'active',192175,'2013-11-08 07:56:26',NULL),(6,29,728449081,'syed.m.umair.71','http://graph.facebook.com/728449081/picture','facebook','CAADG9VZBBuX4BAOfKsD6NB1z0Itq3oXQMpJQgHYgV5ruIHZCtk1ZAWAtaqx5fpr2MvYahbA8llZAHLwUIGnuZAZBf7wTHGnuNWAOjZAEelHl5QV7ZCKFrSCdhMhGZBCfJbN5Mn2ZCuqoVA8WTkzXBjuXIuhsTqRqxpVTKEfNe1BT3Blq4rNVwIQBGud5vwd6cKIs4ZD','CAADG9VZBBuX4BAOfKsD6NB1z0Itq3oXQMpJQgHYgV5ruIHZCtk1ZAWAtaqx5fpr2MvYahbA8llZAHLwUIGnuZAZBf7wTHGnuNWAOjZAEelHl5QV7ZCKFrSCdhMhGZBCfJbN5Mn2ZCuqoVA8WTkzXBjuXIuhsTqRqxpVTKEfNe1BT3Blq4rNVwIQBGud5vwd6cKIs4ZD',1,'Syed Muhammad Umair',NULL,'active',192175,'2013-11-09 04:53:22',NULL),(7,30,502539580,'Anti.Geniuns','http://graph.facebook.com/502539580/picture','facebook','CAADG9VZBBuX4BALQT1D69Am5gSi8hew4PvN6ZC7ZB1ZBSH7abJwniWzjnC7JVLkLc7tL5I5GrV8CCsZCXVpeZC5NDCBIchX161ZCO9eQPTZBS42EP6hbs7EfZATqCb9cCMZA9Iit9iWkmyhGqKFZC42TNkTVEMiG7ONfy38hh0ZCWyyfhZB6mqmyEmHN5','CAADG9VZBBuX4BALQT1D69Am5gSi8hew4PvN6ZC7ZB1ZBSH7abJwniWzjnC7JVLkLc7tL5I5GrV8CCsZCXVpeZC5NDCBIchX161ZCO9eQPTZBS42EP6hbs7EfZATqCb9cCMZA9Iit9iWkmyhGqKFZC42TNkTVEMiG7ONfy38hh0ZCWyyfhZB6mqmyEmHN5',1,'Salik Chughtai',NULL,'active',192175,'2013-11-09 05:11:32',NULL),(8,31,264056222,'salikChughtai','http://pbs.twimg.com/profile_images/2815135735/032d6a6fb486f6677433762152db578e_normal.jpeg','twitter','264056222-2XhbRc6OJP9h6iOdrwoqoiLT6AjdrUPBLOOGNvSI','f30JA39wObyyknPFitiITbzSwK8EchlnIWyixvAF86F5M',1,'salikChughtai',NULL,'active',192175,'2013-11-09 05:22:12',NULL),(9,59,9223372036854775807,'salik456@gmail.com','{\"url\":\"https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50\"}','google_plus','ya29.1.AADtN_VeWmQuxCQqecJ4vA_4mkFmN9jRTzd4syTP69g4QZbWDh3Oy_L-0cmaNkoEe4OKaA','ya29.1.AADtN_VeWmQuxCQqecJ4vA_4mkFmN9jRTzd4syTP69g4QZbWDh3Oy_L-0cmaNkoEe4OKaA',1,'Salik Chughtai',NULL,'active',192175,'2013-11-20 22:42:31',NULL);

/*Table structure for table `users_wishlists` */

DROP TABLE IF EXISTS `users_wishlists`;

CREATE TABLE `users_wishlists` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `trade` enum('buy','trade','buy or trade') character set latin1 default NULL,
  `status` int(11) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_users_games_gid` (`game_id`),
  KEY `FK_users_games_uid` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `users_wishlists` */

insert  into `users_wishlists`(`id`,`user_id`,`game_id`,`trade`,`status`,`created`) values (1,1,4,'buy or trade',1,'2013-11-20 19:21:53'),(2,1,9,'',1,'2013-11-20 19:22:03'),(3,1,6,'buy or trade',1,'2013-11-20 19:23:15'),(4,1,8,'buy or trade',1,'2013-11-20 19:23:49'),(5,8,11,'trade',1,'2013-11-20 19:37:34'),(6,6,2,'trade',1,'2013-11-20 19:42:06'),(7,6,8,'',1,'2013-11-20 19:42:33'),(8,6,9,'trade',1,'2013-11-20 19:43:18'),(9,6,18,'buy or trade',1,'2013-11-20 19:48:19'),(13,1,3,'trade',1,'2013-11-20 20:44:32'),(14,8,8,'',1,'2013-11-20 22:18:25'),(16,1,6,'buy or trade',1,'2013-11-20 22:23:46'),(17,5,8,'trade',1,'2013-11-20 22:23:46'),(18,5,9,'trade',1,'2013-11-20 22:23:47'),(19,59,4,'buy or trade',1,'2013-11-20 22:49:05'),(20,60,16,'buy or trade',1,'2013-11-20 22:53:37'),(21,60,5,'trade',1,'2013-11-21 02:51:31'),(22,61,6,'buy',1,'2013-11-21 14:45:39'),(23,62,2,'trade',1,'2013-11-21 14:46:20'),(24,65,6,'buy or trade',1,'2013-11-23 02:22:54'),(25,65,10,'buy or trade',1,'2013-11-23 02:22:54'),(26,66,26,'buy',1,'2013-11-23 02:24:22'),(27,68,2,'buy',1,'2013-11-27 14:05:57'),(28,87,26,'buy',1,'2013-11-27 14:19:53'),(29,28,1,'buy',0,'2013-11-27 16:42:30'),(30,59,10,'buy or trade',1,'2013-11-27 21:09:23'),(31,59,2,'buy or trade',1,'2013-11-27 21:10:30'),(32,70,26,'buy',1,'2013-11-27 21:18:21'),(33,71,3,'buy or trade',1,'2013-11-27 21:24:30'),(34,72,2,'buy or trade',1,'2013-11-27 21:47:16'),(35,38,26,'buy or trade',1,'2013-11-27 21:54:37'),(36,72,10,'buy',1,'2013-11-28 20:19:09'),(37,72,15,'buy',1,'2013-11-28 20:19:09'),(39,72,8,'buy',1,'2013-11-28 21:24:16'),(40,25,3,'trade',1,'2013-11-28 21:24:34'),(41,25,26,'buy',0,'2013-11-28 21:25:59'),(42,25,4,'buy',1,'2013-11-29 17:21:33');

/*Table structure for table `wallpapers` */

DROP TABLE IF EXISTS `wallpapers`;

CREATE TABLE `wallpapers` (
  `id` int(11) NOT NULL auto_increment,
  `wallpaper` varchar(255) character set latin1 default NULL,
  `status` int(1) default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `wallpapers` */

/* Procedure structure for procedure `add_page` */

/*!50003 DROP PROCEDURE IF EXISTS  `add_page` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `add_page`(  IN `page_title` VARCHAR(50),
				      IN `page_description` text,
				      IN `page_status` int(11),
				      OUT `status` INT)
BEGIN
    
	INSERT INTO pages
					(
						pages.title                  ,
						pages.desc                   , 
						pages.status
					)
	VALUES 
					( 
						page_title                   ,
						page_description             ,
						page_status
								     
					) ;
SET `status` =  LAST_INSERT_ID();
    END */$$
DELIMITER ;

/* Procedure structure for procedure `awaiting_rating` */

/*!50003 DROP PROCEDURE IF EXISTS  `awaiting_rating` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `awaiting_rating`(IN `id_user` INT(11),
					   IN `trade` VARCHAR(30),
					   IN `id_deal` INT(11))
BEGIN
    
	IF trade = 'buy' THEN
	
		SELECT users_deals.deal_id, users_deals.game, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, users.id, users.nickname, users.image_url, users.trusted_user, deals.offered_by
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		LEFT JOIN users ON users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 0 AND
		      users_deals.user_id <> id_user AND users_deals.trade = 'sell only' AND users_deals.action_taken = 'accepted' AND
		      users_deals.`send_confirm` = 1 AND users_deals.`receive_confirm` = 1;
	
	END IF;	
	
	IF trade = 'sell' THEN
	
		SELECT users_deals.deal_id, users_deals.game, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, deals.offered_by
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 0 AND
		      users_deals.user_id = id_user AND users_deals.trade = 'sell only' AND users_deals.action_taken = 'accepted' AND
		      users_deals.`send_confirm` = 1 AND users_deals.`receive_confirm` = 1;
	
	END IF;
	
	IF trade = 'exchange' THEN
	
		SELECT users_deals.deal_id, users_deals.game,users_deals.trade, deals.offered_by, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, users.id, users.nickname, users.image_url, 
		users.trusted_user
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		LEFT JOIN users ON users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 0 AND
		(users_deals.trade = 'exchange' OR users_deals.trade = 'exchange and pay' OR users_deals.trade = 'exchange and charge')
		AND users_deals.action_taken = 'accepted' AND users_deals.`send_confirm` = 1 AND users_deals.`receive_confirm` = 1;
	
	END IF;	
	
	IF `id_deal` IS NOT NULL THEN
	
		SELECT users.id, users.nickname, users.image_url, users.trusted_user
		FROM users
		LEFT JOIN users_deals ON users.id = users_deals.user_id
		WHERE users_deals.deal_id = id_deal AND users_deals.user_id <> id_user;
	
	
	END IF;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `check_api_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `check_api_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `check_api_id`(IN `api_id` INT(11), IN `table_name` VARCHAR(30))
BEGIN
		
	SET @QUERY = CONCAT('SELECT api FROM ', table_name,' WHERE api = ', api_id);
	PREPARE sql_query FROM @QUERY;
	EXECUTE sql_query;
	DEALLOCATE PREPARE sql_query;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `check_game_existance_in_wishlist_or_have` */

/*!50003 DROP PROCEDURE IF EXISTS  `check_game_existance_in_wishlist_or_have` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `check_game_existance_in_wishlist_or_have`( IN `id_user` INT(11),
									        in `id_game` int(11)										
									       )
BEGIN
    
         
          
        
	SELECT count(users_games.game_id) as UserGame, (SELECT COUNT(users_wishlists.game_id) from users_wishlists
	where users_wishlists.game_id = id_game  AND users_wishlists.user_id = id_user) AS UserWish
	FROM users_games
	WHERE users_games.game_id = id_game  and users_games.user_id = id_user; 
			
			
			
		  
           
		
		
		
						
			 
           
       
         
         
         
    END */$$
DELIMITER ;

/* Procedure structure for procedure `check_genre_or_console` */

/*!50003 DROP PROCEDURE IF EXISTS  `check_genre_or_console` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `check_genre_or_console`(IN `genre_name` VARCHAR(30),
							     IN `console_name` VARCHAR(30))
BEGIN
	IF genre_name IS NOT NULL THEN
		SELECT genre FROM genres WHERE genres.genre = genre_name;
	END IF;
	
	
	IF console_name IS NOT NULL THEN
		SELECT console FROM consoles WHERE consoles.console = console_name;
	END IF;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `console_cron` */

/*!50003 DROP PROCEDURE IF EXISTS  `console_cron` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `console_cron`(IN `console` VARCHAR(255))
BEGIN
		
	INSERT INTO `consoles` ( console, `status`, created ) 
		VALUE ( console, 1, NOW() );
		
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `count_user_have_games` */

/*!50003 DROP PROCEDURE IF EXISTS  `count_user_have_games` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `count_user_have_games`(in `id_user` int(11))
BEGIN
    
	select count(users_games.`game_id`) 'GamesOwned'
	from users_games
	where users_games.`user_id` = id_user;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `count_user_unread_deal_messages` */

/*!50003 DROP PROCEDURE IF EXISTS  `count_user_unread_deal_messages` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `count_user_unread_deal_messages`(IN `id_receiver` INT(11))
BEGIN
    
	SELECT COUNT(deals_chatlogs.status) AS UnreadMessageCount
	FROM deals_chatlogs
	WHERE deals_chatlogs.id_receiver= id_receiver AND deals_chatlogs.status='unread';
    END */$$
DELIMITER ;

/* Procedure structure for procedure `count_user_unread_messages` */

/*!50003 DROP PROCEDURE IF EXISTS  `count_user_unread_messages` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `count_user_unread_messages`(IN `id_receiver` INT(11))
BEGIN
    
	SELECT COUNT(users_chatlogs.message) as UnreadMessageCount
	FROM users_chatlogs
	WHERE users_chatlogs.receiver_id = id_receiver AND users_chatlogs.status='unread';
    END */$$
DELIMITER ;

/* Procedure structure for procedure `create_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_group`(IN name VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into groups(
groups.name,
groups.ip_address,
groups.created,
groups.modified
) values (
name,
ip_address,
created,
created 
);
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_user` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_user` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_user`(IN `first_name` VARCHAR(255), IN `last_name` VARCHAR(255), IN `username` VARCHAR(50), IN `password` VARCHAR(255), IN `group_id` INT(11), IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
INSERT INTO users
(
users.first_name                  , 
users.last_name                   , 
users.username                    , 
users.password                    ,
users.group_id                    ,
users.ip_address                  ,
users.created	             ,
users.modified
)
VALUES 
( 
first_name                       , 
last_name                        , 
username                         ,  
password                         ,
group_id                         ,
ip_address                       ,
created                          ,
created
) ; 
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `deals_for_you_buying` */

/*!50003 DROP PROCEDURE IF EXISTS  `deals_for_you_buying` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `deals_for_you_buying`(IN `id_user` INT(11),
						IN `game_count` INT(11), 
						OUT `status1` int(11),
						out `status2` int(11)
						)
BEGIN
    
	DECLARE user_ids VARCHAR(500);
	DECLARE game_ids VARCHAR(500);
	
	IF game_count = 0 THEN
	
		SELECT GROUP_CONCAT(user_id) INTO @user_ids
		FROM users_consoles
		WHERE users_consoles.user_id <> id_user AND users_consoles.console_id IN
		
		(SELECT users_consoles.console_id
		FROM users_consoles, consoles
		WHERE users_consoles.console_id = consoles.id AND users_consoles.user_id = id_user);
		
		
		SELECT GROUP_CONCAT(game_id) INTO @game_ids
		FROM users_wishlists
		WHERE (users_wishlists.trade = 'buy' OR users_wishlists.trade = 'buy or trade') AND users_wishlists.user_id = id_user;
		
 		IF @user_ids AND @game_ids is not NULL THEN 
		
			SET @QUERY = CONCAT('SELECT users_games.user_id, users_games.game_id, users_games.amount, users.nickname, users.image_url, users.created, games.title, games.image_url, games.console_id, games.release_date, users_games.condition, users_games.comes_with
					     FROM users_games
					     left join games on users_games.game_id = games.id
					     left join users on users_games.user_id = users.id
					     WHERE (users_games.trade = "sell" or users_games.trade = "sell or trade") 
					     AND users_games.user_id IN (',@user_ids,') 
					     AND users_games.game_id IN (',@game_ids,') 
					     order by users_games.user_id;');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
			
			SET `status1` = 1;
			
   		ELSE
  		
   			SET `status1` = 0;
 		
		END IF;
	
	ELSE 
	
		SELECT GROUP_CONCAT(user_id) INTO @user_ids
		FROM users_genres
		WHERE users_genres.user_id <> id_user AND users_genres.genre_id IN
		
		(SELECT users_genres.genre_id
		FROM users_genres, genres
		WHERE users_genres.genre_id = genres.id AND users_genres.user_id = id_user);
		
		
		IF @user_ids AND @game_ids IS NOT NULL THEN 
		
			SET @QUERY = CONCAT('SELECT users_games.user_id, users_games.game_id, users_games.amount, users.nickname, users.image_url, users.created, games.title, games.image_url, games.console_id, games.release_date, users_games.condition, users_games.comes_with
					     FROM users_games
					     left join games on users_games.game_id = games.id
					     left join users on users_games.user_id = users.id
					     WHERE (users_games.trade = "sell" or users_games.trade = "sell or trade") 
					     AND users_games.user_id IN (',@user_ids,')
					     order by users_games.user_id;');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
			
			SET `status2` = 1;
			
		ELSE
		
			SET `status2` = 0;
		
		END IF;		
		
		
		
	END IF;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `deals_for_you_selling` */

/*!50003 DROP PROCEDURE IF EXISTS  `deals_for_you_selling` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `deals_for_you_selling`(in `id_user` int(11), in `game_count` int(11), 
								out `status` int(11))
BEGIN
	declare user_ids varchar(500);
	DECLARE game_ids VARCHAR(500); 
	
    
	if game_count = 0 then
	
		SELECT GROUP_CONCAT(user_id) into @user_ids
		FROM users_consoles
		WHERE users_consoles.user_id <> id_user AND users_consoles.console_id IN
		
		(SELECT users_consoles.console_id
		FROM users_consoles, consoles
		WHERE users_consoles.console_id = consoles.id AND users_consoles.user_id = id_user);
		
		
		SELECT group_concat(game_id) into @game_ids
		FROM users_games
		WHERE (users_games.trade = 'sell' or users_games.trade = 'sell or trade') AND users_games.user_id = id_user;
		
		IF @user_ids AND @game_ids IS NOT NULL THEN 
		
			SET @QUERY = CONCAT('SELECT users_wishlists.user_id, users_wishlists.game_id, users.nickname, users.created, users.image_url, games.title, games.image_url, games.console_id, games.release_date, users_games.condition, users_games.amount, users_games.comes_with
					     FROM users_wishlists
					     left join users on users_wishlists.user_id = users.id
					     left join games on users_wishlists.game_id = games.id
					     left join users_games on users_wishlists.game_id = users_games.game_id
					     WHERE (users_wishlists.trade = "buy" or users_wishlists.trade = "buy or trade") 
					     AND users_wishlists.user_id IN (',@user_ids,')
					     AND users_wishlists.game_id IN (',@game_ids,') AND users_games.user_id = ',id_user,' 
					     order by users_wishlists.user_id;');
					     
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
			
			set `status` = 1;
			
		else
		
			set `status` = 0;
		
		end if;
	
	else 
	
		SELECT GROUP_CONCAT(user_id) INTO @user_ids
		FROM users_genres
		WHERE users_genres.user_id <> id_user AND users_genres.genre_id IN
		
		(SELECT users_genres.genre_id
		FROM users_genres, genres
		WHERE users_genres.genre_id = genres.id AND users_genres.user_id = id_user);
		
		SELECT GROUP_CONCAT(game_id) INTO @game_ids
		FROM users_games
		WHERE (users_games.trade = 'sell' OR users_games.trade = 'sell or trade') AND users_games.user_id = id_user;
		
		IF @user_ids AND @game_ids IS NOT NULL THEN
		
			SET @QUERY = CONCAT('SELECT users_wishlists.user_id, users_wishlists.game_id, users.nickname,users.image_url, users.created, games.title, games.image_url, games.console_id, games.release_date, users_games.condition, users_games.amount, users_games.comes_with
					     FROM users_wishlists
					     left join users on users_wishlists.user_id = users.id
					     left join games on users_wishlists.game_id = games.id
					     left join users_games on users_wishlists.game_id = users_games.game_id
					     WHERE (users_wishlists.trade = "buy" or users_wishlists.trade = "buy or trade") 
					     AND users_wishlists.user_id IN (',@user_ids,')
					     AND users_wishlists.game_id IN (',@game_ids,') AND users_games.user_id = ',id_user,'
					     order by users_wishlists.user_id;');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
			
			SET `status` = 1;
			
		ELSE
		
			SET `status` = 0;
		
		END IF;
	
	
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `deals_for_you_trading` */

/*!50003 DROP PROCEDURE IF EXISTS  `deals_for_you_trading` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `deals_for_you_trading`(IN `user_id` INT(11), IN `count` INT(11))
BEGIN
DECLARE uids VARCHAR(255);
	SELECT GROUP_CONCAT(user_ids) INTO uids FROM
		( SELECT DISTINCT(other_user.user_id) user_ids FROM users_consoles login_user JOIN users_consoles other_user
			ON login_user.console_id = other_user.console_id 
			WHERE login_user.user_id = user_id AND other_user.user_id <> user_id ) T1
	INNER JOIN
		( SELECT DISTINCT(other_user.user_id) user_ids FROM users_genres login_user JOIN users_genres other_user
			ON login_user.genre_id = other_user.genre_id 
			WHERE login_user.user_id = user_id AND other_user.user_id <> user_id ) T2
	USING (user_ids);
	
	IF `count`= 0 THEN
	
		if uids is not null then
	
		
			SET @QUERY = CONCAT('SELECT users_games.user_id login_user, GROUP_CONCAT(users_games.game_id) login_user_game_ids,
						GROUP_CONCAT(users_games.amount) login_user_amount,
						GROUP_CONCAT(users_games.condition) login_user_game_condition, GROUP_CONCAT(users_games.comes_with) login_user_game_comes_with, users_wishlists.user_id other_user 
						FROM users_wishlists LEFT JOIN users_games 
						ON users_wishlists.game_id = users_games.game_id WHERE users_games.user_id = ',user_id,' 
						AND (users_games.trade = "trade" OR users_games.trade = "sell or trade") AND 
						users_wishlists.user_id IN (',uids,') AND (users_wishlists.trade = "trade" OR users_wishlists.trade = "buy or trade")
						GROUP BY other_user');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
		
		end if;
	
	ELSE	
	
		IF uids IS NOT NULL THEN
		
			SET @QUERY = CONCAT('SELECT users_games.user_id other_user, GROUP_CONCAT(users_games.game_id) other_user_game_ids, GROUP_CONCAT(users_games.comes_with) other_user_game_comes_with, 
						GROUP_CONCAT(users_games.amount) other_user_amount,
						GROUP_CONCAT(users_games.condition) other_user_game_condition, users_wishlists.user_id login_user 
						FROM users_games LEFT JOIN users_wishlists
						ON users_games.game_id = users_wishlists.game_id WHERE 
						users_wishlists.user_id = ',user_id,' AND (users_wishlists.trade = "trade" OR users_wishlists.trade = "buy or trade")
						AND users_games.user_id IN (',uids,') AND (users_games.trade = "trade" OR users_games.trade = "sell or trade")
						GROUP BY other_user');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;
		
		end if;
		
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `delete_game_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `delete_game_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `delete_game_record`(IN `game_id` int(11))
BEGIN
    
	DELETE FROM games
	WHERE games.id = game_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `delete_tmp_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `delete_tmp_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `delete_tmp_record`(IN `id` INT(5),OUT `status` INT)
    NO SQL
BEGIN 
DELETE FROM tmps
WHERE  tmps.id = id ; 
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `delete_wish_or_have_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `delete_wish_or_have_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `delete_wish_or_have_game`(IN `id_user` INT(11),
							       IN `id_game` INT(11),
						               in `table_name` varchar(30))
BEGIN
    
	IF table_name = 'my_game' THEN
    
		UPDATE users_games
		SET users_games.status = 0
		WHERE users_games.user_id = id_user AND users_games.game_id = id_game;
	
	END IF;
	
	IF table_name = 'wishlist' THEN
	
		update users_wishlists
		set users_wishlists.status = 0
		where users_wishlists.user_id = id_user and users_wishlists.game_id = id_game;
	
	
	end if;
END */$$
DELIMITER ;

/* Procedure structure for procedure `game_cron` */

/*!50003 DROP PROCEDURE IF EXISTS  `game_cron` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `game_cron`(IN api INT(11), IN `title` VARCHAR(255), IN `desc` VARCHAR(255), IN `console_id` INT(11), 
						IN `genre_id` INT(11), IN `publisher` VARCHAR(255), IN `developers` VARCHAR(255),  
						IN `barcode` VARCHAR(255), IN `image_url` VARCHAR(255), 
						IN `screenshot` VARCHAR(255), IN `release_date` VARCHAR(255) )
BEGIN
		
	INSERT INTO `games` ( api, title, `desc`, console_id, genre_id, publisher, developers, barcode, rating, image_url, screenshot, `status`, release_date, created ) 
		VALUE ( api, title, `desc`, console_id, genre_id, publisher, developers, barcode, 10, image_url, screenshot, 1, release_date, NOW() );
		
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `game_screenshot` */

/*!50003 DROP PROCEDURE IF EXISTS  `game_screenshot` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `game_screenshot`(in `id_game` int(11))
BEGIN
    
	select games.screenshot
	from games
	where games.id = id_game;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `genres` */

/*!50003 DROP PROCEDURE IF EXISTS  `genres` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `genres`(in `api_id` int(11), in `table_name` varchar(30))
BEGIN
	
	SELECT genres.api 
	FROM table_name
	WHERE genres.api = api_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `genre_cron` */

/*!50003 DROP PROCEDURE IF EXISTS  `genre_cron` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `genre_cron`(IN `genre` VARCHAR(255))
BEGIN	 
	INSERT INTO `genres` ( genre, `status`, created ) 
		VALUE ( genre, 1, NOW() );
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_arosacos_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_arosacos_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_arosacos_by_group_id`(IN group_id INT(10))
BEGIN
SELECT 	ArosAco.id,ArosAco.aco_id
FROM groups AS `Group`
LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
LEFT JOIN `aros_acos` AS ArosAco ON ArosAco.aro_id = Aro.id
WHERE Group.id = group_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_console` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_console` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_console`()
BEGIN
	SELECT *
	FROM consoles AS `Console`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_games` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_games` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_games`(IN `deals` VARCHAR(30))
BEGIN	
	
	
	SET @q = CONCAT("SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.console_id, Game.image_url, Game.status From games AS Game WHERE Game.status = 1 and Game.release_date < NOW()");
		
	IF deals IS NOT NULL THEN
	SET @q = CONCAT(@q, " AND where Game.ongoing_deals <> 0");
	END IF;
	
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	DEALLOCATE PREPARE stmt;
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_games_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_games_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_games_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
SELECT games.id, games.title, games.console_id, games.selling, games.buying, games.rating, games.image_url, games.trading, games.release_date,
       games.publisher, games.developers, games.ongoing_deals, users_games.trade, users_games.condition, users_games.amount, 
       users_games.comes_with, genres.genre   
FROM games 
left JOIN users_games ON games.id = users_games.game_id 
left join genres on games.genre_id = genres.id
WHERE users_games.user_id = user_id AND games.status = 1 and users_games.status = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_games_web` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_games_web` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_games_web`()
BEGIN
    
	SET @q = CONCAT("SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.image_url, Game.status From games AS Game");	
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	DEALLOCATE PREPARE stmt;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_genre` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_genre` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_genre`()
BEGIN
    
    SELECT *
    FROM genres AS `Genre`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_pages` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_pages` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_pages`()
BEGIN
    
	select id, title, `desc`, `status`
	from pages;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_permissions` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_permissions` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_permissions`()
BEGIN
SELECT 
Child.id,Child.alias,
AuthActionMap.description,AuthActionMap.id,
Feature.id,Feature.description,
Parent.id,Parent.alias
FROM `acos` AS Child
LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.aco_id = Child.id
LEFT JOIN features AS Feature ON Feature.id = AuthActionMap.feature_id AND Feature.is_active = 1
WHERE `Parent`.`parent_id` IS NOT NULL;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_users` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_users` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_users`(in `id_user` int(11))
BEGIN
	IF id_user IS NULL THEN
    
	select users.id, users.`nickname`, users.`email`, users.`dob`, users.`gaming_id`, users.`shipping_address`, users.`location`, 
		users.`paypal_id`, users.`contact`, users.`image_url`, users.`profile_text`, users.`trusted_user`, users.`status`, users.`created`
	from users;
	
	end if;
	
	if id_user is not null then
	
		SELECT users.id, users.`nickname`, users.`email`, users.`dob`, users.`gaming_id`, users.`shipping_address`, users.`location`, 
		users.`paypal_id`, users.`contact`, users.`image_url`, users.`profile_text`, users.`trusted_user`, users.`status`, users.`created`
		FROM users
		where users.`id` = id_user;
	
	
	end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_feature_by_feature_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_feature_by_feature_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_feature_by_feature_id`(IN feature_id INT(11))
BEGIN
select 	Feature.id, Feature.description, Feature.created
from `features` AS Feature
WHERE Feature.id = feature_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_feature_details_by_feature_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_feature_details_by_feature_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_feature_details_by_feature_id`(IN feature_id INT(11))
BEGIN
SELECT 	
Feature.id,Feature.description,Feature.created,
AuthActionMap.id,AuthActionMap.description,AuthActionMap.created
FROM features AS Feature
LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.feature_id = Feature.id
WHERE Feature.id = feature_id AND Feature.is_active = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_game_detail_by_barcode` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_game_detail_by_barcode` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_game_detail_by_barcode`(IN `game_barcode` varchar(255))
BEGIN
	SELECT Game.id, Game.title, Game.buying, Game.image_url, Game.rating, Game.trading
	FROM games AS `Game`
	WHERE `Game`.barcode = game_barcode AND `Game`.`status` = 1;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_game_detail_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_game_detail_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_game_detail_by_id`( IN `game_id` INT(5) )
BEGIN
SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.image_url, Game.status
FROM games AS `Game`
WHERE `Game`.id = game_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_game_detail_by_title` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_game_detail_by_title` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_game_detail_by_title`( IN `game_title` varchar(100))
BEGIN
	SELECT Game.id, Game.title, Game.buying, Game.image_url, Game.rating, Game.console_id, Game.trading
	FROM games AS `Game`
	WHERE `Game`.title LIKE CONCAT('%',game_title,'%') and `Game`.`status` = 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_genre_or_console` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_genre_or_console` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_genre_or_console`(IN `genre_name` VARCHAR(30),
							     IN `console_name` VARCHAR(30))
BEGIN
	IF genre_name IS NOT NULL THEN
		SELECT id FROM genres WHERE genres.genre = genre_name;
	END IF;
	
	
	IF console_name IS NOT NULL THEN
		SELECT id FROM consoles WHERE consoles.console = console_name;
	END IF;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_group_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_group_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_group_by_group_id`(IN group_id INT(11))
BEGIN
select 	Group.id, Group.name, Group.created
from `groups` AS `Group`
WHERE Group.id = group_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_group_details_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_group_details_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_group_details_by_group_id`(IN group_id INT(11))
BEGIN
SELECT 	
Group.id,Group.name,Group.created,
User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
FROM groups AS `Group`
LEFT JOIN users AS `User` ON User.group_id = Group.id AND User.status = 'active'
WHERE Group.id = group_id AND Group.is_active = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_pages_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_pages_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_pages_by_id`(IN `page_id` int(11))
BEGIN
    
	SELECT Page.id, Page.title, Page.desc
	FROM pages AS Page
	WHERE Page.id = page_id AND Page.status = 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_page_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_page_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_page_by_id`(IN `page_id` int(11))
BEGIN
    
	SELECT Page.id, Page.title, Page.desc
	FROM pages AS Page
	WHERE Page.id = page_id AND Page.status = 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_username_or_image` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_username_or_image` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_username_or_image`(in `user_id` int(11), in `game_id` int(11))
BEGIN
    
	if user_id is not null then
	
		select users.nickname, users.image_url
		from users
		where users.id = user_id;
	
	else
	
		SELECT games.title, games.console_id, games.image_url, consoles.console
		FROM games
		left join consoles on games.console_id = consoles.id
		WHERE games.id = game_id;
	
	end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_all_consoles` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_all_consoles` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_all_consoles`(IN `id_user` int(11))
BEGIN
    
	SELECT users_consoles.user_id, consoles.id, consoles.console
	FROM users_consoles, consoles
	WHERE users_consoles.user_id = id_user AND users_consoles.console_id = consoles.id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_all_genres` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_all_genres` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_all_genres`(IN `id_user` int(11))
BEGIN
    
	SELECT users_genres.user_id, genres.id, genres.genre
	FROM users_genres, genres
	WHERE users_genres.user_id = id_user  AND users_genres.genre_id = genres.id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_all_wishlist` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_all_wishlist` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_all_wishlist`(IN `user_id` INT(11))
BEGIN
    
	SELECT games.id, games.title, games.console_id, games.selling, games.buying, games.rating, games.image_url, games.trading, games.release_date,
       games.publisher, games.developers, games.ongoing_deals, users_wishlists.trade, genres.genre    
	FROM games 
	LEFT JOIN users_wishlists ON games.id = users_wishlists.game_id 
	LEFT JOIN genres ON games.genre_id = genres.id
	WHERE users_wishlists.user_id = user_id AND games.status = 1 and users_wishlists.status = 1
	order by games.title asc;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_by_email` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_by_email` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_by_email`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
FROM users AS `User` 
WHERE User.username = email AND User.status = 'active'; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_by_id`(IN `user_id` VARCHAR(30), OUT `status` INT)
BEGIN
    
    declare game_count int(5);
    
	SELECT nickname, created
	FROM users
	WHERE id = user_id;
	
	SELECT COUNT(DISTINCT(game_id)) into game_count
	FROM users_games
	WHERE users_games.user_id = user_id;
	
	SET `status` =  game_count;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_details` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_details` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_details`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.*
FROM users AS User 
WHERE User.username = email AND User.status = 'active'; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_details_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_details_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
		SELECT User.id, User.nickname, User.email, User.dob, User.ip_address, User.gaming_id, User.shipping_address, 
		       User.location, User.paypal_id, User.contact, User.image_url, User.profile_text, User.created
		FROM users AS `User`
		WHERE `User`.id = user_id AND `User`.`status` = 1;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_profile_info` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_profile_info` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_profile_info`(in `user_id` int(11))
BEGIN
    
    select users.id, users.nickname, users.email, users.dob, users.status, users.gaming_id, users.shipping_address, users.location,
           users.paypal_id, users.contact, users.profile_text, users.created
    from users
    where users.id = user_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_social_account_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_social_account_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_social_account_data`(IN `id` INT(5))
    NO SQL
BEGIN 
SELECT UserSocialAccount.*
FROM user_social_accounts AS UserSocialAccount
WHERE UserSocialAccount.user_id = id AND UserSocialAccount.status = 'active';
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_tmp_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_tmp_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_tmp_data`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.*
FROM tmps AS Tmp 
WHERE Tmp.target_id = target_id AND Tmp.type = type; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `is_unverified_email_exists` */

/*!50003 DROP PROCEDURE IF EXISTS  `is_unverified_email_exists` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.email
FROM users AS User
WHERE (User.email = email AND User.status = 1) OR (User.email = email AND User.status = 0);
END */$$
DELIMITER ;

/* Procedure structure for procedure `list_chat_between_two_users` */

/*!50003 DROP PROCEDURE IF EXISTS  `list_chat_between_two_users` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `list_chat_between_two_users`(in `id_sender` int(11), IN `id_receiver` INT(11))
BEGIN
    
	SELECT UserChatlog.chatlog_id, UserChatlog.sender_id, UserChatlog.receiver_id, UserChatlog.message, UserChatlog.created, UserChatlog.status
	FROM users_chatlogs UserChatlog
	WHERE ((UserChatlog.sender_id = id_sender AND UserChatlog.receiver_id = id_receiver) OR   
	       (UserChatlog.sender_id = id_receiver AND UserChatlog.receiver_id = id_sender));
    END */$$
DELIMITER ;

/* Procedure structure for procedure `list_deal_chat` */

/*!50003 DROP PROCEDURE IF EXISTS  `list_deal_chat` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `list_deal_chat`(in `deal_id` int(11), out `status` varchar(50))
BEGIN
    
	DECLARE id_deal INT(11);
	
	SELECT deals.id INTO id_deal
	FROM deals
	WHERE deals.id = deal_id AND deals.status = 1;
	
	IF id_deal IS NOT NULL THEN
	
		SELECT user_deal_id, id_sender, id_receiver, message, deals_chatlogs.status
		FROM deals_chatlogs
		WHERE deals_chatlogs.user_deal_id = deal_id;
		
	ELSE
	
		SET `status` = 'This deal id is not found';
	
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `manage_deals` */

/*!50003 DROP PROCEDURE IF EXISTS  `manage_deals` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `manage_deals`(IN `deal` INT(5), 
						   IN `user_id` INT(5), 
						   IN `member_id` INT(5), 
						   IN `amount` INT(5), 
						   IN `user_game` VARCHAR(90), 
						   IN `member_game` VARCHAR(90), 
						   IN `user_trade` VARCHAR(90), 
						   IN `member_trade` VARCHAR(90), 
						   IN `comment` VARCHAR(255), 
						   IN `action` VARCHAR(20),
						   IN `action_taken_by` INT(11),  
						   IN `ensured` INT(11), 
						   OUT `status` INT(5))
BEGIN	 
	DECLARE deal_ref_id INT(5);
	DECLARE game_ids VARCHAR(90); 
	CASE deal
		WHEN 0 THEN 	
			INSERT INTO `deals` ( offered_by, amount, `comment`, last_action_by, last_action_date, `status`, created ) 
				VALUE ( user_id, amount, `comment`, action_taken_by, NOW(), 1, NOW() );
				
			SET deal_ref_id = LAST_INSERT_ID();		
			
			INSERT INTO `users_deals` ( deal_id, user_id, `game`, action_taken, trade ) 
				VALUE ( deal_ref_id, user_id, user_game, `action`, user_trade );
				
			INSERT INTO `users_deals` ( deal_id, user_id, `game`, trade ) 
				VALUE ( deal_ref_id, member_id, member_game, member_trade );
	 	
			SET @QUERY = CONCAT('UPDATE `games` SET games.ongoing_deals = games.ongoing_deals + 1 WHERE games.id IN (',user_game,',',member_game,');');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;	
			
			IF ensured = 1 THEN
				
				UPDATE users_deals 
				SET users_deals.is_ensured = 1
				WHERE users_deals.deal_id = deal_ref_id AND users_deals.user_id IN (
				SELECT user_id FROM (
				SELECT user_id FROM `users_deals` 
				WHERE users_deals.user_id = action_taken_by
				) tmp);
				
				UPDATE users
				SET users.trusted_user = 1
				WHERE users.id = action_taken_by;
			
			END IF;
			
			SET `status` = 	deal_ref_id;	
			
			
		ELSE
			SET @l_query = CONCAT('SELECT GROUP_CONCAT(game)INTO @game_ids FROM `users_deals` GROUP BY deal_id HAVING deal_id = ', deal);
			PREPARE l_sql FROM @l_query;
			EXECUTE l_sql;
			DEALLOCATE PREPARE l_sql;
			
			SET @QUERY = CONCAT('UPDATE `games` SET games.ongoing_deals = games.ongoing_deals - 1 WHERE games.id IN (',@game_ids,');');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;	
			
			UPDATE `deals` 
			SET amount = amount, `comment` = `comment`, last_action_by = action_taken_by, last_action_date = NOW(), modified = NOW() WHERE id = deal;
			
			UPDATE `users_deals` 
			SET `game` = user_game, trade = user_trade
			WHERE id IN (
			     SELECT id FROM (
			         SELECT id FROM `users_deals` WHERE deal_id = deal ORDER BY id LIMIT 0, 1
			     ) tmp
			 );
			 
			UPDATE `users_deals` 
			SET `game` = member_game, trade = member_trade
			WHERE id IN (
			     SELECT id FROM (
				 SELECT id FROM `users_deals` WHERE deal_id = deal ORDER BY id LIMIT 1, 2
			     ) tmp
			 );
			SET @QUERY = CONCAT('UPDATE `games` SET games.ongoing_deals = games.ongoing_deals + 1 WHERE games.id IN (',user_game,',',member_game,');');
			PREPARE sql_query FROM @QUERY;
			EXECUTE sql_query;
			DEALLOCATE PREPARE sql_query;	
			
			
			IF `action` = 'accepted' THEN
			
				UPDATE users_deals
				SET users_deals.action_taken = `action`
				WHERE users_deals.deal_id = deal ;
			ELSE
			
				UPDATE users_deals
				SET users_deals.action_taken = `action`
				WHERE users_deals.deal_id = deal AND users_deals.user_id = action_taken_by;
			
			END IF;
			
			
			IF ensured = 1 THEN
				
				UPDATE users_deals 
				SET users_deals.is_ensured = 1
				WHERE users_deals.deal_id = deal AND users_deals.user_id IN (
				SELECT user_id FROM (
				SELECT user_id FROM `users_deals` 
				WHERE users_deals.user_id = action_taken_by
				) tmp);
				
				UPDATE users
				SET users.trusted_user = 1
				WHERE users.id = action_taken_by;
			
			END IF;
			
			IF `action`= 'cancelled' THEN
			
				UPDATE deals
				SET deals.status = 0
				WHERE deals.id = deal;
			
			END IF;
			
			SET `status` = 	deal;
		 
												
	END CASE;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `most_owned_games` */

/*!50003 DROP PROCEDURE IF EXISTS  `most_owned_games` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `most_owned_games`(IN `web` VARCHAR(10))
BEGIN
	if web = 'true' then
	
		SELECT UserGame.game_id, COUNT(UserGame.game_id) AS `Count`, Game.title, Game.selling, Game.buying, Game.rating, Game.image_url, Game.genre_id, Game.ongoing_deals
		FROM users_games UserGame, games Game
		WHERE Game.id = UserGame.game_id
		GROUP BY UserGame.game_id
		ORDER BY `Count` DESC
		limit 10;
	
	else
    
		SELECT UserGame.game_id, COUNT(UserGame.game_id) AS `Count`, Game.title, Game.selling, Game.buying, Game.rating, Game.image_url, Game.genre_id, Game.ongoing_deals
		FROM users_games UserGame, games Game
		WHERE Game.id = UserGame.game_id
		GROUP BY UserGame.game_id
		ORDER BY `Count` DESC;
	
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `most_wanted_games` */

/*!50003 DROP PROCEDURE IF EXISTS  `most_wanted_games` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `most_wanted_games`(in `web` varchar(10))
BEGIN
	if web = 'true' then
		SELECT UserWishlist.game_id, COUNT(UserWishlist.game_id) AS `Count`, Game.title, Game.selling, Game.buying, Game.rating, Game.image_url, Game.genre_id, Game.ongoing_deals
		FROM users_wishlists UserWishlist, games Game
		WHERE Game.id = UserWishlist.game_id
		GROUP BY UserWishlist.game_id
		ORDER BY `Count` DESC
		limit 10;
	
	
	else 
    
		SELECT UserWishlist.game_id, COUNT(UserWishlist.game_id) AS `Count`, Game.title, Game.selling, Game.buying, Game.rating, Game.image_url, Game.genre_id, Game.ongoing_deals, Game.release_date
		FROM users_wishlists UserWishlist, games Game
		where Game.id = UserWishlist.game_id
		GROUP BY UserWishlist.game_id
		ORDER BY `Count` DESC;
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `my_current_deals` */

/*!50003 DROP PROCEDURE IF EXISTS  `my_current_deals` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `my_current_deals`(in `id_user` int(11),
						       IN `trade` varchar(30),
						       in `id_deal` int(11)						      						      
						      )
BEGIN
    	
	IF trade = 'buy' THEN
	
		SELECT users_deals.deal_id, users_deals.game, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, users.id, users.nickname, users.image_url, users.trusted_user, deals.offered_by
		FROM users_deals
		left join deals on users_deals.deal_id = deals.id
		left join users on users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) and deals.status = 1 and
		      users_deals.user_id <> id_user and users_deals.trade = 'sell only' and users_deals.action_taken <> 'accepted';
	
	END IF;	
	
	
	IF trade = 'sell' THEN
	
		SELECT users_deals.deal_id, users_deals.game, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, deals.offered_by
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 1 AND
		      users_deals.user_id = id_user AND users_deals.trade = 'sell only' AND users_deals.action_taken <> 'accepted';
	
	END IF;
	
	IF trade = 'exchange' THEN
	
		SELECT users_deals.deal_id, users_deals.game,users_deals.trade, deals.offered_by, deals.amount, deals.last_action_by, deals.last_action_date, deals.comment, users.id, users.nickname, users.image_url, 
		users.trusted_user
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		LEFT JOIN users ON users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) and deals.status = 1 AND
		(users_deals.trade = 'exchange' or users_deals.trade = 'exchange and pay' OR users_deals.trade = 'exchange and charge')
		AND users_deals.action_taken <> 'accepted';
	
	END IF;	
	
	IF `id_deal` IS NOT NULL then
	
		SELECT users.id, users.nickname, users.image_url, users.trusted_user
		FROM users
		LEFT JOIN users_deals ON users.id = users_deals.user_id
		WHERE users_deals.deal_id = id_deal AND users_deals.user_id <> id_user;
	
	
	END IF;
	
	
	
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `recommended_for_you` */

/*!50003 DROP PROCEDURE IF EXISTS  `recommended_for_you` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `recommended_for_you`(in `id_user` int(11),
							  in `genre_id` int(11),
							  IN `recommended` VARCHAR(20),
							  IN `jiggs_pick` varchar(20),							
							  out `out_genre_ids` varchar(256))
BEGIN
	DECLARE genres VARCHAR(256);
	if genre_id is not null then
	
		SELECT games.id, games.title, games.image_url, games.selling, games.trading, games.release_date, games.publisher, games.developers, games.rating, games.genre_id, genres.genre, consoles.console
		from games
		LEFT JOIN genres ON games.genre_id = genres.id
		LEFT JOIN consoles ON games.console_id = consoles.id
		where games.genre_id = genre_id  and games.id not in (SELECT users_games.game_id FROM users_games WHERE users_games.user_id = id_user)
		limit 3; 
		
	end if;
	
	if recommended = 'recommended' then
    
		SELECT games.id, games.title, games.image_url, games.selling, games.trading, games.release_date, games.publisher, games.developers, games.rating, games.console_id, games.genre_id, genres.genre, consoles.console
		FROM games
		left join genres on games.genre_id = genres.id
		LEFT JOIN consoles ON games.console_id = consoles.id
		WHERE (games.console_id IN (SELECT users_consoles.console_id FROM users_consoles WHERE users_consoles.user_id = id_user)
		AND games.genre_id IN (SELECT users_genres.genre_id FROM users_genres WHERE users_genres.user_id = id_user))
		AND games.id NOT IN(SELECT users_games.game_id FROM users_games WHERE users_games.user_id = id_user);
		
		select GROUP_CONCAT(users_genres.genre_id) into @genres
		from users_genres
		where user_id = id_user;
		
		set `out_genre_ids` = @genres;
	
	end if;
	
	
	if jiggs_pick = 'jiggster_pick' then
	
		SELECT games.id, games.title, games.image_url, games.selling, games.trading, games.release_date, games.publisher, games.developers, games.rating, games.genre_id, genres.genre, consoles.console
		FROM games
		LEFT JOIN genres ON games.genre_id = genres.id
		LEFT JOIN consoles ON games.console_id = consoles.id
		WHERE games.jiggster_pick = 1; 
	
	
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_arosacos_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_arosacos_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_arosacos_by_group_id`(IN group_id INT(11) , OUT `status` INT)
BEGIN
DELETE FROM `aros_acos` 
WHERE aros_acos. aro_id = (
SELECT 	Aro.id
FROM groups AS `Group`
LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
WHERE Group.id = group_id
);
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_feature` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_feature` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_feature`(
IN feature_id int(11),
IN `modified` DATETIME,
IN `ip_address` INT(10),
OUT `status` INT)
BEGIN
IF EXISTS (SELECT COUNT(*) AS `count` FROM `features` AS `Feature` WHERE `Feature`.`id` = feature_id AND Feature.is_active = 1) THEN	
UPDATE `features` AS Feature
SET Feature.is_active = 0,
Feature.modified = modified,
Feature.ip_address = ip_address
WHERE Feature.id = feature_id ;
END IF;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_group`(
IN group_id int(11),
IN `modified` DATETIME,
IN `ip_address` INT(10),
OUT `status` INT)
BEGIN
IF EXISTS (SELECT COUNT(*) AS `count` FROM `groups` AS `Group` WHERE `Group`.`id` = group_id AND Group.is_active = 1) THEN	
UPDATE `groups` AS `Group`
SET Group.is_active = 0,
Group.modified = modified,
Group.ip_address = ip_address
WHERE Group.id = group_id ;
END IF;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `reset_password` */

/*!50003 DROP PROCEDURE IF EXISTS  `reset_password` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5), OUT `status` INT)
    NO SQL
BEGIN
UPDATE users
SET    
users.password = `password`                    
WHERE  users.id = id;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `save_console` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_console` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_console`(IN `user_id` INT(11), IN `console_id` INT(11),  OUT `status` INT)
BEGIN
		
	
		INSERT INTO users_consoles
			(
			users_consoles.user_id                 , 
			users_consoles.console_id              
			)
			VALUES 
			( 
			user_id                  , 
			console_id                  
			) ; 
	SET `status` = LAST_INSERT_ID();
	
	END */$$
DELIMITER ;

/* Procedure structure for procedure `save_deal_rating` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_deal_rating` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_deal_rating`(in `id_deal` int(11), in `id_user` int(11), in `rating` int(11))
BEGIN
    
	update users_deals
	
	set users_deals.`rating` = rating
	where users_deals.`deal_id` = id_deal and users_deals.`user_id` = id_user;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_genre` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_genre` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_genre`(IN `user_id` INT(11), IN `genre_id` INT(11),  OUT `status` INT)
BEGIN
		
    
    	INSERT INTO users_genres
(
users_genres.user_id                 , 
users_genres.genre_id              
)
VALUES 
( 
user_id                  , 
genre_id                  
) ; 
SET `status` = LAST_INSERT_ID();
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_record`(IN `email` VARCHAR(100), IN `nickname` VARCHAR(100), IN `password` VARCHAR(100), IN `dob` VARCHAR(100) , IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 
INSERT INTO users
(
users.email                 , 
users.nickname                       , 
users.password                       ,
users.dob                            , 
users.created			     ,
users.status
)
VALUES 
( 
email                           , 
nickname                        , 
`password`                      ,
dob                             , 
created				,
1
) ; 
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `save_tmp_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_tmp_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 
INSERT INTO tmps
(
tmps.target_id                  , 
tmps.type                       , 
tmps.data                       , 
tmps.expire_at                  ,
tmps.created
)
VALUES 
( 
target_id                    , 
type                         , 
data                         ,  
expire_at                    ,
created
) ; 
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_deal_message` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_deal_message` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_deal_message`(in `user_deal_id` int(11),
						  IN `id_sender` INT(11),
						  IN `id_receiver` INT(11),
						  IN `deal_message` varchar(1000),
						  out `status` int(11))
BEGIN
    
	INSERT INTO deals_chatlogs ( 
		    deals_chatlogs.user_deal_id,
		    deals_chatlogs.id_sender,
		    deals_chatlogs.id_receiver,
		    deals_chatlogs.message )
		    				  
	VALUES ( user_deal_id,
		id_sender,
		id_receiver,
		deal_message );
		
	SET `status` = LAST_INSERT_ID();
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_game_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_game_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_game_record`( IN `user_id` INT(11),
                                                  IN `game_id` INT(11),
                                                  IN `trade` varchar(20),
                                                  IN `condit` varchar(20),
                                                  IN `amount` VARCHAR(20),
                                                  IN `comes_with` varchar(30),
                                                  IN `coment` varchar(500),
                                                  OUT `status` INT
                                                  )
BEGIN
INSERT INTO users_games
(
users_games.user_id                  , 
users_games.game_id                  , 
users_games.trade                    , 
users_games.condition                ,
users_games.amount                ,
users_games.comes_with               ,
users_games.comment                  ,
users_games.status                 
)
VALUES 
( 
user_id,
game_id,
trade,
condit,
amount,
comes_with,
coment,
1
) ; 
SET `status` = LAST_INSERT_ID();
CASE trade  
        WHEN 'sell' THEN  
            UPDATE games
	    SET games.selling = games.selling + 1
	    WHERE games.id = game_id;
	      
        WHEN 'sell or trade' THEN  
             UPDATE games
	     SET games.selling = games.selling + 1, games.trading = games.trading + 1
	     WHERE games.id = game_id;  
        ELSE  
             UPDATE games
	     SET games.trading = games.trading + 1
	     WHERE games.id = game_id;  
    END CASE;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_message` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_message` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_message`(in `id_sender` int(11),
							in `id_receiver` int(11), 
							in `user_message` varchar(999),
							out `status` int(11)
							)
BEGIN
    
	declare users_chatlog_id int;
		
	select count(chatlog_id) into users_chatlog_id
	from users_chatlogs
	where (sender_id = id_sender and receiver_id = id_receiver) or 
	      (sender_id = id_receiver AND receiver_id = id_sender);	
	
	
	IF users_chatlog_id = 0 THEN  
		insert into chatlogs (chatlogs.status) values (1);
		
		set users_chatlog_id = last_insert_id();
		
	ELSE  
		SELECT chatlog_id INTO users_chatlog_id
		FROM users_chatlogs
		WHERE (sender_id = id_sender AND receiver_id = id_receiver) OR 
	        (sender_id = id_receiver AND receiver_id = id_sender)
	        limit 0,1;  
          
        END IF;
        
        
	
	insert into 
	`users_chatlogs` ( users_chatlogs.chatlog_id,
		users_chatlogs.sender_id,
		users_chatlogs.receiver_id,
		users_chatlogs.message )				  
	values ( users_chatlog_id,
		id_sender,
		id_receiver,
		user_message );
		
	set `status` = last_insert_id();
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_profile_info` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_profile_info` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_profile_info`(in `gaming_id` text,
						  IN `shipping_address` varchar(300),
						  IN `location` varchar(50),
						  IN `paypal_id` INT(20),
						  IN `contact` INT(20),
						  IN `profile_text` text,
						  in `user_id` int(11),
						  in `status` varchar(20)
						  )
BEGIN
	if `status`= 'details' then
    
	update users
	set users.gaming_id = gaming_id, users.shipping_address = shipping_address, users.location = location,
	    users.paypal_id = paypal_id, users.contact = contact, users.modified = now()
	
	where users.id = user_id;
	
	end if;
	
	if `status`='profile' then
	
	update users
	set users.profile_text = profile_text
	where users.id = user_id;
	
	end if;
    
    
    END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_record`(IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `username` VARCHAR(100), IN `password` VARCHAR(100), IN `role_id` INT(5), IN `created` DATETIME, OUT `status` VARCHAR(255))
    NO SQL
BEGIN 
DECLARE str VARCHAR(255);  
SET str = 'success';
SET status = str;
INSERT INTO users
(
users.first_name                  , 
users.last_name                   , 
users.username                    , 
users.password                    ,
users.role_id                     ,
users.created
)
VALUES 
( 
first_name                       , 
last_name                        , 
username                         ,  
password                         ,
role_id                          ,
created
) ; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `save_user_wishlist_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_user_wishlist_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_user_wishlist_record`( IN `user_id` INT(11),
                                                  IN `game_id` INT(11),
                                                  IN `trade` varchar(20),
                                                  OUT `status` INT
                                                  
                                                   )
BEGIN
   
    INSERT INTO users_wishlists
	(
	users_wishlists.user_id                  , 
	users_wishlists.game_id                  , 
	users_wishlists.trade                 , 
	users_wishlists.status                 
	)
	VALUES 
	( 
	user_id,
	game_id,
	trade,
	1
	) ; 
    SET `status` = LAST_INSERT_ID();
    CASE trade  
        WHEN 'buy' THEN  
            UPDATE games
	    SET games.buying = games.buying + 1
	    WHERE games.id = game_id;
	      
        WHEN 'buy or trade' THEN  
             UPDATE games
	     SET games.buying = games.buying + 1, games.trading = games.trading + 1
	     WHERE games.id = game_id;  
        ELSE  
             UPDATE games
	     SET games.trading = games.trading + 1
	     WHERE games.id = game_id;  
    END CASE;
  
    END */$$
DELIMITER ;

/* Procedure structure for procedure `search_game_by_title` */

/*!50003 DROP PROCEDURE IF EXISTS  `search_game_by_title` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `search_game_by_title`(IN `game_title` VARCHAR(100))
BEGIN
    
	SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.image_url, Game.status
	FROM games AS `Game`
	WHERE `Game`.title LIKE concat('%',game_title,'%');
    END */$$
DELIMITER ;

/* Procedure structure for procedure `search_user_by_nickname_or_email` */

/*!50003 DROP PROCEDURE IF EXISTS  `search_user_by_nickname_or_email` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `search_user_by_nickname_or_email`(in `keyword` varchar(100))
BEGIN
	SELECT users.id, users.`nickname`, users.`email`, users.`status`, users.`created`
	FROM users
	WHERE users.`nickname` LIKE CONCAT('%',keyword,'%') OR users.`email` LIKE CONCAT('%',keyword,'%');
END */$$
DELIMITER ;

/* Procedure structure for procedure `send_and_receive` */

/*!50003 DROP PROCEDURE IF EXISTS  `send_and_receive` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `send_and_receive`(IN `id_user` INT(11),
						       IN `trade` VARCHAR(30),
						       IN `id_deal` INT(11)						      						      
						      )
BEGIN
    	
	IF trade = 'buy' THEN
	
		SELECT users_deals.deal_id, users_deals.game, users_deals.send_confirm, users_deals.receive_confirm, deals.amount, deals.last_action_by, users.id, users.nickname, users.image_url, users.trusted_user, deals.offered_by
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		LEFT JOIN users ON users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 1 AND
		      users_deals.user_id <> id_user AND users_deals.trade = 'sell only' AND users_deals.action_taken = 'accepted';
	
	END IF;	
	
	
	IF trade = 'sell' THEN
	
		SELECT users_deals.deal_id, users_deals.game, users_deals.send_confirm, users_deals.receive_confirm, deals.amount, deals.last_action_by, deals.offered_by
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 1 AND
		      users_deals.user_id = id_user AND users_deals.trade = 'sell only' AND users_deals.action_taken = 'accepted';
	
	END IF;
	
	IF trade = 'exchange' THEN
	
		SELECT users_deals.deal_id, users_deals.game, users_deals.send_confirm, users_deals.receive_confirm, users_deals.trade, deals.offered_by, deals.amount, users.id, deals.last_action_by, users.nickname, users.image_url, 
		users.trusted_user
		FROM users_deals
		LEFT JOIN deals ON users_deals.deal_id = deals.id
		LEFT JOIN users ON users_deals.user_id = users.id
		WHERE users_deals.deal_id IN (SELECT deal_id FROM users_deals WHERE users_deals.user_id = id_user) AND deals.status = 1 AND
		(users_deals.trade = 'exchange' OR users_deals.trade = 'exchange and pay' OR users_deals.trade = 'exchange and charge')
		AND users_deals.action_taken = 'accepted';
	
	END IF;	
	
	IF `id_deal` IS NOT NULL THEN
	
		SELECT users.id, users.nickname, users.image_url, users.trusted_user
		FROM users
		LEFT JOIN users_deals ON users.id = users_deals.user_id
		WHERE users_deals.deal_id = id_deal AND users_deals.user_id <> id_user;
	
	
	END IF;	
			
END */$$
DELIMITER ;

/* Procedure structure for procedure `send_receive_action` */

/*!50003 DROP PROCEDURE IF EXISTS  `send_receive_action` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `send_receive_action`(IN deal INT(11), IN uid INT(11), IN receive_action INT(5), IN send_action INT(5), out `status` VARCHAR(900))
BEGIN
	declare send1 int(11);
	DECLARE receive1 INT(11);
	DECLARE send2 INT(11);
	DECLARE receive2 INT(11);
	DECLARE game_ids varchar(255);
	
		
	SET @QUERY = CONCAT('UPDATE users_deals SET send_confirm = ',send_action,', receive_confirm = ',receive_action,' WHERE deal_id = ',deal,' AND user_id = ',uid);
	PREPARE sql_query FROM @QUERY;
	EXECUTE sql_query;
	DEALLOCATE PREPARE sql_query;			
		
	
		
	SELECT send_confirm, receive_confirm into send1, receive1 FROM users_deals WHERE deal_id = deal LIMIT 0, 1;
		
	SELECT send_confirm, receive_confirm INTO send2, receive2 FROM users_deals WHERE deal_id = deal LIMIT 1, 2;
				
	if (send1 = send2 && send1 = 1) && (receive1 = receive2 && receive1 = 1) then
		
		UPDATE deals SET `status` = 0 WHERE id = deal;
		
		SELECT game into game_ids FROM `users_deals` WHERE deal_id = deal LIMIT 0, 1;
		
		SET @QUERY = CONCAT('UPDATE users_games SET `status` = 0 WHERE game_id IN (',game_ids,') AND user_id = (SELECT user_id FROM (SELECT user_id FROM `users_deals` WHERE 
		deal_id = ',deal,' LIMIT 0, 1) tmp );');
		PREPARE sql_query FROM @QUERY;
		EXECUTE sql_query;
		DEALLOCATE PREPARE sql_query;	
				
		SELECT game INTO game_ids FROM `users_deals` WHERE deal_id = deal LIMIT 1, 2;
		
		SET @QUERY = CONCAT('UPDATE users_games SET `status` = 0 WHERE game_id IN (',game_ids,') AND user_id = (SELECT user_id FROM (SELECT user_id FROM `users_deals` WHERE 
		deal_id = ',deal,' LIMIT 1, 2) tmp );');
		PREPARE sql_query FROM @QUERY;
		EXECUTE sql_query;
		DEALLOCATE PREPARE sql_query;			
		
	end if;
	
	set `status` = 1;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `signup_with_social_account` */

/*!50003 DROP PROCEDURE IF EXISTS  `signup_with_social_account` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `signup_with_social_account`(IN xml_data varchar(1000), OUT `status` INT)
BEGIN
DECLARE user_id BIGINT(11);
DECLARE first_name VARCHAR(255);
DECLARE last_name VARCHAR(255);
DECLARE username VARCHAR(50); 
DECLARE pwd VARCHAR(255);
DECLARE group_id INT(11);
DECLARE ip_address  INT(10); 
DECLARE created DATETIME; 
DECLARE link_id BIGINT(11); 
DECLARE image_url VARCHAR(255);
DECLARE type_id VARCHAR(20);
DECLARE access_token VARCHAR(255);
DECLARE screen_name VARCHAR(255);
SELECT ExtractValue(xml_data, '/response/User/first_name') INTO first_name;
SELECT ExtractValue(xml_data, '/response/User/last_name') INTO last_name;
SELECT ExtractValue(xml_data, '/response/User/username') INTO username;
SELECT ExtractValue(xml_data, '/response/User/password') INTO pwd;
SELECT ExtractValue(xml_data, '/response/User/group_id') INTO group_id;
SELECT ExtractValue(xml_data, '/response/User/ip_address') INTO ip_address;
SELECT ExtractValue(xml_data, '/response/User/created') INTO created;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/link_id') INTO link_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/image_url') INTO image_url;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/type_id') INTO type_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/access_token') INTO access_token;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/screen_name') INTO screen_name;
call `create_user`(first_name,last_name,username,pwd,group_id,created,ip_address,@status);
SELECT @status INTO user_id;
INSERT INTO `user_social_accounts` 
(	
user_social_accounts.user_id,
user_social_accounts.link_id,
user_social_accounts.email_address,
user_social_accounts.image_url,
user_social_accounts.type_id,
user_social_accounts.access_token,
user_social_accounts.screen_name,
user_social_accounts.ip_address,
user_social_accounts.created,
user_social_accounts.modified
)values (
user_id,
link_id,
username,
image_url,
type_id,
access_token,
screen_name,
ip_address,
created,
created
);
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `sync_aam_aco` */

/*!50003 DROP PROCEDURE IF EXISTS  `sync_aam_aco` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sync_aam_aco`(OUT `status` INT)
BEGIN
DELETE FROM auth_action_maps WHERE NOT aco_id IN ( SELECT Aco.id FROM `acos` AS Aco );
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `test` */

/*!50003 DROP PROCEDURE IF EXISTS  `test` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `test`(in `id_user` int(11))
BEGIN
    
	
	
	
	SELECT users_wishlists.game_id, users_wishlists.user_id
	FROM users_wishlists
	WHERE users_wishlists.trade = 'buy' or users_wishlists.trade = 'buy or trade'  AND users_wishlists.user_id IN 
	
	(SELECT user_id FROM users_consoles
	WHERE users_consoles.user_id <> id_user AND users_consoles.console_id IN
	
	(SELECT users_consoles.console_id
	FROM users_consoles, consoles
	WHERE users_consoles.console_id = consoles.id AND users_consoles.user_id = id_user)) 
	
	AND users_wishlists.game_id IN 
	(SELECT users_games.game_id 
	FROM users_games
	WHERE users_games.trade = 'sell' or users_games.trade = 'sell or trade' AND users_games.user_id = id_user);
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `test_multi_sets` */

/*!50003 DROP PROCEDURE IF EXISTS  `test_multi_sets` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
select user() as first_col;
select user() as first_col, now() as second_col;
select user() as first_col, now() as second_col, now() as third_col;
end */$$
DELIMITER ;

/* Procedure structure for procedure `upcoming_and_new_releases` */

/*!50003 DROP PROCEDURE IF EXISTS  `upcoming_and_new_releases` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `upcoming_and_new_releases`(IN `game_status` VARCHAR(20))
BEGIN
    
	CASE   
		WHEN game_status = 'upcoming' THEN  
		    SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.image_url, Game.genre_id, Game.buying, Game.selling, Game.trading, DATE_FORMAT(Game.release_date,'%m/%d/%Y') AS release_date
		    FROM games AS Game
		    WHERE DATE(Game.release_date) > DATE(NOW()) and Game.status = 1;  
		WHEN game_status = 'new releases' THEN  
		    SELECT Game.id, Game.title, Game.desc, Game.publisher, Game.developers, Game.rating, Game.image_url, Game.genre_id, Game.buying, Game.selling, Game.ongoing_deals, Game.trading, DATE_FORMAT(Game.release_date,'%m/%d/%Y') AS release_date
		    FROM games AS Game
		    WHERE DATE(Game.release_date) < DATE(NOW()) AND Game.status = 1;   
	 
		    
	END CASE;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `update_feature` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_feature` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_feature`(
IN feature_id INT(11),
IN description VARCHAR(100),
IN `modified` DATETIME,
IN `ip_address` INT(10),
OUT `status` INT)
BEGIN
UPDATE `features` AS Feature
SET Feature.description = description,
Feature.modified = modified,
Feature.ip_address = ip_address
WHERE Feature.id = feature_id AND Feature.is_active = 1;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_game_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_game_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_game_record`(    IN `game_id` INT(11),
                                                  IN `title` varchar(100),
                                                  IN `description` varchar(500),
                                                  IN `publisher` VARCHAR(100),
                                                  IN `developers` VARCHAR(300)
                                              )
BEGIN
    
	UPDATE games
	SET games.title = title, games.desc = description, games.publisher = publisher, games.developers = developers
	WHERE games.id = game_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `update_game_status` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_game_status` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_game_status`(IN `game_status` INT(5), IN `game_id` INT(5))
BEGIN
    
	UPDATE games
	SET games.status = game_status
	WHERE games.id = game_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `update_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_group`(
IN group_id INT(11),
IN name VARCHAR(100),
IN `modified` DATETIME,
IN `ip_address` INT(10),
OUT `status` INT)
BEGIN
UPDATE `groups` AS `Group`
SET Group.name = name,
Group.modified = modified,
Group.ip_address = ip_address
WHERE Group.id = group_id AND Group.is_active = 1;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_page` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_page` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_page`(in `page_id` int(11), 
						  in `page_title` varchar(50), 
						  in `page_desc` varchar(1000))
BEGIN
	UPDATE pages
	SET pages.title = page_title, pages.desc=page_desc
	WHERE pages.id = page_id;
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_user_game_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_user_game_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_user_game_record`(IN `user_id` INT(11),
                                                   IN `game_id` INT(11),
                                                   IN `amount` INT(11),
                                                   IN `trade` VARCHAR(20),
                                                   IN `condit` VARCHAR(20),
                                                   IN `comes_with` VARCHAR(30),
                                                   IN `coment` VARCHAR(500))
BEGIN
		declare game_trade varchar(30);
		declare sell_count int(11);
		declare trade_count int(11);
		select users_games.trade into game_trade
		from users_games
		where users_games.user_id = user_id and users_games.game_id = game_id;
		
		CASE game_trade  
		
			WHEN 'sell' THEN  
			
			    select games.selling into sell_count from games where games.id = game_id;
			    
			    if sell_count > 0 then
				
				    UPDATE games
				    SET games.selling = games.selling - 1
				    WHERE games.id = game_id;
				    
			    end if;
			      
			WHEN 'sell or trade' THEN
			
			SELECT games.selling INTO sell_count FROM games WHERE games.id = game_id;
			SELECT games.trading INTO trade_count FROM games WHERE games.id = game_id;
			    
			    IF (sell_count > 0 && trade_count > 0) THEN
			      
				     UPDATE games
				     SET games.selling = games.selling - 1, games.trading = games.trading - 1
				     WHERE games.id = game_id; 
			     
			     end if;
			      
			WHEN 'trade' THEN
			
			     SELECT games.trading INTO trade_count FROM games WHERE games.id = game_id;
			    
			    IF trade_count > 0 THEN
			
				     UPDATE games
				     SET games.trading = games.trading - 1
				     WHERE games.id = game_id;
			     
			     end if;  
		END CASE;
    
		UPDATE users_games
		
		SET    	users_games.trade  = trade,                 
			users_games.condition  =   condit,
			users_games.`amount`  =   amount,        
			users_games.comes_with  = comes_with,            
			users_games.comment  =  coment
			                           
		WHERE users_games.user_id = user_id and users_games.game_id = game_id;
		
		CASE trade  
		
			WHEN 'sell' THEN  
			    UPDATE games
			    SET games.selling = games.selling + 1
			    WHERE games.id = game_id;
			      
			WHEN 'sell or trade' THEN  
			     UPDATE games
			     SET games.selling = games.selling + 1, games.trading = games.trading + 1
			     WHERE games.id = game_id;  
			WHEN 'trade' THEN  
			     UPDATE games
			     SET games.trading = games.trading + 1
			     WHERE games.id = game_id;  
		END CASE; 
		                       
    END */$$
DELIMITER ;

/* Procedure structure for procedure `update_user_status` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_user_status` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_user_status`(IN `user_status` INT(5), IN `user_id` INT(5))
BEGIN
    
	UPDATE users
	SET users.`status`= user_status
	WHERE users.`id` = user_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `update_user_wishlist_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_user_wishlist_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_user_wishlist_record`(IN `user_id` INT(11),
						       IN `game_id` INT(11),
                                                       IN `trade` VARCHAR(20))
BEGIN
	DECLARE game_trade VARCHAR(30);
	DECLARE buy_count INT(11);
	DECLARE trade_count INT(11);
	
	SELECT users_wishlists.trade INTO game_trade
	FROM users_wishlists
	WHERE users_wishlists.user_id = user_id AND users_wishlists.game_id = game_id;
	
	CASE game_trade  
		
			WHEN 'buy' THEN  
			
			    SELECT games.buying INTO buy_count FROM games WHERE games.id = game_id;
			    
			    IF buy_count > 0 THEN
				
				    UPDATE games
				    SET games.buying = games.buying - 1
				    WHERE games.id = game_id;
				    
			    END IF;
			      
			WHEN 'buy or trade' THEN
			
			SELECT games.buying INTO buy_count FROM games WHERE games.id = game_id;
			SELECT games.trading INTO trade_count FROM games WHERE games.id = game_id;
			    
			    IF (buy_count > 0 && trade_count > 0) THEN
			      
				     UPDATE games
				     SET games.buying = games.buying - 1, games.trading = games.trading - 1
				     WHERE games.id = game_id; 
			     
			     END IF;
			      
			WHEN 'trade' THEN
			
			     SELECT games.trading INTO trade_count FROM games WHERE games.id = game_id;
			    
			    IF trade_count > 0 THEN
			
				     UPDATE games
				     SET games.trading = games.trading - 1
				     WHERE games.id = game_id;
			     
			     END IF;  
	END CASE;
    
	UPDATE users_wishlists
	
	SET 	users_wishlists.trade = trade
		                          
	WHERE users_wishlists.user_id = user_id and users_wishlists.game_id = game_id ; 
	
	CASE trade  
		WHEN 'buy' THEN  
		    UPDATE games
		    SET games.buying = games.buying + 1
		    WHERE games.id = game_id;
		      
		WHEN 'buy or trade' THEN  
		     UPDATE games
		     SET games.buying = games.buying + 1, games.trading = games.trading + 1
		     WHERE games.id = game_id;  
		WHEN 'trade' THEN  
		     UPDATE games
		     SET games.trading = games.trading + 1
		     WHERE games.id = game_id;  
	END CASE;
    
    END */$$
DELIMITER ;

/* Procedure structure for procedure `users_buying_specific_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `users_buying_specific_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `users_buying_specific_game`(IN `game_id` INT(11))
BEGIN
    
	SELECT users_wishlists.`user_id`, users_wishlists.`trade`, users.`nickname`, users.`gaming_id`, users.`image_url`, users.`trusted_user`, users.`created`, games.`selling`,
	       games.`trading`, games.`buying`
	FROM users_wishlists
	LEFT JOIN users ON users_wishlists.`user_id` = users.`id`
	LEFT JOIN games ON users_wishlists.`game_id` = games.`id`
	WHERE (users_wishlists.`trade` = 'buy' OR users_wishlists.`trade` = 'buy or trade')  AND users_wishlists.`game_id` = game_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `users_selling_specific_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `users_selling_specific_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `users_selling_specific_game`(in `game_id` int(11))
BEGIN
	
	SELECT users_games.`user_id`, users_games.`trade`, users.`nickname`, users.`gaming_id`, users.`image_url`, users.`trusted_user`, users.`created`, games.`selling`,
	       games.`trading`, games.`buying`
	FROM users_games
	LEFT JOIN users ON users_games.`user_id` = users.`id`
	left join games on users_games.`game_id` = games.`id`
	WHERE (users_games.`trade` = 'sell' or users_games.`trade` = 'sell or trade')  AND users_games.`game_id` = game_id;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `users_trading_specific_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `users_trading_specific_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `users_trading_specific_game`(IN `game_id` INT(11), IN `count` INT(11))
BEGIN
    
	IF `count` = 1 THEN
    
		SELECT users_games.`user_id`, users_games.`trade`, users.`nickname`, users.`gaming_id`, users.`image_url`, users.`trusted_user`, users.`created`, games.`selling`,
		       games.`trading`, games.`buying`
		FROM users_games
		LEFT JOIN users ON users_games.`user_id` = users.`id`
		LEFT JOIN games ON users_games.`game_id` = games.`id`
		WHERE (users_games.`trade` = 'trade' OR users_games.`trade` = 'sell or trade' )  AND users_games.`game_id` = game_id;
	
	END IF;
	
	if `count` = 2 then 
	
		SELECT users_wishlists.`user_id`, users_wishlists.`trade`, users.`nickname`, users.`gaming_id`, users.`image_url`, users.`trusted_user`, users.`created`, games.`selling`,
		       games.`trading`, games.`buying`
		FROM users_wishlists
		LEFT JOIN users ON users_wishlists.`user_id` = users.`id`
		LEFT JOIN games ON users_wishlists.`game_id` = games.`id`
		where (users_wishlists.`trade` = 'trade' or users_wishlists.`trade` = 'buy or trade') and users_wishlists.`game_id` = game_id;
	
	
	end if;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `users_who_posess_specific_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `users_who_posess_specific_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `users_who_posess_specific_game`(IN `game_id` VARCHAR(10))
BEGIN
    
	SELECT UserGame.user_id, Users.nickname, UserGame.trade, DATE_FORMAT(Users.created,'%d/%m/%Y') 'MemberSince', 
	UserGame.condition, UserGame.comes_with, UserGame.comment, 
       (select COUNT(game_id) from users_games where user_id = UserGame.user_id) as game_owned
	FROM users Users, users_games UserGame
	WHERE UserGame.game_id = game_id && Users.id = UserGame.user_id 
	group by UserGame.user_id;
		
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `users_who_wish_specific_game` */

/*!50003 DROP PROCEDURE IF EXISTS  `users_who_wish_specific_game` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `users_who_wish_specific_game`(IN `game_id` VARCHAR(10))
BEGIN
    
	SELECT UserWishlist.user_id, Users.nickname, UserWishlist.trade, DATE_FORMAT(Users.created,'%d/%m/%Y') 'MemberSince',      
       (SELECT COUNT(game_id) FROM users_wishlists WHERE user_id = UserWishlist.user_id) AS game_owned
	FROM users Users, users_wishlists UserWishlist 
	WHERE UserWishlist.game_id = game_id && Users.id = UserWishlist.user_id 
	GROUP BY UserWishlist.user_id;
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `user_delete_consoles_genres` */

/*!50003 DROP PROCEDURE IF EXISTS  `user_delete_consoles_genres` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `user_delete_consoles_genres`(in `id_user` int(11), IN `table_name` VARCHAR(30))
BEGIN
    
	SET @QUERY = CONCAT('delete FROM ', table_name,' WHERE user_id = ', id_user);
	PREPARE sql_query FROM @QUERY;
	EXECUTE sql_query;
	DEALLOCATE PREPARE sql_query;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `user_mark_deal_message_read` */

/*!50003 DROP PROCEDURE IF EXISTS  `user_mark_deal_message_read` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `user_mark_deal_message_read`(in `deal_id` int(11), IN `id_receiver` INT(11))
BEGIN
    
	UPDATE deals_chatlogs
	SET deals_chatlogs.status = 'read'
	WHERE deals_chatlogs.user_deal_id= deal_id AND deals_chatlogs.id_receiver= id_receiver;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `user_mark_message_read` */

/*!50003 DROP PROCEDURE IF EXISTS  `user_mark_message_read` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `user_mark_message_read`(in `id_chatlog` int(11), in `id_receiver` int(11))
BEGIN
    
	UPDATE users_chatlogs
	SET users_chatlogs.status = 'read'
	WHERE users_chatlogs.chatlog_id = id_chatlog AND users_chatlogs.receiver_id = id_receiver;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `user_save_profile_image_path` */

/*!50003 DROP PROCEDURE IF EXISTS  `user_save_profile_image_path` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `user_save_profile_image_path`(IN `user_id` int(11), IN `image_url` varchar(256))
BEGIN
    
	update users
	set users.image_url = image_url
	where users.id = user_id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `user_signup_social` */

/*!50003 DROP PROCEDURE IF EXISTS  `user_signup_social` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `user_signup_social`(IN `link_id` VARCHAR(20),
					      IN `email_address` VARCHAR(50),
					      IN `image_url` VARCHAR(255),
					      IN `type_id` VARCHAR(15),
					      IN `access_token` VARCHAR(255),
					      IN `access_token_secret` VARCHAR(255), 
					      IN `is_valid_token` INT(5),
					      IN `screen_name` VARCHAR(50), 
					      IN `user_status` VARCHAR(10),
					      IN `ip_address` VARCHAR(10),
					      OUT `status` INT)
BEGIN
     
    declare user_id int(10);
    set user_id = null;
    
    SELECT id into user_id
    FROM users
    where users.email = email_address;
    
    SET `status` =  user_id;
        
    IF user_id is null THEN 
    
    INSERT INTO users
	(
	users.email                  , 
	users.nickname                   
	)
	VALUES 
	( 
	email_address                       , 
	screen_name                        
	) ;
	SET `status` =  LAST_INSERT_ID();
 
    INSERT INTO users_social_accounts
(
	users_social_accounts.user_id                  ,
	users_social_accounts.link_id                  , 
	users_social_accounts.email_address              ,   
	users_social_accounts.image_url              ,   
	users_social_accounts.type_id              ,   
	users_social_accounts.access_token              ,   
	users_social_accounts.access_token_secret              ,   
	users_social_accounts.is_valid_token              , 
	users_social_accounts.screen_name              ,
	users_social_accounts.status              , 
	users_social_accounts.ip_address   
)
VALUES 
( 
	LAST_INSERT_ID(),
	link_id,
	email_address,
	image_url,
	type_id,
	access_token,
	access_token_secret,
	is_valid_token,
	screen_name,
	user_status,
	ip_address
                     
) ;
 END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `verify_account` */

/*!50003 DROP PROCEDURE IF EXISTS  `verify_account` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `verify_account`(IN `email` VARCHAR(100), OUT `status` INT)
BEGIN
    
    UPDATE users
SET users.status = 1
WHERE users.email = email;
    
SET `status` = ROW_COUNT();
    END */$$
DELIMITER ;

/* Procedure structure for procedure `verify_login` */

/*!50003 DROP PROCEDURE IF EXISTS  `verify_login` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 
		SELECT User.id
		FROM users AS User 
		WHERE User.email = email AND User.password = pass AND User.status = 1; 
	END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
