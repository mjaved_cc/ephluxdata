<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class UsersGenre extends AppModel {

	function save_user_genre($user_id, $genre_ids) {
		
		$this->query('call user_delete_consoles_genres("' . $user_id . '", "' . TABLE_GENRE . '")');

		$count = count($genre_ids);

		for ($i = 0; $i < $count; $i++) {

			$this->query('call save_genre("' . $user_id . '", "' . $genre_ids[$i] . '", @status)');
		}

		return $this->get_status();
	}

}
