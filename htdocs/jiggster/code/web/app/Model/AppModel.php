<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * Synchronize Auth Action Map with Aco table. In case if any entry removed from Aco tables. 
	 * 
	 * @return boolean
	 */
	function sync_with_acos() {
		$this->query('call sync_aam_aco(@status)');

		return $this->get_status();
	}

	/**
	 * Get action items used in whole project. Query will return result only when auth_action_maps have is not sync with acos.
	 * 
	 * @return array actions
	 */
	function get_ctrl_actions() {

		$this->sync_with_acos();

		return $this->query('SELECT Child.* , ActionMapper.id
					FROM `acos` AS `Child` 
					LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
					LEFT JOIN `auth_action_maps` AS `ActionMapper` ON (`ActionMapper`.`aco_id` = `Child`.`id`)
					WHERE `Parent`.`parent_id` IS NOT NULL AND ActionMapper.id IS NULL');
	}

	/**
	 * Get all action that belongs to any of CRUD operation. This mapping is stored in auth_action_maps
	 * 
	 * @return array actions
	 */
	function get_action_maps() {
		return $this->query('SELECT AuthActionMap.id, AuthActionMap.aco_id, AuthActionMap.crud, Action.id, Action.alias, 
						Controller.id, Controller.alias 
					FROM auth_action_maps AS AuthActionMap 
					LEFT JOIN acos AS Action ON (AuthActionMap.aco_id = Action.id) 
					LEFT JOIN acos AS Controller ON (Action.parent_id = Controller.id)');
	}

	/**
	 * Turn off all associations on the fly.
	 *
	 * Example: Turn off the associated Model Support request,
	 * to temporarily lighten the User model:
	 *
	 * `$this->User->unbindModelAll();`
	 */
	function unbindModelAll() {
		$unbind = array();
		foreach ($this->belongsTo as $model => $info) {
			$unbind['belongsTo'][] = $model;
		}
		foreach ($this->hasOne as $model => $info) {
			$unbind['hasOne'][] = $model;
		}
		foreach ($this->hasMany as $model => $info) {
			$unbind['hasMany'][] = $model;
		}
		foreach ($this->hasAndBelongsToMany as $model => $info) {
			$unbind['hasAndBelongsToMany'][] = $model;
		}
		parent::unbindModel($unbind);
	}

	/*
	 * @desc: Bulk insert in one go. Cakephp saveAll method creates n INSERT query if array length is n which degrades performance.
	 * @params: data <array> - it should be same as that of used in saveAll
	 * @return: boolean
	 * @usage: You can also use this format to save several records with custom_saveAll(), using an array like the following:
	 * Array
	  (
	  [0] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  [1] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  )
	 * NOTE: created & modified datetime will not automatically save. and finish off with a single call to mysql_real_escape_string() while querying such as mysql_real_escape_string($json) / mysql_real_escape_string($serialize)
	 */

	function custom_saveAll($data) {

		$first_idx = key($data);
		// check if data is not empty & is of proper format. i.e the one use in saveAll
		if (count($data) > 0 && is_numeric($first_idx)) {
			$value_array = array();

			// table & name defined in lib/Cake/Model/Model.php
			$table_name = $this->table;
			$model_name = $this->name;

			$fields = array_keys($data[$first_idx][$model_name]);

			foreach ($data as $key => $value)
				$value_array[] = "('" . implode('\',\'', $value[$model_name]) . "')";


			$sql = "INSERT INTO " . $table_name . " (" . implode(', ', $fields) . ") VALUES " . implode(',', $value_array);

			$this->query($sql);
			return true;
		}

		return false;
	}

	/**
	 * For debugging cakephp queries
	 * 
	 * @return string SQL-query
	 */
	function get_last_query() {
		$dbo = $this->getDatasource();
		$logs = $dbo->getLog();
		$lastLog = end($logs['log']);
		return $lastLog['query'];
	}

	/**
	 * Get status of stored procedures
	 * 
	 * @return string|int rows affected by insert/update/delete
	 */
	function get_status() {
		$status = $this->query('SELECT @status');
		return ($status['0']['0']['@status'] > 0) ? $status['0']['0']['@status'] : false;
	}

	function get_distance($user1, $user2) {

		$distance = -1;

		if (!empty($user1) && !empty($user2)) {

			$user1_lat_lan = $this->get_user_lat_lan($user1);
			$user2_lat_lan = $this->get_user_lat_lan($user2);

			if($user1_lat_lan['latitude'] != 0 && $user1_lat_lan['longitude'] != 0 && $user2_lat_lan['latitude'] != 0 && $user2_lat_lan['longitude'] != 0 ) {

				$distance = $this->cal_distance($user1_lat_lan['latitude'], $user1_lat_lan['longitude'], $user2_lat_lan['latitude'], $user2_lat_lan['longitude'], DISTANCE_UNIT);
			}
		}

		return $distance;
	}

	function get_user_lat_lan($user_id) {

		$user_lat_lan = $this->query("call get_user_lant_lan( '" . $user_id . "' )"); //pr($user_lat_lan[0]['users']); exit;

		if (!empty($user_lat_lan)) {

			return $user_lat_lan[0]['users'];
		} else {

			return $user_lat_lan;
		}
	}

	function cal_distance($lat1, $lon1, $lat2, $lon2, $unit) {

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

}
