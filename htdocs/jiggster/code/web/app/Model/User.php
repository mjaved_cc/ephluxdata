<?php

App::uses('AppModel', 'Model');

/**

 * User Model

 *

 */
class User extends AppModel {

	/**

	 * Display field

	 *

	 * @var string

	 */
	public $displayField = 'username';
	var $name = 'User';
//	var $belongsTo = array(
//		'Group'
//	);
//	public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

	var $hasMany = array(
		'UserSocialAccount'
	);
	var $validate = array(
		'password' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minlength', 8),
				'message' => 'Password must be between 8 to 15 characters'
			)
		),
		'email' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please supply a valid email address.',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'isUnique',
				'message' => 'Email already exists'
			)
		)
	);

	/**

	 * authorizes user credentials

	 * @param varchar $email email address

	 * @param varchar $password password

	 */
	function verify_login($email, $password, $gcm_regid) {

		$result = $this->query('call verify_login("' . $email . '", "' . $password . '", "' . $gcm_regid . '")');

		if (!empty($result))
			return $result['0'];

		return false;
	}

	function get_details_by_id($id) {



		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');



		$user = $this->query('call get_user_details_by_user_id("' . $id . '")');


		$data = array();
		$user_consoles = '';
		$user_genres = '';



		if (!empty($user)) {



			foreach ($user as $key => $value) {



				$obj_user = new DtUser($value['User']);



				$data[$key]['User'] = $obj_user->get_field();
			}

			$user_consoles = $this->query("call get_user_all_consoles('" . $data[0]['User']['id'] . "')");

			$user_genres = $this->query("call get_user_all_genres('" . $data[0]['User']['id'] . "')");
		}


		if (!empty($user_consoles)) {



			foreach ($user_consoles as $key => $value) {



				$data[0]['UserConsole'][$key]['user_id'] = $value['users_consoles']['user_id'];

				$data[0]['UserConsole'][$key]['console_id'] = $value['consoles']['id'];

				$data[0]['UserConsole'][$key]['console'] = $value['consoles']['console'];
			}
		} else {



			$data[0]['UserConsole'] = array();
		}



		if (!empty($user_genres)) {



			foreach ($user_genres as $key => $value) {



				$data[0]['UserGenre'][$key]['user_id'] = $value['users_genres']['user_id'];

				$data[0]['UserGenre'][$key]['genre_id'] = $value['genres']['id'];

				$data[0]['UserGenre'][$key]['genre'] = $value['genres']['genre'];
			}
		} else {



			$data[0]['UserGenre'] = array();
		}



		return $data;
	}

	function save_image($user_id, $image_path) {

		$this->query("call user_save_profile_image_path('" . $user_id . "', '" . $image_path . "')");
	}

	function is_unverified_email_exists($email) {



		$is_unverified_exist = $this->query('call is_unverified_email_exists("' . $email . '")');



		return $is_unverified_exist;
	}

	function save_record($email, $nickname, $password, $dob, $user_latitude, $user_longitude, $gcm_regid, $created) {

		$this->query('call save_record("' . $email . '", "' . $nickname . '", "' . $password . '", "' . $dob . '" ,"' . $user_latitude . '","' . $user_longitude . '","' . $gcm_regid . '","' . $created . '", @status)');

		return $this->get_status();
	}

	function verify_account($email) {

		$this->query('call verify_account("' . $email . '", @status)');

		return $this->get_status();
	}

	function save_user_social_data($data) { //pr($data) ;exit;
		$this->query("call user_signup_social(

			   '" . $data['link_id'] . "',

			   '" . $data['email_address'] . "',

			   '" . $data['image_url'] . "',

			   '" . $data['type_id'] . "',

			   '" . $data['access_token'] . "',

			   '" . $data['access_token_secret'] . "',

			   '" . $data['is_valid_token'] . "',

			   '" . $data['screen_name'] . "',

			   '" . $data['status'] . "',

			   '" . $data['ip_address'] . "',
			   '" . $data['latitude'] . "',
			   '" . $data['longitude'] . "',

				   @status 

				 )");

		$user_id = $this->get_status();

		$data = $this->get_details_by_id($user_id);

		return $data;
	}

	function save_profile_info($data) {



		$this->query("call save_user_profile_info(

			   '" . $data['nickname'] . "',

			   '" . $data['gaming_id'] . "',

			   '" . $data['address_1'] . "',

			   '" . $data['address_2'] . "',

			   '" . $data['zipcode'] . "',

			   '" . $data['state'] . "',

			   '" . $data['country'] . "',

			   '" . $data['location'] . "',

			   '" . $data['paypal_id'] . "',

			   '" . $data['contact'] . "',

			   '" . $data['profile_text'] . "',

			   '" . $data['user_id'] . "',

			   '" . $data['status'] . "'

				 )");



		return 'Saved successfully';
	}

	function get_profile_info($user_id) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$user = $this->query("call get_user_profile_info('" . $user_id . "')"); //pr($user); exit;

		$user_consoles = $this->query("call get_user_all_consoles('" . $user_id . "')");

		$user_genres = $this->query("call get_user_all_genres('" . $user_id . "')");  //pr($user_genres); 		exit;



		$data = array();



		if (!empty($user)) {



			foreach ($user as $key => $value) {

				$obj_user = new DtUser($value['users']);
				$data[$key]['User'] = $obj_user->get_field();

				if (!empty($value['users']['trusted_since'])) {
					$data[$key]['User']['trusted_since'] = date('F d, Y', strtotime($value['users']['trusted_since']));
				} else {
					$data[$key]['User']['trusted_since'] = '';
				}

				$data[$key]['User']['GamesOwned'] = $value[0]['GameCount'];

				/* 				if ($value[0]['TotalDeals'] > 0) {
				  $data[$key]['User']['Tredes'] = (($value[0]['CompletedDeals'] / $value[0]['TotalDeals']) * 100) . '%';
				  } else {
				  $data[$key]['User']['Tredes'] = '0%';
				  }
				 */
				$data[$key]['User']['CompletedDeals'] = $value[0]['CompletedDeals'];
			}
		}

		if (!empty($user_consoles)) {

			foreach ($user_consoles as $key => $value) {

				$data[0]['UserConsole'][$key]['user_id'] = $value['users_consoles']['user_id'];

				$data[0]['UserConsole'][$key]['console_id'] = $value['consoles']['id'];

				$data[0]['UserConsole'][$key]['console'] = $value['consoles']['console'];
			}
		}

		if (!empty($user_genres)) {

			foreach ($user_genres as $key => $value) {

				$data[0]['UserGenre'][$key]['user_id'] = $value['users_genres']['user_id'];

				$data[0]['UserGenre'][$key]['genre_id'] = $value['genres']['id'];

				$data[0]['UserGenre'][$key]['genre'] = $value['genres']['genre'];
			}
		}

		return $data;
	}

	function get_user_data_by_id($user_id) {

		App::uses('DtUser', 'Lib/DataTypes');

		$user = $this->query('call get_user_by_id("' . $user_id . '", @status)');
		$game_count = $this->get_status();

		$data = array();

		if (!empty($user)) {

			$obj_game = new DtGame($value['games']);
			$data['games'] = $obj_game->get_field();
		}
	}

	function gcm_regid_update($user_id, $gcm_regid) {

		$this->query('call update_gcm_regid("' . $user_id . '", "' . $gcm_regid . '")');

		return 'success';
	}

	function get_all() {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query('call get_all_users(NULL)');

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data[$key]['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function details($id) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query('call get_all_users("' . $id . '")');

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function update_user_status($id, $status) {

		$this->query("call update_user_status('" . $status . "', '" . $id . "')");
	}

	function make_user_trusted($user_id, $transaction_id) {

		$this->query("call mark_user_trusted('" . $user_id . "', '" . $transaction_id . "')");

		return 'success';
	}

	function search($keyword) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query("call search_user_by_nickname_or_email('" . $keyword . "')");

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data[$key]['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function selling_particular_game($game_id, $start = null, $limit = null) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$users = $this->query("call users_selling_specific_game('" . $game_id . "','" . $start . "', '" . $limit . "')"); //pr($users); exit;

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$image_url = '';


				if (strpos($value['users']['image_url'], 'https') === FALSE && strpos($value['users']['image_url'], 'http') === FALSE) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}


				$data[$key]['User']['user_id'] = $value['users_games']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_games']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				if (!empty($value['users']['trusted_since'])) {
					$data[$key]['User']['trusted_since'] = date('F d, Y', strtotime($value['users']['trusted_since']));
				} else {
					$data[$key]['User']['trusted_since'] = '';
				}

				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		return $data;
	}

	function buying_particular_game($game_id, $start = null, $limit = null) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$users = $this->query("call users_buying_specific_game('" . $game_id . "','" . $start . "', '" . $limit . "')"); //pr($users); exit;

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$image_url = '';


				if (strpos($value['users']['image_url'], 'https') === FALSE && strpos($value['users']['image_url'], 'http') === FALSE) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$key]['User']['user_id'] = $value['users_wishlists']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_wishlists']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				if (!empty($value['users']['trusted_since'])) {
					$data[$key]['User']['trusted_since'] = date('F d, Y', strtotime($value['users']['trusted_since']));
				} else {
					$data[$key]['User']['trusted_since'] = '';
				}


				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		return $data;
	}

	function trading_particular_game($game_id) {

		$users = $this->query("call users_trading_specific_game('" . $game_id . "', 1)"); //pr($users); exit;

		$data = array();
		$count = 0;

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$image_url = '';


				if (strpos($value['users']['image_url'], 'https') === FALSE && strpos($value['users']['image_url'], 'http') === FALSE) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$key]['User']['user_id'] = $value['users_games']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_games']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				if (!empty($value['users']['trusted_since'])) {
					$data[$key]['User']['trusted_since'] = date('F d, Y', strtotime($value['users']['trusted_since']));
				} else {
					$data[$key]['User']['trusted_since'] = '';
				}

				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		if (!empty($data)) {

			$count = count($data);
		}

		$user = $this->query("call users_trading_specific_game('" . $game_id . "', 2)"); //pr($user); exit;

		if (!empty($user)) {

			foreach ($user as $key => $value) {

				$image_url = '';

				if (strpos($value['users']['image_url'], 'https') === FALSE && strpos($value['users']['image_url'], 'http') === FALSE) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$count]['User']['user_id'] = $value['users_wishlists']['user_id'];
				$data[$count]['User']['nickname'] = $value['users']['nickname'];
				$data[$count]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$count]['User']['profile_image_url'] = $image_url;
				$data[$count]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$count]['User']['created'] = $value['users']['created'];
				$data[$count]['User']['trade'] = $value['users_wishlists']['trade'];
				$data[$count]['User']['selling'] = $value['games']['selling'];
				$data[$count]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$count]['User']['GamesOwned'] = '';

				if (!empty($value['users']['trusted_since'])) {
					$data[$count]['User']['trusted_since'] = date('F d, Y', strtotime($value['users']['trusted_since']));
				} else {
					$data[$count]['User']['trusted_since'] = '';
				}

				$games_owned = $this->query("call count_user_have_games('" . $data[$count]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$count]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}

				$count++;
			}
		}

		return $data;
	}

	function games_and_deals_count($user_id) {

		$data = array();
		$image_url = '';

		$count = $this->query('call count_games_and_deals("' . $user_id . '")');  //pr($count); exit;

		if (is_array($count) && !empty($count[0][0])) {
			if (strpos($count[0]['users']['image_url'], 'https') === FALSE && strpos($count[0]['users']['image_url'], 'http') === FALSE) {

				$image_url = SITE_URI . $count[0]['users']['image_url'];
			} else {

				$image_url = $count[0]['users']['image_url'];
			}

//		if (!empty($count[0][0])) {

			$data['GamesCount'] = array(
				'MyGames' => $count[0][0]['mygames'],
				'WishGames' => $count[0][0]['mywish'],
				'Deals' => $count[0][0]['mydeal'],
				'OpenAction' => $count[0][0]['openaction'],
				'ClosedDeals' => $count[0][0]['closeddeals'],
				'profile_image_url' => $image_url
			);
		}

		return $data;
	}

	function save_paypal_data($data) {

		$this->query('call save_payapl_return_data("' . $data->body->user_id . '", "' . $data->body->deal_id . '", "' . $data->body->txn_id . '")');


		return 'success';
	}

	function reset_password($password, $user_id) {



		$this->query('call reset_password(

			"' . $password . '",

			"' . $user_id . '",

			@status )');



		return $this->get_status();
	}

	/**

	 * Get details of users by email

	 * @param array $query_params WHERE clause

	 * @return array result

	 */
	function get_by_email($email) {



		$result = $this->query('call get_user_by_email("' . $email . '")');



		if (!empty($result))
			return $result['0'];



		return false;
	}

	/**

	 * Create new user for given params

	 *

	 * @param array $user_details User details

	 * @return type Description

	 */
	function create_user($user_details) {

		$this->query('call create_user(

			"' . $user_details['first_name'] . '", 

			"' . $user_details['last_name'] . '", 

			"' . $user_details['username'] . '", 

			"' . $user_details['password'] . '",

			"' . $user_details['group_id'] . '",

			"' . $user_details['created'] . '",

			"' . $user_details['ip_address'] . '",

			@status )');



		return $this->get_status();
	}

	public function beforeSave($options = array()) {

		if (!empty($this->data['User']['password']))
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);

		return true;
	}

	/**

	 * Signup facebook. Save details in User & UserSocialAcount tables.

	 *

	 * @param xml $xml_data user details

	 */
	function signup_with_social_account($xml_data) {



		$this->query("call signup_with_social_account(

			 '" . $xml_data . "',

			@status )");



		return $this->get_status();
	}

	/**
	 * Configure Push notification.
	 * @param data
	 */
	function push_notification($data) {
		//pr($data); die();

		$this->query("call configure_push_notification('" . $data->body->user_id . "', '" . $data->body->push_notification . "', @status)");

		return $this->get_status();
	}

	/**
	 * Update Latitude and Longitude.
	 * @param data
	 */
	function latlong_update($data) {
		//pr($data); die();

		$this->query("call latlong_update('" . $data->body->user_id . "', '" . $data->body->latitude . "', '" . $data->body->longitude . "', @status)");

		return $this->get_status();
	}

	/**
	 * Update Password.
	 * @param data
	 */
	function user_change_pwd($data) {

		$users = $this->query("call update_pwd('" . $data->body->user_id . "', '" . $data->body->new_pwd . "', '" . $data->body->confirm_pwd . "', 0)");

		if (empty($users)):

			return FALSE;

		elseif (!empty($users)):

			$this->query("call update_pwd('" . $data->body->user_id . "', '" . $data->body->new_pwd . "', '" . $data->body->confirm_pwd . "', 1)");
			return TRUE;

		endif;
	}

	/**
	 * Update Wallpapers.
	 * @param data
	 */
	function update_wallpapers($data) {
		
	}

	/**
	 * Get Archive Deals.
	 * @param data
	 */
	function archive_deals($user_id) {

		$data_buy = array();
		$data_sell = array();
		$data_exchange = array();
		$image_url = '';

		$buy = $this->query("call get_archives('" . $user_id . "', 'buy', NULL)");  //pr($buy); exit;

		if (!empty($buy) && $buy != 1) {

			foreach ($buy as $key => $val) { //pr($val); exit;
				$status1 = true;
				$status2 = true;
				$status3 = true;
				$status4 = true;
				$status5 = true;
				$status6 = true;
				$status7 = true;
				$status8 = true;

				if ($val[0]['loginsendConfirm'] == 0) {
					$status1 = false;
				}
				if ($val[0]['loginreceiveConfirm'] == 0) {
					$status2 = false;
				}
				if ($val[0]['loginsendConfirmpayment'] == 0) {
					$status3 = false;
				}
				if ($val[0]['loginreceiveConfirmpayment'] == 0) {
					$status4 = false;
				}
				if ($val['users_deals']['send_confirm'] == 0) {
					$status5 = false;
				}
				if ($val['users_deals']['receive_confirm'] == 0) {
					$status6 = false;
				}
				if ($val['users_deals']['send_confirm_payment'] == 0) {
					$status7 = false;
				}
				if ($val['users_deals']['receive_confirm_payment'] == 0) {
					$status8 = false;
				}

				$data_buy[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_buy[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_buy[$key]['games_to_buy'] = $val['users_deals']['game'];
				$data_buy[$key]['deal_insured'] = $val['users_deals']['is_ensured'];
				$data_buy[$key]['other_deal_rating'] = $val['users_deals']['rating'];
				$data_buy[$key]['login_deal_rating'] = $val[0]['rating'];
				$data_buy[$key]['amount'] = $val['deals']['amount'];
				$data_buy[$key]['last_action_by'] = $val['deals']['last_action_by'];
				$data_buy[$key]['last_action_date'] = $val['deals']['last_action_date'];
				$data_buy[$key]['is_archive'] = $val['deals']['is_archive'];
				$data_buy[$key]['comment'] = $val['deals']['comment'];
				$data_buy[$key]['seller_id'] = $val['users']['id'];
				$data_buy[$key]['seller_name'] = $val['users']['nickname'];

				$data_buy[$key]['login_send_confirm_game'] = $status1;
				$data_buy[$key]['login_send_confirm_game_time'] = $val[0]['sendconfirmtime'];
				$data_buy[$key]['login_receive_confirm_game'] = $status2;
				$data_buy[$key]['login_receive_confirm_game_time'] = $val[0]['receiveconfirmtime'];

				$data_buy[$key]['login_send_confirm_payment'] = $status3;
				$data_buy[$key]['login_send_confirm_payment_time'] = $val[0]['loginsendConfirmPaymenttime'];
				$data_buy[$key]['login_receive_confirm_payment'] = $status4;
				$data_buy[$key]['login_receive_confirm_payment_time'] = $val[0]['loginreceiveConfirmpaymenttime'];


				$data_buy[$key]['other_send_confirm_game'] = $status5;
				$data_buy[$key]['other_send_confirm_game_time'] = $val['users_deals']['send_confirm_time'];
				$data_buy[$key]['other_receive_confirm_game'] = $status6;
				$data_buy[$key]['other_receive_confirm_game_time'] = $val['users_deals']['receive_confirm_time'];

				$data_buy[$key]['other_send_confirm_payment'] = $status7;
				$data_buy[$key]['other_send_confirm_payment_time'] = $val['users_deals']['send_confirm_payment_time'];
				$data_buy[$key]['other_receive_confirm_payment'] = $status8;
				$data_buy[$key]['other_receive_confirm_payment_time'] = $val['users_deals']['receive_confirm_payment_time'];

				$data_buy[$key]['deal_accept_time'] = $val['users_deals']['action_taken_time'];


				$seller_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$seller_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$seller_profile_image = $val['users']['image_url'];
				}

				$data_buy[$key]['seller_profile_image'] = $seller_profile_image;
				$data_buy[$key]['seller_trust'] = $val['users']['trusted_user'];
				$data_buy[$key]['image_url'] = '';
				$data_buy[$key]['game_title'] = '';
				$data_buy[$key]['game_sub_title'] = '';
				$data_buy[$key]['consoles'] = '';

				$data_buy[$key]['comment'] = '';
				$data_buy[$key]['comment_date'] = '';
				$data_buy[$key]['comment_time'] = '';
				$data_buy[$key]['comment_user'] = '';

				$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

				if (!empty($deal_chat)) {

					$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
					$data_buy[$key]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
					$data_buy[$key]['comment_user'] = $deal_chat[0]['deals_chatlogs']['id_sender'];
					$data_buy[$key]['comment_date'] = $date_time[0];
					$data_buy[$key]['comment_time'] = $date_time[1];
				}

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {
						$data_buy[$key]['image_url'] .= DtGame::get_fully_qualified_image_url($image_url[0]['games']['image_url']) . ', ';
						$data_buy[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						if (!empty($image_url[0]['games']['sub_title'])) {
							$data_buy[$key]['game_sub_title'] .= $image_url[0]['games']['sub_title'] . ',';
						} else {
							$data_buy[$key]['game_sub_title'] .= '0,';
						}
						$data_buy[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_buy[$key]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$sell = $this->query("call get_archives('" . $user_id . "', 'sell', NULL)");

		if (!empty($sell) && $sell != 1) {

			foreach ($sell as $key => $val) {

				$status1 = true;
				$status2 = true;
				$status3 = true;
				$status4 = true;
				$status5 = true;
				$status6 = true;
				$status7 = true;
				$status8 = true;

				if ($val['users_deals']['send_confirm'] == 0) {
					$status1 = false;
				}
				if ($val['users_deals']['receive_confirm'] == 0) {
					$status2 = false;
				}
				if ($val['users_deals']['send_confirm_payment'] == 0) {
					$status3 = false;
				}
				if ($val['users_deals']['receive_confirm_payment'] == 0) {
					$status4 = false;
				}
				if ($val[0]['othersendConfirm'] == 0) {
					$status5 = false;
				}
				if ($val[0]['otherreceiveConfirm'] == 0) {
					$status6 = false;
				}
				if ($val[0]['othersendConfirmpayment'] == 0) {
					$status7 = false;
				}
				if ($val[0]['otherreceiveConfirmpayment'] == 0) {
					$status8 = false;
				}


				$users = $this->query("call get_archives('" . $user_id . "', NULL, '" . $val['users_deals']['deal_id'] . "' )");

				$data_sell[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_sell[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_sell[$key]['games_to_sell'] = $val['users_deals']['game'];
				$data_sell[$key]['deal_insured'] = $val['users_deals']['is_ensured'];
				$data_sell[$key]['other_deal_rating'] = $val[0]['rating'];
				$data_sell[$key]['login_deal_rating'] = $val['users_deals']['rating'];
				$data_sell[$key]['amount'] = $val['deals']['amount'];
				$data_sell[$key]['last_action_by'] = $val['deals']['last_action_by'];
				$data_sell[$key]['last_action_date'] = $val['deals']['last_action_date'];
				$data_sell[$key]['is_archive'] = $val['deals']['is_archive'];
				$data_sell[$key]['comment'] = $val['deals']['comment'];
				$data_sell[$key]['buyer_id'] = $users[0]['users']['id'];
				$data_sell[$key]['buyer_name'] = $users[0]['users']['nickname'];

				$data_sell[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_sell[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_sell[$key]['games_to_sell'] = $val['users_deals']['game'];

				$data_sell[$key]['login_send_confirm_game'] = $status1;
				$data_sell[$key]['login_send_confirm_game_time'] = $val['users_deals']['send_confirm_time'];
				$data_sell[$key]['login_receive_confirm_game'] = $status2;
				$data_sell[$key]['login_receive_confirm_game_time'] = $val['users_deals']['receive_confirm_time'];

				$data_sell[$key]['login_send_confirm_payment'] = $status3;
				$data_sell[$key]['login_send_confirm_payment_time'] = $val['users_deals']['send_confirm_payment_time'];
				$data_sell[$key]['login_receive_confirm_payment'] = $status4;
				$data_sell[$key]['login_receive_confirm_payment_time'] = $val['users_deals']['receive_confirm_payment_time'];

				$data_sell[$key]['other_send_confirm_game'] = $status5;
				$data_sell[$key]['other_send_confirm_game_time'] = $val[0]['othersendconfirmtime'];
				$data_sell[$key]['other_receive_confirm_game'] = $status6;
				$data_sell[$key]['other_receive_confirm_game_time'] = $val[0]['otherreceiveconfirmtime'];

				$data_sell[$key]['other_send_confirm_payment'] = $status7;
				$data_sell[$key]['other_send_confirm_payment_time'] = $val[0]['othersendConfirmPaymenttime'];
				$data_sell[$key]['other_receive_confirm_payment'] = $status8;
				$data_sell[$key]['other_receive_confirm_payment_time'] = $val[0]['otherreceiveConfirmpaymenttime'];

				$data_sell[$key]['deal_accept_time'] = $val['users_deals']['action_taken_time'];

				$buyer_profile_image = '';

				if (strpos($users[0]['users']['image_url'], 'https') === FALSE && strpos($users[0]['users']['image_url'], 'http') === FALSE) {

					$buyer_profile_image = SITE_URI . $users[0]['users']['image_url'];
				} else {

					$buyer_profile_image = $users[0]['users']['image_url'];
				}

				$data_sell[$key]['comment'] = '';
				$data_sell[$key]['comment_date'] = '';
				$data_sell[$key]['comment_time'] = '';
				$data_sell[$key]['comment_user'] = '';

				$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

				if (!empty($deal_chat)) {

					$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
					$data_sell[$key]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
					$data_sell[$key]['comment_user'] = $deal_chat[0]['deals_chatlogs']['id_sender'];
					$data_sell[$key]['comment_date'] = $date_time[0];
					$data_sell[$key]['comment_time'] = $date_time[1];
				}

				$data_sell[$key]['buyer_profile_image'] = $buyer_profile_image;
				$data_sell[$key]['buyer_trust'] = $users[0]['users']['trusted_user'];
				$data_sell[$key]['image_url'] = '';
				$data_sell[$key]['game_title'] = '';
				$data_sell[$key]['game_sub_title'] = '';
				$data_sell[$key]['consoles'] = '';

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_sell[$key]['image_url'] .= DtGame::get_fully_qualified_image_url($image_url[0]['games']['image_url']) . ', ';
						$data_sell[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						if (!empty($image_url[0]['games']['sub_title'])) {
							$data_sell[$key]['game_sub_title'] .= $image_url[0]['games']['sub_title'] . ',';
						} else {
							$data_sell[$key]['game_sub_title'] .= '0,';
						}
						$data_sell[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_sell[$key]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$exchange = $this->query("call get_archives('" . $user_id . "', 'exchange', NULL)");  //pr($exchange);		exit;

		if (!empty($exchange) && $exchange != 1) {

			$count = 0;
			$counter = 0;

			foreach ($exchange as $key => $val) {

				$status1 = true;
				$status2 = true;
				$status3 = true;
				$status4 = true;
				$status5 = true;
				$status6 = true;
				$status7 = true;
				$status8 = true;

				if ($val['users_deals']['send_confirm'] == 0) {
					$status1 = false;
				}
				if ($val['users_deals']['receive_confirm'] == 0) {
					$status2 = false;
				}
				if ($val['users_deals']['send_confirm_payment'] == 0) {
					$status3 = false;
				}
				if ($val['users_deals']['receive_confirm_payment'] == 0) {
					$status4 = false;
				}
				if ($val['users_deals']['send_confirm'] == 0) {
					$status5 = false;
				}
				if ($val['users_deals']['receive_confirm'] == 0) {
					$status6 = false;
				}
				if ($val['users_deals']['send_confirm_payment'] == 0) {
					$status7 = false;
				}
				if ($val['users_deals']['receive_confirm_payment'] == 0) {
					$status8 = false;
				}

				if ($count == 2) {
					$count = 0;
					$counter++;
				}

				if ($val['users']['id'] == $user_id) {

					$data_exchange[$counter]['login_user_id'] = $val['users']['id'];
					$data_exchange[$counter]['login_user_name'] = $val['users']['nickname'];

					$login_user_profile_image = '';

					if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

						$login_user_profile_image = SITE_URI . $val['users']['image_url'];
					} else {

						$login_user_profile_image = $val['users']['image_url'];
					}

					$data_exchange[$counter]['login_user_profile_image'] = $login_user_profile_image;
					$data_exchange[$counter]['login_user_trust'] = $val['users']['trusted_user'];
					$data_exchange[$counter]['login_user_game_ids'] = $val['users_deals']['game'];
					$data_exchange[$counter]['deal_insured'] = $val['users_deals']['is_ensured'];
					$data_exchange[$counter]['login_deal_rating'] = $val['users_deals']['rating'];
					$data_exchange[$counter]['login_user_trade'] = $val['users_deals']['trade'];
					$data_exchange[$counter]['deal_id'] = $val['users_deals']['deal_id'];
					$data_exchange[$counter]['last_action_by'] = $val['deals']['last_action_by'];
					$data_exchange[$counter]['last_action_date'] = $val['deals']['last_action_date'];
					$data_exchange[$counter]['is_archive'] = $val['deals']['is_archive'];
					$data_exchange[$counter]['comment'] = $val['deals']['comment'];
					$data_exchange[$counter]['deal_creater'] = $val['deals']['offered_by'];
					$data_exchange[$counter]['deal_amount'] = $val['deals']['amount'];
					$data_exchange[$counter]['login_user_game_image_url'] = '';
					$data_exchange[$counter]['login_user_game_title'] = '';
					$data_exchange[$counter]['login_user_game_sub_title'] = '';
					$data_exchange[$counter]['login_user_game_consoles'] = '';

					$data_exchange[$counter]['login_send_confirm_game'] = $status1;
					$data_exchange[$counter]['login_send_confirm_game_time'] = $val['users_deals']['send_confirm_time'];
					$data_exchange[$counter]['login_receive_confirm_game'] = $status2;
					$data_exchange[$counter]['login_receive_confirm_game_time'] = $val['users_deals']['receive_confirm_time'];

					$data_exchange[$counter]['login_send_confirm_payment'] = $status3;
					$data_exchange[$counter]['login_send_confirm_payment_time'] = $val['users_deals']['send_confirm_payment_time'];
					$data_exchange[$counter]['login_receive_confirm_payment'] = $status4;
					$data_exchange[$counter]['login_receive_confirm_payment_time'] = $val['users_deals']['receive_confirm_payment_time'];

					$data_exchange[$counter]['deal_accept_time'] = $val['users_deals']['action_taken_time'];


					$game_ids = explode(',', $val['users_deals']['game']);

					$count_game = count($game_ids);

					for ($i = 0; $i < $count_game; $i++) {

						$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

						if (!empty($image_url)) {

							$data_exchange[$counter]['login_user_game_image_url'] .= DtGame::get_fully_qualified_image_url($image_url[0]['games']['image_url']) . ', ';
							$data_exchange[$counter]['login_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
							if (!empty($image_url[0]['games']['sub_title'])) {
								$data_exchange[$counter]['login_user_game_sub_title'] .= $image_url[0]['games']['sub_title'] . ',';
							} else {
								$data_exchange[$counter]['login_user_game_sub_title'] .= '0,';
							}
							$data_exchange[$counter]['login_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
						}
					}

					$data_exchange[$counter]['comment'] = '';
					$data_exchange[$counter]['comment_date'] = '';
					$data_exchange[$counter]['comment_time'] = '';
					$data_exchange[$counter]['comment_user'] = '';

					$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

					if (!empty($deal_chat)) {

						$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
						$data_exchange[$counter]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
						$data_exchange[$counter]['comment_user'] = $deal_chat[0]['deals_chatlogs']['id_sender'];
						$data_exchange[$counter]['comment_date'] = $date_time[0];
						$data_exchange[$counter]['comment_time'] = $date_time[1];
					}

					$count++;
					continue;
				}

				$data_exchange[$counter]['other_user_id'] = $val['users']['id'];
				$data_exchange[$counter]['other_user_name'] = $val['users']['nickname'];

				$other_user_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$other_user_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$other_user_profile_image = $val['users']['image_url'];
				}

				$data_exchange[$counter]['other_user_profile_image'] = $other_user_profile_image;
				$data_exchange[$counter]['other_user_trust'] = $val['users']['trusted_user'];
				$data_exchange[$counter]['other_user_game_ids'] = $val['users_deals']['game'];
				$data_exchange[$counter]['other_user_trade_opt'] = $val['users_deals']['trade'];
				$data_exchange[$counter]['other_deal_rating'] = $val['users_deals']['rating'];
				$data_exchange[$counter]['other_user_game_image_url'] = '';
				$data_exchange[$counter]['other_user_game_title'] = '';
				$data_exchange[$counter]['other_user_game_consoles'] = '';

				$data_exchange[$counter]['other_send_confirm_game'] = $status5;
				$data_exchange[$counter]['other_send_confirm_game_time'] = $val['users_deals']['send_confirm_time'];
				$data_exchange[$counter]['other_receive_confirm_game'] = $status6;
				$data_exchange[$counter]['other_receive_confirm_game_time'] = $val['users_deals']['receive_confirm_time'];

				$data_exchange[$counter]['other_send_confirm_payment'] = $status7;
				$data_exchange[$counter]['other_send_confirm_payment_time'] = $val['users_deals']['send_confirm_payment_time'];
				$data_exchange[$counter]['other_receive_confirm_payment'] = $status8;
				$data_exchange[$counter]['other_receive_confirm_payment_time'] = $val['users_deals']['receive_confirm_payment_time'];

				$game_ids = explode(',', $val['users_deals']['game']);

				$data_exchange[$counter]['other_user_game_sub_title'] = '';

				$count_game = count($game_ids);

				for ($i = 0; $i < $count_game; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_exchange[$counter]['other_user_game_image_url'] .= DtGame::get_fully_qualified_image_url($image_url[0]['games']['image_url']) . ', ';
						$data_exchange[$counter]['other_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
						if (!empty($image_url[0]['games']['sub_title'])) {
							$data_exchange[$counter]['other_user_game_sub_title'] .= $image_url[0]['games']['sub_title'] . ',';
						} else {
							$data_exchange[$counter]['other_user_game_sub_title'] .= '0,';
						}
						$data_exchange[$counter]['other_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$count++;

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_exchange[$counter]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$data = array(
			'buy' => $data_buy,
			'sell' => $data_sell,
			'exchange' => $data_exchange
		);

		return $data;
	}

	function check_user_existance($email) {

		return $this->query("call check_user_existance( '" . $email . "' )");
	}

}

