<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class Genre extends AppModel {

	function get_all_genres() {

		App::uses('DtGenre', 'Lib/DataTypes');

		$genres = $this->query("call get_all_genre()");

		$data = array();

		if (!empty($genres)) {

			foreach ($genres as $key => $value) {

				$obj_genre = new DtGenre($value['Genre']);

				$data[$key]['Genre'] = $obj_genre->get_field();
			}
		}

		return $data;
	}

	function save_genre($data) {

		foreach ($data as $value) {

			$name = Sanitize::escape($value);

			$genre = $this->query("call check_genre_or_console('" . $name . "', NULL)");

			if (empty($genre)) {

				$this->query("call genre_cron( '" . $name . "' )");
			}
		}
	}

}
