@charset "utf-8";
/** ============================== Blue ============================== **/

html { 					color: #3b3b3b;				/** Overall font body color **/ }




/** ============================== background for main content area ============================== **/

#content_bg { background: #bfc0c7 url(../../img/content_bg.jpg) repeat-x; }



/**  ============================== Anchor Styling  ============================== **/

a { 					color: #00339b; }
a:hover { 				color: #000; }




/** ============================== Element Styling ============================== **/

h1 { 					color: #202020; }
h2 { 					color: #4d4d4d; }
h3 { 					color: #161616; }
h4 { 					color: #373737; }
h5 { 					color: #727272; }
h6 { 					color: #373737; }

hr { 					color: #c0c0c0;
						border-bottom-color: #c0c0c0;
						}
blockquote { 			color: #000;
						background-image: url(../../img/blockquote_bg.gif);
						}



/** ============================== Form Styling ============================== **/

fieldset { 				border:1px solid #d2d1d1; }
fieldset legend { 		color: #3a3a3a; }

form {
						margin-bottom: 20px;}
						
form label { 			color: #585858;
						float: left;
						line-height: 4px;
						margin-right: 10px;	}
						
form input {
						padding: 8px;
width: 680px;
float: left;
margin-right: 5px;
						}
form input[type="submit"] {
						background: #333;
						border: 0px;
						color: #fff;
						padding: 8px 20px;
						cursor: pointer;
						width:125px;
						}											
						
form p small { 			color: #777; }
form textarea, form .wysiwyg {
						color:#111111; }
form input.textfield, form select, form textarea, form .wysiwyg {
						background: #ffffff url(../../img/textfield_bg.gif) repeat-x top; /*Define background for input elements here*/
						border: 1px solid #d2d1d1;
						}
/************** Focus styles **************/ 
form input.textfield:focus, form select:focus, form textarea:focus {
						background: #fbfafb url(../../img/textfield_active_bg.gif) repeat-x top; /*Define bacground for focussed form elements here*/
						border: 1px solid #aeaeae;
						}
/************** Error styles ***************/ 
form input.error, form select.error, form textarea.error, form .wysiwyg.error, form input.error:focus, form select.error:focus, form textarea.error:focus, form .wysiwyg.error:focus { 	
						border: 1px solid #ff8388; }

/************** Disabled form Elements *************/
button[disabled]:active, button[disabled], input[type="reset"][disabled]:active, input[type="reset"][disabled], input[type="button"][disabled]:active, input[type="button"][disabled], select[disabled] > input[type="button"], select[disabled] > input[type="button"]:active, input[type="submit"][disabled]:active, input[type="submit"][disabled], .textfield[disabled], .textfield_large[disabled] {
						border: 1px solid #d6d6d6;
						color: GrayText;
						cursor: inherit;
						background: #dbdbdb url(none);
						}
/************** Form Hints and Errors *********/

.form_error { 			color: #ca0000;
						background: url(../../img/form_error.png) no-repeat;
						}
.form_hint { 			color: #7c7c7c;
						background: url(../../img/form_info.png) no-repeat;
						}
						


/** ============================== Buttons ============================== **/

.button { 				background: #fff url(../../img/button_bg.gif) repeat-x left bottom;
						border: 1px solid #c6c6c6;
						color: #595959;
						}
.button:hover { 		background: #fff url(../../img/button_bg_hover.gif) repeat-x left bottom;
						border: 1px solid #c6c6c6;
						}
.button2 { 				background: #7c7c7c url(../../img/button2_bg.gif) repeat-x left bottom;
						border: 1px solid #868789;
						color: #fff;
						}
.button2:hover { 		background: #4b4b4b url(../../img/button2_bg_hover.gif) repeat-x left bottom;
						border: 1px solid #868789;
						}

						
						


/** ============================== Header ============================== **/

#header_bg { 			background: url(../../img/header_bg.png) repeat-x; }
#header_top { 			color: #ffffff; }
#header_top a { 		color:#fff; }

/********* Logo *********/
#logo { 				background: url(../../img/logo.png) no-repeat left bottom;   /*Specify logo Here*/
						height: 33px;
						width: 472px;
						}
/********* Search field in header ***/
.search_field { 		border: 1px solid #a2a2a2; }





/** ============================== Main Navigation ============================== **/

#main_nav a { color:#242424; }			/* main navigation link color */
#main_nav a:hover { 	color:#fff !important;
						background: #242424 url(../../img/nav_active_tab_bg.gif) repeat-x left top;
						border-radius: 0px; }				/* main navigation hover link color */
#main_nav li a.current { 							/* main navigation active link color */
						color:#fff !important;
						background: #242424 url(../../img/nav_active_tab_bg.gif) repeat-x left top;
						border-radius: 0px;
						}
						



/** ============================== Sub Navigation ============================== **/

#main_nav li ul li a { color: #b6b6b6; } /* Sub navigation link color */
#main_nav li ul li a:hover {
						color: #fff; }/* Sub navigation hover link color */
#main_nav li ul li a.current {
						background:url(../../img/active_sub_menu_bg.gif) no-repeat center bottom; /* Pointer for active link in Sub navigation */
						color:#fff;/* Sub navigation active link color */
						}
						
						
						
/** ============================== Navigation in box header ============================== **/

.box .header .sub_nav li a {
						color:#494949; }
.box .header .sub_nav li a:hover {
						color:#000; }
.sub_nav .current, .sub_nav .current:hover, .sub_nav .current a {
						background:#FFF;
						color:#000 !important;
						}



/** ============================== Vertical Nav ============================== **/

.vertical_nav li a { 	color:#333; }
.vertical_nav li a:hover {
						color:#000; }
.vertical_nav li .current {
						background:url(../../img/vertical_nav_bg.gif) no-repeat left top;
						color:#000 !important;
						}
/*****Box body with vertical Navigation *****/
.body_vertical_nav { 	background: url(../../img/vertical_nav_bg.jpg) repeat-y left top; }





/** ============================== Horizontal Nav ============================== **/

.horizontal_nav { 		background: #FFF url(../../img/horizontal_nav_bg.gif) repeat-x left bottom; }
.horizontal_nav li a { 	color:#333; }
.horizontal_nav li a:hover {
						color:#000; }
.horizontal_nav li a.current{
						background:#FFF;
						border-top: 1px solid #bdbdbd;
						border-right: 1px solid #bdbdbd;
						border-left: 1px solid #bdbdbd;
						border-bottom-style: none;
						color:#000 !important;
						}
						
						


/** ============================== Accordion ============================== **/

#accordion, .accordion { background-color:#fff;
						border:1px solid #e6e6e6;
						}
/* accordion header */
#accordion h2, .accordion h2  { 		background:#f7f8f8 url(../../img/accordion_heading_bg.gif) repeat-x bottom;
						color:#111010;
						border:1px solid #fff;
						border-bottom:1px solid #ddd;
						}
/* currently active header */
#accordion h2.current ,  .accordion h2.current{ background-color:#fff; }
/* Accordion pane */
#accordion div.pane, .accordion div.pane { 	color:#5e5e5e;
						background: url(../../img/pane_bg.gif) repeat-x top;
						}





/** ============================== Small Box ============================== **/

.small_box { 			background:#fdfdfd; }
.small_box .header { 	background: url(../../img/small_box_header_bg.gif) repeat-x bottom;
						color:#2a2a2a;
						}






/** ============================== Box ============================== **/

.box { 					background:#fdfdfd; }
.box .header {
						background: url(../../img/box_header_bg.gif) repeat-x bottom;
						color:#4e4e4e;
						}






/** ============================== Tables ============================== **/

.grid_table {
						border-top: 1px solid #dddddd;
						border-bottom: 5px solid #a5a5a5;
						border-right: 1px solid #dddddd;
						border-left: 1px solid #dddddd;
						color:#202020;
						}
.grid_table th {
						color: #3a3a3a;
						text-align: left;
						border-bottom: 4px solid #a5a5a5;
						}
.grid_table td { 		border-bottom: 1px solid #d6d6d6;
						text-align: left;
						vertical-align:middle;
						}

/******* Hover styles for grid items *******/
.grid_table thead tr:hover {
						background: #fff; }
.grid_table th:hover { 	background: #fffaea; }
.grid_table tr:hover { 	color: #000;
						background: #fffaea;
						}

/*********** Selected items and dropdown ************/
tr.grid_dropdown, tr.active {
						color:#000;
						}
tr.active td { 			background: #f9f9f9 url(../../img/grid_active_bg.gif) repeat-x bottom; }
tr.grid_dropdown td { 	background: #f7f7f6 url(../../img/grid_dropdown_bg.gif) repeat-x;
						border-bottom: 1px solid #bcbcbc;
						}
/************ Minimize/Mazimize controls *****************/
.grid_table .toggle { 	background:url(../../img/expand_collapse.png) top left; /*Sprite size should be 10 x 10 pixels or change in style.css */ }






/** ============================== Data Tables ============================== **/

table.display thead th { border-bottom: 4px solid #a5a5a5; }

table.display tfoot th { border-top: 4px solid #a5a5a5; }
						
						
						
						
/** ============================== Pagination ============================== **/

.pagination { 			background: url(../../img/pagination_bg.gif) repeat-x top; /*pagination bg */ }
.pagination li a { 		color:#595959; } /*pagination anchor color */
.pagination li a:hover {
						color:#000; } /*pagination anchor color on hover*/
.pagination li.active a {
						color:#000;  /*active pagination anchor color*/
						background:url(../../img/pagination_active.png) no-repeat top;  /*active pagination pointer*/
						}
						



/** ============================== Footer ============================== **/

#footer { 				background:url(../../img/footer_bg.gif) repeat-x top #282828;
						color:#CCC;
						}


#footer a {				color:#fff;
						text-decoration:underline;
						}


						
/** ==============================  Bulleted List ============================== **/

.bulleted_list li { 	background: url(../../img/bullet_right.png) no-repeat 0px 7px; /* List with custom bullets */ }





/**  ==============================  Users List  ============================== **/

.user_list li { 		border-bottom: 1px solid #CCC; }




/**  ==============================  Borders  ============================== **/

.border_blue { border-bottom: solid 1px #393939; }
.border_lt_blue { border-bottom: solid 6px #bebebe; }
.border_grey { 			border-bottom: solid 6px #e7e7e7; }





/** ============================== Display styles for Error Messages, Notifications and General Info ============================== **/

div.info { 				background: #CADCFF url(../../img/info_box_bg.gif) repeat-x left top;
						border: 1px solid #c3d7ff;
						color: #3760b5;
						}
div.warning { 			background: #fff6d0 url(../../img/warning_box_bg.gif) repeat-x left top;
						border: 1px solid #bc6c34;
						color: #bc6c34;
						}
div.error { 			background: #ffd6d0 url(../../img/error_box_bg.gif) repeat-x left top;
						border: 1px solid #ff0606;
						color: #b22723;
						}



/** ============================== Tooltip ============================== **/

.tooltip, .tooltip.bottom {
						background:#000;
						color:#fff;
						}




/** ============================== Jump Menu ============================== **/

.jump_menu_btn { 		background:url(../../img/jump_menu.png) top left;  /*Sprite for jump button 158 x 28px*/ }
.jump_menu_list { 		background:#f5f5f5;
						border:solid 1px #d5d5d5;
						}
.jump_menu_list li a{
						color:#333;
	}
.jump_menu_list li a:hover {
						color:#000;
						background:#fff;
						}



/** ============================== Login Page ==================================== **/

body#login_page { 		background-color:#bfc0c7; } /*Background color for login page*/




/** ============================== Overlay ============================== **/

#facebox { 				/* border color */
						border:10px solid #666;
						}
#facebox div { 			border:1px solid #3B5998;
						background-color:#fff;
						}
#facebox h2 { 			background: #FFF url(../../img/box_header_bg.gif) repeat-x;
						}
#exposeMask { 			background:#000 !important; }/* background color for overlay and expose mask */ 




/** ============================== Calendar ============================== **/
.jCalMo .overDay {		color:#FFF; background:#000; } /* Hover on a single date */


/**++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/
/**==================             End of Theme                     ==================**/
/**++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++**/


/************                     Center content          **************/
#content-center {
	margin-left: 30px;
}




/** ============================== Edit Form ============================== **/

.input.text {
	display: inline-block;
	width: 100%;
}


.input.text label {
	min-width: 100px;
}


.input.textarea {
	display: inline-block;
	width: 100%;
}

.input.textarea label {
	min-width: 100px;
}

.input.textarea textarea {
	width: 686px;
	border: 1px solid #ABADB3;
}

.preview_img {
	position: absolute;
	top: 70px;
	right: 15px;
}
.input.checkbox {
	display: inline-block;
}

.input.checkbox input[type="checkbox"], .input.checkbox input[type="radio"] {
	padding: 0;
	background: none;
	border: 0;
	width: auto;
}

.input.checkbox label {
	display: block;
	padding: 0px;
	margin: 0px;
	line-height: 20px;
}

.submit {
display: inline-block;
}


.searchform .input.text {
display: block;
width: 100%;
}

/** ============================== Edit Form ============================== **/
