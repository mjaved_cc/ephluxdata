$(document).ready(function() {
	$('#UserResetAccountPasswordForm').validate({
		rules: {
			"data[User][password]": {
				required : true,
				minlength : 8
			},
			"data[User][confirm_password]": {
				required : true,
				equalTo: "#UserPassword"
			}
		}
	});

	//$('#UserSignupForm').submit(function () { return false;})

});