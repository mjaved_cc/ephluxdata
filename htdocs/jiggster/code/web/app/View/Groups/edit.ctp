<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">							
				<ul class="bulleted_list">
					<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?></li>
					<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
					<li><?php echo $this->Html->link(__('Permissions'), array('controller' => 'groups', 'action' => 'edit_permissions', $this->request->data['Group']['id'])); ?> </li>

				</ul>			
		</div>
	</div>

</div>


<div class="main_column">
	<div class="box">
		<div class="body">

			<?php echo $this->Form->create('Group'); ?>
			<fieldset>
				<legend><?php echo __('Edit Group'); ?></legend>

				<?php echo $this->Form->input('id'); ?> 

				<p><?php echo $this->Form->input('name', array('class' => 'textfield large')); ?></p>

				<?php echo $this->Form->submit('Submit', array('class' => 'button2')); ?>
			</fieldset>


		</div>
	</div>
</div>
