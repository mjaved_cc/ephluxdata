<?php
$page = json_decode($page);
//pr($page); //exit;
?>

<div class="main_column">
	<div class="box">
		<div class="body">

			<?php echo $this->Form->create('Page', array('controller' => 'pages', 'action' => 'edit')); ?>

			<p> <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $page->body->Page->id)); ?> <p>
			<p> <?php echo $this->Form->input('title', array('label' => 'Title', 'value' => $page->body->Page->title)); ?> <p>
			<p> <?php echo $this->Form->input('desc', array('label' => 'Description', 'value' => $page->body->Page->description)); ?> <p>
			<p> <?php echo $this->Form->input('page_type', array('type' => 'hidden', 'value' => $page->body->Page->page_type)); ?> <p>


			<p>  <?php echo $this->Form->submit('update'); ?> </p>


		</div>
	</div>
</div>