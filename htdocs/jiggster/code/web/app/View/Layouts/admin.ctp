<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$desc = __d('cake_dev', SITE_NAME);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<!--============================Head============================-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex,nofollow" />

		<!--=========Title=========-->
		<title>
			<?php echo $desc ?>:
			<?php echo $title_for_layout; ?>
		</title>

		
		<!--=========Stylesheets=========-->
		<?php echo $this->Minify->css($css_list); ?>
		<!--[if lt IE 8]>
					<link href="css/ie.css" rel="stylesheet" type="text/css"/>
		<![endif]-->


	</head>
	<!--End Head-->


	<!--============================Body============================-->
	<body>

		<!--==================== Header =======================-->
		<div id="header_bg">

			<!--============Header Wrapper============-->
			<div class="wrapper">

				<!--=======Top Header area======-->
				<div id="header_top">
					<span class="fr"> <a href="<?php echo SITE_URI.'users/logout' ?>">Logout</a><?php // echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout')); ?> </span> <!--Float links to left-->
				</div>
				<!--End Header top Area=-->

				<!--=========Header Area including search field and logo=========-->
				<div class="header_main clearfix">

					<!--===Search field===-->
<!--					<div class="header_search">
						<a href="#"><img src="<?php // echo $this->webroot; ?>img/search_icon.png" alt="Search" width="21" height="20" class="search_icon" /></a>
						<input name="textfield" type="text"  id="textfield" class="search_field" />
					</div>-->

					<!--===Logo===-->
					<a  href="#"><h1>Jiggster Admin Panel</h1></a>

				</div>
				<!--End Search field and logo Header Area-->

				<!--=========Main Navigation=========-->
				<ul id="main_nav">
					<li> <a href="<?php  echo SITE_URI.'games/index' ?>">Games</a>
<!--						<ul>
							<li><a href="#" >Home</a></li>
							<li><a href="#" rel="#facebox">Open Modal</a></li>
						</ul>-->
					</li>
					
					<li> <a href="<?php  echo SITE_URI.'pages/index' ?>">Pages</a>
<!--						<ul>
							<li><a href="#" >Home</a></li>
							<li><a href="#" rel="#facebox">Open Modal</a></li>
						</ul>-->
					</li>
					
					<li> <a href="<?php  echo SITE_URI.'games/most_popular_games' ?>">Reports</a>
<!--						<ul>
							<li><a href="#" >Home</a></li>
							<li><a href="#" rel="#facebox">Open Modal</a></li>
						</ul>-->
					</li>
					
					<li> <a href="<?php  echo SITE_URI.'users/all' ?>">Users</a>
<!--						<ul>
							<li><a href="#" >Home</a></li>
							<li><a href="#" rel="#facebox">Open Modal</a></li>
						</ul>-->
					</li>
					
<!--					<li> <a href="<?php //  echo SITE_URI.'pages/email_templates' ?>">Email Templates</a>
						<ul>
							<li><a href="#" >Home</a></li>
							<li><a href="#" rel="#facebox">Open Modal</a></li>
						</ul>
					</li>-->
					
									
								
								
				</ul>
				<!--End Main Navigation-->

				<!--=========Jump Menu=========-->
<!--				<div class="jump_menu">
					<a href="#" class="jump_menu_btn">Jump To</a>
					<ul class="jump_menu_list">
						<li><a href="#"><img src="<?php // echo $this->webroot; ?>img/users2_icon.png" alt="" width="24" height="24" />Users</a></li>
						<li><a href="#"><img src="<?php // echo $this->webroot; ?>img/tools_icon.png" alt="" width="24" height="24" />Settings</a></li>
						<li><a href="#"><img src="<?php // echo $this->webroot; ?>img/messages_icon.png" alt="" width="24" height="24" />Messages</a></li>
						<li><a href="#"><img src="<?php // echo $this->webroot; ?>img/key_icon.png" alt="" width="24" height="24" />Credentials</a></li>
						<li><a href="#"><img src="<?php // echo $this->webroot; ?>img/documents_icon.png" alt="" width="24" height="24" />Pages</a></li>
					</ul>
				</div>-->
				<!--End Jump Menu-->

			</div>
			<!--End Wrapper-->
		</div>
		<!--End Header-->
		<div id="content_bg" class="clearfix">
			<div class="content wrapper clearfix">
				<?php echo $this->fetch('content'); ?>
			</div>
			
		</div>



		<!--============================Footer============================-->
		<div id="footer">
			<div class="wrapper">
				Copyright (C) 2009    Your Company. All Rights Reserved. Powered by <a href="http://themeforest.net/item/evolution-advanced-admin-theme/113704?ref=cosmive">Evolution Admin</a>.
			</div>
		</div>
		<!--End Footer-->

		<!--=========Javascipts=========-->
		<?php echo $this->Minify->script($js_list); ?>
	</body>
	<pre><?php //echo $this->element('sql_dump'); ?></pre>
	<!--End Body-->



</html>