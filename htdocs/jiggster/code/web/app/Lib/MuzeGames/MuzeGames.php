<?php

require_once "Games.php";

/**
 * MuzeGames
 */

class MuzeGames{


    /** MuzeGames dir path
     *
     */
    const DATAFILES = MUZE_GAME_TMP_LOCATION_TXT_UNZIPPED_DIR ;

    /**
     * @var string
     */
    private $delimiter = '|';

    public function __construct()
    {
    }

    public function readFile($filename)
    {
        $data = file($filename);
        return $data;
    }

    /**
     * Get all games from Product.txt
     *
     * <code>
     * Array
    (
    [0] => ProductID
    [1] => Platform
    [2] => TitleID
    [3] => Cat
    [4] => Barcode
    [5] => Subtitle
    [6] => Dealer
    [7] => SRP
    [8] => ReleaseDate
    [9] => ReleaseDateText
    [10] => DeleteDate
    [11] => ConsumerAdvice
    [12] => UNAV
    [13] => Essent
    [14] => MinOS
    [15] => RecOS
    [16] => MinCPU
    [17] => RecCPU
    [18] => MinRAM
    [19] => RecRAM
    [20] => MinGraphics
    [21] => RecGraphics
    [22] => MinStorage
    [23] => RecStorage
    [24] => DXVersion
    [25] => OpticalDrive
    [26] => Vols
    )
     * </code>
    */
    public function getGames()
    {
        $resource = self::DATAFILES.'Product.txt';

        $games = array_slice($this->readFile($resource), 1);
        $data = array();
//        $count = 1;

        if(!empty($games) && is_array($games)){
            foreach($games as $key => $records)
            {

                $game = new Games();
                $attributes = explode($this->delimiter,$records);

                $game->api   =  (int) $attributes[0];
                $game->title = (string) $this->getTitle($attributes[2]);
                $game->titleID = (string) $attributes[2];
                $game->desc = (string) $this->getDescription($attributes[0]);
                $game->console = (string) $attributes[1];

                // array genres
                $aGenres = $this->getProductGenre($attributes[0]);
                if(!empty($aGenres) && is_array($aGenres)){
                    $game->genre = (string) implode(',', $aGenres);
                }

                $game->screenshot = (string) $this->getScreenshot($attributes[0]);
                $game->publisher =  (string) $this->getCompany($this->getPublisher($attributes[0]));
                $game->developers =  (string) $this->getCompany($this->getDeveloper($attributes[0]));
                $game->barcode = (string) $attributes[4];
                $game->price = (string) $attributes[7];
                $game->image = $this->getCoverImage($attributes[0]);
                $game->releaseDate = strtotime($attributes[8]);
                $game->deleteDate = strtotime($attributes[10]);
                $game->subTitle = Sanitize::escape($attributes[5]);

                $data[$key] = $game;

//            if($count == 25): break; else: $count++; endif;
            }
        }

        return $data;

    }

    public function getTitle($titleID)
    {
        $resource = self::DATAFILES.'Title.txt';

        $titles = array_slice($this->readFile($resource), 1);

        if(!empty($titles) && is_array($titles)){
            foreach($titles as $records)
            {
                $attributes = explode($this->delimiter,$records);

                $pattern = '/^' . preg_quote($attributes[0], '/') . '$/';
                if (preg_match($pattern, $titleID))
                {
                    return Sanitize::escape($attributes[2]);
                    break;
                }

            }
        }
    }

    public function getDescription($gameID)
    {
        $resource = self::DATAFILES.'ProductAnnotation.txt';

        $description = array_slice($this->readFile($resource), 1);

        if(!empty($description) && is_array($description)){
            foreach($description as $records)
            {
                $attributes = explode($this->delimiter,$records);

                $pattern = '/^' . preg_quote($attributes[0], '/') . '$/';

                if (preg_match($pattern, $gameID))
                {
                    return Sanitize::escape($attributes[3]);
                    break;
                }
            }
        }
    }

    public function getCoverImage($gameID)
    {
        $resource = self::DATAFILES.'ProductImage.txt';

        $image = array();

        $coverImages = array_slice($this->readFile($resource), 1);

        if(!empty($coverImages) && is_array($coverImages)){
            foreach($coverImages as $records)
            {
                $image_attributes = explode($this->delimiter,$records);

                $pattern1 = '/^' . preg_quote($image_attributes[0], '/') . '$/';
                $pattern2 = '/^' . preg_quote($image_attributes[3], '/') . '$/';

                if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Cover'))
                {
                    $image[$image_attributes[1]] = $image_attributes[2];

                    break;
                }
            }
        }

        return (!empty($image)) ? Sanitize::escape(json_encode($image)) : '';
    }

    public function getScreenshot($gameID)
    {
        $resource = self::DATAFILES.'ProductImage.txt';

        $Screenshots = array_slice($this->readFile($resource), 1);

        if(!empty($Screenshots) && is_array($Screenshots)){
            foreach($Screenshots as $records)
            {
                $attributes = explode($this->delimiter,$records);

                $pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
                $pattern2 = '/^' . preg_quote($attributes[3], '/') . '$/';

                if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Screenshot'))
                {

                    $screenshot[$attributes[1]] = $attributes[2];
                }
            }
        }

        $screenshot = (!empty($screenshot)) ? Sanitize::escape(json_encode($screenshot)) : '';
        return $screenshot;
    }

    public function getProductGenre($gameID)
    {
        $resource = self::DATAFILES.'ProductGenre.txt';
        $genres = array();
        $productGenre = array_slice($this->readFile($resource), 1);

        if(!empty($productGenre) && is_array($productGenre)){
            foreach($productGenre as $records)
            {
                $genre_attributes = explode($this->delimiter,$records);

                $pattern = '/^' . preg_quote($genre_attributes[1], '/') . '$/';

                if (preg_match($pattern, $gameID))
                {
                    $genres[] = $genre_attributes[2];
                }
            }
        }

        return $genres;
    }

    public function getPublisher($gameID)
    {
        $resource = self::DATAFILES.'ProductCompany.txt';

        $ProductCompany = array_slice($this->readFile($resource), 1);

        if(!empty($ProductCompany) && is_array($ProductCompany)){
            foreach($ProductCompany as $records)
            {
                $attributes = explode($this->delimiter,$records);
                $publisher = trim($attributes[2]);
                $pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
                $pattern2 = '/^' . preg_quote($publisher, '/') . '$/';

                if ( preg_match($pattern1, $gameID) && preg_match($pattern2, 'Publisher') )
                {
                    return $attributes[1];
                    break;
                }
            }
        }
    }

    public function getDeveloper($gameID)
    {
        $resource = self::DATAFILES.'ProductCompany.txt';

        $ProductCompany = array_slice($this->readFile($resource), 1);

        if(!empty($ProductCompany) && is_array($ProductCompany)){
            foreach($ProductCompany as $records)
            {
                $attributes = explode($this->delimiter,$records);
                $developer = trim($attributes[2]);
                $pattern1 = '/^' . preg_quote($attributes[0], '/') . '$/';
                $pattern2 = '/^' . preg_quote($developer, '/') . '$/';

                if (preg_match($pattern1, $gameID) && preg_match($pattern2, 'Developer'))
                {
                    return $attributes[1];
                    break;
                }
            }
        }
    }

    public function getCompany($companyID)
    {
        //print $companyID;
        $resource = self::DATAFILES.'Company.txt';

        $Company = array_slice($this->readFile($resource), 1);

        if(!empty($Company) && is_array($Company)){
            foreach($Company as $records)
            {
                $attributes = explode($this->delimiter,$records);

                $pattern = '/^' . preg_quote($attributes[0], '/') . '$/';

                if (preg_match($pattern, $companyID))
                {
                    return $attributes[1];
                    break;
                }
            }
        }
    }

    public function getAllConsole()
    {
        $resource = self::DATAFILES.'Product.txt';
        $games = array_slice($this->readFile($resource), 1);
        $data = array();

        if(!empty($games) && is_array($games)){
            foreach($games as $records)
            {
                $attributes = explode($this->delimiter,$records);
                $data[]	= $attributes[1];
            }
        }
        return array_unique($data);
    }

    public function getAllGenre()
    {
        $resource = self::DATAFILES.'ProductGenre.txt';
        $genres = array_slice($this->readFile($resource), 1);
        $data = array();

        if(!empty($genres) && is_array($genres)){
            foreach($genres as $records)
            {
                $attributes = explode($this->delimiter,$records);
                $data[]	= $attributes[2];
            }
        }
        return array_unique($data);
    }

    public function getSimilarGames()
    {
        $resource = self::DATAFILES.'Similar.txt';
        $games = array_slice($this->readFile($resource), 1);
        $data = array();
        $count = 0;

        if(!empty($games) && is_array($games)){
            foreach($games as $records)
            {
                $attributes = explode($this->delimiter,$records);
                $data[$count]['TitleID']	= $attributes[0];
                $data[$count]['SimilarTitleID']	= $attributes[1];
                $count++;

            }
        }
        return $data;
    }

    /**
     * Move images from temporary location to our desired location i.e webroot/muze_games/last_two_digit_of_image_folder
     */
    function move_images_from_tmp(){

        set_time_limit(0);

        $tmp_base_path = MUZE_GAME_TMP_LOCATION_IMAGES_UNZIPPED ;
        $all_files_folders = scandir($tmp_base_path);
        $i = 0 ;
        foreach ($all_files_folders as $fileInfo) {

            if($fileInfo == '.' || $fileInfo == '..' || $fileInfo == '.svn' || is_dir($tmp_base_path . $fileInfo)) continue;

            $this->_copy_file_with_dir($fileInfo);

        }

    }

    private function _get_last_two_letter($file_name){

        $file_details = pathinfo($file_name);

        $last_two_letter = substr($file_details['filename'] , -2) ;

        return $last_two_letter ;
    }

    private function _get_abs_path($file_name){

        $relative_path = join(DS, array($this->_get_last_two_letter($file_name) , $file_name));
        $destination_path = MUZE_GAME_IMAGES_DIR . $relative_path ;

        return $destination_path;
    }

    private function _create_two_digit_dir($file_name){

        $two_dgt = $this->_get_last_two_letter($file_name);

        $dest_dir = MUZE_GAME_IMAGES_DIR . DS . $two_dgt . DS ;


        if (!file_exists($dest_dir)) {
            mkdir($dest_dir, 0777, true);
            // debug('FIle exists not');
        } else {
            // debug('FIle exists');
        }

    }

    private function _copy_file_with_dir($file_name){

        $dest_path_image = $this->_get_abs_path($file_name);
        $source_path_image = MUZE_GAME_TMP_LOCATION_IMAGES_UNZIPPED . $file_name;

        debug($source_path_image);
        debug($dest_path_image);

        $this->_create_two_digit_dir($file_name);

        if(copy($source_path_image , $dest_path_image))
            debug('copied');
        else debug('not copied');

    }

    /**
     * Get all information in Deletes.txt . Read file , parse it & return information as array.
     *
     * @return array
     */
    public function getDeletesDetails()
    {
        $resource = self::DATAFILES.'Deletes.txt';

        $delete_details = array_slice($this->readFile($resource), 1);

        $data = array();

        if(!empty($delete_details) && is_array($delete_details)){
            foreach($delete_details as $count => $records)
            {
                $attributes = explode($this->delimiter,$records);

                $data[$count]['TableName']	= $attributes[0];
                $data[$count]['Entity1Name']	= $attributes[1];
                $data[$count]['Entity1ID']	= $attributes[2];
                $data[$count]['Entity2Name']	= $attributes[3];
                $data[$count]['Entity2ID']	= $attributes[4];
            }
        }

        return $data;
    }

}