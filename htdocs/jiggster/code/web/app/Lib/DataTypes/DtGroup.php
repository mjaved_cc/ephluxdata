<?php

class DtGroup {

	private $_fields = array();
	private $_allowed_keys = array('id', 'name', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'name' => $this->name,
			'created' => $this->created
		);
	}

	/**
	 * Add single user to group object
	 * 
	 * @param array $user
	 */
	function add_user($user) {
		if (isset($this->_fields["User"]) && !is_array($this->_fields["User"])) {
			$prev_value = $this->User; // restore previous value
			unset($this->_fields["User"]); // now unset it so that we can convert object to array
			$this->_fields["User"][] = $prev_value;
			$this->_fields["User"][] = new DtUser($user);
		} else if (isset($this->_fields["User"]) && is_array($this->_fields["User"]))
			$this->_fields["User"][] = new DtUser($user);
		else
			$this->_fields["User"] = new DtUser($user);
	}

	/**
	 * Add multiple users to group object
	 * 
	 * @param array $users
	 */
	function add_users($users) {
		if (!empty($users)) {
			foreach ($users as $value)
				$this->add_user($value);
		}
	}

}

?>
