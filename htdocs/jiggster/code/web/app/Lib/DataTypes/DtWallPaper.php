<?php

class DtWallPaper {

	private $_fields = array();
	private $_allowed_keys = array('id','image_name','created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'image_name' => $this->image_name,
			'original_url' => $this->_get_original_url(),
            'thumbnail_url' => $this->_get_thumbnail_url()
		);
	}

    private function _get_original_url(){

        if (file_exists(WALLPAPER_ORG_DIR . $this->image_name)) {
            return SITE_URI . WALLPAPER_ORG_URL . $this->image_name ;
        } else {
//
        }

    }

    private function _get_thumbnail_url(){

        if (file_exists(WALLPAPER_THUMB_DIR . $this->image_name)) {
            return SITE_URI . WALLPAPER_THUMB_URL . $this->image_name  ;
        } else {
            //
        }

    }

}