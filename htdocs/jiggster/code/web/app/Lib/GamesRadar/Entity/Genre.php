<?php
/**
* @package Genre Entity
*/

class ApiGenre
{
	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var string
	 */
	public $name;
}