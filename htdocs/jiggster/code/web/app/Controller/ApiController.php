<?php

class ApiController extends AppController {

    public $name = 'Api';
    public $components = array('App', 'ApiResponse', 'ApiUser', 'ApiGame', 'ApiConsole', 'ApiGenre', 'ApiDeal', 'RequestHandler', 'Auth','ApiWallPaper');
    public $uses = array('User', 'Game', 'Console', 'Genre', 'UsersConsole', 'UsersGame', 'Deal', 'UsersWishlists', 'UsersGenre', 'UsersChatlog', 'DealsChatlog', 'UsersDeal','WallPaper','Tmp');

    function beforeFilter() {
        $this->Auth->allow('*');
    }

    function user_login() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->login($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_signup() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->signup($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_detail_by_id() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->game_detail_by_id($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_detail_by_barcode() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->game_detail_by_barcode($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_detail_by_title() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->game_detail_by_title($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_what_i_have() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->games_by_user_id($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function console_get() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiConsole->get_console($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function genre_get() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGenre->get_genre($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_verify_account() {

        if (!empty($this->request->query)) {

            $response = $this->ApiUser->verify_user_account($this->request->query);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function console_save() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiConsole->save_console($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function genre_save() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGenre->save_genre($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_all_games() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->list_all_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_save_record() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->save_user_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_save_wishlist() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->save_user_wishlist($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_make_offer() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->save_offer($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function wishlist_user_all() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->get_user_all_wishlist($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_signup_social() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->signup_with_social_account($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_owned_by_users() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->users_who_want_to_sell_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_wished_by_users() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->users_who_want_to_buy_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_most_wanted() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->most_wanted_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_most_owned() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->most_owned_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_upcoming_new_releases() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->upcoming_new_releases($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_save_chatlog() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->save_chatlog($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_unread_message_count() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->count_unread_messages($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_chat() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->chat_between_two_users($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_mark_message_read() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->mark_message_read($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_save_profile_info() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->save_profile($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_get_profile_info() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->get_user_profile($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_save_profile_image() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->save_profile_image($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_selling_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->selling_game_user($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_buying_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->buying_game_user($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_trading_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->trading_game_user($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_for_you() {

        if ($this->request->is('post') && !empty($this->data)) {

            $data = $this->App->decoding_json($this->ApiGame->games_for_you($this->data));

            if($data['header']['code'] == ECODE_SUCCESS ){

                $data_jiggster_pick = $this->App->decoding_json($this->ApiGame->jiggster_pick_see_more($this->data));

                if($data_jiggster_pick['header']['code'] == ECODE_SUCCESS) {
                    $data['body'] = array_merge($data['body'] , $data_jiggster_pick['body']);
                } else {
                    $data = $data_jiggster_pick ;
                }
            }

            $response = $this->App->encoding_json($data);
            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_all_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->all_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_ongoing_offers_for_wish_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->all_wish_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_ongoing_offers_for_have_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->all_have_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_current_deals() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->current_deals($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_send_receive_deals() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->send_receive_deals($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_send_receive_action() { 

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->send_receive_action($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_current_deal_on_specific_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->current_deals_on_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_update_have_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->update_have_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_extra_detail() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->extra_detail($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_update_wish_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->update_wish_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_save_chatlog() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->save_deal_chatlog($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_chat() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->chat_deal($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_unread_message_count() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->count_deal_unread_messages($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_mark_message_read() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->mark_deal_message_read($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_delete_have_or_wish_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->delete_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_selling_specific_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->selling_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_buying_specific_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->buying_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_trading_specific_game() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->trading_specific_game($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_awaiting_rating() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->awaiting_rating($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_rating() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->rating_deal($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_mark_trusted() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->mark_trusted($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_count_games_and_deals() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->count_games_and_deals($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_save_paypal_data() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->save_paypal_data($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_update_gcm_regid() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->update_gcm_regid($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function genre_specific_games() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->specific_games_genre($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_archive() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->save_deal_archive($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_save_flag_and_comment() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->save_flag_and_comment($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function deal_detail() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiDeal->detail_deal($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function wishlist_recommended() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->recommended_wishlist($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_autocomplete() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->game_autocomplete($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function game_top_mobile() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->top_mobile_games($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_push_notification() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->push_notification($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_latlong_update() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->latlong_update($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_change_pwd() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->change_pwd($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_wallpapers() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->update_wallpapers($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function user_archive_deals() {

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiUser->archive_deals($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    function wall_paper_get_all(){

        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiWallPaper->get_all($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }

    /**
     * See More for recommended Games
    */
    function game_recommended_see_more(){
        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->recommended_see_more($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }

    }

    /**
     * See More for Jiggster Pick Games
     */
    function game_jp_see_more(){
        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->jiggster_pick_see_more($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }

    }

    /**
     * See More for recommended Games
     */
    function game_genre_see_more(){
        if ($this->request->is('post') && !empty($this->data)) {

            $response = $this->ApiGame->genre_see_more($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }

    }
    function test() {

        $hash = md5('jiggster');

        $data = array(
            'header' => array(
                'code' => 0,
                'message' => 'success'
            ),
            'body' => array(
                'key' => $hash,
//				'game_title' => 'Grand',
                'user_id' => 10,
//				'game_id' => "21520",
//				'amount' => 5,
//				'trade' => 'trade',
//				'condition' => 'good',
//				'comes_with' => 'box + dvd'
//				'genre_id' => '1,2,3,4,5,6,7,8,9,10',
//				'console_id' => '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19',
//				'dist' => 1000,
//				'deal_id' => 23,
//				'receive_action' => 1,
//				'send_action' => 0,
//				'id_receiver' => 1,
//				'user_message' => 'This is a test message'
				'start' => 0,
				'end' => 25,
//				'push_notification' => 1
//				'latitude' => 51.5159722,
//				'longitude' => -0.1444292,
//				'game_title' => "FIFA 1",
//				'search_param' => 2,
//				'new_pwd' => 'salik123',
//				'confirm_pwd' => 'salik',
//				'address_1' => 'test1',
//				'zipcode' => 75760,
//				'state' => 'sindsh',
//				'country' => 'pakistan',
//				'location' => 'shahfaisal',
//				'paypal_id' => 12345,
//				'contact' => 12345678,
//				'profile_text' => 'This is profile text',
//				'status' => 'shipping_address',
//				'gaming_id' => 12345,
//				'address_2' => 'test2',
//				'email' => 'javed.nedian@gmail.com',
//				'password' => '12345678',
//				'dob' => '12345678',
//				'nickname' => 'java',
//				'latitude' => 0,
//				'longitude' => 0,
//				'member_id' => 4,
//				'what_i_have' => '5,6',
//				'what_i_want' => '7,8',
//				'user_trade' => 'exchange',
//				'member_trade' => 'exchange',
//				'comment' => 'This is comment',
//				'action_taken' => 'created',
//				'action_taken_by' => 3,
//				'is_ensurend' => 1,
//				'genre_ids' => '1,2,3',
				'console_ids' => '',
//				'email' => 'javed.nedian@gmail.com',
//				'receive_action' => 0,
//				'send_action' => 1,
//				'receive_action_payment' => 0,
//				'send_action_payment' => 0,
//				'action' => 'send_confirm',
//				'deal_type' => 'trade',
				'search_param' => 1,

            )
        );

        echo json_encode($data);
        exit;
		
		
    }
    function user_forgot_password()
    {

        if ($this->request->is('post') && !empty($this->data)) {
            //$data=json_decode($this->data);

            $response = $this->ApiUser->forgot_password($this->data);

            $this->set(compact('response'));
            $this->render('generic');
        } else {

            $response = $this->ApiResponse->get_bad_request_response();
            $this->set(compact('response'));
            $this->render('generic');
        }
    }
//    function reset_account_password() {
//
//        $this->layout = 'login';
//
//        //if form is post
//        if ($this->request->is('post') && !empty($this->data['User'])) {
//            $user_pass_info = $this->data['User'];
//            $verification_code = $user_pass_info['hidden'];
//            $api_response = $this->ApiUser->reset_password($verification_code, $user_pass_info);
//            $this->set('api_response', $api_response);
//        }
//        if (!empty($this->request->query['v'])) { //if returned from email
//            $verification_code = $this->request->query['v'];
//            $this->set('request_variable', $verification_code);
//        }
//    }
    function testing(){

        //$this->App->send_email('javed.nedian@gmail.com', SITE_NAME . ': EMAIL TEST', 'GOT IT' );
        //die('sent');

    }

}

?>
