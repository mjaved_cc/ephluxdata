<?php
App::uses('Requester','Lib/GamesRadar');
App::uses('Client','Lib/GamesRadar');

// Controller/GamesRadarController.php
class GameRadarsController extends AppController {

	public $components = array('RequestHandler');
	public $name = 'GameRadars';
	public $uses = array('Console','Genre','Game');
	
	private $apiKey = GAMES_RADAR_API_KEY;
	private $limit;
	
	static $requester;
	static $client;

	function beforeFilter() {
		$this->Auth->allow('*');
		self::$requester = new Requester();
		self::$client = new Client(self::$requester, $this->apiKey);
	}

	function index() {		
	}

    public function get_games() {
		$games = array();
		$data = $this->Genre->get_all_genres();
		$this->limit = '50';
		foreach($data as $value){
			$games[] = self::$client->games(array(
				'genre' => strtolower ($value['Genre']['genre']),
				'limit' => 50
			));
		}
		
		//pr($games);
		if(isset($games)){
			$this->Game->save_game($games);
		}
		exit();
    }

    public function get_genres() {
		$genres = self::$client->genres();
		$this->Genre->save_genre($genres);
		exit();
    }

    public function get_consoles() {
		$platforms = self::$client->platforms();
		$this->Console->save_console($platforms);
		exit();
    }
		
}