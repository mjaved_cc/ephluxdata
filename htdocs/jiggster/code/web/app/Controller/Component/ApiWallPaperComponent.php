<?php

class ApiWallPaperComponent extends Component {

    const BASE_CODE = API_WALL_PAPER_BASE_CODE;

    public $components = array('ApiResponse', 'App');
    private $_controller;

    function startup(Controller $controller) {

        $this->_controller = $controller;
        $this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
        $this->ApiResponse->format = ApiResponseComponent::JSON;
    }

    /**
     * Get all wall papers provided by limit default is 100
     *
     * Error code : 1 - 30
     *
     * @return ApiResponse
    */
    function get_all($json_data){


        $data = $this->App->decoding_json($json_data);

        if ( isset($data['body']['key']) && md5(SIGNATURE) == $data['body']['key'] ) {

            $body = $data['body'];
            $limit = 15 ;

            if( isset($body['page']) )
            {
                $page = $body['page'] - 1;
                $offset = ($limit * $page) + 1 ;
            }
            else
            {
                $offset = 0;
            }

            $wall_papers = $this->_controller->WallPaper->get_all($offset , $limit);

            if(is_array($wall_papers) && !empty($wall_papers)){

                $this->ApiResponse->offset = '0';
                $this->ApiResponse->msg = "Wall Papers found.";
                $this->ApiResponse->body = $wall_papers;

            } else {
                $this->ApiResponse->offset = '1';
                $this->ApiResponse->msg = "Wall paper(s) not found.";
            }
        } else {

            $this->ApiResponse->offset = '2';
            $this->ApiResponse->msg = "invalid parameters";
        }

        return $this->ApiResponse->get();
    }
}