<?php

class ApiUserComponent extends Component {

	const BASE_CODE = API_USER_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * User login
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 1-30
	 */
	function login($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->email) && !empty($data->body->password) && !empty($data->body->gcm_regid) && md5(SIGNATURE) == $data->body->key) {

			$email = $data->body->email;

			$this->_controller->User->set($this->_controller->data);

			if ($this->App->is_valid_email($email)) {

				if ($this->_controller->User->validates(array('fieldList' => array('password')))) {

					$password = md5($data->body->password);

					$user_authorized = $this->_controller->User->verify_login($email, $password, $data->body->gcm_regid);  //if user is authorized

					if (!empty($user_authorized)) {

						$data = $this->_controller->User->get_details_by_id($user_authorized['User']['id']);

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "You are successfully logged in";
						$this->ApiResponse->body = $data;
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					} else {
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '61';
						$this->ApiResponse->msg = 'Invalid login credentials!';
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '62';
				$this->ApiResponse->msg = "Pleasee provide valid email address";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '63';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Create new user 
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 31-60
	 */
	function signup($json_data) {

		$data = json_decode($json_data);

		if (empty($data->body->gcm_regid)) {
			$data->body->gcm_regid = 123456787;
		}

		if (!empty($data->body->email) && !empty($data->body->password) && !empty($data->body->nickname) && !empty($data->body->dob) && !empty($data->body->gcm_regid) && md5(SIGNATURE) == $data->body->key) {

			$email = Sanitize::escape($data->body->email);


			if ($this->App->is_valid_email($email)) {

				if (!$this->_controller->User->is_unverified_email_exists($email)) {//if this user already exist as unverified 
					$data->body->password = md5(Sanitize::escape($data->body->password));

					$status = $this->_controller->User->save_record($email, Sanitize::escape($data->body->nickname), $data->body->password, Sanitize::escape($data->body->dob), $data->body->latitude, $data->body->longitude, $data->body->gcm_regid, $this->App->get_current_datetime());

					if ($status) {

						//$ver_link = $this->_get_verification_link($email);
						$first_name = $data->body->nickname;
						$message = $first_name;
						$this->App->send_email($email, 'WellCome To Jiggster', $message, 'signup'); //send email for verification

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->body = array('user_id' => $status);
						$this->ApiResponse->msg = "You are successfully sign up, please check your email to verify your account";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '31';
					$this->ApiResponse->msg = "This email already exist";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '32';
				$this->ApiResponse->msg = "Pleasee provide valid email address";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '33';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get link for email verification
	 * @param string $email email address
	 * @return string signup_link
	 */
	function _get_verification_link($email) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		$verification_code = substr(md5($code), 8, 5);

		$signup_link = SITE_URI . '/user/' . $this->_controller->params['controller'] . '/' . 'verify_account.json?v=' . $verification_code . '&email=' . $email;

		return $signup_link;
	}

	/**
	 * verify email
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 61-70
	 */
	function verify_user_account($data) {

		if (!empty($data['v']) && !empty($data['email'])) {

			$email = $data['email'];
			$verification_code = $data['v'];

			$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
			if (substr(md5($code), 8, 5) == $verification_code) {

				$verify_account = $this->_controller->User->verify_account($email);

				if (!empty($verify_account)) {

					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '0';
					$this->ApiResponse->msg = "Your account has been activated.";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				} else {
					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '61';
					$this->ApiResponse->msg = "Your account has already been activated.";
					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '62';
				$this->ApiResponse->msg = "Verification code is invalid.";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '63';
			$this->ApiResponse->msg = "Invalid Request";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Signup with Soical Account
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 71-80
	 */
	function signup_with_social_account($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->link_id) &&
				//!empty($data->body->email_address) &&
				//!empty($data->body->image_url) &&
				//!empty($data->body->type_id) &&
				//!empty($data->body->access_token) &&
				//!empty($data->body->screen_name) &&
				//!empty($data->body->ip_address) &&
				md5(SIGNATURE) == $data->body->key) {

			$data = array(
				'link_id' => $data->body->link_id,
				'email_address' => $data->body->email_address,
				'image_url' => $data->body->image_url,
				'type_id' => $data->body->type_id,
				'access_token' => $data->body->access_token,
				'access_token_secret' => $data->body->access_token_secret,
				'is_valid_token' => 1,
				'screen_name' => $data->body->screen_name,
				'status' => '1',
				'ip_address' => $data->body->ip_address,
				'latitude' => $data->body->latitude,
				'longitude' => $data->body->longitude
			);



			$exist = $this->_controller->User->check_user_existance($data['email_address']);

			if (empty($exist) && $this->App->is_valid_email($data['email_address'])) {

				$this->App->send_email($data['email_address'], 'WellCome To Jiggster', $data['screen_name'], 'signup');
			}

			$data = $this->_controller->User->save_user_social_data($data);

			if (!empty($data)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User signed up with social account successfully";
				$this->ApiResponse->body = $data;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '71';
				$this->ApiResponse->msg = "User could not signed up with social account";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '72';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Signup with Soical Account
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 81-90
	 */
	function login_with_social_account($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->link_id) &&
				!empty($data->body->email_address) &&
				!empty($data->body->image_url) &&
				!empty($data->body->type_id) &&
				!empty($data->body->access_token) &&
				!empty($data->body->screen_name) &&
				!empty($data->body->ip_address) &&
				md5(SIGNATURE) == $data->body->key) {

			$data = array(
				'link_id' => $data->body->link_id,
				'email_address' => Sanitize::escape($data->body->email_address),
				'image_url' => $data->body->image_url,
				'type_id' => $data->body->type_id,
				'access_token' => $data->body->access_token,
				'access_token_secret' => $data->body->access_token_secret,
				'is_valid_token' => 1,
				'screen_name' => $data->body->screen_name,
				'status' => 'active',
				'ip_address' => $data->body->ip_address
			);

			$status = $this->_controller->User->save_user_social_data($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User signed up with social account successfully";
				$this->ApiResponse->body = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '71';
				$this->ApiResponse->msg = "User could not signed up with social account";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '72';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Web Admin Login
	 * @param array $login_info
	 * @return array $api_response
	 * Error code range : 91-100
	 */
	function web_login($login_info) {

		if (!empty($login_info['username']) && !empty($login_info['password'])) {

			$email = $login_info['username'];
			$web = true;

			$this->_controller->User->set($this->_controller->data);

			if ($this->App->is_valid_email($email)) {

				if ($this->_controller->User->validates(array('fieldList' => array('password')))) {

					$password = md5($login_info['password']);

					$user_authorized = $this->_controller->User->verify_login($email, $password, $web);  //if user is authorized

					if (!empty($user_authorized)) {

						//retieving user data
						$user_details = $this->_controller->User->get_details_by_id($user_authorized['User']['id']);

						if (!empty($user_details)) {

							/* Wrap data with rendering logic */
							$obj_user = new DtUser($user_details['0']['User']); //sending user data to the user class
						}
						/* END: Wrap data with rendering logic */

						/* Retrieve data from rendering logic */
						$data = array();

						$data['User'] = $obj_user->get_field();

						$this->_controller->Auth->login($data['User']);

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "You are successfully logged in";
						$this->ApiResponse->body = $data;
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					} else {
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '91';
						$this->ApiResponse->msg = $this->_controller->Auth->loginError;

						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '62';
				$this->ApiResponse->msg = "Pleasee provide valid email address or password";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '63';
			$this->ApiResponse->msg = "Username or password is empty";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save Users Chatlog
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 101-110
	 */
	function save_chatlog($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->id_sender) &&
				!empty($data->body->id_receiver) &&
				!empty($data->body->user_message) &&
				md5(SIGNATURE) == $data->body->key) {

			$param = array(
				'id_sender' => $data->body->id_sender,
				'id_receiver' => $data->body->id_receiver,
				'user_message' => Sanitize::escape($data->body->user_message)
			);

			$status = $this->_controller->UsersChatlog->save_message($param);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Chat successfully saved";
				$this->ApiResponse->body = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '101';
				$this->ApiResponse->msg = "Chat could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '102';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Count User Unread Messages
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 111-120
	 */
	function count_unread_messages($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->id_user) &&
				!empty($data->body->id_receiver) &&
				md5(SIGNATURE) == $data->body->key) {

			$message_count = $this->_controller->UsersChatlog->unread_count($data->body->id_user, $data->body->id_receiver);

			if (!empty($message_count)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Messages Count Found";
				$this->ApiResponse->body = $message_count;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '111';
				$this->ApiResponse->msg = "Messages count count not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '112';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * List Chat between two Users
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 121-130
	 */
	function chat_between_two_users($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->id_sender) &&
				!empty($data->body->id_receiver) &&
				md5(SIGNATURE) == $data->body->key) {

			$param = array(
				'id_sender' => $data->body->id_sender,
				'id_receiver' => $data->body->id_receiver
			);

			$chat = $this->_controller->UsersChatlog->user_chat($param);

			if (!empty($chat)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Chat found";
				$this->ApiResponse->body = $chat;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '121';
				$this->ApiResponse->msg = "Chat could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '122';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * List Chat between two Users
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 131-140
	 */
	function mark_message_read($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->id_chatlog) &&
				!empty($data->body->id_receiver) &&
				md5(SIGNATURE) == $data->body->key) {

			$param = array(
				'id_chatlog' => $data->body->id_chatlog,
				'id_receiver' => $data->body->id_receiver
			);

			$status = $this->_controller->UsersChatlog->mark_read($param);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Messages marked read";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '131';
				$this->ApiResponse->msg = "Messages could not marked read";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '132';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save User Profile info
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 141-150
	 */
	function save_profile($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$param = array(
				'user_id' => $data->body->user_id,
				'nickname' => Sanitize::escape($data->body->nickname),
				'gaming_id' => $data->body->gaming_id,
				//'shipping_address' => Sanitize::escape($data->body->shipping_address),
				'address_1' => Sanitize::escape($data->body->address_1),
				'address_2' => Sanitize::escape($data->body->address_2),
				'zipcode' => Sanitize::escape($data->body->zipcode),
				'state' => Sanitize::escape($data->body->state),
				'country' => Sanitize::escape($data->body->country),
				'location' => Sanitize::escape($data->body->location),
				'paypal_id' => $data->body->paypal_id,
				'contact' => $data->body->contact,
				'profile_text' => Sanitize::escape($data->body->profile_text),
				'status' => $data->body->status
			);

			$status = $this->_controller->User->save_profile_info($param);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User profile info updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '141';
				$this->ApiResponse->msg = "User profile info could not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '142';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get User Profile info
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 151-160
	 */
	function get_user_profile($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$user_data = $this->_controller->User->get_profile_info($data->body->user_id);

			if (!empty($user_data)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $user_data;
				$this->ApiResponse->msg = "User profile info found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '151';
				$this->ApiResponse->msg = "User profiel info could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '152';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save User Profile image
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 161-170
	 */
	function save_profile_image($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$target_path = 'img/user_profile/';

			$target_path = $target_path . basename($_FILES['image']['name']);

			if (move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {

				$this->_controller->User->save_image($data->body->user_id, $target_path);

				$msg = "The file " . basename($_FILES['image']['name']) .
						" has been uploaded";

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = $msg;
				$this->ApiResponse->body = array('profile_image_paht' => SITE_URI . '/' . $target_path);
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$msg = "There was an error uploading the file, please try again! filename: " . basename($_FILES['image']['name']);
				$msg .= "target_path: " . $target_path;

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '161';
				$this->ApiResponse->msg = $msg;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '162';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get All Users
	 * @param 
	 * @return array $api_response
	 * Error code range : 171-180
	 */
	function get_all_user() {

		$users = $this->_controller->User->get_all();

		if (!empty($users)) {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $users;
			$this->ApiResponse->msg = "Users Found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '171';
			$this->ApiResponse->msg = "No user found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Search User by nickname or title
	 * @param array $search_keyword
	 * @return array $api_response
	 * Error code range : 181-190
	 */
	function search_user($search_keyword) {

		$search_result = $this->_controller->User->search(Sanitize::escape($search_keyword));

		if (!empty($search_result)) {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $search_result;
			$this->ApiResponse->msg = "Search Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '181';
			$this->ApiResponse->msg = "No Search Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * All Users Selling or Selling Trading specific Game
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 191-200
	 */
	function selling_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->game_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			$users = $this->_controller->User->selling_particular_game($data->body->game_id, $data->body->start, $data->body->end);

			if (!empty($users)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $users;
				$this->ApiResponse->msg = "Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '191';
				$this->ApiResponse->msg = "Users not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '192';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * All Users Buying or Buying Trading specific Game
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 201-210
	 */
	function buying_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->game_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			$users = $this->_controller->User->buying_particular_game($data->body->game_id, $data->body->start, $data->body->end);

			if (!empty($users)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $users;
				$this->ApiResponse->msg = "Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '201';
				$this->ApiResponse->msg = "Users not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '202';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * All Trading specific Game
	 * @param array $json_data
	 * @return array $api_response
	 * Error code range : 211-220
	 */
	function trading_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$users = $this->_controller->User->trading_particular_game($data->body->game_id);

			if (!empty($users)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $users;
				$this->ApiResponse->msg = "Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '211';
				$this->ApiResponse->msg = "Users not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '212';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Details
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 221-230
	 */
	function user_details($id) {

		$user = $this->_controller->User->details($id);

		if (!empty($user)) {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $user;
			$this->ApiResponse->msg = "User found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '221';
			$this->ApiResponse->msg = "User not found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Mark User trusted
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 231-240
	 */
	function mark_trusted($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->transaction_id) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->User->make_user_trusted($data->body->user_id, $data->body->transaction_id);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User marked trusted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '231';
				$this->ApiResponse->msg = "User could not marked trusted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '232';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Games and Deal Count
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 241-250
	 */
	function count_games_and_deals($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$data = $this->_controller->User->games_and_deals_count($data->body->user_id);

			if (!empty($data['GamesCount'])) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $data;
				$this->ApiResponse->msg = "Games count found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '241';
				$this->ApiResponse->msg = "Games count could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '242';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User save paypal data
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 241-250
	 */
	function save_paypal_data($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->deal_id) && !empty($data->body->txn_id) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->User->save_paypal_data($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Payapal data saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '241';
				$this->ApiResponse->msg = "Paypal data could not saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '242';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User save paypal data
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 251-260
	 */
	function update_gcm_regid($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->User->gcm_regid_update($data->body->user_id, $data->body->gcm_regid);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "gcm_regid updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '251';
				$this->ApiResponse->msg = "gcm_regid could not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '252';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Push notification data
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 261-270
	 */
	function push_notification($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->push_notification) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->User->push_notification($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Push notification updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '261';
				$this->ApiResponse->msg = "Push notification not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '262';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * latlong data
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 271-280
	 */
	function latlong_update($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->latitude) && isset($data->body->longitude) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->User->latlong_update($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Latitude longitude updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '271';
				$this->ApiResponse->msg = "Latitude longitude not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '272';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Reset password
	 * @param array $id
	 * @return array $api_response
	 * Error code range : 281-290
	 */
	function change_pwd($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->confirm_pwd) && isset($data->body->new_pwd) && md5(SIGNATURE) == $data->body->key) {

			$data->body->new_pwd = md5(Sanitize::escape($data->body->new_pwd));
			$status = $this->_controller->User->user_change_pwd($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Password updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '281';
				$this->ApiResponse->msg = "Password not match";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '282';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Update Wallpapers
	 * @param array $data
	 * @return array $api_response
	 * Error code range : 291-300
	 */
	function update_wallpapers($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->confirm_pwd) && isset($data->body->new_pwd) && md5(SIGNATURE) == $data->body->key) {

			$data->body->new_pwd = md5(Sanitize::escape($data->body->new_pwd));
			$status = $this->_controller->User->update_wallpapers($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Wallpaper updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '291';
				$this->ApiResponse->msg = "Wallpaper not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '292';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Archive deals
	 * @param array $data
	 * @return array $api_response
	 * Error code range : 301-310
	 */
	function archive_deals($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->User->archive_deals($data->body->user_id);

			if (!empty($games['buy']) || !empty($games['sell']) || !empty($games['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Archives found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '301';
				$this->ApiResponse->msg = "No Archives found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '302';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Archive deals
	 * @param array $data
	 * @return array $api_response
	 * Error code range : 311-320
	 */
	function forgot_password($user_info) {

		$data = $this->App->decoding_json($user_info);

		if (isset($data['body']['email']) && !empty($data['body']['email'])) {

			$email = $data['body']['email'];

			if ($this->App->is_valid_email($email)) { //checking that the email is in valid format
				$user_detail = $this->_controller->User->get_by_email($email); // fetching user data

				if (!empty($user_detail)) {
					//making object of user class and passing user data to it
					$obj_user_data_type = new DtUser($user_detail['User']);

					$id = $obj_user_data_type->id;
					$first_name = $obj_user_data_type->nickname;

					$forgot_pass_code = $this->get_forgot_pass_code($id); //generate forgot password code
					//generate forgot password link for email
					$forgot_pass_link = $this->get_forgot_pass_link($forgot_pass_code, $email);

					$user_data = $this->App->get_db_save_json($user_detail); // make json for db save

					$status = $this->_controller->Tmp->save_tmp_record($forgot_pass_code, FORGOT_PASSWORD, $user_data, $this->App->get_tmp_expiry(), $this->App->
									get_current_datetime());

					if ($status) { // if data is saved in tmp table
						$message = $first_name . '|' . $forgot_pass_link;

						$this->App->send_email($email, SITE_NAME . ': Forgot Password Request', $message, 'forgot_password');

						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "Check your email to reset your password";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				} else {
					$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
					$this->ApiResponse->offset = '311';
					$this->ApiResponse->msg =
							"We don't recognise that email address. Give it another shot.";

					$this->ApiResponse->format = ApiResponseComponent::JSON;
					return $this->ApiResponse->get();
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '312';
				$this->ApiResponse->msg = "Please provide valid email address";

				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '313';
			$this->ApiResponse->msg = "Email address is required";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * get link for forget password
	 * @param $forgot_pass_code
	 * @return string $forgot_pass_link
	 */
	function get_forgot_pass_link($forgot_pass_code, $email) {


		$forgot_pass_link = SITE_URI . 'users/reset_account_password?v=' . $forgot_pass_code;

		return $forgot_pass_link;
	}

	/**
	 * get code for forget password
	 * @param int $id user id
	 * @return $forgot_pass_code
	 */
	function get_forgot_pass_code($id) {

		$SALT = Configure::read('Security.salt');
		$date = new DateTime();
		$time_stamp = $date->getTimestamp();
		$code = $time_stamp . $id . $SALT;
		$forgot_pass_code = substr(md5($code), 8, 5);

		return $forgot_pass_code;
	}

	/**
	 * Archive deals
	 * @param array $data
	 * @return array $api_response
	 * Error code range : 321-330
	 */
	function reset_password($verification_code, $user_pass_info = 0) {

		$user_tmp_detail = $this->_controller->Tmp->get_user_tmp_data($verification_code, FORGOT_PASSWORD); //retrieving user data from tmp table

		if (!empty($user_tmp_detail)) {  //if record is found in database			
			$user_tmp_data = $this->App->decoding_json($user_tmp_detail['Tmp']['data']);
			$user_id = $user_tmp_data['User']['id'];

			if (!empty($user_pass_info['password']) && !empty($user_pass_info['confirm_password'])) {

				$this->_controller->User->set($this->_controller->data);

				if ($this->_controller->User->validates(array('fieldList' => array('password')))) {  //validating password field
					if ($user_pass_info['password'] == $user_pass_info['confirm_password']) { // if password and confirm password are not equal 
						if (strlen($user_pass_info['password']) >= 7) {

							$user_pass_info['password'] = md5($user_pass_info['password']); //encrypting password
							//reset password in db
							$status = $this->_controller->User->reset_password($user_pass_info['password'], $user_id);
							if ($status) //delete record from tmp table
								$this->_controller->Tmp->remove_by_id($user_tmp_detail['Tmp']['id']);
						} else {
							$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
							$this->ApiResponse->offset = '321';
							$this->ApiResponse->msg = "password length is less than 8";
							$this->ApiResponse->format = ApiResponseComponent::JSON;
							return $this->ApiResponse->get();
						}
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "Password have successfully been reset";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					} else {
						$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
						$this->ApiResponse->offset = '323';
						$this->ApiResponse->msg = "Passwords does not match";
						$this->ApiResponse->format = ApiResponseComponent::JSON;
						return $this->ApiResponse->get();
					}
				}
			} else {
				$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
				$this->ApiResponse->offset = '325';
				$this->ApiResponse->msg = "Fields are empty";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {
			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '324';
			$this->ApiResponse->msg = "You have reset your password from this email";

			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get Distance Between two users
	 * @param int $user1, int $user2
	 * @return array $api_response
	 * Error code range : 331-340
	 */
	function get_distance($user1, $user2) {

		if (!empty($user1) && !empty($user2)) {

			$user1_lat_lan = $this->_controller->User->get_user_lat_lan($user1); 
			$user2_lat_lan = $this->_controller->User->get_user_lat_lan($user2);
			
			$distance = $this->App->cal_distance($user1_lat_lan['latitude'], $user1_lat_lan['longitude'], $user2_lat_lan['latitude'], $user2_lat_lan['longitude'], DISTANCE_UNIT); 

			if (!empty($distance) || $distance == 0 ) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = array('distance' => $distance);
				$this->ApiResponse->msg = "Distance Found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '331';
				$this->ApiResponse->msg = "Distance could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiUserComponent::BASE_CODE;
			$this->ApiResponse->offset = '332';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

}
