<?php

class ApiGenreComponent extends Component {

	const BASE_CODE = API_GENRE_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get All Genres
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 1-10
	 */
	function get_genre($json_data) {

		$data = json_decode($json_data);

		if (md5(SIGNATURE) == $data->body->key) {

			$genres = $this->_controller->Genre->get_all_genres();

			if (!empty($genres)) {

				$this->ApiResponse->base = ApiGenreComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Genres found";
				$this->ApiResponse->body = $genres;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGenreComponent::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "No Genre found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGenreComponent::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save Genres
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 11-20
	 */
	function save_genre($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->genre_id) && md5(SIGNATURE) == $data->body->key) { 
			
			$genre_ids = $data->body->genre_id;

			$array_genre_ids = explode(',', $genre_ids);  

			$status = $this->_controller->UsersGenre->save_user_genre($data->body->user_id, $array_genre_ids);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Genre saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '11';
				$this->ApiResponse->msg = "No Genre saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
			$this->ApiResponse->offset = '12';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}
	
	
}

?>