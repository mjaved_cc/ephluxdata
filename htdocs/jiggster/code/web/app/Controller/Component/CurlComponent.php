<?php

class CurlComponent extends Component {

    public $controller ;
    public $url, $data;

    private $_file_handler;

    public $components = array('App');

    function startup(Controller $controller) {
        $this->controller = $controller;
    }

    /*
     * @desc: http POST request using Curl.
     * @params: null
     *
     * @return : On success, "errno" is 0, "http_code" is 200, and "content" contains the web page.
     *
     * On an error with a bad URL, unknown host, timeout, or redirect loop,
     * "errno" has a non-zero error code and "errmsg" has an error message (see the CURL error code list).
     *
     * On an error with a missing web page or insufficient permissions,
     * "errno" is 0, "http_code" has a non-200 HTTP status code, and "content" contains the site’s error message page
     */

    function post() {

        //url-ify the data for the POST
        $query_string = $this->_http_build_query();

        // set required POST option for CURL
        $options = array(
            CURLOPT_URL => $this->url, // set the url
            CURLOPT_POST => count($this->data), // number of POST params
            CURLOPT_POSTFIELDS => $query_string, // post data
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_USERAGENT => getenv('HTTP_USER_AGENT')   // timeout on response
        );

        //open connection
        $ch = curl_init();
        curl_setopt_array($ch, $options);

        //execute post
        $content = curl_exec($ch);
        $error_code = curl_errno($ch);
        $error_msg = curl_error($ch);
        $header = curl_getinfo($ch);
        //close connection
        curl_close($ch);

        $header['error_code'] = $error_code;
        $header['error_msg'] = $error_msg;
        $header['content'] = $content;
        return $header;
    }

    /**
     * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
     * array containing the HTTP server response header fields and content.
     */
    function get() {

        //url-ify the data for the POST
        $query_string = $this->_http_build_query();

        // set required POST option for CURL
        $options = array(
            CURLOPT_URL => $this->url . '?' . $query_string , // set the url
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120   // timeout on response
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $error_code = curl_errno($ch);
        $error_msg = curl_error($ch);
        $header = curl_getinfo($ch);
        //close connection
        curl_close($ch);

        $header['error_code'] = $error_code;
        $header['error_msg'] = $error_msg;
        $header['content'] = $content;
        return $header;
    }

    /**
     * Generate URL-encoded query string
     *
     * @return string
     */
    private function _http_build_query() {
        if (!empty($this->data) && is_array($this->data))
            return http_build_query($this->data);
        return ;
    }

    /**
     * This function is explicitly written for muze games for downloading zip file via FTP )
     */
    function download_file_via_ftp() {

        $flag = true ;

        try{

            $this->App->muze_game_logging('Reading file ' . $this->data['file_name']);

            //open connection
            $ch = curl_init();

            $this->_file_handler = fopen($this->data['file_name'], 'w');

            // set required POST option for CURL
            $options = array(
                CURLOPT_URL => $this->url, // set the url
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_FILE => $this->_file_handler, // file handler
                CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
                CURLOPT_TIMEOUT => 120, // timeout on response
                CURLOPT_USERPWD => MUZE_GAME_FTP_USERNAME . ":" . MUZE_GAME_FTP_PWD
            );

            curl_setopt_array($ch, $options);

            //execute post
            $content = curl_exec($ch);
            $error_code = curl_errno($ch);
            $error_msg = curl_error($ch);
            $header = curl_getinfo($ch);
            //close connection
            curl_close($ch);

            $header['error_code'] = $error_code;
            $header['error_msg'] = $error_msg;
            $header['content'] = $content;


            /*		 * ****************************** */

            // $response['error_code'] != 0 > error: bad url, timeout, redirect loop
            // $response['http_code'] != 200 > error: no page, no permissions, no service

            /*		 * ****************************** */

            if ( $header['error_code'] == ECODE_SUCCESS && $header['content'] ) {

                $this->App->muze_game_logging('Saving zip file ====> ' . $this->data['file_name']);
                $this->saving_zip_to_tmp_location();

                // default flag is true


            } else { // in case of failure

                $this->App->muze_game_logging('Unable to save zip file ====> ' . $this->data['file_name']);

                $flag = false ;
            }

            return $flag ;

        } catch(Exception $e){

            $this->App->muze_game_logging('Exception caught');
            $this->App->muze_game_logging($e->getMessage());
            $this->App->muze_game_logging('Unable to open file ' . $this->data['file_name']);
        }

        fclose($this->_file_handler);
    }

    function saving_zip_to_tmp_location(){

        if ($this->_file_handler) {

            $zip = new ZipArchive;

            if ($zip->open($this->data['file_name']) === true) {
                $zip->extractTo($this->data['tmp_location']);
                $zip->close();

                $this->App->muze_game_logging('upzipped ok...........');
            } else {

                $this->App->muze_game_logging('upzipped failure ...........');
            }

        }
    }

}

?>