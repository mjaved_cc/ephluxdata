<?php

class ApiConsoleComponent extends Component {

	const BASE_CODE = API_CONSOLE_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get All Consoles
	 * @param null
	 * @return array $consoles
	 * Error code range : 1-10
	 */
	function get_console($json_data) {

		$data = json_decode($json_data);

		if (md5(SIGNATURE) == $data->body->key) {

			$consoles = $this->_controller->Console->get_all_console();

			if (!empty($consoles)) {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Consoles found";
				$this->ApiResponse->body = $consoles;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "No console found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * save user console
	 * @param INT $user_id, INT $console_id
	 * @return array $api_response
	 * Error code range : 11-20
	 */
	function save_console($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && isset($data->body->console_id) && md5(SIGNATURE) == $data->body->key) {
			
			$console_ids = $data->body->console_id;

			$array_console_ids = explode(',', $console_ids); 

			$status = $this->_controller->UsersConsole->save_user_console($data->body->user_id, $array_console_ids); 

			if (!empty($status)) {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Consoles saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
				$this->ApiResponse->offset = '11';
				$this->ApiResponse->msg = "No console saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		}  else {

			$this->ApiResponse->base = ApiConsoleComponent::BASE_CODE;
			$this->ApiResponse->offset = '12';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

}

?>