<?php

App::uses('Component', 'Controller');

/**
 * Javascript & Css Manager. 
 */
class JCManagerComponent extends Component {

	private $_controller;
	private $_js_list;
	private $_css_list;

	function startup(Controller $controller) {
		$this->_controller = $controller;

		$this->_js_list = $this->get_default_backend_js();
		$this->_css_list = $this->get_default_backend_css();
	}
	
	/**
	 * Get all JS files, template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_js() {

		return array(
			'admin/jquery.tools.min', // Import jquery library and jquery tools from a single file
			'admin/preloadCssImages.jQuery_v5', // Preload script for css and images
			'admin/script',
			'app',
			'jquery.validate'
		);
	}
	
	/**
	 * Get all CSS files, template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_css() {

		return array(
			'admin/style', 'admin/simple', 'admin/invalid'
		);
	}
	
	/**
	 * Push JS files in stack/queue
	 * 
	 * @param string|array $file_names JS file name
	 * @return void
	 */
	function add_js($file_names = array()) {
		
		if (!empty($file_names) && is_array($file_names)) {
			$this->_js_list = array_merge($this->_js_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_js_list[] = $file_names;
		}
	}
	
	/**
	 * Push CSS files in stack/queue
	 * 
	 * @param string|array $file_names CSS file name
	 * @return void
	 */
	function add_css($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			$this->_css_list = array_merge($this->_css_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_css_list[] = $file_names;
		}
	}
	
	/**
	 * Get all required JS & CSS files 
	 * 
	 * @return array
	 */
	function get() {
		return array($this->_js_list, $this->_css_list);
	}

}

?>
