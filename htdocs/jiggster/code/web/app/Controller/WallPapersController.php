<?php

App::uses('AppController', 'Controller');

/**

 * Users Controller

 *

 * @property User $User

 */
class WallPapersController extends AppController {

    // public $components = array('ApiWallPaper');
    public $name = 'WallPapers';
    public $uses = array('WallPaper');

    function beforeFilter(){
        $this->Auth->allow();

        parent::beforeFilter();
    }
    /**
     * Populate wall papers into DB.
     *
     * This function is just for developers i.e populate wallpapers via code
     *
     * THis function renames file name too.
    */
    function populate(){

        $destination_path_org = WALLPAPER_ORG_DIR ;
        $destination_path_thumb = WALLPAPER_THUMB_DIR ;
        $wall_papers = array();

        if ($handle = opendir($destination_path_thumb)) {

            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($handle))) {
                if(in_array($entry , array('.','..','.svn')))
                    continue;

                debug($entry);
                $wall_papers['WallPaper']['image_name'] = $entry ;
                $this->WallPaper->create();
                $this->WallPaper->save($wall_papers);


//                $file_info = pathinfo($entry);
//                $new_name = md5($file_info['filename']) ;
//
//                $old_file_path = $destination_path_thumb . $entry ;
//                $new_file_path = $destination_path_thumb . $new_name . '.' . $file_info['extension'] ;

                // rename($old_file_path, $new_file_path);
            }

            closedir($handle);
        }
        die('aa');
    }
}