<? //pr($conversations); ?>

<div class="conversations index">
	<h2><?php echo __('Inbox'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
        <th><?php echo $this->Paginator->sort('title'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th><?php echo $this->Paginator->sort('last_message_id'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($conversations as $conversation): ?>
	<tr>
		<td><?php echo h($conversation['Conversation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($conversation['User']['username'], array('controller' => 'users', 'action' => 'view', $conversation['User']['id'])); ?>
		</td>
		<td><?php echo h($conversation['Conversation']['title']); ?>&nbsp;</td>
		<td><?php echo h($conversation['Conversation']['created']->date); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($conversation['LastMessage']['message'], array('action' => 'view', $conversation['Conversation']['id'])); ?>
		</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $conversation['Conversation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $conversation['Conversation']['id']), null, __('Are you sure you want to delete # %s?', $conversation['Conversation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	
    </p>

	<div class="paging">
		<?php
            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
	</div>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>