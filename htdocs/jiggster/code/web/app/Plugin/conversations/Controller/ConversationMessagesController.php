<?php
App::uses('conversations.ConversationsAppController', 'Controller');
/**
 * ConversationMessages Controller
 *
 */
class ConversationMessagesController extends ConversationsAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;
	public $components = array('Session','Auth');
	
	var $uses = array('Conversation','ConversationMessage');
	
	public function add() {
		$this->autorender = false;
		echo 'adfaf';
		if($this->request->is('post')) {

			$this->request->data['ConversationMessage']['user_id'] = AuthComponent::user('id');
		
			//debug($this->request->data);
			
			$this->ConversationMessage->create();
			if($this->ConversationMessage->saveAll($this->request->data)) {
				$data['id'] = $this->request->data['ConversationMessage']['conversation_id'];
				$data['last_message_id'] = $this->ConversationMessage->getInsertID();
				
				$this->Conversation->save($data);
				$this->Session->setFlash('Message Sent!');	
			} else {
				$this->Session->setFlash('Failed to Send Message');
			}
		}
		
		$this->redirect(array('controller' => 'Conversations', 'action' => 'view',$this->request->data['ConversationMessage']['conversation_id']));
	}

}