<?php

class CountFacebook extends AppModel {

	public $name = 'CountFacebook';

	function get_all_details($query_params) {
		
		return $this->find('all', array(
			'conditions' => array($query_params),
			'fields' => array('count_likes','count_comments','count_shares','batch_id')
		));
	}

}