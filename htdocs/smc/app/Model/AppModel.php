<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * Turn off all associations on the fly.
	 *
	 * Example: Turn off the associated Model Support request,
	 * to temporarily lighten the User model:
	 *
	 * `$this->User->unbindModelAll();`
	 */
	function unbindModelAll() {
		$unbind = array();
		foreach ($this->belongsTo as $model => $info) {
			$unbind['belongsTo'][] = $model;
		}
		foreach ($this->hasOne as $model => $info) {
			$unbind['hasOne'][] = $model;
		}
		foreach ($this->hasMany as $model => $info) {
			$unbind['hasMany'][] = $model;
		}
		foreach ($this->hasAndBelongsToMany as $model => $info) {
			$unbind['hasAndBelongsToMany'][] = $model;
		}
		parent::unbindModel($unbind);
	}

	/*
	 * @desc: Bulk insert in one go. Cakephp saveAll method creates n INSERT query if array length is n which degrades performance.
	 * @params: data <array> - it should be same as that of used in saveAll
	 * @return: boolean
	 * @usage: You can also use this format to save several records with custom_saveAll(), using an array like the following:
	 * Array
	  (
	  [0] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  [1] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  )
	 * NOTE: created & modified datetime will not automatically save. and finish off with a single call to mysql_real_escape_string() while querying such as mysql_real_escape_string($json) / mysql_real_escape_string($serialize)
	 */

	function custom_saveAll($data) {

		$first_idx = key($data);
		// check if data is not empty & is of proper format. i.e the one use in saveAll
		if (count($data) > 0 && is_numeric($first_idx)) {
			$value_array = array();

			// table & name defined in lib/Cake/Model/Model.php
			$table_name = $this->table;
			$model_name = $this->name;

			$fields = array_keys($data[$first_idx][$model_name]);

			foreach ($data as $key => $value)
				$value_array[] = "('" . implode('\',\'', $value[$model_name]) . "')";


			$sql = "INSERT INTO " . $table_name . " (" . implode(', ', $fields) . ") VALUES " . implode(',', $value_array);

			$this->query($sql);
			return true;
		}

		return false;
	}

}
