<?php

class CountLinkedin extends AppModel {

	public $name = 'CountLinkedin';

	function get_all_details($query_params) {
		
		return $this->find('all', array(
			'conditions' => array($query_params),
			'fields' => array('batch_id')
		));
	}

}