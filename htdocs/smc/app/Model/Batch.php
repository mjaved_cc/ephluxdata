<?php

class Batch extends AppModel {

	public $name = 'Batch';

	/*
	 * @desc: get record if interval is less than 24 hours.
	 * @params: user_id <int> , batch_id <int>
	 * @return: result <array>
	 */

	function get_latest_id() {

		$result =  $this->find('first', array(
			'order' => 'id desc'					
				));
		
		return (count($result) > 0 ) ? $result['Batch']['id'] : 0 ;
	}	

}