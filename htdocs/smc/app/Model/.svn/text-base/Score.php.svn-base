<?php

class Score extends AppModel {

	public $name = 'Score';

	/**
	 * Get record if interval is less than 24 hours.
	 * 
	 * @param string $user_id System generated User Id
	 * @param string $batch_id Batch Id
	 * @return array Query result
	 */
	function get_todays($user_id, $batch_id) {

		return $this->find('first', array(
					'conditions' => array(
						'user_id' => $user_id,
						'batch_id' => $batch_id,
						'DATEDIFF(CURDATE(), Score.created) ' => 0
					)
				));
	}

	/**
	 * Get latest batch id for given user id.
	 * 
	 * @param string $user_id System generated User Id
	 * @return array Query result
	 */
	function get_latest_batchid($user_id) {

		$result = $this->find('first', array(
			'fields' => array('batch_id'),
			'conditions' => array('user_id' => $user_id),
			'order' => 'batch_id desc' // batch_id should be in desc because stats are calculated week-wise & is in order from latest to oldest. When results return from API then same pattern is followed i.e latest to oldest	    
				));

		return (!empty($result)) ? $result['Score']['batch_id'] : 0;
	}

	/**
	 * Get score details of given user_id based on batch_id desc. No. of records will be equal to week_count.
	 * @param string $user_id System generated User Id
	 * @param string $week_count No. of weeks is basically limit i.e how many records we have to fetch
	 * @return array Query result
	 */
	function get_details_by_userid($user_id, $week_count) {

		return $this->find('all', array(
					'conditions' => array('user_id' => $user_id),
					'order' => 'batch_id desc', // batch_id should be in desc because stats are calculated week-wise & is in order from latest to oldest. When results return from API then same pattern is followed i.e latest to oldest
					'limit' => $week_count
				));
	}
	

}