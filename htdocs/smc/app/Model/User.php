<?php

class User extends AppModel {

	public $name = 'User';
	public $hasMany = array(
		'UserSocialAccount' => array(
			'className' => 'UserSocialAccount',
			'conditions' => array('UserSocialAccount.status' => USA_STATUS_ACTIVE)
		)
	);

	/*
	 * @desc: check email existance
	 * @param: email <string>
	 * @return: user id if exists & 0 if not
	 */

	function is_email_already_exists($email) {
		$this->unbindModelAll();
		$user = $this->find('first', array(
			'conditions' => array('User.status' => USER_STATUS_ACTIVE, 'User.username' => $email)
				));

		return (!empty($user['User']['id'])) ? $user['User']['id'] : 0;
	}

	/*
	 * @desc: get details of users
	 * @param: query_params <array>
	 * @return: <array>
	 */

	function get_details($query_params = 0) {

		$clause['User.status'] = USER_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		return $this->find('first', array(
					'conditions' => array($clause
					)
				));
	}

	/**
	 * Get list of users for stats calculation
	 * @return array Query result
	 */
	function get_list_for_stats() {
		$this->unbindModelAll();
		return $this->find('all', array(
					'conditions' => array(
						'User.status' => USER_STATUS_ACTIVE,
						'DATEDIFF(CURDATE(), User.last_stats_updated)' => A_WEEK
					),
					'fields' => array(
						'DATEDIFF(CURDATE(), User.last_stats_updated) AS Days',
						'id'
					)
				));
	}

	/**
	 * Get details of all users
	 * 
	 * @param array $query_params All where clauses
	 * @return array Query result
	 */
	function get_stats_details($query_params = 0) {

		$this->unbindModelAll();

		$this->bindModel(array(
			'hasOne' => array(
				'Score' => array(
					'className' => 'Score',
					'order' => 'Score.batch_id DESC , Score.created DESC'
				), // modify fields array will cause problem as this is used in calculating trends in Lib/Datatypes/DtUser.php
				'CountFacebook' => array(
					'className' => 'CountFacebook',
					'conditions' => array('CountFacebook.batch_id = Score.batch_id'),
					'fields' => array('CountFacebook.count_likes', 'CountFacebook.count_comments', 'CountFacebook.count_talk_about', 'CountFacebook.count_page_likes', 'CountFacebook.count_shares'),
					'order' => 'CountFacebook.batch_id DESC , CountFacebook.created DESC'
				),
				'CountTwitter' => array(
					'className' => 'CountTwitter',
					'conditions' => array('CountTwitter.batch_id = Score.batch_id'),
					'fields' => array('CountTwitter.count_followers', 'CountTwitter.count_following', 'CountTwitter.count_tweets', 'CountTwitter.count_retweets', 'CountTwitter.count_mention'),
					'order' => 'CountTwitter.batch_id DESC , CountTwitter.created DESC'
				),
				'CountLinkedin' => array(
					'className' => 'CountLinkedin',
					'conditions' => array('CountLinkedin.batch_id = Score.batch_id'),
					'fields' => array('CountLinkedin.count_followers', 'CountLinkedin.count_comments', 'CountLinkedin.count_likes', 'CountLinkedin.count_recommendation'),
					'order' => 'CountLinkedin.batch_id DESC , CountLinkedin.created DESC'
				)
			)
		));

		$clause['User.status'] = USER_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		return $this->find('all', array(
					'conditions' => array($clause),
					'limit' => 5
				));
	}

	function search_using_smc($query_params) {

		$this->unbindModelAll();

		$clause['User.status'] = USER_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		$this->bindModel(array(
			'hasOne' => array(
				'UserSocialAccount' => array(
					'className' => 'UserSocialAccount',
					'conditions' => array('UserSocialAccount.status' => USA_STATUS_ACTIVE)
				)
				)));

		return $this->find('all', array(
					'conditions' => array($clause)
				));
	}

	/**
	 * Get minimum & maximum score from Users table
	 * 
	 * Example: `list($min,$max)  =  $this->User->get_min_max();`
	 * 
	 * @return array Minvalue & Maximum value
	 */
	function get_min_max() {

		$this->unbindModelAll();

		$result = $this->find('first', array(
			'conditions' => array(
				'User.role_id > ' => ROLE_ID_ADMIN, // excluding admin  
				'User.status' => USA_STATUS_ACTIVE, // only active users
			),
			'fields' => array(
				'MIN(User.score) AS MinScore',
				'MAX(User.score) AS MaxScore'
			)
				));

		return array($result[0]['MinScore'], $result[0]['MaxScore']);
	}

	/**
	 * Get access tokens to validate . But only those access token which has not expired yet i.e is_valid_token = 0
	 * 
	 * @param array $query_params where clause
	 * Example: $query_params['ModelName.fieldName'] = 'value'
	 * 
	 * Special note for screen_name fields in user table. Here I have used condition "<>" in array & also array_merge is used which might create problem for this field i.e. 
	 * 
	 * $clause['User.screen_name <>'] = '' ;
	 * 
	 * So before using statement like this $query_params['User.screen_name'] = 'value' make sure to check body of function.
	 * 
	 * @return array Query result
	 */
	function get_access_tokens($query_params = 0) {
		
		$clause['User.status'] = USER_STATUS_ACTIVE;
		$clause['User.screen_name <>'] = '' ;
		
		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);
		
		$this->unbindModelAll();
		
		$this->bindModel(array(
			'hasMany' => array(
				'UserSocialAccount' => array(
					'className' => 'UserSocialAccount',
					'conditions' => array(
						'UserSocialAccount.status' => USA_STATUS_ACTIVE,
						'UserSocialAccount.is_valid_token' => USA_IS_VALID_TOKEN
					),
					'fields' => array(
						'UserSocialAccount.id',
						'UserSocialAccount.type_id',
						'UserSocialAccount.link_id',
						'UserSocialAccount.access_token',
						'UserSocialAccount.access_token_secret',
						'UserSocialAccount.is_valid_token'
					)
				)
			)
		));
		return $this->find('all', array(
					'conditions' => array($clause),
					'fields' => array('User.id', 'User.screen_name')
				));
	}
	
	/**
	 * Get access token information irrespective of valid/invalid
	 * 
	 * @param array $query_params where clause
	 * Example: $query_params['ModelName.fieldName'] = 'value'
	 * 
	 * Special note for screen_name fields in user table. Here I have used condition "<>" in array & also array_merge is used which might create problem for this field i.e. 
	 * 
	 * $clause['User.screen_name <>'] = '' ;
	 * 
	 * So before using statement like this $query_params['User.screen_name'] = 'value' make sure to check body of function.
	 * 
	 * @return array Query result
	 */
	function get_token_info($query_params = 0) {
		
		$clause['User.status'] = USER_STATUS_ACTIVE;
		$clause['User.screen_name <>'] = '' ;
		
		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);
		
		$this->unbindModelAll();
		
		$this->bindModel(array(
			'hasMany' => array(
				'UserSocialAccount' => array(
					'className' => 'UserSocialAccount',
					'conditions' => array(
						'UserSocialAccount.status' => USA_STATUS_ACTIVE
					),
					'fields' => array(
						'UserSocialAccount.id',
						'UserSocialAccount.type_id',
						'UserSocialAccount.link_id',
						'UserSocialAccount.access_token',
						'UserSocialAccount.access_token_secret',
						'UserSocialAccount.is_valid_token'
					)
				)
			)
		));
		return $this->find('first', array(
					'conditions' => array($clause),
					'fields' => array('User.id', 'User.screen_name')
				));
	}
	
	/**
	 * Delete user & all data associated with it
	 * 
	 * @param string|int $user_id System generated user id
	 * @return boolean 
	 */
	function remove($user_id){
		
		$this->delete($user_id);
		
		$sql = 'DELETE FROM user_social_accounts WHERE user_id = ' . $user_id ;		
		$this->query($sql);
		
		$sql = 'DELETE FROM count_facebooks WHERE user_id = ' . $user_id ;		
		$this->query($sql);
		
		$sql = 'DELETE FROM count_linkedins WHERE user_id = ' . $user_id ;		
		$this->query($sql);
		
		$sql = 'DELETE FROM count_twitters WHERE user_id = ' . $user_id ;		
		$this->query($sql);
		
		$sql = 'DELETE FROM scores WHERE user_id = ' . $user_id ;		
		$this->query($sql);
		
		return true ;
	}

}