<?php

define('CRON_PQUEUE_STATUS_INPENDING', 'inpending');
// http://stackoverflow.com/questions/9128488/cron-job-server-issue
define('CRON_SMC_BASE_URI', 'http://dev.socialmedia.com');
define('CRON_SMC_SERVER_NAME', 'dev.socialmedia.com');
define('MULTITHREAD_USERS_LIMIT', 200);

require_once('../Config/database.php');

/* * * 
 * 
 * USING CURL - But CURL WONT WORK IN CLI
 * 
  //add a url to the handler
  function add_handle(&$curl_handle, $url) {
  $cURL = curl_init();
  curl_setopt($cURL, CURLOPT_URL, $url);
  curl_setopt($cURL, CURLOPT_HEADER, 0);
  curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
  curl_multi_add_handle($curl_handle, $cURL);
  return $cURL;
  }

  //execute the handle until the flag passed to function is greater then 0
  function exec_handle(&$curl_handle) {
  $flag = null;
  do {
  curl_multi_exec($curl_handle, $flag); //fetch pages in parallel
  } while ($flag > 0);
  }

  // database connection
  $database_config = new DATABASE_CONFIG();
  $host = $database_config->default['host'];
  $login = $database_config->default['login'];
  $password = $database_config->default['password'];
  $database = $database_config->default['database'];
  $link = @mysql_connect($host, $login, $password);

  if (!$link)
  die('Could not connect: ' . mysql_error());

  mysql_select_db($database, $link);

  // Perform Query
  $query = "SELECT ProcessQueue.user_id FROM process_queues ProcessQueue WHERE ProcessQueue.status = '". CRON_PQUEUE_STATUS_INPENDING ."' LIMIT " . MULTITHREAD_USERS_LIMIT ;

  $result = mysql_query($query);

  // Check result
  // This shows the actual query sent to MySQL, and the error. Useful for debugging.
  if (!$result) {
  $message = 'Invalid query: ' . mysql_error() . "\n";
  $message .= 'Whole query: ' . $query;
  die($message);
  }

  $curl_handle = curl_multi_init();
  $i = 0;
  $curl = array();

  while ($row = mysql_fetch_assoc($result)) {
  // exec("php cron_dispatcher.php users/get_social_stats/" . $row['user_id']);
  $curl[$i++] = add_handle($curl_handle, CRON_SMC_BASE_URI . '/users/get_social_stats/' . $row['user_id']);
  }

  exec_handle($curl_handle);

  for ($j = 0; $j < count($curl); $j++)//remove the handles
  curl_multi_remove_handle($curl_handle, $curl[$j]);

  curl_multi_close($curl_handle);

  @mysql_close($link);


 */

# ALTERNATIVE
##### Reference URL : http://phplens.com/phpeverywhere/?q=node/view/254 ########
####### CONTROL ##########

function job_start_async($server, $url, $port = 80, $conn_timeout = 30, $rw_timeout = 86400) {
    $errno = '';
    $errstr = '';

    set_time_limit(0);

    $fp = fsockopen($server, $port, $errno, $errstr, $conn_timeout);
    if (!$fp) {
	echo "$errstr ($errno)<br />\n";
	return false;
    }
    $out = "GET $url HTTP/1.1\r\n";
    $out .= "Host: $server\r\n";
    $out .= "Connection: Close\r\n\r\n";

    stream_set_blocking($fp, false);
    stream_set_timeout($fp, $rw_timeout);
    fwrite($fp, $out);

    return $fp;
}

// returns false if HTTP disconnect (EOF), or a string (could be empty string) if still connected
function job_poll_async(&$fp) {
    if ($fp === false || gettype($fp) == 'unknown type')
	return false;

    if (feof($fp)) {
	fclose($fp);
	$fp = false;
	return false;
    }

    return fread($fp, 10000);
}

function filter_criteria($value) {
    return($value === false);
}

############## BUSINESS LOGIC ##############
// database connection
$database_config = new DATABASE_CONFIG();
$host = $database_config->default['host'];
$login = $database_config->default['login'];
$password = $database_config->default['password'];
$database = $database_config->default['database'];
$link = @mysql_connect($host, $login, $password);

if (!$link)
    die('Could not connect: ' . mysql_error());

mysql_select_db($database, $link);

// Perform Query
$query = "SELECT ProcessQueue.user_id FROM process_queues ProcessQueue WHERE ProcessQueue.status = '" . CRON_PQUEUE_STATUS_INPENDING . "' LIMIT " . MULTITHREAD_USERS_LIMIT;

$result = mysql_query($query);

// Check result
// This shows the actual query sent to MySQL, and the error. Useful for debugging.
if (!$result) {
    $message = 'Invalid query: ' . mysql_error() . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

$fp_list = array();
$response = array();
$i = 0;

while ($row = mysql_fetch_assoc($result)) {
    // exec("php cron_dispatcher.php users/get_social_stats/" . $row['user_id']);    
    $fp_list[$i++] = job_start_async(CRON_SMC_SERVER_NAME, '/users/get_social_stats/' . $row['user_id']);
}

######### WARNING ##############
# When reading from anything that is not a regular local file, such as streams returned when reading remote files or from 
# popen() and fsockopen(), reading will stop after a packet is available. This means that you should collect the data together
# in chunks as shown in the examples below
# since we are collecting data together in chunks so we keep will in loop until & unless all data is fetched
while (true) {
    sleep(1);
    foreach ($fp_list as $key => $value) {
	$response[$key] = job_poll_async($value);
    }
    // will return an array with only false datatype
    $all_same = array_filter($response, 'filter_criteria');
    // if all elements of array are of same datatype
    if (count($all_same) == count($response))
	break;
    
    /** 
     # FOR DEBUG PURPOSE
    foreach ($response as $key => $value) {
	echo "<b>r$key = </b>$value<br>";
    } 
    */

    flush();
    @ob_flush();
}

 @mysql_close($link);
?>
