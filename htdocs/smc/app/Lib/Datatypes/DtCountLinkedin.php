<?php

class DtCountLinkedin extends DtUser {

	public $fields = array();
	// before altering _keys please review class code first as its being used
	private $_keys = array('count_followers', 'count_comments', 'count_likes', 'count_recommendation');

	const BENCHMARK_KEYWORD_PREFIX = 'li_';

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			if (in_array($key, $this->_keys)) {
// below value will be used for benchmarked purpose & is used in other calculations
				$this->$key = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
// below value is actual value recieved from API & is stored as it is.
				$this->{'actual_' . $key} = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
			}
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
			case 'schedule_date':
				$this->fields[$key] = new DtDate($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields() {
		return $this->fields;
	}

	/*	 * *********** Garbage - depends on formulae so no need to save it in db **************** */

	public function get_engagement() {
		return $this->count_recommendation;
	}

	public function get_influence() {
		return $this->count_comments + $this->count_recommendation;
	}

	public function get_reach() {
		return $this->count_followers + $this->count_recommendation;
	}

	/*	 * *********** END: Garbage **************** */

	/**
	 * Get total score on already benchmarked values
	 * 
	 * @return float total score
	 */
	function get_score() {

		$score = $this->count_followers + $this->count_comments + $this->count_likes + $this->count_recommendation;

		return $score;
	}

	/**
	 * Get likes value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_likes
	 */
	function get_bm_count_likes($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_likes / $bm_value['benchmark']))));
	}

	/**
	 * Get comments value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_comments
	 */
	function get_bm_count_comments($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_comments / $bm_value['benchmark']))));
	}

	/**
	 * Get followers value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_followers
	 */
	function get_bm_count_followers($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_followers / $bm_value['benchmark']))));
	}

	/**
	 * Get recommendation value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_recommendation
	 */
	function get_bm_count_recommendation($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_recommendation / $bm_value['benchmark']))));
	}

	/**
	 * Get engagement with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float engagement
	 */
	function get_bm_engagement($benchmarks) {

		$engagement = ($benchmarks['li_count_followers']['engagement'] * $this->count_followers) +
				($benchmarks['li_count_recommendation']['engagement'] * $this->count_recommendation);

		return $engagement;
	}

	/**
	 * Get reach with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float reach
	 */
	function get_bm_reach($benchmarks) {

		$reach = ($benchmarks['li_count_followers']['reach'] * $this->count_followers) +
				($benchmarks['li_count_recommendation']['reach'] * $this->count_recommendation);

		return $reach;
	}

	/**
	 * Get influence with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float influence
	 */
	function get_bm_influence($benchmarks) {

		$influence = ($benchmarks['li_count_followers']['influence'] * $this->count_followers) +
				($benchmarks['li_count_recommendation']['influence'] * $this->count_recommendation);

		return $influence;
	}

	/**
	 * Get total score & also apply benchmarks on required values/fields
	 * 
	 * @return float total score
	 */
	function get_bm_score($benchmarks) {

		$score = 0;

		// fields we need for benchmarking
		$bfields = $this->get_benchmarked_fields();

		foreach ($bfields as $field => $value) {
			if (
					is_numeric($value) &&
					!empty($benchmarks[self::BENCHMARK_KEYWORD_PREFIX . $field])
			) {
				$function_name = 'get_bm_' . $field;
				$score += $this->$function_name($benchmarks[self::BENCHMARK_KEYWORD_PREFIX . $field]);
			}
		}
		return DtUser::format_number($score);
	}

	/**
	 * Get necessary fields for display purpose. This function is specifically used for action `get_user_stats` in `Users` controller. To make logic generic.
	 * 
	 * @return array necessary fields
	 */
	function get_rendering_fields() {
		// '', '', '', ''
		return array(
			'count_followers' => $this->actual_count_followers,
			'count_comments' => $this->actual_count_comments,
			'count_likes' => $this->actual_count_likes,
			'count_recommendation' => $this->actual_count_recommendation,
			'count_followers_trends' => $this->count_followers_trends,
			'count_comments_trends' => $this->count_comments_trends,
			'count_likes_trends' => $this->count_likes_trends,
			'count_recommendation_trends' => $this->count_recommendation_trends
		);
	}

	/**
	 * Get actual values recieved from API
	 * 
	 * @return array actual fields (key-value pair)
	 */
	function get_actual() {
		$actual_fields = array();
		foreach ($this->_keys as $value) {
			$actual_fields['actual_' . $value] = $this->$value;
		}

		return $actual_fields;
	}

	/**
	 * Get benchmarked fields. Original any social param/fields store original value. We alter it when calc_benchmarks is called in DtUser class. 
	 * 
	 * @return array benchmarked fields (key-value pair)
	 */
	function get_benchmarked_fields() {
		$benchmark_fields = array();
		foreach ($this->_keys as $value) {
			$benchmark_fields[$value] = $this->$value;
		}

		return $benchmark_fields;
	}

}