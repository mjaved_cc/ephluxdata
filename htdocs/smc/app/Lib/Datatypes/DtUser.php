<?php

class DtUser {

	public $fields = array();

	public function __construct($data = null) {

		if (isset($_SESSION['Auth']['User']['id']) && $_SESSION['Auth']['User']['id'] > ROLE_ID_ADMIN) {
			foreach ($data as $key => $value) {
				$this->$key = (!empty($value)) ? $value : '0';
			}
		} else {
			foreach ($data as $key => $value) {
				$this->$key = (!empty($value)) ? $value : ''; // in web we do not want to show 0 if no info found 
			}
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'User';
				$this->set_user($value);
				break;
			case 'UserSocialAccount';
				$this->set_user_social_acc($value, $key);
				break;
			case 'CountFacebook';
				$this->fields[$key] = new DtCountFacebook($value);
				break;
			case 'CountTwitter';
				$this->fields[$key] = new DtCountTwitter($value);
				break;
			case 'CountLinkedin';
				$this->fields[$key] = new DtCountLinkedin($value);
				break;
			case 'Score';
				$this->fields[$key] = new DtScore($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	private function set_user($data) {

		$date_fields = array('created', 'modified', 'schedule_date');

		foreach ($data as $key => $value) {
			if (in_array($key, $date_fields))
				$this->fields[$key] = new DtDate($value);
			else if ($key == 'image_url') {
				$this->fields[$key] = $this->get_image($value);
			} else
				$this->fields[$key] = $value;
		}
	}

	private function set_user_social_acc($data, $model) {
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$this->fields[$model][$key] = new DtUserSocialAccount($value);
			}
		}
	}

	public function get_fields() {

		return $this->fields;
	}

	public static function get_trends($new_value, $old_value) {
		// to avoid divided by zero 
		$trend_value = ($new_value > 0) ? ((($new_value - $old_value) / $new_value ) * 100) : 0;

		return self::format_trend($trend_value);
	}

	public static function format_trend($number) {
		// trends are in % 
		$number = self::format_number($number);

		if ($number >= 0) {
			return '+' . $number; // this will convert +ve float value to strings. symbol % is used @ iphone side
		} else
			return (string) $number; // here typecasting is needed as -ve sign is already present with number. symbol % is used @ iphone side
	}

	public static function format_number($number) {

		return number_format($number, 5, '.', '');
	}

	public static function calc_trends($obj_users) {

		$SOCIAL_NETWORKS_MODELS = Configure::read('SOCIAL_NETWORKS_MODELS');

		foreach ($obj_users as $key => $obj_user) {
			
			$next_idx = $key + 1; // next index as we are concerned with new & old values as obj_users is sorted in DESC
			$user_fields = $obj_user->get_fields();
			
			foreach ($user_fields as $social_key => &$obj_social) {
				
				// our only concern in SOCIAL_NETWORKS_MODELS
				if (in_array($social_key, $SOCIAL_NETWORKS_MODELS)) {
					
					// fields we need for benchmarking
					$social_bfields = $obj_social->get_benchmarked_fields();
					
					foreach ($social_bfields as $field => $social_value) {
						
						if (array_key_exists(($next_idx), $obj_users)) { // to ensure if array out of bound not occur
							
							$obj_social->{$field . '_trends'} = self::get_trends($social_value, $obj_users[$next_idx]->$social_key->$field);
						} else {
							
							$obj_social->{$field . '_trends'} = self::format_trend(0);
						}
					}
				}

				if ($social_key == 'Score') {
					
					if (array_key_exists(($next_idx), $obj_users))							
						$obj_social->total_score_trends = self::get_trends($obj_social->total_score, $obj_users[$next_idx]->$social_key->total_score);
					else
						$obj_social->total_score_trends = self::format_trend(0);
				}
			}
		}
	}

	public static function calc_benchmarks($obj_users, $benchmarks) {

		$SOCIAL_NETWORKS_MODELS = Configure::read('SOCIAL_NETWORKS_MODELS');

		foreach ($obj_users as $key => &$obj_user) {
			
			$user_fields = $obj_user->get_fields();
			
			foreach ($user_fields as $social_key => &$obj_social) {
				
				// our only concern in SOCIAL_NETWORKS_MODELS
				if (in_array($social_key, $SOCIAL_NETWORKS_MODELS)) {
					
					$class_name = PREFIX_DATATYPE_CLASS . $social_key;
					$prefix = $class_name::BENCHMARK_KEYWORD_PREFIX;
					
					// fields we need for benchmarking
					$social_bfields = $obj_social->get_benchmarked_fields();
					
					foreach ($social_bfields as $field => $social_value) {
						$function_name = 'get_bm_' . $field;

						if (isset($benchmarks[$prefix . $field]))
							$obj_social->$field = self::format_number($obj_social->$function_name($benchmarks[$prefix . $field]));
						else
							$obj_social->$field = self::format_number($social_value);
					}

					$social_nw_info = explode('Count', $social_key);
					$social_nw = strtolower($social_nw_info[1]);

					$obj_user->Score->{'engagement_' . $social_nw} = $obj_social->get_bm_engagement($benchmarks);
					$obj_user->Score->{'influence_' . $social_nw} = $obj_social->get_bm_influence($benchmarks);
					$obj_user->Score->{'reach_' . $social_nw} = $obj_social->get_bm_reach($benchmarks);
					// since bechmark is applied on indivisual fields so we just need to sum those only.
					// get_score function does the same
					$obj_user->Score->{'score_' . $social_nw} = self::format_number($obj_social->get_score());
					$obj_user->Score->{'total_score'} += $obj_user->Score->{'score_' . $social_nw};
				}

				if ($social_key == 'Score') {
					$obj_social->total_score = 0;
				}
			}
		}
	}

	function get_image($image_url) {
		if (!empty($image_url)) {
			return $image_url;
		} else {
			return SMC_BASE_URI . '/img/default.png';
		}
	}

}