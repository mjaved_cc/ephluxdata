<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

// custom includes
App::uses('DtUser', 'Lib/Datatypes');
App::uses('DtUserSocialAccount', 'Lib/Datatypes');
App::uses('DtDate', 'Lib/Datatypes');
App::uses('DtCountFacebook', 'Lib/Datatypes');
App::uses('DtCountTwitter', 'Lib/Datatypes');
App::uses('DtCountLinkedin', 'Lib/Datatypes');
App::uses('DtScore', 'Lib/Datatypes');
App::uses('PushNotification', 'Lib/PushNotification');

// END: custom includes

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $name = 'App';
	public $helpers = array('Html', 'Session', 'Form');
	public $components = array('Session', 'Cookie', 'RequestHandler', 'Auth', 'SocialNetwork', 'RememberMe');
	public $uses = array('Benchmark');
	public $cacheAction = true;
	public $twitter = null;
	public $facebook = null;
	public $linkedin = null;
	public $benchmarks = array();

	function beforeFilter() {
		// $this->Auth->allow('index');

		$this->Auth->loginAction = array('controller' => 'backoffices', 'action' => 'login');
		$this->Auth->loginRedirect = array('controller' => 'backoffices', 'action' => 'index');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'index');
		$this->Auth->loginError = 'Hmmm. We didn\'t recognise your login details. Want to try again?';

		// Add a pattern value detector.
		$this->request->addDetector('iphone', array('env' => 'HTTP_USER_AGENT', 'pattern' => '/iPhone/i'));

		if ($this->request->is('iphone') && $this->request->params['ext'] != LAYOUT_JSON)
			die('Bad Request');

		$this->Auth->authenticate = array(
			'Form' => array(
				'scope' => array('User.status' => 'active')
			)
		);

		// without hashing Password (cakephp default ) and use only md5
		Security::setHash("md5");

		// again, you can customize the component settings here, like this:  
		// $this->RememberMe->period = '+2 months';  
		$this->RememberMe->cookieName = 'SMC';

		$this->RememberMe->check();

		$this->benchmarks = $this->Session->read('Benchmarks');
		if (empty($this->benchmarks))
			$this->set_benchmarks();

		$this->set('is_loggedin', $this->Auth->User());
	}

	function is_valid_email($email = null) {
		if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
			return true;
		return false;
	}

	function get_current_datetime() {
		return date('Y-m-d H:i:s');
	}

	/*
	 * @desc: get expiry date from expired_at field of Tmp table
	 */

	function get_tmp_expiry() {
		return date('Y-m-d', strtotime("+3 month"));
	}

	function send_email($to, $subject, $message, $template = 'default', $layout = 'default') {

		$email = new CakeEmail('dev');
		$email->to($to);
		$email->template($template, $layout);
		$email->emailFormat('html');
		$email->viewVars(array('message' => $message));
		$email->subject($subject);
		$email->send();
	}

	/*	 * ***** How to save IP Address in DB ****** */
	/* INET_NTOA and INET_ATON functions in mysql. They convert between dotted notation IP address to 32 bit integers. This allows you to store the IP in just 4 bytes rather than a 15 bytes 
	 */

	/*
	  @desc: get an (IPv4) Internet network address into a string in Internet standard dotted format.
	  @param: number <string>
	  @return dotted ip address <string>
	 */

	function get_ip_address($number) {
		// analogous to INET_NTOA in mysql
		return sprintf("%s", long2ip($number));
	}

	/*
	  @desc: get string containing an (IPv4) Internet Protocol dotted address into a proper address
	  @param: null
	  @return number <unsigned-int>
	 */

	function get_numeric_ip_representation() {
		// analogous to INET_ATON in mysql
		return sprintf("%u", ip2long($_SERVER['REMOTE_ADDR']));
	}

	/*	 * ***** END: How to save IP Address in DB ***** */

	/*
	 * Parameters: 
	 *   $data - The sort array in DESC.
	 *   $first - First index of the array to be searched.
	 *   $last - Last index of the array to be searched.
	 *   $key - The key to be searched for.
	 *
	 * Return:
	 *   index of the search key if found, otherwise return (-1). 
	 */

	function binary_search(array $data, $first, $last, $key) {
		if ($first > $last)
			return -1;
		$mid = $first + floor(($last - $first) / 2);

		if ($data[$mid]['created_time'] == $key)
			return $mid;
		else if ($data[$mid]['created_time'] < $key)
			return $this->binary_search($data, $first, $mid - 1, $key);
		else
			return $this->binary_search($data, $mid + 1, $last, $key);
	}

	/*
	 * @desc: send push notification to iphone
	 * @params: device_token <string> , message <string>
	 * @return: null
	 */

	function send_push($device_token, $message) {

		$push = new PushNotification();
		$push->device_token = $device_token;
		$push->message = $message;
		$push->send();
	}

	/*
	 * @desc: format device token properly by removing space & other special char
	 * @param: device_token <string>
	 * @return: device_token <string>
	 */

	function get_formatted_dtoken($device_token) {
		// http://stackoverflow.com/questions/2326125/remove-multiple-whitespaces-in-php
		return preg_replace(array('/\s+/', '/[<>]/'), '', $device_token);
	}

	/*
	 * @desc: fetch & set benchmarks.
	 * @param: null
	 * @return: null
	 */

	function set_benchmarks() {

		$data = $this->Benchmark->get_all();

		if (count($data) > 0) {
			foreach ($data as $value)
				$this->benchmarks[$value['Benchmark']['keyword']] = $value['Benchmark'];
			$this->Session->write('Benchmarks', $this->benchmarks);
		}
	}

	/*
	 * @desc: custom database connect. Please close connection after using it.
	 * @param: null
	 * @return: null
	 */

	function custom_connect() {
		$a = $this->User->getDataSource()->config;
		$host = $a['host'];
		$login = $a['login'];
		$password = $a['password'];
		$database = $a['database'];
		$link = @mysql_connect($host, $login, $password);

		if (!$link)
			die('Could not connect: ' . mysql_error());

		mysql_select_db($database, $link);

		return $link;
	}
	
	/**
	 * Check if webservice request is valid or not. Every service is required hash key for security purpose.
	 * 
	 * All services use post method for submiting data. So hash key is checked if parametres are posted.
	 * 
	 * Return true in case of cron jobs
	 * 
	 * @return boolean true if valid false otherwise
	 */
	function is_valid_wservice() {

		if ($this->request->is('post')) {
			$SALT = Configure::read('Security.salt');
			$posted_params = $this->request->data;
			unset($posted_params['hash']);
			$params = array_keys($posted_params);

			$hash = md5(implode('+', $params) . '+' . $SALT);
			
			if($hash == $this->request->data['hash'])
				return true ;
			else 
				return false ;
		} else 
			return true ;
	}

}