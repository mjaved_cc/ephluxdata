<?php

App::uses('FacebookApi', 'Lib/Facebook');
App::uses('TwitterApi', 'Lib/Twitter');
App::uses('LinkedinApi', 'Lib/Linkedin');

class SocialNetworkComponent extends Component {

    function init() {

	// social network
	$twitter = new TwitterApi();
	$facebook = new FacebookApi();
	$linkedin = new LinkedinApi();

	return array($twitter, $facebook, $linkedin);
    }

    function fb_callback($obj_fb) {
	return $obj_fb->getAccessToken();
    }

    function tw_callback($obj_tw) {
	return $obj_tw->getAccessToken();
    }

    function li_callback($obj_li) {
	return $obj_li->getAccessToken();
    }

}

?>
