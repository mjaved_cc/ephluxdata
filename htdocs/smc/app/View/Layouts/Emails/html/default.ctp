<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$desc = __d('cake_dev', SITE_NAME);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
	<title><?php echo $desc ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
	    a:link {color: #909090; text-decoration:none; outline:none}
	    a:visited {color: #909090; text-decoration:none; outline:none}
	    .a1 a:link {color:#202020; text-decoration:none; outline:none}
	    .a1 a:visited {color:#202020; text-decoration:none; outline:none}
	    .a2 a:link {color:#7c7c7c; text-decoration:none; outline:none}
	    .a2 a:visited {color:#7c7c7c; text-decoration:none; outline:none}

	    <!-- li a:link {color: #777777; text-decoration:none; outline:none}
	    li a:visited {color: #777777; text-decoration:none; outline:none} -->
	</style>
    </head>
    <body style="background-color:#30312d; margin:0; padding:0; line-height:1.2em; font-family:Trebuchet MS, sans-serif">
	<table width="650" cellpadding="0" cellspacing="0" align="center" border="0">
	    <tr>
		<td valign="top">
		    <!--  B E G I N    H E A D E R -->
		    <table width="650" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color:#e1e1d5">
			<tr>
			    <td height="30">
			    </td>
			</tr>
			<tr>
			    <td width="25">
			    </td>
			    <td>
				<a href="JavaScript:void(0);" target="_blank"><img src="<?php echo SMC_BASE_URI; ?>/img/logo.png" border="0" width="72" height="72" alt="" style="display: block" /></a>
			    </td>
			    <td align="right" width="385" style="padding-right:20px; font-size:18px; color: #202020; line-height:1.3em">
				Social Media Calculator<br />
				<span style="font-size:12px; color: #7c7c7c">You are now Member of Our Community</span>
			    </td>
			    <td width="20">
			    </td>
			</tr>
			<tr>
			    <td height="30">
			    </td>
			</tr>
		    </table>
		    <table width="650" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color:#ffffff; margin-bottom:20px">
			<tr>
			    <td height="2" style="background-color:#e1e1d5">
			    </td>
			</tr>
		    </table>
		    <!-- E N D    H E A D E R -->

		    <!-- B E G I N     B L O C K -->
		    <?php echo $content_for_layout; ?>	
		    <!-- E N D    4     B L O C K -->
		   
		</td>
	    </tr>
	</table>
    </body>
</html>