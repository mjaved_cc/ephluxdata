<div class="users form"> 
	<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User > ' . $this->request->data['User']['screen_name']); ?></legend>
		<?php 
		echo $this->Form->input('User.status', array('type' => 'select', 'options' => $USER_STATUS));
		// echo $this->Form->input('User.password');
		if(!empty($this->data['User']['username'])) // if user email exists
			echo $this->Html->link(__('Change Password'), array('controller' => 'users','action' => 'change_pwd', md5($this->data['User']['id'])));
		else 
			echo 'No option for change password as email not exists.' ;
		?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('sidebar'); ?>
