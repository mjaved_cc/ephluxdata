<div id="dv_search_user" class="users index">
	<form id="frm_search_user" action="<?php echo $this->webroot ?>backoffices/search" method="get">
		<label>Search (screen name or email):</label>
		<p><input type="text" name="q" /></p>
		<input type="submit" value="Search" class="fr" />
	</form>
	<?php
	if (isset($this->request->query['q']))
		echo $this->element('display_users', array('data' => $data , 'class' => ''));
	?>
</div>
<?php echo $this->element('sidebar'); ?>