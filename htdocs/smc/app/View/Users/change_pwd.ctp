<script type="text/javascript">
    $(function() {
        $('#passForm').validate({
		
			rules: {
				password: {
					required: true,
					minlength: 5
				},
				cpassword: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				}
			}	
		
		});
    });
</script>
<div class="users form">
	<div class="header clearfix">
		<h2>Reset password > <?php echo $screen_name ?></h2>
	</div>
	<div class="body">
		<?php echo $this->Form->create('User'); ?>
		<fieldset>
			<legend>Enter new credentials</legend>
			<p>
				<label>Enter your new password:</label>
				<input type="password" name="password" id="password"  required="required" minlength="5" />
			</p>
			<p>
				<label>Confirm password:</label>
				<input type="password" name="cpassword" id="cpassword"  required="required"  equalto="#password" />
			</p>
			<p class="tar">						
				<input type="hidden" name="id" value="<?php echo $hash_uid; ?>" />
			</p>
		</fieldset>
		<?php echo $this->Form->end(__('Submit')); ?>		
	</div>				
</div>
<?php echo $this->element('sidebar'); ?>

