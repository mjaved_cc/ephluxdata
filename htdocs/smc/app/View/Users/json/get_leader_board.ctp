<?php

if (isset($leader_board) && !empty($leader_board)) {
    $data['Response'] = "1";
    $data['Data'] = $leader_board;
} else {
    $data['Response'] = "0";
    $data['Message'] = 'No match found';
}

echo json_encode($data);
?>