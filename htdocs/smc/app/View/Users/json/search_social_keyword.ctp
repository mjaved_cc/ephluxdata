<?php

if (isset($param_empty) && $param_empty) {
    $data['Response'] = "0";
    $data['Message'] = 'Invalid parametres';
} else if (isset($no_social_assoc) && $no_social_assoc) {
    $data['Response'] = "0";
    $data['Message'] = 'No social association found. Please add any social account.';
} else if (isset($results) && !empty($results)) {
    $data['Response'] = "1";
    $data['Data'] = $results;
} else {
    $data['Response'] = "0";
    $data['Message'] = 'No match found';
}

echo json_encode($data);
?>