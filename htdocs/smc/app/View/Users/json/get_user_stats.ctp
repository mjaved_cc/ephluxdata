<?php

if (isset($param_empty) && $param_empty) {
    $data['Response'] = "0";
    $data['Message'] = 'Invalid parametres';
} else if (isset($success) && $success) {
    $data['Response'] = "1";
    $data['Data'] = array_merge($user_details,$stats) ;
} else {
    $data['Response'] = "0";
    $data['Message'] = 'Stats not found';
}

echo json_encode($data);
?>