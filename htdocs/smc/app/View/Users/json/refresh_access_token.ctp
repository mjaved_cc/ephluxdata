<?php

if (isset($param_empty) && $param_empty) {
	$data['Response'] = "0";
	$data['Message'] = 'Invalid parametres';
} else if(isset($invalid_social_account_id) && $invalid_social_account_id) {
	$data['Response'] = "0";
	$data['Message'] = 'Invalid social account';
} else if(isset($success) && $success) {
	$data['Response'] = "1";
	$data['Message'] = 'Tokens refreshed successfully';
}else {
	$data['Response'] = "0";
	$data['Message'] = 'Unable to refresh . Please try agains later.';
}

echo json_encode($data);
?>