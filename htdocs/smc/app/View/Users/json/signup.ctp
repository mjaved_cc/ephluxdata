<?php

if (isset($param_empty) && $param_empty) {
	$data['Response'] = "0";
	$data['Message'] = 'Invalid parametres';
} else if (isset($is_valid) && !$is_valid) {

	$data['Response'] = "0";
	$data['Message'] = 'Invalid Email Address';
} else {
	if (isset($is_already_exist) && $is_already_exist) {
		$data['Response'] = "0";
		$data['Message'] = 'Email Already Exist';
	} else {
		if (isset($is_unverified_exist) && $is_unverified_exist) {
			$data['Response'] = "0";
			$data['Message'] = 'We have found an unverified email. Please check your email';
		} else {
			if ($success) {
				$data['Response'] = "1";
				$data['Message'] = 'Please check email for verification';
			} else {
				$data['Response'] = "0";
				$data['Message'] = 'Unable to signup ; please try again later';
			}
		}
	}
}

echo json_encode($data);
?>