<?php

if (isset($param_empty) && $param_empty) {
    $data['Response'] = "0";
    $data['Message'] = 'Invalid parametres';
} else if (isset($stats) && !empty($stats)) {
    $data['Response'] = "1";
    $data['Data'] = $stats;
} else {
    $data['Response'] = "0";
    $data['Message'] = 'Unable to fetch details. Please try again later.';
}

echo json_encode($data);
?>