<?php
if (isset($param_empty) && $param_empty) {
    $data['Response'] = "0";
    $data['Message'] = 'Invalid parametres';
} else if (isset($invalid_vcode) && $invalid_vcode) {
    $data['Response'] = "0";
    $data['Message'] = 'verification code is invalid';
} else if (isset($already_activated) && $already_activated) {
    $data['Response'] = "0";
    $data['Message'] = 'Account already activated';
} else if (isset($success) && $success) {
    $data['Response'] = "1";
    $data['Message'] = 'Congratulations! You are a Member of Buzz Now';
} else {
    $data['Response'] = "0";
    $data['Message'] = 'Unable to verify your account. Please try again later.';
}
?>
<table width="650" cellpadding="0" cellspacing="0" border="0" align="center" 
       style="background-color:#e1e1d5; font-size:12px; color: #7c7c7c; margin-bottom:20px">
    <tr>
	<td height="20">
	</td>
    </tr>
    <tr>
	<td>
	    <table width="650" cellpadding="0" cellspacing="0" border="0">
		<tr>
		    <td valign="top">
			<table width="60" cellpadding="0" cellspacing="0" border="0">
			    <tr>
				<td width="60" height="52" align="right" 
				    style="background-color:#82738a; padding-right:10px; font-size:20px; color: #ffffff; line-height:0.7em">
				    Buzz<br /><span style="font-size:11px">Community</span>
				</td>
			    </tr>
			    <tr>
				<td>
				</td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		    <td valign="top">
			<table width="550" cellpadding="0" cellspacing="0" border="0" align="center">
			    <tr>
				<td valign="top" style="font-size:18px; color: #202020; background-image:url(images/px.gif);
				    background-repeat:repeat-x; background-position:0 10px">
				    <table cellpadding="0" cellspacing="0" border="0" align="left">
					<tr>
					    <td class="a1" valign="top" style="padding-right:10px">
						<span style="background-color:#e1e1d5; padding-right:10px"><a href="javascript:void(0);"><?php echo $data['Message'] ?></a></span>
					    </td>
					    <td>
					    </td>
					</tr>
				    </table>
				</td>
			    </tr>
<?php if (isset($success) && $success) { ?>
    			    <tr>
    				<td style="padding-top:13px">
    				    Hello Member,

    				    You have successfully verified your account and has become an exclusive member of our Buzz Community. With Buzz you will be able to evaluate your 
    				    efforts and activity. You can compare your social activity with registered members in the community. Please provide us with your feed back at feedback@buzz.com.
    				</td>
    			    </tr>
<?php } ?>
			    <tr>
				<td style="padding-top:13px; padding-bottom:20px"></td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		</tr>
	    </table>
	</td>
    </tr>
    <tr>
	<td height="2" style="background-color:#e1e1d5">
	</td>
    </tr>
</table>