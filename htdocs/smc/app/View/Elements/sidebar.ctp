<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Leader Board'), array('controller' => 'users','action' => 'get_leader_board')); ?></li>
		<li><?php echo $this->Html->link(__('Users'), array('controller' => 'backoffices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Search & Browse User'), array('controller' => 'backoffices', 'action' => 'search')); ?> </li>
	</ul>
</div>