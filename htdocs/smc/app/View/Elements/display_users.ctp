<div class="users <?php echo $class ?>">
	<h2><?php echo __('Users'); ?></h2>
	<?php if (!empty($data)) { ?>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo $this->Paginator->sort('screen_name','Social Name'); ?></th>
				<th><?php echo $this->Paginator->sort('username','Email'); ?></th>
				<th><?php echo $this->Paginator->sort('score'); ?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th><?php echo h('url'); ?></th>
				<th><?php echo h('Logo'); ?></th>
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			<?php
			foreach ($data as $user):
				?>
				<tr>
					<td><?php echo h($user['screen_name']); ?>&nbsp;</td>
					<td><?php echo h($user['username']); ?>&nbsp;</td>
					<td><?php echo h($user['score']); ?>&nbsp;</td>
					<td><?php echo h($user['created']); ?>&nbsp;</td>
					<td><a href="<?php echo h($user['url']); ?>"><?php echo h($user['url']); ?></a>&nbsp;</td>
					<td><img height="80" width="80" src="<?php echo h($user['image_url']); ?>" />&nbsp;</td>		
					<td><?php echo h($user['status']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link(__('Details'), array('action' => 'details', $user['id'])); ?>
						<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['id'])); ?>
						<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<p>
			<?php
			echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>

		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>

	<?php } else echo 'No records found' ?>
</div>