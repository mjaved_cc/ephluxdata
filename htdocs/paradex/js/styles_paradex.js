var appVer = navigator.appVersion.toLowerCase();
var iePos  = appVer.indexOf('msie');
	if (iePos !=-1)
	{
		var is_major = parseInt(is_minor);
		var is_minor = parseFloat(appVer.substring(iePos+5,appVer.indexOf(';',iePos)));
    }
	var ie = (typeof window.ActiveXObject != 'undefined');
	var ie7 = (ie && is_minor >= 7);
	var ie8 = (ie && is_minor >= 8);
	var moz = (typeof document.implementation != 'undefined') && (typeof document.implementation.createDocument != 'undefined');
	if(ie)
	{
		if(ie7)
		{
			if(ie8)
			{
				document.write("<link href=\"css/paradex_styles-ie8.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
			}
			else
			{
				document.write("<link href=\"css/paradex_styles-ie7.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
			}
		}
		else
		{
			document.write("<link href=\"css/paradex_styles-ie6.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
		}
		
	}
	else if(moz)
	{
		document.write("<link href=\"css/paradex_styles-moz.css\" rel=\"stylesheet\" type=\"text/css\"></link>");
	}
	

function fillElement(element,eleValue)
{
	var re = /\s/g;
	var str = element.value.replace(re, ""); 
	if (str.length == 0)
	{
		element.value=eleValue;
	}
}
function emptyElement(element,eleValue)
{
	if(element.value==eleValue)
	{element.value='';}
}
function changeColora(element,eleValue)
{
	if(element.value==eleValue)
	{element.style.color='#000000';}
}
function changeColorn(element,eleValue)
{
	if(element.value==eleValue)
	{element.style.color='#bcbcbc';}
}