<?php

define('PAYPAL_ITEM_NAME', 'Test Item');

define('PAYPAL_ITEM_NO', rand());
define('PAYPAL_PAYMENT_STATUS', 'Completed');
define('PARADEX_EMAIL_SUBJECT', 'Email From Paradex');

// for local & staging (app.test)
if (in_array(getenv('SERVER_ADDR'), array('173.201.208.81','127.0.0.1'))) {
	define('BASE_URL', 'http://' . getenv('SERVER_NAME') . '/paradex/');
	define('PAYPAL_SELLER_EMAIL', 'dev.ep_1361376798_biz@gmail.com');
	define('PARADEX_BASE_URL', 'https://www.sandbox.paypal.com'); // sand box
	define('PAYPAL_ITEM_AMOUNT', '0.01');
	define('PAYPAL_CURRENCY_CODE', 'USD');
	define('PARADEX_CONTACT_EMAIL', 'javed.nedian@gmail');	
	define('PARADEX_SMTP_HOST', 'ssl://smtp.gmail.com');
	define('PARADEX_SMTP_USERNAME', 'dev.ephlux@gmail.com');
	define('PARADEX_SMTP_PASSWORD', 'pxkuhosghgzpithj');
	define('PARADEX_SMTP_PORT', '465');
} else {
	// live code contains whole code no parent folder i.e paradex
	define('BASE_URL', 'http://' . getenv('SERVER_NAME') . '/');
	define('PAYPAL_SELLER_EMAIL', 'mk@paradex.co.uk');
	define('PARADEX_BASE_URL', 'https://www.paypal.com'); //live
	define('PAYPAL_ITEM_AMOUNT', '10');
	define('PAYPAL_CURRENCY_CODE', 'GBP');
	define('PARADEX_CONTACT_EMAIL', 'artist@paradex.co.uk');	
	define('PARADEX_SMTP_HOST', 'mail.paradex.co.uk');
	define('PARADEX_SMTP_USERNAME', 'artist@paradex.co.uk');
	define('PARADEX_SMTP_PORT', '25');
}