<?php

session_start();
require_once('config/config.php');
require_once('lib/PHPMailer/class.phpmailer.php');

$msg = "<html><body>";
$msg = "First Name :" . $_SESSION['first_name'] . "<br />";

$msg .= "Surname :" . $_SESSION['Sur_name'] . "<br />";
$msg .= "Date of Birth :" . $_SESSION['date_of_birth'] . "<br />";
$msg .= "Place of Birth :" . $_SESSION['place_of_birth'] . "<br />";
$msg .= "Address :" . $_SESSION['address_line1'] . ' ' . $_SESSION['address_line2'] . "<br />";
$msg .= "Area :" . $_SESSION['area'] . "<br />";
$msg .= "City :" . $_SESSION['city'] . "<br />";
$msg .= "County :" . $_SESSION['county'] . "<br />";
$msg .= "Country :" . $_SESSION['country'] . "<br />";
$msg .= "Postal Code :" . $_SESSION['postal_code'] . "<br />";
$msg .= "Telephone/Mobile :" . $_SESSION['contact_number'] . "<br />";
$msg .= "Email :" . $_SESSION['email'] . '<br />';
$msg .= "Skype ID :" . $_SESSION['skype'] . '<br />';
$msg .= "Work Description :" . $_SESSION['work_description'] . "<br />";
$msg .= "Link to Websites :" . $_SESSION['website_url'] . "<br />";
$msg .= "Ambition/Vision :" . $_SESSION['ambition'] . "<br />";
$msg .= "Plans for the next 3 months :" . $_SESSION['plans'] . "<br />";

$msg.= "</body></html>";

$mail = new PHPMailer;

$mail->IsSMTP();  // Set mailer to use SMTP
$mail->Host = PARADEX_SMTP_HOST;  // Specify main and backup server
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = PARADEX_SMTP_USERNAME; // SMTP username
$mail->Password = PARADEX_SMTP_PASSWORD;   // SMTP password			
$mail->SMTPAuth = true; // Enable SMTP authentication						
$mail->Port = PARADEX_SMTP_PORT;

$mail->From = $_SESSION['email'];
$mail->FromName = $_SESSION['Sur_name'];
$mail->AddAddress(PARADEX_CONTACT_EMAIL);  // Add a recipient
$mail->WordWrap = 50;   // Set word wrap to 50 characters
$mail->IsHTML(true); // Set email format to HTML

$mail->Subject = PARADEX_EMAIL_SUBJECT;
$mail->MsgHTML($msg);



if ($mail->Send()) {
	header('Location:  payment-successful.htm');
} 
 else {
	header('Location:  payment-successful.htm');
}



?>