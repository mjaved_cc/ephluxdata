<?php

session_start();
require_once('config/config.php');
require_once('lib/PHPMailer/class.phpmailer.php');

// PayPal settings
$paypal_email = PAYPAL_SELLER_EMAIL;
$return_url = BASE_URL . 'payment-successful.htm';
$cancel_url = BASE_URL . 'payment-cancelled.htm';
$notify_url = BASE_URL . 'payments.php';

$item_name = PAYPAL_ITEM_NAME;
$item_amount = PAYPAL_ITEM_AMOUNT;



// Check if paypal request or response
if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])) {

	// Firstly Append paypal account to querystring
	$querystring .= "?business=" . urlencode($paypal_email) . "&";

	// Append amount& currency (£) to quersytring so it cannot be edited in html
	//The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
	$querystring .= "item_name=" . urlencode($item_name) . "&";
	$querystring .= "amount=" . urlencode($item_amount) . "&";

	//loop for posted values and append to querystring
	foreach ($_POST as $key => $value) {
		$value = urlencode(stripslashes($value));
		$querystring .= "$key=$value&";
	}

	// Append paypal return addresses
	$querystring .= "return=" . urlencode(stripslashes($return_url)) . "&";
	$querystring .= "cancel_return=" . urlencode(stripslashes($cancel_url)) . "&";
	$querystring .= "notify_url=" . urlencode($notify_url);

	$file_name = session_id() . $_SESSION['first_name'];
	$file_name = md5($file_name);
	$json_data = json_encode($_SESSION);
	$myFile = $file_name;
	$fh = fopen("files/" . $myFile, 'w') or die("can't open file");
	fwrite($fh, $json_data);
	fclose($fh);

	// Append querystring with custom field
	$querystring .= "&custom=" . $file_name;
	// Redirect to paypal IPN
	header('location:' . PARADEX_BASE_URL . '/cgi-bin/webscr' . $querystring);
	exit();
} else {

	// logging each response in log files
	$file_path = "logs/payment.log";
	$payment_handler = fopen($file_path, 'a') or die("can't open file");
	fwrite($payment_handler, date("d-m-Y H:i:s") . "\r\n");
	fwrite($payment_handler, print_r($_POST, true) . "\r\n");
	fwrite($payment_handler, "\r\n\r\n");
	fclose($payment_handler);

	// Response from Paypal
	// assign posted variables to local variables
	$data['item_name'] = $_POST['item_name'];
	$data['item_number'] = $_POST['item_number'];
	$data['payment_status'] = $_POST['payment_status'];
	$data['payment_amount'] = $_POST['mc_gross'];
	$data['payment_currency'] = $_POST['mc_currency'];
	$data['txn_id'] = $_POST['txn_id'];
	$data['receiver_email'] = $_POST['receiver_email'];
	$data['payer_email'] = $_POST['payer_email'];
	$data['custom'] = $_POST['custom'];

	$myFile = $data['custom'];
	$file_path = "files/" . $myFile;
	$fh = fopen($file_path, 'r');
	$json_data = fgets($fh);
	fclose($fh);

	$form_data = json_decode($json_data, true);

	//Payment has been completed successfully
	if (strcasecmp($data['payment_status'], PAYPAL_PAYMENT_STATUS) == 0 && !empty($form_data)) {

		$msg = "<html><body>";
		$msg = "First Name :" . $form_data['first_name'] . "<br />";

		$msg .= "Surname :" . $form_data['Sur_name'] . "<br />";
		$msg .= "Date of Birth :" . $form_data['date_of_birth'] . "<br />";
		$msg .= "Place of Birth :" . $form_data['place_of_birth'] . "<br />";
		$msg .= "Address :" . $form_data['address_line1'] . ' ' . $form_data['address_line2'] . "<br />";
		$msg .= "Area :" . $form_data['area'] . "<br />";
		$msg .= "City :" . $form_data['city'] . "<br />";
		$msg .= "County :" . $form_data['county'] . "<br />";
		$msg .= "Country :" . $form_data['country'] . "<br />";
		$msg .= "Postal Code :" . $form_data['postal_code'] . "<br />";
		$msg .= "Telephone/Mobile :" . $form_data['contact_number'] . "<br />";
		$msg .= "Email :" . $form_data['email'] . '<br />';
		$msg .= "Skype ID :" . $form_data['skype'] . '<br />';
		$msg .= "Work Description :" . $form_data['work_description'] . "<br />";
		$msg .= "Link to Websites :" . $form_data['website_url'] . "<br />";
		$msg .= "Ambition/Vision :" . $form_data['ambition'] . "<br />";
		$msg .= "Plans for the next 3 months :" . $form_data['plans'] . "<br />";

		$msg.= "</body></html>";

		$mail = new PHPMailer;

		$mail->IsSMTP();  // Set mailer to use SMTP
		$mail->Host = PARADEX_SMTP_HOST;  // Specify main and backup server
		$mail->SMTPAuth = true; // Enable SMTP authentication
		$mail->Username = PARADEX_SMTP_USERNAME; // SMTP username
		$mail->Password = PARADEX_SMTP_PASSWORD;   // SMTP password			
		$mail->SMTPAuth = true; // Enable SMTP authentication						
		$mail->Port = PARADEX_SMTP_PORT;

		$mail->From = $form_data['email'];
		$mail->FromName = $form_data['Sur_name'];
		$mail->AddAddress(PARADEX_CONTACT_EMAIL);  // Add a recipient
		$mail->WordWrap = 50;   // Set word wrap to 50 characters
		$mail->IsHTML(true); // Set email format to HTML

		$mail->Subject = PARADEX_EMAIL_SUBJECT;
		$mail->MsgHTML($msg);

		$file_path = "logs/email.log";
		$email_handler = fopen($file_path, "a");
		fwrite($email_handler, date("d-m-Y H:i:s") . "\r\n");

		if (!$mail->Send()) {

			fwrite($email_handler, $mail->ErrorInfo . "\r\n");
		} else {
			// on success delete created files
			unlink($file_path);
			fwrite($email_handler, 'Email sent. Txt-id ' . $_POST["txn_id"] . "\r\n");
		}

		fwrite($email_handler, "\r\n\r\n");
		fclose($email_handler);
	}
}
?>