<?php
	session_start();
require_once('recaptchalib.php');
$privatekey = "6LdGON0SAAAAAIab1_LapC1uVBNTG8z6dlTbvbzY";
$resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
$a['code'] = 1;

if (!$resp->is_valid) {
	// What happens when the CAPTCHA was entered incorrectly
	$a['msg'] = "The CAPTCHA wasn't entered correctly. ";
} else {
	$a['code'] = 0;
	// Your code here to handle a successful verification
	$a['msg'] = "successful";
	
	$_SESSION['first_name'] = $_POST['first_name'];
	$_SESSION['Sur_name'] = $_POST['Sur_name'];
	$_SESSION['date_of_birth'] = $_POST['date_of_birth'];
	$_SESSION['place_of_birth'] = $_POST['place_of_birth'];
	$_SESSION['address_line1'] = $_POST['address_line1'];
	$_SESSION['address_line2'] = $_POST['address_line2'];
	$_SESSION['area'] = $_POST['area'];
	$_SESSION['city'] = $_POST['city'];
	$_SESSION['county'] = $_POST['county'];
	$_SESSION['country'] = $_POST['country'];
	$_SESSION['postal_code'] = $_POST['postal_code'];
	$_SESSION['contact_number'] = $_POST['contact_number'];
	$_SESSION['email'] = $_POST['email'];
	$_SESSION['skype'] = $_POST['skype'];
	$_SESSION['work_description'] = $_POST['work_description'];
	$_SESSION['website_url'] = $_POST['website_url'];
	$_SESSION['ambition'] = $_POST['ambition'];
	$_SESSION['plans'] = $_POST['plans'];
	
	
}

echo json_encode($a);
?>