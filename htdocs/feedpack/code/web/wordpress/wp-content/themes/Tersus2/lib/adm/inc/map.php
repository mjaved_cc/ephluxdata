<?php


/* ------------------------------------
:: OPTIONS FUNCTION
------------------------------------*/

	function get_options_array( $type )
	{
		if( $type == 'transition' )
		{
			return $transition_effect = array("linear","easeInSine","easeOutSine", "easeInOutSine", "easeInCubic", "easeOutCubic", "easeInOutCubic", "easeInQuint", "easeOutQuint", "easeInOutQuint", "easeInCirc", "easeOutCirc", "easeInOutCirc", "easeInBack", "easeOutBack", "easeInOutBack", "easeInQuad", "easeOutQuad", "easeInOutQuad", "easeInQuart", "easeOutQuart", "easeInOutQuart", "easeInExpo", "easeOutExpo", "easeInOutExpo", "easeInElastic", "easeOutElastic", "easeInOutElastic", "easeInBounce", "easeOutBounce", "easeInOutBounce");	
		}

		if( $type == 'animation' )
		{
			return $animation_types = array("fade","blindY","blindZ","blindX","cover","curtainX","curtainY","fadeZoom","growX","growY","none","scrollUp","scrollDown","scrollLeft","scrollRight","scrollHorz","scrollVert","shuffle","slideX","slideY","toss","turnUp","turnDown","turnLeft","turnRight","uncover","wipe","zoom");	
		}

		if( $type == 'datasource' )
		{
			$datasource = array(
				'Select Source' => "", 
				'Attached Media' => "data-1", 
				'Post Categories' => "data-2", 
				'Slide Sets' => "data-4", 
				'Porfolio Categories' => "data-6", 
			);	
			
			if( of_get_option('flickr_userid') !='' )
			{
				$datasource['Flickr Set'] = 'data-3';
			}
			
			if( class_exists('Woocommerce') || class_exists('WPSC_Query') )
			{
				$datasource['Product Categories'] = 'data-5';
			}
			
			return $datasource;
		}
		
		if( $type == 'imageeffect' )
		{
			return $image_effect = array(
				'None' => "", 
				'Shadow' => "shadow",
				'Reflection' => "reflection", 
				'Shadow + Reflection' => "shadowreflection", 
				'Frame' => "frame",
				'Black & White' => 'blackwhite',
				'Frame + Black & White' => 'frameblackwhite',
				'Shadow + Black & White' => 'shadowblackwhite',
			);	
		}

		if( $type == 'colors' )
		{
			return $colors = array(
				'grey-lite ' => "grey-lite", 
				'black' => "black", 
				'blue' => "blue", 
				'blue-lite' => "blue-lite", 
				'green-lite' => "green-lite", 
				'green' => "green", 
				'grey' => "grey",
				'orange-lite' => "orange-lite", 
				'orange' => "orange", 
				'blue' => "blue", 
				'pink-lite' => "pink-lite", 
				'pink' => "pink",
				'purple-lite' => "purple-lite", 
				'purple' => "purple", 		
				'red-lite' => "red-lite", 
				'red' => "pink", 		
				'teal-lite' => "teal-lite", 
				'teal' => "teal", 		
				'transparent' => "transparent", 
				'white' => "white",
				'yellow-lite' => "yellow-lite", 
				'yellow' => "yellow", 																																											

			);	
		}		
	}
	

/* ------------------------------------
:: GALLERY OPTIONS FUNCTION
------------------------------------*/	
	
	function get_common_options( $type, $object ='' )
	{
		
		if( $type == 'datasource' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Data Source", "js_composer"),
				"param_name" => "data_source",
				"value" => get_options_array( 'datasource' )
			);
		}
		elseif( $type == 'data-1' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Attached Media ID", "js_composer"),
				"param_name" => "attached_id",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-1'), 'callback' => ''),
				"description" => __("Comma separate multiple ID's.", "js_composer")
			);
		}	
		elseif( $type == 'data-2' )
		{
			$option = array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Categories", "js_composer"),
				"param_name" => "categories",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"options" => get_data_source( 'data-2', 'shortcode' )
			);
		}		
		elseif( $type == 'data-2-formats' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Filter by Post", "js_composer"),
				"param_name" => "post_format",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"value" => get_data_source( 'data-2-formats', 'shortcode' )
			);
		}
		elseif( $type == 'orderby' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Order Post By", "js_composer"),
				"param_name" => "orderby",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2','data-1'), 'callback' => ''),
				"value" => array(
					'Ascending' => '',
					'Descending' => 'DESC'
				)
			);
		}
		elseif( $type == 'sortby' || $type == 'order' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Sort Post By", "js_composer"),
				"param_name" => $type,
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2','data-1'), 'callback' => ''),
				"value" => array(
					'Post Order' => '',
					'Date' => 'date',
					'Random' => 'rand',
					'Title' => 'title',
				)
			);
		}
		elseif( $type == 'excerpt' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Excerpt", "js_composer"),
				"param_name" => "excerpt",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Default is 55 words.", "js_composer")
			);
		}					
		elseif( $type == 'data-3' && of_get_option('flickr_userid') !='' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Flickr Set", "js_composer"),
				"param_name" => "flickr_set",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-3'), 'callback' => ''),
				"value" => get_data_source( 'data-3', 'shortcode' )
			);
		}
		elseif( $type == 'data-4' )
		{
			$option = array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Slide Sets", "js_composer"),
				"param_name" => "slidesetid",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-4'), 'callback' => ''),
				"options" => get_data_source( 'data-4', 'shortcode' )
			);
		}	
		elseif( $type == 'data-5' )
		{
			$option = array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Product Categories", "js_composer"),
				"param_name" => "product_categories",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-5'), 'callback' => ''),
				"options" => get_data_source( 'data-5', 'shortcode' )
			);
		}
		elseif( $type == 'data-5-tags' )
		{
			$option = array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Product Tags", "js_composer"),
				"param_name" => "product_tags",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-5'), 'callback' => ''),
				"options" => get_data_source( 'data-5-tags', 'shortcode' )
			);
		}
		elseif( $type == 'data-6' )
		{
			$option = array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Product Tags", "js_composer"),
				"param_name" => "portfolio_categories",
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-6'), 'callback' => ''),
				"options" => get_data_source( 'data-6', 'shortcode' )
			);
		}
		elseif( $type == 'width' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Gallery Width", "js_composer"),
				"param_name" => "width",
				"value" => __("", "js_composer"),
			);
		}
		elseif( $type == 'height' )
		{
			if( $object == 'grid' )
			{
				$text = "Row Height";	
			}
			else
			{
				$text = "Gallery Height";	
			}
			
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __( $text, "js_composer"),
				"param_name" => "height",
				"value" => __("", "js_composer"),
			);
		}
		elseif( $type == 'columns' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Columns", "js_composer"),
				"param_name" => "columns",
				"value" => array(
					'3 Columns' => '',
					'1 Columns' => '1',
					'2 Columns' => '2',
					'4 Columns' => '4',
					'5 Columns' => '5',
					'6 Columns' => '6',
					'7 Columns' => '7',
					'8 Columns' => '8',
					'9 Columns' => '9',
					'10 Columns' => '10',
					'11 Columns' => '11',
					'12 Columns' => '12',
				)
			);
		}
		elseif( $type == 'content' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Content", "js_composer"),
				"param_name" => "content_type",
				"value" => array(
					'Text + Image' => 'textimage',
					'Title + Image' => 'titleimage',
					'Image' => 'image',
					'Text' => 'text',
				)
			);
		}		
		elseif( $type == 'align' )
		{

			if( $object !='' )
			{
				$text = $object;	
			}
			
			
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( $text.' Align', "js_composer"),
				"param_name" => "align",
				"value" => array(
					'Normal' => '',
					'Left' => 'alignleft',
					'Center' => 'aligncenter',
					'Right' => 'alignright'
				)
			);
		}			
		elseif( $type == 'imgwidth' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Image Width", "js_composer"),
				"param_name" => "imgwidth",
				"value" => __("", "js_composer"),
			);
		}	
		elseif( $type == 'imgheight' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Image Height", "js_composer"),
				"param_name" => "imgheight",
				"value" => __("", "js_composer"),
			);
		}
		elseif( $type == 'imageeffect' )
		{
			$option = array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Image Effect", "js_composer"),
				"param_name" => "imageeffect",
				"value" => get_options_array( 'imageeffect' )
			);
		}
		elseif( $type == 'timeout' )
		{
			$option = array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Timeout", "js_composer"),
				"param_name" => "timeout",
				"value" => __("", "js_composer"),
				"description" => __("Time between animating slides, in seconds.", "js_composer")
			);
		}												
		
		return $option;
	}



/* ------------------------------------
:: SHORTCODE MAPS	
------------------------------------*/

	/* ------------------------------------
	:: STAGE
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_image",
		"name"		=> __("Stage Gallery", "js_composer"),
		"class"		=> "nv_options stage",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-stagegallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("stage_", "js_composer"),
				"description" => __("Enter a unique ID stage_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Post Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),		
			get_common_options( 'width' ),
			get_common_options( 'height' ),
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Animation Type', "js_composer"),
				"param_name" => "animation",
				"value" => get_options_array( 'animation' )
			),	
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Animation Tween', "js_composer"),
				"param_name" => "tween",
				"value" => get_options_array( 'transition' )
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Navigation', "js_composer"),
				"param_name" => "navigation",
				"value" => array(
					'Bullet Navigation' => '',
					'Bullet + Directional Navigation' => 'enabled',
					'Disable Navigation' => 'disabled'
				)
			),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),								
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );

	/* ------------------------------------
	:: iSLIDER
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_islider",
		"name"		=> __("iSlider Gallery", "js_composer"),
		"class"		=> "nv_options islider",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-islidergallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("islider_", "js_composer"),
				"description" => __("Enter a unique ID islider_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Post Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),		
			get_common_options( 'width' ),
			get_common_options( 'height' ),
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),								
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );	

	/* ------------------------------------
	:: NIVO
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_nivo",
		"name"		=> __("Nivo Gallery", "js_composer"),
		"class"		=> "nv_options islider",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-nivogallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("nivo_", "js_composer"),
				"description" => __("Enter a unique ID nivo_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Post Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),		
			get_common_options( 'width' ),
			get_common_options( 'height' ),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Effect', "js_composer"),
				"param_name" => "animation",
				"value" => array("random","sliceDown","sliceDownLeft", "sliceUp", "sliceUpLeft", "sliceUpDown", "sliceUpDownLeft", "fold", "fade", "slideInRight", "slideInLeft", "boxRandom", "boxRain", "boxRainReverse", "boxRainGrow", "boxRainGrowReverse"
				)
			),			
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),								
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );

	/* ------------------------------------
	:: ACCORDION GALLERY MAP	
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_accordion",
		"name"		=> __("Accordion Gallery", "js_composer"),
		"class"		=> "nv_options accordion",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-accordiongallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("accordion_", "js_composer"),
				"description" => __("Enter a unique ID accordion_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Post Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),
			get_common_options( 'content' ),
			get_common_options( 'width' ),
			get_common_options( 'height' ),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Mini Titles', "js_composer"),
				"param_name" => "minititles",
				"value" => array(
					"Enable" => "enable",
					"Disable" => "disable",
				)
			),			
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),								
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );		

	/* ------------------------------------
	:: GRID GALLERY MAP
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_grid",
		"name"		=> __("Grid Gallery", "js_composer"),
		"class"		=> "nv_options grid",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-gridgallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("grid_", "js_composer"),
				"description" => __("Enter a unique ID grid_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Post Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),
			get_common_options( 'content' ),
			get_common_options( 'columns' ),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Animated Filtering", "js_composer"),
				"param_name" => "filtering",
				"options" => array(
					'Enable' => 'yes',
				)
			),			
			get_common_options( 'width' ),
			get_common_options( 'height', 'grid' ),
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),
			get_common_options( 'imgwidth' ),	
			get_common_options( 'imgheight' ),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),			
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );

	/* ------------------------------------
	:: GROUP GALLERY MAP
	------------------------------------*/

	wpb_map( array(
		"base"		=> "postgallery_slider",
		"name"		=> __("Group Gallery", "js_composer"),
		"class"		=> "nv_options group",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-groupgallery",
		"category"  => __('Gallery', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "",
				"heading" => __("Gallery ID", "js_composer"),
				"param_name" => "id",
				"value" => __("group_", "js_composer"),
				"description" => __("Enter a unique ID group_one.", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional title.", "js_composer")
			),						
			get_common_options( 'datasource' ),
			get_common_options( 'data-1' ),
			get_common_options( 'data-2' ),
			get_common_options( 'data-2-formats' ),
			get_common_options( 'orderby' ),
			get_common_options( 'sortby' ),
			get_common_options( 'excerpt' ),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Posts Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"dependency" => Array('element' => 'data_source' /*, 'not_empty' => true*/, 'value' => array('data-2'), 'callback' => ''),
				"description" => __("Set a posts limit.", "js_composer")
			),				
			get_common_options( 'data-3' ),	
			get_common_options( 'data-4' ),	
			get_common_options( 'data-5' ),	
			get_common_options( 'data-5-tags' ),
			get_common_options( 'data-6' ),
			get_common_options( 'content' ),
			get_common_options( 'timeout' ),
			get_common_options( 'columns' ),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Vertical Layout", "js_composer"),
				"param_name" => "vertical",
				"options" => array(
					'Enable' => 'yes',
				)
			),			
			get_common_options( 'width' ),
			get_common_options( 'height', 'grid' ),
			get_common_options( 'align', 'Gallery' ),
			get_common_options( 'imageeffect' ),
			get_common_options( 'imgwidth' ),	
			get_common_options( 'imgheight' ),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Image Align', "js_composer"),
				"param_name" => "image_align",
				"value" => array(
					'Center' => '',
					'Left' => 'left',
					'Right' => 'right'
				)
			),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" => array(
					'Enable' => 'yes',
				)
			),			
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("CSS Classes", "js_composer"),
				"param_name" => "class",
				"value" => __("", "js_composer"),
				"description" => __("Add an optional CSS classes.", "js_composer")
			),					
		)		
	) );	

	/* ------------------------------------
	:: CONTENT ANIMATOR MAP
	------------------------------------*/

	wpb_map( array(
		"base"		=> "content_animator",
		"name"		=> __("Content Animator", "js_composer"),
		"class"		=> "",
		"controls"	=> "edit_popup_delete",
		"icon"      => "icon-animator",
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Animator ID", "js_composer"),
				"param_name" => "id",
				"value" => __("animator_", "js_composer"),
				"description" => __("Enter a unique ID e.g. animator_text.", "js_composer")
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __("Content to Animate", "js_composer"),
				"param_name" => "content",
				"value" => __("", "js_composer"),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Delay Time", "js_composer"),
				"param_name" => "delay",
				"value" => __("", "js_composer"),
				"description" => __("Miliseconds e.g. 5000 = 5 seconds", "js_composer")
			),	
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Effect Speed", "js_composer"),
				"param_name" => "speed",
				"value" => __("", "js_composer"),
				"description" => __("Miliseconds e.g. 5000 = 5 seconds", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Effect", "js_composer"),
				"param_name" => "effect",
				"value" => array(
					'fade', 
					'slide',
				),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Animate Direction", "js_composer"),
				"param_name" => "direction",
				"value" => array(
					'left', 
					'right',
					'up',
					'down',
				),
			),	
			array(
				"type" => "dropdown",
				"heading" => __("Align", "js_composer"),
				"param_name" => "align",
				"value" => array(
					'left', 
					'center',
					'right',
				),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Float", "js_composer"),
				"param_name" => "float",
				"value" => array(
					'no', 
					'yes',
				),
			),																			
			array(
				"type" => "dropdown",
				"heading" => __("Easing", "js_composer"),
				"param_name" => "easing",
				"value" => get_options_array( 'transition' )
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Top Margin", "js_composer"),
				"param_name" => "margin_top",
				"description" => __("px", "js_composer")
			),	
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Left Margin", "js_composer"),
				"param_name" => "margin_left",
				"description" => __("px", "js_composer")
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Right Margin", "js_composer"),
				"param_name" => "margin_right",
				"description" => __("px", "js_composer")
			),										
		)
	) );

	/* ------------------------------------
	:: BUTTONS MAP
	------------------------------------*/
	
	wpb_map( array(
		"name"		=> __("Button", "js_composer"),
		"base"		=> "button",
		"class"		=> "",
		"icon"		=> "icon-button",
		"controls"	=> "edit_popup_delete",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"heading" => __("Button Text", "js_composer"),
				"param_name" => "title",
				"description" => __("Enter button text.", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Button Type", "js_composer"),
				"param_name" => "type",
				"value" => array(
					__('Button', "js_composer") => "linkbutton", 
					__('Drop Panel Trigger', "js_composer") => "droppanelbutton", 
					__('Custom', "js_composer") => "custom"
				),
				"description" => __("Select button type.", "js_composer")
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Text Color", "js_composer"),
				"param_name" => "text_color",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom'), 'callback' => ''),
				"description" => __("Button Color", "js_composer")
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Background Color", "js_composer"),
				"param_name" => "back_color",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom'), 'callback' => ''),
				"description" => __("Button Color", "js_composer")
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Border Color", "js_composer"),
				"param_name" => "border_color",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom'), 'callback' => ''),
				"description" => __("Button Border Color", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => get_options_array('colors'),
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('droppanelbutton','linkbutton'), 'callback' => ''),
				"description" => __("Select button type.", "js_composer")
			),						
			get_common_options( 'align', 'Button' ),				
			array(
				"type" => "textfield",
				"heading" => __("Link URL", "js_composer"),
				"param_name" => "url",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom','linkbutton'), 'callback' => ''),
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => __("Link Target", "js_composer"),
				"param_name" => "target",
				"value" => array(
					__("Same window", "js_composer") => "_self", 
					__("New window", "js_composer") => "_blank"
				),
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom','linkbutton'), 'callback' => ''),	
			),
			array(
				"type" => "dropdown",
				"heading" => __("Width", "js_composer"),
				"param_name" => "width",
				"value" => array(
					__("Normal", "js_composer") => "", 
					__("Quarter", "js_composer") => "quarter", 
					__("Half", "js_composer") => "half",
					__("Three Quarter", "js_composer") => "threequarter",
					__("Full", "js_composer") => "full"
				),
			),						
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			)
		),
		"js_callback" => array("init" => "wpbMessageInitCallBack")
	) );

	/* ------------------------------------
	:: ENQUIRY FORM MAP
	------------------------------------*/
	
	wpb_map( array(
		"name"		=> __("Enquiry Form", "js_composer"),
		"base"		=> "enquiry_form",
		"class"		=> "",
		"icon"		=> "icon-contact",
		"wrapper_class" => "",
		"controls"	=> "edit_popup_delete",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(		
			array(
				"type" => "textfield",
				"heading" => __("Email To Address", "js_composer"),
				"param_name" => "emailto",
				"value" => "",
				"description" => __("Enter the email address you wish emails to be sent to, default is admin email address.", "js_composer")
			),					
			array(
				"type" => "textarea",
				"class" => "messagebox_text",
				"heading" => __("Thankyou Text", "js_composer"),
				"param_name" => "thankyou",
				"value" => __("", "js_composer"),
			),
			array(
				"type" => "textfield",
				"heading" => __("ID", "js_composer"),
				"param_name" => "id",
				"value" => "",
				"description" => __("Add an ID if you require multiple Contact Forms e.g. contact_two.", "js_composer")
			)
		),
	) );

	/* ------------------------------------
	:: STYLED BOXES MAP
	------------------------------------*/
	
	wpb_map( array(
		"name"		=> __("Styled Boxes", "js_composer"),
		"base"		=> "styledbox",
		"class"		=> "wpb_vc_messagebox wpb_controls_top_right",
		"icon"		=> "icon-stylebox",
		"wrapper_class" => "",
		"controls"	=> "edit_popup_delete",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Message box type", "js_composer"),
				"param_name" => "type",
				"value" => array(__('General', "js_composer") => "general", __('General Shaded', "js_composer") => "general_shaded", __('Blank', "js_composer") => "blank", __('Warning', "js_composer") => "warning", __('Information', "js_composer") => "information", __('Download', "js_composer") => "download", __('Help', "js_composer") => "help", __('Custom', "js_composer") => "custom"),
				"description" => __("Select message type.", "js_composer")
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Custom Background Color", "js_composer"),
				"param_name" => "bg_color",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom'), 'callback' => ''),
				"description" => __("Add a Custom Background Color", "js_composer")
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Custom Border Color", "js_composer"),
				"param_name" => "border_color",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('custom'), 'callback' => ''),
				"description" => __("Add a Custom Border Color", "js_composer")
			),						
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "messagebox_text",
				"heading" => __("Message text", "js_composer"),
				"param_name" => "content",
				"value" => __("<p>I am message box. Click edit button to change this text.</p>", "js_composer"),
				"description" => __("Message text.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			)
		),
	) );

	/* ------------------------------------
	:: SOCIAL ICONS MAP
	------------------------------------*/	

	wpb_map( array(
		"name"		=> __("Social Icons", "js_composer"),
		"base"		=> "socialwrap",
		"controls"	=> "edit_popup_delete",
		"show_settings_on_create" => false,
		"class"		=> "vc_not_inner_content wpb_container_block",
		"icon"		=> "icon-wpb-tweetme",
		"category"  => __('Social', 'js_composer'),
		"wrapper_class" => "clearfix nv_options social_wrap",
		"params"	=> array(
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Share Icon", "js_composer"),
				"param_name" => "share_icon",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),	
			array(
				"type" => "dropdown",
				"heading" => __("Align", "js_composer"),
				"param_name" => "align",
				"value" => array(
					__('Left', "js_composer") => 'left',
					__('Center', "js_composer") => 'center', 
					__('Right', "js_composer") => 'right', 

				),
			),				
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			)
		),
		"custom_markup" => '
		<h4 class="wpb_element_title">'. __("Social Icons", "js_composer") .'</h4>
		<div class="wpb_socialwrap_holder wpb_holder clearfix">
			%content%
		</div>
		<div class="tab_controls">
			<button class="add_tab" title="'.__("Add accordion section", "js_composer").'">'. __("Add Social Icon", "js_composer") .'</button>
		</div>
		',
		'default_content' => '[socialicon name="Edit me" /]',
		"js_callback" => array("init" => "wpbSocialWrapInitCallBack",)
	) );
	
	
	// Get Social Icons
	$get_social_icons = social_icon_data();
	
	foreach( $get_social_icons as $social_icon => $value )
	{
		$social_icons[ $value['name'] ] = str_replace('sociallink_','',$social_icon);
	}
	
	wpb_map( array(
		"name"		=> __("Social Icon", "js_composer"),
		"base"		=> "socialicon",
		"class"		=> "",
		"icon"      => "",
		"wrapper_class" => "",
		"controls"	=> "full",
		"content_element" => false,
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Social Icon", "js_composer"),
				"holder" => "h3",
				"param_name" => "name",
				"value" => $social_icons,
				"description" => __("Select color of the toggle icon.", "js_composer")
			),				
			array(
				"type" => "textfield",
				"heading" => __("Link URL", "js_composer"),
				"param_name" => "url",
				"value" => "",
				"description" => __("Optional Link URL", "js_composer")
			),	
		)
	) );
	

	class WPBakeryShortCode_Socialwrap extends WPBakeryShortCode {
	
		public function __construct($settings) {
			parent::__construct($settings);
		}
	
		protected function content( $atts, $content = null ) {

			$title = $columns = $width = $el_position = $el_class = '';
			//
			extract(shortcode_atts(array(
				'align' => '',
				'share_icon' => '',
				'el_class' => '',
			), $atts));
			
			$output = '';
	
			$el_class = $this->getExtraClass($el_class);

			if( $share_icon == 'yes' )
			{
				$output .= "\n\t".'<div id="togglesocial" class="'. $align .' '. $el_class .'">';
				$output .= "\n\t\t".'<ul>';
				$output .= "\n\t\t\t".'<li class="socialinit nvcolor-wrap"><div class="socialinithide"></div><span class="nvcolor"></span></li>';
				$output .= "\n\t\t\t".'<li style="display: none;" class="socialhide nvcolor-wrap"><div class="socialinithide"></div><span class="nvcolor"></span></li>';
				$output .= "\n\t\t".'</ul>';
				$output .= "\n\t".'</div>';
				$output .= "\n\t".'<div class="socialicons '.$align.' toggle">';
				$output .= "\n\t\t".'<ul>';
				$output .= "\n\t\t\t".remove_wpautop($content);
				$output .= "\n\t\t".'</ul>';
				$output .= "\n\t".'</div>';
				$output .= "\n\t".'<div class="clear"></div>';	
			}
			else
			{
				$output .= "\n\t".'<div class="socialicons display '.$align.' '. $el_class .'">';
				$output .= "\n\t\t".'<ul>';
				$output .= "\n\t\t\t".remove_wpautop($content);
				$output .= "\n\t\t".'</ul>';
				$output .= "\n\t".'</div>';
				$output .= "\n\t".'<div class="clear"></div>';				
			}

			$output = $this->startRow($el_position) . $output . $this->endRow($el_position);
			return $output;
		}
	
		public function contentAdmin( $atts, $content ) {
			$width = $custom_markup = '';
			$shortcode_attributes = array('width' => '1/1');
			foreach ( $this->settings['params'] as $param ) {
				if ( $param['param_name'] != 'content' ) {
					if ( is_string($param['value']) ) {
						$shortcode_attributes[$param['param_name']] = __($param['value'], "js_composer");
					} else {
						$shortcode_attributes[$param['param_name']] = $param['value'];
					}
				} else if ( $param['param_name'] == 'content' && $content == NULL ) {
					$content = __($param['value'], "js_composer");
				}
			}
			extract(shortcode_atts(
				$shortcode_attributes
				, $atts));
	
			$output = '';
	
			$elem = $this->getElementHolder($width);
	
			$iner = '';
			foreach ($this->settings['params'] as $param) {
				$param_value = '';
				$param_value = $$param['param_name'];
				if ( is_array($param_value)) {
					// Get first element from the array
					reset($param_value);
					$first_key = key($param_value);
					$param_value = $param_value[$first_key];
				}
				$iner .= $this->singleParamHtmlHolder($param, $param_value);
			}
	
			$tmp = '';
			$template = '<div class="wpb_template">'.do_shortcode('[socialicon name="Edit me" /]').'</div>';
	
			if ( isset($this->settings["custom_markup"]) && $this->settings["custom_markup"] != '' ) {
				if ( $content != '' ) {
					$custom_markup = str_ireplace("%content%", $tmp.$content.$template, $this->settings["custom_markup"]);
				} else if ( $content == '' && isset($this->settings["default_content"]) && $this->settings["default_content"] != '' ) {
					$custom_markup = str_ireplace("%content%", $this->settings["default_content"].$template, $this->settings["custom_markup"]);
				}
				$iner .= do_shortcode($custom_markup);
			}
			$elem = str_ireplace('%wpb_element_content%', $iner, $elem);
			$output = $elem;
	
			return $output;
		}
	}	

	/* ------------------------------------
	:: jQUERY ACCORDION
	------------------------------------*/	

	wpb_map( array(
		"name"		=> __("Accordion section", "js_composer"),
		"base"		=> "vc_accordion",
		"controls"	=> "full",
		"show_settings_on_create" => false,
		"class"		=> "wpb_accordion vc_not_inner_content wpb_container_block",
		"icon"		=> "icon-wpb-ui-accordion",
		"category"  => __('Content', 'js_composer'),
	//	"wrapper_class" => "clearfix",
		"params"	=> array(
			array(
				"type" => "textfield",
				"heading" => __("Widget title", "js_composer"),
				"param_name" => "title",
				"value" => "",
				"description" => __("What text use as widget title. Leave blank if no title is needed.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("<strong>collapsible</strong> = collapsible accordion <br />
 <strong>collapse</strong> = set initial state as collapsed.", "js_composer")
			)
		),
		"custom_markup" => '
	
		<div class="wpb_accordion_holder wpb_holder clearfix">
			%content%
		</div>
		<div class="tab_controls">
			<button class="add_tab" title="'.__("Add accordion section", "js_composer").'">'.__("Add accordion section", "js_composer").'</button>
		</div>
		',
		'default_content' => '
		 [vc_accordion_tab title="Section 1"][/vc_accordion_tab]
		 [vc_accordion_tab title="Section 2"][/vc_accordion_tab]
		',
		'default_content_old' => '
		<div class="group">
			<h3><a href="#">'.__('Section 1', 'js_composer').'</a></h3>
				<div>
					<div class="row-fluid wpb_column_container wpb_sortable_container not-column-inherit">
						[vc_column_text width="1/1"] '.__('I am text block. Click edit button to change this text.', 'js_composer').' [/vc_column_text]
					</div>
				</div>
		</div>
		<div class="group">
			<h3><a href="#">'.__('Section 2', 'js_composer').'</a></h3>
			<div>
				<div class="row-fluid wpb_column_container wpb_sortable_container not-column-inherit">
					[vc_column_text width="1/1"] '.__('I am text block. Click edit button to change this text.', 'js_composer').' [/vc_column_text]
				</div>
			</div>
		</div>',
		"js_callback" => array("init" => "wpbAccordionInitCallBack", /* "shortcode" => "wpbAccordionGenerateShortcodeCallBack" */)
	) );


	/* ------------------------------------
	:: PRICING TABLE MAP
	------------------------------------*/	

	wpb_map( array(
		"name"		=> __("Pricing Table", "js_composer"),
		"base"		=> "pricing_table",
		"controls"	=> "edit_popup_delete",
		"show_settings_on_create" => false,
		"class"		=> "vc_not_inner_content wpb_container_block",
		"icon"		=> "icon-pricing",
		"category"  => __('Content', 'js_composer'),
		"wrapper_class" => "clearfix nv_options pricing_table",
		"params"	=> array(
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			)
		),
		"custom_markup" => '
		<h4 class="wpb_element_title">'. __("Pricing Table", "js_composer") .'</h4>
		<div class="wpb_pricing_table_holder wpb_holder clearfix">
			%content%
		</div>
		<div class="tab_controls">
			<button class="add_tab" title="'.__("Add Plan", "js_composer").'">'. __("Add Plan", "js_composer") .'</button>
		</div>
		',
		'default_content' => '
		 [plan title="Plan" width="4"]<ul><li>List Item</li><li>List Item</li></ul>[/plan]
		 [plan title="Plan" width="4"]<ul><li>List Item</li><li>List Item</li></ul>[/plan]
		 [plan title="Featured Plan" width="4" featured="true" color="teal-lite"]<ul><li>List Item</li><li>List Item</li></ul>[/plan]
		 [plan title="Plan" width="4"]<ul><li>List Item</li><li>List Item</li></ul>[/plan]
		',
		"js_callback" => array("init" => "wpbPricingPlanInitCallBack",)
	) );
	

	class WPBakeryShortCode_Plan extends WPBakeryShortCode_Pricing_Plan {
		protected  $predefined_atts = array(
			'el_class' => '',
			'width' => '',
			'title' => '',
			'featured' => '',
			'button_text' => '',
			'button_link' => '',
			'price' => '',
			'target' => '',
			'per' => '',
			'color' => '',			
		);
	
		public function contentAdmin($atts, $content = null) {
			$width = $el_class = $title = $featured = $button_text = $button_link = $price = $per = $color = '';
			extract(shortcode_atts($this->predefined_atts, $atts));
			$output = '';
	
			$column_controls = $this->getColumnControls($this->settings('controls'));
			$column_controls_bottom =  $this->getColumnControls('add', 'bottom-controls');
	
			$width = array('');
	
			for ( $i=0; $i < count($width); $i++ ) {
				$output .= '<div class="group wpb_sortable">';
				$output .= '<div class="wpb_element_wrapper pricing_wrapper">';
				$output .= '<div class="row-fluid wpb_row_container not-row-inherit">';
				$output .= '<h3><a href="#">'.$title.'</a></h3>';
				$output .= '<div '.$this->mainHtmlBlockParams($width, $i).'>';
				$output .= '<input type="hidden" class="wpb_vc_sc_base" name="" value="'.$this->settings['base'].'" />';
				$output .= str_replace("%column_size%", wpb_translateColumnWidthToFractional($width[$i]), $column_controls);
				$output .= '<div class="wpb_element_wrapper">';
				$output .= '<div class="row-fluid   wpb_'.$this->settings['base'].'_container ">';
				$output .= WPBakeryVisualComposer::getInstance()->getLayout()->getContainerHelper();
				$output .= '</div>';
				if ( isset($this->settings['params']) ) {
					$inner = '';
					foreach ($this->settings['params'] as $param) {
						$param_value = $$param['param_name'];
						if ( is_array($param_value)) {
							// Get first element from the array
							reset($param_value);
							$first_key = key($param_value);
							$param_value = $param_value[$first_key];
						}
						$inner .= $this->singleParamHtmlHolder($param, $param_value);
					}
					$output .= $inner;
				}
				$output .= '<input type="hidden" class="wpb_vc_param_value width textfield " name="width" value="">';
				$output .= '</div>';
				$output .= str_replace("%column_size%", wpb_translateColumnWidthToFractional($width[$i]), $column_controls_bottom);
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
			}
			return $output;
		}
	
		protected function outputTitle($title) {
			return  '';
		}
	
		public function customAdminBlockParams() {
			return '';
		}
	
		public function mainHtmlBlockParams($width, $i) {
			return 'data-element_type="'.$this->settings["base"].'" class="wpb_'.$this->settings['base'].' wpb_sortable wpb_container_block wpb_content_holder"'.$this->customAdminBlockParams();
		}
	}
	
	wpb_map( array(
		"name"		=> __("Pricing Plan", "js_composer"),
		"base"		=> "plan",
		"class"		=> "",
		"icon"      => "",
		"wrapper_class" => "",
		"controls"	=> "full",
		"content_element" => false,
		"params"	=> array(
			array(
				"type" => "textfield",
				"heading" => __("Title", "js_composer"),
				"param_name" => "title",
				"value" => "",
				"description" => __("Plan Title", "js_composer")
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"heading" => __("Content", "js_composer"),
				"param_name" => "content",
				"value" => "<ul><li>List Item</li><li>List Item</li></ul>",
			),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Featured Plan", "js_composer"),
				"param_name" => "featured",
				"options" =>  array(
					__('Enable', "js_composer") => "true", 
				)
			),			
			array(
				"type" => "textfield",
				"heading" => __("Button Text", "js_composer"),
				"param_name" => "button_text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Link", "js_composer"),
				"param_name" => "button_link",
				"value" => "",
				"description" => __("Enter 'droppaneltrigger' to trigger Drop Panel ( remove quotes )", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Link Target", "js_composer"),
				"param_name" => "target",
				"value" => array(
					__("Same window", "js_composer") => "_self", 
					__("New window", "js_composer") => "_blank"
				),
			),			
			array(
				"type" => "textfield",
				"heading" => __("Price", "js_composer"),
				"param_name" => "price",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"heading" => __("Per", "js_composer"),
				"param_name" => "per",
				"value" => "",
				"description" => __("E.g. / Month", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => get_options_array('colors'),
				"description" => __("Select color of the toggle icon.", "js_composer")
			),			
		)
	) );
	

	class WPBakeryShortCode_Pricing_Table extends WPBakeryShortCode {
	
		public function __construct($settings) {
			parent::__construct($settings);
		}
	
		protected function content( $atts, $content = null ) {

			$title = $columns = $width = $el_position = $el_class = '';
			//
			extract(shortcode_atts(array(
				'columns' => '',
				'el_position' => '',
				'el_class' => ''
			), $atts));
			
			$output = $column_class ='';
	
			$el_class = $this->getExtraClass($el_class);
			
			if( $columns != '' )
			{
				$columns = numberToWords ( $columns );
				$column_class = $columns .'-column';
			}
	
			$output .= "\n\t".'<div class="nv-pricing-table '. $column_class .' '. $el_class .'">'; 
			$output .= "\n\t\t".remove_wpautop($content);
			$output .= "\n\t".'</div> '.$this->endBlockComment('.nv-pricing-table');
	
			//
			$output = $this->startRow($el_position) . $output . $this->endRow($el_position);
			return $output;
		}
	
		public function contentAdmin( $atts, $content ) {
			$width = $custom_markup = '';
			$shortcode_attributes = array('width' => '1/1');
			foreach ( $this->settings['params'] as $param ) {
				if ( $param['param_name'] != 'content' ) {
					if ( is_string($param['value']) ) {
						$shortcode_attributes[$param['param_name']] = __($param['value'], "js_composer");
					} else {
						$shortcode_attributes[$param['param_name']] = $param['value'];
					}
				} else if ( $param['param_name'] == 'content' && $content == NULL ) {
					$content = __($param['value'], "js_composer");
				}
			}
			extract(shortcode_atts(
				$shortcode_attributes
				, $atts));
	
			$output = '';
	
			$elem = $this->getElementHolder($width);
	
			$iner = '';
			foreach ($this->settings['params'] as $param) {
				$param_value = '';
				$param_value = $$param['param_name'];
				if ( is_array($param_value)) {
					// Get first element from the array
					reset($param_value);
					$first_key = key($param_value);
					$param_value = $param_value[$first_key];
				}
				$iner .= $this->singleParamHtmlHolder($param, $param_value);
			}
	
			$tmp = '';
			$template = '<div class="wpb_template">'.do_shortcode('[plan title="New Plan" width="4"]<ul><li>List Item</li><li>List Item</li></ul>[/plan]').'</div>';
	
			if ( isset($this->settings["custom_markup"]) && $this->settings["custom_markup"] != '' ) {
				if ( $content != '' ) {
					$custom_markup = str_ireplace("%content%", $tmp.$content.$template, $this->settings["custom_markup"]);
				} else if ( $content == '' && isset($this->settings["default_content"]) && $this->settings["default_content"] != '' ) {
					$custom_markup = str_ireplace("%content%", $this->settings["default_content"].$template, $this->settings["custom_markup"]);
				}
				$iner .= do_shortcode($custom_markup);
			}
			$elem = str_ireplace('%wpb_element_content%', $iner, $elem);
			$output = $elem;
	
			return $output;
		}
	}	
	

	/* ------------------------------------
	:: REVEAL MAP
	------------------------------------*/
	
	wpb_map( array(
		"name"		=> __("Reveal (Toggle)", "js_composer"),
		"base"		=> "reveal",
		"controls"	=> "edit_popup_delete",
		"class"		=> "wpb_vc_faq wpb_controls_top_right  wpb_vc_column_inner",
		"icon"		=> "icon-toggle",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "textfield",
				"holder" => "h4",
				"class" => "toggle_title",
				"heading" => __("Toggle title", "js_composer"),
				"param_name" => "title",
				"value" => __("Toggle title", "js_composer"),
				"description" => __("Toggle block title.", "js_composer")
			),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "toggle_content",
				"heading" => __("Toggle content", "js_composer"),
				"param_name" => "content",
				"value" => __("<p>Toggle content goes here, click edit button.</p>", "js_composer"),
				"description" => __("Toggle block content.", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Default state", "js_composer"),
				"param_name" => "open",
				"value" => array(__("Closed", "js_composer") => "false", __("Open", "js_composer") => "true"),
				"description" => __("Select this if you want toggle to be open by default.", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => get_options_array('colors'),
				"description" => __("Select color of the toggle icon.", "js_composer")
			),			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "js_composer"),
				"param_name" => "el_class",
				"value" => "",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
			)
		)	
	) );

	/* ------------------------------------
	:: RECENT POSTS  MAP
	------------------------------------*/
	
	wpb_map( array(
		"name"		=> __("Recent Posts", "js_composer"),
		"base"		=> "recent_posts",
		"controls"	=> "edit_popup_delete",
		"class"		=> "",
		"icon"		=> "icon-list",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			get_common_options('content'),				
			array(
				"type" => "checkbox",
				"holder" => "div",
				"heading" => __("Post Categories", "js_composer"),
				"param_name" => "categories",
				"options" => get_data_source( 'data-2', 'shortcode' ),
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Display Date", "js_composer"),
				"param_name" => "show_date",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Display Metadata", "js_composer"),
				"param_name" => "metadata",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),								
			get_common_options( 'orderby' ),
			get_common_options( 'order' ),
			get_common_options( 'excerpt' ),	
			array(
				"type" => "textfield",
				"heading" => __("Offset", "js_composer"),
				"param_name" => "offset",
				"value" => __("", "js_composer"),
				"description" => __("Enter the  number of posts to offset by.", "js_composer")
			),						
			array(
				"type" => "textfield",
				"heading" => __("Limit", "js_composer"),
				"param_name" => "limit",
				"value" => __("", "js_composer"),
				"description" => __("Limit the number of posts.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Width", "js_composer"),
				"param_name" => "image_width",
				"value" => __("", "js_composer"),
				"description" => __("px", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Height", "js_composer"),
				"param_name" => "image_height",
				"value" => __("", "js_composer"),
				"description" => __("px", "js_composer")
			),			
			array(
				"type" => "dropdown",
				"heading" => __("Image Effect", "js_composer"),
				"param_name" => "image_effect",
				"value" => get_options_array( 'imageeffect' ),
			),	
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __( 'Image Align', "js_composer"),
				"param_name" => "image_align",
				"value" => array(
					'Normal' => '',
					'Left' => 'alignleft',
					'Center' => 'aligncenter',
					'Right' => 'alignright'
				)
			),
		)
	) );	

	/* ------------------------------------
	:: IMAGE MAP	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Single Image", "js_composer"),
		"base"		=> "imageeffect",
		"controls"	=> "edit_popup_delete",
		"class"		=> "wpb_vc_single_image_widget",
		"icon"		=> "icon-image",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "attach_image",
				"heading" => __("Image", "js_composer"),
				"param_name" => "image",
				"value" => "",
				"description" => ""
			),
			array(
				"type" => "textfield",
				"holder" => "img",
				"heading" => __("Image URL", "js_composer"),
				"param_name" => "url",
				"value" => "",
				"description" => __("Enter URL if you wish to use an image not in the media library.", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Effect", "js_composer"),
				"param_name" => "type",
				"value" => get_options_array('imageeffect'),
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Width", "js_composer"),
				"param_name" => "width",
				"value" => "",
				"description" => __("Enter image width.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Height", "js_composer"),
				"param_name" => "height",
				"value" => "",
				"description" => __("Enter image height.", "js_composer")
			),
			get_common_options( 'align', 'Image' ),
			array(
				"type" => "textfield",
				"heading" => __("Alt", "js_composer"),
				"param_name" => "alt",
				"value" => "",
				"description" => __("Alternate text", "js_composer")
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Enable Title Overlay", "js_composer"),
				"param_name" => "titleoverlay",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),			
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Enable Lightbox", "js_composer"),
				"param_name" => "lightbox",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),						
			array(
				"type" => "textfield",
				"heading" => __("Media URL", "js_composer"),
				"param_name" => "videourl",
				"value" => "",
				"dependency" => Array('element' => 'lightbox' /*, 'not_empty' => true*/, 'value' => array('yes'), 'callback' => ''),
				"description" => __("Enter url if you want to use lightbox to display a Video or other media.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Image link", "js_composer"),
				"param_name" => "link",
				"value" => "",
				"description" => __("Enter url if you want to link this image with any url. Leave empty if you won't use it", "js_composer")
			),
			array(
				"type" => "dropdown",
				"heading" => __("Link Target", "js_composer"),
				"param_name" => "target",
				"value" => array(
					__("Same window", "js_composer") => "_self", 
					__("New window", "js_composer") => "_blank"
				),
				"dependency" => Array('element' => "img_link", 'not_empty' => true)
			)
		)
	));

	/* ------------------------------------
	:: VIDEO MAP	  	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Video Player", "js_composer"),
		"base"		=> "videoembed",
		"controls"	=> "edit_popup_delete",
		"class"		=> "",
		"icon"		=> "icon-video",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Effect", "js_composer"),
				"param_name" => "type",
				"value" => array(
					__('YouTube', "js_composer") => 'youtube', 
					__('Vimeo', "js_composer") => 'vimeo',
					__('Flash', "js_composer") => 'flash', 
					__('JW Player', "js_composer") => 'jwplayer', 
				),
			),	
			array(
				"type" => "dropdown",
				"heading" => __("Media Ratio", "js_composer"),
				"param_name" => "ratio",
				"value" => array(
					__("16:9", "js_composer") => "sixteen_by_nine", 
					__("4:3", "js_composer") => "four_by_three"
				),
			),				
			array(
				"type" => "textfield",
				"heading" => __("Media URL", "js_composer"),
				"param_name" => "url",
				"value" => "",
				"description" => __("Enter URL of media.", "js_composer")
			),		
			array(
				"type" => "textfield",
				"heading" => __("Image URL", "js_composer"),
				"param_name" => "imageurl",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('jwplayer'), 'callback' => ''),
				"description" => __("Enter URL if you wish to use a holding image ( Paused, Loading ).", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("ID", "js_composer"),
				"param_name" => "id",
				"value" => "video_0",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('jwplayer'), 'callback' => ''),
				"description" => __("Enter a unique ID.", "js_composer")
			),			
			array(
				"type" => "textfield",
				"heading" => __("Media Width", "js_composer"),
				"param_name" => "width",
				"value" => "",
				"description" => __("Enter image width.", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("Media Height", "js_composer"),
				"param_name" => "height",
				"value" => "",
				"description" => __("Enter media height.", "js_composer")
			),
			get_common_options( 'align', 'Media' ),		
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Loop", "js_composer"),
				"param_name" => "loop",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Autoplay", "js_composer"),
				"param_name" => "autoplay",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Shadow Effect", "js_composer"),
				"param_name" => "shadow",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),												
		)
	));	

	/* ------------------------------------
	:: AUDIO MAP	  	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Audio Player", "js_composer"),
		"base"		=> "audioembed",
		"controls"	=> "edit_popup_delete",
		"class"		=> "",
		"icon"		=> "icon-audio",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(			
			array(
				"type" => "textfield",
				"heading" => __("Media URL", "js_composer"),
				"param_name" => "url",
				"value" => "",
				"description" => __("Enter URL of media.", "js_composer")
			),		
			array(
				"type" => "textfield",
				"heading" => __("Cover Image URL", "js_composer"),
				"param_name" => "imageurl",
				"value" => "",
				"description" => __("Optional cover image ( Paused, Loading ).", "js_composer")
			),
			array(
				"type" => "textfield",
				"heading" => __("ID", "js_composer"),
				"param_name" => "id",
				"value" => "audio_0",
				"description" => __("Enter a unique ID.", "js_composer")
			),			
			array(
				"type" => "textfield",
				"heading" => __("Player Width", "js_composer"),
				"param_name" => "width",
				"value" => "",
				"description" => __("Enter image width.", "js_composer")
			),
			get_common_options( 'align', 'Media' ),		
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Loop", "js_composer"),
				"param_name" => "loop",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Autoplay", "js_composer"),
				"param_name" => "autoplay",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Shadow Effect", "js_composer"),
				"param_name" => "shadow",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),												
		)
	));

	/* ------------------------------------
	:: DIVIDER MAP	  	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Divider", "js_composer"),
		"base"		=> "divider_line",
		"controls"	=> "edit_popup_delete",
		"class"		=> "",
		"icon"		=> "icon-divider",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Type", "js_composer"),
				"holder" => 'div',
				"param_name" => "type",
				"value" => array(
					__('Line', "js_composer") => 'divider_line', 
					__('Line + Back to Top', "js_composer") => 'divider_linetop',
					__('Shadow ( Bottom )', "js_composer") => 'divider_shadow', 
					__('Shadow ( Top )', "js_composer") => 'divider_shadow_top', 
					__('Blank', "js_composer") => 'divider_blank',
					__('Clear', "js_composer") => 'clear', 
				),
			),				
			array(
				"type" => "textfield",
				"heading" => __("Opacity", "js_composer"),
				"param_name" => "opacity",
				"value" => "",
				"dependency" => Array('element' => 'type' /*, 'not_empty' => true*/, 'value' => array('divider_shadow','divider_shadow_top'), 'callback' => ''),
				"description" => __("% ( 0 - 100 ).", "js_composer")
			),										
		)
	));	

	/* ------------------------------------
	:: DROPCAPS MAP 	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Drop Caps", "js_composer"),
		"base"		=> "dropcap",
		"controls"	=> "edit_popup_delete",
		"class"		=> "dropcap",
		"icon"		=> "icon-dropcap",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Stle", "js_composer"),
				"param_name" => "style",
				"value" => array(
					__('Circle + Character', "js_composer") => 'two',
					__('Character', "js_composer") => 'one', 
				),
			),				
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => get_options_array('colors'),
				"description" => __("Select color of the toggle icon.", "js_composer")
			),
			array(
				"type" => "textfield",
				"holder" => 'h4',
				"heading" => __("Character", "js_composer"),
				"param_name" => "text",
				"value" => "",
			),								
		)
	));	

	/* ------------------------------------
	:: LISTS MAP 	
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("List", "js_composer"),
		"base"		=> "list",
		"class"		=> "wpb_controls_top_right",
		"icon"		=> "icon-list",
		"controls"	=> "full",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Style", "js_composer"),
				"param_name" => "style",
				"value" => array(
					__('Arrow', "js_composer") => 'arrow',
					__('Check', "js_composer") => 'check', 
					__('Bullet', "js_composer") => 'orb', 
					__('Cross', "js_composer") => 'cross', 
				),
			),				
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => get_options_array('colors'),
				"description" => __("Select color of the toggle icon.", "js_composer")
			),							
		   array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __("Text", "js_composer"),
				"param_name" => "content",
				"value" => "<ul><li>List Item</li><li>List Item</li><li>List Item</li></ul>",
			),		
		),
	));

	/* ------------------------------------
	:: TOOLTIPS MAP 
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Tooltip", "js_composer"),
		"base"		=> "tooltip",
		"class"		=> "",
		"icon"		=> "icon-tooltip",
		"controls"	=> "",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
		   array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Tooltip Trigger", "js_composer"),
				"param_name" => "content",
				"value" => "",
				"description" => __("Enter the content you want to act as a trigger.", "js_composer")
			),
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => __("Information Icon", "js_composer"),
				"param_name" => "icon",
				"options" =>  array(
					__('Enable', "js_composer") => "yes", 
				)
			),			
			array(
				"type" => "dropdown",
				"heading" => __("Position", "js_composer"),
				"param_name" => "position",
				"value" => array(
					__('Top Right', "js_composer") => 'top right',
					__('Top Left', "js_composer") => 'top left', 
					__('Top Center', "js_composer") => 'top center', 
					__('Bottom Right', "js_composer") => 'bottom right',
					__('Bottom Left', "js_composer") => 'bottom left', 
					__('Bottom Center', "js_composer") => 'bottom center', 					
				),
			),			
			array(
				"type" => "dropdown",
				"heading" => __("Color", "js_composer"),
				"param_name" => "color",
				"value" => array(
					__('Dark', "js_composer") => 'dark', 
					__('Light', "js_composer") => 'light', 						
				),
				"description" => __("Select color of the Tooltip.", "js_composer")
			),							
		   array(
				"type" => "textarea",
				"holder" => "div",
				"class" => "",
				"heading" => __("Tooltip Content", "js_composer"),
				"param_name" => "tip",
				"value" => "",
				"description" => __("Enter the content you wish to appear within the Tooltip", "js_composer")
			),		
		),
	));	

	/* ------------------------------------
	:: BLOCK QUOTE MAP 
	------------------------------------*/

	wpb_map( array(
		"name"		=> __("Quote", "js_composer"),
		"base"		=> "blockquote",
		"class"		=> "wpb_controls_top_right",
		"icon"		=> "icon-quote",
		"controls"	=> "full",
		"category"  => __('Content', 'js_composer'),
		"params"	=> array(
			array(
				"type" => "dropdown",
				"heading" => __("Type", "js_composer"),
				"param_name" => "type",
				"value" => array(
					__('Quotes', "js_composer") => 'blockquote_quotes',
					__('Line', "js_composer") => 'blockquote_line', 

				),
			),
			array(
				"type" => "dropdown",
				"heading" => __("Align", "js_composer"),
				"param_name" => "align",
				"value" => array(
					__('Left', "js_composer") => 'left',
					__('Center', "js_composer") => 'center', 
					__('Right', "js_composer") => 'right', 

				),
			),							
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __("Quote", "js_composer"),
				"param_name" => "content",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => __("Width", "js_composer"),
				"param_name" => "width",
				"value" => "",
				"description" => __("Add optional width setting.", "js_composer")
			),			
		),
	));						
	

/* ------------------------------------
:: CHECKBOX PARAM
------------------------------------*/

	function checkbox_settings_field( $settings, $value ='' )
	{
		$dependency = vc_generate_dependencies_attributes($settings);
		
		$param_line = '<input class="wpb_vc_param_value wpb-checkboxes" type="hidden" value="" name="" ' . $dependency . '/>';

		foreach ( $settings['options'] as $text_val => $val )
		{		
			$checked = "";
				
			if ( in_array($val, explode(",", $value)) ) $checked = ' checked="checked"';            
						
			$param_line .= ' <input id="'. $settings['param_name'] . '-' . $val .'" value="' . $val . '" class="'.$settings['param_name'].' '.$settings['type'].'" type="checkbox" name="'.$settings['param_name'].'"'. $checked .'' . $dependency . '> ' . $text_val;
		}
            
		return $param_line;
	}
	
	
	add_shortcode_param('checkbox', 'checkbox_settings_field');	