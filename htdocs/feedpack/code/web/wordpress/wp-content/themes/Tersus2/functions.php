<?php if (!is_admin()) if(!session_id()) session_start();

/* ------------------------------------
:: INITIATE JQUERY / STYLING
------------------------------------ */

	function init_nvscripts()
	{
		if ( !is_admin() )
		{
			wp_register_style('northvantage-style', get_bloginfo('stylesheet_url'),false,null);
			wp_enqueue_style('northvantage-style');
			
			if( of_get_option('enable_responsive') != 'disable' ) :
			
				wp_register_style('northvantage-responsive', get_template_directory_uri().'/stylesheets/responsive.css',false,null);
				wp_enqueue_style('northvantage-responsive');
			
			endif;
		
			wp_enqueue_script('jquery-ui-core',false,array('jquery'));
			wp_enqueue_script('jquery-effects-core',false,array('jquery'));
	
			wp_deregister_script('jquery-fancybox');	
			wp_register_script('jquery-fancybox', get_template_directory_uri().'/js/jquery.fancybox.min.js',false,null,true);
			wp_enqueue_script('jquery-fancybox');
	
			wp_deregister_script('jquery-reflection');	
			wp_register_script('jquery-reflection', get_template_directory_uri().'/js/jquery.reflection.js',false,null,true);
			wp_enqueue_script('jquery-reflection');
	
			$template_array = array(
				'template_url' => get_template_directory_uri(),
			);
	
			wp_deregister_script('nv-script');	
			wp_register_script('nv-script', get_template_directory_uri().'/js/nv-script.pack.js',false,array('jquery'));
			wp_localize_script('nv-script', 'NV_SCRIPT', $template_array );
			wp_enqueue_script('nv-script');

			// Remove Visual Composer Style
			wp_dequeue_style( 'bootstrap' );
			wp_deregister_style( 'js_composer_front' );
			wp_deregister_style( 'ui-custom-theme' );
			wp_dequeue_style( 'ui-custom-theme' );
		}
	}    


	add_action('wp_enqueue_scripts', 'init_nvscripts',101);

	// remove script version
	function _remove_script_version( $src )
	{ 
		$parts = explode( '?', $src );
		return $parts[0];
	}
	
	//add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
	//add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
	
	
	// add defer / async
	function strposa($haystack, $needles=array(), $offset=0)
	{
		$chr = array();
		foreach($needles as $needle)
		{
			$res = strpos($haystack, $needle, $offset);
			if ($res !== false) $chr[$needle] = $res;
		}
		if(empty($chr)) return false;
		return min($chr);
	}
	

	function add_defer_to_scripts( $url )
	{
		$scripts = array(
				'stage.slider.min',
				'group.slider.min',
				'jquery.fancybox.min',
				'content.animator.min',
				'jwplayer',
				'reflection',
				'jw-player.init.min',
				'jquery.tooltips',
				'contact.form',
				'jquery.themepunch.revolution.min.js',
				'jquery.themepunch.plugins.min.js'
			);
			
		if ( FALSE === strposa( $url, $scripts, 1) ) {
			return $url;
		} 
		
		return "$url' defer='defer' async='async";
	}
	
	//add_filter( 'clean_url', 'add_defer_to_scripts', 11, 1 );


/* ------------------------------------
:: DEFINE DIRECTORIES
------------------------------------ */

	define( 'NV_DIR', get_template_directory() );
	define( 'NV_FILES', NV_DIR . '/lib' );

/* ------------------------------------
:: THEME OPTIONS
------------------------------------ */

	require_once dirname( __FILE__ ) . '/lib/adm/functions/core.php';

	if ( !function_exists( 'optionsframework_init' ) )
	{
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/lib/adm/' );
		require_once dirname( __FILE__ ) . '/lib/adm/inc/options-framework.php';
	}

	require_once dirname( __FILE__ ) . '/lib/adm/inc/options-meta.php';

	require NV_FILES .'/adm/inc/note-admin.php';
	require NV_FILES .'/inc/sub-functions.php';
	require NV_DIR .'/custom-functions.php';
	
	require NV_FILES .'/adm/inc/advanced-excerpt/advanced-excerpt.php';
	require NV_FILES .'/adm/inc/custom-widgets.php';	

/* ------------------------------------
:: ARCHIVE EXCERPT
------------------------------------ */

	function new_excerpt_more($more)
	{
		global $post;
		return '... <a href="'. get_permalink($post->ID) . '">' . __( 'Read More', 'NorthVantage' )  . '</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');


/* ------------------------------------
:: MENU FILTER
------------------------------------ */

	function DYN_menupages()
	{
		add_filter('wp_list_pages', 'DYN_page_lists');
		$menupageslist = wp_list_pages('echo=0&title_li=&');
	
		remove_filter('wp_list_pages', 'DYN_page_lists'); // Remove filter to not affect all calls to wp_list_pages
		return $menupageslist;
	}

/* ------------------------------------
:: MENU DESCRIPTIONS
------------------------------------ */

	function DYN_page_lists($output)
	{	
		global $wpdb;
	
		$get_MenuDesc = mysql_query("SELECT p.ID, p.post_title, p.guid, p.post_parent, pm.meta_value FROM " . $wpdb->posts . " AS p LEFT JOIN (SELECT post_id, meta_value FROM " . $wpdb->postmeta . " AS ipm WHERE meta_key = 'pgopts') AS pm ON p.ID = pm.post_id WHERE p.post_type = 'page' AND p.post_status = 'publish' ORDER BY p.menu_order ASC");
		
		while ($row = mysql_fetch_assoc($get_MenuDesc))
		{
			extract($row);
			$post_title = wptexturize($post_title);
			$data = maybe_unserialize(get_post_meta( $ID, 'pgopts', true ));		
	
			$menudesc=$data["menudesc"];		
				
			if($menudesc!="")
			{
				$output = str_replace('>' . $post_title .'</a>' , '>' . $post_title . '</a><span class="menudesc">' . $data["menudesc"] . '</span>', $output);
			}
				
		}	
	
		$parts = preg_split('/(<ul|<li|<\/ul>)/',$output,null,PREG_SPLIT_DELIM_CAPTURE);
		$newmenu = '';
		$level = 0;
		
		foreach ($parts as $part)
		{
			if ('<ul' == $part) { ++$level; }
			if ('</ul>' == $part) { --$level; }
			$newmenu .= $part;
		}
	
		return $newmenu;
	}


/* ------------------------------------
:: SIDEBARS
------------------------------------ */

	global $wpdb;

	$sidebars_num		= ( of_get_option('sidebars_num') !='' ) ? of_get_option('sidebars_num') : '2';
	$get_droppanel_num 	= ( of_get_option('droppanel_columns_num') !='' ) ? of_get_option('droppanel_columns_num') : '4'; // If not set, default to 4 columns
	$get_footer_num 	= ( of_get_option('footer_columns_num') !='' ) ? of_get_option('footer_columns_num') : '4'; // If not set, default to 4 columns
	
	// Sidebar Columns
	$i=1;
    while( $i <= $sidebars_num )
	{
		if ( function_exists('register_sidebar') )
		{
			register_sidebar(
			array(
			'name'=>'sidebar'.$i,
			'id'=>'sidebar'.$i,
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}
		$i++;
	}

	// Drop Panel Columns
	$i=1;
    while( $i <= $get_droppanel_num )
	{
		if ( function_exists('register_sidebar'))
		{
			register_sidebar(array(
			'name'=>'Drop Panel Column '.$i,
			'id'=>'droppanel'.$i,				
			'description' => 'Widgets in this area will be shown in Drop Panel column '.$i.'.',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}
		$i++;
	}
	
	
	// Footer Columns
	$i=1;
    while( $i <= $get_footer_num )
	{
		if ( function_exists('register_sidebar'))
		{
			register_sidebar(
			array(
			'name'=>'Footer Column '.$i,
			'id'=>'footer'.$i,
			'description' => 'Widgets in this area will be shown in Footer column '.$i.'.',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}
		$i++;
	}	


/* ------------------------------------
:: REGISTER POST TYPES
------------------------------------ */

	require NV_FILES .'/adm/inc/register-post-types.php';

/* ------------------------------------
:: BREADCRUMBS
------------------------------------ */

	require NV_FILES .'/inc/breadcrumbs.php';

/* ------------------------------------
:: PAGINATION
------------------------------------ */

	function pagination( $query, $baseURL ) {
		
		$page = $query->query_vars["paged"];
		if ( empty($page) ) $page = 1;
		$qs = $_SERVER["QUERY_STRING"] ? "?".$_SERVER["QUERY_STRING"] : "";
		// Only necessary if there's more posts than posts-per-page
		if ( $query->found_posts > $query->query_vars["posts_per_page"] ) {
			echo '<ul class="paging">';
			// Previous link?
			if ( $page > 1 ) {
				if(get_option("permalink_structure")) {
					echo '<li class="pagebutton previous"><a href="'.$baseURL.'page/'.($page-1).'/'.$qs.'">&laquo; previous</a></li>';
				} else {
					echo '<li class="pagebutton previous"><a href="'.$baseURL.'&amp;paged='.($page-1).'">&laquo; previous</a></li>';
				}			
				
			}
			// Loop through pages
			for ( $i=1; $i <= $query->max_num_pages; $i++ ) {
				// Current page or linked page?
				if ( $i == $page ) {
					echo '<li class="pagebutton active">'.$i.'</li>';
				} else {
				if(get_option("permalink_structure")) {
					echo '<li class="pagebutton"><a href="'.$baseURL.'page/'.$i.'/'.$qs.'">'.$i.'</a></li>';
				} else {
					echo '<li class="pagebutton"><a href="'.$baseURL.'&amp;paged='.$i.'">'.$i.'</a></li>';
				}
				}
			}
			// Next link?
			if ( $page < $query->max_num_pages ) {
				if(get_option("permalink_structure")) {
					echo '<li class="pagebutton next"><a href="'.$baseURL.'page/'.($page+1).'/'.$qs.'">&raquo;</a></li>';
				} else {
					echo '<li class="pagebutton next"><a href="'.$baseURL.'&amp;paged='.($page+1).'">&raquo;</a></li>';
				}				
			}
			echo '</ul>';
		}
	
	}


/* ------------------------------------
:: THUMBNAIL SUPPORT
------------------------------------ */

	add_theme_support( 'post-thumbnails' ); 

/* ------------------------------------
:: POST FORMATS SUPPORT
------------------------------------ */

	add_theme_support( 'post-formats', array( 'aside', 'link', 'status', 'quote', 'image' , 'video', 'audio' ));

/* ------------------------------------
:: AUTOMATIC FEED LINKS
------------------------------------ */

	add_theme_support( 'automatic-feed-links' );

/* ------------------------------------
:: DEFINE CONTENT WIDTH
------------------------------------ */

	if ( ! isset( $content_width ) ) $content_width = 980;

/* ------------------------------------
:: WP CUSTOM MENU SHORTCODE
------------------------------------ */

	function list_menu($atts, $content = null) {
		extract(shortcode_atts(array(  
			'menu'            => '', 
			'container'       => 'div', 
			'container_class' => '', 
			'container_id'    => '', 
			'menu_class'      => 'menu', 
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'walker'          => '',
			'theme_location'  => ''), 
			$atts));
	 
	 
		return wp_nav_menu( array( 
			'menu'            => $menu, 
			'container'       => $container, 
			'container_class' => $container_class, 
			'container_id'    => $container_id, 
			'menu_class'      => $menu_class, 
			'menu_id'         => $menu_id,
			'echo'            => false,
			'fallback_cb'     => $fallback_cb,
			'before'          => $before,
			'after'           => $after,
			'link_before'     => $link_before,
			'link_after'      => $link_after,
			'depth'           => $depth,
			'walker'          => $walker,
			'theme_location'  => $theme_location));
	}
	
	add_shortcode("listmenu", "list_menu"); //Create the shortcode



/* ------------------------------------
:: WP CUSTOM MENU SUPPORT
------------------------------------ */

	add_theme_support( 'nav-menus' );
		register_nav_menus( array(
			'mainnav' => __( 'Main Navigation', 'NorthVantage' ),
		) );
	

	class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu
	{
		var $to_depth = -1;
	
		function start_lvl(&$output, $depth)
		{
		  $output .= '</option>';
		}
	
	
		function end_lvl(&$output, $depth)
		{
		  $indent = str_repeat("\t", $depth); // don't output children closing tag
		}
	
	
		function start_el(&$output, $item, $depth, $args)
		{
			$indent = ( $depth ) ? str_repeat( "&nbsp;", $depth * 4 ) : '';
	
			$class_names = $value = '';
	
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
		
	
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';
	
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
	
			$value = ' value="'. $item->url .'"';
			$output .= '<option'.$id.$value.$class_names.'>';
	
	
			$item_output = $args->before;
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		
			$output .= $indent.$item_output;
	
		}
	
	
		function end_el(&$output, $item, $depth)
		{
			$output = str_replace('id="menu-', 'id="select-menu-', $output);
			if(substr($output, -9) != '</option>')
				$output .= "</option>"; // replace closing </li> with the option tag
		}
	}


	class dyn_walker extends Walker_Nav_Menu
	{
	
		function start_lvl(&$output, $depth) {
			$indent = str_repeat("\t", $depth);
			$output .= "\n$indent<ul class=\"sub-menu skinset-menu nv-skin\">\n";
		}
	
		
		function start_el(&$output, $item, $depth, $args) {
			global $wp_query;
	
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
			$class_names = $value = '';
	
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
	
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';
	
			if($depth=="0") {
			$output .= $indent . '<li class="menubreak"></li><li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
			} else {
			$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';		
			}
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	
			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= '<span class="menutitle">'.$args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after.'</span>';
			if( $item->attr_title ) {
			$item_output .= '<span class="menudesc">' . $item->attr_title  . '</span>';
			}
			$item_output .= '</a>';
			if( $item->description && of_get_option('wpcustommdesc_enable')=='enable' ) {
			$item_output .= '<div class="menudesc">' . do_shortcode($item->description) . '</div>';
			}		
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}


/* ------------------------------------
:: MULTI-SITE PATH
------------------------------------ */

	function dyn_getimagepath($img_src)
	{
		global $blog_id;
		if (isset($blog_id) && $blog_id > 0) {
			$imageParts = explode('/files/', $img_src);
			if (isset($imageParts[1])) {
				$img_src = '/wp-content/blogs.dir/' . $blog_id . '/files/' . $imageParts[1];
			}
		}
		return $img_src;
	}


/* ------------------------------------
:: BBPRESS
------------------------------------ */

	add_theme_support( 'bbpress' );
	
	if (class_exists('bbPress'))
	{
		wp_enqueue_style( 'bbp-nv-bbpress', get_stylesheet_directory_uri() . '/stylesheets/bbpress.css',false,null);
	}
	
	function filter_bbPress_breadcrumb_separator()
	{
	//$sep = ' » ';
	$sep = is_rtl() ? __( ' <span class="subbreak">/</span> ', 'bbpress' ) : __( ' <span class="subbreak">/</span> ', 'bbpress' );
	return $sep;
	}
	
	add_filter('bbp_breadcrumb_separator', 'filter_bbPress_breadcrumb_separator');


/* ------------------------------------
:: BUDDYPRESS
------------------------------------ */

	function bp_dtheme_enqueue_styles()
	{
		// Bump this when changes are made to bust cache
		$version = '20110804';
	 
		// Default CSS
		wp_enqueue_style( 'bp-default-main', get_template_directory_uri() . '/stylesheets/style-buddypress.css', array(), $version );
	 
		// Right to left CSS
		if ( is_rtl() )
			wp_enqueue_style( 'bp-default-main-rtl',  get_template_directory_uri() . '/_inc/css/default-rtl.css', array( 'bp-default-main' ), $version );
	}
	
	
	function bp_dtheme_enqueue_scripts()
	{
		// Do not enqueue JS if it's disabled
		if ( get_option( 'bp_tpack_disable_js' ) )
			return;
	
		// Add words that we need to use in JS to the end of the page so they can be translated and still used.
		$params = array(
			'my_favs'           => __( 'My Favorites', 'buddypress' ),
			'accepted'          => __( 'Accepted', 'buddypress' ),
			'rejected'          => __( 'Rejected', 'buddypress' ),
			'show_all_comments' => __( 'Show all comments for this thread', 'buddypress' ),
			'show_all'          => __( 'Show all', 'buddypress' ),
			'comments'          => __( 'comments', 'buddypress' ),
			'close'             => __( 'Close', 'buddypress' )
		);
	
		// BP 1.5+
		if ( version_compare( BP_VERSION, '1.3', '>' ) ) {
			// Bump this when changes are made to bust cache
			$version            = '20110818';
	
			$params['view']     = __( 'View', 'buddypress' );
		}
		// BP 1.2.x
		else {
			$version = '20110729';
	
			if ( bp_displayed_user_id() )
				$params['mention_explain'] = sprintf( __( "%s is a unique identifier for %s that you can type into any message on this site. %s will be sent a notification and a link to your message any time you use it.", 'buddypress' ), '@' . bp_get_displayed_user_username(), bp_get_user_firstname( bp_get_displayed_user_fullname() ), bp_get_user_firstname( bp_get_displayed_user_fullname() ) );
		}
	
		// Enqueue the global JS - Ajax will not work without it
		wp_enqueue_script( 'dtheme-ajax-js', BP_PLUGIN_URL . '/bp-themes/bp-default/_inc/global.js', array( 'jquery' ), $version );
	
		// Localize the JS strings
		wp_localize_script( 'dtheme-ajax-js', 'BP_DTheme', $params );
	}
	
	
	if ( function_exists('bp_is_blog_page') && !is_admin())
	{
		add_action( 'wp_print_styles', 'bp_dtheme_enqueue_styles' );
		add_action( 'wp_enqueue_scripts', 'bp_dtheme_enqueue_scripts' );
	}
	
	/* Constant paths.*/
	/* We define MY_THEME and MY_THEME_URL here for use in and calling functions-buddypress.php*/
		define( 'MY_THEME', get_stylesheet_directory() );
		define( 'MY_THEME_URL', get_stylesheet_directory_uri() );
	
	
	/* Load the BuddyPress functions for the theme*/
		require_once( MY_THEME . '/functions-buddypress.php' );

/* ------------------------------------
:: WP E-COMMERCE
------------------------------------ */

	function wpsc_product_rater_nv()
	{
		global $wpsc_query;
		$product_id = get_the_ID();
		$output = '';
		if ( get_option( 'product_ratings' ) == 1 )
		{
			$output .= "<div class='product_footer'>";
			$output .= "<div class='product_average_vote'>";
			$output .= "<strong>" . __( 'Avg. Customer Rating', 'wpsc' ) . ":</strong>";
			$output .= wpsc_product_existing_rating_nv( $product_id );
			$output .= "</div>";
			$output .= "<div class='product_user_vote'>";
			$output .= "<strong><span id='rating_" . $product_id . "_text'>" . __( 'Your Rating', 'wpsc' ) . ":</span>";
			$output .= "<span class='rating_saved' id='saved_" . $product_id . "_text'> " . __( 'Saved', 'wpsc' ) . "</span>";
			$output .= "</strong>";
			$output .= wpsc_product_new_rating( $product_id );
			$output .= "</div>";
			$output .= "</div>";
		}
		return $output;
	}
	
	function wpsc_product_existing_rating_nv( $product_id )
	{
		global $wpdb;
		$get_average = $wpdb->get_results( "SELECT AVG(`rated`) AS `average`, COUNT(*) AS `count` FROM `" . WPSC_TABLE_PRODUCT_RATING . "` WHERE `productid`='" . $product_id . "'", ARRAY_A );
		$average = floor( $get_average[0]['average'] );
		$count = $get_average[0]['count'];
		$output  = "  <span class='votetext'>";
		for ( $l = 1; $l <= $average; ++$l )
		{
			$output .= "<img class='goldstar' src='" .get_template_directory_uri() . "/images/gold-star.png' alt='$l' title='$l' />";
		}
		$remainder = 5 - $average;
		for ( $l = 1; $l <= $remainder; ++$l )
		{
			$output .= "<img class='goldstar' src='" .get_template_directory_uri() . "/images/grey-star.png' alt='$l' title='$l' />";
		}
	
		$output .= "</span> \r\n";
		return $output;
	}
	
	function remove_wpsc_style()
	{
		global $wp_styles;
		wp_deregister_style( 'wpsc-gold-cart-grid-view' );
		wp_deregister_style( 'wpsc-gold-cart' );
	}
	
	add_action('wp_print_styles', 'remove_wpsc_style', 100);

/* ------------------------------------
:: WOOCOMMERCE
------------------------------------ */

	add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
	
	function woocommerce_header_add_to_cart_fragment( $fragments ) {
		global $woocommerce;
		
		ob_start();	?>
	 
		<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'NorthVantage'); ?>">
			<span class="shop-cart-itemnum">
				<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'NorthVantage'), $woocommerce->cart->cart_contents_count);?> - 
			</span>
			 <?php echo $woocommerce->cart->get_cart_total(); ?>
			
		</a>
		<?php
	
		$fragments['a.cart-contents'] = ob_get_clean();
	
		return $fragments;
	}

/* ------------------------------------
:: TRANSLATION
------------------------------------ */

	load_theme_textdomain( 'NorthVantage', NV_DIR . '/languages' );

	$locale = get_locale();
	$locale_file = NV_DIR . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );


/* ------------------------------------
:: WP MINIFY FIX FOR GOOGLE JQUERY CDN
------------------------------------ */

	function add_minify_location()
	{
		if (class_exists('WPMinify'))
		{
			echo '<!-- WP-Minify JS -->';
			echo '<!-- WP-Minify CSS -->';
		}
	}
	add_action('wp_head','add_minify_location',99);


	/*	Remove WP auto formatting */
	function remove_wpautop( $content )
	{ 
		$content = do_shortcode( shortcode_unautop( $content ) ); 
	
		$new_content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
		$new_content = str_replace('<br />', "", $new_content);
		return $new_content;
	}


/* ------------------------------------
:: REVOLUTION SLIDER
------------------------------------ */

	require_once dirname( __FILE__ ) . '/lib/adm/inc/class-tgm-plugin-activation.php';
	
	add_action( 'tgmpa_register', 'themeva_register_required_plugins' );

	function themeva_register_required_plugins() {		
	 
		/**
		 * Array of plugin arrays. Required keys are name, slug and required.
		 * If the source is NOT from the .org repo, then source is also required.
		 */
		$plugins = array(
	 
			// This is an example of how to include a plugin pre-packaged with a theme
			array(
				'name'                  => 'Revolution Slider', // The plugin name
				'slug'                  => 'revslider', // The plugin slug (typically the folder name)
				'source'                => get_stylesheet_directory() . '/lib/adm/inc/revslider/revslider.zip', // The plugin source
				'required'              => true, // If false, the plugin is only 'recommended' instead of required
				'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation'    => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url'          => '', // If set, overrides default API URL and points to an external URL
			),
		);
	 
		// Change this to your theme text domain, used for internationalising strings
		$theme_text_domain = 'NorthVantage';
	 
		/**
		 * Array of configuration settings. Amend each line as needed.
		 * If you want the default strings to be available under your own theme domain,
		 * leave the strings uncommented.
		 * Some of the strings are added into a sprintf, so see the comments at the
		 * end of each line for what each argument will be.
		 */
		$config = array(
			'domain'            => $theme_text_domain,           // Text domain - likely want to be the same as your theme.
			'default_path'      => '',                           // Default absolute path to pre-packaged plugins
			'parent_menu_slug'  => 'themes.php',         // Default parent menu slug
			'parent_url_slug'   => 'themes.php',         // Default parent URL slug
			'menu'              => 'install-required-plugins',   // Menu slug
			'has_notices'       => true,                         // Show admin notices or not
			'is_automatic'      => true,            // Automatically activate plugins after installation or not
			'message'           => '',               // Message to output right before the plugins table
			'strings'           => array(
				'page_title'                                => __( 'Install Required Plugins', $theme_text_domain ),
				'menu_title'                                => __( 'Install Plugins', $theme_text_domain ),
				'installing'                                => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
				'oops'                                      => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
				'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
				'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
				'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
				'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                                    => __( 'Return to Required Plugins Installer', $theme_text_domain ),
				'plugin_activated'                          => __( 'Plugin activated successfully.', $theme_text_domain ),
				'complete'                                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ) // %1$s = dashboard link
			)
		);
	 
		tgmpa( $plugins, $config );
	 
	}	


/* ------------------------------------
:: VISUAL COMPOSER
------------------------------------ */

	$dir = dirname(__FILE__) . '/lib/adm/inc';
	
	$composer_settings = Array(
		'APP_ROOT' => $dir . '/js_composer',
		'WP_ROOT' => dirname( dirname( dirname( dirname($dir ) ) ) ). '/',
		'APP_DIR' => basename( dirname( dirname( $dir ) ) ) . '/adm/inc/js_composer/',
		'CONFIG' => $dir . '/js_composer/config/',
		'ASSETS_DIR' => 'assets/',
		'COMPOSER' => $dir . '/js_composer/composer/',
		'COMPOSER_LIB' => $dir . '/js_composer/composer/lib/',
		'SHORTCODES_LIB' => $dir . '/js_composer/composer/lib/shortcodes/',
		'default_post_types' => Array('page', 'post') /* Default post type where to activate visual composer meta box settings */
	); 
	
	require_once locate_template('/lib/adm/inc/js_composer/js_composer.php');
	
	$wpVC_setup->init($composer_settings);
	
	// Remove Default VC Features
	wpb_remove( "vc_button" ); 
	wpb_remove( "vc_cta_button" ); 
	wpb_remove( "vc_message" );
	wpb_remove( "vc_toggle" );
	wpb_remove( "vc_twitter" );
	wpb_remove( "vc_video" );
	wpb_remove( "vc_gallery" );
	wpb_remove( "vc_single_image" );
	wpb_remove( "vc_teaser_grid" );
	wpb_remove( "vc_posts_slider" );
	wpb_remove( "vc_separator" );
	wpb_remove( "vc_text_separator" );
	wpb_remove( "vc_flickr" );
	
	// Update Columns / Row CSS
	if( get_option('themeva_vc') != 1 )
	{
		$columns_css = array(
			"span1"  => 'columns one',
			"span2"  => 'columns two',
			"span3"  => 'columns three',
			"span4"  => 'columns four',
			"span5"  => 'columns five',
			"span6"  => 'columns six',
			"span7"  => 'columns seven',
			"span8"  => 'columns eight',
			"span9"  => 'columns nine',
			"span10" => 'columns ten',
			"span11" => 'columns eleven',
			"span12" => 'columns twelve',
		);
		
		update_option( 'wpb_js_row_css_class', 'row' );
		update_option( 'wpb_js_column_css_classes', $columns_css );
		update_option( 'themeva_vc', 1 );
	}
	
/* ------------------------------------
:: SHORTCODES
------------------------------------ */

	require NV_FILES .'/inc/shortcodes.php';

/* ------------------------------------
:: DISPLAY ADMIN NOTICE & ADMIN.CSS
------------------------------------ */

	// Admin CSS
	function themeva_admin_styles()
	{
		wp_enqueue_style('themeva-admin-styles', get_template_directory_uri().'/lib/adm/css/wp-admin.css' );
	}
	
	add_action('admin_enqueue_scripts', 'themeva_admin_styles');
 
 	// Admin Notice
	add_action('admin_notices', 'nv_admin_notice');
 
	function nv_admin_notice()
	{
		global $current_user ;
		$user_id = $current_user->ID;
		/* Check that the user hasn't already clicked to ignore the message */
		$theme_name = get_current_theme();
		
		if ( ! get_user_meta($user_id, 'nv_ignore_notice') )
		{
			echo '<div class="updated get_started"><p>';
			printf(__('Please read the <strong><a href="http://themeva.com/docs/'. strtolower($theme_name) .'/category/getting-started/" target="_blank">Getting Started</a></strong> section of the documentation for help using this <strong>Theme</strong>. <a href="%1$s">Hide Notice</a>'), '?nv_ignore_notice=0');
			echo "</p></div>";
		}
	}
 
	add_action('admin_init', 'nv_ignore_notice');
 
	function nv_ignore_notice()
	{
		global $current_user;
			$user_id = $current_user->ID;
			/* If user clicks to ignore the notice, add that to their user meta */
			if ( isset($_GET['nv_ignore_notice']) && '0' == $_GET['nv_ignore_notice'] ) {
				 add_user_meta($user_id, 'nv_ignore_notice', 'true', true);
		}
	}