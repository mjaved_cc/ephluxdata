<?php 

	wp_deregister_script('jquery-tooltips');	
	wp_register_script('jquery-tooltips',get_template_directory_uri().'/js/jquery.tooltips.js',false,array('jquery'));
	wp_enqueue_script('jquery-tooltips');
	
	if( $NV_textresize == "yes" )
	{
		echo "\n" . '<div class="textresize">';
		echo "\n\t" . '<ul>';
		echo "\n\t\t" . '<li class="resize-sml decreaseFont"></li>';
		echo "\n\t\t" . '<li class="resize-lrg increaseFont"></li>';
	 	echo "\n\t" . '</ul>';
		echo "\n" . '</div><!-- /textresizer -->' . "\n";
	}
	
	if( $NV_socialicons == "yes" || $NV_socialicons == "global" && get_post_meta( $post->ID, '_cmb_socialicons', true ) != 'disable' )
	{
		$color = '';
		
		if( !empty( $NV_socialicons_color ) )
		{
			$color = 'nv-'. $NV_socialicons_color;
		}
	
		if( $NV_disableheart == "yes" ) 
		{ ?>
			<div class="socialicons display <?php echo $color; ?>"><?php 
		} 
		else 
		{ ?>
			<div id="togglesocial">
				<ul>                     
					<li class="socialinit nvcolor-wrap"><div class="socialinithide"></div><span class="nvcolor"></span></li>
					<li  style="display: none;" class="socialhide nvcolor-wrap"><div class="socialinithide"></div><span class="nvcolor"></span></li>
				</ul>
			</div><!-- /togglesocial -->                            
			<div class="socialicons <?php echo $color; ?>"  style="display:none;">
		<?php 
		}
		
		// get social media button links
		require NV_FILES .'/adm/inc/social-media-urls.php';
		
			$output = '';
			$output .= "\n\t". '<ul>';
		
			// Get Social Icons
			$get_social_icons = social_icon_data();
		
			
			foreach( $get_social_icons as $social_icon => $value )
			{
				$icon_id = str_replace( 'sociallink_','',$social_icon );
				
				// Check global Social Options
				if( of_get_option( 'display_socialicons' ) == "yes" && get_post_meta( $post->ID, '_cmb_socialicons', true ) != "yes" )
				{
					$global_socialicons = of_get_option('socialicons');
					
					if( $global_socialicons[ strtolower( str_replace('.','',$value['name'] ) ) ] == '0' )
					{
						${'NV_social'. $icon_id } = 'yes';	
					}
				
				}
				
				if( ${'NV_social'. $icon_id } != 'yes' )
				{
					$sociallink = ( isset( ${'sociallink_'. strtolower( str_replace('.','',$value['name'] ) ) } ) ) ? getsociallink( ${'sociallink_'. strtolower( str_replace('.','',$value['name'] ) ) } ) : '';
					$output .= "\n\t\t". '<li class="nvcolor-wrap">';
					$output .= "\n\t\t\t". '<div class="tooltip-info top center" data-tooltip-position="top center">';
					$output .= "\n\t\t\t\t". '<a href="'. $sociallink .'" title="'. $value['name'] .'" target="_blank"><span class="nvcolor"></span><div class="social-icon social-'. strtolower( str_replace('.','',$value['name'] ) ) .'"></div></a>';
					$output .= "\n\t\t\t". '</div>';
					$output .= "\n\t\t\t". '<div class="tooltip dark">'. $value['name'] .'</div>';
					$output .= "\n\t\t". '</li>';
				}
			}
			
			$output .= "\n\t". '</ul>';
	
			echo $output; ?>
		
	
		</div><!-- /socialicons -->
	
	<?php 
	} ?>

	<div class="clear"></div>