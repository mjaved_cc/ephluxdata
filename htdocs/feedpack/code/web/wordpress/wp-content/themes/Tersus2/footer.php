
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <!-- jQueryUI from Google CDN, used only for the fancy demo controls, NOT REQUIRED for the counter itself -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
        <!-- Style sheet for the jQueryUI controls, NOT REQUIRED for the counter itself -->
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/vader/jquery-ui.css" />
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/flipcounter.js"></script>
        <!-- Style sheet for the counter, REQUIRED -->
		<h3 id="delivered">FeedPack's delivered so far</h3>
<div id="counter" class="flip-counter"></div>
<?php 

	echo "\n" . '</div><!-- / row -->';
	
/* ------------------------------------
:: HIDE CONTENT END
------------------------------------ */

	global $NV_hidecontent; 
	
	if( $NV_hidecontent != "yes" )
	{
		echo "\n" . '<div class="clear"></div>';
		echo "\n" . '</div><!-- /skinset-main-->';
		echo "\n" . '<div class="clear"></div>';
		echo "\n" . '</div><!-- /content-wrapper -->';
	}

/* ------------------------------------
:: PAGE VARIABLES
------------------------------------ */

	require NV_FILES ."/inc/page-constants.php"; // Page Constants

/* ------------------------------------
:: TWITTER
------------------------------------ */

	if( $NV_twitter == "pagebot" && get_post_meta( $post->ID, '_cmb_twitter', true ) != 'disable' )
	{ 
		global $NV_frame_footer;
		
        echo "\n" . '<div class="row">';
        echo "\n\t" . '<div class="twitter-wrap skinset-main nv-skin bottom '. $NV_frame_footer .'">';
       	require NV_FILES .'/inc/twitter.php'; // Call Twitter Template
        echo "\n\t" . '</div>';
        echo "\n" . '</div>';
	}

/* ------------------------------------
:: GROUP SLIDER
------------------------------------ */

	if( $NV_show_slider == "groupslider" && $NV_groupsliderpos == "below" )
	{
		require NV_FILES ."/inc/gallery-groupslider.php"; // Group Slider Gallery 
	}

/* ------------------------------------
:: GRID
------------------------------------ */
	
	if( $NV_show_slider == "gridgallery" && $NV_groupsliderpos == "below" )
	{
		if( $NV_gridfilter == 'yes' ) $NV_galleryclass = $NV_galleryclass . ' filter';
		echo '<div id="grid-main" data-grid-columns="'. $NV_gridcolumns .'" class="gallery-wrap grid-gallery row bottom '. $NV_galleryclass .' nv-skin">';
			require NV_FILES ."/inc/gallery-grid.php"; // Group Slider Gallery
		echo '</div>';
	}

/* ------------------------------------
:: EXIT TEXT
------------------------------------ */

	global $exittext,$exit_classes,$NV_frame_footer, $NV_disablefooter;
	
	if( !empty( $exittext ) )
	{ 
		if( empty( $exit_classes ) ) $exit_classes='skinset-main'; 
		
		echo "\n" . '<div class="row">';
		echo "\n\t" . '<div class="intro-text skinset-main '. $exit_classes .' '. $NV_frame_footer .' nv-skin">';
		echo "\n\t\t" . '<div>';
		echo do_shortcode( $exittext );
		echo "\n\t\t" . '</div>';
		echo "\n\t\t" . '<div class="clear"></div>';
		echo "\n\t" . '</div>';
		echo "\n" . '</div>';
	}
	
	if( $NV_disablefooter != 'yes' && of_get_option('mainfooter') != 'disable' ) : ?>
    
	<footer id="footer-wrap" class="row">
		<div id="footer" class="clearfix skinset-footer nv-skin <?php echo $NV_frame_footer; ?>">
        	<div class="row content">
				<?php
				
				$get_footer_num = ( of_get_option('footer_columns_num') !='' ) ? of_get_option('footer_columns_num') : '4'; // If not set, default to 4 columns
				
				$NV_footercolumns_text=numberToWords($get_footer_num ); // convert number to word
				
				$i=1;
				
				while( $i <= $get_footer_num )
				{ 
					if ( is_active_sidebar('footer'.$i) )
					{ ?>
                    <div class="block columns <?php echo $NV_footercolumns_text."_column "; if($i == $get_footer_num) { echo "last"; } ?>">
                    
                        <ul>
                            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Column '.$i)) : endif; ?>
                        </ul>
                    
                    </div>
                    <?php 
					}
					
					$i++;	
				} 
				
				// Check for enabled lower footer
				if( of_get_option('lowerfooter')!="disable" )
				{
					$lower_left  = ( of_get_option('lowfooterleft') !='' )  ? of_get_option('lowfooterleft')  : '&copy; '. date("Y") .' '. get_option("blogname");
					$lower_right = ( of_get_option('lowfooterright') !='' ) ? of_get_option('lowfooterright') : 'designed by <a href=\"http://www.apppli.com\" title=\"Buy Theme\" target=\"_blank\">Apppli</a>';
					 
				?>
                <div class="clear"></div>
                <div class="lowerfooter-wrap clearfix">
                    <div class="lowerfooter columns twelve">
                        <div class="lowfooterleft"><?php echo do_shortcode( $lower_left ); ?></div>
                        <div class="lowfooterright"><?php echo do_shortcode( $lower_right ); ?></div>
                    </div><!-- / lowerfooter -->		
                </div><!-- / lowerfooter-wrap -->
				<?php } ?>
			</div><!-- / row -->                
		</div><!-- / footer -->
	</footer><!-- / footer-wrap -->
    
	<?php endif; // disable footer ?>
    <div class="autototop"><a href="#"></a></div>
</div><!-- /wrapper -->

<!-- I would like to give a great thankyou to WordPress for their amazing platform -->
<!-- Theme Design by NorthVantage - http://www.northvantage.co.uk -->

<?php wp_footer();

	if( of_get_option('footer_javascript') !='' ) echo of_get_option('footer_javascript'); ?>



</div>

<?php 

$start = new DateTime('2013-05-01'); 
$today = new DateTime(); 
$diff = $start->diff($today); 

$days = $diff->format('%a'); 

$hour = $diff->format('%H')*60*60; 
$min = $diff->format('%I')*60*2;

$total =  ($days*24*60*60)+($hour+$min);


?>


<script>var myCounter = new flipCounter("counter", {value:<?php echo $total; ?>,inc: 1, pace: 2000}); </script>

   <script type="text/javascript">

        $(function(){
                
              
                var myCounter = new flipCounter('flip-counter', {value:10000, inc:2, pace:1000, auto:true});

                
                var smartInc = 0;           
              
               
        });
     
        </script>
		
		
</body>
</html>