<?php

/* ------------------------------------
:: CONFIGURE HEADER
------------------------------------*/ 

?>

<div class="wrapper"> 
	<header id="header-wrap" class="row <?php echo $NV_frame_header; ?>">

	<?php 
	// Check if drop panel is enabled or infobar is empty
	if( of_get_option('enable_droppanel' ) != 'disable' || !empty ( $NV_infobar ) )
	{ 
		$NV_isdroppanel = 'droppanel'; ?> 

		<div id="toppanel">			
		<?php
					
		if( of_get_option('enable_droppanel' ) !='disable' ) 
		{ ?>    
			<div id="panel" style=" <?php if(isset($hasError) || isset($captchaError)) { ?>min-height:300px <?php } ?>">
				<div class="content row">
				<?php
				
				// If not set, default to 4 columns
				$get_droppanel_num = ( of_get_option('droppanel_columns_num') !="" ) ? of_get_option('droppanel_columns_num' ) : '4'; 
				
				// convert number to word
				$NV_droppanelcolumns_text = numberToWords( $get_droppanel_num ); 
				$i=1;
				
				while( $i <= $get_droppanel_num )
				{ 
					if ( is_active_sidebar( 'droppanel'.$i ) ) 
					{ ?>
						<div class="block columns <?php echo $NV_droppanelcolumns_text."_column "; if($i == $get_droppanel_num) { echo "last"; } ?>">	
							<ul>
								<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Drop Panel Column '.$i)) : endif; ?>
							</ul>
						</div>
					<?php 
					} $i++;	
				} ?>
				</div><!-- / content -->
			</div> <!-- / panel -->
			
            <div class="tab-wrap <?php if( of_get_option('droppanel_button_align') !='' ) echo of_get_option('droppanel_button_align'); ?>">
				<div id="toggle" class="trigger">
					<a id="open" class="toggle open" href="#"></a>
					<a id="close" style="display: none;" class="toggle close" href="#"></a>
				</div><!-- /trigger -->
			</div> <!-- / tab-wrap -->
		<?php 				
		}
				
		if( !empty( $NV_infobar ) ) 
		{ ?>
			<div class="header-infobar <?php echo $NV_infobar_classes; ?>">
				<div class="infobar-content"><?php echo do_shortcode($NV_infobar); ?></div><span class="infobar-close"><a href="#"></a></span>
			</div>
		<?php 
		} ?>
            
		</div> <!--/toppanel -->
	<?php 
	} ?>
        
		<div id="header" class="skinset-header nv-skin <?php echo $NV_isdroppanel; ?>">
		<div id="header-bg" class="skinset-header nv-skin"></div>
		<?php if( of_get_option('enable_search') !=='disable' || function_exists('wpsc_cart_item_count') || class_exists('Woocommerce') || of_get_option('header_customfield') )
		{ ?>
			<div class="icon-dock-wrap">
				<ul class="icon-dock clearfix">
				<?php 
				
				// Custom Header Field			
				if( of_get_option('header_customfield') )
				{
					echo '<li class="customfield">'.do_shortcode( of_get_option('header_customfield') ).'</li>'; 
				}
				
				// WooCommerce Shopping Cart			
				if( class_exists('Woocommerce') )
				{
					global $woocommerce;  ?>
					<li class="shop-cart">
                        <span class="shop-cart-items">
                            <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'NorthVantage'); ?>">
                                <span class="shop-cart-itemnum">
                                    <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'NorthVantage'), $woocommerce->cart->cart_contents_count);?> -
                                </span>
                                <?php echo $woocommerce->cart->get_cart_total(); ?>
                            </a>
                        </span>
                        <span class="shop-cart-icon">
                            <a target="_parent" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"></a>
                        </span>
					</li>
				<?php
                }
				
				// WP e-Commerce Shopping Cart
				if ( function_exists('wpsc_cart_item_count') ) 
				{ ?>
					<li class="shop-cart">
						<span class="shop-cart-items">
							<a target="_parent" href="<?php echo get_option('shopping_cart_url'); ?>"><span class="shop-cart-itemnum"><?php echo wpsc_cart_item_count(); ?></span> <?php _e( 'items', 'NorthVantage' ); ?></a> 
						</span>
						<span class="shop-cart-icon">
							<a target="_parent" href="<?php echo get_option('shopping_cart_url'); ?>"></a>
						</span>
						<div class="shopping-cart-wrapper widget_wp_shopping_cart shop-cart-contents">
							<?php include( wpsc_get_template_file_path( 'wpsc-cart_widget.php' ) ); ?>
						</div>
					</li>
				<?php 
				} 
							
				if( of_get_option('enable_search') !='disable' )
				{ ?>  
					<li class="searchform">
						<form method="get" id="panelsearchform" class="disabled" action="<?php echo home_url(); ?>">
							<fieldset>
								<input type="text" name="s" id="drops" />
								<input type="button" id="panelsearchsubmit" value="&nbsp;" />
							</fieldset>
						</form>
					</li>
				<?php 
				} ?>
				</ul>
                <div class="clear"></div>  
			</div>     
			<?php 
			} 
			
			if( of_get_option('header_html') )
			{ ?>
				<div class="custom-html"><?php echo do_shortcode( of_get_option('header_html') ); ?></div>
			<?php 
			}
				
			if( of_get_option('branding_disable') !='disable' ) : ?>
                
				<div id="header-logo" class="<?php echo $NV_branding_alignment; ?>">
					<div id="logo">
					<?php 
					if( of_get_option('branding_url') ) : // Is Branding Image Set 
			
						if( $NV_branding_ver == 'secondary' ) : 
							$NV_branding_url = of_get_option('branding_url_sec'); 
						else : 
							$NV_branding_url = of_get_option('branding_url'); // check is secondary branding is selected 
						endif; ?>

						<a href="<?php echo home_url(); ?>/"><img src="<?php echo $NV_branding_url; ?>" alt="<?php bloginfo('name'); ?>" /></a>
    				
					<?php
					else: ?>
					
                    	<h1><a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h1>
						<h2 class="description"><?php bloginfo('description'); ?></h2>
                        
    				<?php 
					endif; ?>
					</div>
					<div class="clear"></div>
                </div><!-- /header-logo -->
                <?php 
				endif; ?>  
                
				<nav id="nv-tabs" class="<?php echo $NV_menu_alignment; ?>">
					<?php 
                    
                    // WP3.0 Custom Menu Support
                    if( of_get_option('wpcustomm_enable') !='disable' ) : 
                        
                        $menu_slug = ( get_post_meta( $post->ID, '_cmb_menu', true ) !='' ) ? get_post_meta( $post->ID, '_cmb_menu', true ) : '';
                        
                        if( $menu_slug != 'disable' ) :
                                
                            $walker = new dyn_walker;
                            
                            wp_nav_menu( array(
                                'echo' => true,
                                'container' => 'ul',
                                'menu_class' => 'menu hide-on-phones', 
                                'menu_id' => 'dyndropmenu',
                                'theme_location' => 'mainnav',
                                'walker' => $walker,
                                'menu' => $menu_slug
                            ));
                        
                        
                            if ( !class_exists('UberMenu') && of_get_option('enable_responsive') !='disable' ) : // check if responsive is disabled
                            
                                wp_nav_menu( array(
                                    'theme_location' => 'mainnav',
                                    'container' => 'div',
                                    'container_class' => 'hide-on-desktops', 
                                    'container_id' => 'nv_selectmenu',					
                                    'walker' => new Walker_Nav_Menu_Dropdown(),
                                    'items_wrap' => '<select><option value="">'.__( 'Select a Page', 'NorthVantage' ).'</option>%3$s</select>',
                                    'menu' => $menu_slug
                                ));
                                
                            endif;
                        endif;
                    
                    else :
                        
                        if ( of_get_option('enable_responsive') !='disable' ) : // check if responsive is disabled ?>
                                          
                            <div id="nv_selectmenu" class="wp-page-nav">
                                <?php wp_dropdown_pages( $args ); ?> 
                            </div> 
                        <?php 
                        endif; ?>
                        
                        <ul id="dropmenu" class="skinset-menu nv-skin menu">
                        <?php echo DYN_menupages(); ?>
                            <li class="menubreak"></li>
                        <?php 
                        
                        if( of_get_option('droppanel') !='disable' )
                        { 
                            if( of_get_option('droptriggername') !='' ) 
                            { ?>
                                <li class="page_item">
                                    <a href="#" class="droppaneltrigger" title="<?php echo of_get_option('droptriggername'); ?>"><?php echo of_get_option('droptriggername'); ?></a>
                                    <span class="menudesc"><?php echo of_get_option('droptriggerdesc'); ?></span>
                                </li>
                            <?php 
                            }
                        } ?>
                        </ul>
                    <?php 
                    endif; ?>
				</nav><!-- /nv-tabs -->
				<div class="clear"></div>
		</div><!-- /header -->
	</header><!-- /header-wrap -->
</div>