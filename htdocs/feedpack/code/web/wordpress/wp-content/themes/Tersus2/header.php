<?php
/**
 * @package WordPress
 * @subpackage NorthVantage  */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
	<head>

		<meta charset="<?php bloginfo('charset'); ?>" />




		<meta name="viewport" content="width=device-width" />

		<?php if (of_get_option('header_favicon')) {
			?>
			<link rel="shortcut icon" href="<?php echo of_get_option('header_favicon'); ?>" />
			<?php }
		?>

		<title><?php if (is_front_page()) bloginfo('name'); else wp_title('', true, 'right'); ?></title>

		<?php if (of_get_option('rss_feed')) {
			?>
			<link rel="alternate" type="application/rss+xml" title="<?php echo of_get_option('rss_title'); ?>" href="<?php echo of_get_option('rss_feed'); ?>" />
			<?php
		}

		/* ------------------------------------
		  :: CUSTOM PAGE DATA
		  ------------------------------------ */

		global $exittext, $exit_classes, $post;

		$introtext = ( get_post_meta($post->ID, '_cmb_introtext', true) != '' ) ? get_post_meta($post->ID, '_cmb_introtext', true) : '';
		$intro_classes = ( get_post_meta($post->ID, '_cmb_intro_classes', true) != '' ) ? get_post_meta($post->ID, '_cmb_intro_classes', true) : '';
		$exittext = ( get_post_meta($post->ID, '_cmb_exittext', true) != '' ) ? get_post_meta($post->ID, '_cmb_exittext', true) : '';
		$exit_classes = ( get_post_meta($post->ID, '_cmb_exit_classes', true) != '' ) ? get_post_meta($post->ID, '_cmb_exit_classes', true) : '';
		$show_slider = ( get_post_meta($post->ID, '_cmb_gallery', true) != '' ) ? get_post_meta($post->ID, '_cmb_gallery', true) : '';
		$gallerycat = ( get_post_meta($post->ID, '_cmb_gallerycat', true) != '' ) ? get_post_meta($post->ID, '_cmb_gallerycat', true) : '';

		require NV_FILES . "/inc/page-constants.php"; // Page Constants


		/* ------------------------------------
		  :: SKIN DATA
		  ------------------------------------ */

		delete_option('skin_select');
		$preview_skin = get_option('skin_select');

		// if preivewing skin
		if (!empty($preview_skin)) {
			$get_skin_data = maybe_unserialize(get_option('skin_data_' . $preview_skin));

			global $NV_frame_footer, $NV_branding_ver;

			$NV_skin = $preview_skin;

			$NV_frame_header = $get_skin_data['skin_id_frame_header'];
			$NV_frame_main = $get_skin_data['skin_id_frame_main'];
			$NV_frame_footer = $get_skin_data['skin_id_frame_footer'];
			$NV_branding_ver = $get_skin_data['skin_id_branding_ver'];
		} else {
			if (!empty($NV_skin))
				$skin = $NV_skin; elseif (DEFAULT_SKIN)
				$skin = DEFAULT_SKIN; else
				$skin = $NV_defaultskin;

			$get_skin_data = maybe_unserialize(get_option('skin_data_' . $skin));

			global $NV_frame_footer, $NV_branding_ver;

			$NV_frame_header = $get_skin_data['skin_id_frame_header'];
			$NV_frame_main = $get_skin_data['skin_id_frame_main'];
			$NV_frame_footer = $get_skin_data['skin_id_frame_footer'];
			$NV_branding_ver = $get_skin_data['skin_id_branding_ver'];
		}

		$frame_color = (!empty($get_skin_data['skin_id_icon_color']) ) ? $get_skin_data['skin_id_icon_color'] : 'light';
		?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<style type="text/css"><?php require NV_DIR . '/style.php'; ?></style>

		<?php
		// Cufon Font Replacement Script 
		require NV_FILES . "/inc/cufon-replace.php";

		wp_head();

		/* ------------------------------------
		  :: BROWSER SUPPORT
		  ------------------------------------ */
		?>

		<!--[if IE 7]>
		<link href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie7.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<!--[if lte IE 8]>	
		<script src="<?php echo get_template_directory_uri(); ?>/js/ie7.js" type="text/javascript"></script>
		<link href="<?php echo get_template_directory_uri(); ?>/stylesheets/ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->

<?php if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
	$i_device = true;
} else {
	$i_device = '';
} // detect iPhone / iPad  ?>
		

		
		
		
		



	</head>
	<body <?php body_class('skinset-background nv-skin'); ?>>

		<div id="primary-wrapper" <?php echo 'class="nv-' . $frame_color . '"'; ?>>

			<?php
			$layerset1 = (!empty($get_skin_data['skin_id_layer1_type']) ) ? stripslashes(htmlspecialchars($get_skin_data['skin_id_layer1_type'])) : '';
			$layerset2 = (!empty($get_skin_data['skin_id_layer2_type']) ) ? stripslashes(htmlspecialchars($get_skin_data['skin_id_layer2_type'])) : '';
			$layerset3 = (!empty($get_skin_data['skin_id_layer3_type']) ) ? stripslashes(htmlspecialchars($get_skin_data['skin_id_layer3_type'])) : '';
			$layerset4 = (!empty($get_skin_data['skin_id_layer4_type']) ) ? stripslashes(htmlspecialchars($get_skin_data['skin_id_layer4_type'])) : '';

			// Set Fixed Position for specific background layers

			if ($layerset1 == 'layer1_imagefull' || $layerset1 == 'layer1_video' || $layerset1 == 'layer1_cycle')
				$layer1_fixed = 'fixed'; else
				$layer1_fixed = '';
			if ($layerset2 == 'layer2_imagefull' || $layerset2 == 'layer2_video' || $layerset2 == 'layer2_cycle')
				$layer2_fixed = 'fixed'; else
				$layer2_fixed = '';
			if ($layerset3 == 'layer3_imagefull' || $layerset3 == 'layer3_video' || $layerset3 == 'layer3_cycle')
				$layer3_fixed = 'fixed'; else
				$layer3_fixed = '';
			if ($layerset4 == 'layer4_imagefull' || $layerset4 == 'layer4_video' || $layerset4 == 'layer4_cycle')
				$layer4_fixed = 'fixed'; else
				$layer4_fixed = '';
			?>

			<div id="custom-layer1" class="custom-layer <?php echo $layer1_fixed; ?>"><?php if ($layerset1) {
				echo setlayer_html("layer1", $layerset1, $skin);
			} ?></div>
			<div id="custom-layer2" class="custom-layer <?php echo $layer2_fixed; ?>"><?php if ($layerset2) {
				echo setlayer_html("layer2", $layerset2, $skin);
			} ?></div>
			<div id="custom-layer3" class="custom-layer <?php echo $layer3_fixed; ?>"><?php if ($layerset3) {
				echo setlayer_html("layer3", $layerset3, $skin);
			} ?></div>
			<div id="custom-layer4" class="custom-layer <?php echo $layer4_fixed; ?>"><?php if ($layerset4) {
				echo setlayer_html("layer4", $layerset4, $skin);
			} ?></div>
			<a id="top"></a>

			<?php
			/* ------------------------------------
			  :: CONFIGURE HEADER
			  ------------------------------------ */

			if ($NV_disableheader != 'yes') {
				require NV_FILES . "/inc/config-header.php";
			} else {
				echo '<div class="row"></div>';
			}

			/* ------------------------------------
			  :: REVOLUTION SLIDER
			  ------------------------------------ */

			if (is_page()) {
				if ($NV_show_slider == "revslider") {
					$revslider_id = get_post_meta($post->ID, '_cmb_data-7', true);

					if (!empty($revslider_id)) {
						echo "\n\t" . '<div class="revslider-container">';
						echo "\n\t\t" . putRevSlider($revslider_id);
						echo "\n\t" . '</div>';
					}
				}
			}


			/* ------------------------------------
			  :: STAGE GALLERY, iSLIDER, NIVO
			  ------------------------------------ */

			if (is_page()) {
				if ($NV_show_slider == "stageslider" || $NV_show_slider == "islider" || $NV_show_slider == "nivo") {
					require NV_FILES . "/inc/gallery-stage.php"; // Stage Gallery
				}
			}

			/* ------------------------------------
			  :: PIECEMAKER
			  ------------------------------------ */

			// if iPad or iPhone
			if ($i_device && $NV_show_slider == "gallery3d") {
				if (is_page()) {
					require NV_FILES . "/inc/gallery-stage.php"; // Stage Gallery
				}
			} else {
				if (is_page()) {
					if ($NV_show_slider == "gallery3d") {
						require NV_FILES . '/inc/gallery-piecemaker.php'; //
					}
				}
			}

			echo '<div class="wrapper">';


			/* ------------------------------------
			  :: ACCORDION
			  ------------------------------------ */

			if (is_page()) {
				if ($NV_show_slider == "galleryaccordion") {
					require NV_FILES . '/inc/gallery-accordion.php';
				}
			}

			/* ------------------------------------
			  :: GRID
			  ------------------------------------ */

			if (is_page()) {
				if ($NV_show_slider == "gridgallery" && $NV_groupsliderpos != "below") {
					if ($NV_gridfilter == 'yes')
						$NV_galleryclass = $NV_galleryclass . ' filter';
					echo '<div id="grid-main" data-grid-columns="' . $NV_gridcolumns . '" class="gallery-wrap grid-gallery row ' . $NV_galleryclass . ' nv-skin">';
					require NV_FILES . "/inc/gallery-grid.php"; // Group Slider Gallery
					echo '</div>';
				}
			}

			/* ------------------------------------
			  :: GROUP SLIDER
			  ------------------------------------ */

			if (is_page()) {
				if ($NV_show_slider == "groupslider" && $NV_groupsliderpos != "below") {
					require NV_FILES . "/inc/gallery-groupslider.php"; // Group Slider Gallery
				}
			}

			/* ------------------------------------
			  :: TWITTER
			  ------------------------------------ */

			if ($NV_twitter == 'pagetop' && get_post_meta($post->ID, '_cmb_twitter', true) != 'disable') {
				echo "\n" . '<div class="row">';
				echo "\n\t" . '<div class="twitter-wrap skinset-main nv-skin ' . $NV_frame_header . '">';

				require NV_FILES . '/inc/twitter.php'; // Call Twitter Template

				echo "\n\t" . '</div>';
				echo "\n" . '</div>';
			}

			/* ------------------------------------
			  :: INTRO TEXT
			  ------------------------------------ */

			if (!empty($introtext) || $NV_pagetitle != "BLANK" && is_page()) {
				if (empty($intro_classes))
					$intro_classes = 'skinset-main';

				echo "\n\t" . '<div class="intro-text ' . $intro_classes . ' ' . $NV_frame_main . ' nv-skin">';
				echo "\n\t\t" . '<div>';

				if (!empty($NV_postdate) && !empty($NV_authorname) && !empty($NV_pagesubtitle) && !empty($NV_pagetitle) || $NV_pagetitle != "BLANK") {
					echo "\n\t\t\t" . '';

					if (!empty($NV_pagesubtitle)) {
						echo "" . '';
					}

					if ($NV_postdate || $NV_authorname) {
						echo "\n\t\t\t\t" . '<div class="post-date">';

						if ($NV_postdate) {
							echo "\n\t\t\t\t\t" . '<small>' . get_the_time('F jS  Y') . '</small><span class="break">&nbsp;</span>';
						}

						if ($NV_authorname) {
							echo "\n\t\t\t\t\t" . '<small>' . __('By', 'NorthVantage') . ' <span class="author">' . get_the_author_meta('first_name', $wp_query->post->post_author) . ' ' . get_the_author_meta('last_name', $wp_query->post->post_author) . '</span></small>';
						}

						echo "\n\t\t\t\t" . '</div>';
					}

					echo "\n\t\t\t" . '</div><!-- /post-titles -->';
				}

				// Intro Text
				if (!empty($introtext))
					echo do_shortcode($introtext);

				echo "\n\t\t" . '</div>';
				echo "\n\t\t" . '<div class="clear"></div>';
				echo "\n\t" . '</div>';
				echo "\n" . '</div>';
			}

			// Hide Main Content
			if ($NV_hidecontent != "yes") {
				global $wp_query;

				if ($NV_contentborder != 'yes' || is_search()) {
					$NV_contentborder = "no";
				}

				if ($NV_contentborder == "yes") {
					$NV_frame_main = 'disabled';
				}

				echo "\n" . '<div class="content-wrap row">';
			}


			/* ------------------------------------
			  :: BREADCRUMBS
			  ------------------------------------ */

			if (class_exists('BP_Core_User')) {
				if (!bp_is_blog_page()) {
					if (empty($NV_hidebreadcrumbs))
						$NV_hidebreadcrumbs = "yes";
				}
			}


			if (class_exists('WPSC_Query')) {
				if (get_post_type() == 'wpsc-product' || is_products_page()) {
					if (!wpsc_has_breadcrumbs()) {
						if (empty($NV_hidebreadcrumbs))
							$NV_hidebreadcrumbs = "yes";
					}
				}
			}

			if (is_front_page() || of_get_option('breadcrumb') == 'disable')
				$NV_hidebreadcrumbs = 'yes';

			// Sub Header Display	
			if ($NV_hidebreadcrumbs == 'yes' && ( empty($NV_socialicons) || $NV_socialicons == 'off' ) && empty($NV_textresize))
				$NV_disable_subtabs = 'yes'; else
				$NV_disable_subtabs = '';

			if ($NV_disable_subtabs != 'yes') {
				?>


				<?php
				if ($NV_socialicons == "yes" || $NV_textresize == "yes") {
					echo '<div class="resize-social-wrap columns">';
					require NV_FILES . '/inc/social-icons.php';
					echo '</div><!-- / resize-social-wrap -->';
				}

				if ($NV_hidebreadcrumbs != 'yes') {
					?>
					<div id="sub-tabs" class="columns five">
					<?php
					if (class_exists('bbPress') && is_bbpress()) {
						bbp_breadcrumb();
					} else {
						?>
							<ul>
						<?php
						if (function_exists('wpsc_has_breadcrumbs') || function_exists('woocommerce_breadcrumb')) {
							// Woocommerce Breadcrumb
							if (function_exists('woocommerce_breadcrumb')) {
								woocommerce_breadcrumb('delimiter= / &wrap_before=<div class="breadcrumb">');
							}

							// WP e-Commerce Breadcrumb				
							if (function_exists('wpsc_has_breadcrumbs')) {
								if (wpsc_has_breadcrumbs() && !is_page()) {
									?>
											<div class='breadcrumb'>
												<li class="home">
													<a href='<?php echo get_option('product_list_url'); ?>'><?php echo get_option('blogname'); ?></a>
													<span class="subbreak">/</span>
												</li>
											<?php
											while (wpsc_have_breadcrumbs()) : wpsc_the_breadcrumb();
												if (wpsc_breadcrumb_url()) {
													?> 	   
														<li>
															<a href='<?php echo wpsc_breadcrumb_url(); ?>'><?php echo wpsc_breadcrumb_name(); ?></a>
															<span class="subbreak">/</span>
														</li>
													<?php
												} else {
													?> 
														<li><?php echo wpsc_breadcrumb_name(); ?></li>
														<li class="subbreak">/</li>
														<?php
													}
												endwhile;

												if ($wp_query->is_single === true && get_post_type() == 'wpsc-product') {
													?>
													<li class="current_page_item">
							<?php echo wpsc_the_product_title(); ?>
														<span class="subbreak">/</span>	
													</li>
													<?php }
												?>
											</div>
						<?php
					} else {
						if (function_exists('DYN_breadcrumbs'))
							DYN_breadcrumbs();
					}
				}
			}
			else {
				if (function_exists('DYN_breadcrumbs'))
					DYN_breadcrumbs();
			}
			?>
							</ul>
									<?php }
								?>
					</div>
							<?php }
						?>
			</div>
						<?php
					}

					echo "\n" . '<div class="row">';