<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	//$themename = get_option( 'stylesheet' );
	//$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' ); 
	$optionsframework_settings['id'] = 'themeva'; // _themeva_mod
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */ 

function optionsframework_options() {

	// WP Editor data
	$wp_editor_settings = array(
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'tinymce' => array( 'plugins' => 'wordpress' )
	);

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/lib/adm/images/';	

	// Page Layout
	$page_layout_array = array(
		'layout_one' => array(
			'path' => $imagepath . 'layout-1.png',
			'name' => ''
		),
		'layout_two' => array(
			'path' => $imagepath . 'layout-2.png',
			'name' => ''
		),	
			'layout_three' => array(
			'path' => $imagepath . 'layout-3.png',
			'name' => ''
		),	
		'layout_four' => array(
			'path' => $imagepath . 'layout-4.png',
			'name' => ''
		),	
		'layout_five' => array(
			'path' => $imagepath . 'layout-5.png',
			'name' => ''
		),	
		'layout_six' => array(
			'path' => $imagepath . 'layout-6.png',
			'name' => ''
		),				 			 			 			 			 		 
	);		

	// Columns data
	$columns_array = array(
		'1' => __('One', 'options_framework_theme'),
		'2' => __('Two', 'options_framework_theme'),
		'3' => __('Three', 'options_framework_theme'),
		'4' => __('Four', 'options_framework_theme'),
	);

	// Sidebar data
	$sidebars = ( of_get_option('sidebars_num') !='' ? of_get_option('sidebars_num') : 2 );
	
	for ( $i=1; $i <= $sidebars; $i++ )
	{
		 $sidebar_array['Sidebar'.$i] = 'Sidebar '.$i;
	}
	
	$post_formats = get_theme_support( 'post-formats' );

	foreach( $post_formats[0] as $format )
	{
		 $format_array[$format] = $format;
	}	

	// Author Bio
	$author_bio_array = array(
		'posts' => __('Posts', 'options_framework_theme'),
		'enable' => __('Posts &amp; Pages', 'options_framework_theme'),
		'disable' => __('Disable', 'options_framework_theme'),
	);

	// Twitter Fedd
	$twitter_feed_array = array(
		'none' => __('Disabled', 'options_framework_theme'),
		'pagetop' => __('Top', 'options_framework_theme'),
		'pagebot' => __('Bottom', 'options_framework_theme'),
	);	

	// Social Icon data
	$social_icon_array = social_icon_data();

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );
		
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category){
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}


	$options = array();

	$options[] = array(
		'name' => __('General', 'options_framework_theme'),
		'type' => 'heading'
	);	

	$options[] = array(
		'name' => __('Layout', 'options_framework_theme'),
		'type' => 'info'
	);

	$options[] = array(
		'name' => __('Responsive Design', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'enable_responsive',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => 'Page Layout',
		'desc' => '',
		'id' => 'pagelayout',
		'std' => 'layout_four',
		'type' => "images",
		'options' => $page_layout_array
	);		

	$options[] = array(
		'name' => __('Breadcrumbs', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'breadcrumb',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Page Comments', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'pagecomments',
		'std' => 'disable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Show Author Bio', 'options_framework_theme'),
		'desc' => __('Enable this option to display author bio information.', 'options_framework_theme'),
		'id' => 'author_bio',
		'std' => 'disable',
		'type' => 'radio_inline',
		'options' => $author_bio_array
	);	

	$options[] = array(
		'name' => __('Number of Sidebars', 'options_framework_theme'),
		'plac' => __('Default is 2 sidebars', 'options_framework_theme'),
		'id' => 'sidebars_num',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Typography', 'options_framework_theme'),
		'type' => 'info',
		'tooltip' => 'See documentation for how to setup custom <a target="_blank" href="http://help.northvantage.co.uk/docs/tersus/cufon-font-settings/">Google / Cufón</a> Fonts.',		
	);	

	$options[] = array(
		'name' => __('Font Type', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'nv_font_type',
		'std' => 'enable_google',
		'type' => 'radio_inline',
		'options' => array(
			'enable' => __('Cufón', 'options_framework_theme'),
			'enable_google' => __('Google', 'options_framework_theme'),
			'disable' => __('Standard', 'options_framework_theme')
		)	
	);		
	
	$options[] = array(
		'name' => __('Custom Cufón Font', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'cufon_font',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __('Custom Google Font One', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'googlefont_url_1',
		'plac' => 'URL Name',
		'std' => '',
		'type' => 'text'
	);	

	$options[] = array(
		'name' => __('', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'googlefont_css_1',
		'plac' => 'CSS Name',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Custom Google Font Two', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'googlefont_url_2',
		'plac' => 'URL Name',
		'std' => '',
		'type' => 'text'
	);	

	$options[] = array(
		'name' => __('', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'googlefont_css_2',
		'plac' => 'CSS Name',
		'std' => '',
		'type' => 'text'
	);			

	$options[] = array(
		'name' => __('Image Resizer', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => __('Image Resizer', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'timthumb_disable',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => __('JW Player', 'options_framework_theme'),
		'type' => 'info',
		'desc' => __('See documentation for how to setup JW Player', 'options_framework_theme'),			
	);	

	$options[] = array(
		'name' => __('Javascript File ( jwplayer.js )', 'options_framework_theme'),
		'id' => 'jwplayer_js',
		'plac' => 'Upload jwplayer.js',
		'type' => 'upload'
	);	

	$options[] = array(
		'name' => __('Flash File ( player.swf )', 'options_framework_theme'),
		'id' => 'jwplayer_swf',
		'plac' => 'Upload player.swf',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __('Plugins', 'options_framework_theme'),
		'id' => 'jwplayer_plugins',
		'plac' => 'Comma separated',
		'type' => 'upload'
	);	

	$options[] = array(
		'name' => __('Skin', 'options_framework_theme'),
		'id' => 'jwplayer_skin',
		'plac' => '.zip file',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __('Controlbar Height', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'jwplayer_height',
		'plac' => '24 is the default',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Controlbar Position', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'jwplayer_skinpos',
		'std' => 'over',
		'type' => 'radio_inline',
		'options' => array(
			'over' => __('Over', 'options_framework_theme'),
			'top' => __('Top', 'options_framework_theme'),
			'bottom' => __('Bottom', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Flickr', 'options_framework_theme'),
		'type' => 'info',
		'desc' => __('See documentation for how to setup Flickr', 'options_framework_theme'),		
	);		

	$options[] = array(
		'name' => __('Flickr User ID', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'flickr_userid',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Header', 'options_framework_theme'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __('Branding / Logo', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => __('Primary Logo', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'branding_url',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __('Secondary Logo', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'branding_url_sec',
		'type' => 'upload'
	);		

	$options[] = array(
		'name' => __('Main Menu', 'options_framework_theme'),
		'type' => 'info'
	);

	$options[] = array(
		'name' => __('WP Custom Menu', 'options_framework_theme'),
		'id' => 'wpcustomm_enable',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Custom Menu Descriptions', 'options_framework_theme'),
		'id' => 'wpcustommdesc_enable',
		'std' => 'disable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);	
	
	$options[] = array(
		'name' => __('Drop Panel / Search', 'options_framework_theme'),
		'type' => 'info',
	);

	$options[] = array(
		'name' => __('Display Drop Panel', 'options_framework_theme'),
		'id' => 'enable_droppanel',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Display Search', 'options_framework_theme'),
		'id' => 'enable_search',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => __('Trigger Button Align', 'options_framework_theme'),
		'id' => 'droppanel_button_align',
		'std' => 'center',
		'type' => 'radio_inline',
		'options' => array(
			'left' => __('Left', 'options_framework_theme'),
			'center' => __('Center', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Drop Panel Columns', 'options_framework_theme'),
		'id' => 'droppanel_columns_num',
		'std' => '4',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $columns_array
	);	

	$options[] = array(
		'name' => __('Extras', 'options_framework_theme'),
		'type' => 'info',
	);

	$options[] = array(
		'name' => __('Custom Header HTML / Shortcode', 'options_framework_theme'),
		'id' => 'header_html',
		'type' => 'textarea',
		'settings' => array(
			'rows' => '20'
		)
	);	

	$options[] = array(
		'name' => __('Header Custom Field', 'options_framework_theme'),
		'id' => 'header_customfield',
		'type' => 'editor',
		'settings' => $wp_editor_settings 
	);

	$options[] = array(
		'name' => __('Header Infobar Message', 'options_framework_theme'),
		'id' => 'header_infobar',
		'type' => 'editor',
		'settings' => $wp_editor_settings 
	);
		
	$options[] = array(
		'name' => __('Footer', 'options_framework_theme'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __('Main Footer', 'options_framework_theme'),
		'type' => 'info',
	);		

	$options[] = array(
		'name' => __('Display Footer', 'options_framework_theme'),
		'id' => 'mainfooter',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => __('Footer Columns', 'options_framework_theme'),
		'id' => 'footer_columns_num',
		'std' => '4',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $columns_array
	);

	$options[] = array(
		'name' => __('Lower Footer', 'options_framework_theme'),
		'type' => 'info',
	);

	$options[] = array(
		'name' => __('Display Lower Footer', 'options_framework_theme'),
		'id' => 'lowerfooter',
		'std' => 'enable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'enable' => __('On', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Left Column', 'options_framework_theme'),
		'id' => 'lowfooterleft',
		'type' => 'textarea',
		'settings' => $wp_editor_settings 
	);

	$options[] = array(
		'name' => __('Right Column', 'options_framework_theme'),
		'id' => 'lowfooterright',
		'type' => 'textarea',
		'settings' => $wp_editor_settings 
	);


	$options[] = array(
		'name' => __('Blog', 'options_framework_theme'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __('Layout', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => 'Page Layout',
		'desc' => '',
		'id' => 'arhlayout',
		'std' => 'layout_four',
		'type' => "images",
		'options' => $page_layout_array
	);

	$options[] = array(
		'name' => __('Column 1 Sidebar', 'options_framework_theme'),
		'id' => 'archcolone',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $sidebar_array
	);

	$options[] = array(
		'name' => __('Column 2 Sidebar', 'options_framework_theme'),
		'id' => 'archcoltwo',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $sidebar_array
	);	

	$options[] = array(
		'name' => __('Layout Format', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'arhpostdisplay',
		'std' => 'normal',
		'type' => 'radio_inline',
		'options' => array(
			'normal' => __('Normal', 'options_framework_theme'),
			'grid' => __('Grid', 'options_framework_theme')
		)	
	);

	$options[] = array(
		'name' => __('Grid Columns', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'arhpostcolumns',
		'std' => 'normal',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'2' => 'Two',
			'3' => 'Three',
			'4' => 'Four',
			'5' => 'Five',
			'6' => 'Six',
		)
	);	

	$options[] = array(
		'name' => __('Post Content', 'options_framework_theme'),
		'id' => 'arhpostcontent',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'' => 'Excerpt Only',
			'excerpt_image' => 'Excerpt + First / Custom Image',
			'image_only' => 'Image Only',
			'full_post' => 'Full Post',
		)
	);

	$options[] = array(
		'name' => __('Display Post Metadata', 'options_framework_theme'),
		'id' => 'arhpostpostmeta',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'' => 'Archive / Single Post',
			'archive_only' => 'Archive Only',
			'post_only' => 'Single Post Only',
			'disabled' => 'Disabled',
		)
	);

	$options[] = array(
		'name' => __('Post Metadata Align', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'postmetaalign',
		'std' => 'default',
		'type' => 'radio_inline',
		'options' => array(
			'default' => __('Left', 'options_framework_theme'),
			'post_title' => __('Below Title', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => __('Blog Page Images', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => __('Image Alignment', 'options_framework_theme'),
		'id' => 'arhimgalign',
		'std' => 'center',
		'type' => 'radio_inline',
		'options' => array(
			'alignleft' => __('Left', 'options_framework_theme'),
			'aligncenter' => __('Center', 'options_framework_theme'),
			'alignright' => __('Right', 'options_framework_theme')
		)	
	);
	
	$options[] = array(
		'name' => __('Image Lightbox', 'options_framework_theme'),
		'id' => 'arhimgdisplay',
		'std' => 'disable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'lightbox' => __('On', 'options_framework_theme')
		)	
	);	
	
	$options[] = array(
		'name' => __('Image Effect', 'options_framework_theme'),
		'id' => 'arhimgeffect',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'' => 'Shadow + Reflection',
			'reflect' => 'Reflection',
			'shadow' => 'Shadow',
			'frame' => 'Frame',
			'blackwhite' => 'Black & White',
			'frameblackwhite' => 'Frame + Black & White',
			'shadowblackwhite' => 'Shadow + Black & White',
			'none' => 'None',
		)
	);

	$options[] = array(
		'name' => __('Image Height', 'options_framework_theme'),
		'plac' => __('Default is 300', 'options_framework_theme'),
		'id' => 'arhimgheight',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Image Width', 'options_framework_theme'),
		'id' => 'arhimgwidth',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Single Post Images', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => __('Image Alignment', 'options_framework_theme'),
		'id' => 'postimgalign',
		'std' => 'center',
		'type' => 'radio_inline',
		'options' => array(
			'alignleft' => __('Left', 'options_framework_theme'),
			'aligncenter' => __('Center', 'options_framework_theme'),
			'alignright' => __('Right', 'options_framework_theme')
		)	
	);
	
	$options[] = array(
		'name' => __('Image Lightbox', 'options_framework_theme'),
		'id' => 'postimgdisplay',
		'std' => 'disable',
		'type' => 'switch',
		'options' => array(
			'disable' => __('Off', 'options_framework_theme'),
			'lightbox' => __('On', 'options_framework_theme')
		)	
	);	
	
	$options[] = array(
		'name' => __('Image Effect', 'options_framework_theme'),
		'id' => 'postimgeffect',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'' => 'Shadow + Reflection',
			'reflection' => 'Reflection',
			'shadow' => 'Shadow',
			'frame' => 'Frame',
			'none' => 'None',
		)
	);

	$options[] = array(
		'name' => __('Image Height', 'options_framework_theme'),
		'id' => 'postimgheight',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Image Width', 'options_framework_theme'),
		'id' => 'postimgwidth',
		'std' => '',
		'type' => 'text'
	);	

	/*
	$options[] = array(
		'name' => __('Filter Blog Posts', 'options_framework_theme'),
		'type' => 'info',
	);	

	$options[] = array(
		'name' => __('Disable Post Format(s)', 'options_framework_theme'),
		'id' => 'filter_formats',
		'std' => '',
		'type' => 'multicheck',
		'options' => $format_array		
	);		
	*/

	if (class_exists( 'BP_Core_User' ) || class_exists( 'bbPress' ) ) { 

		$options[] = array(
			'name' => __('BuddyPress / BBPress', 'options_framework_theme'),
			'type' => 'heading'
		);
	
		$options[] = array(
			'name' => __('Layout', 'options_framework_theme'),
			'type' => 'info',
		);	
	
		$options[] = array(
			'name' => 'Page Layout',
			'desc' => '',
			'id' => 'buddylayout',
			'std' => 'layout_four',
			'type' => "images",
			'options' => $page_layout_array
		);
	
		$options[] = array(
			'name' => __('Column 1 Sidebar', 'options_framework_theme'),
			'id' => 'buddycolone',
			'type' => 'select',
			'class' => 'mini', //mini, tiny, small
			'options' => $sidebar_array
		);
	
		$options[] = array(
			'name' => __('Column 2 Sidebar', 'options_framework_theme'),
			'id' => 'buddycoltwo',
			'type' => 'select',
			'class' => 'mini', //mini, tiny, small
			'options' => $sidebar_array
		);	
	
	}
	
	
	$options[] = array(
		'name' => __('Social Media', 'options_framework_theme'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __('Social Icons', 'options_framework_theme'),
		'type' => 'info',
		'id' => 'social_info',
		'tooltip' => 'Switch Social Icons "On" if you want to enable social icons on every Page / Post, it can be disabled on individual pages if required.',		
	);	

	$options[] = array(
		'name' => __('Social Icons', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'display_socialicons',
		'std' => 'off',
		'type' => 'switch',
		'options' => array(
			'off' => __('Off', 'options_framework_theme'),
			'yes' => __('On', 'options_framework_theme')
		)	
	);	

	$options[] = array(
		'name' => __('Share Icon', 'options_framework_theme'),
		'desc' => __('', 'options_framework_theme'),
		'id' => 'socialicons_share',
		'std' => 'on',
		'type' => 'switch',
		'options' => array(
			'yes' => __('Off', 'options_framework_theme'),
			'on' => __('On', 'options_framework_theme')
		)	
	);		

	$options[] = array(
		'name' => __('Social Icons Color', 'options_framework_theme'),
		'id' => 'socialicons_color',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => array (
			'' => 'Default',
			'light' => 'Light',
			'dark' => 'Dark',
			'color' => 'Color',
		)
	);	

	foreach ( $social_icon_array as $key => $socialicon )
	{
		$social_array[ strtolower( str_replace('.','',$socialicon['name'] ) ) ] = $socialicon['name'];
	}

	$options[] = array(
		'name' => __('Enable Social Icons', 'options_framework_theme'),
		'id' => 'socialicons',
		'std' => '',
		'type' => 'multicheck',
		'options' => $social_array		
	);	
	

	$options[] = array(
		'name' => __('Social Icon URL\'s', 'options_framework_theme'),
		'type' => 'info',
		'id' => 'socialurl_info',		
	);			
	
	foreach ( $social_icon_array as $key => $socialicon )
	{
		$options[] = array(
			'name' => __( $socialicon['name'] , 'options_framework_theme'),
			'id' => $key,
			'std' => $socialicon['path'],
			'type' => 'text'
		);	
	}

	$options[] = array(
		'name' => __('Twitter Feed', 'options_framework_theme'),
		'type' => 'info',
		'tooltip' => 'Enter your Twitter details, if you wish to enable Twitter Feed globally, set the Twitter Display, this can be overriden on individual pages.',
	);

	$options[] = array(
		'name' => __('Twitter Username', 'options_framework_theme'),
		'id' => 'twitter_usrname',
		'type' => 'text'
	);	
	
	$options[] = array(
		'name' => __('Number of Tweets', 'options_framework_theme'),
		'id' => 'twitter_feednum',
		'plac' => 'Enter the number of tweets to display',
		'type' => 'text'
	);	

	$options[] = array(
		'name' => __('Twitter Display', 'options_framework_theme'),
		'id' => 'twitter_display',
		'std' => 'none',
		'type' => 'radio_inline',
		'options' => $twitter_feed_array
	);		
	
	$options[] = array(
		'name' => __('Main RSS Feed', 'options_framework_theme'),
		'type' => 'info',
	);	
		
	$options[] = array(
		'name' => __('Main RSS Title', 'options_framework_theme'),
		'id' => 'rss_title',
		'std' => 'Blog',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Main RSS Feed URL', 'options_framework_theme'),
		'id' => 'rss_feed',
		'plac' => 'e.g. /category/YOUR-CATEGORY-NAME/feed/',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __('Customize', 'options_framework_theme'),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __('Custom CSS', 'options_framework_theme'),
		'id' => 'header_css',
		'type' => 'textarea',
		'settings' => array(
			'rows' => '20'
		)		
	);	

	$options[] = array(
		'name' => __('Mobile CSS', 'options_framework_theme'),
		'id' => 'responsive_css',
		'desc' => __('CSS for Mobile / Responsive mode e.g. ( hides sidebars ) <strong>.content-wrap .sidebar {display:none !important;}</strong>', 'options_framework_theme'),
		'type' => 'textarea',
		'settings' => array(
			'rows' => '20'
		)		
	);

	$options[] = array(
		'name' => __('JavaScript', 'options_framework_theme'),
		'id' => 'footer_javascript',
		'desc' => __('Add your scripts here, loads at the end of the page. E.g. <strong>&lt;script type="text/javascript"&gt; YOUR CODE &lt;/script&gt;</strong>', 'options_framework_theme'),
		'type' => 'textarea',
		'settings' => array(
			'rows' => '20'
		)
	);	

	return $options;
}

/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {
  		$('#section-example_text_hidden').fadeToggle(400);
	});

	if ($('#example_showhidden:checked').val() !== undefined) {
		$('#section-example_text_hidden').show();
	}

});
</script>

<?php
}