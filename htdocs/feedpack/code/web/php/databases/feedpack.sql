/*
SQLyog Enterprise - MySQL GUI v5.17
Host - 5.5.20-log : Database - feedpack_new
*********************************************************************
Server version : 5.5.20-log
*/

SET NAMES utf8;

SET SQL_MODE='';
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `security_key` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `admins` */

insert into `admins` (`id`,`username`,`password`,`email_address`,`security_key`) values (1,'admin','admin123','tr.exphlux@gmail.com','21232f297a57a5a743894a0e4a801fc3'),(2,'vendor','vendor','tr.ephlux@gmail.com','7c3613dba5171cb6027c67835dd3b9d4');

/*Table structure for table `answers` */

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT '0',
  `answer` varchar(500) DEFAULT NULL,
  `feedback_id` int(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Data for the table `answers` */

insert into `answers` (`id`,`question_id`,`answer`,`feedback_id`) values (1,15,'tr@testin.com',1),(2,14,'',1),(3,13,'opt1',1),(4,15,'tr2@testin.com',2),(5,14,'aa@me.com',2),(6,13,'opt2',2),(7,15,'tr3@testin.com',3),(8,14,'aa2@me.com',3),(9,13,'opt2',3),(10,15,'tr4@testin.com',4),(11,14,'aa3@me.com',4),(12,13,'opt1',4),(13,15,'tr5@testin.com',5),(14,14,'aa4@me.com',5),(15,13,'opt2',5),(16,15,'tr@testin.com',6),(17,14,'',6),(18,13,'opt1',6),(19,20,'test@aa.com',7),(20,20,'test2@aa.com',8),(21,20,'test3@aa.com',9),(22,20,'test4@aa.com',10),(23,20,'test5@aa.com',11),(24,15,'aa@aad.c',12),(25,14,'test@domain.com',12),(26,13,'opt1',12),(27,21,'a1@aa.com',13),(28,32,'3',14),(29,33,'Yes',14),(30,34,'',14),(31,32,'5',15),(32,33,'No',15),(33,34,'aa@aa.com',15);

/*Table structure for table `campaign_types` */

DROP TABLE IF EXISTS `campaign_types`;

CREATE TABLE `campaign_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_type` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `campaign_types` */

insert into `campaign_types` (`id`,`campaign_type`,`status`) values (1,'Product',1),(2,'Delivery',1);

/*Table structure for table `campaigns` */

DROP TABLE IF EXISTS `campaigns`;

CREATE TABLE `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `recipent_name` varchar(255) DEFAULT NULL,
  `show_location_field` int(11) NOT NULL,
  `is_email_mandatory` int(11) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `no_of_packages` int(11) DEFAULT '0',
  `campaign_status` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `campaign_type` int(10) DEFAULT '2',
  `logo` varchar(255) DEFAULT NULL,
  `header_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `campaigns` */

insert into `campaigns` (`id`,`campaign_name`,`recipent_name`,`show_location_field`,`is_email_mandatory`,`creation_date`,`no_of_packages`,`campaign_status`,`vendor_id`,`campaign_type`,`logo`,`header_text`) values (1,'new test campaign','tahir',1,0,'2013-03-21',10,'Active',2,2,NULL,NULL),(2,'testing campaign type','tahir',1,0,'2013-03-22',12,'Active',2,1,NULL,NULL),(3,'type delivery','tahir',1,0,'2013-03-26',10,'Draft',2,2,'41c9e45416885356fa5520ce6d868ee5.jpg','testing header text'),(4,'testing for last changes','tahir',1,0,'2013-03-26',10,'Draft',2,2,'e612cd6f05630dc9e239a4f43330a02c.jpg','testing header text'),(5,'test test test ','tahir',1,0,'2013-03-26',10,'Active',2,2,'','Fill us in on your delivery');

/*Table structure for table `feedbacks` */

DROP TABLE IF EXISTS `feedbacks`;

CREATE TABLE `feedbacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `on_time_delivery` tinyint(1) DEFAULT NULL COMMENT '1=yes, 0= no',
  `parcel_condition` int(11) DEFAULT '0',
  `behavior` int(11) DEFAULT '0',
  `customer_email` varchar(255) DEFAULT NULL,
  `feedback_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `comment` text NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `parcel_id` int(11) NOT NULL DEFAULT '0',
  `user_location` varchar(500) DEFAULT NULL,
  `p2g_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `feedbacks` */

insert into `feedbacks` (`id`,`on_time_delivery`,`parcel_condition`,`behavior`,`customer_email`,`feedback_time`,`campaign_id`,`comment`,`vendor_id`,`parcel_id`,`user_location`,`p2g_code`) values (1,NULL,0,0,NULL,'2013-03-21 13:01:46',1,'',2,-1,'24.8622844,67.0723768','0'),(2,NULL,0,0,NULL,'2013-03-21 13:05:35',1,'',2,-1,'24.8622844,67.0723768','0'),(3,NULL,0,0,NULL,'2013-03-21 13:05:35',1,'',2,-1,'24.8622844,67.0723768','0'),(4,NULL,0,0,NULL,'2013-03-21 13:05:35',1,'',2,-1,'24.8622844,67.0723768','0'),(5,NULL,0,0,NULL,'2013-03-21 13:31:13',1,'',2,3,'24.8622785,67.0723721','0'),(6,NULL,0,0,NULL,'2013-03-21 13:54:55',1,'',2,9,'24.8622789,67.0723876','0'),(7,NULL,0,0,NULL,'2013-03-22 15:17:08',2,'',2,2,'24.8622688,67.07238339999999',''),(8,NULL,0,0,NULL,'2013-03-22 15:17:08',2,'',2,5,'24.8622688,67.07238339999999',''),(9,NULL,0,0,NULL,'2013-03-22 15:17:08',2,'',2,0,'24.8622688,67.07238339999999',''),(10,NULL,0,0,NULL,'2013-03-22 15:19:19',2,'',2,0,'24.8622688,67.07238339999999',''),(11,NULL,0,0,NULL,'2013-03-22 15:19:19',2,'',2,0,'24.8622688,67.07238339999999',''),(12,NULL,0,0,NULL,'2013-03-22 16:27:39',1,'',2,-1,'24.8622464,67.0724151','0'),(13,NULL,0,0,NULL,'2013-03-22 20:05:26',2,'',2,0,'',''),(14,NULL,0,0,NULL,'2013-03-26 19:32:12',5,'',2,1,'24.8622651,67.0726998','0'),(15,NULL,0,0,NULL,'2013-03-26 19:32:12',5,'',2,2,'24.8622651,67.0726998','0'),(16,NULL,0,0,NULL,'2013-03-26 19:34:28',5,'',2,3,'','0');

/*Table structure for table `parcels` */

DROP TABLE IF EXISTS `parcels`;

CREATE TABLE `parcels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sender_fullname` varchar(255) DEFAULT NULL,
  `sender_dob` date DEFAULT NULL,
  `sender_address` text,
  `receiver_fullname` varchar(255) DEFAULT NULL,
  `receiver_dob` date DEFAULT NULL,
  `receiver_address` text,
  `p2g_code` int(11) DEFAULT NULL,
  `upi_code` bigint(20) unsigned NOT NULL,
  `destination` text,
  `parcel_weight` decimal(2,2) DEFAULT NULL,
  `parcel_weight_unit` varchar(100) DEFAULT NULL,
  `parcel_dimensions` varchar(100) DEFAULT NULL,
  `parcel_dimensions_unit` varchar(100) DEFAULT NULL,
  `parcel_type` varchar(255) DEFAULT NULL,
  `parcel_insurance_value` int(10) DEFAULT NULL,
  `parcel_service_used` varchar(255) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `carrier_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`upi_code`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `parcels` */

insert into `parcels` (`id`,`sender_fullname`,`sender_dob`,`sender_address`,`receiver_fullname`,`receiver_dob`,`receiver_address`,`p2g_code`,`upi_code`,`destination`,`parcel_weight`,`parcel_weight_unit`,`parcel_dimensions`,`parcel_dimensions_unit`,`parcel_type`,`parcel_insurance_value`,`parcel_service_used`,`campaign_id`,`service_name`,`carrier_name`) values (1,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,0,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier'),(2,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,0,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier'),(3,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,0,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier'),(4,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,5,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier'),(5,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,5,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier'),(6,'test123','0000-00-00','123 room, abc square, test city.','test234','0000-00-00','123 room, abc square, test city.',321,5,'acc, zzz, daf','0.50','kg','1,2','m','envelope',10,'1 day',5,'test','test carrier');

/*Table structure for table `questions` */

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT '0',
  `question` varchar(255) DEFAULT NULL,
  `question_type` enum('yes/no','rating','text','mcq','email','dropdown','tenstarrating') DEFAULT NULL,
  `mcq` varchar(1000) DEFAULT NULL,
  `is_mendatory` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `questions` */

insert into `questions` (`id`,`campaign_id`,`question`,`question_type`,`mcq`,`is_mendatory`) values (13,1,'dropdown','dropdown','[\"opt1\",\"opt2\"]',0),(14,1,'not mandatory email','email','',0),(15,1,'mandatory email','email','',1),(21,2,'test email','email','',1),(22,0,NULL,NULL,NULL,0),(31,4,'this is email','email','',0),(32,5,'five star rating','rating','',0),(33,5,'yes no','yes/no','',0),(34,5,'email','email','',0),(35,3,'five star rating','rating','',0),(36,3,'this is ten star rating','tenstarrating','',0),(37,3,'email mandatory','email','',0);

/* Procedure structure for procedure `getAllCampaignsByVendorId` */

drop procedure if exists `getAllCampaignsByVendorId`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllCampaignsByVendorId`( vid int )
BEGIN
	SELECT * FROM campaigns WHERE vendor_id=vid ORDER BY id DESC;
    END$$

DELIMITER ;

/* Procedure structure for procedure `getCampaignDetail` */

drop procedure if exists `getCampaignDetail`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCampaignDetail`(cid int)
BEGIN
	DECLARE total_response INT;
	DECLARE yes_del INT;
	DECLARE parcel_avg INT;
	DECLARE behavior_avg INT;
	
	SET total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	SET yes_del = (SELECT ROUND((SUM(on_time_delivery)/total_response)*100) FROM feedbacks WHERE campaign_id=cid AND on_time_delivery=1 GROUP BY campaign_id);
	SET parcel_avg = (SELECT ROUND((SUM(parcel_condition)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	SET behavior_avg = (SELECT ROUND((SUM(behavior)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,
	total_response,
	yes_del,
	parcel_avg,
	behavior_avg
	FROM campaigns c 
	WHERE c.id=cid;
	
	SELECT parcel_condition,ROUND(COUNT(parcel_condition)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND parcel_condition>0 GROUP BY parcel_condition ORDER BY parcel_condition DESC;
	
	SELECT behavior,ROUND(COUNT(behavior)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND behavior>0 GROUP BY behavior ORDER BY behavior DESC;
END$$

DELIMITER ;

/* Procedure structure for procedure `getCampaignDetailNew` */

drop procedure if exists `getCampaignDetailNew`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCampaignDetailNew`(cid INT)
BEGIN
	DECLARE total_response INT;
		
	SET total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,total_response FROM campaigns c WHERE c.id=cid;
	
	SELECT q.question,ROUND((SELECT COUNT(id) FROM answers WHERE question_id=q.id AND answer='Yes')/(SELECT COUNT(id) FROM answers WHERE question_id=q.id)*100) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='yes/no' GROUP BY q.id;
	
	SELECT q.*,round(SUM(a.answer)/COUNT(a.id))avg_rating FROM questions q LEFT JOIN answers a ON a.question_id=q.id
	WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY q.id;
	
	SELECT q.id,a.answer,ROUND(((COUNT(a.id)/(SELECT COUNT(id) FROM answers WHERE question_id=q.id))*100)) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY a.answer,a.question_id ORDER BY q.id ASC;
END$$

DELIMITER ;

/* Procedure structure for procedure `getCampaignFeedbacks` */

drop procedure if exists `getCampaignFeedbacks`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCampaignFeedbacks`(cid int, off int, lim int,parcel_id INT,uEmail VARCHAR(100))
BEGIN
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @parcel_id = parcel_id;
	SET @email = uEmail;
	
	set @q = concat('SELECT * FROM feedbacks where campaign_id=',@cid);
	IF(@parcel_id!=0) THEN
		SET @q = CONCAT(@q,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @q = CONCAT(@q," and customer_email='",@email,"'");
	END IF;
	set @q = concat(@q,' ORDER BY feedback_time DESC LIMIT ',@off,',',@lim);
	
	PREPARE stm FROM @q;
	EXECUTE stm;
	
    END$$

DELIMITER ;

/* Procedure structure for procedure `getCampaignFeedbacksApi` */

drop procedure if exists `getCampaignFeedbacksApi`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCampaignFeedbacksApi`(vendor_id INT, cid INT, lim INT, off INT, fromDate VARCHAR(100), toDate VARCHAR(100))
BEGIN
	SET @vendor_id = vendor_id;
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @toDate = toDate;
	SET @fromDate = fromDate;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks where vendor_id=',@vendor_id);
	
	IF(@cid!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and campaign_id=',@cid);
	END IF;
	IF(@fromDate!= '') THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) > ","'",@fromDate,"'");
	END IF;
	IF(@toDate!='')THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) < ","'",@toDate,"'");
	END IF;	
	
	SET @strQuery = CONCAT(@strQuery,' order by feedback_time desc limit ',@off,',',@lim);
		
	PREPARE stm FROM @strQuery;
	EXECUTE stm;
    END$$

DELIMITER ;

/* Procedure structure for procedure `getCampaignFeedbacksCountByCid` */

drop procedure if exists `getCampaignFeedbacksCountByCid`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCampaignFeedbacksCountByCid`(cid int,parcel_id int,uEmail varchar(100))
BEGIN
	SET @cid = cid;
	set @parcel_id = parcel_id;
	set @email = uEmail;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks WHERE campaign_id=',@cid);
	
	IF(@parcel_id!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @strQuery = CONCAT(@strQuery," and customer_email='",@email,"'");
	END IF;
	
	SET @strQuery = CONCAT(@strQuery,' ORDER BY feedback_time DESC');
	
	PREPARE stm FROM @strQuery;
	EXECUTE stm;		
    END$$

DELIMITER ;

/* Procedure structure for procedure `getFeedbacksToCsv` */

drop procedure if exists `getFeedbacksToCsv`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getFeedbacksToCsv`(cid int)
BEGIN
	SELECT id,feedback_time,parcel_id FROM feedbacks where campaign_id=cid;
    END$$

DELIMITER ;

/* Procedure structure for procedure `getUpiCodes` */

drop procedure if exists `getUpiCodes`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUpiCodes`(cid int, upicodes varchar(1000))
BEGIN
	SELECT GROUP_CONCAT(upi_code) as upi_code FROM parcels WHERE campaign_id=cid and FIND_IN_SET(upi_code, upicodes);
    END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;