-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 15, 2013 at 10:37 AM
-- Server version: 5.1.68-cll
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dapppli_feedpack`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getAllCampaignsByVendorId`( vid int )
BEGIN
	SELECT * FROM campaigns WHERE vendor_id=vid ORDER BY id DESC;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignDetail`(cid int)
BEGIN
	DECLARE total_response INT;
	DECLARE yes_del INT;
	DECLARE parcel_avg INT;
	DECLARE behavior_avg INT;
	
	set total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	set yes_del = (SELECT ROUND((SUM(on_time_delivery)/total_response)*100) FROM feedbacks WHERE campaign_id=cid AND on_time_delivery=1 GROUP BY campaign_id);
	set parcel_avg = (SELECT ROUND((SUM(parcel_condition)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	set behavior_avg = (SELECT ROUND((SUM(behavior)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,
	total_response,
	yes_del,
	parcel_avg,
	behavior_avg
	FROM campaigns c 
	WHERE c.id=cid;
	
	SELECT parcel_condition,ROUND(COUNT(parcel_condition)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND parcel_condition>0 GROUP BY parcel_condition ORDER BY parcel_condition DESC;
	
	SELECT behavior,ROUND(COUNT(behavior)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND behavior>0 GROUP BY behavior ORDER BY behavior DESC;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignDetailNew`(IN `cid` INT)
BEGIN
	DECLARE total_response INT;
		
	SET total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,total_response FROM campaigns c WHERE c.id=cid;
	
	SELECT q.question,ROUND((SELECT COUNT(id) FROM answers WHERE question_id=q.id AND answer='Yes')/(SELECT COUNT(id) FROM answers WHERE question_id=q.id)*100) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='yes/no' GROUP BY q.id;

	
	SELECT q.*,ROUND(SUM(a.answer)/COUNT(a.id))avg_rating FROM questions q LEFT JOIN answers a ON a.question_id=q.id
	WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY q.id;
	
	SELECT q.id,a.answer,ROUND(((COUNT(a.id)/(SELECT COUNT(id) FROM answers WHERE question_id=q.id))*100)) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY a.answer,a.question_id ORDER BY q.id ASC;

END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacks`(IN `cid` INT, IN `off` INT, IN `lim` INT, IN `parcel_id` INT, IN `uEmail` VARCHAR(100))
BEGIN
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @parcel_id = parcel_id;
	SET @email = uEmail;
	
	SET @q = CONCAT('SELECT * FROM feedbacks where campaign_id=',@cid);
	IF(@parcel_id!=0) THEN
		SET @q = CONCAT(@q,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @q = CONCAT(@q," and customer_email='",@email,"'");
	END IF;
	SET @q = CONCAT(@q,' ORDER BY feedback_time DESC LIMIT ',@off,',',@lim);
	
	PREPARE stm FROM @q;
	EXECUTE stm;
	
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacksApi`(vendor_id INT, cid INT, lim INT, off INT, fromDate VARCHAR(100), toDate VARCHAR(100))
BEGIN
	SET @vendor_id = vendor_id;
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @toDate = toDate;
	SET @fromDate = fromDate;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks where vendor_id=',@vendor_id);
	
	IF(@cid!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and campaign_id=',@cid);
	END IF;
	IF(@fromDate!= '') THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) > ","'",@fromDate,"'");
	END IF;
	IF(@toDate!='')THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) < ","'",@toDate,"'");
	END IF;	
	
	SET @strQuery = CONCAT(@strQuery,' order by feedback_time desc limit ',@off,',',@lim);
		
	PREPARE stm FROM @strQuery;
	EXECUTE stm;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacksCountByCid`(IN `cid` INT, IN `parcel_id` INT, IN `uEmail` VARCHAR(100))
BEGIN
	SET @cid = cid;
	SET @parcel_id = parcel_id;
	SET @email = uEmail;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks WHERE campaign_id=',@cid);
	
	IF(@parcel_id!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @strQuery = CONCAT(@strQuery," and customer_email='",@email,"'");
	END IF;
	
	SET @strQuery = CONCAT(@strQuery,' ORDER BY feedback_time DESC');
	
	PREPARE stm FROM @strQuery;
	EXECUTE stm;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getFeedbacksToCsv`(IN `cid` INT)
BEGIN
	SELECT id,feedback_time,parcel_id FROM feedbacks where campaign_id=cid;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getUpiCodes`(cid int, upicodes varchar(1000))
BEGIN
	SELECT GROUP_CONCAT(upi_code) as upi_code FROM parcels WHERE campaign_id=cid and FIND_IN_SET(upi_code, upicodes);
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `security_key` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email_address`, `security_key`) VALUES
(1, 'admin', 'admin123', 'tr.exphlux@gmail.com', '31232f297a57a5a743894a0e4a801fc3'),
(2, 'vendor', 'vendor', 'tr.ephlux@gmail.com', '7c3613dba5171cb6027c67835dd3b9d4'),
(3, 'matt', 'matt123', 'siraj@apppli.com', '21232f297a57a5a743894a0e4a801fc3'),
(4, 'p2g', 'p2g123', 'siraj@apppli.com', '1ed818bde5155cb6857955141f9a8771'),
(5, 'thesnugg', 'thesnugg123', 'siraj@apppli.com', '5de6a3a98f50b54310f22a1a554d6b2b'),
(6, 'apppli', 'apppli123', 'siraj@apppli.com', 'd1e35ccf6472c92220027b4eedf27fef');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT '0',
  `answer` varchar(500) DEFAULT NULL,
  `feedback_id` int(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `feedback_id`) VALUES
(30, 18, 'Yes', 8),
(29, 17, '5', 8),
(28, 8, 'Matthew.linnecar@feedpack.com', 7),
(27, 5, 'One', 7),
(26, 6, '5', 7),
(25, 7, 'Yes', 7),
(24, 8, '', 6),
(23, 5, 'Four', 6),
(22, 6, '4', 6),
(21, 7, 'No', 6),
(13, 7, 'Yes', 4),
(14, 6, '5', 4),
(15, 5, 'Four', 4),
(16, 8, 'Siraj@apppli.com', 4),
(31, 19, '8', 8),
(32, 17, '5', 9),
(33, 18, 'Yes', 9),
(34, 19, '8', 9),
(35, 17, '4', 10),
(36, 18, 'Yes', 10),
(37, 19, '10', 10),
(38, 17, '5', 11),
(39, 18, 'Yes', 11),
(40, 19, '4', 11),
(41, 20, 'option 2', 12),
(42, 22, '4', 13),
(43, 22, '5', 14),
(44, 20, 'option 3', 15),
(45, 23, '1', 16),
(46, 24, 'three ', 16),
(47, 23, '10', 17),
(48, 24, 'four', 17),
(49, 25, 'Yes', 18),
(50, 25, 'Yes', 19);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `recipent_name` varchar(255) DEFAULT NULL,
  `show_location_field` int(11) NOT NULL,
  `is_email_mandatory` int(11) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `no_of_packages` int(11) DEFAULT '0',
  `campaign_status` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `campaign_type` int(10) DEFAULT '2',
  `logo` varchar(255) DEFAULT NULL,
  `header_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `campaign_name`, `recipent_name`, `show_location_field`, `is_email_mandatory`, `creation_date`, `no_of_packages`, `campaign_status`, `vendor_id`, `campaign_type`, `logo`, `header_text`) VALUES
(5, 'Unlimited Scan Campaign demo', 'Apppli', 1, 0, '2013-04-01', 999, 'Active', 3, 2, '', 'Fill us in on your delivery'),
(12, 'Test from Apppli', 'Test Retailer', 1, 0, '2013-04-09', 8, 'Active', 5, 2, '6a430dbe418afd56f3d5e26cab0e8a04.png', 'The Snugg'),
(13, 'Test product campaign', 'The Snugg', 1, 0, '2013-04-09', 9, 'Active', 5, 1, 'a8eb63a953e7d0755ef225549f74707d.png', 'Product campaign'),
(14, 'testting dropdown ', 'apppli ', 1, 0, '2013-04-12', 20, 'Active', 5, 1, 'e0ec23a713e538a056244e8c2fbfd7d9.png', 'some dropdown '),
(15, 'Gregg Test', 'Parcel2Go.com', 0, 0, '2013-04-12', 1000, 'Active', 4, 2, '', 'Fill us in on your delivery'),
(16, 'Test with Matt for TheSnugg', 'Apppli ', 1, 0, '2013-04-15', 10, 'Active', 5, 1, '', 'This is custom header'),
(17, 'with logo', 'apppli ', 1, 0, '2013-04-15', 3, 'Active', 5, 1, 'e0cc8d1f195ada1112499816255fa5c8.png', 'some header text');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_types`
--

CREATE TABLE IF NOT EXISTS `campaign_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_type` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '1',
  `parcel_required` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `campaign_types`
--

INSERT INTO `campaign_types` (`id`, `campaign_type`, `status`, `parcel_required`) VALUES
(1, 'Product', 1, 0),
(2, 'Delivery', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `on_time_delivery` tinyint(1) DEFAULT NULL COMMENT '1=yes, 0= no',
  `parcel_condition` int(11) DEFAULT '0',
  `behavior` int(11) DEFAULT '0',
  `customer_email` varchar(255) DEFAULT NULL,
  `feedback_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `comment` text NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `parcel_id` int(11) NOT NULL DEFAULT '0',
  `user_location` varchar(500) DEFAULT NULL,
  `p2g_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `on_time_delivery`, `parcel_condition`, `behavior`, `customer_email`, `feedback_time`, `campaign_id`, `comment`, `vendor_id`, `parcel_id`, `user_location`, `p2g_code`) VALUES
(4, NULL, 0, 0, NULL, '2013-04-02 14:24:40', 5, '', 3, -1, '51.51643595255775,-0.14492428053200132', '0'),
(5, NULL, 0, 0, NULL, '2013-04-02 15:30:02', 1, '', 0, 0, '0', '0'),
(6, NULL, 0, 0, NULL, '2013-04-02 18:30:02', 5, '', 3, -1, '24.86231230660569,67.07259762975353', '0'),
(7, NULL, 0, 0, NULL, '2013-04-03 10:00:02', 5, '', 3, -1, '51.52747540605739,-0.13357004348369236', '0'),
(8, NULL, 0, 0, NULL, '2013-04-09 20:27:21', 13, '', 5, 0, '', ''),
(9, NULL, 0, 0, NULL, '2013-04-12 10:23:44', 13, '', 5, 0, '', ''),
(10, NULL, 0, 0, NULL, '2013-04-12 10:24:50', 13, '', 5, 0, '', ''),
(11, NULL, 0, 0, NULL, '2013-04-12 10:25:46', 13, '', 5, 0, '51.51501252062006,-0.14422609828692562', ''),
(12, NULL, 0, 0, NULL, '2013-04-12 10:27:55', 14, '', 5, 0, '51.51564598707609,-0.1444348224606358', ''),
(13, NULL, 0, 0, NULL, '2013-04-12 11:30:01', 15, '', 4, 1, '0', '564654'),
(14, NULL, 0, 0, NULL, '2013-04-12 12:30:02', 15, '', 4, 123, '0', '564654'),
(15, NULL, 0, 0, NULL, '2013-04-15 10:33:10', 14, '', 5, 0, '51.50876368148456,-0.14959587327459917', ''),
(16, NULL, 0, 0, NULL, '2013-04-15 10:39:51', 16, '', 5, 0, '51.51659043647556,-0.12510037202150026', ''),
(17, NULL, 0, 0, NULL, '2013-04-15 10:39:51', 16, '', 5, 0, '51.50880199563875,-0.1496473617846775', ''),
(18, NULL, 0, 0, NULL, '2013-04-15 11:00:02', 17, '', 5, 0, '51.51660046629868,-0.12558061417234423', ''),
(19, NULL, 0, 0, NULL, '2013-04-15 11:00:02', 17, '', 5, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `parcels`
--

CREATE TABLE IF NOT EXISTS `parcels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sender_fullname` varchar(255) DEFAULT NULL,
  `sender_dob` date DEFAULT NULL,
  `sender_address` text,
  `receiver_fullname` varchar(255) DEFAULT NULL,
  `receiver_dob` date DEFAULT NULL,
  `receiver_address` text,
  `p2g_code` int(11) DEFAULT NULL,
  `upi_code` bigint(20) unsigned NOT NULL,
  `destination` text,
  `parcel_weight` decimal(2,2) DEFAULT NULL,
  `parcel_weight_unit` varchar(100) DEFAULT NULL,
  `parcel_dimensions` varchar(100) DEFAULT NULL,
  `parcel_dimensions_unit` varchar(100) DEFAULT NULL,
  `parcel_type` varchar(255) DEFAULT NULL,
  `parcel_insurance_value` int(10) DEFAULT NULL,
  `parcel_service_used` varchar(255) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `carrier_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`upi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT '0',
  `question` varchar(255) DEFAULT NULL,
  `question_type` enum('yes/no','rating','text','mcq','email','dropdown','tenstarrating') DEFAULT NULL,
  `mcq` varchar(1000) DEFAULT NULL,
  `is_mendatory` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `campaign_id`, `question`, `question_type`, `mcq`, `is_mendatory`) VALUES
(7, 5, 'Will you use our service again?', 'yes/no', '', 0),
(6, 5, 'How did you find our service today?', 'rating', '', 0),
(5, 5, 'Please select an option', 'mcq', '["One","Two","Three","Four"]', 0),
(8, 5, 'Please provide us your email address?', 'email', '', 0),
(15, 12, 'Here you have to choose from 10 stars! ', 'tenstarrating', '', 0),
(16, 12, 'Question for drop-down choices', 'dropdown', '["choice 1","choice 2","choice 3",""]', 0),
(14, 12, 'Rate the product', 'rating', '', 0),
(13, 12, 'Please select the option from below choices', 'mcq', '["Option 2","Option 2","Option 3","Option 4"]', 0),
(17, 13, 'choose from 5 stars', 'rating', '', 0),
(18, 13, 'Do you like us? ', 'yes/no', '', 0),
(19, 13, '10 being best, please rate our service. ', 'tenstarrating', '', 0),
(20, 14, 'a drop down question', 'dropdown', '["option1 ","option 2","option 3"]', 0),
(22, 15, 'Please rate our service.', 'rating', '', 0),
(23, 16, 'Rate us from 1 to 10 ', 'tenstarrating', '', 0),
(24, 16, 'This is a drop down, please select something ', 'dropdown', '["one ","two","three ","four"]', 0),
(25, 17, 'test ', 'yes/no', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
