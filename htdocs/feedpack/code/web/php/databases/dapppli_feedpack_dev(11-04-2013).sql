-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 11, 2013 at 10:39 AM
-- Server version: 5.1.68-cll
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dapppli_feedpack_dev`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getAllCampaignsByVendorId`( vid int )
BEGIN
	SELECT * FROM campaigns WHERE vendor_id=vid ORDER BY id DESC;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignDetail`(cid int)
BEGIN
	DECLARE total_response INT;
	DECLARE yes_del INT;
	DECLARE parcel_avg INT;
	DECLARE behavior_avg INT;
	
	set total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	set yes_del = (SELECT ROUND((SUM(on_time_delivery)/total_response)*100) FROM feedbacks WHERE campaign_id=cid AND on_time_delivery=1 GROUP BY campaign_id);
	set parcel_avg = (SELECT ROUND((SUM(parcel_condition)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	set behavior_avg = (SELECT ROUND((SUM(behavior)/total_response)) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,
	total_response,
	yes_del,
	parcel_avg,
	behavior_avg
	FROM campaigns c 
	WHERE c.id=cid;
	
	SELECT parcel_condition,ROUND(COUNT(parcel_condition)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND parcel_condition>0 GROUP BY parcel_condition ORDER BY parcel_condition DESC;
	
	SELECT behavior,ROUND(COUNT(behavior)/total_response*100)tot FROM feedbacks WHERE campaign_id=cid AND behavior>0 GROUP BY behavior ORDER BY behavior DESC;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignDetailNew`(IN `cid` INT)
BEGIN
	DECLARE total_response INT;
		
	SET total_response = (SELECT COUNT(id) FROM feedbacks WHERE campaign_id=cid GROUP BY campaign_id);
	
	SELECT c.*,total_response FROM campaigns c WHERE c.id=cid;
	
	SELECT q.question,ROUND((SELECT COUNT(id) FROM answers WHERE question_id=q.id AND answer='Yes')/(SELECT COUNT(id) FROM answers WHERE question_id=q.id)*100) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='yes/no' GROUP BY q.id;

	
	SELECT q.*,ROUND(SUM(a.answer)/COUNT(a.id))avg_rating FROM questions q LEFT JOIN answers a ON a.question_id=q.id
	WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY q.id;
	
	SELECT q.id,a.answer,ROUND(((COUNT(a.id)/(SELECT COUNT(id) FROM answers WHERE question_id=q.id))*100)) AS yes_answers FROM questions q LEFT JOIN answers a ON a.question_id=q.id WHERE q.campaign_id=cid AND q.question_type='rating' AND a.answer>0 GROUP BY a.answer,a.question_id ORDER BY q.id ASC;


END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacks`(IN `cid` INT, IN `off` INT, IN `lim` INT, IN `parcel_id` INT, IN `uEmail` VARCHAR(100))
BEGIN
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @parcel_id = parcel_id;
	SET @email = uEmail;
	
	SET @q = CONCAT('SELECT * FROM feedbacks where campaign_id=',@cid);
	IF(@parcel_id!=0) THEN
		SET @q = CONCAT(@q,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @q = CONCAT(@q," and customer_email='",@email,"'");
	END IF;
	SET @q = CONCAT(@q,' ORDER BY feedback_time DESC LIMIT ',@off,',',@lim);
	
	PREPARE stm FROM @q;
	EXECUTE stm;
	
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacksApi`(vendor_id INT, cid INT, lim INT, off INT, fromDate VARCHAR(100), toDate VARCHAR(100))
BEGIN
	SET @vendor_id = vendor_id;
	SET @cid = cid;
	SET @off = off;
	SET @lim = lim;
	SET @toDate = toDate;
	SET @fromDate = fromDate;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks where vendor_id=',@vendor_id);
	
	IF(@cid!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and campaign_id=',@cid);
	END IF;
	IF(@fromDate!= '') THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) > ","'",@fromDate,"'");
	END IF;
	IF(@toDate!='')THEN
		SET @strQuery = CONCAT(@strQuery," AND DATE(feedback_time) < ","'",@toDate,"'");
	END IF;	
	
	SET @strQuery = CONCAT(@strQuery,' order by feedback_time desc limit ',@off,',',@lim);
		
	PREPARE stm FROM @strQuery;
	EXECUTE stm;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getCampaignFeedbacksCountByCid`(IN `cid` INT, IN `parcel_id` INT, IN `uEmail` VARCHAR(100))
BEGIN
	SET @cid = cid;
	SET @parcel_id = parcel_id;
	SET @email = uEmail;
	
	SET @strQuery = CONCAT('SELECT * FROM feedbacks WHERE campaign_id=',@cid);
	
	IF(@parcel_id!=0) THEN
		SET @strQuery = CONCAT(@strQuery,' and parcel_id=',@parcel_id);
	END IF;
	
	IF(@email!='') THEN
		SET @strQuery = CONCAT(@strQuery," and customer_email='",@email,"'");
	END IF;
	
	SET @strQuery = CONCAT(@strQuery,' ORDER BY feedback_time DESC');
	
	PREPARE stm FROM @strQuery;
	EXECUTE stm;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getDeleteFeedback`(IN `fid` INT)
BEGIN
delete  from feedbacks where id =fid;
delete from answers where feedback_id =fid;
END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getFeedbacksToCsv`(IN `cid` INT)
BEGIN
	SELECT id,feedback_time,parcel_id FROM feedbacks where campaign_id=cid;
    END$$

CREATE DEFINER=`dapppli`@`localhost` PROCEDURE `getUpiCodes`(cid int, upicodes varchar(1000))
BEGIN
	SELECT GROUP_CONCAT(upi_code) as upi_code FROM parcels WHERE campaign_id=cid and FIND_IN_SET(upi_code, upicodes);
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `security_key` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email_address`, `security_key`) VALUES
(1, 'admin', 'admin123', 'tr.exphlux@gmail.com', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'vendor', 'vendor', 'tr.ephlux@gmail.com', '7c3613dba5171cb6027c67835dd3b9d4'),
(3, 'matt', 'matt123', 'siraj@apppli.com', 'ce86d7d02a229acfaca4b63f01a1171b'),
(4, 'p2g', 'p2g123', 'siraj@apppli.com', '1ed818bde5155cb6857955141f9a8771'),
(5, 'thesnugg', 'thesnugg123', 'siraj@apppli.com', '5de6a3a98f50b54310f22a1a554d6b2b'),
(6, 'apppli', 'apppli123', 'siraj@apppli.com', 'd1e35ccf6472c92220027b4eedf27fef');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT '0',
  `answer` varchar(500) DEFAULT NULL,
  `feedback_id` int(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `answer`, `feedback_id`) VALUES
(3, 22, 'No', 3);

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(255) DEFAULT NULL,
  `recipent_name` varchar(255) DEFAULT NULL,
  `show_location_field` int(11) NOT NULL,
  `is_email_mandatory` int(11) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `no_of_packages` int(11) DEFAULT '0',
  `campaign_status` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `campaign_type` int(10) DEFAULT '2',
  `logo` varchar(255) DEFAULT NULL,
  `header_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `campaign_name`, `recipent_name`, `show_location_field`, `is_email_mandatory`, `creation_date`, `no_of_packages`, `campaign_status`, `vendor_id`, `campaign_type`, `logo`, `header_text`) VALUES
(1, 'Stupid Campaign', 'Test Retailer', 1, 0, '2013-03-28', 999999, 'Active', 2, 2, '', 'This is the most stupid feedback form you will ever see.'),
(2, 'Image Campaign', 'asdf', 1, 0, '2013-03-28', 99999, 'Active', 2, 2, '', 'fads fsad fsadf sadf adsf dsaf sdaf dsaf adsf sdaf sadf sadf sadf'),
(3, 'Image Campaign 2', 'sdafsadf', 1, 0, '2013-03-28', 1423423, 'Active', 2, 2, '', 'Fill us in on your delivery'),
(4, 'asdfsadf', 'asdfasdf', 1, 0, '2013-03-28', 23232, 'Draft', 2, 2, 'b5d4536694ec90d207a092d4b39095f7.jpg', 'Fill us in on your delivery'),
(5, 'Image Cmapaign 365', 'asdfasdf', 0, 0, '2013-03-28', 23423, 'Draft', 2, 2, '2edb95bd182ad80974b0579ab7fce984.jpg', 'sdf'),
(6, 'inspecial kampute', 'sadfasdf', 1, 0, '2013-03-28', 223, 'Active', 2, 2, '', 'Fill us in on your delivery'),
(7, 'SHapaka Campaign', 'asdfasdf', 0, 0, '2013-03-28', 2323, 'Draft', 2, 2, '7357ec5c33495db208418770c2c80de7.jpg', 'Fill us in on your delivery'),
(8, 'atest', 'tahir', 0, 0, '2013-03-29', 10, 'Draft', 2, 2, '2da2178d4e4a30b26bd73d27c7d81ad3.jpeg', 'testing header text'),
(9, '22', '22', 1, 0, '2013-03-29', 22, 'Draft', 2, 2, 'd5564fee2d29f3d4d37266957cb9b4a5.jpg', '22'),
(10, 'Supre Special Sporadiacal Campainal', 'sdfsa', 1, 0, '2013-04-01', 234324234, 'Active', 2, 2, 'eaee24c0660a91b77d0ddb928aca28d3.jpg', 'sadfsdf'),
(11, 'qq', '1', 0, 0, '2013-04-02', 1, 'Active', 2, 2, 'aaa5055b15ddacc7eab06a8c10c22240.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_types`
--

CREATE TABLE IF NOT EXISTS `campaign_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_type` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `campaign_types`
--

INSERT INTO `campaign_types` (`id`, `campaign_type`, `status`) VALUES
(1, 'Product', 1),
(2, 'Delivery', 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `on_time_delivery` tinyint(1) DEFAULT NULL COMMENT '1=yes, 0= no',
  `parcel_condition` int(11) DEFAULT '0',
  `behavior` int(11) DEFAULT '0',
  `customer_email` varchar(255) DEFAULT NULL,
  `feedback_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `comment` text NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT '0',
  `parcel_id` int(11) NOT NULL DEFAULT '0',
  `user_location` varchar(500) DEFAULT NULL,
  `p2g_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `on_time_delivery`, `parcel_condition`, `behavior`, `customer_email`, `feedback_time`, `campaign_id`, `comment`, `vendor_id`, `parcel_id`, `user_location`, `p2g_code`) VALUES
(3, NULL, 0, 0, NULL, '2013-04-03 11:39:11', 11, '', 2, 5, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `parcels`
--

CREATE TABLE IF NOT EXISTS `parcels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sender_fullname` varchar(255) DEFAULT NULL,
  `sender_dob` date DEFAULT NULL,
  `sender_address` text,
  `receiver_fullname` varchar(255) DEFAULT NULL,
  `receiver_dob` date DEFAULT NULL,
  `receiver_address` text,
  `p2g_code` int(11) DEFAULT NULL,
  `upi_code` bigint(20) unsigned NOT NULL,
  `destination` text,
  `parcel_weight` decimal(2,2) DEFAULT NULL,
  `parcel_weight_unit` varchar(100) DEFAULT NULL,
  `parcel_dimensions` varchar(100) DEFAULT NULL,
  `parcel_dimensions_unit` varchar(100) DEFAULT NULL,
  `parcel_type` varchar(255) DEFAULT NULL,
  `parcel_insurance_value` int(10) DEFAULT NULL,
  `parcel_service_used` varchar(255) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `carrier_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`upi_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT '0',
  `question` varchar(255) DEFAULT NULL,
  `question_type` enum('yes/no','rating','text','mcq','email','dropdown','tenstarrating') DEFAULT NULL,
  `mcq` varchar(1000) DEFAULT NULL,
  `is_mendatory` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `campaign_id`, `question`, `question_type`, `mcq`, `is_mendatory`) VALUES
(6, 1, 'to be or not to be', 'dropdown', '["to be","not to be","dont know","none of above"]', 0),
(5, 1, 'yes or not', 'yes/no', '', 0),
(4, 1, 'This is a 10 star rating control', 'tenstarrating', '', 0),
(7, 2, 'asdf', 'rating', '', 0),
(8, 3, 'asdfasdf', 'rating', '', 0),
(11, 4, 'asdf', 'rating', '', 0),
(13, 5, 'asdf', 'rating', '', 0),
(14, 6, 'sadf', 'rating', '', 0),
(16, 7, 'asdf', 'rating', '', 0),
(18, 8, 'adfaf', 'email', '', 0),
(19, 9, '22', 'text', '', 0),
(20, 10, 'sadf', 'rating', '', 0),
(22, 11, 'check yes no', 'yes/no', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
