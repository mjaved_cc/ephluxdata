<?php
$multi = array('developerKey'=>'21232f297a57a5a743894a0e4a801fc3',
				'campaign_ids'=>array(2,3,9), 
				'method'=>'campaignsSummary'
			  );

// encode data into json and store into jsonFile array element. it is neccessory for each requrest to be contained in jsonFile element.
$bodyData = array (
  'jsonFile' => json_encode($multi)
);

// url encode your data 
$bodyStr = http_build_query($bodyData);

$url = 'http://demo.apppli.com/feedpack-dev/api';

// send request using curl.
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/x-www-form-urlencoded',
  'Content-Length: '.strlen($bodyStr))
);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyStr);

$result = curl_exec($ch);

// curl request end

//print result.
echo '<pre>';
print_r($result);
echo '</pre>';

// here is response for successfull request

//{"response":{"status":"success","description":"","data":[{"id":"88","on_time_delivery":"1","parcel_condition":"3","behavior":"4","customer_email":"","feedback_time":"2013-02-22 14:49:31","compaign_id":"6","comment":"adaf","vender_id":"2","parcel_id":"13"}]}}			
?>