<?php
$multi = array('developerKey'=>'7c3613dba5171cb6027c67835dd3b9d4','campaign_id'=>1, 'method'=>'addParcel');

$arr = array('sender_fullname'=>'test123',
			  //'sender_lname'=>'test456',
			  'sender_dob'=>'',
			  'sender_address'=>'123 room, abc square, test city.',
			  'receiver_fullname'=>'test234',
			  //'receiver_lname'=>'test345',
			  'receiver_dob'=>'',
			  'receiver_address'=>'123 room, abc square, test city.',
			  'p2g_code'=>'321',
			  'upi_code'=>'',
			  'destination'=>'acc, zzz, daf',
			  'parcel_weight'=>'.5',
			  'parcel_weight_unit'=>'kg',
			  'parcel_dimensions'=>'1,2',
			  'parcel_dimensions_unit'=>'m',
			  'parcel_type'=>'envelope',
			  'parcel_insurance_value'=>'10',
			  'parcel_service_used'=>'1 day',
			  'service_name'=>'test',
			  'carrier_name'=>'test carrier'
			);


// loop to make 10 entries			
for($i=300; $i<=302; $i++)
{
	//$arr['upi_code'] = $i;
	$multi['parcels'][] = $arr;	
}

//echo json_encode($multi);exit;
// encode data into json and store into jsonFile array element. jsonFile is neccessory.
$bodyData = array (
  'jsonFile' => json_encode($multi)
);


// url encode your data 
$bodyStr = http_build_query($bodyData);


$url = 'http://localhost/feedpack-new/api';

// send request using curl.
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/x-www-form-urlencoded',
  'Content-Length: '.strlen($bodyStr))
);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyStr);

$result = curl_exec($ch);
// curl request end


// print result.
echo '<pre>';
print_r($result);
echo '</pre>';		

//	sample response from request
//{"response":{"status":"success","description":"Parcel data saved."}}
?>