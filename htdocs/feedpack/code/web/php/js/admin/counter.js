var characterlimit = 100;
 
  // select the textarea
  $( "textarea" )
    // add an empty span after our textarea and
    // hide it (this is where we'll show the number of characters remaining)
    .after( "<span></span>" ).next().hide().end()
 
    // fire an event each time a key is pressed inside the textarea
    .keypress( function( e ){
      // get current character count
      var current = $( this ).val().length;
 
      // if current character count is greater than or equal to the character limit
      if( current >= characterlimit ){
        // prevent further characters from being entered apart from the
        // Delete and Backspace keys
        if( e.which != 0 && e.which != 8 ) e.preventDefault();
      }
 
      // add the remaining character count to our span
      $( this ).next().show().text( characterlimit - current );
    }
  );