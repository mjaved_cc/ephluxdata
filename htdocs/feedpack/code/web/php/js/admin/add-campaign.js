
function addQuestion(){
    $('#questionType').show();
}

function loginF()
{
    //if($('#questionGrid').is(':visible') ) {
    if(document.getElementById('questionGrid').style.display=='none'){
        $("#questionError").html("Please add atleast one question.");

        $('#questionError').show();
        return false;
    
    }
    else {
        $('#questionError').hide();
        jQuery('#form1').submit();   
    }
    

}


jQuery(document).keyup(function(e){
	
    if(e.keyCode == 13)
    {
        e.stopPropagation();
        e.preventDefault(); 
        return false;
    }
	
});

function cancelQuestion()
{
    $('#question_type').val('');
    $('#questionFrom').html('');		
}

function SaveQuestionMulti()
{
    $('#questionGrid').show();
    $('#questionFrom :input').each(function(){
        if($(this).val()=="")
        {
            $(this).after('<label class="errorN">This field is required</label>');
            return false;
        }
        else
        {
            $(this).next('label').remove();	
        }	
    });
    var id = $('#questionGrid tr').length;
    var hiddenData = '<input type="hidden" name="questions['+id+'][name]" value="'+$('#question').val()+'" />';
    hiddenData+= '<input type="hidden" name="questions['+id+'][type]" value="multi" />';
	
    for(var i=1; i<5; i++){
        hiddenData+= '<input type="hidden" name="questions['+id+'][mcq][]" value="'+$('#option'+i).val()+'" />';
    }
	
    $('#questionGrid').append('<tr id="question'+id+'"><td width="870">'+$('#question').val()+'</td><td>'+hiddenData+'<a href="#_" onclick="remoevQuestion(\'question'+id+'\')">Remove</a></td></tr>');
	
    cancelQuestion();
}

function removeHidden(qid) {
    var qname = qid+"name";
    var qtype = qid+"type";
    $('#'+qname).remove();
    $('#'+qtype).remove();

       
    
}


function SaveQuestion()
{
    $('#questionGrid').show();
    $('#questionFrom input[type="text"], textarea').each(function(){
        if($(this).val()=="")
        {
            $(this).after('<label class="errorN">This field is required</label>');
            event.preventDefault();
        }
        else
        {
            $(this).next('label').remove();	
        }	
    });
    var id = $('#questionGrid tr').length;
    var hiddenData = '<input type="hidden" name="questions['+id+'][name]" value="'+$('#question').val()+'" />';
    hiddenData+= '<input type="hidden" name="questions['+id+'][type]" value="'+$('#question_type').val()+'" />';
	
    if($('#question_type').val()=="mcq")
    {
        for(var i=1; i<5; i++){
            hiddenData+= '<input type="hidden" name="questions['+id+'][mcq][]" value="'+$('#option'+i).val()+'" />';
        }
    }
    
    if($('#question_type').val()=="dropdown")
    {
        $('input[name^=drpOption]').each(function(){
            var i = this.id ;
         
            hiddenData+= '<input type="hidden" name="questions['+id+'][dropdown][]" value="'+$('#'+i).val()+'" />';     
        });

     
        
    }
	
    if($('#question_type').val()=="email") {
        if($('#is_mendatory').is(':checked'))
        {
            mendatory = 1;	
        }
        else {
            mendatory = 0;
        }
        hiddenData+= '<input type="hidden" name="questions['+id+'][is_mandatory]" value="'+mendatory+'" />';	
    }
	
    $('#questionGrid').append('<tr id="question'+id+'"><td width="870" class="td-r">'+$('#question').val()+'</td><td class="td-r">'+hiddenData+'<a href="#_" onclick="remoevQuestion(\'question'+id+'\')">Remove</a></td></tr>');
	
    cancelQuestion();
}

function remoevQuestion(id)
{
    $('#'+id).remove();	
    if($('#questionGrid tr').length==1)
    {
        $('#questionGrid').hide();	
    }
}

function removeDrpDiv(id)
{
    $('#drpDiv'+id).remove();	
 
}




function createFrom(type)
{
    var html='';
	
    html+='<div class="input_data" id ="textboxgroup">';
    html+='<label>Question:</label>';
    /*if(type=="text")
	{
		html+='<textarea class="input-xlarge" name="question" id="question"></textarea>';
	}
	else {*/
    html+='<div class="input_data"><input type="text" class="input-xlarge" name="question" id="question" /></div>';
    if(type=="email")
    {
        html+='<input type="checkbox" name="is_mendatory_field" id="is_mendatory" /> is email mendatory?';	
    }
		
    //}
    html+='</div>';
	
    if(type=="mcq")
    {
        for(var i=1; i<5;i++)
        {
            html+='<div class="input_data">';
            html+='<label>option'+i+':</label>';
            html+='<input type="text" class="input-xlarge" name="option'+i+'" id="option'+i+'" />';
            html+='</div>';
        }
    }
    if(type=="dropdown")
    {
        var i=1;
        html+='<div id ="textboxgroup">';
        html+='<div class="input_data" id="drpDiv'+i+'">';
        html+='<label>option:</label>';
        html+='<input type="text" class="input-xlarge" name="drpOption'+i+'" id="drpOption'+i+'" /><a class="addmore add_icon" style="cursor: pointer;"> </a>';
        html+='</div>';
        html+='</div>';
		
    }
		
    html+='<div class="input_data">';
    html+='<a href="#_" class="btn_light_green btn-small" onClick="SaveQuestion()" style="margin-right: 10px;">Save</a>';
    html+='<a href="#_" class="btn_light_green btn-small" onClick="cancelQuestion()">Cancel</a>';
    html+='</div>';
	
    $('#questionFrom').html(html);
}

jQuery().ready(function() {
    var counter = 2 ;
    $(document).on("click", ".addmore", function() {
        var html="" ;
        counter ;
        var elVal='';
        var elVal= $('#drpOption1').val();
        $('#drpOption1').val('');
          
        
        html+='<div class="input_data" id="drpDiv'+counter+'" >';
        html+='<label>option:</label>';
        html+='<input type="text" class="input-xlarge" name="drpOption'+counter+'" id="drpOption'+counter+'" value="'+elVal+'" /><span onClick="removeDrpDiv('+counter+')" class="sub_icon" > </span>';
        html+='</div>';
        $('#textboxgroup').append(html);

    
        // var lastEl = $('input[name^=drpOption]').eq($('input[name^=drpOption]').length-1);
        
       
        
        counter++;
   
    });
  

     
     
    $('#question_type').change(function(){
        if($(this).val()!="")
        {
            createFrom($(this).val());
        }
    });
    $.validator.addMethod('filesize', function(value, element, param) {
        // param = size (en bytes) 
        // element = element to validate (<input>)
        // value = value of the element (file name)
        return this.optional(element) || (element.files[0].size <= param) 
    });
    $.validator.addMethod('imgsize', function(value, element, param) {
        // param = size (en bytes) 
        // element = element to validate (<input>)
        // value = value of the element (file name)
     
        if($("#chkImg").val()==1)
           { 
          return 1;
           }else
           {
            return 0;    
           }    
    });


        
    jQuery("#form1").validate({
        rules: {
            campaign_name: {
                required: true
            },
            recipent_name: {
                required: true
            },
            no_of_packages: {
                required: true,
                number:true
            },
            logo: {
                accept: "png|jpe?g",
                filesize: 2097152 /*,
                imgsize:1 */

                
            }
            
        },
        messages: {
            compaign_name: {
                required: "This field required"
            },
            recipent_name: {
                required: "This field required"
            },
            no_of_packages: {
                required: "This field required",
                number:"Only digits allowed"
            },
            logo:{
                accept: "Only  jpeg ,jpg and png allowed",
                filesize : "Only 1MB size of image is allowed"
                /*imgsize : "Only 1MB size of image is allowed ,height should be 90px ,width should be  310px"*/

                
            }
        },
        highlight: function(label) {
            jQuery(label).next('.error').css('color','red');
        },
        errorPlacement: function(error, element){
            error.appendTo(element.parent());
        }
        

    });
   /* $("#logo").after("<input type='hidden' id='chkImg' value='1'>");
    var url = window.URL || window.webkitURL;
    $("#logo").change(function(e) {
        if( this.disabled ){
            //alert('Your browser does not support File upload.');
        }else{
            var chosen = this.files[0];
            var image = new Image();
            image.onload = function() {
               // alert('Width:'+this.width +' Height:'+ this.height+' '+ Math.round(chosen.size/1024)+'KB');
                var chkImg = 1;
                if(this.width>310) {              
                    chkImg =0;   
                }  
                if(this.height>90){
                    chkImg =0;   
                }
                $("#chkImg").val(chkImg);
              
            };
            image.onerror = function() {
                //alert('Not a valid file type: '+ chosen.type);
            };
            image.src = url.createObjectURL(chosen);                    
        }
        $("#form1").valid();
    }); */
	



});	
