function openOneSlot(id) {
    
       
	SpinningWheel.addSlot({1: '<img src="images/star1.png">', 2: '<img src="images/star2.png">', 3: '<img src="images/star3.png">', 4: '<img src="images/star4.png">', 5: '<img src="images/star5.png">', 6: '<img src="images/star6.png">', 7: '<img src="images/star7.png">', 8: '<img src="images/star8.png">', 9: '<img src="images/star9.png">', 10: '<img src="images/star10.png">'});
	
	SpinningWheel.setCancelAction(cancel,id);
	SpinningWheel.setDoneAction(done,id);
	
	SpinningWheel.open();
}

function done() {
        var elId = this.id;
	var results = SpinningWheel.getSelectedValues();
	document.getElementById(elId).value = results.keys.join(', ') ;
}

function cancel() {
       var elId = this.id;
	document.getElementById(elId).value = '';
}


window.addEventListener('load', function(){ setTimeout(function(){ window.scrollTo(0,0); }, 100); }, true);
