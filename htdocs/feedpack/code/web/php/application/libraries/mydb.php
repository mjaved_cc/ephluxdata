<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// We need some constants to clear out some things also code wise
define('FETCH_ARRAY',1);
define('FETCH_ASSOC',2);
define('FETCH_OBJECT',3);
define('MYSQL_ERROR_PARAMETER',1);
define('MYSQL_ERROR_CONNECT',2);
define('MYSQL_ERROR_SELECT',3);
define('MYSQL_ERROR_SQL',4);

class Mydb
{
    private $CI, $mysqli, $result;
    
    /**
      * The constructor
    */
    public function __construct() 
    {
        $this->CI = & get_instance();
        $this->mysqli = $this->CI->db->conn_id;
        //var_dump($this->mysqli);
    }
    
    // run query
    public function Query($query)
    {
		
		$results = array();
		if ($this->mysqli->multi_query($query)) {
			do {
				// Create the records array
				$records = array();
		 
				// Lets work with the first result set
				if ($result = $this->mysqli->use_result()) {
					// Loop the first result set, reading the records into an array
					while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
						$records[] = $row;
					}
		 
					// Close the record set
					
					$result->close();
				}
		 
				// Add this record set into the results array
				if(!empty($records)){
					
					//$results[] = $records;
				}array_push($results,$records);
			} while ($this->mysqli->next_result());
			//$this->mysqli->free_result();
			//$this->mysqli->close();
			return $results;
		}
		 
    }	
}