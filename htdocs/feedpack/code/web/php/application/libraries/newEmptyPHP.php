<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Pagination Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Pagination
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 */
class Mymemcache {
    // The page we are linking to

    /**
     * Constructor
     *
     * @access	public
     * @param	array	initialization parameters
     */
    public function __construct() {
        $CI = & get_instance();
        $this->memHost = $CI->config->item('mymemcache_localhost') or die("could not connect to memcache host config varibale is not defined");
        $this->memPort = $CI->config->item('mymemcache_port') or die("could not connect to memcache host config varibale is not defined");
        $this->memDuration = $CI->config->item('mymemcache_duration') or die("could not connect to memcache host config varibale is not defined");
        log_message('debug', "Memcache Class Initialized");
    }

    public function memConnect() {

        $memcache = new Memcache;
        $memcache->connect($this->memHost, $this->memPort)
                or die("Could not connect to memcache server");

        return $memcache;
    }

    public function getMaxId() {
        $CI = & get_instance();
        $maxid = 1;
        $row = $CI->db->query('SELECT MAX(id) AS `maxid` FROM `feedbacks`')->row();
        if ($row) {
            $maxid = $row->maxid;
        }
        return $maxid;
    }

    public function addFeedBackId($feedbackId, $answers) {
      
        foreach ($answers as $a => $value) {
            $ans[$a]['question_id'] =$value['question_id'];
            $ans[$a]['answer'] = $value['answer'];
            $ans[$a]['feedback_id'] = $feedbackId;
        }
        return $ans;
    }

    public function dumpMem() {
        $memcache = $this->memConnect();
        $feedback = $memcache->get('feedback');
        //echo "<pre>"; print_r($feedback);
        if (!empty($feedback)) {
            $feedbackId = $this->getMaxId();
            $c = 0;
            foreach ($feedback as $value) {
                $feedbackId++;
                $data[$c] = json_decode($value, true);
                echo "<pre>";
                print_r($data[$c]);

                if (array_key_exists("answers", $data[$c])) {
                    echo "found";
                    $answers[$c] = $data[$c]['answers'];
                    $answers[$c] = $this->addFeedBackId($feedbackId, $answers[$c]);
                    unset($data[$c]['answers']);
                }

                echo "<pre>";
                print_r($answers);
                echo "<pre>";
                print_r($data);
                exit();
                $c++;
            }
            //echo "<pre>"; print_r($data);
            $CI = & get_instance();
            // $CI->db->insert_batch('feedbacks', $data);
            if ($memcache->flush()) {
                // echo "memeory Flush";
                if ($memcache->get('feedback') == false) {
                    return true;
                }
            }
        }
    }

    public function setMem($key, $data) {
        $memcache = $this->memConnect();

        if ($this->existsMem($key) == false) {

            if ($memcache->get('feedback') == false) {
                $feedback = array($key => json_encode($data));
                $memcache->set('feedback', $feedback, MEMCACHE_COMPRESSED, $this->memDuration);
            } else {
                // echo "feedback  exists in set mem<hr>"; 
                $feedback = $memcache->get('feedback');
                //echo "<pre>";print_r($feedback);
                $feedback[$key] = json_encode($data);

                //  echo "<pre>";print_r($feedback);
                // echo "<br>";  
                $memcache->set('feedback', $feedback, MEMCACHE_COMPRESSED, $this->memDuration);

                // $feedbacknew = $memcache->get('feedback');
                // echo "<pre>";print_r($feedbacknew); exit();    
            }
            return true;
        } else {
            return false;
        }
    }

    public function getMem($key) {
        $memcache = $this->memConnect();
        if ($memcache->get($key) == true) {
            return true;
        } else {
            return false;
        }
    }

    public function existsMem($key) {
        $memcache = $this->memConnect();
        if ($memcache->get('feedback') == true) {

            $feedback = $memcache->get('feedback');
            if (array_key_exists($key, $feedback)) {
                // echo "<pre>";print_r($feedback);exit(); 
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Initialize Preferences
     *
     * @access	public
     * @param	array	initialization parameters
     * @return	void
     */
}

// END Pagination Class

/* End of file Pagination.php */
/* Location: ./system/libraries/Pagination.php */