<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>feedPack</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/main.css">
<script src="<?php echo base_url();?>js/vendor/modernizr-2.6.2.min.js"></script>
<script>
function getLocation()
  {
  if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(showPosition);
    }
  else{document.getElementById('user_location').value="none";}
  }
function showPosition(position)
  {
      
  document.getElementById('user_location').value= position.coords.latitude +  "," + position.coords.longitude;	
 
  }    
</script>

</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]--> 

<!-- Add your site or application content here -->
<div class="main">
  <h1 class="logo ir">feedPad</h1>
  <div class="container">
    <form action="<?php echo base_url(); ?>feedback/add" method="post" id="form1" name="form1">
      <div class="heading">
        <h2>Fill us in on your delivery</h2>
        <span class="right_arrow"></span> <span class="left_arrow"></span> </div>
      <div class="content">
        <div class="question">
          <h3>Did your package arrive on time?</h3>
          <input name="on_time_delivery" type="checkbox" checked value="1" class="on_off" id="on_time_delivery" />
          
        </div>
        <div class="question">
          <h3>Rate your package's condition</h3>
          <input type="range" min="0" max="5" value="0" step="1" id="backing2" />
          <div class="rateit" id="condition" data-rateit-backingfld="#backing2" data-rateit-resetable="false" data-rateit-starwidth="34" data-rateit-starheight="34"></div>
        </div>
        <div class="question">
          <h3>How great was your delivery person?</h3>
          <div class="rateit" id="behave" data-rateit-backingfld="#backing2" data-rateit-resetable="false" data-rateit-starwidth="34" data-rateit-starheight="34"></div>
        </div>
        <div class="question">
          <h3>Leave us your email address</h3>
          <input type="text" value="" id="customer_email" name="customer_email" />
        </div>
        <?php
		if($detail['show_location_field']==1)
		{
			?>
              <input type="hidden" value="" id="user_location" name="user_location" />
            <script>
            getLocation();
            </script>
            <?php	
		}
		?>
        <div class="question">
          <h3>Any further comments? (Max 250 Characters)</h3>
          <textarea name="comment" id="comment" maxlength="250"></textarea> 
        </div>
        <?php
		foreach($questions as $question)
		{
		?>
        <div class="question">
          <h3><?php echo $question['question'];?></h3>
          <?php
		  if($question['question_type']=="mcq")
		  {
			$options = json_decode($question['mcq']);
			foreach($options as $opt)
			{
				echo '<div style="margin-bottom: 15px;"><input type="radio" name="answers['.$question['id'].']" value="'.$opt.'" />&nbsp;';
				echo ''.$opt.'</div>';	
			}
		  }
		  elseif($question['question_type']=="rating")
		  {
			  ?>
              <div class="rateit" id="<?php echo $question['id'];?>" data-rateit-backingfld="#backing2" data-rateit-resetable="false" data-rateit-starwidth="34" data-rateit-starheight="34"></div>
              <input type="hidden" name="answers[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" value="" />
			  <?php
		  }
		  elseif($question['question_type']=="yes/no")
		  {
			?>
			<input type="checkbox" checked value="1" class="on_off rating_custom" id="yesNo_<?php echo $question['id'];?>" />
			<input type="hidden" name="answers[<?php echo $question['id'];?>]" value="1" id="answer_<?php echo $question['id'];?>" />
			<?php
		  }
		  elseif($question['question_type']=="text")
		  {
			?>
			<input name="answers[<?php echo $question['id'];?>]" type="text" id="answer_<?php echo $question['id'];?>" />
			<?php
		  }
		  ?>
          
        </div>
        <?php	
		}
		?>
        <div class="question">
          <a href="terms.php" style="color:#FFF;">Disclaimer</a>
        </div>
        <a href="#_" onClick="formSubmit()" class="button ir"> Send Feedback</a> </div>
        <input type="hidden" name="parcel_condition" id="parcel_condition" value="" />
        <input type="hidden" name="behavior" id="behavior" value="" />
        <input type="hidden" name="cid" value="<?php echo $cid;?>" />
        <input type="hidden" name="vendorid" value="<?php echo $vendorid;?>" />
        <input type="hidden" name="parcelid" value="<?php echo $parcelid;?>" />
        
    </form>
  </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo base_url();?>js/vendor/jquery-1.8.2.min.js"><\/script>')</script> 
<script src="<?php echo base_url();?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins.js"></script> 
<script src="<?php echo base_url();?>js/main.js"></script>
<script>

function formSubmit()
{
	var hascls = $('#on_time_delivery').closest('.ez-checkbox').hasClass('ez-checked');
	if(hascls)
	{
		$('#on_time_delivery').val('1');
	}
	else
	{
		$('#on_time_delivery').val('0');
	}
	
	$('#form1').submit();
}
$().ready(function() {
	$('.rating_custom').click(function(){
		var hascls = $(this).closest('.ez-checkbox').hasClass('ez-checked');
		var arr = $(this).attr('id').split('_');
		var id = arr[1];
		if(hascls)
		{
			$('#answer_'+id).val('0');
		}
		else
		{
			$('#answer_'+id).val('1');
		}
	});
	$('.rateit').bind('rated', function() { 
		//alert('rating: ' + $(this).rateit('value')); 
		if($(this).attr('id')=="condition")
		{
               
			$('#parcel_condition').val($(this).rateit('value'));
		}
		else if($(this).attr('id')=="behave")
		{
                   
			$('#behavior').val($(this).rateit('value'));
		}
		else
		{
			$('#answer_'+$(this).attr('id')).val($(this).rateit('value'));	
		}
	});
	
	jQuery("#form1").validate({
		rules: {
			customer_email: {
				email:true
			},
			comment: {
				//required: true,
				maxlength:250
			}
		},
		messages: {
			customer_email: {
				//required: "This field required",
				email: "Please enter valid email"
			},
			comment: {
				//required: true,
				maxlength:"Max character limit 250"
			}
		},
		 highlight: function(label) {
			jQuery(label).next('.error').css('color','red');
		},
		errorPlacement: function(error, element){
			error.appendTo(element.parent());
		}

	});
});
</script>
</body>
</html>