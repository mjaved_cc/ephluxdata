 
<div style="padding:10px;">

        <div  style="float:right" >
        <select name="per_page" id ="per_page" style="margin-left:10px;" onchange="javascript: window.location='<?php echo base_url(); ?>campaign/feedbacks/<?php echo $id; ?>/'+this.value+'/<?php echo $currentPage; ?>'">
            <option value ="15" <?php echo ($per_page == "1" ? "Selected" : "" ); ?> >15</option>
            <option value ="30" <?php echo ($per_page == "30" ? "Selected" : "" ); ?> >30</option>
            <option value ="50" <?php echo ($per_page == "50" ? "Selected" : "" ); ?> >50</option>
            <option value ="100" <?php echo ($per_page == "100" ? "Selected" : "" ); ?> >100</option>

        </select>
    </div >

    <div align="right"><a href="<?php echo base_url(); ?>campaign/detail/<?php echo $id; ?>" class="fr btn_light_green btn-small">Back to Summary</a>

    </div>
    <?php
    if (!empty($feedbacks)) {
        /*if ($this->uri->segment(5)) {
            $currentPage = $this->uri->segment(5);
        } else {
            $currentPage = 0;
        }*/
        ?>
        <div align="right"><a href="<?php echo base_url(); ?>campaign/csv/<?php echo $id; ?>/<?php echo $per_page; ?>/<?php echo $currentPage; ?>/<?php echo $filterVal ?>" style="margin-right:10px;" class="fr btn_light_green btn-small">Export Csv</a></div>
        <?php
    }
    ?>
    <?php if ($filterVal == "") { ?>
        <div align="right"><a href="#_" class="fr btn_light_green btn-small" id="filterBtn" style="margin-right:10px;">Filter</a></div>
        <?php
    }
    ?>


    <br clear="all">
    <div id="filterPanel" class="dataTables_paginate" style="<?php if ($filterVal == "") { ?>display:none; <?php } ?>margin-top: 11px; border-top:1px solid #D4D9DF;">
        <form name="form1" id="form1" action="" method="post">
            <div style="display:inline-block; float:left;">
                <label style="float:left;padding-right: 10px; line-height:25px;">Parcel ID</label>
                <input type="text" name="parcel_id" id="parcel_id" value="<?php echo $filterVal ?>" />
            </div>
            <!--<div style="display:inline-block; float:left; margin-left:15px;">
                <label style="float:left;padding-right: 10px;line-height:25px;">Email</label>
            <input type="text" name="email" />
            </div>-->
            <div style="float:right;">
                <a href="#_" class="fr btn_light_green btn-small" id="cancelBtn">Cancel</a>
                <a href="#_" class="fr btn_light_green btn-small" style="margin-right:10px;" id="filterSubmit">Filter</a> 
            </div>
        </form>    
        <br clear="all">
    </div>
</div>

<table width="100%" cellpadding="5">
    <tr>
        <th scope="col" class="nobg">Feedback ID</th>
        <th scope="col">Feedback Time</th>
        <?php
        $q = 1;
        foreach ($questions as $question) {
            ?>
            <th scope="col">Q<?php echo $q; ?></th>
            <?php
            $q++;
        }
        ?> 
        <th scope="col">Parcel ID</th>
        <th scope="col" style="width:60px !important;">Location</th>
        <th scope="col" style="width:60px !important;">Action</th>
        <!--<th scope="col">Answer 1</th>
        <th scope="col">Answer 2</th>
        <th scope="col">Answer 3</th>
        <th scope="col">Answer 4</th>
        <th scope="col">Comment</th>
        <th scope="col">DateTime</th>
        <th scope="col">Parcel ID</th>
        <th scope="col">Detail</th>-->
    </tr>
    <?php
     /*echo '<pre>';
      print_r($feedbacks);exit;*/ 
    if (!empty($feedbacks)) {
        foreach ($feedbacks as $feedback) {
			
            ?>
            <tr>
                <th class="spec" scope="row" abbr="ID"><?php echo $feedback['feedback_id']; ?></th>
                <td><?php echo date('m/d/Y - h:i a', strtotime($feedback['feedback_time'])); ?></td> 
                <?php
				
                $answers = json_decode($feedback['answer'],true);//explode(',', $feedback['answer']);
                //print_r($answers);
				foreach($questions as $question){
				//for ($loop = 0; $loop < count($questions); $loop++) {
                    ?>
                    <td><?php echo isset($answers[$question['id']]) ? $answers[$question['id']] : ""; ?></td>       
                    <?php
                }
                ?>    
                <td><?php echo $feedback['parcel_id']; ?></td>
                <td align="center"><?php
        if (!empty($feedback['user_location']) && $feedback['user_location'] > 0) {
            $disabled = "";
        } else {
            $disabled = "disabled";
        }
        ?>
                    <a href="https://maps.google.com/?q=<?php echo $feedback['user_location']; ?>" target="_blank" class="location_icon <?php echo $disabled; ?>"> </a>

                </td>
                 <td align="center">
 <a href="<?php echo base_url();?>campaign/deletefeedback/<?php echo $id;?>/<?php echo $feedback['feedback_id']; ?>"  class="delete_icon1"style="margin-right:10px;"></a>   
 
                </td>     
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="3" class="td-r">No feedbacks found.</td>       
        </tr>
        <?php
    }
    ?>        
</table>
  <script>
              $(".delete_icon1").click(function(e) {
					e.preventDefault();
                                       var href = $(this).attr('href');
                                        bootbox.confirm("Are you sure you want to delete this feedback?", function(confirmed) {
						if(confirmed==true)
						  {
						   window.location =href;   
						  }  
					});
				});  
                </script>
<div class="dataTables_paginate paging_full_numbers">
<?php echo $this->pagination->create_links(); ?>
</div>