<?php include_once("header.php"); ?>
<style>
    label.error{
        color:#900 !important;
    }
</style>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<div class="highlight_area">
    <div class="container ">
        <!--<p class="fr">Don't have an account ? <a href="#">Sign up here.</a></p>-->
        <h2>Add Campaign</h2>
    </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
    <form action="" method="post" name="form1" id="form1" enctype="multipart/form-data">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
        <div class="span4">
            <div class="input_data">
                <label>Campaign Name</label>
                <input type="text" class="input-xlarge" name="campaign_name" id="campaign_name" value="" />
            </div>
            <div class="input_data">
                <label>Status</label>
                <select name="campaign_status">
                    <option value="Draft">Draft</option>
                    <option value="Active">Active</option>
                    <option value="Completed">Completed</option>
                </select>
            </div>


            <!--<div class="input_data">
                <label>Courier</label>
                <input type="text" class="input-xlarge" name="courier" id="courier" />
            </div>-->

            <div class="input_data">
                <label>Expected number of deliveries</label>
                <input type="text" class="input-xlarge" name="no_of_packages" id="no_of_packages" value="" />
            </div>
            <!--<div class="input_data">
                <label>QR URL</label>
                <input type="text" class="input-xlarge" name="qr_url" id="qr_url" value="" />
            </div>-->

        </div>
        <div class="span4">
            <div class="input_data">
                <label>Creation Date:</label>
                <input type="text" class="input-xlarge" name="creation_date" id="creation_date" readonly="readonly" value="<?php echo date('Y-m-d'); ?>" />
            </div>
            <div class="input_data">
                <label>Retailer Name</label>
                <input type="text" class="input-xlarge" name="recipent_name" id="recipent_name" />
            </div>

            <div class="input_data">
                <label>Select Campaign Type</label>
                <select name="campaign_type">
                    <?php
                    foreach ($campaign_types as $type) {
                        ?>
                        <option value="<?php echo $type['id']; ?>"><?php echo $type['campaign_type']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>


            <?php /* ?><div class="input_data">
              <label>End Time</label>
              <div class="input-append bootstrap-timepicker-component margin-top-11">
              <input type="text" class="input-large timepicker-default" name="end_time" id="end_time"><span class="add-on"><i class="icon-time"></i></span>
              </div>

              </div><?php */ ?>
        </div>
        <div class="span4">
            <div class="input_data">
                <label>Header Text</label>
                <input type="text" class="input-xlarge" name="header_text" id="header_text" />
            </div>
            <div class="input_data">
                <label>Provide logo </label>
                <input type="file"  name="logo" id="logo" />
            </div>
        </div>

        <div class="span12" style="padding-left:20px;">
            <label>Request users's location?</label>
            <input type="checkbox" name="show_location_field" value="1" /> If selected, this option will ask users to share their location on the feedback form.
        </div>
        <!--<div class="span12" style="padding-left:20px;">
            <label>Email addrss is mandatory?</label>
            <input type="checkbox" name="is_email_mandatory" value="1" /> If selected, this option will make email field mandatory on the feedback from.
        </div>-->
        <div class="span12" style="padding-left:20px;">
            <a href="#_" class="" onClick="addQuestion();">Add Questions</a>
            <div id="questionError"class =" span12 errorN" style="display:none;font-weight: bold;"></div>
        </div>

        <div class="span12" id="questionType" style="display:none;padding-left:20px;">
            <label>Select question type:</label>
            <select name="question_type" id="question_type" class="input-xlarge">
                <option value="">Select</option>
                <option value="mcq">Multiple Choice</option>
                <option value="rating">Five Star Rating</option>
                <option value="tenstarrating">Ten Star Rating</option>
                <option value="text">Free Text</option>
                <option value="yes/no">Yes and No</option>
                <option value="email">Email</option>
                <option value="dropdown">DropDown</option>
            </select>
        </div>
        <div class="span12" id="questionFrom" style="padding-left:20px;">

        </div>
        <div class="span12" style="padding-left:20px; display:none;" id="questionGrid">
            <table width="100%" cellpadding="5">
                <tr>
                    <th scope="col" width="870">Question Text</th>
                    <th scope="col"></th>
                </tr>
            </table>
        </div>
        <div class="span12">
            <a href="#" class="fr btn_light_green btn-small" onClick="loginF()" style="margin-left:10px;">Save</a>
            <a href="<?php echo base_url(); ?>campaign/" class="fr btn_light_green btn-small">Cancel</a>
        </div>
    </form>
</div>
<script src="<?php echo base_url(); ?>js/admin/add-campaign.js" type="text/javascript"></script>
<?php include_once("footer.php"); ?>    