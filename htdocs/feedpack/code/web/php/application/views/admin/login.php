<?php include_once("header.php"); ?>
  <div class="highlight_area">
	<div class="container ">
	    <!--<p class="fr">Don't have an account ? <a href="#">Sign up here.</a></p>-->
		<h2>Admin login</h2>
    </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
    <form action="<?php echo base_url(); ?>admin/login" method="post" name="form1" id="form1">
    <div class="span4">
        <div class="input_data">
            <label>Username</label>
            <input type="text" class="input-xlarge" name="username" id="username" value="" />
        </div>
        <div class="input_data">
            <label>Password</label>
            <input type="password" class="input-xlarge" name="password" id="password" />
        </div>
        <div class="input_data">
            <a href="#" class="fr btn_light_green btn-small" onClick="loginF()">Login</a>
        </div>
    </div>
    <?php /*?><div class="span4">
    	<a href="<?php echo base_url();?>index.php/fblogin" class="login-with-facebook  margin-top23">login with Facebook</a>
    </div><?php */?>
    </form>
</div>
<?php
if($error)
{
	?>
    <script>
	bootbox.alert("The username or password you entered is incorrect.", "Okay");
	</script>
    <?php	
}
?>
<script>
function loginF()
{
	jQuery('#form1').submit();
}

jQuery(document).keyup(function(e){
	
	if(e.keyCode == 13)
	{
		loginF();
	}
	
});


jQuery().ready(function() {
	jQuery("#form1").validate({
		rules: {
			username: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			username: {
				required: "This field required"
			},
			password: {
				required: "This field required"
			}
		},
		 highlight: function(label) {
			jQuery(label).next('.error').css('color','red');
		},
		errorPlacement: function(error, element){
			error.appendTo(element.parent());
		}

	});
		
});	

</script>  
<?php include_once("footer.php"); ?>  

