<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<style>
    td{
        border-right:none;
    }
</style>
<div class="highlight_area">
    <div class="container "> 
        <h2>Questions/Answers     <div align="right"><a href="<?php echo base_url(); ?>campaign/feedbacks/<?php echo $id; ?>" class="fr btn_light_green btn-small">Back to Responses</a></div> </h2>

    </div>

</div>
<div class="shadow_bot"></div>
<div class="container">
    <?php // echo "<pre>" ; print_r($questions); ?>
    <table width="100%" cellpadding="5">
        <tr>
            <th scope="col" >Question</th>
            <th scope="col">Question type</th>
            <th scope="col">Answer</th>

        </tr>
        <?php
        $qType = array("mcq" => "Multiple Choice",
            "rating" => "Five Star Rating",
            "text" => "Free Text",
            "yes/no" => "Yes and No",
            "email" => "Email",
        );

        if (!empty($questions)) {
            foreach ($questions as $question) {
                ?>
                <tr>
                    <td valign="top" ><?php echo $question['question'] ?></td>
                    <td valign="top" ><?php
        if (array_key_exists($question['question_type'], $qType)) {

            echo $qType[$question['question_type']];
        }
                ?></td>
                    <td valign="top" ><?php
                if ($question['question_type'] == "mcq") {
                    $options = json_decode($question['mcq']);
                    foreach ($options as $opt) {
                        if ($question['answer'] == $opt) {
                            $opttxt = "<strong style='font-size:16px;'>" . $opt . "</strong>";
                        } else {
                            $opttxt = $opt;
                        }

                        echo '<div style="margin-bottom: 15px;"><input type="radio" disabled="true" name="answers[' . $question['id'] . ']" value="' . $opt . '" ' . ($question['answer'] == $opt ? "checked" : "") . ' />&nbsp;';
                        echo '' . $opttxt . '</div>';
                    }
                } elseif ($question['question_type'] == "rating") {
                    ?>

                            <div class="active_<?php echo $question['answer'] ?> fr" style="float:left !important;"> <span class="star one fl"></span> <span class="star two fl"></span> <span class="star three fl"></span> <span class="star four fl"></span> <span class="star five fl"></span> </div></td>     
                            <?php
                        } elseif ($question['question_type'] == "yes/no") {
                            if ($question['answer'] == "1") {
                                echo "yes";
                            } else {
                                echo "no";
                            }
                        } elseif ($question['question_type'] == "email") {
                            echo $question['answer'];
                        } elseif ($question['question_type'] == "text") {
                            echo $question['answer'];
                        }
                        ?></td>

                </tr>
                    <?php
                }
            } else {
                ?>
            <tr>
                <td colspan="5" class="td-r">No Answer found.</td>       
            </tr>
                    <?php
                }
                ?>        
    </table>
</div>

        <?php include_once("footer.php"); ?>
