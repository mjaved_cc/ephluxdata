<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>feedPack Admin</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!--========= CSS  =========-->
<link href="<?php echo base_url(); ?>css/admin/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/admin/bootstrap-responsive.css" rel="stylesheet">
<!-- Custom Styles -->
<link href="<?php echo base_url(); ?>css/admin/themes.css" rel="stylesheet">
<!-- / Custom Styles -->
<!-- Fonts -->
<link href="<?php echo base_url(); ?>css/admin/fonts.css" rel="stylesheet">
<!-- / Fonts -->
<!-- DatePicker -->
<link href="<?php echo base_url(); ?>css/admin/datepicker.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>datepicker.less" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/admin/data_table.css" rel="stylesheet">
<!-- / Fonts -->
<!-- Fonts -->
<link href="<?php echo base_url(); ?>css/admin/timepicker.css" rel="stylesheet">
<!-- / Fonts -->
<!-- DTTT -->
<link href="<?php echo base_url(); ?>css/admin/TableTools_JUI.css" rel="stylesheet">
<link type="text/css" href="<?php echo base_url(); ?>css/admin/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
<!-- / DatePicker -->
<!-- Placed at the end of the document so the pages load faster --> 
<?php /*?><script type="text/javascript" src="<?php echo base_url(); ?>js/admin/jquery-1.8.0.min.js"></script> <?php */?>
<script src="<?php echo base_url(); ?>js/admin/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/admin/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/admin/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>js/admin/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/admin/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url(); ?>js/admin/modernizr.js"></script>
<script src="<?php echo base_url(); ?>js/admin/bootbox.js"></script>
<!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>-->
<script src="<?php echo base_url(); ?>js/admin/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/admin/additional-methods.js" type="text/javascript"></script>

<script>
//jQuery.noConflict();
</script>
 
<link rel="shortcut icon" href="<?php echo base_url(); ?>ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>ico/apple-touch-icon-57-precomposed.png">


</head>
<!--============================ BODY ============================-->
<body>

<!--============================ MAIN WRAPPER ============================-->
<div class="container clearfix"> 
  <!-- Logo -->
  <div class="span3 no-margin" style="width:auto; height:60px;"><a href="#"><img src="<?php echo base_url(); ?>images/admin/logo.png" style="margin-left:-59px; max-width:none;"></a></div>
  <!-- / Logo --> 
  <!--========= USER SETTING MENU  =========-->
  <div class="offset5">
  
  	<!--<div class="login">
         <a class="btn_white btn-small margin-right-10" href="login.php">Login</a>
    </div>-->
    
    <div class="btn-group margintop10 fr"> <a class="btn btn-primary" href="#">
    <i class="icon-user icon-white"></i></a> <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
      <ul class="dropdown-menu">        
        <li><a href="<?php echo base_url();?>admin/logout"> logout</a></li>
      </ul>
    </div>
    <!-- ENABLE FOR LOGOUT --> 
    <!--a href="#" class="btn btn-default">Logout</a--> 
  </div>
  <!--========= / USER SETTING MENU  =========--> 
  <!--========= MAIN NAVIGATION  =========-->
  <div class="span12">
    <ul class="navigation">
      <!--<li><a href="#">Home</a></li>-->
		<?php 
        if($this->uri->segment(2)=="add")
		{
			$link1 = "";
			$link2 = 'class="active"';	
			$link3 = "";
		}
		elseif($this->uri->segment(1)=="compare"){
			$link1 = "";
			$link2 = "";
			$link3 = 'class="active"';	
		}
		else{
			$link2 = "";
			$link1 = 'class="active"';	
			$link3 = "";
		}
        ?>
      <li><a href="<?php echo base_url();?>campaign/" <?php echo $link1; ?>>Campaigns</a></li>
      <li><a href="<?php echo base_url();?>campaign/add" <?php echo $link2; ?>>Add Campaign</a></li>
      <li><a href="<?php echo base_url();?>compare/" <?php echo $link3; ?>>Compare Campaigns</a></li>
    </ul>
  </div>
  <!--========= / MAIN NAVIGATION  =========--> 
</div>