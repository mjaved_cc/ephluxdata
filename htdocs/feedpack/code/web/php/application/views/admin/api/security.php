<?php include_once(realpath(dirname(__FILE__)  .'/..') .'/'."header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<style>
td{
	border-right:none;
}
</style>
<div class="highlight_area">
  <div class="container "> 
    
    <h2>Enter Security Key</h2>
  </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
  <form method="post" action="parcel/verifySecurity/" name="form1" id="form1">
  	<div class="span4">
        <div class="input_data">
            <label>Security Key</label>
            <input type="text" name="key" id="key" />
        </div>
        <div class="input_data">
            <a href="#" class="fr btn_light_green btn-small" onClick="submitF()">Verify</a>
        </div>
    </div>    
  </form>
</div>

<script>
	function submitF()
	{
		jQuery('#form1').submit();
	}
</script>
<?php include_once(realpath(dirname(__FILE__)  .'/..') .'/'."footer.php"); ?>
