<?php include_once(realpath(dirname(__FILE__)  .'/..') .'/'."header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<style>
td{
	border-right:none;
}
.error{
	color:#900;
}
</style>
<div class="highlight_area">
  <div class="container "> 
    
    <h2>Upload Parcel File</h2>
  </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
  <?php if($this->session->flashdata('flashSuccess')):?>
    <p style="color:#063;"> <?=$this->session->flashdata('flashSuccess')?> </p>
    <?php endif?>
     
    <?php if($this->session->flashdata('flashError')):?>
    <p style="color:#900;"> <?=$this->session->flashdata('flashError')?> </p>
    <?php endif?>
     
    <?php if($this->session->flashdata('flashInfo')):?>
    <p class='flashMsg flashInfo'> <?=$this->session->flashdata('flashInfo')?> </p>
    <?php endif?>
     
    <?php if($this->session->flashdata('flashWarning')):?>
    <p class='flashMsg flashWarning'> <?=$this->session->flashdata('flashWarning')?> </p>
    <?php endif?>
  <form method="post" action="" name="form1" id="form1" enctype="multipart/form-data">
  	<div class="span4">
        <div class="input_data">
            <label>Upload file</label>
            <input type="file" name="jsonFile" id="jsonFile" />
        </div>
        <div class="input_data">
            <a href="#" class="fr btn_light_green btn-small" onClick="submitF()">Upload</a>
        </div>
    </div>    
  </form>
</div>

<script>
	function submitF()
	{
		jQuery('#form1').submit();
	}
	jQuery("#form1").validate({
		rules: {
			jsonFile:{
				required:true,
				accept: "txt"
			}
		},
		messages: {
			jsonFile: {
				accept:"Please upload a file",
				accept:"Please upload only txt files"
			}
		},
		errorPlacement: function(error, element){
			error.appendTo(element.parent());
		},
		highlight: function(label) {
			jQuery('.error').css('color','red');
		}

	});
</script>
<?php include_once(realpath(dirname(__FILE__)  .'/..') .'/'."footer.php"); ?>
