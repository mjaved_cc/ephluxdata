<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<style>
    td{
        border-right:none;
    }
</style>
<div class="highlight_area">
    <div class="container "> 
               <h2>Compare Campaign</h2>
    </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
    <form name ="form1" id="form1" method="post" action="<?php echo base_url(); ?>compare/">
    <table width="100%" border="0" class="input_data">
        <tr>
            <td valign="top" style="border-bottom:none;"><table width="100%" border="0">
                    <tr>
                        <th colspan="4"> Select form below to Compare Campaigns </th>
                    </tr>
                    <tr><td >
                            <select name ="cId[]" >
                                <?php
                               foreach ($campaigns as $comp) {
                                    ?>
                                    <option value ="<?php echo $comp['id'] ?>" <?php echo ($comp['id']==$selected[0] ? "selected":"" )?>><?php echo $comp['campaign_name'] ?>
                                    </option>  
                                    <?php } ?>
                            </select>
                         </td>
                         <td>
                            <select name ="cId[]">
                                <?php
                               foreach ($campaigns as $comp) {
                                    ?>
                                    <option value ="<?php echo $comp['id'] ?>"  <?php echo ($comp['id']==$selected[1] ? "selected":"" )?>><?php echo $comp['campaign_name'] ?>
                                    </option>  
                                    <?php } ?>
                            </select>
                         </td>
                         <td>
                            <select name ="cId[]">
                                <?php
                               foreach ($campaigns as $comp) {
                                    ?>
                                    <option value ="<?php echo $comp['id'] ?>"<?php echo ($comp['id']==$selected[2] ? "selected":"" )?>><?php echo $comp['campaign_name'] ?>
                                    </option>  
                                    <?php } ?>
                            </select>
                         </td>
                         <td ><input type="submit" value="Compare" name="submit"></td>
                    </tr>

        </table>
<?php if(!empty($compare)) {?>
        <table width="95%" border="0">
            <tr><td>
        <style>
     

.res0 {
    border: #DDDDDD 1px dotted;
    float: left;
    margin-right: 31px;
    width: 286px;
}
.res1 {
    border: #DDDDDD 1px dotted;
    float: left;
    margin-right: 30px;
    width: 286px;
}
.res2 {
   border: #DDDDDD 1px dotted;
    float: left;
    margin-right: 26px;
    width: 286px;
}

        </style>
   <?php 
   $c=0;
   foreach($compare as $key) {
   
    
       ?>
                
                
        <div class="res<?php echo $c;?>">    


        <table width="100%" border="0" >
            <tr>
           
                <?php

                if ($key['campaign_status'] != "Draft") {
                    if ($key['total_response'] == 0) {
                        ?>
                        <td valign="top" width="250"><table width="250" border="0">
                                <tr>
                                    <td>No responses found</td>
                                    <td><a href="#" style="text-decoration:underline;"><strong><?php echo $key['total_response']; ?></strong></a></td>
                                </tr>
                            </table>
                        </td>
        <?php
    } else {
        ?>  
                        <td style="border-bottom:none;" width="250" align="left">
                            <table width="100%" border="0" width="250">
          <tr>
            <td colspan='2'><h4><?php echo $key['campaign_name']?></h4></td>
          </tr>
                                
           <tr>
            <td>No. of Responses</td>
            <td><a href="<?php echo base_url();?>campaign/feedbacks/<?php echo $key['id'];?>" style="text-decoration:underline;"><strong><?php echo $key['total_response'];?></strong></a></td>
          </tr>
          <?php 
		  if(!empty($key['yesNo'])){
			  foreach($key['yesNo'] as $k => $v) {
		  ?>
              <tr>
                <td colspan="2"><?php echo $k;?><br />
                  <?php echo $v;?> % responded YES </td>
              </tr>
          <?php
			  }
		  }
		  ?>
          <?php 
		  if(!empty($key['ratings'])){
			  foreach($key['ratings'] as $k => $v) {
		  ?>
          <tr>
            <td colspan="2"><?php echo $v['question'];?><br />
              <div style="float:right;"><?php echo $v['avg_rating'];?> Ratings</div>
              <div class="active_<?php echo $v['avg_rating'];?> fr"> <span class="star one fl"></span> <span class="star two fl"></span> <span class="star three fl"></span> <span class="star four fl"></span> <span class="star five fl"></span> </div></td>
          </tr>
          <tr>
            <td style="border-bottom:none;" colspan="2"><table width="100%" border="0">
                <?php
				  $start = array('once','two','three','four','five');
				  
				  for($col=5; $col>0; $col--)
				  {
				  ?>
                <tr>
                  <td width="119"><?php 
						for($a=$col; $a>0; $a--)
						{
						?>
                    <span class="star fl"></span>
                    <?php
						}
						?></td>
                  <td width="404" style="width:100px;"><div style="width:100%; display:block; height:16px; padding: 4px 0 !important; background:#CCC;">
                      <div style="width:<?php echo isset($v['answers'][$col]) ? $v['answers'][$col] : 0;?>%; max-width:404px; background-color:#666; height:16px; display:block;"></div>
                    </div></td>
                </tr>
                <?php
				  	
				  }
				  ?>
              </table></td>
          </tr>
          <?php
			  }
		  }
		  ?>
          
          <?php
          if($key['total_response']>0)
		  {
			?>
                <tr>
                <td colspan="2" style="border-bottom:none;" align="center"><a href="<?php echo base_url();?>campaign/feedbacks/<?php echo $key['id'];?>" class="btn_light_green btn-small" style="display:block;">View all responses</a></td>
              </tr>
            <?php
		  }
		  ?>
        </table>
                            
                        </td>
                                <?php
                            }
                        }
                        ?>  
                            </td>
                
                         
            </tr>
        </table></div>   <?php
        $c++;
        
        } ?><?php //if($c==1){echo "<td width='250'></td><td width='250'></td>";}elseif($c==2){echo "<td width='250' ></td>";} 
        ?>
        </td>
                                </tr>
                                <?php } ?>
        </table>
        </div>
                        <?php include_once("footer.php"); ?>
