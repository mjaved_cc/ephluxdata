<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<style>
td{
	border-right:none;
}
.btn-primary
{
	margin-top:0px;
}
</style>
<div class="highlight_area">
  <div class="container "> 
    <p class="fr">
    	<?php
		if($campaign_status=="Draft"){
		?>
        <a href="<?php echo base_url();?>campaign/delete/<?php echo $id;?>" id="dellink" class="btn_light_green btn-small" style="margin-right:10px;">Delete Campaign</a>
        <a href="<?php echo base_url();?>campaign/edit/<?php echo $id;?>" class="btn_light_green btn-small" style="margin-right:10px;">Edit Campaign</a>
        <?php
		}
		?>
        
              <script>
              $("#dellink").click(function(e) {
					e.preventDefault();
					bootbox.confirm("Are you sure to delete this campaign?", function(confirmed) {
						if(confirmed==true)
						  {
						   window.location ='<?php echo base_url();?>campaign/delete/<?php echo $id;?>';   
						  }  
					});
				});  
                </script>
        <a href="<?php echo base_url();?>parcel" class="btn_light_green btn-small">Add Parcel</a>
    </p>
    <h2>Campaign Detail</h2>
  </div>
</div>
<div class="shadow_bot"></div>

<div class="container">
  <table width="100%" border="0">
    <tr>
      <td valign="top" style="border-bottom:none;max-width: 750px;"><table width="100%" border="0">
          <tr>
            <th>ID</th>
            <th><?php echo $id;?></th>
          </tr>
          <tr>
            <td class="spec" scope="row">Campaign Type</td>
            <td class="spec" scope="row"><?php echo $campaign_type_name;?></td>
          </tr>
          <tr>
            <td class="spec" scope="row">Campaign Name</td>
            <td class="spec" scope="row"><?php echo $campaign_name;?></td>
          </tr>
          <tr>
            <td>Start Date</td>
            <td><?php echo $creation_date;?></td>
          </tr>
          <tr>
            <td>Retailer</td>
            <td><?php echo $recipent_name;?></td>
          </tr>
          <tr>
            <td>Status</td>
            <td><?php echo $campaign_status;?></td>
          </tr>
          <tr>
            <td>Expected Deliveries</td>
            <td><?php echo $no_of_packages;?></td>
          </tr>
			<?php
            $width = $height = 300; 
            $error = "H";
            if($campaign_type==2) {
            	$url = base_url().'?key='.$developerKey.'&cid='.$id.'&parcelid=0&p2g_code=0';  
            }
            else {
             	$url = base_url().'?key='.$developerKey.'&cid='.$id;
            }
            ?>
          <tr>
            <td>Campaign URL</td>
            <td><?php echo $url;?></td>
          </tr>
          <?php if(!empty($logo)) { ?> 
           <tr>
            <td>Logo</td>
            <td>
                       
                <img src="<?php echo base_url()?>logos/<?php echo $logo; ?>" class="uLogo">
            </td>
          </tr>
          <?php } ?>
          <tr>
            <td>Header Text</td>
            <td>
               <?php echo $header_text?>        
               
            </td>
          </tr>
          <tr>
            <td>Demo QR Code</td>
            <td>
            	<?php
				 // handle up to 30% data loss, or "L" (7%), "M" (15%), "Q" (25%) 
				 createQR($url);
				 //echo "<img src=\"http://chart.googleapis.com/chart?". "chs={$width}x{$height}&cht=qr&chld=$error&chl=$url\" />"; 
				?>
            </td>
          </tr>
        </table></td>
      <?php
	  if($campaign_status!="Draft")
	  {
		  if($total_response==0)
		  {
			?>
            <td valign="top"><table width="100%" border="0">
                <tr>
                <td>No responses found</td>
                <td><a href="#" style="text-decoration:underline;"><strong><?php echo $total_response;?></strong></a></td>
              </tr>
              </table>
              </td>
            <?php
		  }
		  else
		  {
	  ?>  
      		<td style="border-bottom:none;" valign="top"><table width="100%" border="0">
          <tr>
            <td>No. of Responses</td>
            <td><a href="<?php echo base_url();?>campaign/feedbacks/<?php echo $id;?>" style="text-decoration:underline;"><strong><?php echo $total_response;?></strong></a></td>
          </tr>
          <?php 
		  if(!empty($yesNo)){
			  foreach($yesNo as $k => $v) {
		  ?>
              <tr>
                <td colspan="2"><?php echo $k;?><br />
                  <?php echo $v;?> % responded YES </td>
              </tr>
          <?php
			  }
		  }
		  ?>
          <?php 
		  if(!empty($ratings)){
			  foreach($ratings as $k => $v) {
		  ?>
          <tr>
            <td colspan="2"><?php echo $v['question'];?><br />
              <div style="float:right;"><?php echo $v['avg_rating'];?> Ratings</div>
              <div class="active_<?php echo $v['avg_rating'];?> fr"> <span class="star one fl"></span> <span class="star two fl"></span> <span class="star three fl"></span> <span class="star four fl"></span> <span class="star five fl"></span> </div></td>
          </tr>
          <tr>
            <td style="border-bottom:none;" colspan="2"><table width="100%" border="0">
                <?php
				  $start = array('once','two','three','four','five');
				  
				  for($col=5; $col>0; $col--)
				  {
				  ?>
                <tr>
                  <td width="119"><?php 
						for($a=$col; $a>0; $a--)
						{
						?>
                    <span class="star fl"></span>
                    <?php
						}
						?></td>
                  <td width="404" style="width:100px;"><div style="width:100%; display:block; height:16px; padding: 4px 0 !important; background:#CCC;">
                      <div style="width:<?php echo isset($v['answers'][$col]) ? $v['answers'][$col] : 0;?>%; max-width:404px; background-color:#666; height:16px; display:block;"></div>
                    </div></td>
                </tr>
                <?php
				  	
				  }
				  ?>
              </table></td>
          </tr>
          <?php
			  }
		  }
		  ?>
          
          <?php
          if($total_response>0)
		  {
			?>
                <tr>
                <td colspan="2" style="border-bottom:none;" align="center"><a href="<?php echo base_url();?>campaign/feedbacks/<?php echo $id;?>" class="btn_light_green btn-small" style="display:block;">View all responses</a></td>
              </tr>
            <?php
		  }
		  ?>
        </table></td>
      <?php
		  }
	  }
	  ?>  
    </tr>
  </table>
</div>
<?php include_once("footer.php"); ?>
