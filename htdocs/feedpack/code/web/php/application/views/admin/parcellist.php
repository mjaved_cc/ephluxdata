<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>js/admin/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
             "oLanguage": {
      "sEmptyTable": "No data available",
      "sZeroRecords": "No data available"
      
    },
            "sPaginationType": "full_numbers"

        });
    } );
</script>
<style>
    td{
        border-right:none;
        background-color:#FFF !important;
    }
table.display tr.odd {
	background-color: white !important;
}

table.display tr.even {
	background-color: white !important;
}
</style>
<div class="highlight_area">
    <div class="container "> 
        <h2>Campaign Parcels</h2>
    </div>
</div>
<div class="shadow_bot"></div>

<div class="container">

    <table width="100%" border="0" class="input_data">
        <tr>
            <td valign="top" style="border-bottom:none;">
                <form name ="form1" id="form1" method="post" action="<?php echo base_url(); ?>parcellist/">
                    <table width="100%" border="0">
                        <tr>
                            <th colspan="2"> Select from below to List parcels </th>
                        </tr>
                        <tr><td >
                                <select name ="cId" >
                                    <?php
                                    foreach ($campaigns as $comp) {
                                        ?>
                                        <option value ="<?php echo $comp['id'] ?>" <?php echo ($comp['id'] == $selected[0] ? "selected" : "" ) ?>><?php echo $comp['campaign_name'] ?>
                                        </option>  
                                    <?php } ?>
                                </select>
                                &nbsp; &nbsp; &nbsp;
                                <input type="submit" value="List Parcels" name="submit">       
                            </td>
                            
                            <td>
    <?php
    if (!empty($parcels)) {
        /*if ($this->uri->segment(5)) {
            $currentPage = $this->uri->segment(5);
        } else {
            $currentPage = 0;
        }*/
        ?>
        <div align="right"><a href="<?php echo base_url(); ?>parcellist/csv/<?php echo $selected[0]; ?>" style="margin-right:10px;" class="fr btn_light_green btn-small">Export Csv</a></div>
        <?php
    }
    ?>
                            </td>
                        </tr>

                    </table></form>
                <?php  if (!empty($parcels)) { ?>
                <table width="100%" border="0" id="example">
                    <thead>
                        <tr>
                           <th>ID</th>
                            <th>Sender Name</th>
                            <th>Sender Address</th>
                            <th>p2g code</th>
                            <th>upi code</th>
                            <th>Destination</th>
                            <th>Service Name</th>
                            <th>Carrier Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       
                            foreach ($parcels as $p) {
                                ?>


                                <tr>

                                    <td><?php echo $p['id'] ?></td>
                                    <td><?php echo $p['sender_fullname']; ?></td>
                                    <td><?php echo $p['sender_address']; ?></td>
                                    <td><?php echo $p['p2g_code']; ?></td>
                                    <td><?php echo $p['upi_code']; ?></td>
                                    <td><?php echo $p['destination']; ?></td>
                                    <td><?php echo $p['service_name']; ?></td>
                                    <td><?php echo $p['carrier_name']; ?></td>
                                </tr>
                                <?php
                            }
                        
                        ?>  
                    </tbody>
        </tr>
    </table>
                <?php  } ?>

</div>
<?php include_once("footer.php"); ?>
