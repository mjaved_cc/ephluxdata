<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>



<style>
    td{
        border-right:none;
    }
</style>


<div class="highlight_area">
    <div class="container "> 
        <h2>Combine Campaign</h2>
    </div>
</div>
<div class="shadow_bot"></div>

<div class="container">

    <table width="100%" border="0" class="input_data">
        <tr>
            <td valign="top" style="border-bottom:none;"><table width="100%" border="0">
                    <tr>
                        <th colspan="4"> Select form below to List Campaigns </th>
                    </tr>
                    <tr><td >
                            <select name ="cId[]" id ="cItems">
                                <?php
                                foreach ($campaigns as $comp) {
                                    ?>
                                    <option value ="<?php echo $comp['id'] ?>" ><?php echo $comp['campaign_name'] ?></option>  
                                <?php } ?>
                            </select>
                            <input type="button" class="btn_light_green btn-small" value="List Questions" id='l1' name="submit" >
                        </td>
                        <td ></td>
                    </tr>

                </table>

                <table width="100%" border="0" class ="table-campaign">
                    <tr>
                        <td valign="top" style="border-bottom:none;">

                            <div id="accordion">

                            </div>
                        </td>
                    </tr>

                </table>
                 
                <form method="post" class ="from-mQ" action ="<?php echo base_url()?>index.php/combine/combineReport">
                    <table width="100%" border="0">
                        <tr>
                            <td valign="top" style="border-bottom:none;">
                                
                                <div id ="mergeQuestions">


                                </div>   
                                <input name="submit" class="btn_light_green btn-small" value="Generate Report" type="submit" >

                            </td>

                        </tr>

                    </table>
                </form>
                <div class="clear"></div>
                <hr><hr>
                <table width="100%" border="0" class ="table-gQ">
                    <tr>
                        <td valign="top" style="border-bottom:none;">
                            <div id ="groupQuestion">
                                <ul id="gQ">

                                </ul>    

                            </div>   
                            <input name="mQ" id ="mQ" class="btn_light_green btn-small" value="Merge Questions" type="button" >

                        </td>

                    </tr>

                </table>

 
                

                </div>

                <script>
            
                    function addQuestion(data)
                    {
                        var dataArr = data.split('|');
                        // alert(dataArr[0]);
                        //alert(dataArr[1]);
                        //alert(dataArr[2]);
                        var qText = dataArr[0];
                        var qId = dataArr[1];
                        var cId = dataArr[2];
                        
             
                        if($("#" + cId).length == 0) {
                            $("#groupQuestion ul").append('<li id='+qId+'><span id ='+cId+'>'+qText+'</span> | <a class="removebtnabc" onClick="delLi('+qId+')"><img src="images/admin/delete_icon.png" width="9" height="14" alt="Remove"></a></li>'); 
                        }else
                        {
                            var liId = $("#" + cId).closest("li").attr("id");
                   
                            $('#'+liId).replaceWith('<li id='+qId+'><span id ='+cId+'>'+qText+'</span> | <a class="removebtnabc" onClick="delLi('+qId+')"><img src="images/admin/delete_icon.png" width="9" height="14" alt="Remove"></a></li>');
                            
                        }    
                        
        
                         
                    }
            
                    var cnt =1;
                    $('#l1').click(function(){
                        var cId = $("#cItems").val();
                        //alert(cnt);
                        var campaign_name =$("#cItems option:selected").text();
                        
                        if($("#r" + cId).length > 0) {
                            
                                  alert('alerdy Exists');  
                            
                        }else if(cnt>=6)
                        {
                            alert('only five campaign allowed');              
                        }else
                        {    
                            
                            if($("#" + cId).length == 0) {
                                $.ajax({ 
                                    type: 'GET', 
                                    url: '<?php echo base_url(); ?>combine/getQuestion/'+cId+'/'+cnt+'/'+campaign_name, 
                                    data: { get_param: 'value' }, 
                                    dataType: 'html',
                                    success: function (data) { 
                                        $("#accordion").append(data).accordion('destroy').accordion(); 
                                 
                                        cnt++; 
                                    }
                                });
                            }else
                            {
                                alert('alerdy Exists');    

                            }    
                        }     
                    });
                    $('.removebtnabc').on('click',function(){
                        
                        //  alert('abc');  
                        
                    });    
                    
                    function delLi(id) {
                    
                        $("#"+id).remove(); 
                    }
                    
                    function makeQuestion(qText,qIds,cnt)
                    {
                       //var cnt = "mQ+cnt+"";
                        
                        var html = '<div id ="'+cnt+'">'+qText+'<input type="text" name="qid[]" value ='+qIds+'><a class="removebtn" onClick="delLi('+cnt+')"><img src="images/admin/delete_icon.png" width="9" height="14" alt="Remove"></a></div>';  
                        $("#mergeQuestions").append(html); 
                        $('#gQ li').remove();
                        $('#accordion input').removeAttr('checked');
                        $('#accordion div').each(function(){
                        
                            $(this).prop('checked', false);
                        });
                        
                    }
                    
                    
                  cnt2=5000;
                    $('#mQ').click(function(){
                        var liIds = $('#gQ li').map(function(i,n) {
                            return $(n).attr('id');
                        }).get().join(',');
                        if(liIds!='') {
                            var qText= $('#gQ li:first').text().split('|');
                            if(qText!=''){
                                cnt2++
                                makeQuestion(qText[0],liIds,cnt2);  
                               
                            }
                        

                            
                        }
                           
                    });
                    
                    
                </script>
                <script>
                    $(function() {
                        $( "#accordion" ).accordion();
                    });
                </script>
                <?php include_once("footer.php"); ?>
