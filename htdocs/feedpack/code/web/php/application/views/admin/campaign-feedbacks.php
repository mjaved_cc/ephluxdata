<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<div class="highlight_area">
<div class="container ">
    <!--<p class="fr">Don't have an account ? <a href="#">Sign up here.</a></p>-->
    <h2>Responses</h2>
</div>
</div>
<div class="shadow_bot"></div>
<div class="container">
<!--<h2>Questions:</h2>-->
<div style="padding:10px;">
<?php
if(!empty($feedbacks))
{
	$q=1;
	foreach($questions as $question)
	{
	?>
    Q<?php echo $q;?>) <?php echo $question['question'];?><br>
    <?php
		$q++;
	}
	?>
<?php
}
?>
<br><br>

</div>
<div id="ajaxData">

    <?php include_once('campaign-feedbacks-ajax.php'); ?>
</div>    
</div>
<script>
function getFilterData(isCncl)
{
	if(!isCncl){
		formData = 	$('#form1').serialize();
	}
	else {
		formData = '';	
	}
	//$('#filterSubmit').click(function(){
	  $.ajax({
			type: "POST",
			url: "<?php echo base_url();?>campaign/feedbacks/<?php echo $id;?>",
			data: formData
			}).done(function( msg ) {
			$('#ajaxData').html(msg);
		});
	//});	
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
$(function(){
	$('#filterSubmit').live("click",function(){
		$('#parcel_id').next('.error').remove();
		if(!isNumber($('#parcel_id').val())) {
			$('#parcel_id').after('<label class="error">Please enter valid Parcel ID</label>')
			return false;	
		}
		
     getFilterData(false); 
  });
  
  
  $('#cancelBtn').live("click",function(){
	 //$('#form1')[0].reset();
     getFilterData(true); 
  });
  
  
  $('#filterBtn, #cancelBtn').live("click",function(){
     $('#filterPanel').toggle();
     $('#filterBtn').toggle(); 
  })
  
  /*$('#filterBtn, #cancelBtn').click(function(){
     $('#filterPanel').toggle();
     $('#filterBtn').toggle(); 
  });*/
});
</script>
<?php include_once("footer.php"); ?>    