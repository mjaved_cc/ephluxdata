<?php include_once("header.php"); ?>
<link href="<?php echo base_url(); ?>css/admin/table.css" rel="stylesheet" />
<div class="highlight_area">
<div class="container ">
    <!--<p class="fr">Don't have an account ? <a href="#">Sign up here.</a></p>-->
    <h2>Campaigns</h2>
</div>
</div>
<div class="shadow_bot"></div>
<div class="container">
<?php if($this->session->flashdata('flashSuccess')):?>
<p class='flashMsg flashSuccess'> <?=$this->session->flashdata('flashSuccess')?> </p>
<?php endif?>
 
<?php if($this->session->flashdata('flashError')):?>
<p class='flashMsg flashError'> <?=$this->session->flashdata('flashError')?> </p>
<?php endif?>
 
<?php if($this->session->flashdata('flashInfo')):?>
<p class='flashMsg flashInfo'> <?=$this->session->flashdata('flashInfo')?> </p>
<?php endif?>
 
<?php if($this->session->flashdata('flashWarning')):?>
<p class='flashMsg flashWarning'> <?=$this->session->flashdata('flashWarning')?> </p>
<?php endif?>
	<div align="right" style="padding-bottom:10px;height: 30px; display:block;"><a href="<?php echo site_url('campaign/add');?>" class="fr btn_light_green btn-small">Create New</a></div>
    <table width="100%" cellpadding="5">
    	<tr>
        	<th scope="col" class="nobg">ID</th>
            <th scope="col">Campaign Name</th>
            <th scope="col">Start Date</th>
            <th scope="col">Retailer</th>
            <th scope="col">Status</th>
        </tr>
        <?php
		if(!empty($campaigns))
		{
			foreach($campaigns as $campaign)
			{
		?>
        		<tr>
        			<th class="spec" scope="row" abbr="ID"><?php echo $campaign['id'];?></th>
                    <td><a href="<?php echo site_url('campaign/detail')?>/<?php echo $campaign['id'];?>"><?php echo $campaign['campaign_name'];?></a></td>
                    <td><?php echo $campaign['creation_date'];?></td>
                    <td><?php echo $campaign['recipent_name'];?></td>
                    <td><?php echo $campaign['campaign_status'];?></td>        
                </tr>
        <?php
			}
		}
		else
		{
			?>
        		<tr>
        			<td colspan="5" class="td-r">No campaign found.</td>       
                </tr>
        <?php	
		}
		?>        
    </table>
</div>
<?php include_once("footer.php"); ?>    