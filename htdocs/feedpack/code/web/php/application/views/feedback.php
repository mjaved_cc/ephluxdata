<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>feedPack</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/spinningwheel.css"> 
<script src="<?php echo base_url();?>js/vendor/modernizr-2.6.2.min.js"></script>
<script>
function getLocation()
{
  if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(showPosition);
    }
  else{document.getElementById('user_location').value="none";}
}
function showPosition(position)
  {
      
  document.getElementById('user_location').value= position.coords.latitude +  "," + position.coords.longitude;	
 
  }    
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo base_url();?>js/vendor/jquery-1.8.2.min.js"><\/script>')</script> 
<script src="<?php echo base_url();?>js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/plugins.js"></script> 
<script src="<?php echo base_url();?>js/main.js"></script>

<script src="<?php echo base_url();?>js/spinningwheel.js"></script> 
<script src="<?php echo base_url();?>js/spinningwheel-add.js"></script>
</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]--> 

<!-- Add your site or application content here -->
<div class="main">
    <table width="100%" cellspaccing="0" cellpadding="0" class="logo_container">
        <tr><td>
          <h1 class="logo feedpack"><img src="<?php echo base_url(); ?>/images/logo.png"></h1>
            </td>
        <?php if(!empty($detail['logo'])) { ?>    
            <td>
  <h1 class="logo hanging-ribbon"><div class="content_holder"><div class="customer_logo"><img src="<?php echo base_url(); ?>logos/<?php echo $detail['logo'] ;?>"  ></div></div></h1>
    </td><?php } ?>
    </tr>
    </table>
  <div class="container">
    <form action="<?php echo base_url(); ?>feedback/add" method="post" id="form1" name="form1">
      <div class="heading">
        <h2><?php echo $detail['header_text'];?></h2>
        <span class="right_arrow"></span> <span class="left_arrow"></span> </div>
      <div class="content">
        <?php
		if($detail['show_location_field']==1)
		{
			?>
              <input type="hidden" value="" id="user_location" name="user_location" />
            <script>
            getLocation();
            </script>
            <?php	
		}
		?>
        
        <?php
		foreach($questions as $question)
		{
			
		?>
        <div class="question">
          <h3><?php echo $question['question'];?></h3>
          <?php
		  if($question['question_type']=="dropdown")
		  {
			$options = json_decode($question['mcq']);
            echo '<select name="answers['.$question['id'].']" ><option value="">Select</option>';
			foreach($options as $opt)
			{
				echo '<option value="'.$opt.'"> '.$opt.'</option>';
				echo ''.$opt.'</div>';	
			}
            echo '</select>';
		  }elseif($question['question_type']=="mcq")
		  {
			$options = json_decode($question['mcq']);
			foreach($options as $opt)
			{
				echo '<div style="margin-bottom: 15px;"><input type="radio" name="answers['.$question['id'].']" value="'.$opt.'" />&nbsp;';
				echo ''.$opt.'</div>';	
			}
		  }
		  elseif($question['question_type']=="rating")
		  {
			  ?>
              <input type="range" min="0" max="5" value="0" step="1" id="backing<?php echo $question['id'];?>" />
              <div class="rateit" id="<?php echo $question['id'];?>" data-rateit-backingfld="#backing<?php echo $question['id'];?>" data-rateit-resetable="false" data-rateit-starwidth="34" data-rateit-starheight="34"></div>
              <input type="hidden" name="answers[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" value="" />
			  <?php
		  }
		  elseif($question['question_type']=="yes/no")
		  {
			?>
			<input type="checkbox" checked value="Yes" class="on_off rating_custom" id="yesNo_<?php echo $question['id'];?>" />
			<input type="hidden" name="answers[<?php echo $question['id'];?>]" value="Yes" id="answer_<?php echo $question['id'];?>" />
			<?php
		  }
		  elseif($question['question_type']=="email")
		  {
			  if($question['is_mendatory']){
				$required = 'required';  
			  }
			  else {
				$required = '';  
			  }
			?>
			<input type="text" name="answers[<?php echo $question['id'];?>]" class="<?php echo $required; ?> email" id="answer_<?php echo $question['id'];?>" />
            
			<?php
		  }
		  elseif($question['question_type']=="text")
		  {
			?>
			<textarea class="comment" name="answers[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" maxlength="250"></textarea>
			<?php
		  }
		  elseif($question['question_type']=="tenstarrating")
		  {
			?>
                <span id="answer_<?php echo $question['id'];?>uAns" class="tenstar_uAns" ></span>       
                <span onclick="openOneSlot('answer_<?php echo $question['id'];?>')">Tap here to rate</span> 
                 
	  	<input type="hidden" name="answers[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" value="" />
			<?php
		  }
		  ?>
        <input type="hidden" name="question_type[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" value="<?php echo $question['question_type']?>" />
      <input type="hidden" name="question[<?php echo $question['id'];?>]" id="answer_<?php echo $question['id'];?>" value="<?php  echo $question['question'];?>" />
        </div>
        <?php	
		}
		?>
        <div class="question">
          <a href="terms/" style="color:#FFF;">Disclaimer</a>
        </div>
        <a href="#_" onClick="formSubmit()" class="button ir"> Send Feedback</a> </div>
        <input type="hidden" name="cid" value="<?php echo $cid;?>" />
        <input type="hidden" name="isParcelExists" value="<?php echo $isParcelExists;?>" />
        <input type="hidden" name="vendorid" value="<?php echo $vendorid;?>" />
        <input type="hidden" name="parcelid" value="<?php echo $parcelid;?>" />
        <input type="hidden" name="p2g_code" value="<?php echo $p2g_code;?>" />
        <input type="hidden" name="xfeedbacks" value="<?php echo $xfeedbacks;?>" />
        <input type="hidden" name="campaign_type" value="<?php echo $detail['campaign_type'];?>" />
        
    </form>
  </div>
</div>
<script>

function formSubmit()
{
	
	/*var hascls = $('#on_time_delivery').closest('.ez-checkbox').hasClass('ez-checked');
	if(hascls)
	{
		$('#on_time_delivery').val('1');
	}
	else
	{
		$('#on_time_delivery').val('0');
	}*/
	
	$('#form1').submit();
}
$().ready(function() {
	
	$('.rating_custom').click(function(){
		var arr = $(this).attr('id').split('_');
		var id = arr[1];
		
		var hascls = $(this).closest('.ez-checkbox').is('.ez-checked');
		if (navigator.userAgent.indexOf('Firefox') != -1 && parseFloat(navigator.userAgent.substring(navigator.userAgent.indexOf('Firefox') + 8)) >= 3.6){//Firefox
		 	hascls = (hascls==1) ? 0 : 1;
		}
		//alert(hascls);
		
		if(hascls)
		{
			$('#answer_'+id).val('Yes');
		}
		else
		{
			$('#answer_'+id).val('No');
		}
	});
	$('.rateit').bind('rated', function() { 
		//alert('rating: ' + $(this).rateit('value'));
		$('#answer_'+$(this).attr('id')).val($(this).rateit('value')); 
		
	});
	
	$("#form1").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent());
		}
	});
});
</script>
</body>
</html>