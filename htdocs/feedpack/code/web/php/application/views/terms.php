<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Disclaimer</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">

<script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]--> 

<!-- Add your site or application content here -->
<div class="main">
 <table width="100%" cellspaccing="0" cellpadding="0" class="logo_container">
        <tr><td>
          <h1 class="logo feedpack align-center"><img src="<?php echo base_url(); ?>/images/logo.png"></h1>
            </td>
        </tr>
 </table>
  <div class="container">
    <div class="heading">
      <h2>Disclaimer</h2>
      <span class="right_arrow"></span> <span class="left_arrow"></span> </div>
    <div class="content">
      <p>What Happens To Your Feedback and Details</p>
      <p>We collect information from you on this website so that we can improve our future delivery services. Any opinions or feedback you give are solely for our own use and do not relate to the person or retailer from whom you purchased your goods. If you choose to leave your email address we will only use this to a) thank you for leaving feedback and b) provide you with any exciting updates on the development of feedPack.</p>
      
      <!--<div align="center"><a href="index.php" class="back-btn" >BACK</a></div>-->
	  <div align="center"><FORM><INPUT type="button" value="Back" class="back-btn" onClick="history.back();"></FORM></div>
	  
    </div>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/vendor/jquery-1.8.2.min.js"><\/script>')</script> 
<script src="<?php echo base_url(); ?>js/plugins.js"></script> 
<script src="<?php echo base_url(); ?>js/main.js"></script>
</body>
</html>