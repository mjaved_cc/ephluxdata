<?php

class Campaign_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->load->library('mydb');
    }

    function insert($tbl, $data) {
        $this->db->insert($tbl, $data);
        return $this->db->insert_id();
    }

    function insert_batch($tbl, $data) {
        $this->db->insert_batch($tbl, $data);
    }

    function getCampaignTypes() {
        $this->db->where('`status`', '1');
        $this->db->order_by('campaign_type', 'asc');
        $query = $this->db->get('campaign_types');

        return $query->result_array();
    }

    function getCampaignTypeById($id) {
        $this->db->where('`id`', $id);
        $query = $this->db->get('campaign_types');
        //$this->db->last_query();
        return $query->row_array();
    }

    function isParcelRequiredToShowFeedback($campaign_id) {
        $arr = $this->db->query("select campaign_types.parcel_required  from campaigns inner join campaign_types on campaigns.campaign_type=campaign_types.id where campaigns.id=$campaign_id")->result_array();
        if (!empty($arr)) {
            return $arr[0]['parcel_required'];
        } else {
            return 0;
        }
    }

    function getUpiCodes($cid, $upi_codes) {
        $query = $this->db->query("CALL `getUpiCodes`($cid, '$upi_codes')");
        //$query = $this->db->query("SELECT GROUP_CONCAT(p2g_code) FROM parcels WHERE campaign_id=1 AND p2g_code IN($upi_codes)");
        return $query->row_array();
        $query->free_result();
    }

    function getAllCampaignsByVendorId($vendor_id = 0) {
        $query = $this->db->query("CALL `getAllCampaignsByVendorId`($vendor_id)");
        return $query->result_array();
    }

    function getCampaignDetail($id) {
        //$this->load->library('mydb');
        $sql = "CALL getCampaignDetail($id)";
        $result = $this->mydb->Query($sql);
        return $result;
    }

    function getCampaignDetailNew($id) {
        //$this->load->library('mydb');
        $sql = "CALL getCampaignDetailNew($id)";
        $result = $this->mydb->Query($sql);
        return $result;
    }

    function checkCampaignEsixt($ids) {
        $query = $this->db->query("SELECT id FROM campaigns WHERE id IN($ids)");
        $data = array();
        foreach ($query->result_array() as $ids) {
            $data[] = $ids['id'];
        }
        return $data;
    }

    function getDefaultQuestions($id) {
        $query = $this->db->query("CALL `getDefaultQuestions`($id)");
    }

    function getCampaignFeedbacksCountByCid($id, $parcel_id, $email = '') {
        $query = $this->db->query("CALL `getCampaignFeedbacksCountByCid`($id,$parcel_id,'$email')");
        return $query->num_rows();
    }

    function getCampaignFeedbacksApi($vendor_id, $cid, $offset, $limit, $fromDate = '', $toDate = '') {
        /* $query = $this->db->query("CALL `getCampaignFeedbacksApi`($vendor_id, $id, $limit, $offset, '$fromDate', '$toDate')");
          return $query->result_array(); */

        $chkPrcl = $this->isParcelRequiredToShowFeedback($cid);
        $sqlWhere = "";

        if ($chkPrcl == 0) {
            /* if ($cids != '') {
              $sqlWhere.=" and campaign_id in($cids)";
              } */
            if ($fromDate != '') {
                $sqlWhere.=" AND feedback_time > '" . $fromDate . "'";
            }
            if ($toDate != '') {
                $sqlWhere.=" AND feedback_time < '" . $toDate . "'";
            }

            $query = $this->db->query("SELECT id as feedback_id,feedback_time,parcel_id,user_location,
                                                                (SELECT concat('{',GROUP_CONCAT('\"',question_id,'\"',':','\"',answer,'\"'),'}') from answers where feedback_id=feedbacks.id order by question_id,feedback_id asc)answer
                                                                from feedbacks where vendor_id=$vendor_id and campaign_id=$cid $sqlWhere limit $offset, $limit");


            /* $query = $this->db->query("SELECT Feeds.campaign_id, Feeds.question_id,Feeds.feedback_id,group_concat(Feeds.answer)answer,Feeds.feedback_time,Feeds.parcel_id,Feeds.user_location FROM
              (
              SELECT f.campaign_id, a.question_id,a.feedback_id,a.answer,f.feedback_time,f.parcel_id,f.user_location FROM answers a
              INNER JOIN feedbacks f ON f.id=a.feedback_id
              WHERE f.vendor_id=$vendor_id $sqlWhere order by a.question_id asc limit $offset, $limit
              )Feeds GROUP BY Feeds.feedback_id order by Feeds.campaign_id"); */
            //$query = $this->db->query("CALL `getCampaignFeedbacks`($id, $offset, $limit,$cid,'$email')");
        } else {
            if ($fromDate != '') {
                $sqlWhere.=" AND feedbacks.feedback_time > '" . $fromDate . "'";
            }
            if ($toDate != '') {
                $sqlWhere.=" AND feedbacks.feedback_time < '" . $toDate . "'";
            }

            $query = $this->db->query("SELECT feedbacks.id as feedback_id, feedbacks.feedback_time, feedbacks.parcel_id, feedbacks.user_location,
                (SELECT concat('{',GROUP_CONCAT('\"',question_id,'\"',':','\"',answer,'\"'),'}') from answers where  answers.feedback_id=feedbacks.id order by answers.feedback_id,answers.question_id asc)answer 
                from feedbacks
                inner join parcels
                on(parcels.p2g_code=feedbacks.p2g_code
                and parcels.upi_code=feedbacks.parcel_id
                and parcels.campaign_id = feedbacks.campaign_id)
                where feedbacks.vendor_id=$vendor_id and feedbacks.campaign_id=$cid $sqlWhere limit $offset, $limit");
        }
        return $query->result_array();
    }

    function getCampaignFeedbacksCountApi($vendor_id, $cid, $offset, $limit, $fromDate = '', $toDate = '') {


        $chkPrcl = $this->isParcelRequiredToShowFeedback($cid);
        $sqlWhere = "";

        if ($chkPrcl == 0) {
            /*
              if ($cids != '') {
              $sqlWhere.=" and campaign_id in($cids)";
              } */
            if ($fromDate != '') {
                $sqlWhere.=" AND feedback_time > '" . $fromDate . "'";
            }
            if ($toDate != '') {
                $sqlWhere.=" AND feedback_time < '" . $toDate . "'";
            }

            $query = $this->db->query("SELECT id as feedback_id,feedback_time,parcel_id,user_location,
							(SELECT GROUP_CONCAT(answer) from answers where feedback_id=feedbacks.id order by feedback_id,question_id asc)answer
		from feedbacks where vendor_id=$vendor_id and campaign_id=$cid $sqlWhere limit $offset, $limit");

            /* $query = $this->db->query("SELECT Feeds.campaign_id, Feeds.question_id,Feeds.feedback_id,group_concat(Feeds.answer)answer,Feeds.feedback_time,Feeds.parcel_id,Feeds.user_location FROM
              (
              SELECT f.campaign_id, a.question_id,a.feedback_id,a.answer,f.feedback_time,f.parcel_id,f.user_location FROM answers a
              INNER JOIN feedbacks f ON f.id=a.feedback_id
              WHERE f.vendor_id=$vendor_id $sqlWhere order by a.question_id asc
              )Feeds GROUP BY Feeds.feedback_id order by Feeds.campaign_id limit $offset, $limit"); */
            //$query = $this->db->query("CALL `getCampaignFeedbacksApi`($vendor_id, $id, $limit, $offset, '$fromDate', '$toDate')");
        } else {
            if ($fromDate != '') {
                $sqlWhere.=" AND feedbacks.feedback_time > '" . $fromDate . "'";
            }
            if ($toDate != '') {
                $sqlWhere.=" AND feedbacks.feedback_time < '" . $toDate . "'";
            }

            $query = $this->db->query("SELECT feedbacks.id as feedback_id, feedbacks.feedback_time, feedbacks.parcel_id, feedbacks.user_location,
                (SELECT concat('{',GROUP_CONCAT('\"',question_id,'\"',':','\"',answer,'\"'),'}') from answers where  answers.feedback_id=feedbacks.id order by answers.feedback_id,answers.question_id asc)answer 
                from feedbacks
                inner join parcels
                on(parcels.p2g_code=feedbacks.p2g_code
                and parcels.upi_code=feedbacks.parcel_id
                and parcels.campaign_id = feedbacks.campaign_id)
                where feedbacks.vendor_id=$vendor_id and feedbacks.campaign_id=$cid $sqlWhere limit $offset, $limit");
        }
        return $query->num_rows();
    }

    function getCampainQuestionsApi($vendor_id, $cid = 0) {
        $sqlWhere = "";
        if ($cid != 0) {
            $sqlWhere.=" and q.campaign_id=$cid";
        }

        $query = $this->db->query("SELECT q.* FROM questions q 
									inner join campaigns c on c.id=q.campaign_id
									WHERE c.vendor_id=$vendor_id $sqlWhere order by q.id asc");
        return $query->result_array();
    }

    function getCampainQuestions($id) {
        $query = $this->db->query("SELECT * FROM questions WHERE campaign_id=$id order by id asc");
        return $query->result_array();
    }

    function getCampaignFeedbacksRows($id, $parcel_id = '') {

        $chkPrcl = $this->isParcelRequiredToShowFeedback($id);
        $sql = "";
        if ($chkPrcl == 0) {
            if ($parcel_id != '') {
                $sql.=" and parcel_id='$parcel_id'";
            }

            $query = $this->db->query("SELECT id as feedback_id,feedback_time,parcel_id,user_location,
									(SELECT GROUP_CONCAT(answer) from answers where feedback_id=feedbacks.id order by feedback_id,question_id asc)answer 
									from feedbacks where campaign_id=$id $sql");
            //$query = $this->db->query("SELECT a.question_id,a.feedback_id,a.id,a.answer,f.feedback_time,f.parcel_id,f.user_location FROM answers a INNER JOIN feedbacks f ON f.id=a.feedback_id WHERE f.campaign_id=$id $sql order by a.feedback_id,a.question_id");
        } else {
            if ($parcel_id != '') {
                $sql.=" and feedbacks.parcel_id='$parcel_id'";
            }

            $query = $this->db->query("SELECT feedbacks.id as feedback_id, feedbacks.feedback_time, feedbacks.parcel_id, feedbacks.user_location,
									(SELECT GROUP_CONCAT(answer) from answers where answers.feedback_id=feedbacks.id order by answers.feedback_id,answers.question_id asc) answer 
								from feedbacks inner join parcels 
on(
parcels.p2g_code=feedbacks.p2g_code
and
parcels.upi_code=feedbacks.parcel_id
and
parcels.campaign_id = feedbacks.campaign_id  
 ) where feedbacks.campaign_id=$id $sql");
        }
        return $query->num_rows();
    }

    function getCampaignFeedbacks($id, $offset, $limit, $parcel_id = '') {
        $chkPrcl = $this->isParcelRequiredToShowFeedback($id);
        $sql = "";

        if ($chkPrcl == 0) {

            if ($parcel_id != '') {
                $sql.=" and parcel_id='$parcel_id'";
            }
            $query = $this->db->query("SELECT id as feedback_id,feedback_time,parcel_id,user_location,
									(SELECT concat('{',GROUP_CONCAT('\"',question_id,'\"',':','\"',answer,'\"'),'}') from answers where feedback_id=feedbacks.id order by question_id,feedback_id asc)answer
									from feedbacks where campaign_id=$id $sql limit $offset, $limit");
            //$query = $this->db->query("SELECT a.question_id,a.feedback_id,a.id,a.answer,f.feedback_time,f.parcel_id,f.user_location FROM answers a INNER JOIN feedbacks f ON f.id=a.feedback_id WHERE f.campaign_id=$id $sql order by a.feedback_id,a.question_id limit $offset, $limit");
            //echo $this->db->last_query();
        } else {

            if ($parcel_id != '') {
                $sql.=" and parcel_id='$parcel_id'";
            }
            $query = $this->db->query("SELECT feedbacks.id as feedback_id, feedbacks.feedback_time, feedbacks.parcel_id, feedbacks.user_location,
									(SELECT concat('{',GROUP_CONCAT('\"',question_id,'\"',':','\"',answer,'\"'),'}') from answers where answers.feedback_id=feedbacks.id order by answers.feedback_id,answers.question_id asc)answer
									from feedbacks inner join parcels 
on(
parcels.p2g_code=feedbacks.p2g_code
and
parcels.upi_code=feedbacks.parcel_id
and
parcels.campaign_id = feedbacks.campaign_id  
 ) where feedbacks.campaign_id=$id $sql limit $offset, $limit");
        }

        return $query->result_array();
    }

    function getFeedbacksToCsv($id) {
        $query = $this->db->query("CALL `getFeedbacksToCsv`($id)");
        return $query->result_array();
    }

    function delCompagin($id) {
        $this->db->delete('campaigns', array('id' => $id));
        $this->db->delete('questions', array('campaign_id' => $id));
    }

    function getAnswerDetail($id) {
        $sql = "select q.question, q.question_type, q.mcq, a.id, a.answer, a.feedback_id from questions as q 
					left join answers a on (a.question_id = q.id) where a.feedback_id=?";

        return $this->db->query($sql, array($id))->result_array();
    }

}