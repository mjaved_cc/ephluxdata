<?php
class Parcel_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	function insert($tbl,$data)
	{
		$this->db->insert($tbl,$data);
	}
	
	function getVendorByKey($key='')
	{
		$query = $this->db->get_where('admins', array('`security_key`' => $key));
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return false;	
		}
		
	}
        
         function IsParcelExists($campaignId,$p2g_code,$upi_code)
	{
		$query = $this->db->get_where('parcels', array('campaign_id'=>$campaignId,'p2g_code'=>$p2g_code,'upi_code'=>$upi_code));
		if($query->num_rows()>0)
		{
			return 'Yes';
		}
		else
		{
			return 'No';	
		}
		
	}
	
	function insert_batch($tbl, $data)
	{
		return $this->db->insert_batch($tbl, $data); 
		
	}
	
}