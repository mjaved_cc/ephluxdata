<?php
class Admin_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	function insert($tbl,$data)
	{
		$this->db->insert($tbl,$data);
	}
	
	function isUserExist($username="", $password="")
	{
		$query = $this->db->get_where('admins', array('`username`' => $username, '`password`' => $password));
		$result = $query->row_array();
		
		return $result;
	}
}