<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
	}
	
	public function checkLogin()
	{
		if(!$this->session->userdata('admin_login'))
		{
			redirect('admin/', 'refresh');
		}
	}
	
}
