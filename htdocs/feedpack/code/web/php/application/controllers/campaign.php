<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Campaign extends Main {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'phpqrcode/Qr'));
        $this->load->library('session');
        $this->load->model('campaign_model');
        $this->load->model('feedback_model');
        $this->load->library('pagination');

        parent::checkLogin();
    }

    public function index() {
        $user_id = $this->session->userdata('logged_in_id');
        $data['campaigns'] = $this->campaign_model->getAllCampaignsByVendorId($user_id);

        $this->load->view('admin/index', $data);
    }

    public function add() {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $fileData['file_name'] = "";
            if ($_FILES['logo']['size'] > 0) {

                //ini_set('upload_max_filesize','10M');
                $config['upload_path'] = './logos/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '1024';
                $config['max_width'] = '';
                $config['max_height'] = '';
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('logo')) {
                    //echo $this->upload->display_errors();
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = $this->upload->data();
                }
                
            }
          
            $header_text = $this->input->post('header_text');
            if (empty($header_text)) {
                $header_text = "Fill us in on your delivery";
            }


            $data = array('campaign_name' => $this->input->post('campaign_name'),
                'recipent_name' => $this->input->post('recipent_name'),
                'creation_date' => $this->input->post('creation_date'),
                'no_of_packages' => $this->input->post('no_of_packages'),
                'campaign_status' => $this->input->post('campaign_status'),
                'show_location_field' => $this->input->post('show_location_field'),
                'campaign_type' => $this->input->post('campaign_type'),
                'vendor_id' => $this->input->post('user_id'),
                'logo' => $fileData['file_name'],
                'header_text' => $header_text,
            );

            $campaign_id = $this->campaign_model->insert('campaigns', $data);

            $questions = $this->input->post('questions');

            if (!empty($questions)) {
                $data2 = array();

                foreach ($questions as $question) {

                    if (isset($question['is_mandatory']) and ($question['is_mandatory'] == 1)) {
                        $is_mandatory = 1;
                    } else {
                        $is_mandatory = 0;
                    }

                    if (isset($question['mcq'])) {
                        $mcq = json_encode($question['mcq']);
                    } elseif (isset($question['dropdown'])) {
                        $mcq = json_encode($question['dropdown']);
                    } else {
                        $mcq = '';
                    }
                    $d = array('question' => $question['name'],
                        'question_type' => $question['type'],
                        'campaign_id' => $campaign_id,
                        'is_mendatory' => $is_mandatory,
                        'mcq' => $mcq);

                    array_push($data2, $d);
                }

                $this->campaign_model->insert_batch('questions', $data2);
            }

            redirect('campaign/detail/' . $campaign_id, 'refresh');
        } else {
            $data['user_id'] = $this->session->userdata('logged_in_id');
            $data['campaign_types'] = $this->campaign_model->getCampaignTypes();
            $this->load->view('admin/add-campaign', $data);
        }
    }

    public function edit($id) {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $header_text = $this->input->post('header_text');
            if (empty($header_text)) {
                $header_text = "Fill us in on your delivery";
            }


            $data = array('campaign_name' => $this->input->post('campaign_name'),
                'recipent_name' => $this->input->post('recipent_name'),
                'creation_date' => $this->input->post('creation_date'),
                'no_of_packages' => $this->input->post('no_of_packages'),
                'campaign_status' => $this->input->post('campaign_status'),
                'show_location_field' => $this->input->post('show_location_field'),
                'campaign_type' => $this->input->post('campaign_type'),
                'vendor_id' => $this->input->post('user_id'),
                'header_text' => $header_text,
            );
            $campaign_id = $this->input->post('id');
            $fileData['file_name'] = "";
            if ($_FILES['logo']['size'] > 0) {

                //ini_set('upload_max_filesize','10M');
                $config['upload_path'] = './logos/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = '1024';
                $config['max_width'] = '';
                $config['max_height'] = '';
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('logo')) {
                    //echo $this->upload->display_errors();
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = $this->upload->data();
                }

                $data['logo'] = $fileData['file_name'];
            }



            $this->db->update('campaigns', $data, "id = $campaign_id");
            $this->db->delete('questions', array('campaign_id' => $campaign_id));

            $questions = $this->input->post('questions');

            if (!empty($questions)) {
                $data2 = array();

                foreach ($questions as $question) {
                    if (isset($question['is_mandatory']) and ($question['is_mandatory'] == 1)) {
                        $is_mandatory = 1;
                    } else {
                        $is_mandatory = 0;
                    }

                    if (isset($question['mcq'])) {
                        $mcq = json_encode($question['mcq']);
                    } elseif (isset($question['dropdown'])) {
                        $mcq = json_encode($question['dropdown']);
                    } else {
                        $mcq = '';
                    }
                    $d = array('question' => $question['name'],
                        'question_type' => $question['type'],
                        'campaign_id' => $campaign_id,
                        'is_mendatory' => $is_mandatory,
                        'mcq' => $mcq);
                    //print_r($d);
                    array_push($data2, $d);
                }

                $this->campaign_model->insert_batch('questions', $data2);
            }

            redirect('campaign/detail/' . $campaign_id, 'refresh');
        }

        $data = $this->feedback_model->getCampaignDetail($id);
        $data['user_id'] = $this->session->userdata('logged_in_id');
        $data['id'] = $id;
        $data['questions'] = $this->feedback_model->getExtraQuestions($id);
        $data['campaign_types'] = $this->campaign_model->getCampaignTypes();
        $this->load->view('admin/edit-campaign', $data);
    }

    public function detail($id) {
        $user_id = $this->session->userdata('logged_in_id');
        $detail = $this->campaign_model->getCampaignDetailNew($id);
        /* echo '<pre>';
          print_r($detail); */
        //foreach($detail as $k=>$v)
        /* for($k=0; $k>4; $k++)
          { */


        if (!isset($detail[0][0]) or empty($detail[0][0])) {
            $this->session->set_flashdata('flashError', 'Invalid Campaign Id Requested.');
            redirect('/campaign/', 'refresh');
        }
        $data = $detail[0][0];

        if ($user_id != $data['vendor_id']) {
            $this->session->set_flashdata('flashError', 'Invalid Campaign Id Requested.');
            redirect('/campaign/', 'refresh');
        }


        /* }
          else
          { */
        if (isset($detail[1])) {
            $parcel = $detail[1];

            if (isset($parcel) and !empty($parcel)) {
                foreach ($parcel as $p) {
                    $yesNo[$p['question']] = isset($p['yes_answers']) ? $p['yes_answers'] : 0;
                }

                $data['yesNo'] = $yesNo;
            } else {
                $data['yesNo'] = array();
            }
        }
        //$ratings = array();
        if (isset($detail[2])) {

            if (isset($detail[2]) and !empty($detail[2])) {
                $parcel = $detail[2];
                foreach ($parcel as $p) {
                    $ratings[$p['id']]['question'] = isset($p['question']) ? $p['question'] : '';
                    $ratings[$p['id']]['avg_rating'] = isset($p['avg_rating']) ? $p['avg_rating'] : '';
                }
            } else {
                $ratings = array();
            }
            $data['ratings'] = $ratings;
        }

        if (isset($detail[3])) {
            $answers = $detail[3];
            //print_r($answers);
            foreach ($answers as $a) {
                //print_r($a);
                $data['ratings'][$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
                //$ans[$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
                //$ratings[$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
            }
            //$ratings[$a[]]['answers'] = $ans;
        }
        //}
        //} 

        $ctype = $this->campaign_model->getCampaignTypeById($data['campaign_type']);
        $data['campaign_type_name'] = $ctype['campaign_type'];
        $data['developerKey'] = $this->session->userdata('developerKey');
        $this->load->view('admin/detail', $data);
    }

    function initPagination() {
        if ($this->uri->segment(4)) {
            $perPage = $this->uri->segment(4);
        } else {
            $perPage = 15;
        }
        $config['per_page'] = $perPage;

        $config['first_link'] = 'First';
        //$config['first_tag_open'] = '<li class="prev page">';
        //$config['first_tag_close'] = '</li>';
        $config['num_links'] = 1;
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $config['cur_tag_open'] = '<span><a tabindex="0" class="paginate_active">';
        $config['cur_tag_close'] = '</a></span>';
        $config['anchor_class'] = 'class="paginate_button"';

        return $config;
    }

    public function feedbacks($id) {
        $parcel_id = $this->input->post('parcel_id');
        if ($this->uri->segment(4)) {
            $perPage = $this->uri->segment(4);
        } else {
            $perPage = 15;
        }
        $config = $this->initPagination();
        $config['base_url'] = base_url() . 'campaign/feedbacks/' . $id . '/'.$perPage.'/';
        $config['total_rows'] = $this->campaign_model->getCampaignFeedbacksRows($id, $parcel_id);
        $config['uri_segment'] = 5;


        $this->pagination->initialize($config);
        $data['page'] = 'feedbacks';
        if ($this->uri->segment(5)) {
            $currentPage = $this->uri->segment(5);
        } else {
            $currentPage = 0;
        }

        $questions = $this->campaign_model->getCampainQuestions($id);
        $feedbacks = $this->campaign_model->getCampaignFeedbacks($id, $currentPage, $config['per_page'], $parcel_id);
        $data['id'] = $id;
        $data['questions'] = $questions;
        $data['per_page'] = $config['per_page'];
        $data['currentPage'] = $currentPage;

        $i = 0;

        $fData = array();

        $ans = array();
        /* foreach ($feedbacks as $k => $v) {

          $fData[$v['feedback_id']] = array('feedback_time' => $v['feedback_time'],
          'parcel_id' => $v['parcel_id'],
          'feedback_id' => $v['feedback_id'],
          'user_location' => $v['user_location']);

          $ans[$v['feedback_id']][] = $v['answer'];


          $i++;
          }
          foreach ($ans as $k => $v) {
          $fData[$k]['answer'] = $v;
          } */

        /* echo 'fData<br><pre>';
          print_r($fData); */

        $data['feedbacks'] = $feedbacks;
        $data['filterVal'] = $parcel_id;

        if ($this->input->is_ajax_request()) {
            $this->load->view('admin/campaign-feedbacks-ajax', $data);
        } else {
            $this->load->view('admin/campaign-feedbacks', $data);
        }
    }

    public function csv($id, $perpage, $currentPage, $parcel_id = '') {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        $inc_count = 1;
        $f = fopen('php://output', 'w');
        $csv_data = array('Feedback ID', 'Feedback Time');
        //print_r($csv_data);
        $csv_data[] = 'Parcel ID';
        $csv_data[] = 'User Location';
        $questions = $this->campaign_model->getCampainQuestions($id);
        foreach ($questions as $question) {
            array_push($csv_data, $question['question']);
        }

        fputcsv($f, $csv_data);

        $config = $this->initPagination();
        $this->pagination->initialize($config);
        /*if ($this->uri->segment(5)) {
            $currentPage = $this->uri->segment(5);
        } else {
            $currentPage = 0;
        }*/
        $feedbacks = $this->campaign_model->getCampaignFeedbacks($id, $currentPage, $perpage, $parcel_id);
         /*echo '<pre>';
          print_r($feedbacks); */
        $i = 0;

        $fData = array();

        $ans = array();
        /* foreach ($feedbacks as $k => $v) {
          $fData[$v['feedback_id']] = array('feedback_id' => $v['feedback_id'],
          'feedback_time' => $v['feedback_time'],
          'parcel_id' => $v['parcel_id'],
          'user_location' => $v['user_location']);

          $ans[$v['feedback_id']][] = $v['answer'];

          $i++;
          }
          //print_r($ans);
          foreach ($ans as $k => $answer) {
          $fData[$k]['answer'] = $answer;
          //array_push($fData[$k]['answer'], $answer);
          //fputcsv($f, $answer);
          }
         */
        foreach ($feedbacks as $k => $v) {
            $output = array('feedback_id' => $v['feedback_id'],
                'feedback_time' => $v['feedback_time'],
                'parcel_id' => $v['parcel_id'],
                'user_location' => $v['user_location']);

            //fputcsv($f, $output);								  
            if (isset($v['answer'])) {
                //$answers = explode(',', $v['answer']);
				$answers = json_decode($v['answer'],true);
                //foreach ($answers as $k => $answer) {
				foreach($questions as $question){
                    //$fData[$k]['answer'] = $answer;
                    array_push($output, $answers[$question['id']]);
                    //fputcsv($f, $answer); 
                }
            }
            fputcsv($f, $output);
        }

        fclose($f);
    }

    public function delete($id) {

        $this->campaign_model->delCompagin($id);
        $user_id = $this->session->userdata('logged_in_id');
        $data['campaigns'] = $this->campaign_model->getAllCampaignsByVendorId($user_id);
        $this->load->view('admin/index', $data);
    }
    
      public function deletefeedback($campaignid,$feedbackid) {
       
         $this->feedback_model->delFeedback($feedbackid);
        redirect(base_url().'campaign/feedbacks/'.$campaignid, 'location');
    
          
    }

    public function answersdetail($id, $cId) {
        $data['questions'] = $this->campaign_model->getAnswerDetail($id);
        $data['id'] = $cId;

    }

}
