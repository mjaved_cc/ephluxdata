<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	 
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('parcel_model');
		$this->load->model('campaign_model');
		$this->load->model('feedback_model');
		
	}
	
	public function index()
	{
		if(isset($_POST['jsonFile'])){
			$contentJson = $_POST['jsonFile'];
			$contentArray = json_decode($contentJson,TRUE);
		}
		
		if(!isset($contentArray['developerKey'])){
			$data['response'] = array('status'=>'failure','description'=>'Invalid Developer Key.');
			$this->load->view('jsonOutput',$data);
		}
		else {
			if($this->verifySecurity($contentArray['developerKey'])){
				if(isset($contentArray['method'])){
					switch ($contentArray['method']) {
						case 'addParcel':
							$resutl = $this->addParcel($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						
						case 'getFeedbacks':
							$resutl = $this->getFeedbacks($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;	
						
						case 'getFeedbacksCount':
							$resutl = $this->getFeedbacksCount($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						case 'campaignsSummary':
							$resutl = $this->campaignsSummary($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						default:
							$data['response'] = array('status'=>'failure','description'=>'Invalid Method Name.');
							$this->load->view('jsonOutput',$data);
							break;
					}
				}
				else{
					$data['response'] = array('status'=>'failure','description'=>'Invalid Method Name.');
					$this->load->view('jsonOutput',$data);
				}
			}
			else {
				$data['response'] = array('status'=>'failure','description'=>'Invalid Developer Key.');
				$this->load->view('jsonOutput',$data);
			}
		}
	}
	
	public function verifySecurity($key)
	{
		//$key = $this->input->post('key');
		$vendor = $this->parcel_model->getVendorByKey($key);
		if($vendor){
			return true;
		}
		else{
			return false;
		}
	}
	
	function checkData($mydate) {
      	if (date('Y-m-d H:i:s', strtotime($mydate)) == $mydate) {
			return true;
		} else {
			return false;
		}
		/*list($yy,$mm,$dd)=explode("-",$mydate);
		$st = checkdate($mydate);
		if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
			//echo 'adf';
			$st = checkdate($mm,$dd,$yy);
		}
		else{
			$st = false;           
		}
		
		return $st;*/
	} 
	
	function validateParams($contentArray){
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		if($cid==0){
			return array('status'=>'failure','description'=>'Campaign Id is Compulsory.');
		}
		if(is_numeric($cid)==false){
			return array('status'=>'failure','description'=>'Invalid Campaign Id.');
		}
		
		if($fromDate!=""){
			//echo $this->checkData($fromDate);
			if($this->checkData($fromDate) ==0){
				return array('status'=>'failure','description'=>'Invalid Date Format.');
			}
		}
		
		if($toDate!=""){
			if( false==$this->checkData($toDate)){
				return array('status'=>'failure','description'=>'Invalid Date Format.');
			}
		}
		
		if(is_numeric($offset)==false){
			return array('status'=>'failure','description'=>'Invalid Offset.');
		}
		
		if(!is_numeric($limit)){
			return array('status'=>'failure','description'=>'Invalid Limit.');
		}
		else if($limit>1000){
			return array('status'=>'failure','description'=>'Limit Count Exceeds Maximum Limit.');
		}	
	}
	
	public function campaignsSummary($contentArray){
		if(!isset($contentArray['campaign_ids']) or (!is_array($contentArray['campaign_ids']))){
			return array('status'=>'failure','description'=>'Invalid Campaign Ids.');	
		}
		
		$dbIds = $this->campaign_model->checkCampaignEsixt(implode(',',$contentArray['campaign_ids']));
		
		foreach($contentArray['campaign_ids'] as $id)
		{
			if(!in_array($id,$dbIds)){
				return array('status'=>'failure','description'=>'Invalid Campaign Ids.');
			}
		}
		$data = array();
		foreach($contentArray['campaign_ids'] as $ids){
			$detail = array();
			$detail[] = $this->campaign_model->getCampaignDetailNew($ids);
			//$detail[] = $this->campaign_model->getCampaignDetail(34);
			//$detail[] = $this->campaign_model->getCampaignDetail(35);
			/*echo '<pre>';
			print_r($detail);*/
			if(!empty($detail[0]))
			{
				foreach($detail as $k=>$v){
					
					$data[$v[0][0]['id']]['no_of_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
					//$data[$v[0][0]['id']]['yes_on_time_delivery'] = (isset($v[0][0]['yes_del']) ? $v[0][0]['yes_del'] : 0).'%';
					
					if(isset($v[1]) and !empty($v[1])){
						$parcel = $v[1];
						unset($yesNo);
						foreach ($parcel as $p) {
							$yesNo[$p['question']] = isset($p['yes_answers']) ? $p['yes_answers'] : 0;
						}
			
						$data[$v[0][0]['id']]['yesNo'] = $yesNo;
					}
					else{
						$data[$v[0][0]['id']]['yesNo'] = array();
					}
					
					if(isset($v[2]) and !empty($v[2])){
						$parcel = $v[2];
						unset($ratings);
						foreach ($parcel as $p) {
							$ratings[$p['id']]['question'] = isset($p['question']) ? $p['question'] : '';
						}
					}
					else {
						$ratings = array();
					}
					$data[$v[0][0]['id']]['ratings'] = $ratings;
					
					if (isset($v[3])) {
						$answers = $v[3];
						//print_r($answers);
						foreach($answers as $a)
						{
							$data[$v[0][0]['id']]['ratings'][$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
						}
					}
				}
			}
		}
		/*echo '<pre>';
		print_r($data);*/
		return array('status'=>'success','description'=>'','data'=>$data);
	}
	
	public function getFeedbacks($contentArray){
		$vendor = $this->parcel_model->getVendorByKey($contentArray['developerKey']);
		$vendor_id = $vendor['id'];	
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		$status = $this->validateParams($contentArray);
		if($status['status']!="failure") {
			$questions = $this->campaign_model->getCampainQuestionsApi($vendor_id, $cid);
			
			/*if($cid==0) {
				$campaignIds = array();
				foreach($questions as $question) {
					if(!in_array($question['campaign_id'], $campaignIds)) {
						$campaignIds[] = $question['campaign_id']; 	
					}
				}
				
				if(!empty($campaignIds)) {
					$cid = implode(',',$campaignIds);
				}
				else {
					$cid = '';	
				}
			}*/
			
			$feedbacks = $this->campaign_model->getCampaignFeedbacksApi( $vendor_id, $cid, $offset, $limit,$fromDate,$toDate );
			$i=0;
			
			$fData = array();
			
			foreach($feedbacks as $k => $v)
			{
				//if($v['question_id']==$feedbacks[$i]['id'])	{
					
				$answers = json_decode($v['answer'],true);//explode(',',$v['answer']);
				$ans = array();
				foreach($questions as $question){
					
					$ans[$question['question']] = isset($answers[$question['id']]) ? $answers[$question['id']] : '' ;
				}
				$v['answer'] = $ans;
				$fData[$k] = $v;
				
				$i++;
				
			}
			/*echo '<pre>';
			print_r($fData);*/
			return array('status'=>'success','description'=>'','data'=>$fData);	
		}
		else{
			return array('status'=>'failure','description'=>$status['description']);
		}
	}
	
	public function getFeedbacksCount($contentArray){
		$vendor = $this->parcel_model->getVendorByKey($contentArray['developerKey']);
		$vendor_id = $vendor['id'];	
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		$status = $this->validateParams($contentArray);
		
		if($status['status']!="failure") {
			$data = $this->campaign_model->getCampaignFeedbacksCountApi( $vendor_id, $cid, $offset, $limit,$fromDate,$toDate );
			return array('status'=>'success','description'=>'','feedbackCount'=>$data);	
		}
		else {
			return array('status'=>'failure','description'=>$status['description']);
		}
	}
	
        /* to update feedbacks  is_parcel_exists*/
          public function updateFeedbacks($data) {
                  foreach($data as $k=>$v){
                     $campaign_id=$v['campaign_id'];
                     $p2g_code=$v['p2g_code'];
                     $upi_code=$v['upi_code'];
                     $sql="update feedbacks set is_parcel_exists='Yes' where campaign_id=$campaign_id and p2g_code=$p2g_code and parcel_id=$upi_code";
                     $this->db->query($sql);
                      
                      
                  }

              
              
              
          }
        
        
        
        
        
        
	public function addParcel($contentArray){
		if(!isset($contentArray['campaign_id'])){
			return array('status'=>'failure','description'=>'Invalid Campaign Id.');	
		}
		else{
			$cDetail = $this->feedback_model->getCampaignDetail($contentArray['campaign_id']);
			if(empty($cDetail)){
				return array('status'=>'failure','description'=>'Invalid Campaign Id.');
			}
			if(!empty($contentArray['parcels'])){
				$p2gError=0;
				$upiCodeError=0;
				foreach($contentArray['parcels'] as $k=>$v){
					if($v['p2g_code']==""){
						$p2gError++;
					}
					elseif($v['upi_code']==""){
						$upiCodeError++;
					}
					else{
						$upiCodes[] = $v['upi_code'];
						$validData[$k] = $v;
					}
				}
				
				if($p2gError>0){
					return array('status'=>'failure','description'=>'P2G Code is Compulsory.');	
				}
				/*if($upiCodeError>0){
					return array('status'=>'failure','description'=>'UPI Code is Compulsory.');	
				}*/
				
				if(!empty($upiCodes)){
					$unique = array_unique($upiCodes); 
					$dupes = array_diff_key( $upiCodes, $unique ); 
					//print_r(array_count_values($dupes));
					$duplicates = array_count_values($dupes);
					if(!empty($duplicates)){
						return array('status'=>'failure','description'=>'Duplicate UPI Codes Exist in Provided Data.');
					}
					else {
						$upi_codes = $this->campaign_model->getUpiCodes($contentArray['campaign_id'],''.implode(',',$upiCodes).'');
					}
					//$upi_codes = $this->campaign_model->getUpiCodes($contentArray['campaign_id'],''.implode(',',$upiCodes).'');
				}
				
				if(!empty($upi_codes) and $upi_codes['upi_code']!=NULL){
					return array('status'=>'failure','description'=>'UPI Codes Already Exist.');	
				}
				
				foreach($contentArray['parcels'] as $k=>$v){
					$data[$k] = $v;
					$data[$k]['campaign_id'] = $contentArray['campaign_id'];
				}
                               
                                
				
				if($this->parcel_model->insert_batch('parcels', $data)){
                                       $this->UpdateFeedBacks($data);
					return array('status'=>'success','description'=>'Parcel data saved.');
				}
				else{
					return array('status'=>'failure','description'=>'Data not saved.');
				}

                                 
			}
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */