<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Terms extends Main {

    public function __construct() {
        parent::__construct();
   
        parent::checkLogin();
    }

    public function index() {
   
        $this->load->view('terms');
    }

   

}
