<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feedback extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $cid;
    public $vendorid;
    public $parcelid;

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('feedback_model');
        $this->load->model('parcel_model');
        $this->load->model('campaign_model');
        $this->load->library('mymemcache');
    }

    public function index() {
        $this->cid = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : 0;
        $this->parcelid = isset($_REQUEST['parcelid']) ? $_REQUEST['parcelid'] : '';
        $this->p2g_code = isset($_REQUEST['p2g_code']) ? $_REQUEST['p2g_code'] : '';
        $key = isset($_REQUEST['key']) ? $_REQUEST['key'] : '';
	$xfeedbacks = isset($_REQUEST['xfeedbacks']) ? $_REQUEST['xfeedbacks'] : false;

        if ($this->cid == 0 or $key == '') {
            $this->load->view('message-error');
        } else {
			
            $vendor = $this->parcel_model->getVendorByKey($key);
 	    $chkCampaign= $this->campaign_model->checkCampaignEsixt($this->cid);		
            if (!isset($vendor) or empty($vendor) or empty($chkCampaign)) {
                $this->load->view('message-error');
            } 
			else {
                                $this->vendorid = $vendor['id'];
				$detail = $this->feedback_model->getCampaignDetail($this->cid);
				$questions = $this->feedback_model->getExtraQuestions($this->cid);
                                $isParcelExists=$this->parcel_model->IsParcelExists($this->cid,$this->p2g_code,$this->parcelid);
				
				if($xfeedbacks==true){
					
						$data = array('cid' => $this->cid,
							'vendorid' => $this->vendorid,
							'parcelid' => $this->parcelid,
							'questions' => $questions,
							'detail' => $detail,
							'p2g_code' => $this->p2g_code,
							'xfeedbacks' => $xfeedbacks,
                                                        'isParcelExists'=>$isParcelExists
						);
						$this->load->view('feedback', $data);
				}
				else if($detail['campaign_type']==2){
					if($this->parcelid=='') {
						$this->load->view('message-error');	
					}
					else{
						if ($this->mymemcache->existsMem($this->cid . "_" . $this->parcelid) == true) {
							$this->load->view('message');
						} elseif ($this->feedback_model->isFeedbackExist($this->cid, $this->parcelid) > 0) {
							$this->load->view('message');
						} else {
		
							$data = array('cid' => $this->cid,
								'vendorid' => $this->vendorid,
								'parcelid' => $this->parcelid,
								'questions' => $questions,
								'detail' => $detail,
								'p2g_code' => $this->p2g_code,
								'xfeedbacks' => $xfeedbacks,
                                                                'isParcelExists'=>$isParcelExists
							);
                                                        
							$this->load->view('feedback', $data);
						}
					}
				}
				else{
					// need to change it using cache.
						$data = array('cid' => $this->cid,
							'vendorid' => $this->vendorid,
							'parcelid' => $this->parcelid,
							'questions' => $questions,
							'detail' => $detail,
							'p2g_code' => $this->p2g_code,
							'xfeedbacks' => $xfeedbacks 
						);

						$this->load->view('feedback', $data);
				}
            }
        }
    }
	
	public function processData($key) {
		$on_time_delivery = $this->input->post('on_time_delivery');
		$parcel_condition = $this->input->post('parcel_condition');
		$behavior = $this->input->post('behavior');
		$customer_email = $this->input->post('customer_email');
		$comment = $this->input->post('comment');
		$answers = $this->input->post('answers');
                $question_type = $this->input->post('question_type');
                $question  = $this->input->post('question');
		$user_location = $this->input->post('user_location');
                $is_parcel_exists = $this->input->post('isParcelExists');

		$data = array('campaign_id' => $this->cid,
			'vendor_id' => $this->vendorid,
			'parcel_id' => $this->parcelid,
			'user_location' => $user_location,
			'p2g_code' =>$this->p2g_code,
                        'is_parcel_exists'=>$is_parcel_exists  
		);

		// insert into cache.		
		//  $feedback_id = $this->feedback_model->insert('feedbacks', $data);

		if (!empty($answers)) {
			foreach ($answers as $k => $v) {
				$data2[] = array('question_id' => $k, 
                                                 'answer' => $v,
                                                 'question_type'=>$question_type[$k],
                                                 'question'=>$question[$k],
                                                 'campaign_id'=>$this->cid
                                           );
			}
			$data['answers'] = $data2;
			//array_push($data,array('answers'=>$data2));
		}
                //echo "<pre>";print_r($data);exit();
		/* if($this->parcelid == '-1'){
		  $key = $this->vendorid . "_" . $this->parcelid;
		  }
		  else{
		  $key = $this->vendorid . "_" . $this->parcelid;
		  } */

		 //echo "<pre>";print_r($data);exit();
		$this->mymemcache->setMem($key, $data);
                $this->mymemcache->dumpMem();
	}
	
    public function add() {

        $this->cid = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : 1;
        $this->vendorid = isset($_REQUEST['vendorid']) ? $_REQUEST['vendorid'] : 0;
        $this->parcelid = isset($_REQUEST['parcelid']) ? $_REQUEST['parcelid'] : 0;
        $this->p2g_code = isset($_REQUEST['p2g_code']) ? $_REQUEST['p2g_code'] : 0;
		$xfeedbacks = isset($_REQUEST['xfeedbacks']) ? $_REQUEST['xfeedbacks'] : false;
		$campaign_type = isset($_REQUEST['campaign_type']) ? $_REQUEST['campaign_type'] : 1;
        // need to change it using cache.
        if ($this->cid == 0 or ($campaign_type==2 and $this->parcelid == 0)) {
            $this->load->view('message-error');
        } else {
			$detail = $this->feedback_model->getCampaignDetail($this->cid);
			if($detail['campaign_status']!="Active") {
				$this->load->view('message-error2');
			}
			else {
			
				if($xfeedbacks==true){
					$key = $this->cid . "_" . $this->parcelid."-".md5(time());
					$this->processData($key);
					$this->load->view('success');
				}
				else
				{
					if($campaign_type==2) {
						$key = $this->cid . "_" . $this->parcelid;
			
						if ($this->mymemcache->existsMem($key) == true) {
							$this->load->view('message');
						} elseif ($this->feedback_model->isFeedbackExist($this->cid, $this->parcelid) > 0) {
							$this->load->view('message');
						} else {
			
							$this->processData($key);
							$this->load->view('success');
						}
						
					}
					else {
						$key = $this->cid."-".md5(time());
						$this->processData($key);
						$this->load->view('success');
					}
				}
				
			}
        }
    }

    public function dumpDatabase() {

        $this->mymemcache->dumpMem();
    }

    public function addtest() {

        for ($i = 1; $i <= 100; $i++) {
            $key = "2_" . $i;
            $data = array('campaign_id' => 3,
                'vendor_id' => 2,
                'parcel_id' => $i,
                'on_time_delivery' => 1,
                'parcel_condition' => 3,
                'behavior' => 0,
                'customer_email' => "test@test.com",
                'comment' => 'hi testing is here',
                'user_location' => 0);

            // insert into cache.		
            //  $feedback_id = $this->feedback_model->insert('feedbacks', $data);
            $this->mymemcache->setMem($key, $data);
        }
    }

    public function addtest2() {

        for ($i = 1; $i <= 100; $i++) {
            $key = "2_" . $i;
            $data = array('campaign_id' => 3,
                'vendor_id' => 2,
                'parcel_id' => $i,
                'on_time_delivery' => 1,
                'parcel_condition' => 3,
                'behavior' => 0,
                'customer_email' => "test@test.com",
                'comment' => 'hi testing is here',
                'user_location' => 0);
            for ($k = 0; $k <= 2; $k++) {

                $data['answers'][] = array('question_id' => 1, 'answer' => $i);
            }

            // insert into cache.		
            //  $feedback_id = $this->feedback_model->insert('feedbacks', $data);
            $this->mymemcache->setMem($key, $data);
        }
    }
    
    public function addtest3() {
                   $campaign_id="21";
                   $vendor_id="1";
                   $dt = date("Y-m-d H:i:s");       
                for ($i = 1; $i <= 50000; $i++) {
                    $parcelId =$i;
                    $p2g_code = $i+2;
                    $sqlFeedback = "insert into feedbacks set feedback_time='$dt',campaign_id='$campaign_id',vendor_id='$vendor_id',parcel_id='$parcelId',p2g_code='$p2g_code'";
                    $this->db->query($sqlFeedback);
                   $feedback_id = $this->db->insert_id();
                    
                   
                    
                    if($i%2==0)
                    {
                    $sqlParcel="insert into parcels set 
                        p2g_code='$p2g_code',
                        upi_code='$parcelId',
                        campaign_id='$campaign_id'";    
                      $this->db->query($sqlParcel);   
                    
                      
                    }  
                   if(rand(0,1)==0)
                   {
                     $yesno="No";    
                   }  else {
                     $yesno="Yes";        
                   }      
                    
                 $sqlQ1= "insert into answers 
                  set
                  question_id ='94', 
                  answer='$yesno',
                  feedback_id='$feedback_id'";
                  $this->db->query($sqlQ1);
                  
                  $rating=rand(0,5);
                  $sqlQ2="insert into answers 
                  set
                  question_id ='93', 
                  answer='$rating',
                  feedback_id='$feedback_id'";
                  $this->db->query($sqlQ2);  
                 echo  $feedback_id;
                 echo "<br>";
              
        }
    
        
        
    }
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */