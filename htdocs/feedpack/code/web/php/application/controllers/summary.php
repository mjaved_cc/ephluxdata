<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Summary extends Main {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('campaign_model');
        $this->load->library('pagination');

        parent::checkLogin();
    }

    public function fillValues($arr) {
     
        $key = array(1, 2, 3, 4, 5);

        foreach ($key as $k) {
            if (!array_key_exists($k, $arr)) {
                $arr[$k] = 0;
            }
        }
        return $arr;
    }

    public function index() {

ni_set('max_execution_time', 30*60);
        $cIds = $this->db->query("select id from campaigns where campaign_status='active'")->result_array();

        //echo "<pre>";print_r($cIds);
        // exit();
        //$cIds = array('0'=>'1','1'=>'34','2'=>'35');


        if (!empty($cIds)) {
            $c = 0;
            foreach ($cIds as $cId) {
                $campaignId = $cId['id'];
                $detail[] = $this->campaign_model->getCampaignDetailNew($campaignId);
                $c++;

                foreach ($detail as $k => $v) {
                    /*
                      $data['compare'][$v[0][0]['id']]['id'] = isset($v[0][0]['id']) ? $v[0][0]['id'] : 0;
                      $data['compare'][$v[0][0]['id']]['recipent_name'] = isset($v[0][0]['recipent_name']) ? $v[0][0]['recipent_name'] : 0;
                      $data['compare'][$v[0][0]['id']]['show_location_field'] = isset($v[0][0]['show_location_field']) ? $v[0][0]['show_location_field'] : 0;
                      $data['compare'][$v[0][0]['id']]['is_email_mandatory'] = isset($v[0][0]['id']) ? $v[0][0]['is_email_mandatory'] : 0;
                      $data['compare'][$v[0][0]['id']]['total_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
                      $data['compare'][$v[0][0]['id']]['campaign_status'] = isset($v[0][0]['campaign_status']) ? $v[0][0]['campaign_status'] : 0;
                      $data['compare'][$v[0][0]['id']]['campaign_name'] = isset($v[0][0]['campaign_name']) ? $v[0][0]['campaign_name'] : 0;


                      $data['compare'][$v[0][0]['id']]['no_of_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
                     * 
                     */
                    $totalResponse = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;

                    if (isset($v[1]) and !empty($v[1])) {
                        $parcel = $v[1];
                        unset($yesNo);
                        foreach ($parcel as $p) {
                            $yesNo[$p['question']] = isset($p['yes_answers']) ? $p['yes_answers'] : '';
                        }

                        $data['compare'][$v[0][0]['id']]['yesNo'] = $yesNo;
                    } else {
                        $data['compare'][$v[0][0]['id']]['yesNo'] = array();
                    }

                    if (isset($v[2]) and !empty($v[2])) {
                        $parcel = $v[2];

                        unset($ratings);
                        $ratings = array();
                        foreach ($parcel as $p) {
                            $ratings[$p['id']]['question'] = isset($p['question']) ? $p['question'] : '';
                            $ratings[$p['id']]['avg_rating'] = isset($p['avg_rating']) ? $p['avg_rating'] : '';
                        }
                    } else {
                        $ratings = array();
                    }
                    $data['compare'][$v[0][0]['id']]['ratings'] = $ratings;

                    if (isset($v[3])) {
                        $answers = $v[3];
                        //print_r($answers);
                        foreach ($answers as $a) {
                            $data['compare'][$v[0][0]['id']]['ratings'][$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
                            $ratings[$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
                        }
                    }
                }

               $this->db->query("delete from campaign_summary where cid =$campaignId");


                $sqlTotalResponse = "insert into campaign_summary set cid=$campaignId,fieldName='total_response', fieldValue='$totalResponse',dataType='TotalResponse'";
                $this->db->query($sqlTotalResponse);
                // echo $campaignId;
                if (isset($yesNo) && !empty($yesNo)) {
                    foreach ($yesNo as $k => $v) {

                        $sqlYesNo = "insert into campaign_summary set cid=$campaignId,fieldName='$k', fieldValue='$v',dataType='YesNo'";

                        $this->db->query($sqlYesNo);
                    }
                }

                if (isset($ratings) && !empty($ratings)) {
                    //  echo "<pre>";print_r($ratings);
                    foreach ($ratings as $k => $v) {
                        $question = $v['question'];
                        $avg = $v['avg_rating'];
                        $json = json_encode($this->fillValues($v['answers']));
                        $sqlRating = "insert into campaign_summary set cid=$campaignId,fieldName='$question', fieldValue='$avg',jsonText='$json', dataType='5star'";

                        $this->db->query($sqlRating);
                    }
                }
            }
        }


    }

}
