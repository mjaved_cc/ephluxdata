<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parcel extends Main {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('parcel_model');
        $this->load->model('campaign_model');
    }

    public function index() {
        /* if(!$this->session->userdata('security_key')){
          $data = array('error'=>'');
          $this->load->view('admin/api/security',$data);
          }
          else{
          redirect('parcel/add', 'refresh');
          } */
        redirect('parcel/add', 'refresh');
    }

    public function verifySecurity($key) {
        //$key = $this->input->post('key');
        $vendor = $this->parcel_model->getVendorByKey($key);
        if ($vendor) {
            return true;
        } else {
            return false;
        }
    }

    /* to update feedbacks  is_parcel_exists */

    public function updateFeedbacks($data) {
        foreach ($data as $k => $v) {
            $campaign_id = $v['campaign_id'];
            $p2g_code = $v['p2g_code'];
            $upi_code = $v['upi_code'];
    $sql = "update feedbacks set is_parcel_exists='Yes' where campaign_id=$campaign_id and p2g_code=$p2g_code and parcel_id=$upi_code";
            $this->db->query($sql);
        }

    }

    public function processParcel($contentJson) {
        $contentArray = json_decode($contentJson, TRUE);

        if ($this->verifySecurity($contentArray['developerKey'])) {
            if (!isset($contentArray['campaign_id'])) {
                $this->session->set_flashdata('flashError', 'Invalid Campaign Id.');
                redirect('parcel/add/', 'refresh');
            } else {
                if (!empty($contentArray['parcels'])) {
                    $p2gError = 0;
                    $upiCodeError = 0;
                    foreach ($contentArray['parcels'] as $k => $v) {
                        if ($v['p2g_code'] == "") {
                            $p2gError++;
                        } elseif ($v['upi_code'] == "") {
                            $upiCodeError++;
                        } else {
                            $upiCodes[] = $v['upi_code'];
                            $validData[$k] = $v;
                        }
                    }

                    if ($p2gError > 0) {
                        $this->session->set_flashdata('flashError', 'P2G Code is Compulsory.');
                        redirect('parcel/add/', 'refresh');
                    }
                    /* if($upiCodeError>0){
                      $this->session->set_flashdata('flashError', 'UPI Code is Compulsory.');
                      redirect('parcel/add/', 'refresh');
                      } */

                    if (!empty($upiCodes)) {
                        $unique = array_unique($upiCodes);
                        $dupes = array_diff_key($upiCodes, $unique);
                        //print_r(array_count_values($dupes));
                        $duplicates = array_count_values($dupes);
                        if (!empty($duplicates)) {
                            $this->session->set_flashdata('flashError', 'Duplicate UPI Codes Exist in Provided Data.');
                            redirect('parcel/add/', 'refresh');
                        } else {
                            $upi_codes = $this->campaign_model->getUpiCodes($contentArray['campaign_id'], '' . implode(',', $upiCodes) . '');
                        }
                    }

                    if (!empty($upi_codes) and $upi_codes['upi_code'] != NULL) {
                        $this->session->set_flashdata('flashError', 'UPI Codes Already Exist.');
                        redirect('parcel/add/', 'refresh');
                    }

                    foreach ($contentArray['parcels'] as $k => $v) {
                        $data[$k] = $v;
                        $data[$k]['campaign_id'] = $contentArray['campaign_id'];
                    }

                    if ($this->parcel_model->insert_batch('parcels', $data)) {
                             $this->UpdateFeedBacks($data);
                        $this->session->set_flashdata('flashSuccess', 'Parcel data saved.');
                        redirect('parcel/add/', 'refresh');
                    } else {
                        $this->session->set_flashdata('flashSuccess', 'Data not saved.');
                        redirect('parcel/add/', 'refresh');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('flashError', 'Security key is invalid');
            redirect('parcel/add/', 'refresh');
        }
    }

    public function add() {
        parent::checkLogin();
        if (($_SERVER['REQUEST_METHOD'] == "POST")) {
            if (isset($_FILES['jsonFile']['tmp_name']) and is_uploaded_file($_FILES['jsonFile']['tmp_name'])) {
                $contentJson = file_get_contents($_FILES['jsonFile']['tmp_name']);
                $this->processParcel($contentJson);
            }
        } else {
            $data = array('error' => '');
            $this->load->view('admin/api/upload-file', $data);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */