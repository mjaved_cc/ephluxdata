<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Combine extends Main {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('campaign_model');
        $this->load->library('pagination');

        parent::checkLogin();
    }

    public function index() {
        $user_id = $this->session->userdata('logged_in_id');
    
        $data['campaigns'] = $this->campaign_model->getAllCampaignsByVendorId($user_id);



        $this->load->view('admin/combine', $data);
    }
    
    public function getQuestion($id,$cnt,$campaign){
           $campaign = urldecode($campaign);
           $data = $this->db->query("select * from questions where campaign_id = $id")->result_array();
           if(!empty($data)) {
               
               
               
                            $html = "<h3>$campaign</h3><div><p>";   
                            $c=1;
                           foreach($data as $k) {
                              
                            $html .= $c.".  <input type='radio' name='c".$id."' value='".$k['question']."|".$k['id']."|".$id."'onClick='addQuestion(this.value)' >  ".$k['question']."<br>";   
                               $c++;
                               
                           }
               
               
               

                                   
                              $html.="</p></div>";
                                             
               
               
               
              echo  $html;
               
           }
            
        
        
        
    }

   

}
