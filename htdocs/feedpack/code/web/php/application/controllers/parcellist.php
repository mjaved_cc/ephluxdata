<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parcellist extends Main {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('campaign_model');
        $this->load->library('pagination');

        parent::checkLogin();
    }

    public function index() {
        $user_id = $this->session->userdata('logged_in_id');
        //  $detail = $this->campaign_model->getCampaignDetail('6');
        $data['campaigns'] = $this->campaign_model->getAllCampaignsByVendorId($user_id);
        $data['selected'][0] = 0;


        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $cId = $_POST['cId'];
            $data['selected'][0] = $cId;
            $sql = "select
                        id,
                        sender_fullname,
                        sender_address,
                        p2g_code,
                        upi_code,
                        destination,
                        service_name,
                        carrier_name
                    from
                        parcels 
                    where 
                        campaign_id = $cId";
            $data['parcels'] = $this->db->query($sql)->result_array();
        }

        $this->load->view('admin/parcellist', $data);
    }


        public function csv($id) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=parcellist.csv');
        $inc_count = 1;
        $f = fopen('php://output', 'w');
        $csv_data[] = 'ID';
        $csv_data[] = 'Sender Name';
        $csv_data[] = 'Sender Dob';
        $csv_data[] = 'Sender Address';
        $csv_data[] = 'Receiver Name';
        $csv_data[] = 'Receiver Dob';
        $csv_data[] = 'Receiver Address';
        $csv_data[] = 'p2g code';
        $csv_data[] = 'upi code';
        $csv_data[] = 'Destination';
        $csv_data[] = 'Parcel Weight';
        $csv_data[] = 'Parcel Weight Unit';
        $csv_data[] = 'Parcel Dimensions';
        $csv_data[] = 'Parcel Dimensions Unit';
        $csv_data[] = 'Parcel Type';
        $csv_data[] = 'Parcel Insurance Value';
        $csv_data[] = 'Parcel Service Used';
        $csv_data[] = 'Campaign Id';
        $csv_data[] = 'Service Name';
        $csv_data[] = 'Carrier Name';
        fputcsv($f, $csv_data);
           $sql = "select
                      *
                    from
                        parcels 
                    where 
                        campaign_id = $id";
            $result = $this->db->query($sql)->result_array();
              foreach($result as $k)
              {
                 $output = array(
                           'id'=>$k['id'],
                            'sender_fullname'=>$k['sender_fullname'],
                            'sender_dob'=>$k['sender_dob'],
                            'sender_address'=>$k['sender_address'],
                            'receiver_fullname'=>$k['receiver_fullname'],
                            'receiver_dob'=>$k['receiver_dob'],
                            'receiver_address'=>$k['receiver_address'],
                            'p2g_code'=>$k['p2g_code'],
                            'upi_code'=>$k['upi_code'],
                            'destination'=>$k['destination'],
                            'parcel_weight'=>$k['parcel_weight'],
                            'parcel_weight_unit'=>$k['parcel_weight_unit'],
                            'parcel_dimensions'=>$k['parcel_dimensions'],
                            'parcel_dimensions_unit'=>$k['parcel_dimensions_unit'],
                            'parcel_type'=>$k['parcel_type'],
                            'parcel_insurance_value'=>$k['parcel_insurance_value'],
                            'parcel_service_used'=>$k['parcel_service_used'],
                            'campaign_id'=>$k['campaign_id'],
                            'service_name'=>$k['service_name'],
                            'carrier_name'=>$k['carrier_name']
                          ); 
                   fputcsv($f, $output);
                  
              }

            
            //fputcsv($f, $output);
        
          //fputcsv($f, $output);
       fclose($f);
    }
    
    
    
}
