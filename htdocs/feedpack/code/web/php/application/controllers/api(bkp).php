<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	 
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('parcel_model');
		$this->load->model('campaign_model');
		$this->load->model('feedback_model');
		
	}
	
	public function index()
	{
		if(isset($_POST['jsonFile'])){
			$contentJson = $_POST['jsonFile'];
			$contentArray = json_decode($contentJson,TRUE);
		}
		
		if(!isset($contentArray['developerKey'])){
			$data['response'] = array('status'=>'failure','description'=>'Invalid Developer Key.');
			$this->load->view('jsonOutput',$data);
		}
		else {
			if($this->verifySecurity($contentArray['developerKey'])){
				if(isset($contentArray['method'])){
					switch ($contentArray['method']) {
						case 'addParcel':
							$resutl = $this->addParcel($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						
						case 'getFeedbacks':
							$resutl = $this->getFeedbacks($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;	
						
						case 'getFeedbacksCount':
							$resutl = $this->getFeedbacksCount($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						case 'campaignsSummary':
							$resutl = $this->campaignsSummary($contentArray);
							$data['response'] = $resutl;
							$this->load->view('jsonOutput',$data);
							
							break;
						default:
							$data['response'] = array('status'=>'failure','description'=>'Invalid Method Name.');
							$this->load->view('jsonOutput',$data);
							break;
					}
				}
				else{
					$data['response'] = array('status'=>'failure','description'=>'Invalid Method Name.');
					$this->load->view('jsonOutput',$data);
				}
			}
			else {
				$data['response'] = array('status'=>'failure','description'=>'Invalid Developer Key.');
				$this->load->view('jsonOutput',$data);
			}
		}
	}
	
	public function verifySecurity($key)
	{
		//$key = $this->input->post('key');
		$vendor = $this->parcel_model->getVendorByKey($key);
		if($vendor){
			return true;
		}
		else{
			return false;
		}
	}
	
	function checkData($mydate) {
      
		list($yy,$mm,$dd)=explode("-",$mydate);
		
		if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
			//echo 'adf';
			$st = checkdate($mm,$dd,$yy);
		}
		else{
			$st = false;           
		}
		
		return $st;
	} 
	
	function validateParams($contentArray){
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		if(is_numeric($cid)==false){
			return array('status'=>'failure','description'=>'Invalid Campaign Id.');
		}
		
		if($fromDate!=""){
			//echo $this->checkData($fromDate);
			if($this->checkData($fromDate) ==0){
				return array('status'=>'failure','description'=>'Invalid Date Format.');
			}
		}
		
		if($toDate!=""){
			if( false==$this->checkData($toDate)){
				return array('status'=>'failure','description'=>'Invalid Date Format.');
			}
		}
		
		if(is_numeric($offset)==false){
			return array('status'=>'failure','description'=>'Invalid Offset.');
		}
		
		if(!is_numeric($limit)){
			return array('status'=>'failure','description'=>'Invalid Limit.');
		}
		else if($limit>1000){
			return array('status'=>'failure','description'=>'Limit Count Exceeds Maximum Limit.');
		}	
	}
	
	public function campaignsSummary($contentArray){
		if(!isset($contentArray['campaign_ids']) or (!is_array($contentArray['campaign_ids']))){
			return array('status'=>'failure','description'=>'Invalid Campaign Ids.');	
		}
		$data = array();
		foreach($contentArray['campaign_ids'] as $ids){
			$detail = array();
			$detail[] = $this->campaign_model->getCampaignDetailNew($ids);
			//$detail[] = $this->campaign_model->getCampaignDetail(34);
			//$detail[] = $this->campaign_model->getCampaignDetail(35);
			
			if(!empty($detail[0]))
			{
				foreach($detail as $k=>$v){
					
					$data[$v[0][0]['id']]['no_of_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
					$data[$v[0][0]['id']]['yes_on_time_delivery'] = (isset($v[0][0]['yes_del']) ? $v[0][0]['yes_del'] : 0).'%';
					
					if(isset($v[1]) and !empty($v[1])){
						$parcel = $v[1];
						foreach($parcel as $p){
							$parcel_conditions[$p['parcel_condition']] = $p['tot'];
						}
						
						for($i=5; $i>0; $i--){
							if(isset($parcel_conditions[$i])){
								$record[$i] = $parcel_conditions[$i];
							}
							else{
								$record[$i] = 0;
							}
						}
						
						$data[$v[0][0]['id']]['parcel_condition_rating'] = $record;
					}
					else{
						$data[$v[0][0]['id']]['parcel_condition_rating'] = array(5=>0,4=>0,3=>0,2=>0,1=>0);
					}
					if(isset($v[2]) and !empty($v[2])){
						$parcel = $v[2];
						
						foreach($parcel as $p){
							$behavior[$p['behavior']] = $p['tot'];
						}
						
						for($i=5; $i>0; $i--){
							if(isset($behavior[$i])){
								$record[$i] = $behavior[$i];
							}
							else{
								$record[$i] = 0;
							}
						}
						
						$data[$v[0][0]['id']]['behavior_rating'] = $record;
					}
					else{
						$data[$v[0][0]['id']]['behavior_rating'] = array(5=>0,4=>0,3=>0,2=>0,1=>0);
					}
				}
			}
		}
		//print_r($data);
		return array('status'=>'success','description'=>'','data'=>$data);
	}
	
	public function getFeedbacks($contentArray){
		$vendor = $this->parcel_model->getVendorByKey($contentArray['developerKey']);
		$vendor_id = $vendor['id'];	
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		$this->validateParams($contentArray);
		
		$data = $this->campaign_model->getCampaignFeedbacksApi( $vendor_id, $cid, $offset, $limit,$fromDate,$toDate );
		return array('status'=>'success','description'=>'','data'=>$data);	
	}
	
	public function getFeedbacksCount($contentArray){
		$vendor = $this->parcel_model->getVendorByKey($contentArray['developerKey']);
		$vendor_id = $vendor['id'];	
		$offset = isset($contentArray['offset']) ? $contentArray['offset'] : 0 ;
		$limit = isset($contentArray['limit']) ? $contentArray['limit'] : 50 ;
		$fromDate = (isset($contentArray['fromDate']) and $contentArray['fromDate']!="") ? $contentArray['fromDate'] : '' ;
		$toDate = (isset($contentArray['toDate']) and $contentArray['toDate']!="") ? $contentArray['toDate'] : '' ;
		$cid = (isset($contentArray['campaign_id']) and $contentArray['campaign_id']!="") ? $contentArray['campaign_id'] : 0;
		
		$this->validateParams($contentArray);
		
		$data = $this->campaign_model->getCampaignFeedbacksCountApi( $vendor_id, $cid, $offset, $limit,$fromDate,$toDate );
		return array('status'=>'success','description'=>'','feedbackCount'=>$data);	
	}
	
	public function addParcel($contentArray){
		if(!isset($contentArray['campaign_id'])){
			return array('status'=>'failure','description'=>'Invalid Campaign Id.');	
		}
		else{
			$cDetail = $this->feedback_model->getCampaignDetail($contentArray['campaign_id']);
			if(empty($cDetail)){
				return array('status'=>'failure','description'=>'Invalid Campaign Id.');
			}
			if(!empty($contentArray['parcels'])){
				$p2gError=0;
				$upiCodeError=0;
				foreach($contentArray['parcels'] as $k=>$v){
					if($v['p2g_code']==""){
						$p2gError++;
					}
					elseif($v['upi_code']==""){
						$upiCodeError++;
					}
					else{
						$upiCodes[] = $v['upi_code'];
						$validData[$k] = $v;
					}
				}
				
				if($p2gError>0){
					return array('status'=>'failure','description'=>'P2G Code is Compulsory.');	
				}
				if($upiCodeError>0){
					return array('status'=>'failure','description'=>'UPI Code is Compulsory.');	
				}
				
				if(!empty($upiCodes)){
					$upi_codes = $this->campaign_model->getUpiCodes($contentArray['campaign_id'],''.implode(',',$upiCodes).'');
				}
				
				if(!empty($upi_codes) and $upi_codes['upi_code']!=NULL){
					return array('status'=>'failure','description'=>'UPI Codes Already Exist.');	
				}
				
				foreach($contentArray['parcels'] as $k=>$v){
					$data[$k] = $v;
					$data[$k]['campaign_id'] = $contentArray['campaign_id'];
				}
				
				if($this->parcel_model->insert_batch('parcels', $data)){
					return array('status'=>'success','description'=>'Parcel data saved.');
				}
				else{
					return array('status'=>'failure','description'=>'Data not saved.');
				}
			}
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */