<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Compare extends Main {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('campaign_model');
        $this->load->library('pagination');

        parent::checkLogin();
    }

    public function index() {
        $user_id = $this->session->userdata('logged_in_id');
        //  $detail = $this->campaign_model->getCampaignDetail('6');
        $data['campaigns'] = $this->campaign_model->getAllCampaignsByVendorId($user_id);
        $data['selected'][0] = 0;
        $data['selected'][1] = 0;
        $data['selected'][2] = 0;

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            
            $cIds = $_POST['cId'];
            //$cIds = array('0'=>'1','1'=>'34','2'=>'35');
     
            $c = 0;
            if (!empty($cIds)) {
                foreach ($cIds as $cId) {
                    $data['selected'][$c] = $cId;
                    $detail[]=$this->campaign_model->getCampaignDetailNew($cId);
                    $c++;
          
                    foreach($detail as $k=>$v){       
						
						$data['compare'][$v[0][0]['id']]['id'] = isset($v[0][0]['id']) ? $v[0][0]['id'] : 0;
						$data['compare'][$v[0][0]['id']]['recipent_name'] = isset($v[0][0]['recipent_name']) ? $v[0][0]['recipent_name'] : 0;
						$data['compare'][$v[0][0]['id']]['show_location_field'] = isset($v[0][0]['show_location_field']) ? $v[0][0]['show_location_field'] : 0;
						$data['compare'][$v[0][0]['id']]['is_email_mandatory'] = isset($v[0][0]['id']) ? $v[0][0]['is_email_mandatory'] : 0;
						$data['compare'][$v[0][0]['id']]['total_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
						$data['compare'][$v[0][0]['id']]['campaign_status'] = isset($v[0][0]['campaign_status']) ? $v[0][0]['campaign_status'] : 0;
                        $data['compare'][$v[0][0]['id']]['campaign_name'] = isset($v[0][0]['campaign_name']) ? $v[0][0]['campaign_name'] : 0;
                                                
					
					$data['compare'][$v[0][0]['id']]['no_of_response'] = isset($v[0][0]['total_response']) ? $v[0][0]['total_response'] : 0;
					//$data[$v[0][0]['id']]['yes_on_time_delivery'] = (isset($v[0][0]['yes_del']) ? $v[0][0]['yes_del'] : 0).'%';
			
					if(isset($v[1]) and !empty($v[1])){
						$parcel = $v[1];
						unset($yesNo);
						foreach ($parcel as $p) {
							$yesNo[$p['question']] = isset($p['yes_answers']) ? $p['yes_answers'] : 0;
						}
			
						$data['compare'][$v[0][0]['id']]['yesNo'] = $yesNo;
					}
					else{
						$data['compare'][$v[0][0]['id']]['yesNo'] = array();
					}
					
					if(isset($v[2]) and !empty($v[2])){
						$parcel = $v[2];
                                               
						unset($ratings);
                        $ratings = array();
						foreach ($parcel as $p) {
							$ratings[$p['id']]['question'] = isset($p['question']) ? $p['question'] : '';
							$ratings[$p['id']]['avg_rating'] = isset($p['avg_rating']) ? $p['avg_rating'] : '';
						}
					}
					else {
						$ratings = array();
					}
					$data['compare'][$v[0][0]['id']]['ratings'] = $ratings;
					
					if (isset($v[3])) {
						$answers = $v[3];
						//print_r($answers);
						foreach($answers as $a)
						{
							$data['compare'][$v[0][0]['id']]['ratings'][$a['id']]['answers'][$a['answer']] = $a['yes_answers'];
						}
					}
			}
                    
                    
                }
            }
        }
      //echo "<pre>";print_r($data);
        $this->load->view('admin/compare', $data);
    }

   

}
