<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('campaign_model');
		$this->load->model('admin_model');
		
		//$this->index();
	}
	
	public function index()
	{
		if(!$this->session->userdata('admin_login'))
		{
			$data = array('error'=>'');
			$this->load->view('admin/login', $data);
		}
		else
		{
			redirect('campaign/', 'refresh');	
		}
	}
	
	public function add()
	{
		if($_SERVER['REQUEST_METHOD']=="POST")
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
		}
	}
	
	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$userData = $this->admin_model->isUserExist($username, $password);
		if(!empty($userData))
		{
			$newdata = array(
			   'admin_name'  => $this->input->post('username'),
			   'logged_in_id'  => $userData['id'],
			   'developerKey'  => $userData['security_key'],
			   'admin_login' => TRUE
			);
			$this->session->set_userdata($newdata);	
			redirect('campaign/', 'refresh');
		}
		else
		{
			$data = array('error'=>'User not exist.');
			$this->load->view('admin/login', $data);
		}
	}
	
	public function logout()
	{
		$newdata = array(
		   'admin_name'  => '',
		   'logged_in_id'  => '',
		   'admin_login' => false
		);
		$this->session->set_userdata($newdata);	
		redirect('admin/', 'refresh');
	}
	
}
