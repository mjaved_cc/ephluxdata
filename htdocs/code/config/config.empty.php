<?php

  /**
   * Configuration file used by activeCollab before system is installed
   */

  define('ROOT', realpath(CONFIG_PATH . '/../activecollab'));
  define('ROOT_URL', 'http://activecollab.dev/public');

  define('FORCE_ROOT_URL', false);
  
  if(!defined('CONFIG_PATH')) {
    define('CONFIG_PATH', dirname(__FILE__));
  } // if
  
    define('USE_UNPACKED_FILES', true);

  require_once CONFIG_PATH . '/license.php';
  require_once CONFIG_PATH . '/version.php';
  require_once CONFIG_PATH . '/defaults.php';