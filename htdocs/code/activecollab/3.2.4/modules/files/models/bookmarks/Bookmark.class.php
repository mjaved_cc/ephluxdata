<?php

  /**
   * Bookmark added to project assets area
   *
   * @package activeCollab.modules.files
   * @subpackage models
   */
  class Bookmark extends ProjectAsset implements ISharing {
    /**
     * Field map
     *
     * @var array
     */
    var $field_map = array(
      'bookmark_url' => 'varchar_field_1'
    );
    
    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------
    
    /**
     * Returns bookmark url
     * 
     * @return string
     */
    function getBookmarkUrl() {
    	return $this->getVarcharField1();
    } // getBookmarkUrl
    
    /**
     * Sets the bookmark url
     * 
     * @param string $value
     * @return string
     */
    function setBookmarkUrl($value) {
    	return $this->setVarcharField1($value);
    } // setBookmarkUrl
    
    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------

    /**
     * Cached preview interface
     * 
     * @param IBookmarkPreviewImplementation
     */
    private $preview = false;

    /**
     * Return preview helper
     *
     * @return IDownloadPreviewImplementation
     */
    function preview() {
      if($this->preview === false) {
        $this->preview = new IBookmarkPreviewImplementation($this);
      } // if
      
      return $this->preview;
    } // preview
    
    /**
     * Cached sharing implementation helper
     *
     * @var IBookmarkSharingImplementation
     */
    private $sharing = false;
    
    /**
     * Return sharing helper instance
     * 
     * @return IBookmarkSharingImplementation
     */
    function sharing() {
      if($this->sharing === false) {
        $this->sharing = new IBookmarkSharingImplementation($this);
      } // if
      
      return $this->sharing;
    } // sharing
    
    /**
     * Return history helper instance
     * 
     * @return IHistoryImplementation
     */
    function history() {
      return parent::history()->alsoTrackFields('varchar_field_1');
    } // history
    
    // ---------------------------------------------------
    //  URL-s
    // ---------------------------------------------------
    
    /**
     * Return view asset URL
     *
     * @return string
     */
    function getViewUrl() {
      return Router::assemble('project_assets_bookmark', array(
        'project_slug' => $this->getProject()->getSlug(),
        'asset_id' => $this->getId(),
      ));
    } // getViewUrl
    
    /**
     * Return icon URL
     *
     * @return string
     */
    function getIconUrl() {
      return AngieApplication::getImageUrl('icons/32x32/bookmark.png', FILES_MODULE, AngieApplication::INTERFACE_PHONE);
    } // getIconUrl
    
    // ---------------------------------------------------
    //  SYSTEM
    // ---------------------------------------------------
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $result['bookmark_url'] = $this->getBookmarkUrl(); 
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      $result = parent::describeForApi($user, $detailed);

      $result['bookmark_url'] = $this->getBookmarkUrl();

      return $result;
    } // describeForApi
    
    /**
     * Validate before save
     *
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->validatePresenceOf('bookmark_url', 3)) {
        $errors->addError(lang('Bookmark URL is required'), 'bookmark_url');
      } // if
      
      if (!is_valid_Url($this->getFieldValue('bookmark_url'))) {
      	$errors->addError(lang('Bookmark URL is not valid'), 'bookmark_url');
      } // if
      
      parent::validate($errors, true);
    } // validate    
  }