<?php 

  /**
   * YouTube video preview implementation
   * 
   * @package activeCollab.modules.files
   * @subpackage models
   */
	class IYoutubePreviewImplementation extends IPreviewImplementation {
	  
    /**
     * Construct download preview implementation
     *
     * @param IPreview $object
     */
    function __construct(IPreview $object) {
      if($object instanceof YouTubeVideo) {
        parent::__construct($object);
      } else {
        throw new InvalidInstanceError('object', $object, 'YouTubeVideo');
      } // if
    } // __construct
    		
		/**
		 * Every youtube video has preview
		 * 
		 * @return boolean
		 */
		function has() {
			return true;
		} // has
		
		/**
		 * YouTube previews are not email friendly since they use IFRAME-s
		 * 
		 * @return boolean
		 */
		function isEmailFriendly() {
		  return false;
		} // isEmailFriendly
		
    /**
     * Render small preview
     *
     * @return string
     */
    function renderSmall() {
      return $this->renderPreview(80, 80);
    } // renderSmall
    
    /**
     * Render large preview
     *
     * @return string
     */
    function renderLarge() {
      return $this->renderPreview(550, 335);
    } // renderLarge
    
    /**
     * Renders the small icon url
     * 
     * @return string
     */
    function getSmallIconUrl() {
    	return AngieApplication::getImageUrl('icons/16x16/youtube-video.png', FILES_MODULE);
    } // getSmallIconUrl
    
    /**
     * Returns the large icon
     * 
		 * @return string
     */
    function getLargeIconUrl() {
    	return AngieApplication::getImageUrl('icons/32x32/youtube-video.png', FILES_MODULE);
    } // getLargeIconUrl
    
    /**
     * Do the render
     * 
     * @param integer $width
     * @param integer $height
     * @return string
     */
    function renderPreview($width, $height) {
    	return '<div class="preview"><iframe width="' . $width . '" height="' . $height . '" src="http://www.youtube.com/embed/' . $this->object->getVideoId() . '?theme=light" frameborder="0" allowfullscreen></iframe></div>';
    } // renderPreview
    
	}