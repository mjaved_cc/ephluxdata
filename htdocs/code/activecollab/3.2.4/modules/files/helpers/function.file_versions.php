<?php

  /**
   * file_versions helper implementation
   *
   * @package activeCollab.modules.files
   * @subpackage helpers
   */

  /**
   * Render file versions table
   * 
   * Parameters:
   * 
   * - file - Selected file
   * - user - User who is viewing the page
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_file_versions($params, &$smarty) {
    static $ids = array();
    
    $file = array_var($params, 'file');
    if(!($file instanceof File)) {
      throw new InvalidInstanceError('file', $file, 'File');
    } // if
    
    $user = array_var($params, 'user');
    if(!($user instanceof User)) {
      throw new InvalidInstanceError('user', $user, 'User');
    } // if
    
    $id = array_var($params, 'id');
    if(empty($id)) {
      $counter = 1;
      do {
        $id = 'file_versions_' . $counter++;
      } while(in_array($id, $ids));
    } // if
    $ids[] = $id;
        
    $smarty->assign(array(
      '_file_versions' => $file->versions()->get(), 
      '_file_versions_file' => $file, 
      '_file_versions_user' => $user, 
      '_file_versions_id' => $id,  
      '_file_versions_wrap' => array_var($params, 'wrap', true),
    	'_file_versions_can_add' => $file->canUploadNewVersions($user),
    	'_file_versions_upload_new_version_url' => extend_url($file->getNewVersionUrl(), array('async' => 1))
    ));

    $interface = array_var($params, 'interface', AngieApplication::getPreferedInterface(), true);
    
    return $smarty->fetch(get_view_path('_file_versions', 'files', FILES_MODULE,$interface));
  } // smarty_function_file_versions