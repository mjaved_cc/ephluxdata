{title}Edit Bookmark{/title}
{add_bread_crumb}Edit Bookmark{/add_bread_crumb}

<div id="edit_bookmark">
  {form action=$active_asset->getEditUrl() method=post enctype="multipart/form-data" autofocus=yes ask_on_leave=yes class='big_form youtube_form'}
    {include file=get_view_path('_bookmark_form', 'bookmarks', 'files')}
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>