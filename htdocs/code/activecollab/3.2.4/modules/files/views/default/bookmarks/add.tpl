{title}New Bookmark{/title}
{add_bread_crumb}New Bookmark{/add_bread_crumb}

<div id="add_bookmark">
  {form action=$add_bookmark_url method=post enctype="multipart/form-data" autofocus=yes ask_on_leave=yes class='big_form youtube_form'}
    {include file=get_view_path('_bookmark_form', 'bookmarks', 'files')}
    
    {wrap_buttons}
      {submit}Add Bookmark{/submit}
    {/wrap_buttons}
  {/form}
</div>