{title}New Bookmark{/title}
{add_bread_crumb}New Bookmark{/add_bread_crumb}

<div id="add_bookmark">
  {form action=$add_bookmark_url}
    {include file=get_view_path('_bookmark_form', 'bookmarks', $smarty.const.FILES_MODULE)}
    
    {wrap_buttons}
      {submit}Add Bookmark{/submit}
    {/wrap_buttons}
  {/form}
</div>