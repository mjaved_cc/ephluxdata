{wrap field=url}
  {text_field name="youtube_video[video_url]" value=$youtube_data.video_url id=youtube_asset_url label='YouTube URL' required=true}
{/wrap}

{wrap field=name}
  {text_field name="youtube_video[name]" value=$youtube_data.name id=youtube_asset_title label='Video Title' required=true}
{/wrap}

{wrap_editor field=body}
  {editor_field name="youtube_video[body]" label='Body' id=youtube_asset_body}{$youtube_data.body nofilter}{/editor_field}
{/wrap_editor}

{wrap field=parent_id}
  {select_asset_category name="youtube_video[category_id]" value=$youtube_data.category_id id=youtube_asset_category parent=$active_project user=$logged_user label='Category'}
{/wrap}

{if $logged_user->canSeeMilestones($active_project)}
  {wrap field=milestone_id}
    {select_milestone name="youtube_video[milestone_id]" value=$youtube_data.milestone_id project=$active_project id=youtube_asset_milestone user=$logged_user label='Milestone'}
  {/wrap}
{/if}

{if $logged_user->canSeePrivate()}
  {wrap field=visibility}
    {select_visibility name="youtube_video[visibility]" value=$youtube_data.visibility label='Visibility' id=youtube_asset_visibility}
  {/wrap}
{else}
  <input type="hidden" name="youtube_video[visibility]" value="1" />
{/if}

{if $active_asset->isNew()}
	{wrap field=notify_users}
	  {select_subscribers name="notify_users[]" exclude=$youtube_data.exclude_ids object=$active_asset user=$logged_user label='Notify People' id=youtube_asset_notify_people}
	{/wrap}
{/if}

<script type="text/javascript">
	$(document).ready(function() {
		App.Wireframe.SelectBox.init();
	});
</script>