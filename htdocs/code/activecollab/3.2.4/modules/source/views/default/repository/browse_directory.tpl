{title}Browse Directory{/title}

{add_bread_crumb url=$project_object_repository->getBrowseUrl($active_revision, $active_file)}{$active_file_basename}{/add_bread_crumb}
{add_bread_crumb}Browse{/add_bread_crumb}

{object object=$active_commit user=$logged_user repository=$project_object_repository}
  <input type="hidden" id="folder_open_icon_url" value="{image_url name='icons/16x16/folder-opened.png' module=$smarty.const.SOURCE_MODULE}" />
  <input type="hidden" id="folder_closed_icon_url" value="{image_url name='icons/16x16/folder-closed.png' module=$smarty.const.SOURCE_MODULE}" />
  
  <div class="wireframe_content_wrapper source_navbar">
    <h3>{lang}Browsing Repository{/lang}</h3>
    {change_repository_revision url=$browse_url repository=$active_repository test_url=$change_revision_url value=""}
  </div>
  
  <div id="repository_browse" class="wireframe_content_wrapper">
  	<div class="ticket main_object browse_directory" id="file_">
  		<div class="resources">
  			<div class="source_container">
  				<table class="source_directory_browser common" id="source_directory_browser" cellspacing="0">
  					<tr>
              <th>{lang}Name{/lang}</th>
              <th class="file_size">{lang}Size{/lang}</th>
              <th class="date">{lang}Date{/lang}</th>
              <th class="author">{lang}Author{/lang}</th>
              <th class="revision">{lang}Revision{/lang}</th>
            </tr>
  
  					{if is_foreachable($list)}
  						{foreach from=$list.entries item=entry}
  							<tr class="{cycle values='odd,even'}" id="result_container_{$entry.url_key}" toggle_key="{$entry.url_key}" expanded="false">
  								<td class="{if $entry.kind eq 'dir'}directory{else}file{/if}">
  									{assign_var name=source_path}{$active_file}/{$entry.name}{/assign_var}
  									{if $entry.kind eq 'dir'}
  										<span class="toggle_tree" state="shrinked" toggle_key ="{$entry.url_key}" toggle_url="{$project_object_repository->getToggleUrl($active_commit, $source_path, $entry.url_key)}" loaded="false">
  											<img id="img_{$entry.url_key}" src="{image_url name='icons/16x16/folder-closed.png' module=$smarty.const.SOURCE_MODULE}"></img>
  											{$entry.name}
  										</span>
  										{else}
  										<a class="browse_url" href="{$project_object_repository->getBrowseUrl($active_commit, $source_path,$entry.revision_number)}">
  						  				{$entry.name}
  										</a>
  									{/if}
  								</td>
  								<td class="file_size">{if $entry.size <> null}{$entry.size}{else}{lang}Folder{/lang}{/if}</td>
                                {if $entry.date === false}
                                  <td class="date">{lang}N/A{/lang}</td>
                                {else}
                                  <td class="date">{$entry.date|date:0}</td>
                                {/if}
  								
  								<td class="author">{$entry.author}</td>
  								<td class="revision"><a title="View commit information" href="{$project_object_repository->getCommitUrl($entry.revision_number)}">{substr($entry.revision,0,8)}</a></td>
  							</tr>
  						{/foreach}
  					{/if}
  				</table>
  			</div>
  		</div>
  	</div>
  </div>
{/object}

<script type="text/javascript">
  /**
   * Initialise every single row
   * 
   * @param jQuery obje
   * @return null  
   */
	row_init = function (obj) {
	  obj.click(function (ev) {
	    //initializing variables form tag attributes
	      var state = obj.attr('state');
	      var folder_closed_url = $('#folder_closed_icon_url').attr('value');
	      var folder_open_url = $('#folder_open_icon_url').attr('value');
	      var loaded = obj.attr('loaded');
	      var toggle_key = obj.attr('toggle_key');
	      var toggle_url = obj.attr('toggle_url');
	      var result_container = $('#result_container_'+toggle_key);
	      //if directory is shrinked we will show it
	      if (state == 'shrinked') {
	        obj.attr('state','expanded');
	        // if directory is not loaded - we will load it
	        if (loaded == 'false') {
	          $('#img_'+toggle_key).attr('src',App.Wireframe.Utils.indicatorUrl());
	          $.get(toggle_url, function (data) {
	            $('#img_'+toggle_key).attr('src',folder_open_url);
	            result_container.after(data);
	            $('.child_of_'+toggle_key).each(function() {
	              // initializing new rows
	              row_init($(this).find('.toggle_tree'));
	              // padding rows to the left with style  
	              var level = 18 * $(this).attr('level');
	              $(this).find('.toggle_tree').css('padding-left',level+'px');
	              $(this).find('.file_toggle_tree').css('padding-left',level+'px');
	            });
	          });
	          obj.attr('loaded','true');
	        } else {
	          $('#img_'+toggle_key).attr('src',folder_open_url);
	        }
	        toggle_open_close_all_childs(toggle_key,true,true);
	      } else {
	        obj.attr('state','shrinked');
	        toggle_open_close_all_childs(toggle_key,false,true);
	        $('#img_'+toggle_key).attr('src',folder_closed_url);
	      }
	  });
	};

  /**
   * Toggle all children
   *
   * @param String parent_toggle_key
   * @param Boolean toggle_open_close
   * @param Boolean head
   * @return null
   */
	toggle_open_close_all_childs = function(parent_toggle_key, toggle_open_close, head) {
	  if (toggle_open_close) {
	    $('.child_of_'+parent_toggle_key).show();
	    if (head) {
	      $('#result_container_'+parent_toggle_key).attr('expanded','true');
	    }
	  } else {
	    $('.child_of_'+parent_toggle_key).hide();
	    if (head) {
	      $('#result_container_'+parent_toggle_key).attr('expanded','false');
	    }
	  }
	  
	  $('#source_directory_browser').find('.child_of_'+parent_toggle_key).each(function() {
	    if (!toggle_open_close || (toggle_open_close && $(this).attr('expanded') == "true")) {
	      toggle_open_close_all_childs($(this).attr('toggle_key'),toggle_open_close,false);
	    }
	  });
	};

	var table = $('#source_directory_browser');
	table.find('.toggle_tree').each(function() {
		row_init($(this));
	});
</script>