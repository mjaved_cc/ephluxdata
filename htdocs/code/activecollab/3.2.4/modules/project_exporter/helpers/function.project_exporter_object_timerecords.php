<?php

  /**
   * project_exporter_object_timerecords helper
   *
   * @package activeCollab.modules.project_exporter
   * @subpackage helpers
   */
  
  /**
   * Show a list of timerecords and expenses
   *
   * Parameters:
   * 
   * - object - timerecords parent object
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_project_exporter_object_timerecords($params, $template) {
    $object = array_var($params, 'object', null);
    if(!($object instanceof ProjectObject)) {
      throw new InvalidInstanceError('object', $object, 'ProjectObject');
    } // if
    
    AngieApplication::useHelper('date', GLOBALIZATION_FRAMEWORK, 'modifier');
    
    $return = '';
		$tracking = TrackingObjects::findByParent($object);
		if (is_foreachable($tracking)) {
			foreach ($tracking as $tracking_object) {
				if ($tracking_object instanceof Expense) {
					$value = $tracking_object->getValue();
				} else {
					$value = $tracking_object->getValue() . ' ' . lang('hours');
				} // if

				$return.= '<tr><td>' . $value . '</td><td>' . $tracking_object->getSummary() . '</td><td class="column_author">' . smarty_function_project_exporter_user_link(array('id' => $tracking_object->getCreatedById(), 'name' => $tracking_object->getCreatedByName(), 'email' => $tracking_object->getCreatedByEmail()), $template) . '</td><td class="column_date">' . smarty_modifier_date($tracking_object->getCreatedOn()) . '</td></tr>';	
			};
		} // if
		       
    if ($return) {
      $return = '<div id="object_tracking" class="object_info"><h3>' . lang('Time Records and Expenses') . '</h3><table class="common">' . $return . '</table></div>';
    } // if
    
    return $return;
  } // smarty_function_project_exporter_object_timerecords