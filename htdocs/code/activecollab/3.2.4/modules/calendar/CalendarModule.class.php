<?php

  /**
   * Calendar module definition
   *
   * @package activeCollab.modules.calendar
   * @subpackage models
   */
  class CalendarModule extends AngieModule {
    
    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'calendar';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '3.0';
    
    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------
    
    /**
     * Define module routes
     *
     * @return null
     */
    function defineRoutes() {
      Router::map('dashboard_calendar', 'dashboard/calendar', array('controller' => 'calendar', 'action' => 'index'));
      Router::map('dashboard_calendar_year', 'dashboard/calendar/:year', array('controller' => 'calendar', 'action' => 'index'), array('year' => '\d+'));
      Router::map('dashboard_calendar_month', 'dashboard/calendar/:year/:month', array('controller' => 'calendar', 'action' => 'index'), array('year' => '\d+', 'month' => '\d+'));
      Router::map('dashboard_calendar_day', 'dashboard/calendar/:year/:month/:day', array('controller' => 'calendar', 'action' => 'day'), array('year' => '\d+', 'month' => '\d+', 'day' => '\d+'));
      Router::map('ical', 'ical', array('controller' => 'calendar', 'action' => 'ical'));
      Router::map('ical_subscribe', 'ical-subscribe', array('controller' => 'calendar', 'action' => 'ical_subscribe'));
      
      // Project Calendar
      Router::map('project_calendar', 'projects/:project_slug/calendar', array('controller' => 'project_calendar', 'action' => 'index'));
      Router::map('project_calendar_year', 'projects/:project_slug/calendar/:year', array('controller' => 'project_calendar', 'action' => 'index'), array('year' => '\d+'));
      Router::map('project_calendar_month', 'projects/:project_slug/calendar/:year/:month', array('controller' => 'project_calendar', 'action' => 'index'), array('year' => '\d+', 'month' => '\d+'));
      Router::map('project_calendar_day', 'projects/:project_slug/calendar/:year/:month/:day', array('controller' => 'project_calendar', 'action' => 'day'), array('year' => '\d+', 'month' => '\d+', 'day' => '\d+'));
    
      // Profile Calendar
      Router::map('profile_calendar', 'people/:company_id/users/:user_id/calendar', array('controller' => 'profile_calendar', 'action' => 'index'), array('user_id' => '\d+'));
      Router::map('profile_calendar_year', 'people/:company_id/users/:user_id/calendar/:year', array('controller' => 'profile_calendar', 'action' => 'index'), array('user_id' => '\d+', 'year' => '\d+'));
      Router::map('profile_calendar_month', 'people/:company_id/users/:user_id/calendar/:year/:month', array('controller' => 'profile_calendar', 'action' => 'index'), array('user_id' => '\d+', 'year' => '\d+', 'month' => '\d+'));
      Router::map('profile_calendar_day', 'people/:company_id/users/:user_id/calendar/:year/:month/:day', array('controller' => 'profile_calendar', 'action' => 'day'), array('user_id' => '\d+', 'year' => '\d+', 'month' => '\d+', 'day' => '\d+'));
      Router::map('profile_calendar_ical', 'people/:company_id/users/:user_id/calendar/ical', array('controller' => 'profile_calendar', 'action' => 'ical'), array('user_id' => '\d+'));
      Router::map('profile_calendar_ical_subscribe', 'people/:company_id/users/:user_id/calendar/ical-subscribe', array('controller' => 'profile_calendar', 'action' => 'ical_subscribe'), array('user_id' => '\d+'));
    } // defineRoutes
    
    /**
     * Define event handlers
     */
    function defineHandlers() {
      EventsManager::listen('on_main_menu', 'on_main_menu');
      EventsManager::listen('on_project_tabs', 'on_project_tabs');
      EventsManager::listen('on_available_project_tabs', 'on_available_project_tabs');
      EventsManager::listen('on_object_options', 'on_object_options');
    } // defineHandlers
    
    // ---------------------------------------------------
    //  Names
    // ---------------------------------------------------
    
    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Calendar');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @param void
     * @return string
     */
    function getDescription() {
      return lang('Enables viewing project data in a calendar. Global calendar is available plus a calendar for each project and user');
    } // getDescription
    
    /**
     * Return module uninstallation message
     *
     * @param void
     * @return string
     */
    function getUninstallMessage() {
      return lang('Module will be deactivated. Data that is shown in the calendars will not be deleted');
    } // getUninstallMessage
    
  }