{title}Company and Role{/title}
{add_bread_crumb}Company and Role{/add_bread_crumb}

<div id="user_edit_company_and_role">
  {form action=$active_user->getEditCompanyAndRoleUrl() csfr_protect=true}
    {wrap_fields}
	    {wrap field=company_id}
	      {select_company name='user[company_id]' exclude=$exclude_ids value=$user_data.company_id user=$logged_user optional=false id=userCompanyId required=true can_create_new=false label='Company'}
	    {/wrap}
	    
	    {wrap field=role_id}
	      {select_role name='user[role_id]' value=$user_data.role_id active_user=$active_user optional=false id=userRoleId disabled=Roles::isLastAdministrator($active_user) required=true label='Role'}
	    {/wrap}
    {/wrap_fields}
    
    {wrap_buttons}
    	{submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>