<div id="print_container">
{object object=$active_company user=$logged_user}
	<div class="wireframe_content_wrapper">{project_list projects=$company_projects}</div>
	<div class="wireframe_content_wrapper">{user_list users=$active_company->getUsers()}</div>
	<div class="wireframe_content_wrapper">{user_list users=$active_company->getArchivedUsers() label="Archived Users" archived=true}</div>
	<div class="wireframe_content_wrapper">{invoice_list invoices=$company_invoices}</div>
	<div class="wireframe_content_wrapper">{quotes_list quotes=Quotes::findByCompany($active_company,$logged_user)}</div>
{/object}
</div>