<?php

  /**
   * Quick Jump callback
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class QuickJumpCallback extends JavaScriptCallback {
    
    /**
     * Additional options
     *
     * @var Object
     */
    protected $options;
    
    /**
     * User for which is this callback created
     * 
     * @var User
     */
    protected $user;
    
    /**
     * construct
     *
     * @param array $options
     */
    function __construct($options = null) {
      $this->options = $options;      
    } // __construct
        
    /**
     * Render callback body
     *
     * @return string
     */
    function render() {
    	$cache = cache_get('quick_jump_for_' . Authentication::getLoggedUser()->getId());
    	if ($cache) {
    		$this->options['data'] = JSON::valueToMap($cache);
    	} // if
    	
      return '(function () { $(this).quickJump(' . JSON::encode($this->options) . '); })';
    } // render
    
  }