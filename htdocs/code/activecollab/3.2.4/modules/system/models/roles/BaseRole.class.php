<?php

  /**
   * BaseRole class
   *
   * @package ActiveCollab.modules.system
   * @subpackage models
   */
  abstract class BaseRole extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'roles';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'name', 'permissions', 'is_default', 'homescreen_id');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of name field
     *
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of permissions field
     *
     * @return string
     */
    function getPermissions() {
      return $this->getFieldValue('permissions');
    } // getPermissions
    
    /**
     * Set value of permissions field
     *
     * @param string $value
     * @return string
     */
    function setPermissions($value) {
      return $this->setFieldValue('permissions', $value);
    } // setPermissions

    /**
     * Return value of is_default field
     *
     * @return boolean
     */
    function getIsDefault() {
      return $this->getFieldValue('is_default');
    } // getIsDefault
    
    /**
     * Set value of is_default field
     *
     * @param boolean $value
     * @return boolean
     */
    function setIsDefault($value) {
      return $this->setFieldValue('is_default', $value);
    } // setIsDefault

    /**
     * Return value of homescreen_id field
     *
     * @return integer
     */
    function getHomescreenId() {
      return $this->getFieldValue('homescreen_id');
    } // getHomescreenId
    
    /**
     * Set value of homescreen_id field
     *
     * @param integer $value
     * @return integer
     */
    function setHomescreenId($value) {
      return $this->setFieldValue('homescreen_id', $value);
    } // setHomescreenId

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, (integer) $value);
        case 'name':
          return parent::setFieldValue($real_name, (string) $value);
        case 'permissions':
          return parent::setFieldValue($real_name, (string) $value);
        case 'is_default':
          return parent::setFieldValue($real_name, (boolean) $value);
        case 'homescreen_id':
          return parent::setFieldValue($real_name, (integer) $value);
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }