<?php

  // We need admin controller
  AngieApplication::useController('admin');

  /**
   * Project settings administration controller
   * 
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class ProjectsAdminController extends AdminController {
  
    /**
     * Show and process project settings form
     */
    function index() {
      if($this->request->isAsyncCall()) {
        $settings_data = $this->request->post('settings', ConfigOptions::getValue(array(
          'project_tabs', 
          'project_templates_category', 
          'default_project_object_visibility',
          'clients_can_delegate_to_employees',
        )));
        
        $this->response->assign('settings_data', $settings_data);
        
        if($this->request->isSubmitted()) {
          try {
            ConfigOptions::setValue(array(
            	'project_tabs' => (array) $settings_data['project_tabs'], 
        		  'project_templates_category' => (integer) $settings_data['project_templates_category'], 
              'default_project_object_visibility' => (integer) $settings_data['default_project_object_visibility'], 
              'clients_can_delegate_to_employees' => (boolean) $settings_data['clients_can_delegate_to_employees'],
        		));

            CustomFields::setCustomFieldsByType('Project', $settings_data['custom_fields']);
        		
						clean_quick_jump_and_quick_add_cache();
            cache_clear();
        		
        		$this->response->ok();
          } catch(Exception $e) {
            $this->response->exception($e);
          } // try
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // index
    
  }