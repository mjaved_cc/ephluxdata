<?php

  /**
   * milestone_schedule helper implementation
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

  /**
   * Render milestone schedule
   * 
   * Parameteres:
   * 
   * - object - Milestone
   * - user - User viewing the page
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_milestone_schedule($params, &$smarty) {
    static $ids = array();
    
    $object = array_var($params, 'object');
    if(!($object instanceof Milestone)) {
      return new InvalidParamError('object', $object, '$object is expected to be an instance of Milestone class', true);
    } // if
    
    $user = array_var($params, 'user');
    if(!($user instanceof User)) {
      return new InvalidParamError('user', $user, '$user is expected to be an instance of User class', true);
    } // if
    
    $id = array_var($params, 'id');
    if(empty($id)) {
      $counter = 1;
      do {
        $id = "milestone_schedule_link_$counter";
        $counter++;
      } while(in_array($id, $ids));
    } // if
    $ids[] = $id;
    
    AngieApplication::useHelper('date', GLOBALIZATION_FRAMEWORK, 'modifier');
    
    if($object->isToBeDetermined()) {
      $date_range = lang('To Be Determined');
    } elseif($object->isDayMilestone()) {
      $date_range = smarty_modifier_date($object->getDueOn(), 0);
    } else {
      $date_range = smarty_modifier_date($object->getStartOn(), 0) . ' &mdash; ' . smarty_modifier_date($object->getDueOn(), 0);
    } // if
    
    if($object->canEdit($user)) {
      return open_html_tag('a', array(
        'href' => $object->getRescheduleUrl(),
        'class' => 'milestone_schedule',
        'id' => $id,
        'title' => lang('Click to Reschedule...'),
      )) . $date_range . '</a><script type="text/javascript">App.milestones.Schedule.init_schedule_link("' . clean($id) . '")</script>';
      
    } else {
      return '<span class="milestone_schedule">' . $date_range . '</span>';
    } // if
  } // smarty_function_milestone_schedule

?>