<?php

  /**
   * assignment_filters helper implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

  /**
   * Render assignment filters
   * 
   * @param array $params
   * @param Smarty $smarty
   */
  function smarty_function_assignment_filters($params, &$smarty) {
    $user = array_required_var($params, 'user', null, 'User');
    $filter = array_var($params, 'filter', null, true);
    
    $view = $smarty->createTemplate(AngieApplication::getViewPath('_assignment_filters', 'assignment_filters', SYSTEM_MODULE, AngieApplication::INTERFACE_DEFAULT));
    
    $projects = Projects::getIdNameMap($user, STATE_ARCHIVED, null, null, true);
    $labels = Labels::getIdDetailsMap('AssignmentLabel');
      
    $view->assign(array(
      'assignment_filters' => AssignmentFilters::findByUser($user),
      'pre_select_filter' => $filter,
      'new_filter_url' => AssignmentFilters::canAdd($user) ? Router::assemble('assignment_filters_add') : null,
      'users' => Users::getForSelect($user),
      'companies' => Companies::getIdNameMap(null, STATE_VISIBLE),
      'projects' => $projects, 
      'project_slugs' => $projects ? Projects::getIdSlugMap(array_keys($projects)) : null, 
      'project_categories' => Categories::getIdNameMap(null, 'ProjectCategory'), 
      'labels' => empty($labels) ? null : $labels,
      'job_types' => AngieApplication::isModuleLoaded('tracking') ? JobTypes::getIdNameMap() : null,
    ));
      
    return $view->fetch();
  } // smarty_function_assignment_filters