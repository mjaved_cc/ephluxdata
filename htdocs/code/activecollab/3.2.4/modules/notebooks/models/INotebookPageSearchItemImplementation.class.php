<?php

  /**
   * Notebook page search item implementation
   * 
   * @package activeCollab.modules.notebooks
   * @subpackage models
   */
  class INotebookPageSearchItemImplementation extends ISearchItemImplementation {
  
    /**
     * Return list of indices that index parent object
     * 
     * Result is an array where key is the index name, while value is list of 
     * fields that's watched for changes
     * 
     * @return array
     */
    function getIndices() {
      return array(
        'project_objects' => array('parent_type', 'parent_id', 'name', 'body'), 
        'names' => array('name'), 
      );
    } // getIndices
    
//    /**
//     * Return item context for given index
//     * 
//     * @param SearchIndex $index
//     * @return string
//     */
//    function getContext(SearchIndex $index) {
//      if($this->object->getNotebook() instanceof Notebook) {
//        return 'projects/' . $this->object->getNotebook()->getProjectId() . '/notebooks';
//      } else {
//        return 'unknown';
//      } // if
//    } // getContext
    
    /**
     * Return additional properties for a given index
     * 
     * @param SearchIndex $index
     * @return mixed
     */
    function getAdditional(SearchIndex $index) {
      if($index instanceof ProjectObjectsSearchIndex) {
        $notebook = $this->object->getNotebook();
        
        $result = array(   
          'name' => $this->object->getName(), 
          'body' => $this->object->getBody() ? $this->object->getBody() : null,
        );
        
        if($notebook instanceof Notebook) {
          $result['project_id'] = $notebook->getProjectId();
          $result['project'] = $notebook->getProject() instanceof Project ? $notebook->getProject()->getName() : null;
          $result['milestone_id'] = $notebook->getMilestoneId() ? $notebook->getMilestoneId() : null;
          $result['milestone'] = $notebook->getMilestone() instanceof Milestone ? $notebook->getMilestone()->getName() : null;
        } else {
          $result['context'] = 'unknown';
        } // if
        
        return $result;
      } elseif($index instanceof NamesSearchIndex) {
        return array(
          'name' => $this->object->getName(), 
        );
      } else {
        throw new InvalidInstanceError('index', $index, array('ProjectObjectsSearchIndex', 'NamesSearchIndex'));
      } // if
    } // getAdditional
    
  }