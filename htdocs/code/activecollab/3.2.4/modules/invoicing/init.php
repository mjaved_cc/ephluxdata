<?php

  /**
   * Init invoicing module
   *
   * @package activeCollab.modules.invoicing
   */

  define('INVOICING_MODULE', 'invoicing');
  define('INVOICING_MODULE_PATH', APPLICATION_PATH . '/modules/invoicing');

  define('INVOICE_STATUS_DRAFT', 0);
  define('INVOICE_STATUS_ISSUED', 1);
  define('INVOICE_STATUS_PAID', 2);
  define('INVOICE_STATUS_CANCELED', 3);
  
  define('QUOTE_STATUS_DRAFT', 0);
  define('QUOTE_STATUS_SENT', 1);
  define('QUOTE_STATUS_WON', 2);
  define('QUOTE_STATUS_LOST', 3);
  
  define('INVOICES_WORK_PATH', WORK_PATH . '/invoices');
  
  define('INVOICE_NUMBER_COUNTER_TOTAL', ':invoice_in_total');
  define('INVOICE_NUMBER_COUNTER_YEAR', ':invoice_in_year');
  define('INVOICE_NUMBER_COUNTER_MONTH', ':invoice_in_month');
  
  define('INVOICE_VARIABLE_CURRENT_YEAR', ':current_year');
  define('INVOICE_VARIABLE_CURRENT_MONTH', ':current_month');
  define('INVOICE_VARIABLE_CURRENT_MONTH_SHORT', ':current_short_month');
  define('INVOICE_VARIABLE_CURRENT_MONTH_LONG', ':current_long_month');

  AngieApplication::useModel(array(
    'invoices',
    'invoice_items',
    'invoice_payments',
    'tax_rates',
    'invoice_item_templates',
    'invoice_note_templates',
    'quotes',
    'quote_items',
    'recurring_profiles',
    'recurring_profile_items',
  ), INVOICING_MODULE);
  
  require INVOICING_MODULE_PATH . '/functions.php';
  
  AngieApplication::setForAutoload(array(
  	'IInvoice' => INVOICING_MODULE_PATH . '/models/IInvoice.class.php',
  	'InvoiceTemplate' => INVOICING_MODULE_PATH . '/models/InvoiceTemplate.class.php',
  	'IQuoteCommentsImplementation' => INVOICING_MODULE_PATH . '/models/IQuoteCommentsImplementation.class.php',
    'IQuoteAttachmentsImplementation' => INVOICING_MODULE_PATH . '/models/IQuoteAttachmentsImplementation.class.php',
    'IQuoteSubscriptionsImplementation' => INVOICING_MODULE_PATH . '/models/IQuoteSubscriptionsImplementation.class.php',
    'IInvoicePaymentsImplementation' => INVOICING_MODULE_PATH . '/models/IInvoicePaymentsImplementation.class.php',
    'RecurringInvoice' => INVOICING_MODULE_PATH . '/models/RecurringInvoice.class.php',
    'QuoteComment' => INVOICING_MODULE_PATH . '/models/QuoteComment.class.php',
    'QuoteAttachment' => INVOICING_MODULE_PATH . '/models/QuoteAttachment.class.php',
    'IInvoiceBasedOnImplementation' => INVOICING_MODULE_PATH . '/models/IInvoiceBasedOnImplementation.class.php',
  	'IInvoiceBasedOnQuoteImplementation' => INVOICING_MODULE_PATH . '/models/IInvoiceBasedOnQuoteImplementation.class.php',
  	'InvoicePDFGenerator' => INVOICING_MODULE_PATH . '/models/InvoicePDFGenerator.class.php',
   
  	'IInvoiceActivityLogsImplementation' => INVOICING_MODULE_PATH . '/models/IInvoiceActivityLogsImplementation.class.php',
  	'IInvoiceInspectorImplementation' => INVOICING_MODULE_PATH . '/models/IInvoiceInspectorImplementation.class.php',
   
  	'InvoiceIssuedActivityLogCallback' => INVOICING_MODULE_PATH . '/models/javascript_callbacks/InvoiceIssuedActivityLogCallback.class.php', 
  	'InvoicePaidActivityLogCallback' => INVOICING_MODULE_PATH . '/models/javascript_callbacks/InvoicePaidActivityLogCallback.class.php', 
  	'InvoiceCanceledActivityLogCallback' => INVOICING_MODULE_PATH . '/models/javascript_callbacks/InvoiceCanceledActivityLogCallback.class.php', 
  	'InvoiceNewPaymentActivityLogCallback' => INVOICING_MODULE_PATH . '/models/javascript_callbacks/InvoiceNewPaymentActivityLogCallback.class.php',
  ));
  
  DataObjectPool::registerTypeLoader('Invoice', function($ids) {
    return Invoices::findByIds($ids);
  });
  
  DataObjectPool::registerTypeLoader('Quote', function($ids) {
    return Quotes::findByIds($ids);
  });