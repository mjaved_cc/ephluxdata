<?php

  /**
   * notification_invoice_items_table helper implementation
   *
   * @package activeCollab.modules.invoicing
   * @subpackage helpers
   */

  /**
   * Render notification invoice item table
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_notification_invoice_items_table($params, &$smarty) {
    $context = array_required_var($params, 'context', true, 'ApplicationObject');
    $recipient = array_required_var($params, 'recipient', true, 'IUser');

    $language = $recipient->getLanguage();

    $separator_row = '<tr><td align="left" colspan="6">&nbsp;</td></tr>';
    
    $content = '<table cellpadding="0" cellspacing="0" style="text-align: left" bgcolor="#ffffff" width="100%">
      <tr style="font-weight: bold; background-color: #e8e8e8;">
        <td style="width: 13px; padding: 2px; padding-rigth: 5px; text-align: right;">#</td>
        <td style="padding: 2px;">' . lang("Description", null, null, $language) . '</td>
        <td style="width: 56px; padding: 2px; text-align: center;">' . lang("Qty.", null, null, $language) . '</td>
        <td style="width: 86px; padding: 2px; text-align: right;">' . lang("Unit Cost", null, null, $language) . '</td>
        <td style="width: 56px; padding: 2px; text-align: center;">' . lang("Tax", null, null, $language) . '</td>
        <td style="width: 56px; padding: 2px; text-align: right;">' . lang("Total", null, null, $language) . '</td>
      </tr>' . $separator_row;

    $items = $context->getItems();

    if($items)  {
      foreach($items as $item) {
        $content .= '<tr>
          <td style="padding: 2px; padding-right: 5px; text-align: right;">'. $item->getPosition() . '</td>
          <td style="padding: 2px;">'. clean($item->getDescription()) . '</td>
          <td style="padding: 2px; text-align: center;">'. $item->getQuantity() . '</td>
          <td style="padding: 2px; text-align: right;">'. number_format($item->getUnitCost(), 2, '.', ',') . '</td>
          <td style="padding: 2px; text-align: center;">'. $item->getTaxRateName() . '</td>
          <td style="padding: 2px; text-align: right;">'. number_format($item->getTotal(), 2, '.', ',') . '</td>
        </tr>';
      } // foreach
    } // if

    $content .= '</table>';

    $content .= '<table cellpadding="0" cellspacing="0" border="0" style="margin-top: 36px;" bgcolor="#ffffff" width="100%">
      <tr>
        <td style="padding: 2px; text-align: right;">' . lang('Total', null, null, $language) . '</td>
        <td style="padding: 2px; width: 56px; text-align: right;">' . number_format($context->getTotal(), 2, '.', ',') . '</td>
      </tr>
      <tr>
        <td style="padding: 2px; text-align: right;">' . lang('Tax', null, null, $language) . '</td>
        <td style="padding: 2px; width: 56px; text-align: right;">' . number_format($context->getTax(), 2, '.', ',') . '</td>
      </tr>
      <tr>
        <td style="padding: 2px; text-align: right; font-weight: bold;">' . lang('Total Cost', null, null, $language) . '</td>
        <td style="padding: 2px; width: 56px; text-align: right; font-weight: bold;">' . number_format($context->getTaxedTotal(), 2, '.', ',') . '</td>
      </tr>
    </table>';

    return $content;
  } // smarty_function_notification_invoice_items_table