{title}Invoicing Module{/title}
{add_bread_crumb}Details{/add_bread_crumb}

<div id="invoicing_module" class="module_admin_details">
  {include file=get_view_path('_module_info', 'fw_modules_admin', $smarty.const.MODULES_FRAMEWORK)}
  
  <h2 class="section_name"><span class="section_name_span">{lang}Permissions{/lang}</span></h2>
  <div class="section_container">
    <table class="module_role_permissions">
      <tr>
        <th class="role_name">{lang}Role{/lang}</th>
        <th class="permission">{lang}Create and Manage Invoices?{/lang}</th>
      </tr>
    {foreach from=$roles item=role}
      <tr class="{cycle values='odd,even'}">
        <td class="role_name"><a href="{$role->getViewUrl()}">{$role->getName()}</a></td>
        <td class="permission wide">{system_role_permission_value role=$role permission=can_manage_invoices yes_for_admins=false}</td>
      </tr>
    {/foreach}
    </table>
  </div>
</div>