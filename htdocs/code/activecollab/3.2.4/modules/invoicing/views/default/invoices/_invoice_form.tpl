<script type="text/javascript"> 
  App.widgets.FlyoutDialog.front().setAutoSize(false);
</script>

<div class="big_form_wrapper two_form_sidebars invoice_form">
  <div class="main_form_column">  
      {wrap field=items class="invoice_items_wrapper"}
        <table class="validate_callback validate_invoice_items" cellspacing="0">
          <thead>
	          <tr class="header">
	            <th class="num">
	              <input type="hidden" name="invoice_sub_total" id="invoice_sub_total" />
	              <input type="hidden" name="invoice_total" id="invoice_total" />
	              #
	            </th>
	            <th class="description">{lang}Description{/lang}</th>
	            <th class="quantity">{lang}Quantity{/lang}</th>
	            <th class="unit_cost">{lang}Unit Cost{/lang}</th>
	            <th class="tax_rate">{lang}Tax{/lang}</th>
	            <th class="subtotal" style="display: none">{lang}Subtotal{/lang}</th>
	            <th class="total">{lang}Total{/lang}</th>
	            <th class="options"></th>
	          </tr>
          </thead>
          <tbody>
          </tbody>
	        <tfoot>
	          <tr class="invoice_subtotal">
	            <td colspan="5">{lang}Subtotal{/lang}</td>
	            <td class="total"><input class="subtotal" type="text" disabled="disabled" /></td>
	            <td></td>
	          </tr>
	          <tr class="invoice_total">
	            <td colspan="5">{lang}Total{/lang}</td>
	            <td class="total"><input class="total" type="text" disabled="disabled"/></td>
	            <td></td>
	          </tr>
          </tfoot>
        </table>
        
        <div class="invoice_item_buttons">
	        {link_button label="Add New Item" icon_class=button_add id="add_new"}
	        {if is_foreachable($invoice_item_templates)}
		        {link_button_dropdown label="Add From Template" icon_class=button_duplicate id="add_from_template"}
		          <ul>
		            {foreach from=$invoice_item_templates item=invoice_item_template}
		              <li><a href="{$invoice_item_template->getId()}">{$invoice_item_template->getDescription()}</a></li>
		            {/foreach}
		          </ul>
		        {/link_button_dropdown}
	        {/if}
        </div>
      {/wrap}
    
    {if $active_invoice->isNew() && is_foreachable($invoice_data.time_record_ids)}
      {foreach from=$invoice_data.time_record_ids item=time_record_id}
      <input type="hidden" name="invoice[time_record_ids][]" value="{$time_record_id}" />
      {/foreach}
    {/if}    
  </div>
  
  <div class="form_sidebar form_second_sidebar">
    {wrap field=number id=invoiceNumberGenerator}
      {label for=invoiceNumber}Invoice ID{/label}
      {if $active_invoice->getStatus() == $smarty.const.INVOICE_STATUS_ISSUED}
        {text_field name="invoice[number]" value=$invoice_data.number class='' id=invoiceNumber disabled=disabled}
      {else}
        <div id="autogenerateID" style="{if $invoice_data.number}display:none{/if}">
          <div class="field_wrapper">{lang}Auto-generate on issue{/lang}<a href="#">({lang}Specify{/lang})</a></div>
        </div>
        <div id="manuallyID" style="{if !$invoice_data.number}display:none{/if}">
          <div class="field_wrapper">{text_field name="invoice[number]" value=$invoice_data.number class='' id=invoiceNumber}<a href="#">({lang}Generate{/lang})</a></div>
        </div>        
      {/if}
    {/wrap}
    
    {if $active_invoice->getStatus() == $smarty.const.INVOICE_STATUS_ISSUED}
      {wrap field=issued_on class=firstHolder}
        {select_date name="invoice[issued_on]" value=$invoice_data.issued_on label="Issued On" required=true}
      {/wrap}

      {wrap field=issued_on class=secondHolder}
        {select_date name="invoice[due_on]" value=$invoice_data.due_on label='Payment Due On' required=true}
      {/wrap}
    {/if}
        
    <div class="invoice_client_address">
      {wrap field=company_id}
        {select_company name="invoice[company_id]" value=$invoice_data.company_id class=required id="companyId" user=$logged_user can_create_new=true required=true label='Client' success_event=company_created}
      {/wrap}
      
      {wrap field=company_address class=companyAddressContainer}
        {textarea_field name="invoice[company_address]" id=companyAddress class='required long'}{$invoice_data.company_address nofilter}{/textarea_field}
      {/wrap}    
    </div>
            
    {wrap field=project_id}
      {if $active_invoice->isLoaded() && $active_invoice->getBasedOn() instanceof Project}
        {label}Project{/label}
        <p>{$active_invoice->getBasedOn()->getName()}</p>
        <input type="hidden" name="invoice[project_id]" value="{$active_invoice->getBasedOn()->getId()}">
      {else}
        {select_project name="invoice[project_id]" value=$invoice_data.project_id user=$logged_user optional=true label='Project'}
        {checkbox label="Only selected client's projects" name="selected_company_projects" id="selected_company_projects"}
      {/if}
    {/wrap}
    
    <div class="form_sidebar_two_controls">
      {wrap field=currencyId class=firstHolder}
        {select_currency name="invoice[currency_id]" value=$invoice_data.currency_id class=short id=currencyId label='Currency'}
      {/wrap}
      
      {wrap field=language class=secondHolder}
        {select_language name="invoice[language_id]" value=$invoice_data.language_id optional=true label='Language'}
      {/wrap}    
    </div>
    
    {if $allow_payment}
      <div class="invoice_client_address">
        {wrap field=allow_partial}
          {select_payments_type name="invoice[allow_payments]" selected=$invoice_data.payment_type label='Payments' required=true}
        {/wrap}
      </div>
    {/if}
    
  </div>
  
  <div class="invoice_details_wrapper"> 
    <div class="invoice_note_wrapper">
    	{wrap field=note}
      	{invoice_note name='invoice[note]' original_note=$original_note label='Public Invoice Note'}{$invoice_data.note nofilter}{/invoice_note}
      	<p class="aid">{lang}Invoice note is included in the final invoice and visible to the client{/lang}</p>
     	{/wrap}
    </div> 
    
    <div class="invoice_comment_wrapper">
      {wrap field=comment}
        {invoice_comment name="invoice[comment]" label='Our Private Comment'}{$invoice_data.comment nofilter}{/invoice_comment}
        <p class="aid">{lang}This comment is never displayed to the client or included in the final invoice{/lang}</p>
      {/wrap}
    </div>
  </div>  
</div>

<input type="hidden" name="invoice[quote_id]" value="{$invoice_data.quote_id}" />