<?php

  /**
   * RecurringProfileItems class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class RecurringProfileItems extends BaseRecurringProfileItems {
  
    // Put custom methods here
  
    /**
     * Return items by profile id
     * 
     * @param $profile_id
     */
    function findByProfileId($profile_id) {
      return self::find(array(
        'conditions' => array('recurring_profile_id = ?', $profile_id),
      ));
    }//findByProfileId
    
  }