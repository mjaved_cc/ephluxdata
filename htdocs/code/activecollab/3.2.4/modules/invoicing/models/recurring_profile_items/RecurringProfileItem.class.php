<?php

  /**
   * RecurringProfileItem class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class RecurringProfileItem extends BaseRecurringProfileItem {
  
    // Put custom methods here
	/**
     * Return subtotal total cost of this item
     *
     * @param void
     * @return float
     */
    function getSubTotal() {
      return $this->getUnitCost() * $this->getQuantity();
    } // getSubTotal
    
    /**
     * Returns full price of the item
     * 
     * @param void
     * @return float
     */
    function getTotal() {
      return $this->getSubTotal() + $this->getTax();
    } // getTotal
    
    
    /**
     * Return related tax rate
     *
     * @param void
     * @return TaxRate
     */
    function getTaxRate() {
      return TaxRates::findById($this->getTaxRateId());
    } // getTaxRate
    
    /**
     * Return tax rate name string
     *
     * @param void
     * @return string
     */
    function getTaxRateName() {
      $tax_rate = $this->getTaxRate();
      return $tax_rate instanceof TaxRate ? $tax_rate->getName() : '--';
    } // getTaxRateName
    
    /**
     * Return tax
     *
     * @param void
     * @return float
     */
    function getTax() {
      $tax_rate = $this->getTaxRate();
      return $tax_rate instanceof TaxRate ? round($this->getSubTotal() * $tax_rate->getPercentage() / 100) : 0;
    } // getTax
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
    	$result = parent::describe($user, $detailed, $for_interface);
    	
    	unset($result['name']);
    	
    	$result['description'] = $this->getDescription();
    	
    	$result['unit_cost'] = $this->getUnitCost();
    	$result['quantity'] = $this->getQuantity();
    	
			$result['subtotal'] = $this->getSubTotal();
			$result['tax'] = array(
				'id'	=> $this->getTaxRateId(),
				'name' => $this->getTaxRateName(),
				'value' => $this->getTax()
			);
			$result['total'] = $this->getTotal();
			
			$result['position'] = $this->getPosition();
			
			return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      throw new NotImplementedError(__METHOD__);
    } // describeForApi
    
  }