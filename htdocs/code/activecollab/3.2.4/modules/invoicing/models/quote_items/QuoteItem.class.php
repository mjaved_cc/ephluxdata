<?php

  /**
   * QuoteItem class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class QuoteItem extends BaseQuoteItem {
  
    /**
     * Return subtotal total cost of this item
     *
     * @return float
     */
    function getSubTotal() {
      return $this->getUnitCost() * $this->getQuantity();
    } // getSubTotal
    
    /**
     * Returns full price of the item
     * 
     * @return float
     */
    function getTotal() {
      return $this->getSubTotal() + $this->getTax();
    } // getTotal
    
    /**
     * Return tax
     *
     * @return float
     */
    function getTax() {
      return $this->getTaxRate() instanceof TaxRate ? $this->getSubTotal() * $this->getTaxRate()->getPercentage() / 100 : 0;
    } // getTax
    
    /**
     * Cached tax rate instance
     *
     * @var TaxRate
     */
    var $tax_rate = false;
    
    /**
     * Return related tax rate
     *
     * @return TaxRate
     */
    function getTaxRate() {
      if($this->tax_rate === false) {
        $this->tax_rate = TaxRates::findById($this->getTaxRateId());
      } // if
      return $this->tax_rate;
    } // getTaxRate
    
    /**
     * Return tax rate name string
     *
     * @return string
     */
    function getTaxRateName() {
      return $this->getTaxRate() instanceof TaxRate ? $this->getTaxRate()->getName() : '--';
    } // getTaxRateName
    
    /**
     * Return tax rate value string
     *
     * @return string
     */
    function getTaxRatePercentageVerbose() {
      return $this->getTaxRate() instanceof TaxRate ? '(' . $this->getTaxRate()->getVerbosePercentage() . ')' : '';
    } // getTaxRatePercentage
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
    	$result = parent::describe($user, $detailed, $for_interface);
    	
    	unset($result['name']);
    	
    	$result['description'] = $this->getDescription();
    	
    	$result['unit_cost'] = $this->getUnitCost();
    	$result['quantity'] = $this->getQuantity();
    	
			$result['subtotal'] = $this->getSubTotal();
			$result['tax'] = array(
				'id'	=> $this->getTaxRateId(),
				'name' => $this->getTaxRateName(),
				'value' => $this->getTax()
			);
			$result['total'] = $this->getTotal();
			
			$result['position'] = $this->getPosition();
			
			return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      $result = parent::describeForApi($user, $detailed);

      unset($result['name']);

      $result['description'] = $this->getDescription();

      $result['unit_cost'] = $this->getUnitCost();
      $result['quantity'] = $this->getQuantity();

      $result['subtotal'] = $this->getSubTotal();
      $result['tax'] = array(
        'id'	=> $this->getTaxRateId(),
        'name' => $this->getTaxRateName(),
        'value' => $this->getTax()
      );
      $result['total'] = $this->getTotal();

      $result['position'] = $this->getPosition();

      return $result;
    } // describeForApi

    /**
     * Validate before save
     *
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->validatePresenceOf('description')) {
        $errors->addError(lang('Item description is required'), 'name');
      } // if

      if(!$this->validatePresenceOf('quantity')) {
        $errors->addError(lang('Quantity is required'), 'quantity');
      } // if

      if (!$this->getUnitCost()) {
        $this->setUnitCost(0);
      } // if

      return parent::validate($errors);
    } // validate
    
  }