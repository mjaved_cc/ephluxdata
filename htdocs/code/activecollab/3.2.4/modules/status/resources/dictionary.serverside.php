<?php return array(
  'Status Updates',
  'Status Update #:id',
  'New Status Message',
  ':display_name\'s Status Updates',
  'Use Status Updates',
  'Set to Yes to let people use a simple communication tool that is easily accessible from all system pages',
  'Status message is required',
  'Status',
  'Adds simple, globally available communication channel. Tell your team members or clients what you are working on or have a quick chat',
  'Module will be deactivated. All data generated using it will be deleted',
  'Replies',
  'Status Updates Archive',
  'Type your message and hit Enter to post it',
  'Browse Archive',
  'Track Using RSS',
  'Archive',
  'Status Archive',
  'User',
  'All users',
  'Next Page',
  'User has no status updates',
  'No status messages logged',
  'Status Update',
  'View',
  'Status Module',
  'Permissions',
  'Role',
  'Can post updates?',
  'Details',
  'Add Status Message',
  'Add',
  'Add Message',
  'Status Message',
  'There are no status updates',
  'All Status Updates',
  'Update',
  'Update Status',
); ?>