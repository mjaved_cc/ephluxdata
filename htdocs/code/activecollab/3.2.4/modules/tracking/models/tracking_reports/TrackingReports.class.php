<?php

  /**
   * TrackingReports class
   *
   * @package activeCollab.modules.tracking
   * @subpackage models
   */
  class TrackingReports extends BaseTrackingReports {
  
    /**
     * Returns true if $user can create new tracking reports
     *
     * @param User $user
     * @return boolean
     */
    static function canAdd(User $user) {
      return $user->isProjectManager();
    } // canAdd
  
  }