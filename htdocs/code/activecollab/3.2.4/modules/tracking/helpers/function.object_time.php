<?php

  /**
   * object_time helper definition
   * 
   * Reason why this helper needs to be here is because it is used across entire 
   * system and other modules may required it without check if tracking module 
   * is installed
   *
   * @package activeCollab.modules.resources
   * @subpackage helpers
   */

  /**
   * Render object time widget
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_object_time($params, &$smarty) {
    $object = array_required_var($params, 'object', true, 'ITracking');
    $user = array_required_var($params, 'user', true, 'IUser');
    
    $id = isset($params['id']) && $params['id'] ? $params['id'] : HTML::uniqueId('object_tracking_for_' . $object->getId());
		
		$params = array(
			'object' => $object->describe($user, array(
		    'detailed' => true, 
		  )),
			'show_label' => array_var($params, 'show_label', true, true),
			'interface' => array_var($params, 'interface', AngieApplication::getPreferedInterface(), true),
      'default_billable_status' => $object->tracking()->getDefaultBillableStatus() ? 1 : 0,
		);
		
    return '<span class="object_tracking" id="' . $id . '"></span><script type="text/javascript">$(\'#' . $id . '\').objectTime(' . JSON::encode($params, $user, null) . ')</script>';
  } // smarty_function_object_time