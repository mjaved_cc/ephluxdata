<?php

  /**
   * Documents on_system_permissions handler
   *
   * @package activeCollab.modules.documents
   * @subpackage handlers
   */
  
  /**
   * Handle on_system_permissions
   *
   * @param NamedList $permissions
   */
  function documents_handle_on_system_permissions(NamedList &$permissions) {
    $permissions->add('can_use_documents', array(
      'name' => lang('Access Global Documents'), 
      'description' => lang('Set to Yes to let users access Global Documents section, and view files and documents that are available in that section'), 
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_add_documents', array(
      'name' => lang('Create Global Documents'), 
      'description' => lang('This permissions lets users who have access to Global Documents section to also post text documents and upload files to the section'), 
      'depends_on' => 'can_use_documents', 
    ));

    $permissions->add('can_manage_documents', array(
      'name' => lang('Manage Global Documents'),
      'description' => lang('This permissions lets users who have access to Global Documents section to also manage all documents that are in that section'),
      'depends_on' => 'can_use_documents',
    ));
  } // documents_handle_on_system_permissions