<?php return array(
  'Company logo successfully updated',
  'Avatar successfully updated',
  'Project icon successfully updated',
  'Unexpected error occurred',
  'Are you sure you want to reset this picture to default one',
  'Failed to reset avatar',
  'Update Avatar',
  'Avatar',
); ?>