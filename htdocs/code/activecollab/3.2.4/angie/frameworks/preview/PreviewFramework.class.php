<?php

  /**
   * Preview framework definition
   *
   * @package angie.frameworks.preview
   */
  class PreviewFramework extends AngieFramework {
    
    /**
     * Short framework name
     *
     * @var string
     */
    protected $name = 'preview';

    /**
     * Define attachment routes for given context
     *
     * @param string $context
     * @param string $context_path
     * @param string $controller_name
     * @param string $module_name
     * @param array $context_requirements
     */
    function definePreviewRoutesFor($context, $context_path, $controller_name, $module_name, $context_requirements = null) {
      Router::map("{$context}_preview", "$context_path/preview", array('controller' => $controller_name, 'action' => "{$context}_preview_content", 'module' => $module_name), $context_requirements);
    } // defineDownloadRoutesFor
    
  }

?>