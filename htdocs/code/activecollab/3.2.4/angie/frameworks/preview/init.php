<?php

  /**
   * Preview framework initialization file
   *
   * @package angie.frameworks.preview
   */
  
  define('PREVIEW_FRAMEWORK', 'preview');
  define('PREVIEW_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/preview');

  @define('PREVIEW_FRAMEWORK_INJECT_INTO', 'system');
  
  AngieApplication::setForAutoload(array(
    'IPreview' => PREVIEW_FRAMEWORK_PATH . '/models/IPreview.class.php', 
    'IPreviewImplementation' => PREVIEW_FRAMEWORK_PATH . '/models/IPreviewImplementation.class.php', 
    
    'FwThumbnails' => PREVIEW_FRAMEWORK_PATH . '/models/FwThumbnails.class.php', 
  ));