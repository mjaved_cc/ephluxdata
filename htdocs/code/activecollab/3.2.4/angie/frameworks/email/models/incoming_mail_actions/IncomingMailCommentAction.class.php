<?php
  
  /**
   * Incoming mail action - Add comment
   * 
   * @package angie.framework.email
   * @subpackage models.incoming_mail_actions
   *
   */
  class IncomingMailCommentAction extends IncomingMailAction {
    
    /**
     * Required additional settings needed for actions
     * 
     * @var array
     */
     protected $required_additional_settings = array();
    
    /*
     * Constructor
     */
    function __construct() {
      $this->setActionClassName(__CLASS__);
      $this->setSettings();
    }//__construct
    
    /**
     * Set settings as name, descriptions..
     */
    public function setSettings() {
      $this->setName(lang('Add New Comment'));
      $this->setDescription(lang('Add new comment to project object.'));
      $this->setTemplateName('incoming_mail_add_comment_action');
      $this->setCanUse(true);
      $this->setModuleName(EMAIL_FRAMEWORK);
    }//setSettings
    
    /**
     * Render project elements into filter form
     * 
     */
    function renderProjectElements(IUser $user, Project $project, IncomingMailFilter $filter = null) {
     
      return $this->elements;
    }//renderProjectElements
    
    /**
     * Do actions over incoming email
     *
     * @params $incoming_mail 
     * @params $additional_settings
     * @return Comment
     */
    public function doActions(IncomingMail $incoming_email, $additional_settings = false, $force = false) {
      //check all parameters
      $this->checkActionsParameters($incoming_email,$additional_settings);
 
      $parent = $incoming_email->getParent();
      
      if (!$parent instanceof ApplicationObject) {
        // parent object does not exists
        throw new Error(IncomingMessageImportErrorActivityLog::ERROR_PARENT_NOT_EXISTS);
      } // if
      
      if(!$incoming_email->getCreatedById() == 0) {
         $user = Users::findById($incoming_email->getCreatedById());
      } else {
        $user = new AnonymousUser($incoming_email->getCreatedByName(), $incoming_email->getCreatedByEmail());
        //anonymous user doesn't need to receive notification about comment he just submit
        $additional_params['exclude_to_notify'] = $user;
      }//if
      
      if($force) {
        //from conflict controller
        
        if(!$additional_settings['subscribe_sender']) {
          //if not checked "submit" will not subscribe comment author
          $additional_params['subscribe_author'] = false;
        }//if
      } else {
        //directly from mailbox - on frequently
        
        if (!$parent->comments()->canComment($user) && ($parent instanceof ISubscriptions && !$parent->subscriptions()->isSubscribed($user))) {
          // user cannot create comments to parent object
          throw new Error(IncomingMessageImportErrorActivityLog::ERROR_USER_CANNOT_CREATE_COMMENT);
        } //if
        $additional_params['set_source'] = OBJECT_SOURCE_EMAIL;
      }//if
      
      $comment = $parent->comments()->submit($incoming_email->getBody(), $user, $additional_params);
      
      IncomingMailImporter::attachFilesToProjectObject($incoming_email, $comment);

      $comment->save();
      
      return $comment;
    }//doActions
    
  }