<?php

  /**
   * notification_attachments_table helper implementation
   *
   * @package angie.frameworks.email
   * @subpackage helpers
   */

  /**
   * Render notification new comment made
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_notification_attachments_table($params, &$smarty) {
    $object = array_required_var($params, 'object', false, 'ApplicationObject');
    $recipient = array_required_var($params, 'recipient', false, 'IUser');

    if($object instanceof IAttachments) {
      $attachments = $object->attachments()->get($recipient);

      if($attachments) {
        $content = "<table cellspacing='0' cellspacing='0' border='0'><tr>
        	  <td style='text-align: right; vertical-align: top; width:10px;'><img src='" . AngieApplication::getImageUrl('icons/12x12/manage-attachments.png', ATTACHMENTS_FRAMEWORK) . "'></td>
            <td style='vertical-align: top; padding-left: 5px;'><table cellspacing='0' cellspacing='0' border='0'>";

        $link_style = Theme::getProperty('notifications.link');
        foreach ($attachments as $attachment) {
          $content .= "<tr><td><a href='" . clean($attachment->getViewUrl(true)) . "' style='" . $link_style . "'>" . clean($attachment->getName()) . "</a><span style='margin-left:10px;font-size:9px;color:#91918D;'>" . format_file_size($attachment->getSize()) . "</span></td></tr>";
        } // foreach

        return "$content</table></td></tr></table>";
      } // if
    } // if
  } // smarty_function_notification_attachments_table