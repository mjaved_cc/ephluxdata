<div id="comments_working_box" class="working_box">
	
	<table class="comment_conflict">
		{if method_exists($active_mail->getParent(),'getProject') && $active_mail->getParent()->getProject()}
  		<tr>
  			<th>{lang}Project:{/lang}</th>
  			<td>{project_link project=$active_mail->getParent()->getProject()}</td>
  		</tr>
  	{/if}
		<tr>
			<th>{lang}In {/lang}{$active_mail->getParent()->getVerboseType()}:</th>
			<td>{object_link object=$active_mail->getParent()}</td>
		</tr>
		<tr>
			<th colspan="2">{checkbox_field value=1 name="filter[action_parameters][subscribe_sender]" label="Subscribe sender to parent object" checked=true}</th>
		</tr>
	</table>
	
    
</div>
