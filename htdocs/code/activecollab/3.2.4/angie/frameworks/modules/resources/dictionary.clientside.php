<?php return array(
  'Module has been enabled',
  'Please wait until activeCollab refreshes the page',
  'Module has been disabled',
  'Module has been installed',
  'Module has been uinstalled',
); ?>