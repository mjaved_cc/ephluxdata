{title}Modules{/title}
{add_bread_crumb}All Modules{/add_bread_crumb}

<div id="modules_admin">
{if is_foreachable($modules)}
  <table class="common modules_list" cellspacing="0">
    <tr>
      <th class="is_enabled"></th>
      <th class="name" colspan="2">{lang}Module{/lang}</th>
      <th class="options"></th>
    </tr>
  {foreach from=$modules item=module}
    <tr class="{cycle values='even, odd'} {$module->getName()}">
      <td class="is_enabled">
      {if $module->isInstalled()}
        {checkbox_field on_url=$module->getEnableUrl() off_url=$module->getDisableUrl() class="enabling_disabling_chx" checked=$module->isEnabled() name="enabling" disabled=!$module->canDisable($logged_user)}
      {/if}
      </td>
      <td class="icon"><img src="{$module->getIconUrl()}"></td>
      <td class="name">
        {$module->getDisplayName()}, <span class="details">v{$module->getVersion()}</span>
      {if $module->getDescription()}
        <span class="details block">{$module->getDescription()|clickable nofilter}</span>
      {/if}
      </td>
      <td class="options">
      {if $module->isInstalled()}
        {if $module->canUninstall($logged_user)}
          {button href=$module->getUninstallUrl() class="uninstall_module_btn" confirm=$module->getUninstallMessage() success_event="module_deleted"}Uninstall{/button}
        {/if}
      {else}
        {if $module->canInstall($logged_user)}
          {button href=$module->getInstallUrl() class="install_module_btn" title="Install Module" mode="flyout_form"}Install{/button}
        {/if}  
      {/if}
        </td>
    </tr>
  {/foreach}
  </table>
{else}
  <p>{lang}There are no modules{/lang}</p>
{/if}
</div>
<script type="text/javascript">
  (function() {
    $('.enabling_disabling_chx').asyncCheckbox({
      'success' : function() {
        if(this.checked) {
          App.Wireframe.Flash.success(App.lang('Module has been enabled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));
        } else {
          App.Wireframe.Flash.success(App.lang('Module has been disabled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));
        } // if

        location.reload();
      }
    });

    App.Wireframe.Events.bind('module_created.content', function(event, module) {
      App.Wireframe.Flash.success(App.lang('Module has been installed') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));

      location.reload();
    });

    App.Wireframe.Events.bind('module_deleted.content', function(event, module) {
      App.Wireframe.Flash.success(App.lang('Module has been uinstalled') + '. ' + App.lang('Please wait until activeCollab refreshes the page'));

      location.reload();
    });

  })();
</script>