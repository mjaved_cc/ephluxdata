{title}Playground Page{/title}
{add_bread_crumb}Playground Page{/add_bread_crumb}

<div class="developer_playground_page">
  <p>This is a playground page for developers.</p>
  <p>Be Free to modify this page and it's controller to test some stuff, but you will not be able to commit this page.</p>
  <p>There's no need to edit random controller actions to test your code, and risk commiting them accidentally.</p>
  <br /><br />
  <p><strong>Controller Path:</strong><br />{$smarty.const.DEVELOPER_FRAMEWORK_PATH}/controllers/DeveloperPlaygroundController.class.php</p>
  <p><strong>View Path:</strong><br />{$smarty.const.DEVELOPER_FRAMEWORK_PATH}/views/default/developer_playground/index.tpl</p>
</div>