<?php

  /**
   * Home screen widget that displays system notifications
   * 
   * @package angie.frameworks.environment
   * @subpackage models
   */
  class SystemNotificationsHomescreenWidget extends HomescreenWidget {
    
    /**
     * Return widget name
     * 
     * @return string
     */
    function getName() {
      return lang('System Notifications');
    } // getName
    
    /**
     * Return widget description
     * 
     * @return string
     */
    function getDescription() {
      return lang("Displays a list of messages that require user's attention. System is aware of user's role, so it will display a proper set of notifications for any given user");
    } // getDescription
    
    /**
     * Return widget body
     * 
     * @param IUser $user
     * @param string $widget_id
     * @param string $column_wrapper_class
     * @return string
     */
    function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
      return '';
    } // renderBody
    
    /**
     * Return footer content
     * 
     * @param IUser $user
     * @param string $widget_id
     * @param string $column_wrapper_class
     *
     * @return string
     */
    function renderFooter(IUser $user, $widget_id, $column_wrapper_class = null) {
      $notifications = new NamedList();
      EventsManager::trigger('on_system_notifications', array(&$notifications, &$user));

      return '<script type="text/javascript">$("#' . $widget_id . '").systemNotificationsHomescreenWidget(' . JSON::encode(array(
        'notifications' => $notifications->count() ? $notifications : null,
        'widget_id'     => $widget_id,
        'refresh_url'   => $this->getRenderUrl()
      )) . ');</script>';
    } // renderFooter
    
  }