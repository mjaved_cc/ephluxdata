/**
 * Simple permalink property handler
 */
App.Inspector.Properties.SimplePermalink = function (object, client_interface, permalink_field, label_field, attributes) {
  var wrapper = $(this);
  
  var permalink = App.Inspector.Utils.getFieldValue(permalink_field, object);
  var label = App.Inspector.Utils.getFieldValue(label_field, object);

  if (permalink && !label) {
    label = permalink;
  } // if
  
  if (permalink) {
    var check_string = label.clean() + permalink.clean();
  } else {
    var check_string = 'no_link';
  } // if
  
  var link_attributes = [];

  attributes = (!attributes || !attributes.length) ? {} : attributes;
  attributes['class'] = attributes['class'] ? attributes['class'] + ' quick_view_item' : 'quick_view_item';
  
  $.each(attributes, function (attribute_name, attribute_value) {
    link_attributes.push(attribute_name + '="' + attribute_value + '"');
  });
  
  link_attributes = link_attributes.length ? link_attributes.join(' ') : '';

  if (wrapper.attr('check_string') == check_string) {
    return true;
  } // if  
  wrapper.attr('check_string', check_string);
  
  var wrapper_row = wrapper.parents('div.property:first');
  
  // if we don't have permalink, hide the row
  if (!permalink) {
    wrapper_row.hide();
    return false;
  } // if
  
  if (App.isValidEmail(permalink)) {
    permalink = 'mailto:' + permalink;
  } // if
  
  wrapper_row.show();

  wrapper.empty().append('<a href="' + permalink.clean() + '" ' + link_attributes + '>' + label.clean() + '</a>');
};
