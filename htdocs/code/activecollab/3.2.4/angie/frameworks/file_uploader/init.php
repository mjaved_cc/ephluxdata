<?php

/**
 * Initalize file uploader framework
 *
 * @package angie.frameworks.file_uploader
 */

define('FILE_UPLOADER_FRAMEWORK', 'file_uploader');
define('FILE_UPLOADER_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/file_uploader');

if (!defined('FILE_UPLOADER_RUNTIMES')) {
  if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
    define('FILE_UPLOADER_RUNTIMES', 'html5,html4');
  } else {
    define('FILE_UPLOADER_RUNTIMES', 'html5,flash,silverlight,html4');
  } // if
} // if