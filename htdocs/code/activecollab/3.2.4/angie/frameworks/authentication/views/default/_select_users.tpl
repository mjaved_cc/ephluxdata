<div class="select_users_widget form_field {if $_select_users_required}validate_callback select_users_value_present{/if}" id="{$_select_users_id}">
  <div class="select_users_widget_users">
  {if $_select_users_value}
    <ul class="users_list">
    {foreach $_select_users_value as $_select_users_user_id => $_select_users_user_display_name}
      <li>{$_select_users_user_display_name}</li>
    {/foreach}
    </ul>
    
    {foreach $_select_users_value as $_select_users_user_id => $_select_users_user_display_name}
      <input type="hidden" name="{$_select_users_name}[]" value="{$_select_users_user_id}" user_display_name="{$_select_users_user_display_name}" />
    {/foreach}
    <p class="no_users_selected details" style="display: none">{lang}No users selected{/lang}</p>
  {else}
    <ul class="users_list" style="display: none"></ul>
    <p class="no_users_selected details">{lang}No users selected{/lang}</p>
  {/if}
  </div>
  <a href="#" class="assignees_button">{lang}Change{/lang}</a>
</div>
<script type="text/javascript">
  App.widgets.SelectUsers.init({$_select_users_id|json nofilter}, {$_select_users_name|json nofilter}, {$_select_users_users|json nofilter});
</script>