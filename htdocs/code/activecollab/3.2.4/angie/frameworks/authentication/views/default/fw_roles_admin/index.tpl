{title}All System Roles{/title}
{add_bread_crumb}All System Roles{/add_bread_crumb}

<div id="roles_admin"></div>

<script type="text/javascript">
  $('#roles_admin').manageRoles({
    'roles' : {$roles|json nofilter}
  });

  App.Wireframe.Events.bind('role_updated.content', function(event, role) {
    if(role['id'] == {$logged_user->getRoleId()|json nofilter}) {
      App.Wireframe.Updates.get({
  		  'refresh_backend_wireframe' : true 
  		});
    } // if
  });
</script>