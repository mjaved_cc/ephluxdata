<?php

  /**
   * Users context helper implementation
   *
   * @package angie.frameworks.authentication
   * @subpackage models
   */
  abstract class IUsersContextImplementation implements IDescribe {
    
    /**
     * Parent object instance
     *
     * @var IUsers
     */
    protected $object;
    
    /**
     * Construct users implementation
     *
     * @param IUsers $object
     */
    function __construct(IUsersContext $object) {
      $this->object = $object;
    } // __construct
    
    /**
     * Count users
     * 
     * @param User $user
     * @return integer
     */
    function count(User $user) {
      return Users::count();
    } // count
    
    /**
     * Return true if $user is member of this users context
     *
     * @param User $user
     * @return boolean
     */
    function isMember(User $user) {
      throw new NotImplementedError(__CLASS__ . '::' . __METHOD__);
    } // isMember
    
    /**
     * Return users in given context
     *
     * @param User $user
     * @return DBResult
     */
    function get(User $user) {
      return Users::find();
    } // get
    
    /**
     * Return users for select box
     *
     * @param User $user
     * @param array $exclude_ids
     * @param integer $min_state
     * @return array
     */
    function getForSelect(User $user, $exclude_ids = null, $min_state = STATE_VISIBLE) {
      return Users::getForSelect($user, $exclude_ids, $min_state);
    } // getForSelect
    
    /**
     * Add user to this context
     *
     * @param User $user
     */
    function add(User $user) {
      throw new NotImplementedError(__CLASS__ . '::' . __METHOD__);
    } // add
    
    /**
     * Remove user from this context
     *
     * @param User $user
     */
    function remove(User $user) {
      throw new NotImplementedError(__CLASS__ . '::' . __METHOD__);
    } // remove
    
    /**
     * Clear all relations
     *
     * @param User $user
     */
    function clear(User $user) {
      throw new NotImplementedError(__CLASS__ . '::' . __METHOD__);
    } // clear
    
    /**
     * Replace one user with another user
     *
     * @param User $replace
     * @param User $with
     */
    function replace(User $replace, User $with) {
      $this->remove($replace);
      $this->add($with);
    } // replace
    
  }