<?php

  /**
   * smarty_function_select_payment_gateway helper implementation
   * 
   * @package angie.frameworks.payments
   * @subpackage helpers
   */

  /**
   * Render select_payment_gateway helper
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_make_payment($params, &$smarty) {
    $user = array_required_var($params, 'user', true, 'IUser');
    
    $object = array_var($params, 'object', true, true);
    
    $options = array();
    
    if($object instanceof IPayments && $object->payments()->isEnabled()) {
      $gateways = PaymentGateways::findAllCurrencySupported($object->getCurrency()->getCode());
  	  
      if($gateways) {
        foreach($gateways as $gateway) {
          $hidden_fileds = "<input type='hidden' value='" . $gateway->getId() . "' name='payment_gateway_id' />"; 
          $options[] = array(
            'name' => $gateway->getName(),
            'description' => $gateway->getGatewayDescription(),
            'options' => $gateway->renderPaymentForm($user) . $hidden_fileds,
            'selected' => $gateway->isDefault()
          );
        } // foreach
      } // if
    }//if
    
    if($user->isFinancialManager()) {
      $custom_gateway = new CustomPaymentGateway();
      $hidden_ = "<input type='hidden' value='0' name='payment_gateway_id' />"; 
      $options[] = array(
        'name' => lang('Custom payment'),
        'description' => $custom_gateway->getGatewayDescription(), 
        'options' => $custom_gateway->renderPaymentForm($user) . $hidden_,
      );
    }//if  
    
    
    
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('select_payment_gateway');
    } // if
    
    if(empty($params['class'])) {
      $params['class'] = 'select_payment_gateway';
    } else {
      $params['class'] .= ' select_payment_gateway';
    } // if
    
    return HTML::openTag('div', $params) . '</div><script type="text/javascript">$("#' . $params['id'] . '").selectPaymentGateway(' . JSON::encode(array(
      'types' => $options
    )) . ')</script>';
  } // smarty_function_select_payment_gateway