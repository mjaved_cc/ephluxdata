<?php

  /**
   * Framework level payment gateway instance implementation
   *
   * @package angie.frameworks.payments
   * @subpackage models
   */
  abstract class FwPaymentGateway extends BasePaymentGateway implements IRoutingContext  {
    
    /**
     * Return list of supported currencies
     * 
     * @return Array
     */
    function getSupportedCurrencies() {
      return $this->supported_currencies;
    }//getSupportedCurrencies
    
    /**
     * Get payment gateway icon path
     */
    function getIconPath() {
      return $this->icon_path;
    } //getIconPath
   
    
    /**
     * Return true if this expense category is used for estimate
     * 
     * @return boolean
     */
    function isUsed() {
      return boolval(Payments::findByGateway($this));
    }//isUsed
    
   /**
  	 * Get payment gateway name 
  	 * 
  	 */
  	function getName() {
  	  return $this->getAdditionalProperty('name');
  	} //getName
  	
  	/**
  	 * Set payment gateway name
  	 * 
  	 * @param $value
  	 */
  	function setName($value) {
  	  $this->setAdditionalProperty('name',$value);
  	} //setName
  	
  	/**
  	 * Get payment gateway api_username 
  	 */
  	function getApiUsername() {
  	  return $this->getAdditionalProperty('api_username');
  	} //getApiUsername
  	
  	/**
  	 * Set payment gateway api_username
  	 * 
  	 * @param $value
  	 */
  	function setAPIUsername($value) {
  	  $this->setAdditionalProperty('api_username',$value);
  	} //setAPIUsername
  	
  	
  	/**
  	 * Get payment gateway api_login_id 
  	 * 
  	 */
  	function getApiLoginId() {
  	  return $this->getAdditionalProperty('api_login_id');
  	} //getApiLoginName
  	
  	/**
  	 * Set payment gateway api_login_id
  	 * 
  	 * @param $value
  	 */
  	function setApiLoginId($value) {
  	  $this->setAdditionalProperty('api_login_id',$value);
  	} //setApiLoginName
  	
  	/**
  	 * Get payment gateway transaction_id 
  	 * 
  	 */
  	function getTransactionId() {
  	  return $this->getAdditionalProperty('transaction_id');
  	} //getTransactionId
  	
  	/**
  	 * Set payment gateway transaction_id
  	 * 
  	 * @param $value
  	 */
  	function setTransactionId($value) {
  	  $this->setAdditionalProperty('transaction_id',$value);
  	} //setTransactionId
 	
  	/**
  	 * Get payment gateway api_password 
  	 */
  	function getApiPassword() {
  	  return $this->getAdditionalProperty('api_password');
  	} //getApiUsername
  	
  	/**
  	 * Set payment gateway api_password
  	 * 
  	 * @param $value
  	 */
  	function setAPIPassword($value) {
  	  $this->setAdditionalProperty('api_password',$value);
  	} //setAPIUsername
  	
  	/**
  	 * Get payment gateway api_signature 
  	 */
  	function getApiSignature() {
  	  return $this->getAdditionalProperty('api_signature');
  	} //getApiSignature
  	
  	/**
  	 * Set payment gateway api_signature
  	 * 
  	 * @param $value
  	 */
  	function setAPISignature($value) {
  	  $this->setAdditionalProperty('api_signature',$value);
  	} //setAPISignature
  	
  	/**
  	 * Get payment gateway go_live 
  	 */
  	function getGoLive() {
  	  return $this->getAdditionalProperty('go_live');
  	} //getGoLive
  	
  	/**
  	 * Set payment gateway go_live
  	 * 
  	 * @param $value
  	 */
  	function setGoLive($value) {
  	  $this->setAdditionalProperty('go_live',$value);
  	} //setGoLive
  	  	
  	/**
  	 * Check if payment gateway is default payment gateway
  	 * 
  	 * @return boolean
  	 */
  	function isDefault() {
  	  if($this->getIsDefault() == 1) {
  	    return true;
  	  } else {
  	    return false;
  	  } //if
  	} //isDefault
  	
  	/**
  	 * Display or hide user form
  	 * 
  	 */
  	function showUserForm() {
  	  if($this->isDefault()) {
  	    return 'block';
  	  }//if
  	    return 'none';
  	}//if
  	
  	/**
  	 * Returns main page url
  	 * 
  	 */
  	function getMainPageUrl() {
  	  return Router::assemble('payment_gateways_admin_section');
  	} //getMainPageUrl
  	
  	/**
  	 * Returns allow payments url
  	 * 
  	 */
  	function getAllowPaymentsUrl() {
  	  return Router::assemble('payment_gateways_allow_payments');
  	} //getAllowPaymentsUrl
  	
  	/**
  	 * Returns allow_payments_for_invoice_url
  	 * 
  	 */
  	function getAllowPaymentsForInvoiceUrl() {
  	  return Router::assemble('payment_gateways_allow_payments_for_invoice');
  	} //getAllowPartialPaymentsUrl
  	
  	/**
     * Get enforce settings URL
     */
    function getEnforceSettingsUrl() {
      return Router::assemble('payment_gateways_enforce_settings');
    }//getEnforceSettingsUrl
  	
  	/**
     * Return set as default gateway
     *
     * @param void
     * @return string
     */
    function getSetAsDefaultUrl() {
      return Router::assemble('admin_payment_set_as_default', array('payment_gateway_id' => $this->getId()));
    } // getSetAsDefaultPaymentGatewayUrl

    /**
     * Return enable url
     *
     * @param void
     * @return string
     */
    function getEnableUrl() {
      return Router::assemble('admin_payment_enable', array('payment_gateway_id' => $this->getId()));
    } // getEnableUrl
    
     /**
     * Return disable url
     *
     * @param void
     * @return string
     */
    function getDisableUrl() {
      return Router::assemble('admin_payment_disable', array('payment_gateway_id' => $this->getId()));
    } // getDisableUrl
    
    /**
     * Add new payment gateway URL
     *
     * @param void
     * @return string
     */
    function getAddUrl() {
      return Router::assemble('admin_payment_gateway_add');
    } // getAddUrl
    
    /**
     * Update payment gateway URL
     *
     * @param void
     * @return string
     */
    function getEditUrl() {
      return Router::assemble('admin_payment_gateway_edit', array('payment_gateway_id' => $this->getId()));
    } // getEditUrl
    
    /**
     * View payment gateway URL
     *
     * @return string
     */
    function getViewUrl() {
      return Router::assemble('admin_payment_gateway_view', array('payment_gateway_id' => $this->getId()));
    } // getViewUrl
    
    /**
     * Return delete payment gateway URL
     *
     * @param void
     * @return string
     */
    function getDeleteUrl() {
      return Router::assemble('admin_payment_gateway_delete', array('payment_gateway_id' => $this->getId()));
    } // getDeleteUrl
    
  	/**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $result['is_default'] = $this->getIsDefault();
      $result['is_enabled'] = $this->getIsEnabled();
      $result['is_used'] = $this->isUsed();
      
      $result['urls']['set_as_default'] = $this->getSetAsDefaultUrl();
      $result['urls']['disable'] = $this->getDisableUrl();
      $result['urls']['enable'] = $this->getEnableUrl();
      $result['urls']['delete'] = $this->getDeleteUrl();
      $result['urls']['edit'] = $this->getEditUrl();
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      throw new NotImplementedError(__METHOD__);
    } // describeForApi
    
    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------
    
    /**
     * Return routing context name
     *
     * @return string
     */
    function getRoutingContext() {
      return 'incoming_email_admin_mailbox';
    } // getRoutingContext
    
    /**
     * Return routing context parameters
     *
     * @return mixed
     */
    function getRoutingContextParams() {
      return array('mailbox_id' => $this->getId());
    } // getRoutingContextParams
    /**
     * Clear cache on save
     *
     * @return boolean
     */
    function save() {
      $name_changed = $this->isModifiedField('name');
      
      parent::save();
      
      if($name_changed) {
    	  cache_remove('payment_gateways_id'); // remove ID - name map from cache
      } // if
    	
    	return true;
    } // save
   
    
    /**
     * Validate before save
     *
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->getName()) {
          $errors->addError(lang('Payment gateway name is required'), 'name');
      } // if
      
      if($this instanceof PaypalGateway) {
        
        if(!$this->getApiUsername()) {
          $errors->addError(lang('API username is required'), 'api_username');
        } // if
        
        if(!$this->getApiPassword()) {
          $errors->addError(lang('API password is required'), 'api_password');
        } // if
      }//if
      
      if($this instanceof AuthorizeGateway) {
        if(!$this->getApiLoginId()) {
          $errors->addError(lang('API login ID is required'), 'api_login_id');
        } // if
        
        if(!$this->getTransactionId()) {
          $errors->addError(lang('Transaction ID is required'), 'transaction_id');
        } // if
      }//if
      
    } // validate
    
   
} //FwPaymentGateway