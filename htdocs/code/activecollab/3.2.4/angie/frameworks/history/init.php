<?php

  /**
   * History framework initialization file
   *
   * @package angie.frameworks.history
   */
  
  define('HISTORY_FRAMEWORK', 'history');
  define('HISTORY_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/history');
  
  // Inject history framework into given module
  @define('HISTORY_FRAMEWORK_INJECT_INTO', 'system');
  
  AngieApplication::setForAutoload(array(
    'FwModificationLog' => HISTORY_FRAMEWORK_PATH . '/models/modification_logs/FwModificationLog.class.php', 
    'FwModificationLogs' => HISTORY_FRAMEWORK_PATH . '/models/modification_logs/FwModificationLogs.class.php', 
    
    'IHistory' => HISTORY_FRAMEWORK_PATH . '/models/IHistory.class.php', 
    'IHistoryImplementation' => HISTORY_FRAMEWORK_PATH . '/models/IHistoryImplementation.class.php', 
    'FwHistoryRenderer' => HISTORY_FRAMEWORK_PATH . '/models/FwHistoryRenderer.class.php', 
  ));