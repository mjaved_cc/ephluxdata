<?php return array(
  'Filename: :filename ',
  'Type: :filetype',
  'Size: :filesize',
  'File is not correctly uploaded',
  'Failed to write uploaded file to the :folder_name folder',
  'Download',
  'Trash',
  'File name is required. Min length is 3 letters',
  'Attach Files',
  'Attachments preview for ":name" ',
  'Attachments',
  'There are no files attached to this :type',
); ?>