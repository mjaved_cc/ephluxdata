<?php

  /**
   * Framework level home screen controller implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage controllers
   */
  class FwHomescreenController extends Controller {
    
    /**
     * Active object that uses this home screen
     *
     * @var IHomescreen
     */
    protected $active_object;
    
    /**
     * Active home screen
     *
     * @var Homescreen
     */
    protected $active_homescreen;
    
    /**
     * Active home screen tab
     *
     * @var HomescreenTab
     */
    protected $active_homescreen_tab;
    
    /**
     * Active home screen widget
     *
     * @var HomescreenWidget
     */
    protected $active_homescreen_widget;
    
    /**
     * Execute before any action is executed
     */
    function __before() {
      parent::__before();
      
      if($this->active_object instanceof IHomescreen) {
        $this->active_homescreen = $this->active_object->homescreen()->get();
      } else {
        $this->active_homescreen = Homescreens::findDefault();
      } // if
      
      $this->wireframe->breadcrumbs->add('homescreen', lang('Home Screen'), $this->active_homescreen->getViewUrl());
      
      if(($this->active_object instanceof IHomescreen && $this->active_object->homescreen()->hasOwn()) || empty($this->active_object)) {
        $homescreen_tab_id = $this->request->get('homescreen_tab_id');
        if($homescreen_tab_id) {
          $this->active_homescreen_tab = HomescreenTabs::findById($homescreen_tab_id);
        } // if
        
        if($this->active_homescreen_tab instanceof HomescreenTab) {
          if($this->active_homescreen_tab->getHomescreenId() != $this->active_homescreen->getId()) {
            $this->response->badRequest();
          } // if
          
          $homescreen_widget_id = $this->request->getId('homescreen_widget_id');
          if($homescreen_widget_id) {
            $this->active_homescreen_widget = HomescreenWidgets::findById($homescreen_widget_id);
          } // if
          
          if($this->active_homescreen_widget instanceof HomescreenWidget && $this->active_homescreen_widget->getHomescreenTabId() != $this->active_homescreen_tab->getId()) {
            $this->response->badRequest();
          } // if
        } // if
      } // if
      
      $this->response->assign(array(
        'active_object' => $this->active_object, 
        'active_homescreen' => $this->active_homescreen, 
        'active_homescreen_tab' => $this->active_homescreen_tab, 
        'active_homescreen_widget' => $this->active_homescreen_widget, 
      ));
    } // __before
  
    /**
     * Show home screen management page
     */
    function homescreen() {
      if(($this->active_object instanceof IHomescreen && $this->active_object->homescreen()->hasOwn()) || empty($this->active_object)) {
        if($this->active_homescreen->canDelete($this->logged_user)) {
          $this->wireframe->actions->add('delete_deskop_set', lang('Revert to Default Home Screen'), $this->active_homescreen->getDeleteUrl(), array(
            'onclick' => new AsyncLinkCallback(array(
              'confirmation' => lang('Are you sure that you want to revert to default home screen? This will revert all custom changes that you made to this home screen'),
              'success_event' => 'homescreen_deleted', 
            )), 
          ));
        } // if
      } elseif($this->active_object instanceof IHomescreen && $this->active_object->homescreen()->canHaveOwn() && $this->active_object->homescreen()->canCreateSet($this->logged_user)) {
        if($this->active_object instanceof User) {
          if($this->logged_user->getId() == $this->active_object->getId()) {
            $label = lang('Configure Your Own Home Screen');
          } else {
            $label = lang("Configure :user's Home Screen", array(
              'user' => $this->active_object->getFirstName(true)
            ));
          } // if
        } else {
          $label = lang('Configure Home Screen');
        } // if
        
        $this->wireframe->actions->add('create_homescreen', $label, $this->active_object->homescreen()->getCreateUrl(), array(
          'onclick' => new FlyoutFormCallback('homescreen_created', array(
            'width' => 400
          )),
        	'icon' => AngieApplication::getImageUrl('layout/button-add.png', ENVIRONMENT_FRAMEWORK, AngieApplication::getPreferedInterface()),          
        ));
      } // if
    } // homescreen
    
    /**
     * Create a custom home screen for a given parent object
     */
    function homescreen_create() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_object instanceof IHomescreen && $this->active_object->homescreen()->canHaveOwn() && !$this->active_object->homescreen()->hasOwn()) {
          $homescreen_data = $this->request->post('homescreen');
          $this->response->assign('homescreen_data', $homescreen_data);
          
          if($this->request->isSubmitted()) {
            try {
              $based_on_id = array_var($homescreen_data, 'based_on_id');
              if($based_on_id) {
                $based_on = Homescreens::findById($based_on_id);
              } else {
                $based_on = null;
              } // if
              
              if($based_on instanceof Homescreen) {
                $homescreen = $this->active_object->homescreen()->createBasedOn($based_on);
              } else {
                $homescreen = $this->active_object->homescreen()->createBlank();
              } // if
              
              $this->response->respondWithData($homescreen, array(
              	'as' => 'homescreen', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } // if
          
          $this->setView('create_homescreen');
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_create
    
    /**
     * Remove custom home screen
     */
    function homescreen_delete() {
      if(($this->request->isApiCall() || $this->request->isAsyncCall()) && $this->request->isSubmitted()) {
        if($this->active_object instanceof IHomescreen && $this->active_object->homescreen()->canHaveOwn() && $this->active_object->homescreen()->hasOwn()) {
          if($this->active_object->homescreen()->canDeleteSet($this->logged_user)) {
            $homescreen = $this->active_object->homescreen()->getOwnSet();
            $homescreen->delete();
            
            $this->response->respondWithData($homescreen, array(
              'as' => 'homescreen', 
              'detailed' => true, 
            ));
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_delete
    
    // ---------------------------------------------------
    //  Home screen tabs
    // ---------------------------------------------------
    
    /**
     * Create a home screen tab
     */
    function homescreen_tabs_add() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_homescreen instanceof Homescreen && $this->active_homescreen->isLoaded()) {
          $homescreen_tab_data = $this->request->post('homescreen_tab');
          
          $this->response->assign('homescreen_tab_data', $homescreen_tab_data);
          
          if($this->request->isSubmitted()) {
            try {
              DB::beginWork('Creating home screen tab @ ' . __CLASS__);
              
              $homescreen_tab_type = array_var($homescreen_tab_data, 'type', null, true);
            
              if($homescreen_tab_type && class_exists($homescreen_tab_type)) {
                $this->active_homescreen_tab = new $homescreen_tab_type();
              } // if
              
              if(!($this->active_homescreen_tab instanceof HomescreenTab)) {
                $this->active_homescreen_tab = new SplitHomescreenTab();
              } // if
              
              $this->active_homescreen_tab->setAttributes($homescreen_tab_data);
              $this->active_homescreen_tab->setHomescreenId($this->active_homescreen->getId());
              $this->active_homescreen_tab->setPosition(HomescreenTabs::getNextPosition($this->active_homescreen));
              $this->active_homescreen_tab->save();
              
              DB::commit('Home screen tab created @ ' . __CLASS__);
              
              $this->response->respondWithData($this->active_homescreen_tab, array(
                'as' => 'homescreen_tab', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              DB::rollback('Failed to create home screen tab @ ' . __CLASS__);
              $this->response->exception($e);
            } // try
          } // if
          
          $this->setView('add_homescreen_tab');
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_tabs_add
    
    /**
     * Reorder home screen tabs
     */
    function homescreen_tabs_reorder() {
      if(($this->request->isApiCall() || $this->request->isAsyncCall()) && $this->request->isSubmitted()) {
        $homescreen_tabs = $this->request->post('homescreen_tabs');
        
        if(is_foreachable($homescreen_tabs)) {
          foreach($homescreen_tabs as $homescreen_tab_id => $position) {
            $homescreen_tab = $homescreen_tab_id ? HomescreenTabs::findById($homescreen_tab_id) : null;
            
            if($homescreen_tab instanceof HomescreenTab) {
              $homescreen_tab->setPosition($position);
              $homescreen_tab->save();
            } // if
          } // foreach
        } // if
        
        $this->response->ok();
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_tabs_reorder
    
    /**
     * Show homescreen tab details
     */
    function homescreen_tab() {
      if($this->request->isApiCall()) {
        if($this->active_homescreen_tab instanceof HomescreenTab && $this->active_homescreen_tab->isLoaded()) {
          $this->response->respondWithData($this->active_homescreen_tab, array(
            'as' => 'homescreen_tab', 
            'detailed' => true, 
          ));
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_tab
    
    /**
     * Update home screen tab
     */
    function homescreen_tab_edit() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_homescreen_tab instanceof HomescreenTab && $this->active_homescreen_tab->isLoaded()) {
          if(!$this->active_homescreen_tab->canEdit($this->logged_user)) {
            $this->response->forbidden();
          } // if
          
          $homescreen_tab_data = $this->request->post('homescreen_tab', array(
            'name' => $this->active_homescreen_tab->getName(), 
          ));
          
          $this->response->assign('homescreen_tab_data', $homescreen_tab_data);
            
          if($this->request->isSubmitted()) {
            try {
              DB::beginWork('Updating home screen tab @ ' . __CLASS__);
              
              $this->active_homescreen_tab->setAttributes($homescreen_tab_data);
              $this->active_homescreen_tab->save();
              
              DB::commit('Home screen tab updated @ ' . __CLASS__);
              
              $this->response->respondWithData($this->active_homescreen_tab, array(
                'as' => 'homescreen_tab', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              DB::rollback('Failed to updated home screen tab @ ' . __CLASS__);
              $this->response->exception($e);
            } // try
          } // if
          
          $this->setView('edit_homescreen_tab');
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_tab_edit
    
    /**
     * Remove home screen tab
     */
    function homescreen_tab_delete() {
      if(($this->request->isApiCall() || $this->request->isAsyncCall()) && $this->request->isSubmitted()) {
        if($this->active_homescreen_tab instanceof HomescreenTab && $this->active_homescreen_tab->isLoaded()) {
          if($this->active_homescreen_tab->canDelete($this->logged_user)) {
            try {
              $this->active_homescreen_tab->delete();
              $this->response->respondWithData($this->active_homescreen_tab, array(
                'as' => 'homescreen_tab', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_tab_delete
    
    // ---------------------------------------------------
    //  Widgets
    // ---------------------------------------------------
    
    /**
     * Add home screen widget
     */
    function homescreen_widgets_add() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_homescreen_tab instanceof HomescreenTab && $this->active_homescreen_tab->isLoaded()) {
          $column_id = (integer) $this->request->get('column_id');
          if(empty($column_id)) {
            $column_id = 1;
          } elseif($column_id > $this->active_homescreen_tab->countColumns()) {
            $column_id = $this->active_homescreen_tab->countColumns();
          } // if
          
          $widget_data = $this->request->post('homescreen_widget');
          
          $this->response->assign(array(
            'column_id' => $column_id, 
            'widget_data' => $widget_data, 
          ));
          
          if($this->request->isSubmitted()) {
            try {
              DB::beginWork('Adding home screen widget @ ' . __CLASS__);
              
              $widget_type = array_var($widget_data, 'type', null, true);
            
              if($widget_type && class_exists($widget_type, true)) {
                $this->active_homescreen_widget = new $widget_type();
              } // if
              
              if(!($this->active_homescreen_widget instanceof HomescreenWidget)) {
                throw new ValidationErrors(array(
                  'type' => lang('Invalid home screen widget type'), 
                ));
              } // if
              
              $this->active_homescreen_widget->setAttributes($widget_data);
              $this->active_homescreen_widget->setHomescreenTabId($this->active_homescreen_tab->getId());
              $this->active_homescreen_widget->setColumnId($column_id);
              $this->active_homescreen_widget->setPosition(HomescreenWidgets::getNextPosition($this->active_homescreen_tab, $column_id));
              
              $this->active_homescreen_widget->save();
              
              DB::commit('Home screen widget added @ ' . __CLASS__);
              
              $this->response->respondWithData($this->active_homescreen_widget, array(
                'as' => 'homescreen_widget', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              DB::rollback('Failed to add home screen widget @ ' . __CLASS__);
              $this->response->exception($e);
            } // try
          } // if
          
          $this->setView('add_homescreen_widget');
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_widgets_add
    
    /**
     * Reorder home screen widgets
     */
    function homescreen_widgets_reorder() {
      if(($this->request->isApiCall() || $this->request->isAsyncCall()) && $this->request->isSubmitted()) {
        if($this->active_homescreen_tab instanceof HomescreenTab && $this->active_homescreen_tab->isLoaded()) {
          $widgets_order = $this->request->post('widgets_order');
          
          // Reorder widgets
          if(is_foreachable($widgets_order)) {
            try {
              DB::beginWork('Reordering home screen widgets @ ' . __CLASS__);
              
              foreach($this->active_homescreen_tab->getWidgets() as $widget) {
                $widget_id = $widget->getId();
                
                if(isset($widgets_order[$widget_id])) {
                  $column_id = (integer) array_var($widgets_order[$widget_id], 'column_id');
                  $position = (integer) array_var($widgets_order[$widget_id], 'position');
                  
                  if($column_id) {
                    $widget->setColumnId($column_id);
                  } // if
                  
                  if($position) {
                    $widget->setPosition($position);
                  } // if
                  
                  $widget->save();
                } // if
              } // foreach
              
              DB::commit('Home screen widgets reordered @ ' . __CLASS__);
              $this->response->ok();
            } catch(Exception $e) {
              DB::rollback('Failed to reorder home screen widgets @ ' . __CLASS__);
              $this->response->exception($e);
            } // try
            
          // Nothing to reorder
          } else {
            $this->response->ok();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_widgets_reorder
    
    /**
     * Display home screen widget details
     */
    function homescreen_widget() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_homescreen_widget instanceof HomescreenWidget && $this->active_homescreen_widget->isLoaded()) {
          $this->response->respondWithData($this->active_homescreen_widget, array(
            'as' => 'homescreen_widget', 
            'detailed' => true, 
          ));
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_widget

    /**
     * Renders the homescreen widget
     */
    function homescreen_widget_render() {
      if (!$this->request->isAsyncCall()) {
        $this->response->badRequest();
      } // if

      if(!($this->active_homescreen_widget instanceof HomescreenWidget && $this->active_homescreen_widget->isLoaded())) {
        $this->response->notFound();
      } // if

      $temp_widget_id = $this->request->get('custom_widget_id', HTML::uniqueId('homescreen_widget'));

      $this->response->respondWithData(array(
        'title'   => $this->active_homescreen_widget->renderTitle($this->logged_user, $temp_widget_id),
        'body'    => $this->active_homescreen_widget->renderBody($this->logged_user, $temp_widget_id),
        'footer'  => $this->active_homescreen_widget->renderFooter($this->logged_user, $temp_widget_id)
      ));
    } // renders the homescreen widget
    
    /**
     * Update home screen widget settings
     */
    function homescreen_widget_edit() {
      if($this->request->isApiCall() || $this->request->isAsyncCall()) {
        if($this->active_homescreen_widget instanceof HomescreenWidget && $this->active_homescreen_widget->isLoaded()) {
          $widget_data = $this->request->post('homescreen_widget');
          $this->response->assign('widget_data', $widget_data);
          
          if($this->request->isSubmitted()) {
            try {
              DB::beginWork('Updating home screen widget @ ' . __CLASS__);
              
              $this->active_homescreen_widget->setAttributes($widget_data);
              $this->active_homescreen_widget->save();
              
              DB::commit('Updating widget added @ ' . __CLASS__);
              
              $this->response->respondWithData($this->active_homescreen_widget, array(
                'as' => 'homescreen_widget', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              DB::rollback('Failed to update home screen widget @ ' . __CLASS__);
              $this->response->exception($e);
            } // try
          } // if
          
          $this->setView('edit_homescreen_widget');
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_widget_edit
    
    /**
     * Delete home screen widget
     */
    function homescreen_widget_delete() {
      if(($this->request->isApiCall() || $this->request->isAsyncCall()) && $this->request->isSubmitted()) {
        if($this->active_homescreen_widget instanceof HomescreenWidget && $this->active_homescreen_widget->isLoaded()) {
          if($this->active_homescreen_widget->canDelete($this->logged_user)) {
            try {
              $this->active_homescreen_widget->delete();
              $this->response->respondWithData($this->active_homescreen_widget, array(
                'as' => 'homescreen_widget', 
                'detailed' => true, 
              ));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // homescreen_widget_delete
    
  }