<?php

  /**
   * Complete framework initialization file
   * 
   * @package angie.frameworks.complete
   */
  
  define('COMPLETE_FRAMEWORK', 'complete');
  define('COMPLETE_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/complete');
  
  @define('COMPLETE_FRAMEWORK_INJECT_INTO', 'system');
  
  AngieApplication::setForAutoload(array(
    'IComplete' => COMPLETE_FRAMEWORK_PATH . '/models/IComplete.class.php', 
    'ICompleteImplementation' => COMPLETE_FRAMEWORK_PATH . '/models/ICompleteImplementation.class.php',
  	'PriorityInspectorTitlebarWidget' => COMPLETE_FRAMEWORK_PATH . '/models/PriorityInspectorTitlebarWidget.class.php'
  ));