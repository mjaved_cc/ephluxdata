<?php

  /**
   * Globalization framework initialization file
   *
   * @package angie.frameworks.globalization
   */
  
  define('GLOBALIZATION_FRAMEWORK', 'globalization');
  define('GLOBALIZATION_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/globalization');
  
  // Inject framework into system module by default
  @define('GLOBALIZATION_FRAMEWORK_INJECT_INTO', 'system');
  
  // Route base for all globalization administration routes
  @define('GLOBALIZATION_ADMIN_ROUTE_BASE', 'admin');
  
  // Functions
  require_once GLOBALIZATION_FRAMEWORK_PATH . '/functions.php';
  
  AngieApplication::setForAutoload(array(
    'FwCurrency' => GLOBALIZATION_FRAMEWORK_PATH . '/models/currencies/FwCurrency.class.php',
    'FwCurrencies' => GLOBALIZATION_FRAMEWORK_PATH . '/models/currencies/FwCurrencies.class.php',
   
    'FwDayOff' => GLOBALIZATION_FRAMEWORK_PATH . '/models/day_offs/FwDayOff.class.php', 
    'FwDayOffs' => GLOBALIZATION_FRAMEWORK_PATH . '/models/day_offs/FwDayOffs.class.php',
   
    'FwLanguage' => GLOBALIZATION_FRAMEWORK_PATH . '/models/languages/FwLanguage.class.php', 
    'FwLanguages' => GLOBALIZATION_FRAMEWORK_PATH . '/models/languages/FwLanguages.class.php', 
  ));