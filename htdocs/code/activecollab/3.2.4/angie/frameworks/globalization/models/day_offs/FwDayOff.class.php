<?php

  /**
   * Framework level day off model implemetaiton
   *
   * @package angie.frameworks.globalization
   * @subpackage models
   */
  class FwDayOff extends BaseDayOff {
    
    /**
     * Validate before save
     *
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if($this->validatePresenceOf('name')) {
        if(!$this->validateUniquenessOf('name', 'event_date')) {
          $errors->addError(lang('Event already specified for given date'), 'name');
        } // if
      } else {
        $errors->addError(lang('Name is required'), 'name');
      } // if
      
      if(!$this->validatePresenceOf('event_date')) {
        $errors->addError(lang('Event date is required'), 'event_date');
      } // if
    } // validate
    
  }