<?php

  /**
   * Smarty for Angie initialization file
   * 
   * @package angie.vendor.smarty
   */

  // Location of Smarty for Angie library
  define('SMARTY_FOR_ANGIE_PATH', ANGIE_PATH . '/vendor/smarty');

  AngieApplication::setForAutoload(array(
    'SmartyForAngie' => SMARTY_FOR_ANGIE_PATH . '/SmartyForAngie.class.php', 
    'PharSmartyResource' => SMARTY_FOR_ANGIE_PATH . '/PharSmartyResource.class.php', 
  ));
  
  require_once SMARTY_FOR_ANGIE_PATH . '/smarty/Smarty.class.php';
  
  // Register Smarty auto-loader
  AngieApplication::registerAutoloader('smartyAutoload');