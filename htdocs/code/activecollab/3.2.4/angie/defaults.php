<?php

  /**
   * Default configuration option, that application and user can override via config/config.php or application level
   * defaults.php file
   *
   * @package angie
   */

  if(!defined('ROOT_URL') && php_sapi_name() == 'cli') {
    define('ROOT_URL', 'unknown'); // In case we are executing this command via CLI, for testing and initialization
  } // if

  @define('ADMIN_EMAIL', false);

  @define('PUBLIC_FOLDER_NAME', 'public');
  @define('PUBLIC_AS_DOCUMENT_ROOT', false);
  @define('FORCE_QUERY_STRING', true);
  @define('PATH_INFO_THROUGH_QUERY_STRING', true); // Force query string for hosts that does not support PATH_INFO or make problems with it
  @define('FORCE_ROOT_URL', true);
  @define('URL_BASE', ROOT_URL . '/index.php');
  @define('ASSETS_URL', ROOT_URL . '/assets');
  @define('ASSETS_PATH', PUBLIC_PATH . '/assets');

  @define('FORCE_INTERFACE', false);
  @define('FORCE_DEVICE_CLASS', false);
  @define('PROTECT_SCHEDULED_TASKS', true);
  @define('PROTECT_ASSETS_FOLDER', false);
  @define('PURIFY_HTML', true);
  @define('REMOVE_EMPTY_PARAGRAPHS', true);
  @define('MAINTENANCE_MESSAGE', null);
  @define('CREATE_THUMBNAILS', true); // create thumbnails for images
  @define('RESIZE_SMALLER_THAN', 524288); // resize images smaller than 500kb
  @define('IMAGE_SIZE_CONSTRAINT', '2240x1680'); // 4 mega pixels
  @define('COMPRESS_ASSET_REQUESTS', true);
  @define('PAGE_PLACEHOLDER', '-PAGE-');
  @define('NUMBER_FORMAT_DEC_SEPARATOR', '.');
  @define('NUMBER_FORMAT_THOUSANDS_SEPARATOR', ',');
  @define('DEFAULT_CSV_SEPARATOR', ',');

  @define('CACHE_PATH', ENVIRONMENT_PATH . '/cache');
  @define('COLLECTOR_CHECK_ETAG', true);

  // Caching
  define('USE_CACHE', true);
  if (!defined('CACHE_BACKEND')) {
    if(extension_loaded('apc')) {
      define('CACHE_BACKEND', 'APCCacheBackend');
    } else {
      define('CACHE_BACKEND', 'FileCacheBackend');
    } // if
  } // if
  define('CACHE_LIFETIME', 7200);

  // Cookies
  define('USE_COOKIES', true);
  if(!defined('COOKIE_DOMAIN')) {
    $parts = parse_url(ROOT_URL);
    if(is_array($parts) && isset($parts['host'])) {
      define('COOKIE_DOMAIN', $parts['host']);
    } else {
      define('COOKIE_DOMAIN', '');
    } // if
  } // if

  define('COOKIE_PATH', '/');
  if(substr(ROOT_URL, 0, 5) == 'https') {
    define('COOKIE_SECURE', 1);
  } else {
    define('COOKIE_SECURE', 0);
  } // if
  define('COOKIE_PREFIX', 'ac');

  @define('DEFAULT_MODULE', 'system');
  @define('DEFAULT_CONTROLLER', 'backend');
  @define('DEFAULT_ACTION', 'index');
  @define('DEFAULT_FORMAT', 'html');

  // Formats can be overriden with constants with same name that start with
  // USER_ (USER_FORMAT_DATE will override FORMAT_DATE)
  if(DIRECTORY_SEPARATOR == '\\') {
    @define('FORMAT_DATETIME', '%b %#d. %Y, %I:%M %p');
  } else {
    @define('FORMAT_DATETIME', '%b %e. %Y, %I:%M %p');
  } // if

  if(DIRECTORY_SEPARATOR == '\\') {
    @define('FORMAT_DATE', '%b %#d. %Y');
  } else {
    @define('FORMAT_DATE', '%b %e. %Y');
  } // if

  @define('FORMAT_TIME', '%I:%M %p');

  // Read environment name from environment path
  @define('ENVIRONMENT', substr(ENVIRONMENT_PATH, strrpos(ENVIRONMENT_PATH, '/') + 1));
  @define('DEVELOPMENT_PATH', ROOT . '/development');
  @define('UPLOAD_PATH', ENVIRONMENT_PATH . '/upload');
  @define('LIMIT_DISK_SPACE_USAGE', null);
  @define('BASIC_FILE_UPLOADS', false);

  @define('CUSTOM_PATH', ENVIRONMENT_PATH . '/custom');
  @define('LOCALIZATION_PATH', CUSTOM_PATH . '/localization');
  @define('IMPORT_PATH', ENVIRONMENT_PATH . '/import');
  @define('THUMBNAILS_PATH', ENVIRONMENT_PATH . '/thumbnails');
  @define('WORK_PATH', ENVIRONMENT_PATH . '/work');
  @define('THEMES_PATH', ENVIRONMENT_PATH . '/' . PUBLIC_FOLDER_NAME . '/assets/themes');