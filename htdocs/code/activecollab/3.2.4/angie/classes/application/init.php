<?php

  /**
   * Angie application initialization file
   *
   * @package angie.library.application
   */
  
  require_once ANGIE_PATH . '/classes/application/AngieApplication.class.php';
  
  spl_autoload_register(array('AngieApplication', 'autoload'));
  
  require_once ANGIE_PATH . '/classes/application/AngieApplicationAdapter.class.php';
  require_once ANGIE_PATH . '/classes/application/AngieFramework.class.php';
  require_once ANGIE_PATH . '/classes/application/AngieModule.class.php';

  AngieApplication::setForAutoload(array(
    'AngieApplicationModel' => ANGIE_PATH . '/classes/application/model/AngieApplicationModel.class.php', 
    'AngieFrameworkModel' => ANGIE_PATH . '/classes/application/model/AngieFrameworkModel.class.php', 
    'AngieModuleModel' => ANGIE_PATH . '/classes/application/model/AngieModuleModel.class.php', 
    'AngieFrameworkModelBuilder' => ANGIE_PATH . '/classes/application/model/AngieFrameworkModelBuilder.class.php',
  ));