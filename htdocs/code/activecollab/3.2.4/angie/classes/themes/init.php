<?php

  /**
   * Theme library initialization file
   *
   * @package angie.library.themes
   */

  require_once ANGIE_PATH . '/classes/themes/Theme.class.php';
  require_once ANGIE_PATH . '/classes/themes/ThemeAdapter.class.php';