<?php

AngieApplication::useController('shared_object', SYSTEM_MODULE);

/**
 * Notebooks Shared object controller delegate
 *
 * @package activeCollab.modules.notebooks
 * @subpackage controllers
 */
class NotebooksSharedObjectController extends SharedObjectController {

  /**
   * Shared object
   *
   * @var NotebookPage
   */
  protected $active_shared_notebook_page;

  /**
   * Do the stuff before
   */
  function __before() {
    parent::__before();

    $notebook_page_id = $this->request->get('notebook_page_id');
    $this->active_shared_notebook_page = NotebookPages::findById($notebook_page_id);
    if ($this->active_shared_notebook_page instanceof NotebookPage) {
      $this->wireframe->breadcrumbs->add('shared_notebook_page', $this->active_shared_notebook_page->getName(), $this->active_shared_object->sharing()->getPageUrl($this->active_shared_notebook_page));
    } else {
      $this->active_shared_notebook_page = new NotebookPage();
    } // if
  } // __before

  /**
   * Notebook page action
   */
  function notebook_page() {
    if ($this->active_shared_notebook_page->isNew()) {
      $this->response->notFound();
    } // if

    if ($this->active_shared_notebook_page->getState() < STATE_ARCHIVED) {
      $this->response->notFound();
    } // if

    $this->response->assign('active_shared_notebook_page', $this->active_shared_notebook_page);
  } // notebook_page

}