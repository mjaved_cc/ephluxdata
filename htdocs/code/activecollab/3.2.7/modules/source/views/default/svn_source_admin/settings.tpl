<div class="page_wrapper">
{form action="$settings_source_url" method="post" id="main_form"}
  <div id="radio_group">
  	<label for="radio_svn_extension">
      <input id="radio_svn_extension" type="radio" name="source[svn_type]" value="extension" 
      	{if !$svn_extension} disabled="disabled" 
      	{elseif $source_data.svn_type === "extension"} checked="checked" {/if}
      />
      {lang}SVN Extension{/lang}
    </label>
    <br />
  	{if !$svn_extension}
  	  <p class="details">{lang}This method requires SVN PHP extension to be installed. Read more about SVN extension in PHP documentation{/lang}:
  	  <a href="http://www.php.net/manual/en/book.svn.php">http://www.php.net/manual/en/book.svn.php</a></p>
  	{/if}
  	<label for="radio_svn_exec">
      <input id="radio_svn_exec" type="radio" name="source[svn_type]" value="exec" 
      	{if !$svn_exec_path} disabled="disabled"
      	{elseif $source_data.svn_type === "exec"} checked="checked" {/if}
      />
      {lang}SVN Over Executable Command{/lang}
    <label for="radio_svn_exec">
    {if !$svn_exec_path}
  	  <p class="details">{lang}This method requires XML PHP extension to be installed. Read more about XML extension in PHP documentation: 
  	  <a href="http://www.php.net/xml">http://www.php.net/xml</a>{/lang}</p>
  	{/if}
  </div>
  <div id="svn_exec">
    <table id="source_admin_table">
      <tr>
        <td class="field">
          {wrap field=svn_path}
            {label for=svn_path required=false}SVN Location{/label}
            {text_field id='svn_path' name="source[svn_path]" value=$source_data.svn_path}
            <p class="details">Enter path for SVN executable (without executable name)</p>
          {/wrap}
        </td>
        <td class="test_facility">
          <div class="admin_test_setting" id="check_svn_path">
            <button type="button"><span><span>{lang}Check SVN Path{/lang}</span></span></button>
            <span class="test_results">
              <span></span>
            </span>
          </div>
        </td>
      </tr>
    </table>

    <div class="col_wide">
      {wrap field=svn_config_dir}
        {label for=svn_config_dir required=false}SVN Config Directory Path{/label}
        {text_field id="svn_config_dir" name="source[svn_config_dir]" value=$source_data.svn_config_dir}
        <p class="details">{lang}Leave empty to use system default{/lang}</p>
      {/wrap}
    </div>

    <div class="col_wide">
      {wrap field=svn_trust_server_cert}
        {label for=svn_trust_server_cert required=false}SVN Trust Server Certificate ?{/label}
        {radio_field name="source[svn_trust_server_cert]" id=svn_trust_server_cert_yes checked=$source_data.svn_trust_server_cert label=Yes value=1}
        {radio_field name="source[svn_trust_server_cert]" id=svn_trust_server_cert_no checked=!$source_data.svn_trust_server_cert label=No value=0}
        <p class="details">{lang}This option is valid only for Subversion v1.6 and later{/lang}</p>
      {/wrap}
    </div>
  </div>
      
  {wrap_buttons}
    {submit}Save Changes{/submit}
  {/wrap_buttons}
    
{/form}
</div>
<input type="hidden" value="{$test_svn_url}" id="test_svn_url" />
{literal}
<!--Javascript-->
<script type="text/javascript">

    $(document).ready(function () {
      var test_results_div = $(this).find('.test_results');
      var test_div = test_results_div.parent();
      var radio_extension = $('#radio_svn_extension');
      var radio_exec = $('#radio_svn_exec');
      
	  if (! radio_exec.attr("checked")) {
		$('#svn_exec').hide();
	  }  
	  
      test_results_div.prepend('<img class="source_results_img" src="" alt=""/>');
      $('.source_results_img').hide();

      radio_extension.change(function () {
    	  $('#svn_exec').hide('fast');
	  });

      radio_exec.change(function () {
    	  $('#svn_exec').show('fast');
	  });	

      
//	   AJAX call for testing svn path
      $('#check_svn_path button').click(function () {
        $('.source_results_img').show();
        var svn_path = $('#svn_path').val();
        var svn_config_dir = $('#svn_config_dir').val();
        var indicator_img = $('.source_results_img');
        var result_span = test_div.find('.test_results span:eq(0)');
        indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl());
        result_span.html('');
        var test_url = App.extendUrl($('#test_svn_url').val(), {
          'async' : 1,
          'svn_path' : svn_path,
          'svn_config_dir' : svn_config_dir
        });
        $.get(test_url, function(data) {
          if ($.trim(data)=='true') {
            indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl('ok'));
            result_span.html(App.lang('SVN executable found'));
          } else {
            indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl('error'));
            result_span.html(App.lang('Error accessing SVN executable') + ': ' + data);
          } // if
      	});
      });
    });
</script>
{/literal}