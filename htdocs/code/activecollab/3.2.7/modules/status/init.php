<?php
  /**
   * Status module initialization file
   * 
   * @package activeCollab.modules.status
   */
  
  define('STATUS_MODULE', 'status');
  define('STATUS_MODULE_PATH', APPLICATION_PATH . '/modules/status'); 
  
  AngieApplication::useModel('status_updates', STATUS_MODULE);
?>