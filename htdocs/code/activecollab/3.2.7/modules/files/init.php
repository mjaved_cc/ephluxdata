<?php

  /**
   * Assets module initialization file
   *
   * @package activeCollab.modules.files
   */
  
  define('FILES_MODULE', 'files');
  define("FILES_MODULE_PATH", APPLICATION_PATH . '/modules/files');
  
  AngieApplication::useModel(array(
    'file_versions', 'text_document_versions', 
  ), FILES_MODULE);
  
  require FILES_MODULE_PATH . '/functions.php';
  
  AngieApplication::setForAutoload(array(
    'ProjectAsset' => FILES_MODULE_PATH . '/models/project_assets/ProjectAsset.class.php', 
    'ProjectAssets' => FILES_MODULE_PATH . '/models/project_assets/ProjectAssets.class.php',

    // Comments
    'IAssetCommentsImplementation' => FILES_MODULE_PATH . '/models/IAssetCommentsImplementation.class.php',
    'AssetComment' => FILES_MODULE_PATH . '/models/AssetComment.class.php',

		// FILES
    'File' => FILES_MODULE_PATH . '/models/files/File.class.php', 
    'Files' => FILES_MODULE_PATH . '/models/files/Files.class.php',
    'IFileDownloadImplementation' => FILES_MODULE_PATH . '/models/files/IFileDownloadImplementation.class.php',
    'IFileVersionsImplementation' => FILES_MODULE_PATH . '/models/files/IFileVersionsImplementation.class.php', 
    'IFileVersionDownloadImplementation' => FILES_MODULE_PATH . '/models/files/IFileVersionDownloadImplementation.class.php',
  	'IFilePreviewImplementation' => FILES_MODULE_PATH . '/models/files/IFilePreviewImplementation.class.php',
  	'IFileActivityLogsImplementation' => FILES_MODULE_PATH . '/models/files/IFileActivityLogsImplementation.class.php', 
   
	  // YOUTUBE 
    'YouTubeVideo' => FILES_MODULE_PATH . '/models/you_tube_videos/YouTubeVideo.class.php',
    'YouTubeVideos' => FILES_MODULE_PATH . '/models/you_tube_videos/YouTubeVideos.class.php',
  	'IYoutubePreviewImplementation' => FILES_MODULE_PATH . '/models/you_tube_videos/IYoutubePreviewImplementation.class.php',

  	// BOOKMARKS
    'Bookmark' => FILES_MODULE_PATH . '/models/bookmarks/Bookmark.class.php',
    'Bookmarks' => FILES_MODULE_PATH . '/models/bookmarks/Bookmarks.class.php',
    'IBookmarkPreviewImplementation' => FILES_MODULE_PATH . '/models/bookmarks/IBookmarkPreviewImplementation.class.php',
  
  	// TEXT DOCUMENTS
    'TextDocument' => FILES_MODULE_PATH . '/models/text_documents/TextDocument.class.php',
    'TextDocuments' => FILES_MODULE_PATH . '/models/text_documents/TextDocuments.class.php',
		'ITextDocumentPreviewImplementation' => FILES_MODULE_PATH . '/models/text_documents/ITextDocumentPreviewImplementation.class.php',
		'ITextDocumentVersionsImplementation' => FILES_MODULE_PATH . '/models/text_documents/ITextDocumentVersionsImplementation.class.php',
		'ITextDocumentActivityLogsImplementation' => FILES_MODULE_PATH . '/models/text_documents/ITextDocumentActivityLogsImplementation.class.php',
  
    'AssetCategory' => FILES_MODULE_PATH . '/models/AssetCategory.class.php', 
    'IAssetCategoryImplementation' => FILES_MODULE_PATH . '/models/IAssetCategoryImplementation.class.php',

    'FlyoutFileFormCallback' => FILES_MODULE_PATH . '/models/javascript_callbacks/FlyoutFileFormCallback.class.php',
    'FileVersionCreatedActivityLogCallback' => FILES_MODULE_PATH . '/models/javascript_callbacks/FileVersionCreatedActivityLogCallback.class.php',
    'TextDocumentVersionCreatedActivityLogCallback' => FILES_MODULE_PATH . '/models/javascript_callbacks/TextDocumentVersionCreatedActivityLogCallback.class.php',
  
  	'IAssetSearchItemImplementation' => FILES_MODULE_PATH . '/models/IAssetSearchItemImplementation.class.php',
  	'IProjectAssetInspectorImplementation' => FILES_MODULE_PATH . '/models/IProjectAssetInspectorImplementation.class.php',
  
  	// SHARING
  	'IProjectAssetSharingImplementation' => FILES_MODULE_PATH . '/models/IProjectAssetSharingImplementation.class.php', 
  	'IBookmarkSharingImplementation' => FILES_MODULE_PATH . '/models/bookmarks/IBookmarkSharingImplementation.class.php',
  	'IYoutubeVideoSharingImplementation' => FILES_MODULE_PATH . '/models/you_tube_videos/IYoutubeVideoSharingImplementation.class.php',
  	'IFileSharingImplementation' => FILES_MODULE_PATH . '/models/files/IFileSharingImplementation.class.php',
  	'ITextDocumentSharingImplementation' => FILES_MODULE_PATH . '/models/text_documents/ITextDocumentSharingImplementation.class.php',

    // Project exporter
    'FilesProjectExporter' => FILES_MODULE_PATH . '/models/FilesProjectExporter.class.php',
  ));
  
  DataObjectPool::registerTypeLoader(array('File', 'TextDocument', 'Bookmark', 'YouTubeVideo'), function($ids) {
    return ProjectAssets::findByIds($ids, STATE_TRASHED, VISIBILITY_PRIVATE);
  });
  
  DataObjectPool::registerTypeLoader('AssetComment', function($ids) {
    return Comments::findByIds($ids);
  });