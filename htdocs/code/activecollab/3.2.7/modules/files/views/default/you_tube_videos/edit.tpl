{title}Edit YouTube Video{/title}
{add_bread_crumb}Edit Youtube Video{/add_bread_crumb}

<div id="edit_youtube">
  {form action=$active_asset->getEditUrl() method=post enctype="multipart/form-data" autofocus=yes ask_on_leave=yes class='big_form youtube_form'}
    {include file=get_view_path('_you_tube_video_form', 'you_tube_videos', 'files')}
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>