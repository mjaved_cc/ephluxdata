{add_bread_crumb}Bookmark Details{/add_bread_crumb}

{object object=$active_asset user=$logged_user show_body=false}
	{$active_asset->preview()->renderLarge() nofilter}
	
  {object_comments object=$active_asset user=$logged_user interface=AngieApplication::INTERFACE_PHONE id=bookmark_comments}
  
  {if $active_asset->getState() == $smarty.const.STATE_VISIBLE}
  	{render_comment_form object=$active_asset id=bookmark_comments}
  {/if}
{/object}