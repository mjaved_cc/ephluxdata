{title}Edit Bookmark{/title}
{add_bread_crumb}Edit Bookmark{/add_bread_crumb}

<div id="edit_bookmark">
  {form action=$active_asset->getEditUrl()}
    {include file=get_view_path('_bookmark_form', 'bookmarks', $smarty.const.FILES_MODULE)}
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>