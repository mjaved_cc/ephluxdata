<?php

  /**
   * Project object attachments helper implementation
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class IProjectObjectAttachmentsImplementation extends IAttachmentsImplementation {
    
    /**
     * Create a new attachment instance
     *
     * @return ProjectObjectAttachment
     */
    function newAttachment() {
      return new ProjectObjectAttachment();
    } // newAttachment
    
  }