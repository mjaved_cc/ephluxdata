<?php

  /**
   * Backend wireframe
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  abstract class BackendWireframe extends FwBackendWireframe {
    
//    /**
//     * Construct backend wireframe
//     *
//     * @param Request $request
//     */
//    function __construct(Request $request) {
//    	parent::__construct($request);
//
//      $this->breadcrumbs->add('home', lang('Home'), Router::assemble('homepage'));
//    } // __construct
    
    /**
     * Return init wireframe parameters
     * 
     * @param User $user
     * @return array
     */
    function getInitParams($user = null) {
      $params = parent::getInitParams($user);
      
      if($user instanceof User) {
        $params['search_url'] = Router::assemble('backend_search');
        $params['quick_search_url'] = Router::assemble('quick_backend_search');
      } // if
      
      if ($user instanceof User) {
      	$params['menu_items_current'] = $this->getCurrentMenuItem();
      	$params['page_tabs'] = $this->tabs->toArray();
      	$params['page_tabs_current'] = $this->tabs->getCurrentTab();
      	$params['breadcrumbs'] = $this->breadcrumbs;
      	$params['page_title'] = $this->getPageTitle();
      	$params['page_title_actions'] = $this->actions;
      	$params['list_mode_enabled'] = $this->list_mode->isEnabled();
      	$params['print_url'] = $this->print->getUrl();
      	
      	if (AngieApplication::isInDevelopment() || AngieApplication::isInDebugMode()) {
	      	$params['benchmark'] = array(
	      		'execution_time'	=> number_format(BenchmarkForAngie::getTimeElapsed()),
	      		'memory_usage'		=> format_file_size(BenchmarkForAngie::getMemoryUsage()),
	      		'queries_count'		=> BenchmarkForAngie::getQueriesCount()
	      	);
      	} // if
      } // if

      return $params;
    } // getInitParams
    
    
  }