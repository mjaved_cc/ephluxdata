<?php

  /**
   * Application level attachments class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class Attachments extends FwAttachments {

    /**
     * Find all project attachments
     *
     * @param Project $project
     * @param integer $min_state
     * @return DBResult
     */
    static function findForApiByProject(Project $project, $min_state = STATE_ARCHIVED) {
      $attachments_table = TABLE_PREFIX . 'attachments';

      if($project->getState() >= STATE_VISIBLE) {
        $map = Attachments::findTypeIdMapOfPotentialAttachmentParentsByProject($project, $min_state);

        if($map) {
          $conditions = array();

          foreach($map as $type => $ids) {
            $conditions[] = DB::prepare('(parent_type = ? AND parent_id IN (?))', array($type, $ids));
          } // if

          $conditions = implode(' OR ', $conditions);

          $result = DB::execute("SELECT id, type, parent_type, parent_id, state, name, mime_type, size FROM $attachments_table WHERE ($conditions) AND state >= ?", $min_state);

          if($result) {
            $result->setCasting(array(
              'id' => DBResult::CAST_INT,
              'parent_id' => DBResult::CAST_INT,
              'state' => DBResult::CAST_INT,
              'size' => DBResult::CAST_INT,
            ));

            return $result;
          } // if
        } // if
      } // if

      return null;
    } // findForApiByProject

    /**
     * Find type ID map of potential attechment parents in a given project
     *
     * @param Project $project
     * @param integer $min_state
     * @return array
     */
    static function findTypeIdMapOfPotentialAttachmentParentsByProject(Project $project, $min_state = STATE_ARCHIVED) {
      $map = array();

      $rows = DB::execute('SELECT id, type FROM ' . TABLE_PREFIX . 'project_objects WHERE project_id = ? AND state >= ?', $project->getId(), $min_state);
      if($rows) {
        foreach($rows as $row) {
          if(array_key_exists($row['type'], $map)) {
            $map[$row['type']][] = (integer) $row['id'];
          } else {
            $map[$row['type']] = array((integer) $row['id']);
          } // if
        } // foreach
      } // if

      EventsManager::trigger('on_extend_project_items_type_id_map', array(&$project, $min_state, &$map));

      $comment_parent_conditions = array();
      foreach($map as $type => $ids) {
        $comment_parent_conditions[] = DB::prepare('(parent_type = ? AND parent_id IN (?))', array($type, $ids));
      } // foreach

      if(count($comment_parent_conditions)) {
        $comment_parent_conditions = implode(' OR ', $comment_parent_conditions);

        $rows = DB::execute('SELECT id, type FROM ' . TABLE_PREFIX . "comments WHERE ($comment_parent_conditions) AND state >= ?", $min_state);
        if($rows) {
          $rows->setCasting(array(
            'id' => DBResult::CAST_INT,
          ));

          foreach($rows as $row) {
            if(array_key_exists($row['type'], $map)) {
              $map[$row['type']][] = $row['id'];
            } else {
              $map[$row['type']] = array($row['id']);
            } // if
          } // foreach
        } // if
      } // if

      return count($map) ? $map : null;
    } // findTypeIdMapOfPotentialAttachmentParentsByProjects

  }