<?php

  /**
   * Subtasks class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class Subtasks extends FwSubtasks {

    /**
     * Find subtasks for outline
     *
     * @param ISubtasks $parent
     * @param IUser $user
     * @param $visibility
     *
     * @return array
     */
    function findForOutline(ISubtasks $parent, IUser $user) {
      $parent_id = $parent->getId();
      $parent_type = $parent->getType();
      $can_edit_parent = $parent->canEdit($user);
      $subtask_class = 'ProjectObjectSubtask';

      $subtask_ids = DB::executeFirstColumn('SELECT id FROM ' . TABLE_PREFIX . 'subtasks WHERE parent_id = ? AND parent_type = ? AND completed_on IS NULL AND state >= ?', $parent_id, $parent_type, STATE_VISIBLE);

      if (!is_foreachable($subtask_ids)) {
        return false;
      } // if

      $subtasks = DB::execute('SELECT id, body, due_on, assignee_id, label_id, priority FROM ' . TABLE_PREFIX . 'subtasks WHERE ID IN(?)', $subtask_ids);

      // casting
      $subtasks->setCasting(array(
        'due_on'        => DBResult::CAST_DATE,
        'start_on'      => DBResult::CAST_DATE
      ));

      $subtasks_id_prefix_pattern = '--SUBTASK-ID--';
      $routing_context = $parent->getRoutingContext();
      $routing_params = array_merge($parent->getRoutingContextParams(), array('subtask_id' => $subtasks_id_prefix_pattern));
      $view_subtask_url_pattern = Router::assemble($routing_context . '_subtask', $routing_params);
      $edit_subtask_url_pattern = Router::assemble($routing_context . '_subtask_edit', $routing_params);
      $trash_subtask_url_pattern = Router::assemble($routing_context . '_subtask_trash', $routing_params);
      $subscribe_subtask_url_pattern = Router::assemble($routing_context . '_subtask_subscribe', $routing_params);
      $unsubscribe_subtask_url_pattern = Router::assemble($routing_context . '_subtask_unsubscribe', $routing_params);
      $reschedule_subtask_url_pattern = Router::assemble($routing_context . '_subtask_reschedule', $routing_params);
      $complete_subtask_url_pattern = Router::assemble($routing_context . '_subtask_complete', $routing_params);

      // all subscriptions
      $user_subscriptions_on_tasks = DB::executeFirstColumn('SELECT parent_id FROM ' . TABLE_PREFIX . 'subscriptions WHERE parent_id IN (?) AND parent_type = ? AND user_id = ?', $subtask_ids, $subtask_class, $user->getId());

      $results = array();
      foreach ($subtasks as $subobject) {
        $subtask_id = array_var($subobject, 'id');

        $results[] = array(
          'id'                  => $subtask_id,
          'name'                => array_var($subobject, 'body'),
          'class'               => $subtask_class,
          'priority'            => array_var($subobject, 'priority'),
          'parent_id'           => $parent_id,
          'parent_class'        => $parent_type,
          'due_on'              => array_var($subobject, 'due_on'),
          'assignee_id'         => array_var($subobject, 'assignee_id'),
          'label_id'            => array_var($subobject, 'label_id', null),
          'user_is_subscribed'  => in_array($subtask_id, $user_subscriptions_on_tasks),
          'event_names'         => array(
            'updated'             => 'subtask_updated'
          ),
          'urls'                => array(
            'view'                => str_replace($subtasks_id_prefix_pattern, $subtask_id, $view_subtask_url_pattern),
            'edit'                => str_replace($subtasks_id_prefix_pattern, $subtask_id, $edit_subtask_url_pattern),
            'trash'               => str_replace($subtasks_id_prefix_pattern, $subtask_id, $trash_subtask_url_pattern),
            'subscribe'           => str_replace($subtasks_id_prefix_pattern, $subtask_id, $subscribe_subtask_url_pattern),
            'unsubscribe'         => str_replace($subtasks_id_prefix_pattern, $subtask_id, $unsubscribe_subtask_url_pattern),
            'reschedule'          => str_replace($subtasks_id_prefix_pattern, $subtask_id, $reschedule_subtask_url_pattern),
            'complete'            => str_replace($subtasks_id_prefix_pattern, $subtask_id, $complete_subtask_url_pattern),
          ),
          'permissions'         => array(
            'can_edit'            => $can_edit_parent,
            'can_trash'           => $can_edit_parent,
          )
        );
      } // foreach

      return $results;
    } // findForOutline
    
  }