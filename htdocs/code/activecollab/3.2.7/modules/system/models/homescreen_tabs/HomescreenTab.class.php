<?php

  /**
   * Homescreen tab
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  abstract class HomescreenTab extends FwHomescreenTab {
    
    /**
     * Return URL of page that will render this home screen tab
     * 
     * @return string
     */
    function getHomescreenTabUrl() {
      return Router::assemble('homepage', array('homescreen_tab_id' => $this->getId()));
    } // getHomescreenTabUrl
    
  }