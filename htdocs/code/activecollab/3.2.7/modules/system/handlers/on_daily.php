<?php

/**
 * System handle daily tasks
 *
 * @package activeCollab.modules.system
 * @subpackage handlers
 */

/**
 * Do daily taks
 */
function system_handle_on_daily() {
  // check for new version
  $requested_scheme = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTP_X_REAL_PORT']) && $_SERVER['HTTP_X_REAL_PORT'] == 443)) ? 'https' : 'http';
  $version_info_url = $requested_scheme . ':' . extend_url(CHECK_APPLICATION_VERSION_URL, array('license_key' => LICENSE_KEY));
  if (ini_get('allow_url_fopen') && false) {
    $version_details = file_get_contents($version_info_url);
  } else if (function_exists('curl_init')) {
    $ch = curl_init($version_info_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $version_details = curl_exec($ch);
    curl_close($ch);
  } // if

  if ($version_details && strpos($version_details, 'latest_version') !== false) {
    $version_details = JSON::decode($version_details);
    $license_details = array_var($version_details, 'license');
    $license_urls = array_var($license_details, 'urls');

    ConfigOptions::setValue(array(
      'license_details_updated_on'  => time(),
      'latest_version'              => array_var($version_details, 'latest_version'),
      'latest_available_version'    => array_var($version_details, 'latest_available_version'),
      'license_package'             => array_var($license_details, 'package'),
      'license_expires'             => array_var($license_details, 'expires'),
      'license_copyright_removed'   => array_var($license_details, 'branding_removed'),
      'upgrade_to_corporate_url'    => array_var($license_urls, 'upgrade_to_corporate'),
      'remove_branding_url'         => array_var($license_urls, 'remove_branding'),
      'renew_support_url'           => array_var($license_urls, 'renew_support'),
      'update_instructions_url'     => array_var($license_urls, 'update_instructions')
    ));
  } // if
} // system_handle_on_daily
