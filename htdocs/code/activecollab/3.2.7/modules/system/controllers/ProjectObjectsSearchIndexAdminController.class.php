<?php

  // Build on top of search index controller
  AngieApplication::useController('search_index_admin', SEARCH_FRAMEWORK_INJECT_INTO);

  /**
   * Project objects search index administration controller
   * 
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class ProjectObjectsSearchIndexAdminController extends SearchIndexAdminController {
  
    /**
     * Execute before other any controller action
     */
    function __before() {
      parent::__before();
      
      if(!($this->active_search_index instanceof ProjectObjectsSearchIndex)) {
        $this->response->operationFailed();
      } // if
    } // __before
    
    /**
     * Build search index
     */
    function build() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        $project_id = $this->request->getId('project_id');
        
        $project = $project_id ? Projects::findById($project_id) : null;
        
        if($project instanceof Project) {
          $users_map = $project->users()->getIdNameMap(); // Prepare users map
          $milestones_map = array(); // Load and index milestones
          
          $milestones = DB::execute("SELECT id, name, body, visibility, assignee_id, priority, due_on, completed_on FROM " . TABLE_PREFIX . "project_objects WHERE type = 'Milestone' AND project_id = ? AND state >= ?", $project_id, STATE_ARCHIVED);
          
          if($milestones) {
            $item_context = 'projects:projects/' . $project->getId() . '/milestones';
            
            $project_name = $project->getName(); // Lets shave off a couple of function calls
            
            foreach($milestones as $milestone) {
              $milestone_id = (integer) $milestone['id'];
              $assignee_id = $milestone['assignee_id'] ? (integer) $milestone['assignee_id'] : null;
              
              Search::set($this->active_search_index, array(
                'class' => 'Milestone', 
                'id' => $milestone_id, 
                'context' => $item_context,
                'project_id' => $project_id, 
        				'project' => $project_name, 
                'name' => $milestone['name'], 
                'body' => $milestone['body'] ? $milestone['body'] : null,
                'visibility' => $milestone['visibility'], 
                'assignee_id' => $assignee_id, 
                'assignee' => $assignee_id && isset($users_map[$assignee_id]) ? $users_map[$assignee_id] : null, 
                'priority' => (integer) $milestone['priority'], 
                'due_on' => $milestone['due_on'], 
                'completed_on' => $milestone['completed_on'], 
              ));
            } // foreach
            
            $milestones_map[$milestone_id] = $milestone['name'];
          } // if
          
          // Trigger event that notifies all modules to rebuild index for a 
          // given projects
          EventsManager::trigger('on_build_project_search_index', array(&$this->active_search_index, &$project, &$users_map, &$milestones_map));
          
          $this->response->ok();
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // build
    
  }