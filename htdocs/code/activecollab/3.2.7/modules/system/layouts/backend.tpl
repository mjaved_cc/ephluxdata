<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us" class="loading">
<head>
  <!-- Meta -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" >
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />

{if activeCollab::getBrandingRemoved()}
  <meta name="msapplication-TileImage" content="{image_url name='layout/windows-tiles/not-branded.png' module=$smarty.const.SYSTEM_MODULE interface=$smarty.const.AngieApplication::INTERFACE_DEFAULT}">
{else}
  <meta name="msapplication-TileImage" content="{image_url name='layout/windows-tiles/branded.png' module=$smarty.const.SYSTEM_MODULE interface=$smarty.const.AngieApplication::INTERFACE_DEFAULT}">
{/if}
  <meta name="msapplication-TileColor" content="#95000">

  <title>{lang}Loading{/lang}...</title>
	<link rel="shortcut icon" href="{brand what=favicon}" type="image/x-icon" />

  <!--[if lte IE 8]>
  <script language="javascript" type="text/javascript" src="{$wireframe->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_DEFAULT, AngieApplication::getDeviceClass(), 'ie8', true)}"></script>
  <!--[elseif lte IE 9]>
    <script language="javascript" type="text/javascript" src="{$wireframe->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_DEFAULT, AngieApplication::getDeviceClass(), 'ie9', true)}"></script>
  <![endif]-->
  
  <!-- Head tags -->
  {if $wireframe->getAllHeadTags()}
    {foreach $wireframe->getAllHeadTags() as $head_tag}
      {$head_tag nofilter}
    {/foreach}
  {/if}

  {if is_foreachable($wireframe->getRssFeeds())}
    {foreach $wireframe->getRssFeeds() as $rss_feed}
      <link rel="alternate" type="{$rss_feed.feed_type}" title="{$rss_feed.title}" href="{$rss_feed.url}" />
    {/foreach}
  {/if}

  <style type="text/css">
    html.loading {
      margin: 0px;
      padding: 0px;
      height: 100%;
      font-family: Verdana,​ Arial,​ Helvetica,​ sans-serif;
    }

    html.loading body {
      padding: 0px;
      margin: 0px;
      height: 100%;
      background: #ecede4;
      position: relative;
    }

    html.loading body div#page_preloader {
      width: 300px;
      height: 56px;
      text-align: center;
      position: absolute;
      left: 50%;
      top: 50%;
      margin-left: -150px;
      margin-top: -20px;
    }

    html.loading body div#page_preloader h3 {
      font-size: 12px;
      margin: 0px 0px 13px 0px;
      color: #68695E;
      text-shadow: 0px 1px 0px #FFF;
      font-weight: bold !important;
    }

    html.loading body div#page_preloader div#page_preloader_progressbar {
      background: #FFFFFF;
      height: 28px;
      border-radius: 10px;
      position: relative;
      overflow: hidden;
      background: url('{$smarty.const.ASSETS_URL nofilter}/images/environment/default/layout/login/loader.gif') no-repeat center center;
    }
  </style>

  {if !($wireframe->getInitParams($logged_user) instanceof User)}
      <!-- *%NOT LOGGED IN%* -->
  {/if}
</head>

<body class="{implode values=$wireframe->getBodyClasses() separator=' '}">

  <div id="page_preloader">
    <h3>{lang}Loading{/lang} ...</h3>
    <div id="page_preloader_progressbar"></div>
  </div>

  <script type="text/javascript">
      var stylesheets_loaded = false;
      var javascript_loaded = false;
      var page_initialized = false;
      var document_head = document.head ? document.head : document.getElementsByTagName('head')[0];

      var initialize_page = function () {
        if (stylesheets_loaded && javascript_loaded && !page_initialized) {
          // remove preloader
          var preloader = document.getElementById('page_preloader');
          preloader.parentNode.removeChild(preloader);

          // remove class from html element
          document.documentElement.removeAttribute('class');

          // set template vars
          App.Config.reset({template_vars_to_js wireframe=$wireframe wrap=false});

          App.Wireframe.Navigation.init({
            'init_params'       : {$wireframe->getInitParams($logged_user)|json nofilter},
            'loaded_content'    : {$content_for_layout|json nofilter},
            'loaded_url'        : {$request->getUrl()|json nofilter}
          });
        } // if
      }; // initialize_page

      var load_style_sheet = function ( path, fn, scope, id ) {
        var head = document.getElementsByTagName( 'head' )[0], // reference to document.head for appending/ removing link nodes
        link = document.createElement( 'link' );           // create the link node
        link.setAttribute( 'href', path );
        link.setAttribute( 'rel', 'stylesheet' );
        link.setAttribute( 'type', 'text/css' );
        link.setAttribute( 'id', id );

        if (navigator && navigator.userAgent.indexOf('MSIE 8.') != -1) {
          var stylesheet_interval = setInterval(function () {
            for (var x in document.styleSheets) {
              if (document.styleSheets[x]['id'] == id) {
                clearInterval(stylesheet_interval);
                stylesheets_loaded = true;
                initialize_page();
              } // if
            } // for
          }, 50);
        } else {
          var sheet, cssRules;
          if ( 'sheet' in link ) {
            sheet = 'sheet'; cssRules = 'cssRules';
          } else {
            sheet = 'styleSheet'; cssRules = 'rules';
          } // if

          var interval_id = setInterval( function() {                    // start checking whether the style sheet has successfully loaded
            try {
              if ( link[sheet] && link[sheet][cssRules].length ) { // SUCCESS! our style sheet has loaded
                clearInterval( interval_id );                     // clear the counters
                clearTimeout( timeout_id );
                fn.call( scope || window, true, link );           // fire the callback with success == true
              }
            } catch( e ) {} finally {}
          }, 10 ),                                                   // how often to check if the stylesheet is loaded
          timeout_id = setTimeout( function() {       // start counting down till fail
            clearInterval( interval_id );            // clear the counters
            clearTimeout( timeout_id );
            head.removeChild( link );                // since the style sheet didn't load, remove the link node from the DOM
            fn.call( scope || window, false, link ); // fire the callback with success == false
          }, 15000 );
        } // if

        document_head.appendChild( link );
        return link;
      } // load_style_sheet

      var load_script = function (url, callback, id) {
        // create script element
        var script_tag = document.createElement('script');
        script_tag.type = "text/javascript";
        script_tag.src = url;
        script_tag.id = id;

        if (navigator && navigator.userAgent.indexOf('MSIE 8.') != -1) {
          var javascript_interval = setInterval(function () {
            if (window.main_javascript_loaded) {
              callback();
              clearInterval(javascript_interval);
            } // if
          }, 50);

        } else {
          script_tag.onload = callback;
        } // if

        document_head.appendChild(script_tag);
        return script_tag
      } // load_script

      load_script('{$wireframe->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_DEFAULT, AngieApplication::getDeviceClass(), $wireframe->getAssetsContext()) nofilter}', function () {
        javascript_loaded = true; initialize_page();
      }, 'main_javascript');

      load_style_sheet('{$wireframe->getCollectedStylesheetsUrl(AngieApplication::INTERFACE_DEFAULT, AngieApplication::getDeviceClass(), $wireframe->getAssetsContext()) nofilter}', function () {
        stylesheets_loaded = true;
        initialize_page();
      }, null, 'main_stylesheet');
  </script>
</body>
</html>