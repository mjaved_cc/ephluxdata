<div class="welcome_homescreen_widget">
  <div class="welcome_homescreen_widget_logo">
    <img src="{$logo_url}">
  </div>
  
{if $welcome_message}
  <div class="welcome_homescreen_widget_welcome_message">{$welcome_message|clean|clickable|nl2br nofilter}</div>
{/if}
</div>