{title}New Project{/title}
{add_bread_crumb}New Project{/add_bread_crumb}

<div id="new_project">
  {form action=Router::assemble('projects_add')}
    {include file=get_view_path('_project_form', 'project', $smarty.const.SYSTEM_MODULE)}
    
    {wrap_buttons}
      {submit}Create Project{/submit}
    {/wrap_buttons}
  {/form}
</div>