<?php

  /**
   * on_project_get_started event handler implementation
   *
   * @package activeCollab.modules.discussions
   * @subpackage handlers
   */

  /**
   * Handle on_project_get_started event
   *
   * @param Project $project
   * @param Project $template
   * @param NamedList $items
   * @param Smarty $smarty
   */
  function discussions_handle_on_project_get_started(Project &$project, &$template, NamedList &$items, &$smarty) {
    $smarty->assign('discussions_kickoff_url', Router::assemble('project_discussions_kickoff', array('project_slug' => $project->getSlug())));
    
    $items->add('kick_off_discussion', array(
      'text' => lang('Create a Kickoff Discussion'), 
      'description' => lang('This could be a great time to create a kickoff discussion. Everyone in the project will be notified about it, and they can join the discussion if they have some comments or questions'), 
      'url' => Router::assemble('project_discussions_add', array('project_slug' => $project->getSlug())), 
      'icon' => AngieApplication::getImageUrl('icons/32x32/discussion.png', DISCUSSIONS_MODULE), 
      'content' => $smarty->fetch(get_view_path('_project_kickoff', null, DISCUSSIONS_MODULE)), 
    ));
  } // discussions_handle_on_project_get_started