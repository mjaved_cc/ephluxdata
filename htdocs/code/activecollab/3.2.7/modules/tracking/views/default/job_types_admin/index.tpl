{title}Job Types & Hourly Rates{/title}
{add_bread_crumb}All{/add_bread_crumb}

<div id="job_types"></div>

<script type="text/javascript">
  $('#job_types').pagedObjectsList({
    'load_more_url' : '{assemble route=job_types_admin}', 
    'items' : {$job_types|json nofilter},
    'items_per_load' : {$job_types_per_page}, 
    'total_items' : {$total_job_types}, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'job_type' }, 
    'columns' : {
      'is_default' : '', 
      'name' : App.lang('Job Type'), 
      'default_hourly_rate' : App.lang('Default Hourly Rate'), 
      'options' : '' 
    },
    'empty_message' : App.lang('There are no job types defined'), 
    'listen' : 'job_type', 
    'on_add_item' : function(item) {
      var job_type = $(this);
      
      job_type.append(
       	'<td class="is_default"></td>' + 
        '<td class="name"></td>' + 
        '<td class="default_hourly_rate"></td>' + 
        '<td class="options"></td>'
      );

      var radio = $('<input name="set_default_job_type" type="radio" value="' + job_type['id'] + '" />').click(function() {
        if(!job_type.is('tr.is_default')) {
          if(confirm(App.lang('Are you sure that you want to set this job type as default job type?'))) {
            var cell = radio.parent();
            
            $('#job_types td.is_default input[type=radio]').hide();

            cell.append('<img src="' + App.Wireframe.Utils.indicatorUrl() + '">');

            $.ajax({
              'url' : App.extendUrl(item['urls']['set_as_default'], { 'async' : 1 }), 
              'type' : 'post', 
              'data' : { 'submitted' : 'submitted' }, 
              'success' : function(response) {
                App.Wireframe.Flash.success('Default job type has been changed successfully');
                App.Wireframe.Content.setFromUrl('{$job_types_page_url}');
              }, 
              'error' : function(response) {
                cell.find('img').remove();
                $('#job_types td.is_default input[type=radio]').show();

                App.Wireframe.Flash.error('Failed to set selected job type as default');
              } 
            });
          } // if
        } // if

        return false;
      }).appendTo(job_type.find('td.is_default'));

      if(item['is_default']) {
        job_type.addClass('is_default');
        radio[0].checked = true;
      } // if
      
      job_type.find('td.name').html('<a href="' + item['urls']['view'] + '">' + item['name'].clean() + '</a>');
      job_type.find('td.default_hourly_rate').html(App.moneyFormat(item['default_hourly_rate']));

      var options_cell = job_type.find('td.options');

      // Edit
      options_cell.append('<a href="' + item['urls']['edit'] + '" class="edit_job_type" title="' + App.lang('Change Settings') + '"><img src="{image_url name="icons/12x12/edit.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" alt="' + App.lang('Edit') + '" /></a>').find('a.edit_job_type').flyoutForm({
        'success_event' : 'job_type_updated'
      });

      // Delete
      if(item['permissions']['can_delete']) {
        options_cell.append('<a href="' + item['urls']['delete'] + '" class="delete_job_type" title="' + App.lang('Remove Job Type') + '"><img src="{image_url name="icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" alt="' + App.lang('Delete') + '" /></a>').find('a.delete_job_type').asyncLink({
          'confirmation' : App.lang('Are you sure that you want to permanently delete this job type?'),
          'success_event' : 'job_type_deleted', 
          'success_message' : App.lang('Job type has been deleted successfully')
        });
      } // if
    }
  });
</script>