<?php

  // Build on top of project controller
  AngieApplication::useController('project', SYSTEM_MODULE);

  /**
   * Project hourly rates management controller
   * 
   * @package activeCollab.modules.tracking
   * @subpackage controllers
   */
  class ProjectHourlyRatesController extends ProjectController {
    
    /**
     * Execute before any controller action
     */
    function __before() {
      parent::__before();
      
      if(!JobTypes::canManageProjectHourlyRates($this->logged_user, $this->active_project)) {
        $this->response->forbidden();
      } // if
    } // __before
  
    /**
     * Show project hourly rates
     */
    function index() {
      $this->response->assign('job_types', JobTypes::find());
    } // view
    
    /**
     * Show and process project hourly rate form
     */
    function edit() {
      $job_type_id = $this->request->getId('job_type_id');
      
      $job_type = $job_type_id ? JobTypes::findById($job_type_id) : null;
      
      if($job_type instanceof JobType) {
        $project_hourly_rate_data = $this->request->post('project_hourly_rate', array(
          'use_custom' => $job_type->hasCustomHourlyRateFor($this->active_project), 
          'hourly_rate' => $job_type->getHourlyRateFor($this->active_project), 
        ));
        
        $this->response->assign(array(
          'active_job_type' => $job_type, 
          'project_hourly_rate_data' => $project_hourly_rate_data, 
        ));
        
        if($this->request->isSubmitted()) {
          try {
            if($project_hourly_rate_data['use_custom']) {
              $job_type->setCustomHourlyRateFor($this->active_project, (float) $project_hourly_rate_data['hourly_rate']);
            } else {
              $job_type->dropCustomHourlyRateFor($this->active_project);
            } // if
            
            $this->response->respondWithData(array(
              'job_type_id' => $job_type->getId(), 
              'default_hourly_rate' => $job_type->getDefaultHourlyRate(), 
            	'custom_hourly_rate' => $job_type->getCustomHourlyRateFor($this->active_project), 
            ), array('as' => 'project_hourly_rate'));
          } catch(Exception $e) {
            $this->response->exception($e);
          } // try
        } // if
      } else {
        $this->response->notFound();
      } // if
    } // edit
    
  }