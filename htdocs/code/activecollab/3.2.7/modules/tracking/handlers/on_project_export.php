<?php

  /**
   * Tracking module on_project_export event handler
   *
   * @package activeCollab.modules.tracking
   * @subpackage handlers
   */

  /**
   * Handle project exporting
   *
   * @param array $exportable_modules
   * @param Project $project
   * @param array $project_tabs
   * @return null
   */
  function tracking_handle_on_project_export(&$exporters, $project, $project_tabs) {
  	if (in_array('time', $project_tabs, true)) {
    	$exporters['tracking'] = array(
    		'name' => lang('Time & Expenses'),
    		'exporter' => 'TrackingExporter',
    	);
  	} //if
  } //tracking_handle_on_project_export