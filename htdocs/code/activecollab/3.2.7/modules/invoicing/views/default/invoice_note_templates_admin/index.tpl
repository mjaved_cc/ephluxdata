{title}Invoice Note Templates{/title}
{add_bread_crumb}List{/add_bread_crumb}

<div id="invoice_notes"></div>

<script type="text/javascript">
  $('#invoice_notes').pagedObjectsList({
    'load_more_url' : '{assemble route=admin_invoicing_notes}', 
    'items' : {$invoice_note_templates|json nofilter},
    'items_per_load' : {$items_per_page}, 
    'total_items' : {$total_items}, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'note' }, 
    'columns' : {
      'name' : App.lang('Name'), 
      'options' : '' 
    }, 
    'sort_by' : 'name', 
    'empty_message' : App.lang('There are no invoice notes defined'), 
    'listen' : 'invoice_note_template', 
    'on_add_item' : function(item) {
      var note = $(this);
      
      note.append(
       	'<td class="name"></td>' +  
        '<td class="options"></td>'
      );

      note.attr('id',item['id']);
	  
      $('<a></a>').attr('href', item['urls']['view']).text(item['name']).appendTo(note.find('td.name'));
      note.find('td.name').text(item['name'].clean());
      
      note.find('td.options')
        .append('<a href="' + item['urls']['edit'] + '" class="edit_note" title="' + App.lang('Change Settings') + '"><img src="{image_url name="icons/12x12/edit.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_note" title="' + App.lang('Remove Note') + '"><img src="{image_url name="icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>')
      ;

      note.find('td.options a.note_details').flyout();
      note.find('td.options a.edit_note').flyoutForm({
        'success_event' : 'invoice_note_template_updated',
        'width' : 580
      });
      
      note.find('td.options a.delete_note').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this note?'), 
        'success_event' : 'invoice_note_template_deleted', 
        'success_message' : App.lang('Note has been deleted successfully')
      });
    }
  });
</script>