<?php

  // Extend modules admin
  AngieApplication::useController('modules_admin', SYSTEM_MODULE);

  /**
   * Invoicing module administration controller
   *
   * @package activeCollab.modules.invoicing
   * @subpackage controllers
   */
  class InvoicingModuleAdminController extends ModulesAdminController {
    
    /**
     * Show module details page
     */
    function module() {
      $this->smarty->assign('roles', Roles::find());
    } // module
    
  }