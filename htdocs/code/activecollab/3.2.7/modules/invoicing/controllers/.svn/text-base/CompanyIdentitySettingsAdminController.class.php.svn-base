<?php

  // We need admin controller
  AngieApplication::useController('admin');

  /**
   * Company identity controller implementation
   *
   * @package activeCollab.modules.invoicing
   * @subpackage controllers
   */
  class CompanyIdentitySettingsAdminController extends AdminController {
    
    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();

      $this->wireframe->breadcrumbs->add('company_identity', lang('Invoicing Company Identity Settings'), Router::assemble('admin_invoicing_company_identity'));
    } // __construct
    
    /**
     * Save company details info
     * 
     * @param void
     * @return void
     */
    function index() {
      $brand_path = PUBLIC_PATH . '/brand';
      $default_image_name = 'invoicing_logo.jpg';
      $default_full_image_name = $brand_path . '/' . $default_image_name;
      
      $company_data = $this->request->post('company');
      if (!is_foreachable($company_data)) {
        $company_data = array(
          'name' => ConfigOptions::getValue('invoicing_company_name'),
          'details' => ConfigOptions::getValue('invoicing_company_details'),
        );
      } // if
      
      if ($this->request->isSubmitted()) {
        $errors = new ValidationErrors();
        
        DB::beginWork();
        
        $company_name = trim(array_var($company_data, 'name'));
        $company_details = trim(array_var($company_data, 'details'));
        
        if (!$company_name || !$company_details) {
          if (!$company_name) {
            $errors->addError(lang('Company name is required'), 'company_name');
          } // if
          if (!$company_details) {
            $errors->addError(lang('Company details are required'), 'company_details');
          } // if
        } else {         
          // copy and convert logo
          $logo_file = array_var($_FILES, 'company_logo', null);
          if ($logo_file['name']) {
            $pathinfo = pathinfo($logo_file['name']);
            
            do {
              $new_filename = make_string(30) . '.' . array_var($pathinfo, 'extension');
              $new_file_full_path = $brand_path . '/' . $new_filename;
            } while (is_file($new_file_full_path));
            
            if (move_uploaded_file($logo_file['tmp_name'], $new_file_full_path)) {
              scale_image($new_file_full_path, $new_file_full_path, 600, 150, IMAGETYPE_JPEG, 100);
            } else {
              $errors->addError(lang('Could not upload company logo'), 'company_logo');
            } // if          
          } // if

          DB::commit();
        } // if
        
        if(!$errors->hasErrors()) {
          // set config options
          
          ConfigOptions::setValue(array(
            'invoicing_company_name' => $company_name, 
            'invoicing_company_details' => $company_details
          ));
          @unlink($default_full_image_name);
          rename($new_file_full_path, $default_full_image_name);
          
          DB::commit();
          
          $this->flash->success('Company identity successfully modified');
          $this->response->redirectTo('admin_invoicing_company_identity');
        } else {
          @unlink($new_file_full_path);
          DB::rollback();
          $this->smarty->assign('errors', $errors);
        } // if
      } // if
      
      $company_logo_url = get_company_invoicing_logo_url();
      
      $this->smarty->assign(array(
        'company_data' => $company_data,
        'company_logo_url' => $company_logo_url,
      ));      
    } // index
    
  }