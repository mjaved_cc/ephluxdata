<?php

  /**
   * InvoiceNoteTemplate class
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class InvoiceNoteTemplate extends BaseInvoiceNoteTemplate implements IRoutingContext {
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $result['content'] = $this->getContent(); 
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      throw new NotImplementedError(__METHOD__);
    } // describeForApi
    
    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------
    
    /**
     * Return routing context name
     *
     * @return string
     */
    function getRoutingContext() {
      return 'admin_invoicing_note';
    } // getRoutingContext
    
    /**
     * Return routing context parameters
     *
     * @return mixed
     */
    function getRoutingContextParams() {
      return array('note_id' => $this->getId());
    } // getRoutingContextParams
    
    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------
    
    /**
     * Validate model
     *
     * @param ValidationErrors $errors
     */
    function validate(&$errors) {
      if (!$this->validatePresenceOf('name')) {
        $errors->addError(lang('Note name is required'), 'name');
      } // if
      
      if (!$this->validatePresenceOf('content')) {
        $errors->addError(lang('Note content is required'), 'unit_cost');
      } // if
      
      return parent::validate($errors);
    } // validate
  
  }