<?php
  /**
   * Test quote create and send notification
   * 
   */
  class TestQuoteCreateAndSend extends AngieModelTestCase {
    
    /**
     * Logged user
     * 
     * @var IUser
     */
    private $logged_user;
    
    /**
     * Quote
     * 
     * @var Quote
     */
    private $active_quote;
    
    
    function setUp() {
        parent::setUp();
      
        $this->logged_user = new User(1);
        ConfigOptions::setValueFor('language', $this->logged_user,1);
        
        $this->active_quote = new Quote();

        $this->active_quote->setName('Test Quote');
        $this->active_quote->setCurrencyId(1);
        $this->active_quote->setCreatedBy($this->logged_user);
        
        $language = new Language(1);
        
        $this->active_quote->setLanguageId($language->getId());
        $this->active_quote->setNote('Test Note');
        $this->active_quote->setPrivateNote('Private Note test');
        
        $this->active_quote->setCompanyId(1);
        $this->active_quote->setCompanyName('Owner');
        $this->active_quote->setCompanyAddress('Test address');
        $recipient = new User(1);
        
        $this->active_quote->setRecipient($recipient);
          
        $this->active_quote->save();
        
        $this->assertIsA($this->active_quote->getRecipient(), 'IUser', 'Recipient valid');
        
        $quote_item1 = new QuoteItem();
        $quote_item1->setAttributes(array(
          'quote_id' => $this->active_quote->getId(),
          'position' => 1,
          'tax_rate_id' => 0,
          'description' => 'Test qoute item 1',
          'quantity' => 2,
          'unit_cost' => 10
        ));
        $quote_item1->save();
        
        $quote_item2 = new QuoteItem();
        $quote_item2->setAttributes(array(
          'quote_id' => $this->active_quote->getId(),
          'position' => 2,
          'tax_rate_id' => 0,
          'description' => 'Test qoute item 2',
          'quantity' => 3,
          'unit_cost' => 13
        ));
        $quote_item2->save();
  
        ApplicationMailer::setAdapter(new SilentMailerAdapter());
        ApplicationMailer::setDefaultSender(new AnonymousUser('Default From', 'default@from.com'));
        ApplicationMailer::setDecorator(new ApplicationMailerDecorator());
        ApplicationMailer::connect();
    	
    }//setUp
    
    
    function tearDown() {
      parent::tearDown();
      
      $this->logged_user = null;
      $this->active_quote = null;
      ApplicationMailer::disconnect();
    } // tearDown
    
    function testInitialization() {
      $this->assertTrue($this->logged_user->isLoaded(), 'Test user loaded');
    } // testInitialization
    
    
    function testSendQuoteNotificationExistingCompany() {
      
      ApplicationMailer::getDefaultSender()->notifier()->notifyUsers($this->active_quote->getRecipient(), $this->active_quote, 'invoicing/new_quote', null, array(
  		  'method' => ApplicationMailer::SEND_INSTANTNLY, 
  		));

      $this->assertEqual((integer) DB::executeFirstCell('SELECT COUNT(id) FROM ' . TABLE_PREFIX . 'mailing_activity_logs'), 1);
      $log_entry = MailingActivityLogs::find(array(
        'one' => true,
      ));
      
      $this->assertIsA($log_entry, 'MessageSentActivityLog');
      
  		
    }//testSendQuoteNotificationExistingCompany
    
    
    function testSendQuoteNotificationNewCompany() {
      
      $recipient = new AnonymousUser('Test recipient name', 'test_recipient@test.com');
      
      $this->active_quote->setCompanyId(0);
      $this->active_quote->setCompanyName('Test Company Name');
      $this->active_quote->setCompanyAddress('Test Company address, 12');
      
      $this->active_quote->setRecipient($recipient);
      $this->active_quote->save();
      
      $this->assertIsA($this->active_quote->getRecipient(), 'IUser', 'Recipient valid');
      
      
      ApplicationMailer::getDefaultSender()->notifier()->notifyUsers($this->active_quote->getRecipient(), $this->active_quote, 'invoicing/new_quote', null, array(
  		  'method' => ApplicationMailer::SEND_INSTANTNLY, 
  		));
  		
  	  $this->assertEqual((integer) DB::executeFirstCell('SELECT COUNT(id) FROM ' . TABLE_PREFIX . 'mailing_activity_logs'), 1);
      $log_entry = MailingActivityLogs::find(array(
        'one' => true,
      ));
      
      $this->assertIsA($log_entry, 'MessageSentActivityLog');
      
    }//testSendQuoteNotificationNewCompany
  }