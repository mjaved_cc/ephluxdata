<?php

  /**
   * notification_invoice_pay helper implementation
   *
   * @package activeCollab.modules.invoicing
   * @subpackage helpers
   */

  /**
   * Return payment link for given invoice, if needed
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_notification_invoice_pay($params, &$smarty) {
    $context = array_required_var($params, 'context', true, 'ApplicationObject');
    $context_view_url = array_required_var($params, 'context_view_url');
    $recipient = array_required_var($params, 'recipient', true, 'IUser');

    if($context->isIssued() && $context->payments()->canMake($recipient)) {
      $label = $recipient->isFinancialManager() ? lang('Go to Invoice', null, true, $recipient->getLanguage()) : lang('Pay Online Now', null, true, $recipient->getLanguage());

      return '<table style="margin-top: 16px;" bgcolor="#ffffff" width="100%"><tr><td style="font-size: 120%; text-align: center;"><a href="' . clean($context_view_url) . '" style="' . Theme::getProperty('notifications.link') . '">' . $label . '</a></td></tr></table>';
    } // if
  } // smarty_function_notification_invoice_pay