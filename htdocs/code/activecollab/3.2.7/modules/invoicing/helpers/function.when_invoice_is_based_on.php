<?php

  /**
   * when_invoice_is_based_on helper implementation
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage helpers
   */

  /**
   * Render what to do when invoice is based on another object picker
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_when_invoice_is_based_on($params, &$smarty) {
    $possibilities = array(
      Invoice::INVOICE_SETTINGS_SUM_ALL => lang('Sum all time records as a single invoice item'), 
      Invoice::INVOICE_SETTINGS_SUM_ALL_BY_JOB_TYPE => lang('Sum time records grouped by job type'), 
      Invoice::INVOICE_SETTINGS_KEEP_AS_SEPARATE => lang('Keep time records as separate invoice items'), 
    );
    
    $name = array_required_var($params, 'name', true);
    
    $default_value = ConfigOptions::getValue('on_invoice_based_on');
    if(!$default_value) {
      $default_value = Invoice::INVOICE_SETTINGS_SUM_ALL;
    }//if
    
    $value = array_var($params, 'value', $default_value, true);
    
    return HTML::radioGroupFromPossibilities($name, $possibilities, $value, $params);
  } // smarty_function_when_invoice_is_based_on