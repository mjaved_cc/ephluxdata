<?php 

  /**
   * Task schedule controller
   * 
   * @package activeCollab.modules.tasks
   * @subpackage controllers
   */
  class TaskScheduleController extends ScheduleController {
    
    /**
     * Reschedule task
     */
    function reschedule() {
      if($this->request->isAsyncCall() || ($this->request->isApiCall() && $this->request->isSubmitted())) {
        $old_due_on = $this->active_object->getDueOn();
        
        $reschedule_data = $this->request->post('reschedule', array(
          'due_on' => $old_due_on,
          'tbd' => empty($old_due_on),
          'reschedule_subtasks' => true,  
        ));
        
        $this->response->assign(array(
          'reschedule_data' => $reschedule_data,
          'reschedule_url' => $this->active_object->schedule()->getRescheduleUrl()
        ));
        
        if ($this->request->isSubmitted()) {
          try {
            DB::beginWork('Rescheduling task @ ' . __CLASS__);
            
            $tbd = isset($reschedule_data['tbd']) && $reschedule_data['tbd'];
            $reschedule_subtasks = isset($reschedule_data['reschedule_subtasks']) && $reschedule_data['reschedule_subtasks'];
            
            if(empty($tbd) && isset($reschedule_data['due_on']) && $reschedule_data['due_on']) {
              $due_on = new DateValue($reschedule_data['due_on']);
            } else {
              $due_on = null;
            } // if
            
            if(empty($due_on)) {
              $this->active_object->setDueOn(null);
              $this->active_object->save();
            } else {
              ProjectScheduler::rescheduleProjectObject($this->active_object, $due_on, $reschedule_subtasks, ConfigOptions::getValue('skip_days_off_when_rescheduling'));
            } // if
            
            DB::commit('Task rescheduled @ ' . __CLASS__);
            
            $this->response->respondWithData($this->active_object, array(
              'detailed' => true, 
            ));
          } catch (Exception $e) {
            DB::rollback('Task rescheduling failed @ ' . __CLASS__);
            $this->response->exception($e);
          } // try
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // reschedule
    
  }