<?php

  class TestSessionCache extends UnitTestCase {
    
    /**
     * Chace instance
     *
     * @var Cache
     */
    var $cache;
  
    function setUp() {
      $this->recreateCacheInstance();
    } // setUp
    
    function tearDown() {
      $this->cache->backend->clear();
    } // tearDown
    
    function testInternals() {
      $this->cache->backend->set('var', 'value');
      
      $this->assertEqual($_SESSION[$this->cache->backend->var_name]['var'][0], 'value');
      $this->assertEqual($_SESSION[$this->cache->backend->var_name]['var'][1], $this->cache->backend->reference_time + 3600);
      
      $this->assertEqual($this->cache->backend->get('var'), 'value');
      
      // Remove...
      $this->cache->backend->remove('var');
      $this->assertFalse(isset($_SESSION[$this->cache->backend->var_name]['var']));
      $this->assertEqual($this->cache->backend->get('var'), null);
    } // testSetGet
    
    function testClear() {
      $this->cache->backend->set('var1', 1);
      $this->cache->backend->set('var2', 2);
      
      $this->assertTrue(is_array($_SESSION[$this->cache->backend->var_name]));
      $this->assertEqual(count($_SESSION[$this->cache->backend->var_name]), 2);
      
      $this->cache->backend->clear();
      
      $this->assertTrue(is_array($_SESSION[$this->cache->backend->var_name]));
      $this->assertEqual(count($_SESSION[$this->cache->backend->var_name]), 0);
    } // testClear
    
    function recreateCacheInstance() {
      $this->cache = new Cache();
      $this->cache->useBackend('SessionCacheBackend');
    } // recreateCacheInstance
  
  } // TestSessionCache

?>