<?php

  /**
   * Angie application installer
   * 
   * @package angie.library.application
   */
  final class AngieApplicationInstaller {
  
    /**
     * Installer adapter
     *
     * @var AngieApplicationInstallerAdapter
     */
    private static $adapter;
    
    /**
     * Initialize installer
     */
    static function init() {
      $adapter_class = APPLICATION_NAME . 'InstallerAdapter';
      
      $adapter_class_path = APPLICATION_PATH . "/resources/$adapter_class.class.php";
      if(is_file($adapter_class_path)) {
        require_once $adapter_class_path;
        
        if(class_exists($adapter_class)) {
          $adapter = new $adapter_class();
          
          if($adapter instanceof AngieApplicationInstallerAdapter) {
            self::$adapter = $adapter;
          } else {
            throw new InvalidInstanceError('adapter', $adapter, $adapter_class);
          } // if
        } else {
          throw new ClassNotImplementedError($adapter_class, $adapter_class_path);
        } // if
      } else {
        throw new FileDnxError($adapter_class_path);
      } // if
    } // init
    
    /**
     * Render installer dialog
     */
    static function render() {
      print '<!DOCTYPE html>';
      print '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"><title>' . AngieApplication::getName() . ' Installer</title>';
      print '<script type="text/javascript">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/foundation/javascript/jquery.official/jquery.js') . '</script>';
      print '<script type="text/javascript">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/foundation/javascript/jquery.plugins/jquery.form.js') . '</script>';
      print '<script type="text/javascript">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/installer/javascript/jquery.installer.js') . '</script>';
      print '<style type="text/css">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/foundation/stylesheets/reset.css') . '</style>';
      print '<style type="text/css">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/foundation/stylesheets/classes.css') . '</style>';
      print '<style type="text/css">' . file_get_contents(ANGIE_PATH . '/frameworks/environment/assets/installer/stylesheets/installer.css') . '</style>';
      print '</head>';
      
      print '<body><div id="application_installer">';
      
      $counter = 1;
      foreach(AngieApplicationInstaller::getSections() as $section_name => $section_title) {
        print '<div class="installer_section" installer_section="' . $section_name . '">';
        print '<h1 class="head">' . $counter . '. <span>' . clean($section_title) . '</span></h1>';
        print '<div class="body">' . AngieApplicationInstaller::getSectionContent($section_name) . '</div>';
        print '</div>';
        
        $counter++;
      } // foreach
      
      print '</div><p class="center">&copy;' . date('Y') . ' ' . AngieApplication::getVendor() . '. All rights reserved.</p><script type="text/javascript">$("#application_installer").installer({"name" : "' . AngieApplication::getName() . '"});</script>';
      print '</body></html>';
    } // render
    
    // ---------------------------------------------------
    //  Sections
    // ---------------------------------------------------
    
    /**
     * Return all installer sections
     * 
     * @return array
     */
    static function getSections() {
      return self::$adapter->getSections();
    } // getSections
    
    /**
     * Return initial content for a given section
     * 
     * @param string $name
     * @return string
     */
    static function getSectionContent($name) {
      return self::$adapter->getSectionContent($name);
    } // getSectionContent
    
    /**
     * Secuted section submission
     * 
     * @param string $name
     * @param mixed $data
     * @return boolean
     */
    static function executeSection($name, $data = null) {
      $response = '';
      
      if(self::$adapter->executeSection($name, $data, $response)) {
        header("HTTP/1.0 200 OK");
      } else {
        header("HTTP/1.0 409 Conflict");
      } // if
      
      print $response;
    } // executeSection
    
  }