{$context->comments()->getNotificationSubjectPrefix()}{lang object_name=$context->getName() object_type=$context->getVerboseType(true, $language) language=$language}New comment on ':object_name' :object_type{/lang}
================================================================================
<table cellpadding="0" cellspacing="0" border="0" style="width: 656px; margin-left: auto; margin-right: auto; text-align: left;">
  <tr>
    <td style="padding-top: 40px;">
      <div style="text-align: left; background-color: #eff1e7; color: #4b4b4b; width: 600px; margin-left: auto; margin-right: auto; padding: 27px; padding-top: 15px; padding-bottom: 15px; border-color: #d0d2c9; border-width: 1px; border-top-left-radius: 20px; border-top-right-radius: 20px; border-style: solid;">
        <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#eff1e7">
          <tr>
            <td style="vertical-align: middle;">{notification_identity context=$context recipient=$recipient sender=$sender bgcolor="#eff1e7" show_site_name=false}</td>
            <td style="vertical-align: middle;">{notification_inspector context=$context context_view_url=$context_view_url recipient=$recipient sender=$sender}</td>
          </tr>
        </table>
      </div>
      <div style="background-color: #fff; width: 654px; margin-left: auto; margin-right: auto; border-color: #d0d2c9; border-width: 1px; border-top: 0; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; border-style: solid;">
      {foreach $context->comments()->getLatest($recipient, 5) as $comm}
        <div style="padding: 15px; {if !$comm@last}border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #d0d2c9;{/if}" bgcolor="#ffffff">
          <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
            <tr>
              <td style="width: 50px; vertical-align: top;"><a href="{$comm->getCreatedBy()->getViewUrl()}"><img src="{$comm->getCreatedBy()->avatar()->getUrl(IProjectAvatarImplementation::SIZE_BIG)}"></a></td>
              <td style="vertical-align: top;">
                <a href="{$comm->getCreatedBy()->getViewUrl()}" style="{$style.link} font-weight: bold">{$comm->getCreatedBy()->getDisplayName(true)}</a> &nbsp; {$comm->getCreatedOn()->formatDateForUser($recipient)}{if $comment->getId() == $comm->getId()} &nbsp; <span style="font-weight: bold">{lang language=$language}New!{/lang}</span>{/if}<br>
                {$comm->getBody()|rich_text:'notification' nofilter}
                {notification_attachments_table object=$comm recipient=$recipient}
              </td>
            </tr>
          </table>
        </div>
      {/foreach}

      {assign var=total_comments value=$context->comments()->count($recipient)}

        <div style="color: #999; text-align: center; padding: 15px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">
        {if $total_comments == 1}
          {lang language=$language}This is the First Comment in the Thread{/lang}
          {elseif $total_comments > 5}
          {if $context_view_url}
            {lang total=$total_comments url=$context_view_url link_style=$style.link language=$language}Showing <a href=":url" style=":link_style">5 out of :total Comments</a>{/lang}
            {else}
            {lang total=$total_comments language=$language}Showing 5 out of :total Comments{/lang}
          {/if}
          {else}
          {lang total=$total_comments language=$language}Showing All :total Comments{/lang}
        {/if}
        </div>
      </div>
    </td>
  </tr>
</table>