<?php

  /**
   * Properly display money value
   * 
   * @package angie.frameworks.globalization
   * @subpackage helpers
   */

  /**
   * Returns formatted money value based on float input
   *
   * @param float $content
   * @param Currency $currency
   * @return string
   */
  function smarty_modifier_money($content, $currency = null) {
    if($currency instanceof Currency) {
      return $currency->format($content);
    } else {
      return number_format($content, 2, '.', ',');
    } // if
  } // smarty_modifier_money