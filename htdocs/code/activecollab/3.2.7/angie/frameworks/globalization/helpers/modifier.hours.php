<?php

  /**
   * hours modifier implementation
   *
   * @package angie.frameworks.environment
   * @subpackage helpers
   */
  
  /**
   * Return formatted hours based on float value
   *
   * @param float $content
   * @param boolean $decimal
   * @return string
   */
  function smarty_modifier_hours($content, $decimal = true) {
    if($decimal) {
      return (string) (float) number_format($content, 2, '.', ',');
    } else {
      return float_to_time($content);
    } // if
  } // smarty_modifier_hours