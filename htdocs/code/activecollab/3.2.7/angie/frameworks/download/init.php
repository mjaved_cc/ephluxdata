<?php

  /**
   * Download framework initialization file
   *
   * @package angie.frameworks.download
   */
  
  define('DOWNLOAD_FRAMEWORK', 'download');
  define('DOWNLOAD_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/download');
  
  // Inject download framework into specified module
  @define('DOWNLOAD_FRAMEWORK_INJECT_INTO', 'system');
  
  define('DOWNLOAD_PREVIEW_IMAGE', 'image');
  define('DOWNLOAD_PREVIEW_VIDEO', 'video');
  define('DOWNLOAD_PREVIEW_FLASH', 'flash');
  define('DOWNLOAD_PREVIEW_AUDIO', 'audio');
  
  AngieApplication::setForAutoload(array(
    'IDownload' => DOWNLOAD_FRAMEWORK_PATH . '/models/IDownload.class.php', 
    'IDownloadImplementation' => DOWNLOAD_FRAMEWORK_PATH . '/models/IDownloadImplementation.class.php', 
    'IDownloadPreviewImplementation' => DOWNLOAD_FRAMEWORK_PATH . '/models/IDownloadPreviewImplementation.class.php', 
  ));