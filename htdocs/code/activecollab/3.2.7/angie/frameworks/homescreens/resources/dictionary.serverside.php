<?php return array(
  'Home Screen',
  'Revert to Default Home Screen',
  'Are you sure that you want to revert to default home screen? This will revert all custom changes that you made to this home screen',
  'Configure Your Own Home Screen',
  'Configure Home Screen',
  'Invalid home screen widget type',
  'Configure :user\'s Home Screen',
  'Home Screens',
  'Default Home Screen',
  'Click to Configure',
  'Start with blank home screen',
  'Other',
  'Three Columns, Center Column is Wide',
  'No additional options available',
  'Home screen tab type is required',
  'Home screen is required',
  'Home screen tab name is required',
  'Three Columns, Left Column is Wide',
  'Three Columns, Right Column is Wide',
  'Two Columns of the Same Width',
  'Home screen tab with no widgets',
  'Home screen tab with one widget',
  'Home screen tab with :num widgets',
  'Empty',
  'Home screen widget type is required',
  'Home screen tab is required',
  'Home screen tab column is required',
  'Home screen type is required',
  'Welcome',
  'Based on default home screen',
  'Based on home screen used for ":role" role',
  'Save Changes',
  'Name',
  'Type',
  'Add Homescreen Tab',
  'Add Home Screen Widget',
  'Create Home Screen',
  'You are using default home screen',
  'Click on the link in the upper right corner to configure your own home screen',
  ':user is using default home screen',
  'Click on the link in the upper right corner to configure :user\'s home screen',
  'This :type does not have a home screen configured, but uses home screen configured for parent :parent_type',
  'This :type does not have a home screen configured, but uses default home screen',
  'Default',
  'Role Specific',
  'Legend',
  'Roles that use their own home screen settings',
  'Roles that don\'t have their own configuration, but use Default Home Screen settings instead',
  'Overview',
); ?>