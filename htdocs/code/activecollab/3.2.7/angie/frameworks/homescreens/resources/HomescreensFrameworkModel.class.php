<?php

  /**
   * Homescreens framework model
   * 
   * @package angie.frameworks.homescreens
   * @subpackage resources
   */
  class HomescreensFrameworkModel extends AngieFrameworkModel {
  
    /**
     * Construct homescreens framework model definition
     *
     * @param HomescreensFramework $parent
     */
    function __construct(HomescreensFramework $parent) {
      parent::__construct($parent);
      
      $this->addModel(DB::createTable('homescreens')->addColumns(array(
        DBIdColumn::create(), 
        DBTypeColumn::create('Homescreen'),
        DBParentColumn::create(),  
      )))->setTypeFromField('type');
      
      $this->addModel(DB::createTable('homescreen_tabs')->addColumns(array(
        DBIdColumn::create(), 
        DBTypeColumn::create('HomescreenTab'), 
        DBIntegerColumn::create('homescreen_id', 6, 0)->setUnsigned(true), 
        DBNameColumn::create(50), 
        DBIntegerColumn::create('position', 5, 0)->setUnsigned(true), 
        DBAdditionalPropertiesColumn::create(), 
      )))->setTypeFromField('type');
      
      $this->addModel(DB::createTable('homescreen_widgets')->addColumns(array(
        DBIdColumn::create(), 
        DBTypeColumn::create('HomescreenWidget'), 
        DBIntegerColumn::create('homescreen_tab_id', 5, 0)->setUnsigned(true), 
        DBIntegerColumn::create('column_id', 3, 1)->setUnsigned(true), 
        DBIntegerColumn::create('position', 5, 0)->setUnsigned(true), 
        DBAdditionalPropertiesColumn::create(), 
      )))->setTypeFromField('type');
      
      $users_table = AngieApplicationModel::getTable('users');
      $users_table->addColumn(DBIntegerColumn::create('homescreen_id', 5)->setUnsigned(true), 'role_id');
      
      $roles_table = AngieApplicationModel::getTable('roles');
			$roles_table->addColumn(DBIntegerColumn::create('homescreen_id', 5)->setUnsigned(true), 'type');
    } // __construct
    
  }