{title}Home Screen{/title}
{add_bread_crumb}Configure Home Screen{/add_bread_crumb}

{if $active_object instanceof IHomescreen}
  {if $active_object->homescreen()->hasOwn()}
  <div id="manage_own_homescreen">
    {configure_homescreen homescreen=$active_homescreen parent=$active_object user=$logged_user}
  </div>
  {else}
  <div id="doesnt_have_own_homescreen">
    {if $active_object instanceof User}
    	<p class="empty_page">
      {if $active_user->getId() == $logged_user->getId()}
        {lang}You are using default home screen{/lang}. 
        
        {if $active_object->homescreen()->canHaveOwn()}
      	  {lang}Click on the link in the upper right corner to configure your own home screen{/lang}
      	{/if}
      {else}
      	{lang user=$active_object->getFirstName(true)}:user is using default home screen{/lang}. 
      	
      	{if $active_object->homescreen()->canHaveOwn()}
      	  {lang user=$active_object->getFirstName(true)}Click on the link in the upper right corner to configure :user's home screen{/lang}
      	{/if}
      {/if}
      </p>
    {else}
      {if $active_homescreen->getParent() instanceof IHomescreen}
        <p class="empty_page">{lang type=$active_object->getVerboseType(true) parent_type=$active_homescreen->getParent()->getVerboseType(true)}This :type does not have a home screen configured, but uses home screen configured for parent :parent_type{/lang}.</p>
      {else}
        <p class="empty_page">{lang type=$active_object->getVerboseType(true)}This :type does not have a home screen configured, but uses default home screen{/lang}.</p>
      {/if}
    {/if}
  </div>
  {/if}
{else}
<div id="manage_default_homescreen">
	{configure_homescreen homescreen=$active_homescreen parent=$active_object user=$logged_user}
</div>
{/if}

<script type="text/javascript">
  App.Wireframe.Content.bind('homescreen_created.content', function() {
    App.Wireframe.Content.reload();
  });

	App.Wireframe.Content.bind('homescreen_deleted.content', function() {
	  App.Wireframe.Content.reload();
  });
</script>