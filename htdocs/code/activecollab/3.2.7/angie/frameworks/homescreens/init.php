<?php

  /**
   * Homescreens framework initialization file
   *
   * @package angie.frameworks.homescreens
   */
  
  define('HOMESCREENS_FRAMEWORK', 'homescreens');
  define('HOMESCREENS_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/homescreens');
  
  // Inject framework into system module by default
  @define('HOMESCREENS_FRAMEWORK_INJECT_INTO', 'system');
  
  // Route base for all globalization administration routes
  @define('HOMESCREENS_ADMIN_ROUTE_BASE', 'admin');
  
  AngieApplication::setForAutoload(array(
    'IHomescreen' => HOMESCREENS_FRAMEWORK_PATH . '/models/IHomescreen.class.php',
    'IHomescreenImplementation' => HOMESCREENS_FRAMEWORK_PATH . '/models/IHomescreenImplementation.class.php',
  	'IRoleHomescreenImplementation' => HOMESCREENS_FRAMEWORK_PATH . '/models/IRoleHomescreenImplementation.class.php',
    'IUserHomescreenImplementation' => HOMESCREENS_FRAMEWORK_PATH . '/models/IUserHomescreenImplementation.class.php',
   
    'FwHomescreen' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreens/FwHomescreen.class.php', 
    'FwHomescreens' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreens/FwHomescreens.class.php',

  	'FwHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/FwHomescreenTab.class.php', 
    'FwHomescreenTabs' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/FwHomescreenTabs.class.php',
  
  	'FwHomescreenWidget' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_widgets/FwHomescreenWidget.class.php', 
    'FwHomescreenWidgets' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_widgets/FwHomescreenWidgets.class.php',
  
  	'WidgetsHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/WidgetsHomescreenTab.class.php',
  	'SplitHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/SplitHomescreenTab.class.php',
  	'CenterHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/CenterHomescreenTab.class.php',
  	'LeftHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/LeftHomescreenTab.class.php',
  	'RightHomescreenTab' => HOMESCREENS_FRAMEWORK_PATH . '/models/homescreen_tabs/RightHomescreenTab.class.php',
  ));