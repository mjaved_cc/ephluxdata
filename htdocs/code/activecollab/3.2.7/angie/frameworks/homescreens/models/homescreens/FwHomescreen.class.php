<?php

  /**
   * Framework level homescreen implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  abstract class FwHomescreen extends BaseHomescreen implements IReadOnly, IRoutingContext {
  
    /**
     * Return homescreen tabs that belong to this homescreen
     *
     * @var array
     */
    private $tabs = false;
    
    /**
     * Return tabs that belong to this set
     * 
     * @return DBResult
     */
    function getTabs() {
      if($this->tabs === false) {
        $this->tabs = HomescreenTabs::findByHomescreen($this);
      } // if
      
      return $this->tabs;
    } // getTabs
    
    /**
     * Routing context name
     *
     * @var string
     */
    private $routing_context = false;
    
    /**
     * Return routing context name
     *
     * @return string
     */
    function getRoutingContext() {
      if($this->routing_context === false) {
        if($this->getParent() instanceof IRoutingContext) {
          $this->routing_context = $this->getParent()->getRoutingContext() . '_homescreen';
        } else {
          $this->routing_context = 'homescreens_admin_homescreen';
        } // if
      } // if
      
      return $this->routing_context;
    } // getRoutingContext
    
    /**
     * Routing context params
     *
     * @var array
     */
    private $routing_context_params = false;
    
    /**
     * Return routing context parameters
     *
     * @return mixed
     */
    function getRoutingContextParams() {
      if($this->routing_context_params === false) {
        if($this->getParent() instanceof IRoutingContext) {
          $this->routing_context_params = $this->getParent()->getRoutingContextParams();
        } else {
          $this->routing_context_params = null;
        } // if
      } // if
      
      return $this->routing_context_params;
    } // getRoutingContextParams
    
    // ---------------------------------------------------
    //  URL-s
    // ---------------------------------------------------
    
    /**
     * Return add home screen tab URL
     * 
     * @return string
     */
    function getAddTabUrl() {
      return Router::assemble($this->getRoutingContext() . '_tabs_add', $this->getRoutingContextParams());
    } // getAddTabUrl
    
    /**
     * Return reorder tabs URL
     * 
     * @return string
     */
    function getReorderTabsUrl() {
      return Router::assemble($this->getRoutingContext() . '_tabs_reorder', $this->getRoutingContextParams());
    } // getReorderTabsUrl
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if $user can update this home screen
     * 
     * @param IUser $user
     * @return boolean
     */
    function canEdit(IUser $user) {
      if($this->getParent() instanceof IHomescreen) {
        return $this->getParent()->homescreen()->canManageSet($user);
      } else {
        return $user->isAdministrator();
      } // if
    } // canEdit
    
    /**
     * Returns true if $user can delete this home screen
     * 
     * @param IUser $user
     * @return boolean
     */
    function canDelete(IUser $user) {
      return $this->getParent() instanceof IHomescreen && $this->getParent()->homescreen()->canDeleteSet($user);
    } // canDelete
    
    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------
    
    /**
     * Validate before save
     * 
     * @param array $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->validatePresenceOf('type')) {
        $errors->addError(lang('Home screen type is required'), 'type');
      } // if
    } // validate
    
    /**
     * Delete this home screen
     */
    function delete() {
      try {
        DB::beginWork('Removing a home screen @ ' . __CLASS__);
        
        HomescreenWidgets::deleteByHomescreen($this);
        HomescreenTabs::deleteByHomescreen($this);
        
        parent::delete();
        
        DB::commit('Home screen removed @ ' . __CLASS__);
      } catch(Exception $e) {
        DB::rollback('Failed to remove home screen @ ' . __CLASS__);
        throw $e;
      } // try
    } // delete
    
  }