<?php

  /**
   * select_homescreen_tab_type helper implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage helpers
   */

  /**
   * Render select home screen types box
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_homescreen_tab_type($params, &$smarty) {
    $value = array_var($params, 'value', null, true);
    
    $types = array(
      new SplitHomescreenTab(), 
      new LeftHomescreenTab(), 
      new RightHomescreenTab(), 
      new CenterHomescreenTab(),  
    );
    
    EventsManager::trigger('on_homescreen_tab_types', array(&$types));
    
    $possibilities = array();
    
    foreach($types as $type) {
      $possibilities[get_class($type)] = $type->getDescription();
    } // foreach
    
    return HTML::radioGroupFromPossibilities($params['name'], $possibilities, $value, $params);
  } // smarty_function_select_homescreen_tab_type