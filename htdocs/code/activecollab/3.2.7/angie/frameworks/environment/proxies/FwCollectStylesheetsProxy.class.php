<?php

  // Build on top of collect files proxy
  require_once ANGIE_PATH . '/frameworks/environment/proxies/CollectFilesProxy.class.php';

  /**
   * Collect CSS files for given modules and context
   * 
   * @package angie.frameworks.environment
   * @subpackage proxies
   */
  abstract class FwCollectStylesheetsProxy extends CollectFilesProxy {
    
    /**
     * Targeted interface
     *
     * @var string
     */
    protected $interface;
    
    /**
     * Targeted device
     *
     * @var string
     */
    protected $device;
    
    /**
     * Selected theme
     * 
     * @var string
     */
    protected $theme;
    
    /**
     * Targeted context
     *
     * @var string
     */
    protected $context;
    
    /**
     * Load only context files
     * 
     * @var boolean
     */
    protected $only_context;
    
    /**
     * Array of modules that need to be checked for JavaScript files
     *
     * @var array
     */
    protected $modules;
    
    /**
     * Construct proxy request handler
     * 
     * @param array $params
     */
    function __construct($params = null) {
      $this->interface = isset($params['interface']) && $params['interface'] ? $params['interface'] : 'default';
      $this->device = isset($params['device']) && $params['device'] ? $params['device'] : 'unknown';
      $this->theme = isset($params['theme']) && $params['theme'] ? $params['theme'] : 'default';
      $this->context = isset($params['context']) && $params['context'] ? $params['context'] : null;
      $this->only_context = isset($params['only_context']) && $params['only_context'] ? $params['only_context'] : false;
      
      $modules = isset($_GET['modules']) && $_GET['modules'] ? explode(',', trim($_GET['modules'], ',')) : null;
      if($modules) {
        $this->modules = array();
        
        foreach($modules as $module) {
          if($module && preg_match('/\W/', $module) == 0) {
            $this->modules[] = $module;
          } // if
        } // if
      } // if
      
      require_once ANGIE_PATH . '/vendor/less/init.php';
      
      $this->setPreProcessor(function($content, $file_name = null) {
        if($file_name && substr($file_name, strlen($file_name) - 9) == '.less.css') {
          return LessForAngie::compile($content);
        } else {
          return $content;
        } // if
      });
    } // __construct
  
    /**
     * Return content type of the data that handler will forward to the browser
     * 
     * @return string
     */
    protected function getContentType() {
      return 'text/css; charset: UTF-8';
    } // getContentType
    
    /**
     * Return array of files that need to be forwarded to the browser
     * 
     * @return array
     */
    protected function getFiles() {
    	$files = array();
    	
    	if (!$this->only_context) {
	      $files[] = ANGIE_PATH . '/frameworks/environment/assets/foundation/stylesheets/reset.css';
	      $files[] = ANGIE_PATH . '/frameworks/environment/assets/foundation/stylesheets/classes.css';
	    
	      if($this->interface == 'default') {
	        $this->collectFilesFromDir($files, ANGIE_PATH . "/frameworks/environment/assets/foundation/stylesheets/jquery.plugins");
	      } elseif($this->interface == 'phone' || $this->interface == 'tablet') {
	        $files[] = ANGIE_PATH . '/frameworks/environment/assets/foundation/stylesheets/jquery.official/jquery.mobile.css';
        } // if
    	} // if

      if ($this->interface == 'printer') {
        if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
          $files[] = ANGIE_PATH . '/frameworks/environment/assets/printer/stylesheets/ie/print.css';
        } // if
      } // if
      
      // load framework css files
      $frameworks = explode(',', APPLICATION_FRAMEWORKS);
      foreach($frameworks as $framework) {
      	if (!$this->only_context) {
	        if($framework != 'environment') {
	          $this->collectFilesFromDir($files, ANGIE_PATH . "/frameworks/$framework/assets/foundation/stylesheets", array('main.css', 'wireframe.css'));
	        } // if
	        $this->collectFilesFromDir($files, ANGIE_PATH . "/frameworks/$framework/assets/$this->interface/stylesheets", array('main.css', 'wireframe.css'));
      	} // if
        
        if($this->context) {
          $this->collectFilesFromDir($files, ANGIE_PATH . "/frameworks/$framework/assets/$this->interface/stylesheets/$this->context", array('main.css', 'wireframe.css'));
        } // if
      } // foreach
      
      // load module css fies
      if($this->modules) {
        foreach($this->modules as $module) {
        	if (is_dir(APPLICATION_PATH . "/modules/$module")) {
        		$base_folder = APPLICATION_PATH . "/modules/$module";
        	} else if (is_dir(CUSTOM_PATH . "/modules/$module")) {
        		$base_folder = CUSTOM_PATH . "/modules/$module";
        	} else {
        		continue;
        	} // if
        	
        	if (!$this->only_context) {
	          $this->collectFilesFromDir($files, "$base_folder/assets/foundation/stylesheets", array('main.css', 'wireframe.css'));
	          $this->collectFilesFromDir($files, "$base_folder/assets/$this->interface/stylesheets", array('main.css', 'wireframe.css'));
        	} // if
          
          if($this->context) {
            $this->collectFilesFromDir($files, "$base_folder/assets/$this->interface/stylesheets/$this->context", array('main.css', 'wireframe.css'));
          } // if
        } // foreach
      } // if

      if ($this->theme && $this->theme != 'default') {
      	$theme_dir = CUSTOM_PATH . "/modules/" . $this->theme . "/assets";
      	if (is_dir($theme_dir)) {
          $this->collectFilesFromDir($files, $theme_dir . "/foundation/stylesheets", array('main.css', 'wireframe.css'));
          $this->collectFilesFromDir($files, $theme_dir . "/$this->interface/stylesheets", array('main.css', 'wireframe.css'));      		
      	} // if
      } // if
      
      return $files;
    } // getFiles
    
  }