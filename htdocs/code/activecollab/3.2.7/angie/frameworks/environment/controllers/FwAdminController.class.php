<?php

  // Build on top of backend controller
  AngieApplication::useController('backend', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

  /**
   * Abstract administration controller that application can inherit
   *
   * @package angie.frameworks.environments
   * @subpackage controllers
   */
  abstract class FwAdminController extends BackendController {
    
    /**
     * User needs to be logged in and have admin_access permissions to use this 
     * controller
     *
     * @var string
     */
    protected $minimal_access_permission_required = 'has_admin_access';

    /**
     * Execute before any action
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->tabs->clear();
      $this->wireframe->tabs->add('admin', lang('Administration'), Router::assemble('admin'), null, true);
      
      EventsManager::trigger('on_admin_tabs', array(&$this->wireframe->tabs, &$this->logged_user));
      
      $this->wireframe->breadcrumbs->add('admin', lang('Administration'), Router::assemble('admin'));
      $this->wireframe->hidePrintButton();
      
      $this->wireframe->setCurrentMenuItem('admin');
    } // __before
    
    /**
     * Display administration index page
     */
    function index() {
      $admin_panel = new AdminPanel();
      
      $admin_panel->addToGeneral('modules', lang('Modules'), Router::assemble('modules_admin'), AngieApplication::getImageUrl('admin_panel/modules.png', ENVIRONMENT_FRAMEWORK));
      $admin_panel->addToTools('scheduled_tasks', lang('Scheduled Tasks'), Router::assemble('scheduled_tasks_admin'), AngieApplication::getImageUrl('admin_panel/scheduled-tasks.png', ENVIRONMENT_FRAMEWORK));
      $admin_panel->addToTools('indices', lang('Rebuild Indexes'), Router::assemble('indices_admin'), AngieApplication::getImageUrl('admin_panel/indices.png', ENVIRONMENT_FRAMEWORK));

      $admin_panel->addToTools('control_tower', lang('Control Tower'), Router::assemble('control_tower_settings'), AngieApplication::getImageUrl('admin_panel/control-tower.png', ENVIRONMENT_FRAMEWORK), array(
        'onclick' => new FlyoutFormCallback(array(
          'success_event' => 'control_tower_settings_updated',
          'success_message' => lang('Control Tower settings have been updated'),
          'width' => 650,
        )),
      ));

      $admin_panel->addToTools('maintenance_mode', lang('Maintenance Mode'), Router::assemble('maintenance_mode_settings'), AngieApplication::getImageUrl('admin_panel/maintenance-mode.png', ENVIRONMENT_FRAMEWORK), array(
        'onclick' => new FlyoutFormCallback(array(
          'success_event' => 'maintenance_settings_updated',
          'success_message' => lang('Maintenance mode settings have been updated'),
          'width' => 500,
        )),
      ));
      
//      $admin_panel->addToTools('theme', lang('Theme'), Router::assemble('theme_admin'), AngieApplication::getImageUrl('admin_panel/theme.png', ENVIRONMENT_FRAMEWORK), array(
//        'onclick' => new FlyoutFormCallback(array(
//          'success_event' => 'theme_settings_updated',
//          'success_message' => lang('Theme settings have been updated'),
//        ))
//      ));
      
      EventsManager::trigger('on_admin_panel', array(&$admin_panel));
      
      $this->smarty->assign('admin_panel', $admin_panel);
    } // index
    
  }