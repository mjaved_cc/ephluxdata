<?php

  /**
   * actionOnby property defintiion class
   *
   * @package angie.frameworks.environment
   * @subpackage models
   */
  class ActionOnByInspectorProperty extends InspectorProperty {
  	
  	/**
  	 * Prefix for fields
  	 * 
  	 * @var string
  	 */
  	protected $field_prefix;
  	
  	/**
  	 * Constructor
  	 * 
  	 * @param FwApplicationObject $object
  	 * @param string $url
  	 * @param string $label
  	 */
  	function __construct($object, $field_prefix = null) {
  		$this->field_prefix = $field_prefix ? $field_prefix : 'created';
  	} // __construct
  	
  	/**
  	 * Function which will render the property
  	 * 
  	 * @return string
  	 */
  	function render() {
  		return '(function (field, object, client_interface) { App.Inspector.Properties.ActionOnBy.apply(field, [object, client_interface, \'' . $this->field_prefix . '\']) })';
  	} // render    
  } // ActionOnByInspectorProperty