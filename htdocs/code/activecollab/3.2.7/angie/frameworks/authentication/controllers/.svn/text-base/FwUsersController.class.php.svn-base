<?php

  // Build on top backend controller
  AngieApplication::useController('backend', AUTHENTICATION_FRAMEWORK_INJECT_INTO);

  /**
   * Framework level users controller implementation
   *
   * @package angie.frameworks.authentication
   * @subpackage controllers
   */
  abstract class FwUsersController extends BackendController {
    
    /**
     * Selected user account
     *
     * @var User
     */
    protected $active_user;

    /**
     * Activity logs delegate
     *
     * @var ActivityLogsController
     */
    protected $activity_logs_delegate;

    /**
     * Construct users controller
     *
     * @param Request $parent
     * @param mixed $context
     */
    function __construct(Request $parent, $context = null) {
      parent::__construct($parent, $context);

      if($this->getControllerName() == 'users') {
        $this->activity_logs_delegate = $this->__delegate('activity_logs', ACTIVITY_LOGS_FRAMEWORK_INJECT_INTO, 'user');
      } // if
    } // __construct
    
    /**
     * Execute before any other action
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->tabs->clear();
      $this->wireframe->tabs->add('users', lang('Users'), Router::assemble('users'), null, true);
      
      EventsManager::trigger('on_users_tabs', array(&$this->wireframe->tabs, &$this->logged_user));
      
      $this->wireframe->setCurrentMenuItem('users');
      $this->wireframe->breadcrumbs->add('users', lang('Users'), Router::assemble('users'));
      
      $user_id = $this->request->getId('user_id');
      
      if($user_id) {
        $this->active_user = Users::findById($user_id);
      } // if
      
      if($this->active_user instanceof User) {
        $this->wireframe->breadcrumbs->add('user', $this->active_user->getDisplayName(true), $this->active_user->getViewUrl());
      } else {
        $this->active_user = new User();
      } // if

      if($this->activity_logs_delegate instanceof ActivityLogsController) {
        $this->activity_logs_delegate->__setProperties(array(
          'show_activities_by' => &$this->active_user
        ));
      } // if
      
      $this->response->assign('active_user', $this->active_user);
    } // __before
    
    /**
     * Show users index page
     */
    function index() {
      $this->wireframe->list_mode->enable();
      
      if(Users::canAdd($this->logged_user)) {
        $this->wireframe->actions->add('new_user', lang('New User'), Router::assemble('users_add'), array(
          'onclick' => new FlyoutFormCallback('user_created'),
          'icon' => AngieApplication::getImageUrl('layout/button-add.png', ENVIRONMENT_FRAMEWORK, AngieApplication::getPreferedInterface()),
          'primary' => true, 
        ));
      } // if
      
      $this->response->assign('users', Users::find());
    } // index
    
    /**
     * View an account of a single user
     */
    function view() {
      if($this->active_user->isLoaded()) {
        if($this->active_user->canView($this->logged_user)) {
          if($this->request->isApiCall()) {
            $this->response->respondWithData($this->active_user, array(
              'as' => 'user', 
              'detailed' => true,
            ));
          } // if
        } else {
          $this->response->forbidden();
        } // if
      } else {
        $this->response->notFound();
      } // if
    } // view
    
    /**
     * Create a new user account
     */
    function add() {
      if($this->request->isAsyncCall() || $this->request->isApiCall()) {
        if(Users::canAdd($this->logged_user)) {
          $user_data = $this->request->post('user');
          $this->response->assign('user_data', $user_data);
          
          if($this->request->isSubmitted()) {
            try {
              $password = array_var($user_data, 'password');
              $password_a = array_var($user_data, 'password_a');
              
              if(trim($password)) {
                if($password != $password_a) {
                  throw new ValidationErrors(array(
                    'passwords' => lang('Passwords do not match'), 
                  ));
                } // if
              } else {
                throw new ValidationErrors(array(
                  'password' => lang('Password is required')
                ));
              } // if
              
              $this->active_user->setAttributes($user_data);
              $this->active_user->save();
              
              $this->response->respondWithData($this->active_user, array(
                'as' => 'user', 
                'detailed' => true,
              ));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } // if
        } else {
          $this->response->forbidden();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // add
    
    /**
     * Update existing user account
     */
    function edit() {
      if($this->request->isAsyncCall() || ($this->request->isApiCall() && $this->request->isSubmitted())) {
        if($this->active_user->isLoaded()) {
          if($this->active_user->canEdit($this->logged_user)) {
            $user_data = $this->request->post('user', array(
              'email' => $this->active_user->getEmail(), 
              'first_name' => $this->active_user->getFirstName(), 
              'last_name' => $this->active_user->getLastName(), 
            ));
            $this->response->assign('user_data', $user_data);
          
            if($this->request->isSubmitted()) {
              try {
                $this->active_user->setAttributes($user_data);
                $this->active_user->save();
                
                $this->response->respondWithData($this->active_user, array(
                  'as' => 'user', 
                  'detailed' => true,
                ));
              } catch(Exception $e) {
                $this->response->exception($e);
              } // try
            } // if
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // edit
    
    /**
     * Edit user password
     */
    function edit_password() {
      if($this->request->isAsyncCall() || $this->request->isMobileDevice()) {
        if($this->active_user->isLoaded()) {
          if($this->active_user->canChangePassword($this->logged_user)) {
            $user_data = $this->request->post('user');
            $this->response->assign('user_data', $user_data);
            
          	if($this->request->isSubmitted(true, $this->response)) {
          	  try {
          	    $errors = new ValidationErrors();
      	
              	$password = array_var($user_data, 'password');
              	$repeat_password = array_var($user_data, 'repeat_password');
              	
              	if(empty($password)) {
              		$errors->addError(lang('Password value is required'), 'password');
              	} // if
              	
              	if(empty($repeat_password)) {
              		$errors->addError(lang('Repeat Password value is required'), 'repeat_password');
              	} // if
              	
              	if(!$errors->hasErrors() && ($password !== $repeat_password)) {
              	  $errors->addError(lang('Inserted values does not match'));
              	} // if
              	
              	if($errors->hasErrors()) {
              	  throw $errors;
              	} // if
              	
              	$this->active_user->setPassword($user_data['password']);
            	  $this->active_user->save();
            	  
            	  if($this->request->isPageCall()) {
			            $this->response->redirectToUrl($this->active_user->getViewUrl());
			          } else {
			            $this->response->respondWithData($this->active_user, array(
			              'as' => 'user', 
			              'detailed' => true,
	    		      	));
			          } // if
          	  } catch(Exception $e) {
          	    AngieApplication::revertCsfrProtectionCode();
          	    $this->response->exception($e);
          	  } // try
          	} // if
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // edit_password

    /**
     * Export vCard
     */
    function export_vcard() {
      if($this->active_user->isLoaded()) {
        try{
          $this->active_user->toVCard();
          die();
        } catch(Exception $e) {
          $this->response->exception($e);
        } // try
      } else {
        $this->response->notFound();
      } // if
    } // export_vcard

    /**
     * Not found...
     */
    function delete() {
      $this->response->notFound();
    } // delete
    
//    /**
//     * Delete an account
//     */
//    function delete() {
//      if(($this->request->isAsyncCall() || $this->request->isApiCall()) && $this->request->isSubmitted()) {
//        if($this->active_user->isLoaded()) {
//          if($this->active_user->canDelete($this->logged_user)) {
//            try {
//              $this->active_user->delete();
//              $this->response->respondWithData($this->active_user, array(
//	              'as' => 'user',
//	              'detailed' => true,
//  		      	));
//            } catch(Exception $e) {
//              $this->response->exception($e);
//            } // try
//          } else {
//            $this->response->forbidden();
//          } // if
//        } else {
//          $this->response->notFound();
//        } // if
//      } else {
//        $this->response->badRequest();
//      } // if
//    } // delete
    
    
  }