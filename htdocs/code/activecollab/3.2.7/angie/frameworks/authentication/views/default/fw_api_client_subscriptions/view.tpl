{title lang=false}API Subscription Details{/title}
{add_bread_crumb}API Subscription Details{/add_bread_crumb}

<div id="api_client_subscription_details" class="object_inspector">
  <div class="head">
    <div class="properties">
      <div class="property">
        <div class="label">{lang}Client{/lang}</div>
        <div class="data">{$active_api_client_subscription->getClientName()}</div>
      </div>
      
      <div class="property">
        <div class="label">{lang}Client Vendor{/lang}</div>
        <div class="data">
        {if $active_api_client_subscription->getClientVendor()}
          {$active_api_client_subscription->getClientVendor()}
        {else}
          <span class="details">{lang}Unknown{/lang}</span>
        {/if}
        </div>
      </div>
      
      <div class="property">
        <div class="label">{lang}Enabled{/lang}</div>
        <div class="data">
        {if $active_api_client_subscription->getIsEnabled()}
          {lang}Yes{/lang}
        {else}
          {lang}No{/lang}
        {/if}
        </div>
      </div>
      
      {if $active_api_client_subscription->getIsEnabled()}
      <div class="property">
        <div class="label">{lang}Access Level{/lang}</div>
        <div class="data">
        {if $active_api_client_subscription->getIsReadOnly()}
          {lang}Read Only{/lang}
        {else}
          {lang}Read and Write{/lang}
        {/if}
        </div>
      </div>
      {/if}
      
      <div class="property">
        <div class="label">{lang}Created On{/lang}</div>
        <div class="data">{$active_api_client_subscription->getCreatedOn()|datetime}</div>
      </div>
      
      <div class="property">
        <div class="label">{lang}Last Used On{/lang}</div>
        <div class="data">
        {if $active_api_client_subscription->getLastUsedOn()}
          {$active_api_client_subscription->getLastUsedOn()|datetime}
        {else}
          <span class="details">{lang}Never Used{/lang}</span>
        {/if}
        </div>
      </div>
    </div>
  </div>
  
  <div class="body">
    <p>{lang url=$active_api_client_subscription->getApiUrl()}API URL: <span class="token">:url</span>{/lang}</p>
    <p>{lang token=$active_api_client_subscription->getFormattedToken()}Token: <span class="token">:token</span>{/lang}</p>
  </div>
</div>