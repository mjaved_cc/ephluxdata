<?php

  /**
   * Modules framework intialization file
   *
   * @package angie.frameworks.modules
   */

  define('MODULES_FRAMEWORK', 'modules');
  define('MODULES_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/modules');
  
  @define('MODULES_FRAMEWORK_INJECT_INTO', 'system');
  @define('MODULES_FRAMEWORK_ADMIN_ROUTE_BASE', 'admin');