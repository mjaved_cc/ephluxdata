<?php

  /**
   * Favorites framework initialization file
   * 
   * @package angie.frameworks.favorites
   */

  define('FAVORITES_FRAMEWORK', 'favorites');
  define('FAVORITES_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/favorites');
  
  // Inject favorites framework into given module
  @define('FAVORITES_FRAMEWORK_INJECT_INTO', 'system');
  
  if (!defined('FAVORITES_FRAMEWORK_DEFINE_ROUTES')) {
		@define('FAVORITES_FRAMEWORK_DEFINE_ROUTES', false);
  } // if
  
  
  AngieApplication::setForAutoload(array(
    'FwFavorites' => FAVORITES_FRAMEWORK_PATH . '/models/FwFavorites.class.php', 
    'IUserFavoritesImplementation' => FAVORITES_FRAMEWORK_PATH . '/models/IUserFavoritesImplementation.class.php', 
    'ICanBeFavorite' => FAVORITES_FRAMEWORK_PATH . '/models/ICanBeFavorite.class.php',
  	'FavoriteInspectorIndicator' => FAVORITES_FRAMEWORK_PATH .'/models/FavoriteInspectorIndicator.class.php' 
  ));