<?php

/**
 * Project object action
 *
 * @package angie.framework.angie.email
 * @subpackage models
 * 
 */
abstract class IncomingMailProjectObjectAction extends IncomingMailAction {
    
    /**
     * Action form elements
     * 
     * @var string
     */
    protected $elements;
    
    /**
     * If edit mode and filter_id is sent, take pre-defined action parameters and set the initial value for elements
     * 
     * @var $action_parameters array
     */
    protected $action_parameters;
  
    /**
     * Render action project elements
     * 
     */
    abstract function renderProjectElements(IUser $user, Project $project, IncomingMailFilter $filter = null);
  
    
  	/**
     * Add category select to elements
     * 
     * @param $category_type
     */
    function addCategorySelect(IUser $user, $category_type, Project $project) {
     
      $category_params = array(
        'parent' => $project,
        'user' => $user,
        'value' => array_var($this->action_parameters,'category_id',null,true),
        'label' => 'In Category',
        'type' => $category_type,
        'name' => 'filter[action_parameters][category_id]',
        'id' => 'category_id',
        'can_create_new' => false
      );
       
      AngieApplication::useHelper('select_category',CATEGORIES_FRAMEWORK);
      //category
      $category_select = smarty_function_select_category($category_params);
      $this->elements .= $category_select;
      
    }//addCategorySelect
    
    /**
     * Add label drop down
     * 
     * @param $label_type
     */
    function addLabelSelect(IUser $user, $label_type) {
     
      $label_params = array(
        'user' => $user,
        'value' => array_var($this->action_parameters,'label_id',null,true),
        'label' => 'Label',
        'type' => $label_type,
        'name' => 'filter[action_parameters][label_id]',
        'can_create_new' => false
      );
      
      AngieApplication::useHelper('select_label',LABELS_FRAMEWORK);
      //label
      $label_select = smarty_function_select_label($label_params);
      $this->elements .= $label_select;
    
    }//addLabelSelect
    
    /**
     * Add assignee element
     * 
     * @param $object
     */
    function addAssigneeElement(IUser $user, IAssignees $object) {
      
      AngieApplication::useHelper('select_assignees',ASSIGNEES_FRAMEWORK);
      
      $assignees_params = array(
        'object' => $object,
        'user' => $user,
        'value' => array_var($this->action_parameters,'assignee_id',null,true),
        'label' => 'Assignee',
        'choose_responsible' => true,
        'choose_subscribers' => true,
        'inline' => true,
        'name' => 'filter[action_parameters]',
        'can_create_new' => false,
        'id' => 'assignee_id',
        'other_assignees' => array_var($this->action_parameters,'other_assignees',null,true),
        'subscribers' => array_var($this->action_parameters,'subscribers',null,true)
      );
     
      AngieApplication::useHelper('label',ENVIRONMENT_FRAMEWORK,'block');
      $assignee_category_select .= smarty_block_label(array('for' => 'assignee_id'),'Assignees');
   
      $assignee_category_select .= smarty_function_select_assignees($assignees_params);
      
      $this->elements .= $assignee_category_select;
      
    }//addAssigneeElement
    
    /**
     * Add notify people element
     * 
     * @param $object
     */
    function addNotifyElement(IUser $user, ISubscriptions $object) {
     
      $notify_params = array(
        'object' => $object,
        'user' => $user,
        'value' => array_var($this->action_parameters,'notify_users',null,true),
        'label' => 'Notify People',
        'inline' => true,
        'name' => 'filter[action_parameters][notify_users]',
      );
      
      AngieApplication::useHelper('select_subscribers',SUBSCRIPTIONS_FRAMEWORK);
      //label
      $notify_select = smarty_function_select_subscribers($notify_params);
      $this->elements .= $notify_select;
    
    }//addLabelSelect
    
    /**
     * Add select milestone drop down
     * 
     * @param $object
     */
    function addMilestoneSelect(IUser $user, Project $project) {
     
      $milestone_params = array(
        'project' => $project,
        'user' => $user,
        'value' => array_var($this->action_parameters,'milestone_id',null,true),
        'label' => 'Milestone',
        'name' => 'filter[action_parameters][milestone_id]',
      );
      
      AngieApplication::useHelper('select_milestone',SYSTEM_MODULE);
      //label
      $milestone_select = smarty_function_select_milestone($milestone_params);
      $this->elements .= $milestone_select;
    
    }//addMilestoneSelect
    
    
}//IncomingMailProjectObjectAction