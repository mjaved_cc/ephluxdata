<?php return array(
  'Email',
  'Mailing Log',
  'Outgoing Queue',
  'Incoming Mail Conflicts',
  'Test Connection',
  'Define Mailbox',
  'Enable IMAP Extension',
  'IMAP extension is installed and enabled',
  'IMAP extension is not installed',
  'Mailbox that checks :reply_to_address address is configured',
  'Mailbox that checks :reply_to_address address is not configured',
  'Mailbox is enabled',
  'Mailbox is not enabled',
  'Last mailbox connection was successful or not-checked yet via scheduled task',
  'Last mailbox connection was not successful',
  'Configure Scheduled Tasks',
  'Reply-To header of email notifications sent by activeCollab will be set to :reply_to_address',
  'Change Reply-To Address',
  'Frequently scheduled task has been triggered in last 10 minutes',
  'Frequently scheduled task has not been triggered in last 10 minutes',
  'Email value is not valid',
  'Incoming Mail',
  'New Mailbox',
  'IMAP extension is not loaded',
  'Can\'t find mailbox.',
  'Action object is not instance of IncomingMailAction @',
  'New Filter',
  'Can\'t find filter.',
  'Failed to connect. Reason: :reason',
  'activeCollab - test email',
  'Message subject is required',
  'Message body is required',
  'Invalid email address',
  'Invalid recipient',
  'Subject is required',
  'Body is required',
  'Please select recipients from the list on the right',
  'Incoming Mail Conflict Created',
  'Incoming mail module has just encountered on an email in your mailbox that can\'t be imported automatically and it must be done manually',
  'To resolve conflict with \':subject\' email click on <a href=":url" style=":link_style" target="_blank">this</a> link',
  'Conflict reason: :conflict_reason',
  'New Incoming Mail Conflict',
  'One Incoming Mail Conflict',
  ':num Incoming Mail Conflicts',
  'There is <a href=":url" style=":link_style" target="_blank">one incoming mail conflict</a> that requires your attention',
  'There are <a href=":url" style=":link_style" target="_blank">:num incoming mail conflicts</a> that requires your attention',
  'Mailbox Disabled',
  'Mailbox ":mailbox_name" has been disabled. <a href=":resolve_url" style=":link_style">Click here</a> to see disabled mailbox',
  'Mailbox not Checked',
  'Frequently scheduled task has experienced some issues',
  'Mailbox :mailbox_name has not been checked and emails aren\'t imported from this mailbox',
  'Info',
  'New :type has been Created',
  'Thank you for your message. :type <a href=":url" style=":link_style">:name</a> has been created',
  ':type Created',
  'Email Reply to Comment',
  'Mass Mailer',
  'This action requires at least one active project defined.',
  'Reply to Comment',
  'Mailing Queue',
  'Mail Conflicts',
  'Check if "Reply to Comment" Feature is Properly Configured',
  'Check Unsent Messages in Email Queue',
  'Check Incoming Mail Conflicts Count',
  ':count incoming email conflicts',
  ':count incoming mail conflict',
  'Notification',
  'Hi :name',
  'Open this :type in Your Web Browser',
  ':company Projects',
  'Created by',
  'Category',
  'Responsibility',
  'You are responsible for this :type',
  'You are assigned to this :type',
  'Instantly Notify Administrators About New Conflicts',
  'Notify Administrators About New Conflicts Once a Day',
  'Don\'t Notify Administrators About New Conflicts',
  'None',
  'After 10 failures',
  'After 30 failures',
  'After 3 failures',
  '-- REPLY ABOVE THIS LINE --',
  'Connection has been established successfully',
  'Validate error @ ',
  'Email provided is not instance of IncominEmail object.',
  'Additional settings is not array',
  'Add New Comment',
  'Add new comment to project object.',
  'Add New Discussion',
  'Add new discussion to specific project.',
  'Add New File',
  'Add new file to specific project.',
  'Ignore',
  'Ignore incoming mail.',
  'Move to trash',
  'Move incoming mail to trash.',
  'Add New Task',
  'Add new task to specific project.',
  'Not Checked',
  'Status OK',
  'Last update failed',
  'Mailbox name is required',
  'Username is required',
  'Password is required',
  'Hostname is required',
  'Port is required',
  'Server type is required',
  'Security type is required',
  'Email is required',
  'Email is already used by :name user',
  'Unknown conflict status',
  'Unknown',
  'Auto respond or delivery failure message deleted.',
  '[SUBJECT NOT PROVIDED]',
  '[CONTENT NOT PROVIDED]',
  'This object is not instance of IAttachments @ ',
  'Disabled',
  'Native PHP mail()',
  'Silent',
  'SMTP (:host::port)',
  'Messages',
  'This is a single email that contains following messages',
  'Please scroll through the entire message to see all individual messages',
  '<a href=":unsubscribe_url">Click here</a> to stop receiveing notifications about this :type',
  '<a href=":unsubscribe_url">Stop receiving email notifications</a> about this :object_type',
  'object',
  'Powered by',
  'Auto respond message \':subject\' from \':mailbox_name\' mailbox deleted.',
  'Incoming mail ":subject" deleted.',
  'Message ":subject" - import error',
  'Mailbox does not accept emails from unregistered users',
  'User does not have permission to create object in selected project',
  'Requested parent object does not exist',
  'User does not have permission to create comment in selected object',
  'Object cannot be saved, possibly because of validation errors',
  'Project does not exists',
  'Object does not accept comments. Either it is locked for comments or it does not support comments',
  'No filter applied over incoming mail. No match.',
  'Message ":subject" received and imported',
  'Couldn\'t get message from server - :mailbox_name. Error message: :error_message.',
  '":subject" sent',
  '[No Subject]',
  'Sender email is not valid email address',
  'Sender email is required',
  'Recipient email is not valid email address',
  'Recipient email is required',
  'Hello :first_name',
  'System compiled a list of :count notifications that might interest you',
  'Sender',
  'Recipient',
  'Mailbox',
  'Subject',
  'Message',
  'Original Sender',
  'Error Message',
  'Filter',
  'Action',
  'Resolve',
  'To resolve incoming mail conflict go to \'Incoming Mail Conflicts\' tab.',
  'Target',
  'Click {link href=$log_entry->getIncomingMail()->getImportUrl() title=\'Resolve Conflict\' class=\'import_button\'}here{/link} to resolve this conflict.',
  'View created object',
  'here',
  'Resolve Conflict',
  'About Incoming Mail',
  'Incoming mail module allows you to define any number of POP3 or IMAP mailboxes that will be checked periodically for new messages. When new message is detected, it will be downloaded and handled based on mailbox settings. If message is successfully imported it will be deleted from mail server',
  'Messages sent as replies to activeCollab email notifications are automatically submitted as comments to object notification is related to. If message is not a reply, or object notification is related to cannot have comments, new discussion or ticket is created based on mailbox settings',
  'Files attached to email messages are properly imported and attached to discussions, tasks or comments that are created. Note that your platform may limit maximum attachment size that can be handled. Attachments that are too large to be handled will be ignored',
  'Outgoing Mail',
  'Engine',
  'Method',
  'From',
  'Queue',
  'One Message in Queue',
  ':total Messages in Queue',
  'Failures: :unsent',
  'Mailboxes',
  'Conflicts',
  'You have 1 conflict.',
  'You have :num conflicts.',
  'No Conflicts',
  'Filters',
  'Change Settings',
  'Manage Mailboxes',
  'Manage Filters',
  'Control Panel',
  'Email Settings',
  'Incoming Email Settings',
  'Email Log',
  'Log',
  'Leave email field empty to use default address (:admin_email)',
  'From: Name',
  'From: Email',
  'Use this checklist to configure activeCollab\'s "Reply to Comment" feature. This feature will not work unless all of the listed requirements are met!',
  'Required Extension Status',
  'Installed',
  'Not Yet Installed',
  'Installation Instructions',
  'IMAP PHP extension is required for activeCollab to be able to connect to POP3 and IMAP mailboxes and import messages',
  'Different operating systems have differnet installation instructions, so please consult <a href=":instructions_url">main setup article</a> in PHP documentation, as well as resources available for the operating system that you are using. Example queries',
  'If someone else is hosting or managing a server for you, please consult your system administrator for assistance with IMAP extension installation',
  'This is mailbox name on your POP3/IMAP server. In most cases it should be left as default value (\'INBOX\') unless you want to check some other mailbox',
  'Test and Update Connection Parameters',
  'Server Type',
  'Server Address',
  'Port',
  'Username',
  'Password',
  'Security',
  'Mailbox Name',
  'General Settings',
  'Conflict Notifications',
  'Save Changes',
  'Conflict Mail',
  'Sender:',
  'Recipient:',
  'Mailbox:',
  'Subject:',
  'Mail priority:',
  'Created on:',
  'Conflict reason:',
  'Mail body:',
  'Attachments:',
  'Unavailable Actions',
  'Conflict resolve',
  'Remove All',
  'Remove Selected',
  'Are you sure that you want to remove all conflicts from your system? These conflicts will be permanently removed.',
  'There are no actions defined.',
  'Apply this Filter To',
  'Body',
  'Priority',
  'Has Attachments',
  'Sender Email',
  'To Email',
  'Filter Name',
  'All Mailboxes',
  'Selected Mailboxes',
  'Enabled',
  'Action Name',
  'Reason',
  'There are no unavailable actions',
  'Add Filter',
  'Edit Filter',
  'Incoming Mail Filters',
  'View Filter',
  'Filter Details',
  'Name:',
  'Description:',
  'Body rule:',
  'Subject rule:',
  'Priority rule:',
  'Attachments rule:',
  'Sender rule:',
  'Action name:',
  'In project:',
  'Account',
  'Email address that will be checked',
  'Connection',
  'Account Name',
  'Email Address',
  'View',
  'Add Mailbox',
  'Edit Mailbox',
  'List emails',
  'Emails in "{$active_mailbox->getName()} - {$active_mailbox->getEmail()}" mailbox (:unread unread of :total total)',
  'UID',
  'Date',
  'No emails in mailbox',
  'Delete',
  'Are you sure that you want to delete selected emails?',
  'Mailbox Log',
  'Content',
  'Send Email',
  'Recipients',
  'Send Test Message',
  'This value is set in <strong>Notifications From</strong> block on <strong>Administration</strong> &raquo; <strong>Outgoing Mail</strong> page',
  'Outgoing Email Settings',
  'Native Mailer Options',
  'SMTP host',
  'SMTP port',
  'SMTP Authentication',
  'Allow users to individually configure how they would like to have email messages delivered',
  'Don\'t send any email messages or notifications',
  'Notification and email system is working, but does not send any messages. This mode is great for testing',
  'Native',
  'Native PHP mailing. PHP needs to be properly configured so system can send messages using <a href="http://www.php.net/mail" target="_blank">mail()</a> function',
  'Default value is "-oi -f %s". If you are <strong>experiencing problems</strong> with default value, try with no native mailer options',
  'SMTP',
  'Use external SMTP server to send messages',
  'Send Emails',
  'Instantly',
  'Send emails in the same requests in which they were prepared',
  'In the Background',
  'Don\'t slow down the request that prepared the notification, but send the message as soon as possible',
  'Message Settings',
  'Force for All Notifications (Recommended)',
  'Mark Notifications as Bulk Email (Recommended)',
  'Outgoing Mail Queue',
  'Message Details',
  'Project:',
  'In ',
  'Subscribe sender to parent object',
  'In Project',
  'Allow for',
  'Create task as',
  'Send notification email and create public page',
  'Everyone',
  'Registered users with proper permissions',
  'Create as:',
  'Subscribe and notify sender and all cc recipients',
  'Create public page',
  'Create file as',
  'Send notification email',
  'Use message priority',
  'Set it to:',
); ?>