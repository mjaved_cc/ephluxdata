<?php

  /**
   * Reports framework initialization file
   * 
   * @package angie.frameworks.reports
   */

  define('REPORTS_FRAMEWORK', 'reports');
  define('REPORTS_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/reports');
  
  // Name of the module that provides interface to this framework
  @define('REPORTS_FRAMEWORK_INJECT_INTO', 'system');
  
  AngieApplication::setForAutoload(array(
    'FwReportsPanel' => REPORTS_FRAMEWORK_PATH . '/models/FwReportsPanel.class.php',  
    'IReportsPanelRow' => REPORTS_FRAMEWORK_PATH . '/models/IReportsPanelRow.class.php',  
    'ReportsPanelRow' => REPORTS_FRAMEWORK_PATH . '/models/ReportsPanelRow.class.php',  
  ));