<?php

  /**
   * Framework level payments gateways manager class
   *
   * @package angie.frameworks.payments
   * @subpackage models
   */
  abstract class FwPaymentGateways extends BasePaymentGateways {
  	
    /**
  	 * Return slice of incoming mailbox definitions based on given criteria
  	 * 
  	 * @param integer $num
  	 * @param array $exclude
  	 * @param integer $timestamp
  	 * @return DBResult
  	 */
  	function getSlice($num = 10, $exclude = null, $timestamp = null) {
  		if($exclude) {
  			return self::find(array(
  			  'conditions' => array('id NOT IN (?)', $exclude), 
  			  'order' => 'id', 
  			  'limit' => $num,  
  			));
  		} else {
  			return self::find(array(
  			  'order' => 'id', 
  			  'limit' => $num,  
  			));
  		} // if
  	} // getSlice
  	
  	
  	/**
  	 * Return all gateways with supported currency
  	 * 
  	 * @param $currency_code
  	 */
  	static function findAllCurrencySupported($currency_code, $enabled = true) {
  	  $all_gateways = self::find(array(
  	    'conditions' => array('is_enabled = ?',$enabled)
  	  ));
  	  $supported_gateways = array();
  	  if(is_foreachable($all_gateways)) {
    	  foreach ($all_gateways as $gateway) {
    	    if(is_foreachable($gateway->supported_currencies)) {
        	  if(in_array($currency_code,$gateway->supported_currencies)) {
        	    $supported_gateways[] = $gateway;
        	  } //if
    	    } //if
    	  } //foreach
  	  } //if
  	  return $supported_gateways;
  	} //findAllCurrencySupported
  	
  	/**
  	 * Return default payment gateway
  	 * 
  	 * @return PaymentGateway object
  	 */
  	static function findDefault() {
  	   return self::find(array(
        'conditions' => array("is_default = ?", 1),
  	    'one' => true  	    
      ));
   } //findDefault
   
   /**
  	 * Return enabled payment gateways
  	 * 
  	 * @return mixed
  	 */
  	static function findEnabled() {
  	   return self::find(array(
        'conditions' => array("is_enabled = ?", 1),
  	   ));
   } //findDefault
   
   
  } //FwPaymentGateways