<?php

  /**
   * ActiveCollab application class
   * 
   * @package activeCollab
   */
  class ActiveCollab extends AngieApplicationAdapter {
    
    /**
     * Return application name
     *
     * @return string
     */
    function getName() {
      return 'activeCollab';
    } // getName
    
    /**
     * Return application URL
     * 
     * @return string
     */
    function getUrl() {
      return 'http://www.activecollab.com';
    } // getUrl
    
    /**
     * Returns true if current application version is stable
     *
     * @return boolean
     */
    function isStable() {
      return false;
    } // isStable
    
    /**
     * Return vendor name
     * 
     * @return string
     */
    function getVendor() {
      return 'A51';
    } // getVendor
    
    /**
     * Return license agreement URL
     * 
     * @return string
     */
    function getLicenseAgreementUrl() {
      return 'http://www.activecollab.com/docs/manuals/licensing/license-agreement';
    } // getLicenseAgreementUrl
    
    /**
     * Return application API version
     *
     * @return string
     */
    function getApiVersion() {
      return '3.1.16';
    } // getApiVersion
    
    /**
     * Return something that makes this application instance unique
     *
     * @return string
     */
    function getUniqueKey() {
      return LICENSE_KEY;
    } // getUniqueKey
    
    /**
     * Returns true if branding removal is part of the license
     * 
     * @return boolean
     */
    function getBrandingRemoved() {
      $config_option = ConfigOptions::getValue('license_copyright_removed');
      return isset($config_option) ? $config_option : LICENSE_COPYRIGHT_REMOVED;
    } // getBrandingRemoved
    
    // ---------------------------------------------------
    //  First run
    // ---------------------------------------------------
    
    /**
     * Do operations that require entire system to be loaded, but are executed 
     * on the first system run
     */
    function onFirstRun() {
      if(!AngieApplication::isModuleLoaded('tasks')) {
        DB::execute('DELETE FROM ' . TABLE_PREFIX . 'homescreen_widgets WHERE type IN (?)', array(
          'MyTasksHomescreenWidget'
        ));
      } // if

      if(AngieApplication::isModuleLoaded('invoicing')) {
        
        // Convert original admin role to admin + F role
        $admin_plus_f = Roles::findById(1);
        
        $admin_permissions = $admin_plus_f->getPermissions();
        
        $admin_plus_f->setName('Administrator (+ Finances)');
        $admin_plus_f->setPermissionValue('can_manage_finances', true);
        $admin_plus_f->save();
        
        // Recreate admin role
        $admin = new Role();
        
        $admin->setAttributes(array(
          'name' => 'Administrator', 
          'permissions' => $admin_permissions, 
        ));
        
        $admin->save();
      } // if
    } // onFirstRun
    
  }