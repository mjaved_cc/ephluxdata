<?php

  /**
   * Tasks module on_get_project_object_types events handler
   *
   * @package activeCollab.modules.tasks
   * @subpackage handlers
   */
  
  /**
   * Return tasks module project object types
   *
   * @return string
   */
  function tasks_handle_on_get_project_object_types() {
    return 'task';
  } // tasks_handle_on_get_project_object_types