<?php

  /**
   * Init tasks module
   *
   * @package activeCollab.modules.tasks
   */
  
  define('TASKS_MODULE', 'tasks');
  define('TASKS_MODULE_PATH', APPLICATION_PATH . '/modules/tasks');
  
  AngieApplication::useModel('public_task_forms', TASKS_MODULE);
  
  AngieApplication::setForAutoload(array(
    'Task' => TASKS_MODULE_PATH . '/models/tasks/Task.class.php',
    'Tasks' => TASKS_MODULE_PATH . '/models/tasks/Tasks.class.php',
    
    'TaskCategory' => TASKS_MODULE_PATH . '/models/task_categories/TaskCategory.class.php',
    'ITaskCategoryImplementation' => TASKS_MODULE_PATH . '/models/task_categories/ITaskCategoryImplementation.class.php',
    
    'TaskComment' => TASKS_MODULE_PATH . '/models/comments/TaskComment.class.php',
    'ITaskCommentsImplementation' => TASKS_MODULE_PATH . '/models/comments/ITaskCommentsImplementation.class.php',
    
  	'ITaskSharingImplementation' => TASKS_MODULE_PATH . '/models/ITaskSharingImplementation.class.php',
  
    'TaskHistoryRenderer' => TASKS_MODULE_PATH . '/models/TaskHistoryRenderer.class.php',
  
  	'TasksProjectExporter' => TASKS_MODULE_PATH . '/models/TasksProjectExporter.class.php',
   
  	'TasksFilterHomescreenWidget' => TASKS_MODULE_PATH . '/models/homescreen_widgets/TasksFilterHomescreenWidget.class.php', 
  	'MyTasksHomescreenWidget' => TASKS_MODULE_PATH . '/models/homescreen_widgets/MyTasksHomescreenWidget.class.php', 
  	'DelegatedTasksHomescreenWidget' => TASKS_MODULE_PATH . '/models/homescreen_widgets/DelegatedTasksHomescreenWidget.class.php', 
  	'UnassignedTasksHomescreenWidget' => TASKS_MODULE_PATH . '/models/homescreen_widgets/UnassignedTasksHomescreenWidget.class.php', 
  
  	'ITaskSearchItemImplementation' => TASKS_MODULE_PATH . '/models/ITaskSearchItemImplementation.class.php',
  
  	'ITaskInspectorImplementation' => TASKS_MODULE_PATH . '/models/ITaskInspectorImplementation.class.php',

  	'ITaskStateImplementation' => TASKS_MODULE_PATH . '/models/ITaskStateImplementation.class.php',

    'AggregatedTasksReport'     => TASKS_MODULE_PATH . '/models/AggregatedTasksReport.class.php',
  	
    'IInvoiceBasedOnTaskImplementation' => TASKS_MODULE_PATH . '/models/IInvoiceBasedOnTaskImplementation.class.php',
  ));
  
  DataObjectPool::registerTypeLoader('Task', function($ids) {
    return Tasks::findByIds($ids, STATE_TRASHED, VISIBILITY_PRIVATE);
  });
  
  DataObjectPool::registerTypeLoader('TaskComment', function($ids) {
    return Comments::findByIds($ids);
  });