<?php

  // Include applicaiton specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

  /**
   * Tasks module model
   * 
   * @package activeCollab.modules.tasks
   * @subpackage models
   */
  class TasksModuleModel extends ActiveCollabModuleModel {
    
    /**
     * Construct tasks module model
     * 
     * @param TasksModule $parent
     */
    function __construct(TasksModule $parent) {
      parent::__construct($parent);
      
      $this->addModel(DB::createTable('public_task_forms')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('project_id', 11, '0')->setUnsigned(true), 
        DBStringColumn::create('slug', 50, ''), 
        DBNameColumn::create(100), 
        DBTextColumn::create('body'), 
        DBBoolColumn::create('is_enabled', true), 
        DBAdditionalPropertiesColumn::create(), 
      ))->addIndices(array(
        DBIndex::create('slug', DBIndex::UNIQUE, 'slug'), 
      )));
    } // __construct
  
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
      $this->addConfigOption('task_categories', array('General'));
      
      $this->addConfigOption('tasks_auto_reopen', true);
      $this->addConfigOption('tasks_auto_reopen_clients_only', true);
      $this->addConfigOption('tasks_public_submit_enabled', false);
      $this->addConfigOption('tasks_use_captcha', false);
      
      parent::loadInitialData($environment);
    } // loadInitialData
    
  }