<div id="add_invoice">
  {form action=$active_object->invoice()->getUrl() method='post'}
  <input type="hidden" name="filter_data" value={$filter_data} />
  	<div class="content_stack_wrapper">
      {if !$is_based_on_quote}
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Time & Expenses{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          <div id="add_invoice_time_and_expenses">
            <div id="add_invoice_time">
              <h3>{lang}Time{/lang}</h3>
              <ul>
                <li>{lang}Total{/lang}: {sum_time object=$active_object user=$logged_user}</li>
                <li>{lang}Billable{/lang}: {sum_time object=$active_object user=$logged_user mode=billable}</li>
              </ul>
          	</div>
          	<div id="add_invoice_expenses">
          		<h3>{lang}Expenses{/lang}</h3>
          	  <ul>
          	    <li>{lang}Total{/lang}: {sum_expenses object=$active_object user=$logged_user}</li>
                <li>{lang}Billable{/lang}: {sum_expenses object=$active_object user=$logged_user mode=billable}</li>
          	  </ul>
          	</div>
          </div>
        </div>
      </div>
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Settings{/lang}</h3>
        </div>
        <div class="content_stack_element_body">

          {wrap field=invoice_settings}
            {when_invoice_is_based_on name="invoice_data[settings]" label='When creating a new invoice' required=true}
          {/wrap}

          {wrap field=tax_rate}
            {select_tax_rate name="invoice_data[tax_rate_id]" label='Tax Rate' optional=true }
          {/wrap}

          {wrap field=allow_partial}
            {select_payments_type name="invoice_data[payments_type]" label='Payments' required=true}
          {/wrap}

          {if $is_based_on_tracking_report}
            {wrap field=currency}
              {select_currency name="invoice_data[currency_id]" label='Currency' required=true optional=true}
              <p class="details">{lang}Keep in mind that <u>invoice will be generated in selected currency</u>, regardless of invoice items currency{/lang}.</p>
            {/wrap}
          {/if}
        </div>
      </div>
      {/if}

      {if $project instanceof Project}
        <div class="content_stack_element">
          <div class="content_stack_element_info">
            <h3>{lang}Project{/lang}</h3>
          </div>
          <div class="content_stack_element_body">
          {wrap field=project_id}
            {select_project name="invoice[project_id]" value=$project->getId() user=$logged_user optional=true required=false}
          {/wrap}
          </div>
        </div>
      {/if}

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Client{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=company_id}
            {select_company name="invoice_data[company_id]" value=$company_id id="companyId" user=$logged_user can_create_new=false label='Client' required=true}
          {/wrap}
          {wrap field=company_address class=companyAddressContainer}
            {textarea_field name="invoice_data[company_address]" id=companyAddress label='Address' required=true}{/textarea_field}
          {/wrap}
         </div>
      </div>
         
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Notes & Comments{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          <div class="wrap_invoice_note_and_comment">
            {wrap field=invoice_note class='wrap_invoice_note'}
              {invoice_note name="invoice_data[note]" class='long' label='Invoice Note'}{$note nofilter}{/invoice_note}
            {/wrap}
              
            {wrap field=comment class='wrap_invoice_comment'}
              {invoice_comment name="invoice_data[comment]" label='Our Comment'}{/invoice_comment}
            {/wrap}
          </div>
        </div>
      </div>
    </div>
    
    {wrap_buttons}
	  	{submit}Create Invoice{/submit}
    {/wrap_buttons}
  {/form} 
</div>
<script type="text/javascript">
  $('#add_invoice').each(function() {
    var add_invoice = $(this);
    
    var company_id = add_invoice.find('#companyId');
    var company_address = add_invoice.find('#companyAddress');
    var company_details_url = {$js_company_details_url|json nofilter};
    
    var ajax_request;    
    company_id.change(function() {
  	  add_address();
    });
    add_address();

    function add_address() {
  	   if(company_id.length > 0) {
  	    var ajax_url = App.extendUrl(company_details_url, {
  	      company_id  : company_id.val(),
  	      async       : 1,
  	      skip_layout : 1
  	    });
  	    
  	    // abort request if already exists and it's active
  	    if ((ajax_request) && (ajax_request.readyState !=4)) {
  	      ajax_request.abort();
  	    } // if
  	    
  	    if (!company_address.is('loading')) {
  	      company_address.addClass('loading');
  	    } // if
  	    
  	    company_address.attr("disabled", true);
  	    company_id.attr("disabled", true);
  	    
  	    ajax_request = $.ajax({
  	      'url' : ajax_url,
  	      'success' : function (response) {
  	        company_address.val(response);
  	        company_address.removeClass('loading');
  	        company_address.attr("disabled", false);
  	        company_id.attr("disabled", false);
  	      }
  	    });
  	   }
    }
  });
</script>