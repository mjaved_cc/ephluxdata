{title}Tax Rates{/title}
{add_bread_crumb}List All{/add_bread_crumb}

<div id="tax_rates"></div>

<script type="text/javascript">
  $('#tax_rates').pagedObjectsList({
    'load_more_url' : '{assemble route=admin_tax_rates}', 
    'items' : {$tax_rates|json nofilter},
    'items_per_load' : {$items_per_page}, 
    'total_items' : {$total_items}, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'tax_rates' }, 
    'columns' : {
      'name' : App.lang('Name'), 
      'tax_rate' : App.lang('Percentage'), 
      'options' : '' 
    }, 
    'sort_by' : 'name', 
    'empty_message' : App.lang('There are no tax rates defined'), 
    'listen' : 'tax_rate', // created, updated, deleted
    'on_add_item' : function(item) {
      var tax_rate = $(this);
      
      tax_rate.append(
	    	'<td class="name"></td>' +
       	'<td class="percentage"></td>' + 
       	'<td class="options"></td>'
      );

      tax_rate.attr('id',item['id']);
	  
      tax_rate.find('td.name').text(item['name'].clean());
	  	tax_rate.find('td.percentage').text(item['percentage']);

 		  if(item['permissions']['can_edit']) {
	 		  tax_rate.find('td.options').append('<a href="' + item['urls']['edit'] + '" class="edit_tax_rate" title="' + App.lang('Change Settings') + '"><img src="{image_url name="icons/12x12/edit.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>'); 
    	  tax_rate.find('td.options a.edit_tax_rate').flyoutForm({
          'success_event' : 'tax_rate_updated',
          'width' : 500
        }); 
     	} // if

  	  if(item['permissions']['can_delete']) {
  		  tax_rate.find('td.options').append('<a href="' + item['urls']['delete'] + '" class="delete_tax_rate" title="' + App.lang('Remove Item') + '"><img src="{image_url name="icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>'); 
  		  tax_rate.find('td.options a.delete_tax_rate').asyncLink({
          'confirmation' : App.lang('Are you sure that you want to permanently delete this item?'), 
          'success_event' : 'tax_rate_deleted', 
          'success_message' : App.lang('Tax rate has been deleted successfully'), 
          'error' : function() {
            App.Wireframe.Flash.error(App.lang('Failed to delete selected item'));
          }
        }); 
  	 	} // if
    }
  });
</script>