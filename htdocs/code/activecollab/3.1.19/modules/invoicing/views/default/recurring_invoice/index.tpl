{title}Recurring Profiles{/title}
{add_bread_crumb}Recurring Profiles{/add_bread_crumb}

<div id="recurring_profile">

  <div class="empty_content">
      <div class="objects_list_title">{lang}Recurring Profiles{/lang}</div>
      <div class="objects_list_icon"><img src="{image_url name='icons/48x48/recurring-profiles.png' module=invoicing}" alt=""/></div>
      <div class="objects_list_details_actions">
          <a href="{assemble route='recurring_profile_add'}" id="new_recurring_profile">{lang}New Recurring Profile{/lang}</a>
      </div>
      <div id="skipped_profiles_wrapper">
			{if is_foreachable($skipped_profiles)}
        <table cellspacing="0" class="skipped_profiles_table">
        {foreach $skipped_profiles as $profile}
          <tr id="skipped_profile_{$profile->getId()}">
            <td class="warning_icon"><img src="{image_url name="layout/bits/indicator-warning.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></td>
            <td class="name left">
             {object_link object=$profile}
            </td>
            <td class="next_trigger right">{lang}Skipped to trigger on:{/lang} 
             {$profile->getNextTriggerOn()|date}
            </td>
            <td class="occurrance right">{lang}Occurrence:{/lang} # 
             {$profile->getNextOccurrenceNumber()}
            </td>
            <td class="skipped_link right"> 
            	{link href=$profile->getTriggerUrl() class="link_button_alternative"}Trigger{/link}
            </td>
          </tr>
        {/foreach}
        </table>
      {/if}
      </div>

      <div class="object_lists_details_tips">
        <h3>{lang}Tips{/lang}:</h3>
        <ul>
          <li>{lang}To select a recurring profile and load its details, please click on it in the list on the left{/lang}</li>
          <!--<li>{lang}It is possible to select multiple recurring profiles at the same time. Just hold Ctrl key on your keyboard and click on all the recurring profiles that you want to select{/lang}</li>-->
        </ul>
      </div>
  </div>
  
  <!--<div class="multi_content">
      <table>    
        <tr>
          <td class="checkbox"><label><input type="checkbox" value="change_project_client" />{lang}Change Client{/lang}</label></td>
          <td class="new_value">{select_company name=company_id user=$logged_user optional=true can_create_new=false}</td>
        </tr>
      </table>
  </div>-->
</div>

<script type="text/javascript">
  $('#new_recurring_profile').flyoutForm({
    'success_event' : 'recurring_profile_created',
    'title' : App.lang('New Recurring Profile')    
  });

  $('#recurring_profile').each(function() {
    var objects_list_wrapper = $(this);
  

    var items = {$recurring_profiles|json nofilter};
    var companies_map = {$companies_map|json nofilter};
    var mass_edit_url = '{assemble route=recurring_profiles_mass_edit}';
    var print_url = '{assemble route=recurring_profiles print=1}';

    objects_list_wrapper.find('div#skipped_profiles_wrapper td.skipped_link a').asyncLink({
        	'success_event' : 'recurring_profile_updated',
        	'confirmation' : App.lang('Are you sure that you want to trigger this profile?'),
          'success_message' : App.lang('Recurring invoice profile has been successfully triggered')  	    
	 			});
			


    
    objects_list_wrapper.objectsList({
      'id'                : 'recurring_invoices',
      'items'             : items,
      'required_fields'   : ['id', 'name', 'client_id', 'permalink'],
      'objects_type'      : 'recurring_profiles',
      'events'            : App.standardObjectsListEvents(),
      'multi_title'       : App.lang(':num Recurring Profiles Selected'),
      'multi_url'         : mass_edit_url,
      'print_url'          : print_url,
      'prepare_item'      : function (item) {
        return {
          'id' : item['id'],
          'name' : item['name'],
          'client_id' : item['client']['id'],
          'permalink' : item['urls']['view'],
          'is_archived' : item['state'] == '2' ? '1' : '0',
          'is_skipped' : item['is_skipped']
        };
      },
      'render_item'       : function (item) {
  	    var warning_image = '';
  	    if (item.is_skipped) {
  	  	  warning_image = '<img src="' + App.Wireframe.Utils.indicatorUrl('warning') + '" id="item_icon_' + item.id + '" />';
  	    }//if
        return '<td class="recurring_profile_name">' + item.name.clean() + '</td><td class="recurring_profile_attention_image"> ' + warning_image + '</td>'; 
  	  },

      'grouping'          : [{ 
        'label' : App.lang("Don't group"), 
        'property' : '', 
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      }, { 
        'label' : App.lang('By Client'), 
        'property' : 'client_id', 
        'map' : companies_map , 
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-client.png', 'system'), 
        'uncategorized_label' : App.lang('Unknown Client'), 
        'default' : true  
      }],
      
      'filtering' : [{ 
          'label' : App.lang('Status'), 
          'property'  : 'is_archived', 
          'values'  : [{ 
            'label' : App.lang('All Profiles'), 
            'value' : '', 
            'icon' : App.Wireframe.Utils.imageUrl('objects-list/all-invoices.png', 'invoicing') , 
            'default' : true, 
            'breadcrumbs' : App.lang('All Profiles')  
          }, { 
            'label' : App.lang('Active'), 
            'value' : '0', 
            'icon' : App.Wireframe.Utils.imageUrl('objects-list/draft-invoices.png', 'invoicing'), 
            'breadcrumbs' : App.lang('Active') 
          }, { 
            'label' : App.lang('Archived'), 
            'value' : '1', 
            'icon' : App.Wireframe.Utils.imageUrl('objects-list/issued-invoices.png', 'invoicing'), 
            'breadcrumbs' : App.lang('Archived')
          }]
        }]
    });

    
    // recurring profile added
    App.Wireframe.Events.bind('recurring_profile_created.content', function (event, profile) { 
      objects_list_wrapper.objectsList('add_item', profile);
    });

    // recurring profile update
    App.Wireframe.Events.bind('recurring_profile_updated.content', function (event, profile) {
      objects_list_wrapper.objectsList('update_item', profile);
      var skipped_profiles_wrapper = objects_list_wrapper.find('div#skipped_profiles_wrapper');
      skipped_profiles_wrapper.find('tr#skipped_profile_' + profile.id).remove();
      if(skipped_profiles_wrapper.find('.skipped_profiles_table tr').length == 0) {
    	  skipped_profiles_wrapper.remove();
      }
      
    });

    // recurring profile deleted
    App.Wireframe.Events.bind('recurring_profile_deleted.content', function (event, profile) {
      objects_list_wrapper.objectsList('delete_item', profile['id']);
    });

    // keep company_id map up to date
    App.objects_list_keep_companies_map_up_to_date(objects_list_wrapper, 'client_id', 'content');

    // Pre select item if this is permalink
	  {if $active_recurring_profile->isLoaded()}
	  	objects_list_wrapper.objectsList('load_item', {$active_recurring_profile->getId()}, {$active_recurring_profile->getViewUrl()|json nofilter});
	  {/if}


		 
  });
</script>