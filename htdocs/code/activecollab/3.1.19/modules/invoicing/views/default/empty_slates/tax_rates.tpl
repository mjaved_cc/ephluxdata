<div id="empty_slate_tax_rates" class="empty_slate">
  <h3>{lang}About Tax Rates{/lang}</h3>
  
  <ul class="icon_list">
    <li>
      <img src="{image_url name="empty-slates/tax-rates.png" module=$smarty.const.INVOICING_MODULE}" class="icon_list_icon" alt="" />
      <span class="icon_list_title">{lang}Tax Rates{/lang}</span>
      <span class="icon_list_description">{lang}System supports definition of unlimited number of tax rates, but only one tax rate can be used per invoice item. Each invoice item can have a different tax rate set or no tax applied at all. Tax rates that are already in used can't be deleted unless all invoices which use it are deleted{/lang}.</span>
    </li>
    
    <li>
      <img src="{image_url name="empty-slates/administration/update.png" module=$smarty.const.SYSTEM_MODULE}" class="icon_list_icon" alt="" />
      <span class="icon_list_title">{lang}Updating Tax Rate{/lang}</span>
      <span class="icon_list_description">{lang}When you update a tax rate, change will be applied to all past and future invoices. If rate of any specific tax has been changed and you would like old invoices to preserve old rate, create a new tax rate instead of updating the old one{/lang}.</span>
    </li>
  </ul>
</div>