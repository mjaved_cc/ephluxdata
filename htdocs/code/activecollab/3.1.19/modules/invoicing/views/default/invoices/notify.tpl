{title}Resend Email{/title}
{add_bread_crumb}Resend Email{/add_bread_crumb}

<div id="resend_invoice_email">
  {form action=$active_invoice->getNotifyUrl() method=post}
    {wrap_fields}
      <p>{lang}Issued On{/lang}: {$active_invoice->getIssuedOn()|date:0}<br>{lang}Payment Due On{/lang}:

        {if $active_invoice->isOverdue()}
          <span class="nok">{$active_invoice->getDueOn()|date:0} ({lang}Overdue{/lang})</span>
        {else}
          {$active_invoice->getDueOn()|date:0}
        {/if}
      </p>
      
      {if $client_company_managers}
				<div id="issue_invoice_dates">
          {wrap field=due_on}
            {select_date name='issue[due_on]' value=$issue_data.due_on id=issueFormDueOn required=true label='Payment Due On'}
          {/wrap}
  	    </div>
	    {/if}
	    
	    {if $client_company_managers || $client_company_manager_roles}
  	    <div id="issue_invoice_send_email">
  	  		{label}Resend Email{/label}

          {if $client_company_managers}
            <div>
    	        {select_user name='issue[user_id]' value=$issue_data.issued_to_id users=$client_company_managers user=$logged_user}
    	        <input type="hidden" name="issue[send_emails]" value="1">
            </div>
          {else}
        	  <p>{lang}System is able to a send copy of issued invoice to the client, but to do that, there needs to be at least one user in the client company with one of the following roles{/lang}:</p>
        	
            {if $client_company_manager_roles}
              <ul>
              {foreach $client_company_manager_roles as $client_company_manager_role}
                <li>{$client_company_manager_role->getName()}</li>
              {/foreach}
              </ul>
            {/if}
          {/if}
      	</div>
      {else}
        <input type="hidden" name="issue[send_emails]" value="0">
      {/if}
    {/wrap_fields}
   
    {wrap_buttons}
    	 {if $client_company_managers}
      	 {submit}Resend Email{/submit}
       {else}
         {button id="close_btn"}Close{/button}
       {/if}
    {/wrap_buttons}
  {/form}
</div>

<script type="text/javascript">
	var close_btn = $('#close_btn');
	close_btn.click(function(){
		App.widgets.FlyoutDialog.front().close();
	});
</script>
