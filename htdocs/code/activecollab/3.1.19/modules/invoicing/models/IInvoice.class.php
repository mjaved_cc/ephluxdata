<?php

  /**
   * Interface that all invoice instances need to implement
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  interface IInvoice {
  	
  	/**
  	 * Return invoice ID
  	 * 
  	 * @param void
  	 * @return number
  	 */
		function getId();
		
		/**
		 * Return invoice number
		 * 
		 * @param void
		 * @return integer
		 */
		function getNumber();

		/**
		 * Return invoice name
		 * 
		 * @param boolean $short
		 * @return string
		 */
		function getName($short=false);

		/**
		 * Return date when invoice is due
		 * 
		 * @param void
		 * @return DateValue
		 */
		function getDueOn();
		
		/**
		 * Return date when invoice is issued
		 * 
		 * @param void
		 * @return DateValue
		 */
		function getIssuedOn();
		
		/**
		 * Get company name
		 * 
		 * @param void
		 * @return null
		 */
		function getCompanyName();
		
		/**
		 * Return company address
		 * 
		 * @param void
		 * @return null
		 */
		function getCompanyAddress();
		
		/**
		 * Get invoice items
		 * 
		 * @param void
		 * @return array
		 */
		function getItems();
		
    /**
     * Return invoice total
     *
     * @param boolean $cache
     * @return float
     */
    function getTotal();

    /**
     * Return calculated tax
     *
     * @param boolean $cache
     * @return float
     */
    function getTax();

    /**
     * Returned taxed total
     *
     * @param boolean $cache
     * @return float
     */
    function getTaxedTotal();
    
		/**
		 * Is invoice issued
		 * 
		 * @param void
		 * @return boolean
		 */
		function isIssued();
      
		/**
		 * Is invoice paid
     *
		 * @return boolean
		 */		
		function isPaid();

  }