<?php


  /**
   * BaseRecurringProfileItem class
   *
   * @package ActiveCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseRecurringProfileItem extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'recurring_profile_items';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'recurring_profile_id', 'position', 'tax_rate_id', 'description', 'quantity', 'unit_cost');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of recurring_profile_id field
     *
     * @return integer
     */
    function getRecurringProfileId() {
      return $this->getFieldValue('recurring_profile_id');
    } // getRecurringProfileId
    
    /**
     * Set value of recurring_profile_id field
     *
     * @param integer $value
     * @return integer
     */
    function setRecurringProfileId($value) {
      return $this->setFieldValue('recurring_profile_id', $value);
    } // setRecurringProfileId

    /**
     * Return value of position field
     *
     * @return integer
     */
    function getPosition() {
      return $this->getFieldValue('position');
    } // getPosition
    
    /**
     * Set value of position field
     *
     * @param integer $value
     * @return integer
     */
    function setPosition($value) {
      return $this->setFieldValue('position', $value);
    } // setPosition

    /**
     * Return value of tax_rate_id field
     *
     * @return integer
     */
    function getTaxRateId() {
      return $this->getFieldValue('tax_rate_id');
    } // getTaxRateId
    
    /**
     * Set value of tax_rate_id field
     *
     * @param integer $value
     * @return integer
     */
    function setTaxRateId($value) {
      return $this->setFieldValue('tax_rate_id', $value);
    } // setTaxRateId

    /**
     * Return value of description field
     *
     * @return string
     */
    function getDescription() {
      return $this->getFieldValue('description');
    } // getDescription
    
    /**
     * Set value of description field
     *
     * @param string $value
     * @return string
     */
    function setDescription($value) {
      return $this->setFieldValue('description', $value);
    } // setDescription

    /**
     * Return value of quantity field
     *
     * @return float
     */
    function getQuantity() {
      return $this->getFieldValue('quantity');
    } // getQuantity
    
    /**
     * Set value of quantity field
     *
     * @param float $value
     * @return float
     */
    function setQuantity($value) {
      return $this->setFieldValue('quantity', $value);
    } // setQuantity

    /**
     * Return value of unit_cost field
     *
     * @return float
     */
    function getUnitCost() {
      return $this->getFieldValue('unit_cost');
    } // getUnitCost
    
    /**
     * Set value of unit_cost field
     *
     * @param float $value
     * @return float
     */
    function setUnitCost($value) {
      return $this->setFieldValue('unit_cost', $value);
    } // setUnitCost

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'recurring_profile_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'position':
          return parent::setFieldValue($real_name, intval($value));
        case 'tax_rate_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'description':
          return parent::setFieldValue($real_name, strval($value));
        case 'quantity':
          return parent::setFieldValue($real_name, floatval($value));
        case 'unit_cost':
          return parent::setFieldValue($real_name, floatval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

