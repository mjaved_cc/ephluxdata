<?php


  /**
   * BaseQuote class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseQuote extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'quotes';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'public_id', 'based_on_type', 'based_on_id', 'company_id', 'company_name', 'company_address', 'currency_id', 'language_id', 'name', 'note', 'private_note', 'status', 'created_on', 'created_by_id', 'created_by_name', 'created_by_email', 'sent_on', 'sent_by_id', 'sent_by_name', 'sent_by_email', 'recipient_id', 'recipient_name', 'recipient_email', 'sent_to_id', 'sent_to_name', 'sent_to_email', 'closed_on', 'closed_by_id', 'closed_by_name', 'closed_by_email', 'is_locked', 'last_comment_on');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getPublicId() {
      return $this->getFieldValue('public_id');
    } // getPublicId

    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setPublicId($value) {
      return $this->setFieldValue('public_id', $value);
    } // setPublicId

    /**
     * Return value of based_on_type field
     *
     * @return string
     */
    function getBasedOnType() {
      return $this->getFieldValue('based_on_type');
    } // getBasedOnType
    
    /**
     * Set value of based_on_type field
     *
     * @param string $value
     * @return string
     */
    function setBasedOnType($value) {
      return $this->setFieldValue('based_on_type', $value);
    } // setBasedOnType

    /**
     * Return value of based_on_id field
     *
     * @return integer
     */
    function getBasedOnId() {
      return $this->getFieldValue('based_on_id');
    } // getBasedOnId
    
    /**
     * Set value of based_on_id field
     *
     * @param integer $value
     * @return integer
     */
    function setBasedOnId($value) {
      return $this->setFieldValue('based_on_id', $value);
    } // setBasedOnId

    /**
     * Return value of company_id field
     *
     * @return integer
     */
    function getCompanyId() {
      return $this->getFieldValue('company_id');
    } // getCompanyId
    
    /**
     * Set value of company_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCompanyId($value) {
      return $this->setFieldValue('company_id', $value);
    } // setCompanyId

    /**
     * Return value of company_name field
     *
     * @return integer
     */
    function getCompanyName() {
      return $this->getFieldValue('company_name');
    } // getCompanyName

    /**
     * Set value of company_name field
     *
     * @param integer $value
     * @return integer
     */
    function setCompanyName($value) {
      return $this->setFieldValue('company_name', $value);
    } // setCompanyName

    /**
     * Return value of currency_id field
     *
     * @return integer
     */
    function getCurrencyId() {
      return $this->getFieldValue('currency_id');
    } // getCurrencyId
    
    /**
     * Set value of currency_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCurrencyId($value) {
      return $this->setFieldValue('currency_id', $value);
    } // setCurrencyId

    /**
     * Return value of language_id field
     *
     * @return integer
     */
    function getLanguageId() {
      return $this->getFieldValue('language_id');
    } // getLanguageId
    
    /**
     * Set value of language_id field
     *
     * @param integer $value
     * @return integer
     */
    function setLanguageId($value) {
      return $this->setFieldValue('language_id', $value);
    } // setLanguageId

    /**
     * Return value of name field
     *
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of note field
     *
     * @return string
     */
    function getNote() {
      return $this->getFieldValue('note');
    } // getNote

    /**
     * Set value of note field
     *
     * @param string $value
     * @return string
     */
    function setNote($value) {
      return $this->setFieldValue('note', $value);
    } // setNote

    /**
     * Return value of private_note field
     *
     * @return string
     */
    function getPrivateNote() {
      return $this->getFieldValue('private_note');
    } // getPrivateNote
    
    /**
     * Set value of private_note field
     *
     * @param string $value
     * @return string
     */
    function setPrivateNote($value) {
      return $this->setFieldValue('private_note', $value);
    } // setPrivateNote

    /**
     * Return value of company_address field
     *
     * @return string
     */
    function getCompanyAddress() {
      return $this->getFieldValue('company_address');
    } // getCompanyAddress

    /**
     * Set value of company_address field
     *
     * @param string $value
     * @return string
     */
    function setCompanyAddress($value) {
      return $this->setFieldValue('company_address', $value);
    } // setCompanyAddress

    /**
     * Return value of status field
     *
     * @return integer
     */
    function getStatus() {
      return $this->getFieldValue('status');
    } // getStatus
    
    /**
     * Set value of status field
     *
     * @param integer $value
     * @return integer
     */
    function setStatus($value) {
      return $this->setFieldValue('status', $value);
    } // setStatus

    /**
     * Return value of created_on field
     *
     * @return DateTimeValue
     */
    function getCreatedOn() {
      return $this->getFieldValue('created_on');
    } // getCreatedOn
    
    /**
     * Set value of created_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setCreatedOn($value) {
      return $this->setFieldValue('created_on', $value);
    } // setCreatedOn

    /**
     * Return value of created_by_id field
     *
     * @return integer
     */
    function getCreatedById() {
      return $this->getFieldValue('created_by_id');
    } // getCreatedById
    
    /**
     * Set value of created_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCreatedById($value) {
      return $this->setFieldValue('created_by_id', $value);
    } // setCreatedById

    /**
     * Return value of created_by_name field
     *
     * @return string
     */
    function getCreatedByName() {
      return $this->getFieldValue('created_by_name');
    } // getCreatedByName
    
    /**
     * Set value of created_by_name field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByName($value) {
      return $this->setFieldValue('created_by_name', $value);
    } // setCreatedByName

    /**
     * Return value of created_by_email field
     *
     * @return string
     */
    function getCreatedByEmail() {
      return $this->getFieldValue('created_by_email');
    } // getCreatedByEmail
    
    /**
     * Set value of created_by_email field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByEmail($value) {
      return $this->setFieldValue('created_by_email', $value);
    } // setCreatedByEmail

    /**
     * Return value of sent_on field
     *
     * @return DateTimeValue
     */
    function getSentOn() {
      return $this->getFieldValue('sent_on');
    } // getSentOn
    
    /**
     * Set value of sent_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setSentOn($value) {
      return $this->setFieldValue('sent_on', $value);
    } // setSentOn

    /**
     * Return value of sent_by_id field
     *
     * @return integer
     */
    function getSentById() {
      return $this->getFieldValue('sent_by_id');
    } // getSentById
    
    /**
     * Set value of sent_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setSentById($value) {
      return $this->setFieldValue('sent_by_id', $value);
    } // setSentById

    /**
     * Return value of sent_by_name field
     *
     * @return string
     */
    function getSentByName() {
      return $this->getFieldValue('sent_by_name');
    } // getSentByName
    
    /**
     * Set value of sent_by_name field
     *
     * @param string $value
     * @return string
     */
    function setSentByName($value) {
      return $this->setFieldValue('sent_by_name', $value);
    } // setSentByName

    /**
     * Return value of sent_by_email field
     *
     * @return string
     */
    function getSentByEmail() {
      return $this->getFieldValue('sent_by_email');
    } // getSentByEmail
    
    /**
     * Set value of sent_by_email field
     *
     * @param string $value
     * @return string
     */
    function setSentByEmail($value) {
      return $this->setFieldValue('sent_by_email', $value);
    } // setSentByEmail

    /**
     * Return value of recipient_id field
     *
     * @return integer
     */
    function getRecipientId() {
      return $this->getFieldValue('recipient_id');
    } // getRecipientId

    /**
     * Set value of recipient_id field
     *
     * @param integer $value
     * @return integer
     */
    function setRecipientId($value) {
      return $this->setFieldValue('recipient_id', $value);
    } // setRecipientId

    /**
     * Return value of recipient_name field
     *
     * @return string
     */
    function getRecipientName() {
      return $this->getFieldValue('recipient_name');
    } // getRecipientName

    /**
     * Set value of recipient_name field
     *
     * @param string $value
     * @return string
     */
    function setRecipientName($value) {
      return $this->setFieldValue('recipient_name', $value);
    } // setRecipientName

    /**
     * Return value of recipient_email field
     *
     * @return string
     */
    function getRecipientEmail() {
      return $this->getFieldValue('recipient_email');
    } // getRecipientEmail

    /**
     * Set value of recipient_email field
     *
     * @param string $value
     * @return string
     */
    function setRecipientEmail($value) {
      return $this->setFieldValue('recipient_email', $value);
    } // setRecipientEmail

    /**
     * Return value of sent_to_id field
     *
     * @return integer
     */
    function getSentToId() {
      return $this->getFieldValue('sent_to_id');
    } // getSentToId

    /**
     * Return value of sent_to_name field
     *
     * @return string
     */
    function getSentToName() {
      return $this->getFieldValue('sent_to_name');
    } // getSentToName

    /**
     * Set value of sent_to_name field
     *
     * @param string $value
     * @return string
     */
    function setSentToName($value) {
      return $this->setFieldValue('sent_to_name', $value);
    } // setSentToName

    /**
     * Return value of sent_to_email field
     *
     * @return string
     */
    function getSentToEmail() {
      return $this->getFieldValue('sent_to_email');
    } // getSentToEmail

    /**
     * Set value of sent_to_email field
     *
     * @param string $value
     * @return string
     */
    function setSentToEmail($value) {
      return $this->setFieldValue('sent_to_email', $value);
    } // setSentToEmail

    /**
     * Set value of sent_to_id field
     *
     * @param integer $value
     * @return integer
     */
    function setSentToId($value) {
      return $this->setFieldValue('sent_to_id', $value);
    } // setSentToId

    /**
     * Return value of closed_on field
     *
     * @return DateTimeValue
     */
    function getClosedOn() {
      return $this->getFieldValue('closed_on');
    } // getClosedOn
    
    /**
     * Set value of closed_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setClosedOn($value) {
      return $this->setFieldValue('closed_on', $value);
    } // setClosedOn

    /**
     * Return value of closed_by_id field
     *
     * @return integer
     */
    function getClosedById() {
      return $this->getFieldValue('closed_by_id');
    } // getClosedById
    
    /**
     * Set value of closed_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setClosedById($value) {
      return $this->setFieldValue('closed_by_id', $value);
    } // setClosedById

    /**
     * Return value of closed_by_name field
     *
     * @return string
     */
    function getClosedByName() {
      return $this->getFieldValue('closed_by_name');
    } // getClosedByName
    
    /**
     * Set value of closed_by_name field
     *
     * @param string $value
     * @return string
     */
    function setClosedByName($value) {
      return $this->setFieldValue('closed_by_name', $value);
    } // setClosedByName

    /**
     * Return value of closed_by_email field
     *
     * @return string
     */
    function getClosedByEmail() {
      return $this->getFieldValue('closed_by_email');
    } // getClosedByEmail
    
    /**
     * Set value of closed_by_email field
     *
     * @param string $value
     * @return string
     */
    function setClosedByEmail($value) {
      return $this->setFieldValue('closed_by_email', $value);
    } // setClosedByEmail

    /**
     * Return value of is_locked field
     *
     * @return boolean
     */
    function getIsLocked() {
      return $this->getFieldValue('is_locked');
    } // getIsLocked
    
    /**
     * Set value of is_locked field
     *
     * @param boolean $value
     * @return boolean
     */
    function setIsLocked($value) {
      return $this->setFieldValue('is_locked', $value);
    } // setIsLocked

    /**
     * Return value of last_comment_on field
     *
     * @return DateTimeValue
     */
    function getLastCommentOn() {
      return $this->getFieldValue('last_comment_on');
    } // getLastCommentOn
    
    /**
     * Set value of last_comment_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setLastCommentOn($value) {
      return $this->setFieldValue('last_comment_on', $value);
    } // setLastCommentOn

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'public_id':
          return parent::setFieldValue($real_name, strval($value));
        case 'based_on_type':
          return parent::setFieldValue($real_name, strval($value));
        case 'based_on_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'company_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'company_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'company_address':
          return parent::setFieldValue($real_name, strval($value));
        case 'currency_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'language_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'name':
          return parent::setFieldValue($real_name, strval($value));
        case 'note':
          return parent::setFieldValue($real_name, strval($value));
        case 'private_note':
          return parent::setFieldValue($real_name, strval($value));
        case 'status':
          return parent::setFieldValue($real_name, intval($value));
        case 'created_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'created_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'created_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'sent_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'sent_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'sent_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'sent_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'recipient_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'recipient_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'recipient_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'sent_to_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'sent_to_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'sent_to_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'closed_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'closed_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'closed_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'closed_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'is_locked':
          return parent::setFieldValue($real_name, boolval($value));
        case 'last_comment_on':
          return parent::setFieldValue($real_name, datetimeval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

