<?php

  /**
   * BaseTaxRate class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseTaxRate extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'tax_rates';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'name', 'percentage');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @param void
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of name field
     *
     * @param void
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of percentage field
     *
     * @param void
     * @return float
     */
    function getPercentage() {
      return $this->getFieldValue('percentage');
    } // getPercentage
    
    /**
     * Set value of percentage field
     *
     * @param float $value
     * @return float
     */
    function setPercentage($value) {
      return $this->setFieldValue('percentage', $value);
    } // setPercentage

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'name':
          return parent::setFieldValue($real_name, strval($value));
        case 'percentage':
          return parent::setFieldValue($real_name, floatval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

?>