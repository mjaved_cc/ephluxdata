<?php

  /**
   * Model options for invoicing module
   *
   * @package activeCollab.modules.invoicing
   * @subpackage resources
   */
  
  $this->setTableOptions(array(
    'invoices', 
    'invoice_items', 
    'invoice_payments', 
    'tax_rates', 
    'invoice_item_templates', 
    'invoice_note_templates'
  ), array('module' => 'invoicing', 'object_extends' => 'ApplicationObject'));
  
  // Item templates
  $this->setTableOptions('invoice_item_templates', array('module' => 'invoicing', 'object_extends' => 'ApplicationObject', 'order_by' => 'ISNULL(position) ASC, position, id ASC'));
  
  // Note templates
  $this->setTableOptions('invoice_note_templates', array('module' => 'invoicing', 'object_extends' => 'ApplicationObject', 'order_by' => 'ISNULL(position) ASC, position, id ASC'));
  
  // Tax Rates
  $this->setTableOptions('tax_rates', array('module' => 'invoicing', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));

  // Recurring profile and items
  $this->setTableOptions(array('recurring_profiles'), array('module' => 'invoicing', 'object_extends' => 'ApplicationObject'));
  
  $this->setTableOptions('recurring_profile_items', array('module' => 'invoicing', 'object_extends' => 'ApplicationObject', 'order_by' => 'ISNULL(position) ASC, position, id ASC'));
  //$this->setTableOptions(array('recurring_approval_requests'), array('module' => 'invoicing', 'object_extends' => 'ApplicationObject'));
  
  //Quotes
  $this->setTableOptions('quotes', array('module' => 'invoicing', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));
  