<?php

  /**
   * Render select tax rate box
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_tax_rate($params, &$smarty) {
    $value = array_var($params, 'value', null, true);
    $optional = (boolean) array_var($params, 'optional', false, true);
    $name = array_required_var($params, 'name', true);
     
    $options = array();
    if($optional) {
      $options[] = '-- No Tax --';
    } // if

    $tax_rates = TaxRates::find();
    foreach($tax_rates as $tax_rate) {
      $selected = $tax_rate->getId() == $value ? true : null;
      $options[$tax_rate->getId()] = $tax_rate->getName() . ' (' . $tax_rate->getPercentage() . ')';
    } // foreach

    return  HTML::selectFromPossibilities($name, $options, $selected, $params);
    //return select_box($options, $params);
  } // smarty_function_select_tax_rate

?>