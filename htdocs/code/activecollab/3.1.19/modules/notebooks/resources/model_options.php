<?php

  /**
   * Model generator options for notebooks module
   *
   * @package activeCollab.modules.notebooks
   * @subpackage resources
   */
  
  $this->setTableOptions(array('notebook_pages', 'notebook_page_versions'), array('module' => 'notebooks', 'object_extends' => 'ApplicationObject'));