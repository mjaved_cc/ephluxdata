<?php

  /**
   * Notebooks module on_get_project_object_types event handler
   *
   * @package activeCollab.modules.notebooks
   * @subpackage handlers
   */
  
  /**
   * Return notebooks module project object types
   *
   * @param void
   * @return string
   */
  function notebooks_handle_on_get_project_object_types() {
    return 'notebook';
  } // notebooks_handle_on_get_project_object_types

?>