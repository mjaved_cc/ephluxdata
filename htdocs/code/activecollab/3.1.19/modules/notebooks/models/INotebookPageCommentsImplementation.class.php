<?php

  /**
   * Notebook pages specific comments implementation
   *
   * @package activeCollab.modules.notebooks
   * @subpackage models
   */
  class INotebookPageCommentsImplementation extends ICommentsImplementation {
    
    /**
     * Construct notebook page subscriptions implementation
     *
     * @param IComments $object
     */
    function __construct(IComments $object) {
      if($object instanceof NotebookPage) {
        parent::__construct($object);
      } else {
        throw new InvalidInstanceError('object', $object, 'NotebookPage');
      } // if
    } // __construct
    
    /**
     * Create a new comment instance
     *
     * @return NotebookPageComment
     */
    function newComment() {
      $comment = new NotebookPageComment();
      $comment->setParent($this->object);
      
      return $comment;
    } // newComment
    
  }