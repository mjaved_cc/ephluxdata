<?php

  /**
   * Todo lists module on_get_project_object_types event handler
   *
   * @package activeCollab.modules.todo
   * @subpackage handlers
   */
  
  /**
   * Return todo lists module project object types
   *
   * @return string
   */
  function todo_handle_on_get_project_object_types() {
    return 'TodoList';
  } // todo_handle_on_get_project_object_types

?>