<script type="text/javascript">
  App.widgets.FlyoutDialog.front().setAutoSize(false);
</script>

<div class="big_form_wrapper two_form_sidebars">
  <div class="main_form_column">
    {wrap field=url}
      {text_field name="bookmark[bookmark_url]" value=$bookmark_data.bookmark_url class='title required validate_minlength 3' label='Bookmark URL'}
    {/wrap}
    
    {wrap field=name}
      {text_field name="bookmark[name]" value=$bookmark_data.name id=youtube_asset_title class='title required validate_minlength 3' label='Bookmark Title' maxlength="150"}
    {/wrap}
    
    {wrap_editor field=body}
      {label}Description{/label}
      {editor_field name="bookmark[body]" id=textDocumentBody inline_attachments=$bookmark_data.inline_attachments}{$bookmark_data.body nofilter}{/editor_field}
    {/wrap_editor}
  </div>
  
  <div class="form_sidebar form_first_sidebar">
    {wrap field=parent_id}
      {label for=textDocumentCategory}Category{/label}
      {select_asset_category name="bookmark[category_id]" value=$bookmark_data.category_id id=textDocumentCategory parent=$active_project user=$logged_user success_event="category_created"}
    {/wrap}
    
    {if $logged_user->canSeeMilestones($active_project)}
      {wrap field=milestone_id}
        {label for=textDocumentMilestone}Milestone{/label}
        {select_milestone name="bookmark[milestone_id]" value=$bookmark_data.milestone_id project=$active_project id=textDocumentMilestone user=$logged_user}
      {/wrap}
    {/if}

    {if $logged_user->canSeePrivate()}
      {wrap field=visibility}
        {label for=textDocumentVisibility}Visibility{/label}
        {select_visibility name="bookmark[visibility]" value=$bookmark_data.visibility short_description=true}
      {/wrap}
    {else}
      <input type="hidden" name="bookmark[visibility]" value="1" />
    {/if}
  </div>
  
  <div class="form_sidebar form_second_sidebar">
  {if $active_asset->isNew()}
    {wrap field=notify_users}
      {select_subscribers name="notify_users" exclude=$bookmark_data.exclude_ids object=$active_asset user=$logged_user label='Notify People'}
    {/wrap}
  {/if}
  </div>
</div>