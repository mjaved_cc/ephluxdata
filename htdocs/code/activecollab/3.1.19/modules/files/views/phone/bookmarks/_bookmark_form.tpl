{wrap field=url}
  {url_field name="bookmark[bookmark_url]" value=$bookmark_data.bookmark_url id=bookmark_url label='Bookmark URL' required=true}
{/wrap}

{wrap field=name}
  {text_field name="bookmark[name]" value=$bookmark_data.name id=bookmark_name label='Bookmark Title' required=true}
{/wrap}

{wrap_editor field=body}
	{editor_field name='bookmark[body]' label='Body' id=bookmark_body}{$bookmark_data.body nofilter}{/editor_field}
{/wrap_editor}

{wrap field=category_id}
  {select_asset_category name="bookmark[category_id]" value=$bookmark_data.category_id parent=$active_project user=$logged_user label='Category' id=bookmark_category}
{/wrap}

{if $logged_user->canSeeMilestones($active_project)}
  {wrap field=milestone_id}
    {select_milestone name="bookmark[milestone_id]" value=$bookmark_data.milestone_id project=$active_project user=$logged_user label='Milestone' id=bookmark_milestone}
  {/wrap}
{/if}

{if $logged_user->canSeePrivate()}
  {wrap field=visibility}
    {select_visibility name="bookmark[visibility]" value=$bookmark_data.visibility label='Visibility' id=bookmark_visibility}
  {/wrap}
{else}
  <input type="hidden" name="bookmark[visibility]" value="1" />
{/if}

{if $active_asset->isNew()}
  {wrap field=notify_users}
    {select_subscribers name="notify_users[]" object=$active_asset exclude=$bookmark_data.exclude_ids user=$logged_user label='Notify People' id=bookmark_notify_users}
  {/wrap}
{/if}

<script type="text/javascript">
	$(document).ready(function() {
		App.Wireframe.SelectBox.init();
	});
</script>