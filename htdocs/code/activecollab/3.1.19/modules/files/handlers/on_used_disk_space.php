<?php

  /**
   * on_used_disk_space event handler implementation
   *
   * @package activeCollab.modules.files
   * @subpackage handlers
   */

  /**
   * Handle on_used_disk_space event
   *
   * @param integer $used_disk_space
   */
  function files_handle_on_used_disk_space(&$used_disk_space) {
    $project_objects_table = TABLE_PREFIX . 'project_objects';

    $file_ids = DB::executeFirstColumn("SELECT id FROM $project_objects_table WHERE type = 'File' AND state > ?", STATE_DELETED);

    if($file_ids) {
      $used_disk_space += (integer) DB::executeFirstCell("SELECT SUM(integer_field_2) FROM $project_objects_table WHERE id IN (?)", $file_ids);
      $used_disk_space += (integer) DB::executeFirstCell("SELECT SUM(size) FROM " . TABLE_PREFIX . "file_versions WHERE file_id IN (?)", $file_ids);
    } // if
  } // files_handle_on_used_disk_space