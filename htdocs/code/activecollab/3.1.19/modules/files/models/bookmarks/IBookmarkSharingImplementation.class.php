<?php

  /**
   * Bookmarks sharing implementation
   * 
   * @package activeCollab.modules.files
   * @subpackage models
   */
  class IBookmarkSharingImplementation extends IProjectAssetSharingImplementation {
  
    /**
     * Return sharing context
     * 
     * @return string
     */
    function getSharingContext() {
      return ProjectAssets::BOOKMAKRS_SHARING_CONTEXT;
    } // getSharingContext
    
    /**
     * Returns true if this implementation has body text to display
     * 
     * @return boolean
     */
    function hasSharedBody() {
      return true;
    } // hasSharedBody
    
    /**
     * Return prepared shared body
     * 
     * @return string
     */
    function getSharedBody() {
      $result = '<div class="shared_bookmark">';
      $result.=   '<div class="real_preview">';
      $result.=     $this->object->preview()->renderLarge();
      $result.=   '</div>';
      $result.=   '<div class="object_body_content formatted_content">';
      if ($this->object->getBody()) {
        $result .= HTML::toRichText($this->object->getBody());
      } // if
      $result.=   '</div>';
      $result.= '</div>';
      return $result;
    } // getSharedBody
    
  }