<?php

  /**
   * YouTube video added to assets section of the project
   *
   * @package activeCollab.modules.files
   * @subpackage models
   */
  class YouTubeVideo extends ProjectAsset implements IPreview, ISharing {
    
    /**
     * Field map
     *
     * @var array
     */
    var $field_map = array(
      'video_url' => 'varchar_field_1'
    );
    
    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------
    
    /**
     * Get video_id
     *
     * @return string
     */
    function getVideoUrl() {
      return $this->getVarcharField1();
    } // getVideoId
    
    /**
     * Set video_id value
     *
     * @param string $value
     */
    function setVideoUrl($value) {
      return $this->setVarcharField1($value);
    } // setVideoId
    
    /**
     * Get video_id
     *
     * @return string
     */
    function getVideoId() {
    	return get_youtube_video_id($this->getVideoUrl());
    } // getVideoId
      	        
    // ---------------------------------------------------
    //  URL-s
    // ---------------------------------------------------
    
    /**
     * Return view asset URL
     *
     * @return string
     */
    function getViewUrl() {
      return Router::assemble('project_assets_you_tube_video', array(
        'project_slug' => $this->getProject()->getSlug(),
        'asset_id' => $this->getId(),
      ));
    } // getViewUrl
    
    /**
     * Return icon URL
     *
     * @return string
     */
    function getIconUrl() {
      return AngieApplication::getImageUrl('icons/32x32/youtube-video.png', FILES_MODULE, AngieApplication::INTERFACE_PHONE);
    } // getIconUrl
    
    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------
    
    /**
     * Preview helper instance
     *
     * @var IPreviewImplementation
     */
    protected $preview = false;
    
    /**
     * Return preview helper
     *
     * @return IDownloadPreviewImplementation
     */
    function preview() {
      if($this->preview === false) {
        $this->preview = new IYoutubePreviewImplementation($this);
      } // if
      
      return $this->preview;
    } // preview

    /**
     * Cached sharing implementation helper
     *
     * @var IYoutubeVideoSharingImplementation
     */
    private $sharing = false;

    /**
     * Return sharing helper instance
     *
     * @return IYoutubeVideoSharingImplementation
     */
    function sharing() {
      if($this->sharing === false) {
        $this->sharing = new IYoutubeVideoSharingImplementation($this);
      } // if

      return $this->sharing;
    } // sharing
    
    /**
     * Return history helper instance
     * 
     * @return IHistoryImplementation
     */
    function history() {
      return parent::history()->alsoTrackFields('varchar_field_1');
    } // history
    
    // ---------------------------------------------------
    //  Utils
    // ---------------------------------------------------
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $result['youtube_url'] = $this->getVideoUrl(); 
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      $result = parent::describeForApi($user, $detailed);

      $result['youtube_url'] = $this->getVideoUrl();

      return $result;
    } // describeForApi
    
    /**
     * Validate before save
     *
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->validatePresenceOf('video_url', 3)) {
        $errors->addError(lang('Youtube URL is required'), 'video_url');
      } // if
      
      if (!is_valid_youtube_url($this->getFieldValue('video_url'))) {
      	$errors->addError(lang('Youtube URL is not valid'), 'video_url');
      } // if
      
      parent::validate($errors, true);
    } // validate
    
  }