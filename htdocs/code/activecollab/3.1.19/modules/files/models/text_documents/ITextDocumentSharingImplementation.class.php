<?php

/**
 * Text documents sharing implementation
 *
 * @package activeCollab.modules.files
 * @subpackage models
 */
class ITextDocumentSharingImplementation extends IProjectAssetSharingImplementation {

  /**
   * Return sharing context
   *
   * @return string
   */
  function getSharingContext() {
    return ProjectAssets::DOCUMENTS_SHARING_CONTEXT;
  } // getSharingContext

  /**
   * Returns true if this implementation has body text to display
   *
   * @return boolean
   */
  function hasSharedBody() {
    return true;
  } // hasSharedBody

  /**
   * Return prepared shared body
   *
   * @return string
   */
  function getSharedBody() {
    $result = '<div class="shared_text_document">';
    $result.= HTML::toRichText($this->object->getBody());
    $result.= '</div>';

    return $result;
  } // getSharedBody

}