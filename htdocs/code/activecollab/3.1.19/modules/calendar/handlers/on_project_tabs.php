<?php

  /**
   * Calendar on_project_tabs handler
   *
   * @package activeCollab.modules.calendar
   * @subpackage handlers
   */
  
  /**
   * Handle on project tabs event
   *
   * @param NamedList $tabs
   * @param User $logged_user
   * @param Project $project
   * @param array $tabs_settings
   * @param string $interface
   */
  function calendar_handle_on_project_tabs(&$tabs, &$logged_user, &$project, &$tabs_settings, $interface) {
    if($interface == AngieApplication::INTERFACE_DEFAULT && in_array('calendar', $tabs_settings)) {
      $tabs->add('calendar', array(
        'text' => lang('Calendar'),
        'url' => Calendar::getProjectCalendarUrl($project),
        'icon' => $interface == AngieApplication::INTERFACE_DEFAULT ? 
        	AngieApplication::getImageUrl('icons/16x16/calendar-tab-icon.png', CALENDAR_MODULE) : 
        	AngieApplication::getImageUrl('icons/listviews/calendar.png', CALENDAR_MODULE, AngieApplication::INTERFACE_PHONE)
      ));
    } // if
  } // calendar_handle_on_project_tabs