<?php

  /**
   * monthly calendar helper
   *
   * @package activeCollab.modules.calendar
   * @subpackage helpers
   */
  
  /**
   * Render monthly calendar
   *
   * Params:
   *
   * - calendar - calendar render
   * - current_month - current month
   * - current_year - current year
   * - user	- user
   * - first_week_day - Value for first day in week from settings
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_monthly_calendar($params, &$smarty) {
  	$date = array_var($params, 'date');
  	$calendar = array_var($params, 'calendar');
  	
  	$month = $date->getMonth();
  	$year = $date->getYear();
  	
    // next month
		$next_month = $month + 1;
		$next_year = $year;
		if ($next_month == 13) {
			$next_month = 1;
			$next_year ++;
		} // if

		//previous month
		$previous_month = $month - 1;
		$previous_year = $year;
		if ($previous_month == 0) {
			$previous_month = 12;
			$previous_year--;
		} // if
		
		if ($calendar instanceof ProfileCalendarGenerator) {
	  	$user = array_var($params, 'user');
			$previous_month_url = Calendar::getProfileMonthUrl($user, $previous_year, $previous_month);
			$next_month_url = Calendar::getProfileMonthUrl($user, $next_year, $next_month);
		} else if ($calendar instanceof ProjectCalendarGenerator) {
			$project = array_var($params, 'project');
			$previous_month_url = Calendar::getProjectMonthUrl($project, $previous_year, $previous_month);
			$next_month_url = Calendar::getProjectMonthUrl($project, $next_year, $next_month);
		} else {
			$previous_month_url = Calendar::getDashboardMonthUrl($previous_year, $previous_month);
			$next_month_url = Calendar::getDashboardMonthUrl($next_year, $next_month);
		} // if

    $view = $smarty->createTemplate(AngieApplication::getViewPath('_monthly_calendar', null, CALENDAR_MODULE, AngieApplication::INTERFACE_DEFAULT));
		
		$view->assign(array(
      'calendar' => $calendar,
			'year' => $year,
			'month_string' => strftime("%B", $date->getTimestamp()),
			'previous_month_url' => $previous_month_url,
			'next_month_url' => $next_month_url,
		));

    return $view->fetch();
  } // smarty_function_monthly_calendar