<?php

  /**
   * Calendar module initialization file
   * 
   * @package activeCollab.modules.calendar
   */
  
  define('CALENDAR_MODULE', 'calendar');
  define('CALENDAR_MODULE_PATH', APPLICATION_PATH . '/modules/calendar');
  
  AngieApplication::setForAutoload('Calendar', CALENDAR_MODULE_PATH . '/models/Calendar.class.php');
  
  /**
   * Returns true if $user can see $profile calendar
   *
   * @param User $user
   * @return boolean
   */
  function can_access_profile_calendar($user) {
    return $user->isProjectManager();
  } // can_access_profile_calendar