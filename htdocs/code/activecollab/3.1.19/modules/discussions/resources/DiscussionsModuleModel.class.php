<?php

  // Include applicaiton specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

  /**
   * Discussions module model
   * 
   * @package activeCollab.modules.discussions
   * @subpackage models
   */
  class DiscussionsModuleModel extends ActiveCollabModuleModel {
  
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
      $this->addConfigOption('discussion_categories', array('General'));
      
      parent::loadInitialData($environment);
    } // loadInitialData
    
  }