<div class="object_time_and_expenses" id="object_time_and_expenses_for_{$active_tracking_object->getId()}" add_time_url="{$active_tracking_object->tracking()->getAddTimeUrl()}" add_expense_url="{$active_tracking_object->tracking()->getAddExpenseUrl()}" time_icon_url="{image_url name='icons/16x16/time-record.png' module=$smarty.const.TRACKING_MODULE}" expense_icon_url="{image_url name='icons/16x16/expense.png' module=$smarty.const.TRACKING_MODULE}">
  <div class="object_inspector_wrapper">
    
    <ul class="object_tracking_totals">
    
      <li class="object_tracking_totals_estimated_time">
        <span class="label">{lang}Estimated Time{/lang}</span>
        <span class="value">{object_estimate object=$active_tracking_object user=$logged_user}</span>
      </li>
      
      <li class="object_tracking_totals_tracked_time">
        <span class="label">{lang}Logged Time{/lang}</span>
        <span class="value object_total_time"></span>      
      </li>
      
      <li class="object_tracking_totals_tracked_expenses">
        <span class="label">{lang}Logged Expenses{/lang}</span>
        <span class="value object_total_expenses"></span>      
      </li>
      
    </ul>

  </div>
{if $can_add}
  <form action="{$active_tracking_object->tracking()->getAddTimeUrl()}" method="post">
{/if}
    <div class="table_wrapper">
    <table class="common" cellspacing="0">
      <thead>
        <tr>
          <th class="type">{lang}Type{/lang}</th>
          <th class="user">{lang}User{/lang}</th>
          <th class="job_type">{lang}Job{/lang} / {lang}Categ.{/lang}</th>
          <th class="record_date">{lang}Day{/lang}</th>
          <th class="value right">{lang}Value{/lang}</th>
          <th class="summary">{lang}Summary{/lang}</th>
          <th class="status">{lang}Status{/lang}</th>
          <th class="options"></th>
        </tr>
      </thead>
      <tbody>
      {if $can_add}
        <tr class="add_time_or_expense highlighted">
          <td class="type">
            <span class="tracking_type_toggler"></span>
          </td>
          <td class="user">
            {if $can_track_for_others}
              {select_project_user name='item[user_id]' value=$logged_user->getId() project=$active_tracking_object->getProject() user=$logged_user optional=false short=true}
            {else}
              {user_link user=$logged_user short=true}
              <input type="hidden" name="item[user_id]" value="{$logged_user->getId()}" />
            {/if}
          </td>
          <td class="job_type">
            {select_job_type name='item[job_type_id]' class="timerecord_control"}
            {select_expense_category name='item[category_id]' class="expense_control"}
          </td>
          <td class="record_date">{select_date name='item[record_date]' value=DateTimeValue::now()}</td>
          <td class="value right"><input type="text" name="item[value]" /></td>
          <td class="summary"><input type="text" name="item[summary]"></td>
          <td class="status">
            <span class="billable_toggler"></span>
          </td>
          <td class="options"><button type="submit">{lang}Add{/lang}</button></td>
        </tr>
      {/if}
      
    {if $items}
      {foreach $items as $item}
        <tr class="item {if $item instanceof TimeRecord}time_record{else}expense{/if}" record_value="{$item->getValue()}">
        {if $item instanceof TimeRecord}
          <td class="type"><img src="{image_url name='icons/16x16/time-record.png' module=$smarty.const.TRACKING_MODULE}" title="{lang}Time Record{/lang}" /></td>
        {else}
          <td class="type"><img src="{image_url name='icons/16x16/expense.png' module=$smarty.const.TRACKING_MODULE}" title="{lang}Expense{/lang}" /></td>
        {/if}
          <td class="user">{user_link user=$item->getUser() short=yes}</td>
          <td class="job_type">
          {if $item instanceof TimeRecord}
            {$item->getJobTypeName()}
          {else}
          	{$item->getCategoryName()}
          {/if}
          </td>
          <td class="record_date">{$item->getRecordDate()|date:0}</td>
          <td class="value right">
          {if $item instanceof TimeRecord}
            {$item->getValue()|hours}h
          {else}
            {$item->getValue()|money}
          {/if}
          </td>
          <td class="summary">{$item->getSummary()}</td>
          <td class="status">{$item->getBillableVerboseStatus()}</td>
          <td class="options">
          {if $item->state()->canTrash($logged_user)}
            <a href="{$item->state()->getTrashUrl()}" title="{lang}Move to Trash{/lang}" class="trash"><img src="{image_url name="icons/12x12/move-to-trash.png" module=$smarty.const.SYSTEM_MODULE}" alt="" /></a>
          {/if}
          </td>
        </tr>
      {/foreach}
    {/if}
        <tr class="empty" style="display: none">
          <td colspan="7">{lang type=$active_tracking_object->getVerboseType(true)}There is no time logged for this :type{/lang}</td>
        </tr>
      </tbody>
    </table>
    </div>
{if $can_add}
  </form>
{/if}
</div>