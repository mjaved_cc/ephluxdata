{title}Expenses{/title}
{add_bread_crumb}Expenses{/add_bread_crumb}

{if is_foreachable($records)}
  <div id="expenses_bar_chart">{tracked_time_expenses_bar_chart records=$records}</div>
{else}
  <p class="empty_page"><span class="inner">{lang}There are no expenses{/lang}</span></p>
{/if}