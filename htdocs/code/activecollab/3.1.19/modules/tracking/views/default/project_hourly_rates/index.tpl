{title}Hourly Rates{/title}
{add_bread_crumb}Hourly Rates{/add_bread_crumb}

<div id="project_hourly_rates">
  <table class="common" cellspacing="0">
    <thead>
      <tr>
        <th class="name">{lang}Job Type{/lang}</th>
        <th class="hourly_rate">{lang}Hourly Rate{/lang}</th>
        <th class="options"></th>
      </tr>
    </thead>
    <tbody>
    {foreach $job_types as $job_type}
      <tr job_type_id="{$job_type->getId()}" class="job_type {if $job_type->hasCustomHourlyRateFor($active_project)}custom_hourly_rate{/if}">
        <td class="name">{$job_type->getName()}</td>
        <td class="hourly_rate">{$job_type->getHourlyRateFor($active_project)|money}</td>
        <td class="options">{link href=$job_type->getProjectHourlyRateUrl($active_project) class="edit_hourly_rate"}<img src="{image_url name="icons/12x12/edit.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}">{/link}</td>
      </tr>
    {/foreach}
    </tbody>
  </table>
</div>

<script type="text/javascript">
  $('#project_hourly_rates table td.options a.edit_hourly_rate').flyoutForm({
    'title' : App.lang('Hourly Rate'), 
    'success' : function(response) {
      if(typeof(response) == 'object') {
        var job_type_row = $('#project_hourly_rates table tbody tr[job_type_id=' + response['job_type_id'] + ']');

        if(response['custom_hourly_rate']) {
          job_type_row.find('td.hourly_rate').text(App.moneyFormat(response['custom_hourly_rate']));

          if(job_type_row.not('.custom_hourly_rate')) {
            job_type_row.addClass('custom_hourly_rate');
          } // if
        } else {
          job_type_row.find('td.hourly_rate').text(App.moneyFormat(response['default_hourly_rate']));

          if(job_type_row.is('.custom_hourly_rate')) {
            job_type_row.removeClass('custom_hourly_rate');
          } // if
        } // if

        job_type_row.highlightFade();
      } // if
    }
  });
</script>