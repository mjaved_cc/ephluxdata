<?php

  /**
   * JobTypes class
   *
   * @package activeCollab.modules.tracking
   * @subpackage models
   */
  class JobTypes extends BaseJobTypes {
    
    /**
  	 * Return types slice based on given criteria
  	 * 
  	 * @param integer $num
  	 * @param array $exclude
  	 * @param integer $timestamp
  	 * @return DBResult
  	 */
  	function getSlice($num = 10, $exclude = null, $timestamp = null) {
  		if($exclude) {
  			return JobTypes::find(array(
  			  'conditions' => array("id NOT IN (?)", $exclude), 
  			  'order' => 'name', 
  			  'limit' => $num,  
  			));
  		} else {
  			return JobTypes::find(array(
  			  'order' => 'name', 
  			  'limit' => $num,  
  			));
  		} // if
  	} // getSlice
    
    /**
     * Cached ID name map
     *
     * @var array
     */
    static private $id_name_map = false;
    
    /**
     * Return map of defined job types indexed by ID
     * 
     * @return array
     */
    static function getIdNameMap() {
      if(self::$id_name_map === false) {
        $rows = DB::execute('SELECT id, name FROM ' . TABLE_PREFIX . 'job_types ORDER BY name');
        
        if($rows) {
          self::$id_name_map = array();
          
          foreach($rows as $row) {
            self::$id_name_map[(integer) $row['id']] = trim($row['name']);
          } // foreach
        } else {
          self::$id_name_map = null;
        } // if
      } // if
      
      return self::$id_name_map;
    } // getIdNameMap
    
    /**
     * Return array of hourly rates for given project
     * 
     * @param Project $project
     * @return array
     */
    static function getIdRateMapFor(Project $project) {
      $result = array();
      
      $rows = DB::execute('SELECT id, default_hourly_rate FROM ' . TABLE_PREFIX . 'job_types ORDER BY name');
      if($rows) {
        foreach($rows as $row) {
          $result[(integer) $row['id']] = (float) $row['default_hourly_rate'];
        } // foreach
        
        $rows = DB::execute('SELECT job_type_id, hourly_rate FROM ' . TABLE_PREFIX . 'project_hourly_rates WHERE project_id = ?', $project->getId());
        if($rows) {
          foreach($rows as $row) {
            $result[(integer) $row['job_type_id']] = (float) $row['hourly_rate'];
          } // foreach
        } // if
      } // if
      
      return $result;
    } // getIdRateMapFor
    
    /**
     * Return job type name by job type ID
     * 
     * @param integer $job_type_id
     * @return string
     */
    static function getNameById($job_type_id) {
      return array_var(self::getIdNameMap(), $job_type_id);
    } // getNameById
    
    /**
     * Default job type ID
     *
     * @var integer
     */
    static private $default_job_type_id = false;
    
    /**
     * Return ID of the default job type
     * 
     * @return integer
     */
    static function getDefaultJobTypeId() {
      if(self::$default_job_type_id === false) {
        self::$default_job_type_id = (integer) DB::executeFirstCell('SELECT id FROM ' . TABLE_PREFIX . 'job_types ORDER BY is_default DESC LIMIT 0, 1');
      } // if
      
      return self::$default_job_type_id;
    } // getDefaultJobTypeId
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if $user can define a new job type
     * 
     * @param IUser $user
     * @return boolean
     */
    static function canAdd(IUser $user) {
      return $user->isAdministrator();
    } // canAdd
    
    /**
     * Returns true if $user can manage hourly rates for $project
     * 
     * @param IUser $user
     * @param Project $project
     * @return boolean
     */
    static function canManageProjectHourlyRates(IUser $user, Project $project) {
      return $user instanceof User && $project->canEdit($user) && $user->canSeeProjectBudgets();
    } // canManageProjectHourlyRates
  
  }