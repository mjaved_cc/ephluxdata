<?php

  /**
   * GIT commits managament class
   * 
   * @package activeCollab.modules.source
   * @subpackage models
   */
  class GitCommits extends SourceCommits {
    
    /**
     * Find git commit by revision
     *
     * @param int $revision
     * @param GitRepository $source_repository
     * @return GitCommit
     */
    function findByRevision($revision, $source_repository) {
      return BaseSourceCommits::find(array(
        'conditions'  => array('`revision_number` = ? AND `repository_id` = ? AND `type` = ?', $revision, $source_repository->getId(), 'GitCommit'),
        'one'         => true
      ));
    } //findByRevision
    
    /**
     * Find all commits with $revision_ids ids in $repository
     *
     * @param array $revision_ids
     * @param GitRepository $source_repository
     * @return array of GitCommit
     */
    function findByRevisionIds($revision_ids, $source_repository) {
      return BaseSourceCommits::find(array(
        'conditions' => array('revision_number IN (?) AND repository_id = ? AND `type` = ?', $revision_ids, $source_repository->getId(), 'GitCommit'),
        'order'      => 'commited_on DESC, revision_number DESC',
      ));
    } //findByRevisionIds
    
    /**
     * Find last commit
     *
     * @param GitRepository $source_repository
     * @return GitCommit
     */
    function findLastCommit($source_repository) {
      return BaseSourceCommits::find(array(
         'conditions'  => array('repository_id = ? AND type = ?', $source_repository->getId(), 'GitCommit'),
         'order'       => 'revision_number DESC',
         'one'         => true
       ));
    } //findLastCommit
    
  } //GitCommits