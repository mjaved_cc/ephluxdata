<?php

  /**
   * SourcePaths class
   *
   * @package activeCollab.modules.source
   * @subpackage models
   */
  class SourcePaths extends BaseSourcePaths {
    
    protected $active_commit = null;
    
    
    /**
     * Get SourcePaths that belong to forwarded commit
     *
     * @param int $source_commit_id
     * @return SourcePath[]
     */
    static function getPathsForCommit($source_commit_id) {
      return parent::find(array(
        'conditions' => array("`commit_id` = ?", $source_commit_id),
      	'order'	=> 'action ASC'
      ));
    }//getPathsForCommit
    
    /**
     * Get SourcePaths for the path
     *
     * @param string $path
     * @return array
     */
    static function findSourcePathsForPath($path) {
      return parent::find(array(
        'conditions' => array("`path` = ?", $path),
      ));
    }//findSourcePathsForPath
    
  
  } //SourcePaths