<?php

  /**
   * select_project_requests_managers helper implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

  /**
   * Select project requests managers
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_project_requests_managers($params, &$smarty) {
    if(isset($params['class'])) {
      $params['class'] .= ' select_project_requests_managers';
    } else {
      $params['class'] = 'select_project_requests_managers';
    } // if
    
    if(!array_key_exists('inline', $params)) {
      $params['inline'] = true;
    } // if
    
    $role_ids = array();
    foreach(Roles::find() as $role) {
      if($role->getPermissionValue('can_manage_project_requests') || $role->isProjectManager() || $role->isAdministrator()) {
        $role_ids[] = $role->getId();
      } // if
    } // foreach
    
    if(count($role_ids)) {
      require_once AUTHENTICATION_FRAMEWORK_PATH . '/helpers/function.select_users.php';

      $users_table = TABLE_PREFIX.'users';
      $params['users'] = Users::getForSelectByConditions(DB::prepare("$users_table.role_id IN (?) AND $users_table.state >= ?", array($role_ids, STATE_VISIBLE)));
      
      return smarty_function_select_users($params, $smarty);
    } else {
      return '<span class="no_project_requests_managers">' . lang('There are no users with project requests management permissions') . '</span>';
    } // if
  } // smarty_function_select_project_requests_managers