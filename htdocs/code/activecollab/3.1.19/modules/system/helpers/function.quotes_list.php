<?php

  /**
   * List quotes helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render quotes list 
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_quotes_list($params, &$smarty) {
    $quotes = array_required_var($params, 'quotes');
    $user = array_var($params, 'user'); 
    
    $smarty->assign(array(
      '_quotes' => $quotes,
      '_user' => $user
    ));
    
    $interface = array_var($params, 'interface', AngieApplication::getPreferedInterface(), true);
    
    return $smarty->fetch(get_view_path('_quotes_list', 'project', SYSTEM_MODULE, $interface));
    
    
  } // smarty_function_quotes_list