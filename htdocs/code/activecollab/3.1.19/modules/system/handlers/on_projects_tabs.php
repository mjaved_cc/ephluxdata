<?php

  /**
   * System module on_projects_tabs event handler
   *
   * @package activeCollab.modules.system
   * @subpackage handlers
   */
  
  /**
   * Handle on prepare projects tabs event
   *
   * @param WireframeTabs $tabs
   * @param IUser $logged_user
   */
  function system_handle_on_projects_tabs(WireframeTabs &$tabs, IUser &$logged_user) {
	  if(ConfigOptions::getValue('project_requests_enabled')) {
	  	if($logged_user->isProjectManager() || $logged_user->getSystemPermission('can_manage_project_requests')) {
		  	$tabs->add('project_requests', lang('Requests'), Router::assemble('project_requests'));
	  	} // if
	  } // if
  } // system_handle_on_projects_tabs