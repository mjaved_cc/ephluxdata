<?php

  // Build on top of reports module
  AngieApplication::useController('reports', SYSTEM_MODULE);

  /**
   * Milestone filters controller
   *
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class MilestoneFiltersController extends ReportsController {

    /**
     * Selected milestone filter
     *
     * @var MilestoneFilter
     */
    protected $active_milestone_filter;

    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();

      $milestone_filter_id = $this->request->getId('milestone_filter_id');
      if($milestone_filter_id) {
        $this->active_milestone_filter = MilestoneFilters::findById($milestone_filter_id);

        if(!($this->active_milestone_filter instanceof MilestoneFilter)) {
          $this->response->notFound();
        } // if
      } else {
        $this->active_milestone_filter = new MilestoneFilter();
      } // if

      $this->response->assign('active_milestone_filter', $this->active_milestone_filter);
    } // __construct

    /**
     * Show tracking report form and options
     */
    function index() {

    } // index

    /**
     * View selected filter
     */
    function view() {
      if ($this->request->isApiCall()) {
        if ($this->active_milestone_filter->isLoaded()) {
          $this->response->respondWithData($this->active_milestone_filter, array('as' => 'milestone_filter'));
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // view

    /**
     * Run a given report
     */
    function run() {
      if ($this->request->isPrintCall() || ($this->request->isWebBrowser() && $this->request->isAsyncCall())) {
        try {
          $filter = new MilestoneFilter();
          $filter->setAttributes($this->request->get('filter'));

          $milestones = $filter->run($this->logged_user);
        } catch(MilestoneFilterConditionsError $e) {
          $milestones = null;
        } catch(Exception $e) {
          $this->response->exception($e);
        } // try

        if ($this->request->isPrintCall()) {
          $this->response->assign(array(
            'filter' => $filter,
            'milestones' => $milestones,
          ));
        } else {
          $this->response->respondWithData($milestones, array(
            'as' => 'filter_result'
          ));
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // run

    /**
     * Export result as CSV
     */
    function export() {
      if($this->request->isAsyncCall()) {
        try {
          $filter = new MilestoneFilter();
          $filter->setAttributes($this->request->get('filter'));

          $csv = $filter->runToCsv($this->logged_user);
        } catch(MilestoneFilterConditionsError $e) {
          $csv = null;
        } catch(Exception $e) {
          $this->response->exception($e);
        } // try

        if($csv) {
          $this->response->respondWithFileDownload($csv, BaseHttpResponse::CSV, 'filter-result.csv');
        } else {
          $this->response->respondWithContentDownload('', BaseHttpResponse::CSV, 'filter-result.csv');
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // export

    /**
     * Create new filter
     */
    function add() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if(MilestoneFilters::canAdd($this->logged_user)) {
          try {
            $this->active_milestone_filter = new MilestoneFilter();
            $this->active_milestone_filter->setAttributes($this->request->post('filter'));
            $this->active_milestone_filter->save();

            $this->response->respondWithData($this->active_milestone_filter, array('as' => 'milestone_filter'));
          } catch(Exception $e) {
            $this->response->exception($e);
          } // try
        } else {
          $this->response->forbidden();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // add

    /**
     * Update an existing filter
     */
    function edit() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if($this->active_milestone_filter->isLoaded()) {
          if($this->active_milestone_filter->canEdit($this->logged_user)) {
            try {
              $this->active_milestone_filter->setAttributes($this->request->post('filter'));
              $this->active_milestone_filter->save();

              $this->response->respondWithData($this->active_milestone_filter, array('as' => 'milestone_filter'));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // edit

    /**
     * Drop an existing filter
     */
    function delete() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if($this->active_milestone_filter->isLoaded()) {
          if($this->active_milestone_filter->canDelete($this->logged_user)) {
            try {
              $this->active_milestone_filter->delete();
              $this->response->respondWithData($this->active_milestone_filter, array('as' => 'milestone_filter'));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // delete

  }