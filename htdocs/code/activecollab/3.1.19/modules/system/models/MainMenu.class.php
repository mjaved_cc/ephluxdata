<?php

  /**
   * Application level main menu implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class MainMenu extends FwMainMenu {

    /**
     * Load status bar items
     * 
     * @param IUser $user
     */
    function load(IUser $user) {
      if($this->isLoaded()) {
        return;
      } // if
      
      parent::load($user);
      
      $this->remove('users');
      
      $this->addAfter('people', lang('People'), Router::assemble('people'), AngieApplication::getImageUrl('main-menu/people.png', SYSTEM_MODULE), null, 'homepage');
  	  $this->addAfter('projects', lang('Projects'), Router::assemble('projects'), AngieApplication::getImageUrl('main-menu/projects.png', SYSTEM_MODULE), null, 'people');
    } // load
    
  }