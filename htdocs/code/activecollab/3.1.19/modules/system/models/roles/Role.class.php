<?php

  /**
   * Role class
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class Role extends FwRole implements IHomescreen {
  
    /**
     * Returns true if this role includes project managemenet permissions
     *
     * @return boolean
     */
    function isProjectManager() {
      return $this->getPermissionValue('can_manage_projects');
    } // isProjectManager
    
    /**
     * Returns true if this role includes people managemenet permissions
     *
     * @return boolean
     */
    function isPeopleManager() {
      return $this->getPermissionValue('can_manage_people');
    } // isPeopleManager
    
    /**
     * Returns true if users with this role can see private objects
     * 
     * @return boolean
     */
    function canSeePrivate() {
      return $this->isAdministrator() || $this->isProjectManager() || (boolean) $this->getPermissionValue('can_see_private_objects');
    } // canSeePrivate
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $result['is_project_manager'] = $this->isProjectManager();
      $result['is_people_manager'] = $this->isPeopleManager();
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      $result = parent::describeForApi($user, $detailed);

      $result['is_project_manager'] = $this->isProjectManager();
      $result['is_people_manager'] = $this->isPeopleManager();

      return $result;
    } // describeForApi
    
    // ---------------------------------------------------
    //  Interface implementations
    // ---------------------------------------------------
    
    /**
     * Homescreen helper instance
     *
     * @var IRoleHomescreenImplementation
     */
    private $homescreen = false;
    
    /**
     * Return homescreen helper instance
     * 
     * @return IRoleHomescreenImplementation
     */
    function homescreen() {
      if($this->homescreen === false) {
        $this->homescreen = new IRoleHomescreenImplementation($this);
      } // if
      
      return $this->homescreen;
    } // homescreen
    
  }