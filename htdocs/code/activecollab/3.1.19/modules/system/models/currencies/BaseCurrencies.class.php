<?php

  /**
   * BaseCurrencies class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  abstract class BaseCurrencies extends DataManager {
  
    /**
     * Do a SELECT query over database with specified arguments
     * 
     * This function can return single instance or array of instances that match 
     * requirements provided in $arguments associative array
     *
     * @param array $arguments Array of query arguments. Fields:
     * 
     *  - one        - select first row
     *  - conditions - additional conditions
     *  - order      - order by string
     *  - offset     - limit offset, valid only if limit is present
     *  - limit      - number of rows that need to be returned
     * 
     * @return mixed
     * @throws DBQueryError
     */
    function find($arguments = null) {
      if($arguments === null) {
        $arguments = array('order' => 'name');
      } else {
        if(!isset($arguments['order'])) {
          $arguments['order'] = 'name';
        } // if
      } // if      
      return parent::find($arguments, TABLE_PREFIX . 'currencies', DataManager::CLASS_NAME_FROM_TABLE, 'Currency', '');
    } // find
    
    /**
     * Return array of objects that match specific SQL
     *
     * @param string $sql
     * @param array $arguments
     * @param boolean $one
     * @return mixed
     */
    function findBySQL($sql, $arguments = null, $one = false) {
      return parent::findBySQL($sql, $arguments, $one, TABLE_PREFIX . 'currencies', DataManager::CLASS_NAME_FROM_TABLE, 'Currency', '');
    } // findBySQL
    
    /**
     * Return object by ID
     *
     * @param mixed $id
     * @return Currency
     */
    function findById($id) {
      return parent::findById($id, TABLE_PREFIX . 'currencies', DataManager::CLASS_NAME_FROM_TABLE, 'Currency', '');
    } // findById
    
    /**
     * Return paginated result
     * 
     * This function will return paginated result as array. First element of 
     * returned array is array of items that match the request. Second parameter 
     * is Pager class instance that holds pagination data (total pages, current 
     * and next page and so on)
     *
     * @param array $arguments
     * @param integer $page
     * @param integer $per_page
     * @return array
     * @throws DBQueryError
     */
    function paginate($arguments = null, $page = 1, $per_page = 10) {
      return parent::paginate($arguments, $page, $per_page, TABLE_PREFIX . 'currencies', DataManager::CLASS_NAME_FROM_TABLE, 'Currency', '');
    } // paginate
    
    /**
     * Return number of rows in this table
     *
     * @param string $conditions Query conditions
     * @return integer
     * @throws DBQueryError
     */
    function count($conditions = null) {
      return parent::count($conditions, TABLE_PREFIX . 'currencies');
    } // count
    
    /**
     * Update table
     * 
     * $updates is associative array where key is field name and value is new 
     * value
     *
     * @param array $updates
     * @param string $conditions
     * @return boolean
     * @throws DBQueryError
     */
    function update($updates, $conditions = null) {
      return parent::update($updates, $conditions, TABLE_PREFIX . 'currencies');
    } // update
    
    /**
     * Delete all rows that match given conditions
     *
     * @param string $conditions Query conditions
     * @param string $table_name
     * @return boolean
     * @throws DBQueryError
     */
    function delete($conditions = null) {
      return parent::delete($conditions, TABLE_PREFIX . 'currencies');
    } // delete
  
  }

