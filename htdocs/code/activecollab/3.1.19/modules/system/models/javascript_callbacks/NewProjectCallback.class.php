<?php

  /**
   * New project callback implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class NewProjectCallback extends JavaScriptCallback {
    
    /**
     * Render callback function code
     * 
     * @return string
     */
    function render() {
      return '(function () { $(this).newProjectFlyout(); })';
    } // render
    
  }