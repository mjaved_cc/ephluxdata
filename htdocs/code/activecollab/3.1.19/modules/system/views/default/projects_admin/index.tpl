<div id="projects_admin">
  {form action=Router::assemble('admin_projects')}
    <div class="content_stack_wrapper">
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Tabs{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=project_tabs}
            {select_project_tabs name="settings[project_tabs]" value=$settings_data.project_tabs}
          {/wrap}
        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Assignment Delegation{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=clients_can_delegate_to_employees}
            {yes_no name="settings[clients_can_delegate_to_employees]" value=$settings_data.clients_can_delegate_to_employees label='Clients can Delegate Assignments to all Project Members'}
            <p class="aid">{lang}When this option is set to Yes, client can delegate assignments to all project members. Set this option to No to limit delegation list only to project members from client's company{/lang}.</p>
          {/wrap}
        </div>
      </div>
      
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Visibility{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=default_project_object_visibility}
            {select_visibility name="settings[default_project_object_visibility]" value=$settings_data.default_project_object_visibility label='Default Visibility'}
            <p class="aid">{lang}When creating new objects in projects, set their visibility to selected value by default{/lang}.</p>
          {/wrap}
        </div>
      </div>
      
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Templates{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=project_templates_category}
            {select_project_category name="settings[project_templates_category]" value=$settings_data.project_templates_category user=$logged_user optional=true label='Project Templates Category'}
            <p class="aid">{lang}Only treat projects from selected category as project templates. If no category is selected all projects will be treated as potential templates{/lang}.</p>
          {/wrap}
        </div>
      </div>
    </div>
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>