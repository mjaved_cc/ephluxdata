{title}New Company{/title}
{add_bread_crumb}New Company{/add_bread_crumb}

{form action=Router::assemble('people_companies_add') method=post}
  {include file=get_view_path('_company_form', 'companies', 'system')}

  {wrap_buttons}
    {submit}Add Company{/submit}
  {/wrap_buttons}
{/form}