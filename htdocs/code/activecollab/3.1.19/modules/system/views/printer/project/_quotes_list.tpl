<div class="project_list">
{if is_foreachable($_quotes)}
    <h2>{lang}Quotes{/lang}</h2>
     
        <table class="common" cellspacing="0">
          <thead>
            <tr>
              <th class="quotes">{lang}Quotes{/lang}</th>
              <th class="status">{lang}Status{/lang}</th>
              <th class="created_on">{lang}Created On{/lang}</th>
            </tr>
          </thead>
          <tbody>
          {foreach from=$_quotes item=object}
            <tr>
              <td class="quote_name">{$object->getName()}</td>
              <td class="status">{$object->getVerboseStatus()}</td>
              <td class="created">{$object->getCreatedOn()|date}</td>
            </tr>
          {/foreach}
          </tbody>
        </table>
     
  {/if}  
</div>