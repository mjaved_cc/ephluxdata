{title}Edit Category{/title}
{add_bread_crumb}Edit{/add_bread_crumb}

{form action=$active_document_category->getEditUrl() method=post}
{include file=get_view_path('_document_category_form', 'document_categories', 'documents')}
{wrap_buttons}
  {submit}Save Changes{/submit}
{/wrap_buttons}
{/form}