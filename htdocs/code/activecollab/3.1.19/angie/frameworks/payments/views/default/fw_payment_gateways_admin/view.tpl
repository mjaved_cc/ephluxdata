{title}{$payment_gateway->getName()}{/title}
{add_bread_crumb}View{/add_bread_crumb}

<div id="payment">
  <div class="content_stack_element">
    <div class="content_stack_element_info">
      <h3>{lang}Payment Gateway Details{/lang}</h3>
    </div>
    <div class="content_stack_element_body">
      <dt>{lang}Name{/lang}</dt>
      <dd>{$payment_gateway->getName()}</dd>
      <dt>{lang}Type{/lang}</dt>
      <dd>{$payment_gateway->getGatewayName()}</dd>
      
      {if $payment_gateway->getAPIUsername()}
      	<dt>{lang}API Username{/lang}</dt>
      	<dd>{$payment_gateway->getAPIUsername()}</dd>
      {/if}
      {if $payment_gateway->getAPISignature()}
      	<dt>{lang}API Signature{/lang}</dt>
      	<dd>{$payment_gateway->getAPISignature()}</dd>
      {/if}
      
      {if $payment_gateway->getApiLoginId()}
      	<dt>{lang}API Login Id{/lang}</dt>
      	<dd>{$payment_gateway->getApiLoginId()}</dd>
      {/if}
      
      {if $payment_gateway->getTransactionId()}
      	<dt>{lang}Transaction Id{/lang}</dt>
      	<dd>{$payment_gateway->getTransactionId()}</dd>
      {/if}
     
      <dt>{lang}Default Gateway{/lang}</dt>
      <dd>{if $payment_gateway->getIsDefault() == '0'}{lang}No{/lang}{else}{lang}Yes{/lang}{/if}</dd>
      
      <dt>{lang}Go Live{/lang}</dt>
      <dd>{if $payment_gateway->getGoLive() == '0'}{lang}No{/lang}{else}{lang}Yes{/lang}{/if}</dd>
    </div>
  </div>
 </div>