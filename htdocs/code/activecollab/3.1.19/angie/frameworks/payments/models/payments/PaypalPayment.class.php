<?php

/**
 * Paypal class used for paypal payments
 * 
 * @package angie.frameworks.payments
 * @subpackage models.payments
 * 
 */
abstract class PaypalPayment extends Payment {
  
  
    /**
     * Construct paypal payment object
     * 
     * @param array $response
     */
    function __construct($response = null, PaymentGateway $gateway = null) {
      
      if($response) {
       $this->response = $response;
        $this->parseResponse();
        if($gateway instanceof PaymentGateway) {
          $this->setGateway($gateway);
        }//if
      }//if
    }//__construct
    
  
  /**
   * Parse response from service
   * 
   */
  function parseResponse($response = null) {
    if($response) {
      //if returning from gateway 
      $this->response = $response;
    }//if
    if(strtoupper($this->response['ACK']) == 'SUCCESS' || strtoupper($this->response['ACK']) == 'SUCCESSWITHWARNING') {
      $this->setIsError(false);
      
    } else {
      $this->setIsError(true);
      $this->setErrorMessage(urldecode($this->response['L_LONGMESSAGE0']));
    }//if
    $this->setAdditionalProperties($this->response);
    $this->setPaidOn($this->response['TIMESTAMP']);
    
    
  }//parseResponse
  
  
  
  
}//PaypalPayment