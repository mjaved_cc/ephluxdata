<?php

/**
 * Paypal express checkout payment class
 * 
 * @package angie.frameworks.payments
 * @subpackage models.payments
 * 
 */
class PaypalExpressCheckoutPayment extends PaypalPayment {
  
  
  
  /**
   * Set token
   * 
   * @param $token
   */
  function setToken($token) {
    return $this->setAdditionalProperty('TOKEN',$token);
  }//setToken
  
  /**
   * Return token from additional parameters
   * 
   * @return string
   */
  function getToken() {
    return $this->getAdditionalProperty('TOKEN');
  }//getToken
  
  
}