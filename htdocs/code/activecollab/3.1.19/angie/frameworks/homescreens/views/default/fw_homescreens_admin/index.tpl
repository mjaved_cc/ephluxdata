{title}Home Screens{/title}
{add_bread_crumb}Overview{/add_bread_crumb}

<div id="homescreens_admin">
  <div id="homescreens_admin_default" class="homescreens_admin_section odd">
  	<h3>{lang}Default{/lang}</h3>
  	{homescreen_indicator homescreen=$default_homescreen}
  </div>
  
  <div id="homescreens_admin_roles" class="homescreens_admin_section even">
  	<h3>{lang}Role Specific{/lang}</h3>
   	{foreach $roles as $role}
      {homescreen_indicator parent=$role}
    {/foreach}
  </div>
  
  <div id="homescreens_admin_legend" class="homescreens_admin_section odd">
    <h3>{lang}Legend{/lang}</h3>
    
    <ul>
      <li id="homescreens_admin_legend_active">{lang}Roles that use their own home screen settings{/lang}.</li>
      <li id="homescreens_admin_legend_inactive">{lang}Roles that don't have their own configuration, but use Default Home Screen settings instead{/lang}.</li>
    </ul>
  </div>
</div>