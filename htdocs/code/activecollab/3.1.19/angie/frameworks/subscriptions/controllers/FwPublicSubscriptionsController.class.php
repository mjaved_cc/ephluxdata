<?php

  // Build on top of system module
  AngieApplication::useController('frontend', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

  /**
   * Framework level public subscriptions controller
   *
   * @package angie.frameworks.subscriptions
   * @subpackage controllers
   */
  abstract class FwPublicSubscriptionsController extends FrontendController {
    
    /**
     * One click unsubscribe, no login required
     */
    function unsubscribe() {
      $subscription_id = $this->request->getId('subscription_id');
      $subscription_code = trim($this->request->get('code'));
      
      if($subscription_id && $subscription_code) {
        $subscription = Subscriptions::findById($subscription_id);
        
        if($subscription) {
          if($subscription['code'] == $subscription_code) {
            try {
              Subscriptions::deleteByIdAndCode($subscription_id, $subscription_code);
              
              $this->flash->success('You have been successfully unsubscribed from selected object');
              $this->response->redirectTo('public');
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->notFound();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->notFound();
      } // if
    } // unsubscribe
    
  }