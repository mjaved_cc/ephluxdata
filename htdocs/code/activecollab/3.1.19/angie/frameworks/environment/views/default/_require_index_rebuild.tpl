{title}Rebuild Indexes{/title}
{add_bread_crumb}Rebuild Indexes{/add_bread_crumb}

<div id="require_index_rebuild">
  <table>
    <tr>
      <td>
        <p id="require_index_rebuild_congrat">{lang}Congratulations, upgrade is almost done!{/lang}</p>
        <p id="require_index_rebuild_click">{lang}Please click on the button below to complete the process and get back to your home screen{/lang}</p>
        <p id="require_index_rebuild_button">{link href=Router::assemble('indices_admin_rebuild') mode=flyout title="Rebuilding Indexes" class=link_button_alternative}Rebuild Indexes and Complete Upgrade{/link}</p>
      </td>
    </tr>
  </table>
</div>