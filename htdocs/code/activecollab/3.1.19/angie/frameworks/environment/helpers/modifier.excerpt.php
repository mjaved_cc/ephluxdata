<?php

  /**
   * Excerpt modifier definition
   */

  /**
   * Return excerpt from string
   *
   * @param string $string
   * @param integer $lenght
   * @param string $etc
   * @param boolean $flat
   * @return string
   */
  function smarty_modifier_excerpt($string, $lenght = 100, $etc = '...', $flat = false) {
    return $flat ? str_excerpt(strip_tags($string), $lenght, $etc) : str_excerpt($string, $lenght, $etc);
  } // smarty_modifier_excerpt