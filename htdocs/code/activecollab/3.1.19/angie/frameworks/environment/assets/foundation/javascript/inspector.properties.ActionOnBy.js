/**
 * Created on property handler
 */
App.Inspector.Properties.ActionOnBy = function (object, client_interface, widget_field_prefix) {
  var wrapper = $(this);
  
  if (!widget_field_prefix) {
    widget_field_prefix = 'created';
  } // if
  
  var action_on = App.Inspector.Utils.getFieldValue(widget_field_prefix + '_on', object);
  var action_by = App.Inspector.Utils.getFieldValue(widget_field_prefix + '_by', object);  
  
  if (!action_on || !action_by) {
    var check_string = 'not-set';
  } else {
    var check_string = action_on.formatted_date + action_by.display_name.clean();    
  } // if
  
  if (wrapper.attr('check_string') == check_string) {
    return true;
  } // if  
  wrapper.attr('check_string', check_string);
  
  wrapper_row = wrapper.parents('div.property:first');
  
  if (!action_on || !action_by) {
    wrapper_row.hide();
    return true;
  } // if
  
  wrapper_row.show();
  
  // @todo make ago js function
  wrapper.empty().append(App.lang(':when by <a href=":permalink">:display_name</a>', {
    'when' : action_on.formatted_date_gmt,
    'permalink' : action_by.permalink,
    'display_name' : action_by.display_name.clean()
  }));
};