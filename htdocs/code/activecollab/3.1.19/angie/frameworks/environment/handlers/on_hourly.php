<?php

  /**
   * on_hourly event handler implementation
   * 
   * @package angie.frameworks.environment
   * @subpackage handlers
   */

  /**
   * Handle on_hourly event
   */
  function environment_handle_on_hourly() {
    $cache =& Cache::instance();
    if($cache->backend instanceof CacheBackend) {
      $cache->backend->cleanup();
    } // if
  } // environment_handle_on_hourly