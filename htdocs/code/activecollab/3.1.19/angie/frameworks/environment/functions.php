<?php

  /**
   * Return object link
   * 
   * @param ApplicationObject $object
   * @param integer $excerpt
   * @return string
   */
  function object_link($object, $excerpt = null) {
    if($object instanceof ApplicationObject) {
      $link = '<a href="' . $object->getViewUrl() . '">' . clean(
        ($excerpt ? str_excerpt($object->getName(), $excerpt) : $object->getName())
      ) . '</a>';
      
      if($object instanceof IComplete && $object->complete()->isCompleted()) {
        return '<del class="completed">' . $link . '</del>';
      } else {
        return $link;
      } // if
    } else {
      return '';
    } // if
  } // object_link
  
  // ------------------------------------------------------------
  //  Map app stuff with files / resolve paths
  // ------------------------------------------------------------
  
  /**
   * Return path of specific template
   *
   * @param string $view
   * @param string $controller_name
   * @param string $module_name
   * @param string $interface
   * @return string
   */
  function get_view_path($view, $controller_name = null, $module_name = DEFAULT_MODULE, $interface = null) {
    return AngieApplication::getViewPath($view, $controller_name, $module_name, $interface);
  } // get_view_path

  /**
   * Get the file icon for the specified file
   *
   * @param string $filename
   * @param string $size
   * @return string
   */
  function get_file_icon_url($filename, $size) {
    if (!$size) {
      $size = '16x16';
    } // if

    $extension = strtolower(get_file_extension($filename));
    $path = ENVIRONMENT_FRAMEWORK_PATH . "/assets/default/images/file-types/{$size}/{$extension}.png";

    return is_file($path) ?
      AngieApplication::getImageUrl("file-types/{$size}/$extension.png", ENVIRONMENT_FRAMEWORK) :
      AngieApplication::getImageUrl("file-types/{$size}/default.png", ENVIRONMENT_FRAMEWORK);
  } // get_file_icon