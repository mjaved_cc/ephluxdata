<?php

  /**
   * Resources on_system_notifications handler
   *
   * @package activeCollab.modules.resources
   * @subpackage handlers
   */

  /**
   * Handle on_system_notifications event
   *
   * @param NamedList $items
   * @param IUser $user
   */
  function reminders_handle_on_system_notifications(NamedList &$items, IUser &$user) {
    if ($user instanceof User && $reminders_count = Reminders::countActiveByUser($user)) {
      $items->add('reminders', array(
        'label'   => $reminders_count > 1 ? lang(':count reminders', array('count' => $reminders_count)) : lang('One reminder', array('count' => $reminders_count)),
        'icon'    => AngieApplication::getImageUrl('icons/12x12/important.png', ENVIRONMENT_FRAMEWORK),
        'url'     => $user->reminders()->getUrl(),
        'refresh' => array('reminder_created', 'reminder_updated', 'reminder_deleted'),
        'onclick' => new FlyoutCallback(array(
          'title'   => lang(":name's Reminders", array('name' => $user->getFirstName(true))),
        )), 
      ));
    } // if
  } // reminders_handle_on_system_notifications