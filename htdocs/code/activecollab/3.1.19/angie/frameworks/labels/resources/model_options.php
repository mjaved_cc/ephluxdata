<?php

  /**
   * Model generator options for labels framework
   *
   * @package angie.frameworks.labels
   * @subpackage resources
   */

  // Labels
  $this->setTableOptions('labels', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));