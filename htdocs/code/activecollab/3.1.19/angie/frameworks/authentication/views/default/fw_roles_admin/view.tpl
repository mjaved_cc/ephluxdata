{title lang=false}{$active_role->getName()}{/title}
{add_bread_crumb}Details{/add_bread_crumb}

<div id="role">
{if $users}
  <table class="common">
    <thead>
      <tr>
        <th colspan="3">{lang role_name=$active_role->getName()}Users with <span class="active_role_name">:role_name</span> role{/lang}</th>
      </tr>
    </thead>
    <tbody>
    {foreach $users as $user}
      <tr>
        <td class="name">{user_link user=$user}</td>
        <td class="group name">{$user->getGroupName()}</td>
        <td class="checkbox">
          {if $user->getState() == $smarty.const.STATE_ARCHIVED}
            <img src="{image_url name='icons/12x12/unarchive.png' module=$smarty.const.ENVIRONMENT_FRAMEWORK}" title="{lang}User is archived{/lang}" alt="{lang}User is archived{/lang}" />
          {/if}
          {if $user->getState() == $smarty.const.STATE_TRASHED}
            <img src="{image_url name='icons/12x12/trashed.png' module=$smarty.const.ENVIRONMENT_FRAMEWORK}" title="{lang}User is in Trash{/lang}" alt="{lang}User is archived{/lang}" />
          {/if}
        </td>
      </tr>
    {/foreach}
    </tbody>
  </table>
{else}
  <p class="empty_page"><span class="inner">{lang role_name=$active_role->getName()}There are no users with <span class="active_role_name">:role_name</span> role{/lang}</span></p>
{/if}
</div>

<script type="text/javascript">
  App.Wireframe.Events.bind('role_updated.single', function (event, role) {
    App.Wireframe.PageTitle.set(role['name']);
    $('#role span.active_role_name').text(role['name']);
  });
</script>