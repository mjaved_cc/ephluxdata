<div id="maintenance_mode_settings">
  {form action=Router::assemble('maintenance_mode_settings')}
    {wrap_fields}
      {wrap field=maintenance_enabled}
        {yes_no name='maintenance[maintenance_enabled]' value=$maintenance_data.maintenance_enabled label='Enable Maintenance Mode' id=enable_maintenance_mode}
        <p class="aid">{lang}Select Yes to allow only administrators to log in and use the system{/lang}</p>
      {/wrap}

      <div id="maintenance_message_wrapper" style="display: none">
      {wrap field=maintenance_message}
        {text_field name='maintenance[maintenance_message]' value=$maintenance_data.maintenance_message label='Info Message'}
        <p class="aid">{lang}Short message that is displayed to non-administrator users when they try to log in{/lang}</p>
      {/wrap}
      </div>
    {/wrap_fields}
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>

<script type="text/javascript">
  $('#maintenance_mode_settings').each(function() {
    var wrapper = $(this);
    var info_wrapper = wrapper.find('#maintenance_message_wrapper');

    if(wrapper.find('#enable_maintenance_modeYesInput').prop('checked')) {
      info_wrapper.show();
    } // if

    wrapper.find('#enable_maintenance_modeYesInput').click(function() {
      info_wrapper.show().find('input[type=text]').focus();
    });

    wrapper.find('#enable_maintenance_modeNoInput').click(function() {
      info_wrapper.hide().find('input[type=text]').val('');
    });
  });
</script>