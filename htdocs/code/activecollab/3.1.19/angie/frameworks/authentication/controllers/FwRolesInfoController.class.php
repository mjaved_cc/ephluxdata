<?php

  // Build on top of API controller
  AngieApplication::useController('api', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

  /**
   * Roles information controller
   * 
   * @package angie.frameworks.authentication
   * @subpackage controllers
   */
  abstract class FwRolesInfoController extends ApiController {
    
    /**
     * Actions that are available through API
     *
     * @var array
     */
    protected $api_actions = array('index', 'role');
    
    /**
     * Execute before any action is called
     */
    function __before() {
      parent::__before();
      
      if(!$this->canSeeRoleInfo($this->logged_user)) {
        $this->response->forbidden();
      } // if
    } // __before
  
    /**
     * List all available system roles
     */
    function index() {
      if($this->logged_user->isPeopleManager() || $this->logged_user->isProjectManager()) {
        $this->response->respondWithData(Roles::find(), array(
          'as' => 'roles', 
        ));
      } else {
        $this->response->forbidden();
      } // if
    } // index
    
    /**
     * Show role details
     */
    function role() {
      if($this->logged_user->isPeopleManager() || $this->logged_user->isProjectManager()) {
        $role_id = $this->request->getId('role_id');
        $role = $role_id ? Roles::findById($role_id) : null;
        
        if($role instanceof Role) {
          $this->response->respondWithData($role, array(
            'as' => 'role', 
          ));
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->forbidden();
      } // if
    } // role
    
    /**
     * Return true if $user can see role information
     * 
     * @param IUser $user
     * @return boolean
     */
    protected function canSeeRoleInfo($user) {
      return $user instanceof User && $user->isAdministrator();
    } // canSeeRoleInfo
    
  }