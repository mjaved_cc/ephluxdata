<?php

  /**
   * attachments_uploader helper
   *
   * @package activeCollab.modules.resources
   * @subpackage helpers
   */
  
  /**
   * Render multiupload attachments form
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_attachments($params, &$smarty) {
    static $ids = array();
    
    $user = array_var($params, 'user');
    if(!($user instanceof IUser)) {
      throw new InvalidInstanceError('user', $user, 'IUser');
    } // if
    
    $id = array_var($params, 'id');
    if(empty($id)) {
      $counter = 1;
      do {
        $id = 'file_uploader_' . $counter++;
      } while(in_array($id, $ids));
    } // if
    
    $id.= '_' .  time();
    $ids[] = $id;
    
    $name = array_var($params, 'name', null);
    if(empty($name)) {
      throw new InvalidParamError('name', $name, 'name attribute is required for select object attachments helper');
    } // if
    $delete_name = $name.'[delete][]';
    $name.= '[pending_parent][]';
    
   
    $object = array_var($params, 'object', null);
    if($object instanceof IAttachments) {
      $existing_attachments = $object->isLoaded() ? $object->attachments()->get($user) : null;
    } else {
      throw new InvalidParamError('object', $object, '$object does not support attachments');
    } // if
    
    $formated_existing_attachments = array();
    if (is_foreachable($existing_attachments)) {
    	foreach ($existing_attachments as $existing_attachment) {
				$formated_existing_attachments[] = array(
					'id'				=> $existing_attachment->getId(),
					'filename'	=> $existing_attachment->getName()
				);
    	} // foreach
    } // if

    $smarty->assign(array(
      '_select_object_attachments_id' => $id,
      '_select_object_attachments_name' => $name,
      '_select_object_attachments_delete_name' => $delete_name,
      '_select_object_attachments_existing_attachments' => $formated_existing_attachments,
      '_select_object_attachments_show_first_input' => array_var($params, 'show_first_input', $object->isNew()),
    ));

    // variables needed for flash upload
    if (!BASIC_FILE_UPLOADS) {
      $smarty->assign('_select_object_attachments_flash_vars', '?'.implode('&amp;', array(
        'uploader_id='.urlencode($id),
        'cookie_name='.urlencode(Authentication::getProvider() instanceof BasicAuthenticationProvider ? Authentication::getProvider()->getSessionIdVarName() : null),
        'cookie_value='.urlencode(Cookies::getVariable(Authentication::getProvider() instanceof BasicAuthenticationProvider ? Authentication::getProvider()->getSessionIdVarName() : null)),
        'script_url='.urlencode(Router::assemble('temporary_attachment_add', array('async' => 1, 'flash' => 1))),
        'post_field_name='.urlencode('attachment'),
        'debug_mode='.(AngieApplication::isInDebugMode() ? '1': '0'),
        'negotiator='.urlencode('App.widgets.FlashUploader')
      )));
    } else {
      $smarty->assign('_select_object_attachments_url', Router::assemble('temporary_attachment_add', array('async' => 1)));
    } // if

    return $smarty->fetch(get_view_path('_select_attachments', null, ATTACHMENTS_FRAMEWORK));
  } // smarty_function_select_object_attachments