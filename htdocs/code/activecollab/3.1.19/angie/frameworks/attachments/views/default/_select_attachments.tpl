<div class="select_attachments" id="{$_select_object_attachments_id nofilter}">
  <table class="select_attachments_list" cellspacing="0"></table>
  
  <div class="upload_button">
    <a href="#" id="{$_select_object_attachments_id nofilter}_attach_file_button" class="link_button"><span class="inner"><span class="icon button_add">{lang}Attach Files{/lang}</span></span></a>
  </div>

  <p class="select_object_attachments_max_size details">{max_file_size_warning}</p>
</div>

<script type="text/javascript">
  (function () {
    var wrapper = $('#{$_select_object_attachments_id}');
    var upload_button_wrapper = wrapper.find('.upload_button');

    {if !$smarty.const.BASIC_FILE_UPLOADS}
      var flash_container = $('<div class="flash_container"></div>').appendTo(upload_button_wrapper).flash({
        swf:                App.Wireframe.Utils.assetUrl('flash_uploader.swf', 'environment', 'flash') + '{$_select_object_attachments_flash_vars nofilter}',
        width:              '100%',
        height:             '100%',
        play:               'false',
        id:                 '{$_select_object_attachments_id nofilter}_flash',
        name:               '{$_select_object_attachments_id nofilter}_flash',
        menu:               'false',
        allowFullScreen:    'false',
        scale:              'noscale',
        wmode:              'transparent',
        allowScriptAccess:  'always'
      });

      wrapper.selectAttachmentsAdvanced({
        name                  : {$_select_object_attachments_name|json nofilter},
        delete_name           : {$_select_object_attachments_delete_name|json nofilter},
        existing_attachments  : {$_select_object_attachments_existing_attachments|json nofilter}
      });
    {else}
      wrapper.selectAttachmentsBasic({
        name                  : {$_select_object_attachments_name|json nofilter},
        delete_name           : {$_select_object_attachments_delete_name|json nofilter},
        upload_url            : {$_select_object_attachments_url|json nofilter},
        existing_attachments  : {$_select_object_attachments_existing_attachments|json nofilter}
      });
    {/if}

  }());
</script>