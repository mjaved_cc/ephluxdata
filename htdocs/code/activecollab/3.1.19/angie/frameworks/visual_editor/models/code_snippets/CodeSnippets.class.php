<?php


  /**
   * CodeSnippets class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class CodeSnippets extends BaseCodeSnippets {
  	
    /**
     * Returns true if $user can add code snippets
     *
     * @param User $user
     * @param Project $project
     * @return boolean
     */
    static function canAdd(User $user) {
    	if ($user->isAdministrator()) {
    		return true;
    	} // if
    	
    	// @todo rethink if we want to allow snippets to be added by everyone 
      return true;
    } // canAdd
  }

