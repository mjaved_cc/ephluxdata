<?php

  /**
   * Model generator options for visual editor framework
   *
   * @package angie.frameworks.visual_editor
   * @subpackage resources
   */

  // Models
  $this->setTableOptions('code_snippets', array(
    'module' => 'visual_editor', 'object_extends' => 'ApplicationObject'
  ));