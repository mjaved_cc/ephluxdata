<?php

  /**
   * Model generator options for subtasks framework
   *
   * @package angie.frameworks.subtasks
   * @subpackage resources
   */

  // Subtasks
  $this->setTableOptions('subtasks', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));