<?php

  /**
   * Email framework intialization file
   *
   * @package angie.frameworks.email
   */

  define('EMAIL_FRAMEWORK', 'email');
  define('EMAIL_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/email');
  
  // ---------------------------------------------------
  //  Overridable Settings
  // ---------------------------------------------------
  
  @define('OBJECT_SOURCE_EMAIL', 'email');
  
  @define('EMAIL_FRAMEWORK_INJECT_INTO', 'system');
  @define('EMAIL_FRAMEWORK_ROUTE_BASE', 'admin');
  
  @define('EMAIL_ENCODING', '8bit');
  @define('EMAIL_CHARSET', 'utf-8');
  
  // Max number of messages that can be sent per request
  @define('MAILING_QUEUE_MAX_PER_REQUEST', 50);
  
  // Max number of retries for outgoing message before it gets ignored
  @define('MAILING_QUEUE_MAX_SEND_RETRIES', 5);

  // lang('-- REPLY ABOVE THIS LINE --')
  @define('EMAIL_SPLITTER', '-- REPLY ABOVE THIS LINE --');

  @define('INCOMING_MAIL_ATTACHMENTS_FOLDER',WORK_PATH);

  @define('INCOMING_MAIL_DEFAULT_MAILBOX', 'INBOX');

  @define('INCOMING_MAIL_OBJECT_TYPE_TICKET', 'ticket');
  @define('INCOMING_MAIL_OBJECT_TYPE_DISCUSSION', 'discussion');
  @define('INCOMING_MAIL_OBJECT_TYPE_COMMENT', 'comment');
  
  //notify admin when disable mailbox after connecting failure
  @define('NOTIFY_ADMIN_ON_DISABLE_MAILBOX',false);
  @define('INCOMING_MAIL_FAILURE_ATTEMPTS_NUM',3);
  @define('INCOMING_MAIL_ADD_REPLAY_AS_BLOCKQUOTE',false);
  
  
  @define('INCOMING_MAIL_INVALID_EMAIL_ADDRESS',"invalid.email.address@unknown.com");
  
  
  // ---------------------------------------------------
  //  Constants used by framework
  // ---------------------------------------------------
  
  define('MAILING_DISABLED', 'disabled');
  define('MAILING_SILENT', 'silent');
  define('MAILING_NATIVE', 'native');
  define('MAILING_SMTP', 'smtp');
  
  define('MAILING_METHOD_INSTANTLY', 'instantly');
  define('MAILING_METHOD_IN_BACKGROUND', 'in_background');
  
  AngieApplication::setForAutoload(array(
    'ApplicationMailer' => EMAIL_FRAMEWORK_PATH . '/models/ApplicationMailer.class.php',
  
    'ApplicationMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/ApplicationMailerAdapter.class.php',  
    'SilentMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/SilentMailerAdapter.class.php',  
    'DisabledMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/DisabledMailerAdapter.class.php',  
      
    'SwiftMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/SwiftMailerAdapter.class.php',
    'NativeSwiftMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/NativeSwiftMailerAdapter.class.php',
    'SmtpSwiftMailerAdapter' => EMAIL_FRAMEWORK_PATH . '/models/mailer_adapters/SmtpSwiftMailerAdapter.class.php',
  
    'ApplicationMailerDecorator' => EMAIL_FRAMEWORK_PATH . '/models/mailer_decorators/ApplicationMailerDecorator.class.php',
  
    // Outgoing messages
    'FwOutgoingMessage' => EMAIL_FRAMEWORK_PATH . '/models/outgoing_messages/FwOutgoingMessage.class.php',
    'FwOutgoingMessages' => EMAIL_FRAMEWORK_PATH . '/models/outgoing_messages/FwOutgoingMessages.class.php',
  
    'IOutgoingMessageAttachmentsImplementation' => EMAIL_FRAMEWORK_PATH . '/models/IOutgoingMessageAttachmentsImplementation.class.php',
  
    // Notifier
    'INotifier' => EMAIL_FRAMEWORK_PATH . '/models/notifier/INotifier.class.php',
    'INotifierImplementation' => EMAIL_FRAMEWORK_PATH . '/models/notifier/INotifierImplementation.class.php',
    'INotifierContext' => EMAIL_FRAMEWORK_PATH . '/models/notifier/INotifierContext.class.php',
  
    'NotifierEvent' => EMAIL_FRAMEWORK_PATH . '/models/notifier/NotifierEvent.class.php', 
    
    // Activity log
    'FwMailingActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/FwMailingActivityLog.class.php', 
    'FwMailingActivityLogs' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/FwMailingActivityLogs.class.php',
    
    'FwIncomingMailbox' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mailboxes/FwIncomingMailbox.class.php', 
    'FwIncomingMailboxes' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mailboxes/FwIncomingMailboxes.class.php', 
    'FwIncomingMail' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mails/FwIncomingMail.class.php', 
    'FwIncomingMails' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mails/FwIncomingMails.class.php', 
    'FwIncomingMailAttachment' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_attachments/FwIncomingMailAttachment.class.php', 
    'FwIncomingMailAttachments' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_attachments/FwIncomingMailAttachments.class.php',
    'FwIncomingMailActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_activity_logs/FwIncomingMailActivityLog.class.php', 
    'FwIncomingMailActivityLogs' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_activity_logs/FwIncomingMailActivityLogs.class.php',  
    'FwIncomingMailFilter' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_filters/FwIncomingMailFilter.class.php',  
    'FwIncomingMailFilters' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_filters/FwIncomingMailFilters.class.php',

    'IncomingMailAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailAction.class.php',
    'IncomingMailIgnoreAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailIgnoreAction.class.php',
    'IncomingMailMoveToTrashAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailMoveToTrashAction.class.php',
    'IncomingMailCommentAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailCommentAction.class.php',
    'IncomingMailTaskAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailTaskAction.class.php',
  	'IncomingMailDiscussionAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailDiscussionAction.class.php',
  	'IncomingMailFileAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailFileAction.class.php',
    'IncomingMailProjectObjectAction' => EMAIL_FRAMEWORK_PATH . '/models/incoming_mail_actions/IncomingMailProjectObjectAction.class.php',
  
  	'IncomingMailImporter' => EMAIL_FRAMEWORK_PATH . '/models/IncomingMailImporter.class.php',
  
    // Mailing activity logs
    'OutgoingMailingActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/outgoing/OutgoingMailingActivityLog.class.php',
    'MessageSentActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/outgoing/MessageSentActivityLog.class.php',
  	'SmtpFailedToConnectActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/SmtpFailedToConnectActivityLog.class.php', 
    'SmtpFailedToSendActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/SmtpFailedToSendActivityLog.class.php',
  
    'IncomingMailingActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMailingActivityLog.class.php',
    'IncomingMessageActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageActivityLog.class.php',
    'IncomingMessageImportErrorActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageImportErrorActivityLog.class.php',
    'IncomingMessageReceivedActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageReceivedActivityLog.class.php',
    'IncomingMessageServerErrorActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageServerErrorActivityLog.class.php',
  	'IncomingMessageAutoRespondActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageAutoRespondActivityLog.class.php',
  	'IncomingMessageDeleteActivityLog' => EMAIL_FRAMEWORK_PATH . '/models/mailing_activity_logs/incoming/IncomingMessageDeleteActivityLog.class.php',
  ));