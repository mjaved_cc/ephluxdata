<?php

  /**
   * Mail notification context interface
   * 
   * This interface needs to be implemented by all objects that want to be able 
   * to receive replies to email notifications
   *
   * @package angie.frameworks.email
   * @subpackage models
   */
  interface INotifierContext {
    
    /**
     * Return context ID for object that implements this interface
     *
     * @return string
     */
    function getNotifierContextId();
    
  }