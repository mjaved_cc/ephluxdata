<?php

  /**
   * Notifier interface definition
   * 
   * @package angie.frameworks.email
   * @subpackage models
   */
  interface INotifier {
  
  	/**
  	 * Return notification helper
  	 *
  	 * @return INotifierImplementation
  	 */
    function notifier();
    
  }