<?php

  /**
   * TestEmailObject class
   * 
   * @package angie.framework.email
   * @subpackage tests
   */
  class TestEmailObject extends BaseTestEmailObject implements INOtifier, INotifierContext {
  
  	/**
  	 * Return notifier helper instance
  	 * 
  	 * @var unknown_type
  	 */
    private $notifier = false;
    
    /**
     * Return notifier helper instance
     * 
     * @return INotifierImplementation
     */
    function notifier() {
    	if($this->notifier === false) {
    		$this->notifier = new INotifierImplementation($this);
    	} // if
    	
    	return $this->notifier;
    } // notifier
    
    /**
     * Return email notification context ID
     * 
     * @return string
     */
    function getNotifierContextId() {
    	return 'TESTOBJ/' . $this->getId();
    } // getNotifierContextId
    
  }