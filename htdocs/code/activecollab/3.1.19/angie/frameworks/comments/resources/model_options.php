<?php

  /**
   * Model generator options for comments framework
   *
   * @package angie.frameworks.comments
   * @subpackage resources
   */

  // Comments
  $this->setTableOptions('comments', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));