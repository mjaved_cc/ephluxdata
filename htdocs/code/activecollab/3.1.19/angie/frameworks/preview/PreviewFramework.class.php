<?php

  /**
   * Preview framework definition
   *
   * @package angie.frameworks.preview
   */
  class PreviewFramework extends AngieFramework {
    
    /**
     * Short framework name
     *
     * @var string
     */
    protected $name = 'preview';
    
  }

?>