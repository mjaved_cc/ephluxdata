<?php

  /**
   * Preview implementation for downloadble files
   *
   * @package angie.frameworks.preview
   * @subpackage models
   */
  class IDownloadPreviewImplementation extends IPreviewImplementation {
    
    /**
     * Parent object instance
     *
     * @var IDownload
     */
    protected $object;
    
    /**
     * Construct download preview implementation
     *
     * @param IPreview $object
     */
    function __construct(IPreview $object) {
      if($object instanceof IPreview && $object instanceof IDownload) {
        parent::__construct($object);
      } else {
        throw new InvalidInstanceError('object', $object, array('IPreview', 'IDownload'));
      } // if
    } // __construct
    
    /**
     * Returns true if parent object has preview
     *
     * @return boolean
     */
    function has() {
      return (boolean) $this->getPreviewType();
    } // has
    
    /**
     * Return small icon URL
     *
     * @return string
     */
    function getSmallIconUrl() {
      return $this->getIconUrl(16, 16);
    } // getSmallIconUrl
    
    /**
     * Return large icon URL
     *
     * @return string
     */
    function getLargeIconUrl() {
      return $this->getIconUrl(48, 48);
    } // getLargeIconUrl
    
    /**
     * Cached icon URL-s
     *
     * @var array
     */
    private $icon_urls = array();
    
    /**
     * Return icon URL
     *
     * @param integer $width
     * @param integer $height
     * @return string
     */
    private function getIconUrl($width, $height) {
      $dimensions = "{$width}x{$height}";
      
      if(!array_key_exists($dimensions, $this->icon_urls)) {
      	$this->icon_urls[$dimensions] = get_file_icon_url($this->object->getName(), $dimensions);
      } // if
      
      return $this->icon_urls[$dimensions];
    } // getIconUrl
    
    /**
     * Render small preview
     *
     * @return string
     */
    function renderSmall() {
      return $this->renderPreview(80, 80);
    } // renderSmall
    
    /**
     * Render large preview
     *
     * @return string
     */
    function renderLarge() {
      return $this->renderPreview(550, 300);
    } // renderLarge
    
    /**
     * Cached rendered previews
     *
     * @var array
     */
    private $previews = array();
    
    /**
     * Render preview
     *
     * @param integer $width
     * @param integer $height
     * @return string
     */
    function renderPreview($width, $height) {
      $dimensions = "{$width}x{$height}";
      
      if(!array_key_exists($dimensions, $this->previews)) {
        switch($this->getPreviewType()) {
          
          // Render video player
          case DOWNLOAD_PREVIEW_VIDEO:
            break;
            
          // Render audio player
          case DOWNLOAD_PREVIEW_AUDIO:
            break;
            
          // Render image preview
          case DOWNLOAD_PREVIEW_IMAGE:
            $this->previews[$dimensions] = '<div class="file_preview">' . HTML::openTag('a', array(
              'href' => $this->object->download()->getDownloadUrl(),
              'title' => lang('Click to Download'),
              'target' => '_blank',
            )) . HTML::openTag('img', array(
              'src' => Thumbnails::getUrl($this->object->download()->getPath(), $width, $height), 
              'alt' => lang('File preview'), 
            )) . '</a></div>';
            
            break;
            
          // Render flash preview box
          case DOWNLOAD_PREVIEW_FLASH:
            break;
            
          // Render icon inside of preview box
          default:
            $this->previews[$dimensions] = HTML::openTag('div', array(
              'class' => 'preview', 
              'width' => $width, 
              'height' => $height, 
            )) . HTML::openTag('a', array(
              'href' => $this->object->download()->getDownloadUrl(),
              'title' => lang('Click to Download'),
              'target' => '_blank',
            )) . HTML::openTag('img', array(
              'src' => $this->getIconUrl(48, 48)
            ), true) . '</a></div>';

        } // switch
      } // if
      
      return $this->previews[$dimensions];
    } // renderPreview
    
    /**
     * Cached preview type
     *
     * @var mixed
     */
    private $preview_type = null;
    
    /**
     * Return preview type
     *
     * @return mixed
     */
    function getPreviewType() {
      if($this->preview_type === null) {
        if(in_array($this->object->getMimeType(), array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png'))) {
          $this->preview_type = DOWNLOAD_PREVIEW_IMAGE;
        } else {
          $file_extension = strtolower(get_file_extension($this->object->getName()));
          
          if($file_extension == 'flv' || $file_extension == 'mp4') {
            $this->preview_type = DOWNLOAD_PREVIEW_VIDEO;
          } elseif($file_extension == 'mp3' || $file_extension == 'aac') {
            $this->preview_type = DOWNLOAD_PREVIEW_AUDIO;
          } elseif($file_extension == 'swf') {
            $this->preview_type = DOWNLOAD_PREVIEW_FLASH;
          } else {
            $this->preview_type = false; // Preview not support
          } // if
        } // if
      } // if
      
      return $this->preview_type;
    } // getPreviewType
    
  }