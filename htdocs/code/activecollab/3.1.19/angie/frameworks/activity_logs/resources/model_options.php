<?php

  /**
   * Model generator options for activity_logs framework
   *
   * @package angie.frameworks.activity_logs
   * @subpackage resources
   */

  // Activity logs
  $this->setTableOptions('activity_logs', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject'
  ));