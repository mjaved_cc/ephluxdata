{title}Workweek Settings{/title}
{add_bread_crumb}Workweek Settings{/add_bread_crumb}

<div id="workweek_settings">
  {form action=Router::assemble('workweek_settings') method=post}
    <div class="content_stack_wrapper">
    
      <!-- Rescheduling -->
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Rescheduling{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=first_week_day}
            {label}Skip Days Off When Rescheduling{/label}
            {yes_no name="workweek[skip_days_off_when_rescheduling]" value=$workweek_data.skip_days_off_when_rescheduling}
            <p class="aid">{lang}When set to Yes, activeCollab will make sure that start and due dates don't fall on days off when rescheduling milestones, creating project from a template etc{/lang}.</p>
          {/wrap}
        </div>
      </div>
    
      <!-- Work days -->
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Workdays{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=first_week_day}
            {label for=workweekFirstWeekDay required=yes}First Day in a Week{/label}
            {select_week_day name="workweek[time_first_week_day]" value=$workweek_data.time_first_week_day id=workweekFirstWeekDay}
          {/wrap}
          
          {wrap field=workdays}
            {label required=yes}Workdays{/label}
            {select_week_days name="workweek[time_workdays]" value=$workweek_data.time_workdays required=yes}
          {/wrap}
        </div>
      </div>
      
      <!-- Days Off -->
      <div class="content_stack_element last">
        <div class="content_stack_element_info">
          <h3>{lang}Days Off{/lang}</h3>
        </div>
        <div id="days_off_wrapper" class="content_stack_element_body">
          {wrap field=days_off}
            <table class="form form_field validate_callback validate_days_off" id="workweek_days_off" style="{if !is_foreachable($workweek_data.days_off)}display: none{/if}">
              <tr>
                <th class="name">{label required=yes}Event Name{/label}</th>
                <th class="date">{label required=yes}Date{/label}</th>
                <th class="yearly center">{label}Repeat Yearly?{/label}</th>
                <th></th>
              </tr>
            {if is_foreachable($workweek_data.days_off)}
              {foreach $workweek_data.days_off as $day_off_key => $day_off}
                <tr class="day_off_row {cycle values='odd,even'}">
                  <td class="name">{text_field name="workweek[days_off][$day_off_key][name]" value=$day_off.name}</td>
                  <td class="date">{select_date name="workweek[days_off][$day_off_key][event_date]" value=$day_off.date}</td>
                  <td class="yearly center"><input name="workweek[days_off][{$day_off_key}][repeat_yearly]" type="checkbox" value="1" class="inline" {if $day_off.repeat_yearly}checked="checked"{/if} /></td>
                  <td class="options right"><a href="#" title="{lang}Remove Day Off{/lang}" class="remove_day_off"><img src='{image_url name="icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}' alt='' /></a></td>
                </tr>
              {/foreach}
            {/if}
            </table>
            <p id="no_days_off_message" style="{if is_foreachable($workweek_data.days_off)}display: none{/if}">{lang}There are no days off defined{/lang}</p>
            <p><a href="#" class="button_add">{lang}New Day Off{/lang}</a></p>
          {/wrap}
        </div>
      </div>
    </div>
    
    {wrap_buttons}
  	  {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>

<script type="text/javascript">

  /**
   * Validate days off table
   *
   * @param jQuery field
   * @param String caption
   * return mixed
   */
  window.validate_days_off = function(field, caption) {
    var data = [];
    
    field.find('tr.day_off_row').each(function() {
      var row = $(this);
      
      data.push({
        'name' : jQuery.trim(row.find('td.name input').val()),
        'date' : jQuery.trim(row.find('td.date input').val())
      });
    });
    
    for(var i in data) {
      if(data[i]['name'] == '' || data[i]['date'] == '') {
        return App.lang('Fill out missing fields');
      } // if
      
      for(var j in data) {
        if(i != j && (data[i]['name'].toLowerCase() == data[j]['name'].toLowerCase())) {
          return App.lang('Event names need to be unique');
        } // if
      } // for
    } // for
    
    return true;
  };
  
  // Initialize widget
  App.widgets.DaysOff.init('days_off_wrapper');
</script>