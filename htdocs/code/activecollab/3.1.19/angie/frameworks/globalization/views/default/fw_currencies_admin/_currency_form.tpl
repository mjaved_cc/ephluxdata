{wrap field=name}
  {text_field name='currency[name]' value=$currency_data.name label="Name" required=true}
{/wrap}

{wrap field=code}
  {text_field name='currency[code]' value=$currency_data.code label="Code" required=true}
{/wrap}