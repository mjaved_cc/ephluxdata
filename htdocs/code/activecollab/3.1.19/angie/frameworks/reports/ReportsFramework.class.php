<?php

  /**
   * Reports framework
   * 
   * @package angie.frameworks.reports
   */
  class ReportsFramework extends AngieFramework {
  
    /**
     * Short framework name
     *
     * @var string
     */
    protected $name = 'reports';
    
    /**
     * Define module routes
     */
    function defineRoutes() {
      Router::map('reports', 'reports', array('controller' => 'reports', 'action' => 'index', 'module' => REPORTS_FRAMEWORK_INJECT_INTO));
    } // defineRoutes
    
    /**
     * Define handlers
     */
    function defineHandlers() {
      EventsManager::listen('on_main_menu', 'on_main_menu');
    } // defineHandlers(
    
  }