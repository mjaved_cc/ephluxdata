<?php

  /**
   * Parent composite column
   *
   * @package angie.library.database
   * @subpackage engineer
   */
  class DBParentColumn extends DBCompositeColumn {
    
    /**
     * Flag if we need to add key on parent fields or not
     *
     * @var boolean
     */
    private $add_key;
    
    /**
     * Construct parent column instance
     *
     * @param boolean $add_key
     */
    function __construct($add_key = true) {
      $this->add_key = $add_key;
      
      $this->columns = array(
        DBStringColumn::create('parent_type', 50), 
        DBIntegerColumn::create('parent_id', DBColumn::NORMAL)->setUnsigned(true), 
      );
    } // __construct
    
    /**
     * Construct and return parent column
     *
     * @param boolean $add_key
     * @return DBParentColumn
     */
    static function create($add_key = true) {
      return new DBParentColumn($add_key);
    } // create
    
    /**
     * Event that table triggers when this column is added to the table
     */
    function addedToTable() {
      if($this->add_key) {
        $this->table->addIndex(new DBIndex('parent', DBIndex::KEY, array('parent_type', 'parent_id')));
      } // if
    } // addedToTable
    
  }