<?php

  /**
   * activeCollab specific model definition
   *
   * @package activeCollab
   * @subpackage resources
   */
  class ActiveCollabModuleModel extends AngieModuleModel {
  	
  	/**
  	 * Create a new company and return company ID
  	 * 
  	 * @param string $name
  	 * @param array $additional
  	 * @return integer
  	 */
  	function addCompany($name, $additional = null) {
  		$properties = array(
  		  'name' => $name, 
  		  'state' => 3, // STATE_VISIBLE
  		);
  		
  		if(is_array($additional)) {
  			$properties = array_merge($properties, $additional);
  		} // if
  		
  		return $this->createObject('companies', $properties);
  	} // addCompany
  	
  	/**
  	 * Create a user and return user ID
  	 * 
  	 * @param string $email
  	 * @param integer $company_id
  	 * @param integer $role_id
  	 * @param array $additional
  	 * @return integer
  	 */
  	function addUser($email, $company_id, $role_id, $additional = null) {
  		$properties = array(
  		  'company_id' => $company_id, 
  		  'role_id' => $role_id, 
  		  'state' => 3, // STATE_VISIBLE 
  		  'email' => $email, 
  		);
  		
  		if(is_array($additional)) {
  			$properties = array_merge($properties, $additional);
  		} // if
  		
  		if(isset($properties['password'])) {
  			$properties['password'] = base64_encode(pbkdf2($properties['password'], LICENSE_KEY, 1000, 40));
  		} else {
  			$properties['password'] = base64_encode(pbkdf2('test', LICENSE_KEY, 1000, 40));
  		} // if

      $properties['password_hashed_with'] = 'pbkdf2';
  		
  		$properties['created_on'] = date(DATETIME_MYSQL);
  		if(!isset($properties['created_by_id'])) {
  			$properties['created_by_id'] = 1;
  		} // if
  		
  		return $this->createObject('users', $properties);
  	} // addUser
    
    /**
     * Create a new object in a given table, with given properties
     * 
     * This function is specific because it creates proper records in search 
     * index, modification log etc
     *
     * @param string $table
     * @param array $properties
     * @return integer
     */
    function createObject($table, $properties) {
    	$to_insert = array();
    	foreach($properties as $k => $v) {
    		$to_insert[DB::escapeFieldName($k)] = DB::escape($v);
    	} // foreach
    	
      DB::execute('INSERT INTO ' . DB::escapeTableName(TABLE_PREFIX . $table) . ' (' . implode(', ', array_keys($to_insert)) . ') VALUES (' . implode(', ', $to_insert) . ')');
      
      return DB::lastInsertId();
    } // createObject
    
  }