<?php

  /**
   * Default configuration values
   */
  
  define('APPLICATION_NAME', 'ActiveCollab');
  define('APPLICATION_BUILD', '3766');
  
  if(!defined('APPLICATION_VERSION_STABLE')) {
    define('APPLICATION_VERSION_STABLE', 0);
  } // if
  
  // If we are using unpacked file, make sure that value is well set
  if(defined('USE_UNPACKED_FILES') && USE_UNPACKED_FILES) {
    define('APPLICATION_PATH', ROOT . '/' . APPLICATION_VERSION);
    
    if(!defined('ANGIE_PATH')) {
      define('ANGIE_PATH', APPLICATION_PATH . '/angie');
    } // if
    
  // Not using packed files? Properly set angie path
  } else {
    if(!defined('USE_UNPACKED_FILES')) {
      define('USE_UNPACKED_FILES', false);
    } // if
    
    define('APPLICATION_PATH', 'phar://' . APPLICATION_NAME . '-' . APPLICATION_VERSION . '.phar');
    
    if(!defined('ANGIE_PATH')) {
      define('ANGIE_PATH', APPLICATION_PATH . '/angie');
    } // if
  } // if

  if(!defined('APPLICATION_MODE')) {
    define('APPLICATION_MODE', 'in_production');
  } // if

  define('APPLICATION_FRAMEWORKS', 'environment,modules,globalization,authentication,activity_logs,reports,history,email,download,preview,homescreens,complete,attachments,subscriptions,comments,categories,labels,assignees,subtasks,favorites,visual_editor,payments,avatar,text_compare,reminders,search');
  
  if(LICENSE_PACKAGE == 'corporate') {
    define('APPLICATION_MODULES', 'system,discussions,milestones,files,todo,calendar,notebooks,tasks,tracking,project_exporter,status,documents,source,invoicing,flow');
  } elseif(LICENSE_PACKAGE == 'smallbiz') {
    define('APPLICATION_MODULES', 'system,discussions,milestones,files,todo');
  } else {
    define('APPLICATION_MODULES', 'system');
  } // if

  if(!defined('GLOBALIZATION_ADAPTER')) {
    define('GLOBALIZATION_ADAPTER', 'ActiveCollabGlobalizationAdapter');
  } // if
  
  // Use activeCollab specific favorites routes
  define('FAVORITES_FRAMEWORK_DEFINE_ROUTES', false);
  
  // Public folder
  if(!defined('PUBLIC_FOLDER_NAME')) {
    define('PUBLIC_FOLDER_NAME', 'public');
  } // if
  
  // URL-s
  if(!defined('URL_BASE')) {
  	define('URL_BASE', ROOT_URL . '/index.php');
  } // if
  if(!defined('ASSETS_URL')) {
    define('ASSETS_URL', ROOT_URL . '/assets');
  } // if
  
  if(!defined('ASSETS_PATH')) {
    define('ASSETS_PATH', PUBLIC_PATH . '/assets');
  } // if
  
  // Authentication provider
  if(!defined('AUTH_PROVIDER')) {
    define('AUTH_PROVIDER', 'BasicAuthenticationProvider');
  } // if
  
  // Force query string for hosts that does not support
  // PATH_INFO or make problems with it
  if(!defined('FORCE_QUERY_STRING')) {
    define('FORCE_QUERY_STRING', true);
  } // if
  
  if(!defined('PATH_INFO_THROUGH_QUERY_STRING')) {
    define('PATH_INFO_THROUGH_QUERY_STRING', true);
  } // if

  if (!defined('FORCE_ROOT_URL')) {
    define('FORCE_ROOT_URL', true);
  } // if
  
  if(!defined('PUBLIC_AS_DOCUMENT_ROOT')) {
    define('PUBLIC_AS_DOCUMENT_ROOT', false);
  } // if
  
  if(!defined('ADMIN_EMAIL')) {
    define('ADMIN_EMAIL', false);
  } // if
  
  define('TEST_SMTP_BY_SENDING_EMAIL_TO', 'noreply@activecollab.com');
  
  // Enable mass mailer
  if (!defined('MASS_MAILER_ENABLED')) {
    define('MASS_MAILER_ENABLED', true);
  } // if
  
  if(!defined('WARN_WHEN_JAVASCRIPT_IS_DISABLED')) {
    define('WARN_WHEN_JAVASCRIPT_IS_DISABLED', true);
  } // if
  
  // Device class and interface related
  if(!defined('FORCE_INTERFACE')) {
    define('FORCE_INTERFACE', false);
  } // if
  
  if(!defined('FORCE_DEVICE_CLASS')) {
    define('FORCE_DEVICE_CLASS', false);
  } // if
  
  // Scheduled tasks
  if(!defined('PROTECT_SCHEDULED_TASKS')) {
    define('PROTECT_SCHEDULED_TASKS', true);
  } // if

  // Protect shared assets from accidental overwrite
  if(!defined('PROTECT_ASSETS_FOLDER')) {
    define('PROTECT_ASSETS_FOLDER', false);
  } // if
  
  if(!defined('PURIFY_HTML')) {
    define('PURIFY_HTML', true);
  } // if

  if(!defined('REMOVE_EMPTY_PARAGRAPHS')) {
    define('REMOVE_EMPTY_PARAGRAPHS', true);
  } // if
  
  if(!defined('MAINTENANCE_MESSAGE')) {
    define('MAINTENANCE_MESSAGE', null);
  } // if
  
  // Thumbnails
  if(!defined('CREATE_THUMBNAILS')) {
    define('CREATE_THUMBNAILS', true); // create thumbnails for images
  } // if
  
  if(!defined('RESIZE_SMALLER_THAN')) {
    define('RESIZE_SMALLER_THAN', 524288); // resize images smaller than 500kb
  } // if

  // image size constraints
  if (!defined('IMAGE_SIZE_CONSTRAINT')) {
    define('IMAGE_SIZE_CONSTRAINT', '2240x1680'); // 4 mega pixels
  } // if

  if(!defined('USER_SESSION_LIFETIME')) {
    define('USER_SESSION_LIFETIME', 1800); // 30 minutes
  } // if
  
  // if this option is set to true, mailbox manager will use some of custom functions
  // to handle retrieveing emails
  if(!defined('FAIL_SAFE_IMAP_FUNCTIONS')) {
    define('FAIL_SAFE_IMAP_FUNCTIONS', false);
  } // if
  
  if(!defined('FAIL_SAFE_IMAP_ATTACHMENT_SIZE_MAX')) {
    define('FAIL_SAFE_IMAP_ATTACHMENT_SIZE_MAX', 512000);
  } // if
  
  if(!defined('COMPRESS_ASSET_REQUESTS')) {
    define('COMPRESS_ASSET_REQUESTS', true);
  } // if
  
  define('PAGE_PLACEHOLDER', '-PAGE-');
  
  // Number format
  define('NUMBER_FORMAT_DEC_SEPARATOR', '.');
  define('NUMBER_FORMAT_THOUSANDS_SEPARATOR', ',');
  
  if(!defined('DEFAULT_CSV_SEPARATOR')) {
    define('DEFAULT_CSV_SEPARATOR', ',');
  } // if
  
  // ---------------------------------------------------
  //  Caching
  // ---------------------------------------------------
  
  // Cache folder path, for 
  define('CACHE_PATH', ENVIRONMENT_PATH . '/cache');
  
  if (!defined('COLLECTOR_CHECK_ETAG')) {
  	define('COLLECTOR_CHECK_ETAG', true);
  } // if
  
  // Cache
  define('USE_CACHE', true);
  if (!defined('CACHE_BACKEND')) {
    if(extension_loaded('apc')) {
      define('CACHE_BACKEND', 'APCCacheBackend');
    } else {
      define('CACHE_BACKEND', 'FileCacheBackend');
    } // if
  } // if
  define('CACHE_LIFETIME', 7200);
  
  // ---------------------------------------------------
  //  Cookies
  // ---------------------------------------------------
  
  // Cookie...
  define('USE_COOKIES', true);
  
  if(!defined('COOKIE_DOMAIN')) {
    $parts = parse_url(ROOT_URL);
    if(is_array($parts) && isset($parts['host'])) {
      define('COOKIE_DOMAIN', $parts['host']);
    } else {
      define('COOKIE_DOMAIN', '');
    } // if
  } // if
  
  define('COOKIE_PATH', '/');
  if(substr(ROOT_URL, 0, 5) == 'https') {
    define('COOKIE_SECURE', 1);
  } else {
    define('COOKIE_SECURE', 0);
  } // if
  define('COOKIE_PREFIX', 'ac');
  
  // ---------------------------------------------------
  //  Defaults MVC mapping
  // ---------------------------------------------------
  
  define('DEFAULT_MODULE', 'system');
  define('DEFAULT_CONTROLLER', 'backend');
  define('DEFAULT_ACTION', 'index');
  define('DEFAULT_FORMAT', 'html');
  
  // ---------------------------------------------------
  //  Default date and time formats
  // ---------------------------------------------------
  
  // Formats can be overriden with constants with same name that start with
  // USER_ (USER_FORMAT_DATE will override FORMAT_DATE)
  if(!defined('FORMAT_DATETIME')) {
    if(DIRECTORY_SEPARATOR == '\\') {
      define('FORMAT_DATETIME', '%b %#d. %Y, %I:%M %p');
    } else {
      define('FORMAT_DATETIME', '%b %e. %Y, %I:%M %p');
    } // if
  } // if
  
  if(!defined('FORMAT_DATE')) {
    if(DIRECTORY_SEPARATOR == '\\') {
      define('FORMAT_DATE', '%b %#d. %Y');
    } else {
      define('FORMAT_DATE', '%b %e. %Y');
    } // if
  } // if
  
  if(!defined('FORMAT_TIME')) {
    define('FORMAT_TIME', '%I:%M %p');
  } // if
  
  // Read environment name from environment path
  define('ENVIRONMENT', substr(ENVIRONMENT_PATH, strrpos(ENVIRONMENT_PATH, '/') + 1));
  
  if(!defined('DEVELOPMENT_PATH')) {
    define('DEVELOPMENT_PATH', ROOT . '/development');
  } // if
  
  if(!defined('UPLOAD_PATH')) {
    define('UPLOAD_PATH', ENVIRONMENT_PATH . '/upload');
  } // if

  if(!defined('LIMIT_DISK_SPACE_USAGE')) {
    define('LIMIT_DISK_SPACE_USAGE', null);
  } // if

  if (!defined('BASIC_FILE_UPLOADS')) {
    define('BASIC_FILE_UPLOADS', false);
  } // if
  
  define('CUSTOM_PATH', ENVIRONMENT_PATH . '/custom');
  define('LOCALIZATION_PATH', CUSTOM_PATH . '/localization');
  define('IMPORT_PATH', ENVIRONMENT_PATH . '/import');
  define('THUMBNAILS_PATH', ENVIRONMENT_PATH . '/thumbnails');
  define('WORK_PATH', ENVIRONMENT_PATH . '/work');
  
  if (!defined('THEMES_PATH')) {
    define('THEMES_PATH', ENVIRONMENT_PATH . '/' . PUBLIC_FOLDER_NAME . '/assets/themes');
  } // if
  
  if (!defined('PROJECT_EXPORT_PATH')) {
    define('PROJECT_EXPORT_PATH', WORK_PATH . '/export');
  } // if
  
  if (!defined('MSPROJECT_EXPORT_PATH')) {
  	define('MSPROJECT_EXPORT_PATH', WORK_PATH . '/ms_project_export');
  } //if

  if (!defined('UPDATE_INSTRUCTIONS_URL')) {
    define('UPDATE_INSTRUCTIONS_URL', 'http://www.activecollab.com/docs/manuals/admin-version-3/upgrade/latest-stable');
  } // if

  if (!defined('UPGRADE_TO_CORPORATE_URL')) {
    define('UPGRADE_TO_CORPORATE_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/upgrade-to-corporate?license_key=' . LICENSE_KEY);
  } // if

  if (!defined('REMOVE_BRANDING_URL')) {
    define('REMOVE_BRANDING_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/purchase-branding-removal?license_key=' . LICENSE_KEY);
  } // if

  if (!defined('RENEW_SUPPORT_URL')) {
    define('RENEW_SUPPORT_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/extend-support?license_key=' . LICENSE_KEY);
  } // if