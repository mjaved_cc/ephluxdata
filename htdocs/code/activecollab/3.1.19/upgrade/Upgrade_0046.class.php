<?php

  /**
   * Update activeCollab 3.1.14 to activeCollab 3.1.15
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0046 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.1.14';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.1.15';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return null;
    } // getActions

  }