<?php

  /**
   * Update activeCollab 3.1.18 to activeCollab 3.1.19
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0050 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.1.18';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.1.19';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return array(
        'removeDeprecatedFonts' => 'Remove deprecated fonts',
        'scheduleIndexesRebuild' => 'Schedule index rebuild',
        'endUpgrade' => 'Finish upgrade',
      );
    } // getActions

    /**
     * Remove deprecated Fonts
     *
     * @return boolean
     */
    /**
     * Remove deprecated Fonts
     *
     * @return boolean
     */
    function removeDeprecatedFonts() {
      try {
        $config_option_name = 'invoice_template'; // config option name

        $raw_value = DB::executeFirstCell('SELECT value FROM ' . TABLE_PREFIX . 'config_options WHERE name = ?', $config_option_name);

        if($raw_value) {
          $invoice_template = unserialize($raw_value);

          if(is_array($invoice_template)) {
            $affected_properties = array(
              'header_font',
              'client_details_font',
              'invoice_details_font',
              'items_font',
              'note_font',
              'footer_font'
            );

            $replacements = array(
              'freesans' => 'dejavusans',
              'freesansb' => 'dejavusansb',
              'freesansbi' => 'dejavusansbi',
              'freesansi' => 'dejavusansi',
              'freeserif' => 'dejavuserif',
              'freeserifb' => 'dejavuserifb',
              'freeserifbi' => 'dejavuserifbi',
              'freeserifi' => 'dejavuserifi'
            );

            foreach($affected_properties as $affected_property) {
              $affected_font = isset($invoice_template[$affected_property]) ? $invoice_template[$affected_property] : null;

              if($affected_font && array_key_exists($affected_font, $replacements)) {
                $invoice_template[$affected_property] = $replacements[$affected_font];
              } // if
            } // foreach

            // update config option
            DB::execute('UPDATE ' . TABLE_PREFIX . 'config_options SET value = ? WHERE name = ?', serialize($invoice_template), $config_option_name);
          } // if
        } // if
      } catch (Exception $e) {
        return $e->getMessage();
      } // try

      return true;
    } // removeDeprecatedFonts

  }