<?php

  /**
   * Tracking module model options
   *
   * @package activeCollab.modules.tracking
   * @subpackage resources
   */
  
  $this->setTableOptions(
    array('tracking_reports', 'job_types', 'expense_categories'), array('module' => 'tracking', 'object_extends' => 'ApplicationObject', 'order_by' => 'name')
  );
  
  $this->setTableOptions(
    'estimates', array('module' => 'tracking', 'object_extends' => 'ApplicationObject', 'order_by' => 'created_on DESC')
  );
  
  $this->setTableOptions(
    array('expenses', 'time_records'), array('module' => 'tracking', 'object_extends' => 'TrackingObject')
  );