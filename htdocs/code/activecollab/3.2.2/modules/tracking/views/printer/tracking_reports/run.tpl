{title}Time & Expenses Report{/title}

<div id="print_container">
{if $records}
  {foreach $records as $records_group}
    {if is_foreachable($records_group.records)}
      <h3>{$records_group.label}</h3>
      <table class="common" cellspacing="0">
        <thead>
          <tr>
        	{if $report->getSumByUser()}
            <th class="user">{lang}User{/lang}</th>
            {if $report->queryTimeRecords()}<th class="time">{lang}Time{/lang}</th>{/if}
            {if $report->queryExpenses()}
              {foreach $currencies as $currency}
                <th class="expenses">{lang currency=$currency.code}Expenses (:currency){/lang}</th>
              {/foreach}
            {/if}
        	{else}
        	  {if $report->getGroupBy() != TrackingReport::GROUP_BY_DATE}<th class="date">{lang}Date{/lang}</th>{/if}
            <th class="value">{lang}Value{/lang}</th>
            <th class="user">{lang}User{/lang}</th>
            <th class="summary">{lang}Summary{/lang}</th>
            <th class="status">{lang}Status{/lang}</th>
            {if $report->getGroupBy() != TrackingReport::GROUP_BY_PROJECT}<th class="project">{lang}Project{/lang}</th>{/if}
        	{/if}
          </tr>
        </thead>
        <tbody>
        {foreach $records_group.records as $record}
          <tr>
          {if $report->getSumByUser()}
            <td class="user">{$record.user_name}</td>
          	{if $report->queryTimeRecords()}<td class="time">{$record.time|hours}h</td>{/if}
          	{if $report->queryExpenses()}
              {foreach $currencies as $currency_id => $currency}
                {assign_var name=expenses_for_currency}expenses_for_{$currency_id}{/assign_var}
                <td class="expenses">{$record.$expenses_for_currency|money}</td>
              {/foreach}
            {/if}
         	{else}
         		{if $report->getGroupBy() != TrackingReport::GROUP_BY_DATE}
         	  <td class="date">{$record.record_date|date:0}</td>
         	  {/if}
            <td class="value">
            {if $record.type == 'TimeRecord'}
            	{if $record.group_name}
            	  {lang hours=$record.value|hours job_type=$record.group_name}:hours of :job_type{/lang}
            	{else}
            	  {$record.value|hours}h
            	{/if}
            {else}
            	{if $record.group_name}
            	  {lang amount=$record.value|money category=$record.group_name}:amount in :category{/lang}
            	{else}
            	  {$record.value|money}
            	{/if}
            {/if}
            </td>
            <td class="user">{$record.user_name}</td>
            <td class="summary">
            {if ($record.parent_type == 'Task' && $record.parent_name) && $record.summary}
              {lang name=$record.parent_name}Task: :name{/lang} ({$record.summary})
            {elseif $record.parent_type == 'Task' && $record.parent_name}
              {lang name=$record.parent_name}Task: :name{/lang}
            {elseif $record.summary}
              {$record.summary}
            {/if}
            </td>
            <td class="status">
            {if $record.billable_status == $smarty.const.BILLABLE_STATUS_NOT_BILLABLE}
              {lang}Not Billable{/lang}
            {elseif $record.billable_status == $smarty.const.BILLABLE_STATUS_BILLABLE}
              {lang}Billable{/lang}
            {elseif $record.billable_status == $smarty.const.BILLABLE_STATUS_PENDING_PAYMENT}
              {lang}Pending Payment{/lang}
            {else $record.billable_status == $smarty.const.BILLABLE_STATUS_PAID}
              {lang}Paid{/lang}
            {/if}
            </td>
            {if $report->getGroupBy() != TrackingReport::GROUP_BY_PROJECT}<td class="project">{$record.project_name}</td>{/if}
          {/if}
          </tr>
        {/foreach}
        </tbody>
      </table>
    {/if}
  {/foreach}
{else}
  <p>{lang}Filter returned an empty result{/lang}</p>
{/if}
</div>