{title}Tracked Time{/title}
{add_bread_crumb}Tracked Time{/add_bread_crumb}

{if is_foreachable($records)}
  <div id="tracked_time_bar_chart">{tracked_time_expenses_bar_chart records=$records}</div>
{else}
  <p class="empty_page"><span class="inner">{lang}There is no tracked time{/lang}</span></p>
{/if}