{title}Time & Expenses{/title}
{add_bread_crumb}Log{/add_bread_crumb}

<div class="wireframe_content_wrapper"><div class="project_time_expenses_wrapper"><div class="project_time_expenses_wrapper_inner">
  <div id="project_time_expenses">

  </div>
</div></div></div>

<script type="text/javascript">
  $('#project_time_expenses').timeExpensesLog({
    'initial_data' : {$items|json nofilter},
    'parent_tasks' : {$parent_tasks|json nofilter},
    'currency_code' : {$active_project->getCurrency()->getCode()|json nofilter},
    'project_id' : {$active_project->getId()|json nofilter}
  });  
</script>