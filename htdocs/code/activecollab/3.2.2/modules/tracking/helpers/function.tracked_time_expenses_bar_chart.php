<?php 

  /**
   * Render bar chart time and expenses tracks 
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
	function smarty_function_tracked_time_expenses_bar_chart($params, &$smarty) {
		if(AngieApplication::isModuleLoaded('tracking')) {
			
			$records = array_var($params, 'records', array(), true);
			
			$bar_chart = new BarChart('60%', '400px', 'tracked_time_expenses_bar_chart_placeholder');
			$grouped_records = TimeRecords::groupByUserDaily($records);
			
			$series_array = array();
			foreach ($grouped_records as $user_data => $daily_record) {
				$points = array();
        $user_data = explode('||',$user_data);
        $user_email = first($user_data);
        $user_name = $user_data[1] ? $user_data[1] : 'Anonymous';
				foreach ($daily_record as $timestamp => $value) {
					$points[] = new ChartPoint($timestamp * 1000, $value);
				} //foreach
				$serie = new ChartSerie($points);
				$user = Users::findByEmail($user_email);
        if (!($user instanceof User)) {
          $user = new AnonymousUser($user_name,$user_email);
        } // if
				$serie->setOption('label', $user->getDisplayName());
				$series_array[] = $serie;
			} //foreach
			
			$bar_chart->setLegendPlaceholder();
			$bar_chart->addSeries($series_array);
			return $bar_chart->render(true);
		} //if
		return '';
	} //smarty_function_tracked_time_expenses_bar_chart