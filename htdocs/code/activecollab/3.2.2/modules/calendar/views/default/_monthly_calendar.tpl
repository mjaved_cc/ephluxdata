<div id="calendar">
<div class="calendar_shadow"><div class="calendar_shadow_left"></div><div class="calendar_shadow_center"></div><div class="calendar_shadow_right"></div></div>
<div class="calendar_header">
  <div class="calendar_header_left"></div>
  <div class="calendar_header_center">
    <a href="{$previous_month_url}" class="calendar_previous_month">{lang}Previous Month{/lang}</a>
      {lang}{$month_string}{/lang}, {$year}
    <a href="{$next_month_url}" class="calendar_next_month">{lang}Next Month{/lang}</a>
  </div>
  <div class="calendar_header_right"></div>
  <div class="calendar_header_metal_spiral"></div>
</div>

  <div class="calendar_table_wrapper" id="monthly_calendar"><div class="calendar_table_wrapper_inner"><div class="calendar_table_wrapper_inner_2">
    {$calendar->render() nofilter}
  </div></div></div>
  
  <script type="text/javascript">
    App.widgets.Calendar.init('calendar', 'monthly');
  </script>
</div>