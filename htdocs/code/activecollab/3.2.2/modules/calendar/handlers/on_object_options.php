<?php

  /**
   * Calendar module on_object_options event handler
   *
   * @package activeCollab.modules.calendar
   * @subpackage handlers
   */
  
  /**
   * Return array of options $logged_user can do to $user account
   *
   * @param ApplicationObject $object_id
   * @param IUser $user
   * @param NamedList $options
   * @param string $interface
   */
  function calendar_handle_on_object_options(&$object, &$user, &$options, $interface) {
    if($object instanceof User && can_access_profile_calendar($user, $object)) {
      $options->add('calendar', array(
        'text' => lang(":name's Calendar", array('name' => $object->getFirstName(true))),
        'icon' => AngieApplication::getImageUrl('icons/12x12/reschedule.png', ENVIRONMENT_FRAMEWORK),
        'url'  => Calendar::getProfileCalendarUrl($object),
      ));
    } // if
  } // calendar_handle_on_object_options