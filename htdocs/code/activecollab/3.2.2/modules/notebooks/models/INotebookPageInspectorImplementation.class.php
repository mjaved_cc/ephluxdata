<?php

  /**
   * Base Notebook Page Inspector implementation
   * 
   * @package activeCollab.modules.notebooks
   * @subpackage models
   */
  class INotebookPageInspectorImplementation extends IInspectorImplementation {
    
    /**
     * Load data for given interface
     * 
     * @param IUser $user
     * @param string $interface
     */
    public function load(IUser $user, $interface = AngieApplication::INTERFACE_DEFAULT) {
      parent::load($user, $interface);

      $this->supports_body = false;
      $this->optional_body_content = '<p class="empty_page">' . lang('This page has no content') . '</p>';
    } // load
    
  }