<?php

/**
 * Sharing implementation for notebooks
 *
 * @package activeCollab.modules.notebooks
 * @subpackage models
 */
class INotebookSharingImplementation extends ISharingImplementation {

  /**
   * Return sharing context
   *
   * @return string
   */
  function getSharingContext() {
    return Notebooks::SHARING_CONTEXT;
  } // getSharingContext

  /**
   * Returns true if this implementation has body text to display
   *
   * @return boolean
   */
  function hasSharedBody() {
    return true;
  } // hasSharedBody

  /**
   * Return prepared shared body
   *
   * @param Language $language
   * @return string
   */
  function getSharedBody(Language $language) {
    $result = '<div class="shard_notebook_wrapper">';
    $result.= '<div class="shared_notebook_page_tree">';
    $result.= '<h2><a href="' . $this->getUrl() . '">' . clean($this->object->getName()) . '</a></h2>';
    $result.= $this->renderSubpages($this->object);
    $result.= '</div>';

    $result.= '<div class="shared_notebook">';
    $result.= '<h2 class="main_title">' . clean($this->object->getName()) . '</h2>';
    if ($this->object->getBody()) {
      $result.= HTML::toRichText($this->object->getBody());
    } else {
      $result.= '<p class="empty_page">' . lang('Content not provided') . '</p>';
    } // if
    $result.= '</div>';
    $result.= '</div>';

    return $result;
  } // getSharedBody

  /**
   * Render links to subpages
   */
  function renderSubpages($object) {
    if ($object instanceof Notebook) {
      $subpages = NotebookPages::findByNotebook($object);
    } else if ($object instanceof NotebookPage) {
      $subpages = NotebookPages::findSubpages($object);
    } else {
      $object = $this->object;
      $subpages = NotebookPages::findByNotebook($object);
    } // if

    if (!is_foreachable($subpages)) {
      return null;
    } // if

    $result = '<ul>';

    foreach ($subpages as $subpage) {
      $result .= '<li><a href="' . $this->getPageUrl($subpage) . '">' . clean($subpage->getName()) . '</a>';
      $result .= $this->renderSubpages($subpage);
      $result .= '</li>';
    } // foreach

    $result.= '</ul>';

    return $result;
  } // renderSubpages

  /**
   * Return shared object URL
   *
   * @return string
   */
  function getUrl() {
    if($this->getSharingProfile() instanceof SharedObjectProfile) {
      return Router::assemble('shared_notebook', array(
        'sharing_code' => $this->getSharingProfile()->getSharingCode(),
      ));
    } else {
      throw new InvalidInstanceError('sharing_profile', $this->getSharingProfile(), 'SharedObjectProfile');
    } // if
  } // getUrl

  /**
   * Return URL proposal, based on $code value
   *
   * @param string $code
   * @return string
   */
  function getUrlProposal($code) {
    return Router::assemble('shared_notebook', array(
      'sharing_code' => $code,
    ));
  } // getUrlProposal

  /**
   * Returns the URL for shared page
   *
   * @param NotebookPage $page
   * @return string
   * @throws InvalidInstanceError
   */
  function getPageUrl(NotebookPage $page) {
    if($this->getSharingProfile() instanceof SharedObjectProfile) {
      return Router::assemble('shared_notebook_page', array(
        'sharing_code' => $this->getSharingProfile()->getSharingCode(),
        'notebook_page_id' => $page->getId()
      ));
    } else {
      throw new InvalidInstanceError('sharing_profile', $this->getSharingProfile(), 'SharedObjectProfile');
    } // if
  } // getPageUrl

}