<?php

  /**
   * on_build_names_search_index_for_project event handler implementation
   * 
   * @package activeCollab.modules.discussions
   * @subpackage handlers
   */

  /**
   * Handle on_build_names_search_index_for_project event
   * 
   * @param NamesSearchIndex $search_index
   * @param Project $project
   */
  function discussions_handle_on_build_names_search_index_for_project(NamesSearchIndex &$search_index, Project &$project) {
    $discussions = DB::execute("SELECT id, name, visibility FROM " . TABLE_PREFIX . "project_objects WHERE type = 'Discussion' AND project_id = ? AND state >= ?", $project->getId(), STATE_VISIBLE);
    
    if($discussions) {
      $project_id = $project->getId();
      
      foreach($discussions as $discussion) {
        $visibility = $discussion['visibility'] == VISIBILITY_PRIVATE ? 'private' : 'normal';
        
        Search::set($search_index, array(
          'class' => 'Discussion', 
          'id' => (integer) $discussion['id'], 
        	'context' => "projects:projects/$project_id/discussions/$visibility/$discussion[id]",
          'name' => $discussion['name'], 
          'visibility' => $discussion['visibility'],  
        ));
      } // foreach
    } // if
  } // discussions_handle_on_build_names_search_index_for_project