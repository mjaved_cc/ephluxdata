<div id="project_kickoff_discussions">
  {form action=$discussions_kickoff_url method=post}
    {wrap field=name}
      {label for=discussionSummary required=yes}Summary{/label}
      {text_field name="discussion[name]" value=$discussion_data.name id=discussionSummary class='title required validate_minlength 3'}
    {/wrap}
    
    {wrap field=body}
      {label for=discussionBody required=yes}Message{/label}
      {textarea_field name="discussion[body]" id=discussionBody class="validate_callback tiny_value_present"}{/textarea_field}
    {/wrap}
  
    {submit}Submit{/submit}
  {/form}
</div>