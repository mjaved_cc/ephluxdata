{title}Edit YouTube Video{/title}
{add_bread_crumb}Edit Youtube Video{/add_bread_crumb}

<div id="edit_you_tube_video">
  {form action=$active_asset->getEditUrl()}
    {include file=get_view_path('_you_tube_video_form', 'you_tube_videos', $smarty.const.FILES_MODULE)}
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>