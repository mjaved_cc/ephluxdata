<?php

/**
 * Youtube Videos sharing implementation
 *
 * @package activeCollab.modules.files
 * @subpackage models
 */
class IYoutubeVideoSharingImplementation extends IProjectAssetSharingImplementation {

  /**
   * Return sharing context
   *
   * @return string
   */
  function getSharingContext() {
    return ProjectAssets::YOUTUBE_SHARING_CONTEXT;
  } // getSharingContext

  /**
   * Returns true if this implementation has body text to display
   *
   * @return boolean
   */
  function hasSharedBody() {
    return true;
  } // hasSharedBody

  /**
   * Return prepared shared body
   *
   * @param string $interface
   * @return string
   */
  function getSharedBody($interface = AngieApplication::INTERFACE_DEFAULT) {
  	
  	// Default web interface
  	if($interface == AngieApplication::INTERFACE_DEFAULT) {
  		$result = '<div class="shared_youtube_video">';
	    $result.=   '<div class="real_preview">';
	    $result.=     $this->object->preview()->renderLarge();
	    $result.=   '</div>';
	    $result.=   '<div class="object_body_content formatted_content">';
	    if($this->object->getBody()) {
	      $result .= HTML::toRichText($this->object->getBody());
	    } // if
	    $result.=   '</div>';
	    $result.= '</div>';
  	
  	// Phone interface
	  } elseif($interface == AngieApplication::INTERFACE_PHONE) {
	  	$result = '<div class="shared_youtube_video">';
	  	$result.= 	'<div class="object_content">';
	    $result.= 		$this->object->preview()->renderLarge();
	    $result.=   	'<div class="wireframe_content_wrapper">';
	    $result.=   		'<div class="object_body_content">';
	    if($this->object->getBody()) {
	      $result .= HTML::toRichText($this->object->getBody());
	    } // if
	    $result.=   		'</div>';
	    $result.=   	'</div>';
	    $result.= 	'</div>';
	    $result.= '</div>';
	  } // if

    return $result;
  } // getSharedBody

}