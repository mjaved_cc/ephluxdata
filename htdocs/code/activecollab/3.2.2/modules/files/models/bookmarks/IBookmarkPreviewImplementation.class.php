<?php 

  /**
   * Bookmark preview implementation
   * 
   * @package activeCollab.modules.files
   * @subpackage models
   */
  class IBookmarkPreviewImplementation extends IPreviewImplementation {
    
    /**
     * Construct download preview implementation
     *
     * @param IPreview $object
     */
    function __construct(IPreview $object) {
      if($object instanceof Bookmark) {
        parent::__construct($object);
      } else {
        throw new InvalidInstanceError('object', $object, 'Bookmark');
      } // if
    } // __construct
        
    /**
     * Does this object has preview - as this is YouTubeVideo every youtube video has preview
     * 
     * @return boolean
     */
    function has() {
      return true;
    } // has
    
    /**
     * Render small preview
     *
     * @return string
     */
    function renderSmall() {
      return $this->renderPreview(80, 80);
    } // renderSmall
    
    /**
     * Render large preview
     *
     * @return string
     */
    function renderLarge() {
      return $this->renderPreview(550, 300);
    } // renderLarge
    
    /**
     * Renders the small icon url
     * 
     * @return string
     */
    function getSmallIconUrl() {
      return AngieApplication::getImageUrl('icons/16x16/bookmark.png', FILES_MODULE);
    } // getSmallIconUrl
    
    /**
     * Returns the large icon
     * 
     * @return string
     */
    function getLargeIconUrl() {
      return AngieApplication::getImageUrl('icons/32x32/bookmark.png', FILES_MODULE);
    } // getLargeIconUrl
    
    /**
     * Do the render
     * 
     * @param integer $width
     * @param integer $height
     * @return string
     */
    function renderPreview($width, $height) {
      return '<div class="bookmark_preview"><a href="' . clean($this->object->getBookmarkUrl()) . '" target="_blank">' . str_excerpt(clean($this->object->getBookmarkUrl()), 50) . '</a></div>';
    } // renderPreview
  }