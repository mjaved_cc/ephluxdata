<?php

  /**
   * Model generator options for files module
   *
   * @package activeCollab.modules.files
   * @subpackage resources
   */
  
  $this->setTableOptions(array('file_versions', 'text_document_versions'), array('module' => 'files', 'object_extends' => 'ApplicationObject'));