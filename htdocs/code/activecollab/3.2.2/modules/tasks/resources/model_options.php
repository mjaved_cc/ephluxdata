<?php

  /**
   * Model generator options for tasks module
   *
   * @package activeCollab.modules.tasks
   * @subpackage resources
   */
  
  $this->setTableOptions(array('public_task_forms'), array('module' => 'tasks', 'object_extends' => 'ApplicationObject'));