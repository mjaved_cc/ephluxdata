<?php

  /**
   * Model options
   * 
   * @package activeCollab.modules.source
   * @subpackage resources
   */
  
  $this->setTableOptions(array(
      'source_paths',
      'source_commits',
      'source_repositories',
      'source_users',
      'commit_project_objects',
    ), array('module' => 'source', 'object_extends' => 'ApplicationObject'));
  
  $this->setTableOptions('source_repositories', array(
    'module' => 'source', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));

  $this->setTableOptions('source_commits', array(
    'module' => 'source', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));

  $this->setTableOptions('source_paths', array(
    'module' => 'source', 'object_extends' => 'ApplicationObject'
  ));

  $this->setTableOptions('source_users', array(
    'module' => 'source', 'object_extends' => 'ApplicationObject'
  ));

  $this->setTableOptions('commit_project_objects', array(
    'module' => 'source', 'object_extends' => 'ApplicationObject'
  ));