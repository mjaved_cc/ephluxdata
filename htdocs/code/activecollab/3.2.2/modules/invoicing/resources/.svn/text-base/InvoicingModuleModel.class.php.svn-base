<?php

  // Include application specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

  /**
   * Invoicing module model definition
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
	class InvoicingModuleModel extends ActiveCollabModuleModel {
    
    /**
     * Construct invoicing module model definition
     *
     * @param InvoicingModule $parent
     */
		function __construct(InvoicingModule $parent) {
      parent::__construct($parent);
      
      $this->addModel(DB::createTable('invoices')->addColumns(array(
        DBIdColumn::create(), 
        DBStringColumn::create('based_on_type', 50), 
        DBIntegerColumn::create('based_on_id', 10)->setUnsigned(true),
        DBIntegerColumn::create('company_id', 5, 0)->setUnsigned(true), 
        DBIntegerColumn::create('project_id', 5)->setUnsigned(true), 
        DBIntegerColumn::create('currency_id', 4, 0), 
        DBIntegerColumn::create('language_id', 3, 0), 
        DBStringColumn::create('number', 50), 
        DBTextColumn::create('company_address'), 
        DBStringColumn::create('comment', 255), 
        DBTextColumn::create('note'), 
        DBIntegerColumn::create('status', 4, '0'), 
        DBActionOnByColumn::create('issued', true), 
        DBIntegerColumn::create('issued_to_id', 11), 
        DBDateColumn::create('due_on'), 
        DBActionOnByColumn::create('closed', true), 
        DBActionOnByColumn::create('created'), 
        DBIntegerColumn::create('allow_payments', 3)->setSize(DBColumn::TINY), 
      ))->addIndices(array(
        DBIndex::create('company_id'), 
        DBIndex::create('project_id'), 
        DBIndex::create('number'), 
        DBIndex::create('issued_on'), 
        DBIndex::create('due_on'), 
        DBIndex::create('closed_on'), 
      )));
      
      $this->addModel(DB::createTable('invoice_items')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('invoice_id', 5, '0')->setUnsigned(true), 
        DBIntegerColumn::create('position', 11), 
        DBIntegerColumn::create('tax_rate_id', 3, '0')->setUnsigned(true), 
        DBStringColumn::create('description', 255), 
        DBDecimalColumn::create('quantity', 12, 2, '1')->setUnsigned(true), 
        DBMoneyColumn::create('unit_cost', 0), 
      ))->addIndices(array(
        DBIndex::create('invoice_id', DBIndex::KEY, array('invoice_id', 'position')), 
      )));
      
      $this->addModel(DB::createTable('invoice_item_templates')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('tax_rate_id', 3, '0')->setUnsigned(true), 
        DBStringColumn::create('description', 255), 
        DBDEcimalColumn::create('quantity', 12, 2, 1)->setUnsigned(true), 
        DBMOneyColumn::create('unit_cost', 0), 
        DBIntegerColumn::create('position', 11, 0), 
      )));
      
      $this->addModel(DB::createTable('invoice_note_templates')->addColumns(array(
        DBIdColumn::create(), 
        DBNameColumn::create(150, true), 
        DBTextColumn::create('content'), 
        DBIntegerColumn::create('position', 10, 0)->setUnsigned(true), 
      )));
      
      $this->addTable(DB::createTable('invoice_related_records')->addColumns(array(
        DBIntegerColumn::create('invoice_id', 5)->setUnsigned(true), 
        DBIntegerColumn::create('item_id', 10)->setUnsigned(true),
        DBParentColumn::create(true), 
      ))->addIndices(array(
        DBIndexPrimary::create(array('invoice_id', 'item_id', 'parent_type', 'parent_id')),
      )));
      
      $this->addModel(DB::createTable('quotes')->addColumns(array(
    	  DBIdColumn::create(), 
    	  DBStringColumn::create('public_id', 32, ''),
        DBStringColumn::create('based_on_type', 50),
        DBIntegerColumn::create('based_on_id', 10)->setUnsigned(true),
    	  DBIntegerColumn::create('company_id', 5, '0')->setUnsigned(true),
        DBStringColumn::create('company_name', 150),
    	  DBTextColumn::create('company_address'), 
    	  DBIntegerColumn::create('currency_id', 4, '0')->setUnsigned(true), 
    	  DBIntegerColumn::create('language_id', 3, '0')->setUnsigned(true), 
    	  DBNameColumn::create(150), 
    	  DBTextColumn::create('note'), 
    	  DBTextColumn::create('private_note'),
    	  DBIntegerColumn::create('status', 4, '0'), 
    	  DBActionOnByColumn::create('created'),
    	  DBActionOnByColumn::create('sent'),
        DBIntegerColumn::create('recipient_id', 10, '0')->setUnsigned(true),
        DBStringColumn::create('recipient_name', 100),
        DBStringColumn::create('recipient_email', 150),
    	  DBIntegerColumn::create('sent_to_id', 10, '0')->setUnsigned(true),
        DBStringColumn::create('sent_to_name', 100),
        DBStringColumn::create('sent_to_email', 150),
    	  DBActionOnByColumn::create('closed'),
    	  DBBoolColumn::create('is_locked', false), 
    	  DBDateTimeColumn::create('last_comment_on'), 
    	))->addIndices(array(
    	  DBIndex::create('based_on_id'), 
    	)));
    	
    	$this->addModel(DB::createTable('quote_items')->addColumns(array(
    	  DBIdColumn::create(), 
    	  DBIntegerColumn::create('quote_id', 5, '0')->setUnsigned(true), 
    	  DBIntegerColumn::create('position', 11, 0), 
    	  DBIntegerColumn::create('tax_rate_id', 3, '0')->setUnsigned(true), 
    	  DBStringColumn::create('description', 255, ''), 
    	  DBDecimalColumn::create('quantity', 12, 2, '1')->setUnsigned(true), 
    	  DBMOneyColumn::create('unit_cost', 0), 
    	))->addIndices(array(
    	  DBIndex::create('quote_id'), 
    	  DBIndex::create('position'), 
    	)));
            
      $this->addModel(DB::createTable('recurring_profiles')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('company_id', 11), 
        DBTextColumn::create('company_address'), 
        DBIntegerColumn::create('currency_id', 11), 
        DBIntegerColumn::create('language_id', 11), 
        DBNameColumn::create(150), 
        DBTextColumn::create('note'), 
        DBTextColumn::create('our_comment'), 
        DBActionOnByColumn::create('created'),
        DBDateColumn::create('start_on'), 
        DBStringColumn::create('frequency', 150), 
        DBIntegerColumn::create('occurrences',10), 
        DBBoolColumn::create('auto_issue'),
        DBIntegerColumn::create('invoice_due_after'),  
        DBStringColumn::create('allow_payments', 100), 
        DBIntegerColumn::create('project_id', 10), 
        DBIntegerColumn::create('state', 3), 
        DBIntegerColumn::create('original_state', 3), 
        DBIntegerColumn::create('triggered_number', 11), 
        DBDateColumn::create('last_triggered_on'), 
        DBDateColumn::create('next_trigger_on'),
        DBIntegerColumn::create('visibility',0),
        DBUserColumn::create('recipient') 
      )));
      
      $this->addModel(DB::createTable('recurring_profile_items')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('recurring_profile_id', 11), 
        DBIntegerColumn::create('position', 5), 
        DBIntegerColumn::create('tax_rate_id', 11), 
        DBStringColumn::create('description', 255), 
        DBDecimalColumn::create('quantity'), 
        DBMoneyColumn::create('unit_cost'), 
      ))->addIndices(array(
        DBIndex::create('recurring_profile_id'), 
        DBIndex::create('position'), 
      )));
     
    } // __construct
    
    /**
     * Load initial module data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
      $this->addConfigOption('prefered_currency');
      $this->addConfigOption('invoicing_number_pattern', ':invoice_in_year/:current_year');
      $this->addConfigOption('invoicing_number_date_counters');
      $this->addConfigOption('invoicing_number_counter_padding');
      $this->addConfigOption('invoice_template');
      $this->addConfigOption('print_invoices_as');
      $this->addConfigOption('print_proforma_invoices_as');
      $this->addConfigOption('on_invoice_based_on', 'sum_all');
      $this->addConfigOption('invoicing_default_due', 15);

      // Job types
      $job_types_table = TABLE_PREFIX . 'job_types';
      
      if(AngieApplication::isModuleLoaded('tracking') && DB::tableExists($job_types_table) && DB::executeFirstCell("SELECT COUNT(id) FROM $job_types_table") == 0) {
        $this->loadTableData('job_types', array(
          array(
            'name' => 'General', 
            'default_hourly_rate' => 1, 
            'is_default' => true, 
          ), 
        ));
      } // if
      
      // Tax rates
      $tax_rates_table = TABLE_PREFIX . 'tax_rates';
      
      if(DB::tableExists($tax_rates_table) && DB::executeFirstCell("SELECT COUNT(id) FROM $tax_rates_table WHERE name='VAT'") == 0) {
        $this->loadTableData('tax_rates', array(
          array(
            'name' => 'VAT', 
            'percentage' => 17.5, 
          )
        ));
      }//if
      
      parent::loadInitialData($environment);
    } // loadInitialData
    
  }