<?php

  /**
   * invoice_note helper implementation
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage helpers
   */

  /**
   * Render invoice note field
   * 
   * @param array $params
   * @param string $content
   * @param Smarty $smarty
   * @param boolean $repeat
   */
  function smarty_block_invoice_note($params, $content, &$smarty, &$repeat) {
    if($repeat) {
      return;
    } // if
    
    $id = isset($params['id']) && $params['id'] ? $params['id'] : HTML::uniqueId('invoice_note');
    
    $settings = array(
      'name' => array_required_var($params, 'name'), 
      'value' => $content, 
      'label' => isset($params['label']) && $params['label'] ? lang($params['label']) : null,
      'notes' => array(), 
      'required' => isset($params['required']) && $params['required'], 
    );
    
    $notes = InvoiceNoteTemplates::find();
    
    if($notes) {
      foreach($notes as $note) {
        $settings['notes'][$note->getId()] = array(
          'name' => $note->getName(), 
          'content' => $note->getContent(), 
        );
      } // foreach
    } // if
    
    return '<div id="' . $id . '"></div><script type="text/javascript">$("#' . $id . '").invoiceNote(' . JSON::encode($settings) . ');</script>';
  } // smarty_block_invoice_note