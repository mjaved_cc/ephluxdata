<?php

  /**
   * on_project_additional_step event handler
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage handlers
   */

  /**
   * Handle on_project_additional_step event
   * 
   * @param string $step
   * @param Project $project
   * @param User $user
   */
  function invoicing_handle_on_project_additional_step($step, Project &$project, User &$user) {
    
    // Import comments as a discussion
    if($step == Quotes::STEP_IMPORT_DISCUSSION) {
      $quote = $project->getBasedOn();
      
      if($quote instanceof Quote) {
        $quote->copyComments($project, $user);
      } // if
      
    // Import quote items as milestones
    } elseif($step == Quotes::STEP_IMPORT_MILESTONES) {
      $quote = $project->getBasedOn();
      $template = $project->getTemplate();
      
      if($quote instanceof Quote && !($template instanceof Project)) {
        $quote->createMilestones($project, $user);
      } // if
    } // if
    
  } // invoicing_handle_on_project_additional_step