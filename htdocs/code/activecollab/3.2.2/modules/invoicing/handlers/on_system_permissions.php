<?php

  /**
   * Invoicing on_system_permissions handler
   *
   * @package activeCollab.modules.invoicing
   * @subpackage handlers
   */
  
  /**
   * Handle on_system_permissions
   *
   * @param NamedList $permissions
   */
  function invoicing_handle_on_system_permissions(NamedList &$permissions) {
    $permissions->add('can_manage_finances', array(
      'name' => lang('Manage Finances'), 
      'description' => lang('This permissions enables users to view existing and create new invoices, manage payments, as well as to run all invoicing and payment related reports'), 
      'depends_on' => 'has_system_access',
    ));

    $permissions->add('can_manage_quotes', array(
      'name' => lang('Manage Quotes'),
      'description' => lang('This permissions enables users to manage quotes within Invoicing module'),
      'depends_on' => 'has_system_access',
    ));
  } // invoicing_handle_on_system_permissions