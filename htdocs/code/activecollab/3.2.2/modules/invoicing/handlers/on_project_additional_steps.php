<?php

  /**
   * on_project_additional_steps event handler
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage handlers
   */

  /**
   * Handle on_project_additional_steps event
   * 
   * @param NamedList $steps
   * @param Project $project
   * @param IUser $user
   */
  function invoicing_handle_on_project_additional_steps(NamedList &$steps, Project &$project, &$user) {
    if($project->getBasedOn() instanceof Quote) {
      $steps->add(Quotes::STEP_IMPORT_DISCUSSION, lang('Import quote discussion'));
      $steps->add(Quotes::STEP_IMPORT_MILESTONES, lang('Import quote items as milestones'));
    } // if
  } // invoicing_handle_on_project_additional_steps