{title}Item Templates{/title}
{add_bread_crumb}View All{/add_bread_crumb}

<div id="invoice_items"></div>

<script type="text/javascript">
  $('#invoice_items').pagedObjectsList({
    'load_more_url' : '{assemble route=admin_invoicing_items}', 
    'items' : {$invoice_item_templates|json nofilter},
    'items_per_load' : {$items_per_page}, 
    'total_items' : {$total_items}, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'item_templates' }, 
    'columns' : {
      'description' : App.lang('Description'), 
      'tax_rate' : App.lang('Tax Rate'), 
      'quantity' : App.lang('Quantity'),
      'unit_cost' : App.lang('Unit Cost'), 
      'options' : '' 
    }, 
    'sort_by' : 'description', 
    'empty_message' : App.lang('There are no invoice items defined'), 
    'listen' : 'invoice_item_template', 
    'on_add_item' : function(item) {
      var invoice_item_template = $(this);
      
      invoice_item_template.append(
        '<td class="description"></td>' +
       	'<td class="tax_rate"></td>' + 
       	'<td class="quantity"></td>' +
       	'<td class="unit_cost"></td>' + 
       	'<td class="options"></td>'
      );

      invoice_item_template.attr('id',item['id']);
      invoice_item_template.find('td.description').text(item['description']);

      if(item['tax_rate']) {
      	invoice_item_template.find('td.tax_rate').text(item['tax_rate']['name'].clean());
      } else {
        invoice_item_template.find('td.tax_rate').html('<i>' + App.lang('No Tax') + '</i>');
      } // if
      
      invoice_item_template.find('td.quantity').text(item['quantity']);
      invoice_item_template.find('td.unit_cost').text(item['unit_cost']);
      
      invoice_item_template.find('td.options')
        .append('<a href="' + item['urls']['edit'] + '" class="edit_note" title="' + App.lang('Change Settings') + '"><img src="{image_url name="icons/12x12/edit.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_note" title="' + App.lang('Remove Item') + '"><img src="{image_url name="icons/12x12/delete.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" /></a>')
      ;

      invoice_item_template.find('td.options a.note_details').flyout();
      invoice_item_template.find('td.options a.edit_note').flyoutForm({
        'success_event' : 'invoice_item_template_updated',
        'width' : 480
      });
      invoice_item_template.find('td.options a.delete_note').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this item?'), 
        'success_event' : 'invoice_item_template_deleted', 
        'success_message' : App.lang('Item has been deleted successfully')
      });
    }
  });
</script>