<div id="empty_slate_send_quote" class="empty_slate">
  <h3>{lang}About Project Creation{/lang}</h3>
  
  <ul class="icon_list">
    <li>
      <img src="{image_url name="empty-slates/bulb.png" module=$smarty.const.ENVIRONMENT_FRAMEWORK}" class="icon_list_icon" alt="" />
      <span class="icon_list_title">{lang}Lorem Ipsum is simply dummy text{/lang}</span>
      <span class="icon_list_description">{lang}It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages{/lang}.</span>
    </li>
  </ul>
</div>