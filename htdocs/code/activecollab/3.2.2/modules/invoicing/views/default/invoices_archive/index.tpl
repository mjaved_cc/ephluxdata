{title}Archive{/title}
{add_bread_crumb}Statistics{/add_bread_crumb}

{wrap_columns}
  {wrap_content_column}
    {if is_foreachable($invoiced_companies)}
      <div id="invoices_archive">
        <table>
          <tr>
            <th class="name">{lang}Client{/lang}</th>
            <th class="count">{lang}Total Invoices{/lang}</th>
            <th class="paid">{lang}Total Paid{/lang}</th>
          </tr>
        {foreach from=$invoiced_companies item=company_info}
          <tr class="{cycle values='odd,even'}">
            <td class="name"><a href="{assemble route=company_invoices company_id=$company_info.id}">{$company_info.name}</td>
            <td class="count">{$company_info.invoices_count}</td>
            <td class="paid">$1230</td>
          </tr>
        {/foreach}
        </table>
      </div>
    {else}
      <p class="empty_page"><span class="inner">{lang}There are no archived invoices in the database{/lang}</span></p>
    {/if}
  {/wrap_content_column}
  
  {wrap_sidebar_column}
    <ul class="category_list">
      <li><a href="{assemble route=invoices}"><span>{lang}Active Invoices{/lang}</span></a></li>
      <li><a href="{assemble route=invoice_payments}"><span>{lang}Payments{/lang}</span></a></li>
      <li class="selected"><a href="{assemble route=invoices_archive}"><span>{lang}Archive{/lang}</span></a></li>
    </ul>
  {/wrap_sidebar_column}
{/wrap_columns}