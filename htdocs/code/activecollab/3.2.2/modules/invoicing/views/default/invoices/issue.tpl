{title}Issue Invoice{/title}
{add_bread_crumb}Issue{/add_bread_crumb}

<div id="issue_invoice">
  {form action=$active_invoice->getIssueUrl() method=post}
    {wrap_fields}
      <div id="issue_invoice_dates">
  	      {wrap field=issued_on}
  	        {select_date name='issue[issued_on]' value=$issue_data.issued_on required=true label='Issued On'}
  	      {/wrap}

  	      {wrap field=due_in_days}
  	        {select_invoice_due_on name='issue[due_in_days]' value=$issue_data.due_in_days required=true label='Payment Due On'}
  	      {/wrap}
	    </div>
	    
	    {if $client_company_managers || $client_company_manager_roles}
  	    <div id="issue_invoice_send_email">
  	  		{label}Notify Client{/label}
  	  		
    	  {if $client_company_managers}
    	    <p><input type="radio" name="issue[send_emails]" value="1" {if $issue_data.send_emails}checked="checked"{/if} class="send_mails_radio inline input_radio" id="issueFormSendEmailsYes"> {label for="issueFormSendEmailsYes" main_label=false after_text=''}Send email to client{/label}</p>
    	    <div id="select_invoice_recipients" style="display: none">
    	      {select_user name='issue[user_id]' value=$issue_data.user_id users=$client_company_managers user=$logged_user}
            <p id="issue_invoice_send_email_pdf">{lang}PDF version of the invoice will be attached to the email{/lang}</p>
    	    </div>
    	    <p><input type="radio" name="issue[send_emails]" value="0" {if !$issue_data.send_emails}checked="checked"{/if} class="send_mails_radio inline input_radio" id="issueFormSendEmailsNo"> {label for="issueFormSendEmailsNo" main_label=false after_text=''}Don't send emails, but mark invoice as issued{/label}</p>
    	    
    	    <script type="text/javascript">
          	$("#issue_invoice .send_mails_radio").click(function(){
          		var obj = $(this);
          		if(obj.val() == '1') {
          			$("#select_invoice_recipients").slideDown();
          		} else {
          			$("#select_invoice_recipients").slideUp();
          		}//if
          	});
          </script>
        {else}
        	<p>{lang}System is able to send a copy of issued invoice to the client, but to do that, there needs to be at least one user in the client company with one of the following roles{/lang}:</p>
        	
        	{if $client_company_manager_roles}
        	<ul>
        	{foreach $client_company_manager_roles as $client_company_manager_role}
        	  <li>{$client_company_manager_role->getName()}</li>
        	{/foreach}
        	</ul>
        	{/if}
        	<input type="hidden" name="issue[send_emails]" value="0">
        {/if}
      	</div>
      {else}
        <input type="hidden" name="issue[send_emails]" value="0">
    	{/if}
    {/wrap_fields}
    
    {wrap_buttons}
      {submit}Issue Invoice{/submit}
    {/wrap_buttons}
  {/form}
</div>