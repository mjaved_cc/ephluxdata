<?php

  /**
   * BaseInvoiceNoteTemplate class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseInvoiceNoteTemplate extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'invoice_note_templates';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'position', 'name', 'content');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @param void
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of position field
     *
     * @param void
     * @return integer
     */
    function getPosition() {
      return $this->getFieldValue('position');
    } // getPosition
    
    /**
     * Set value of position field
     *
     * @param integer $value
     * @return integer
     */
    function setPosition($value) {
      return $this->setFieldValue('position', $value);
    } // setPosition

    /**
     * Return value of name field
     *
     * @param void
     * @return string
     */
    function getName() {
      return $this->getFieldValue('name');
    } // getName
    
    /**
     * Set value of name field
     *
     * @param string $value
     * @return string
     */
    function setName($value) {
      return $this->setFieldValue('name', $value);
    } // setName

    /**
     * Return value of content field
     *
     * @param void
     * @return string
     */
    function getContent() {
      return $this->getFieldValue('content');
    } // getContent
    
    /**
     * Set value of content field
     *
     * @param string $value
     * @return string
     */
    function setContent($value) {
      return $this->setFieldValue('content', $value);
    } // setContent

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'position':
          return parent::setFieldValue($real_name, intval($value));
        case 'name':
          return parent::setFieldValue($real_name, strval($value));
        case 'content':
          return parent::setFieldValue($real_name, strval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

?>