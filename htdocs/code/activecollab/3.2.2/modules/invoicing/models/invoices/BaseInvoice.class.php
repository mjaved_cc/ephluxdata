<?php


  /**
   * BaseInvoice class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseInvoice extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'invoices';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'based_on_type', 'based_on_id', 'company_id', 'project_id', 'currency_id', 'language_id', 'number', 'company_address', 'comment', 'note', 'status', 'issued_on', 'issued_by_id', 'issued_by_name', 'issued_by_email', 'issued_to_id', 'due_on', 'closed_on', 'closed_by_id', 'closed_by_name', 'closed_by_email', 'created_on', 'created_by_id', 'created_by_name', 'created_by_email', 'allow_payments');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of based_on_type field
     *
     * @return string
     */
    function getBasedOnType() {
      return $this->getFieldValue('based_on_type');
    } // getBasedOnType
    
    /**
     * Set value of based_on_type field
     *
     * @param string $value
     * @return string
     */
    function setBasedOnType($value) {
      return $this->setFieldValue('based_on_type', $value);
    } // setBasedOnType

    /**
     * Return value of based_on_id field
     *
     * @return integer
     */
    function getBasedOnId() {
      return $this->getFieldValue('based_on_id');
    } // getBasedOnId
    
    /**
     * Set value of based_on_id field
     *
     * @param integer $value
     * @return integer
     */
    function setBasedOnId($value) {
      return $this->setFieldValue('based_on_id', $value);
    } // setBasedOnId

    /**
     * Return value of company_id field
     *
     * @return integer
     */
    function getCompanyId() {
      return $this->getFieldValue('company_id');
    } // getCompanyId
    
    /**
     * Set value of company_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCompanyId($value) {
      return $this->setFieldValue('company_id', $value);
    } // setCompanyId

    /**
     * Return value of project_id field
     *
     * @return integer
     */
    function getProjectId() {
      return $this->getFieldValue('project_id');
    } // getProjectId
    
    /**
     * Set value of project_id field
     *
     * @param integer $value
     * @return integer
     */
    function setProjectId($value) {
      return $this->setFieldValue('project_id', $value);
    } // setProjectId

    /**
     * Return value of currency_id field
     *
     * @return integer
     */
    function getCurrencyId() {
      return $this->getFieldValue('currency_id');
    } // getCurrencyId
    
    /**
     * Set value of currency_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCurrencyId($value) {
      return $this->setFieldValue('currency_id', $value);
    } // setCurrencyId

    /**
     * Return value of language_id field
     *
     * @return integer
     */
    function getLanguageId() {
      return $this->getFieldValue('language_id');
    } // getLanguageId
    
    /**
     * Set value of language_id field
     *
     * @param integer $value
     * @return integer
     */
    function setLanguageId($value) {
      return $this->setFieldValue('language_id', $value);
    } // setLanguageId

    /**
     * Return value of number field
     *
     * @return string
     */
    function getNumber() {
      return $this->getFieldValue('number');
    } // getNumber
    
    /**
     * Set value of number field
     *
     * @param string $value
     * @return string
     */
    function setNumber($value) {
      return $this->setFieldValue('number', $value);
    } // setNumber

    /**
     * Return value of company_address field
     *
     * @return string
     */
    function getCompanyAddress() {
      return $this->getFieldValue('company_address');
    } // getCompanyAddress
    
    /**
     * Set value of company_address field
     *
     * @param string $value
     * @return string
     */
    function setCompanyAddress($value) {
      return $this->setFieldValue('company_address', $value);
    } // setCompanyAddress

    /**
     * Return value of comment field
     *
     * @return string
     */
    function getComment() {
      return $this->getFieldValue('comment');
    } // getComment
    
    /**
     * Set value of comment field
     *
     * @param string $value
     * @return string
     */
    function setComment($value) {
      return $this->setFieldValue('comment', $value);
    } // setComment

    /**
     * Return value of note field
     *
     * @return string
     */
    function getNote() {
      return $this->getFieldValue('note');
    } // getNote
    
    /**
     * Set value of note field
     *
     * @param string $value
     * @return string
     */
    function setNote($value) {
      return $this->setFieldValue('note', $value);
    } // setNote

    /**
     * Return value of status field
     *
     * @return integer
     */
    function getStatus() {
      return $this->getFieldValue('status');
    } // getStatus
    
    /**
     * Set value of status field
     *
     * @param integer $value
     * @return integer
     */
    function setStatus($value) {
      return $this->setFieldValue('status', $value);
    } // setStatus

    /**
     * Return value of issued_on field
     *
     * @return DateTimeValue
     */
    function getIssuedOn() {
      return $this->getFieldValue('issued_on');
    } // getIssuedOn
    
    /**
     * Set value of issued_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setIssuedOn($value) {
      return $this->setFieldValue('issued_on', $value);
    } // setIssuedOn

    /**
     * Return value of issued_by_id field
     *
     * @return integer
     */
    function getIssuedById() {
      return $this->getFieldValue('issued_by_id');
    } // getIssuedById
    
    /**
     * Set value of issued_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setIssuedById($value) {
      return $this->setFieldValue('issued_by_id', $value);
    } // setIssuedById

    /**
     * Return value of issued_by_name field
     *
     * @return string
     */
    function getIssuedByName() {
      return $this->getFieldValue('issued_by_name');
    } // getIssuedByName
    
    /**
     * Set value of issued_by_name field
     *
     * @param string $value
     * @return string
     */
    function setIssuedByName($value) {
      return $this->setFieldValue('issued_by_name', $value);
    } // setIssuedByName

    /**
     * Return value of issued_by_email field
     *
     * @return string
     */
    function getIssuedByEmail() {
      return $this->getFieldValue('issued_by_email');
    } // getIssuedByEmail
    
    /**
     * Set value of issued_by_email field
     *
     * @param string $value
     * @return string
     */
    function setIssuedByEmail($value) {
      return $this->setFieldValue('issued_by_email', $value);
    } // setIssuedByEmail

    /**
     * Return value of issued_to_id field
     *
     * @return integer
     */
    function getIssuedToId() {
      return $this->getFieldValue('issued_to_id');
    } // getIssuedToId
    
    /**
     * Set value of issued_to_id field
     *
     * @param integer $value
     * @return integer
     */
    function setIssuedToId($value) {
      return $this->setFieldValue('issued_to_id', $value);
    } // setIssuedToId

    /**
     * Return value of due_on field
     *
     * @return DateValue
     */
    function getDueOn() {
      return $this->getFieldValue('due_on');
    } // getDueOn
    
    /**
     * Set value of due_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setDueOn($value) {
      return $this->setFieldValue('due_on', $value);
    } // setDueOn

    /**
     * Return value of closed_on field
     *
     * @return DateTimeValue
     */
    function getClosedOn() {
      return $this->getFieldValue('closed_on');
    } // getClosedOn
    
    /**
     * Set value of closed_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setClosedOn($value) {
      return $this->setFieldValue('closed_on', $value);
    } // setClosedOn

    /**
     * Return value of closed_by_id field
     *
     * @return integer
     */
    function getClosedById() {
      return $this->getFieldValue('closed_by_id');
    } // getClosedById
    
    /**
     * Set value of closed_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setClosedById($value) {
      return $this->setFieldValue('closed_by_id', $value);
    } // setClosedById

    /**
     * Return value of closed_by_name field
     *
     * @return string
     */
    function getClosedByName() {
      return $this->getFieldValue('closed_by_name');
    } // getClosedByName
    
    /**
     * Set value of closed_by_name field
     *
     * @param string $value
     * @return string
     */
    function setClosedByName($value) {
      return $this->setFieldValue('closed_by_name', $value);
    } // setClosedByName

    /**
     * Return value of closed_by_email field
     *
     * @return string
     */
    function getClosedByEmail() {
      return $this->getFieldValue('closed_by_email');
    } // getClosedByEmail
    
    /**
     * Set value of closed_by_email field
     *
     * @param string $value
     * @return string
     */
    function setClosedByEmail($value) {
      return $this->setFieldValue('closed_by_email', $value);
    } // setClosedByEmail

    /**
     * Return value of created_on field
     *
     * @return DateTimeValue
     */
    function getCreatedOn() {
      return $this->getFieldValue('created_on');
    } // getCreatedOn
    
    /**
     * Set value of created_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setCreatedOn($value) {
      return $this->setFieldValue('created_on', $value);
    } // setCreatedOn

    /**
     * Return value of created_by_id field
     *
     * @return integer
     */
    function getCreatedById() {
      return $this->getFieldValue('created_by_id');
    } // getCreatedById
    
    /**
     * Set value of created_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCreatedById($value) {
      return $this->setFieldValue('created_by_id', $value);
    } // setCreatedById

    /**
     * Return value of created_by_name field
     *
     * @return string
     */
    function getCreatedByName() {
      return $this->getFieldValue('created_by_name');
    } // getCreatedByName
    
    /**
     * Set value of created_by_name field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByName($value) {
      return $this->setFieldValue('created_by_name', $value);
    } // setCreatedByName

    /**
     * Return value of created_by_email field
     *
     * @return string
     */
    function getCreatedByEmail() {
      return $this->getFieldValue('created_by_email');
    } // getCreatedByEmail
    
    /**
     * Set value of created_by_email field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByEmail($value) {
      return $this->setFieldValue('created_by_email', $value);
    } // setCreatedByEmail

    /**
     * Return value of allow_payments field
     *
     * @return integer
     */
    function getAllowPayments() {
      return $this->getFieldValue('allow_payments');
    } // getAllowPayments
    
    /**
     * Set value of allow_payments field
     *
     * @param integer $value
     * @return integer
     */
    function setAllowPayments($value) {
      return $this->setFieldValue('allow_payments', $value);
    } // setAllowPayments

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'based_on_type':
          return parent::setFieldValue($real_name, strval($value));
        case 'based_on_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'company_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'project_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'currency_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'language_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'number':
          return parent::setFieldValue($real_name, strval($value));
        case 'company_address':
          return parent::setFieldValue($real_name, strval($value));
        case 'comment':
          return parent::setFieldValue($real_name, strval($value));
        case 'note':
          return parent::setFieldValue($real_name, strval($value));
        case 'status':
          return parent::setFieldValue($real_name, intval($value));
        case 'issued_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'issued_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'issued_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'issued_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'issued_to_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'due_on':
          return parent::setFieldValue($real_name, dateval($value));
        case 'closed_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'closed_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'closed_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'closed_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'created_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'created_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'allow_payments':
          return parent::setFieldValue($real_name, intval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

