<?php

  /**
   * BaseQuoteItem class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  abstract class BaseQuoteItem extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'quote_items';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'quote_id', 'position', 'tax_rate_id', 'description', 'quantity', 'unit_cost', 'start_on', 'due_on');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @param void
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of quote_id field
     *
     * @param void
     * @return integer
     */
    function getQuoteId() {
      return $this->getFieldValue('quote_id');
    } // getQuoteId
    
    /**
     * Set value of quote_id field
     *
     * @param integer $value
     * @return integer
     */
    function setQuoteId($value) {
      return $this->setFieldValue('quote_id', $value);
    } // setQuoteId

    /**
     * Return value of position field
     *
     * @param void
     * @return integer
     */
    function getPosition() {
      return $this->getFieldValue('position');
    } // getPosition
    
    /**
     * Set value of position field
     *
     * @param integer $value
     * @return integer
     */
    function setPosition($value) {
      return $this->setFieldValue('position', $value);
    } // setPosition

    /**
     * Return value of tax_rate_id field
     *
     * @param void
     * @return integer
     */
    function getTaxRateId() {
      return $this->getFieldValue('tax_rate_id');
    } // getTaxRateId
    
    /**
     * Set value of tax_rate_id field
     *
     * @param integer $value
     * @return integer
     */
    function setTaxRateId($value) {
      return $this->setFieldValue('tax_rate_id', $value);
    } // setTaxRateId

    /**
     * Return value of description field
     *
     * @param void
     * @return string
     */
    function getDescription() {
      return $this->getFieldValue('description');
    } // getDescription
    
    /**
     * Set value of description field
     *
     * @param string $value
     * @return string
     */
    function setDescription($value) {
      return $this->setFieldValue('description', $value);
    } // setDescription

    /**
     * Return value of quantity field
     *
     * @param void
     * @return float
     */
    function getQuantity() {
      return $this->getFieldValue('quantity');
    } // getQuantity
    
    /**
     * Set value of quantity field
     *
     * @param float $value
     * @return float
     */
    function setQuantity($value) {
      return $this->setFieldValue('quantity', $value);
    } // setQuantity

    /**
     * Return value of unit_cost field
     *
     * @param void
     * @return float
     */
    function getUnitCost() {
      return $this->getFieldValue('unit_cost');
    } // getUnitCost
    
    /**
     * Set value of unit_cost field
     *
     * @param float $value
     * @return float
     */
    function setUnitCost($value) {
      return $this->setFieldValue('unit_cost', $value);
    } // setUnitCost

    /**
     * Return value of start_on field
     *
     * @param void
     * @return DateValue
     */
    function getStartOn() {
      return $this->getFieldValue('start_on');
    } // getStartOn
    
    /**
     * Set value of start_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setStartOn($value) {
      return $this->setFieldValue('start_on', $value);
    } // setStartOn

    /**
     * Return value of due_on field
     *
     * @param void
     * @return DateValue
     */
    function getDueOn() {
      return $this->getFieldValue('due_on');
    } // getDueOn
    
    /**
     * Set value of due_on field
     *
     * @param DateValue $value
     * @return DateValue
     */
    function setDueOn($value) {
      return $this->setFieldValue('due_on', $value);
    } // setDueOn

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'quote_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'position':
          return parent::setFieldValue($real_name, intval($value));
        case 'tax_rate_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'description':
          return parent::setFieldValue($real_name, strval($value));
        case 'quantity':
          return parent::setFieldValue($real_name, floatval($value));
        case 'unit_cost':
          return parent::setFieldValue($real_name, floatval($value));
        case 'start_on':
          return parent::setFieldValue($real_name, dateval($value));
        case 'due_on':
          return parent::setFieldValue($real_name, dateval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

?>