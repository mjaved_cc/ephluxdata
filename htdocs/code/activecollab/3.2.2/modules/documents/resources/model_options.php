<?php

  /**
   * Model options for documents module
   *
   * @package activeCollab.modules.documents
   * @subpackage resources
   */
  
  $this->setTableOptions(
    array('document_categories', 'documents'), array('module' => 'documents', 'object_extends' => 'ApplicationObject')
  );

?>