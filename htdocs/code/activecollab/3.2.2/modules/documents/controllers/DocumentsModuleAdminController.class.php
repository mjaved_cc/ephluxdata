<?php

  // Extend modules admin
  AngieApplication::useController('modules_admin', SYSTEM_MODULE);

  /**
   * Documents module admin controller
   *
   * @package activeCollab.modules.documents
   * @subpackage controllers
   */
  class DocumentsModuleAdminController extends ModulesAdminController {
    
    /**
     * Show module details page
     */
    function module() {
      $this->smarty->assign('roles', Roles::find());
    } // module
    
  }