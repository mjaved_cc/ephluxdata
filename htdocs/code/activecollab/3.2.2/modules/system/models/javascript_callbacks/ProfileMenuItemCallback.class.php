<?php

  /**
   * User profile menu item callback code
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class ProfileMenuItemCallback extends JavaScriptCallback {
  
    /**
     * Render callback code
     * 
     * @return string
     */
    function render() {
      return '';
    } // render
    
  }