<?php

  /**
   * Project object attachment implementation
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class ProjectObjectAttachment extends Attachment {
    
    /**
     * Retrieve project
     * 
     * @return Project
     */
    function getProject() {
      return $this->getParent()->getProject();
    } // getProject
    
  }