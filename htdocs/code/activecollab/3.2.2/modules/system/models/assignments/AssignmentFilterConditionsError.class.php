<?php

  /**
   * Assignment filter conditions error
   * 
   * This error is thrown when assignments filter can't prepare filter because 
   * conditions would be not possible
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class AssignmentFilterConditionsError extends Error {
    
    /**
     * 
     * @param unknown_type $filter_name
     * @param unknown_type $filter_value
     * @param unknown_type $filter_data
     * @param unknown_type $message
     */
    function __construct($filter_name, $filter_value, $filter_data = null, $message = null) {
      if($message === null) {
        $message = 'Can not prepare filter contiions';
      } // if
      
      parent::__construct($message, array(
        'filter_name' => $filter_name, 
        'filter_value' => $filter_value, 
        'filter_data' => $filter_data, 
      ));
    } // __construct
    
  }