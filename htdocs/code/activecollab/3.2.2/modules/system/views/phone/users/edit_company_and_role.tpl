{title}Company and Role{/title}
{add_bread_crumb}Company and Role{/add_bread_crumb}

<div id="user_edit_company_and_role">
  {form action=$active_user->getEditCompanyAndRoleUrl() csfr_protect=true}
    {wrap field=company_id}
      {select_company name='user[company_id]' value=$user_data.company_id user=$logged_user optional=false id=userCompanyId label="Company" required=true}
    {/wrap}
    
    {wrap field=role_id}
      {select_role name='user[role_id]' value=$user_data.role_id active_user=$active_user optional=false id=userRoleId disabled=Roles::isLastAdministrator($active_user) label="Role" required=true}
    {/wrap}
    
    <script type="text/javascript">
    	$(document).ready(function() {
  			App.Wireframe.SelectBox.init();
  		});
		</script>
    
    {wrap_buttons}
    	{submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>