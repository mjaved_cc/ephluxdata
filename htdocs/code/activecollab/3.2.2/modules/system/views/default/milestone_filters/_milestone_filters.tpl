<div id="milestone_filters" class="filter_criteria">
  <form action="{assemble route=milestone_filters_run}" method="get" class="expanded">
  
    <!-- Filter Picker -->
    <div class="filter_criteria_head">
      <div class="filter_criteria_head_inner">
        <div class="filter_criteria_picker">
          {lang}Filter{/lang}: 
          <select>
            <option value="">{lang}Custom{/lang}</option>
          </select>
        </div>
        
        <div class="filter_criteria_run">{button type="submit" class="default"}Run{/button}</div>
        <div class="filter_criteria_options" style="display: none"></div>
      </div>
    </div>
    
    <div class="filter_criteria_body"></div>
  </form>
  
  <div class="filter_results"></div>
</div>

<script type="text/javascript">
  $('#milestone_filters').each(function() {
    /**
     * Prepare select user box
     *
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Objevt data
     */
    var prepare_select_company = function(submit_as, criterion, filter, data) {
      if(data['companies']) {
        if(criterion == 'project_filter') {
          var selected_company_id = typeof(filter) == 'object' && filter && filter['project_client_id'] ? filter['project_client_id'] : null; 
          var name = 'project_client_id';
        } else if(criterion == 'created_by_filter') {
          var selected_company_id = typeof(filter) == 'object' && filter && filter['created_by_company_id'] ? filter['created_by_company_id'] : null; 
          var name = 'created_by_company_id';
        } else if(criterion == 'delegated_by_filter') {
          var selected_company_id = typeof(filter) == 'object' && filter && filter['delegated_by_company_id'] ? filter['delegated_by_company_id'] : null; 
          var name = 'delegated_by_company_id';
        } else if(criterion == 'completed_by_filter') {
          var selected_company_id = typeof(filter) == 'object' && filter && filter['completed_by_company_id'] ? filter['completed_by_company_id'] : null; 
          var name = 'completed_by_company_id';
        } else {
          var selected_company_id = typeof(filter) == 'object' && filter && filter['company_id'] ? filter['company_id'] : null; 
          var name = 'company_id';
        } // if
        
        var select = $('<select name="' + submit_as + '[' + name + ']"></select>');
        
        for(var company_id in data['companies']) {
          if(company_id == selected_company_id) {
            select.append('<option value="' + company_id + '" selected="selected">' + data['companies'][company_id].clean() + '</option>');
          } else {
            select.append('<option value="' + company_id + '">' + data['companies'][company_id].clean() + '</option>');
          } // if
        } // for
        
        $(this).append(select);
      } else {
        $(this).text(App.lang('There are no companies to select from'));
      } // if
    }; // prepare_select_company

    /**
     * Prepare user picker
      * 
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_users = function(submit_as, criterion, filter, data) {
      if(data['users']) {
        if(criterion == 'created_by_filter') {
          var selected_user_ids = typeof(filter) == 'object' && filter && filter['created_by_user_ids'] ? filter['created_by_user_ids'] : null;
          var name = 'created_by_user_ids';
        } else if(criterion == 'delegated_by_filter') {
          var selected_user_ids = typeof(filter) == 'object' && filter && filter['delegated_by_user_ids'] ? filter['delegated_by_user_ids'] : null;
          var name = 'delegated_by_user_ids';
        } else if(criterion == 'completed_by_filter') {
          var selected_user_ids = typeof(filter) == 'object' && filter && filter['completed_by_user_ids'] ? filter['completed_by_user_ids'] : null;
          var name = 'completed_by_user_ids';
        } else {
          var selected_user_ids = typeof(filter) == 'object' && filter && filter['user_ids'] ? filter['user_ids'] : null;
          var name = 'user_ids';
        } // if
        
        var select = $('<div class="milestone_filter_' + criterion + '"></div>');
        
        for(var company_name in data['users']) {
          var company_wrapper = $('<div class="tracking_report_select_users_company">' +
            '<div class="company_name">' + company_name.clean() + '</div>' +
            '<div class="company_users"></div>' + 
          '</div>').appendTo(select);
          
          var company_users_wrapper = company_wrapper.find('div.company_users');
          
          for(var user_id in data['users'][company_name]) {
            var id = 'milestone_filter_' + criterion + '_' + user_id;
            var company_user = $('<div class="company_user"><input type="checkbox" name="' + submit_as + '[' + name + '][]" value="' + user_id + '" id="' + id + '" /> <label for="' + id + '">' + data['users'][company_name][user_id].clean() + '</label></div>').appendTo(company_users_wrapper);
            
            if(jQuery.isArray(selected_user_ids) && selected_user_ids.indexOf(parseInt(user_id)) >= 0) {
              company_user.find('input[type=checkbox]')[0].checked = true;
            } // if
          } // for
        } // for

        $(this).append(select);
      } else {
        $(this).text(App.lang('There are no users to select from'));
      } // if
    }; // prepare_select_users
    
    /**
     * Prepare select labels widget
     * 
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_labels = function(submit_as, criterion, filter, data) {
      var value = typeof(filter) == 'object' && filter && typeof(filter['label_names']) != 'undefined' ? filter['label_names'] : '';
      var input = $('<input type="text" name="' + submit_as + '[label_names]">').attr('value', value).appendTo(this).focus();
    }; // prepare_select_labels
    
    /**
     * Prepare select categories widget
     * 
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_categories = function(submit_as, criterion, filter, data) {
      var value = typeof(filter) == 'object' && filter && typeof(filter['category_names']) != 'undefined' ? filter['category_names'] : '';
      var input = $('<input type="text" name="' + submit_as + '[category_names]">').attr('value', value).appendTo(this).focus();
    }; // prepare_select_categories
    
    /**
     * Prepare select milestones widget
     * 
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_milestones = function(submit_as, criterion, filter, data) {
      var value = typeof(filter) == 'object' && filter && typeof(filter['milestone_names']) != 'undefined' ? filter['milestone_names'] : '';
      var input = $('<input type="text" name="' + submit_as + '[milestone_names]">').attr('value', value).appendTo(this).focus();
    }; // prepare_select_milestones

    /**
     * Prepare select projects box
     *
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_projects = function(submit_as, criterion, filter, data) {
      if(data['projects']) {
        var selected_project_ids = typeof(filter) == 'object' && filter && filter['project_ids'] ? filter['project_ids'] : null;
        var select = $('<div class="milestone_filter_' + criterion + '"></div>');
        
        for(var project_id in data['projects']) {
          var id = 'milestone_filter_' + criterion + '_' + project_id;
          var project = $('<div class="project"><input type="checkbox" name="' + submit_as + '[project_ids][]" value="' + project_id + '" id="' + id + '" /> <label for="' + id + '">' + data['projects'][project_id].clean() + '</label></div>').appendTo(select);
          
          if(jQuery.isArray(selected_project_ids) && selected_project_ids.indexOf(parseInt(project_id)) >= 0) {
            project.find('input[type=checkbox]')[0].checked = true;
          } // if
        } // for
        
        $(this).append(select);
      } else {
        $(this).text(App.lang('There are no projects to select from'));
      } // if
    }; // prepare_select_projects

    /**
     * Prepare select project category picker
     *
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_select_project_category = function(submit_as, criterion, filter, data) {
      if(data['project_categories']) {
        var selected_category_id = typeof(filter) == 'object' && filter && filter['project_category_id'] ? filter['project_category_id'] : null;
        var select = $('<select name="' + submit_as + '[project_category_id]"></select>');
        
        for(var project_category_id in data['project_categories']) {
          if(project_category_id == selected_category_id) {
            select.append('<option value="' + project_category_id + '" selected="selected">' + data['project_categories'][project_category_id].clean() + '</option>');
          } else {
            select.append('<option value="' + project_category_id + '">' + data['project_categories'][project_category_id].clean() + '</option>');
          } // if
        } // for
        
        $(this).append(select);
      } else {
        $(this).append(App.lang('There are no project categories to select from'));
      } // if
    }; // prepare_select_project_category

    /**
     * Prepare date picker
     *
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_date = function(submit_as, criterion, filter, data) {
      switch(criterion) {
        case 'created_on_filter':
          var name = 'created_on'; break;
        case 'due_on_filter':
          var name = 'due_on'; break;
        case 'completed_on_filter':
          var name = 'completed_on'; break;
      } // switch

      // Value
      if(typeof(filter) == 'object' && filter) {
        switch(criterion) {
          case 'created_on_filter':
            var value = filter['created_on']; break;
          case 'due_on_filter':
            var value = filter['due_on']; break;
          case 'completed_on_filter':
            var value = filter['completed_on']; break;
        } // switch
      } else {
        var value = '';
      } // if
      
      var select = $('<div class="select_date"><input name="' + submit_as + '[' + name + ']" value="' + (typeof(value) == 'object' && value ? value['mysql'] : '') + '" /></div>');
      
      select.find('input').datepicker({
        'dateFormat' : "yy/mm/dd",
        'minDate' : new Date("2000/01/01"),
        'maxDate' : new Date("2050/01/01"),
        'showAnim' : "blind",
        'duration' : 0,
        'changeYear' : true,
        'showOn' : "both",
        'buttonImage' : App.Wireframe.Utils.imageUrl('icons/16x16/calendar.png', 'tracking'),
        'buttonImageOnly' : true,
        'buttonText' : App.lang('Select Date'),
        'firstDay' : App.Config.get('first_week_day'),
        'hideIfNoPrevNext' : true,
        'defaultDate' : typeof(value) == 'object' && value ? new Date(value['mysql']) : new Date()
      });

      $(this).append(select);
    }; // prepare_date

    /**
     * Prepare date range picker
     *
     * @param String submit_as
     * @param String criterion
     * @param Object filter
     * @param Object data
     */
    var prepare_date_range = function(submit_as, criterion, filter, data) {
      switch(criterion) {
        case 'created_on_filter':
          var name_prefix = 'created'; break;
        case 'due_on_filter':
          var name_prefix = 'due'; break;
        case 'completed_on_filter':
          var name_prefix = 'completed'; break;
      } // switch

      // Value
      if(typeof(filter) == 'object' && filter) {
        switch(criterion) {
          case 'created_on_filter':
            var value_from = filter['created_from'];
            var value_to = filter['created_to'];
            var name_prefix = 'created';
            
            break;
          case 'due_on_filter':
            var value_from = filter['due_from'];
            var value_to = filter['due_to'];
            var name_prefix = 'due';
            
            break;
          case 'completed_on_filter':
            var value_from = filter['completed_from'];
            var value_to = filter['completed_to'];
            var name_prefix = 'completed';
            
            break;
        } // switch
      } else {
        var value_from = new Date();
        var value_to = new Date();
      } // if

      if(value_from instanceof Date) {
        var value_from_str = value_from.toString('yyyy/MM/dd');
      } else if(typeof(value_from) == 'object' && value_from) {
        var value_from_str = value_from['mysql'];
      } else {
        var value_from_str = date.today().toString('yyyy/MM/dd');
      } // if

      if(value_to instanceof Date) {
        var value_to_str = value_to.toString('yyyy/MM/dd');
      } else if(typeof(value_to) == 'object' && value_to) {
        var value_to_str = value_to['mysql'];
      } else {
        var value_to_str = date.today().toString('yyyy/MM/dd');
      } // if
      
      var select = $('<div class="select_range">' + 
        '<div class="select_date range_from"><span>' + App.lang('From') + ':</span><span><input name="' + submit_as + '[' + name_prefix + '_from]" value="' + value_from_str + '"></span></div>' +
        '<div class="select_date range_to"><span>' + App.lang('To') + ':</span><span><input name="' + submit_as + '[' + name_prefix + '_to]" value="' + value_to_str + '"></span></div>' +
      '</div>');

      var datepicker_settings = {
        'dateFormat' : "yy/mm/dd",
        'minDate' : new Date("2000/01/01"),
        'maxDate' : new Date("2050/01/01"),
        'showAnim' : "blind",
        'duration' : 0,
        'changeYear' : true,
        'showOn' : "both",
        'buttonImage' : App.Wireframe.Utils.imageUrl('icons/16x16/calendar.png', 'tracking'),
        'buttonImageOnly' : true,
        'buttonText' : App.lang('Select Date'),
        'firstDay' : App.Config.get('first_week_day'),
        'hideIfNoPrevNext' : true
      };
      
      select.find('div.select_date.range_from input').datepicker(jQuery.extend(datepicker_settings, {
        'defaultDate' : typeof(value_from) == 'object' && value_from ? new Date(value_from['mysql']) : new Date(),
      }));

      select.find('div.select_date.range_to input').datepicker(jQuery.extend(datepicker_settings, {
        'defaultDate' : typeof(value_to) == 'object' && value_to ? new Date(value_to['mysql']) : new Date()
      }));

      $(this).append(select);
    }; // prepare_date_range

    $(this).filterCriteria({
      'pre_select_filter_id' : {if $pre_select_filter instanceof MilestoneFilter}{$pre_select_filter->getId()|json nofilter}{else}null{/if},
      'filter_type' : 'milestones',    
      'options' : {
        'include_all_projects' : {
          'label' : App.lang('Include All Projects')
        }
      },
      'criterions' : {
        'user_filter' : {
          'label' : App.lang('Assigned To'), 
          'choices' : {
            'anybody' : App.lang('Anybody'), 
            'not_assigned' : App.lang('Not Assigned'), 
            'logged_user' : App.lang('Person Using the Filter is Assigned or Responsible'), 
            'logged_user_responsible' : App.lang('Person Using the Filter is Responsible Only'), 
            'company' : {
              'label' : App.lang('Member of a Company is Assigned or Responsible ...'), 
              'prepare' : prepare_select_company
            }, 
            'company_responsible' : {
              'label' : App.lang('Member of a Company is Responsible ...'), 
              'prepare' : prepare_select_company
            }, 
            'selected' : {
              'label' : App.lang('Selected Users are Assigned or Responsible ...'), 
              'prepare' : prepare_select_users
            }, 
            'selected_responsible' : {
              'label' : App.lang('Selected Users are Responsible ...'), 
              'prepare' : prepare_select_users
            }
          }
        },
        'created_by_filter' : {
          'label' : App.lang('Created By'), 
          'choices' : {
            'anybody' : App.lang('Anybody'), 
            'anonymous' : App.lang('Anonymous'), 
            'logged_user' : App.lang('Person Using the Filter'), 
            'company' : {
              'label' : App.lang('Member of a Company ...'), 
              'prepare' : prepare_select_company
            },
            'selected' : {
              'label' : App.lang('Selected Users ...'), 
              'prepare' : prepare_select_users
            }
          }
        }, 
        'delegated_by_filter' : {
          'label' : App.lang('Delegated By'), 
          'choices' : {
            'anybody' : App.lang('Anybody'), 
            'logged_user' : App.lang('Person Using the Filter'), 
            'company' : {
              'label' : App.lang('Member of a Company ...'), 
              'prepare' : prepare_select_company
            },
            'selected' : {
              'label' : App.lang('Selected Users ...'), 
              'prepare' : prepare_select_users
            }
          }
        },
        'created_on_filter' : {
          'label' : App.lang('Created On'), 
          'choices' : {
            'any' : App.lang('Any Time'), 
            'last_month' : App.lang('Last Month'), 
            'last_week' : App.lang('Last Week'), 
            'yesterday' : App.lang('Yesterday'), 
            'today' : App.lang('Today'), 
            'this_week' : App.lang('This Week'), 
            'this_month' : App.lang('This Month'),  
            'selected_date' : {
              'label' : App.lang('Select Date ...'), 
              'prepare' : prepare_date
            },  
            'selected_range' : {
              'label' : App.lang('Select Date Range ...'), 
              'prepare' : prepare_date_range
            } 
          }
        }, 
        'due_on_filter' : {
          'label' : App.lang('Due On'), 
          'choices' : {
            'any' : App.lang('Any Time'), 
            'is_not_set' : App.lang('Due Date not Set'), 
            'late' : App.lang('Late'), 
            'today' : App.lang('Today'), 
            'tomorrow' : App.lang('Tomorrow'), 
            'this_week' : App.lang('This Week'), 
            'next_week' : App.lang('Next Week'), 
            'this_month' : App.lang('This Month'), 
            'next_month' : App.lang('Next Month'), 
            'selected_date' : {
              'label' : App.lang('Select Date ...'), 
              'prepare' : prepare_date
            }, 
            'selected_range' : {
              'label' : App.lang('Selected Date Range ...'),
              'prepare' : prepare_date_range 
            } 
          }
        },
        'completed_by_filter' : {
          'label' : App.lang('Completed By'), 
          'choices' : {
            'anybody' : App.lang('Anybody'), 
            'logged_user' : App.lang('Person Using the Filter'), 
            'company' : {
              'label' : App.lang('Member of a Company ...'), 
              'prepare' : prepare_select_company
            },
            'selected' : {
              'label' : App.lang('Selected Users ...'), 
              'prepare' : prepare_select_users
            }
          }
        },
        'completed_on_filter' : {
          'label' : App.lang('Completed On'), 
          'choices' : {
            'any' : App.lang('Open and Completed'), 
            'is_not_set' : App.lang('Not Yet Completed'), 
            'is_set' : App.lang('Completed at Any Time'), 
            'last_month' : App.lang('Completed Last Month'), 
            'last_week' : App.lang('Completed Last Week'), 
            'yesterday' : App.lang('Completed Yesterday'), 
            'today' : App.lang('Completed Today'), 
            'this_week' : App.lang('Completed This Week'), 
            'this_month' : App.lang('Completed This Month'), 
            'selected_date' : {
              'label' : App.lang('Completed on a Selected Date ...'), 
              'prepare' : prepare_date
            }, 
            'selected_range' : {
              'label' : App.lang('Completed in Selected Date Range ...'), 
              'prepare' : prepare_date_range
            }
          }
        }, 
        'project_filter' : {
          'label' : App.lang('Projects'), 
          'choices' : {
            'any' : App.lang('Any Project'), 
            'active' : App.lang('Active Projects'), 
            'completed' : App.lang('Completed Projects'), 
            'category' : {
              'label' : App.lang('From Category ...'), 
              'prepare' : prepare_select_project_category
            }, 
            'client' : {
              'label' : App.lang('For Client ...'), 
              'prepare' : prepare_select_company
            },  
            'selected' : {
              'label' : App.lang('Selected Projects ...'),
              'prepare' : prepare_select_projects 
            } 
          }
        }, 
        'group_by' : {
          'label' : App.lang('Group By'), 
          'choices' : {
            'dont' : App.lang("Don't Group"), 
            'assignee' : App.lang('Assignee'), 
            'project' : App.lang('Project'),  
            'project_client' : App.lang('Project Client'),
            'created_on' : App.lang('Creation Date'), 
            'due_on' : App.lang('Due Date'), 
            'completed_on' : App.lang('Completion Date')
          }
        },
        'additional_column_1' : {
          'label' : App.lang('Additional Column #1'),
          'choices' : {
            'none' : App.lang('None'), 
            'assignee' : App.lang('Assignee'), 
            'project' : App.lang('Project'),
            'created_on' : App.lang('Created On'), 
            'created_by' : App.lang('Created By'), 
            'due_on' : App.lang('Due On'), 
            'completed_on' : App.lang('Completed On') 
          }
        },
        'additional_column_2' : {
          'label' : App.lang('Additional Column #2'),
          'choices' : {
            'none' : App.lang('None'), 
            'assignee' : App.lang('Assignee'), 
            'project' : App.lang('Project'),
            'created_on' : App.lang('Created On'), 
            'created_by' : App.lang('Created By'), 
            'due_on' : App.lang('Due On'), 
            'completed_on' : App.lang('Completed On') 
          }
        } 
      }, 
      'filters' : {$milestone_filters|json nofilter},
      'new_filter_url' : {$new_filter_url|json nofilter},
      'can_add_filter' : {if $new_filter_url}true{else}false{/if},
      'on_result_links' : function(response, data, links) {
        links.push({
          'text' : App.lang('Export CSV'), 
          'url' : '{assemble route=milestone_filters_export}', 
          'download' : true
        });
      },
      'on_show_results' : function(response, data, form_data) {
        var results_wrapper = $(this);
        
        // Settings that affect the way results are displayed
        var group_by = 'dont';
        var additional_columns = {
          'additional_column_1' : 'none',
          'additional_column_2' : 'none'
        };
        var show_stats = false;
        
        if(jQuery.isArray(form_data)) {
          for(var i in form_data) {
            switch(form_data[i]['name']) {
              case 'filter[group_by]':
                group_by = form_data[i]['value'];
                break;
              case 'filter[additional_column_1]':
                additional_columns['additional_column_1'] = form_data[i]['value'];
                break;
              case 'filter[additional_column_2]':
                additional_columns['additional_column_2'] = form_data[i]['value'];
                break;
              case 'filter[show_stats]':
                show_stats = form_data[i]['value'] ? true : false;
                break;
            } // switch
          } // for
        } // if
        
        /**
         * Return priority pill
         * 
         * @param Number priority_id
         * @return String
         */
        var render_priority = function(priority_id) {
          if(priority_id == -2 || priority_id == -1 || priority_id == 1 || priority_id == 2) {
            switch(priority_id) {
              case -2:
                var priority_text = App.lang('Lowest');  var priority_class = 'not_important'; break;
              case -1:
                var priority_text = App.lang('Low');     var priority_class = 'not_important'; break;
              case 1:
                var priority_text = App.lang('High');    var priority_class = 'important'; break;
              case 2:
                var priority_text = App.lang('Highest'); var priority_class = 'important'; break;
            } // switch
            
            return '<span class="pill priority ' + priority_class + '">' + priority_text + '</span>';
          } else {
            return '';
          } // if
        }; // render_priority
        
        /**
         * Return milestone URL
         * 
         * @param Object milestone
         * @param String suffix
         * @return string
         */
        var milestone_url = function(milestone, suffix) {
          var project_slug = data['project_slugs'] && typeof(data['project_slugs'][milestone['project_id']]) == 'string' ? data['project_slugs'][milestone['project_id']] : null;
          
          if(project_slug) {
            return data['milestone_url'].replace('--PROJECT_SLUG--', project_slug).replace('--MILESTONE_ID--', milestone['task_id']);
          } else {
            return '#';
          } // if
        }; // milestone_url
        
        for(var group_id in response) {
          if(typeof(response[group_id]['milestones']) == 'object') {
            var group_label = typeof(response[group_id]['label']) == 'string' ? response[group_id]['label'] : '--';
            var group_wrapper = $('<div class="milestone_filter_result_group_wrapper">' +
              '<h2>' + group_label.clean() + '</h2>' + 
              '<div class="milestone_filter_result_group_inner_wrapper"></div>' +
            '</div>').appendTo(results_wrapper);
            
            var group_table = $('<table class="common" cellspacing="0">' + 
              '<tbody></tbody>' + 
            '</table>').appendTo(group_wrapper.find('div.milestone_filter_result_group_inner_wrapper'));
            
            var group_table_body = group_table.find('tbody');
            
            for(var milestone_id in response[group_id]['milestones']) {
              var milestone_url_base = milestone_url(response[group_id]['milestones'][milestone_id]);
              
              // Open row and prepare label and priority
              var row = '<tr class="milestone" milestone_id="' + milestone_id + '"><td class="labels">';
              
              row += render_priority(response[group_id]['milestones'][milestone_id]['priority']);
              
              // Name and link
              row += '</td><td class="name">';
              
              row += '<a href="' + milestone_url_base + '" class="milestone_name">' + response[group_id]['milestones'][milestone_id]['name'].clean() + '</a></td>';
              
              for(var additional_column in additional_columns) {
                if(additional_columns[additional_column] && additional_columns[additional_column] != 'none') {
                  row += '<td class="additional_column ' + additional_column + '">';
                  
                  switch(additional_columns[additional_column]) {
                    case 'assignee':
                      if(response[group_id]['milestones'][milestone_id]['assignee_id'] && typeof(response[group_id]['milestones'][milestone_id]['assignee']) == 'string') {
                        row += response[group_id]['milestones'][milestone_id]['assignee'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Not Set') + '</span>';
                      } // if
                      
                      break;
                      
                    case 'project':
                      if(response[group_id]['milestones'][milestone_id]['project_id'] && typeof(response[group_id]['milestones'][milestone_id]['project']) == 'string') {
                        row += response[group_id]['milestones'][milestone_id]['project'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Unknown') + '</span>';
                      } // if
                      
                      break;
                      
                    case 'milestone':
                      if(response[group_id]['milestones'][milestone_id]['milestone_id'] && typeof(response[group_id]['milestones'][milestone_id]['milestone']) == 'string') {
                        row += response[group_id]['milestones'][milestone_id]['milestone'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Not Set') + '</span>';
                      } // if
                      
                      break;
                      
                    case 'category':
                      if(response[group_id]['milestones'][milestone_id]['category_id'] && typeof(response[group_id]['milestones'][milestone_id]['category']) == 'string') {
                        row += response[group_id]['milestones'][milestone_id]['category'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Not Set') + '</span>';
                      } // if
                      
                      break;
                      
                    case 'created_on':
                      if(response[group_id]['milestones'][milestone_id]['created_on'] && typeof(response[group_id]['milestones'][milestone_id]['created_on']) == 'object') {
                        row += response[group_id]['milestones'][milestone_id]['created_on']['formatted_date_gmt'].clean();
                      } // if
                      
                      break;
                    case 'created_by':
                      if(typeof(response[group_id]['milestones'][milestone_id]['created_by']) == 'string') {
                        row += response[group_id]['milestones'][milestone_id]['created_by'].clean();
                      } // if
                      
                      break;
                    case 'due_on':
                      if(response[group_id]['milestones'][milestone_id]['due_on'] && typeof(response[group_id]['milestones'][milestone_id]['due_on']) == 'object') {
                        row += response[group_id]['milestones'][milestone_id]['due_on']['formatted_gmt'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Not Set') + '</span>';
                      } // if
                      
                      break;
                    case 'completed_on':
                      if(response[group_id]['milestones'][milestone_id]['completed_on'] && typeof(response[group_id]['milestones'][milestone_id]['completed_on']) == 'object') {
                        row += response[group_id]['milestones'][milestone_id]['completed_on']['formatted_date_gmt'].clean();
                      } else {
                        row += '<span class="empty">' + App.lang('Open') + '</span>';
                      } // if
                      
                      break;
                  } // switch
                  
                  row += '</td>';
                } // if
              } // for
              
              // Options
              row += '</tr>';
              
              group_table_body.append(row);
            } // for
          } // if
        } // for
      }, 
      'data' : {
        'milestone_url' : "{assemble route=project_milestone project_slug='--PROJECT_SLUG--' milestone_id='--MILESTONE_ID--'}",
        'users' : {$users|json nofilter},
        'companies' : {$companies|json nofilter},
        'projects' : {$projects|json nofilter},
        'project_slugs' : {$project_slugs|json nofilter},
        'project_categories' : {$project_categories|json nofilter}
      }
    });
  });
</script>