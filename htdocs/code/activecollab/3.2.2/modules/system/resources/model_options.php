<?php

  /**
   * Model generator options for system module
   *
   * @package activeCollab.modules.system
   * @subpackage resources
   */
  
  $this->setTableOptions(array(
    'companies', 
    'projects', 
    'project_requests', 
    'subscriptions',  
    'outgoing_messages',  
    'reminders', 
    'shared_object_profiles', 
    ), array('module' => 'system', 'object_extends' => 'ApplicationObject')
  );
  
  // Assignment filters
  $this->setTableOptions('assignment_filters', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));

  // Milestone filters
  $this->setTableOptions('milestone_filters', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));

  // Project objects
  $this->setTableOptions('project_objects', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'));
  $this->setTableOptions('project_roles', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));