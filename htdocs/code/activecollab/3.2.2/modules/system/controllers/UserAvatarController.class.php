<?php

  // Build on top of avatar controller implementation
  AngieApplication::useController('fw_avatar', AVATAR_FRAMEWORK);

  /**
   * User Avatar controller implementation
   *
   * @package activeCollab.modules.system
   * @subpackage controllers
   */
  class UserAvatarController extends FwAvatarController {
    
  }