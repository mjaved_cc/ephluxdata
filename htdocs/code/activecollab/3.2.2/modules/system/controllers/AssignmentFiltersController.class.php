<?php

  // Build on top of reports module
  AngieApplication::useController('reports', SYSTEM_MODULE);

  /**
   * Assignment filters controler
   *
   * @package activeCollab.modules.resources
   * @subpackage controllers
   */
  class AssignmentFiltersController extends ReportsController {

    /**
     * This report should be available to users with non-managerial permissions
     *
     * @var bool
     */
    protected $check_reports_access_permissions = false;
    
    /**
     * Selected assignment filter
     *
     * @var AssignmentFilter
     */
    protected $active_assignment_filter;
    
    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();
    	
      $assignment_filter_id = $this->request->getId('assignment_filter_id');
      if($assignment_filter_id) {
        $this->active_assignment_filter = AssignmentFilters::findById($assignment_filter_id);
        
        if(!($this->active_assignment_filter instanceof AssignmentFilter)) {
          $this->response->notFound();
        } // if
      } else {
        $this->active_assignment_filter = new AssignmentFilter();
      } // if
      
      $this->response->assign('active_assignment_filter', $this->active_assignment_filter);
    } // __construct
    
    /**
     * Show tracking report form and options
     */
    function index() {
      
    } // index
    
    /**
     * View selected filter
     */
    function view() {
      if ($this->request->isApiCall()) {
        if ($this->active_assignment_filter->isLoaded()) {
          $this->response->respondWithData($this->active_assignment_filter, array('as' => 'assignment_filter'));
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // view
    
    /**
     * Run a given report
     */
    function run() {
      if ($this->request->isPrintCall() || ($this->request->isWebBrowser() && $this->request->isAsyncCall())) {
        try {
          $filter = new AssignmentFilter();
          $filter->setAttributes($this->request->get('filter'));
          
          $assignments = $filter->run($this->logged_user);
        } catch(AssignmentFilterConditionsError $e) {
          $assignments = null;
        } catch(Exception $e) {
          $this->response->exception($e);
        } // try
        
        if ($this->request->isPrintCall()) {
          $this->response->assign(array(
            'filter' => $filter, 
            'assignments' => $assignments, 
          ));
        } else {
          AssignmentFilters::filterResultToMap($assignments); // Optimize for mapping

          $this->response->respondWithMap($assignments);
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // run
    
    /**
     * Export result as CSV
     */
    function export() {
      if($this->request->isAsyncCall()) {
        try {
          $filter = new AssignmentFilter();
          $filter->setAttributes($this->request->get('filter'));
          
          $csv = $filter->runToCsv($this->logged_user);
        } catch(AssignmentFilterConditionsError $e) {
          $csv = null;
        } catch(Exception $e) {
          $this->response->exception($e);
        } // try
        
        if($csv) {
          $this->response->respondWithFileDownload($csv, BaseHttpResponse::CSV, 'filter-result.csv');
        } else {
          $this->response->respondWithContentDownload('', BaseHttpResponse::CSV, 'filter-result.csv');
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // export
    
    /**
     * Create new filter
     */
    function add() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if(AssignmentFilters::canAdd($this->logged_user)) {
          try {
        	  $this->active_assignment_filter = new AssignmentFilter();
        	  $this->active_assignment_filter->setAttributes($this->request->post('filter'));
        	  $this->active_assignment_filter->save();
        	  
        	  $this->response->respondWithData($this->active_assignment_filter, array('as' => 'assignment_filter'));
          } catch(Exception $e) {
            $this->response->exception($e);
          } // try
        } else {
          $this->response->forbidden();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // add
    
    /**
     * Update an existing filter
     */
    function edit() {
      if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if($this->active_assignment_filter->isLoaded()) {
          if($this->active_assignment_filter->canEdit($this->logged_user)) {
            try {
          	  $this->active_assignment_filter->setAttributes($this->request->post('filter'));
          	  $this->active_assignment_filter->save();
          	  
          	  $this->response->respondWithData($this->active_assignment_filter, array('as' => 'assignment_filter'));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // edit
    
    /**
     * Drop an existing filter
     */
    function delete() {
    	if($this->request->isAsyncCall() && $this->request->isSubmitted()) {
        if($this->active_assignment_filter->isLoaded()) {
          if($this->active_assignment_filter->canDelete($this->logged_user)) {
            try {
          	  $this->active_assignment_filter->delete();
          	  $this->response->respondWithData($this->active_assignment_filter, array('as' => 'assignment_filter'));
            } catch(Exception $e) {
              $this->response->exception($e);
            } // try
          } else {
            $this->response->forbidden();
          } // if
        } else {
          $this->response->notFound();
        } // if
      } else {
        $this->response->badRequest();
      } // if
    } // delete
    
  }