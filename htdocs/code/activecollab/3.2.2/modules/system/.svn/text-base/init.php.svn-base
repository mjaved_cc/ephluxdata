<?php

  /**
   * Init system module
   *
   * @package activeCollab.modules.system
   */
  
  define('SYSTEM_MODULE', 'system');
  define('SYSTEM_MODULE_PATH', APPLICATION_PATH . '/modules/system');

  if (!defined('CHECK_APPLICATION_VERSION_URL')) {
    define('CHECK_APPLICATION_VERSION_URL', '//www.activecollab.com/update/latest_info');
  } // if

  require_once SYSTEM_MODULE_PATH . '/functions.php';
  
  // ---------------------------------------------------
  //  Load
  // ---------------------------------------------------
  
  AngieApplication::useModel(array(
    'access_logs', 
    'assignment_filters',
    'milestone_filters',
    'api_client_subscriptions', 
    'users',
    'companies',
    'roles', 
    'project_roles', 
  	'currencies',
  	'day_offs',
    'languages',
    'projects',
    'project_objects',
    'project_requests',
    'activity_logs',
    'comments',
    'categories',
    'labels',
    'payments',
    'payment_gateways',
    'reminders', 
    'subtasks',
    'attachments',
    'subscriptions',
    'mailing_activity_logs',
    'incoming_mailboxes',
    'incoming_mails',
    'incoming_mail_attachments',
    'incoming_mail_activity_logs',
    'incoming_mail_filters',
    'outgoing_messages', 
    'shared_object_profiles', 
    'modification_logs',
    'homescreens', 
    'homescreen_tabs', 
    'homescreen_widgets'
  ), SYSTEM_MODULE);
  
  require_once SYSTEM_MODULE_PATH . '/models/controller/Request.class.php';
  require_once SYSTEM_MODULE_PATH . '/models/controller/Response.class.php';
  require_once SYSTEM_MODULE_PATH . '/models/application_objects/ApplicationObject.class.php';
  require_once SYSTEM_MODULE_PATH . '/controllers/ApplicationController.class.php';
  
  AngieApplication::setForAutoload(array(
    'ApplicationObjects' => SYSTEM_MODULE_PATH . '/models/application_objects/ApplicationObjects.class.php', 
  
    'BackendWebInterfaceResponse' => SYSTEM_MODULE_PATH . '/models/response/BackendWebInterfaceResponse.class.php', 
    'FrontendWebInterfaceResponse' => SYSTEM_MODULE_PATH . '/models/response/FrontendWebInterfaceResponse.class.php', 

    // Wireframe
    'Wireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/Wireframe.class.php',
  
    'BackendWireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/BackendWireframe.class.php',
    'WebBrowserBackendWireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/WebBrowserBackendWireframe.class.php',
    'PhoneBackendWireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/PhoneBackendWireframe.class.php',
    'TabletBackendWireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/TabletBackendWireframe.class.php',
  
    'FrontendWireframe' => SYSTEM_MODULE_PATH . '/models/wireframe/FrontendWireframe.class.php',
    'WireframeListMode' => SYSTEM_MODULE_PATH . '/models/wireframe/elements/WireframeListMode.class.php',
  
    'ProjectScheduler' => SYSTEM_MODULE_PATH . '/models/ProjectScheduler.class.php',
    'ProjectCreator' => SYSTEM_MODULE_PATH . '/models/ProjectCreator.class.php',
    'ProjectProgress' => SYSTEM_MODULE_PATH . '/models/ProjectProgress.class.php',
  
    'IProjectBasedOn' => SYSTEM_MODULE_PATH . '/models/IProjectBasedOn.class.php',
  
  	'IProjectAvatarImplementation'	=> SYSTEM_MODULE_PATH . '/models/IProjectAvatarImplementation.class.php',
  
  	'ISchedule' => SYSTEM_MODULE_PATH . '/models/ISchedule.class.php',
  	'IScheduleImplementation' => SYSTEM_MODULE_PATH . '/models/IScheduleImplementation.class.php',
    
    'ProjectCategory' => SYSTEM_MODULE_PATH . '/models/ProjectCategory.class.php',
    'ProjectLabel' => SYSTEM_MODULE_PATH . '/models/ProjectLabel.class.php',
    
    'ProjectObjectComment' => SYSTEM_MODULE_PATH . '/models/ProjectObjectComment.class.php',
    'ProjectObjectSubtask' => SYSTEM_MODULE_PATH . '/models/ProjectObjectSubtask.class.php',
    'ProjectObjectAttachment' => SYSTEM_MODULE_PATH . '/models/ProjectObjectAttachment.class.php',
    'ProjectObjectCategory' => SYSTEM_MODULE_PATH . '/models/ProjectObjectCategory.class.php',
    
    'ProjectRequestComment' => SYSTEM_MODULE_PATH . '/models/ProjectRequestComment.class.php',
    'ProjectRequestAttachment' => SYSTEM_MODULE_PATH . '/models/ProjectRequestAttachment.class.php',
    
    'Favorites' => SYSTEM_MODULE_PATH . '/models/Favorites.class.php',
    
    'ICompanyStateImplementation' => SYSTEM_MODULE_PATH . '/models/ICompanyStateImplementation.class.php',
    'ICompanyUsersContextImplementation' => SYSTEM_MODULE_PATH . '/models/ICompanyUsersContextImplementation.class.php',
		'ICompanyAvatarImplementation'	=> SYSTEM_MODULE_PATH . '/models/ICompanyAvatarImplementation.class.php',
    
    'IUserStateImplementation' => SYSTEM_MODULE_PATH . '/models/IUserStateImplementation.class.php',
    'IUserProjectsImplementation' => SYSTEM_MODULE_PATH . '/models/IUserProjectsImplementation.class.php',
  	'IUserAvatarImplementation'	=> SYSTEM_MODULE_PATH . '/models/IUserAvatarImplementation.class.php',
  
  	'IActiveCollabNotifierImplementation'	=> SYSTEM_MODULE_PATH . '/models/IActiveCollabNotifierImplementation.class.php',
    
    'IProjectCategoriesContextImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectCategoriesContextImplementation.class.php',
    'IProjectCategoryImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectCategoryImplementation.class.php',
    'IProjectCustomFieldsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectCustomFieldsImplementation.class.php',
    'IProjectStateImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectStateImplementation.class.php',
    'IProjectUsersContextImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectUsersContextImplementation.class.php',
    
    'IProjectObjectActivityLogsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectActivityLogsImplementation.class.php',
    'IProjectObjectAssigneesImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectAssigneesImplementation.class.php',
    'IProjectObjectCategoryImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectCategoryImplementation.class.php',
    'IProjectObjectCommentsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectCommentsImplementation.class.php',
    'IProjectObjectCompleteImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectCompleteImplementation.class.php',
    'IProjectObjectSubscriptionsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectSubscriptionsImplementation.class.php',
    'IProjectObjectAttachmentsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectAttachmentsImplementation.class.php',
    'IProjectObjectStateImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectStateImplementation.class.php',
    'IProjectObjectSubtasksImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectSubtasksImplementation.class.php',
    'IProjectObjectSubtaskAssigneesImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectSubtaskAssigneesImplementation.class.php',
    'IProjectLabelImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectLabelImplementation.class.php',
    'IProjectObjectRemindersImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectObjectRemindersImplementation.class.php',
    
    'IProjectRequestCommentsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectRequestCommentsImplementation.class.php',
    'IProjectRequestAttachmentsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectRequestAttachmentsImplementation.class.php',
    'IProjectRequestSubscriptionsImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectRequestSubscriptionsImplementation.class.php',
    
    'AnonymousUser' => SYSTEM_MODULE_PATH . '/models/AnonymousUser.class.php',
    'Thumbnails' => SYSTEM_MODULE_PATH . '/models/Thumbnails.class.php',
    
    'HistoryRenderer' => SYSTEM_MODULE_PATH . '/models/history/HistoryRenderer.class.php',
    'ProjectObjectHistoryRenderer' => SYSTEM_MODULE_PATH . '/models/history/ProjectObjectHistoryRenderer.class.php',
  
    // Assignments
    'IAssignmentLabelImplementation' => SYSTEM_MODULE_PATH . '/models/assignments/IAssignmentLabelImplementation.class.php',
  	'Assignments' => SYSTEM_MODULE_PATH . '/models/assignments/Assignments.class.php',
  	'AssignmentLabel' => SYSTEM_MODULE_PATH . '/models/assignments/AssignmentLabel.class.php',
  	'AssignmentFilterConditionsError' => SYSTEM_MODULE_PATH . '/models/assignments/AssignmentFilterConditionsError.class.php',
  
    // Invoicing injection
    'IInvoiceBasedOn' => SYSTEM_MODULE_PATH . '/models/invoices/IInvoiceBasedOn.class.php', 
    'IInvoiceBasedOnImplementationStub' => SYSTEM_MODULE_PATH . '/models/invoices/IInvoiceBasedOnImplementationStub.class.php', 
  
    // Tracking injection
    'ITracking' => SYSTEM_MODULE_PATH . '/models/tracking/ITracking.class.php',
    'ITrackingImplementationStub' => SYSTEM_MODULE_PATH . '/models/tracking/ITrackingImplementationStub.class.php',
  
    // Search
    'UsersSearchIndex' => SYSTEM_MODULE_PATH . '/models/search/UsersSearchIndex.class.php', 
    'ProjectsSearchIndex' => SYSTEM_MODULE_PATH . '/models/search/ProjectsSearchIndex.class.php', 
    'ProjectObjectsSearchIndex' => SYSTEM_MODULE_PATH . '/models/search/ProjectObjectsSearchIndex.class.php', 
    'NamesSearchIndex' => SYSTEM_MODULE_PATH . '/models/search/NamesSearchIndex.class.php',
   
    'IProjectSearchItemImplementation' => SYSTEM_MODULE_PATH . '/models/search/IProjectSearchItemImplementation.class.php',
    'IProjectObjectSearchItemImplementation' => SYSTEM_MODULE_PATH . '/models/search/IProjectObjectSearchItemImplementation.class.php',  
    'ICompanySearchItemImplementation' => SYSTEM_MODULE_PATH . '/models/search/ICompanySearchItemImplementation.class.php',
    'IUserSearchItemImplementation' => SYSTEM_MODULE_PATH . '/models/search/IUserSearchItemImplementation.class.php',
    'IMilestoneSearchItemImplementation' => SYSTEM_MODULE_PATH . '/models/search/IMilestoneSearchItemImplementation.class.php',
  
    // JavaScript callbacks
    'NewProjectCallback' => SYSTEM_MODULE_PATH . '/models/javascript_callbacks/NewProjectCallback.class.php',
  	'QuickJumpCallback' => SYSTEM_MODULE_PATH . '/models/javascript_callbacks/QuickJumpCallback.class.php', 
  	'QuickAddCallback' => SYSTEM_MODULE_PATH . '/models/javascript_callbacks/QuickAddCallback.class.php', 
  
    // Home screen tabs and home screen widgets
    'AssignmentFiltersHomescreenTab' => SYSTEM_MODULE_PATH . '/models/homescreen_tabs/AssignmentFiltersHomescreenTab.class.php',
  
    'ProjectsHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/ProjectsHomescreenWidget.class.php',
    'MyProjectsHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/MyProjectsHomescreenWidget.class.php',
    'FavoriteProjectsHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/FavoriteProjectsHomescreenWidget.class.php',
    'DayOverviewHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/DayOverviewHomescreenWidget.class.php',
    'AssignmentsFilterHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/AssignmentsFilterHomescreenWidget.class.php',
    'UserAssignmentsHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/UserAssignmentsHomescreenWidget.class.php',
    'DelegatedAssignmentsHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/DelegatedAssignmentsHomescreenWidget.class.php',
    'WelcomeHomescreenWidget' => SYSTEM_MODULE_PATH . '/models/homescreen_widgets/WelcomeHomescreenWidget.class.php',
  
    // Main menu
    'MainMenu' => SYSTEM_MODULE_PATH . '/models/MainMenu.class.php', 
    'ProfileMenuItemCallback' => SYSTEM_MODULE_PATH . '/models/javascript_callbacks/ProfileMenuItemCallback.class.php',

    // Status menu
    'StatusBar' => SYSTEM_MODULE_PATH . '/models/StatusBar.class.php',
  
    // Admin panel
  	'AdminPanel' => SYSTEM_MODULE_PATH . '/models/admin_panel/AdminPanel.class.php',
  	'SystemInfoAdminPanelRow' => SYSTEM_MODULE_PATH . '/models/admin_panel/SystemInfoAdminPanelRow.class.php',
  
    // Reports panel
    'ReportsPanel' => SYSTEM_MODULE_PATH . '/models/ReportsPanel.class.php',

    // Control Tower
    'ControlTower' => SYSTEM_MODULE_PATH . '/models/ControlTower.class.php',

  	// Milestones
    'Milestone' => SYSTEM_MODULE_PATH . '/models/milestones/Milestone.class.php',
    'Milestones' => SYSTEM_MODULE_PATH . '/models/milestones/Milestones.class.php', 
    'IMilestoneCommentsImplementation' => SYSTEM_MODULE_PATH . '/models/IMilestoneCommentsImplementation.class.php',
    'MilestonesProjectExporter' => SYSTEM_MODULE_PATH . '/models/MilestonesProjectExporter.class.php',
    'MilestoneComment' => SYSTEM_MODULE_PATH . '/models/MilestoneComment.class.php',
    
    // Trash
    'Trash' => SYSTEM_MODULE_PATH . '/models/Trash.class.php',
  
	  // Mass Manager
    'MassManager' => SYSTEM_MODULE_PATH . '/models/MassManager.class.php',
  	
  	// Sharing
  	'ISharing' => SYSTEM_MODULE_PATH . '/models/sharing/ISharing.class.php',
  	'ISharingImplementation' => SYSTEM_MODULE_PATH . '/models/sharing/ISharingImplementation.class.php',
  
    // Project exporter
    'PeopleProjectExporter' => SYSTEM_MODULE_PATH . '/models/PeopleProjectExporter.class.php',
  	'SystemProjectExporter' => SYSTEM_MODULE_PATH .'/models/SystemProjectExporter.class.php',
  	
  	// Inspector
  	'IMilestoneInspectorImplementation' => SYSTEM_MODULE_PATH . '/models/IMilestoneInspectorImplementation.class.php',
  	'IUserInspectorImplementation' => SYSTEM_MODULE_PATH . '/models/IUserInspectorImplementation.class.php',
  	'ICompanyInspectorImplementation' => SYSTEM_MODULE_PATH . '/models/ICompanyInspectorImplementation.class.php',
  	'IProjectInspectorImplementation' => SYSTEM_MODULE_PATH . '/models/IProjectInspectorImplementation.class.php',
  	'MilestoneInspectorProperty' => SYSTEM_MODULE_PATH . '/models/MilestoneInspectorProperty.class.php',
  	'ScheduleInspectorProperty' => SYSTEM_MODULE_PATH . '/models/ScheduleInspectorProperty.class.php',
  	'ProjectRequestClientInspectorProperty' => SYSTEM_MODULE_PATH . '/models/ProjectRequestClientInspectorProperty.class.php',
  	'SharingInspectorIndicator' => SYSTEM_MODULE_PATH . '/models/SharingInspectorIndicator.class.php',
  	'VisibilityInspectorIndicator' => SYSTEM_MODULE_PATH . '/models/VisibilityInspectorIndicator.class.php',
  	'ProjectBudgetInspectorProperty' => SYSTEM_MODULE_PATH . '/models/ProjectBudgetInspectorProperty.class.php',
  	'MilestoneProgressbarInspectorWidget' => SYSTEM_MODULE_PATH . '/models/MilestoneProgressbarInspectorWidget.class.php',

    // Invoice interfaces
    'IInvoiceBasedOnMilestoneImplementation' => SYSTEM_MODULE_PATH . '/models/IInvoiceBasedOnMilestoneImplementation.class.php',
  	'IInvoiceBasedOnProjectImplementation' => SYSTEM_MODULE_PATH . '/models/IInvoiceBasedOnProjectImplementation.class.php',

    // Feed subscription
    'FeedClientSubscription' => SYSTEM_MODULE_PATH . '/models/api_client_subscriptions/FeedClientSubscription.class.php',
    'FeedClientSubscriptions' => SYSTEM_MODULE_PATH . '/models/api_client_subscriptions/FeedClientSubscriptions.class.php',

    // Custom fields
    'CustomFields' => SYSTEM_MODULE_PATH . '/models/CustomFields.class.php',
  ));
  
  DataObjectPool::registerTypeLoader('Project', function($ids) {
    return Projects::findByIds($ids);
  });
  
  DataObjectPool::registerTypeLoader('ProjectRequest', function($ids) {
    return ProjectRequests::findByIds($ids);
  });
  
  DataObjectPool::registerTypeLoader('Milestone', function($ids) {
    return Milestones::findByIds($ids, STATE_TRASHED, VISIBILITY_PRIVATE);
  });
  
  DataObjectPool::registerTypeLoader('ProjectObjectSubtask', function($ids) {
    return Subtasks::findByIds($ids);
  });