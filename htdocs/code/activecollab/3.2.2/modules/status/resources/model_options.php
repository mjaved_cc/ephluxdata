<?php

  /**
   * Model options for status module
   *
   * @package activeCollab.modules.status
   * @subpackage resources
   */
  
  $this->setTableOptions(
    array('status_updates'), array('module' => 'status', 'object_extends' => 'ApplicationObject')
  );

?>