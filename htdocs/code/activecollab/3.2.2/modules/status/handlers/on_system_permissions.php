<?php

  /**
   * Status on_system_permissions handler
   *
   * @package activeCollab.modules.status
   * @subpackage handlers
   */
  
  /**
   * Handle on_system_permissions
   *
   * @param NamedList $permissions
   */
  function status_handle_on_system_permissions(NamedList &$permissions) {
    $permissions->add('can_use_status_updates', array(
      'name' => lang('Use Status Updates'), 
      'description' => lang('Set to Yes to let people use a simple communication tool that is easily accessible from all system pages'), 
      'depends_on' => 'has_system_access', 
    ));
  } // status_handle_on_system_permissions