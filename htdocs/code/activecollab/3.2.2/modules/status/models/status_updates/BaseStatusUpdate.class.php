<?php

  /**
   * BaseStatusUpdate class
   *
   * @package activeCollab.modules.status
   * @subpackage models
   */
  class BaseStatusUpdate extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'status_updates';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'parent_id', 'message', 'created_by_id', 'created_by_name', 'created_by_email', 'created_on', 'last_update_on');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @param void
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of parent_id field
     *
     * @param void
     * @return integer
     */
    function getParentId() {
      return $this->getFieldValue('parent_id');
    } // getParentId
    
    /**
     * Set value of parent_id field
     *
     * @param integer $value
     * @return integer
     */
    function setParentId($value) {
      return $this->setFieldValue('parent_id', $value);
    } // setParentId

    /**
     * Return value of message field
     *
     * @param void
     * @return string
     */
    function getMessage() {
      return $this->getFieldValue('message');
    } // getMessage
    
    /**
     * Set value of message field
     *
     * @param string $value
     * @return string
     */
    function setMessage($value) {
      return $this->setFieldValue('message', $value);
    } // setMessage

    /**
     * Return value of created_by_id field
     *
     * @param void
     * @return integer
     */
    function getCreatedById() {
      return $this->getFieldValue('created_by_id');
    } // getCreatedById
    
    /**
     * Set value of created_by_id field
     *
     * @param integer $value
     * @return integer
     */
    function setCreatedById($value) {
      return $this->setFieldValue('created_by_id', $value);
    } // setCreatedById

    /**
     * Return value of created_by_name field
     *
     * @param void
     * @return string
     */
    function getCreatedByName() {
      return $this->getFieldValue('created_by_name');
    } // getCreatedByName
    
    /**
     * Set value of created_by_name field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByName($value) {
      return $this->setFieldValue('created_by_name', $value);
    } // setCreatedByName

    /**
     * Return value of created_by_email field
     *
     * @param void
     * @return string
     */
    function getCreatedByEmail() {
      return $this->getFieldValue('created_by_email');
    } // getCreatedByEmail
    
    /**
     * Set value of created_by_email field
     *
     * @param string $value
     * @return string
     */
    function setCreatedByEmail($value) {
      return $this->setFieldValue('created_by_email', $value);
    } // setCreatedByEmail

    /**
     * Return value of created_on field
     *
     * @param void
     * @return DateTimeValue
     */
    function getCreatedOn() {
      return $this->getFieldValue('created_on');
    } // getCreatedOn
    
    /**
     * Set value of created_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setCreatedOn($value) {
      return $this->setFieldValue('created_on', $value);
    } // setCreatedOn

    /**
     * Return value of last_update_on field
     *
     * @param void
     * @return DateTimeValue
     */
    function getLastUpdateOn() {
      return $this->getFieldValue('last_update_on');
    } // getLastUpdateOn
    
    /**
     * Set value of last_update_on field
     *
     * @param DateTimeValue $value
     * @return DateTimeValue
     */
    function setLastUpdateOn($value) {
      return $this->setFieldValue('last_update_on', $value);
    } // setLastUpdateOn

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'parent_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'message':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_by_id':
          return parent::setFieldValue($real_name, intval($value));
        case 'created_by_name':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_by_email':
          return parent::setFieldValue($real_name, strval($value));
        case 'created_on':
          return parent::setFieldValue($real_name, datetimeval($value));
        case 'last_update_on':
          return parent::setFieldValue($real_name, datetimeval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

?>