<?php

  /**
   * Model generator class
   *
   * @package angie.library.application
   * @subpackage application
   */
  class AngieFrameworkModelBuilder {
    
    /**
     * Model that this builder instance belongs to
     *
     * @var AngieFrameworkModel
     */
    protected $model;
    
    /**
     * Name of the table that this model builder is added to
     *
     * @var string
     */
    protected $table_name;
    
    /**
     * Name of the class that base object extends
     *
     * @var string
     */
    protected $base_object_extends = 'ApplicationObject';
    
    /**
     * Name of the class that base manager extends
     *
     * @var string
     */
    protected $base_manager_extends = 'DataManager';
    
    /**
     * Construct instances based on class name stored in a field
     *
     * @var string
     */
    protected $type_from_field;
    
    /**
     * Value used for ordering records
     *
     * @var string
     */
    protected $order_by;
    
    /**
     * Construct new model builder instance
     *
     * @param AngieFrameworkModel $model
     * @param string $table_name
     */
    function __construct(AngieFrameworkModel $model, $table_name) {
      $this->model = $model;
      $this->table_name = $table_name;
    } // __construct
    
    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------
    
    /**
     * Return base_object_extends
     *
     * @return string
     */
    function getBaseObjectExtends() {
    	return $this->base_object_extends;
    } // getBaseObjectExtends
    
    /**
     * Set base_object_extends
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setBaseObjectExtends($value) {
      $this->base_object_extends = $value;
      
      return $this;
    } // setBaseObjectExtends
    
    /**
     * Return base_manager_extends
     *
     * @return string
     */
    function getBaseManagerExtends() {
    	return $this->base_manager_extends;
    } // getBaseManagerExtends
    
    /**
     * Set base_manager_extends
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setBaseManagerExtends($value) {
      $this->base_manager_extends = $value;
      
      return $this;
    } // setBaseManagerExtends
    
    /**
     * Return type_from_field
     *
     * @return string
     */
    function getTypeFromField() {
    	return $this->type_from_field;
    } // getTypeFromField
    
    /**
     * Set type_from_field
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setTypeFromField($value) {
      $this->type_from_field = $value;
      
      return $this;
    } // setTypeFromField
    
    /**
     * Return order_by
     *
     * @return string
     */
    function getOrderBy() {
    	return $this->order_by;
    } // getOrderBy
    
    /**
     * Set order_by
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setOrderBy($value) {
      $this->order_by = $value;
      
      return $this;
    } // setOrderBy
    
  }

?>