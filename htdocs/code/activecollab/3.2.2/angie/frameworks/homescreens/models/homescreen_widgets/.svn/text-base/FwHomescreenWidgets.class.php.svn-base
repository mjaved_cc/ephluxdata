<?php

  /**
   * Framework level homescreen widgets manager implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  abstract class FwHomescreenWidgets extends BaseHomescreenWidgets {
  
    /**
     * Return widgets by home screen tab
     * 
     * @param HomescreenTab $homescreen_tab
     * @return DBResult
     */
    static function findByHomescreenTab(HomescreenTab $homescreen_tab) {
      return HomescreenWidgets::find(array(
        'conditions' => array('homescreen_tab_id = ?', $homescreen_tab->getId()), 
        'order' => 'position', 
      ));
    } // findByHomescreenTab
    
    /**
     * Return widgets by home screen tab
     * 
     * @param HomescreenTab $homescreen_tab
     * @return DBResult
     */
    static function countByHomescreenTab(HomescreenTab $homescreen_tab) {
      return HomescreenWidgets::count(array('homescreen_tab_id = ?', $homescreen_tab->getId()));
    } // countByHomescreenTab
    
    /**
     * Return next widget position
     * 
     * @param HomescreenTab $homescreen_tab
     * @param integer $column_id
     * @return integer
     */
    static function getNextPosition(HomescreenTab $homescreen_tab, $column_id) {
      return ((integer) DB::executeFirstCell('SELECT MAX(position) FROM ' . TABLE_PREFIX . 'homescreen_widgets WHERE homescreen_tab_id = ? AND column_id = ?', $homescreen_tab->getId(), $column_id)) + 1;
    } // getNextPosition
    
    /**
     * Delete widgets by home screen
     * 
     * @param Homescreen $homescreen
     * @return boolean
     */
    static function deleteByHomescreen(Homescreen $homescreen) {
      $homescreen_tab_ids = DB::execute('SELECT id FROM ' . TABLE_PREFIX . 'homescreen_tabs WHERE homescreen_id = ?', $homescreen->getId());
      if($homescreen_tab_ids) {
        DB::execute('DELETE FROM ' . TABLE_PREFIX . 'homescreen_widgets WHERE homescreen_tab_id IN (?)', $homescreen_tab_ids);
      } // if
    } // deleteByHomescreen
    
    /**
     * Remove widgets by parent home screen tab
     * 
     * @param HomescreenTab $homescreen_tab
     * @return boolean
     */
    static function deleteByHomescreenTab(HomescreenTab $homescreen_tab) {
      return DB::execute('DELETE FROM ' . TABLE_PREFIX . 'homescreen_widgets WHERE homescreen_tab_id = ?', $homescreen_tab->getId());
    } // deleteByHomescreenTab

    /**
     * Delete widgets by module
     *
     * @param AngieModule $module
     * @return boolean
     */
    function deleteByModule(AngieModule $module) {
      $widgets = array();

      $d = dir($module->getPath() . '/models/homescreen_widgets');
      if($d) {
        while(($entry = $d->read()) !== false) {
          $class_name = str_ends_with($entry, '.class.php') ? str_replace('.class.php', '', $entry) : null;

          if($class_name) {
            $widgets[] = $class_name;
          } // if
        } // if

        $d->close();
      } // if

      if (count($widgets)) {
        return DB::execute("DELETE FROM " . TABLE_PREFIX . "homescreen_widgets WHERE `type` IN (?)", $widgets);
      } // if
    } // findByModule
    
  }