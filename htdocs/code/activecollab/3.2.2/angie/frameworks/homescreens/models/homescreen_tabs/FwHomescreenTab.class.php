<?php

  /**
   * Framework level homescreen tab implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  abstract class FwHomescreenTab extends BaseHomescreenTab implements IRoutingContext {
    
    // Column classes
    const WIDE_COLUMN = 'wide';
    const NARROW_COLUMN = 'narrow';
  
    /**
     * This home screen does not accept widgets
     *
     * @var boolean
     */
    protected $accept_widgets = false;
    
    /**
     * Return tab description
     * 
     * @return string
     */
    abstract function getDescription();
    
    /**
     * Cached home screen instance
     *
     * @var Homescreen
     */
    private $homescreen = false;
    
    /**
     * Return parent home screen instance
     * 
     * @return Homescreen
     */
    function getHomescreen() {
      if($this->homescreen === false) {
        $this->homescreen = $this->getHomescreenId() ? Homescreens::findById($this->getHomescreenId()) : null;
      } // if
      
      return $this->homescreen;
    } // getHomescreen
    
    // ---------------------------------------------------
    //  Describe
    // ---------------------------------------------------
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user);
      
      $result['description'] = $this->getDescription();
      
      if($for_interface) {
        $result['manager'] = $this->renderManager($user);
      } // if
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      throw new NotImplementedError(__METHOD__);
    } // describeForApi
    
    // ---------------------------------------------------
    //  Renderer
    // ---------------------------------------------------
    
    /**
     * Render tab content
     * 
     * @param IUser $user
     * @return string
     */
    abstract function render(IUser $user);
    
    /**
     * Render management widget
     * 
     * @param IUser $user
     * @return string
     */
    function renderManager(IUser $user) {
      if($this->hasOptions()) {
        $view = SmartyForAngie::getInstance()->createTemplate(AngieApplication::getViewPath('_homescreen_tab_options', null, HOMESCREENS_FRAMEWORK, AngieApplication::INTERFACE_DEFAULT));
        
        $view->assign(array(
          'homescreen_tab' => &$this, 
          'options' => $this->renderOptions($user), 
        ));
        
        return $view->fetch();
      } else {
        return '<div class="homescreen_tab_manager without_options"><p class="homescreen_tab_name">' . clean($this->getName()) . '</p><p class="homescreen_tab_desc">' . lang('No additional options available') . '</p></div>';
      } // if
    } // renderManager
    
    // ---------------------------------------------------
    //  Options
    // ---------------------------------------------------
    
    /**
     * Returns true if this widget has additional options
     * 
     * @return boolean
     */
    protected function hasOptions() {
      return false;
    } // hasOptions
    
    /**
     * Render widget options form section
     * 
     * @param IUser $user
     * @return string
     */
    protected function renderOptions(IUser $user) {
      return '';
    } // renderOptions
    
    // ---------------------------------------------------
    //  URL-s
    // ---------------------------------------------------
    
    /**
     * Return URL of page that will render this tab
     * 
     * @return string
     */
    abstract function getHomescreenTabUrl();
    
    // ---------------------------------------------------
    //  Interface implementation
    // ---------------------------------------------------
    
    /**
     * Routing context name
     *
     * @var string
     */
    private $routing_context = false;
    
    /**
     * Return routing context name
     *
     * @return string
     */
    function getRoutingContext() {
      if($this->routing_context === false) {
        $this->routing_context = $this->getHomescreen()->getRoutingContext() . '_tab';
      } // if
      
      return $this->routing_context;
    } // getRoutingContext
    
    /**
     * Routing context parameters
     *
     * @var array
     */
    private $routing_context_params = false;
    
    /**
     * Return routing context parameters
     *
     * @return mixed
     */
    function getRoutingContextParams() {
      if($this->routing_context_params === false) {
        $this->routing_context_params = $this->getHomescreen()->getRoutingContextParams() ? array_merge($this->getHomescreen()->getRoutingContextParams(), array('homescreen_tab_id' => $this->getId())) : array('homescreen_tab_id' => $this->getId());
      } // if
      
      return $this->routing_context_params;
    } // getRoutingContextParams
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if $user can edit this destkop
     * 
     * @param IUser $user
     * @return boolean
     */
    function canEdit(IUser $user) {
      return $this->getHomescreen()->canEdit($user);
    } // canEdit
    
    /**
     * Return true if $user can remove this destkop
     * 
     * @param IUser $user
     * @return boolean
     */
    function canDelete(IUser $user) {
      return $this->getHomescreen()->canEdit($user);
    } // canDelete
    
    // ---------------------------------------------------
    //  System
    // ---------------------------------------------------
    
    /**
     * Copy this tab and all of its settings and widgets to a home screen
     * 
     * @param Homescreen $homescreen
     * @return HomescreenTab
     */
    function copyTo(Homescreen $homescreen) {
      $class_name = get_class($this);
      
      // Create a home screen tab of the same class as this tab
      $homescreen_tab = new $class_name();
      
      $homescreen_tab->setHomescreenId($homescreen->getId());
      $homescreen_tab->setName($this->getName());
      $homescreen_tab->setPosition($this->getPosition());
      
      foreach($this->getAdditionalProperties() as $k => $v) {
        $homescreen_tab->setAdditionalProperty($k, $v);
      } // foreach
      
      $homescreen_tab->save();
      
      return $homescreen_tab;
    } // copyTo

    /**
     * Don't delete homescreen tab if it's the only one left
     *
     * @return bool|void
     * @throws Exception
     */
    function delete() {
      $homescreen_tabs = $this->getHomescreen()->getTabs();
      if (is_foreachable($homescreen_tabs) && count($homescreen_tabs) == 1) {
        throw new Error("Last home screen tab cannot be deleted");
      } // if

      parent::delete();
    } // if
    
    /**
     * Validate before save
     * 
     * @param ValidationErrors $errors
     */
    function validate(ValidationErrors &$errors) {
      if(!$this->validatePresenceOf('type')) {
        $errors->addError(lang('Home screen tab type is required'), 'type');
      } // if
      
      if(!$this->validatePresenceOf('homescreen_id')) {
        $errors->addError(lang('Home screen is required'), 'homescreen_id');
      } // if
      
      if(!$this->validatePresenceOf('name')) {
        $errors->addError(lang('Home screen tab name is required'), 'name');
      } // if
    } // validate
    
  }