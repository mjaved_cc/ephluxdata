<?php

  /**
   * User home screen implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  class IUserHomescreenImplementation extends IHomescreenImplementation {
    
    /**
     * Home screen
     *
     * @var Homescreen
     */
    private $homescreen = false;
    
    /**
     * Return home screen for parent object
     * 
     * @return Homescreen
     */
    function get() {
      if($this->homescreen === false) {
        if($this->canHaveOwn()) {
          $this->homescreen = Homescreens::findByParent($this->object);
        } // if
        
        if(!($this->homescreen instanceof Homescreen)) {
          $this->homescreen = Homescreens::findByParent($this->object->getRole());
        } // if
        
        if(!($this->homescreen instanceof Homescreen)) {
          $this->homescreen = Homescreens::findByParent(null);
        } // if
      } // if
      
      return $this->homescreen;
    } // get
    
    /**
     * Return list of sets that can be used as a template for custom set
     * 
     * @return array
     */
    function getPossibleTemplates() {
      $result = array(
        Homescreens::findDefaultId() => lang('Based on default home screen'), 
      );
      
      if($this->object->getRole()->homescreen()->getOwnSet() instanceof Homescreen) {
        $result[$this->object->getRole()->homescreen()->getOwnSet()->getId()] = lang('Based on home screen used for ":role" role', array('role' => $this->object->getRole()->getName()));
      } // if
      
      return $result;
    } // getPossibleTemplates
    
    /**
     * Returns true if parent object can have a descktop sec configured for it
     * 
     * @return boolean
     */
    function canHaveOwn() {
      return $this->object->isAdministrator() || $this->object->getSystemPermission('can_have_homescreen');
    } // canHaveOwnSet
  
  }