<?php

  /**
   * Role home screen implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  class IRoleHomescreenImplementation extends IHomescreenImplementation {
    
    /**
     * Home screen
     *
     * @var Homescreen
     */
    private $homescreen = false;

    /**
     * Return home screen for parent object
     * 
     * @return Homescreen
     */
    function get() {
      if($this->homescreen === false) {
        $this->homescreen = Homescreens::findByParent($this->object);
        
        if(!($this->homescreen instanceof Homescreen)) {
          $this->homescreen = Homescreens::findByParent(null);
        } // if
      } // if
      
      return $this->homescreen;
    } // get
    
    /**
     * Return list of sets that can be used as a template for custom set
     * 
     * @return array
     */
    function getPossibleTemplates() {
      return array(Homescreens::findDefaultId() => lang('Based on default home screen'));
    } // getPossibleTemplates
    
    /**
     * Roles can have home screens defined for them
     * 
     * @return boolean
     */
    function canHaveOwn() {
      return true;
    } // canHaveOwn
    
  }