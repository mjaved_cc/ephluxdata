<?php

  /**
   * Model generator options for home screens framework
   *
   * @package angie.frameworks.homescreens
   * @subpackage resources
   */

  $this->setTableOptions(array('homescreens', 'homescreen_tabs', 'homescreen_widgets'), array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));