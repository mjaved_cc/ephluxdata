<?php

  /**
   * on_system_role_options event handler
   * 
   * @package angie.frameworks.homescreens
   * @subpackage handlers
   */

  /**
   * Handle on_system_role_options event
   * 
   * @param Role $role
   * @param IUser $user
   * @param NamedList $options
   */
  function homescreens_handle_on_system_role_options(Role &$role, IUser &$user, NamedList &$options) {
    if($role instanceof IHomescreen && $role->homescreen()->canHaveOwn()) {
      $options->beginWith('homescreen', array(
      	'text' => lang('Home Screen'),
        'url' => $role->homescreen()->getManageUrl(),
      	'icon' => AngieApplication::getImageUrl('configure-homescreen.png', HOMESCREENS_FRAMEWORK, AngieApplication::INTERFACE_DEFAULT), 
      ));
    } // if
  } // homescreens_handle_on_system_role_options