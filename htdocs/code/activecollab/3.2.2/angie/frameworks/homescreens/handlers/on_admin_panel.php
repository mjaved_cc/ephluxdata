<?php

  /**
   * on_admin_panel event handler
   * 
   * @package angie.framework.homescreens
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function homescreens_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToGeneral('homescreens', lang('Home Screens'), Router::assemble('homescreens_admin'), AngieApplication::getImageUrl('admin_panel/homescreens.png', HOMESCREENS_FRAMEWORK));
  } // homescreens_handle_on_admin_panel