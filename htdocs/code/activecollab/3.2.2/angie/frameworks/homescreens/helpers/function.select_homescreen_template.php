<?php

  /**
   * select_homescreen_template helper
   * 
   * @package angie.frameworks.homescreens
   * @subpackage helpers
   */

  /**
   * Render select_homescreen_template widget
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_homescreen_template($params, &$smarty) {
    $for = array_var($params, 'for', true, 'IHomescreen');
    $value = array_var($params, 'value', null, true);
    
    $possibilities = array(0 => lang('Start with blank home screen'));
    
    $other_possibilities = $for->homescreen()->getPossibleTemplates();
    if(is_foreachable($other_possibilities)) {
      $possibilities = array_merge($possibilities, $other_possibilities);
    } // if
    
    return HTML::radioGroupFromPossibilities($params['name'], $possibilities, $value, $params);
  } // smarty_function_select_homescreen_template