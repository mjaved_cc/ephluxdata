{lang language=$language}Mailbox Disabled{/lang}
================================================================================
{notification_wrapper title='Mailbox Disabled' recipient=$recipient sender=$sender}
  <p>{lang mailbox_name=$mailbox_name resolve_url=$resolve_url link_style=$style.link}Mailbox ":mailbox_name" has been disabled. <a href=":resolve_url" style=":link_style">Click here</a> to see disabled mailbox{/lang}.</p>
{/notification_wrapper}