<?php

  /**
   * Model generator options for email framework
   *
   * @package angie.frameworks.email
   * @subpackage resources
   */

  // Mailing activity logs
  $this->setTableOptions('mailing_activity_logs', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type',
  ));
  
  // Incoming mail
  $this->setTableOptions(array(
    'incoming_mail_activity_logs',
    'incoming_mail_attachments', 
    'incoming_mails'
  ), array('module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'));
  
  $this->setTableOptions('incoming_mail_filters', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'position'));
  $this->setTableOptions('incoming_mailboxes', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));