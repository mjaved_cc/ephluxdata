<?php

  /**
   * on_load_control_tower event handler
   *
   * @package angie.frameworks.email
   * @subpackage handlers
   */

  /**
   * Handle on_load_control_tower event
   *
   * @param ControlTower $control_tower
   * @param User $user
   */
  function email_handle_on_load_control_tower(ControlTower &$control_tower, User &$user) {

    // ---------------------------------------------------
    //  Email to Comment
    // ---------------------------------------------------

    $reply_to_comment_label = null;
    if(ConfigOptions::getValue('control_tower_check_reply_to_comment')) {
      $control_tower->indicators()->add('mail_to_comment', array(
        'label' => lang('Reply to Comment'),
        'value' => null,
        'url' => Router::assemble('email_admin_reply_to_comment'),
        'is_ok' => IncomingMailboxes::testReplyToComments(ApplicationMailer::getDefaultSender()->getEmail()),
      ));
    } // if

    // ---------------------------------------------------
    //  Queue Count
    // ---------------------------------------------------

    if(ConfigOptions::getValue('control_tower_check_email_queue')) {
      $queue_count_label = OutgoingMessages::countUnsent();

      if(empty($queue_count_label)) {
        $queue_count_label = null;
      } // if

      $control_tower->indicators()->add('queue', array(
        'label' => lang('Mailing Queue'),
        'value' => $queue_count_label,
        'url' => Router::assemble('outgoing_messages_admin'),
        'is_ok' => empty($queue_count_label),
      ));
    } // if

    // ---------------------------------------------------
    //  Conflicts Count
    // ---------------------------------------------------

    if(ConfigOptions::getValue('control_tower_check_email_conflicts')) {
      $conflicts_count_label = IncomingMails::countConflicts();

      if(empty($conflicts_count_label)) {
        $conflicts_count_label = null;
      } // if

      $control_tower->indicators()->add('conflicts', array(
        'label' => lang('Mail Conflicts'),
        'value' => $conflicts_count_label,
        'url' => Router::assemble('incoming_email_admin_conflict'),
        'is_ok' => empty($conflicts_count_label),
      ));
    } // if

  } // email_handle_on_load_control_tower