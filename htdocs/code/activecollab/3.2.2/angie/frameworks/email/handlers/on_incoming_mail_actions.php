<?php

  /**
   * on_incoming_mail_actions event handler implementation
   *
   * @package angie.frameworks.email
   * @subpackage handlers
   */

  /**
   * Add email related incoming mail actions
   *
   * @param array $actions
   */
  function email_handle_on_incoming_mail_actions(NamedList &$actions, IUser $user, &$unavailable_actions) {
    $actions->add('Move_to_trash', new IncomingMailMoveToTrashAction());
    $actions->add('Ignore', new IncomingMailIgnoreAction());
    //$actions->add('Add Comment', new IncomingMailCommentAction()); 
    
    if(Projects::countActive($user) > 0) {
      if(LICENSE_PACKAGE == 'corporate') {
        $actions->add('Add Task', new IncomingMailTaskAction()); 
      }//if
      $actions->add('Add Discussion', new IncomingMailDiscussionAction());
      $actions->add('Add Files', new IncomingMailFileAction());
    } else {
      if(LICENSE_PACKAGE == 'corporate') {
        $unavailable_actions[] = array(
          'action' => new IncomingMailTaskAction(),
          'reason' => lang('This action requires at least one active project defined.'),
        );
      }//if
      $unavailable_actions[] = array(
        'action' => new IncomingMailDiscussionAction(),
        'reason' => lang('This action requires at least one active project defined.'),
      );
      $unavailable_actions[] = array(
        'action' => new IncomingMailFileAction(),
        'reason' => lang('This action requires at least one active project defined.'),
      );
    } 
  } // email_handle_on_incoming_mail_actions