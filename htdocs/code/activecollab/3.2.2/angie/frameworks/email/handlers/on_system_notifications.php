<?php

  /**
   * Email on_system_notifications handler
   *
   * @package angie.frameworks.email
   * @subpackage handlers
   */

  /**
   * Handle on_system_notifications event
   *
   * @param NamedList $items
   * @param IUser $user
   */
  function email_handle_on_system_notifications(NamedList &$items, IUser &$user) {
    
    if($user->isAdministrator()) {
      $conflicts = IncomingMails::countConflicts();
      if($conflicts > 0) {
        $items->add('admin_incoming_email_conflicts', array(
          'label' => $conflicts > 1 ? lang(':count incoming email conflicts', array('count' => $conflicts)) : lang(':count incoming mail conflict', array('count' => $conflicts)),
          'class' => 'adminoverdue_invoices',
          'icon' => AngieApplication::getImageUrl('icons/12x12/important.png', ENVIRONMENT_FRAMEWORK),
          'url' => Router::assemble('incoming_email_admin_conflict'),
        ));
      }//if
    }//if
  
  } // email_handle_on_system_notifications