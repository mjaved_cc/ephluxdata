{title lang=false}{$active_outgoing_message->getSubject()}{/title}
{add_bread_crumb}Message Details{/add_bread_crumb}

<div id="message_sent_details" class="object_inspector">
  <div class="head">
    <div class="properties">
      <div class="property">
        <div class="label">{lang}Sender{/lang}</div>
        <div class="data">{user_link user=$active_outgoing_message->getSender()}</div>
      </div>
      
      <div class="property">
        <div class="label">{lang}Recipient{/lang}</div>
        <div class="data">{user_link user=$active_outgoing_message->getRecipient()}</div>
      </div>
      
      <div class="property">
        <div class="label">{lang}Subject{/lang}</div>
        <div class="data">{$active_outgoing_message->getSubject()}</div>
      </div>
    </div>
  </div>
  
  <div class="body">{$active_outgoing_message->getBody() nofilter}</div>
</div>