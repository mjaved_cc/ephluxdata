{title}Mass Mailer{/title}

<div id="mass_mailer">
	{form action=Router::assemble('admin_tools_mass_mailer') class='big_form'}
		<script type="text/javascript">
      App.widgets.FlyoutDialog.front().setAutoSize(false);
		</script>
	
		<div class="big_form_wrapper one_form_sidebar">
		  <div class="main_form_column">
		    {wrap field=subject}
			    {text_field name="email[subject]" value=$email_data.subject id=emailSubject class='title' label="Subject" required=true}
			  {/wrap}
			  
			  {wrap_editor field=body}
          {label required=true}Content{/label}
			    {editor_field name="email[body]" id=emailBody disable_image_upload=true required=true}{$email_data.body nofilter}{/editor_field}
			  {/wrap_editor}
		  </div>
		  
		  <div class="form_sidebar form_second_sidebar">
		    {wrap field=recipients}
			    {select_users name="email[recipients]" value=$email_data.recipients exclude=$exclude user=$logged_user label='Recipients'}
			  {/wrap} 
		  </div>
		</div>
	  
	  {wrap_buttons}
	    {submit}Send Email{/submit}
	  {/wrap_buttons}
	{/form}
</div>