<?php

  /**
   * Model generator options for attachments framework
   *
   * @package angie.frameworks.attachments
   * @subpackage resources
   */

  // Attachments
  $this->setTableOptions('attachments', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type'
  ));