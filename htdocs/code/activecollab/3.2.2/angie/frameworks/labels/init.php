<?php

  /**
   * Labels framework initialization file
   *
   * @package angie.frameworks.labels
   */
  
  define('LABELS_FRAMEWORK', 'labels');
  define('LABELS_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/labels');
  
  @define('LABELS_FRAMEWORK_INJECT_INTO', 'system'); // Inject labels framework into given module
  @define('LABELS_FRAMEWORK_ADMIN_ROUTE_BASE', 'admin'); // Base string for all admin routes
  
  AngieApplication::setForAutoload(array(
    'ILabel' => LABELS_FRAMEWORK_PATH . '/models/ILabel.class.php',
    'ILabelImplementation' => LABELS_FRAMEWORK_PATH . '/models/ILabelImplementation.class.php',
    
    'FwLabel' => LABELS_FRAMEWORK_PATH . '/models/labels/FwLabel.class.php',
    'FwLabels' => LABELS_FRAMEWORK_PATH . '/models/labels/FwLabels.class.php',
   
    'LabelInspectorTitlebarWidget' => LABELS_FRAMEWORK_PATH . '/models/LabelInspectorTitlebarWidget.class.php',
    'LabelInspectorProperty' => LABELS_FRAMEWORK_PATH . '/models/LabelInspectorProperty.class.php',
  ));