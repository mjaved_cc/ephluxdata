<?php

  /**
   * Developer module on_status_bar event handler
   *
   * @package angie.frameworks.developer
   * @subpackage handlers
   */
  
  /**
   * Register status bar items
   *
   * @param StatusBar $status_bar
   * @param IUser $logged_user
   */
  function developer_handle_on_status_bar(StatusBar &$status_bar, IUser &$user) {
    if($user->isAdministrator()) {
      $status_bar->add('developer_tools', lang('Admin Tools'), Router::assemble('developer_popup'), AngieApplication::getImageUrl('icons/12x12/developer-tools.png', DEVELOPER_FRAMEWORK), array(
        'group' => StatusBar::GROUP_RIGHT 
      ));
    } // if
  } // developer_handle_on_status_bar