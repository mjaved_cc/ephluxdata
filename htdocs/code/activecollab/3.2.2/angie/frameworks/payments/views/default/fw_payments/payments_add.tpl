{title}Make a Payment{/title}
{add_bread_crumb}Make a Payment{/add_bread_crumb}

<div id="add_payment_gateway">
  {form action=$active_object->payments()->getAddUrl()}
  	<div class='content_stack_wrapper autoscrolled'>
    	{make_payment user=$logged_user object=$active_object}
    </div>
    {wrap_buttons}
      {submit}Make payment{/submit}
    {/wrap_buttons}
  {/form}
</div>