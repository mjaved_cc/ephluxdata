{title}Edit Payment{/title}
{add_bread_crumb}Edit Payment{/add_bread_crumb}

<div class="payment_user_form">
{form action="{$active_payment->getEditUrl()}" method="POST"}

{wrap field=type}
  {label for=last_name required=yes}Status{/label}
  {select_payment_status name="payment[status]" selected=$active_payment->getStatus() id="payment_status"}
{/wrap}

{wrap field=type}
  {label for=last_name required=yes}Status Reason{/label}
  {select_payment_status_reason name="payment[status_reason]" selected=$active_payment->getReason() id="payment_status_reason"}
{/wrap}

{wrap field=type id="reason_text_box"}
  {label for=last_name required=yes}Reason Text{/label}
  <textarea name="payment[status_reason_text]" id="payment_status_reason_text" style="width:420px;">{$active_payment->getReasonText()}</textarea>
{/wrap}

{wrap_buttons}
  {submit}Edit Payment{/submit}
{/wrap_buttons}
	
	
{/form}
</div>
<script type="text/javascript">
$(document).ready(function() { 
	var reason = $('#payment_status_reason');
	var reason_text_box = $('#reason_text_box');
	if(reason.val() == 0) {
		reason_text_box.hide();	
	}
	
	reason.change(function () {
		if(reason.val() == 0) {
			reason_text_box.hide('slow');
		} else {
			reason_text_box.show('slow');
		}	
	});
});
</script>