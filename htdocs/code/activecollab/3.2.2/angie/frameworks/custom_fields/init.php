<?php

  /**
   * Custom fields initialization file
   *
   * @package angie.frameworks.custom_fields
   */
  define('CUSTOM_FIELDS_FRAMEWORK', 'custom_fields');
  define('CUSTOM_FIELDS_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/custom_fields');

  // Inject custom fields framework into specified module
  @define('CUSTOM_FIELDS_FRAMEWORK_INJECT_INTO', 'system');

  AngieApplication::setForAutoload(array(
    'FwCustomFields' => CUSTOM_FIELDS_FRAMEWORK_PATH . '/models/FwCustomFields.class.php',

    'ICustomFields' => CUSTOM_FIELDS_FRAMEWORK_PATH . '/models/ICustomFields.class.php',
    'ICustomFieldsImplementation' => CUSTOM_FIELDS_FRAMEWORK_PATH . '/models/ICustomFieldsImplementation.class.php',

    'CustomFieldInspectorProperty' => CUSTOM_FIELDS_FRAMEWORK_PATH . '/models/CustomFieldInspectorProperty.class.php',
  ));