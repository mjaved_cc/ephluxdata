<?php

  /**
   * Model generator options for history framework
   *
   * @package angie.frameworks.history
   * @subpackage resources
   */
  
  $this->setTableOptions('modification_logs', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'created_on'));