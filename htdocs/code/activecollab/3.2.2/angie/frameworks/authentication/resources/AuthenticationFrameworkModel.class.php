<?php

  /**
   * Authentication framework model definition
   *
   * @package angie.frameworks.authentication
   * @subpackage resources
   */
  class AuthenticationFrameworkModel extends AngieFrameworkModel {
    
    /**
     * Construct authentication framework model
     * 
     * @param AuthenticationFramework $parent
     */
    function __construct(AuthenticationFramework $parent) {
      parent::__construct($parent);
      
      // API client subscriptions
      $this->addModel(DB::createTable('api_client_subscriptions')->addColumns(array(
        DBIdColumn::create(), 
        DBTypeColumn::create('ApiClientSubscription'), 
        DBIntegerColumn::create('user_id', 10, '0')->setUnsigned(true), 
        DBStringColumn::create('token', 40), 
        DBStringColumn::create('client_name', 100), 
        DBStringColumn::create('client_vendor', 100), 
        DBDateTimeColumn::create('created_on'), 
        DBDateTimeColumn::create('last_used_on'), 
        DBBoolColumn::create('is_enabled', false), 
        DBBoolColumn::create('is_read_only', true), 
      ))->addIndices(array(
        DBIndex::create('token', DBIndex::UNIQUE, 'token'), 
      )))->setTypeFromField('type');
      
      // Roles and Permissions
      $this->addModel(DB::createTable('roles')->addColumns(array(
        DBIdColumn::create(DBColumn::SMALL),  
        DBNameColumn::create(50), 
        DBTextColumn::create('permissions'), 
        DBBoolColumn::create('is_default'), 
      ))->addIndices(array(
        DBIndex::create('name', DBIndex::UNIQUE, 'name'), 
      )));
      
      // Users model
      $this->addModel(DB::createTable('users')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('role_id', 3)->setUnsigned(true), 
        DBStateColumn::create(), 
        DBStringColumn::create('first_name', 50), 
        DBStringColumn::create('last_name', 50), 
        DBStringColumn::create('email', 150, ''), 
        DBStringColumn::create('password', 255, ''),
        DBEnumColumn::create('password_hashed_with', array('pbkdf2', 'sha1'), 'pbkdf2'),
        DBDateColumn::create('password_expires_on'),
        DBStringColumn::create('password_reset_key', 20), 
        DBDateTimeColumn::create('password_reset_on'),
        DBActionOnByColumn::create('created'), 
        DBActionOnByColumn::create('updated'), 
        DBDateTimeColumn::create('last_login_on'), 
        DBDateTimeColumn::create('last_visit_on'), 
        DBDateTimeColumn::create('last_activity_on'), 
      ))->addIndices(array(
        DBIndex::create('email'),
        DBIndex::create('role_id'), 
        DBIndex::create('last_activity_on'), 
      )));
      
      // User sessions
      $this->addTable(DB::createTable('user_sessions')->addColumns(array(
        DBIdColumn::create(), 
        DBIntegerColumn::create('user_id', 10, '0')->setUnsigned(true), 
        DBIpAddressColumn::create('user_ip'), 
        DBStringColumn::create('user_agent', 255), 
        DBIntegerColumn::create('visits', 10, '0')->setUnsigned(true), 
        DBIntegerColumn::create('remember', 3, '0')->setUnsigned(true),
        DBEnumColumn::create('interface', array('default', 'phone', 'tablet'), 'default'), 
        DBDateTimeColumn::create('created_on'), 
        DBDateTimeColumn::create('last_activity_on'), 
        DBDateTimeColumn::create('expires_on'), 
        DBStringColumn::create('session_key', 40), 
      ))->addIndices(array(
        DBIndex::create('session_key', DBIndex::UNIQUE, 'session_key'), 
        DBIndex::create('expires_on'), 
      )));    
    } // __construct
    
    /**
     * Load initial data
     * 
     * @param string $environment
     */
    function loadInitialData($environment = null) {
      parent::loadInitialData($environment);

      $this->addConfigOption('maintenance_enabled', false);
      
      $this->addRole('Administrator', array(
        'has_system_access' => true,
      	'has_admin_access' => true,
        'can_use_api' => true,
      	'can_use_feeds' => true,
      	'can_see_private_objects' => true, 
      ), false);
      
      $this->addRole('Employee', array(
        'has_system_access' => true, 
        'can_use_api' => true,
      	'can_use_feeds' => true,
      	'can_see_private_objects' => true, 
      ), true);
    } // loadInitialData
    
  }