<?php

  /**
   * Model generator options for authentication framework
   *
   * @package angie.frameworks.authentication
   * @subpackage resources
   */

  // Users and roles
  $this->setTableOptions('users', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'CONCAT(first_name, last_name, email)'));
  $this->setTableOptions('roles', array('module' => 'system', 'object_extends' => 'ApplicationObject', 'order_by' => 'name'));

  // API client subscriptions
  $this->setTableOptions('api_client_subscriptions', array(
    'module' => 'system', 'object_extends' => 'ApplicationObject', 'class_name_from_field' => 'type',
  ));