<?php

  /**
   * select_user helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render select user control
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_user($params, &$smarty) {
    $user = array_required_var($params, 'user', true, 'IUser');
    
    $object = array_var($params, 'object', null, true);
    
    if(isset($params['class'])) {
      $params['class'] .= ' select_user';
    } else {
      $params['class'] = 'select_user';
    } // if
    
    // Users are provided
    if(array_key_exists('users', $params)) {
      $grouped_users = array_var($params, 'users', null, true);
      
    // We need to load users
    } else {
      if($object) {
        if($object instanceof IUsersContext) {
          $grouped_users = $object->users()->getForSelect($user, array_var($params, 'exclude_ids', null, true));
        } else {
          throw new InvalidInstanceError('object', $object, 'IUsersContext');
        } // if
      } else {
        $grouped_users = Users::getForSelect($user, array_var($params, 'exclude_ids', null, true));
      } // if
    } // if
    
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('select_user');
    } // if
    
    $name = array_var($params, 'name', null, true);  
    $value = array_var($params, 'value', null, true);

    // prepare selected user's information in case he's archived/trashed/deleted
    if (!is_null($value)) {
      $selected_user = Users::getForSelectByConditions(array("id = ?", $value));
      if (is_foreachable($selected_user)) {
        $selected_users_company = key($selected_user);
        $selected_users_id = key($selected_user[$selected_users_company]);
        $selected_users_display_name = $selected_user[$selected_users_company][$selected_users_id];
      } // if
    } // if
    
    $options = array();
    if(is_foreachable($grouped_users)) {

      // create a company and put the user if he is the only one visible
      if (isset($selected_user) && is_foreachable($selected_user) && !array_key_exists($selected_users_company, $grouped_users)) {
        $grouped_users[$selected_users_company][$selected_users_id] = $selected_users_display_name;
      } // if

      foreach($grouped_users as $company_name => $users) {
        $company_users = array();

        // put user in the list if he is archived/trashed/deleted
        if (isset($selected_user) && is_foreachable($selected_user) && $company_name == $selected_users_company && !array_key_exists($selected_users_id, $users)) {
          $users[$selected_users_id] = $selected_users_display_name;
        } // if
        
        foreach($users as $user_id => $user_display_name) {
          $company_users[] = option_tag($user_display_name, $user_id, array(
            'selected' => $user_id == $value, 
          ));
        } // foreach
        
        $options[] = option_group_tag($company_name, $company_users);
      } // foreach
    } else {
      return lang('No users available');
    } // if
    
    if(array_var($params, 'optional', false, true)) {
      $optional_text = array_var($params, 'optional_text', lang('-- None --'), true);
      
      return HTML::optionalSelect($name, $options, $params, $optional_text);
    } else {
      return HTML::select($name, $options, $params);
    } // if
  } // smarty_function_select_user