<?php

  /**
   * on_wireframe_updates event handler implementation
   * 
   * @package angie.frameworks.environment
   * @subpackage handlers
   */

  /**
   * Handle on_wireframe_updates event
   * 
   * @param array $wireframe_data
   * @param array $response_data
   * @param User $user
   */
  function environment_handle_on_wireframe_updates(&$wireframe_data, &$response_data, &$user) {
    if(isset($wireframe_data['refresh_backend_wireframe']) && $wireframe_data['refresh_backend_wireframe']) {
      $main_menu = new MainMenu();
      $main_menu->load($user);
      
      $status_bar = new StatusBar();
      $status_bar->load($user);
      
      $response_data['refresh_backend_wireframe'] = array(
        'main_menu' => $main_menu, 
        'status_bar' => $status_bar, 
      );
    } // if

    if($user->isAdministrator()) {
      $control_tower = new ControlTower($user);
      $response_data['status_bar_badges']['control_tower'] = $control_tower->loadBadgeValue();
    } // if
  } // environment_handle_on_wireframe_updates