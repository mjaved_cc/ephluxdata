<?php

  /**
   * Framework level main menu implementation
   * 
   * @package angie.frameworks.environment
   * @subpackage model
   */
  class FwMainMenu extends NamedList {
    
    /**
     * Indicates whether status bar is loaded or not
     *
     * @var boolean
     */
    protected $is_loaded = false;
    
    /**
     * Returns true if status bar is loaded
     * 
     * @return boolean
     */
    function isLoaded() {
      return $this->is_loaded;
    } // isLoaded
    
    /**
     * Load status bar items
     * 
     * @param IUser $user
     */
    function load(IUser $user) {
      if($this->isLoaded()) {
        return;
      } // if
      
      $this->add('homepage', lang('Home Screen'), Router::assemble('homepage'), AngieApplication::getImageUrl('main-menu/homepage.png', ENVIRONMENT_FRAMEWORK));
      
      if($user->isAdministrator()) {
  	    $this->add('admin', lang('Administration'), Router::assemble('admin'), AngieApplication::getImageUrl('main-menu/administration.png', ENVIRONMENT_FRAMEWORK));
  	  } // if
  	  
  	  $this->add('profile', $user->getDisplayName(), $user->getViewUrl(), $user->avatar()->getUrl(IUserAvatarImplementation::SIZE_BIG), array(
  	    'group' => 'profile', 
  	    'onclick' => new ProfileMenuItemCallback(), 
  	    'onclick_data' => $user->describe($user, true, true), 
  	  ));
        
      EventsManager::trigger('on_main_menu', array(&$this, &$user));
    } // load
    
    // ---------------------------------------------------
    //  Overrides
    // ---------------------------------------------------
  
    /**
     * Add an item to the row
     * 
     * @param string $name
     * @param string $title
     * @param string $url
     * @param string $icon_url
     * @param array $additional
     * @return mixed
     */
    function add($name, $title, $url, $icon_url, $additional = null) {
      $data = array(
        'title' => $title, 
        'url' => $url, 
        'icon' => $icon_url,
      );
      
      if($additional) {
        $data = array_merge($data, $additional);
      } // if
      
      return parent::add($name, $data);
    } // add
    
    /**
     * Add data to the beginning of the list
     *
     * @param string $name
     * @param string $title
     * @param string $url
     * @param string $icon_url
     * @param array $additional
     * @return mixed
     */
    function beginWith($name, $title, $url, $icon_url, $additional = null) {
      $data = array(
        'title' => $title, 
        'url' => $url, 
        'icon' => $icon_url,
      );
      
      if($additional) {
        $data = array_merge($data, $additional);
      } // if
      
      return parent::beginWith($name, $data);
    } // beginWith
    
    /**
     * Add data before $before element
     *
     * @param string $name
     * @param string $title
     * @param string $url
     * @param string $icon_url
     * @param array $additional
     * @param string $before
     * @return mixed
     */
    function addBefore($name, $title, $url, $icon_url, $additional, $before) {
      $data = array(
        'title' => $title, 
        'url' => $url, 
        'icon' => $icon_url,
      );
      
      if($additional) {
        $data = array_merge($data, $additional);
      } // if
      
      return parent::addBefore($name, $data, $before);
    } // addBefore
    
    /**
     * Add item after $after list element
     *
     * @param string $name
     * @param string $title
     * @param string $url
     * @param string $icon_url
     * @param array $additional
     * @param string $after
     * @return mixed
     */
    function addAfter($name, $title, $url, $icon_url, $additional, $after) {
      $data = array(
        'title' => $title, 
        'url' => $url, 
        'icon' => $icon_url,
      );
      
      if($additional) {
        $data = array_merge($data, $additional);
      } // if
      
      return parent::addAfter($name, $data, $after);
    } // addAfter
    
  }