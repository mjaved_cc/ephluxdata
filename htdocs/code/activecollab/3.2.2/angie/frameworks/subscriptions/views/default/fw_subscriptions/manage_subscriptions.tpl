<div id="object_subscriptions" class="fields_wrapper">
{if is_foreachable($grouped_subscribers)}
  {foreach $grouped_subscribers as $group_name => $group_subscribers}
    <table class="common" cellspacing="0">
      <thead>
        <tr>
	        <th colspan="3" class="company_name">{$group_name}</th>
	      </tr>
      </thead>
      <tbody>
	    {foreach $group_subscribers as $subscriber}
	      <tr class="{cycle values='odd,even'} subscriber">
	        <td class="avatar"><img src="{$subscriber->avatar()->getUrl(IUserAvatarImplementation::SIZE_SMALL)}" alt="" /></td>
	        <td class="name">{$subscriber->getDisplayName()}</td>
	        <td class="subscription">
	          <input type="checkbox" class="auto input_checkbox" on_url="{$active_object->subscriptions()->getSubscribeUrl($subscriber)}" off_url="{$active_object->subscriptions()->getUnsubscribeUrl($subscriber)}" {if $active_object->subscriptions()->isSubscribed($subscriber)}checked="checked"{/if} {if !$can_be_managed}disabled="disabled"{/if} />
	        </td>
	      </tr>
	    {/foreach}
      </tbody>
    </table>
  {/foreach}
{/if}
</div>

<script type="text/javascript">
  if ({$can_be_managed|json nofilter}) {
    $('#object_subscriptions table td.subscription input[type=checkbox]').asyncCheckbox({
      'success_event' : {$active_object->getUpdatedEventName()|json nofilter}
    });
  } // if
</script>