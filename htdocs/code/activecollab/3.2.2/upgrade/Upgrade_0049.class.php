<?php

  /**
   * Update activeCollab 3.1.17 to activeCollab 3.1.18
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0049 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.1.17';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.1.18';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return null;
    } // getActions

  }