<?php

  /**
   * Update activeCollab 3.2.0 to activeCollab 3.2.1
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0052 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.2.0';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.2.1';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return array(
        'upgradeOutgoingMessages' => 'Upgrade outgoing messages table',
      );
    } // getActions

    /**
     * Upgrade outgoing messages table
     *
     * @return bool|string
     */
    function upgradeOutgoingMessages() {
      try {
        DB::execute('ALTER TABLE ' . TABLE_PREFIX . 'outgoing_messages ADD code VARCHAR(25) NULL DEFAULT NULL AFTER context_id');
      } catch(Exception $e) {
        return $e->getMessage();
      } // if
      return true;
    } // upgradeOutgoingMessages

  }