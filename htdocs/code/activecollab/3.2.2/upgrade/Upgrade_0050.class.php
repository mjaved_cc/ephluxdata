<?php

/**
 * Update activeCollab 3.1.18 to activeCollab 3.1.19
 *
 * @package activeCollab.upgrade
 * @subpackage scripts
 */
class Upgrade_0050 extends AngieApplicationUpgradeScript {

  /**
   * Initial system version
   *
   * @var string
   */
  public $from_version = '3.1.18';

  /**
   * Final system version
   *
   * @var string
   */
  public $to_version = '3.1.19';

  /**
   * Return script actions
   *
   * @return array
   */
  function getActions() {
    return array(
      'removeDeprecatedFonts' => 'Remove deprecated fonts',
    );
  } // getActions

  /**
   * Remove deprecated Fonts
   *
   * @return boolean
   */
  function removeDeprecatedFonts() {
    try {
      $config_option_name = 'invoice_template'; // config option name
      $invoice_template = unserialize(DB::executeFirstCell('SELECT value FROM ' . TABLE_PREFIX . 'config_options WHERE name = ?', $config_option_name));

      $affected_properties = array(
        'header_font',
        'client_details_font',
        'invoice_details_font',
        'items_font',
        'note_font',
        'footer_font'
      );

      $replacements = array(
        'freesans'    => 'dejavusans',
        'freesansb'   => 'dejavusansb',
        'freesansbi'  => 'dejavusansbi',
        'freesansi'   => 'dejavusansi',
        'freeserif'   => 'dejavuserif',
        'freeserifb'  => 'dejavuserifb',
        'freeserifbi' => 'dejavuserifbi',
        'freeserifi'  => 'dejavuserifi'
      );

      if (is_foreachable($affected_properties)) {
        foreach ($affected_properties as $affected_property) {
          $affected_font = isset($invoice_template[$affected_property]) ? $invoice_template[$affected_property] : null;

          if (!$affected_font) {
            continue;
          } // if

          if (array_key_exists($affected_font, $replacements)) {
            $invoice_template[$affected_property] = $replacements[$affected_font];
          } // if
        } // foreach
      } // if

      // update config option
      DB::execute('UPDATE ' . TABLE_PREFIX . 'config_options SET value = ? WHERE name = ?', serialize($invoice_template), $config_option_name);
    } catch (Exception $e) {
      return $e->getMessage();
    } // try
  } // removeDeprecatedFonts

}