<?php

  /**
   * Update activeCollab 3.2.1 to activeCollab 3.2.2
   *
   * @package activeCollab.upgrade
   * @subpackage scripts
   */
  class Upgrade_0053 extends AngieApplicationUpgradeScript {

    /**
     * Initial system version
     *
     * @var string
     */
    public $from_version = '3.2.1';

    /**
     * Final system version
     *
     * @var string
     */
    public $to_version = '3.2.2';

    /**
     * Return script actions
     *
     * @return array
     */
    function getActions() {
      return array(
        'scheduleIndexesRebuild' => 'Schedule index rebuild',
        'endUpgrade' => 'Finish upgrade',
      );
    } // getActions

  }