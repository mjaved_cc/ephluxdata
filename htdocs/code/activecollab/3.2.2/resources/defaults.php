<?php

  /**
   * Default configuration values
   */
  
  define('APPLICATION_NAME', 'ActiveCollab');
  define('APPLICATION_BUILD', '3754');

  @define('APPLICATION_VERSION_STABLE', 0);
  
  // If we are using unpacked file, make sure that value is well set
  if(defined('USE_UNPACKED_FILES') && USE_UNPACKED_FILES) {
    define('APPLICATION_PATH', ROOT . '/' . APPLICATION_VERSION);
    
    if(!defined('ANGIE_PATH')) {
      define('ANGIE_PATH', APPLICATION_PATH . '/angie');
    } // if
    
  // Not using packed files? Properly set angie path
  } else {
    if(!defined('USE_UNPACKED_FILES')) {
      define('USE_UNPACKED_FILES', false);
    } // if
    
    define('APPLICATION_PATH', 'phar://' . APPLICATION_NAME . '-' . APPLICATION_VERSION . '.phar');
    
    if(!defined('ANGIE_PATH')) {
      define('ANGIE_PATH', APPLICATION_PATH . '/angie');
    } // if
  } // if

  if(!defined('APPLICATION_MODE')) {
    define('APPLICATION_MODE', 'in_production');
  } // if
  
  define('APPLICATION_FRAMEWORKS', 'environment,modules,globalization,authentication,activity_logs,reports,history,email,download,preview,homescreens,complete,attachments,subscriptions,comments,categories,labels,assignees,subtasks,favorites,visual_editor,file_uploader,payments,avatar,text_compare,reminders,search,custom_fields');
  
  if(LICENSE_PACKAGE == 'corporate') {
    define('APPLICATION_MODULES', 'system,discussions,milestones,files,todo,calendar,notebooks,tasks,tracking,project_exporter,status,documents,source,invoicing,flow,sitemaps');
  } elseif(LICENSE_PACKAGE == 'smallbiz') {
    define('APPLICATION_MODULES', 'system,discussions,milestones,files,todo');
  } else {
    define('APPLICATION_MODULES', 'system');
  } // if

  @define('GLOBALIZATION_ADAPTER', 'ActiveCollabGlobalizationAdapter');
  
  // Use activeCollab specific favorites routes
  define('FAVORITES_FRAMEWORK_DEFINE_ROUTES', false);

  define('TEST_SMTP_BY_SENDING_EMAIL_TO', 'noreply@activecollab.com');
  
  // ---------------------------------------------------
  //  Defaults MVC mapping
  // ---------------------------------------------------

  define('DEFAULT_CONTROLLER', 'backend');

  define('UPDATE_INSTRUCTIONS_URL', 'http://www.activecollab.com/docs/manuals/admin-version-3/upgrade/latest-stable');
  define('UPGRADE_TO_CORPORATE_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/upgrade-to-corporate?license_key=' . LICENSE_KEY);
  define('REMOVE_BRANDING_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/purchase-branding-removal?license_key=' . LICENSE_KEY);
  define('RENEW_SUPPORT_URL', 'http://www.activecollab.com/user/' . LICENSE_UID . '/extend-support?license_key=' . LICENSE_KEY);