<div class="page_wrapper">
{form action="$settings_source_url" method="post" id="main_form"}
  <div id="mercurial_settings">
      <table id="source_admin_table">
        <tr>
          <td class="field">
            {wrap field=mercurial_path}
              {label for=mercurial_path required=false}Mercurial Location{/label}
              {text_field id='mercurial_path' name="source[mercurial_path]" value=$source_data.mercurial_path}
              <p class="details">{lang}Enter path for Mercurial executable (without executable name){/lang}</p>
            {/wrap}
          </td>
          <td class="test_facility">
            <div class="admin_test_setting" id="check_mercurial_path">
              <button type="button"><span><span>{lang}Check Mercurial Path{/lang}</span></span></button>
              <span class="test_results">
                <span></span>
              </span>
            </div>
          </td>
        </tr>
      </table>
  </div>
      
  {wrap_buttons}
    {submit}Save Changes{/submit}
  {/wrap_buttons}
    
{/form}
</div>
<input type="hidden" value="{$test_mercurial_url}" id="test_mercurial_url" />
{literal}
<!--Javascript-->
<script type="text/javascript">

    $(document).ready(function () {
      var test_results_div = $(this).find('.test_results');
      var test_div = test_results_div.parent();
	  
      test_results_div.prepend('<img class="source_results_img" src="" alt=""/>');
      $('.source_results_img').hide();

//	   AJAX call for testing mercurial path
      $('#check_mercurial_path button').click(function () {
        $('.source_results_img').show();
        var mercurial_path = $('#mercurial_path').val();
        var indicator_img = $('.source_results_img');
        var result_span = test_div.find('.test_results span:eq(0)');
        indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl());
        result_span.html('');
        var test_url = App.extendUrl($('#test_mercurial_url').val(), {
			'async' : 1, 
			'mercurial_path' : mercurial_path
		});
        $.get(test_url, function(data) {
          if ($.trim(data)=='true') {
            indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl('ok'));
            result_span.html(App.lang('Mercurial executable found'));
          } else {
            indicator_img.attr('src', App.Wireframe.Utils.indicatorUrl('error'));
            result_span.html(App.lang('Error accessing Mercurial executable') + ': ' + data);
          } // if
      	});
      });
    });
</script>
{/literal}