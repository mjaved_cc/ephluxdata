<div id="change_branch_wrapper" class="fields_wrapper">
  <input type="text" id="filter_source_branches" placeholder="Filter branches"/>
  <table class="common" id="change_branch_table">
    <thead>
      <tr>
        <th>{lang}Repository Branches{/lang}</th>
      </tr>
    </thead>
    <tbody>
      {foreach $all_branches as $branch}

        <tr class="branch_row" _search_index=" {strtolower($branch)} ">
          <td class="branch_name">
            {radio_field name=branch value=$branch label=$branch checked=($active_branch == $branch) change_branche_url=$project_object_repository->getDoChangeBranchUrl($branch)}
          </td>
        </tr>
      {/foreach}
    </tbody>
  </table>
</div>

<script type="text/javascript">
  var active_branch = {$active_branch|json nofilter};
  var branch_table = $('#change_branch_table');
  var filter_source_branches = $("#filter_source_branches");


  branch_table.find('input:radio').click(function() {
    if (active_branch == $(this).val()) {
      return false;
    } else {
      if (confirm(App.lang('Are you sure that you want to set this branch as default branch'))) {
        $.ajax({
          'url' : App.extendUrl($(this).attr('change_branche_url'),{ 'async' : 1}),
          'type' : 'GET',
          'success' : function (data) {
            App.widgets.FlyoutDialog.front().close();
            App.Wireframe.Content.reload();
          }
        });
      } //if
      return false;
    } //if
  });

  filter_source_branches.quickSearch({
    'target' : branch_table,
    'rows' : 'tr.branch_row'
  });

  $(document).ready (function () {
    filter_source_branches.focus();
  });


</script>