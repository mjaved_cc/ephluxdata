<?php
  
  // Extend projects controller
  AngieApplication::useController('project', SYSTEM_MODULE);
  
  /**
   * Project calendar controller
   *
   * @package activeCollab.modules.calendar
   * @subpackage controllers
   */
  class ProjectCalendarController extends ProjectController {
    
    /**
     * Active module
     *
     * @var string
     */
    protected $active_module = CALENDAR_MODULE;
    
    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->breadcrumbs->add('project_calendar', lang('Calendar'), Calendar::getProjectCalendarUrl($this->active_project));
    } // __construct
    
    /**
     * Index
     */
    function index() {
      require_once CALENDAR_MODULE_PATH . '/models/generators/ProjectCalendarGenerator.class.php';
      
      $this->wireframe->tabs->setCurrentTab('calendar');

      if ($this->logged_user->isFeedUser()) {
        $this->wireframe->actions->add('ical', lang('iCalendar Feed'), Router::assemble('project_ical_subscribe', array(
          'project_slug' => $this->active_project->getSlug()
        )), array(
          'onclick' => new FlyoutCallback()
        ));
      } // if
      
      if($this->request->get('month') && $this->request->get('year')) {
      	$date = DateTimeValue::make(0, 0, 0, $this->request->get('month'), 1, $this->request->get('year'));
      } else {
        $date = new DateTimeValue(time() + get_user_gmt_offset());
      } // if
      
      $first_weekday = ConfigOptions::getValueFor('time_first_week_day', $this->logged_user);
      $work_days = ConfigOptions::getValueFor('time_workdays', $this->logged_user);

      $generator = new ProjectCalendarGenerator($date->getMonth(), $date->getYear(), $first_weekday, $work_days);
			$generator->setProject($this->active_project);
			$generator->setData(Calendar::getProjectData($this->logged_user, $this->active_project, $date->getMonth(), $date->getYear()));

      $this->smarty->assign(array(
      	'date'		 => $date,
        'calendar' => $generator,
      ));
    } // index
    
    /**
     * Show events for a given day
     */
    function day() {
      if($this->request->get('year') && $this->request->get('month') && $this->request->get('day')) {
        $day = new DateValue($this->request->get('year') . '-' . $this->request->get('month') . '-' . $this->request->get('day'));
      } else {
        $day = DateValue::now();
      } // if
      
      $this->wireframe->breadcrumbs->add('project_calendar_day', $day->getYear() . ' / ' . $day->getMonth(), Calendar::getProjectMonthUrl($this->active_project, $day->getYear(), $day->getMonth()));
      $objects = Calendar::getProjectDayData($this->logged_user, $this->active_project, $day);
      
    	$this->smarty->assign(array(
        'day' => $day,
        'objects' => $objects,
      ));
    } // day
    
  }