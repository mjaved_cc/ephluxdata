<?php

  /**
   * InvoiceItemTemplates class
   * 
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class InvoiceItemTemplates extends BaseInvoiceItemTemplates {
  	
  	/**
  	 * Find templates for select

  	 * @return array
  	 */
  	function findForSelect() {
  		$invoice_item_templates = InvoiceItemTemplates::find();
  		
      $cleaned_item_templates = array();
      if(is_foreachable($invoice_item_templates)) {
        foreach ($invoice_item_templates as $invoice_item_template) {
        	$cleaned_item_templates[$invoice_item_template->getId()] = array(
        	 'description' => $invoice_item_template->getDescription(),
        	 'unit_cost' => $invoice_item_template->getUnitCost(),
        	 'quantity' => $invoice_item_template->getQuantity(),
        	 'tax_rate_id' => $invoice_item_template->getTaxRateId(),
        	);
        } // foreach
      } // if
  		
      return $cleaned_item_templates;
  	} // findForSelect
  
  	/**
  	 * Return slice of invoice item template definitions based on given criteria
  	 * 
  	 * @param integer $num
  	 * @param array $exclude
  	 * @param integer $timestamp
  	 * @return DBResult
  	 */
  	function getSlice($num = 10, $exclude = null, $timestamp = null) {
  		if($exclude) {
  			return self::find(array(
  			  'conditions' => array('id NOT IN (?)', $exclude), 
  			  'order' => 'description', 
  			  'limit' => $num,  
  			));
  		} else {
  			return self::find(array(
  			  'order' => 'description', 
  			  'limit' => $num,  
  			));
  		} // if
  	} // getSlice
  	
  }