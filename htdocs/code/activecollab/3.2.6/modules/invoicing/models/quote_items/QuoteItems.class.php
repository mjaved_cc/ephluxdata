<?php

  /**
   * QuoteItems class
   *
   * @package activeCollab.modules.invoicing
   * @subpackage models
   */
  class QuoteItems extends BaseQuoteItems {
  
    /**
     * Return items by quote
     *
     * @param Quote $quote
     * @return array
     */
    static function findByQuote(Quote $quote) {
      return QuoteItems::find(array(
        'conditions' => array('quote_id = ?', $quote->getId()),
        'order' => 'position'
      ));
    } // findByQuote
    
    /**
     * Delete all items for $quote
     *
     * @param Quote $quote
     * @return null
     */
    static function deleteByQuote(Quote $quote) {
    	try {
	      DB::beginWork();
	      
        QuoteItems::delete(array('quote_id = ?', $quote->getId()));
        
        DB::commit();
    	} catch(Exception $e) {
    		DB::rollback();
    		
    		throw $e;
    	} // try
    } // deleteByQuote
  
  }