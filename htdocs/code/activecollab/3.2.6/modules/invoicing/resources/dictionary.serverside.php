<?php return array(
  'Invoicing Company Identity Settings',
  'Company name is required',
  'Company details are required',
  'Could not upload company logo',
  'Invoices',
  'Issued Invoices',
  'Paid Invoices',
  'Canceled Invoices',
  'All Invoices',
  'Draft',
  'Grouped by Status',
  'Grouped by Issued On Month',
  'Grouped by Due On Month',
  '#:invoice_id.pdf',
  'Quotes',
  '#:quote_id.pdf',
  'Invoicing',
  'Invoice Item Templates',
  'New Invoice Item',
  'Invoice Note Templates',
  'New Note Template',
  'Archive',
  'New Invoice',
  'Recurring Profiles',
  'Drafts Invoices',
  'Unknown Client',
  'Grouped by Client',
  'Make a Payment',
  ':invoice_id.pdf',
  'Invoice items data is not valid. All descriptions are required and there need to be at least one unit with cost set per item!',
  'At least one invoice item is required',
  'One of invoice counters is required (:total, :year, :month)',
  'Number generator pattern is required',
  'Invoice Designer',
  'Your name is required',
  'Valid email address is required',
  'Your email address is required',
  'Your comment is required',
  ':quote_id.pdf',
  'New Quote',
  'Drafts quotes',
  'Sent quotes',
  'Won quotes',
  'Lost quotes',
  'All quotes',
  'Quotes items data is not valid. All descriptions are required and there need to be at least one unit with cost set per item!',
  'Quotes items data is not valid. All descriptions are required and there has to be at least one unit with cost set per item!',
  'Company Name is required',
  'Company with that name ":name" already exists',
  'Company Address is required',
  'User with email address ":email" already exists',
  'Role is required',
  'Role',
  'Client\'s Email is required',
  'Recurring profiles',
  'New Recurring Profile',
  'Tax Rates',
  'New Tax Rate',
  'Invoice \':num\' has been Canceled',
  'Invoice Canceled',
  'Invoice Draft has been Created',
  'Draft Invoice Created',
  'Invoice has been Created and Issued',
  'Invoice Issued',
  'Invoice \':name\' has been Issued',
  'Invoice Overdue Reminder',
  'Invoice Reminder',
  'Invoice Overdue',
  'Invoice Details',
  'Invoice \':num\' has been Paid',
  'Invoice Paid',
  'Recurring Profile has been Archived',
  'Recurring invoice profile "<a href=":url" style=":link_style">:name"</a> has been archived',
  'Profile Archived',
  'Quote \':name\' has been Sent',
  '<a href=":sent_by_url" style=":link_style">:sent_by_name</a> has just sent you a quote: <a href=":quote_url" style=":link_style">:quote_name</a>. You can <a href=":quote_url" style=":link_style">view the quote details</a> or <a href=":pdf_url" style=":link_style">download the PDF version here</a>.',
  'Quote Sent',
  'Quote \':name\' has been Updated',
  '<a href=":sent_by_url" style=":link_style">:sent_by_name</a> has just modified this quote: <a href=":quote_url" style=":link_style">:quote_name</a>. You can <a href=":quote_url" style=":link_style">view the quote details</a> or <a href=":pdf_url" style=":link_style">download the PDF version here</a>.',
  'Quote Updated',
  'Invoicing Settings',
  'Item Templates',
  'Note Templates',
  'Approval Requests',
  'Issued On',
  'Payment Due On',
  'Canceled On',
  'Paid On',
  'Issued',
  'Paid',
  'Canceled',
  'Based On',
  '(:date)',
  'Status',
  'Public URL',
  'Started On',
  'Starts On',
  'Frequency',
  'Occurrence',
  'Next Trigger On',
  'Auto issue',
  'Yes',
  'No',
  'Invoice due after',
  'Allow Payments',
  'Notified On',
  'Import quote discussion',
  'Import quote items as milestones',
  'Invoice',
  'Create Invoice',
  'Quote',
  'Create Quote',
  'Recurring Profile',
  'Create Recurring Profile',
  'Rebuild invoicing log entries',
  'Rebuild invoicing object contexts',
  ':count overdue invoices',
  ':count overdue invoice',
  ':count outstanding invoice',
  ':count outstanding invoices',
  'One overdue invoice for your company',
  ':count overdue invoices for your company',
  'Manage Finances',
  'This permissions enables users to view existing and create new invoices, manage payments, as well as to run all invoicing and payment related reports',
  'Manage Quotes',
  'This permissions enables users to manage quotes within Invoicing module',
  'Note',
  'Invoice #',
  'Created On',
  'Total',
  'Tax',
  'Total Cost',
  'Description',
  'Qty.',
  'Unit Cost',
  'Go to Invoice',
  'Pay Online Now',
  'Use Plain Counter Value',
  'Fix to :num Letters',
  'Due Upon Receipt',
  '10 Days After Issue (NET 10)',
  '15 Days After Issue (NET 15)',
  '30 Days After Issue (NET 30)',
  '60 Days After Issue (NET 60)',
  'Specify Due Date',
  'Select Custom Due Date',
  'Daily',
  'Weekly',
  'Biweekly',
  'Monthly',
  'Bimonthly',
  'Every 3 Months',
  'Every 6 Months',
  'Yearly',
  'Sum all time records as a single invoice item',
  'Sum time records grouped by job type',
  'Keep time records as separate invoice items',
  'Adds invoicing support to activeCollab',
  'Module will be deactivated. Invoices created using this module will be deleted',
  'Total time logged',
  'Total of :hours logged',
  'Other expenses',
  'If you add this payment you will run over maximum payment amount. No payment added',
  'Description is required',
  'Quantity is required',
  'Item description is required',
  'Note name is required',
  'Note content is required',
  ':invoice_show_as #:invoice_num',
  'Draft #:invoice_num',
  'Unknown Currency',
  'Preview PDF',
  'View PDF',
  'Resend Email',
  'Issue',
  'Cancel',
  'Are you sure that you want to cancel this invoice? All existing payments associated with this invoice will be deleted!',
  'Invoice has been successfully canceled',
  'Duplicate',
  'Items (:count)',
  'Edit',
  'Delete',
  'Are you sure that you want to delete this invoice?',
  'Invoice successfully deleted',
  'Client is required',
  'Client address is required',
  'Invoice No. needs to be unqiue',
  'Proforma Invoice',
  'Invoice :name',
  'Invoice Draft #:invoice_num',
  'Project: :project_name',
  'Issued On: :issued_date',
  'Payment Due On: :due_date',
  'Sent On: :sent_date',
  'Subtotal:',
  'Tax:',
  'Total:',
  'Page :page_no of :total_pages',
  'Owner company does not exists',
  'Background image should be PNG or JPEG image',
  'Logo should be PNG or JPEG image',
  ':name invoice canceled',
  '<a href=":url" title=":name">:name_short</a> invoice canceled',
  ':name invoice issued',
  '<a href=":url" title=":name">:name_short</a> invoice issued',
  'New payment has been made for :name invoice',
  'New payment has been made for <a href=":url" title=":name">:name_short</a> invoice',
  'New Payment',
  ':name invoice paid',
  '<a href=":url" title=":name">:name_short</a> invoice paid',
  'Send',
  'Resend Quote',
  'Quote has been sent',
  'Mark as Won',
  'Are you sure that you want to mark this request as won?',
  'Quote has been marked as won',
  'Mark as Lost',
  'Are you sure that you want to mark this request as lost?',
  'Quote has been marked as lost',
  'Convert to Project',
  'Are you sure that you want to delete this quote permanently?',
  'Quote has been successfully deleted',
  'Quote summary is required',
  'Lost',
  'Sent',
  'Won',
  'Recurring profile items data is not valid. All descriptions are required and there need to be at least one unit with cost set per item!',
  'Do not allow payments',
  'Allow full payments',
  'Allow partial payments',
  'Use system default',
  'Trigger',
  'Are you sure that you want to trigger this :object_name profile?',
  ':object_name profile has been successfully triggered',
  'Please enter recurring profile name.',
  'Occurrence has to be numeric value larget than 0',
  'Start date has to be valid date value.',
  'Tax Rate name needs to be unique',
  'Tax Rate name is required',
  'Percentage maximum value is 99.999',
  'Percentage minimum value is 0',
  'Percentage is required',
  'Currency',
  'Project',
  'Closed On',
  'Subtotal',
  'This invoice has no items',
  'Sent On',
  'Contact Person',
  'Add Client to People',
  'This quote has no items',
  'Modify Company Identity',
  'Company Name',
  'Company Details',
  'Current Logo',
  'New Company logo:',
  'Additional information you want to be displayed in invoice header (address, bank account number etc)',
  'Image is scaled for display purposes',
  'Modify',
  'Save Changes',
  'Upload a New logo',
  'Tips',
  'To select a invoice and load its details, please click on it in the list on the left',
  'It is possible to select multiple invoices at the same time. Just hold Ctrl key on your keyboard and click on all the invoices that you want to select',
  'Download PDF',
  'Invoice is overdue',
  'Download Invoice in PDF Format',
  'There are no invoices to display',
  ':name\'s Payments',
  'Amount',
  ':name has not made any payments',
  'Payments',
  'Details',
  'To select a quote and load its details, please click on it in the list on the left',
  'It is possible to select multiple quotes at the same time. Just hold Ctrl key on your keyboard and click on all quotes that you want to select',
  'All Quotes',
  'Name',
  'Download Quote in PDF Format',
  'There are no quotes for :company_name',
  'About Invoice Expenses',
  'Related Expenses',
  'All expenses related to this invoice will be automatically marked as "Pending Payment" when this invoice gets issued. When invoice is marked as paid, then all related expenses will be automatically marked as paid, too. When the invoice is canceled, all related records will be automatically reverted to their original, billable state and released',
  'On Releasing Expenses',
  'When records are released, relation between this invoice and them is removed, without any records being deleted. Instead, releated records will be reverted to their original, billable state and invoice will not change their status in the future',
  'About Quote Sending',
  'Email will be Sent',
  'Selected contact person will receive an e-mail with this quote attached in PDF format.',
  'Also, a public page will be generated where client can post comments in case that they do not have activeCollab user account.',
  'About Tax Rates',
  'System supports definition of unlimited number of tax rates, but only one tax rate can be used per invoice item. Each invoice item can have a different tax rate set or no tax applied at all. Tax rates that are already in used can\'t be deleted unless all invoices which use it are deleted',
  'Updating Tax Rate',
  'When you update a tax rate, change will be applied to all past and future invoices. If rate of any specific tax has been changed and you would like old invoices to preserve old rate, create a new tax rate instead of updating the old one',
  'About Invoice Time & Expenses',
  'Related Time & Expense Records',
  'All time & expense records related to this invoice will be automatically marked as "Pending Payment" when this invoice gets issued. When invoice is marked as paid, then all related time & expense records will be automatically marked as paid, too. When the invoice is canceled, all related records will be automatically reverted to their original, billable state, and released',
  'On Releasing Time & Expense Records',
  'When records are released, relation between this invoice and them is removed, without any records being deleted. Instead, releated records will be reverted to their original, billable state, and invoice will not change their status in the future',
  'About Project Creation',
  'Lorem Ipsum is simply dummy text',
  'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages',
  'Time & Expenses',
  'Time',
  'Billable',
  'Expenses',
  'Settings',
  'Keep in mind that <u>invoice will be generated in selected currency</u>, regardless of invoice items currency',
  'Client',
  'Notes & Comments',
  'When creating a new invoice',
  'Tax Rate',
  'Address',
  'Invoice Note',
  'Our Comment',
  'Item Description',
  'Unit Price',
  'Item Quantity',
  'New Predefined Invoice Item',
  'New Item',
  'Add Predefined Invoice Item',
  'Edit Item: :description',
  'View All',
  'Invoice Note Name',
  'Note Content',
  'HTML not supported! Line breaks are preserved.',
  'Add Invoicing Note',
  'New Invoicing Note',
  'Edit Note Template: :name',
  'List',
  'Invoice ID',
  'Quantity',
  'Auto-generate on issue',
  'Specify',
  'Generate',
  'Invoice note is included in the final invoice and visible to the client',
  'This comment is never displayed to the client or included in the final invoice',
  'Language',
  'Public Invoice Note',
  'Our Private Comment',
  'Add New Item',
  'Add From Template',
  'Only selected client\'s projects',
  'Total Invoices',
  'Total Paid',
  'There are no archived invoices in the database',
  'Statistics',
  'Update Invoice',
  'Update',
  ':name Expenses',
  'Date',
  'User',
  'Cost',
  'There is no expenses attached to this invoice',
  'Release Expenses',
  'Are you sure that you want to remove relation between this invoice and expenses listed above? Note that expenses will NOT be deleted!',
  'Change Status',
  'Active',
  'Completed',
  'Change Category',
  'Change Client',
  'Change Label',
  'Issue Invoice',
  'Notify Client',
  'Send email to client',
  'Don\'t send emails, but mark invoice as issued',
  'PDF version of the invoice will be attached to the email',
  'System is able to send a copy of issued invoice to the client, but to do that, there needs to be at least one user in the client company with one of the following roles',
  'Overdue',
  'System is able to a send copy of issued invoice to the client, but to do that, there needs to be at least one user in the client company with one of the following roles',
  'Close',
  'Active Invoices',
  ':name Time & Expenses',
  'Item',
  'Value',
  'There is no time attached to this invoice',
  'Release',
  'Are you sure that you want to remove relation between this invoice and time records listed above? Note that time records will NOT be deleted!',
  ':name\'s Paid / Canceled Invoices',
  'Paid / Canceled On',
  'There are no archived company invoices to show',
  'Invoicing Module',
  'Permissions',
  'Create and Manage Invoices?',
  'Show Invoice As',
  'Specify how invoices will be called in generated PDF files ("Tax Invoice" for example). To use default value, <u>leave these fields blank</u> (activeCollab will use "Invoice" and "Proforma Invoice", optionally translated using localization feature)',
  'Due After Issue',
  'Users are able to change due date when they issue an invoice. This is just the default, pre-selected value',
  'Number Generator',
  'Next Value',
  'Change',
  'Invoice number in total',
  'Invoice number in current year',
  'Invoice number in current month',
  'Current year in number format',
  'Current month in number format',
  'Current month in short text format',
  'Current month in long text format',
  'When counter value length is fixed, system will prefix current counter value with appropriate number of zeros',
  'Click to Change',
  'When creating a new invoice based on a project, or a task or a milestone etc',
  'Default Due Date',
  'Draft (Proforma) Invoices',
  'Generator Pattern',
  'Fix Counter Length',
  'Choose layout',
  'Text Style',
  'Layout',
  'Client Details',
  'Items',
  'Client details on the left, invoice details on the right',
  'Invoice details on the left, client details on the right',
  'Show table bottom and top border',
  'Show border between invoice items',
  'Footer layout',
  'Appearance',
  'Print',
  'Invoice number on the left, page number on the right',
  'Page number on the left, invoice number on the right',
  'Show footer border',
  'Upload New Company Logo',
  'Logo',
  'Logo on the left, company details on the right',
  'Company details on the left, logo on the right',
  'Company Address and Details',
  'Show header border',
  'Picture on the left is only the draft preview. Font will not be applied to it due to technical limitations.<br /><br /> Download the <a href=":url" target="_blank">sample invoice</a> to see the final invoice design.',
  'Paper Size:',
  'Background Image',
  'Background image needs to be in exact dimensions of <strong>210mm x 297mm 300dpi</strong> and in <strong>PNG</strong> format. If you upload image which is not in those dimensions, it will be stretched out to fit invoice. Bear in mind that it\'s desirable to optimize this image as that will affect PDF file size. If possible, avoid using transparency as it will greatly increase rendering time, and memory consumption.',
  'Background image needs to be in exact dimensions of <strong>8½ by 11 inches 300dpi</strong> and in <strong>PNG</strong> format. If you upload image which is not in those dimensions, it will be stretched out to fit invoice. Bear in mind that it\'s desirable to optimize this image as that will affect PDF file size. If possible, avoid using transparency as it will greatly increase rendering time, and memory consumption.',
  'To read more about this topic and find template you need, visit <a href=":documentation_link" target="_blank">this page</a> on activeCollab documentation website',
  'Remove Background Image',
  '\':name\' Quote',
  '<span class="label">Important</span>: Access to this quote is no longer available to clients',
  'This note will be visible to the client',
  'This comment will never be displayed to the client',
  'Summary',
  'Existing Client',
  'New Client',
  'Company Address',
  'Company',
  'Contact person',
  'Add Note to this Quote',
  'Contact Person\'s E-mail',
  'Quotes Archive',
  'There are no archived quotes in the database',
  'View Quote Details',
  'Convert Quote to Project',
  'Create Milestones based on quote items',
  'Edit Quote',
  'Do not notify the client about this modification',
  'Create Client',
  'Client Company Name',
  'Client Company Address',
  'Send Welcome e-mail to the client',
  'Client\'s Email',
  'Client\'s First Name',
  'Client\'s Last Name',
  'Send Quote',
  'Active Quotes',
  'Profile Name',
  'Notify person',
  'Start On',
  'Auto Issue',
  'Add Recurring Profile',
  'Duplicate Recurring Profile',
  'Update Recurring Profile',
  'Skipped to trigger on:',
  'Occurrence:',
  'To select a recurring profile and load its details, please click on it in the list on the left',
  'It is possible to select multiple recurring profiles at the same time. Just hold Ctrl key on your keyboard and click on all the recurring profiles that you want to select',
  'Resolve',
  'Approve',
  'Disapprove',
  'This is last occurrence of this recurring profile and it will be archived after this occurrence.',
  'Archive this recurring profile after this occurrence.',
  'Resolve Conflict',
  'Tax Percentage ',
  'New',
  'Add Tax Rate',
  'Update Currency',
  'List All',
  'Comment',
  'There are no Invoices',
  ':invoice_name',
  'Contact',
  'There are no Quotes',
  ':quote_name',
  'All Recurring Profiles',
  'There are no Recurring Profiles',
  ':recurring_profile_name',
  'This Recurring Profile has no items',
  'There are no Invoices that match this criteria',
  'Time and Expenses',
  'Invoice related items',
  'No description provided',
  'Unknown',
  'Quote Name',
  'There are no Quotes that match this criteria',
  'There are no Recurring Profiles that match this criteria',
); ?>