{form method="POST" action=$form_url id="invoice_body_form" enctype="multipart/form-data"}
  <div class="content_stack_wrapper">
  
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3>{lang}Layout{/lang}</h3>
      </div>
      <div class="content_stack_element_body">
      
        {wrap field="body_layout"}
          {label}Choose layout{/label}
	        {radio_field name="template[body_layout]" label="Client details on the left, invoice details on the right" value=0 checked=!$template_data.body_layout}<br/>
	        {radio_field name="template[body_layout]" label="Invoice details on the left, client details on the right" value=1 checked=$template_data.body_layout}
         {/wrap}
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3>{lang}Client Details{/lang}</h3>
      </div>
      <div class="content_stack_element_body">
        {wrap field="client_details_font"}
          {label}Text Style{/label}
          {select_font name="template[client_details_font]" value=$template_data.client_details_font}
          {color_field name="template[client_details_text_color]" value=$template_data.client_details_text_color class="inline_color_picker"}
        {/wrap}            
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3>{lang}Invoice Details{/lang}</h3>
      </div>
      <div class="content_stack_element_body">
        {wrap field="invoice_details_font"}
          {label}Text Style{/label}
          {select_font name="template[invoice_details_font]" value=$template_data.invoice_details_font}
          {color_field name="template[invoice_details_text_color]" value=$template_data.invoice_details_text_color class="inline_color_picker"}
        {/wrap}            
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3>{lang}Items{/lang}</h3>
      </div>
      <div class="content_stack_element_body">
        {wrap field="items_font"}
          {label}Text Style{/label}
          {select_font name="template[items_font]" value=$template_data.items_font}
          {color_field name="template[items_text_color]" value=$template_data.items_text_color class="inline_color_picker"}
        {/wrap}
        
        {wrap field="header_text_color"}
          {checkbox_field name="template[print_table_border]" label="Show table bottom and top border" value=1 checked=$template_data.print_table_border id="table_border_toggler"}&nbsp;
          {color_field name="template[table_border_color]" value=$template_data.table_border_color class="inline_color_picker table_border_property"}
        {/wrap}        
        
        {wrap field="header_text_color"}
          {checkbox_field name="template[print_items_border]" label="Show border between invoice items" value=1 checked=$template_data.print_items_border id="border_toggler"}&nbsp;
          {color_field name="template[items_border_color]" value=$template_data.items_border_color class="inline_color_picker border_property"}
        {/wrap}        
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3>{lang}Note{/lang}</h3>
      </div>
      <div class="content_stack_element_body">
        {wrap field="items_font"}
          {label}Text Style{/label}
          {select_font name="template[note_font]" value=$template_data.note_font}
          {color_field name="template[note_text_color]" value=$template_data.note_text_color class="inline_color_picker"}
        {/wrap}
      </div>
    </div>
    
  </div>

  {wrap_buttons}
    {submit}Save Changes{/submit}
  {/wrap_buttons}
{/form}

  <script type="text/javascript">
    var wrapper = $('#invoice_body_form');

    var border_toggler = wrapper.find('#border_toggler');
    var border_properties = wrapper.find('.border_property');
    var check_border_properties = function () {
      var is_checked = border_toggler.is(':checked');
      if (is_checked) {
        border_properties.show();
      } else {
        border_properties.hide();
      } // if      
    };    
    border_toggler.bind('click', check_border_properties);
    check_border_properties();

    var table_border_toggler = wrapper.find('#table_border_toggler');
    var table_border_properties = wrapper.find('.table_border_property');
    var check_table_border_properties = function () {
      var is_checked = table_border_toggler.is(':checked');
      if (is_checked) {
        table_border_properties.show();
      } else {
        table_border_properties.hide();
      } // if            
    };

    table_border_toggler.bind('click', check_table_border_properties);
    check_table_border_properties();
  </script>

