<ul class="category_list">
  <li {if $request->getController() == 'quotes' && $request->getAction() == 'index'}class="selected"{/if}><a href="{assemble route=quotes}"><span>{lang}Active Quotes{/lang}</span></a></li>
  <li {if ($request->getController() == 'quotes' && $request->getAction() == 'archive') || ($request->getController() == 'quotes_archive')}class="selected"{/if}><a href="{assemble route=quotes_archive}"><span>{lang}Archive{/lang}</span></a></li>
</ul>