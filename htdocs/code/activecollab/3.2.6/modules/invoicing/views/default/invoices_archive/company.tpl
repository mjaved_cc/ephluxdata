{title name=$company->getName()}:name's Paid / Canceled Invoices{/title}
{add_bread_crumb}Invoices{/add_bread_crumb}


{wrap_columns}
  {wrap_content_column}
    <div class="pagination_container top">
      <div class="pagination_toggle_group">
        <ul>
          <li class="first {if $status == 'paid'}active{/if}"><a href="{assemble route=company_invoices company_id=$company->getId()}">{lang}Paid{/lang}</a></li>
          <li class="last {if $status == 'canceled'}active{/if}"><a href="{assemble route=company_invoices company_id=$company->getId() status=canceled}">{lang}Canceled{/lang}</a></li>
        </ul>
      </div>
    </div>
     
    {if is_foreachable($invoices)}
      <div id="archived_company_invoices">
      {foreach from=$invoices item=invoices_group}
        {if is_foreachable($invoices_group.invoices)}
          <h2 class="new_section_name">{$invoices_group.currency->getName()}</h2>
          <div class="section_container">
            <table class="invoices">
              <tr>
                <th class="invoice_id">{lang}Invoice #{/lang}</th>
                <th class="comment">{lang}Our Comment{/lang}</th>
                <th class="paid_on">{lang}Paid / Canceled On{/lang}</th>
                <th class="total">{lang}Total{/lang}</th>
              </tr>
              {foreach from=$invoices_group.invoices item=invoice}
              <tr class="{cycle values='odd,even'}">
                <td class="invoice_id"><a href="{$invoice->getViewUrl()}">{$invoice->getName(true)}</a></td>
                <td class="comment">{$invoice->getComment()}</td>
                <td class="paid_on">{$invoice->getClosedOn()|date}</td>
                <td class="total">{$invoice->getTotal()|money} {$invoice->getCurrencyCode()}</td>
              </tr>
              {/foreach}
            </table>
          </div>
        {/if}
      {/foreach}
      </div>
    {else}
      <p class="empty_page"><span class="inner">{lang}There are no archived company invoices to show{/lang}</span></p>
    {/if}
  {/wrap_content_column}
  
  {wrap_sidebar_column}
    {include file=get_view_path('tabs', 'invoices', 'invoicing')}  
  {/wrap_sidebar_column}
{/wrap_columns}