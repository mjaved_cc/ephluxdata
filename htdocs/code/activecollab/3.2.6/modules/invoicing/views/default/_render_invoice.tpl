{object object=$active_invoice user=$logged_user}
  {assign_var name=invoice_class}
    {if $active_invoice->isDraft()}
      invoice_draft
    {elseif $active_invoice->isIssued()}
    	{if ActiveCollab::getBrandingRemoved()}
      	invoice_issued branding_removed
      {else}
      	invoice_issued
      {/if}
    {elseif $active_invoice->isPaid()}
      invoice_paid
    {elseif $active_invoice->isCanceled()}
      invoice_canceled
    {/if}
  {/assign_var}
  
  <div class="invoice_paper_wrapper {$invoice_class|trim}_wrapper invoice_{$active_invoice->getId()}">
    <div class="invoice_paper {$invoice_class|trim}">
      <div class="invoice_paper_below">
        <div class="invoice_paper_top"></div>
        <div class="invoice_paper_center"><div class="invoice_paper_area"></div></div>
        <div class="invoice_paper_bottom"></div>
      </div>

      <div class="invoice_paper_top"></div>
      <div class="invoice_paper_center">
        <div class="invoice_paper_area">
          <div class="invoice_paper_logo"></div>
          {if $logged_user->isFinancialManager()}
          <div class="invoice_comment property_wrapper" style="display: {if $active_invoice->getComment()}block{else}none{/if}"><span class="invoice_comment_paperclip"></span><span class="property_invoice_comment">{$active_invoice->getComment()}</span></div>
          {/if}
          <div class="invoice_paper_header">
            <div class="invoice_paper_details">
              <h2><span class="property_invoice_name">{$active_invoice->getName()}</span></h2>
              <ul>

                <li class="invoice_currency property_wrapper">{lang}Currency{/lang}: <strong><span class="property_invoice_currency">{$active_invoice->getCurrencyCode()}</span></strong></li>
                <li class="invoice_project property_wrapper" {if !($active_invoice->getProject() instanceof Project)} style="display: none" {/if}>{lang}Project{/lang}: <span class="property_invoice_project">{if ($active_invoice->getProject() instanceof Project)}{object_link object=$active_invoice->getProject()}{/if}</span></li>
                
                <li class="invoice_created_on property_wrapper">{lang}Created On{/lang}: <span class="property_invoice_created_on">{$active_invoice->getCreatedOn()|date:0}</span></li>
                <li class="invoice_issued_on property_wrapper">{lang}Issued On{/lang}: <span class="property_invoice_issued_on">{$active_invoice->getIssuedOn()|date:0}</span></li>
                <li class="invoice_due_on property_wrapper" {if !$active_invoice->getDueOn()} style="display: none" {/if}>{lang}Payment Due On{/lang}: <span class="property_invoice_due_on">{$active_invoice->getDueOn()|date:0}</span></li>
                <li class="invoice_paid_on property_wrapper">{lang}Paid On{/lang}: <span class="property_invoice_paid_on">{$active_invoice->getClosedOn()|date:0}</span></li>
                <li class="invoice_closed_on property_wrapper">{lang}Closed On{/lang}: <span class="property_invoice_closed_on">{$active_invoice->getClosedOn()|date:0}</span></li>
                
              </ul>
            </div>

            <div class="invoice_paper_client"><div class="invoice_paper_client_inner">
              <div class="invoice_paper_client_name property_wrapper"><span class="property_invoice_client_name">{company_link company=$active_invoice->getCompany()}</span></div>
              <div class="invoice_paper_client_address property_wrapper"><span class="property_invoice_client_address">{$active_invoice->getCompanyAddress()|clean|nl2br nofilter}</span></div>
            </div></div>
          </div>

          <div class="invoice_paper_items">
            {if is_foreachable($active_invoice->getItems())}
              {if $active_invoice->getTax() != 0}
                <!-- With tax values -->

                {assign var="common_tax_rate" value=$active_invoice->commonTaxRate()}

                {if !$common_tax_rate}
                  <!-- With different tax values -->
                  {assign var="tax_head_colspan" value="2"}
                  {assign var="totals_colspan" value="6"}
                {else}
                  <!-- With common tax values -->
                  {assign var="tax_head_colspan" value="1"}
                  {assign var="totals_colspan" value="5"}
                {/if}
              {else}
                <!-- Without tax values -->
                {assign var="totals_colspan" value="4"}
              {/if}
              <table cellspacing="0" >
                <thead>
                  <tr class='items_head'>
                    <td class="num"></th>
                    <td class="description">{lang}Description{/lang}</td>
                    <td class="quantity">{lang}Qty.{/lang}</td>
                    <td class="unit_cost">{lang}Unit Cost{/lang}</td>
                    {if $active_invoice->getTax() != 0}
                      <td class="tax_rate" colspan={$tax_head_colspan}>{$active_invoice->getTaxColumnText()}</td>
                    {/if}
                    <td class="total">{lang}Total{/lang}</td>
                  </tr>
                </thead>
                <tbody>
                {foreach from=$active_invoice->getItems() item=invoice_item}
                  <tr class="{cycle values='odd,even'}">
                    <td class="num">#{$invoice_item->getPosition()}</td>
                    <td class="description">{$invoice_item->getDescription()}</td>
                    <td class="quantity">{$invoice_item->getQuantity()|money}</td>
                    <td class="unit_cost">{$invoice_item->getUnitCost()|money}</td>
                    {if $active_invoice->getTax() != 0}
                      {if $common_tax_rate}
                        <td class="tax_rate">{$invoice_item->getTax()|money}</td>
                      {else}
                        <td class="tax_rate">{$invoice_item->getTaxRateName()} {$invoice_item->getTaxRatePercentageVerbose()}</td>
                        <td class="tax_rate tax_rate_money">{$invoice_item->getTax()|money}</td>
                      {/if}

                    {/if}
                    <td class="total">{$invoice_item->getTotal()|money}</td>
                  </tr>
                {/foreach}
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan={$totals_colspan} class="label">{lang}Subtotal{/lang}</td>
                    <td class="value"><span class="property_wrapper property_invoice_subtotal">{$active_invoice->getTotal()|money}</span></td>
                  </tr>
                  <tr>
                    <td colspan={$totals_colspan} class="label">{lang}Tax{/lang}</td>
                    <td class="value"><span class="property_wrapper property_invoice_tax">{$active_invoice->getTax()|money}</span></td>
                  </tr>
                  <tr class="total">
                    <td colspan={$totals_colspan} class="label">{lang}Total{/lang}</td>
                    <td class="value total"><span class="property_wrapper property_invoice_total">{$active_invoice->getTaxedTotal()|money}</span></td>
                  </tr>
                </tfoot>
              </table>
            {else}
              <p class="empty_page"><span class="inner">{lang}This invoice has no items{/lang}</span></p>
            {/if}
          </div>


          <div class="invoice_paper_notes property_wrapper" style="display: {if $active_invoice->getNote()}block{else}none{/if}">
            <h3>{lang}Note{/lang}</h3>
            <p><span class="property_invoice_note">{$active_invoice->getNote()|clean|nl2br nofilter}</span></p>
          </div>

        </div>
      </div>
      <div class="invoice_paper_bottom"></div>
      
      <div class="invoice_paper_peel_draft"></div>
      <div class="invoice_paper_stamp_paid"></div>
      <div class="invoice_paper_stamp_canceled"></div>
    </div>
  </div>
{/object}

<script type="text/javascript">
  $('.invoice_{$active_invoice->getId()}').each(function() {
    var wrapper = $(this);

    var wrapper_paper = wrapper.find('.invoice_paper:first');

  {if $active_invoice->isOverdue() && $active_invoice->isIssued()}
    wrapper.addClass('invoice_overdue_wrapper');
    wrapper_paper.addClass('invoice_overdue');
  {/if}

    App.Wireframe.Events.bind('resend_email.{$request->getEventScope()}', function(event, invoice) {
      if(invoice['id']) {
        App.Wireframe.Flash.success('Email notification has been sent');
        App.Wireframe.Events.trigger('invoice_updated',invoice);
      } //if
    });

    /**
     * Handle what happens when invoices get issued
     */
    App.Wireframe.Events.bind('invoice_updated.{$request->getEventScope()}', function(event, invoice) {
      $('#render_object_payments').paymentContainer('refresh',invoice);

      wrapper.removeClass('invoice_canceled_wrapper invoice_draft_wrapper invoice_issued_wrapper invoice_paid_wrapper invoice_overdue_wrapper');
      wrapper_paper.removeClass('invoice_canceled invoice_draft invoice_issued invoice_paid invoice_overdue');

      // update invoice properties
      wrapper.updateProperty([
        {
          'name' : 'invoice_name',
          'value' : invoice.name.clean(),
          'auto_hide' : false
        }, {
          'name': 'invoice_currency',
          'value': invoice.currency.code.clean(),
          'auto_hide': false
        }, {
          'name': 'invoice_project',
          'value': invoice.project ? '<a href="' + invoice.project.permalink + '">' + invoice.project.name.clean() + '</a>' : null
        }, {
          'name': 'invoice_created_on',
          'value' : invoice.created_on.formatted_date_gmt
        }, {
          'name': 'invoice_issued_on',
          'value' : typeof(invoice['issued_on']) == 'object' && invoice['issued_on'] ? invoice['issued_on']['formatted_date_gmt'] : null
        }, {
          'name': 'invoice_due_on',
          'value' : typeof(invoice['due_on']) == 'object' && invoice['due_on'] ? invoice['due_on']['formatted_date_gmt'] : null,
          'auto_hide' : true
        }, {
          'name': 'invoice_paid_on',
          'value' : typeof(invoice['paid_on']) == 'object' && invoice['paid_on'] ? invoice['paid_on']['formatted_date_gmt'] : null
        }, {
          'name': 'invoice_closed_on',
          'value' : typeof(invoice['closed_on']) == 'object' && invoice['closed_on'] ? invoice['closed_on']['formatted_date_gmt'] : null
        }, {
          'name': 'invoice_client_name',
          'value' : '<a href="' + invoice.client.permalink + '">' + invoice.client.name.clean() + '</a>'
        }, {
          'name': 'invoice_client_address',
          'value' : typeof(invoice['client']['address']) == 'string' ? invoice['client']['address'].clean().nl2br() : ''
        }, {
          'name': 'invoice_subtotal',
          'value' : invoice.subtotal.toFixed(2),
          'auto_hide' : false
        }, {
          'name': 'invoice_tax',
          'value' : invoice.tax.toFixed(2),
          'auto_hide' : false
        }, {
          'name': 'invoice_total',
          'value' : invoice.total.toFixed(2),
          'auto_hide' : false
        }, {
          'name': 'invoice_note',
          'value' : typeof(invoice['note']) == 'string' ? invoice['note'].clean().nl2br() : '',
          'auto_hide' : true
        }, {
          'name': 'invoice_comment',
          'value' : typeof(invoice['comment']) == 'string' ? invoice['comment'].clean() : '',
          'auto_hide' : true
        }
      ]);

      // update invoice items
      var items_parent = wrapper.find('.invoice_paper_items table tbody').empty();
      var new_items = '';

      var items_head = wrapper.find('tr.items_head');
      items_head.find('td.tax_rate').remove();

      if (invoice.tax != 0) {
        if(!invoice.common_tax) {
          //different tax rate
          var tax_head_colspan = 2;
          var totals_colspan = 6;
        } else {
          //common tax rate
          var tax_head_colspan = 1;
          var totals_colspan = 5;
        }//if
      } else {
        var totals_colspan = 4;
      }//if

      var tax_rate_head = '<td class="tax_rate" colspan=' + tax_head_colspan + '>' + invoice.tax_column_text + '</td>';
      if (invoice.tax != 0) {
        items_head.find('td.unit_cost').after(tax_rate_head);
      }//if

      //change colspan for label (subtotals...)
      wrapper.find('td.label').attr('colspan',totals_colspan);

      if (invoice.items && invoice.items.length) {
        $.each(invoice.items, function (item_index, item) {
          new_items += '<tr>' +
            '<td class="num">#' + item.position + '</td>' +
            '<td class="description">' + item.description.clean() + '</td>' +
            '<td class="quantity">' + item.quantity.toFixed(2) + '</td>' +
            '<td class="unit_cost">' + item.unit_cost.toFixed(2) + '</td>';

          if (invoice.tax != 0) {
            if(invoice.common_tax) {
              new_items +='<td class="tax_rate">' + item.tax.value.toFixed(2) + '</td>';
            } else {
              new_items += '<td class="tax_rate">' + item.tax.name + ' ' + item.tax.verbose_percentage + '</td>' +
                           '<td class="tax_rate tax_rate_money">' + item.tax.value.toFixed(2) + '</td>';
            } //if
          } //if

          new_items +=  '<td class="total">' + item.total.toFixed(2) + '</td>' +
                        '</tr>';
        });

        items_parent.html(new_items);
      } // if

      // DRAFT
      if (invoice.state.is_draft) {
        wrapper.addClass('invoice_draft_wrapper');
        wrapper_paper.addClass('invoice_draft');

      // ISSUED
      } else if (invoice.state.is_issued) {
        wrapper.addClass('invoice_issued_wrapper');
        wrapper_paper.addClass('invoice_issued');

        if (invoice.state.is_overdue) {
          wrapper.addClass('invoice_overdue_wrapper');
          wrapper_paper.addClass('invoice_overdue');
        } // if

        wrapper.updateProperty([{
          'name' : 'invoice_name',
          'value' : invoice.name.clean(),
          'auto_hide' : false
        }, {
          'name' : 'invoice_issued_on',
          'value' : invoice.issued_on.formatted_date_gmt
        }, {
          'name' : 'invoice_due_on',
          'value' : invoice.due_on.formatted_date_gmt,
          'auto_hide' : true
        }]);

      // Paid
      } else if (invoice.state.is_paid) {
        wrapper.addClass('invoice_paid_wrapper');
        wrapper_paper.addClass('invoice_paid');

      // CANCELED
      } else if (invoice.state.is_canceled) {
        wrapper.addClass('invoice_canceled_wrapper');
        wrapper_paper.addClass('invoice_canceled');

        wrapper.updateProperty([{
          'name' : 'invoice_name',
          'value' : invoice.name.clean(),
          'auto_hide' : false
        }, {
          'name' : 'invoice_closed_on',
          'value' : invoice.closed_on.formatted_date
        }]);
      } // if

    });
  });
</script>