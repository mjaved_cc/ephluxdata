<?php

  /**
   * Invoicing on_system_notifications handler
   *
   * @package activeCollab.modules.invoicing
   * @subpackage handlers
   */

  /**
   * Handle on_system_notifications event
   *
   * @param NamedList $items
   * @param IUser $user
   */
  function invoicing_handle_on_system_notifications(NamedList &$items, IUser &$user) {
    $company = $user->getCompany();
    
    // if user can manage invoices, list overdue invoices for all companies
    if ($user->isFinancialManager()) {
      // if it's administrator list only overdue invoices
      if (($admin_overdue_invoices = Invoices::countOverdue()) > 0) {
        $items->add('admin_overdue_invoices', array(
          'label' => $admin_overdue_invoices > 1 ? lang(':count overdue invoices', array('count' => $admin_overdue_invoices)) : lang(':count overdue invoice', array('count' => $admin_overdue_invoices)),
          'class' => 'adminoverdue_invoices',
          'icon' => AngieApplication::getImageUrl('icons/12x12/important.png', ENVIRONMENT_FRAMEWORK),
          'url' => Router::assemble('invoices'),
        ));
      } // if
    } // if
    
    // if user is company manager or can manage invoices show outstanding and overdue invoices for his company
    if (Invoices::canAccessCompanyInvoices($user, $company)) {

      $invoice_view_url = $company->isOwner() ? "getViewUrl" : "getCompanyViewUrl";
      $invoicing_url = $company->isOwner() ? Router::assemble('invoices') : Router::assemble('people_company_invoices', array('company_id' => $company->getId()));

      // ---------------------------------------------------
      //  Outstanding Invoices
      // ---------------------------------------------------
      
      $issued_invoices_count = Invoices::countOutstanding($company);
      if ($issued_invoices_count > 0) {
        if ($issued_invoices_count == 1) {
          $issued_invoice = Invoices::findOutstanding($company, array(INVOICE_STATUS_ISSUED));
          $label = lang(':count outstanding invoice', array('count' => 1));
        } else {
          $label = lang(':count outstanding invoices', array('count' => $issued_invoices_count));
        } // if

        $items->add('issued_invoices', array(
          'label' => $label,
          'class' => 'issued_invoices',
          'icon' => AngieApplication::getImageUrl('icons/12x12/issued-invoices.png', INVOICING_MODULE),
          'url' => $issued_invoices_count == 1 ? $issued_invoice[0]->$invoice_view_url() : $invoicing_url
        ));
      } // if
      
      // ---------------------------------------------------
      //  Overdue Invoices
      // ---------------------------------------------------
      
      $overdue_invoices_count = Invoices::countOverdue($company);
      if ($overdue_invoices_count > 0) {
        if ($overdue_invoices_count == 1) {
          $overdue_invoice = Invoices::findOverdue($company, array(INVOICE_STATUS_ISSUED));
          $label = lang('One overdue invoice for your company');
        } else {
          $label = lang(':count overdue invoices for your company', array('count' => $overdue_invoices_count));
        } // if
        
        $items->add('overdue_invoices', array(
          'label' => $label,
          'class' => 'overdue_invoices',
          'icon' => AngieApplication::getImageUrl('icons/12x12/important.png', ENVIRONMENT_FRAMEWORK),
          'url' => $overdue_invoices_count == 1 ? $overdue_invoice[0]->$invoice_view_url() : $invoicing_url,
        ));
      } // if
      
     
    } // if
  } // invoicing_handle_on_system_notifications