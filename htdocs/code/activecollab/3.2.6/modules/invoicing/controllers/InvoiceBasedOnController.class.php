<?php

  /**
   * Invoice based on controller
   *
   * @package activeCollab.modules.invoicing
   * @subpackage controllers
   */
  class InvoiceBasedOnController extends Controller {
    
    /**
     * Selected object
     *
     * @var Object
     */
    protected $active_object;
    
    /**
     * Active invoice object
     * 
     * @var Invoice
     */
    protected $active_invoice;
    
    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();
      
      $this->response->assign(array(
        'active_object' => $this->active_object,
        'is_based_on_quote' => $this->active_object instanceof Quote,
        'is_based_on_tracking_report' => $this->active_object instanceof TrackingReport,
        'on_invoice_based_on' => ConfigOptions::getValue('on_invoice_based_on'),
      	'invoice_notes' => InvoiceNoteTemplates::find(),
        'js_company_details_url' => Router::assemble('people_company_details'),
      ));
    } // __before
    
    /**
     * Create invoice based on active_object
     */
    function add_invoice() {
      if($this->active_object instanceof Quote) {
         $this->smarty->assign(array(
           'note' => $this->active_object->getNote()
         )); 
      } // if

      if($this->active_object instanceof TrackingReport) {
        $filter_data = $this->request->get('filter') ? $this->request->get('filter') : unserialize($this->request->post('filter_data'));
        
        $this->active_object->setAttributes($filter_data);
        $company_id = $this->active_object->getCompany($this->logged_user)->getId();
        
        $this->response->assign(array(
        	'filter_data' =>  serialize($filter_data)
        ));
      } else {
        $company_id = $this->active_object->getCompany() instanceof Company ? $this->active_object->getCompany()->getId() : null;
      } // if

      $project = null;
      if ($this->active_object instanceof Quote) {
        $project = $this->active_object->getProject();
      } // if
      
      $this->smarty->assign(array(
        'company_id' => $company_id,
        'project' => $project
      )); 
      
      if($this->request->isSubmitted()) {
        if($this->active_object->invoice()->canAdd($this->logged_user)) {
          try {
            $invoice_data = $this->request->post('invoice_data');

            if (!isset($invoice_data['currency_id'])) {
              $default_currency = Currencies::getDefault();
              $invoice_data['currency_id'] = $default_currency->getId();
            } // if

            $allow_payments = (boolean) ConfigOptions::getValue('allow_payments');
            if ($allow_payments && !isset($invoice_data['allow_payments'])) {
              $invoice_data['allow_payments'] = -1;
            } // if
            
            $invoice = $this->active_object->invoice()->create($invoice_data, $this->logged_user);
            
            $this->response->respondWithData($invoice, array(
  	          'as' => 'invoice', 
  	        ));  
          } catch(Exception $e) {
            $this->response->exception($e);
          } // try
        } else {
          $this->response->badRequest();
        } // if
      } // if
    } // add_invoice
     
  }