<?php

  /**
   * Subtask class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  abstract class Subtask extends FwSubtask implements ISchedule {
    
    /**
     * Schedule helper
     * 
     * @var IScheduleImplementation
     */
    private $schedule = false;
    
    /**
     * Return schedule helper instance
     * 
     * @return IScheduleImplementation
     */
    function schedule() {
    	if ($this->schedule === false) {
    		$this->schedule = new IScheduleImplementation($this);	
    	} // if
    	
    	return $this->schedule;
    } // schedule
    
  }