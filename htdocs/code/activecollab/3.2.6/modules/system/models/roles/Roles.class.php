<?php

  /**
   * Roles manager class
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class Roles extends FwRoles {
    
    /**
     * Return roles that have manage company details permissions
     * 
     * @return array
     */
    static function findCompanyManagerRoles() {
      return Roles::findByPermissions(array(
        'has_system_access', 'can_manage_company_details', 
      ));
    } // findCompanyManagerRoles
    
  }