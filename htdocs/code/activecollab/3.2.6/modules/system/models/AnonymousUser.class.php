<?php

  /**
   * Anonymous user class
   *
   * @package activeCollab.modules.system
   * @subpackage models
   */
  class AnonymousUser extends FwAnonymousUser implements IAvatar {
  	
    /**
     * UserAvatar implementation instance for this object
     *
     * @var IUserAvatarImplementation
     */
  	private $avatar;
    
    /**
     * Return subtasks implementation for this object
     *
     * @return IUserAvatarImplementation
     */
    function avatar() {
      if(empty($this->avatar)) {
        $this->avatar = new IUserAvatarImplementation($this);
      } // if
      
      return $this->avatar;
    } // avatar
    
    /**
     * Return array or property => value pairs that describes this object
     *
     * $user is an instance of user who requested description - it's used to get
     * only the data this user can see
     *
     * @param IUser $user
     * @param boolean $detailed
     * @param boolean $for_interface
     * @return array
     */
    function describe(IUser $user, $detailed = false, $for_interface = false) {
      $result = parent::describe($user, $detailed, $for_interface);
      
      $this->avatar()->describe($user, $detailed, $for_interface, $result);
      
      return $result;
    } // describe

    /**
     * Return array or property => value pairs that describes this object
     *
     * @param IUser $user
     * @param boolean $detailed
     * @return array
     */
    function describeForApi(IUser $user, $detailed = false) {
      $result = parent::describeForApi($user, $detailed);

      $this->avatar()->describeForApi($user, $detailed, $result);

      return $result;
    } // describeForApi
    
    /**
     * Return user avatar URL
     *
     * @param boolean $large
     * @return string
     */
    function getAvatarUrl($large = false) {
      if($large) {
        return ROOT_URL . '/avatars/default.40x40.gif';
      } else {
        return ROOT_URL . '/avatars/default.16x16.gif';
      } // if
    } // getAvatarUrl
    
    /**
     * Notifier helper instance
     * 
     * @var IActiveCollabNotifierImplementation
     */
    private $notifier = false;
    
    /**
     * Return notifier helper
     * 
     * @return IActiveCollabNotifierImplementation
     */
    function notifier() {
    	if($this->notifier === false) {
    		$this->notifier = new IActiveCollabNotifierImplementation($this);
    	} // if
    	
    	return $this->notifier;
    } // notifier
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if this user is member of owner company
     *
     * @return boolean
     */
    function isOwner() {
      return false;
    } // isOwner
    
    /**
     * Returns true if this user has management permissions in People section
     *
     * @return boolean
     */
    function isPeopleManager() {
      return false;
    } // isPeopleManager
    
    /**
     * Returns true if this user has global project management permissions
     *
     * @return boolean
     */
    function isProjectManager() {
      return false;
    } // isProjectManager
    
  }