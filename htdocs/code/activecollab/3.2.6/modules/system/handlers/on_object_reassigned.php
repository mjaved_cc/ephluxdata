<?php

  /**
   * on_object_reassigned event handler implementation
   *
   * @package activeCollab.modules.system
   * @subpackage handlers
   */

  /**
   * Handle on_project_object_reassigned event
   *
   * @param IAssignees $object
   * @param integer $old_assignee_id
   * @param integer $new_assignee_id
   * @param array $old_other_assignees
   * @param array $new_other_assignees
   */
  function resources_handle_on_object_reassigned(IAssignees &$object, $old_assignee_id, $new_assignee_id, $old_other_assignees, $new_other_assignees) {
    return;
    
//    if($object instanceof ProjectObject) {
//    	if(is_array($old_assignment_data)) {
//    	  list($old_assignees, $old_owner_id) = $old_assignment_data;
//    	} else {
//    	  $old_assignees = array();
//    	  $old_owner_id = 0;
//    	} // if
//    	
//    	if(is_array($new_assignment_data)) {
//    	  list($new_assignees, $new_owner_id) = $new_assignment_data;
//    	} else {
//    	  $new_assignees = array();
//    	  $new_owner_id = 0;
//    	} // if
//    	
//    	// ---------------------------------------------------
//    	//  Collect user data
//    	// ---------------------------------------------------
//    	
//    	$all_user_ids = array();
//    	foreach($old_assignees as $assignee_id) {
//    	  if(!in_array($assignee_id, $all_user_ids)) {
//    	    $all_user_ids[] = $assignee_id;
//    	  } // foreach
//    	} // if
//    	
//    	foreach($new_assignees as $assignee_id) {
//    	  if(!in_array($assignee_id, $all_user_ids)) {
//    	    $all_user_ids[] = $assignee_id;
//    	  } // foreach
//    	} // if
//    	
//    	if(is_foreachable($all_user_ids)) {
//    	  $all_users = Users::findByIds($all_user_ids);
//    	} else {
//    	  return;
//    	} // if
//    	
//    	$user_map = array();
//    	foreach($all_users as $user) {
//    	  $user_map[$user->getId()] = $user->getDisplayName();
//    	} // if
//    	
//    	// ---------------------------------------------------
//    	//  Prepare changes array
//    	// ---------------------------------------------------
//    	
//    	$changes = array();
//    	
//    	// Nobody assigned
//    	if($new_owner_id == 0) {
//    	  $changes[] = array('text' => 'Anyone can pick and complete this task');
//    	  
//    	  if($old_owner_id && isset($user_map[$old_owner_id])) {
//    	    $changes[] = array('text' => ':name is no longer responsible for this task', 'name' => $user_map[$old_owner_id]);
//    	  } // if
//    	  
//    	  foreach($old_assignees as $assignee_id) {
//    	    if(isset($user_map[$assignee_id])) {
//    	      $changes[] = array('text' => ':name has been removed from this task', 'name' => $user_map[$assignee_id]);
//    	    } // if
//    	  } // foreach
//    	  
//    	// We have new assignees
//    	} else {
//    	  if($old_owner_id != $new_owner_id) {
//    	    if(isset($user_map[$new_owner_id])) {
//    	      $changes[] = array('text' => ':name is responsible for this task', 'name' => $user_map[$new_owner_id]);
//    	    } // if
//    	    
//    	    if($old_owner_id && isset($user_map[$old_owner_id])) {
//    	      $changes[] = array('text' => ':name is no longer responsible for this task', 'name' => $user_map[$old_owner_id]);
//    	    } // if
//    	  } // if
//    	  
//    	  foreach($new_assignees as $assignee_id) {
//    	    if(isset($user_map[$assignee_id]) && !in_array($assignee_id, $old_assignees)) {
//    	      $changes[] = array('text' => ':name has been added to this task', 'name' => $user_map[$assignee_id]);
//    	    } // if
//    	  } // foreach
//    	  
//    	  foreach($old_assignees as $assignee_id) {
//    	    if(isset($user_map[$assignee_id]) && !in_array($assignee_id, $new_assignees)) {
//    	      $changes[] = array('text' => ':name has been removed from this task', 'name' => $user_map[$assignee_id]);
//    	    } // if
//    	  } // foreach
//    	} // if
//    	
//    	if(is_foreachable($changes) && is_foreachable($all_users)) {
//    	  $updated_by = $object->getUpdatedBy();
//    	  
//        if(is_foreachable($all_users)) {
//        	$updated_by->notifier()->notifyUsers($all_user_ids, $object, 'assignees/reassigned', array(
//        	  'notification_object_updated_by' => $updated_by,
//            'notification_assignment_changes' => $changes,
//            'notification_object' => $object,
//            'notification_parent' => $object->getParent(),
//            'notification_project' => $object->getProject(),
//        	));
//        } // if
//    	} // if
//    } // if
  } // resources_handle_on_object_reassigned