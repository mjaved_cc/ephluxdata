<?php

  /**
   * System on_system_permissions handler
   *
   * @package activeCollab.modules.system
   * @subpackage handlers
   */
  
  /**
   * Handle on_system_permissions
   *
   * @param NamedList $permissions
   */
  function system_handle_on_system_permissions(NamedList &$permissions) {
    $permissions->add('can_manage_projects', array(
      'name' => lang('Project Management'), 
      'description' => lang("Project managers are people who can do anything with all projects. They can see and access any project and they have all the permissions in them. Project managers can create projects even if 'add_project' permission is set to No!"),
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_manage_project_requests', array(
      'name' => lang('Manage Project Requests'), 
      'description' => lang('Set whether user can or cannot access Project Requests section and manage project requests submitted by clients. Note that project requests are disabled by default, and that you can enable them using Administration > Project Requests tool'), 
      'depends_on' => 'can_manage_projects', 
    ));
    
    $permissions->add('can_add_project', array(
      'name' => lang('Create New Projects'), 
      'description' => lang("Set whether user can create new projects or not. When creating a project members of client companies will see ONLY people and companies they have previously worked with. They will not see everyone! Administrators and project managers can create new project no matter which value this permission has!"),
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_see_project_budgets', array(
      'name' => lang('See Project Budgets'), 
      'description' => lang("Set whether user can see budget details for projects. Budget details are great for managers and (sometimes) clients, but you usually don't want to have this information available to all employees and sub-contractors"),
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_manage_people', array(
      'name' => lang('People Management'), 
      'description' => lang("People managers have all permissions regarding account management in People section. They can create new and manage all existing users and companies without any restriction"), 
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_manage_company_details', array(
      'name' => lang('Manage Company Details'), 
      'description' => lang("Set to Yes if you want to allow client company employee to manage company details. This user will be able to change its own company information, add new people to that company, and receive and access Invoices issued to that company"), 
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_see_contact_details', array(
      'name' => lang('See Contact Details'), 
      'description' => lang("People with this permission set to Yes in their system role can see contact details of other users in the system"), 
      'depends_on' => 'has_system_access', 
    ));

    $permissions->add('can_see_company_notes', array(
      'name' => lang('See Company Notes'),
      'description' => lang("People with this permission set to Yes in their system role can see notes of companies defined in People section"),
      'depends_on' => 'has_system_access',
    ));
    
    $permissions->add('can_have_homescreen', array(
      'name' => lang('Configure Own Home Screen'), 
      'description' => lang('Allow users with this role to configure their own home screen. When this permission is set to No, system will not allow users to configure their own home screen and it will use home screen configured for this role (or global home screen if role does not have home screen configured)'), 
      'depends_on' => 'has_system_access', 
    ));
    
    $permissions->add('can_manage_assignment_filters', array(
      'name' => lang('Manage Assignment Filters'), 
      'description' => lang("Set to Yes for roles you want to be able to create new and manage existing assignment filters. If set to No users will be able just to use existing filters, but not to create new or change setting of existing ones"),
      'depends_on' => 'has_system_access', 
    ));
  } // system_handle_on_system_permissions