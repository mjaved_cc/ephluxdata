<?php

  /**
   * select_project_template helper implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

  /**
   * Render select project template widget
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_project_template($params, &$smarty) {
    $value = array_var($params, 'value', null, true);
    
    $category_id = (integer) ConfigOptions::getValue('project_templates_category');
    
    if($category_id) {
      $additional_conditions = DB::prepare(TABLE_PREFIX . 'projects.category_id = ?', array($category_id));
    } else {
      $additional_conditions = null;
    } // if
    
    if($additional_conditions) {
      $additional_conditions .= ' AND ' . DB::prepare(TABLE_PREFIX . 'projects.state > ?', array(STATE_TRASHED));
    } else {
      $additional_conditions = DB::prepare(TABLE_PREFIX . 'projects.state > ?', array(STATE_TRASHED));
    } // if
    
    return HTML::optionalSelectFromPossibilities(@$params['name'], Projects::getIdNameMap(Authentication::getLoggedUser(), STATE_VISIBLE, null, $additional_conditions, true), $value, $params, lang('-- Create a Blank Project --'));
  } // smarty_function_select_project_template