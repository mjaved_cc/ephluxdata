<?php

  /**
   * select_job_type helper implementation
   * 
   * @package activeCollab.modules.tracking
   * @subpackage helpers
   */

  /**
   * Render select job type box
   * 
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_job_type($params, &$smarty) {
    if(AngieApplication::isModuleLoaded('tracking')) {
      $name = array_required_var($params, 'name', true);
      $value = array_var($params, 'value', null, true);
      
      if(empty($value)) {
        $value = JobTypes::getDefaultJobTypeId();
      } // if
      
      if(isset($params['class'])) {
        $params['class'] .= ' select_job_type';
      } else {
        $params['class'] = 'select_job_type';
      } // if
      
      if(array_var($params, 'short', false, true)) {
        $params['class'] .= ' short';
      } // if
      
      return HTML::selectFromPossibilities($name, JobTypes::getIdNameMap(), $value, $params);
    } // if
  } // smarty_function_select_job_type