<div class="project_list">
{if is_foreachable($_invoices)}
    <div class="project_overview_box">
      <div class="project_overview_box_title">
        <h2>{lang}Invoices{/lang}</h2>
      </div>
      <div class="project_overview_box_content"><div class="project_overview_box_content_inner">
        <table class="common" cellspacing="0">
          <thead>
            <tr>
              <th class="invoice">{lang}Invoice{/lang} #</th>
              <th class="project">{lang}Project{/lang}</th>
              <th class="status">{lang}Status{/lang}</th>
              <th class="due">{lang}Payment Due On{/lang}</th>
            </tr>
          </thead>
          <tbody>
          {foreach from=$_invoices item=object}
            <tr>
              <td class="invoice_name">{$object->getName()}</td>
              <td class="project">
              {if $object->getProject()} 
              	{$object->getProject()->getName()}
              {else}
              	--
              {/if}
              </td>
              <td class="status">{$object->getVerboseStatus()}</td>
              <td class="due">{$object->getDueOn()|date}</td>
            </tr>
          {/foreach}
          </tbody>
        </table>
      </div></div>
    </div>
  {/if}  
</div>