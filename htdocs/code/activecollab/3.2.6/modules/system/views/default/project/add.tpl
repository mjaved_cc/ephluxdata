{title}New Project{/title}
{add_bread_crumb}New project{/add_bread_crumb}

<div id="add_project_form">
  {form action=Router::assemble('projects_add') method=post class=big_form}
    {include file=get_view_path('_project_form', 'project', 'system')}
    
    {wrap_buttons}
      {submit}Create Project{/submit}
    {/wrap_buttons}
  {/form}
</div>

<script type="text/javascript">
  $('#add_project_form').each(function() {
    var wrapper = $(this);
    
    var template_picker = wrapper.find('div.select_project_template select');
    var first_milestone_starts_on_wrapper = wrapper.find('div.select_first_milestone_starts_on');

    var selected_template_id = template_picker.val() == '' ? 0 : parseInt(template_picker.val());

    if(selected_template_id < 1) {
      first_milestone_starts_on_wrapper.hide();
    } // if

    template_picker.change(function() {
      var selected_template_id = template_picker.val() == '' ? 0 : parseInt(template_picker.val());
      
      if(selected_template_id > 0) {
        first_milestone_starts_on_wrapper.slideDown('fast');
      } else {
        first_milestone_starts_on_wrapper.slideUp('fast');
      } // if
    });
  });
</script>