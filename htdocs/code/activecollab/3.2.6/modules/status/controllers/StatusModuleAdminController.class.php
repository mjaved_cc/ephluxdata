<?php

  // Extend modules admin
  AngieApplication::useController('modules_admin', SYSTEM_MODULE);

  /**
   * Status module admin controller
   *
   * @package activeCollab.modules.status
   * @subpackage controllers
   */
  class StatusModuleAdminController extends ModulesAdminController {
    
    /**
     * Show module details page
     *
     * @param void
     * @return null
     */
    function module() {
      $this->smarty->assign('roles', Roles::find());
    } // module
    
  }