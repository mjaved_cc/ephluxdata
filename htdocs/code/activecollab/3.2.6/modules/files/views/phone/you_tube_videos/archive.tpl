{title}Archived YouTube Videos{/title}
{add_bread_crumb}Archived YouTube Videos{/add_bread_crumb}

<div id="assets_text_documents">
	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
		<li data-role="list-divider"><img src="{image_url name="icons/listviews/navigate-icon.png" module=$smarty.const.SYSTEM_MODULE interface=AngieApplication::INTERFACE_PHONE}" class="divider_icon" alt="">{lang}Navigate{/lang}</li>
		{if is_foreachable($you_tube_videos)}
	    {foreach $you_tube_videos as $you_tube_video}
	    	<li><a href="{$you_tube_video->getViewUrl()}">{$you_tube_video->getName()}</a></li>
	    {/foreach}
		{else}
			<li>{lang}No archived YouTube Videos{/lang}</li>
		{/if}
	</ul>
</div>