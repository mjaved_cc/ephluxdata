<script type="text/javascript">
  App.widgets.FlyoutDialog.front().setAutoSize(false);
</script>

<div class="big_form_wrapper two_form_sidebars">
  <div class="main_form_column">
    {wrap field=url}
      {text_field name="youtube_video[video_url]" value=$youtube_data.video_url id=youtube_asset_url class='title required validate_minlength 3' required=true label="YouTube URL"}
    {/wrap}
    
    {wrap field=name}
      {text_field name="youtube_video[name]" value=$youtube_data.name id=youtube_asset_title class='title required validate_minlength 3' label="Video Title" maxlength="150" required=true}
    {/wrap}
    
    {wrap_editor field=body}
      {label}Description{/label}
      {editor_field name="youtube_video[body]" id=textDocumentBody inline_attachments=$youtube_data.inline_attachments}{$youtube_data.body nofilter}{/editor_field}
    {/wrap_editor}
  </div>
  
  <div class="form_sidebar form_first_sidebar">
    {wrap field=parent_id}
      {label for=textDocumentCategory}Category{/label}
      {select_asset_category name="youtube_video[category_id]" value=$youtube_data.category_id id=textDocumentCategory parent=$active_project user=$logged_user success_event="category_created"}
    {/wrap}
    
    {if $logged_user->canSeeMilestones($active_project)}
      {wrap field=milestone_id}
        {label for=textDocumentMilestone}Milestone{/label}
        {select_milestone name="youtube_video[milestone_id]" value=$youtube_data.milestone_id project=$active_project id=textDocumentMilestone user=$logged_user}
      {/wrap}
    {/if}

    {if $logged_user->canSeePrivate()}
      {wrap field=visibility}
        {label for=textDocumentVisibility}Visibility{/label}
        {select_visibility name="youtube_video[visibility]" value=$youtube_data.visibility short_description=true}
      {/wrap}
    {else}
      <input type="hidden" name="youtube_video[visibility]" value="1" />
    {/if}
  </div>
  
  <div class="form_sidebar form_second_sidebar">
  {if $active_asset->isNew()}
    {wrap field=notify_users}
      {select_subscribers name="notify_users" exclude=$youtube_data.exclude_ids object=$active_asset user=$logged_user label='Notify People'}
    {/wrap}
  {/if}
  </div>
</div>