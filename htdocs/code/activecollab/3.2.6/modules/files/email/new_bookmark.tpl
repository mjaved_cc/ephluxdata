[{$context->getProject()->getName()}] {lang name=$context->getName() language=$language}Bookmark ':name' has been Added{/lang}
================================================================================
{notification_wrapper title='Bookmark Added' context=$context context_view_url=$context_view_url recipient=$recipient sender=$sender}
  <p>{lang author_name=$context->getCreatedBy()->getDisplayName() url=$context_view_url name=$context->getName() link_style=$style.link language=$language}:author_name has just added "<a href=":url" style=":link_style">:name</a>" bookmark{/lang}.</p>

  <div style="padding: 16px; text-align: center">
    <img src="{$context->preview()->getLargeIconUrl()}">
    <p style="font-style: italic"><a href="{$context->getBookmarkUrl()}" style="{$style.link}" title="{lang}Click to Open{/lang}">{$context->getName()}</a></p>
  </div>

  {notification_wrap_body}{$context->getBody() nofilter}{/notification_wrap_body}
{/notification_wrapper}