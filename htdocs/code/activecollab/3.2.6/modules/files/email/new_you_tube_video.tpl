[{$context->getProject()->getName()}] {lang name=$context->getName() language=$language}YouTube Video ':name' has been Added{/lang}
================================================================================
{notification_wrapper title='YouTube Video Added' context=$context context_view_url=$context_view_url recipient=$recipient sender=$sender}
  <p>{lang author_name=$context->getCreatedBy()->getDisplayName() url=$context_view_url name=$context->getName() link_style=$style.link language=$language}:author_name has just added "<a href=":url" style=":link_style">:name</a>" YouTube video{/lang}.</p>

  <div style="padding: 10px; text-align: center">
    <img src="{$context->preview()->getLargeIconUrl()}">
    <p style="font-style: italic"><a href="{$context->getVideoUrl()}">{$context->getName()}</a></p>
  </div>

  {notification_wrap_body}{$context->getBody() nofilter}{/notification_wrap_body}
{/notification_wrapper}