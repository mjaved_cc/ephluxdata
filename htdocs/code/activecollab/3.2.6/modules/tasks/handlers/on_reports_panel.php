<?php

  /**
   * on_reports_panel event handler
   * 
   * @package activeCollab.modules.tasks
   * @subpackage handlers
   */

  /**
   * Handle on_reports_panel event
   * 
   * @param ReportsPanel $panel
   * @param User $user
   */
  function tasks_handle_on_reports_panel(ReportsPanel &$panel, User &$user) {
    if($user->canUseReports()) {
      $panel->addTo('assignments', 'aggregated_tasks', lang('Aggregate Tasks'), Router::assemble('project_tasks_aggregated_report'), AngieApplication::getImageUrl('reports/assignments.png', SYSTEM_MODULE, AngieApplication::INTERFACE_DEFAULT));
    } // if
  } // tracking_handle_on_reports_panel