<?php

  /**
   * on_build_names_search_index_for_project event handler implementation
   * 
   * @package activeCollab.modules.tasks
   * @subpackage handlers
   */

  /**
   * Handle on_build_names_search_index_for_project event
   * 
   * @param NamesSearchIndex $search_index
   * @param Project $project
   */
  function tasks_handle_on_build_names_search_index_for_project(NamesSearchIndex &$search_index, Project &$project) {
    $tasks = DB::execute("SELECT id, name, visibility, integer_field_1 FROM " . TABLE_PREFIX . "project_objects WHERE type = 'Task' AND project_id = ? AND state >= ?", $project->getId(), STATE_VISIBLE);
    
    if($tasks) {
      $project_id = $project->getId();
      
      foreach($tasks as $task) {
        $visibility = $task['visibility'] == VISIBILITY_PRIVATE ? 'private' : 'normal';
        
        Search::set($search_index, array(
          'class' => 'Task', 
          'id' => $task['id'], 
        	'context' => "projects:projects/$project_id/tasks/$visibility/$task[id]", 
          'name' => $task['name'], 
          'short_name' => '#' . $task['integer_field_1'], 
          'visibility' => $task['visibility'], 
        ));
      } // foreach
    } // if
  } // tasks_handle_on_build_names_search_index_for_project