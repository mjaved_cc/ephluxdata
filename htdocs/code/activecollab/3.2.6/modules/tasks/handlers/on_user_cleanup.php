<?php

  /**
   * Tasks on_user_cleanup event handler
   *
   * @package activeCollab.modules.tasks
   * @subpackage handlers
   */

  /**
   * Handle on_user_cleanup event
   *
   * @param array $cleanup
   */
  function tasks_handle_on_user_cleanup(&$cleanup) {
    if(!isset($cleanup['task_changes'])) {
      $cleanup['task_changes'] = array();
    } // if
    
    $cleanup['task_changes'][] = 'created_by';
  } // tasks_handle_on_user_cleanup

?>