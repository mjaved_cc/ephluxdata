{title}New Task{/title}
{add_bread_crumb}New{/add_bread_crumb}

<div id="new_task">
  {form action=$add_task_url}
    {include file=get_view_path('_task_form', 'tasks', $smarty.const.TASKS_MODULE)}
    
    {wrap_buttons}
      {submit}Add Task{/submit}
    {/wrap_buttons}
  {/form}
</div>