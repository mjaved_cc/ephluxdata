<?php

  /**
   * Objects label helper implementation
   * 
   * @package angie.frameworks.labels
   * @subpackage helpers
   */

  /**
   * Render object's label
   * 
   * Parameters:
   * 
   * - object - Parent object instance
   *
   * @param array $params
   * @param Smarty $smarty
   */
  function smarty_function_object_label($params, &$smarty) {
    $object = array_required_var($params, 'object', false, 'ILabel');
    
    if($object->label()->get() instanceof Label) {
      return $object->label()->get()->render();
    } else {
      return '';
    } // if
  } // smarty_function_object_label