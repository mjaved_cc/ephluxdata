<?php

  /**
   * Home screen widget that displays reminders
   *
   * @package angie.frameworks.reminders
   * @subpackage models
   */
  class RemindersHomescreenWidget extends HomescreenWidget {

    /**
     * Return widget name
     *
     * @return string
     */
    function getName() {
      return lang('Reminders');
    } // getName

    /**
     * Return widget description
     *
     * @return string
     */
    function getDescription() {
      return lang("Displays a list of reminders sent to user that has this widget on his desktop");
    } // getDescription

    /**
     * Return widget body
     *
     * @param IUser $user
     * @param string $widget_id
     * @param string $column_wrapper_class
     * @return string
     */
    function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
      $reminders = Reminders::findActiveByUser($user);
      $id = HTML::uniqueId('reminders_widget');

      $result = '<ul class="reminders_widget" id="' . $id . '">';
      if (is_foreachable($reminders)) {
        AngieApplication::useHelper('ago', GLOBALIZATION_FRAMEWORK, 'modifier');

        foreach ($reminders as $reminder) {
          $result.= '<li class="reminder" reminder_id="' . $reminder->getId() . '">';
          $result.= '<span class="reminder_avatar"><a href="' . $reminder->getCreatedBy()->getViewUrl() . '"><img src="' . $reminder->getCreatedBy()->avatar()->getUrl(IUserAvatarImplementation::SIZE_BIG) . '" /></a></span>';
          $result.= '<span class="reminder_author">' . clean($reminder->getCreatedBy()->getDisplayName(true)) . smarty_modifier_ago($reminder->getSentOn()) . '</span>';
          $result.= '<span class="reminder_related_object">' . object_link($reminder->getParent(), 40, array('class' => 'quick_view_item')) . '</span>';
          if ($reminder->getComment()) {
            $result.= '<span class="reminder_comment">' . nl2br($reminder->getComment()) . '</span>';
          } // if
          $result.= '<a href="' . $reminder->getDismissUrl(true) . '" class="reminder_dismiss"><img src="' . AngieApplication::getImageUrl('icons/12x12/dismiss.png', REMINDERS_FRAMEWORK) . '" /></a>';
          $result.= '</li>';
        } // foreach
      } // if
      $result.= '</ul><script type="text/javascript">$("#' . $id . '").remindersHomescreenWidget()</script>';

      return $result;
    } // renderBody

  }