<?php

  /**
   * date modifier implementation
   *
   * @package angie.frameworks.environment
   */

  /**
   * Return formated date
   *
   * @param string $content
   * @param string $default
   * @return string
   */
  function smarty_modifier_date($content, $offset = null) {
  	if ($content instanceof DateTimeValue) {
  		return $content->formatDateForUser(Authentication::getLoggedUser(), $offset);
  	} else if($content instanceof DateValue) {
      return $content->formatForUser(Authentication::getLoggedUser(), $offset);
    } else {
      $date = new DateValue($content);
      return $date->formatForUser(Authentication::getLoggedUser(), $offset);
    } // if
  } // smarty_modifier_date