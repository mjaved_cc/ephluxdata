{wrap field=name}
  {label for=languageName required=yes}Name{/label}
  {text_field name='language[name]' value=$language_data.name id=languageName required=true}
{/wrap}

{wrap field=type}
  {label for=languageLocale required=yes}Locale{/label}
  {select_locale name='language[locale]' value=$language_data.locale id=languageLocale required=true class="select_locale_slc"}
{/wrap}