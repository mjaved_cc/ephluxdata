{title}View Payment{/title}
{add_bread_crumb}View Payment{/add_bread_crumb}

<div id="payment" class="content_stack_wrapper" style="margin-bottom:10px;">
  <div class="content_stack_element">
    <div class="content_stack_element_info">
      <h3>{lang}Payment Details{/lang}</h3>
    </div>
    <div class="content_stack_element_body">
      {if $active_payment->getTransactionId()}
      	<dt>{lang}Transaction id{/lang}</dt>
     	 <dd>{$active_payment->getTransactionId()}</dd>
      {/if}
      
      <dt>{lang}Paid for:{/lang}</dt>
      <dd>{$active_payment->getParent()->getName()}</dd>
      
      <dt>{lang}Amount{/lang}</dt>
      <dd>{$active_payment->getAmount()|money}</dd>
      
      {if $active_payment->getTaxAmount()}
        <dt>{lang}Transaction tax{/lang}</dt>
        <dd>{$active_payment->getTaxAmount()|money}</dd>
      {/if}
      
      <dt>{lang}Currency{/lang}</dt>
      <dd>{$active_payment->getCurrency()->getCode()}</dd>
      
      {if $active_payment->getPayerId()}
        <dt>{lang}Payer id{/lang}</dt>
        <dd>{$active_payment->getPayerId()}</dd>
      {/if}
      
      <dt>{lang}Status{/lang}</dt>
      <dd>{$active_payment->getStatus()}</dd>
     
      {if $active_payment->getReason()}
        <dt>{lang}Reason{/lang}</dt>
        <dd>{$active_payment->getReason()}</dd>
        
        <dt>{lang}Reason text{/lang}</dt>
        <dd>{$active_payment->getReasonText()}</dd>
      {/if}
      
      <dt>{lang}Paid on{/lang}</dt>
      <dd>{$active_payment->getTimestamp()|date:0}</dd>
      
      <dt>{lang}Service{/lang}</dt>
      <dd>{$active_payment->getGateway()->getGatewayName()}</dd>
    
      {if $active_payment->getComment()}
      	<dt>{lang}Comment{/lang}</dt>
      	<dd>{$active_payment->getComment()}</dd>
       {/if}
    </div>
  </div>
 </div>