<?php

  /**
   * select_system_permissions helper implementation
   *
   * @package angie.frameworks.authentication
   * @subpackage helpers
   */

  /**
   * Render select system permissions box
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_system_permissions($params, &$smarty) {
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('select_system_permissions');
    } // if
    
    if(isset($params['class'])) {
      $params['class'] .= ' select_system_permissions';
    } else {
      $params['class'] = 'select_system_permissions';
    } // if
    
    $options = array(
      'name' => array_required_var($params, 'name', true), 
    	'value' => array_var($params, 'value', null, true), 
    	'permissions' => Roles::getPermissions(), 
    	'protect' => array_var($params, 'protect', null, true), 
    );
    
    if(empty($options['value'])) {
      $options['value'] = array();
    } // if
    
    if(empty($options['protect'])) {
      $options['protect'] = array();
    } // if
    
    return HTML::openTag('div', $params) . '</div><script type="text/javascript">$("#' . $params['id'] . '").selectSystemPermissions(' . JSON::encode($options) . ')</script>';
  } // smarty_function_select_system_permissions