<?php return array(
  'API Subscriptions',
  'New Subscription',
  'Email address is required',
  'Password is required',
  'Failed to log you in with data you provided. Please try again',
  'Email address is required field',
  'Email address is not in valid format',
  'There is no user account that matches the e-mail address you entered',
  'We emailed reset password instructions at :email',
  'Minimum password length is 3 characters',
  'Passwords do not match',
  'Maintenance Mode',
  'Roles and Permissions',
  'New Role',
  'Change Settings',
  'Users',
  'New User',
  'Password value is required',
  'Repeat Password value is required',
  'Inserted values does not match',
  'An Account has been Created for You',
  '<a href=":creator_url" style=":link_style">:name</a> has created an account for you. You can <a href=":login_url" style=":link_style">log in</a> with the following parameters',
  'Email',
  'without quotes',
  'Password',
  'Additionally, following welcome message was provided',
  'Welcome',
  'User account not found',
  'User account is no longer active',
  'Invalid password',
  'System is in maintenance mode',
  'Unknown error. Please contact support for assistance',
  'Minimal password length is :min_length letters',
  'At least one number is required',
  'At least one lower case and one uppercase letter are required',
  'At least one of the following symbols is required: , . ; : ! $ % ^ &',
  'Password Rules',
  '-- None --',
  'None',
  'No users available',
  'No users found',
  'Select Users',
  'Please Select',
  'Unknown User',
  'API Subscription #:num',
  'Client name is required',
  'Subscription token needs to be unique',
  'Subscription token is required',
  'Anonymous Users',
  'Who is Online?',
  'one minute',
  'hour',
  ':num minutes',
  'People who were online in the last :minutes',
  'Nobody was online in the last :minutes',
  'Avatar',
  'Role',
  'Last Visit On',
  'Local Time',
  'Password is too short',
  'Password requires at least one number',
  'Password requires at least one lower case and at least one uppercase letter',
  'Password requires that you use at least one symbol',
  'role',
  'Edit',
  'Delete',
  'Are you sure that you want to delete this system role?',
  'Role name needs to be unique',
  'Role name is required and it needs to be at least 3 characters long',
  'System Access',
  'Administration Access',
  'Use API',
  'Use Feeds',
  'See Private Objects',
  'Manage Trashed Data',
  'People with this permissions can list objects that are moved to trash. This permission also lets people to restore object from trash, or to permanently remove them from the system',
  'This permission defines whether user has permission to access the system. Set this to No if you don\'t want to delete specific user accounts but you want to restrict them access to the system.',
  'Set this permission to Yes if you want to give administration permissions to users with selected role. This permission overrides every other permissions and additionally gives access to administration panel to users who have it.',
  'Set this permission to Yes if you want to let users use API. API is used to let external applications work with application data (calendar applications, RSS readers, specialized tools like timer applications etc)',
  'Set this permission to Yes if you want to let users use feeds. Feeds are used to let external applications work with application data (calendar applications, RSS readers etc)',
  'Set to Yes for roles that you want to be able to see objects marked as private. Usually this is set to Yes only for the members of your own company and to No for the client roles to hide sensitive, internal discussions and other confidential information.',
  'Build Index',
  'Company and Role',
  'Company and Role have been updated',
  'Update Profile',
  'User profile has been updated',
  'Settings have been updated',
  'Home Screen',
  'Change Password',
  'Password has been changed',
  'Email address you provided is already in use',
  'Email value is not valid',
  'Email value is required',
  'Role is required',
  'Feed Token',
  'Reset Your Password',
  'Sorry, Your Current Password has Expired',
  'Please, click on the button bellow to change your password and return to your home screen',
  'Change Password Now!',
  'No users selected',
  'Change',
  'Minutes',
  'About System Roles',
  'System Roles',
  'System roles define permissions on system level, such as administration access, ability to manage other people\'s accounts and so on. Default system role will be preselected when creating a new user. It can be changed by the person creating a new account',
  'Example: activeCollab Timer',
  'Client Name',
  'Client Vendor',
  'Read Only',
  'Add Subscription',
  'Update Subscription',
  'Save Changes',
  'API access is not enabled for this user account',
  'List',
  'API Subscription Details',
  'Client',
  'Unknown',
  'Enabled',
  'Yes',
  'No',
  'Access Level',
  'Read and Write',
  'Created On',
  'Last Used On',
  'Never Used',
  'API URL: <span class="token">:url</span>',
  'Token: <span class="token">:token</span>',
  'Forgot Password',
  'Email Address',
  'Back to start page',
  'Back to Login Form',
  'Submit',
  'Login',
  'Forgot password?',
  'Interface',
  'Remember me for 14 days',
  'Reset Password',
  'New Password',
  'Repeat',
  'Use the form below to reset password for :name\'s account',
  'Reset',
  'Enable Maintenance Mode',
  'Maintenance Message',
  'Permissions',
  'Add Role',
  'Update Role',
  'All System Roles',
  'Users with <span class="active_role_name">:role_name</span> role',
  'User is archived',
  'User is in Trash',
  'There are no users with <span class="active_role_name">:role_name</span> role',
  'Details',
  'First Name',
  'Last Name',
  'Repeat Password',
  'Add User',
  'Update User',
  'Update Password',
  'Repeat password',
  'Tips',
  'To select a user and load its details, please click on it in the list on the left',
  'It is possible to select multiple users at the same time. Just hold Ctrl key on your keyboard and click on all the users that you want to select',
  'All Users',
  'Log In',
  'Sign in with your credentials',
  'Remember me',
); ?>