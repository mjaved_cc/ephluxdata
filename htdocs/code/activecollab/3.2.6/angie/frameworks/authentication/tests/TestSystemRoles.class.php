<?php

  /**
   * Authentication tests
   *
   * @package angie.frameworks.authentication
   * @subpackage tests
   */
  class TestRoles extends AngieModelTestCase {
    
    /**
     * Test if we have all the permissions that we need and their dependencies
     */
    function testPermissions() {
      $permissions = Roles::getPermissions();
      
      $this->assertIsA($permissions, 'NamedList');
      
      $this->assertTrue(isset($permissions['has_system_access']));
      $this->assertTrue(isset($permissions['has_admin_access']));
      $this->assertTrue(isset($permissions['can_see_private_objects']));
      
      $this->assertEqual($permissions['has_admin_access']['depends_on'], 'has_system_access');
      $this->assertEqual($permissions['can_see_private_objects']['depends_on'], 'has_system_access');
      
      $role = new Role();
      
      $this->assertFalse($role->getPermissionValue('has_system_access'));
      $this->assertFalse($role->getPermissionValue('has_admin_access'));
      $this->assertFalse($role->getPermissionValue('can_see_private_objects'));
      
      $role->setPermissionValue('has_admin_access', true);
      $this->assertFalse($role->getPermissionValue('has_admin_access')); // Depends on system access
      $role->setPermissionValue('has_system_access', true);
      $this->assertTrue($role->getPermissionValue('has_admin_access')); // Depends on system access
    } // testPermissions
    
  }