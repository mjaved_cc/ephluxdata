<?php

  /**
   * select_theme helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */

  /**
   * Render select theme widget
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_theme($params, &$smarty) {
    $name = array_required_var($params, 'name');
    $value = array_var($params, 'value', false);
    
    $possibilities = array(
      'default' => lang('Default'), 
    );

    if(array_var($params, 'optional', false, true)) {
      return HTML::optionalSelectFromPossibilities($name, $possibilities, $value, $params, lang('-- System Default (:theme) --', array('theme' => ConfigOptions::getValue('theme'))));
    } else {
      return HTML::selectFromPossibilities($name, $possibilities, $value, $params);
    } // if
  } // smarty_function_select_theme