<?php

  /**
   * Wrap editor field
   * 
   * @param array $params
   * @param string $content
   * @param Smarty $smarty
   * @param boolean $repeat
   * @return string
   */
  function smarty_block_wrap_editor($params, $content, &$smarty, &$repeat) {
    if($repeat) {
      return;
    } // if
    
    require_once ENVIRONMENT_FRAMEWORK_PATH . '/helpers/block.wrap.php';
    
    if(isset($params['class']) && $params['class']) {
      $params['class'] .= ' big_editor';
    } else {
      $params['class'] = 'big_editor';
    } // if
    
    if(isset($params['visual'])) {
      $visual = (boolean) $params['visual'];
      unset($params['visual']);
    } else {
      $visual = Authentication::getLoggedUser() instanceof User && ConfigOptions::getValueFor('visual_editor', Authentication::getLoggedUser());
    } // if
    
    $params['class'] .= ($visual ? ' visual' : ' not_visual');
    
    return smarty_block_wrap($params, $content, $smarty, $repeat);  
  } // smarty_block_wrap_editor