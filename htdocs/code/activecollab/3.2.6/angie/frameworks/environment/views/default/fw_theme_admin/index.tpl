<div id="theme_admin">
  {form action=Router::assemble('theme_admin')}
    <div class="content_stack_wrapper">
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}Default Theme{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
          {wrap field=theme}
            {select_theme name="settings[theme]" value=$settings_data.theme optional=false label='Default Theme'}
          {/wrap}
        </div>
      </div>
    </div>
    
    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>