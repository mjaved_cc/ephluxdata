<?php

  /**
   * editor_field helper
   *
   * @package activeCollab.modules.system
   * @subpackage helpers
   */
  
  /**
   * Render HTML editor
   *
   * @param array $params
   * @param string $content
   * @param Smarty $smarty
   * @param boolean $repeat
   * @return string
   */
  function smarty_block_editor_field($params, $content, &$smarty, &$repeat) {
    if($repeat) {
      return;
    } // if
    
    // Determine if we need to use visual editor or textarea
    if(AngieApplication::getPreferedInterface() == AngieApplication::INTERFACE_DEFAULT) {
      if(isset($params['visual'])) {
        $visual = (boolean) array_var($params, 'visual', true, true);
      } else {
        $visual = Authentication::getLoggedUser() instanceof User && ConfigOptions::getValueFor('visual_editor', Authentication::getLoggedUser());
      } // if
    } else {
      $visual = false;
    } // if
    
    if(empty($params['id'])) {
      $params['id'] = HTML::uniqueId('visual_editor');
    } // if
    
    $name_parameter = array_var($params, 'name');
    $variable_name = substr($name_parameter, 0, strrpos($name_parameter, '['));
    $return_string = '';
    
    if($visual) {
      $label = array_var($params, 'label', null, true);
      
      if($label) {
        $return_string .= HTML::label($label, null, isset($params['required']) && $params['required'], array('class' => 'main_label'));
      } // if
      
      $buttons = array_var($params, 'buttons', null);

      $editor_params = array(
      	'name'         			=> $name_parameter,
      	'buttons'      			=> $buttons,
      	'value'        			=> $content,
      	'vertical_resize'		=> array_var($params, 'resize', false),
      	'editor_css'   			=> file_get_contents(VISUAL_EDITOR_FRAMEWORK_PATH . '/assets/default/stylesheets/editor/editor.css'),
      	'embeded_name'			=> $variable_name ? $variable_name . '[embeded_objects]' : 'embeded_objects',
      	'whitelisted_tags'	=> HTML::getWhitelistedTagsForEditor(),
				'required'					=> isset($params['required']) && $params['required'],   
        'add_script_url'					=> Router::assemble('code_snippets_add'),
				'view_script_url'					=> Router::assemble('code_snippet', array('code_snippet_id' => '--SNIPPET-ID--')),
        'edit_script_url'					=> Router::assemble('code_snippet_edit', array('code_snippet_id' => '--SNIPPET-ID--')),
      	'quick_search_url'				=> Router::assemble('quick_backend_search'),
      	'attachments_upload_url'	=> Router::assemble('temporary_attachment_add'),      
      );
            
      $return_string .= '<div id="' . $params['id'] . '"></div><script type="text/javascript">$("#' . $params['id'] . '").visualEditor(' . JSON::encode($editor_params) . ')</script>';
    } else {
      if(isset($params['class'])) {
        $params['class'] .= ' editor';
      } else {
        $params['class'] = 'editor';
      } // if
      
      $return_string .= HTML::textarea(@$params['name'], $content, $params);
    } // if
    
    return $return_string;
  } // smarty_block_editor_field