<table class="common developer_action_list" cellspacing="0" id="developer_popup_actions">
  <tr>
    <td class="submit"><a href="{assemble route=developer_empty_cache}" message="{lang}Emptying cache{/lang}"><img src="{image_url name='icons/16x16/proceed.png' module='environment'}" alt="go" /></a></td>
    <td class="action_title">{lang}Empty cache{/lang}</td>
  </tr>
  <tr>
    <td class="submit"><a href="{assemble route=developer_delete_compiled_templates}" message="{lang}Deleting compiled templates{/lang}"><img src="{image_url name='icons/16x16/proceed.png' module='environment'}" alt="go" /></a></td>
    <td class="action_title">{lang}Delete compiled templates{/lang}</td>
  </tr>
  <tr>
    <td class="submit"><a href="{assemble route=developer_rebuild_images}" message="{lang}Rebuilding images{/lang}"><img src="{image_url name='icons/16x16/proceed.png' module='environment'}" alt="go" /></a></td>
    <td class="action_title">{lang}Rebuild assets{/lang}</td>
  </tr>
  <tr>
    <td class="submit"><a href="{assemble route=developer_rebuild_localization}" message="{lang}Rebuilding localization{/lang}"><img src="{image_url name='icons/16x16/proceed.png' module='environment'}" alt="go" /></a></td>
    <td class="action_title">{lang}Rebuild localization{/lang}</td>
  </tr>
</table>

{if AngieApplication::isInDevelopment()}
<div class="developer_playground">
  <a href="{assemble route='developer_playground'}">{lang}Playground Page{/lang}</a>
</div>
{/if}

<script type="text/javascript">
  var original_image = false;
  $("#developer_popup_actions td.submit a").each(function () {
    var anchor = $(this);
    var image = anchor.find('img:first');

    if (original_image === false) {
      original_image = image.attr('src');
    } // if

    anchor.click(function () {
      image.attr('src', App.Wireframe.Utils.indicatorUrl());
      $.ajax({
        'url' : App.extendUrl(anchor.attr('href'), { 'async' : 1 }),
        'type' : 'post',
        'data' : { 'submitted' : 'submitted' },
        success : function () {
          App.Wireframe.Flash.success(anchor.attr('message') + ' ' + App.lang('succeeded'));
          image.attr('src', original_image);
        },
        error : function () {
          App.Wireframe.Flash.error(anchor.attr('message') + ' ' + App.lang('failed'));
          image.attr('src', original_image);
        }
      });
      
      return false;
    });
  });
</script>