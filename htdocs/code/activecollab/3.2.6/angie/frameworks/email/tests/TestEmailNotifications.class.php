<?php

  /**
   * Test email notifications
   */
  class TestEmailNotifications extends AngieModelTestCase {
  	
  	/**
  	 * Test objects table instance
  	 * 
  	 * @var DBTable
  	 */
  	protected $test_objects_table;
  	
  	/**
  	 * Construct test email notifications case
  	 * 
  	 * @param mixed $label
  	 */
  	function __construct($label = false) {
  		parent::__construct($label);
  		
  		$test_email_folder = dirname(__FILE__);
  		
  		require_once "$test_email_folder/test_notifier_event/TestNotifierEvent.class.php";
  		
  		require_once "$test_email_folder/test_email_objects/BaseTestEmailObject.class.php";
  		require_once "$test_email_folder/test_email_objects/BaseTestEmailObjects.class.php";
  		require_once "$test_email_folder/test_email_objects/TestEmailObject.class.php";
  		require_once "$test_email_folder/test_email_objects/TestEmailObjects.class.php";
  	} // __construct(
  	
  	/**
  	 * Set up test case
  	 */
  	function setUp() {
  		parent::setUp();
  		
  		ApplicationMailer::setAdapter(new SilentMailerAdapter());
  		ApplicationMailer::setDefaultSender(new AnonymousUser('Default From', 'default@from.com'));
  		ApplicationMailer::setDecorator(new ApplicationMailerDecorator());
  		ApplicationMailer::connect();
  		
  		$this->test_objects_table = DB::createTable(TABLE_PREFIX . 'test_email_objects');
  		$this->test_objects_table->addColumns(array(
  		  DBIdColumn::create(), 
  		  DBNameColumn::create(), 
  		  DBTextColumn::create('body'), 
  		));
  		
  		$this->test_objects_table->save();
  	} // setUp
  	
  	/**
  	 * Tear down test case
  	 */
  	function tearDown() {
  		ApplicationMailer::disconnect();
  		
  		if($this->test_objects_table->exists()) {
  		  $this->test_objects_table->delete();
  		} // if
  		
  		parent::tearDown();
  	} // tearDown
  	
  	/**
  	 * Test if we have properly developed test email objects
  	 */
  	function testObjects() {
  		$this->assertIsA($this->test_objects_table, 'DBTable');
  		$this->assertFalse($this->test_objects_table->isNew());
  		
  		$object = new TestEmailObject();
  		
  		$this->assertIsA($object, 'INotifier');
  		$this->assertIsA($object, 'INotifierContext');
  	} // testObjects
  	
  	/**
  	 * Notify group of people
  	 */
  	function testNotify() {
  	  $test_context = new TestEmailObject();
  		
  		$test_context->setName('Test Context 1');
  		$test_context->setBody('Test Body 1');
  		
  		$test_context->save();
  		
  		$this->assertTrue($test_context->isLoaded());
  		$this->assertEqual($test_context->getId(), 1);
  		
  		$users = Users::find();
  		
  		$this->assertIsA($users, 'DBResult');
  		$this->assertEqual($users->count(), 1);
  		
  		ApplicationMailer::getDefaultSender()->notifier()->notifyUsers($users, $test_context, new TestNotifierEvent(), array(
  		  'test_subject' => 'Test Notify Users Subject',
  			'test_body' => 'Test Notify Users Body',  
  		), array(
  		  'method' => ApplicationMailer::SEND_INSTANTNLY, 
  		));
  		
  		$this->assertEqual((integer) DB::executeFirstCell('SELECT COUNT(id) FROM ' . TABLE_PREFIX . 'mailing_activity_logs'), 1);
  		$log_entry = MailingActivityLogs::find(array(
  		  'one' => true,
  		));
  		
  		$this->assertIsA($log_entry, 'MessageSentActivityLog');
  		
  		if($log_entry instanceof MessageSentActivityLog) {
  		  $this->assertEqual($log_entry->getFromName(), ApplicationMailer::getDefaultSender()->getName());
    		$this->assertEqual($log_entry->getFromEmail(), ApplicationMailer::getDefaultSender()->getEmail());
    		
    		$this->assertEqual($log_entry->getToName(), $users[0]->getDisplayName());
    		$this->assertEqual($log_entry->getToEmail(), $users[0]->getEmail());
    		
    		$this->assertEqual($log_entry->getAdditionalProperty('subject'), 'Test Notify Users Subject {' . $test_context->getNotifierContextId() . '}');
    		$this->assertContains($log_entry->getAdditionalProperty('body'), 'Test Notify Users Body');
  		} // if
  	} // testNotify
  	
//  	/**
//  	 * Send notification to subscribers
//  	 */
//  	function testNotifySubscribers() {
//  		
//  	} // testNotifySubscribers
//  	
//  	/**
//  	 * Test notify administrators
//  	 */
//  	function testNotifyAdmins() {
//  		
//  	} // testNotifyAdmins
  	
  }