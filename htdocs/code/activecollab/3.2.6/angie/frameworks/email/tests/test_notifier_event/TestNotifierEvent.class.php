<?php

  /**
   * Test notifier event implementation
   * 
   * @package angie.frameworks.email
   * @subpackage tests
   */
  class TestNotifierEvent extends NotifierEvent {
  	
  	/**
  	 * Construct test notifier event
  	 */
  	function __construct() {
  		parent::__construct('test', EMAIL_FRAMEWORK);
  	} // __construct
  
  	/**
  	 * Return email template path
  	 * 
  	 * @return string
  	 */
  	function getTemplatePath() {
  		return dirname(__FILE__) . '/message.tpl';
  	} // getTemplatePath
  	
  }