<?php

  /**
   * Render inspector table for email notifications
   *
   * @package angie.frameworks.email
   * @subpackage helpers
   */

  /**
   * Render notification inspector
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_notification_inspector($params, &$smarty) {
    $context = array_required_var($params, 'context', true, 'ApplicationObject');
    $context_view_url = array_var($params, 'context_view_url');
    $recipient = array_required_var($params, 'recipient', true, 'IUser');
    $language = $recipient->getLanguage();

    $action = lang('Created by', null, true, $language);
    $action_by = $context->getCreatedBy();

    $link_to_context = $context_view_url ?
      array($context_view_url, $context->getName()) : // Link
      $context->getName(); // Just the name

    $properties = new NamedList(array(
      'name' => array(
        'label' => $context->getVerboseType(false, $language),
        'value' => array($link_to_context),
      )
    ));

    if($context instanceof ICategory && $context->category()->get() instanceof Category) {
      $properties->add('category', array(
        'label' => lang('Category', null, null, $language),
        'value' => array($context->category()->get()->getName()),
      ));
    } // if

    EventsManager::trigger('on_notification_inspector', array(&$context, &$recipient, &$properties, &$action, &$action_by));

    if($context instanceof IAssignees) {
      if($context->assignees()->isResponsible($recipient)) {
        $properties->add('responsibility', array(
          'label' => lang('Responsibility', null, null, $language),
          'value' => array(lang('You are responsible for this :type', array(
            'type' => $context->getVerboseType(true, $language),
          ), null, $language)),
        ));
      } elseif($context->assignees()->isAssignee($recipient)) {
        $properties->add('responsibility', array(
          'label' => lang('Responsibility', null, null, $language),
          'value' => array(lang('You are assigned to this :type', array(
            'type' => $context->getVerboseType(true, $language),
          ), null, $language)),
        ));
      } // if
    } // if

    // Open inspector table
    $result = '<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; text-align: left">';

    $counter = 0;
    foreach($properties as $k => $v) {
      $result .= '<tr>';

      $result .= '<td style="width: 80px; vertical-align: top; padding: 2px;">' . clean($v['label']) . ':</td>';

      if(is_array($v['value'])) {
        $links = array();

        foreach($v['value'] as $link) {
          if (is_string($link)) {
            $links[] = clean($link);
          } else if (is_array($link) && is_string($link[0]) && is_string($link[1])) {
            $links[] = '<a href="' . clean($link[0]) . '" style="' . Theme::getProperty('notifications.link') . '">' . clean($link[1]) . '</a>';
          } // if
        } // foreach

        $links = implode(' - ', $links);
      } else {
        $links = '<b>' . clean($v['value']) . '</b>';
      } // if

      $result .= '<td style="vertical-align: top; padding: 2px;">' . $links . '</td>';

      // In case of first row, add creation info as well
      if(empty($counter) && $action_by instanceof IUser) {
        $result .= '<td style="width: 100px; padding: 2px; text-align: right; vertical-align: middle" rowspan="' . $properties->count() . '">' . clean($action) . '<br><a href="' . clean($action_by->getViewUrl()) . '" style="' . Theme::getProperty('notifications.link') . '">' . clean($action_by->getDisplayName(true)) . '</a></td>';
        $result .= '<td style="width: 50px; padding: 2px; text-align: right; vertical-align: middle" rowspan="' . $properties->count() . '"><img src="' . clean($action_by->avatar()->getUrl(40)) . '" alt=""></td>';
      } // if

      $result .= '</tr>';

      $counter++;
    } // foreach

    return "$result</table>";
  } // smarty_function_notification_inspector