<?php

  /**
   * Wrap notification core block
   *
   * @package angie.frameworks.angie
   * @subpackage helpers
   */

  /**
   * Render main notification block wrapper
   *
   * @param array $params
   * @param string $content
   * @param Smarty $smarty
   * @param boolean $repeat
   * @return mixed
   */
  function smarty_block_notification_wrapper($params, $content, &$smarty, &$repeat) {
    if($repeat) {
      return;
    } // if

    $context = array_var($params, 'context', null, false);
    $sender = array_required_var($params, 'sender', false, 'IUser');
    $recipient = array_required_var($params, 'recipient', false, 'IUser');
    $language = $recipient->getLanguage();

    $inspect = $context && array_var($params, 'inspect', true);

    $title = isset($params['title']) && $params['title'] ? lang($params['title'], null, true, $language) : lang('Notification', null, true, $language);
    
    if(strpos($title, ':') !== false && $context instanceof ApplicationObject) {
      $title = lang($title, array(
        'name' => $context->getName(),
        'type' => $context->getVerboseType(false, $language),
        'type_lowercase' => $context->getVerboseType(true, $language),
      ), true, $language);
    } // if

    // Open content wrapper
    $result = '<table cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="656" style="width: 656px; background-color: #fff; margin-top: 40px; margin-left: auto; margin-right: auto; border-color: #d0d2c9; border-width: 1px; border-bottom: 0; border-top-left-radius: 20px; border-top-right-radius: 20px; border-style: solid;"><tr><td style="text-align: left; color: #4b4b4b; padding: 47px; padding-bottom: 16px;">';

    // Greeting and title
    $result .= '<table cellpadding="0" cellspacing="0" width="100%"><tr><td style="vertical-align: middle; text-align: left;" bgcolor="#ffffff">';

    if(array_var($params, 'greet', true)) {
      $result .= '<div style="font-size: 20px; margin-bottom: 4px;">' . lang('Hi :name', array(
        'name' => $recipient->getFirstName(true),
      ), true, $language) . ',</div>';
    } // if

    $result .= '<div style="font-size: 36px">' . clean($title) . '</div>';
    $result .= '</td>';

    // Logo
    AngieApplication::useHelper('notification_identity', EMAIL_FRAMEWORK);

    $result .= '<td style="vertical-align: middle; text-align: center; width: 100px" bgcolor="#ffffff">' . smarty_function_notification_identity($params, $smarty) . '</td>';
    $result .= '</tr></table>';

    // Body block
    if($inspect) {
      $result .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td style="padding-top: 16px; padding-bottom: 16px">' . $content . '</td></tr></table>'; // We need extra padding at the bottom of the message in case when we are displaying additional info and inspector
    } else {
      $result .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#ffffff"><tr><td style="padding-top: 16px">' . $content . '</td></tr></table>';
    } // if

    // Context related information, if present
    if($context) {
      if($context instanceof IAttachments) {
        AngieApplication::useHelper('notification_attachments_table', EMAIL_FRAMEWORK);

        $result .= smarty_function_notification_attachments_table(array(
          'object' => $context,
          'recipient' => $recipient,
        ), $smarty);
      } // if

      // Open in Browser link
      if(array_var($params, 'open_in_browser', true) && isset($params['context_view_url']) && $params['context_view_url']) {
        $result .= '<div style="text-align: center; padding: 10px;"><a href="' . clean($params['context_view_url']) . '" style="padding: 5px; padding-left: 15px; padding-right: 15px; color: #4b4b4b; text-decoration: none; background-color: #eeeeed; border-color: #dedede; border-width: 1px; border-radius: 20px; border-style: solid">' . lang('Open this :type in Your Web Browser', array(
          'type' => $context->getVerboseType($language),
        ), true, $language) . '</a></div>';
      } // if
    } // if

    $result .= '</td></tr></table>'; // Close content wrapper
    //$result .= '</div>'; // Close content wrapper

    // Inspector
    if($inspect) {
      AngieApplication::useHelper('notification_inspector', EMAIL_FRAMEWORK);

      $result .= '<table cellpadding="0" cellspacing="0" bgcolor="#ededec" style="background-color: #ededec; width: 656px; margin-left: auto; margin-right: auto; border-color: #d0d2c9; border-width: 1px; border-top: 0; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; border-style: solid;"><tr><td style="padding: 27px;">' . smarty_function_notification_inspector(array(
        'recipient' => $recipient,
        'context' => $context,
        'context_view_url' => array_var($params, 'context_view_url'),
        'sender' => $sender,
      ), $smarty) . '</td></tr></table>';

    // Just close the blokc
    } else {
      $result .= '<div style="background-color: #fff; width: 600px; margin-left: auto; margin-right: auto; padding: 27px; border-color: #d0d2c9; border-width: 1px; border-top: 0; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; border-style: solid;">&nbsp;</div>';
    } // if

    // Close wrapper and done
    return "$result</div>";
  } // smarty_block_notification_wrapper