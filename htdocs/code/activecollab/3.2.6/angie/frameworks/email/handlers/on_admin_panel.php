<?php

  /**
   * on_admin_panel event handler
   * 
   * @package angie.framework.email
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function email_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToGeneral('email', lang('Email'), Router::assemble('email_admin'), AngieApplication::getImageUrl('admin_panel/email.png', EMAIL_FRAMEWORK));

    $admin_panel->addToTools('email_to_comment', lang('Email Reply to Comment'), Router::assemble('email_admin_reply_to_comment'), AngieApplication::getImageUrl('admin_panel/email-to-comment.png', EMAIL_FRAMEWORK));

    if(MASS_MAILER_ENABLED) {
      $admin_panel->addToTools('mass_mailer', lang('Mass Mailer'), Router::assemble('admin_tools_mass_mailer'), AngieApplication::getImageUrl('admin_panel/mass-mailer.png', EMAIL_FRAMEWORK), array(
        'onclick' => new FlyoutFormCallback('mass_mail_sent'), 
      ));
    } // if
  } // email_handle_on_admin_panel