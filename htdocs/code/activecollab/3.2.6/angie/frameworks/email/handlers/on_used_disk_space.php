<?php

  /**
   * on_used_disk_space event handler implementation
   *
   * @package angie.frameworks.email
   * @subpackage handlers
   */

  /**
   * Handle on_used_disk_space event
   *
   * @param integer $used_disk_space
   */
  function email_handle_on_used_disk_space(&$used_disk_space) {
    $used_disk_space += DB::executeFirstCell('SELECT SUM(file_size) FROM ' . TABLE_PREFIX . 'incoming_mail_attachments');
  } // email_handle_on_used_disk_space