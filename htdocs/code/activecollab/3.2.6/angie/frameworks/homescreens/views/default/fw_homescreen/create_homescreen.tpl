<div id="create_homescreen">
  {form action=$active_object->homescreen()->getCreateUrl()}
    {wrap_fields}
      {wrap field=homescreen_template}
        {select_homescreen_template name='homescreen[based_on_id]' value=$homescreen_data.based_on_id for=$active_object}
      {/wrap}
    {/wrap_fields}

    {wrap_buttons}
      {submit}Create Home Screen{/submit}
    {/wrap_buttons}
  {/form}
</div>