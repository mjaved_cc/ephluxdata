<?php

  /**
   * Homescreen helper implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   */
  abstract class IHomescreenImplementation {
    
    /**
     * Parent object
     *
     * @var IHomescreen
     */
    protected $object;
  
    /**
     * Construct deksktop helper instance
     * 
     * @param IHomescreen $object
     */
    function __construct(IHomescreen $object) {
      $this->object = $object;
    } // __construct
    
    /**
     * Return home screen for parent object
     * 
     * @return Homescreen
     */
    abstract function get();
    
    /**
     * Return list of sets that can be used as a template for custom set
     * 
     * @return array
     */
    function getPossibleTemplates() {
      return null;
    } // getPossibleTemplates
    
    /**
     * Create a blank home screen
     * 
     * @return Homescreen
     */
    function createBlank() {
      try {
        DB::beginWork('Creating a blank homescreen @ ' . __CLASS__);
        
        $homescreen = new Homescreen();
        $homescreen->setParent($this->object);
        $homescreen->save();
        
        $homescreen_tab = new CenterHomescreenTab();
        $homescreen_tab->setHomescreenId($homescreen->getId());
        $homescreen_tab->setName(lang('Welcome'));
        $homescreen_tab->setPosition(1);
        $homescreen_tab->save();
        
        DB::commit('Blank homescreen created @ ' . __CLASS__);
        
        return $homescreen;
      } catch(Exception $e) {
        DB::rollback('Failed to create a blank homescreen @ ' . __CLASS__);
        throw $e;
      } // try
    } // createBlank
    
    /**
     * Create a home screen based on existing home screen
     * 
     * @param Homescreen $based_on
     * @return Homescreen
     */
    function createBasedOn(Homescreen $based_on) {
      if($based_on instanceof Homescreen) {
        try {
          DB::beginWork('Creating a home screen based on an existing home screen @ ' . __CLASS__);
          
          $homescreen = new Homescreen();
          $homescreen->setParent($this->object);
          $homescreen->save();
          
          foreach($based_on->getTabs() as $tab) {
            $tab->copyTo($homescreen);
          } // foreach
          
          DB::commit('Home screen based on a home screen created @ ' . __CLASS__);
          
          return $homescreen;
        } catch(Exception $e) {
          DB::rollback('Failed to create a home screen based on a home screen @ ' . __CLASS__);
          throw $e;
        } // try
      } else {
        throw new InvalidInstanceError('based_on', $based_on, 'Homescreen');
      } // if
    } // createBasedOn
    
    /**
     * Return parent's own home screen
     */
    function getOwnSet() {
      return $this->hasOwn() ? $this->get() : null;
    } // getOwnSet
    
    /**
     * Returns true if parent object has its own home screen, or is it using one 
     * of parent sets or default home screen
     * 
     * @return boolean
     */
    function hasOwn() {
      return $this->get()->getParentType() == get_class($this->object) && $this->get()->getParentId() == $this->object->getId();
    } // hasOwn
    
    /**
     * Returns true if parent object can have a descktop sec configured for it
     * 
     * @return boolean
     */
    abstract function canHaveOwn();
    
    // ---------------------------------------------------
    //  URL-s
    // ---------------------------------------------------
    
    /**
     * Return create home screen URL
     * 
     * @return string
     */
    function getCreateUrl() {
      return Router::assemble($this->object->getRoutingContext() . '_homescreen_create', $this->object->getRoutingContextParams());
    } // getCreateUrl
    
    /**
     * Return manage home screen URL
     * 
     * @return string
     */
    function getManageUrl() {
      return Router::assemble($this->object->getRoutingContext() . '_homescreen', $this->object->getRoutingContextParams());
    } // getManageUrl
    
    /**
     * Return delete home screen URL
     * 
     * @return string
     */
    function getDeleteUrl() {
      return Router::assemble($this->object->getRoutingContext() . '_homescreen_delete', $this->object->getRoutingContextParams());
    } // getDeleteUrl
    
    // ---------------------------------------------------
    //  Permissions
    // ---------------------------------------------------
    
    /**
     * Returns true if $user can create home screen for parent object
     * 
     * @param IUser $user
     * @return boolean
     */
    function canCreateSet(IUser $user) {
      if($this->hasOwn()) {
        return false; // Parent object already has a set
      } else {
        return $this->object->canEdit($user);
      } // if
    } // canCreateSet
    
    /**
     * Returns true if $user can manage home screen for parent object
     * 
     * @param IUser $user
     * @return boolean
     */
    function canManageSet(IUser $user) {
      if($user instanceof IUser) {
        return $this->object->canEdit($user);
      } else {
        throw new InvalidInstanceError('user', $user, 'User');
      } // if
    } // canManageSet
    
    /**
     * Returns true if $user can delete parent's custom home screen
     * 
     * @param IUser $user
     * @return boolean
     */
    function canDeleteSet(IUser $user) {
      return $this->object->canEdit($user);
    } // canDeleteSet
    
  }