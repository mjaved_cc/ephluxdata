<?php

  /**
   * Subtask specific assignees implementation
   *
   * @package angie.frameworks.subtasks
   * @subpackage models
   */
  class ISubtaskAssigneesImplementation extends IAssigneesImplementation {

    /**
     * Subtasks don't support other assignees
     *
     * @var bool
     */
    protected $support_multiple_assignees = false;

    /**
     * Send email notifications about re-assignment
     *
     * @param User $old_assignee
     * @param User $new_assignee
     * @param User $reassigned_by
     */
    function notifyOnReassignment($old_assignee, $new_assignee, User $reassigned_by) {
      $notify_new_assignee = $notify_old_assignee = false;

      if($old_assignee instanceof User && $new_assignee instanceof User) {
        if($old_assignee->getId() != $new_assignee->getId()) {
          $notify_new_assignee = $notify_old_assignee = true;
        } // if
      } elseif($old_assignee instanceof User) {
        $notify_old_assignee = true;
      } elseif($new_assignee instanceof User) {
        $notify_new_assignee = true;
      } // if

      if($notify_new_assignee) {
        $reassigned_by->notifier()->notifyUsers($new_assignee, $this->object->getParent(), 'subtasks/notify_new_assignee', array(
          'reassigned_by' => $reassigned_by,
          'subtask' => $this->object,
        ));
      } // if

      if($notify_old_assignee) {
        $reassigned_by->notifier()->notifyUsers($old_assignee, $this->object->getParent(), 'subtasks/notify_old_assignee', array(
          'reassigned_by' => $reassigned_by,
          'subtask' => $this->object,
        ));
      } // if
    } // notifyOnReassignment

  }