<?php

  /**
   * Text compare framework initialization file
   *
   * @package angie.framework.text_compare
   */
  
  define('TEXT_COMPARE_FRAMEWORK', 'text_compare');
  define('TEXT_COMPARE_FRAMEWORK_PATH', ANGIE_PATH . '/frameworks/text_compare');
  
  // Inject text compare framework in system module by default
  @define('TEXT_COMPARE_FRAMEWORK_INJECT_INTO', 'system');

?>