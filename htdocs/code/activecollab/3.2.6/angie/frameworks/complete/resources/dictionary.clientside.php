<?php return array(
  'Completed by :user on :date',
  'Open',
  'Lowest',
  'Low',
  'High',
  'Highest',
  'Normal',
  'Change',
  'Change Priority',
  'Priority has been changed',
  'Failed to update priority. Please try again later',
  'Priority successfully changed',
); ?>