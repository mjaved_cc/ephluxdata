<?php

  /**
   * Model generator class
   *
   * @package angie.library.application
   * @subpackage application
   */
  class AngieFrameworkModelBuilder {
    
    /**
     * Model that this builder instance belongs to
     *
     * @var AngieFrameworkModel
     */
    protected $model;
    
    /**
     * Name of the table that this model builder is added to
     *
     * @var DBTable
     */
    protected $table;
    
    /**
     * Name of the class that base object extends
     *
     * @var string
     */
    protected $base_object_extends = 'ApplicationObject';
    
    /**
     * Name of the class that base manager extends
     *
     * @var string
     */
    protected $base_manager_extends = 'DataManager';
    
    /**
     * Construct instances based on class name stored in a field
     *
     * @var string
     */
    protected $type_from_field;

    /**
     * Generate permissions in model class
     *
     * @var bool
     */
    protected $generate_permissions = false;

    /**
     * Generate view edit and delete URL-s
     *
     * @var bool
     */
    protected $generate_urls = false;
    
    /**
     * Value used for ordering records
     *
     * @var string
     */
    protected $order_by;

    /**
     * Name of the module where this model is injected to (works only for framework models)
     *
     * @var string
     */
    protected $inject_into = 'system';
    
    /**
     * Construct new model builder instance
     *
     * @param AngieFrameworkModel $model
     * @param string $table
     */
    function __construct(AngieFrameworkModel $model, DBTable $table) {
      $this->model = $model;

      if($table instanceof DBTable) {
        $this->table = $table;
      } else {
        throw new InvalidInstanceError('table', $table, 'DBTable');
      } // if
    } // __construct

    // ---------------------------------------------------
    //  Generation
    // ---------------------------------------------------

    /**
     * Return parent mode instance
     *
     * @return AngieFrameworkModel
     */
    function getModel() {
      return $this->model;
    } // getModel

    /**
     * Return fields
     *
     * @return NamedList
     */
    function getFields() {
      return $this->table->getColumns();
    } // getFields

    /**
     * Return destination module name
     *
     * @return string
     */
    function getDestinationModuleName() {
      return $this->model->getParent() instanceof AngieModule ? $this->model->getParent()->getName() : $this->getInjectInto();
    } // getDestinationModuleName

    /**
     * Return destination path of the module
     *
     * @return string
     */
    function getDestinationModulePath() {
      return $this->model->getParent() instanceof AngieModule ? $this->model->getParent()->getPath() : APPLICATION_PATH . '/modules/' . $this->getInjectInto();
    } // getDestinationModulePath
    
    // ---------------------------------------------------
    //  Getters and setters
    // ---------------------------------------------------
    
    /**
     * Return base_object_extends
     *
     * @return string
     */
    function getBaseObjectExtends() {
    	return $this->base_object_extends;
    } // getBaseObjectExtends
    
    /**
     * Set base_object_extends
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setBaseObjectExtends($value) {
      $this->base_object_extends = $value;
      
      return $this;
    } // setBaseObjectExtends
    
    /**
     * Return base_manager_extends
     *
     * @return string
     */
    function getBaseManagerExtends() {
    	return $this->base_manager_extends;
    } // getBaseManagerExtends
    
    /**
     * Set base_manager_extends
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setBaseManagerExtends($value) {
      $this->base_manager_extends = $value;
      
      return $this;
    } // setBaseManagerExtends
    
    /**
     * Return type_from_field
     *
     * @return string
     */
    function getTypeFromField() {
    	return $this->type_from_field;
    } // getTypeFromField
    
    /**
     * Set type_from_field
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setTypeFromField($value) {
      $this->type_from_field = $value;
      
      return $this;
    } // setTypeFromField

    /**
     * Return generate permissions flag
     *
     * @return bool
     */
    function getGeneratePermissions() {
      return $this->generate_permissions;
    } // getGeneratePermissions

    /**
     * Set generate permissions
     *
     * @param boolean $value
     * @return AngieFrameworkModelBuilder
     */
    function &setGeneratePermissions($value) {
      $this->generate_permissions = (boolean) $value;

      return $this;
    } // setGeneratePermissions

    /**
     * Return generate URL-s flag
     *
     * @return bool
     */
    function getGenerateUrls() {
      return $this->generate_urls;
    } // getGenerateUrls

    /**
     * Set generate URL-s flag
     *
     * @param boolean $value
     * @return AngieFrameworkModelBuilder
     */
    function &setGenerateUrls($value) {
      $this->generate_urls = (boolean) $value;

      return $this;
    } // setGenerateUrls
    
    /**
     * Return order_by
     *
     * @return string
     */
    function getOrderBy() {
    	return $this->order_by;
    } // getOrderBy
    
    /**
     * Set order_by
     *
     * @param string $value
     * @return AngieFrameworkModelBuilder
     */
    function &setOrderBy($value) {
      $this->order_by = $value;
      
      return $this;
    } // setOrderBy

    /**
     * Return inject into module name
     *
     * @return string
     */
    function getInjectInto() {
      return $this->inject_into;
    } // getInjectInto

    /**
     * Set inject into module name
     *
     * @param $value
     * @return AngieFrameworkModelBuilder
     */
    function &setInjectInto($value) {
      $this->inject_into = $value;

      return $this;
    } // setInjectInto
    
  }