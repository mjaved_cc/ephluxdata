<?php

  /**
   * Base angie theme adapter implementation
   *
   * @package angie.library.application
   */
  class ThemeAdapter {

    /**
     * Properties storage
     *
     * @var array
     */
    private $properties = array(
      'notifications.wrapper.background-color' => '#EFF1E7',
      'notifications.wrapper.font-family' => 'Lucida Grande, Verdana, Arial, Helvetica, sans-serif',
      'notifications.wrapper.font-size' => '11px',
      'notifications.wrapper.color' => '#333',
      'notifications.reply_above_this_line.color' => '#A3A59F',
      'notifications.reply_above_this_line.border-color' => '#D7D8CF',
      'notifications.link.color' => '#950000',
      'notifications.link.text-decoration' => 'underline',
    );

    /**
     * Return property by name
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function getProperty($name, $default = null) {
      if(isset($this->properties[$name])) {
        return $this->properties[$name];
      } else {
        $result = array();

        $name_length = strlen($name);

        foreach($this->properties as $k => $v) {
          if(str_starts_with($k, $name)) {
            $result[] = substr($k, $name_length + 1) . ': ' . $v;
          } // if
        } // foreach

        return count($result) ? implode('; ', $result) . '; ' : $default;
      } // if
    } // getProperty

    /**
     * Set a specific property
     *
     * @param string $name
     * @param mixed $value
     */
    function setProperty($name, $value) {
      if(is_array($value)) {
        foreach($value as $k => $v) {
          $this->properties["$name.$k"] = $v;
        } // foreach
      } else {
        $this->properties[$name] = $value;
      } // if
    } // setProperty

  }