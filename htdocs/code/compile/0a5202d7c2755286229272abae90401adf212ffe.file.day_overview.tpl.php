<?php /* Smarty version Smarty-3.1.7, created on 2012-12-28 11:55:47
         compiled from "/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/modules/system/views/default/homescreen_widgets/day_overview.tpl" */ ?>
<?php /*%%SmartyHeaderCode:210217339550dd88c3b2af94-29524125%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0a5202d7c2755286229272abae90401adf212ffe' => 
    array (
      0 => '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/modules/system/views/default/homescreen_widgets/day_overview.tpl',
      1 => 1356551946,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210217339550dd88c3b2af94-29524125',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'widget_data' => 0,
    'due_today' => 0,
    'active_object' => 0,
    'completed_object' => 0,
    'key' => 0,
    'total_expense' => 0,
    'currency_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_50dd88c8a68fa',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50dd88c8a68fa')) {function content_50dd88c8a68fa($_smarty_tpl) {?><?php if (!is_callable('smarty_function_project_link')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/modules/system/helpers/function.project_link.php';
if (!is_callable('smarty_function_render_priority')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/modules/system/helpers/function.render_priority.php';
if (!is_callable('smarty_function_object_label')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/labels/helpers/function.object_label.php';
if (!is_callable('smarty_function_object_link')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/helpers/function.object_link.php';
if (!is_callable('smarty_function_user_link')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/authentication/helpers/function.user_link.php';
if (!is_callable('smarty_function_due')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/complete/helpers/function.due.php';
if (!is_callable('smarty_block_lang')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/globalization/helpers/block.lang.php';
?><div class="day_overview">
  <?php if (is_foreachable($_smarty_tpl->tpl_vars['widget_data']->value['due_today'])){?>
    <?php if (!$_smarty_tpl->tpl_vars['widget_data']->value['is_overview_in_past']){?>
      <!-- Project-->
      <?php  $_smarty_tpl->tpl_vars['due_today'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['due_today']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['widget_data']->value['due_today']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['due_today']->key => $_smarty_tpl->tpl_vars['due_today']->value){
$_smarty_tpl->tpl_vars['due_today']->_loop = true;
?>
      <table class="common older" cellspacing="0">
        <tr>
          <th class="project" colspan="4"><?php echo smarty_function_project_link(array('project'=>$_smarty_tpl->tpl_vars['due_today']->value['project']),$_smarty_tpl);?>
</th>
        </tr>
        <?php if (is_foreachable($_smarty_tpl->tpl_vars['due_today']->value['objects_active'])){?>
          <!-- Active objects -->
          <?php  $_smarty_tpl->tpl_vars['active_object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['active_object']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['due_today']->value['objects_active']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['active_object']->key => $_smarty_tpl->tpl_vars['active_object']->value){
$_smarty_tpl->tpl_vars['active_object']->_loop = true;
?>
          <tr>
            <td class="label">
              <?php echo smarty_function_render_priority(array('mode'=>'image','priority_id'=>$_smarty_tpl->tpl_vars['active_object']->value->getPriority()),$_smarty_tpl);?>

            <?php if ($_smarty_tpl->tpl_vars['active_object']->value instanceof ILabel){?>
              <?php echo smarty_function_object_label(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value),$_smarty_tpl);?>

            <?php }?>
            </td>
            <td class="object_name">
              <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'quick_view'=>true),$_smarty_tpl);?>

              <?php if ($_smarty_tpl->tpl_vars['widget_data']->value['selected_user']->getId()!==$_smarty_tpl->tpl_vars['active_object']->value->getAssigneeId()){?>
                (Delegated to <?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['active_object']->value->assignees()->getAssignee()),$_smarty_tpl);?>
)
              <?php }?>
            </td>
            <td class="object_type">
                <span class="object_type inverse object_type_<?php echo clean($_smarty_tpl->tpl_vars['active_object']->value->getBaseTypeName(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(),$_smarty_tpl);?>
</span>
            </td>
            <td class="due">
              <?php if (!$_smarty_tpl->tpl_vars['widget_data']->value['is_overview_in_future']){?><?php echo smarty_function_due(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value),$_smarty_tpl);?>
<?php }?>
            </td>
          </tr>
          <?php } ?>
        <?php }?>
        <!-- Completed objects -->
        <?php if (is_foreachable($_smarty_tpl->tpl_vars['due_today']->value['objects_completed'])){?>
          <tr>
            <td class="object_name" colspan="3"> Completed:
          <?php  $_smarty_tpl->tpl_vars['completed_object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['completed_object']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['due_today']->value['objects_completed']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['completed_object']->key => $_smarty_tpl->tpl_vars['completed_object']->value){
$_smarty_tpl->tpl_vars['completed_object']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['completed_object']->key;
?>
            <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['completed_object']->value),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['due_today']->value['objects_completed'][$_smarty_tpl->tpl_vars['key']->value+1])){?>, <?php }?>
          <?php } ?>
            </td>
          </tr>
        <?php }?>
      </table>
      <?php } ?>
    <?php }else{ ?>
      <table class="common older" cellspacing="0">
        <tr>
          <th class="project" colspan="5"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Late<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
        </tr>
      <?php if (is_foreachable($_smarty_tpl->tpl_vars['widget_data']->value['due_today']['objects_active'])){?>
        <?php  $_smarty_tpl->tpl_vars['active_object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['active_object']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['widget_data']->value['due_today']['objects_active']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['active_object']->key => $_smarty_tpl->tpl_vars['active_object']->value){
$_smarty_tpl->tpl_vars['active_object']->_loop = true;
?>
        <tr>
          <td class="label">
            <?php echo smarty_function_render_priority(array('mode'=>'image','priority_id'=>$_smarty_tpl->tpl_vars['active_object']->value->getPriority()),$_smarty_tpl);?>

          <?php if ($_smarty_tpl->tpl_vars['active_object']->value instanceof ILabel){?>
            <?php echo smarty_function_object_label(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value),$_smarty_tpl);?>

          <?php }?>
          </td>
          <td class="object_name">
            <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'quick_view'=>true),$_smarty_tpl);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
in<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_project_link(array('project'=>$_smarty_tpl->tpl_vars['widget_data']->value['projects'][$_smarty_tpl->tpl_vars['active_object']->value->getProjectId()]),$_smarty_tpl);?>

            <?php if ($_smarty_tpl->tpl_vars['widget_data']->value['selected_user']->getId()!==$_smarty_tpl->tpl_vars['active_object']->value->getAssigneeId()){?>
              (Delegated to <?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['active_object']->value->assignees()->getAssignee()),$_smarty_tpl);?>
)
            <?php }?>
          </td>
          <td class="object_type">
              <span class="object_type inverse object_type_<?php echo clean($_smarty_tpl->tpl_vars['active_object']->value->getBaseTypeName(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['active_object']->value->getVerboseType(),$_smarty_tpl);?>
</span>
          </td>
          <td class="due">
            <?php if (!$_smarty_tpl->tpl_vars['widget_data']->value['is_overview_in_future']){?><?php echo smarty_function_due(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value),$_smarty_tpl);?>
<?php }?>
          </td>
        </tr>
        <?php } ?>
      <?php }else{ ?>
        <tr><td><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No tasks are due on this day<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td></tr>
      <?php }?>
      </table>
      <?php if (is_foreachable($_smarty_tpl->tpl_vars['widget_data']->value['due_today']['objects_completed'])){?>
      <table class="common older" cellspacing="0">
        <tr>
          <th class="project" colspan="2"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Completed<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
        </tr>
        <?php  $_smarty_tpl->tpl_vars['completed_object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['completed_object']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['widget_data']->value['due_today']['objects_completed']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['completed_object']->key => $_smarty_tpl->tpl_vars['completed_object']->value){
$_smarty_tpl->tpl_vars['completed_object']->_loop = true;
?>
          <tr>
            <td class="object_type">
              <span class="object_type inverse object_type_<?php echo clean($_smarty_tpl->tpl_vars['completed_object']->value->getBaseTypeName(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['completed_object']->value->getVerboseType(),$_smarty_tpl);?>
</span>
            </td>
            <td class="object_name">
              <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['completed_object']->value),$_smarty_tpl);?>
 in <?php echo smarty_function_project_link(array('project'=>$_smarty_tpl->tpl_vars['widget_data']->value['projects'][$_smarty_tpl->tpl_vars['completed_object']->value->getProjectid()]),$_smarty_tpl);?>

            </td>
          </tr>
        <?php } ?>
      </table>
      <?php }?>
    <?php }?>
  <?php }else{ ?>
    <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No tasks are due on this day<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
  <?php }?>
  
  <?php if (!$_smarty_tpl->tpl_vars['widget_data']->value['is_overview_in_future']&&$_smarty_tpl->tpl_vars['widget_data']->value['timetracking_available']){?>
  <table class="common day_overview_tracking" cellspacing="0">
    <tr>
      <th><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time Records<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
      <th><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
    </tr>
    <tr>
      <td><?php echo clean($_smarty_tpl->tpl_vars['widget_data']->value['total_time'],$_smarty_tpl);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
hours<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
      <td>
      <?php if (is_foreachable($_smarty_tpl->tpl_vars['widget_data']->value['total_expenses'])){?>
        <?php  $_smarty_tpl->tpl_vars['total_expense'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['total_expense']->_loop = false;
 $_smarty_tpl->tpl_vars['currency_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['widget_data']->value['total_expenses']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['total_expense']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['total_expense']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['total_expense']->key => $_smarty_tpl->tpl_vars['total_expense']->value){
$_smarty_tpl->tpl_vars['total_expense']->_loop = true;
 $_smarty_tpl->tpl_vars['currency_id']->value = $_smarty_tpl->tpl_vars['total_expense']->key;
 $_smarty_tpl->tpl_vars['total_expense']->iteration++;
 $_smarty_tpl->tpl_vars['total_expense']->last = $_smarty_tpl->tpl_vars['total_expense']->iteration === $_smarty_tpl->tpl_vars['total_expense']->total;
?>
        <b><?php echo clean($_smarty_tpl->tpl_vars['total_expense']->value,$_smarty_tpl);?>
 <?php echo clean($_smarty_tpl->tpl_vars['widget_data']->value['currencies_map'][$_smarty_tpl->tpl_vars['currency_id']->value]['code'],$_smarty_tpl);?>
<?php if (!$_smarty_tpl->tpl_vars['total_expense']->last){?>,<?php }?></b>
        <?php } ?>
      <?php }else{ ?>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No expenses have been logged<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php }?>
      </td>
    </tr>
  </table>
  <?php }?>
</div><?php }} ?>