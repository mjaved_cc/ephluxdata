<?php /* Smarty version Smarty-3.1.7, created on 2012-12-28 11:38:46
         compiled from "/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/views/default/_require_index_rebuild.tpl" */ ?>
<?php /*%%SmartyHeaderCode:162852646850dd84c6083c47-85590629%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9a0b077d8365c321eb50c21c4b35fda1032973d' => 
    array (
      0 => '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/views/default/_require_index_rebuild.tpl',
      1 => 1356551948,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '162852646850dd84c6083c47-85590629',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_50dd84c6450b3',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50dd84c6450b3')) {function content_50dd84c6450b3($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_lang')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_link')) include '/hermes/bosoraweb060/b2760/whl.ephluxunix/projectierdev/activecollab/3.2.7/angie/frameworks/environment/helpers/block.link.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rebuild Indexes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rebuild Indexes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="require_index_rebuild" class="homescreen_block_message">
  <table>
    <tr>
      <td>
        <p class="homescreen_block_message_title"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Congratulations, upgrade is almost done!<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
        <p class="homescreen_block_message_aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Please click on the button below to complete the process and get back to your home screen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
        <p class="homescreen_block_message_button"><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('indices_admin_rebuild'),'mode'=>'flyout','title'=>"Rebuilding Indexes",'class'=>'link_button_alternative')); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('indices_admin_rebuild'),'mode'=>'flyout','title'=>"Rebuilding Indexes",'class'=>'link_button_alternative'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rebuild Indexes and Complete Upgrade<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('indices_admin_rebuild'),'mode'=>'flyout','title'=>"Rebuilding Indexes",'class'=>'link_button_alternative'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
      </td>
    </tr>
  </table>
</div><?php }} ?>