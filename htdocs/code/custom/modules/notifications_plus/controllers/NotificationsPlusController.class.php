<?php

// We need backend controller
AngieApplication::useController ( 'backend', ENVIRONMENT_FRAMEWORK_INJECT_INTO );

/**
 * 
 * @package custom.modules.notifications_plus
 * @subpackage controllers
 * 
 */

class NotificationsPlusController extends BackendController {
	
	var $object_color_map = array( 'Task' => '#B6D138',
				        		   'Todo List' => '#D1A438',
                                   'Time Record' => '#A7A987',
                                   'Project' => '#EC9BAA',
                                   'user' => '#B6D138',
                                   'TaskComment' => '#66C0DE',
                                   'File'=> '#56BA96',
                                   'Discussion'=> '#5271D0',
                                   'Notebook' => '#92A1D0',
                                   'due_object' => '#950000',
                                   'Milestone'=>'#B05ECB'
								 );
	
	protected $active_module = NOTIFICATION_PLUS_MODULE;
	
	function index() {
		
		if ($this->request->isAsyncCall ()) {
			
			$final_messages = array ();
			if ($this->logged_user instanceof User) {
				
			  $show_notifications = ConfigOptions::getValueFor ( 'show_notifications', $this->logged_user );
				if ($show_notifications) {
					//Online user
					$online_users = NotificationPlus::getOnlineUser ( $this->logged_user );
					if (is_foreachable ( $online_users )) {
						foreach ( $online_users as $online_user ) {
							if ($online_user->getId () != $this->logged_user->getId ()) {
								$message_text = '<a href=' . $online_user->getViewUrl () . '>' . $online_user->getDisplayName () . '</a> is online now';
								$message = array ('message_type' => NOTIFICAITON_TYPE_INFO, 'message_text' => $message_text, 'color_code' => '#B6D138' );
								$final_messages[] =  $message ;
							}
						}
					}
					
					// Due objects
					 $is_due_object_shown =  ConfigOptions::getValueFor ( 'is_due_object_shown', $this->logged_user );
				
					if ($is_due_object_shown) {
						
						$due_objects = ( array ) NotificationPlus::getDueObjects ( $this->logged_user );
						
						foreach ( $due_objects as $due_object ) {
							//send notifications
							$due_on = $due_object->getDueon();
							$due_on_text = '';
							if ($due_on->isToday()) {
								$due_on_text = lang('today');
							} else if ($due_on->isTomorrow()) {
								$due_on_text = lang('tomorrow');
							} else {
								$due_on_text = lang('on') . ' '.$due_on->formatForUser($this->logged_user);
							}
							
							$params = array( 'url' => $due_object->getViewUrl (),
											 'type' => $due_object->getType (),
											 'name' => $due_object->getName (),
											 'due_on' => $due_on_text
							); 
							$message_text = lang('<a href=":url">:type :name is due :due_on</a>', $params);
							$message = array ('message_type' => NOTIFICAITON_TYPE_INFO, 'message_text' => $message_text, 'color_code' => '#FF0000' );
							$final_messages[] =  $message ;
						} //foreach
						ConfigOptions::setValueFor ( 'is_due_object_shown', $this->logged_user, false );
					}
					
					//recent activities
					$notification_enable = ( boolean ) ConfigOptions::getValue ( 'notifications_plus_enabled_activity_updates' );
					
					
					if ($notification_enable) {
						$recent_activities = NotificationPlus::getRecentActivities ( $this->logged_user );
						if (is_foreachable ( $recent_activities )) {
							foreach ( $recent_activities as $recent_activity ) {
								
								$params = array( 'url' => $recent_activity ['url'],
												 'type' => $recent_activity ['type'],
												 'name' => $recent_activity ['name'],
												 'action' => $recent_activity ['action'],
												 'author' => $recent_activity ['created_by_name'],
								); 
								$message_text = lang('<a href=":url">:type :name :action by :author</a>', $params);
								$message = array ('message_type' => NOTIFICAITON_TYPE_INFO, 'message_text' => $message_text, 'color_code' => $this->object_color_map[$recent_activity ['type']] );
							        $final_messages[] =  $message ;
							} //foreach
						} //if
					}
					
					// Find all notification from the queue
					$notifications = NotificationsPlusQueueItems::findBySQL ( 'SELECT * FROM ' . TABLE_PREFIX . 'notifications_queue WHERE `seen` = 0 AND `user_id` = '.$this->logged_user->getId() );
					if ($notifications instanceof DBResult && $notifications->count() > 0) {
						foreach ( $notifications as $notification ) {
							$message = array ('message_type' => $notification->getMessageType (), 
                            				  'message_text' => $notification->getMessage (), 
                                              'send_via_email' => $notification->getSendViaEmail (),
                                              'color_code' => $notification->getAdditionalProperty('color_code') 
                                             );
							$final_messages[] =  $message ;
                            $ids[] = $notification->getId();
						}
						
						//Remove Notifications after displaying
						NotificationsPlusQueueItems::delete(array('id in ( ? )', $ids));
					}
					ConfigOptions::setValueFor ( 'recent_activity_time', $this->logged_user, gmdate ( DATETIME_MYSQL, time () ) );
				}
				$this->response->respondWithData ( $final_messages, array ('format' => FORMAT_JSON ) );
			} else {
				$this->response->badRequest ();
			} //if
		}	
	} //index
	

	function mute_notifications() {
		if ($this->request->isAsyncCall ()) {
			$show_notifications = ConfigOptions::getValueFor ( 'show_notifications', $this->logged_user, false );
			ConfigOptions::setValueFor ( 'show_notifications', $this->logged_user, ! $show_notifications );
			$show_notifications = $show_notifications ? 'false' : 'true';
			$this->wireframe->javascriptAssign ( 'show_notifications', $show_notifications );
			$this->response->respondWithData ( $show_notifications );
		} else {
			$this->response->badRequest ();
		}
	} //mute_notifications
}