<?php


  /**
   * BaseNotificationsPlus class
   *
   * @package ActiveCollab.modules.system
   * @subpackage models
   */
  class NotificationsPlusQueueItem extends ApplicationObject {
  
    /**
     * Name of the table where records are stored
     *
     * @var string
     */
    protected $table_name = 'notifications_queue';
    
    /**
     * All table fields
     *
     * @var array
     */
    protected $fields = array('id', 'message_type', 'message', 'module','seen','send_via_email', 'raw_additional_properties');
    
    /**
     * Primary key fields
     *
     * @var array
     */
    protected $primary_key = array('id');
    
    /**
     * Name of AI field (if any)
     *
     * @var string
     */
    protected $auto_increment = 'id';
    

    /**
     * Return value of id field
     *
     * @return integer
     */
    function getId() {
      return $this->getFieldValue('id');
    } // getId
    
    /**
     * Set value of id field
     *
     * @param integer $value
     * @return integer
     */
    function setId($value) {
      return $this->setFieldValue('id', $value);
    } // setId

    /**
     * Return value of message type field
     *
     * @return string
     */
    function getMessageType() {
      return $this->getFieldValue('message_type');
    } // getMessageType
    
    /**
     * Set value of message type field
     *
     * @param string $value
     * @return string
     */
    function setMessageType($value) {
      return $this->setFieldValue('message_type', $value);
    } // setMessageType

    /**
     * Return value of Message field
     *
     * @return string
     */
    function getMessage() {
      return $this->getFieldValue('message');
    } // getMessage
    /**
     * Set value of string
     *
     * @param string $value
     * @return string
     */
    function setMessage($value) {
      return $this->setFieldValue('message', $value);
    } // setMessage

    /**
     * Return value of Module field
     *
     * @return string
     */
    function getModule() {
      return $this->getFieldValue('module');
    } // getModule
    
    /**
     * Set value of Module field
     *
     *  @param string $value
     * @return string
     */
    function setModule($value) {
      return $this->setFieldValue('module', $value);
    } // setModule
    /**
     * Return value of seen field
     *
     * @return boolean
     */
    function getSeen() {
    	return $this->getFieldValue('seen');
    } // getModule
    
     
    /**
     * Set value of seen field
     *
     *  @param boolean $value
     * @return boolean
     */
    function setSeen($value) {
    	return $this->setFieldValue('seen', $value);
    } // setModule
    /**
     * Return value of seen field
     *
     * @return boolean
     */
    function getSendViaEmail() {
    	return $this->getFieldValue('send_via_email');
    } // getModule
    
     
    /**
     * Set value of seen field
     *
     *  @param boolean $value
     * @return boolean
     */
    function setSendViaEmail($value) {
    	return $this->setFieldValue('send_via_email', $value);
    } // setModule

   

    /**
     * Return value of raw_additional_properties field
     *
     * @return string
     */
    function getRawAdditionalProperties() {
      return $this->getFieldValue('raw_additional_properties');
    } // getRawAdditionalProperties
    
    /**
     * Set value of raw_additional_properties field
     *
     * @param string $value
     * @return string
     */
    function setRawAdditionalProperties($value) {
      return $this->setFieldValue('raw_additional_properties', $value);
    } // setRawAdditionalProperties

    /**
     * Set value of specific field
     *
     * @param string $name
     * @param mided $value
     * @return mixed
     */
    function setFieldValue($name, $value) {
      switch($real_name = $this->realFieldName($name)) {
        case 'id':
          return parent::setFieldValue($real_name, intval($value));
        case 'message_type':
          return parent::setFieldValue($real_name, strval($value));
        case 'message':
          return parent::setFieldValue($real_name, strval($value));
        case 'module':
          return parent::setFieldValue($real_name,strval($value));
        case 'seen':
          return parent::setFieldValue($real_name, boolval($value));
       	case 'send_via_email':
          return parent::setFieldValue($real_name, boolval($value));
        case 'raw_additional_properties':
          return parent::setFieldValue($real_name, strval($value));
      } // switch
      
      throw new InvalidParamError('name', $name, "Field $name (maps to $real_name) does not exist in this table");
    } // switch
  
  }

