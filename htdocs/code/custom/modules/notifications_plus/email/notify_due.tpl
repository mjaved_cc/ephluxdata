[{$project_name}] {lang object_name=$object_name language=$recipient->getLanguage()}Reminder for {$object_type} - ':object_name' is due soon{/lang}
================================================================================
{notification_inspector context=$context recipient=$recipient sender=$sender}

<p>This is a friendly automatic reminder. {$object_type} '{$object_name nofilter}' in '{$project_name nofilter}' project is assigned to you and is <strong>due {$due_on nofilter}.</strong></p>