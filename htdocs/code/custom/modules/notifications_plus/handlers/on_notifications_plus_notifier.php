<?php

/**
 *on_notifications_plus_notifier event handler
 * 
 * @package custom.modules.notifications_plus
 * @subpackage handlers

 **
 * Handle on_notifications_plus_notifier event
 * 
 * @param array $messages
 * 
 * $message = array('message_type', 'message', 'send_via_email', 'user_ids' => array(), 'additional_properties' => array('color_code'))
 * @param IUser $user
 */
function notifications_plus_handle_on_notifications_plus_notifier(&$messages) {
    DB::beginWork ( 'Inserting Messages @ ' . __CLASS__ );
	$batch = DB::batchInsert ( TABLE_PREFIX . 'notifications_queue', array ('message_type', 'message', 'send_via_email', 'user_id', 'raw_additional_properties' ) );
	if (is_foreachable ( $messages )) {
		foreach ( $messages as $message ) {
			if (is_foreachable ( $message ['notify_user_ids'] )) {
				foreach ( $message ['notify_user_ids'] as $user_id ) {
					$batch->insert ( $message ['message_type'], $message ['message_text'], $message ['send_via_email'], $user_id, serialize ( $message ['additional_properties'] ) );
				}
            }
		}
		$batch->done ();
	}
     DB::commit ( 'Notification messages  has been Inserted @ ' . __CLASS__ );
} // on_notifications_plus_handle_notifications_plus_notifier