<?php return array (

  'Enable activity update notifications',
  'Email Reminders',
  'Enable email reminders for due dates',
  'Send notification on the due date',
  'Send notifications in advance',
  'No advance notification',
  'days before due date',
  'days before due date',
  'When should the notifications be sent (Hour of day in UTC)',
  'Current UTC time is',
  'Notify only Assignees from Owner Company',
  'Notify only Responsible assignee',
  'What items to send notifications for',
  'Activity Updates'
  
); ?>