<?php

  // Include applicaiton specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';
	
  /**
   * NotificationPlus module model definition
   *
   * @package custom.modules.notifications_plus
   * @subpackage resources
   */
  class NotificationsPlusModuleModel extends ActiveCollabModuleModel {
  
    /**
     * Construct NotificationPlus module model definition
     *
     * @param NotificationPlusModuleModel $parent
     */
  function __construct(NotificationsPlusModule $parent) {
      parent::__construct($parent);
      if(!DB::tableExists(TABLE_PREFIX . 'notifications_queue')) {
	     $this->addModel(DB::createTable('notifications_queue')->addColumns(array(
	        DBIdColumn::create(), 
	        DBStringColumn::create('message_type', 50),
	        DBStringColumn::create('message', 255),
	        DBIntegerColumn::create('user_id', 10), 
	        DBStringColumn::create('module', 50),
	        DBBoolColumn::create('seen'),
	        DBBoolColumn::create('send_via_email',false),
	        DBAdditionalPropertiesColumn::create(),
	        )
	     ));
     }   
    }
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
	    $this->addConfigOption('recent_activity_time', gmdate(DATETIME_MYSQL));
	    $this->addConfigOption('show_notifications', true);
	    $this->addConfigOption('is_due_object_shown', true);
	    
	    $this->addConfigOption('notifications_plus_enabled_activity_updates', true);
	    $this->addConfigOption('notifications_plus_enabled_due_dates_email', true);
      	$this->addConfigOption('notifications_plus_days_to_due', false);
      	$this->addConfigOption('notifications_plus_notify_today', true);
      	$this->addConfigOption('notifications_plus_notify_owner_company', true);
      	$this->addConfigOption('notifications_plus_notify_responsible', true);
      	$this->addConfigOption('notifications_plus_when_to_send', 9);
      	$this->addConfigOption('notifications_plus_notify_types', array('Milestone'=>1, 'Task'=>1, 'SubTask' => 1));
      
	    parent::loadInitialData($environment);
    }
    
  }
  