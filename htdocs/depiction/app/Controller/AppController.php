<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

// datatypes
App::uses('DtUser', 'Lib/DataTypes');
App::uses('DtDate', 'Lib/DataTypes');
App::uses('DtUserSocialAccount', 'Lib/DataTypes');
App::uses('DtFeature', 'Lib/DataTypes');
App::uses('DtAuthActionMap', 'Lib/DataTypes');
App::uses('DtGroup', 'Lib/DataTypes');
App::uses('DtAco', 'Lib/DataTypes');
App::uses('DtArosAco', 'Lib/DataTypes');

// Folder & File API
App::uses('File', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array('Session', 'Auth', 'Acl', 'App', 'JCManager', 'RequestHandler');
	public $uses = array('AuthActionMap');
	public $helpers = array('Misc', 'Minify.Minify');
	public $error_msg, $error_code, $user_id;

	/**
	 * handler for action mapper file
	 */
	protected static $_am_handler;

	function beforeFilter() {

		$this->Auth->allow('signup', 'twitter_connect', 'login_with_twitter', 'facebook_connect', 'twitter_authorize', 'login_with_facebook', 'verify_account', 'user_login', 'forgot_password', 'reset_account_password', 'logout');

		$this->Auth->loginAction = array('controller' => 'demos', 'action' => 'login', 'plugin' => false);
		$this->Auth->loginRedirect = array('controller' => 'demos', 'action' => 'secure');
		$this->Auth->logoutRedirect = array('controller' => 'demos', 'action' => 'secure');
		$this->Auth->loginError = 'The username or password you entered is incorrect';

		// Add a pattern value detector.
		// $this->request->addDetector('iphone', array('env' => 'HTTP_USER_AGENT', 'pattern' => '/iPhone/i'));

		$this->Auth->authenticate = array(
			'Form' => array(
				'scope' => array('User.status' => 'active')
			)
		);

		// without hashing Password (cakephp default ) and use only md5
		Security::setHash("md5");

		$this->_check_access();

		$this->user_id = $this->Auth->User('id');

		$this->set('is_loggedin', $this->Auth->User());
	}

	function beforeRender() {

		// file name for indivisual views used for JS & CSS
		$file_name = $this->name . '/' . $this->request->params['action'];
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$css_path = CSS_URL . $file_name . EXTENSION_CSS;
		$action_js = new File($js_path);
		$action_css = new File($css_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);
		if ($action_css->exists())
			$this->JCManager->add_css($file_name);

		list($js_list, $css_list) = $this->JCManager->get();

		$this->set('js_list', $js_list);
		$this->set('css_list', $css_list);
	}

	/**
	 * setup the Auth component to use the 'crud' method, which validates the requested 
	 * 
	 * @return boolean
	 */
	private function _setup_auth() {
		if (isset($this->Auth)) {

			$action_mapppers = $this->_get_action_mappers();

			$this->Auth->authorize = 'Crud';
			$this->Auth->actionMap = $action_mapppers;

			return true;
		}

		return false;
	}

	/**
	 * Get all action mappers. This methods uses cache technique. It stores required result in file to 
	 * 
	 * reduce DB hit. File is used because cache itself have some expiry & will then removed. But we treat this file especially 
	 * 
	 * as cache but with lifetime expiry unless remove. 
	 * 
	 * @return array
	 * @access private
	 */
	private function _get_action_mappers() {
		return $this->_file_as_cache();
	}

	/**
	 * Read results from file & act as cache
	 * 
	 * @return array
	 * @access private
	 */
	private function _file_as_cache() {

		$file = self::_get_am_handler();
		// if exists so read only else create
		if ($file->exists()) {
			$json = $file->read(true, 'r');
			return $this->App->decoding_json($json);
		} else {
			$mapping_info = $this->{$this->modelClass}->get_action_maps();
			$action_mapppers = array();

			if (!empty($mapping_info)) {
				foreach ($mapping_info as $value) {
					$action_mapppers[$value['Controller']['alias']][$value['Action']['alias']] = $value['AuthActionMap']['crud'];
				}

				$json = $this->App->encoding_json($action_mapppers);

				$file->create();
				if ($file->write($json))
					return $action_mapppers;
			}
		}

		$file->close();

		// in case of any failure
		return array();
	}

	/**
	 * Get instance of File.
	 * 
	 * @uses singleton Design Pattern
	 * @return resource handler of action mapper
	 */
	protected static function _get_am_handler() {

		if (self::$_am_handler == null) {
			self::$_am_handler = new File(ACTION_MAPPER_FILE);
		}

		return self::$_am_handler;
	}

	/**
	 * Check if user is allowed for requested action
	 * 
	 * @link http://stackoverflow.com/questions/54230/cakephp-acl-database-setup-aro-aco-structure ACL implementation guide
	 */
	private function _check_access() {

		if ($this->Auth->User('id') && $this->_setup_auth()) {

			if (isset($this->Auth->actionMap[$this->name][$this->request->params['action']]))
				$action = $this->Auth->actionMap[$this->name][$this->request->params['action']];

			if (!empty($action)) {

				$aro = ARO_ALIAS . $this->Auth->User('group_id');
				// action based ACL based 
				$valid = $this->Acl->check(
						$aro, $this->name . '/' . $this->request->params['action'] // , $action
				);

				// check this user-level aro for access
				if ($valid) {
					$this->Auth->allow();
				} else {
					$this->Auth->authorize = 'Controller';
					// remove all the actions.
					$this->Auth->deny();
				}
			} else {
				$this->Auth->authorize = 'Controller';
				// remove all the actions.
				$this->Auth->deny();
			}
		}
	}

	public function isAuthorized($user = null) {

		if (!empty($user) && $user['group_id'] == GROUP_ID_SUPER_ADMIN)
			return true;
		else
			$this->Auth->logout();
		// Default deny
		return false;
	}

	/*	 * ***** END: How to save IP Address in DB ***** */

	function get_error_msg($error_code) {
		switch ($error_code) {
			case ERR_CODE_SUCCESS:
				return 'Success!';
			case ERR_CODE_FAILURE:
				return 'Something went wrong. Please try again later.';
			case ERR_CODE_INVALID_PARAMS:
				return 'Invalid parameters.';
			case ERR_CODE_INVALID_EMAIL:
				return 'Invalid Email Address.';
			case ERR_CODE_EMAIL_ALREADY_EXISTS:
				return 'Email Already Exist.';
			case ERR_CODE_UNVERIFIED_EMAIL:
				return 'We have found an unverified email. Please check your email.';
			case ERR_CODE_INVALID_VCODE:
				return 'Verification code is invalid.';
			case ERR_CODE_UNVERIFIED_EMAIL:
				return 'Account already activated.';
			case ERR_CODE_USERNAME_ALREADY_EXISTS:
				return 'Username already activated.';
		}
	}
	
	function is_valid_email($email = null) {
		if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
			return true;
		return false;
	}

	function get_current_datetime() {
		return date('Y-m-d H:i:s');
	}

	/*
	 * @desc: get expiry date from expired_at field of Tmp table
	 */

	function get_tmp_expiry() {
		return date('Y-m-d', strtotime("+3 month"));
	}

	function send_email($to, $subject, $message, $template = 'default', $layout = 'default') {

		$email = new CakeEmail('dev');
		$email->to($to);
		$email->template($template, $layout);
		$email->emailFormat('html');
		$email->viewVars(array('message' => $message));
		$email->subject($subject);
		$email->send();
	}

	/*	 * ***** How to save IP Address in DB ****** */
	/* INET_NTOA and INET_ATON functions in mysql. They convert between dotted notation IP address to 32 bit integers. This allows you to store the IP in just 4 bytes rather than a 15 bytes 
	 */

	/*
	  @desc: get an (IPv4) Internet network address into a string in Internet standard dotted format.
	  @param: number <string>
	  @return dotted ip address <string>
	 */

	function get_ip_address($number) {
		// analogous to INET_NTOA in mysql
		return sprintf("%s", long2ip($number));
	}

	/*
	  @desc: get string containing an (IPv4) Internet Protocol dotted address into a proper address
	  @param: null
	  @return number <unsigned-int>
	 */

	function get_numeric_ip_representation() {
		// analogous to INET_ATON in mysql
		return sprintf("%u", ip2long($_SERVER['REMOTE_ADDR']));
	}

}
