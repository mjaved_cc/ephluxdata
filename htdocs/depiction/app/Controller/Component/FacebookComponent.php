<?php

App::uses('Component', 'Controller');
App::uses('Facebook', 'Lib/Facebook');

class FacebookComponent extends Component {

	public $components = array('App');
	private $_key = null;
	private $_secret = null;
	private $_limit; // limit is for fetching records.	
	private $_params;
	private $_redirect_uri;
	private $_scope = null;
	private $_controller;
	private $_facebook_obj = null;

	function startup(Controller $controller) {

		Configure::load('facebook');

		$this->_controller = $controller;

		$this->_scope = 'publish_stream, offline_access, email, read_stream';
		$this->_key = Configure::read('Facebook.APP_ID');
		$this->_secret = Configure::read('Facebook.API_KEY');
		$this->_limit = 300;

		$this->_facebook_obj = new Facebook(array('appId' => $this->_key, 'secret' => $this->_secret));

		$this->_redirect_uri = CAKEPHP_FRAMEWORK_URI . '/demos/facebook_connect';

		$this->_params = array(
			'scope' => $this->_scope,
			'redirect_uri' => $this->_redirect_uri,
			'display' => 'popup'
		);
	}

	function getUserDetails($token) {

		try {
			$this->_facebook_obj->setAccessToken($token);
			return $this->getDataFromFeed('/me?fields=picture,username,email,first_name,last_name');
		} catch (FacebookApiException $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}

	function getFbLoginurl() {
		try {
			return $this->_facebook_obj->getLoginUrl($this->_params);
		} catch (FacebookApiException $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}

	function getAccessToken() {
		try {
			return $this->_facebook_obj->getAccessToken();
		} catch (Exception $e) {
			$this->log($e->getCode() . ' ' . $e->getMessage(), 'social');
		}
	}

	function getDataFromFeed($url) {

		$feeds = $this->_facebook_obj->api($url);
		return $feeds;
	}

	function logout() {
		$params = array('next' => CAKEPHP_FRAMEWORK_URI . '/demos/logout');
		$logout = $this->_facebook_obj->getLogoutUrl($params);
		return $logout;
	}

}

?>