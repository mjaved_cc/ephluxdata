<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class ApiController extends AppController {
	
		public $uses = array('User', 'Tmp');

	function beforeFilter() {
//		parent::beforeFilter();
		$this->Auth->allow();
	}

	function users_index() {
		$data['a'] = 111;
		$data['b'] = 111;
		$a = json_encode($data);
		$this->set('data', $a);
	}

	function users_signup() {

		// http://book.cakephp.org/2.0/en/models/saving-your-data.html

		if ($this->request->is('post') && !empty($this->request->data['signup_data'])) {
			
			$json_data = $this->request->data['signup_data'];  
			$data = json_decode($json_data, true); 
			$file = array();
			$first_name = $data['first_name']; 
			$last_name = $data['last_name'];
			$email = $data['email'];
			$password = AuthComponent::password($data['password']);
			$username = $data['username'];
			$image_url = '';


			if (!$this->is_valid_email($email))
				$error_code = ERR_CODE_INVALID_EMAIL;
			else if ($this->User->is_email_already_exists($email))
				$error_code = ERR_CODE_EMAIL_ALREADY_EXISTS;
			else if ($this->User->is_username_already_exists($username))
				$error_code = ERR_CODE_USERNAME_ALREADY_EXISTS;
			else if ($this->Tmp->is_unverified_email_exists($email))
				$error_code = ERR_CODE_UNVERIFIED_EMAIL;
			else {

				if (!empty($_FILES['file'])) {
					$file = $_FILES['file'];



					if (is_uploaded_file($file['tmp_name'])) {

						$image_url = $this->saveToFile($file, IMAGE_PROFILE); //file url
					}
				}

				$user_info['username'] = $username;
				$user_info['password'] = AuthComponent::password($password);
				$user_info['ip'] = $this->get_numeric_ip_representation();
				$user_info['email'] = $email;
				$user_info['first_name'] = $first_name;
				$user_info['last_name'] = $last_name;
				$user_info['url'] = $image_url;
				$this->Tmp->create();
				$this->Tmp->set('target_id', $email);
				$this->Tmp->set('data', json_encode($user_info));
				$this->Tmp->set('type', 'registration');
				$this->Tmp->set('expire_at', $this->get_tmp_expiry());

				if ($this->Tmp->save()) {

					$signup_link = $this->_get_signup_link($email);

					$this->send_email($email, SITE_NAME . ': Account Activation', $signup_link, 'signup');

					$error_code = ERR_CODE_SUCCESS;
				} else
					$error_code = ERR_CODE_FAILURE;
			}
		} else
			$error_code = ERR_CODE_INVALID_PARAMS;

		$error_msg = $this->get_error_msg($error_code);

		$this->set(compact('error_code', 'error_msg'));
	}
	
		/*
	 * @desc: get signup link for email verification
	 * @params: email <string>
	 * @return: signup_link <string>
	 */

	function _get_signup_link($email) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		$verification_code = substr(md5($code), 8, 5);

		$signup_link = DEPECTION_BASE_URI . '/' . $this->params['controller'] . '/' . "verify_account?v=$verification_code&email=" . $email;
		return $signup_link;
	}

}