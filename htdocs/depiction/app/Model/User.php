<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class User extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'username';
	var $name = 'User';
	var $belongsTo = array(
		'Group'
	);
	public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));
	var $hasMany = array(
		'UserSocialAccount'
	);
	var $validate = array(
		'first_name' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minLength', 3),
				'message' => 'First Name must have atleast 3 characters',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Firstname must only contain letters and numbers.'
			)
		),
		'last_name' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minLength', 3),
				'message' => 'Last Name must have atleast 3 characters',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Lastname must only contain letters and numbers.'
			)
		),
		'password' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minlength', 8),
				'message' => 'Password must be between 8 to 15 characters'
			)
		),
		'username' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please supply a valid email address.',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'isUnique',
				'message' => 'Email already exists'
			)
		)
	);

	/**
	 * authorizes user credentials
	 * @param varchar $email email address
	 * @param varchar $password password
	 */
	function verify_login($email, $password) {

		$result = $this->query('call verify_login("' . $email . '", "' . $password . '")');

		if (!empty($result))
			return $result['0'];

		return false;
	}

	function get_details_by_id($id) {
		$result = $this->query('call get_user_details_by_user_id("' . $id . '")');

		return $result;
	}

//	function verify_email($email) {
//
//		$verify = $this->find('count', array(
//			'conditions' => array('username' => $email)
//				));
//		return $verify;
//	}
//	function get_user_info($email) {
//		$user_info = $this->find('first', array('conditions' => array('username' => $email)));
//		return $user_info;
//	}

	function reset_password($password, $user_id) {

		$this->query('call reset_password(
			"' . $password . '",
			"' . $user_id . '",
			@status )');

		return $this->get_status();
	}

	/**
	 * Get details of users by email
	 * @param array $query_params WHERE clause
	 * @return array result
	 */
	function get_by_email($email) {

		$result = $this->query('call get_user_by_email("' . $email . '")');

		if (!empty($result))
			return $result['0'];

		return false;
	}

	/**
	 * Create new user for given params
	 * 
	 * @param array $user_details User details
	 * @return type Description
	 */
	function create_user($user_details) {
		$this->query('call create_user(
			"' . $user_details['first_name'] . '", 
			"' . $user_details['last_name'] . '", 
			"' . $user_details['username'] . '", 
			"' . $user_details['password'] . '",
			"' . $user_details['group_id'] . '",
			"' . $user_details['created'] . '",
			"' . $user_details['ip_address'] . '",
			@status )');

		return $this->get_status();
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['User']['password']))
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		return true;
	}

	/**
	 * This is for Group only ACL.
	 * 
	 * @link http://stackoverflow.com/questions/6154285/aros-table-in-cakephp-is-still-including-users-even-after-bindnode
	 * 
	 * @param array $user User details
	 * @return array Group info
	 */
	function bindNode($user) {
		return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}

	public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['User']['group_id'])) {
			$groupId = $this->data['User']['group_id'];
		} else {
			$groupId = $this->field('group_id');
		}
		if (!$groupId) {
			return null;
		} else {
			return array('Group' => array('id' => $groupId));
		}
	}

	/**
	 * Signup facebook. Save details in User & UserSocialAcount tables.
	 * 
	 * @param xml $xml_data user details
	 */
	function signup_with_social_account($xml_data) { 
		
		$this->query("call signup_with_social_account(
			 '". $xml_data . "',
			@status )");

		return $this->get_status();
	}
	
	/***********************************Functions copied from old depiction User Model***************************/
	
	function is_email_already_exists($email) {
		$user = $this->find('first', array(
			'conditions' => array('User.status' => USER_STATUS_ACTIVE, 'User.email' => $email)
				));

		return (!empty($user['User']['id'])) ? $user['User']['id'] : 0;
	}
	
	function is_username_already_exists($username) {
		$user = $this->find('first', array(
			'conditions' => array('User.status' => USER_STATUS_ACTIVE, 'User.username' => $username)
				));

		return (!empty($user['User']['id'])) ? $user['User']['id'] : 0;
	}

	/*
	 * @desc: get details of users
	 * @param: query_params <array>
	 * @return: <array>
	 */

	function get_details($query_params = 0) {

		$clause['User.status'] = USER_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		return $this->find('first', array(
					'conditions' => array($clause
					)
				));
	}	
	
	
	function get_user_info($uid=0){
		if(is_numeric($uid)){
			$result = $this->query("SELECT * from users as user where id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_user_posts($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT * from user_assets posts where user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
	function get_followers_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS followers_count FROM user_followerships WHERE target_id={$uid}");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
 
	
	function get_following_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS following_count FROM user_followerships WHERE user_id={$uid}");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_liked_posts($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT posts.* FROM user_assets AS posts
INNER JOIN  (SELECT user_assets_id FROM user_assets_likes WHERE user_id={$uid}) AS likes ON likes.user_assets_id=posts.id;");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
		function get_likes_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS likes_count FROM user_assets_likes  WHERE user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_followers($uid=0,$type){
		if(is_numeric($uid)&& $type=='followers'){
			$result =$this->query("SELECT `users`.* FROM users AS `users`
INNER JOIN  (SELECT target_id,user_id FROM user_followerships WHERE target_id={$uid}) AS followers ON followers.user_id=`users`.id;");
			if(!empty($result)){
				return $result;
			}
			}
			
			
		if(is_numeric($uid)&& $type=='following'){
			$result =$this->query("SELECT target_id FROM user_followerships WHERE user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}	
	
			return 0;
	}
	
		function get_following($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT `users`.* FROM users AS `users`
INNER JOIN  (SELECT target_id FROM user_followerships WHERE user_id={$uid}) AS following ON following.target_id=`users`.id;");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
	function search_user($uid=0,$search_data=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT `USERS`.username,`USERS`.first_name,`USERS`.last_name,`USERS`.url,`USERS`.id,if(follower.target_id > 0, 'TRUE','FALSE')as following 
from users `USERS`
left join (SELECT target_id,user_id 
		FROM user_followerships 
		WHERE user_id={$uid}) as follower  on `USERS`.id=follower.target_id
WHERE	BINARY `USERS`.first_name LIKE '{$search_data}%' OR 
	BINARY `USERS`.last_name LIKE '{$search_data}%' OR 
	BINARY `USERS`.username LIKE '{$search_data}%';");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}

}
