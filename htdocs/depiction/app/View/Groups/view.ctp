<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">						
				<ul class="bulleted_list">
					<li><?php echo $this->Html->link(__('Edit Group'), array('action' => 'edit', $group['Group']['id'])); ?> </li>
					<li><?php echo $this->Form->postLink(__('Delete Group'), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?> </li>
					<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?> </li>
					<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>

				</ul>
		</div>
	</div>

</div>


<div class="main_column">
	<div class="box">
		<div class="body">

			<h2><?php echo __('Group'); ?></h2>

			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
				<thead>
					<tr>
						<th><?php echo __('Name'); ?></th>
						<th><?php echo __('Created'); ?></th>

					</tr>
				</thead>

				<tbody>
					<tr>
						<td><?php echo h($group['Group']['name']); ?></td>
						<td><?php echo h($group['Group']['created']); ?></td>

					</tr>
				</tbody>
			</table>

			<h3><?php echo __('Related Users'); ?></h3>

			<?php if (!empty($group['User'])): ?>
				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
					<thead>
						<tr>
							<th><?php echo __('First Name'); ?></th>
							<th><?php echo __('Last Name'); ?></th>
							<th><?php echo __('Username'); ?></th>							
							<th><?php echo __('Created'); ?></th>					
							<th><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php						
						foreach ($group['User'] as $user):
							?>						
							<tr>

								<td><?php echo $user['first_name']; ?></td>
								<td><?php echo $user['last_name']; ?></td>
								<td><?php echo $user['username']; ?></td>				
								<td><?php echo $user['created']; ?></td>					
								<td >
									<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
								</td>
							</tr>

						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</div>




