<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">
			<div class="actions">				
				<ul>
					<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
					<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>					

				</ul>
			</div>
		</div>
	</div>

</div>


<div class="main_column">
	<div class="box">
		<div class="body">

			<?php echo $this->Form->create('User'); ?>
			<fieldset>
				<legend><?php echo __('Edit User'); ?></legend>

				<?php echo $this->Form->input('id'); ?> 

				<?php echo $this->Form->input('username', array('class' => 'textfield large')); ?>

				<p><?php echo $this->Form->input('group_id', array('class' => 'textfield large')); ?></p>

				<?php echo $this->Form->submit('Submit', array('class' => 'button2')); ?>
			</fieldset>


		</div>
	</div>
</div>