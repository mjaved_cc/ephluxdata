<?php
App::uses('Sanitize', 'Utility');
class DtContent {
	public $content;
	
	function __construct($content){
		$this->content = $content;
	}	
	
	function htmlContent() {
		return $this->content;	
	}
	
	function textConent() {
		return Sanitize::html($this->content,array('remove' => true));
	}
}
?>