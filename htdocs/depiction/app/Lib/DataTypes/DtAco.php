<?php

class DtAco {

	private $_fields = array();
	private $_allowed_keys = array('id', 'alias');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		$this->_fields[$key] = $value;
//		switch ($key) {			
//			default:
//				$this->_fields[$key] = $value;
//		}
	}

	function __get($key) {

		try {
			if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'alias' => $this->alias
		);
	}

}

?>
