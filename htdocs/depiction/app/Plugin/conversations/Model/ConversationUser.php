<?php
App::uses('Conversations.ConversationsAppModel', 'Model');
App::uses('User', 'Model');
/**
 * ConversationUser Model
 *
 * @property Conversation $Conversation
 * @property User $User
 */
class ConversationUser extends ConversationsAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 		
	public $belongsTo = array(
		'Conversation' => array(
			'className' => 'Conversation',
			'foreignKey' => 'conversation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
