<div class="conversations form">
<?php echo $this->Form->create('Conversation'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Conversation'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('title');
		echo $this->Form->input('last_message_id');
		echo $this->Form->input('allow_add');
		echo $this->Form->input('count');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Conversations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversation Messages'), array('controller' => 'conversation_messages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Last Message'), array('controller' => 'conversation_messages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversation Users'), array('controller' => 'conversation_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation User'), array('controller' => 'conversation_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
