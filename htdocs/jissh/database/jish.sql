/*
SQLyog Enterprise - MySQL GUI v5.17
Host - 5.5.28-0ubuntu0.12.04.3 : Database - jish
*********************************************************************
Server version : 5.5.28-0ubuntu0.12.04.3
*/

SET NAMES utf8;

SET SQL_MODE='';

create database if not exists `jish`;

USE `jish`;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=latin1;

/*Data for the table `acos` */

insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,NULL,NULL,'controllers',1,520),(2,1,NULL,NULL,'AuthActionMaps',2,17),(3,2,NULL,NULL,'index',3,4),(4,2,NULL,NULL,'view',5,6),(5,2,NULL,NULL,'add',7,8),(6,2,NULL,NULL,'edit',9,10),(7,2,NULL,NULL,'delete',11,12),(16,2,NULL,NULL,'sync_action_maps',13,14),(17,1,NULL,NULL,'Demos',18,45),(21,17,NULL,NULL,'facebook_connect',19,20),(25,17,NULL,NULL,'twitter_connect',21,22),(27,17,NULL,NULL,'signup',23,24),(28,17,NULL,NULL,'verify_account',25,26),(30,17,NULL,NULL,'forgot_password',27,28),(31,17,NULL,NULL,'reset_account_password',29,30),(40,1,NULL,NULL,'Groups',46,61),(41,40,NULL,NULL,'index',47,48),(42,40,NULL,NULL,'view',49,50),(43,40,NULL,NULL,'add',51,52),(44,40,NULL,NULL,'edit',53,54),(45,40,NULL,NULL,'delete',55,56),(54,1,NULL,NULL,'Users',62,75),(55,54,NULL,NULL,'index',63,64),(56,54,NULL,NULL,'view',65,66),(57,54,NULL,NULL,'add',67,68),(58,54,NULL,NULL,'edit',69,70),(59,54,NULL,NULL,'delete',71,72),(82,1,NULL,NULL,'AclExtras',76,77),(83,1,NULL,NULL,'Minify',78,83),(84,83,NULL,NULL,'Minify',79,82),(85,84,NULL,NULL,'index',80,81),(86,17,NULL,NULL,'index',31,32),(87,17,NULL,NULL,'secure',33,34),(88,2,NULL,NULL,'isAuthorized',15,16),(89,17,NULL,NULL,'isAuthorized',35,36),(90,40,NULL,NULL,'isAuthorized',57,58),(91,54,NULL,NULL,'isAuthorized',73,74),(92,1,NULL,NULL,'Conversations',84,141),(93,92,NULL,NULL,'ConversationMessages',85,90),(94,93,NULL,NULL,'add',86,87),(102,93,NULL,NULL,'isAuthorized',88,89),(103,92,NULL,NULL,'ConversationUsers',91,114),(104,103,NULL,NULL,'index',92,93),(105,103,NULL,NULL,'view',94,95),(106,103,NULL,NULL,'add',96,97),(107,103,NULL,NULL,'edit',98,99),(108,103,NULL,NULL,'delete',100,101),(109,103,NULL,NULL,'admin_index',102,103),(110,103,NULL,NULL,'admin_view',104,105),(111,103,NULL,NULL,'admin_add',106,107),(112,103,NULL,NULL,'admin_edit',108,109),(113,103,NULL,NULL,'admin_delete',110,111),(121,103,NULL,NULL,'isAuthorized',112,113),(122,92,NULL,NULL,'Conversations',115,140),(123,122,NULL,NULL,'index',116,117),(124,122,NULL,NULL,'sent',118,119),(125,122,NULL,NULL,'view',120,121),(126,122,NULL,NULL,'add',122,123),(127,122,NULL,NULL,'edit',124,125),(128,122,NULL,NULL,'delete',126,127),(129,122,NULL,NULL,'admin_index',128,129),(130,122,NULL,NULL,'admin_view',130,131),(131,122,NULL,NULL,'admin_add',132,133),(132,122,NULL,NULL,'admin_edit',134,135),(133,122,NULL,NULL,'admin_delete',136,137),(141,122,NULL,NULL,'isAuthorized',138,139),(157,17,NULL,NULL,'login',37,38),(161,17,NULL,NULL,'logout',39,40),(165,17,NULL,NULL,'login_with_facebook',41,42),(166,17,NULL,NULL,'login_with_twitter',43,44),(174,1,NULL,NULL,'Features',142,155),(175,174,NULL,NULL,'index',143,144),(176,174,NULL,NULL,'view',145,146),(177,174,NULL,NULL,'add',147,148),(178,174,NULL,NULL,'edit',149,150),(179,174,NULL,NULL,'delete',151,152),(180,174,NULL,NULL,'isAuthorized',153,154),(184,1,NULL,NULL,'AllowedServiceGroups',156,169),(185,184,NULL,NULL,'index',157,158),(186,184,NULL,NULL,'view',159,160),(187,184,NULL,NULL,'add',161,162),(188,184,NULL,NULL,'edit',163,164),(189,184,NULL,NULL,'delete',165,166),(190,184,NULL,NULL,'isAuthorized',167,168),(191,1,NULL,NULL,'AllowedServices',170,185),(192,191,NULL,NULL,'search',171,172),(193,191,NULL,NULL,'index',173,174),(194,191,NULL,NULL,'view',175,176),(195,191,NULL,NULL,'add',177,178),(196,191,NULL,NULL,'edit',179,180),(197,191,NULL,NULL,'delete',181,182),(198,191,NULL,NULL,'isAuthorized',183,184),(199,1,NULL,NULL,'AvpTypes',186,199),(200,199,NULL,NULL,'index',187,188),(201,199,NULL,NULL,'view',189,190),(202,199,NULL,NULL,'add',191,192),(203,199,NULL,NULL,'edit',193,194),(204,199,NULL,NULL,'delete',195,196),(205,199,NULL,NULL,'isAuthorized',197,198),(206,1,NULL,NULL,'CaseLoads',200,213),(207,206,NULL,NULL,'index',201,202),(208,206,NULL,NULL,'view',203,204),(209,206,NULL,NULL,'add',205,206),(210,206,NULL,NULL,'edit',207,208),(211,206,NULL,NULL,'delete',209,210),(212,206,NULL,NULL,'isAuthorized',211,212),(213,1,NULL,NULL,'ConfigOptions',214,227),(214,213,NULL,NULL,'index',215,216),(215,213,NULL,NULL,'view',217,218),(216,213,NULL,NULL,'add',219,220),(217,213,NULL,NULL,'edit',221,222),(218,213,NULL,NULL,'delete',223,224),(219,213,NULL,NULL,'isAuthorized',225,226),(220,1,NULL,NULL,'Countries',228,241),(221,220,NULL,NULL,'index',229,230),(222,220,NULL,NULL,'view',231,232),(223,220,NULL,NULL,'add',233,234),(224,220,NULL,NULL,'edit',235,236),(225,220,NULL,NULL,'delete',237,238),(226,220,NULL,NULL,'isAuthorized',239,240),(227,1,NULL,NULL,'Dashboard',242,253),(228,227,NULL,NULL,'index',243,244),(229,227,NULL,NULL,'temp',245,246),(230,227,NULL,NULL,'update',247,248),(231,227,NULL,NULL,'create',249,250),(232,227,NULL,NULL,'isAuthorized',251,252),(233,1,NULL,NULL,'Divisions',254,267),(234,233,NULL,NULL,'index',255,256),(235,233,NULL,NULL,'view',257,258),(236,233,NULL,NULL,'add',259,260),(237,233,NULL,NULL,'edit',261,262),(238,233,NULL,NULL,'delete',263,264),(239,233,NULL,NULL,'isAuthorized',265,266),(240,1,NULL,NULL,'EmployeeDetails',268,283),(241,240,NULL,NULL,'index',269,270),(242,240,NULL,NULL,'doctor_details',271,272),(243,240,NULL,NULL,'get_doctor_working_hours',273,274),(244,240,NULL,NULL,'doctor_register',275,276),(245,240,NULL,NULL,'doctor_edit',277,278),(246,240,NULL,NULL,'upload_profile_image',279,280),(247,240,NULL,NULL,'isAuthorized',281,282),(248,40,NULL,NULL,'edit_permissions',59,60),(249,1,NULL,NULL,'Languages',284,295),(250,249,NULL,NULL,'index',285,286),(251,249,NULL,NULL,'add',287,288),(252,249,NULL,NULL,'edit',289,290),(253,249,NULL,NULL,'delete',291,292),(254,249,NULL,NULL,'isAuthorized',293,294),(255,1,NULL,NULL,'Levels',296,309),(256,255,NULL,NULL,'index',297,298),(257,255,NULL,NULL,'view',299,300),(258,255,NULL,NULL,'add',301,302),(259,255,NULL,NULL,'edit',303,304),(260,255,NULL,NULL,'delete',305,306),(261,255,NULL,NULL,'isAuthorized',307,308),(262,1,NULL,NULL,'LicenseTypes',310,323),(263,262,NULL,NULL,'index',311,312),(264,262,NULL,NULL,'view',313,314),(265,262,NULL,NULL,'add',315,316),(266,262,NULL,NULL,'edit',317,318),(267,262,NULL,NULL,'delete',319,320),(268,262,NULL,NULL,'isAuthorized',321,322),(269,1,NULL,NULL,'Modalities',324,337),(270,269,NULL,NULL,'index',325,326),(271,269,NULL,NULL,'view',327,328),(272,269,NULL,NULL,'add',329,330),(273,269,NULL,NULL,'edit',331,332),(274,269,NULL,NULL,'delete',333,334),(275,269,NULL,NULL,'isAuthorized',335,336),(276,1,NULL,NULL,'MpdActivities',338,351),(277,276,NULL,NULL,'index',339,340),(278,276,NULL,NULL,'view',341,342),(279,276,NULL,NULL,'add',343,344),(280,276,NULL,NULL,'edit',345,346),(281,276,NULL,NULL,'delete',347,348),(282,276,NULL,NULL,'isAuthorized',349,350),(283,1,NULL,NULL,'MpdActivityLists',352,365),(284,283,NULL,NULL,'index',353,354),(285,283,NULL,NULL,'view',355,356),(286,283,NULL,NULL,'add',357,358),(287,283,NULL,NULL,'edit',359,360),(288,283,NULL,NULL,'delete',361,362),(289,283,NULL,NULL,'isAuthorized',363,364),(290,1,NULL,NULL,'OfficialTitles',366,379),(291,290,NULL,NULL,'index',367,368),(292,290,NULL,NULL,'view',369,370),(293,290,NULL,NULL,'add',371,372),(294,290,NULL,NULL,'edit',373,374),(295,290,NULL,NULL,'delete',375,376),(296,290,NULL,NULL,'isAuthorized',377,378),(297,1,NULL,NULL,'PatientDetails',380,397),(298,297,NULL,NULL,'index',381,382),(299,297,NULL,NULL,'view',383,384),(300,297,NULL,NULL,'saveReligion',385,386),(301,297,NULL,NULL,'saveCountry',387,388),(302,297,NULL,NULL,'add',389,390),(303,297,NULL,NULL,'edit',391,392),(304,297,NULL,NULL,'delete',393,394),(305,297,NULL,NULL,'isAuthorized',395,396),(306,1,NULL,NULL,'Programs',398,411),(307,306,NULL,NULL,'index',399,400),(308,306,NULL,NULL,'view',401,402),(309,306,NULL,NULL,'add',403,404),(310,306,NULL,NULL,'edit',405,406),(311,306,NULL,NULL,'delete',407,408),(312,306,NULL,NULL,'isAuthorized',409,410),(313,1,NULL,NULL,'Providers',412,425),(314,313,NULL,NULL,'index',413,414),(315,313,NULL,NULL,'view',415,416),(316,313,NULL,NULL,'add',417,418),(317,313,NULL,NULL,'edit',419,420),(318,313,NULL,NULL,'delete',421,422),(319,313,NULL,NULL,'isAuthorized',423,424),(320,1,NULL,NULL,'RelationShips',426,439),(321,320,NULL,NULL,'index',427,428),(322,320,NULL,NULL,'view',429,430),(323,320,NULL,NULL,'add',431,432),(324,320,NULL,NULL,'edit',433,434),(325,320,NULL,NULL,'delete',435,436),(326,320,NULL,NULL,'isAuthorized',437,438),(327,1,NULL,NULL,'Religions',440,453),(328,327,NULL,NULL,'index',441,442),(329,327,NULL,NULL,'view',443,444),(330,327,NULL,NULL,'add',445,446),(331,327,NULL,NULL,'edit',447,448),(332,327,NULL,NULL,'delete',449,450),(333,327,NULL,NULL,'isAuthorized',451,452),(334,1,NULL,NULL,'Sponsors',454,467),(335,334,NULL,NULL,'index',455,456),(336,334,NULL,NULL,'view',457,458),(337,334,NULL,NULL,'add',459,460),(338,334,NULL,NULL,'edit',461,462),(339,334,NULL,NULL,'delete',463,464),(340,334,NULL,NULL,'isAuthorized',465,466),(341,1,NULL,NULL,'VpdActivities',468,481),(342,341,NULL,NULL,'index',469,470),(343,341,NULL,NULL,'view',471,472),(344,341,NULL,NULL,'add',473,474),(345,341,NULL,NULL,'edit',475,476),(346,341,NULL,NULL,'delete',477,478),(347,341,NULL,NULL,'isAuthorized',479,480),(348,1,NULL,NULL,'VpdActivityTypes',482,495),(349,348,NULL,NULL,'index',483,484),(350,348,NULL,NULL,'view',485,486),(351,348,NULL,NULL,'add',487,488),(352,348,NULL,NULL,'edit',489,490),(353,348,NULL,NULL,'delete',491,492),(354,348,NULL,NULL,'isAuthorized',493,494),(355,1,NULL,NULL,'WaitingListsTitles',496,513),(356,355,NULL,NULL,'index',497,498),(357,355,NULL,NULL,'addToList',499,500),(358,355,NULL,NULL,'assign',501,502),(359,355,NULL,NULL,'view',503,504),(360,355,NULL,NULL,'add',505,506),(361,355,NULL,NULL,'edit',507,508),(362,355,NULL,NULL,'delete',509,510),(363,355,NULL,NULL,'isAuthorized',511,512),(364,1,NULL,NULL,'conversations',514,519),(365,364,NULL,NULL,'ConversationsApp',515,518),(366,365,NULL,NULL,'isAuthorized',516,517);

/*Table structure for table `allowed_service_groups` */

DROP TABLE IF EXISTS `allowed_service_groups`;

CREATE TABLE `allowed_service_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `allowed_service_groups` */

insert into `allowed_service_groups` (`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'xyz group','Iprem',2130706433,1,'2013-05-07 17:22:45','2013-05-10 21:53:07'),(2,'Losem','Iprem',2130706433,1,'2013-05-10 21:32:52','2013-05-10 21:40:14');

/*Table structure for table `allowed_services` */

DROP TABLE IF EXISTS `allowed_services`;

CREATE TABLE `allowed_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `allowed_service_group_id` int(11) DEFAULT NULL,
  `description` text,
  `ip_address` int(10) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_allowed_services_asg_id` (`allowed_service_group_id`),
  CONSTRAINT `allowed_services_ibfk_1` FOREIGN KEY (`allowed_service_group_id`) REFERENCES `allowed_service_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `allowed_services` */

insert into `allowed_services` (`id`,`name`,`allowed_service_group_id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Group Tx',1,'this is description',2130706433,1,'2013-05-14 14:34:14','0000-00-00 00:00:00'),(2,'Tx',2,'this is testing description 3',2130706433,1,'2013-05-14 14:39:45','0000-00-00 00:00:00'),(3,'InTake Consultation',2,'adfa',2130706433,1,'2013-05-14 15:28:40','0000-00-00 00:00:00'),(4,'Offsite Tx',1,'adfaf',2130706433,1,'2013-05-14 15:28:48','0000-00-00 00:00:00'),(5,'Tx with live supervision',1,'a',2130706433,1,'2013-05-14 15:29:08','0000-00-00 00:00:00'),(6,'yyy',1,'adfa',2130706433,0,'2013-05-14 15:29:15','2013-05-28 14:57:57'),(7,'Co Tx',1,'aa',2130706433,1,'2013-05-14 15:29:23','0000-00-00 00:00:00'),(8,'uu',2,'adaf',2130706433,0,'2013-05-14 15:29:31','2013-05-28 14:57:52'),(9,'Dx with live supervision',1,'adaf',2130706433,1,'2013-05-14 15:29:39','0000-00-00 00:00:00'),(10,'utt',1,'adaf',2130706433,0,'2013-05-14 15:29:49','2013-05-28 14:57:46'),(11,'Dx',1,'adfa',2130706433,1,'2013-05-14 15:29:56','0000-00-00 00:00:00');

/*Table structure for table `annual_vacation_plans` */

DROP TABLE IF EXISTS `annual_vacation_plans`;

CREATE TABLE `annual_vacation_plans` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `avp_type_id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `last_work_day` datetime DEFAULT NULL COMMENT 'its analogous to start time',
  `report_to_work` datetime DEFAULT NULL COMMENT 'its analogous to end time',
  `status` enum('approved','pending') DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_annual_vacation_plans_ed_id` (`employee_detail_id`),
  KEY `FK_annual_vacation_plans_avpt_id` (`avp_type_id`),
  CONSTRAINT `annual_vacation_plans_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `annual_vacation_plans_ibfk_2` FOREIGN KEY (`avp_type_id`) REFERENCES `avp_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `annual_vacation_plans` */

/*Table structure for table `appointment_groups` */

DROP TABLE IF EXISTS `appointment_groups`;

CREATE TABLE `appointment_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `patient_detail_id` bigint(20) NOT NULL,
  `appointment_id` bigint(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `crearted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_appointment_groups_ed_id` (`employee_detail_id`),
  KEY `FK_appointment_groups_pd_id` (`patient_detail_id`),
  KEY `FK_appointment_groups_app_id` (`appointment_id`),
  CONSTRAINT `appointment_groups_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `appointment_groups_ibfk_2` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `appointment_groups_ibfk_3` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appointment_groups` */

/*Table structure for table `appointments` */

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_working_slot_id` bigint(20) NOT NULL,
  `patient_waiting_list_id` bigint(20) DEFAULT NULL,
  `employee_detail_id` bigint(20) DEFAULT NULL,
  `patient_detail_id` bigint(20) DEFAULT NULL,
  `appointment_date` datetime DEFAULT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '1',
  `is_acknowledged` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_appointments_pwl_id` (`patient_waiting_list_id`),
  KEY `FK_appointments_ed_id` (`employee_detail_id`),
  KEY `FK_appointments_pd_id` (`patient_detail_id`),
  KEY `FK_appointments_ews_id` (`employee_working_slot_id`),
  CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`patient_waiting_list_id`) REFERENCES `patient_waiting_lists` (`id`),
  CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `appointments_ibfk_4` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `appointments_ibfk_5` FOREIGN KEY (`employee_working_slot_id`) REFERENCES `employee_working_slots` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appointments` */

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `aros` */

insert into `aros` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,'Group',1,'group-1',1,2),(2,NULL,'Group',2,'group-2',3,4),(3,NULL,'Group',3,'group-3',5,6),(4,NULL,'Group',4,'group-4',7,8),(5,NULL,'Group',5,'group-5',9,10);

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `aros_acos` */

insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (7,2,228,'1','1','1','1'),(8,2,229,'1','1','1','1'),(9,2,230,'1','1','1','1'),(10,2,231,'1','1','1','1'),(11,2,241,'1','1','1','1'),(12,2,242,'1','1','1','1'),(13,2,243,'1','1','1','1'),(14,2,244,'1','1','1','1'),(15,2,245,'1','1','1','1'),(16,2,246,'1','1','1','1');

/*Table structure for table `auth_action_maps` */

DROP TABLE IF EXISTS `auth_action_maps`;

CREATE TABLE `auth_action_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL DEFAULT '5',
  `crud` enum('create','read','update','delete') NOT NULL DEFAULT 'read',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_auth_action_maps_ftr_id` (`feature_id`),
  CONSTRAINT `auth_action_maps_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `features` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=latin1;

/*Data for the table `auth_action_maps` */

insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (2,'Display mapping information',4,2,'read','2013-04-10 20:21:58','2013-06-03 17:05:22'),(3,'Adding mapping information',5,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:06'),(4,'Editing mapping information',6,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:23'),(5,'Deleting mapping information',7,2,'read','2013-04-10 20:21:58','2013-04-22 22:05:17'),(14,'Add/Synchronise all acos with auth action mapper',16,2,'read','2013-04-10 20:21:58','2013-04-19 21:21:52'),(17,'callback action for facebook',21,1,'read','2013-04-10 20:21:58','2013-04-19 21:14:23'),(21,'callback action for twitter',25,1,'read','2013-04-10 20:21:58','2013-06-03 17:05:13'),(23,'Register user',27,1,'create','2013-04-10 20:21:58','2013-04-22 22:05:47'),(24,'Email verification',28,1,'read','2013-04-10 20:21:58','2013-04-22 22:06:07'),(26,'Forgot password',30,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:18'),(27,'Reset password',31,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:23'),(35,'List all groups',41,3,'read','2013-04-10 20:21:58','2013-04-19 21:22:45'),(36,'Display group',42,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:01'),(37,'Add group',43,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:15'),(38,'Edit group',44,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:36'),(39,'Delete group',45,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:52'),(47,'List all users',55,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:37'),(48,'Display user',56,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:48'),(49,'Add user',57,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:57'),(50,'Edit user',58,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:26'),(51,'Delete user',59,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:36'),(73,'Minify plugin',84,4,'read','2013-04-10 20:21:58','2013-04-19 21:27:01'),(74,'Minify plugin',85,4,'read','2013-04-10 20:21:58','2013-04-19 21:29:09'),(75,'Dummy index action',86,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:04'),(76,'Action after successful login',87,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:42'),(77,'',88,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(78,'',89,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(79,'',90,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(80,'',91,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(81,'ConversationMessages',93,4,'read','2013-04-10 20:21:58','2013-04-22 22:10:27'),(82,'ConversationMessages -> add',94,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:12'),(90,'',102,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(91,'Conversations -> ConversationUsers',103,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:42'),(92,'ConversationUsers -> index',104,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:55'),(93,'ConversationUsers -> view',105,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:12'),(94,'ConversationUsers -> add',106,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:31'),(95,'ConversationUsers -> edit',107,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:42'),(96,'ConversationUsers -> delete',108,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:53'),(97,'ConversationUsers -> admin_index',109,4,'read','2013-04-10 20:21:58','2013-04-22 22:13:10'),(98,'ConversationUsers -> admin_view',110,4,'read','2013-04-10 20:21:58','2013-04-22 22:17:48'),(99,'ConversationUsers -> admin_add',111,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:00'),(100,'ConversationUsers -> admin_edit',112,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:10'),(101,'ConversationUsers -> admin_delete',113,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:20'),(109,'',121,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(110,'Conversations -> Conversations',122,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:37'),(111,'Conversations -> index',123,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:00'),(112,'Conversations -> sent',124,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:13'),(113,'Conversations -> view',125,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:27'),(114,'Conversations -> add',126,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:37'),(115,'Conversations -> edit',127,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:49'),(116,'ConversationUsers -> delete',128,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:01'),(117,'Conversations -> admin_index',129,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:14'),(118,'Conversations -> admin_view',130,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:33'),(119,'Conversations -> admin_add',131,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:51'),(120,'Conversations -> admin_edit',132,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:06'),(121,'Conversations -> admin_delete',133,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:26'),(129,'',141,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(141,'Login',157,1,'read','2013-04-12 21:53:51','2013-04-19 21:30:07'),(144,'Logout',161,1,'read','2013-04-15 20:01:29','2013-04-19 21:30:12'),(147,'Login action for facebook',165,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:19'),(148,'Login action for twitter',166,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:25'),(153,'List all auth action maps',3,2,'read','2013-04-19 20:03:52','2013-04-22 16:11:29'),(155,'Display features',175,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:00'),(156,'View Feature',176,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:14'),(157,'Add new Feature',177,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:23'),(158,'Edit Feature',178,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:33'),(159,'Delete Feature',179,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:43'),(160,'',180,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05'),(163,'',185,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(164,'',186,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(165,'',187,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(166,'',188,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(167,'',189,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(168,'',190,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(169,'',192,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(170,'',193,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(171,'',194,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(172,'',195,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(173,'',196,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(174,'',197,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(175,'',198,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(176,'',200,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(177,'',201,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(178,'',202,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(179,'',203,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(180,'',204,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(181,'',205,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(182,'',207,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(183,'',208,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(184,'',209,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(185,'',210,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(186,'',211,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(187,'',212,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(188,'',214,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(189,'',215,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(190,'',216,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(191,'',217,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(192,'',218,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(193,'',219,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(194,'',221,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(195,'',222,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(196,'',223,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(197,'',224,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(198,'',225,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(199,'',226,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(200,'Dashboard',228,10,'read','2013-06-03 16:12:15','2013-06-03 20:58:44'),(201,'',229,10,'read','2013-06-03 16:12:15','2013-06-03 20:58:53'),(202,'',230,10,'read','2013-06-03 16:12:15','2013-06-03 20:59:01'),(203,'',231,10,'read','2013-06-03 16:12:15','2013-06-03 20:59:07'),(204,'',232,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(205,'',234,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(206,'',235,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(207,'',236,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(208,'',237,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(209,'',238,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(210,'',239,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(211,'Employee Listings',241,9,'read','2013-06-03 16:12:15','2013-06-03 16:52:30'),(212,'Doctor detail profile view',242,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:12'),(213,'Doctor working hours',243,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:28'),(214,'Doctor registration',244,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:52'),(215,'Doctor edit profile',245,9,'read','2013-06-03 16:12:15','2013-06-03 20:36:44'),(216,'Image upload for doctor profile',246,9,'read','2013-06-03 16:12:15','2013-06-03 20:37:05'),(217,'',247,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(218,'',248,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(219,'',250,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(220,'',251,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(221,'',252,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(222,'',253,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(223,'',254,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(224,'',256,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(225,'',257,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(226,'',258,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(227,'',259,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(228,'',260,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(229,'',261,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(230,'',263,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(231,'',264,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(232,'',265,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(233,'',266,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(234,'',267,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(235,'',268,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(236,'',270,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(237,'',271,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(238,'',272,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(239,'',273,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(240,'',274,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(241,'',275,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(242,'',277,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(243,'',278,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(244,'',279,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(245,'',280,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(246,'',281,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(247,'',282,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(248,'',284,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(249,'',285,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(250,'',286,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(251,'',287,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(252,'',288,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(253,'',289,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(254,'',291,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(255,'',292,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(256,'',293,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(257,'',294,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(258,'',295,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(259,'',296,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(260,'',298,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(261,'',299,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(262,'',300,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(263,'',301,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(264,'',302,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(265,'',303,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(266,'',304,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(267,'',305,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(268,'',307,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(269,'',308,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(270,'',309,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(271,'',310,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(272,'',311,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(273,'',312,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(274,'',314,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(275,'',315,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(276,'',316,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(277,'',317,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(278,'',318,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(279,'',319,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(280,'',321,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(281,'',322,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(282,'',323,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(283,'',324,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(284,'',325,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(285,'',326,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(286,'',328,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(287,'',329,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(288,'',330,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(289,'',331,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(290,'',332,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(291,'',333,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(292,'',335,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(293,'',336,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(294,'',337,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(295,'',338,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(296,'',339,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(297,'',340,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(298,'',342,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(299,'',343,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(300,'',344,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(301,'',345,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(302,'',346,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(303,'',347,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(304,'',349,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(305,'',350,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(306,'',351,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(307,'',352,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(308,'',353,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(309,'',354,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(310,'',356,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(311,'',357,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(312,'',358,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(313,'',359,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(314,'',360,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(315,'',361,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(316,'',362,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(317,'',363,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(318,'',365,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(319,'',366,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15');

/*Table structure for table `avp_types` */

DROP TABLE IF EXISTS `avp_types`;

CREATE TABLE `avp_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `avp_types` */

insert into `avp_types` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Paid',2130706433,1,'2013-05-07 18:22:35','2013-05-07 18:24:06'),(2,'UnPaid',2130706433,1,'2013-05-07 18:24:13','2013-05-07 18:24:45'),(3,'Medical',2130706433,1,'2013-05-07 18:24:31','2013-05-07 18:24:31'),(4,'Business',2130706433,1,'2013-05-07 18:24:38','2013-05-10 21:52:55');

/*Table structure for table `call_logs` */

DROP TABLE IF EXISTS `call_logs`;

CREATE TABLE `call_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appointment_id` bigint(20) NOT NULL,
  `calling_date_time` datetime NOT NULL,
  `call_subject` varchar(100) NOT NULL,
  `call_answered_by` int(11) NOT NULL,
  `call_summary` varchar(255) DEFAULT NULL,
  `caller_id` bigint(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_call_logs_rs_id` (`call_answered_by`),
  KEY `FK_call_logs_ed_id` (`caller_id`),
  KEY `FK_call_logs_app_id` (`appointment_id`),
  CONSTRAINT `call_logs_ibfk_2` FOREIGN KEY (`call_answered_by`) REFERENCES `relation_ships` (`id`),
  CONSTRAINT `call_logs_ibfk_3` FOREIGN KEY (`caller_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `call_logs_ibfk_4` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `call_logs` */

/*Table structure for table `case_loads` */

DROP TABLE IF EXISTS `case_loads`;

CREATE TABLE `case_loads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `case_loads` */

insert into `case_loads` (`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Arctic','this is discription',2130706433,1,'2013-05-13 12:57:36','2013-05-28 14:50:59'),(2,'this is testing 2','this is discription 2',2130706433,0,'2013-05-13 13:03:55','2013-05-13 13:04:05'),(3,'Voice','Losem',2130706433,1,'2013-05-28 14:51:14','2013-05-28 14:51:14'),(4,'Autism','Losem',2130706433,1,'2013-05-28 14:51:28','2013-05-28 14:51:28');

/*Table structure for table `clinicial_levels` */

DROP TABLE IF EXISTS `clinicial_levels`;

CREATE TABLE `clinicial_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `fees` decimal(8,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `ip_address` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_clinicial_levels_div_id` (`division_id`),
  CONSTRAINT `clinicial_levels_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `clinicial_levels` */

/*Table structure for table `config_options` */

DROP TABLE IF EXISTS `config_options`;

CREATE TABLE `config_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `config_options` */

insert into `config_options` (`id`,`name`,`value`,`ip_address`,`is_active`,`created`,`modified`) values (1,'SITEURL','http://www.example.com',NULL,1,'2013-05-13 15:11:59','2013-05-13 15:11:59'),(2,'gender','a:2:{s:4:\"male\";s:4:\"Male\";s:6:\"female\";s:6:\"Female\";}',NULL,1,'2013-05-16 17:05:08','2013-05-16 17:05:08');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert into `countries` (`id`,`name`,`nationality`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Afghanistan','Afghan',2130706433,1,'2013-05-06 20:27:30','2013-05-13 09:14:20'),(2,'Albania','Albanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(3,'Algeria','Algerian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(4,'Andorra','Andorran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(5,'Angola','Angolan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(6,'Antigua and Barbuda','Antiguans, Barbudans',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(7,'Argentina','Argentinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(8,'Armenia','Armenian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(9,'Australia','Australian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(10,'Austria','Austrian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(11,'Azerbaijan','Azerbaijani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(12,'The Bahamas','Bahamian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(13,'Bahrain','Bahraini',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(14,'Bangladesh','Bangladeshi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(15,'Barbados','Barbadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(16,'Belarus','Belarusian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(17,'Belgium','Belgian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(18,'Belize','Belizean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(19,'Benin','Beninese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(20,'Bhutan','Bhutanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(21,'Bolivia','Bolivian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(22,'Bosnia and Herzegovina','Bosnian, Herzegovinian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(23,'Botswana','Motswana (singular), Batswana (plural)',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(24,'Brazil','Brazilian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(25,'Brunei','Bruneian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(26,'Bulgaria','Bulgarian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(27,'Burkina Faso','Burkinabe',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(28,'Burundi','Burundian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(29,'Cambodia','Cambodian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(30,'Cameroon','Cameroonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(31,'Canada','Canadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(32,'Cape Verde','Cape Verdian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(33,'Central African Republic','Central African',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(34,'Chad','Chadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(35,'Chile','Chilean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(36,'China','Chinese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(37,'Colombia','Colombian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(38,'Comoros','Comoran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(39,'Congo, Republic of the','Congolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(40,'Congo, Democratic Republic of the','Congolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(41,'Costa Rica','Costa Rican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(42,'Cote d\'Ivoire','Ivorian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(43,'Croatia','Croatian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(44,'Cuba','Cuban',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(45,'Cyprus','Cypriot',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(46,'Czech Republic','Czech',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(47,'Denmark','Danish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(48,'Djibouti','Djibouti',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(49,'Dominica','Dominican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(50,'Dominican Republic','Dominican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(51,'East Timor','East Timorese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(52,'Ecuador','Ecuadorean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(53,'Egypt','Egyptian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(54,'El Salvador','Salvadoran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(55,'Equatorial Guinea','Equatorial Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(56,'Eritrea','Eritrean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(57,'Estonia','Estonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(58,'Ethiopia','Ethiopian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(59,'Fiji','Fijian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(60,'Finland','Finnish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(61,'France','French',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(62,'Gabon','Gabonese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(63,'The Gambia','Gambian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(64,'Georgia','Georgian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(65,'Germany','German',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(66,'Ghana','Ghanaian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(67,'Greece','Greek',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(68,'Grenada','Grenadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(69,'Guatemala','Guatemalan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(70,'Guinea','Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(71,'Guinea-Bissau','Guinea-Bissauan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(72,'Guyana','Guyanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(73,'Haiti','Haitian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(74,'Honduras','Honduran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(75,'Hungary','Hungarian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(76,'Iceland','Icelander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(77,'India','Indian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(78,'Indonesia','Indonesian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(79,'Iran','Iranian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(80,'Iraq','Iraqi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(81,'Ireland','Irish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(82,'Israel','Israeli',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(83,'Italy','Italian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(84,'Jamaica','Jamaican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(85,'Japan','Japanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(86,'Jordan','Jordanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(87,'Kazakhstan','Kazakhstani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(88,'Kenya','Kenyan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(89,'Kiribati','I-Kiribati',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(90,'Korea, North','North Korean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(91,'Korea, South','South Korean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(92,'Kuwait','Kuwaiti',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(93,'Kyrgyz Republic','Kirghiz',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(94,'Laos','Laotian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(95,'Latvia','Latvian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(96,'Lebanon','Lebanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(97,'Lesotho','Mosotho',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(98,'Liberia','Liberian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(99,'Libya','Libyan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(100,'Liechtenstein','Liechtensteiner',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(101,'Lithuania','Lithuanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(102,'Luxembourg','Luxembourger',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(103,'Macedonia','Macedonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(104,'Madagascar','Malagasy',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(105,'Malawi','Malawian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(106,'Malaysia','Malaysian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(107,'Maldives','Maldivan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(108,'Mali','Malian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(109,'Malta','Maltese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(110,'Marshall Islands','Marshallese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(111,'Mauritania','Mauritanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(112,'Mauritius','Mauritian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(113,'Mexico','Mexican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(114,'Federated States of Micronesia','Micronesian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(115,'Moldova','Moldovan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(116,'Monaco','Monegasque',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(117,'Mongolia','Mongolian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(118,'Morocco','Moroccan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(119,'Mozambique','Mozambican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(120,'Myanmar (Burma)','Burmese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(121,'Namibia','Namibian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(122,'Nauru','Nauruan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(123,'Nepal','Nepalese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(124,'Netherlands','Dutch',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(125,'New Zealand','New Zealander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(126,'Nicaragua','Nicaraguan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(127,'Niger','Nigerien',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(128,'Nigeria','Nigerian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(129,'Norway','Norwegian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(130,'Oman','Omani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(131,'Pakistan','Pakistani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(132,'Palau','Palauan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(133,'Panama','Panamanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(134,'Papua New Guinea','Papua New Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(135,'Paraguay','Paraguayan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(136,'Peru','Peruvian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(137,'Philippines','Filipino',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(138,'Poland','Polish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(139,'Portugal','Portuguese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(140,'Qatar','Qatari',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(141,'Romania','Romanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(142,'Russia','Russian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(143,'Rwanda','Rwandan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(144,'Saint Kitts and Nevis','Kittian and Nevisian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(145,'Saint Lucia','Saint Lucian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(146,'Samoa','Samoan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(147,'San Marino','Sammarinese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(148,'Sao Tome and Principe','Sao Tomean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(149,'Saudi Arabia','Saudi Arabian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(150,'Senegal','Senegalese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(151,'Serbia and Montenegro','Serbian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(152,'Seychelles','Seychellois',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(153,'Sierra Leone','Sierra Leonean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(154,'Singapore','Singaporean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(155,'Slovakia','Slovak',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(156,'Slovenia','Slovene',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(157,'Solomon Islands','Solomon Islander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(158,'Somalia','Somali',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(159,'South Africa','South African',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(160,'Spain','Spanish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(161,'Sri Lanka','Sri Lankan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(162,'Sudan','Sudanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(163,'Suriname','Surinamer',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(164,'Swaziland','Swazi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(165,'Sweden','Swedish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(166,'Switzerland','Swiss',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(167,'Syria','Syrian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(168,'Taiwan','Taiwanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(169,'Tajikistan','Tadzhik',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(170,'Tanzania','Tanzanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(171,'Thailand','Thai',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(172,'Togo','Togolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(173,'Tonga','Tongan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(174,'Trinidad and Tobago','Trinidadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(175,'Tunisia','Tunisian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(176,'Turkey','Turkish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(177,'Turkmenistan','Turkmen',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(178,'Tuvalu','Tuvaluan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(179,'Uganda','Ugandan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(180,'Ukraine','Ukrainian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(181,'United Arab Emirates','Emirian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(182,'United Kingdom','British',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(183,'United States','American',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(184,'Uruguay','Uruguayan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(185,'Uzbekistan','Uzbekistani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(186,'Vanuatu','Ni-Vanuatu',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(187,'Vatican City (Holy See)','none',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(188,'Venezuela','Venezuelan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(189,'Vietnam','Vietnamese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(190,'Yemen','Yemeni',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(191,'Zambia','Zambian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(192,'Zimbabwe','Zimbabwean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30');

/*Table structure for table `divisions` */

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `ip_address` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `divisions` */

insert into `divisions` (`id`,`name`,`description`,`is_active`,`ip_address`,`created`,`modified`) values (4,'ABA','',1,2130706433,'2013-05-06 13:05:58','2013-05-10 21:50:37'),(8,'SLP',NULL,1,2130706433,'2013-05-06 14:02:49','2013-05-06 22:02:05'),(9,'OT',NULL,1,2130706433,'2013-05-06 22:02:11','2013-05-07 15:50:37'),(10,'Audiology',NULL,1,2130706433,'2013-05-06 22:24:06','2013-05-06 22:37:12'),(11,'Other',NULL,1,2130706433,'2013-05-07 15:37:07','2013-05-07 15:37:07'),(12,'test2 with proc',NULL,0,2130706433,'2013-05-09 11:09:17','2013-05-09 11:09:24'),(13,'testing tahir a',NULL,1,2130706433,'2013-05-09 12:09:07','2013-05-09 12:11:00'),(14,'again test','losem IPREM',1,2130706433,'2013-05-10 20:07:02','2013-05-10 21:31:11'),(15,'Audiology 456','qwe rty tyui',1,2130706433,'2013-05-13 16:07:27','2013-05-13 16:07:39');

/*Table structure for table `employee_allowed_services` */

DROP TABLE IF EXISTS `employee_allowed_services`;

CREATE TABLE `employee_allowed_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `allowed_service_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_services_ed_id` (`employee_detail_id`),
  KEY `FK_employee_services_as_id` (`allowed_service_id`),
  CONSTRAINT `employee_allowed_services_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_allowed_services_ibfk_2` FOREIGN KEY (`allowed_service_id`) REFERENCES `allowed_services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `employee_allowed_services` */

insert into `employee_allowed_services` (`id`,`employee_detail_id`,`allowed_service_id`,`is_active`,`created`,`modified`) values (11,8,1,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(12,8,2,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(13,8,1,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(14,8,2,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(15,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,1,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(18,10,2,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(19,11,1,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(20,11,2,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(21,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(22,12,2,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(23,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(24,12,1,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(25,12,2,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(26,12,3,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(27,13,1,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(28,13,2,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_case_loads` */

DROP TABLE IF EXISTS `employee_case_loads`;

CREATE TABLE `employee_case_loads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `case_load_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_case_loads_cl_id` (`case_load_id`),
  KEY `FK_employee_case_loads_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_case_loads_ibfk_1` FOREIGN KEY (`case_load_id`) REFERENCES `case_loads` (`id`),
  CONSTRAINT `employee_case_loads_ibfk_2` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `employee_case_loads` */

insert into `employee_case_loads` (`id`,`employee_detail_id`,`case_load_id`,`is_active`,`created`,`modified`) values (11,8,1,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(12,8,3,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(13,8,1,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(14,8,3,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(15,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,3,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,1,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(18,10,3,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(19,11,1,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(20,11,3,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(21,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(22,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(23,12,1,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(24,12,3,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(25,13,1,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(26,13,3,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_details` */

DROP TABLE IF EXISTS `employee_details`;

CREATE TABLE `employee_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `picture_name` varchar(50) DEFAULT NULL,
  `mobile_saudi` varchar(50) DEFAULT NULL,
  `landline_saudi` varchar(50) DEFAULT NULL,
  `mobile_homecountry` varchar(50) DEFAULT NULL,
  `landline_homecountry` varchar(50) DEFAULT NULL,
  `jish_email` varchar(50) DEFAULT NULL,
  `license_type_id` int(11) DEFAULT NULL,
  `license_number` varchar(50) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `contract_expiry_date` date DEFAULT NULL,
  `gender` enum('male','female') NOT NULL,
  `dob` date DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `status` enum('active','delete') NOT NULL DEFAULT 'active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_details_u_id` (`user_id`),
  KEY `FK_employee_details_lt_id` (`license_type_id`),
  CONSTRAINT `employee_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `employee_details_ibfk_2` FOREIGN KEY (`license_type_id`) REFERENCES `license_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `employee_details` */

insert into `employee_details` (`id`,`user_id`,`first_name`,`last_name`,`middle_name`,`picture_name`,`mobile_saudi`,`landline_saudi`,`mobile_homecountry`,`landline_homecountry`,`jish_email`,`license_type_id`,`license_number`,`joining_date`,`contract_expiry_date`,`gender`,`dob`,`ip_address`,`status`,`created`,`modified`) values (4,23,'Ahmed','Ahsan','Ali','0','12345678','1234567','12345678','12345678','a@jish.com',2,'12345678','2013-05-01','2013-05-02','male','2013-05-03',2130706433,'active','2013-05-23 14:47:17','2013-05-28 22:57:53'),(5,25,'Lady','losem','Losem','ca39f38a652b93f15c52fb55c252aade.jpeg','12345678','12345678','12345678','12345678','lady@jish.com',2,'1234567','2013-05-01','2013-05-09','female','2013-05-11',2130706433,'active','2013-05-28 16:29:46','2013-05-28 16:29:46'),(6,26,'Lady','losem','Losem','0bb7911681c5d5b740242948c7270442.png','12345678','12345678','12345678','12345678','ladies@jish.com',NULL,'1234567','2013-05-09','2013-05-17','female','2013-05-31',2130706433,'active','2013-05-28 16:34:57','2013-05-28 16:34:57'),(7,27,'Jason','Losem','Borne','afb5e9b679b7f370a3d7bc3c9a216f34.JPG','12345678','12345678','12345678','12345678','tes1t@jish.com',NULL,'1234567','2013-05-01','2013-05-02','male','2013-05-04',2130706433,'active','2013-05-28 16:37:54','2013-05-28 16:37:54'),(8,28,'Jason','losem','Borne','ee60f0c3e2c9dc4bca6eb8aa88ef99aa.JPG','12345678','12345678','12345678','12345678','tes1t@jish.com',2,'12345678','2013-05-16','2013-05-23','male','2013-05-16',2130706433,'active','2013-05-28 17:17:24','2013-05-30 22:12:02'),(9,30,'John','Abraham','Losem','9b58503846dcc952ff5dfa5f3dc59dd8.jpg','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:00:12','2013-06-03 15:00:12'),(10,31,'John','Abraham','Losem','e92f20e6d739d27a0a24af168980d0d6.JPG','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:02:01','2013-06-03 15:02:01'),(11,32,'John','Abraham','Losem','9029276c41d9e0d56ba5b86678944eda.jpg','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:03:56','2013-06-03 15:03:56'),(12,33,'Jonny','Quest','Bruce','852ef17b5dec1f00d39d740877099941.jpeg','12345678','12345678','12345678','12345678','a@test.com',1,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:08:51','2013-06-03 15:49:56'),(13,35,'John','Abraham','Losem','d20040931733446f553fa535bd29c65b.jpeg','12345678','12345678','12345678','12345678','losem@jish.com',1,'12345678','2013-06-06','2013-06-04','male','2013-06-04',2130706433,'active','2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_divisions` */

DROP TABLE IF EXISTS `employee_divisions`;

CREATE TABLE `employee_divisions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_divisions_div_id` (`division_id`),
  KEY `FK_employee_divisions_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_divisions_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
  CONSTRAINT `employee_divisions_ibfk_2` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `employee_divisions` */

insert into `employee_divisions` (`id`,`employee_detail_id`,`division_id`,`is_active`,`created`,`modified`) values (12,8,4,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(13,8,8,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(14,8,4,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(15,8,8,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(16,9,4,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,9,8,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(18,10,4,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(19,10,8,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(20,11,8,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(21,11,9,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(22,12,4,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(23,12,9,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(24,12,10,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(25,12,4,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(26,12,9,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(27,12,10,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(28,13,4,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(29,13,8,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_levels` */

DROP TABLE IF EXISTS `employee_levels`;

CREATE TABLE `employee_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `level_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_levels_ed_id` (`employee_detail_id`),
  KEY `FK_employee_levels_lvl_id` (`level_id`),
  CONSTRAINT `employee_levels_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_levels_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `employee_levels` */

insert into `employee_levels` (`id`,`employee_detail_id`,`level_id`,`is_active`,`created`,`modified`) values (7,8,1,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(8,8,1,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(9,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(10,10,1,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(11,11,1,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(12,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(13,12,1,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(14,13,1,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(15,13,3,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_official_titles` */

DROP TABLE IF EXISTS `employee_official_titles`;

CREATE TABLE `employee_official_titles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `official_title_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_official_title_ed_id` (`employee_detail_id`),
  KEY `FK_employee_official_title_ot_id` (`official_title_id`),
  CONSTRAINT `employee_official_titles_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_official_titles_ibfk_2` FOREIGN KEY (`official_title_id`) REFERENCES `official_titles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `employee_official_titles` */

insert into `employee_official_titles` (`id`,`employee_detail_id`,`official_title_id`,`is_active`,`created`,`modified`) values (11,8,2,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(12,8,3,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(13,8,2,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(14,8,3,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(15,9,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,3,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,2,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(18,10,3,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(19,11,2,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(20,11,3,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(21,12,2,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(22,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(23,12,2,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(24,12,3,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(25,13,2,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(26,13,3,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_working_hours` */

DROP TABLE IF EXISTS `employee_working_hours`;

CREATE TABLE `employee_working_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `working_day` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_possible_slots` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_working_hours_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_working_hours_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `employee_working_hours` */

insert into `employee_working_hours` (`id`,`employee_detail_id`,`division_id`,`working_day`,`start_time`,`end_time`,`total_possible_slots`,`is_active`,`created`,`modified`) values (23,8,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(24,8,4,'sunday','2013-05-26 07:00:00','2013-05-26 09:00:00',8,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(25,8,8,'monday','2013-05-27 08:00:00','2013-05-27 10:00:00',8,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(26,8,8,'tuesday','2013-05-28 08:00:00','2013-05-28 10:30:00',10,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(27,9,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(28,9,4,'sunday','2013-06-02 07:00:00','2013-06-02 09:00:00',8,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(29,9,8,'monday','2013-06-03 09:00:00','2013-06-03 11:00:00',8,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(30,9,4,'tuesday','2013-06-04 07:00:00','2013-06-04 09:30:00',10,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(31,10,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(32,10,4,'sunday','2013-06-02 05:00:00','2013-06-02 07:30:00',10,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(33,10,8,'monday','2013-06-03 06:30:00','2013-06-03 09:00:00',10,1,'2013-06-03 15:02:02','2013-06-03 15:02:02'),(34,10,4,'tuesday','2013-06-04 07:30:00','2013-06-04 11:00:00',14,1,'2013-06-03 15:02:02','2013-06-03 15:02:02'),(35,11,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(36,11,8,'sunday','2013-06-02 06:30:00','2013-06-02 09:30:00',12,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(37,11,9,'monday','2013-06-03 07:30:00','2013-06-03 09:30:00',8,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(38,11,8,'tuesday','2013-06-04 07:00:00','2013-06-04 10:30:00',14,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(39,12,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(40,12,4,'sunday','2013-06-02 08:00:00','2013-06-02 11:00:00',12,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(41,12,10,'monday','2013-06-03 09:30:00','2013-06-03 12:00:00',10,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(42,12,4,'tuesday','2013-06-04 11:30:00','2013-06-04 14:00:00',10,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(43,12,4,'sunday','2013-06-02 08:00:00','2013-06-02 11:00:00',12,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(44,12,10,'monday','2013-06-03 09:30:00','2013-06-03 12:00:00',10,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(45,12,4,'tuesday','2013-06-04 11:30:00','2013-06-04 14:00:00',10,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(46,13,0,'','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(47,13,4,'sunday','2013-06-02 06:00:00','2013-06-02 08:00:00',8,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(48,13,8,'monday','2013-06-03 07:30:00','2013-06-03 10:00:00',10,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(49,13,8,'tuesday','2013-06-04 07:00:00','2013-06-04 09:00:00',8,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `employee_working_slots` */

DROP TABLE IF EXISTS `employee_working_slots`;

CREATE TABLE `employee_working_slots` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_working_hour_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employee_working_slots_ewh_id` (`employee_working_hour_id`),
  CONSTRAINT `employee_working_slots_ibfk_1` FOREIGN KEY (`employee_working_hour_id`) REFERENCES `employee_working_hours` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_working_slots` */

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'User Management',2130706433,1,'2013-04-19 20:33:14','2013-04-24 14:44:06'),(2,'Acl Managements',2130706433,1,'2013-04-19 21:18:13','2013-05-10 21:50:24'),(3,'Group Management',2130706433,1,'2013-04-19 21:22:18','2013-04-24 14:44:23'),(4,'Plugins',2130706433,1,'2013-04-19 21:26:44','2013-04-24 14:44:26'),(5,'Others',2130706433,1,'2013-04-22 16:13:27','2013-04-24 14:44:29'),(7,'Test Feature',2130706433,0,'2013-04-24 16:05:54','2013-04-24 16:11:26'),(8,'Feature Management',2130706433,1,'2013-04-24 21:54:03','2013-04-24 21:54:03'),(9,'Employee Details Management',2130706433,1,'2013-06-03 16:28:14','2013-06-03 16:28:14'),(10,'Dashboard',2130706433,1,'2013-06-03 20:58:09','2013-06-03 20:58:09');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

insert into `groups` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Super Admin',2130706433,1,'2013-05-06 16:23:49','2013-05-06 16:40:15'),(2,'Front Desk Officer',2130706433,1,'2013-05-06 16:41:01','2013-05-06 16:41:32'),(3,'Patient',2130706433,1,'2013-05-06 16:41:10','2013-05-06 16:41:10'),(4,'Doctor',2130706433,1,'2013-05-06 16:41:41','2013-05-14 19:20:35'),(5,'Admin',2130706433,1,'2013-05-17 20:52:52','2013-05-17 20:52:52');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `languages` */

insert into `languages` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'English',2130706433,1,'2013-05-10 15:29:37','2013-05-13 16:37:02'),(2,'Arabic',2130706433,1,'2013-05-28 14:48:47','2013-05-28 14:48:47'),(3,'Mandarin',2130706433,1,'2013-05-28 14:49:04','2013-05-28 14:49:04'),(4,'Spanish',2130706433,1,'2013-05-28 14:49:17','2013-05-28 14:49:17'),(5,'Japanese',2130706433,1,'2013-05-28 14:49:41','2013-05-28 14:49:41'),(6,'German',2130706433,1,'2013-05-28 14:49:53','2013-05-28 14:49:53');

/*Table structure for table `levels` */

DROP TABLE IF EXISTS `levels`;

CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `levels` */

insert into `levels` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Manager',2130706433,1,'2013-05-13 16:29:34','2013-05-13 16:38:15'),(3,'Supervisor',2130706433,1,'2013-05-13 16:30:00','2013-05-13 16:30:00');

/*Table structure for table `license_types` */

DROP TABLE IF EXISTS `license_types`;

CREATE TABLE `license_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `license_types` */

insert into `license_types` (`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA',NULL,2130706433,1,'2013-05-13 17:08:03','2013-05-13 17:08:03'),(2,'OTR',NULL,2130706433,1,'2013-05-13 17:08:09','2013-05-13 17:08:09'),(3,'AAA',NULL,2130706433,1,'2013-05-13 17:08:18','2013-05-13 17:08:35');

/*Table structure for table `modalities` */

DROP TABLE IF EXISTS `modalities`;

CREATE TABLE `modalities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `modalities` */

insert into `modalities` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Online Course',2130706433,1,'2013-05-13 18:03:42','2013-05-13 18:03:42'),(2,'DVD',2130706433,1,'2013-05-13 18:04:49','2013-05-13 18:04:49'),(3,'Workshop',2130706433,1,'2013-05-13 18:04:55','2013-05-13 18:05:13');

/*Table structure for table `mpd_activities` */

DROP TABLE IF EXISTS `mpd_activities`;

CREATE TABLE `mpd_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `program_id` int(11) DEFAULT NULL,
  `mpd_activity_list_id` int(11) DEFAULT NULL,
  `modality_id` int(11) DEFAULT NULL,
  `status` enum('pending','complete') DEFAULT 'pending',
  `date_complition` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mpd_activities_ed_id` (`employee_detail_id`),
  KEY `FK_mpd_activities_prm_id` (`program_id`),
  KEY `FK_mpd_activities_mod_id` (`modality_id`),
  KEY `FK_mpd_activities_mpd_atl_id` (`mpd_activity_list_id`),
  CONSTRAINT `mpd_activities_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `mpd_activities_ibfk_2` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  CONSTRAINT `mpd_activities_ibfk_3` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`),
  CONSTRAINT `mpd_activities_ibfk_4` FOREIGN KEY (`mpd_activity_list_id`) REFERENCES `mpd_activity_lists` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `mpd_activities` */

insert into `mpd_activities` (`id`,`employee_detail_id`,`program_id`,`mpd_activity_list_id`,`modality_id`,`status`,`date_complition`,`is_active`,`created`,`modified`) values (8,4,1,1,1,'pending','2013-05-25 00:00:00',1,'2013-05-28 13:53:47','2013-05-28 13:53:47'),(9,4,1,1,1,'pending','2013-05-24 00:00:00',1,'2013-05-28 14:07:09','2013-05-28 14:07:09'),(10,4,1,1,1,'pending','2013-05-25 00:00:00',1,'2013-05-28 14:07:22','2013-05-28 14:07:22'),(11,4,1,1,1,'pending','2013-05-22 00:00:00',1,'2013-05-28 14:09:35','2013-05-28 14:09:35'),(12,4,2,2,2,'complete','2013-05-22 00:00:00',1,'2013-05-28 14:09:44','2013-05-28 14:16:37'),(13,4,2,2,3,'complete','2013-05-22 00:00:00',1,'2013-05-28 14:09:53','2013-05-28 15:30:28');

/*Table structure for table `mpd_activity_lists` */

DROP TABLE IF EXISTS `mpd_activity_lists`;

CREATE TABLE `mpd_activity_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mpd_activity_lists` */

insert into `mpd_activity_lists` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'JARAS',2130706433,1,'2013-05-13 18:19:38','2013-05-13 18:19:38'),(2,'Oral Motors',2130706433,1,'2013-05-13 18:20:33','2013-05-13 18:21:03');

/*Table structure for table `official_titles` */

DROP TABLE IF EXISTS `official_titles`;

CREATE TABLE `official_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `official_titles` */

insert into `official_titles` (`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (2,'PhD','adf afa daf',2130706433,1,'2013-05-13 14:05:26','2013-05-28 15:01:33'),(3,'Clinical Supervisor','Losem',2130706433,1,'2013-05-28 15:01:46','2013-05-28 15:01:46'),(4,'Pathologist','Losem',2130706433,1,'2013-05-28 15:02:23','2013-05-28 15:02:23');

/*Table structure for table `patient_details` */

DROP TABLE IF EXISTS `patient_details`;

CREATE TABLE `patient_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `iqama_no` varchar(50) DEFAULT NULL,
  `marital_status` enum('single','married') DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `allow_info_from_agencies` tinyint(1) NOT NULL DEFAULT '1',
  `allow_info_to_agencies` tinyint(1) NOT NULL DEFAULT '1',
  `multimedia_agreement` varchar(50) DEFAULT NULL,
  `multimedia_for_research` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `prefered_contact` varchar(50) DEFAULT NULL,
  `po_box` varchar(50) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `residence_no` varchar(250) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `patient_lives_with` varchar(255) DEFAULT NULL,
  `sibling_order` varchar(255) DEFAULT NULL,
  `gardian_relationship` varchar(255) DEFAULT NULL,
  `refered_by` varchar(255) DEFAULT NULL,
  `status` enum('active','discharged','discontinued','suspended','temporary') NOT NULL DEFAULT 'temporary',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patient_details_reg_id` (`religion_id`),
  KEY `FK_patient_details_cty_id` (`country_id`),
  KEY `FK_patient_details_u_id` (`user_id`),
  CONSTRAINT `patient_details_ibfk_2` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`id`),
  CONSTRAINT `patient_details_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `patient_details_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `patient_details` */

insert into `patient_details` (`id`,`user_id`,`iqama_no`,`marital_status`,`religion_id`,`allow_info_from_agencies`,`allow_info_to_agencies`,`multimedia_agreement`,`multimedia_for_research`,`mobile`,`prefered_contact`,`po_box`,`postal_code`,`district`,`city`,`country_id`,`residence_no`,`gender`,`dob`,`first_name`,`last_name`,`middle_name`,`patient_lives_with`,`sibling_order`,`gardian_relationship`,`refered_by`,`status`,`created`,`modified`) values (4,5,'22222','single',1,0,0,'','','','','','','','',1,'','male',NULL,'tahir','rasheed','','parents','oldest','n/a','friend','active','2013-05-17 10:46:22','2013-05-23 15:06:32'),(5,6,'23666','single',1,1,1,'1','1','03463511039','03463511039','20','4056','sindh','karachi',131,'','male','2013-05-01','ali','ahmed','','parents','oldest','n/a','friend','discontinued','2013-05-17 11:23:09','2013-05-24 14:37:47'),(10,13,'22222','married',1,1,1,'0','0','03463511039','','','4056','sindh','karachi',131,'','male','2013-05-01','tahir dd','rasheed aa','','parents','oldest','n/a','teacher','active','2013-05-20 08:56:18','2013-05-24 12:56:28'),(11,16,'66666','single',1,1,0,'0','1','','','','','','',1,'','male',NULL,'adf','adf','','adf','adfaf','adfaf','','discontinued','2013-05-20 14:21:45','2013-05-24 14:37:30'),(13,18,'66666','single',1,0,1,'1','0','03463511039','','20','4056','sindh','karachi',1,'','male','2013-05-07','new pt','pt','','parents','oldest','n/a','friend','active','2013-05-21 15:10:14','2013-06-04 12:03:55'),(14,19,'66666','single',1,0,1,'1','0','03463511039','','20','4056','sindh','karachi',1,'house no 123','male','2013-05-07','new pt','pt','','parents','oldest','n/a','friend','active','2013-05-21 15:11:53','2013-06-04 14:26:02'),(15,29,'12345','single',1,0,0,'','','','','','','','',1,'','male',NULL,'shan','jafri','raza','ss','2','dd','','temporary','2013-05-30 10:49:59','2013-05-30 10:50:00'),(16,34,'22222','single',1,0,0,'0','0','','','','','','',1,'','male',NULL,'new pt test','aa','aa','aa','aa','adf','','temporary','2013-06-04 12:58:47','2013-06-04 14:12:07');

/*Table structure for table `patient_family_details` */

DROP TABLE IF EXISTS `patient_family_details`;

CREATE TABLE `patient_family_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_detail_id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `work_number` varchar(50) DEFAULT NULL,
  `mobile_number` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `relation_ship_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patient_family_details_pd_id` (`patient_detail_id`),
  KEY `FK_patient_family_details_rs_id` (`relation_ship_id`),
  CONSTRAINT `patient_family_details_ibfk_1` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `patient_family_details_ibfk_2` FOREIGN KEY (`relation_ship_id`) REFERENCES `relation_ships` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `patient_family_details` */

insert into `patient_family_details` (`id`,`patient_detail_id`,`name`,`email`,`work_number`,`mobile_number`,`occupation`,`relation_ship_id`,`is_active`,`created`,`modified`) values (1,10,'test','aa@aa.com','222','979762','php',1,1,'2013-05-20 08:56:18','2013-05-24 12:56:28'),(2,10,'test','bb@aa.com','222','235454','testing',2,1,'2013-05-20 08:56:18','2013-05-24 12:56:28'),(3,13,'abc','aa@aa.com','222','979762','php',1,1,'2013-05-21 15:10:14','2013-06-04 12:03:55'),(4,14,'abc','aa@aa.com','222','979762','php',1,1,'2013-05-21 15:11:53','2013-06-04 14:26:02');

/*Table structure for table `patient_payment_logs` */

DROP TABLE IF EXISTS `patient_payment_logs`;

CREATE TABLE `patient_payment_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appointment_id` bigint(20) NOT NULL,
  `amout` decimal(5,3) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patient_payment_logs_app_id` (`appointment_id`),
  CONSTRAINT `patient_payment_logs_ibfk_1` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `patient_payment_logs` */

/*Table structure for table `patient_waiting_lists` */

DROP TABLE IF EXISTS `patient_waiting_lists`;

CREATE TABLE `patient_waiting_lists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `patient_detail_id` bigint(20) NOT NULL,
  `waiting_list_title_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patient_waiting_lists_pd_id` (`patient_detail_id`),
  KEY `FK_patient_waiting_lists_wlt_id` (`waiting_list_title_id`),
  CONSTRAINT `patient_waiting_lists_ibfk_1` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `patient_waiting_lists_ibfk_2` FOREIGN KEY (`waiting_list_title_id`) REFERENCES `waiting_lists_titles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `patient_waiting_lists` */

insert into `patient_waiting_lists` (`id`,`patient_detail_id`,`waiting_list_title_id`,`is_active`,`created`,`modified`) values (5,10,8,1,'2013-05-22 14:00:36','2013-05-22 14:00:36'),(6,10,9,1,'2013-05-22 14:03:25','2013-05-22 14:03:25'),(7,15,4,1,'2013-05-30 10:50:44','2013-05-30 10:50:44'),(8,13,3,1,'2013-05-30 14:58:10','2013-05-30 14:58:10'),(9,13,14,1,'2013-06-04 11:21:07','2013-06-04 11:21:07');

/*Table structure for table `programs` */

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `programs` */

insert into `programs` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA CCC',2130706433,1,'2013-05-07 11:19:44','2013-05-10 21:50:59'),(2,'JCCC',2130706433,1,'2013-05-07 11:25:15','2013-05-13 18:23:41');

/*Table structure for table `providers` */

DROP TABLE IF EXISTS `providers`;

CREATE TABLE `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `providers` */

insert into `providers` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA',2130706433,1,'2013-05-13 18:31:00','2013-05-13 18:31:00'),(2,'JISH',2130706433,1,'2013-05-13 18:31:37','2013-05-13 18:31:37'),(3,'HANEN',2130706433,1,'2013-05-13 18:31:44','2013-05-13 18:31:59');

/*Table structure for table `relation_ships` */

DROP TABLE IF EXISTS `relation_ships`;

CREATE TABLE `relation_ships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `relation_ships` */

insert into `relation_ships` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Father',2130706433,1,'2013-05-13 18:37:13','2013-05-13 18:37:13'),(2,'Mother',2130706433,1,'2013-05-13 18:37:20','2013-05-13 18:37:20'),(3,'Guardian',2130706433,1,'2013-05-13 18:37:27','2013-05-13 18:37:40');

/*Table structure for table `religions` */

DROP TABLE IF EXISTS `religions`;

CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `religions` */

insert into `religions` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Islam',2130706433,1,'2013-05-13 18:44:21','2013-05-13 18:46:10'),(2,'Hinduism',2130706433,1,'2013-05-13 18:45:04','2013-05-13 18:46:05'),(3,'Buddhism',2130706433,1,'2013-05-21 11:11:16','2013-05-21 11:11:16'),(4,'abc',2130706433,1,'2013-05-21 11:35:36','2013-05-21 11:35:36'),(5,'testing',2130706433,1,'2013-05-21 13:33:17','2013-05-21 13:33:17'),(6,'newreligion',2130706433,1,'2013-05-21 13:36:54','2013-05-21 13:36:54'),(7,'ggg',2130706433,1,'2013-05-21 13:41:21','2013-05-21 13:41:21'),(8,'yy',2130706433,1,'2013-05-21 13:49:43','2013-05-21 13:49:43'),(9,'zzz',2130706433,1,'2013-05-21 13:52:32','2013-05-21 13:52:32'),(10,'new',2130706433,1,'2013-05-21 14:09:03','2013-05-21 14:09:03');

/*Table structure for table `search_index` */

DROP TABLE IF EXISTS `search_index`;

CREATE TABLE `search_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `association_key` int(11) NOT NULL,
  `model` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `association_key` (`association_key`,`model`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `search_index` */

/*Table structure for table `sponsors` */

DROP TABLE IF EXISTS `sponsors`;

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `ip_address` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sponsors` */

insert into `sponsors` (`id`,`name`,`is_active`,`ip_address`,`created`,`modified`) values (1,'Sponsor Losem',1,2130706433,'2013-05-07 08:51:07','2013-05-10 21:50:48'),(2,'testing 123',0,2130706433,'2013-05-07 15:40:32','2013-05-07 15:56:43'),(3,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:56:47'),(4,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:45:00'),(5,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:46:45'),(6,'testing 123',0,2130706433,'2013-05-07 15:44:47','2013-05-07 15:46:48');

/*Table structure for table `tmps` */

DROP TABLE IF EXISTS `tmps`;

CREATE TABLE `tmps` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` varchar(50) NOT NULL COMMENT 'string field holding the unique ID of a record for e.g registration',
  `type` enum('registration','forgot_password','file_upload') NOT NULL,
  `data` mediumtext NOT NULL,
  `expire_at` date NOT NULL COMMENT 'Date on which the row will be expired / Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `tmps` */

insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (1,'5bd4ff00005569a4508825ad749e7032.jpg','file_upload','{\"name\":\"5bd4ff00005569a4508825ad749e7032.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/5bd4ff00005569a4508825ad749e7032.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/5bd4ff00005569a4508825ad749e7032.jpg\"}','2013-08-22','2013-05-22 20:16:55','0000-00-00 00:00:00'),(2,'a568ed5b18b26af6bb768fa7d6c4e032.jpg','file_upload','{\"name\":\"a568ed5b18b26af6bb768fa7d6c4e032.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/a568ed5b18b26af6bb768fa7d6c4e032.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/a568ed5b18b26af6bb768fa7d6c4e032.jpg\"}','2013-08-22','2013-05-22 20:18:07','0000-00-00 00:00:00'),(3,'5b8fe179175ce41ea4aac3cd11c901de.jpg','file_upload','{\"name\":\"5b8fe179175ce41ea4aac3cd11c901de.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/5b8fe179175ce41ea4aac3cd11c901de.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/5b8fe179175ce41ea4aac3cd11c901de.jpg\"}','2013-08-22','2013-05-22 20:21:11','0000-00-00 00:00:00'),(4,'7646f7d5f1135852239763d2c030cd31.jpg','file_upload','{\"name\":\"7646f7d5f1135852239763d2c030cd31.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/7646f7d5f1135852239763d2c030cd31.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/7646f7d5f1135852239763d2c030cd31.jpg\"}','2013-08-22','2013-05-22 20:21:36','0000-00-00 00:00:00'),(5,'014ecca9ed4906d5432672115131f601.','file_upload','{\"name\":\"014ecca9ed4906d5432672115131f601.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 21:31:49','0000-00-00 00:00:00'),(6,'63a867f123c68d1f6eb19299aba5cc0a.jpg','file_upload','{\"name\":\"63a867f123c68d1f6eb19299aba5cc0a.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/63a867f123c68d1f6eb19299aba5cc0a.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/63a867f123c68d1f6eb19299aba5cc0a.jpg\"}','2013-08-22','2013-05-22 21:53:48','0000-00-00 00:00:00'),(7,'96ab9a841c93af9353f294467afb5c9b.jpg','file_upload','{\"name\":\"96ab9a841c93af9353f294467afb5c9b.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/96ab9a841c93af9353f294467afb5c9b.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/96ab9a841c93af9353f294467afb5c9b.jpg\"}','2013-08-22','2013-05-22 21:58:58','0000-00-00 00:00:00'),(8,'735370bc08613dfd9d8fa2047a2e6f1f.jpg','file_upload','{\"name\":\"735370bc08613dfd9d8fa2047a2e6f1f.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/735370bc08613dfd9d8fa2047a2e6f1f.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/735370bc08613dfd9d8fa2047a2e6f1f.jpg\"}','2013-08-22','2013-05-22 22:00:06','0000-00-00 00:00:00'),(9,'8abf5144878fbcf7328b96cb2af510dc.','file_upload','{\"name\":\"8abf5144878fbcf7328b96cb2af510dc.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 22:03:38','0000-00-00 00:00:00'),(10,'c7d127109783ff0fa615b756359eecf2.jpg','file_upload','{\"name\":\"c7d127109783ff0fa615b756359eecf2.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/c7d127109783ff0fa615b756359eecf2.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/c7d127109783ff0fa615b756359eecf2.jpg\"}','2013-08-22','2013-05-22 22:07:19','0000-00-00 00:00:00'),(11,'4d142e88180bfcb9f89d138334963a94.','file_upload','{\"name\":\"4d142e88180bfcb9f89d138334963a94.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 22:09:39','0000-00-00 00:00:00'),(12,'fcabc789b79cd9e98e6df86bc1e46c3b.jpg','file_upload','{\"name\":\"fcabc789b79cd9e98e6df86bc1e46c3b.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/fcabc789b79cd9e98e6df86bc1e46c3b.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/fcabc789b79cd9e98e6df86bc1e46c3b.jpg\"}','2013-08-22','2013-05-22 22:28:15','0000-00-00 00:00:00'),(13,'c9c6b685ab04448be7ada34041ad4d9d.jpg','file_upload','{\"name\":\"c9c6b685ab04448be7ada34041ad4d9d.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/c9c6b685ab04448be7ada34041ad4d9d.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/c9c6b685ab04448be7ada34041ad4d9d.jpg\"}','2013-08-22','2013-05-22 22:31:04','0000-00-00 00:00:00'),(14,'00cdbb91091473fdde083a983c4a1943.jpg','file_upload','{\"name\":\"00cdbb91091473fdde083a983c4a1943.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/00cdbb91091473fdde083a983c4a1943.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/00cdbb91091473fdde083a983c4a1943.jpg\"}','2013-08-23','2013-05-23 14:01:45','0000-00-00 00:00:00'),(15,'cbcb89006dd76717c45bcd4f165773d6.jpg','file_upload','{\"name\":\"cbcb89006dd76717c45bcd4f165773d6.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/cbcb89006dd76717c45bcd4f165773d6.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/cbcb89006dd76717c45bcd4f165773d6.jpg\"}','2013-08-23','2013-05-23 14:31:50','0000-00-00 00:00:00'),(16,'702b8521ca44529a8b23995222a0dcf8.png','file_upload','{\"name\":\"702b8521ca44529a8b23995222a0dcf8.png\",\"size\":34952,\"type\":\"image\\/png\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/702b8521ca44529a8b23995222a0dcf8.png\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/702b8521ca44529a8b23995222a0dcf8.png\"}','2013-08-27','2013-05-27 20:14:51','0000-00-00 00:00:00'),(17,'26c4b20e25eb19e401e0156189be5e90.JPG','file_upload','{\"name\":\"26c4b20e25eb19e401e0156189be5e90.JPG\",\"size\":6940,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/26c4b20e25eb19e401e0156189be5e90.JPG\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/26c4b20e25eb19e401e0156189be5e90.JPG\"}','2013-08-27','2013-05-27 20:16:39','0000-00-00 00:00:00'),(18,'3997fbc47c102262e635988bf5798c6a.jpeg','file_upload','{\"name\":\"3997fbc47c102262e635988bf5798c6a.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/3997fbc47c102262e635988bf5798c6a.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/3997fbc47c102262e635988bf5798c6a.jpeg\"}','2013-08-27','2013-05-27 20:26:17','0000-00-00 00:00:00'),(19,'1997f6f675454c7ca0c35f79fed19b81.jpeg','file_upload','{\"name\":\"1997f6f675454c7ca0c35f79fed19b81.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/1997f6f675454c7ca0c35f79fed19b81.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/1997f6f675454c7ca0c35f79fed19b81.jpeg\"}','2013-08-27','2013-05-27 20:28:22','0000-00-00 00:00:00'),(20,'31109e56c71550d59086f75a64e57a87.jpg','file_upload','{\"name\":\"31109e56c71550d59086f75a64e57a87.jpg\",\"size\":50903,\"type\":\"image\\/jpeg\",\"code\":\"0\"}','2013-08-28','2013-05-28 16:25:15','0000-00-00 00:00:00'),(24,'7913aa5aac3f455b4fff321ea1356de2.JPG','file_upload','{\"name\":\"7913aa5aac3f455b4fff321ea1356de2.JPG\",\"size\":26517,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/7913aa5aac3f455b4fff321ea1356de2.JPG\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/7913aa5aac3f455b4fff321ea1356de2.JPG\"}','2013-08-28','2013-05-28 17:16:28','0000-00-00 00:00:00'),(25,'f81fffc3bf56ab52adf117c07b185e4e.jpg','file_upload','{\"name\":\"f81fffc3bf56ab52adf117c07b185e4e.jpg\",\"size\":10962,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/f81fffc3bf56ab52adf117c07b185e4e.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/f81fffc3bf56ab52adf117c07b185e4e.jpg\"}','2013-09-03','2013-06-03 14:57:19','0000-00-00 00:00:00'),(26,'a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg','file_upload','{\"name\":\"a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\"}','2013-09-04','2013-06-04 19:18:38','0000-00-00 00:00:00');

/*Table structure for table `user_languages` */

DROP TABLE IF EXISTS `user_languages`;

CREATE TABLE `user_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_languages_u_id` (`user_id`),
  KEY `FK_users_languages_lang_id` (`language_id`),
  CONSTRAINT `user_languages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Data for the table `user_languages` */

insert into `user_languages` (`id`,`user_id`,`language_id`,`is_active`,`created`,`modified`) values (21,28,1,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(22,28,2,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(23,28,3,0,'2013-05-30 22:09:42','2013-05-30 22:12:02'),(24,28,1,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(25,28,2,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(26,28,3,1,'2013-05-30 22:12:02','2013-05-30 22:12:02'),(27,30,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(28,30,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(29,31,1,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(30,31,2,1,'2013-06-03 15:02:01','2013-06-03 15:02:01'),(31,32,1,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(32,32,2,1,'2013-06-03 15:03:56','2013-06-03 15:03:56'),(33,33,1,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(34,33,2,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(35,33,3,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(36,33,1,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(37,33,2,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(38,33,3,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(39,35,1,1,'2013-06-04 19:46:17','2013-06-04 19:46:17'),(40,35,2,1,'2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) unsigned NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `type_id` enum('facebook','twitter','linkedin') NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `access_token_secret` varchar(255) DEFAULT NULL,
  `is_valid_token` tinyint(1) NOT NULL DEFAULT '1',
  `screen_name` varchar(255) NOT NULL,
  `extra` mediumtext COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') NOT NULL DEFAULT 'active',
  `ip_address` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_social_accounts` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT 'username is basically email',
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_type` enum('front_desk','patient','doctor') NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `status` enum('active','disabled','deleted','suspended') NOT NULL DEFAULT 'active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_grp_id` (`group_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert into `users` (`id`,`username`,`password`,`group_id`,`user_type`,`ip_address`,`status`,`created`,`modified`) values (1,'shakeel@test.com','68c91a594329677d6041c1c899c85272',1,'doctor',2130706433,'active','2013-05-06 16:26:39','2013-05-06 16:26:39'),(2,'test@test.com','68c91a594329677d6041c1c899c85272',1,'doctor',2130706433,'active','2013-05-06 16:26:39','2013-05-06 16:26:39'),(5,'tr.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-17 10:46:22','2013-05-23 15:06:32'),(6,'tr2.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-17 11:23:09','2013-05-22 09:14:16'),(13,'tr4.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-20 08:56:18','2013-05-24 12:56:28'),(16,'aa@aa.com','',1,'patient',2130706433,'active','2013-05-20 14:21:45','2013-05-23 08:58:40'),(18,'tr5.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-21 15:10:14','2013-06-04 12:03:55'),(19,'tr6.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-21 15:11:53','2013-06-04 14:26:02'),(23,'ahmed@gmail.com','68c91a594329677d6041c1c899c85272',2,'doctor',2130706433,'active','2013-05-23 14:47:17','2013-05-28 22:57:53'),(25,'lady@gmail.com','4d07e388d264b1eeab113757b2491554',4,'doctor',2130706433,'active','2013-05-28 16:29:46','2013-05-28 16:29:46'),(26,'ladies@gmail.com','05bd4ce86483b4d2df4ee79e9ad6bb05',4,'doctor',2130706433,'active','2013-05-28 16:34:56','2013-05-28 16:34:56'),(27,'tes1t@test.com','73e512550302e76dfe791bd24f975f02',4,'doctor',2130706433,'active','2013-05-28 16:37:54','2013-05-28 16:37:54'),(28,'aa@test.com','ea2ae67d7ed0153d9cb769224d55eb31',4,'doctor',2130706433,'active','2013-05-28 17:17:24','2013-05-30 22:12:02'),(29,'a@a.com','',1,'patient',2130706433,'active','2013-05-30 10:49:59','2013-05-30 10:50:00'),(30,'aaaqa@gmail.com','1ff90edf3d7370660df93008e2f59cd7',4,'doctor',2130706433,'active','2013-06-03 15:00:12','2013-06-03 15:00:12'),(31,'dsasadia@gmail.com','417a776fe25ffed1e43c82d39f57c409',4,'doctor',2130706433,'active','2013-06-03 15:02:01','2013-06-03 15:02:01'),(32,'qwera@gmail.com','1f11f2d56207ef05f93d2d5691147b0b',4,'doctor',2130706433,'active','2013-06-03 15:03:56','2013-06-03 15:03:56'),(33,'anasanjaria@gmail.com','5d94159de25c785c97373f56890cf9fe',4,'doctor',2130706433,'active','2013-06-03 15:08:51','2013-06-03 15:49:56'),(34,'trtest.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-06-04 12:58:47','2013-06-04 14:12:07'),(35,'losem@losem.com','69d0608b1b4ec997efce64a82cd3ade4',4,'doctor',2130706433,'active','2013-06-04 19:46:17','2013-06-04 19:46:17');

/*Table structure for table `vpd_activities` */

DROP TABLE IF EXISTS `vpd_activities`;

CREATE TABLE `vpd_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_detail_id` bigint(20) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `vpd_activity_type_id` int(11) DEFAULT NULL,
  `modality_id` int(11) DEFAULT NULL,
  `status` enum('pending','complete') DEFAULT NULL,
  `sponsor_id` int(11) DEFAULT NULL,
  `date_complition` datetime DEFAULT NULL,
  `no_of_units` int(11) DEFAULT NULL,
  `activity_name` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_vpd_activities_ed_id` (`employee_detail_id`),
  KEY `FK_vpd_activities_pdr_id` (`provider_id`),
  KEY `FK_vpd_activities_vpd_at_id` (`vpd_activity_type_id`),
  KEY `FK_vpd_activities_mod_id` (`modality_id`),
  KEY `FK_vpd_activities_spn_id` (`sponsor_id`),
  CONSTRAINT `vpd_activities_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `vpd_activities_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`),
  CONSTRAINT `vpd_activities_ibfk_3` FOREIGN KEY (`vpd_activity_type_id`) REFERENCES `vpd_activity_types` (`id`),
  CONSTRAINT `vpd_activities_ibfk_4` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`),
  CONSTRAINT `vpd_activities_ibfk_5` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `vpd_activities` */

insert into `vpd_activities` (`id`,`employee_detail_id`,`provider_id`,`vpd_activity_type_id`,`modality_id`,`status`,`sponsor_id`,`date_complition`,`no_of_units`,`activity_name`,`is_active`,`created`,`modified`) values (2,4,1,1,1,'pending',1,'2013-05-31 00:00:00',2,'testing activity a',1,'2013-05-29 14:16:30','2013-05-29 14:16:30');

/*Table structure for table `vpd_activity_types` */

DROP TABLE IF EXISTS `vpd_activity_types`;

CREATE TABLE `vpd_activity_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `ip_address` int(10) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `vpd_activity_types` */

insert into `vpd_activity_types` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'test vpd edit',2130706433,0,'2013-05-07 13:18:07','2013-05-07 13:23:48'),(2,'CBUs',2130706433,1,'2013-05-07 13:23:57','2013-05-10 21:52:22');

/*Table structure for table `waiting_lists_titles` */

DROP TABLE IF EXISTS `waiting_lists_titles`;

CREATE TABLE `waiting_lists_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_waiting_lists_titles_div_id` (`division_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `waiting_lists_titles` */

insert into `waiting_lists_titles` (`id`,`title`,`description`,`division_id`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Waiting list edit','testing description',15,2130706433,1,'2013-05-14 12:33:30','2013-05-14 13:54:38'),(2,'my testing','this is other waiting list',11,2130706433,0,'2013-05-14 13:55:00','2013-05-14 13:55:14'),(3,'my testing','adfaf',8,2130706433,1,'2013-05-15 11:01:07','2013-05-15 11:01:07'),(4,'dd','adf',4,2130706433,1,'2013-05-15 11:01:16','2013-05-15 11:01:16'),(5,'gg','adaf',9,2130706433,1,'2013-05-15 11:01:40','2013-05-15 11:01:40'),(6,'rr','adaf',8,2130706433,1,'2013-05-15 11:01:59','2013-05-15 11:01:59'),(7,'jj','adfaf',4,2130706433,1,'2013-05-15 11:02:18','2013-05-15 11:02:18'),(8,'aa','',8,2130706433,1,'2013-05-15 11:02:55','2013-05-15 11:02:55'),(9,'aa','adfaf',8,2130706433,1,'2013-05-15 11:03:05','2013-05-15 11:03:05'),(11,'dd','ad',0,2130706433,1,'2013-05-21 08:33:22','2013-05-21 09:08:46'),(14,'new list','',0,2130706433,1,'2013-06-04 11:21:07','2013-06-04 11:21:07');

/* Procedure structure for procedure `common_insert_multi_params` */

drop procedure if exists `common_insert_multi_params`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `common_insert_multi_params`(
   IN tblname VARCHAR(250),
   IN in_string_array VARCHAR(2000),
   IN in_array_count INT,
   OUT `status` INT
)
BEGIN
	DECLARE tmp_key_value_pair VARCHAR(255);
	DECLARE tmp_key VARCHAR(255);
	DECLARE tmp_value TEXT;
	DECLARE counter INT DEFAULT 0;
	
	set @q = concat("insert into ",tblname," set ");
	simple_loop: LOOP
		SET counter = counter + 1;
		SELECT strSplit(in_string_array, '|', counter) INTO tmp_key_value_pair;
		SELECT strSplit(tmp_key_value_pair, '=', 1) INTO tmp_key;
		SELECT strSplit(tmp_key_value_pair, '=', 2) INTO tmp_value;
		
		IF counter = in_array_count THEN
			set @q = concat(@q,tmp_key,"='",tmp_value,"'");
		      -- break out since we have done parsing
		      -- the 2 elements in our example
		       LEAVE simple_loop;
		else
			set @q = concat(@q,tmp_key,"='",tmp_value,"',");
		END IF;
		
	END LOOP simple_loop;
        SELECT @q;
	#PREPARE stmt FROM @q;
	#EXECUTE stmt;
	#DEALLOCATE PREPARE stmt;
	#SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_allowed_services` */

drop procedure if exists `create_allowed_services`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_allowed_services`(in name varchar(100), in description text,in allowed_service_group_id int,in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @name = name;
set @description = description;
set @allowed_service_group_id = allowed_service_group_id;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into allowed_services(name,description,allowed_service_group_id,ip_address,created,modified) values (?,?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name,@description,@allowed_service_group_id, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_common` */

drop procedure if exists `create_common`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_common`(in tbl_name varchar(100), in name VARCHAR(100),in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @name = name;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into ",tbl_name,"(name,ip_address,created,modified) values (?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_common_with_col_name` */

drop procedure if exists `create_common_with_col_name`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_common_with_col_name`(in tbl_name varchar(100),in col_name varchar(100), in name VARCHAR(100),in ip_address int, in created datetime,IN description VARCHAR(50),OUT `status` INT)
BEGIN
set @name = name;
set @description = description;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into ",tbl_name,"(name,",col_name,",ip_address,created,modified) values (?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name,@description,@ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_feature` */

drop procedure if exists `create_feature`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_feature`(IN description VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into features(
	features.description,
	features.ip_address,
	features.created,
	features.modified
) values (
	description,
	ip_address,
	created,
	created 
);
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_group` */

drop procedure if exists `create_group`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_group`(IN name VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into groups(
	groups.name,
	groups.ip_address,
	groups.created,
	groups.modified
) values (
	name,
	ip_address,
	created,
	created 
);
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_user` */

drop procedure if exists `create_user`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user`(IN `first_name` VARCHAR(255), IN `last_name` VARCHAR(255), IN `username` VARCHAR(50), IN `password` VARCHAR(255), IN `group_id` INT(11), IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
INSERT INTO users
(
	users.first_name                  , 
	users.last_name                   , 
	users.username                    , 
	users.password                    ,
	users.group_id                    ,
	users.ip_address                  ,
	users.created	             ,
	users.modified
)
VALUES 
( 
	first_name                       , 
	last_name                        , 
	username                         ,  
	password                         ,
	group_id                         ,
	ip_address                       ,
	created                          ,
	created
) ; 
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `create_waiting_list_title` */

drop procedure if exists `create_waiting_list_title`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_waiting_list_title`(in title varchar(100), in description text,in division_id int,in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @title = title;
set @description = description;
set @division_id = division_id;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into waiting_lists_titles(title,description,division_id,ip_address,created,modified) values (?,?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @title,@description,@division_id, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `get_all_arosacos_by_group_id` */

drop procedure if exists `get_all_arosacos_by_group_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_arosacos_by_group_id`(IN group_id INT(10))
BEGIN
SELECT 	ArosAco.id,ArosAco.aco_id
FROM groups AS `Group`
LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
LEFT JOIN `aros_acos` AS ArosAco ON ArosAco.aro_id = Aro.id
WHERE Group.id = group_id;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_all_common` */

drop procedure if exists `get_all_common`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_common`(IN tbl_name VARCHAR(25),IN model_name VARCHAR(25))
BEGIN	
	set @q = concat("SELECT " , model_name , ".id, ", model_name , ".name"," FROM " , tbl_name , " AS " , model_name , " WHERE " , model_name , ".is_active = 1");
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_all_permissions` */

drop procedure if exists `get_all_permissions`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_permissions`()
BEGIN
	SELECT 
		Child.id,Child.alias,
		AuthActionMap.description,AuthActionMap.id,
		Feature.id,Feature.description,
		Parent.id,Parent.alias
	FROM `acos` AS Child
	LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.aco_id = Child.id
	LEFT JOIN features AS Feature ON Feature.id = AuthActionMap.feature_id AND Feature.is_active = 1
	WHERE `Parent`.`parent_id` IS NOT NULL;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_common_by_common_id` */

drop procedure if exists `get_common_by_common_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_common_by_common_id`(IN common_id INT(11),IN tbl_name VARCHAR(25),IN model_name VARCHAR(25))
BEGIN
	set @common_id = common_id;
	set @q = concat("SELECT " , model_name , ".* FROM " , tbl_name , " AS " , model_name , " WHERE " , model_name , ".id =?");
	PREPARE stmt FROM @q;
	EXECUTE stmt USING @common_id ;
	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_config_option_by_name` */

drop procedure if exists `get_config_option_by_name`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_config_option_by_name`(IN name VARCHAR(50))
BEGIN
	select 	ConfigOption.id, ConfigOption.name, ConfigOption.value, ConfigOption.created
	from config_options AS ConfigOption
	WHERE ConfigOption.name = name AND ConfigOption.is_active = 1;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_doctor_details_by_user_id` */

drop procedure if exists `get_doctor_details_by_user_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_doctor_details_by_user_id`(IN `user_id` BIGINT(20))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,		
		EmployeeDetail.*,
		Language.name,Language.id,Language.created,
		AllowedService.name,AllowedService.id,AllowedService.created,
		CaseLoad.name,CaseLoad.id,CaseLoad.created,
		Division.name,Division.id,Division.created,		
		Level.name,Level.id,Level.created,
		OfficialTitle.name,OfficialTitle.id,OfficialTitle.created,
		LicenseType.name,LicenseType.id,LicenseType.created,LicenseType.is_active
		
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'	
	LEFT JOIN license_types AS LicenseType ON LicenseType.id = EmployeeDetail.license_type_id AND LicenseType.is_active = 1
	LEFT JOIN user_languages AS `UserLanguage` ON UserLanguage.user_id = User.id AND UserLanguage.is_active = 1
	LEFT JOIN languages AS `Language` ON Language.id = UserLanguage.language_id AND Language.is_active = 1
	LEFT JOIN employee_allowed_services AS EmployeeAllowedService ON EmployeeAllowedService.employee_detail_id = EmployeeDetail.id AND EmployeeAllowedService.is_active = 1
	LEFT JOIN allowed_services AS AllowedService ON AllowedService.id = EmployeeAllowedService.allowed_service_id AND AllowedService.is_active = 1
	LEFT JOIN employee_case_loads AS EmployeeCaseLoad ON EmployeeCaseLoad.employee_detail_id = EmployeeDetail.id AND EmployeeCaseLoad.is_active = 1
	LEFT JOIN case_loads AS CaseLoad ON CaseLoad.id = EmployeeCaseLoad.case_load_id AND CaseLoad.is_active = 1
	LEFT JOIN employee_divisions AS EmployeeDivision ON EmployeeDivision.employee_detail_id = EmployeeDetail.id AND EmployeeDivision.is_active = 1
	LEFT JOIN divisions AS Division ON Division.id = EmployeeDivision.division_id AND Division.is_active = 1
	
	LEFT JOIN employee_levels AS EmployeeLevel ON EmployeeLevel.employee_detail_id = EmployeeDetail.id AND EmployeeLevel.is_active = 1
	LEFT JOIN levels AS Level ON Level.id = EmployeeLevel.level_id AND Level.is_active = 1
	LEFT JOIN employee_official_titles AS EmployeeOfficialTitle ON EmployeeOfficialTitle.employee_detail_id = EmployeeDetail.id AND EmployeeOfficialTitle.is_active = 1
	LEFT JOIN official_titles AS OfficialTitle ON OfficialTitle.id = EmployeeOfficialTitle.official_title_id AND OfficialTitle.is_active = 1
	
	WHERE `User`.id = user_id AND `User`.`status` = 'active' AND User.user_type = 'doctor';
END$$

DELIMITER ;

/* Procedure structure for procedure `get_doctor_working_hours_by_user_id` */

drop procedure if exists `get_doctor_working_hours_by_user_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_doctor_working_hours_by_user_id`(IN `user_id` BIGINT(20))
    NO SQL
BEGIN 
	SELECT 		
		EmployeeDetail.id,
		Division.name,Division.id,Division.created,		
		EmployeeWorkingHour.id,EmployeeWorkingHour.working_day,EmployeeWorkingHour.start_time,EmployeeWorkingHour.end_time,EmployeeWorkingHour.total_possible_slots,EmployeeWorkingHour.created
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'	
	LEFT JOIN employee_divisions AS EmployeeDivision ON EmployeeDivision.employee_detail_id = EmployeeDetail.id AND EmployeeDivision.is_active = 1
	LEFT JOIN divisions AS Division ON Division.id = EmployeeDivision.division_id AND Division.is_active = 1
	LEFT JOIN employee_working_hours AS EmployeeWorkingHour ON EmployeeWorkingHour.employee_detail_id = EmployeeDetail.id AND EmployeeWorkingHour.is_active = 1 AND EmployeeWorkingHour.division_id = Division.id	
	WHERE `User`.id = user_id AND `User`.`status` = 'active' AND User.user_type = 'doctor' AND EmployeeWorkingHour.id > 0 AND Division.id > 0;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_employee_details_by_user_id` */

drop procedure if exists `get_employee_details_by_user_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_employee_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		
		EmployeeDetail.*,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END$$

DELIMITER ;

/* Procedure structure for procedure `get_feature_by_feature_id` */

drop procedure if exists `get_feature_by_feature_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_feature_by_feature_id`(IN feature_id INT(11))
BEGIN
select 	Feature.id, Feature.description, Feature.created
from `features` AS Feature
WHERE Feature.id = feature_id;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_feature_details_by_feature_id` */

drop procedure if exists `get_feature_details_by_feature_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_feature_details_by_feature_id`(IN feature_id INT(11))
BEGIN
	SELECT 	
		Feature.id,Feature.description,Feature.created,
		AuthActionMap.id,AuthActionMap.description,AuthActionMap.created
	FROM features AS Feature
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.feature_id = Feature.id
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_group_by_group_id` */

drop procedure if exists `get_group_by_group_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_group_by_group_id`(IN group_id INT(11))
BEGIN
	select 	Group.id, Group.name, Group.created
	from `groups` AS `Group`
	WHERE Group.id = group_id;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_group_details_by_group_id` */

drop procedure if exists `get_group_details_by_group_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_group_details_by_group_id`(IN group_id INT(11))
BEGIN
	SELECT 	
		Group.id,Group.name,Group.created,
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		EmployeeDetail.first_name,EmployeeDetail.last_name,EmployeeDetail.created,
		PatientDetail.first_name,PatientDetail.last_name,PatientDetail.created
	FROM groups AS `Group`
	LEFT JOIN users AS `User` ON User.group_id = Group.id AND User.status = 'active'
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id
	LEFT JOIN patient_details AS PatientDetail ON PatientDetail.user_id = User.id 
	WHERE Group.id = group_id AND Group.is_active = 1 AND User.id > 0;
END$$

DELIMITER ;

/* Procedure structure for procedure `get_patient_details_by_user_id` */

drop procedure if exists `get_patient_details_by_user_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_patient_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		
		PatientDetail.*,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN patient_details AS PatientDetail ON PatientDetail.user_id = User.id AND PatientDetail.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END$$

DELIMITER ;

/* Procedure structure for procedure `get_user_by_email` */

drop procedure if exists `get_user_by_email`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_by_email`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
FROM users AS `User` 
WHERE User.username = email AND User.status = 'active'; 
END$$

DELIMITER ;

/* Procedure structure for procedure `get_user_details_by_user_id` */

drop procedure if exists `get_user_details_by_user_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,
		
		UserSocialAccount.id, UserSocialAccount.link_id, UserSocialAccount.email_address, UserSocialAccount.image_url, 
		UserSocialAccount.type_id,UserSocialAccount.is_valid_token, UserSocialAccount.screen_name, UserSocialAccount.status, 
		UserSocialAccount.created,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN user_social_accounts AS UserSocialAccount ON UserSocialAccount.user_id = `User`.id AND UserSocialAccount.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END$$

DELIMITER ;

/* Procedure structure for procedure `get_user_tmp_data` */

drop procedure if exists `get_user_tmp_data`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_tmp_data`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.*
FROM tmps AS Tmp 
WHERE Tmp.target_id = target_id AND Tmp.type = type; 
END$$

DELIMITER ;

/* Procedure structure for procedure `is_unverified_email_exists` */

drop procedure if exists `is_unverified_email_exists`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.target_id
FROM tmps AS Tmp
WHERE Tmp.target_id = email AND Tmp.type = 'registration';
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_all_common_by_field_name` */

drop procedure if exists `remove_all_common_by_field_name`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_all_common_by_field_name`(IN tbl_name varchar(100),IN field_value INT, IN modified DATETIME, IN field_name VARCHAR(20),OUT `status` INT)
BEGIN
	
	set @numQ = concat("SELECT COUNT(*) INTO @num FROM ",tbl_name," AS `tbl` WHERE `tbl`.`", field_name ,"` = ",field_value," AND tbl.is_active = 1");
	
	PREPARE stmt1 FROM @numQ;
	EXECUTE stmt1 ;
	DEALLOCATE PREPARE stmt1;
	
	if(@num>0)THEN
	set @q = concat("UPDATE ",tbl_name," SET 		
		is_active = 0  , 		
		modified = '" , modified ,"'
		WHERE ", field_name ," = " , field_value );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;
	else
	set `status` = 0;
	END IF;
		
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_arosacos_by_group_id` */

drop procedure if exists `remove_arosacos_by_group_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_arosacos_by_group_id`(IN group_id INT(11) , OUT `status` INT)
BEGIN
	DELETE FROM `aros_acos` 
	WHERE aros_acos. aro_id = (
		SELECT 	Aro.id
		FROM groups AS `Group`
		LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
		WHERE Group.id = group_id
	);
SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_common` */

drop procedure if exists `remove_common`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_common`(IN tbl_name varchar(100),IN id INT,IN ip_address INT, IN modified DATETIME,	OUT `status` INT)
BEGIN
	
	set @numQ = concat("SELECT COUNT(*) INTO @num FROM ",tbl_name," AS `tbl` WHERE `tbl`.`id` = ",id," AND tbl.is_active = 1");
	
	PREPARE stmt1 FROM @numQ;
	EXECUTE stmt1 ;
	DEALLOCATE PREPARE stmt1;
	
	if(@num>0)THEN
	set @q = concat("UPDATE ",tbl_name," SET 		
		is_active = 0  , 
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;
	else
	set `status` = 0;
	END IF;
		
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_feature` */

drop procedure if exists `remove_feature`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_feature`(
	IN feature_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `features` AS `Feature` WHERE `Feature`.`id` = feature_id AND Feature.is_active = 1) THEN	
		UPDATE `features` AS Feature
		SET Feature.is_active = 0,
		Feature.modified = modified,
		Feature.ip_address = ip_address
		WHERE Feature.id = feature_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_group` */

drop procedure if exists `remove_group`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_group`(
	IN group_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `groups` AS `Group` WHERE `Group`.`id` = group_id AND Group.is_active = 1) THEN	
		UPDATE `groups` AS `Group`
		SET Group.is_active = 0,
		Group.modified = modified,
		Group.ip_address = ip_address
		WHERE Group.id = group_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_tmp_record_by_id` */

drop procedure if exists `remove_tmp_record_by_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_tmp_record_by_id`(IN `id` INT(5),OUT `status` INT)
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.id = id ; 
SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `remove_tmp_record_by_target_id` */

drop procedure if exists `remove_tmp_record_by_target_id`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_tmp_record_by_target_id`(IN `target_id` VARCHAR(50),OUT `status` INT)
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.target_id = target_id ; 
SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `reset_password` */

drop procedure if exists `reset_password`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5), OUT `status` INT)
    NO SQL
BEGIN
    UPDATE users
    SET    
           users.password = `password`                    
    WHERE  users.id = id;
SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `save_tmp_record` */

drop procedure if exists `save_tmp_record`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 
    INSERT INTO tmps
         (
           tmps.target_id                  , 
           tmps.type                       , 
           tmps.data                       , 
           tmps.expire_at                  ,
           tmps.created
         )
    VALUES 
         ( 
           target_id                    , 
           type                         , 
           data                         ,  
           expire_at                    ,
           created
          
         ) ; 
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `signup_with_social_account` */

drop procedure if exists `signup_with_social_account`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `signup_with_social_account`(IN xml_data varchar(1000), OUT `status` INT)
BEGIN
DECLARE user_id BIGINT(11);
DECLARE first_name VARCHAR(255);
DECLARE last_name VARCHAR(255);
DECLARE username VARCHAR(50); 
DECLARE pwd VARCHAR(255);
DECLARE group_id INT(11);
DECLARE ip_address  INT(10); 
DECLARE created DATETIME; 
DECLARE link_id BIGINT(11); 
DECLARE image_url VARCHAR(255);
DECLARE type_id VARCHAR(20);
DECLARE access_token VARCHAR(255);
DECLARE screen_name VARCHAR(255);
SELECT ExtractValue(xml_data, '/response/User/first_name') INTO first_name;
SELECT ExtractValue(xml_data, '/response/User/last_name') INTO last_name;
SELECT ExtractValue(xml_data, '/response/User/username') INTO username;
SELECT ExtractValue(xml_data, '/response/User/password') INTO pwd;
SELECT ExtractValue(xml_data, '/response/User/group_id') INTO group_id;
SELECT ExtractValue(xml_data, '/response/User/ip_address') INTO ip_address;
SELECT ExtractValue(xml_data, '/response/User/created') INTO created;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/link_id') INTO link_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/image_url') INTO image_url;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/type_id') INTO type_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/access_token') INTO access_token;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/screen_name') INTO screen_name;
call `create_user`(first_name,last_name,username,pwd,group_id,created,ip_address,@status);
SELECT @status INTO user_id;
INSERT INTO `user_social_accounts` 
(	
	user_social_accounts.user_id,
	user_social_accounts.link_id,
	user_social_accounts.email_address,
	user_social_accounts.image_url,
	user_social_accounts.type_id,
	user_social_accounts.access_token,
	user_social_accounts.screen_name,
	user_social_accounts.ip_address,
	user_social_accounts.created,
	user_social_accounts.modified
)values (
	user_id,
	link_id,
	username,
	image_url,
	type_id,
	access_token,
	screen_name,
	ip_address,
	created,
	created
);
SET `status` = LAST_INSERT_ID();
END$$

DELIMITER ;

/* Procedure structure for procedure `sync_aam_aco` */

drop procedure if exists `sync_aam_aco`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sync_aam_aco`(OUT `status` INT)
BEGIN
DELETE FROM auth_action_maps WHERE NOT aco_id IN ( SELECT Aco.id FROM `acos` AS Aco );
SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `test_multi_sets` */

drop procedure if exists `test_multi_sets`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end$$

DELIMITER ;

/* Procedure structure for procedure `update_allowed_services` */

drop procedure if exists `update_allowed_services`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_allowed_services`(in name varchar(100), 
	in description text,
	in allowed_service_group_id int,
	in ip_address int, 
	in modified datetime,
	IN id INT,
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE allowed_services SET 
		name = '" , name ,"',
		description = '" , description ,"',
		allowed_service_group_id = '" , allowed_service_group_id ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END$$

DELIMITER ;

/* Procedure structure for procedure `update_common` */

drop procedure if exists `update_common`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_common`(
	IN tbl_name varchar(100),
	IN id INT, 
	IN name VARCHAR(100),
	IN ip_address INT, 
	IN modified DATETIME, 	
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE ",tbl_name," SET 
		name = '" , name ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END$$

DELIMITER ;

/* Procedure structure for procedure `update_common_with_col_name` */

drop procedure if exists `update_common_with_col_name`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_common_with_col_name`(
	IN tbl_name varchar(100),
	IN col_name varchar(100),
	IN id INT, 
	IN name VARCHAR(100),
	IN ip_address INT, 
	IN modified DATETIME,
	IN description VARCHAR(50), 	
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE ",tbl_name," SET 
		name = '" , name ,"',
		",col_name," = '" , description ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END$$

DELIMITER ;

/* Procedure structure for procedure `update_feature` */

drop procedure if exists `update_feature`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_feature`(
	IN feature_id INT(11),
	IN description VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `features` AS Feature
	SET Feature.description = description,
	Feature.modified = modified,
	Feature.ip_address = ip_address
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
	SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `update_group` */

drop procedure if exists `update_group`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_group`(
	IN group_id INT(11),
	IN name VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `groups` AS `Group`
	SET Group.name = name,
	Group.modified = modified,
	Group.ip_address = ip_address
	WHERE Group.id = group_id AND Group.is_active = 1;
	SET `status` = ROW_COUNT();
END$$

DELIMITER ;

/* Procedure structure for procedure `update_waiting_list_title` */

drop procedure if exists `update_waiting_list_title`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_waiting_list_title`(in title varchar(100), 
	in description text,
	in division_id int,
	in ip_address int, 
	in modified datetime,
	IN id INT,
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE waiting_lists_titles SET 
		title = '" , title ,"',
		description = '" , description ,"',
		division_id = '" , division_id ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END$$

DELIMITER ;

/* Procedure structure for procedure `verify_login` */

drop procedure if exists `verify_login`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.user_type
FROM users AS `User` 
WHERE User.username = email AND User.password = pass AND User.status = 'active'; 
END$$

DELIMITER ;

/* Function  structure for function  `strSplit` */

drop function  if exists `strSplit`;

DELIMITER $$

CREATE DEFINER=`root`@`192.175.0.%` FUNCTION `strSplit`(x varchar(255), delim varchar(12), pos int) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
   RETURN replace(substring(substring_index(x, delim, pos), 
      length(substring_index(x, delim, pos - 1)) + 1), delim, '');
	
-- end the stored function code block
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
