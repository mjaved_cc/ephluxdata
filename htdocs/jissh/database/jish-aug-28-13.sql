/*
SQLyog Ultimate v8.55 
MySQL - 5.0.45-community-nt : Database - jish
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=latin1;

/*Data for the table `acos` */

insert  into `acos`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,NULL,NULL,'controllers',1,520),(2,1,NULL,NULL,'AuthActionMaps',2,17),(3,2,NULL,NULL,'index',3,4),(4,2,NULL,NULL,'view',5,6),(5,2,NULL,NULL,'add',7,8),(6,2,NULL,NULL,'edit',9,10),(7,2,NULL,NULL,'delete',11,12),(16,2,NULL,NULL,'sync_action_maps',13,14),(17,1,NULL,NULL,'Demos',18,45),(21,17,NULL,NULL,'facebook_connect',19,20),(25,17,NULL,NULL,'twitter_connect',21,22),(27,17,NULL,NULL,'signup',23,24),(28,17,NULL,NULL,'verify_account',25,26),(30,17,NULL,NULL,'forgot_password',27,28),(31,17,NULL,NULL,'reset_account_password',29,30),(40,1,NULL,NULL,'Groups',46,61),(41,40,NULL,NULL,'index',47,48),(42,40,NULL,NULL,'view',49,50),(43,40,NULL,NULL,'add',51,52),(44,40,NULL,NULL,'edit',53,54),(45,40,NULL,NULL,'delete',55,56),(54,1,NULL,NULL,'Users',62,75),(55,54,NULL,NULL,'index',63,64),(56,54,NULL,NULL,'view',65,66),(57,54,NULL,NULL,'add',67,68),(58,54,NULL,NULL,'edit',69,70),(59,54,NULL,NULL,'delete',71,72),(82,1,NULL,NULL,'AclExtras',76,77),(83,1,NULL,NULL,'Minify',78,83),(84,83,NULL,NULL,'Minify',79,82),(85,84,NULL,NULL,'index',80,81),(86,17,NULL,NULL,'index',31,32),(87,17,NULL,NULL,'secure',33,34),(88,2,NULL,NULL,'isAuthorized',15,16),(89,17,NULL,NULL,'isAuthorized',35,36),(90,40,NULL,NULL,'isAuthorized',57,58),(91,54,NULL,NULL,'isAuthorized',73,74),(92,1,NULL,NULL,'Conversations',84,141),(93,92,NULL,NULL,'ConversationMessages',85,90),(94,93,NULL,NULL,'add',86,87),(102,93,NULL,NULL,'isAuthorized',88,89),(103,92,NULL,NULL,'ConversationUsers',91,114),(104,103,NULL,NULL,'index',92,93),(105,103,NULL,NULL,'view',94,95),(106,103,NULL,NULL,'add',96,97),(107,103,NULL,NULL,'edit',98,99),(108,103,NULL,NULL,'delete',100,101),(109,103,NULL,NULL,'admin_index',102,103),(110,103,NULL,NULL,'admin_view',104,105),(111,103,NULL,NULL,'admin_add',106,107),(112,103,NULL,NULL,'admin_edit',108,109),(113,103,NULL,NULL,'admin_delete',110,111),(121,103,NULL,NULL,'isAuthorized',112,113),(122,92,NULL,NULL,'Conversations',115,140),(123,122,NULL,NULL,'index',116,117),(124,122,NULL,NULL,'sent',118,119),(125,122,NULL,NULL,'view',120,121),(126,122,NULL,NULL,'add',122,123),(127,122,NULL,NULL,'edit',124,125),(128,122,NULL,NULL,'delete',126,127),(129,122,NULL,NULL,'admin_index',128,129),(130,122,NULL,NULL,'admin_view',130,131),(131,122,NULL,NULL,'admin_add',132,133),(132,122,NULL,NULL,'admin_edit',134,135),(133,122,NULL,NULL,'admin_delete',136,137),(141,122,NULL,NULL,'isAuthorized',138,139),(157,17,NULL,NULL,'login',37,38),(161,17,NULL,NULL,'logout',39,40),(165,17,NULL,NULL,'login_with_facebook',41,42),(166,17,NULL,NULL,'login_with_twitter',43,44),(174,1,NULL,NULL,'Features',142,155),(175,174,NULL,NULL,'index',143,144),(176,174,NULL,NULL,'view',145,146),(177,174,NULL,NULL,'add',147,148),(178,174,NULL,NULL,'edit',149,150),(179,174,NULL,NULL,'delete',151,152),(180,174,NULL,NULL,'isAuthorized',153,154),(184,1,NULL,NULL,'AllowedServiceGroups',156,169),(185,184,NULL,NULL,'index',157,158),(186,184,NULL,NULL,'view',159,160),(187,184,NULL,NULL,'add',161,162),(188,184,NULL,NULL,'edit',163,164),(189,184,NULL,NULL,'delete',165,166),(190,184,NULL,NULL,'isAuthorized',167,168),(191,1,NULL,NULL,'AllowedServices',170,185),(192,191,NULL,NULL,'search',171,172),(193,191,NULL,NULL,'index',173,174),(194,191,NULL,NULL,'view',175,176),(195,191,NULL,NULL,'add',177,178),(196,191,NULL,NULL,'edit',179,180),(197,191,NULL,NULL,'delete',181,182),(198,191,NULL,NULL,'isAuthorized',183,184),(199,1,NULL,NULL,'AvpTypes',186,199),(200,199,NULL,NULL,'index',187,188),(201,199,NULL,NULL,'view',189,190),(202,199,NULL,NULL,'add',191,192),(203,199,NULL,NULL,'edit',193,194),(204,199,NULL,NULL,'delete',195,196),(205,199,NULL,NULL,'isAuthorized',197,198),(206,1,NULL,NULL,'CaseLoads',200,213),(207,206,NULL,NULL,'index',201,202),(208,206,NULL,NULL,'view',203,204),(209,206,NULL,NULL,'add',205,206),(210,206,NULL,NULL,'edit',207,208),(211,206,NULL,NULL,'delete',209,210),(212,206,NULL,NULL,'isAuthorized',211,212),(213,1,NULL,NULL,'ConfigOptions',214,227),(214,213,NULL,NULL,'index',215,216),(215,213,NULL,NULL,'view',217,218),(216,213,NULL,NULL,'add',219,220),(217,213,NULL,NULL,'edit',221,222),(218,213,NULL,NULL,'delete',223,224),(219,213,NULL,NULL,'isAuthorized',225,226),(220,1,NULL,NULL,'Countries',228,241),(221,220,NULL,NULL,'index',229,230),(222,220,NULL,NULL,'view',231,232),(223,220,NULL,NULL,'add',233,234),(224,220,NULL,NULL,'edit',235,236),(225,220,NULL,NULL,'delete',237,238),(226,220,NULL,NULL,'isAuthorized',239,240),(227,1,NULL,NULL,'Dashboard',242,253),(228,227,NULL,NULL,'index',243,244),(229,227,NULL,NULL,'temp',245,246),(230,227,NULL,NULL,'update',247,248),(231,227,NULL,NULL,'create',249,250),(232,227,NULL,NULL,'isAuthorized',251,252),(233,1,NULL,NULL,'Divisions',254,267),(234,233,NULL,NULL,'index',255,256),(235,233,NULL,NULL,'view',257,258),(236,233,NULL,NULL,'add',259,260),(237,233,NULL,NULL,'edit',261,262),(238,233,NULL,NULL,'delete',263,264),(239,233,NULL,NULL,'isAuthorized',265,266),(240,1,NULL,NULL,'EmployeeDetails',268,283),(241,240,NULL,NULL,'index',269,270),(242,240,NULL,NULL,'doctor_details',271,272),(243,240,NULL,NULL,'get_doctor_working_hours',273,274),(244,240,NULL,NULL,'doctor_register',275,276),(245,240,NULL,NULL,'doctor_edit',277,278),(246,240,NULL,NULL,'upload_profile_image',279,280),(247,240,NULL,NULL,'isAuthorized',281,282),(248,40,NULL,NULL,'edit_permissions',59,60),(249,1,NULL,NULL,'Languages',284,295),(250,249,NULL,NULL,'index',285,286),(251,249,NULL,NULL,'add',287,288),(252,249,NULL,NULL,'edit',289,290),(253,249,NULL,NULL,'delete',291,292),(254,249,NULL,NULL,'isAuthorized',293,294),(255,1,NULL,NULL,'Levels',296,309),(256,255,NULL,NULL,'index',297,298),(257,255,NULL,NULL,'view',299,300),(258,255,NULL,NULL,'add',301,302),(259,255,NULL,NULL,'edit',303,304),(260,255,NULL,NULL,'delete',305,306),(261,255,NULL,NULL,'isAuthorized',307,308),(262,1,NULL,NULL,'LicenseTypes',310,323),(263,262,NULL,NULL,'index',311,312),(264,262,NULL,NULL,'view',313,314),(265,262,NULL,NULL,'add',315,316),(266,262,NULL,NULL,'edit',317,318),(267,262,NULL,NULL,'delete',319,320),(268,262,NULL,NULL,'isAuthorized',321,322),(269,1,NULL,NULL,'Modalities',324,337),(270,269,NULL,NULL,'index',325,326),(271,269,NULL,NULL,'view',327,328),(272,269,NULL,NULL,'add',329,330),(273,269,NULL,NULL,'edit',331,332),(274,269,NULL,NULL,'delete',333,334),(275,269,NULL,NULL,'isAuthorized',335,336),(276,1,NULL,NULL,'MpdActivities',338,351),(277,276,NULL,NULL,'index',339,340),(278,276,NULL,NULL,'view',341,342),(279,276,NULL,NULL,'add',343,344),(280,276,NULL,NULL,'edit',345,346),(281,276,NULL,NULL,'delete',347,348),(282,276,NULL,NULL,'isAuthorized',349,350),(283,1,NULL,NULL,'MpdActivityLists',352,365),(284,283,NULL,NULL,'index',353,354),(285,283,NULL,NULL,'view',355,356),(286,283,NULL,NULL,'add',357,358),(287,283,NULL,NULL,'edit',359,360),(288,283,NULL,NULL,'delete',361,362),(289,283,NULL,NULL,'isAuthorized',363,364),(290,1,NULL,NULL,'OfficialTitles',366,379),(291,290,NULL,NULL,'index',367,368),(292,290,NULL,NULL,'view',369,370),(293,290,NULL,NULL,'add',371,372),(294,290,NULL,NULL,'edit',373,374),(295,290,NULL,NULL,'delete',375,376),(296,290,NULL,NULL,'isAuthorized',377,378),(297,1,NULL,NULL,'PatientDetails',380,397),(298,297,NULL,NULL,'index',381,382),(299,297,NULL,NULL,'view',383,384),(300,297,NULL,NULL,'saveReligion',385,386),(301,297,NULL,NULL,'saveCountry',387,388),(302,297,NULL,NULL,'add',389,390),(303,297,NULL,NULL,'edit',391,392),(304,297,NULL,NULL,'delete',393,394),(305,297,NULL,NULL,'isAuthorized',395,396),(306,1,NULL,NULL,'Programs',398,411),(307,306,NULL,NULL,'index',399,400),(308,306,NULL,NULL,'view',401,402),(309,306,NULL,NULL,'add',403,404),(310,306,NULL,NULL,'edit',405,406),(311,306,NULL,NULL,'delete',407,408),(312,306,NULL,NULL,'isAuthorized',409,410),(313,1,NULL,NULL,'Providers',412,425),(314,313,NULL,NULL,'index',413,414),(315,313,NULL,NULL,'view',415,416),(316,313,NULL,NULL,'add',417,418),(317,313,NULL,NULL,'edit',419,420),(318,313,NULL,NULL,'delete',421,422),(319,313,NULL,NULL,'isAuthorized',423,424),(320,1,NULL,NULL,'RelationShips',426,439),(321,320,NULL,NULL,'index',427,428),(322,320,NULL,NULL,'view',429,430),(323,320,NULL,NULL,'add',431,432),(324,320,NULL,NULL,'edit',433,434),(325,320,NULL,NULL,'delete',435,436),(326,320,NULL,NULL,'isAuthorized',437,438),(327,1,NULL,NULL,'Religions',440,453),(328,327,NULL,NULL,'index',441,442),(329,327,NULL,NULL,'view',443,444),(330,327,NULL,NULL,'add',445,446),(331,327,NULL,NULL,'edit',447,448),(332,327,NULL,NULL,'delete',449,450),(333,327,NULL,NULL,'isAuthorized',451,452),(334,1,NULL,NULL,'Sponsors',454,467),(335,334,NULL,NULL,'index',455,456),(336,334,NULL,NULL,'view',457,458),(337,334,NULL,NULL,'add',459,460),(338,334,NULL,NULL,'edit',461,462),(339,334,NULL,NULL,'delete',463,464),(340,334,NULL,NULL,'isAuthorized',465,466),(341,1,NULL,NULL,'VpdActivities',468,481),(342,341,NULL,NULL,'index',469,470),(343,341,NULL,NULL,'view',471,472),(344,341,NULL,NULL,'add',473,474),(345,341,NULL,NULL,'edit',475,476),(346,341,NULL,NULL,'delete',477,478),(347,341,NULL,NULL,'isAuthorized',479,480),(348,1,NULL,NULL,'VpdActivityTypes',482,495),(349,348,NULL,NULL,'index',483,484),(350,348,NULL,NULL,'view',485,486),(351,348,NULL,NULL,'add',487,488),(352,348,NULL,NULL,'edit',489,490),(353,348,NULL,NULL,'delete',491,492),(354,348,NULL,NULL,'isAuthorized',493,494),(355,1,NULL,NULL,'WaitingListsTitles',496,513),(356,355,NULL,NULL,'index',497,498),(357,355,NULL,NULL,'addToList',499,500),(358,355,NULL,NULL,'assign',501,502),(359,355,NULL,NULL,'view',503,504),(360,355,NULL,NULL,'add',505,506),(361,355,NULL,NULL,'edit',507,508),(362,355,NULL,NULL,'delete',509,510),(363,355,NULL,NULL,'isAuthorized',511,512),(364,1,NULL,NULL,'conversations',514,519),(365,364,NULL,NULL,'ConversationsApp',515,518),(366,365,NULL,NULL,'isAuthorized',516,517);

/*Table structure for table `allowed_service_groups` */

DROP TABLE IF EXISTS `allowed_service_groups`;

CREATE TABLE `allowed_service_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `allowed_service_groups` */

insert  into `allowed_service_groups`(`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Evaluation','Evaluation Group',2130706433,1,'2013-05-07 17:22:45','2013-06-18 20:58:14'),(2,'Consultancy','Consultancy Group',2130706433,1,'2013-05-10 21:32:52','2013-06-18 20:38:14'),(3,'Treatment','Treatment Group',2130706433,1,'2013-06-18 20:58:37','2013-06-18 20:58:37');

/*Table structure for table `allowed_services` */

DROP TABLE IF EXISTS `allowed_services`;

CREATE TABLE `allowed_services` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `allowed_service_group_id` int(11) default NULL,
  `description` text,
  `ip_address` int(10) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_allowed_services_asg_id` (`allowed_service_group_id`),
  CONSTRAINT `allowed_services_ibfk_1` FOREIGN KEY (`allowed_service_group_id`) REFERENCES `allowed_service_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `allowed_services` */

insert  into `allowed_services`(`id`,`name`,`allowed_service_group_id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Group Tx',3,'this is description',2130706433,1,'2013-05-14 14:34:14','0000-00-00 00:00:00'),(2,'Tx',3,'this is testing description 3',2130706433,1,'2013-05-14 14:39:45','0000-00-00 00:00:00'),(3,'InTake Consultation',2,'adfa',2130706433,1,'2013-05-14 15:28:40','0000-00-00 00:00:00'),(4,'Offsite Tx',3,'adfaf',2130706433,1,'2013-05-14 15:28:48','0000-00-00 00:00:00'),(5,'Tx with live supervision',3,'a',2130706433,1,'2013-05-14 15:29:08','0000-00-00 00:00:00'),(6,'yyy',1,'adfa',2130706433,0,'2013-05-14 15:29:15','2013-05-28 14:57:57'),(7,'Co Tx',3,'losem iprem',2130706433,1,'2013-05-14 15:29:23','0000-00-00 00:00:00'),(8,'uu',2,'adaf',2130706433,0,'2013-05-14 15:29:31','2013-05-28 14:57:52'),(9,'Dx with live supervision',1,'adaf',2130706433,1,'2013-05-14 15:29:39','0000-00-00 00:00:00'),(10,'utt',1,'adaf',2130706433,0,'2013-05-14 15:29:49','2013-05-28 14:57:46'),(11,'Dx',1,'adfa',2130706433,1,'2013-05-14 15:29:56','0000-00-00 00:00:00');

/*Table structure for table `annual_vacation_plans` */

DROP TABLE IF EXISTS `annual_vacation_plans`;

CREATE TABLE `annual_vacation_plans` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `avp_type_id` int(11) default NULL,
  `description` varchar(50) default NULL,
  `last_work_day` date default NULL COMMENT 'its analogous to start time',
  `report_to_work` date default NULL COMMENT 'its analogous to end time',
  `status` enum('approved','pending') default 'pending',
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_annual_vacation_plans_ed_id` (`employee_detail_id`),
  KEY `FK_annual_vacation_plans_avpt_id` (`avp_type_id`),
  CONSTRAINT `annual_vacation_plans_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `annual_vacation_plans_ibfk_2` FOREIGN KEY (`avp_type_id`) REFERENCES `avp_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `annual_vacation_plans` */

insert  into `annual_vacation_plans`(`id`,`employee_detail_id`,`avp_type_id`,`description`,`last_work_day`,`report_to_work`,`status`,`is_active`,`created`,`modified`) values (1,13,3,'Sickness','2013-06-01','2013-06-07','pending',0,'2013-06-07 22:01:56','2013-06-07 22:01:56'),(2,13,1,'Sickness','2013-06-10','2013-06-17','pending',0,'2013-06-11 16:49:36','2013-06-11 16:49:36'),(3,13,1,'Sickness','2013-06-20','2013-06-25','approved',1,'2013-06-11 16:57:43','2013-06-11 16:57:43'),(4,13,1,'Sickness','2013-06-27','2013-06-30','approved',1,'2013-06-12 20:06:41','2013-06-12 20:06:41'),(5,13,1,'Sickness','2013-07-06','2013-07-11','approved',1,'2013-06-12 22:19:51','2013-06-12 22:19:51');

/*Table structure for table `appointment_groups` */

DROP TABLE IF EXISTS `appointment_groups`;

CREATE TABLE `appointment_groups` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `patient_detail_id` bigint(20) NOT NULL,
  `appointment_id` bigint(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `crearted` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_appointment_groups_ed_id` (`employee_detail_id`),
  KEY `FK_appointment_groups_pd_id` (`patient_detail_id`),
  KEY `FK_appointment_groups_app_id` (`appointment_id`),
  CONSTRAINT `appointment_groups_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `appointment_groups_ibfk_2` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `appointment_groups_ibfk_3` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appointment_groups` */

/*Table structure for table `appointments` */

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` bigint(20) NOT NULL auto_increment,
  `patient_waiting_list_id` bigint(20) default NULL,
  `employee_working_hour_breakup_id` bigint(20) NOT NULL,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `patient_detail_id` bigint(20) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `is_group` tinyint(1) NOT NULL default '0',
  `status` enum('pending','completed','inprogess','cancelled') NOT NULL default 'pending',
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_appointments_pwl_id` (`patient_waiting_list_id`),
  KEY `FK_appointments_ed_id` (`employee_detail_id`),
  KEY `FK_appointments_pd_id` (`patient_detail_id`),
  KEY `FK_appointments_ewhb_id` (`employee_working_hour_breakup_id`),
  CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `appointments_ibfk_4` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `appointments_ibfk_5` FOREIGN KEY (`employee_working_hour_breakup_id`) REFERENCES `employee_working_hour_breakups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `appointments` */

insert  into `appointments`(`id`,`patient_waiting_list_id`,`employee_working_hour_breakup_id`,`employee_detail_id`,`division_id`,`patient_detail_id`,`appointment_date`,`is_group`,`status`,`is_active`,`created`,`modified`) values (1,0,359,20,4,4,'2013-09-21 07:00:00',0,'pending',1,'2013-07-03 13:42:57','2013-07-03 13:42:57'),(2,5,359,8,4,10,'2013-07-28 07:00:00',0,'pending',1,'2013-07-04 11:38:53','2013-07-04 11:38:53'),(3,5,359,20,4,10,'2013-07-21 07:00:00',0,'pending',1,'2013-07-04 12:09:10','2013-07-04 12:09:10'),(5,6,359,8,4,4,'2013-07-14 07:00:00',0,'pending',1,'2013-07-04 12:19:53','2013-07-04 12:19:53');

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `aros` */

insert  into `aros`(`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,'Group',1,'group-1',1,2),(2,NULL,'Group',2,'group-2',3,4),(3,NULL,'Group',3,'group-3',5,6),(4,NULL,'Group',4,'group-4',7,8),(5,NULL,'Group',5,'group-5',9,10);

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL auto_increment,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL default '0',
  `_read` varchar(2) NOT NULL default '0',
  `_update` varchar(2) NOT NULL default '0',
  `_delete` varchar(2) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `aros_acos` */

insert  into `aros_acos`(`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (7,2,228,'1','1','1','1'),(8,2,229,'1','1','1','1'),(9,2,230,'1','1','1','1'),(10,2,231,'1','1','1','1'),(11,2,241,'1','1','1','1'),(12,2,242,'1','1','1','1'),(13,2,243,'1','1','1','1'),(14,2,244,'1','1','1','1'),(15,2,245,'1','1','1','1'),(16,2,246,'1','1','1','1');

/*Table structure for table `auth_action_maps` */

DROP TABLE IF EXISTS `auth_action_maps`;

CREATE TABLE `auth_action_maps` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL default '5',
  `crud` enum('create','read','update','delete') NOT NULL default 'read',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_auth_action_maps_ftr_id` (`feature_id`),
  CONSTRAINT `auth_action_maps_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `features` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=latin1;

/*Data for the table `auth_action_maps` */

insert  into `auth_action_maps`(`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (2,'Display mapping information',4,2,'read','2013-04-10 20:21:58','2013-06-03 17:05:22'),(3,'Adding mapping information',5,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:06'),(4,'Editing mapping information',6,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:23'),(5,'Deleting mapping information',7,2,'read','2013-04-10 20:21:58','2013-04-22 22:05:17'),(14,'Add/Synchronise all acos with auth action mapper',16,2,'read','2013-04-10 20:21:58','2013-04-19 21:21:52'),(17,'callback action for facebook',21,1,'read','2013-04-10 20:21:58','2013-04-19 21:14:23'),(21,'callback action for twitter',25,1,'read','2013-04-10 20:21:58','2013-06-03 17:05:13'),(23,'Register user',27,1,'create','2013-04-10 20:21:58','2013-04-22 22:05:47'),(24,'Email verification',28,1,'read','2013-04-10 20:21:58','2013-04-22 22:06:07'),(26,'Forgot password',30,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:18'),(27,'Reset password',31,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:23'),(35,'List all groups',41,3,'read','2013-04-10 20:21:58','2013-04-19 21:22:45'),(36,'Display group',42,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:01'),(37,'Add group',43,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:15'),(38,'Edit group',44,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:36'),(39,'Delete group',45,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:52'),(47,'List all users',55,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:37'),(48,'Display user',56,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:48'),(49,'Add user',57,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:57'),(50,'Edit user',58,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:26'),(51,'Delete user',59,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:36'),(73,'Minify plugin',84,4,'read','2013-04-10 20:21:58','2013-04-19 21:27:01'),(74,'Minify plugin',85,4,'read','2013-04-10 20:21:58','2013-04-19 21:29:09'),(75,'Dummy index action',86,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:04'),(76,'Action after successful login',87,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:42'),(77,'',88,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(78,'',89,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(79,'',90,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(80,'',91,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(81,'ConversationMessages',93,4,'read','2013-04-10 20:21:58','2013-04-22 22:10:27'),(82,'ConversationMessages -> add',94,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:12'),(90,'',102,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(91,'Conversations -> ConversationUsers',103,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:42'),(92,'ConversationUsers -> index',104,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:55'),(93,'ConversationUsers -> view',105,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:12'),(94,'ConversationUsers -> add',106,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:31'),(95,'ConversationUsers -> edit',107,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:42'),(96,'ConversationUsers -> delete',108,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:53'),(97,'ConversationUsers -> admin_index',109,4,'read','2013-04-10 20:21:58','2013-04-22 22:13:10'),(98,'ConversationUsers -> admin_view',110,4,'read','2013-04-10 20:21:58','2013-04-22 22:17:48'),(99,'ConversationUsers -> admin_add',111,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:00'),(100,'ConversationUsers -> admin_edit',112,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:10'),(101,'ConversationUsers -> admin_delete',113,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:20'),(109,'',121,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(110,'Conversations -> Conversations',122,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:37'),(111,'Conversations -> index',123,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:00'),(112,'Conversations -> sent',124,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:13'),(113,'Conversations -> view',125,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:27'),(114,'Conversations -> add',126,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:37'),(115,'Conversations -> edit',127,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:49'),(116,'ConversationUsers -> delete',128,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:01'),(117,'Conversations -> admin_index',129,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:14'),(118,'Conversations -> admin_view',130,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:33'),(119,'Conversations -> admin_add',131,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:51'),(120,'Conversations -> admin_edit',132,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:06'),(121,'Conversations -> admin_delete',133,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:26'),(129,'',141,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58'),(141,'Login',157,1,'read','2013-04-12 21:53:51','2013-04-19 21:30:07'),(144,'Logout',161,1,'read','2013-04-15 20:01:29','2013-04-19 21:30:12'),(147,'Login action for facebook',165,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:19'),(148,'Login action for twitter',166,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:25'),(153,'List all auth action maps',3,2,'read','2013-04-19 20:03:52','2013-04-22 16:11:29'),(155,'Display features',175,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:00'),(156,'View Feature',176,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:14'),(157,'Add new Feature',177,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:23'),(158,'Edit Feature',178,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:33'),(159,'Delete Feature',179,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:43'),(160,'',180,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05'),(163,'',185,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(164,'',186,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(165,'',187,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(166,'',188,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(167,'',189,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(168,'',190,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(169,'',192,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(170,'',193,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(171,'',194,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(172,'',195,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(173,'',196,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(174,'',197,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(175,'',198,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(176,'',200,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(177,'',201,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(178,'',202,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(179,'',203,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(180,'',204,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(181,'',205,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(182,'',207,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(183,'',208,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(184,'',209,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(185,'',210,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(186,'',211,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(187,'',212,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(188,'',214,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(189,'',215,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(190,'',216,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(191,'',217,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(192,'',218,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(193,'',219,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(194,'',221,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(195,'',222,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(196,'',223,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(197,'',224,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(198,'',225,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(199,'',226,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(200,'Dashboard',228,10,'read','2013-06-03 16:12:15','2013-06-03 20:58:44'),(201,'',229,10,'read','2013-06-03 16:12:15','2013-06-03 20:58:53'),(202,'',230,10,'read','2013-06-03 16:12:15','2013-06-03 20:59:01'),(203,'',231,10,'read','2013-06-03 16:12:15','2013-06-03 20:59:07'),(204,'',232,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(205,'',234,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(206,'',235,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(207,'',236,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(208,'',237,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(209,'',238,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(210,'',239,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(211,'Employee Listings',241,9,'read','2013-06-03 16:12:15','2013-06-03 16:52:30'),(212,'Doctor detail profile view',242,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:12'),(213,'Doctor working hours',243,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:28'),(214,'Doctor registration',244,9,'read','2013-06-03 16:12:15','2013-06-03 20:35:52'),(215,'Doctor edit profile',245,9,'read','2013-06-03 16:12:15','2013-06-03 20:36:44'),(216,'Image upload for doctor profile',246,9,'read','2013-06-03 16:12:15','2013-06-03 20:37:05'),(217,'',247,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(218,'',248,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(219,'',250,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(220,'',251,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(221,'',252,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(222,'',253,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(223,'',254,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(224,'',256,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(225,'',257,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(226,'',258,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(227,'',259,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(228,'',260,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(229,'',261,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(230,'',263,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(231,'',264,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(232,'',265,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(233,'',266,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(234,'',267,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(235,'',268,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(236,'',270,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(237,'',271,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(238,'',272,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(239,'',273,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(240,'',274,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(241,'',275,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(242,'',277,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(243,'',278,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(244,'',279,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(245,'',280,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(246,'',281,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(247,'',282,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(248,'',284,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(249,'',285,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(250,'',286,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(251,'',287,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(252,'',288,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(253,'',289,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(254,'',291,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(255,'',292,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(256,'',293,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(257,'',294,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(258,'',295,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(259,'',296,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(260,'',298,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(261,'',299,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(262,'',300,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(263,'',301,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(264,'',302,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(265,'',303,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(266,'',304,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(267,'',305,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(268,'',307,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(269,'',308,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(270,'',309,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(271,'',310,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(272,'',311,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(273,'',312,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(274,'',314,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(275,'',315,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(276,'',316,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(277,'',317,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(278,'',318,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(279,'',319,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(280,'',321,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(281,'',322,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(282,'',323,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(283,'',324,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(284,'',325,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(285,'',326,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(286,'',328,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(287,'',329,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(288,'',330,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(289,'',331,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(290,'',332,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(291,'',333,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(292,'',335,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(293,'',336,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(294,'',337,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(295,'',338,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(296,'',339,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(297,'',340,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(298,'',342,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(299,'',343,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(300,'',344,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(301,'',345,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(302,'',346,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(303,'',347,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(304,'',349,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(305,'',350,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(306,'',351,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(307,'',352,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(308,'',353,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(309,'',354,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(310,'',356,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(311,'',357,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(312,'',358,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(313,'',359,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(314,'',360,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(315,'',361,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(316,'',362,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(317,'',363,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(318,'',365,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15'),(319,'',366,5,'read','2013-06-03 16:12:15','2013-06-03 16:12:15');

/*Table structure for table `avp_types` */

DROP TABLE IF EXISTS `avp_types`;

CREATE TABLE `avp_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `avp_types` */

insert  into `avp_types`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Paid',2130706433,1,'2013-05-07 18:22:35','2013-05-07 18:24:06'),(2,'UnPaid',2130706433,1,'2013-05-07 18:24:13','2013-05-07 18:24:45'),(3,'Medical',2130706433,1,'2013-05-07 18:24:31','2013-05-07 18:24:31'),(4,'Business',2130706433,1,'2013-05-07 18:24:38','2013-05-10 21:52:55');

/*Table structure for table `call_logs` */

DROP TABLE IF EXISTS `call_logs`;

CREATE TABLE `call_logs` (
  `id` bigint(20) NOT NULL auto_increment,
  `appointment_id` bigint(20) NOT NULL,
  `calling_date_time` date NOT NULL,
  `call_subject` varchar(100) NOT NULL,
  `call_answered_by` varchar(500) NOT NULL,
  `call_summary` varchar(255) default NULL,
  `caller_id` bigint(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `ip_address` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_call_logs_rs_id` (`call_answered_by`),
  KEY `FK_call_logs_ed_id` (`caller_id`),
  KEY `FK_call_logs_app_id` (`appointment_id`),
  CONSTRAINT `call_logs_ibfk_3` FOREIGN KEY (`caller_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `call_logs_ibfk_4` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `call_logs` */

insert  into `call_logs`(`id`,`appointment_id`,`calling_date_time`,`call_subject`,`call_answered_by`,`call_summary`,`caller_id`,`is_active`,`created`,`modified`,`ip_address`) values (1,5,'2013-07-10','test','abc name','this is summary',4,1,NULL,NULL,NULL),(2,5,'2013-07-11','test 2','abc name2','this is summary 2',4,1,NULL,NULL,NULL),(5,5,'2013-08-20','abc','aad',NULL,8,1,'2013-08-19 14:51:32','2013-08-19 14:51:32',2130706433),(6,1,'2013-08-24','test','Abc user',NULL,20,1,'2013-08-26 12:32:26','2013-08-26 12:32:26',2130706433),(7,1,'2013-08-24','test2','Abc user',NULL,20,1,'2013-08-26 12:32:45','2013-08-26 12:32:47',2130706433);

/*Table structure for table `case_loads` */

DROP TABLE IF EXISTS `case_loads`;

CREATE TABLE `case_loads` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `description` varchar(45) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `case_loads` */

insert  into `case_loads`(`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Arctic','this is discription',2130706433,1,'2013-05-13 12:57:36','2013-05-28 14:50:59'),(2,'this is testing 2','this is discription 2',2130706433,0,'2013-05-13 13:03:55','2013-05-13 13:04:05'),(3,'Voice','Losem',2130706433,1,'2013-05-28 14:51:14','2013-05-28 14:51:14'),(4,'Autism','Losem',2130706433,1,'2013-05-28 14:51:28','2013-05-28 14:51:28');

/*Table structure for table `clinicial_levels` */

DROP TABLE IF EXISTS `clinicial_levels`;

CREATE TABLE `clinicial_levels` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(45) default NULL,
  `division_id` int(11) NOT NULL,
  `fees` decimal(8,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` varchar(45) default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_clinicial_levels_div_id` (`division_id`),
  CONSTRAINT `clinicial_levels_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `clinicial_levels` */

/*Table structure for table `config_options` */

DROP TABLE IF EXISTS `config_options`;

CREATE TABLE `config_options` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `config_options` */

insert  into `config_options`(`id`,`name`,`value`,`ip_address`,`is_active`,`created`,`modified`) values (1,'SITEURL','http://www.example.com',NULL,1,'2013-05-13 15:11:59','2013-05-13 15:11:59'),(2,'gender','a:2:{s:4:\"male\";s:4:\"Male\";s:6:\"female\";s:6:\"Female\";}',NULL,1,'2013-05-16 17:05:08','2013-05-16 17:05:08'),(3,'test','232131',2130706433,0,'2013-07-03 06:47:28','2013-07-03 06:47:34');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `nationality` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`name`,`nationality`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Afghanistan','Afghan',2130706433,1,'2013-05-06 20:27:30','2013-05-13 09:14:20'),(2,'Albania','Albanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(3,'Algeria','Algerian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(4,'Andorra','Andorran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(5,'Angola','Angolan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(6,'Antigua and Barbuda','Antiguans, Barbudans',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(7,'Argentina','Argentinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(8,'Armenia','Armenian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(9,'Australia','Australian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(10,'Austria','Austrian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(11,'Azerbaijan','Azerbaijani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(12,'The Bahamas','Bahamian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(13,'Bahrain','Bahraini',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(14,'Bangladesh','Bangladeshi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(15,'Barbados','Barbadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(16,'Belarus','Belarusian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(17,'Belgium','Belgian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(18,'Belize','Belizean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(19,'Benin','Beninese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(20,'Bhutan','Bhutanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(21,'Bolivia','Bolivian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(22,'Bosnia and Herzegovina','Bosnian, Herzegovinian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(23,'Botswana','Motswana (singular), Batswana (plural)',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(24,'Brazil','Brazilian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(25,'Brunei','Bruneian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(26,'Bulgaria','Bulgarian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(27,'Burkina Faso','Burkinabe',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(28,'Burundi','Burundian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(29,'Cambodia','Cambodian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(30,'Cameroon','Cameroonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(31,'Canada','Canadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(32,'Cape Verde','Cape Verdian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(33,'Central African Republic','Central African',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(34,'Chad','Chadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(35,'Chile','Chilean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(36,'China','Chinese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(37,'Colombia','Colombian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(38,'Comoros','Comoran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(39,'Congo, Republic of the','Congolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(40,'Congo, Democratic Republic of the','Congolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(41,'Costa Rica','Costa Rican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(42,'Cote d\'Ivoire','Ivorian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(43,'Croatia','Croatian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(44,'Cuba','Cuban',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(45,'Cyprus','Cypriot',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(46,'Czech Republic','Czech',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(47,'Denmark','Danish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(48,'Djibouti','Djibouti',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(49,'Dominica','Dominican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(50,'Dominican Republic','Dominican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(51,'East Timor','East Timorese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(52,'Ecuador','Ecuadorean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(53,'Egypt','Egyptian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(54,'El Salvador','Salvadoran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(55,'Equatorial Guinea','Equatorial Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(56,'Eritrea','Eritrean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(57,'Estonia','Estonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(58,'Ethiopia','Ethiopian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(59,'Fiji','Fijian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(60,'Finland','Finnish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(61,'France','French',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(62,'Gabon','Gabonese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(63,'The Gambia','Gambian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(64,'Georgia','Georgian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(65,'Germany','German',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(66,'Ghana','Ghanaian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(67,'Greece','Greek',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(68,'Grenada','Grenadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(69,'Guatemala','Guatemalan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(70,'Guinea','Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(71,'Guinea-Bissau','Guinea-Bissauan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(72,'Guyana','Guyanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(73,'Haiti','Haitian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(74,'Honduras','Honduran',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(75,'Hungary','Hungarian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(76,'Iceland','Icelander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(77,'India','Indian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(78,'Indonesia','Indonesian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(79,'Iran','Iranian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(80,'Iraq','Iraqi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(81,'Ireland','Irish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(82,'Israel','Israeli',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(83,'Italy','Italian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(84,'Jamaica','Jamaican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(85,'Japan','Japanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(86,'Jordan','Jordanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(87,'Kazakhstan','Kazakhstani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(88,'Kenya','Kenyan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(89,'Kiribati','I-Kiribati',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(90,'Korea, North','North Korean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(91,'Korea, South','South Korean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(92,'Kuwait','Kuwaiti',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(93,'Kyrgyz Republic','Kirghiz',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(94,'Laos','Laotian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(95,'Latvia','Latvian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(96,'Lebanon','Lebanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(97,'Lesotho','Mosotho',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(98,'Liberia','Liberian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(99,'Libya','Libyan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(100,'Liechtenstein','Liechtensteiner',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(101,'Lithuania','Lithuanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(102,'Luxembourg','Luxembourger',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(103,'Macedonia','Macedonian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(104,'Madagascar','Malagasy',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(105,'Malawi','Malawian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(106,'Malaysia','Malaysian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(107,'Maldives','Maldivan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(108,'Mali','Malian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(109,'Malta','Maltese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(110,'Marshall Islands','Marshallese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(111,'Mauritania','Mauritanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(112,'Mauritius','Mauritian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(113,'Mexico','Mexican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(114,'Federated States of Micronesia','Micronesian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(115,'Moldova','Moldovan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(116,'Monaco','Monegasque',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(117,'Mongolia','Mongolian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(118,'Morocco','Moroccan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(119,'Mozambique','Mozambican',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(120,'Myanmar (Burma)','Burmese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(121,'Namibia','Namibian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(122,'Nauru','Nauruan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(123,'Nepal','Nepalese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(124,'Netherlands','Dutch',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(125,'New Zealand','New Zealander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(126,'Nicaragua','Nicaraguan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(127,'Niger','Nigerien',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(128,'Nigeria','Nigerian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(129,'Norway','Norwegian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(130,'Oman','Omani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(131,'Pakistan','Pakistani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(132,'Palau','Palauan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(133,'Panama','Panamanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(134,'Papua New Guinea','Papua New Guinean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(135,'Paraguay','Paraguayan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(136,'Peru','Peruvian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(137,'Philippines','Filipino',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(138,'Poland','Polish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(139,'Portugal','Portuguese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(140,'Qatar','Qatari',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(141,'Romania','Romanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(142,'Russia','Russian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(143,'Rwanda','Rwandan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(144,'Saint Kitts and Nevis','Kittian and Nevisian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(145,'Saint Lucia','Saint Lucian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(146,'Samoa','Samoan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(147,'San Marino','Sammarinese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(148,'Sao Tome and Principe','Sao Tomean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(149,'Saudi Arabia','Saudi Arabian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(150,'Senegal','Senegalese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(151,'Serbia and Montenegro','Serbian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(152,'Seychelles','Seychellois',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(153,'Sierra Leone','Sierra Leonean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(154,'Singapore','Singaporean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(155,'Slovakia','Slovak',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(156,'Slovenia','Slovene',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(157,'Solomon Islands','Solomon Islander',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(158,'Somalia','Somali',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(159,'South Africa','South African',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(160,'Spain','Spanish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(161,'Sri Lanka','Sri Lankan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(162,'Sudan','Sudanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(163,'Suriname','Surinamer',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(164,'Swaziland','Swazi',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(165,'Sweden','Swedish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(166,'Switzerland','Swiss',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(167,'Syria','Syrian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(168,'Taiwan','Taiwanese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(169,'Tajikistan','Tadzhik',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(170,'Tanzania','Tanzanian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(171,'Thailand','Thai',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(172,'Togo','Togolese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(173,'Tonga','Tongan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(174,'Trinidad and Tobago','Trinidadian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(175,'Tunisia','Tunisian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(176,'Turkey','Turkish',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(177,'Turkmenistan','Turkmen',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(178,'Tuvalu','Tuvaluan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(179,'Uganda','Ugandan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(180,'Ukraine','Ukrainian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(181,'United Arab Emirates','Emirian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(182,'United Kingdom','British',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(183,'United States','American',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(184,'Uruguay','Uruguayan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(185,'Uzbekistan','Uzbekistani',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(186,'Vanuatu','Ni-Vanuatu',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(187,'Vatican City (Holy See)','none',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(188,'Venezuela','Venezuelan',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(189,'Vietnam','Vietnamese',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(190,'Yemen','Yemeni',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(191,'Zambia','Zambian',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(192,'Zimbabwe','Zimbabwean',NULL,1,'2013-05-06 20:27:30','2013-05-06 20:27:30'),(193,'test','test',2130706433,1,'2013-07-01 10:27:41','2013-07-01 10:27:41'),(194,'test','test',2130706433,1,'2013-07-01 10:27:42','2013-07-01 10:27:42');

/*Table structure for table `divisions` */

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `description` varchar(50) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `divisions` */

insert  into `divisions`(`id`,`name`,`description`,`is_active`,`ip_address`,`created`,`modified`) values (4,'ABA','',1,2130706433,'2013-05-06 13:05:58','2013-05-10 21:50:37'),(8,'SLP',NULL,1,2130706433,'2013-05-06 14:02:49','2013-05-06 22:02:05'),(9,'OT',NULL,1,2130706433,'2013-05-06 22:02:11','2013-05-07 15:50:37'),(10,'Audiology',NULL,1,2130706433,'2013-05-06 22:24:06','2013-05-06 22:37:12'),(11,'Other',NULL,1,2130706433,'2013-05-07 15:37:07','2013-05-07 15:37:07'),(12,'test2 with proc',NULL,0,2130706433,'2013-05-09 11:09:17','2013-05-09 11:09:24'),(13,'testing tahir a',NULL,1,2130706433,'2013-05-09 12:09:07','2013-05-09 12:11:00'),(14,'again test','losem IPREM',1,2130706433,'2013-05-10 20:07:02','2013-05-10 21:31:11'),(15,'Audiology 456','qwe rty tyui',1,2130706433,'2013-05-13 16:07:27','2013-05-13 16:07:39');

/*Table structure for table `employee_allowed_services` */

DROP TABLE IF EXISTS `employee_allowed_services`;

CREATE TABLE `employee_allowed_services` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `allowed_service_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_services_ed_id` (`employee_detail_id`),
  KEY `FK_employee_services_as_id` (`allowed_service_id`),
  CONSTRAINT `employee_allowed_services_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_allowed_services_ibfk_2` FOREIGN KEY (`allowed_service_id`) REFERENCES `allowed_services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

/*Data for the table `employee_allowed_services` */

insert  into `employee_allowed_services`(`id`,`employee_detail_id`,`allowed_service_id`,`is_active`,`created`,`modified`) values (11,8,1,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(12,8,2,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(13,8,1,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(14,8,2,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(15,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,1,0,'2013-06-03 15:02:01','2013-06-24 17:37:16'),(18,10,2,0,'2013-06-03 15:02:01','2013-06-24 17:37:16'),(19,11,1,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(20,11,2,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(21,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(22,12,2,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(23,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(24,12,1,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(25,12,2,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(26,12,3,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(27,13,1,0,'2013-06-04 19:46:17','2013-06-24 17:35:42'),(28,13,2,0,'2013-06-04 19:46:17','2013-06-24 17:35:42'),(29,14,1,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(30,14,2,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(31,13,1,0,'2013-06-10 21:38:16','2013-06-24 17:35:42'),(32,13,2,0,'2013-06-10 21:38:16','2013-06-24 17:35:42'),(33,13,1,0,'2013-06-10 22:16:03','2013-06-24 17:35:42'),(34,13,2,0,'2013-06-10 22:16:03','2013-06-24 17:35:42'),(35,13,1,0,'2013-06-10 22:17:03','2013-06-24 17:35:42'),(36,13,2,0,'2013-06-10 22:17:03','2013-06-24 17:35:42'),(37,13,1,0,'2013-06-10 22:17:26','2013-06-24 17:35:42'),(38,13,2,0,'2013-06-10 22:17:26','2013-06-24 17:35:42'),(39,13,1,0,'2013-06-10 22:17:44','2013-06-24 17:35:42'),(40,13,2,0,'2013-06-10 22:17:44','2013-06-24 17:35:42'),(41,13,1,0,'2013-06-10 22:18:33','2013-06-24 17:35:42'),(42,13,2,0,'2013-06-10 22:18:33','2013-06-24 17:35:42'),(43,13,1,0,'2013-06-10 22:19:02','2013-06-24 17:35:42'),(44,13,2,0,'2013-06-10 22:19:02','2013-06-24 17:35:42'),(45,13,1,0,'2013-06-10 22:19:16','2013-06-24 17:35:42'),(46,13,2,0,'2013-06-10 22:19:16','2013-06-24 17:35:42'),(47,13,1,0,'2013-06-10 22:20:01','2013-06-24 17:35:42'),(48,13,2,0,'2013-06-10 22:20:01','2013-06-24 17:35:42'),(49,13,1,0,'2013-06-10 22:20:42','2013-06-24 17:35:42'),(50,13,2,0,'2013-06-10 22:20:42','2013-06-24 17:35:42'),(51,13,1,0,'2013-06-10 22:22:13','2013-06-24 17:35:42'),(52,13,2,0,'2013-06-10 22:22:13','2013-06-24 17:35:42'),(53,13,1,0,'2013-06-10 22:23:29','2013-06-24 17:35:42'),(54,13,2,0,'2013-06-10 22:23:29','2013-06-24 17:35:42'),(55,13,1,0,'2013-06-10 22:24:56','2013-06-24 17:35:42'),(56,13,2,0,'2013-06-10 22:24:56','2013-06-24 17:35:42'),(57,13,1,0,'2013-06-11 14:43:48','2013-06-24 17:35:42'),(58,13,2,0,'2013-06-11 14:43:48','2013-06-24 17:35:42'),(59,4,1,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(60,4,3,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(61,8,1,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(62,8,2,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(63,10,1,0,'2013-06-18 18:12:00','2013-06-24 17:37:16'),(64,10,2,0,'2013-06-18 18:12:00','2013-06-24 17:37:16'),(65,4,1,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(66,4,3,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(67,4,1,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(68,4,3,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(69,8,1,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(70,8,2,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(71,13,1,0,'2013-06-24 17:07:25','2013-06-24 17:35:42'),(72,13,2,0,'2013-06-24 17:07:25','2013-06-24 17:35:42'),(73,13,1,0,'2013-06-24 17:08:54','2013-06-24 17:35:42'),(74,13,2,0,'2013-06-24 17:08:54','2013-06-24 17:35:42'),(75,13,1,0,'2013-06-24 17:09:59','2013-06-24 17:35:42'),(76,13,2,0,'2013-06-24 17:09:59','2013-06-24 17:35:42'),(77,13,1,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(78,13,2,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(79,10,1,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(80,10,2,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(81,8,1,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(82,8,2,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(83,4,1,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(84,4,3,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(85,11,1,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(86,11,2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(87,5,1,0,'2013-06-28 23:06:09','2013-06-28 23:25:16'),(88,5,2,0,'2013-06-28 23:06:09','2013-06-28 23:25:16'),(89,5,1,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(90,5,2,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(91,15,1,1,'2013-07-02 15:21:12','2013-07-02 15:21:12'),(92,18,1,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(93,19,1,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(94,20,1,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_case_loads` */

DROP TABLE IF EXISTS `employee_case_loads`;

CREATE TABLE `employee_case_loads` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `case_load_id` int(11) default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_case_loads_cl_id` (`case_load_id`),
  KEY `FK_employee_case_loads_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_case_loads_ibfk_1` FOREIGN KEY (`case_load_id`) REFERENCES `case_loads` (`id`),
  CONSTRAINT `employee_case_loads_ibfk_2` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

/*Data for the table `employee_case_loads` */

insert  into `employee_case_loads`(`id`,`employee_detail_id`,`case_load_id`,`is_active`,`created`,`modified`) values (11,8,1,0,'2013-05-30 22:09:42','2013-06-24 17:40:37'),(12,8,3,0,'2013-05-30 22:09:42','2013-06-24 17:40:37'),(13,8,1,0,'2013-05-30 22:12:02','2013-06-24 17:40:37'),(14,8,3,0,'2013-05-30 22:12:02','2013-06-24 17:40:37'),(15,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,3,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,1,0,'2013-06-03 15:02:01','2013-06-24 17:37:16'),(18,10,3,0,'2013-06-03 15:02:01','2013-06-24 17:37:16'),(19,11,1,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(20,11,3,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(21,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(22,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:57'),(23,12,1,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(24,12,3,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(25,13,1,0,'2013-06-04 19:46:17','2013-06-24 17:35:42'),(26,13,3,0,'2013-06-04 19:46:17','2013-06-24 17:35:42'),(27,14,1,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(28,14,3,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(29,13,1,0,'2013-06-10 21:38:16','2013-06-24 17:35:42'),(30,13,3,0,'2013-06-10 21:38:16','2013-06-24 17:35:42'),(31,13,1,0,'2013-06-10 22:16:03','2013-06-24 17:35:42'),(32,13,3,0,'2013-06-10 22:16:03','2013-06-24 17:35:42'),(33,13,1,0,'2013-06-10 22:17:03','2013-06-24 17:35:42'),(34,13,3,0,'2013-06-10 22:17:03','2013-06-24 17:35:42'),(35,13,1,0,'2013-06-10 22:17:26','2013-06-24 17:35:42'),(36,13,3,0,'2013-06-10 22:17:26','2013-06-24 17:35:42'),(37,13,1,0,'2013-06-10 22:17:45','2013-06-24 17:35:42'),(38,13,3,0,'2013-06-10 22:17:45','2013-06-24 17:35:42'),(39,13,1,0,'2013-06-10 22:18:33','2013-06-24 17:35:42'),(40,13,3,0,'2013-06-10 22:18:33','2013-06-24 17:35:42'),(41,13,1,0,'2013-06-10 22:19:02','2013-06-24 17:35:42'),(42,13,3,0,'2013-06-10 22:19:02','2013-06-24 17:35:42'),(43,13,1,0,'2013-06-10 22:19:16','2013-06-24 17:35:42'),(44,13,3,0,'2013-06-10 22:19:16','2013-06-24 17:35:42'),(45,13,1,0,'2013-06-10 22:20:01','2013-06-24 17:35:42'),(46,13,3,0,'2013-06-10 22:20:01','2013-06-24 17:35:42'),(47,13,1,0,'2013-06-10 22:20:42','2013-06-24 17:35:42'),(48,13,3,0,'2013-06-10 22:20:42','2013-06-24 17:35:42'),(49,13,1,0,'2013-06-10 22:22:13','2013-06-24 17:35:42'),(50,13,3,0,'2013-06-10 22:22:13','2013-06-24 17:35:42'),(51,13,1,0,'2013-06-10 22:23:29','2013-06-24 17:35:42'),(52,13,3,0,'2013-06-10 22:23:29','2013-06-24 17:35:42'),(53,13,1,0,'2013-06-10 22:24:56','2013-06-24 17:35:42'),(54,13,3,0,'2013-06-10 22:24:56','2013-06-24 17:35:42'),(55,13,1,0,'2013-06-11 14:43:48','2013-06-24 17:35:42'),(56,13,3,0,'2013-06-11 14:43:48','2013-06-24 17:35:42'),(57,4,1,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(58,4,3,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(59,8,1,0,'2013-06-17 16:05:23','2013-06-24 17:40:37'),(60,8,3,0,'2013-06-17 16:05:23','2013-06-24 17:40:37'),(61,10,1,0,'2013-06-18 18:12:00','2013-06-24 17:37:16'),(62,10,3,0,'2013-06-18 18:12:00','2013-06-24 17:37:16'),(63,4,1,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(64,4,3,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(65,4,1,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(66,4,3,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(67,8,1,0,'2013-06-24 17:03:44','2013-06-24 17:40:37'),(68,8,3,0,'2013-06-24 17:03:44','2013-06-24 17:40:37'),(69,13,1,0,'2013-06-24 17:07:25','2013-06-24 17:35:42'),(70,13,3,0,'2013-06-24 17:07:25','2013-06-24 17:35:42'),(71,13,1,0,'2013-06-24 17:08:54','2013-06-24 17:35:42'),(72,13,3,0,'2013-06-24 17:08:54','2013-06-24 17:35:42'),(73,13,1,0,'2013-06-24 17:09:59','2013-06-24 17:35:42'),(74,13,3,0,'2013-06-24 17:09:59','2013-06-24 17:35:42'),(75,13,1,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(76,13,3,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(77,10,1,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(78,10,3,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(79,8,1,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(80,8,3,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(81,4,1,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(82,4,3,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(83,11,1,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(84,11,3,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(85,5,1,0,'2013-06-28 23:06:09','2013-06-28 23:25:16'),(86,5,3,0,'2013-06-28 23:06:09','2013-06-28 23:25:16'),(87,5,1,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(88,5,3,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(89,15,1,1,'2013-07-02 15:21:12','2013-07-02 15:21:12'),(90,18,1,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(91,19,1,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(92,20,1,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_details` */

DROP TABLE IF EXISTS `employee_details`;

CREATE TABLE `employee_details` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) default NULL,
  `picture_name` varchar(50) default NULL,
  `mobile_saudi` varchar(50) default NULL,
  `landline_saudi` varchar(50) default NULL,
  `mobile_homecountry` varchar(50) default NULL,
  `landline_homecountry` varchar(50) default NULL,
  `jish_email` varchar(50) default NULL,
  `license_type_id` int(11) default NULL,
  `license_number` varchar(50) default NULL,
  `joining_date` date default NULL,
  `contract_expiry_date` date default NULL,
  `gender` enum('male','female') NOT NULL,
  `dob` date default NULL,
  `ip_address` int(10) unsigned default NULL,
  `status` enum('active','delete') NOT NULL default 'active',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_details_u_id` (`user_id`),
  KEY `FK_employee_details_lt_id` (`license_type_id`),
  CONSTRAINT `employee_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `employee_details_ibfk_2` FOREIGN KEY (`license_type_id`) REFERENCES `license_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `employee_details` */

insert  into `employee_details`(`id`,`user_id`,`first_name`,`last_name`,`middle_name`,`picture_name`,`mobile_saudi`,`landline_saudi`,`mobile_homecountry`,`landline_homecountry`,`jish_email`,`license_type_id`,`license_number`,`joining_date`,`contract_expiry_date`,`gender`,`dob`,`ip_address`,`status`,`created`,`modified`) values (4,23,'Ahmed','Ahsan','Ali','0','12345678','1234567','12345678','12345678','a@jish.com',2,'12345678','2013-05-01','2013-05-02','male','2013-05-03',2130706433,'active','2013-05-23 14:47:17','2013-06-24 17:43:15'),(5,25,'Lady','losem','Losem','ca39f38a652b93f15c52fb55c252aade.jpeg','12345678','12345678','12345678','12345678','lady@jish.com',2,'1234567','2013-05-01','2013-05-09','female','2013-05-11',2130706433,'active','2013-05-28 16:29:46','2013-06-28 23:25:15'),(6,26,'Lady','losem','Losem','0bb7911681c5d5b740242948c7270442.png','12345678','12345678','12345678','12345678','ladies@jish.com',NULL,'1234567','2013-05-09','2013-05-17','female','2013-05-31',2130706433,'active','2013-05-28 16:34:57','2013-05-28 16:34:57'),(7,27,'Jason','Losem','Borne','afb5e9b679b7f370a3d7bc3c9a216f34.JPG','12345678','12345678','12345678','12345678','tes1t@jish.com',NULL,'1234567','2013-05-01','2013-05-02','male','2013-05-04',2130706433,'active','2013-05-28 16:37:54','2013-05-28 16:37:54'),(8,28,'Jason','losem','Borne','ee60f0c3e2c9dc4bca6eb8aa88ef99aa.JPG','12345678','12345678','12345678','12345678','tes1t@jish.com',2,'12345678','2013-05-16','2013-05-23','male','2013-05-16',2130706433,'active','2013-05-28 17:17:24','2013-06-24 17:40:36'),(9,30,'John','Abraham','Losem','9b58503846dcc952ff5dfa5f3dc59dd8.jpg','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:00:12','2013-06-03 15:00:12'),(10,31,'John','Abraham','Losem','e92f20e6d739d27a0a24af168980d0d6.JPG','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:02:01','2013-06-24 17:37:15'),(11,32,'John','Abraham','Losem','9029276c41d9e0d56ba5b86678944eda.jpg','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:03:56','2013-06-24 19:14:53'),(12,33,'Jonny','Quest','Bruce','852ef17b5dec1f00d39d740877099941.jpeg','12345678','12345678','12345678','12345678','a@test.com',1,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-03 15:08:51','2013-06-03 15:49:56'),(13,35,'John','Abraham','Losem','d20040931733446f553fa535bd29c65b.jpeg','12345678','12345678','12345678','12345678','losem@jish.com',1,'12345678','2013-06-06','2013-06-04','male','2013-06-04',2130706433,'active','2013-06-04 19:46:17','2013-06-24 17:35:41'),(14,36,'Denzel','Washinton','Losem','4e49065ee66be7e46a1eec0faa24009f.jpeg','12345678','12345678','12345678','12345678','a@test.com',2,'12345678','2013-06-03','2013-06-03','male','2013-06-03',2130706433,'active','2013-06-07 21:18:48','2013-06-07 21:18:48'),(15,37,'azhar','javaid','a','','2312','','','','aa@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:21:11','2013-07-02 15:21:11'),(16,38,'azhar','javaid','a','','2312','','','','aa@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:24:01','2013-07-02 15:24:01'),(17,39,'azhar','javaid','a','','2312','','','','aa@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:26:10','2013-07-02 15:26:10'),(18,40,'azhar','javaid','a','','2312','','','','aa3a@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:28:05','2013-07-02 15:28:05'),(19,41,'azhar','javaid','a','','2312','','','','aa4a@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:30:36','2013-07-02 15:30:36'),(20,42,'azhar','javaid','a','','2312','','','','aa5a@jish3.com',NULL,'2255','2013-07-02','2013-07-31','male','2013-01-23',2130706433,'active','2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_divisions` */

DROP TABLE IF EXISTS `employee_divisions`;

CREATE TABLE `employee_divisions` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_divisions_div_id` (`division_id`),
  KEY `FK_employee_divisions_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_divisions_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
  CONSTRAINT `employee_divisions_ibfk_2` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

/*Data for the table `employee_divisions` */

insert  into `employee_divisions`(`id`,`employee_detail_id`,`division_id`,`is_active`,`created`,`modified`) values (16,9,4,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,9,8,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(20,11,8,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(21,11,9,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(25,12,4,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(26,12,9,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(27,12,10,1,'2013-06-03 15:49:57','2013-06-03 15:49:57'),(30,14,4,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(31,14,8,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(78,13,4,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(79,13,8,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(80,10,9,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(81,10,10,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(82,8,4,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(83,8,8,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(84,4,9,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(85,4,10,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(86,11,10,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(87,11,14,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(88,5,9,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(89,5,10,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(90,5,9,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(91,5,10,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(92,15,4,1,'2013-07-02 15:21:12','2013-07-02 15:21:12'),(93,18,4,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(94,19,4,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(95,19,8,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(96,20,4,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_levels` */

DROP TABLE IF EXISTS `employee_levels`;

CREATE TABLE `employee_levels` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `level_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_levels_ed_id` (`employee_detail_id`),
  KEY `FK_employee_levels_lvl_id` (`level_id`),
  CONSTRAINT `employee_levels_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_levels_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Data for the table `employee_levels` */

insert  into `employee_levels`(`id`,`employee_detail_id`,`level_id`,`is_active`,`created`,`modified`) values (7,8,1,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(8,8,1,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(9,9,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(10,10,1,0,'2013-06-03 15:02:01','2013-06-24 17:37:16'),(11,11,1,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(12,12,1,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(13,12,1,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(14,13,1,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(15,13,3,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(16,14,1,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(17,13,1,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(18,13,3,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(19,13,1,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(20,13,3,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(21,13,1,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(22,13,3,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(23,13,1,0,'2013-06-10 22:17:26','2013-06-24 17:35:41'),(24,13,3,0,'2013-06-10 22:17:26','2013-06-24 17:35:41'),(25,13,1,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(26,13,3,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(27,13,1,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(28,13,3,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(29,13,1,0,'2013-06-10 22:19:02','2013-06-24 17:35:41'),(30,13,3,0,'2013-06-10 22:19:02','2013-06-24 17:35:41'),(31,13,1,0,'2013-06-10 22:19:16','2013-06-24 17:35:41'),(32,13,3,0,'2013-06-10 22:19:16','2013-06-24 17:35:41'),(33,13,1,0,'2013-06-10 22:20:01','2013-06-24 17:35:41'),(34,13,3,0,'2013-06-10 22:20:01','2013-06-24 17:35:41'),(35,13,1,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(36,13,3,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(37,13,1,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(38,13,3,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(39,13,1,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(40,13,3,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(41,13,1,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(42,13,3,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(43,13,1,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(44,13,3,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(45,4,1,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(46,8,1,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(47,10,1,0,'2013-06-18 18:11:59','2013-06-24 17:37:16'),(48,4,1,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(49,4,1,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(50,8,1,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(51,13,1,0,'2013-06-24 17:07:25','2013-06-24 17:35:41'),(52,13,3,0,'2013-06-24 17:07:25','2013-06-24 17:35:41'),(53,13,1,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(54,13,3,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(55,13,1,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(56,13,3,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(57,13,1,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(58,13,3,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(59,10,1,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(60,8,1,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(61,4,1,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(62,11,1,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(63,5,1,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(64,5,1,1,'2013-06-28 23:25:15','2013-06-28 23:25:15'),(65,15,1,1,'2013-07-02 15:21:11','2013-07-02 15:21:11'),(66,18,1,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(67,19,1,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(68,20,1,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_official_titles` */

DROP TABLE IF EXISTS `employee_official_titles`;

CREATE TABLE `employee_official_titles` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `official_title_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_official_title_ed_id` (`employee_detail_id`),
  KEY `FK_employee_official_title_ot_id` (`official_title_id`),
  CONSTRAINT `employee_official_titles_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `employee_official_titles_ibfk_2` FOREIGN KEY (`official_title_id`) REFERENCES `official_titles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

/*Data for the table `employee_official_titles` */

insert  into `employee_official_titles`(`id`,`employee_detail_id`,`official_title_id`,`is_active`,`created`,`modified`) values (11,8,2,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(12,8,3,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(13,8,2,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(14,8,3,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(15,9,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(16,9,3,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(17,10,2,0,'2013-06-03 15:02:01','2013-06-24 17:37:15'),(18,10,3,0,'2013-06-03 15:02:01','2013-06-24 17:37:15'),(19,11,2,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(20,11,3,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(21,12,2,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(22,12,3,0,'2013-06-03 15:08:52','2013-06-03 15:49:56'),(23,12,2,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(24,12,3,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(25,13,2,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(26,13,3,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(27,14,2,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(28,14,3,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(29,13,2,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(30,13,3,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(31,13,2,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(32,13,3,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(33,13,2,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(34,13,3,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(35,13,2,0,'2013-06-10 22:17:26','2013-06-24 17:35:41'),(36,13,3,0,'2013-06-10 22:17:26','2013-06-24 17:35:41'),(37,13,2,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(38,13,3,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(39,13,2,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(40,13,3,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(41,13,2,0,'2013-06-10 22:19:02','2013-06-24 17:35:41'),(42,13,3,0,'2013-06-10 22:19:02','2013-06-24 17:35:41'),(43,13,2,0,'2013-06-10 22:19:16','2013-06-24 17:35:41'),(44,13,3,0,'2013-06-10 22:19:16','2013-06-24 17:35:41'),(45,13,2,0,'2013-06-10 22:20:00','2013-06-24 17:35:41'),(46,13,3,0,'2013-06-10 22:20:00','2013-06-24 17:35:41'),(47,13,2,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(48,13,3,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(49,13,2,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(50,13,3,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(51,13,2,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(52,13,3,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(53,13,2,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(54,13,3,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(55,13,2,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(56,13,3,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(57,4,2,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(58,4,3,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(59,8,2,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(60,8,3,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(61,10,2,0,'2013-06-18 18:11:59','2013-06-24 17:37:15'),(62,10,3,0,'2013-06-18 18:11:59','2013-06-24 17:37:15'),(63,4,2,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(64,4,3,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(65,4,2,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(66,4,3,0,'2013-06-24 16:17:36','2013-06-24 17:43:15'),(67,8,2,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(68,8,3,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(69,13,2,0,'2013-06-24 17:07:25','2013-06-24 17:35:41'),(70,13,3,0,'2013-06-24 17:07:25','2013-06-24 17:35:41'),(71,13,2,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(72,13,3,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(73,13,2,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(74,13,3,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(75,13,2,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(76,13,3,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(77,10,2,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(78,10,3,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(79,8,2,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(80,8,3,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(81,4,2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(82,4,3,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(83,11,2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(84,11,3,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(85,5,2,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(86,5,3,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(87,5,2,1,'2013-06-28 23:25:15','2013-06-28 23:25:15'),(88,5,3,1,'2013-06-28 23:25:15','2013-06-28 23:25:15'),(89,15,2,1,'2013-07-02 15:21:11','2013-07-02 15:21:11'),(90,18,2,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(91,19,2,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(92,20,2,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_working_hour_breakups` */

DROP TABLE IF EXISTS `employee_working_hour_breakups`;

CREATE TABLE `employee_working_hour_breakups` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_working_hour_id` int(11) NOT NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_working_slots_ewh_id` (`employee_working_hour_id`),
  CONSTRAINT `employee_working_hour_breakups_ibfk_1` FOREIGN KEY (`employee_working_hour_id`) REFERENCES `employee_working_hours` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=latin1;

/*Data for the table `employee_working_hour_breakups` */

insert  into `employee_working_hour_breakups`(`id`,`employee_working_hour_id`,`start_time`,`end_time`,`is_active`,`created`,`modified`) values (311,136,'2013-06-24 07:30:00','2013-06-24 07:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(312,136,'2013-06-24 07:45:00','2013-06-24 08:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(313,136,'2013-06-24 08:00:00','2013-06-24 08:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(314,136,'2013-06-24 08:15:00','2013-06-24 08:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(315,136,'2013-06-24 08:30:00','2013-06-24 08:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(316,136,'2013-06-24 08:45:00','2013-06-24 09:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(317,136,'2013-06-24 09:00:00','2013-06-24 09:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(318,136,'2013-06-24 09:15:00','2013-06-24 09:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(319,137,'2013-06-25 07:00:00','2013-06-25 07:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(320,137,'2013-06-25 07:15:00','2013-06-25 07:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(321,137,'2013-06-25 07:30:00','2013-06-25 07:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(322,137,'2013-06-25 07:45:00','2013-06-25 08:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(323,137,'2013-06-25 08:00:00','2013-06-25 08:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(324,137,'2013-06-25 08:15:00','2013-06-25 08:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(325,138,'2013-06-23 06:00:00','2013-06-23 06:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(326,138,'2013-06-23 06:15:00','2013-06-23 06:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(327,138,'2013-06-23 06:30:00','2013-06-23 06:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(328,138,'2013-06-23 06:45:00','2013-06-23 07:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(329,138,'2013-06-23 07:00:00','2013-06-23 07:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(330,138,'2013-06-23 07:15:00','2013-06-23 07:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(331,138,'2013-06-23 07:30:00','2013-06-23 07:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(332,138,'2013-06-23 07:45:00','2013-06-23 08:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(333,138,'2013-06-23 08:00:00','2013-06-23 08:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(334,138,'2013-06-23 08:15:00','2013-06-23 08:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(335,139,'2013-06-26 07:30:00','2013-06-26 07:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(336,139,'2013-06-26 07:45:00','2013-06-26 08:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(337,139,'2013-06-26 08:00:00','2013-06-26 08:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(338,139,'2013-06-26 08:15:00','2013-06-26 08:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(339,139,'2013-06-26 08:30:00','2013-06-26 08:45:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(340,139,'2013-06-26 08:45:00','2013-06-26 09:00:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(341,139,'2013-06-26 09:00:00','2013-06-26 09:15:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(342,139,'2013-06-26 09:15:00','2013-06-26 09:30:00',1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(343,140,'2013-06-23 07:30:00','2013-06-23 07:45:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(344,140,'2013-06-23 07:45:00','2013-06-23 08:00:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(345,140,'2013-06-23 08:00:00','2013-06-23 08:15:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(346,140,'2013-06-23 08:15:00','2013-06-23 08:30:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(347,140,'2013-06-23 08:30:00','2013-06-23 08:45:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(348,140,'2013-06-23 08:45:00','2013-06-23 09:00:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(349,140,'2013-06-23 09:00:00','2013-06-23 09:15:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(350,140,'2013-06-23 09:15:00','2013-06-23 09:30:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(351,141,'2013-06-24 08:00:00','2013-06-24 08:15:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(352,141,'2013-06-24 08:15:00','2013-06-24 08:30:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(353,141,'2013-06-24 08:30:00','2013-06-24 08:45:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(354,141,'2013-06-24 08:45:00','2013-06-24 09:00:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(355,141,'2013-06-24 09:00:00','2013-06-24 09:15:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(356,141,'2013-06-24 09:15:00','2013-06-24 09:30:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(357,141,'2013-06-24 09:30:00','2013-06-24 09:45:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(358,141,'2013-06-24 09:45:00','2013-06-24 10:00:00',1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(359,142,'2013-06-23 07:00:00','2013-06-23 07:15:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(360,142,'2013-06-23 07:15:00','2013-06-23 07:30:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(361,143,'2013-06-24 07:30:00','2013-06-24 07:45:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(362,143,'2013-06-24 07:45:00','2013-06-24 08:00:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(363,144,'2013-06-25 07:00:00','2013-06-25 07:15:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(364,144,'2013-06-25 07:15:00','2013-06-25 07:30:00',1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(365,145,'2013-06-23 08:00:00','2013-06-23 08:15:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(366,145,'2013-06-23 08:15:00','2013-06-23 08:30:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(367,146,'2013-06-24 08:30:00','2013-06-24 08:45:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(368,146,'2013-06-24 08:45:00','2013-06-24 09:00:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(369,147,'2013-06-25 08:00:00','2013-06-25 08:15:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(370,147,'2013-06-25 08:15:00','2013-06-25 08:30:00',1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(371,148,'2013-06-23 06:00:00','2013-06-23 06:15:00',1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(372,148,'2013-06-23 06:15:00','2013-06-23 06:30:00',1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(373,149,'2013-06-24 07:00:00','2013-06-24 07:15:00',1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(374,149,'2013-06-24 07:15:00','2013-06-24 07:30:00',1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(375,150,'2013-06-25 08:00:00','2013-06-25 08:15:00',1,'2013-06-24 19:14:54','2013-06-24 19:14:54'),(376,150,'2013-06-25 08:15:00','2013-06-25 08:30:00',1,'2013-06-24 19:14:54','2013-06-24 19:14:54'),(377,151,'2013-06-23 06:00:00','2013-06-23 06:15:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(378,151,'2013-06-23 06:15:00','2013-06-23 06:30:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(379,152,'2013-06-24 07:00:00','2013-06-24 07:15:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(380,152,'2013-06-24 07:15:00','2013-06-24 07:30:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(381,153,'2013-06-27 06:00:00','2013-06-27 06:15:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(382,153,'2013-06-27 06:15:00','2013-06-27 06:30:00',1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(383,154,'2013-06-30 05:00:00','2013-06-30 05:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(384,154,'2013-06-30 05:15:00','2013-06-30 05:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(385,154,'2013-06-30 05:30:00','2013-06-30 05:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(386,154,'2013-06-30 05:45:00','2013-06-30 06:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(387,154,'2013-06-30 06:00:00','2013-06-30 06:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(388,154,'2013-06-30 06:15:00','2013-06-30 06:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(389,154,'2013-06-30 06:30:00','2013-06-30 06:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(390,154,'2013-06-30 06:45:00','2013-06-30 07:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(391,154,'2013-06-30 07:00:00','2013-06-30 07:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(392,154,'2013-06-30 07:15:00','2013-06-30 07:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(393,154,'2013-06-30 07:30:00','2013-06-30 07:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(394,154,'2013-06-30 07:45:00','2013-06-30 08:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(395,154,'2013-06-30 08:00:00','2013-06-30 08:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(396,154,'2013-06-30 08:15:00','2013-06-30 08:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(397,154,'2013-06-30 08:30:00','2013-06-30 08:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(398,154,'2013-06-30 08:45:00','2013-06-30 09:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(399,154,'2013-06-30 09:00:00','2013-06-30 09:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(400,154,'2013-06-30 09:15:00','2013-06-30 09:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(401,154,'2013-06-30 09:30:00','2013-06-30 09:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(402,154,'2013-06-30 09:45:00','2013-06-30 10:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(403,154,'2013-06-30 10:00:00','2013-06-30 10:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(404,154,'2013-06-30 10:15:00','2013-06-30 10:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(405,154,'2013-06-30 10:30:00','2013-06-30 10:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(406,154,'2013-06-30 10:45:00','2013-06-30 11:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(407,154,'2013-06-30 11:00:00','2013-06-30 11:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(408,154,'2013-06-30 11:15:00','2013-06-30 11:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(409,154,'2013-06-30 11:30:00','2013-06-30 11:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(410,154,'2013-06-30 11:45:00','2013-06-30 12:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(411,154,'2013-06-30 12:00:00','2013-06-30 12:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(412,154,'2013-06-30 12:15:00','2013-06-30 12:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(413,154,'2013-06-30 12:30:00','2013-06-30 12:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(414,154,'2013-06-30 12:45:00','2013-06-30 13:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(415,155,'2013-07-01 05:30:00','2013-07-01 05:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(416,155,'2013-07-01 05:45:00','2013-07-01 06:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(417,155,'2013-07-01 06:00:00','2013-07-01 06:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(418,155,'2013-07-01 06:15:00','2013-07-01 06:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(419,155,'2013-07-01 06:30:00','2013-07-01 06:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(420,155,'2013-07-01 06:45:00','2013-07-01 07:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(421,155,'2013-07-01 07:00:00','2013-07-01 07:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(422,155,'2013-07-01 07:15:00','2013-07-01 07:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(423,155,'2013-07-01 07:30:00','2013-07-01 07:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(424,155,'2013-07-01 07:45:00','2013-07-01 08:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(425,155,'2013-07-01 08:00:00','2013-07-01 08:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(426,155,'2013-07-01 08:15:00','2013-07-01 08:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(427,155,'2013-07-01 08:30:00','2013-07-01 08:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(428,155,'2013-07-01 08:45:00','2013-07-01 09:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(429,155,'2013-07-01 09:00:00','2013-07-01 09:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(430,155,'2013-07-01 09:15:00','2013-07-01 09:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(431,155,'2013-07-01 09:30:00','2013-07-01 09:45:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(432,155,'2013-07-01 09:45:00','2013-07-01 10:00:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(433,155,'2013-07-01 10:00:00','2013-07-01 10:15:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(434,155,'2013-07-01 10:15:00','2013-07-01 10:30:00',1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(435,156,'2013-06-30 05:00:00','2013-06-30 05:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(436,156,'2013-06-30 05:15:00','2013-06-30 05:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(437,156,'2013-06-30 05:30:00','2013-06-30 05:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(438,156,'2013-06-30 05:45:00','2013-06-30 06:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(439,156,'2013-06-30 06:00:00','2013-06-30 06:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(440,156,'2013-06-30 06:15:00','2013-06-30 06:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(441,156,'2013-06-30 06:30:00','2013-06-30 06:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(442,156,'2013-06-30 06:45:00','2013-06-30 07:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(443,156,'2013-06-30 07:00:00','2013-06-30 07:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(444,156,'2013-06-30 07:15:00','2013-06-30 07:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(445,156,'2013-06-30 07:30:00','2013-06-30 07:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(446,156,'2013-06-30 07:45:00','2013-06-30 08:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(447,156,'2013-06-30 08:00:00','2013-06-30 08:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(448,156,'2013-06-30 08:15:00','2013-06-30 08:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(449,157,'2013-07-01 06:30:00','2013-07-01 06:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(450,157,'2013-07-01 06:45:00','2013-07-01 07:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(451,157,'2013-07-01 07:00:00','2013-07-01 07:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(452,157,'2013-07-01 07:15:00','2013-07-01 07:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(453,157,'2013-07-01 07:30:00','2013-07-01 07:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(454,157,'2013-07-01 07:45:00','2013-07-01 08:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(455,157,'2013-07-01 08:00:00','2013-07-01 08:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(456,157,'2013-07-01 08:15:00','2013-07-01 08:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(457,157,'2013-07-01 08:30:00','2013-07-01 08:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(458,157,'2013-07-01 08:45:00','2013-07-01 09:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(459,157,'2013-07-01 09:00:00','2013-07-01 09:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(460,157,'2013-07-01 09:15:00','2013-07-01 09:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(461,157,'2013-07-01 09:30:00','2013-07-01 09:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(462,157,'2013-07-01 09:45:00','2013-07-01 10:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(463,157,'2013-07-01 10:00:00','2013-07-01 10:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(464,157,'2013-07-01 10:15:00','2013-07-01 10:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(465,157,'2013-07-01 10:30:00','2013-07-01 10:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(466,157,'2013-07-01 10:45:00','2013-07-01 11:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(467,157,'2013-07-01 11:00:00','2013-07-01 11:15:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(468,157,'2013-07-01 11:15:00','2013-07-01 11:30:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(469,157,'2013-07-01 11:30:00','2013-07-01 11:45:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(470,157,'2013-07-01 11:45:00','2013-07-01 12:00:00',1,'2013-07-02 15:30:37','2013-07-02 15:30:37'),(471,158,'2013-06-30 05:00:00','2013-06-30 05:15:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(472,158,'2013-06-30 05:15:00','2013-06-30 05:30:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(473,158,'2013-06-30 05:30:00','2013-06-30 05:45:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(474,158,'2013-06-30 05:45:00','2013-06-30 06:00:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(475,158,'2013-06-30 06:00:00','2013-06-30 06:15:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(476,158,'2013-06-30 06:15:00','2013-06-30 06:30:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(477,158,'2013-06-30 06:30:00','2013-06-30 06:45:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(478,158,'2013-06-30 06:45:00','2013-06-30 07:00:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(479,158,'2013-06-30 07:00:00','2013-06-30 07:15:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(480,158,'2013-06-30 07:15:00','2013-06-30 07:30:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(481,158,'2013-06-30 07:30:00','2013-06-30 07:45:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(482,158,'2013-06-30 07:45:00','2013-06-30 08:00:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(483,158,'2013-06-30 08:00:00','2013-06-30 08:15:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(484,158,'2013-06-30 08:15:00','2013-06-30 08:30:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(485,158,'2013-06-30 08:30:00','2013-06-30 08:45:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(486,158,'2013-06-30 08:45:00','2013-06-30 09:00:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(487,158,'2013-06-30 09:00:00','2013-06-30 09:15:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(488,158,'2013-06-30 09:15:00','2013-06-30 09:30:00',1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(489,159,'2013-07-01 06:00:00','2013-07-01 06:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(490,159,'2013-07-01 06:15:00','2013-07-01 06:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(491,159,'2013-07-01 06:30:00','2013-07-01 06:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(492,159,'2013-07-01 06:45:00','2013-07-01 07:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(493,159,'2013-07-01 07:00:00','2013-07-01 07:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(494,159,'2013-07-01 07:15:00','2013-07-01 07:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(495,159,'2013-07-01 07:30:00','2013-07-01 07:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(496,159,'2013-07-01 07:45:00','2013-07-01 08:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(497,159,'2013-07-01 08:00:00','2013-07-01 08:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(498,159,'2013-07-01 08:15:00','2013-07-01 08:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(499,159,'2013-07-01 08:30:00','2013-07-01 08:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(500,159,'2013-07-01 08:45:00','2013-07-01 09:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(501,159,'2013-07-01 09:00:00','2013-07-01 09:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(502,159,'2013-07-01 09:15:00','2013-07-01 09:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(503,159,'2013-07-01 09:30:00','2013-07-01 09:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(504,159,'2013-07-01 09:45:00','2013-07-01 10:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(505,159,'2013-07-01 10:00:00','2013-07-01 10:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(506,159,'2013-07-01 10:15:00','2013-07-01 10:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(507,159,'2013-07-01 10:30:00','2013-07-01 10:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(508,159,'2013-07-01 10:45:00','2013-07-01 11:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(509,159,'2013-07-01 11:00:00','2013-07-01 11:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(510,159,'2013-07-01 11:15:00','2013-07-01 11:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(511,159,'2013-07-01 11:30:00','2013-07-01 11:45:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(512,159,'2013-07-01 11:45:00','2013-07-01 12:00:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(513,159,'2013-07-01 12:00:00','2013-07-01 12:15:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05'),(514,159,'2013-07-01 12:15:00','2013-07-01 12:30:00',1,'2013-07-02 15:33:05','2013-07-02 15:33:05');

/*Table structure for table `employee_working_hours` */

DROP TABLE IF EXISTS `employee_working_hours`;

CREATE TABLE `employee_working_hours` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `working_day` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `total_possible_slots` int(11) NOT NULL default '0',
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_employee_working_hours_ed_id` (`employee_detail_id`),
  CONSTRAINT `employee_working_hours_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

/*Data for the table `employee_working_hours` */

insert  into `employee_working_hours`(`id`,`employee_detail_id`,`division_id`,`working_day`,`start_time`,`end_time`,`total_possible_slots`,`is_active`,`created`,`modified`) values (136,13,4,'monday','2013-06-24 07:30:00','2013-06-24 09:30:00',8,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(137,13,4,'tuesday','2013-06-25 07:00:00','2013-06-25 08:30:00',6,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(138,13,8,'sunday','2013-06-23 06:00:00','2013-06-23 08:30:00',10,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(139,13,8,'wednesday','2013-06-26 07:30:00','2013-06-26 09:30:00',8,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(140,10,9,'sunday','2013-06-23 07:30:00','2013-06-23 09:30:00',8,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(141,10,10,'monday','2013-06-24 08:00:00','2013-06-24 10:00:00',8,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(142,8,4,'sunday','2013-06-23 07:00:00','2013-06-23 07:30:00',2,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(143,8,8,'monday','2013-06-24 07:30:00','2013-06-24 08:00:00',2,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(144,8,8,'tuesday','2013-06-25 07:00:00','2013-06-25 07:30:00',2,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(145,4,9,'sunday','2013-06-23 08:00:00','2013-06-23 08:30:00',2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(146,4,10,'monday','2013-06-24 08:30:00','2013-06-24 09:00:00',2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(147,4,10,'tuesday','2013-06-25 08:00:00','2013-06-25 08:30:00',2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(148,11,14,'sunday','2013-06-23 06:00:00','2013-06-23 06:30:00',2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(149,11,10,'monday','2013-06-24 07:00:00','2013-06-24 07:30:00',2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(150,11,10,'tuesday','2013-06-25 08:00:00','2013-06-25 08:30:00',2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(151,5,9,'sunday','2013-06-23 06:00:00','2013-06-23 06:30:00',2,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(152,5,10,'monday','2013-06-24 07:00:00','2013-06-24 07:30:00',2,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(153,5,10,'thursday','2013-06-27 06:00:00','2013-06-27 06:30:00',2,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(154,18,4,'sunday','2013-06-30 05:00:00','2013-06-30 13:00:00',32,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(155,18,4,'monday','2013-07-01 05:30:00','2013-07-01 10:30:00',20,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(156,19,4,'sunday','2013-06-30 05:00:00','2013-06-30 08:30:00',14,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(157,19,8,'monday','2013-07-01 06:30:00','2013-07-01 12:00:00',22,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(158,20,4,'sunday','2013-06-30 05:00:00','2013-06-30 09:30:00',18,1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(159,20,4,'monday','2013-07-01 06:00:00','2013-07-01 12:30:00',26,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `employee_working_per_week_slots` */

DROP TABLE IF EXISTS `employee_working_per_week_slots`;

CREATE TABLE `employee_working_per_week_slots` (
  `id` bigint(20) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `division_id` int(11) NOT NULL,
  `total_possible_slots` int(11) NOT NULL default '0',
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `employee_working_per_week_slots` */

insert  into `employee_working_per_week_slots`(`id`,`employee_detail_id`,`division_id`,`total_possible_slots`,`is_active`,`created`,`modified`) values (32,13,4,14,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(33,13,8,18,1,'2013-06-24 17:35:42','2013-06-24 17:35:42'),(34,10,9,8,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(35,10,10,8,1,'2013-06-24 17:37:16','2013-06-24 17:37:16'),(36,8,4,2,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(37,8,8,4,1,'2013-06-24 17:40:37','2013-06-24 17:40:37'),(38,4,9,2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(39,4,10,4,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(40,11,14,2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(41,11,10,4,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(42,5,9,2,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(43,5,10,4,1,'2013-06-28 23:25:16','2013-06-28 23:25:16'),(44,18,4,52,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(45,19,4,14,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(46,19,8,22,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(47,20,4,44,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert  into `features`(`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'User Management',2130706433,1,'2013-04-19 20:33:14','2013-04-24 14:44:06'),(2,'Acl Managements',2130706433,1,'2013-04-19 21:18:13','2013-05-10 21:50:24'),(3,'Group Management',2130706433,1,'2013-04-19 21:22:18','2013-04-24 14:44:23'),(4,'Plugins',2130706433,1,'2013-04-19 21:26:44','2013-04-24 14:44:26'),(5,'Others',2130706433,1,'2013-04-22 16:13:27','2013-04-24 14:44:29'),(7,'Test Feature',2130706433,0,'2013-04-24 16:05:54','2013-04-24 16:11:26'),(8,'Feature Management',2130706433,1,'2013-04-24 21:54:03','2013-04-24 21:54:03'),(9,'Employee Details Management',2130706433,1,'2013-06-03 16:28:14','2013-06-03 16:28:14'),(10,'Dashboard',2130706433,1,'2013-06-03 20:58:09','2013-06-03 20:58:09');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Super Admin',2130706433,1,'2013-05-06 16:23:49','2013-05-06 16:40:15'),(2,'Front Desk Officer',2130706433,1,'2013-05-06 16:41:01','2013-05-06 16:41:32'),(3,'Patient',2130706433,1,'2013-05-06 16:41:10','2013-05-06 16:41:10'),(4,'Doctor',2130706433,1,'2013-05-06 16:41:41','2013-05-14 19:20:35'),(5,'Admin',2130706433,1,'2013-05-17 20:52:52','2013-05-17 20:52:52');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) default NULL,
  `is_active` int(11) default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `languages` */

insert  into `languages`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'English',2130706433,1,'2013-05-10 15:29:37','2013-05-13 16:37:02'),(2,'Arabic',2130706433,1,'2013-05-28 14:48:47','2013-05-28 14:48:47'),(3,'Mandarin',2130706433,1,'2013-05-28 14:49:04','2013-05-28 14:49:04'),(4,'Spanish',2130706433,1,'2013-05-28 14:49:17','2013-05-28 14:49:17'),(5,'Japanese',2130706433,1,'2013-05-28 14:49:41','2013-05-28 14:49:41'),(6,'German',2130706433,1,'2013-05-28 14:49:53','2013-05-28 14:49:53');

/*Table structure for table `levels` */

DROP TABLE IF EXISTS `levels`;

CREATE TABLE `levels` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `levels` */

insert  into `levels`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Manager',2130706433,1,'2013-05-13 16:29:34','2013-05-13 16:38:15'),(3,'Supervisor',2130706433,1,'2013-05-13 16:30:00','2013-05-13 16:30:00');

/*Table structure for table `license_types` */

DROP TABLE IF EXISTS `license_types`;

CREATE TABLE `license_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `description` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `license_types` */

insert  into `license_types`(`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA',NULL,2130706433,1,'2013-05-13 17:08:03','2013-05-13 17:08:03'),(2,'OTR',NULL,2130706433,1,'2013-05-13 17:08:09','2013-05-13 17:08:09'),(3,'AAA',NULL,2130706433,1,'2013-05-13 17:08:18','2013-05-13 17:08:35');

/*Table structure for table `modalities` */

DROP TABLE IF EXISTS `modalities`;

CREATE TABLE `modalities` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `modalities` */

insert  into `modalities`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Online Course',2130706433,1,'2013-05-13 18:03:42','2013-05-13 18:03:42'),(2,'DVD',2130706433,1,'2013-05-13 18:04:49','2013-05-13 18:04:49'),(3,'Workshop',2130706433,1,'2013-05-13 18:04:55','2013-05-13 18:05:13');

/*Table structure for table `mpd_activities` */

DROP TABLE IF EXISTS `mpd_activities`;

CREATE TABLE `mpd_activities` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `program_id` int(11) default NULL,
  `mpd_activity_list_id` int(11) default NULL,
  `modality_id` int(11) default NULL,
  `status` enum('pending','complete') default 'pending',
  `date_complition` datetime default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_mpd_activities_ed_id` (`employee_detail_id`),
  KEY `FK_mpd_activities_prm_id` (`program_id`),
  KEY `FK_mpd_activities_mod_id` (`modality_id`),
  KEY `FK_mpd_activities_mpd_atl_id` (`mpd_activity_list_id`),
  CONSTRAINT `mpd_activities_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `mpd_activities_ibfk_2` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  CONSTRAINT `mpd_activities_ibfk_3` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`),
  CONSTRAINT `mpd_activities_ibfk_4` FOREIGN KEY (`mpd_activity_list_id`) REFERENCES `mpd_activity_lists` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `mpd_activities` */

insert  into `mpd_activities`(`id`,`employee_detail_id`,`program_id`,`mpd_activity_list_id`,`modality_id`,`status`,`date_complition`,`is_active`,`created`,`modified`) values (8,4,1,1,1,'pending','2013-05-25 00:00:00',1,'2013-05-28 13:53:47','2013-05-28 13:53:47'),(9,4,1,1,1,'pending','2013-05-24 00:00:00',1,'2013-05-28 14:07:09','2013-05-28 14:07:09'),(10,4,1,1,1,'pending','2013-05-25 00:00:00',1,'2013-05-28 14:07:22','2013-05-28 14:07:22'),(11,4,1,1,1,'pending','2013-05-22 00:00:00',1,'2013-05-28 14:09:35','2013-05-28 14:09:35'),(12,4,2,2,2,'complete','2013-05-22 00:00:00',1,'2013-05-28 14:09:44','2013-05-28 14:16:37'),(13,4,2,2,3,'complete','2013-05-22 00:00:00',1,'2013-05-28 14:09:53','2013-05-28 15:30:28'),(14,14,2,2,2,'pending','2013-06-07 00:00:00',1,'2013-06-07 21:30:27','2013-06-07 21:30:27');

/*Table structure for table `mpd_activity_lists` */

DROP TABLE IF EXISTS `mpd_activity_lists`;

CREATE TABLE `mpd_activity_lists` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mpd_activity_lists` */

insert  into `mpd_activity_lists`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'JARAS',2130706433,1,'2013-05-13 18:19:38','2013-05-13 18:19:38'),(2,'Oral Motors',2130706433,1,'2013-05-13 18:20:33','2013-05-13 18:21:03');

/*Table structure for table `official_titles` */

DROP TABLE IF EXISTS `official_titles`;

CREATE TABLE `official_titles` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `description` text,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `official_titles` */

insert  into `official_titles`(`id`,`name`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (2,'PhD','adf afa daf',2130706433,1,'2013-05-13 14:05:26','2013-05-28 15:01:33'),(3,'Clinical Supervisor','Losem',2130706433,1,'2013-05-28 15:01:46','2013-05-28 15:01:46'),(4,'Pathologist','Losem',2130706433,1,'2013-05-28 15:02:23','2013-05-28 15:02:23');

/*Table structure for table `patient_attachment_reports` */

DROP TABLE IF EXISTS `patient_attachment_reports`;

CREATE TABLE `patient_attachment_reports` (
  `id` int(11) NOT NULL auto_increment,
  `patient_id` int(11) default NULL,
  `appointment_id` int(11) default NULL,
  `attachment_title` varchar(255) default NULL,
  `attachment_file` varchar(500) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `patient_attachment_reports` */

insert  into `patient_attachment_reports`(`id`,`patient_id`,`appointment_id`,`attachment_title`,`attachment_file`,`created`,`modified`) values (1,4,1,'Report','darker.png','2013-06-18 09:54:21','2013-06-18 09:54:21'),(2,4,1,NULL,NULL,'2013-06-15 10:28:51','2013-06-18 10:28:51'),(3,4,1,NULL,NULL,'2013-06-19 08:39:48','2013-06-19 08:39:48'),(4,3,1,NULL,NULL,'2013-06-19 08:40:07','2013-06-19 08:40:07'),(5,5,1,NULL,NULL,'2013-06-20 14:38:59','2013-06-20 14:38:59'),(6,4,1,NULL,NULL,'2013-06-24 13:07:01','2013-06-24 13:07:01'),(8,4,1,'test','hgignore_global.txt','2013-08-27 12:30:17','2013-08-27 12:30:17');

/*Table structure for table `patient_details` */

DROP TABLE IF EXISTS `patient_details`;

CREATE TABLE `patient_details` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `iqama_no` varchar(50) default NULL,
  `marital_status` enum('single','married') default NULL,
  `religion_id` int(11) default NULL,
  `allow_info_from_agencies` tinyint(1) NOT NULL default '1',
  `allow_info_to_agencies` tinyint(1) NOT NULL default '1',
  `multimedia_agreement` varchar(50) default NULL,
  `multimedia_for_research` varchar(50) default NULL,
  `mobile` varchar(50) default NULL,
  `prefered_contact` varchar(50) default NULL,
  `po_box` varchar(50) default NULL,
  `postal_code` varchar(50) default NULL,
  `district` varchar(50) default NULL,
  `city` varchar(50) default NULL,
  `country_id` int(11) default NULL,
  `residence_no` varchar(250) default NULL,
  `gender` enum('male','female') default NULL,
  `dob` date default NULL,
  `first_name` varchar(50) default NULL,
  `last_name` varchar(50) default NULL,
  `middle_name` varchar(50) default NULL,
  `patient_lives_with` varchar(255) default NULL,
  `sibling_order` varchar(255) default NULL,
  `gardian_relationship` varchar(255) default NULL,
  `refered_by` varchar(255) default NULL,
  `status` enum('active','discharged','discontinued','suspended','temporary') NOT NULL default 'temporary',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_patient_details_reg_id` (`religion_id`),
  KEY `FK_patient_details_cty_id` (`country_id`),
  KEY `FK_patient_details_u_id` (`user_id`),
  CONSTRAINT `patient_details_ibfk_2` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`id`),
  CONSTRAINT `patient_details_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `patient_details_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `patient_details` */

insert  into `patient_details`(`id`,`user_id`,`iqama_no`,`marital_status`,`religion_id`,`allow_info_from_agencies`,`allow_info_to_agencies`,`multimedia_agreement`,`multimedia_for_research`,`mobile`,`prefered_contact`,`po_box`,`postal_code`,`district`,`city`,`country_id`,`residence_no`,`gender`,`dob`,`first_name`,`last_name`,`middle_name`,`patient_lives_with`,`sibling_order`,`gardian_relationship`,`refered_by`,`status`,`created`,`modified`) values (4,5,'22222','single',1,0,0,'','','','','','','','',1,'','male',NULL,'Tahir','Rasheed','','parents','oldest','n/a','friend','active','2013-05-17 10:46:22','2013-05-23 15:06:32'),(5,6,'23666','single',1,1,1,'1','1','03463511039','03463511039','20','4056','sindh','karachi',131,'','male','2013-05-01','Ali','Ahmed','','parents','oldest','n/a','friend','discontinued','2013-05-17 11:23:09','2013-05-24 14:37:47'),(10,13,'22222','married',1,1,1,'0','0','03463511039','','','4056','sindh','karachi',131,'','male','2013-05-01','Jason','Borne','','parents','oldest','n/a','teacher','active','2013-05-20 08:56:18','2013-05-24 12:56:28'),(11,16,'66666','single',1,1,0,'0','1','','','','','','',1,'','male',NULL,'Aidan','Duckland','','adf','adfaf','adfaf','','discontinued','2013-05-20 14:21:45','2013-05-24 14:37:30'),(13,18,'66666','single',1,0,1,'1','0','03463511039','','20','4056','sindh','karachi',1,'','male','2013-05-07','Richeal','Borne','','parents','oldest','n/a','friend','active','2013-05-21 15:10:14','2013-06-04 12:03:55'),(14,19,'66666','single',1,0,1,'1','0','03463511039','','20','4056','sindh','karachi',1,'house no 123','male','2013-05-07','Ahmed','Zafar','','parents','oldest','n/a','friend','active','2013-05-21 15:11:53','2013-06-04 14:26:02'),(15,29,'12345','single',1,0,0,'','','','','','','','',1,'','male',NULL,'Shan','Jafri','raza','ss','2','dd','','temporary','2013-05-30 10:49:59','2013-05-30 10:50:00'),(16,34,'22222','single',1,0,0,'0','0','','','','','','',1,'','male',NULL,'Umair','Aziz','aa','aa','aa','adf','','temporary','2013-06-04 12:58:47','2013-06-04 14:12:07');

/*Table structure for table `patient_family_details` */

DROP TABLE IF EXISTS `patient_family_details`;

CREATE TABLE `patient_family_details` (
  `id` int(11) NOT NULL auto_increment,
  `patient_detail_id` bigint(20) NOT NULL,
  `name` varchar(50) default NULL,
  `email` varchar(50) default NULL,
  `work_number` varchar(50) default NULL,
  `mobile_number` varchar(50) default NULL,
  `occupation` varchar(50) default NULL,
  `relation_ship_id` int(11) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_patient_family_details_pd_id` (`patient_detail_id`),
  KEY `FK_patient_family_details_rs_id` (`relation_ship_id`),
  CONSTRAINT `patient_family_details_ibfk_1` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `patient_family_details_ibfk_2` FOREIGN KEY (`relation_ship_id`) REFERENCES `relation_ships` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `patient_family_details` */

insert  into `patient_family_details`(`id`,`patient_detail_id`,`name`,`email`,`work_number`,`mobile_number`,`occupation`,`relation_ship_id`,`is_active`,`created`,`modified`) values (1,10,'test','aa@aa.com','222','979762','php',1,1,'2013-05-20 08:56:18','2013-05-24 12:56:28'),(2,10,'test','bb@aa.com','222','235454','testing',2,1,'2013-05-20 08:56:18','2013-05-24 12:56:28'),(3,13,'abc','aa@aa.com','222','979762','php',1,1,'2013-05-21 15:10:14','2013-06-04 12:03:55'),(4,14,'abc','aa@aa.com','222','979762','php',1,1,'2013-05-21 15:11:53','2013-06-04 14:26:02');

/*Table structure for table `patient_medication_details` */

DROP TABLE IF EXISTS `patient_medication_details`;

CREATE TABLE `patient_medication_details` (
  `patient_medication_id` int(11) default NULL,
  `attribute_name` varchar(255) default NULL,
  `attribute_value` text,
  `attribute_order` int(11) default NULL,
  `attribute_type` varchar(255) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `patient_medication_details` */

insert  into `patient_medication_details`(`patient_medication_id`,`attribute_name`,`attribute_value`,`attribute_order`,`attribute_type`) values (1,'test','test hello',NULL,NULL),(1,'test new','on',NULL,NULL),(1,'test2','<p>test editor</p>',NULL,NULL),(2,'test new','on',NULL,NULL),(2,'test','test hello template',NULL,NULL),(4,'test','test da',NULL,'text'),(4,'test2','<p>adfaf daf dfa dfad fda fdaf adfaf</p>',NULL,'texteditor'),(5,'name','thr',NULL,'text'),(5,'test','sfdsf',NULL,'text'),(5,'test new','on',NULL,'checkbox'),(5,'test','sfdsf',NULL,'text'),(5,'test2','<p>sfsfsdf</p>',NULL,'texteditor'),(6,'test','test da',NULL,'text'),(6,'test2','<table style=\"height: 44px;\" width=\"883\">\r\n<tbody>\r\n<tr>\r\n<td>adf</td>\r\n<td>dfd</td>\r\n<td>dfdfd</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>',NULL,'texteditor');

/*Table structure for table `patient_medications` */

DROP TABLE IF EXISTS `patient_medications`;

CREATE TABLE `patient_medications` (
  `id` int(11) NOT NULL auto_increment,
  `patient_id` int(11) default NULL,
  `employee_id` int(11) default NULL,
  `appointment_id` int(11) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `patient_medications` */

insert  into `patient_medications`(`id`,`patient_id`,`employee_id`,`appointment_id`,`created`,`modified`) values (1,4,13,1,'2013-06-18 09:54:21','2013-06-18 09:54:21'),(2,4,4,1,'2013-06-15 10:28:51','2013-06-18 10:28:51'),(3,4,13,1,'2013-06-19 08:39:48','2013-06-19 08:39:48'),(4,4,13,1,'2013-06-19 08:40:07','2013-06-19 08:40:07'),(5,4,13,1,'2013-06-20 14:38:59','2013-06-20 14:38:59'),(6,4,13,1,'2013-06-24 13:07:01','2013-06-24 13:07:01');

/*Table structure for table `patient_payment_logs` */

DROP TABLE IF EXISTS `patient_payment_logs`;

CREATE TABLE `patient_payment_logs` (
  `id` bigint(20) NOT NULL auto_increment,
  `appointment_id` bigint(20) NOT NULL,
  `amout` decimal(5,3) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_patient_payment_logs_app_id` (`appointment_id`),
  CONSTRAINT `patient_payment_logs_ibfk_1` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `patient_payment_logs` */

/*Table structure for table `patient_waiting_lists` */

DROP TABLE IF EXISTS `patient_waiting_lists`;

CREATE TABLE `patient_waiting_lists` (
  `id` bigint(20) NOT NULL auto_increment,
  `patient_detail_id` bigint(20) NOT NULL,
  `waiting_list_title_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_patient_waiting_lists_pd_id` (`patient_detail_id`),
  KEY `FK_patient_waiting_lists_wlt_id` (`waiting_list_title_id`),
  CONSTRAINT `patient_waiting_lists_ibfk_1` FOREIGN KEY (`patient_detail_id`) REFERENCES `patient_details` (`id`),
  CONSTRAINT `patient_waiting_lists_ibfk_2` FOREIGN KEY (`waiting_list_title_id`) REFERENCES `waiting_lists_titles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `patient_waiting_lists` */

insert  into `patient_waiting_lists`(`id`,`patient_detail_id`,`waiting_list_title_id`,`is_active`,`created`,`modified`) values (5,10,2,1,'2013-05-22 14:00:36','2013-05-22 14:00:36'),(6,4,2,1,'2013-05-22 14:03:25','2013-05-22 14:03:25'),(7,15,4,1,'2013-05-30 10:50:44','2013-05-30 10:50:44'),(8,13,3,1,'2013-05-30 14:58:10','2013-05-30 14:58:10'),(9,13,14,1,'2013-06-04 11:21:07','2013-06-04 11:21:07');

/*Table structure for table `programs` */

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `programs` */

insert  into `programs`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA CCC',2130706433,1,'2013-05-07 11:19:44','2013-05-10 21:50:59'),(2,'JCCC',2130706433,1,'2013-05-07 11:25:15','2013-05-13 18:23:41');

/*Table structure for table `providers` */

DROP TABLE IF EXISTS `providers`;

CREATE TABLE `providers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `providers` */

insert  into `providers`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'ASHA',2130706433,1,'2013-05-13 18:31:00','2013-05-13 18:31:00'),(2,'JISH',2130706433,1,'2013-05-13 18:31:37','2013-05-13 18:31:37'),(3,'HANEN',2130706433,1,'2013-05-13 18:31:44','2013-05-13 18:31:59');

/*Table structure for table `relation_ships` */

DROP TABLE IF EXISTS `relation_ships`;

CREATE TABLE `relation_ships` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `relation_ships` */

insert  into `relation_ships`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Father',2130706433,1,'2013-05-13 18:37:13','2013-05-13 18:37:13'),(2,'Mother',2130706433,1,'2013-05-13 18:37:20','2013-05-13 18:37:20'),(3,'Guardian',2130706433,1,'2013-05-13 18:37:27','2013-05-13 18:37:40');

/*Table structure for table `religions` */

DROP TABLE IF EXISTS `religions`;

CREATE TABLE `religions` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `religions` */

insert  into `religions`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Islam',2130706433,1,'2013-05-13 18:44:21','2013-05-13 18:46:10'),(2,'Hinduism',2130706433,1,'2013-05-13 18:45:04','2013-05-13 18:46:05'),(3,'Buddhism',2130706433,1,'2013-05-21 11:11:16','2013-05-21 11:11:16'),(4,'abc',2130706433,1,'2013-05-21 11:35:36','2013-05-21 11:35:36'),(5,'testing',2130706433,1,'2013-05-21 13:33:17','2013-05-21 13:33:17'),(6,'newreligion',2130706433,1,'2013-05-21 13:36:54','2013-05-21 13:36:54'),(7,'ggg',2130706433,1,'2013-05-21 13:41:21','2013-05-21 13:41:21'),(8,'yy',2130706433,1,'2013-05-21 13:49:43','2013-05-21 13:49:43'),(9,'zzz',2130706433,1,'2013-05-21 13:52:32','2013-05-21 13:52:32'),(10,'new',2130706433,1,'2013-05-21 14:09:03','2013-05-21 14:09:03');

/*Table structure for table `search_index` */

DROP TABLE IF EXISTS `search_index`;

CREATE TABLE `search_index` (
  `id` int(11) NOT NULL auto_increment,
  `association_key` int(11) NOT NULL,
  `model` varchar(128) collate utf8_unicode_ci NOT NULL,
  `data` longtext collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `association_key` (`association_key`,`model`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `search_index` */

/*Table structure for table `sponsors` */

DROP TABLE IF EXISTS `sponsors`;

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `ip_address` int(10) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sponsors` */

insert  into `sponsors`(`id`,`name`,`is_active`,`ip_address`,`created`,`modified`) values (1,'Sponsor Losem',1,2130706433,'2013-05-07 08:51:07','2013-05-10 21:50:48'),(2,'testing 123',0,2130706433,'2013-05-07 15:40:32','2013-05-07 15:56:43'),(3,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:56:47'),(4,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:45:00'),(5,'testing 123',0,2130706433,'2013-05-07 15:42:15','2013-05-07 15:46:45'),(6,'testing 123',0,2130706433,'2013-05-07 15:44:47','2013-05-07 15:46:48');

/*Table structure for table `template_attributes` */

DROP TABLE IF EXISTS `template_attributes`;

CREATE TABLE `template_attributes` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(500) default NULL,
  `attr_type` varchar(255) default NULL,
  `category_id` int(11) default NULL,
  `order` int(11) default NULL,
  `is_active` int(11) default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `ip_address` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `template_attributes` */

insert  into `template_attributes`(`id`,`title`,`attr_type`,`category_id`,`order`,`is_active`,`created`,`modified`,`ip_address`) values (1,'test','text',2,NULL,1,'2013-06-11 14:35:27','2013-06-11 14:35:27',NULL),(2,'test2','texteditor',3,NULL,1,'2013-06-11 14:35:37','2013-06-11 14:35:37',NULL),(3,'test new','checkbox',2,NULL,1,'2013-06-11 14:35:47','2013-06-11 14:35:47',NULL),(4,'name','text',2,NULL,1,'2013-06-20 14:24:57','2013-06-20 14:24:57',NULL),(5,'description','textarea',3,NULL,1,'2013-06-21 12:21:31','2013-06-21 13:56:50',NULL),(6,'location','text',2,NULL,1,'2013-06-21 14:00:25','2013-06-21 14:00:25',NULL);

/*Table structure for table `template_attributes_categories` */

DROP TABLE IF EXISTS `template_attributes_categories`;

CREATE TABLE `template_attributes_categories` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `is_active` int(11) default '1',
  `ip_address` int(11) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `template_attributes_categories` */

insert  into `template_attributes_categories`(`id`,`name`,`is_active`,`ip_address`,`created`,`modified`) values (1,'Special',0,0,NULL,'0000-00-00 00:00:00'),(2,'General',1,NULL,'2013-06-21 13:27:43','2013-06-21 13:27:43'),(3,'Special',1,NULL,'2013-06-21 13:27:53','2013-06-21 13:27:53');

/*Table structure for table `template_attributes_items` */

DROP TABLE IF EXISTS `template_attributes_items`;

CREATE TABLE `template_attributes_items` (
  `id` int(11) NOT NULL auto_increment,
  `template_id` int(10) unsigned NOT NULL,
  `template_attribute_id` int(11) default NULL,
  `order` int(11) default NULL,
  `is_active` int(11) default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_template_attributes_items2` (`template_id`),
  CONSTRAINT `FK_template_attributes_items2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `template_attributes_items` */

insert  into `template_attributes_items`(`id`,`template_id`,`template_attribute_id`,`order`,`is_active`,`created`,`modified`) values (1,1,1,1,1,'2013-06-18 11:41:54','2013-06-18 11:41:54'),(2,1,2,2,1,'2013-06-18 11:41:54','2013-06-18 11:41:54'),(3,2,3,1,1,'2013-06-18 11:42:22','2013-06-18 11:42:22'),(4,2,2,2,1,'2013-06-18 11:42:22','2013-06-18 11:42:22'),(5,5,3,1,1,'2013-06-18 12:30:01','2013-06-18 12:30:01'),(6,5,2,2,1,'2013-06-18 12:30:01','2013-06-18 12:30:01'),(7,5,1,3,1,'2013-06-18 12:30:01','2013-06-18 12:30:01'),(20,6,4,1,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(21,6,1,2,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(22,6,3,3,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(23,6,1,4,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(24,6,2,5,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(25,6,3,6,1,'2013-06-24 12:53:50','2013-06-24 12:53:50'),(26,6,5,7,1,'2013-06-24 12:53:50','2013-06-24 12:53:50');

/*Table structure for table `templates` */

DROP TABLE IF EXISTS `templates`;

CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `template_name` varchar(500) default NULL,
  `description` text,
  `division_id` int(11) default NULL,
  `ip_address` int(11) default NULL,
  `is_active` int(11) default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `templates` */

insert  into `templates`(`id`,`template_name`,`description`,`division_id`,`ip_address`,`is_active`,`created`,`modified`) values (1,'template test','this is description',4,2130706433,1,'2013-06-18 11:41:54','2013-06-18 11:41:54'),(2,'template new','this is description',9,2130706433,1,'2013-06-18 11:42:21','2013-06-18 11:42:21'),(5,'template new dup','this is description',9,2130706433,1,'2013-06-18 12:30:01','2013-06-18 12:30:01'),(6,'slp abc','gfgfg',4,2130706433,1,'2013-06-20 14:35:35','2013-06-24 12:53:50');

/*Table structure for table `tmps` */

DROP TABLE IF EXISTS `tmps`;

CREATE TABLE `tmps` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `target_id` varchar(50) NOT NULL COMMENT 'string field holding the unique ID of a record for e.g registration',
  `type` enum('registration','forgot_password','file_upload') NOT NULL,
  `data` mediumtext NOT NULL,
  `expire_at` date NOT NULL COMMENT 'Date on which the row will be expired / Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `tmps` */

insert  into `tmps`(`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (1,'5bd4ff00005569a4508825ad749e7032.jpg','file_upload','{\"name\":\"5bd4ff00005569a4508825ad749e7032.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/5bd4ff00005569a4508825ad749e7032.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/5bd4ff00005569a4508825ad749e7032.jpg\"}','2013-08-22','2013-05-22 20:16:55','0000-00-00 00:00:00'),(2,'a568ed5b18b26af6bb768fa7d6c4e032.jpg','file_upload','{\"name\":\"a568ed5b18b26af6bb768fa7d6c4e032.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/a568ed5b18b26af6bb768fa7d6c4e032.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/a568ed5b18b26af6bb768fa7d6c4e032.jpg\"}','2013-08-22','2013-05-22 20:18:07','0000-00-00 00:00:00'),(3,'5b8fe179175ce41ea4aac3cd11c901de.jpg','file_upload','{\"name\":\"5b8fe179175ce41ea4aac3cd11c901de.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/5b8fe179175ce41ea4aac3cd11c901de.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/5b8fe179175ce41ea4aac3cd11c901de.jpg\"}','2013-08-22','2013-05-22 20:21:11','0000-00-00 00:00:00'),(4,'7646f7d5f1135852239763d2c030cd31.jpg','file_upload','{\"name\":\"7646f7d5f1135852239763d2c030cd31.jpg\",\"size\":1696,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/7646f7d5f1135852239763d2c030cd31.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/7646f7d5f1135852239763d2c030cd31.jpg\"}','2013-08-22','2013-05-22 20:21:36','0000-00-00 00:00:00'),(5,'014ecca9ed4906d5432672115131f601.','file_upload','{\"name\":\"014ecca9ed4906d5432672115131f601.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 21:31:49','0000-00-00 00:00:00'),(6,'63a867f123c68d1f6eb19299aba5cc0a.jpg','file_upload','{\"name\":\"63a867f123c68d1f6eb19299aba5cc0a.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/63a867f123c68d1f6eb19299aba5cc0a.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/63a867f123c68d1f6eb19299aba5cc0a.jpg\"}','2013-08-22','2013-05-22 21:53:48','0000-00-00 00:00:00'),(7,'96ab9a841c93af9353f294467afb5c9b.jpg','file_upload','{\"name\":\"96ab9a841c93af9353f294467afb5c9b.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/96ab9a841c93af9353f294467afb5c9b.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/96ab9a841c93af9353f294467afb5c9b.jpg\"}','2013-08-22','2013-05-22 21:58:58','0000-00-00 00:00:00'),(8,'735370bc08613dfd9d8fa2047a2e6f1f.jpg','file_upload','{\"name\":\"735370bc08613dfd9d8fa2047a2e6f1f.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/735370bc08613dfd9d8fa2047a2e6f1f.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/735370bc08613dfd9d8fa2047a2e6f1f.jpg\"}','2013-08-22','2013-05-22 22:00:06','0000-00-00 00:00:00'),(9,'8abf5144878fbcf7328b96cb2af510dc.','file_upload','{\"name\":\"8abf5144878fbcf7328b96cb2af510dc.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 22:03:38','0000-00-00 00:00:00'),(10,'c7d127109783ff0fa615b756359eecf2.jpg','file_upload','{\"name\":\"c7d127109783ff0fa615b756359eecf2.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/c7d127109783ff0fa615b756359eecf2.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/c7d127109783ff0fa615b756359eecf2.jpg\"}','2013-08-22','2013-05-22 22:07:19','0000-00-00 00:00:00'),(11,'4d142e88180bfcb9f89d138334963a94.','file_upload','{\"name\":\"4d142e88180bfcb9f89d138334963a94.\",\"size\":0,\"type\":\"\",\"code\":\"1\",\"error\":4}','2013-08-22','2013-05-22 22:09:39','0000-00-00 00:00:00'),(12,'fcabc789b79cd9e98e6df86bc1e46c3b.jpg','file_upload','{\"name\":\"fcabc789b79cd9e98e6df86bc1e46c3b.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/fcabc789b79cd9e98e6df86bc1e46c3b.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/fcabc789b79cd9e98e6df86bc1e46c3b.jpg\"}','2013-08-22','2013-05-22 22:28:15','0000-00-00 00:00:00'),(13,'c9c6b685ab04448be7ada34041ad4d9d.jpg','file_upload','{\"name\":\"c9c6b685ab04448be7ada34041ad4d9d.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/c9c6b685ab04448be7ada34041ad4d9d.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/c9c6b685ab04448be7ada34041ad4d9d.jpg\"}','2013-08-22','2013-05-22 22:31:04','0000-00-00 00:00:00'),(14,'00cdbb91091473fdde083a983c4a1943.jpg','file_upload','{\"name\":\"00cdbb91091473fdde083a983c4a1943.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/00cdbb91091473fdde083a983c4a1943.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/00cdbb91091473fdde083a983c4a1943.jpg\"}','2013-08-23','2013-05-23 14:01:45','0000-00-00 00:00:00'),(15,'cbcb89006dd76717c45bcd4f165773d6.jpg','file_upload','{\"name\":\"cbcb89006dd76717c45bcd4f165773d6.jpg\",\"size\":1907,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/cbcb89006dd76717c45bcd4f165773d6.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/cbcb89006dd76717c45bcd4f165773d6.jpg\"}','2013-08-23','2013-05-23 14:31:50','0000-00-00 00:00:00'),(16,'702b8521ca44529a8b23995222a0dcf8.png','file_upload','{\"name\":\"702b8521ca44529a8b23995222a0dcf8.png\",\"size\":34952,\"type\":\"image\\/png\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/702b8521ca44529a8b23995222a0dcf8.png\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/702b8521ca44529a8b23995222a0dcf8.png\"}','2013-08-27','2013-05-27 20:14:51','0000-00-00 00:00:00'),(17,'26c4b20e25eb19e401e0156189be5e90.JPG','file_upload','{\"name\":\"26c4b20e25eb19e401e0156189be5e90.JPG\",\"size\":6940,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/26c4b20e25eb19e401e0156189be5e90.JPG\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/26c4b20e25eb19e401e0156189be5e90.JPG\"}','2013-08-27','2013-05-27 20:16:39','0000-00-00 00:00:00'),(18,'3997fbc47c102262e635988bf5798c6a.jpeg','file_upload','{\"name\":\"3997fbc47c102262e635988bf5798c6a.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/3997fbc47c102262e635988bf5798c6a.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/3997fbc47c102262e635988bf5798c6a.jpeg\"}','2013-08-27','2013-05-27 20:26:17','0000-00-00 00:00:00'),(19,'1997f6f675454c7ca0c35f79fed19b81.jpeg','file_upload','{\"name\":\"1997f6f675454c7ca0c35f79fed19b81.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/1997f6f675454c7ca0c35f79fed19b81.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/1997f6f675454c7ca0c35f79fed19b81.jpeg\"}','2013-08-27','2013-05-27 20:28:22','0000-00-00 00:00:00'),(20,'31109e56c71550d59086f75a64e57a87.jpg','file_upload','{\"name\":\"31109e56c71550d59086f75a64e57a87.jpg\",\"size\":50903,\"type\":\"image\\/jpeg\",\"code\":\"0\"}','2013-08-28','2013-05-28 16:25:15','0000-00-00 00:00:00'),(24,'7913aa5aac3f455b4fff321ea1356de2.JPG','file_upload','{\"name\":\"7913aa5aac3f455b4fff321ea1356de2.JPG\",\"size\":26517,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/7913aa5aac3f455b4fff321ea1356de2.JPG\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/7913aa5aac3f455b4fff321ea1356de2.JPG\"}','2013-08-28','2013-05-28 17:16:28','0000-00-00 00:00:00'),(25,'f81fffc3bf56ab52adf117c07b185e4e.jpg','file_upload','{\"name\":\"f81fffc3bf56ab52adf117c07b185e4e.jpg\",\"size\":10962,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/f81fffc3bf56ab52adf117c07b185e4e.jpg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/f81fffc3bf56ab52adf117c07b185e4e.jpg\"}','2013-09-03','2013-06-03 14:57:19','0000-00-00 00:00:00'),(26,'a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg','file_upload','{\"name\":\"a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\",\"size\":45096,\"type\":\"image\\/jpeg\",\"code\":\"0\",\"upload_url\":\"profile_pic\\/original\\/a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\",\"thumbnail_url\":\"profile_pic\\/thumbnail\\/a748f42ff5ea5bd3e8a0e2069dcf1dbb.jpeg\"}','2013-09-04','2013-06-04 19:18:38','0000-00-00 00:00:00');

/*Table structure for table `user_languages` */

DROP TABLE IF EXISTS `user_languages`;

CREATE TABLE `user_languages` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `language_id` int(11) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_users_languages_u_id` (`user_id`),
  KEY `FK_users_languages_lang_id` (`language_id`),
  CONSTRAINT `user_languages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

/*Data for the table `user_languages` */

insert  into `user_languages`(`id`,`user_id`,`language_id`,`is_active`,`created`,`modified`) values (21,28,1,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(22,28,2,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(23,28,3,0,'2013-05-30 22:09:42','2013-06-24 17:40:36'),(24,28,1,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(25,28,2,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(26,28,3,0,'2013-05-30 22:12:02','2013-06-24 17:40:36'),(27,30,1,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(28,30,2,1,'2013-06-03 15:00:12','2013-06-03 15:00:12'),(29,31,1,0,'2013-06-03 15:02:01','2013-06-24 17:37:15'),(30,31,2,0,'2013-06-03 15:02:01','2013-06-24 17:37:15'),(31,32,1,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(32,32,2,0,'2013-06-03 15:03:56','2013-06-24 19:14:53'),(33,33,1,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(34,33,2,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(35,33,3,0,'2013-06-03 15:08:51','2013-06-03 15:49:56'),(36,33,1,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(37,33,2,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(38,33,3,1,'2013-06-03 15:49:56','2013-06-03 15:49:56'),(39,35,1,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(40,35,2,0,'2013-06-04 19:46:17','2013-06-24 17:35:41'),(41,36,1,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(42,36,2,1,'2013-06-07 21:18:48','2013-06-07 21:18:48'),(43,35,1,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(44,35,2,0,'2013-06-10 21:38:15','2013-06-24 17:35:41'),(45,35,1,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(46,35,2,0,'2013-06-10 22:16:02','2013-06-24 17:35:41'),(47,35,1,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(48,35,2,0,'2013-06-10 22:17:03','2013-06-24 17:35:41'),(49,35,1,0,'2013-06-10 22:17:25','2013-06-24 17:35:41'),(50,35,2,0,'2013-06-10 22:17:25','2013-06-24 17:35:41'),(51,35,1,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(52,35,2,0,'2013-06-10 22:17:44','2013-06-24 17:35:41'),(53,35,1,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(54,35,2,0,'2013-06-10 22:18:33','2013-06-24 17:35:41'),(55,35,1,0,'2013-06-10 22:19:01','2013-06-24 17:35:41'),(56,35,2,0,'2013-06-10 22:19:01','2013-06-24 17:35:41'),(57,35,1,0,'2013-06-10 22:19:15','2013-06-24 17:35:41'),(58,35,2,0,'2013-06-10 22:19:15','2013-06-24 17:35:41'),(59,35,1,0,'2013-06-10 22:20:00','2013-06-24 17:35:41'),(60,35,2,0,'2013-06-10 22:20:00','2013-06-24 17:35:41'),(61,35,1,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(62,35,2,0,'2013-06-10 22:20:42','2013-06-24 17:35:41'),(63,35,1,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(64,35,2,0,'2013-06-10 22:22:13','2013-06-24 17:35:41'),(65,35,1,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(66,35,2,0,'2013-06-10 22:23:28','2013-06-24 17:35:41'),(67,35,1,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(68,35,2,0,'2013-06-10 22:24:55','2013-06-24 17:35:41'),(69,35,1,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(70,35,2,0,'2013-06-11 14:43:48','2013-06-24 17:35:41'),(71,23,1,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(72,23,2,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(73,23,3,0,'2013-06-17 15:53:01','2013-06-24 17:43:15'),(74,28,1,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(75,28,2,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(76,28,3,0,'2013-06-17 16:05:23','2013-06-24 17:40:36'),(77,31,1,0,'2013-06-18 18:11:59','2013-06-24 17:37:15'),(78,31,2,0,'2013-06-18 18:11:59','2013-06-24 17:37:15'),(79,23,1,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(80,23,2,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(81,23,3,0,'2013-06-21 18:09:03','2013-06-24 17:43:15'),(82,23,1,0,'2013-06-24 16:17:35','2013-06-24 17:43:15'),(83,23,2,0,'2013-06-24 16:17:35','2013-06-24 17:43:15'),(84,23,3,0,'2013-06-24 16:17:35','2013-06-24 17:43:15'),(85,28,1,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(86,28,2,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(87,28,3,0,'2013-06-24 17:03:44','2013-06-24 17:40:36'),(88,35,1,0,'2013-06-24 17:07:24','2013-06-24 17:35:41'),(89,35,2,0,'2013-06-24 17:07:24','2013-06-24 17:35:41'),(90,35,1,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(91,35,2,0,'2013-06-24 17:08:54','2013-06-24 17:35:41'),(92,35,1,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(93,35,2,0,'2013-06-24 17:09:59','2013-06-24 17:35:41'),(94,35,1,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(95,35,2,1,'2013-06-24 17:35:41','2013-06-24 17:35:41'),(96,31,1,1,'2013-06-24 17:37:15','2013-06-24 17:37:15'),(97,31,2,1,'2013-06-24 17:37:15','2013-06-24 17:37:15'),(98,28,1,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(99,28,2,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(100,28,3,1,'2013-06-24 17:40:36','2013-06-24 17:40:36'),(101,23,1,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(102,23,2,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(103,23,3,1,'2013-06-24 17:43:15','2013-06-24 17:43:15'),(104,32,1,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(105,32,2,1,'2013-06-24 19:14:53','2013-06-24 19:14:53'),(106,25,1,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(107,25,2,0,'2013-06-28 23:06:09','2013-06-28 23:25:15'),(108,25,1,1,'2013-06-28 23:25:15','2013-06-28 23:25:15'),(109,25,2,1,'2013-06-28 23:25:15','2013-06-28 23:25:15'),(110,37,1,1,'2013-07-02 15:21:11','2013-07-02 15:21:11'),(111,40,1,1,'2013-07-02 15:28:05','2013-07-02 15:28:05'),(112,41,1,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(113,41,2,1,'2013-07-02 15:30:36','2013-07-02 15:30:36'),(114,42,1,1,'2013-07-02 15:33:04','2013-07-02 15:33:04'),(115,42,2,1,'2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `user_id` bigint(11) unsigned NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) default NULL,
  `image_url` varchar(255) default NULL,
  `type_id` enum('facebook','twitter','linkedin') NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `access_token_secret` varchar(255) default NULL,
  `is_valid_token` tinyint(1) NOT NULL default '1',
  `screen_name` varchar(255) NOT NULL,
  `extra` mediumtext COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') NOT NULL default 'active',
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_social_accounts` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL COMMENT 'username is basically email',
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_type` enum('front_desk','patient','doctor') NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `status` enum('active','disabled','deleted','suspended') NOT NULL default 'active',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_users_grp_id` (`group_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`group_id`,`user_type`,`ip_address`,`status`,`created`,`modified`) values (1,'shakeel@test.com','68c91a594329677d6041c1c899c85272',1,'doctor',2130706433,'active','2013-05-06 16:26:39','2013-05-06 16:26:39'),(2,'test@test.com','68c91a594329677d6041c1c899c85272',1,'doctor',2130706433,'active','2013-05-06 16:26:39','2013-05-06 16:26:39'),(5,'tr.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-17 10:46:22','2013-05-23 15:06:32'),(6,'tr2.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-17 11:23:09','2013-05-22 09:14:16'),(13,'tr4.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-20 08:56:18','2013-05-24 12:56:28'),(16,'aa@aa.com','',1,'patient',2130706433,'active','2013-05-20 14:21:45','2013-05-23 08:58:40'),(18,'tr5.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-21 15:10:14','2013-06-04 12:03:55'),(19,'tr6.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-05-21 15:11:53','2013-06-04 14:26:02'),(23,'ahmed@gmail.com','68c91a594329677d6041c1c899c85272',4,'doctor',2130706433,'active','2013-05-23 14:47:17','2013-06-24 17:43:15'),(25,'lady@gmail.com','4d07e388d264b1eeab113757b2491554',4,'doctor',2130706433,'active','2013-05-28 16:29:46','2013-06-28 23:25:15'),(26,'ladies@gmail.com','05bd4ce86483b4d2df4ee79e9ad6bb05',4,'doctor',2130706433,'active','2013-05-28 16:34:56','2013-05-28 16:34:56'),(27,'tes1t@test.com','73e512550302e76dfe791bd24f975f02',4,'doctor',2130706433,'active','2013-05-28 16:37:54','2013-05-28 16:37:54'),(28,'aa@test.com','ea2ae67d7ed0153d9cb769224d55eb31',4,'doctor',2130706433,'active','2013-05-28 17:17:24','2013-06-24 17:40:36'),(29,'a@a.com','',1,'patient',2130706433,'active','2013-05-30 10:49:59','2013-05-30 10:50:00'),(30,'aaaqa@gmail.com','1ff90edf3d7370660df93008e2f59cd7',4,'doctor',2130706433,'active','2013-06-03 15:00:12','2013-06-03 15:00:12'),(31,'dsasadia@gmail.com','417a776fe25ffed1e43c82d39f57c409',4,'doctor',2130706433,'active','2013-06-03 15:02:01','2013-06-24 17:37:15'),(32,'qwera@gmail.com','1f11f2d56207ef05f93d2d5691147b0b',4,'doctor',2130706433,'active','2013-06-03 15:03:56','2013-06-24 19:14:53'),(33,'aria@gmail.com','5d94159de25c785c97373f56890cf9fe',4,'doctor',2130706433,'active','2013-06-03 15:08:51','2013-06-03 15:49:56'),(34,'trtest.ephlux@gmail.com','',1,'patient',2130706433,'active','2013-06-04 12:58:47','2013-06-04 14:12:07'),(35,'losem@losem.com','69d0608b1b4ec997efce64a82cd3ade4',4,'doctor',2130706433,'active','2013-06-04 19:46:17','2013-06-24 17:35:41'),(36,'ephluxqa2@gmail.com','099c09482caa1ee8368a1b55ce87f813',4,'doctor',2130706433,'active','2013-06-07 21:18:48','2013-06-07 21:18:48'),(37,'aa@cc3.com','c8fe520881c3f1fd98c4fec9edf89ed5',4,'doctor',2130706433,'active','2013-07-02 15:21:11','2013-07-02 15:21:11'),(38,'aa@cc33.com','c7cd1ae5f9de76284340d8e58e2b1d03',4,'doctor',2130706433,'active','2013-07-02 15:24:01','2013-07-02 15:24:01'),(39,'aam@cc.com','59ed4b3f99499550763197007be4b466',4,'doctor',2130706433,'active','2013-07-02 15:26:10','2013-07-02 15:26:10'),(40,'aam3@cc.com','63b14816949cb7520b4d21995a414dde',4,'doctor',2130706433,'active','2013-07-02 15:28:05','2013-07-02 15:28:05'),(41,'aam4@cc.com','6514607f3e5e7199b0bac3150e380019',4,'doctor',2130706433,'active','2013-07-02 15:30:36','2013-07-02 15:30:36'),(42,'aam5@cc.com','60eeb422c8ba6700421f1c30d6c41d16',4,'doctor',2130706433,'active','2013-07-02 15:33:04','2013-07-02 15:33:04');

/*Table structure for table `vpd_activities` */

DROP TABLE IF EXISTS `vpd_activities`;

CREATE TABLE `vpd_activities` (
  `id` int(11) NOT NULL auto_increment,
  `employee_detail_id` bigint(20) NOT NULL,
  `provider_id` int(11) default NULL,
  `vpd_activity_type_id` int(11) default NULL,
  `modality_id` int(11) default NULL,
  `status` enum('pending','complete') default NULL,
  `sponsor_id` int(11) default NULL,
  `date_complition` datetime default NULL,
  `no_of_units` int(11) default NULL,
  `activity_name` varchar(50) default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_vpd_activities_ed_id` (`employee_detail_id`),
  KEY `FK_vpd_activities_pdr_id` (`provider_id`),
  KEY `FK_vpd_activities_vpd_at_id` (`vpd_activity_type_id`),
  KEY `FK_vpd_activities_mod_id` (`modality_id`),
  KEY `FK_vpd_activities_spn_id` (`sponsor_id`),
  CONSTRAINT `vpd_activities_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employee_details` (`id`),
  CONSTRAINT `vpd_activities_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`),
  CONSTRAINT `vpd_activities_ibfk_3` FOREIGN KEY (`vpd_activity_type_id`) REFERENCES `vpd_activity_types` (`id`),
  CONSTRAINT `vpd_activities_ibfk_4` FOREIGN KEY (`modality_id`) REFERENCES `modalities` (`id`),
  CONSTRAINT `vpd_activities_ibfk_5` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `vpd_activities` */

insert  into `vpd_activities`(`id`,`employee_detail_id`,`provider_id`,`vpd_activity_type_id`,`modality_id`,`status`,`sponsor_id`,`date_complition`,`no_of_units`,`activity_name`,`is_active`,`created`,`modified`) values (2,4,1,1,1,'pending',1,'2013-05-31 00:00:00',2,'testing activity a',1,'2013-05-29 14:16:30','2013-05-29 14:16:30');

/*Table structure for table `vpd_activity_types` */

DROP TABLE IF EXISTS `vpd_activity_types`;

CREATE TABLE `vpd_activity_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `ip_address` int(10) default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `vpd_activity_types` */

insert  into `vpd_activity_types`(`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'test vpd edit',2130706433,0,'2013-05-07 13:18:07','2013-05-07 13:23:48'),(2,'CBUs',2130706433,1,'2013-05-07 13:23:57','2013-05-10 21:52:22');

/*Table structure for table `waiting_lists_titles` */

DROP TABLE IF EXISTS `waiting_lists_titles`;

CREATE TABLE `waiting_lists_titles` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) default NULL,
  `division_id` int(11) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `FK_waiting_lists_titles_div_id` (`division_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `waiting_lists_titles` */

insert  into `waiting_lists_titles`(`id`,`title`,`description`,`division_id`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Acromegaly','Acromegaly is a hormonal disorder that develops when your pituitary gland produces too much growth h',4,2130706433,1,'2013-05-14 12:33:30','2013-05-14 13:54:38'),(2,'Acne','this is other waiting list',4,2130706433,1,'2013-05-14 13:55:00','2013-05-14 13:55:14'),(3,'Acute sinusitis','Acute sinusitis (acute rhinosinusitis) causes the cavities around your nasal passages (sinuses) to b',10,2130706433,1,'2013-05-15 11:01:07','2013-05-15 11:01:07'),(4,'Back pain','Back pain is a common complaint. Most people in the United States will experience low back pain at l',14,2130706433,1,'2013-05-15 11:01:16','2013-05-15 11:01:16'),(5,'Delirium','Delirium is a serious disturbance in a person\'s mental abilities that results in a decreased awarene',9,2130706433,1,'2013-05-15 11:01:40','2013-05-15 11:01:40'),(6,'Anxiety','Anxiety happens as a normal part of life. It can even be useful when it alerts you to danger. But fo',8,2130706433,1,'2013-05-15 11:01:59','2013-05-15 11:01:59'),(7,'Dehydration','Dehydration occurs when you lose more fluid than you take in, and your body doesn\'t have enough wate',4,2130706433,1,'2013-05-15 11:02:18','2013-05-15 11:02:18'),(8,'Obesity','Obesity is defined as having an excessive amount of body fat. Obesity is more than just a cosmetic c',8,2130706433,1,'2013-05-15 11:02:55','2013-05-15 11:02:55'),(9,'Obsessive-compulsive disorder (OCD)','bsessive-compulsive disorder (OCD) is an anxiety disorder characterized by unreasonable thoughts and',8,2130706433,1,'2013-05-15 11:03:05','2013-05-15 11:03:05'),(11,'Dementia','Dementia isn\'t a specific disease. Instead, dementia describes a group of symptoms affecting thinkin',0,2130706433,1,'2013-05-21 08:33:22','2013-05-21 09:08:46'),(14,'new list','',0,2130706433,0,'2013-06-04 11:21:07','2013-06-04 11:21:07');

/* Function  structure for function  `get_total_appointments` */

/*!50003 DROP FUNCTION IF EXISTS `get_total_appointments` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` FUNCTION `get_total_appointments`(employee_detail_id BIGINT, division_id INT) RETURNS int(11)
BEGIN
	DECLARE total_appointments INT;
	SET total_appointments = (
		SELECT count(Appointment.id)
		FROM appointments AS Appointment 
		WHERE (
				Appointment.employee_detail_id,Appointment.division_id
			) IN (
				(employee_detail_id , division_id)
			) AND Appointment.appointment_date BETWEEN NOW() AND DATE_ADD(NOW(),INTERVAL 4 WEEK) 
	); 
RETURN total_appointments ;
END */$$
DELIMITER ;

/* Function  structure for function  `strSplit` */

/*!50003 DROP FUNCTION IF EXISTS `strSplit` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` FUNCTION `strSplit`(x varchar(255), delim varchar(12), pos int) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
   RETURN replace(substring(substring_index(x, delim, pos), 
      length(substring_index(x, delim, pos - 1)) + 1), delim, '');
	
-- end the stored function code block
END */$$
DELIMITER ;

/* Procedure structure for procedure `common_insert_multi_params` */

/*!50003 DROP PROCEDURE IF EXISTS  `common_insert_multi_params` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `common_insert_multi_params`(
   IN tblname VARCHAR(250),
   IN in_string_array VARCHAR(2000),
   IN in_array_count INT,
   OUT `status` INT
)
BEGIN
	DECLARE tmp_key_value_pair VARCHAR(255);
	DECLARE tmp_key VARCHAR(255);
	DECLARE tmp_value TEXT;
	DECLARE counter INT DEFAULT 0;
	
	set @q = concat("insert into ",tblname," set ");
	simple_loop: LOOP
		SET counter = counter + 1;
		SELECT strSplit(in_string_array, '|', counter) INTO tmp_key_value_pair;
		SELECT strSplit(tmp_key_value_pair, '=', 1) INTO tmp_key;
		SELECT strSplit(tmp_key_value_pair, '=', 2) INTO tmp_value;
		
		IF counter = in_array_count THEN
			set @q = concat(@q,tmp_key,"='",tmp_value,"'");
		      -- break out since we have done parsing
		      -- the 2 elements in our example
		       LEAVE simple_loop;
		else
			set @q = concat(@q,tmp_key,"='",tmp_value,"',");
		END IF;
		
	END LOOP simple_loop;
        SELECT @q;
	#PREPARE stmt FROM @q;
	#EXECUTE stmt;
	#DEALLOCATE PREPARE stmt;
	#SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_allowed_services` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_allowed_services` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_allowed_services`(in name varchar(100), in description text,in allowed_service_group_id int,in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @name = name;
set @description = description;
set @allowed_service_group_id = allowed_service_group_id;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into allowed_services(name,description,allowed_service_group_id,ip_address,created,modified) values (?,?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name,@description,@allowed_service_group_id, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_annual_vacation_plan` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_annual_vacation_plan` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_annual_vacation_plan`(
	IN employee_detail_id BIGINT(20),
	IN avp_type_id INT(11),
	IN description VARCHAR(100) ,
	IN last_work_day date,
	IN report_to_work date,	
	IN created DATETIME,	
	OUT `status` INT
)
BEGIN
	insert into `jish`.`annual_vacation_plans` 
	(
	annual_vacation_plans.employee_detail_id,
	annual_vacation_plans.avp_type_id,
	annual_vacation_plans.description,
	annual_vacation_plans.last_work_day,
	annual_vacation_plans.annual_vacation_plans.report_to_work,	
	annual_vacation_plans.created,
	annual_vacation_plans.modified
	)
	values
	(
	employee_detail_id, 
	avp_type_id, 
	description, 
	last_work_day, 
	report_to_work, 	
	created, 
	created
	);
	SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_common` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_common` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_common`(in tbl_name varchar(100), in name VARCHAR(100),in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @name = name;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into ",tbl_name,"(name,ip_address,created,modified) values (?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_common_with_col_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_common_with_col_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_common_with_col_name`(in tbl_name varchar(100),in col_name varchar(100), in name VARCHAR(100),in ip_address int, in created datetime,IN description VARCHAR(50),OUT `status` INT)
BEGIN
set @name = name;
set @description = description;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into ",tbl_name,"(name,",col_name,",ip_address,created,modified) values (?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @name,@description,@ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_feature` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_feature` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_feature`(IN description VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into features(
	features.description,
	features.ip_address,
	features.created,
	features.modified
) values (
	description,
	ip_address,
	created,
	created 
);
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_group`(IN name VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
insert into groups(
	groups.name,
	groups.ip_address,
	groups.created,
	groups.modified
) values (
	name,
	ip_address,
	created,
	created 
);
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_user` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_user` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `create_user`(IN `first_name` VARCHAR(255), IN `last_name` VARCHAR(255), IN `username` VARCHAR(50), IN `password` VARCHAR(255), IN `group_id` INT(11), IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN
INSERT INTO users
(
	users.first_name                  , 
	users.last_name                   , 
	users.username                    , 
	users.password                    ,
	users.group_id                    ,
	users.ip_address                  ,
	users.created	             ,
	users.modified
)
VALUES 
( 
	first_name                       , 
	last_name                        , 
	username                         ,  
	password                         ,
	group_id                         ,
	ip_address                       ,
	created                          ,
	created
) ; 
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_waiting_list_title` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_waiting_list_title` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`192.175.0.%` PROCEDURE `create_waiting_list_title`(in title varchar(100), in description text,in division_id int,in ip_address int, in created datetime,OUT `status` INT)
BEGIN
set @title = title;
set @description = description;
set @division_id = division_id;
set @ip_address = ip_address;
set @created = created;
set @modified = created;
set @q = concat("insert into waiting_lists_titles(title,description,division_id,ip_address,created,modified) values (?,?,?,?,?,?)");
PREPARE stmt FROM @q;
EXECUTE stmt USING @title,@description,@division_id, @ip_address, @created,@modified ;
DEALLOCATE PREPARE stmt;
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_arosacos_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_arosacos_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_arosacos_by_group_id`(IN group_id INT(10))
BEGIN
SELECT 	ArosAco.id,ArosAco.aco_id
FROM groups AS `Group`
LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
LEFT JOIN `aros_acos` AS ArosAco ON ArosAco.aro_id = Aro.id
WHERE Group.id = group_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_common` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_common` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_common`(IN tbl_name VARCHAR(25),IN model_name VARCHAR(25))
BEGIN	
	set @q = concat("SELECT " , model_name , ".id, ", model_name , ".name"," FROM " , tbl_name , " AS " , model_name , " WHERE " , model_name , ".is_active = 1");
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	DEALLOCATE PREPARE stmt;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_emp_working_hours_by_emp_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_emp_working_hours_by_emp_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_emp_working_hours_by_emp_id`(IN employee_detail_id BIGINT(20))
BEGIN
	SELECT
		EmployeeWorkingHour.id, EmployeeWorkingHour.start_time, EmployeeWorkingHour.end_time
	FROM employee_working_hours AS EmployeeWorkingHour
	WHERE EmployeeWorkingHour.is_active = 1 AND EmployeeWorkingHour.employee_detail_id = employee_detail_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_patients` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_patients` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_patients`(IN keyword VARCHAR(50))
BEGIN
	IF (ISNULL(keyword)) THEN
		SELECT PatientDetail.id,PatientDetail.first_name
		FROM users AS User
		LEFT JOIN patient_details AS PatientDetail
			ON PatientDetail.user_id = User.id 
		WHERE User.status = 'active' AND PatientDetail.status = 'active' AND User.user_type = 'patient' ;
	ELSE
		SELECT PatientDetail.id,PatientDetail.first_name
		FROM users AS User
		LEFT JOIN patient_details AS PatientDetail
			ON PatientDetail.user_id = User.id 
		WHERE User.status = 'active' AND PatientDetail.status = 'active' AND User.user_type = 'patient' AND 
			(
				PatientDetail.first_name LIKE CONCAT('%' , keyword , '%' ) OR 
				PatientDetail.last_name LIKE CONCAT('%' , keyword , '%' ) 
			);
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_permissions` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_permissions` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_permissions`()
BEGIN
	SELECT 
		Child.id,Child.alias,
		AuthActionMap.description,AuthActionMap.id,
		Feature.id,Feature.description,
		Parent.id,Parent.alias
	FROM `acos` AS Child
	LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.aco_id = Child.id
	LEFT JOIN features AS Feature ON Feature.id = AuthActionMap.feature_id AND Feature.is_active = 1
	WHERE `Parent`.`parent_id` IS NOT NULL;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_all_waiting_list_titles` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_all_waiting_list_titles` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_all_waiting_list_titles`()
BEGIN
	SELECT 
		WaitingListsTitle.id, WaitingListsTitle.title AS name
	FROM waiting_lists_titles AS WaitingListsTitle
	WHERE WaitingListsTitle.is_active = 1
	ORDER BY WaitingListsTitle.title ASC;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_annual_vacation_plan_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_annual_vacation_plan_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_annual_vacation_plan_by_id`(IN annual_vacation_plan_id BIGINT(20))
BEGIN
	SELECT 
	`AnnualVacationPlan`.`id`, `AnnualVacationPlan`.`employee_detail_id`, `AnnualVacationPlan`.`avp_type_id`,
	`AnnualVacationPlan`.`description`, `AnnualVacationPlan`.`last_work_day`, `AnnualVacationPlan`.`report_to_work`, 
	`AnnualVacationPlan`.`status`, 
	`EmployeeDetail`.`id`, `EmployeeDetail`.`created`,
	
	`AvpType`.`id`, `AvpType`.`name`, `AvpType`.`created`
	FROM `jish`.`annual_vacation_plans` AS `AnnualVacationPlan` 
	
	LEFT JOIN `jish`.`employee_details` AS `EmployeeDetail` ON (
		`AnnualVacationPlan`.`employee_detail_id` = `EmployeeDetail`.`id` AND `EmployeeDetail`.`status` = 'active' )
	
	LEFT JOIN `users` AS `User` ON (`User`.`id` = `EmployeeDetail`.`user_id` AND `User`.`status` = 'active' )
	
	LEFT JOIN `jish`.`avp_types` AS `AvpType` ON (`AnnualVacationPlan`.`avp_type_id` = `AvpType`.`id` AND `AvpType`.`is_active` = 1)  
	
	WHERE `AnnualVacationPlan`.`id` = annual_vacation_plan_id AND User.id > 0 AND EmployeeDetail.id > 0 AND `AnnualVacationPlan`.`is_active` = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_available_doctors_lists_for_given_weeks_and_wlist` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_available_doctors_lists_for_given_weeks_and_wlist` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_available_doctors_lists_for_given_weeks_and_wlist`(IN waiting_list_id INT(11), IN no_of_weeks INT )
BEGIN
	DECLARE division_id INT ;
	DECLARE is_valid_wlist INT ;
	SELECT WaitingListsTitle.id, WaitingListsTitle.division_id INTO is_valid_wlist,division_id
	FROM waiting_lists_titles AS WaitingListsTitle
	WHERE WaitingListsTitle.is_active = 1 AND WaitingListsTitle.id = waiting_list_id; 
	
	IF( is_valid_wlist > 0) THEN
		IF( division_id > 0) THEN 
			SELECT AvailableDoctor.EmployeeId , AvailableDoctor.EmployeeFName, AvailableDoctor.UserId
			FROM (
			SELECT 	
				EmployeeDetail.user_id AS UserId,EmployeeDetail.id AS EmployeeId,EmployeeDetail.first_name AS EmployeeFName, (EmployeeWorkingPerWeekSlot.total_possible_slots * 4) AS WeeksTotalSlots , 
				get_total_appointments(EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id) AS TotalAppointments	,
				EmployeeWorkingPerWeekSlot.division_id
			FROM employee_details AS EmployeeDetail	
			LEFT JOIN employee_working_per_week_slots AS EmployeeWorkingPerWeekSlot 
				ON EmployeeWorkingPerWeekSlot.employee_detail_id = EmployeeDetail.id 
			LEFT JOIN waiting_lists_titles AS WaitingListsTitle
					ON WaitingListsTitle.is_active = 1 AND
						EmployeeWorkingPerWeekSlot.division_id = WaitingListsTitle.division_id			
			WHERE EmployeeDetail.status = 'active'  AND 
					EmployeeWorkingPerWeekSlot.is_active = 1 AND
					WaitingListsTitle.id = waiting_list_id	
			GROUP BY EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id
			) AS AvailableDoctor
			WHERE AvailableDoctor.WeeksTotalSlots > AvailableDoctor.TotalAppointments 
			GROUP BY AvailableDoctor.EmployeeId;
		ELSE
			SELECT AvailableDoctor.EmployeeId , AvailableDoctor.EmployeeFName, AvailableDoctor.UserId
			FROM (
			SELECT 	
				EmployeeDetail.user_id AS UserId,EmployeeDetail.id AS EmployeeId,EmployeeDetail.first_name AS EmployeeFName, (EmployeeWorkingPerWeekSlot.total_possible_slots * 4) AS WeeksTotalSlots , 
				get_total_appointments(EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id) AS TotalAppointments	,
				EmployeeWorkingPerWeekSlot.division_id
			FROM employee_details AS EmployeeDetail	
			LEFT JOIN employee_working_per_week_slots AS EmployeeWorkingPerWeekSlot 
				ON EmployeeWorkingPerWeekSlot.employee_detail_id = EmployeeDetail.id 					
			WHERE EmployeeDetail.status = 'active'  AND 
					EmployeeWorkingPerWeekSlot.is_active = 1 
			GROUP BY EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id
			) AS AvailableDoctor
			WHERE AvailableDoctor.WeeksTotalSlots > AvailableDoctor.TotalAppointments
			GROUP BY AvailableDoctor.EmployeeId;
		END IF;
	ELSE 
		SELECT AvailableDoctor.EmployeeId , AvailableDoctor.EmployeeFName, AvailableDoctor.UserId
		FROM (
		SELECT 	
			EmployeeDetail.user_id AS UserId,EmployeeDetail.id AS EmployeeId,EmployeeDetail.first_name AS EmployeeFName, (EmployeeWorkingPerWeekSlot.total_possible_slots * 4) AS WeeksTotalSlots , 
			get_total_appointments(EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id) AS TotalAppointments	,
			EmployeeWorkingPerWeekSlot.division_id
		FROM employee_details AS EmployeeDetail	
		LEFT JOIN employee_working_per_week_slots AS EmployeeWorkingPerWeekSlot 
			ON EmployeeWorkingPerWeekSlot.employee_detail_id = EmployeeDetail.id 					
		WHERE EmployeeDetail.status = 'active'  AND 
				EmployeeWorkingPerWeekSlot.is_active = 1 
		GROUP BY EmployeeDetail.id , EmployeeWorkingPerWeekSlot.division_id
		) AS AvailableDoctor
		WHERE AvailableDoctor.WeeksTotalSlots > AvailableDoctor.TotalAppointments
		GROUP BY AvailableDoctor.EmployeeId;
	END IF;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_available_working_slots` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_available_working_slots` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_available_working_slots`(
	IN waiting_list_id INT(11), 
	IN employee_detail_id BIGINT(20),
	IN date_as_appointment DATE,
	IN working_day VARCHAR(20)
)
BEGIN
	DECLARE division_id INT ;
	DECLARE is_valid_wlist INT ;
	DECLARE appointment_id INT ;
	SELECT WaitingListsTitle.id, WaitingListsTitle.division_id INTO is_valid_wlist,division_id
	FROM waiting_lists_titles AS WaitingListsTitle
	WHERE WaitingListsTitle.is_active = 1 AND WaitingListsTitle.id = waiting_list_id; 
	
	SELECT Appointment.id INTO appointment_id
			FROM appointments AS Appointment
			WHERE Appointment.is_active = 1 AND
				DATE(Appointment.appointment_date) = date_as_appointment AND 	
				Appointment.employee_detail_id = employee_detail_id AND 
				Appointment.division_id = division_id ;
	IF( is_valid_wlist > 0) THEN
		IF( division_id > 0) THEN 			
			IF(appointment_id > 0) THEN 
				SELECT EmployeeWorkingHourBreakup.*,Division.name,Division.id
				FROM employee_working_hours AS EmployeeWorkingHour
				LEFT JOIN employee_working_hour_breakups AS EmployeeWorkingHourBreakup 
					ON EmployeeWorkingHourBreakup.employee_working_hour_id = EmployeeWorkingHour.id AND
						EmployeeWorkingHourBreakup.is_active = 1
				LEFT JOIN divisions AS Division 
					ON Division.id = EmployeeWorkingHour.division_id AND Division.is_active = 1
				LEFT JOIN appointments AS Appointment
					ON Appointment.employee_detail_id = EmployeeWorkingHour.employee_detail_id AND 
						Appointment.division_id = EmployeeWorkingHour.division_id  AND
						EmployeeWorkingHourBreakup.is_active = 1
				WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND 
						EmployeeWorkingHour.division_id = division_id AND
						EmployeeWorkingHour.working_day = working_day AND
						DATE(Appointment.appointment_date) = date_as_appointment AND 	
						NOT Appointment.employee_working_hour_breakup_id = EmployeeWorkingHourBreakup.id ;
			ELSE 
				SELECT EmployeeWorkingHourBreakup.*,Division.name,Division.id
				FROM employee_working_hours AS EmployeeWorkingHour
				LEFT JOIN employee_working_hour_breakups AS EmployeeWorkingHourBreakup 
					ON EmployeeWorkingHourBreakup.employee_working_hour_id = EmployeeWorkingHour.id AND
						EmployeeWorkingHourBreakup.is_active = 1
				LEFT JOIN divisions AS Division 
					ON Division.id = EmployeeWorkingHour.division_id AND Division.is_active = 1				
				WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND 
						EmployeeWorkingHour.division_id = division_id AND
						EmployeeWorkingHour.working_day = working_day;						
			END IF;			
		ELSE
			IF(appointment_id > 0) THEN 
				SELECT EmployeeWorkingHourBreakup.*,Division.name,Division.id
				FROM employee_working_hours AS EmployeeWorkingHour
				LEFT JOIN employee_working_hour_breakups AS EmployeeWorkingHourBreakup 
					ON EmployeeWorkingHourBreakup.employee_working_hour_id = EmployeeWorkingHour.id AND
						EmployeeWorkingHourBreakup.is_active = 1
				LEFT JOIN divisions AS Division 
					ON Division.id = EmployeeWorkingHour.division_id AND Division.is_active = 1
				LEFT JOIN appointments AS Appointment
					ON Appointment.employee_detail_id = EmployeeWorkingHour.employee_detail_id AND 
						Appointment.division_id = EmployeeWorkingHour.division_id  AND
						EmployeeWorkingHourBreakup.is_active = 1
				WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND 						
						EmployeeWorkingHour.working_day = working_day AND
						DATE(Appointment.appointment_date) = date_as_appointment AND 	
						NOT Appointment.employee_working_hour_breakup_id = EmployeeWorkingHourBreakup.id ;
			ELSE 
				SELECT EmployeeWorkingHourBreakup.*,Division.name,Division.id
				FROM employee_working_hours AS EmployeeWorkingHour
				LEFT JOIN employee_working_hour_breakups AS EmployeeWorkingHourBreakup 
					ON EmployeeWorkingHourBreakup.employee_working_hour_id = EmployeeWorkingHour.id AND
						EmployeeWorkingHourBreakup.is_active = 1
				LEFT JOIN divisions AS Division 
					ON Division.id = EmployeeWorkingHour.division_id AND Division.is_active = 1				
				WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND 						
						EmployeeWorkingHour.working_day = working_day;						
			END IF;	
		END IF;
	ELSE 
		SELECT EmployeeWorkingHourBreakup.*,Division.name,Division.id
		FROM employee_working_hours AS EmployeeWorkingHour
		LEFT JOIN employee_working_hour_breakups AS EmployeeWorkingHourBreakup 
			ON EmployeeWorkingHourBreakup.employee_working_hour_id = EmployeeWorkingHour.id AND
				EmployeeWorkingHourBreakup.is_active = 1
		LEFT JOIN divisions AS Division 
			ON Division.id = EmployeeWorkingHour.division_id AND Division.is_active = 1				
		WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND 						
				EmployeeWorkingHour.working_day = working_day; 
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_common_by_common_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_common_by_common_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_common_by_common_id`(IN common_id INT(11),IN tbl_name VARCHAR(25),IN model_name VARCHAR(25))
BEGIN
	set @common_id = common_id;
	set @q = concat("SELECT " , model_name , ".* FROM " , tbl_name , " AS " , model_name , " WHERE " , model_name , ".id =?");
	PREPARE stmt FROM @q;
	EXECUTE stmt USING @common_id ;
	DEALLOCATE PREPARE stmt;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_config_option_by_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_config_option_by_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_config_option_by_name`(IN NAME VARCHAR(50))
BEGIN
	SELECT 	ConfigOption.id, ConfigOption.name, ConfigOption.value, ConfigOption.created
	FROM config_options AS ConfigOption
	WHERE ConfigOption.name = `name` AND ConfigOption.is_active = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_division_id_by_waiting_list_title_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_division_id_by_waiting_list_title_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_division_id_by_waiting_list_title_id`(IN waiting_list_title_id INT(11))
BEGIN
	select WaitingListsTitle.division_id
	from waiting_lists_titles AS WaitingListsTitle
	WHERE WaitingListsTitle.is_active = 1 AND WaitingListsTitle.id = waiting_list_title_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_doctor_appointments_count_between_intervals` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_doctor_appointments_count_between_intervals` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_doctor_appointments_count_between_intervals`(IN start_interval DATETIME , IN end_interval DATETIME, IN employee_detail_id BIGINT(20))
BEGIN
	select 	count(Appointment.id) AS AppointmentCount
	from appointments AS Appointment
	WHERE Appointment.appointment_date BETWEEN start_interval AND end_interval AND Appointment.employee_detail_id = employee_detail_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_doctor_avp_and_appointments_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_doctor_avp_and_appointments_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_doctor_avp_and_appointments_by_user_id`(IN `user_id` BIGINT(20))
    NO SQL
BEGIN 
	SELECT 		
	EmployeeDetail.id,
	User.id,
	Division.name,Division.id,Division.created,
	AvpType.name,AvpType.id,AvpType.created,		
	AnnualVacationPlan.*,
	Appointment.*
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail 
		ON EmployeeDetail.user_id = User.id 
		AND EmployeeDetail.status = 'active'	
	LEFT JOIN employee_divisions AS EmployeeDivision 
		ON EmployeeDivision.employee_detail_id = EmployeeDetail.id 
		AND EmployeeDivision.is_active = 1
	LEFT JOIN divisions AS Division 
		ON Division.id = EmployeeDivision.division_id 
		AND Division.is_active = 1
	LEFT JOIN annual_vacation_plans AS AnnualVacationPlan 
		ON AnnualVacationPlan.employee_detail_id = EmployeeDetail.id 
		AND AnnualVacationPlan.is_active = 1
		AND AnnualVacationPlan.status = 'approved'
	LEFT JOIN avp_types AS AvpType 
		ON AnnualVacationPlan.avp_type_id = AvpType.id 
		AND AvpType.is_active = 1
	LEFT JOIN appointments AS Appointment 
		ON EmployeeDetail.id = Appointment.employee_detail_id 
		AND Appointment.is_active = 1
	WHERE `User`.id = user_id 
		AND `User`.`status` = 'active' 
		AND User.user_type = 'doctor' 
		AND (Appointment.id > 0 OR AnnualVacationPlan.id > 0);
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_doctor_details_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_doctor_details_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_doctor_details_by_user_id`(IN `user_id` BIGINT(20))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,		
		EmployeeDetail.*,
		Language.name,Language.id,Language.created,
		AllowedService.name,AllowedService.id,AllowedService.created,
		CaseLoad.name,CaseLoad.id,CaseLoad.created,
		Division.name,Division.id,Division.created,		
		Level.name,Level.id,Level.created,
		OfficialTitle.name,OfficialTitle.id,OfficialTitle.created,
		LicenseType.name,LicenseType.id,LicenseType.created,LicenseType.is_active
		
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'	
	LEFT JOIN license_types AS LicenseType ON LicenseType.id = EmployeeDetail.license_type_id AND LicenseType.is_active = 1
	LEFT JOIN user_languages AS `UserLanguage` ON UserLanguage.user_id = User.id AND UserLanguage.is_active = 1
	LEFT JOIN languages AS `Language` ON Language.id = UserLanguage.language_id AND Language.is_active = 1
	LEFT JOIN employee_allowed_services AS EmployeeAllowedService ON EmployeeAllowedService.employee_detail_id = EmployeeDetail.id AND EmployeeAllowedService.is_active = 1
	LEFT JOIN allowed_services AS AllowedService ON AllowedService.id = EmployeeAllowedService.allowed_service_id AND AllowedService.is_active = 1
	LEFT JOIN employee_case_loads AS EmployeeCaseLoad ON EmployeeCaseLoad.employee_detail_id = EmployeeDetail.id AND EmployeeCaseLoad.is_active = 1
	LEFT JOIN case_loads AS CaseLoad ON CaseLoad.id = EmployeeCaseLoad.case_load_id AND CaseLoad.is_active = 1
	LEFT JOIN employee_divisions AS EmployeeDivision ON EmployeeDivision.employee_detail_id = EmployeeDetail.id AND EmployeeDivision.is_active = 1
	LEFT JOIN divisions AS Division ON Division.id = EmployeeDivision.division_id AND Division.is_active = 1
	
	LEFT JOIN employee_levels AS EmployeeLevel ON EmployeeLevel.employee_detail_id = EmployeeDetail.id AND EmployeeLevel.is_active = 1
	LEFT JOIN levels AS Level ON Level.id = EmployeeLevel.level_id AND Level.is_active = 1
	LEFT JOIN employee_official_titles AS EmployeeOfficialTitle ON EmployeeOfficialTitle.employee_detail_id = EmployeeDetail.id AND EmployeeOfficialTitle.is_active = 1
	LEFT JOIN official_titles AS OfficialTitle ON OfficialTitle.id = EmployeeOfficialTitle.official_title_id AND OfficialTitle.is_active = 1
	
	WHERE `User`.id = user_id AND `User`.`status` = 'active' AND User.user_type = 'doctor';
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_doctor_leaves_count_between_intervals` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_doctor_leaves_count_between_intervals` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_doctor_leaves_count_between_intervals`(IN start_interval DATETIME , IN end_interval DATETIME, IN employee_detail_id BIGINT(20))
BEGIN
	select 	count(AnnualVacationPlan.id) AS LeavesCount
	from annual_vacation_plans AS AnnualVacationPlan
	WHERE (
		(AnnualVacationPlan.last_work_day BETWEEN start_interval AND end_interval) OR 
		(AnnualVacationPlan.report_to_work BETWEEN start_interval AND end_interval) 
	      ) AND AnnualVacationPlan.employee_detail_id = employee_detail_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_doctor_working_hours_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_doctor_working_hours_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_doctor_working_hours_by_user_id`(IN `user_id` BIGINT(20))
    NO SQL
BEGIN 
	SELECT 		
		EmployeeDetail.id,
		Division.name,Division.id,Division.created,		
		EmployeeWorkingHour.id,EmployeeWorkingHour.working_day,EmployeeWorkingHour.start_time,EmployeeWorkingHour.end_time,EmployeeWorkingHour.total_possible_slots,EmployeeWorkingHour.created
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'	
	LEFT JOIN employee_divisions AS EmployeeDivision ON EmployeeDivision.employee_detail_id = EmployeeDetail.id AND EmployeeDivision.is_active = 1
	LEFT JOIN divisions AS Division ON Division.id = EmployeeDivision.division_id AND Division.is_active = 1
	LEFT JOIN employee_working_hours AS EmployeeWorkingHour ON EmployeeWorkingHour.employee_detail_id = EmployeeDetail.id AND EmployeeWorkingHour.is_active = 1 AND EmployeeWorkingHour.division_id = Division.id	
	WHERE `User`.id = user_id AND `User`.`status` = 'active' AND User.user_type = 'doctor' AND EmployeeWorkingHour.id > 0 AND Division.id > 0;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_employee_details_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_employee_details_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_employee_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		
		EmployeeDetail.*,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id AND EmployeeDetail.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_feature_by_feature_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_feature_by_feature_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_feature_by_feature_id`(IN feature_id INT(11))
BEGIN
select 	Feature.id, Feature.description, Feature.created
from `jish`.`features` AS Feature
WHERE Feature.id = feature_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_feature_details_by_feature_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_feature_details_by_feature_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_feature_details_by_feature_id`(IN feature_id INT(11))
BEGIN
	SELECT 	
		Feature.id,Feature.description,Feature.created,
		AuthActionMap.id,AuthActionMap.description,AuthActionMap.created
	FROM features AS Feature
	LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.feature_id = Feature.id
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_group_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_group_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_group_by_group_id`(IN group_id INT(11))
BEGIN
	select 	Group.id, Group.name, Group.created
	from `jish`.`groups` AS `Group`
	WHERE Group.id = group_id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_group_details_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_group_details_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_group_details_by_group_id`(IN group_id INT(11))
BEGIN
	SELECT 	
		Group.id,Group.name,Group.created,
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		EmployeeDetail.first_name,EmployeeDetail.last_name,EmployeeDetail.created,
		PatientDetail.first_name,PatientDetail.last_name,PatientDetail.created
	FROM groups AS `Group`
	LEFT JOIN users AS `User` ON User.group_id = Group.id AND User.status = 'active'
	LEFT JOIN employee_details AS EmployeeDetail ON EmployeeDetail.user_id = User.id
	LEFT JOIN patient_details AS PatientDetail ON PatientDetail.user_id = User.id 
	WHERE Group.id = group_id AND Group.is_active = 1 AND User.id > 0;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_patient_attachment_reports` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_patient_attachment_reports` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_patient_attachment_reports`(in appointment_id int, in patient_id int, in off int, in lim int)
BEGIN
set @appointment_id = appointment_id;
set @patient_id = patient_id;
set @off = off;
set @lim = lim;
set @q = concat("SELECT * FROM `patient_attachment_reports` AS `PatientAttachmentReport` WHERE `PatientAttachmentReport`.`appointment_id` = ",@appointment_id,"   ORDER BY `PatientAttachmentReport`.`id` desc  LIMIT ?,?");
PREPARE stmt FROM @q;
EXECUTE stmt USING @off,@lim ;
DEALLOCATE PREPARE stmt;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_patient_by_waiting_list_title_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_patient_by_waiting_list_title_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_patient_by_waiting_list_title_id`(IN waiting_list_title_id INT(11))
BEGIN
	SELECT 	
		PatientDetail.id,PatientDetail.first_name AS name
	FROM patient_details AS PatientDetail
	LEFT JOIN patient_waiting_lists AS PatientWaitingList ON PatientWaitingList.patient_detail_id = PatientDetail.id AND PatientWaitingList.is_active = 1
	LEFT JOIN waiting_lists_titles AS WaitingListsTitle ON WaitingListsTitle.id = PatientWaitingList.waiting_list_title_id AND WaitingListsTitle.is_active = 1
	WHERE PatientDetail.status = 'active' AND WaitingListsTitle.id = waiting_list_title_id ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_patient_details_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_patient_details_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_patient_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,User.user_type,
		
		PatientDetail.*,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN patient_details AS PatientDetail ON PatientDetail.user_id = User.id AND PatientDetail.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_per_week_slots` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_per_week_slots` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_per_week_slots`(IN employee_detail_id BIGINT(20) , IN division_id INT(11))
BEGIN		
	SELECT SUM(EmployeeWorkingHour.total_possible_slots) AS TotalPossibleSlotsPerWeek
	FROM employee_working_hours AS EmployeeWorkingHour
	WHERE EmployeeWorkingHour.employee_detail_id = employee_detail_id AND EmployeeWorkingHour.division_id = division_id AND EmployeeWorkingHour.is_active = 1;
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_by_email` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_by_email` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_by_email`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status
FROM users AS `User` 
WHERE User.username = email AND User.status = 'active'; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_details_by_user_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_details_by_user_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 
	SELECT 
		User.id,User.username,User.group_id,User.created,User.status,
		
		UserSocialAccount.id, UserSocialAccount.link_id, UserSocialAccount.email_address, UserSocialAccount.image_url, 
		UserSocialAccount.type_id,UserSocialAccount.is_valid_token, UserSocialAccount.screen_name, UserSocialAccount.status, 
		UserSocialAccount.created,
		Group.id, Group.name, Group.created
	FROM users AS `User`
	LEFT JOIN user_social_accounts AS UserSocialAccount ON UserSocialAccount.user_id = `User`.id AND UserSocialAccount.status = 'active'
	LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1
	WHERE `User`.id = user_id AND `User`.`status` = 'active';
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_user_tmp_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_user_tmp_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_user_tmp_data`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.*
FROM tmps AS Tmp 
WHERE Tmp.target_id = target_id AND Tmp.type = type; 
END */$$
DELIMITER ;

/* Procedure structure for procedure `is_doctor_consult_division_for_given_working_day` */

/*!50003 DROP PROCEDURE IF EXISTS  `is_doctor_consult_division_for_given_working_day` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `is_doctor_consult_division_for_given_working_day`(
	IN employee_detail_id BIGINT(20),
	IN wlist_title_id INT,
	IN working_day VARCHAR(20)
)
BEGIN	
	DECLARE is_valid_wlist INT ;
	SELECT WaitingListsTitle.id INTO is_valid_wlist
	FROM waiting_lists_titles AS WaitingListsTitle
	WHERE WaitingListsTitle.is_active = 1 AND WaitingListsTitle.id = wlist_title_id; 
	
	IF( is_valid_wlist > 0) THEN
		SELECT EmployeeWorkingHour.id
		FROM employee_working_hours AS EmployeeWorkingHour
		LEFT JOIN waiting_lists_titles AS WaitingListsTitle
			ON WaitingListsTitle.division_id = EmployeeWorkingHour.division_id 
			AND WaitingListsTitle.is_active = 1
		WHERE EmployeeWorkingHour.is_active = 1 
			AND EmployeeWorkingHour.employee_detail_id = employee_detail_id
			AND WaitingListsTitle.id = wlist_title_id
			AND EmployeeWorkingHour.working_day = working_day ;
	ELSE  -- this is just to return true since division is not concerned
		SELECT EmployeeWorkingHour.id
		FROM employee_working_hours AS EmployeeWorkingHour		
		WHERE EmployeeWorkingHour.is_active = 1
		LIMIT 1 ;
	END IF;
END */$$
DELIMITER ;

/* Procedure structure for procedure `is_doctor_on_leave` */

/*!50003 DROP PROCEDURE IF EXISTS  `is_doctor_on_leave` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `is_doctor_on_leave`(
		IN employee_detail_id BIGINT(20),
		IN date_as_appointment DATE
		)
BEGIN
	SELECT AnnualVacationPlan.id
	FROM annual_vacation_plans AS AnnualVacationPlan
	WHERE AnnualVacationPlan.is_active = 1 
	AND AnnualVacationPlan.status = 'approved'
	AND AnnualVacationPlan.employee_detail_id = employee_detail_id
	AND date_as_appointment BETWEEN AnnualVacationPlan.last_work_day AND AnnualVacationPlan.report_to_work;
END */$$
DELIMITER ;

/* Procedure structure for procedure `is_employee_exists` */

/*!50003 DROP PROCEDURE IF EXISTS  `is_employee_exists` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `is_employee_exists`(IN employee_detail_id BIGINT(20))
BEGIN	
	SELECT count(User.id) AS IsExists 
	FROM users AS `User`
	LEFT JOIN employee_details AS EmployeeDetail ON User.id = EmployeeDetail.user_id AND EmployeeDetail.status = 'active'
	WHERE EmployeeDetail.id = employee_detail_id AND User.status = 'active';
END */$$
DELIMITER ;

/* Procedure structure for procedure `is_unverified_email_exists` */

/*!50003 DROP PROCEDURE IF EXISTS  `is_unverified_email_exists` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.target_id
FROM tmps AS Tmp
WHERE Tmp.target_id = email AND Tmp.type = 'registration';
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_all_common_by_field_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_all_common_by_field_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_all_common_by_field_name`(IN tbl_name varchar(100),IN field_value INT, IN modified DATETIME, IN field_name VARCHAR(50),OUT `status` INT)
BEGIN
	
	set @numQ = concat("SELECT COUNT(*) INTO @num FROM ",tbl_name," AS `tbl` WHERE `tbl`.`", field_name ,"` = ",field_value," AND tbl.is_active = 1");
	
	PREPARE stmt1 FROM @numQ;
	EXECUTE stmt1 ;
	DEALLOCATE PREPARE stmt1;
	
	if(@num>0)THEN
	set @q = concat("UPDATE ",tbl_name," SET 		
		is_active = 0  , 		
		modified = '" , modified ,"'
		WHERE ", field_name ," = " , field_value );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;
	else
	set `status` = 0;
	END IF;
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_arosacos_by_group_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_arosacos_by_group_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_arosacos_by_group_id`(IN group_id INT(11) , OUT `status` INT)
BEGIN
	DELETE FROM `aros_acos` 
	WHERE aros_acos. aro_id = (
		SELECT 	Aro.id
		FROM groups AS `Group`
		LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key
		WHERE Group.id = group_id
	);
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_common` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_common` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_common`(IN tbl_name varchar(100),IN id INT,IN ip_address INT, IN modified DATETIME,	OUT `status` INT)
BEGIN
	
	set @numQ = concat("SELECT COUNT(*) INTO @num FROM ",tbl_name," AS `tbl` WHERE `tbl`.`id` = ",id," AND tbl.is_active = 1");
	
	PREPARE stmt1 FROM @numQ;
	EXECUTE stmt1 ;
	DEALLOCATE PREPARE stmt1;
	
	if(@num>0)THEN
	set @q = concat("UPDATE ",tbl_name," SET 		
		is_active = 0  , 
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;
	else
	set `status` = 0;
	END IF;
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_feature` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_feature` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_feature`(
	IN feature_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `jish`.`features` AS `Feature` WHERE `Feature`.`id` = feature_id AND Feature.is_active = 1) THEN	
		UPDATE `jish`.`features` AS Feature
		SET Feature.is_active = 0,
		Feature.modified = modified,
		Feature.ip_address = ip_address
		WHERE Feature.id = feature_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_group`(
	IN group_id int(11),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	IF EXISTS (SELECT COUNT(*) AS `count` FROM `jish`.`groups` AS `Group` WHERE `Group`.`id` = group_id AND Group.is_active = 1) THEN	
		UPDATE `jish`.`groups` AS `Group`
		SET Group.is_active = 0,
		Group.modified = modified,
		Group.ip_address = ip_address
		WHERE Group.id = group_id ;
	END IF;
	SET `status` = ROW_COUNT();
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_tmp_record_by_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_tmp_record_by_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_tmp_record_by_id`(IN `id` INT(5),OUT `status` INT)
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.id = id ; 
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `remove_tmp_record_by_target_id` */

/*!50003 DROP PROCEDURE IF EXISTS  `remove_tmp_record_by_target_id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `remove_tmp_record_by_target_id`(IN `target_id` VARCHAR(50),OUT `status` INT)
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.target_id = target_id ; 
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `reset_password` */

/*!50003 DROP PROCEDURE IF EXISTS  `reset_password` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5), OUT `status` INT)
    NO SQL
BEGIN
    UPDATE users
    SET    
           users.password = `password`                    
    WHERE  users.id = id;
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `save_tmp_record` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_tmp_record` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 
    INSERT INTO tmps
         (
           tmps.target_id                  , 
           tmps.type                       , 
           tmps.data                       , 
           tmps.expire_at                  ,
           tmps.created
         )
    VALUES 
         ( 
           target_id                    , 
           type                         , 
           data                         ,  
           expire_at                    ,
           created
          
         ) ; 
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `signup_with_social_account` */

/*!50003 DROP PROCEDURE IF EXISTS  `signup_with_social_account` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `signup_with_social_account`(IN xml_data varchar(1000), OUT `status` INT)
BEGIN
DECLARE user_id BIGINT(11);
DECLARE first_name VARCHAR(255);
DECLARE last_name VARCHAR(255);
DECLARE username VARCHAR(50); 
DECLARE pwd VARCHAR(255);
DECLARE group_id INT(11);
DECLARE ip_address  INT(10); 
DECLARE created DATETIME; 
DECLARE link_id BIGINT(11); 
DECLARE image_url VARCHAR(255);
DECLARE type_id VARCHAR(20);
DECLARE access_token VARCHAR(255);
DECLARE screen_name VARCHAR(255);
SELECT ExtractValue(xml_data, '/response/User/first_name') INTO first_name;
SELECT ExtractValue(xml_data, '/response/User/last_name') INTO last_name;
SELECT ExtractValue(xml_data, '/response/User/username') INTO username;
SELECT ExtractValue(xml_data, '/response/User/password') INTO pwd;
SELECT ExtractValue(xml_data, '/response/User/group_id') INTO group_id;
SELECT ExtractValue(xml_data, '/response/User/ip_address') INTO ip_address;
SELECT ExtractValue(xml_data, '/response/User/created') INTO created;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/link_id') INTO link_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/image_url') INTO image_url;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/type_id') INTO type_id;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/access_token') INTO access_token;
SELECT ExtractValue(xml_data, '/response/UserSocialAccount/screen_name') INTO screen_name;
call `create_user`(first_name,last_name,username,pwd,group_id,created,ip_address,@status);
SELECT @status INTO user_id;
INSERT INTO `jish`.`user_social_accounts` 
(	
	user_social_accounts.user_id,
	user_social_accounts.link_id,
	user_social_accounts.email_address,
	user_social_accounts.image_url,
	user_social_accounts.type_id,
	user_social_accounts.access_token,
	user_social_accounts.screen_name,
	user_social_accounts.ip_address,
	user_social_accounts.created,
	user_social_accounts.modified
)values (
	user_id,
	link_id,
	username,
	image_url,
	type_id,
	access_token,
	screen_name,
	ip_address,
	created,
	created
);
SET `status` = LAST_INSERT_ID();
END */$$
DELIMITER ;

/* Procedure structure for procedure `sync_aam_aco` */

/*!50003 DROP PROCEDURE IF EXISTS  `sync_aam_aco` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `sync_aam_aco`(OUT `status` INT)
BEGIN
DELETE FROM auth_action_maps WHERE NOT aco_id IN ( SELECT Aco.id FROM `acos` AS Aco );
SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `test_multi_sets` */

/*!50003 DROP PROCEDURE IF EXISTS  `test_multi_sets` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end */$$
DELIMITER ;

/* Procedure structure for procedure `update_allowed_services` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_allowed_services` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_allowed_services`(in name varchar(100), 
	in description text,
	in allowed_service_group_id int,
	in ip_address int, 
	in modified datetime,
	IN id INT,
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE allowed_services SET 
		name = '" , name ,"',
		description = '" , description ,"',
		allowed_service_group_id = '" , allowed_service_group_id ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_common` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_common` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_common`(
	IN tbl_name varchar(100),
	IN id INT, 
	IN name VARCHAR(100),
	IN ip_address INT, 
	IN modified DATETIME, 	
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE ",tbl_name," SET 
		name = '" , name ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_common_with_col_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_common_with_col_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_common_with_col_name`(
	IN tbl_name varchar(100),
	IN col_name varchar(100),
	IN id INT, 
	IN name VARCHAR(100),
	IN ip_address INT, 
	IN modified DATETIME,
	IN description VARCHAR(50), 	
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE ",tbl_name," SET 
		name = '" , name ,"',
		",col_name," = '" , description ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_feature` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_feature` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_feature`(
	IN feature_id INT(11),
	IN description VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `jish`.`features` AS Feature
	SET Feature.description = description,
	Feature.modified = modified,
	Feature.ip_address = ip_address
	WHERE Feature.id = feature_id AND Feature.is_active = 1;
	SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_group` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_group` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_group`(
	IN group_id INT(11),
	IN name VARCHAR(100),
	IN `modified` DATETIME,
	IN `ip_address` INT(10),
	OUT `status` INT)
BEGIN
	UPDATE `jish`.`groups` AS `Group`
	SET Group.name = name,
	Group.modified = modified,
	Group.ip_address = ip_address
	WHERE Group.id = group_id AND Group.is_active = 1;
	SET `status` = ROW_COUNT();
END */$$
DELIMITER ;

/* Procedure structure for procedure `update_waiting_list_title` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_waiting_list_title` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `update_waiting_list_title`(in title varchar(100), 
	in description text,
	in division_id int,
	in ip_address int, 
	in modified datetime,
	IN id INT,
	OUT `status` INT)
BEGIN
	set @q = concat("UPDATE waiting_lists_titles SET 
		title = '" , title ,"',
		description = '" , description ,"',
		division_id = '" , division_id ,"',
		ip_address = " , ip_address ,",
		modified = '" , modified ,"'
		WHERE id = " , id );
	
	PREPARE stmt FROM @q;
	EXECUTE stmt ;
	SET `status` = ROW_COUNT();
	DEALLOCATE PREPARE stmt;	
END */$$
DELIMITER ;

/* Procedure structure for procedure `verify_login` */

/*!50003 DROP PROCEDURE IF EXISTS  `verify_login` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.id,User.user_type
FROM users AS `User` 
WHERE User.username = email AND User.password = pass AND User.status = 'active'; 
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
