<div class="conversationUsers form">
<?php echo $this->Form->create('ConversationUser'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Conversation User'); ?></legend>
	<?php
		echo $this->Form->input('conversation_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
		echo $this->Form->input('last_view');
		echo $this->Form->input('folder');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Conversation Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Conversations'), array('controller' => 'conversations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation'), array('controller' => 'conversations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
