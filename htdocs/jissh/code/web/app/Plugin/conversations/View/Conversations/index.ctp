<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo "Inbox" ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
					<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
            <h2>Conversations</h2>
           

		</div>
		<!-- End Header -->
		<div class="body">
        
            <table class="datagrid wf">
				<thead>
					<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('last_message_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
				</thead>
				<tbody>
				<?php
	foreach ($conversations as $conversation): ?>
    
	<tr>
		<td><?php echo h($conversation['Conversation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($conversation['User']['username'], array('controller' => 'users', 'action' => 'view', $conversation['User']['id'])); ?>
		</td>
		<td><?php echo h($conversation['Conversation']['title']); ?>&nbsp;</td>
		<td><?php echo h($conversation['Conversation']['created']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($conversation['LastMessage']['messsage_short'], array('action' => 'view', $conversation['Conversation']['id'])); ?>
		</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View |'), array('action' => 'view', $conversation['Conversation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $conversation['Conversation']['id']), null, __('Are you sure you want to delete # %s?', $conversation['Conversation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
				</tbody>
            </table>
			
			
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>




