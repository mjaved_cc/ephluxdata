<?php
echo $this->Html->css('/conversations/css/chosen.jquery');
echo $this->Html->script('/conversations/js/jquery.1.6.2.min');
echo $this->Html->script('/conversations/js/chosen.jquery.min');
echo $this->Html->script('/conversations/js/jquery.validate');
echo $this->Html->script('/conversations/js/additional-methods');
echo $this->Html->script('/conversations/js/messaging');
?>
<script>
	jQuery(document).ready(function($) {
		$('.i-select').chosen();
	});
</script>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<div class="actions">
				<?php echo $this->element('menu'); ?>
			</div>
		</div>
		<!-- End Header -->
		<div class="body">

			<div class="conversations view">
				<h2><?php echo __('Conversation'); ?></h2>
				<!--	<dl>
						<dt><?php //echo __('Id');    ?></dt>
						<dd>
				<?php //echo h($conversation['Conversation']['id']); ?>
							&nbsp;
						</dd>
						<dt><?php //echo __('Started by');    ?></dt>
						<dd>
				<?php //echo $this->Html->link($conversation['User']['username'], array('controller' => 'users', 'action' => 'view', $conversation['User']['id'])); ?>
							&nbsp;
						</dd>
						<dt><?php //echo __('Title');    ?></dt>
						<dd>
				<?php //echo h($conversation['Conversation']['title']); ?>
							&nbsp;
						</dd>
						<dt><?php //echo __('Created');    ?></dt>
						<dd>
				<?php //echo h($conversation['Conversation']['created']); ?>
							&nbsp;
						</dd>
						<dt><?php //echo __('Last Message');    ?></dt>
						<dd>
				<?php //echo $this->Html->link(Sanitize::html($conversation['LastMessage']['message'],array('remove' => true)), array('action' => 'view', $conversation['Conversation']['id'])); ?>
				<?php //echo $this->Html->link($conversation['LastMessage']['message'], array('controller' => 'conversation_messages', 'action' => 'view', $conversation['LastMessage']['id'])); ?>
							&nbsp;
						</dd>
						
					</dl>-->

				<br />

				<div class="related">
					<h3><?php echo __('Related Conversation Messages'); ?></h3>
					<?php if (!empty($conversation['ConversationMessage'])): ?>

			<!--	<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php //echo __('Id');    ?></th>
					<th><?php //echo __('User');    ?></th>
					<th><?php //echo __('Message');    ?></th>
					<th><?php //echo __('Created');    ?></th>
					<th class="actions"><?php //echo __('Actions');    ?></th>
				</tr>-->

						<?php
						$i = 0;
						foreach ($conversation['Messages'] as $conversationMessage):  ?>

							<div class="short_height" onclick="changediv(this)">

								<div> <?php echo $conversationMessage['User']['username']; ?>
									<?php echo $conversationMessage['ConversationMessage']['message']; ?> </div>
							</div>

							<div class="expand_height">
								<div> <?php echo $conversationMessage['User']['username']; ?>
									<?php echo $conversationMessage['ConversationMessage']['message']; ?> </div>

								<div class="actions">
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'conversation_messages', 'action' => 'delete', $conversationMessage['ConversationMessage']['id'], $conversation['Conversation']['id']), null, __('Are you sure you want to delete # %s?', $conversationMessage['ConversationMessage']['id'])); ?>
								</div>
							</div>
							<?php
							$i++;
						endforeach;
						?>

					<?php endif; ?>

					<div>
						 <?php // echo $this->Html->link(Sanitize::html($conversation['LastMessage']['message'], array('remove' => true)), array('action' => 'view', $conversation['Conversation']['id'])); ?>
					     <?php // echo h($conversation['Conversation']['title']); ?> 
						
					</div>

					<?php echo $this->Form->create('ConversationMessages', array('action' => 'add')); ?>

					<fieldset>
						<?php
						echo $this->Form->input('ConversationMessage.conversation_id', array('type' => 'hidden', 'value' => $conversation['Conversation']['id']));
						echo $this->Tinymce->input('ConversationMessage.message', array(
							'label' => 'Reply'
								), array(
							'language' => 'en'
								), ''
						);
						?>
					</fieldset>
					<?php echo $this->Form->end(__('Submit')); ?>
				</div>
			</div>
			<!--			<div class="actions">
			<?php //echo $this->element('menu'); ?>
						</div>-->

		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>