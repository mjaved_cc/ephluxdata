/* =============== Custom Accordion =============== */
	
		//Function for Custom Turning on Accordion 2
			$switchToAccordion2 = function() {
				$(".accordion_header.accordion_1").removeClass("active");
				
				$(".accordion_header.accordion_2").removeClass("disabled");
				$(".accordion_header.accordion_2").addClass("active");
				
				$switchAccordionContent(); //switch content areas
			};
		//Function Check classes of Accordion headers and switch Content Areas accordingly.
			$switchAccordionContent = function() {
				//Switch Accordion_1 Content
					if($(".accordion_header.accordion_1").hasClass("active")){
						$(".accordion_content.accordion_1").removeClass("hide");
						$(".accordion_content.accordion_1").addClass("show");
						
						$('.accordion_content.accordion_1').animate({opacity:1},200, function(){
							//code triggered after animatin goes here
						});
						
					}else{
						$('.accordion_content.accordion_1').animate({opacity:0.5},100, function(){
							$(".accordion_content.accordion_1").removeClass("show");
							$(".accordion_content.accordion_1").addClass("hide");
						});
						
					}
					
					//Switch Accordion_2 content
					if($(".accordion_header.accordion_2").hasClass("active")){
						$(".accordion_content.accordion_2").removeClass("hide");
						$(".accordion_content.accordion_2").addClass("show");
						
						$('.accordion_content.accordion_2').animate({opacity:1},200, function(){
							//code triggered after animatin goes here
						});
					}else{
						$('.accordion_content.accordion_2').animate({opacity:0.5},100, function(){
							$(".accordion_content.accordion_2").removeClass("show");
							$(".accordion_content.accordion_2").addClass("hide");
						});
					}
			};
		$(function(){
		//clicking Accordion Header
			$(".accordion_header").click(function(){
				
				// Switch Accordions
				if($(this).hasClass("accordion_1")){
					
					//Switch Active class from accordion 1 to accordion 2 -- Header
					if($(this).hasClass("active")){ 
						$(this).removeClass("active");
						if(!$(".accordion_header.accordion_2").hasClass("disabled")){
							$(".accordion_header.accordion_2").addClass("active");
						}
					}else{
						$(this).addClass("active");
						$(".accordion_header.accordion_2").removeClass("active");
					}
						
				}else if($(this).hasClass("accordion_2")){
					
					//Switch Active class from accordion 2 to accordion 1 -- Header
					if(!$(".accordion_header.accordion_2").hasClass("disabled")){
						if($(this).hasClass("active")){
							$(this).removeClass("active");
							$(".accordion_header.accordion_1").addClass("active");
						}else{
							$(this).addClass("active");
							$(".accordion_header.accordion_1").removeClass("active");
						}
					}					
				}
				
				//Reader Accordion header clases for 1 & 2 and make the content visible respectively.
				$switchAccordionContent();
				
			});
		});