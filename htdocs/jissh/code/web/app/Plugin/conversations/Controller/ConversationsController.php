<?php

App::uses('conversations.ConversationsAppController', 'Controller');
App::uses('Sanitize', 'Utility');

/**
 * Conversations Controller
 *
 * @property Conversation $Conversation
 * @property AuthComponent $Auth
 */
class ConversationsController extends ConversationsAppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Session', 'Auth');
	public $helpers = array('conversations.Tinymce');

	/* function beforeFilter() {
	  parent::beforeFilter();

	  $this->Auth->deny('index');
	  } */

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$id = AuthComponent::user('id');

		$this->paginate = array(
			'Conversation' => array(
				'conditions' => array('inbox.user_id!=' . $id),
				'limit' => 10,
				'order' => '',
				'group' => array('Conversation.id')
			)
		);
		//$this->paginate = $options;
		$conversation = $this->paginate();
//		pr($conversation);
//		exit;
		$conversations = array();
		foreach ($conversation as $key => $conv) {

			$conversationdt = new DtConversation($conv['Conversation']);

			$conversationdt->add_last_message($conv['LastMessage']);
			$conversations[$key]['LastMessage'] = $conversationdt->get_last_message();

			$conversationdt->add_conversation_meassages($conv['ConversationMessage']);

			$conversations[$key]['ConversationMessage'] = $conversationdt->get_conversation_details();


			$conversationdt->add_User($conv['User']);

			$conversations[$key]['User'] = $conversationdt->get_user();



			$conversations[$key]['Conversation'] = $conversationdt->get_field();

//			echo $conversation;
		}

//		pr($conversations);
//		exit;
		//debug($conversations->get());
		//$conversations = $this->Conversation->ownConversations($id);
		//$this->Conversation->recursive = 0;
		$this->set(compact('conversations'));
	}

	public function sent() {
		$id = AuthComponent::user('id');

		$this->paginate = array(
			'Conversation' => array(
				'conditions' => array('inbox.user_id=' . $id),
				'limit' => 1,
				'order' => '',
				'group' => array('Conversation.id')
			)
		);
		$conversation = $this->paginate();

//		$conversationdt = new DtConversation($conversation);
//		$conversations = $conversationdt->get();
		$conversations = array();
		foreach ($conversation as $key => $conv) {

			$conversationdt = new DtConversation($conv['Conversation']);

			$conversationdt->add_last_message($conv['LastMessage']);
			$conversations[$key]['LastMessage'] = $conversationdt->get_last_message();

			$conversationdt->add_conversation_meassages($conv['ConversationMessage']);
			
		    $conversations[$key]['ConversationMessage'] = $conversationdt->get_conversation_details();
			
			
			$conversationdt->add_User($conv['User']);

			
			
			$conversations[$key]['Conversation'] = $conversationdt->get_field();

//			echo $conversation;
		}
		
		
		

		//$conversations = $this->Conversation->ownConversations($id);
		$this->loadModel('ConversationUser');
		foreach ($conversations as $k => $con) {
			$gUsers = $this->ConversationUser->find('all', array(
				'conditions' => array('ConversationUser.conversation_id' => $con['Conversation']['id'], 'ConversationUser.user_id!=' . $id)));

			foreach ($gUsers as $user) {
				$conversations[$k]['User']['id'] = $user['User']['id'];
				$conversations[$k]['User']['username'] = $user['User']['username'];
			}
		}
		//pr($conversations);
		//$this->Conversation->recursive = 0;
		$this->set(compact('conversations'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		
		$status = $this->Conversation->mark_read( $id ); 
		
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		//$this->Conversation->getConversationMessages($id);
		$this->set('conversation', $this->Conversation->getConversation($id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->__setConversationUsers();

			$this->request->data['Conversation']['user_id'] = AuthComponent::user('id');

			$this->request->data['ConversationMessage'][0]['user_id'] = AuthComponent::user('id');

			//debug($this->request->data);

			$this->Conversation->create();
			//$this->request->data['Conversation']['last_message_id'] = $this->Conversation->ConversationMessage->getInsertID();

			if ($this->Conversation->saveAll($this->request->data)) {
				$data['id'] = $this->Conversation->id;
				$data['last_message_id'] = $this->Conversation->ConversationMessage->getInsertID();

				$this->Conversation->save($data);
				$this->Session->setFlash('Message Sent!');
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash('Failed to Send Message');
			}
		}

		$userRecipients = $this->Conversation->User->find('list', array('conditions' => array('id !=' => AuthComponent::user('id'))));

		$this->loadModel('ConversationGroup');
		$group = $this->ConversationGroup->find('list');
		foreach ($group as $k => $v) {
			$groups[$v] = $v;
		}

		$this->set(compact('userRecipients', 'groups'));
	}

	private function __setConversationUsers() {

		$this->request->data['Recipients']['to'][] = AuthComponent::user('id');
		//debug($this->request->data['Recipients']['to'])	;exit;
		$i = 0;
		$this->loadModel('ConversationGroup');
		foreach ($this->request->data['Recipients']['to'] AS $receiver) {
			if (!is_numeric($receiver)) {
				$gUsers = $this->ConversationGroup->find('list', array(
					'conditions' => array('ConversationGroup.title' => $receiver),
					'fields' => array('id')));

				$users = $this->Conversation->User->find('all', array(
					'conditions' => array('conversation_group_id' => array_keys($gUsers)),
					'fields' => array('id')
						));

				//debug($users);exit;
				foreach ($users as $user) {
					if (AuthComponent::user('id') != $user['User']['id']) {
						$this->request->data['Recipients']['to'][] = $user['User']['id'];
					}
				}
				unset($this->request->data['Recipients']['to'][$i]);
			}
			$i++;
		}

		$this->request->data['Recipients']['to'] = array_unique($this->request->data['Recipients']['to']);
		//debug($this->request->data['Recipients']['to']);exit;

		foreach ($this->request->data['Recipients']['to'] AS $receiver) {
			if ($receiver == AuthComponent::user('id')) {
				$folder = 'sent';
			} else {
				$folder = 'inbox';
			}
			$conversationUser['ConversationUser'][] =
					array(
						'user_id' => $receiver,
						'status' => 0,
						'folder' => $folder
			);
		}

		//debug($conversationUser);
		unset($this->request->data['Recipients']);

		$this->request->data = array_merge($this->request->data, $conversationUser);
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Conversation->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Conversation->read(null, $id);
		}
		$users = $this->Conversation->User->find('list');
		$lastMessages = $this->Conversation->LastMessage->find('list');
		$this->set(compact('users', 'lastMessages'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		if ($this->Conversation->delete()) {
			$this->Session->setFlash(__('Conversation deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Conversation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index() {
		$this->Conversation->recursive = 0;
		$this->set('conversations', $this->paginate());
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null) {
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		$this->set('conversation', $this->Conversation->read(null, $id));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Conversation->create();
			if ($this->Conversation->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation could not be saved. Please, try again.'));
			}
		}
		$users = $this->Conversation->User->find('list');
		$lastMessages = $this->Conversation->LastMessage->find('list');
		$this->set(compact('users', 'lastMessages'));
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null) {
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Conversation->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Conversation->read(null, $id);
		}
		$users = $this->Conversation->User->find('list');
		$lastMessages = $this->Conversation->LastMessage->find('list');
		$this->set(compact('users', 'lastMessages'));
	}

	/**
	 * admin_delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Conversation->id = $id;
		if (!$this->Conversation->exists()) {
			throw new NotFoundException(__('Invalid conversation'));
		}
		if ($this->Conversation->delete()) {
			$this->Session->setFlash(__('Conversation deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Conversation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
