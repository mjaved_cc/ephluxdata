<?php
App::uses('Conversations.ConversationsAppModel', 'Model');
App::uses('User', 'Model');
/**
 * Group Model
 *
 * @property User $User
 */
class ConversationGroup extends ConversationsAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 	
	public $hasMany = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'User.conversation_group_id'
        )
    );
}
