<?php

App::uses('AppController', 'Controller');

/**
 * Providers Controller
 *
 * @property Provider $Provider
 */
class ProvidersController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Provider.name' => 'asc'
		),
		'conditions' => array('Provider.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Provider->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Provider']['q'];
			$cond=array('or'=>array("Provider.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Provider.name' => 'asc'
				),
				'conditions' => array('Provider.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$providers = array();
		foreach ($data as $key => $provider) {
			$obj_providers = new DtProvider($provider[$this->modelClass]);
			$providers[$key][$this->modelClass] = $obj_providers->get_field();
		}

		$this->set(compact('providers'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Provider->id = $id;
		if (!$this->Provider->exists()) {
			throw new NotFoundException(__('Invalid provider'));
		}
		$this->set('provider', $this->Provider->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Provider->set($this->request->data);
			if ($this->Provider->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Provider->create($data)) {
					$this->Session->setFlash(__('The Provider has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Provider could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Provider could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Provider->id = $id;
		if (!$this->Provider->exists()) {
			throw new NotFoundException(__('Invalid provider'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Provider->set($this->request->data);
			if ($this->Provider->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Provider->update($data)) {
					$this->Session->setFlash(__('The provider has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The provider could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The provider could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Provider->get_by_id($id);
			$obj_provider = new DtProvider($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_provider->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Provider->remove($data)) {
			$this->Session->setFlash(__('Provider deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Provider was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
