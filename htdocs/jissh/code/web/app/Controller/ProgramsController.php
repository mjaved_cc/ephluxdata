<?php

App::uses('AppController', 'Controller');

/**
 * Programs Controller
 *
 * @property Program $Program
 */
class ProgramsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Program.name' => 'asc'
		),
		'conditions' => array('Program.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Program->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Program']['q'];
			$cond=array("Program.name LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Program.name' => 'asc'
				),
				'conditions' => array('Program.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$programs = array();
		foreach ($data as $key => $program) {
			$obj_programs = new DtProgram($program[$this->modelClass]);
			$programs[$key][$this->modelClass] = $obj_programs->get_field();
		}

		$this->set(compact('programs'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Program->id = $id;
		if (!$this->Program->exists()) {
			throw new NotFoundException(__('Invalid program'));
		}

		$program = array();

		$result = $this->Program->get_by_id($id);

		$obj_program = new DtProgram($result[$this->modelClass]);
		$program[$this->modelClass] = $obj_program->get_field();

		$this->set(compact('program'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Program->set($this->request->data);
			if ($this->Program->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Program->create($data)) {
					$this->Session->setFlash(__('The Program has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Program could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Program could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Program->id = $id;
		if (!$this->Program->exists()) {
			throw new NotFoundException(__('Invalid program'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Program->set($this->request->data);
			if ($this->Program->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Program->update($data)) {
					$this->Session->setFlash(__('The Program has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Program could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Program could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Program->get_by_id($id);
			$obj_program = new DtProgram($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_program->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Program->remove($data)) {
			$this->Session->setFlash(__('Program deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Program was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
