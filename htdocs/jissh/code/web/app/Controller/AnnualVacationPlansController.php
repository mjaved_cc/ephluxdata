<?php

App::uses('AppController', 'Controller');

/**
 * AnnualVacationPlans Controller
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class AnnualVacationPlansController extends AppController {

	public $uses = array('AnnualVacationPlan', 'Appointment','EmailTemplate');

	function beforeRender() {

		$this->set('page_heading', 'Annual Vacation Plan');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index($employee_detail_id = null) {
		$this->AnnualVacationPlan->recursive = 0;
		
		$this->paginate = array(
			'limit' => 25,
			'order' => array(
				'AnnualVacationPlan.id' => 'desc'
			),
			'conditions' => array('AnnualVacationPlan.employee_detail_id' =>$employee_detail_id )
		);
		
		$data = $this->paginate();
		
		$annualVacationPlans = array();
		foreach ($data as $key => $annualVacationPlan) {
			$obj_annualVacationPlan = new DtAnnualVacationPlan($annualVacationPlan['AnnualVacationPlan']);
			$obj_annualVacationPlan->add_employee_detail($annualVacationPlan['EmployeeDetail']);
			$obj_annualVacationPlan->add_AvpType($annualVacationPlan['AvpType']);
			$annualVacationPlans[$key]['AnnualVacationPlan'] = $obj_annualVacationPlan->get_field();
			$annualVacationPlans[$key]['EmployeeDetail'] = $obj_annualVacationPlan->get_employee_details();
			$annualVacationPlans[$key]['AvpType'] = $obj_annualVacationPlan->get_AvpType();
			
		}
		$this->set('employee_detail_id',$employee_detail_id);
		$this->set('annualVacationPlans', $annualVacationPlans);
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->AnnualVacationPlan->id = $id;
		if (!$this->AnnualVacationPlan->exists()) {
			throw new NotFoundException(__('Invalid annual vacation plan'));
		}
		$this->set('annualVacationPlan', $this->AnnualVacationPlan->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @param string|int $employee_detail_id employee detail id
	 * @return void
	 */
	public function add($employee_detail_id=0) {
		if ($this->AnnualVacationPlan->EmployeeDetail->is_exists($employee_detail_id)) {

			if ($this->request->is('post')) {
				$this->AnnualVacationPlan->set($this->request->data);

				if ($this->AnnualVacationPlan->validates()) {
					$employee_detail_id = $this->request->data['AnnualVacationPlan']['employee_detail_id'];
					$leave_exists = $this->AnnualVacationPlan->is_doctor_leaves_exists_between_intervals(
					$this->request->data['AnnualVacationPlan']['last_work_day'], $this->request->data['AnnualVacationPlan']['report_to_work'], $employee_detail_id);

					if (!$leave_exists) {
						$appointment_exists = $this->Appointment->is_doctor_appointments_exists_between_intervals(
								$this->request->data['AnnualVacationPlan']['last_work_day'], $this->request->data['AnnualVacationPlan']['report_to_work'], $employee_detail_id);
						if (!$appointment_exists) {

							$this->request->data['AnnualVacationPlan']['employee_detail_id'] = $employee_detail_id;
							if ($this->AnnualVacationPlan->create($this->request->data['AnnualVacationPlan'])) {

								$this->Session->setFlash(__('The annual vacation plan has been saved'));
								$this->redirect(array('action' => 'index', $employee_detail_id));
							} else {
								$this->Session->setFlash(__('The annual vacation plan could not be saved. Please, try again.'));
								$this->redirect(array('controller' => 'annual_vacation_plans', 'action' => 'index',$employee_detail_id));
							}
						} else {
							$this->Session->setFlash(__('Appointment exists between given intervals so leave is not allowed'));
							$this->redirect(array('controller' => 'annual_vacation_plans', 'action' => 'add', $employee_detail_id));
						}
					} else {
						$this->Session->setFlash(__('You have already asked for leave between intervals'));
						$this->redirect(array('controller' => 'annual_vacation_plans', 'action' => 'add', $employee_detail_id));
					}
				}
			}
			
			$this->set('employee_detail_id',$employee_detail_id);
			$avpTypes = $this->AnnualVacationPlan->AvpType->format_drop_down();
			$this->set(compact('avpTypes'));
		} else {
			$this->Session->setFlash(__('Employee does not exists'));
			$this->redirect(array('controller' => 'employee_details', 'action' => 'index'));
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id annual vacation plan id
	 * @param string $employee_detail_id employee detail id
	 * @return void
	 */
	public function edit($id, $employee_detail_id) {
		
//		if ($this->AnnualVacationPlan->EmployeeDetail->is_exists($employee_detail_id)) {
//			if ($this->request->is('post')) {
//				$this->AnnualVacationPlan->set($this->request->data);
//
//				if ($this->AnnualVacationPlan->validates()) {
//
//					$leave_exists = $this->AnnualVacationPlan->is_doctor_leaves_exists_between_intervals(
//							$this->request->data['AnnualVacationPlan']['last_work_day'], $this->request->data['AnnualVacationPlan']['report_to_work'], $employee_detail_id);
//
//					if (!$leave_exists) {
//						$appointment_exists = $this->Appointment->is_doctor_appointments_exists_between_intervals(
//								$this->request->data['AnnualVacationPlan']['last_work_day'], $this->request->data['AnnualVacationPlan']['report_to_work'], $employee_detail_id);
//						if (!$appointment_exists) {
//
//							$this->request->data['AnnualVacationPlan']['employee_detail_id'] = $employee_detail_id;
//							if ($this->AnnualVacationPlan->create($this->request->data['AnnualVacationPlan'])) {
//								$this->Session->setFlash(__('The annual vacation plan has been saved'));
//								$this->redirect(array('action' => 'index', $employee_detail_id));
//							} else {
//								$this->Session->setFlash(__('The annual vacation plan could not be saved. Please, try again.'));
//								$this->redirect(array('controller' => 'employee_details', 'action' => 'index'));
//							}
//						} else {
//							$this->Session->setFlash(__('Appointment exists between given intervals so leave is not allowed'));
//							$this->redirect(array('controller' => 'annual_vacation_plans', 'action' => 'add', $employee_detail_id));
//						}
//					} else {
//						$this->Session->setFlash(__('You have already asked for leave between intervals'));
//						$this->redirect(array('controller' => 'annual_vacation_plans', 'action' => 'add', $employee_detail_id));
//					}
//				}
//			}
//
//			$avpTypes = $this->AnnualVacationPlan->AvpType->format_drop_down();
//			$this->set(compact('avpTypes'));
//			
//		} else {
//			$this->Session->setFlash(__('Employee does not exists'));
//			$this->redirect(array('controller' => 'employee_details', 'action' => 'index'));
//		}

		$this->AnnualVacationPlan->id = $id;
		if (!$this->AnnualVacationPlan->exists()) {
			throw new NotFoundException(__('Invalid annual vacation plan'));
		}
		if ($this->request->is('post')) {
			if ($this->AnnualVacationPlan->save($this->request->data)) {
				$this->Session->setFlash(__('The annual vacation plan has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The annual vacation plan could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AnnualVacationPlan->get_details_by_id($id);
		}
		
		$avpTypes = $this->AnnualVacationPlan->AvpType->format_drop_down();
		$this->set(compact('avpTypes'));
		$this->render('add');
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null,$employee_detail_id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AnnualVacationPlan->id = $id;
		if (!$this->AnnualVacationPlan->exists()) {
			throw new NotFoundException(__('Invalid annual vacation plan'));
		}
		if ($this->AnnualVacationPlan->delete()) {
			$this->Session->setFlash(__('Annual vacation plan deleted'));
			$this->redirect(array('action' => 'index',$employee_detail_id));
		}
		$this->Session->setFlash(__('Annual vacation plan was not deleted'));
		$this->redirect(array('action' => 'index',$employee_detail_id));
	}

}
