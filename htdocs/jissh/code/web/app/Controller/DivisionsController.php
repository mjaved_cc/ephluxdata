<?php

App::uses('AppController', 'Controller');

/**
 * Divisions Controller
 *
 * @property Division $Division
 */
class DivisionsController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Division.name' => 'asc'
		),
		'conditions' => array('Division.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Division->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Division']['q'];
			$cond=array("Division.name LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Division.name' => 'asc'
				),
				'conditions' => array('Division.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$divisions = array();
		foreach ($data as $key => $division) {
			$obj_divisions = new DtDivision($division[$this->modelClass]);
			$divisions[$key][$this->modelClass] = $obj_divisions->get_field();
		}

		$this->set(compact('divisions'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Division->id = $id;
		if (!$this->Division->exists()) {
			throw new NotFoundException(__('Invalid division'));
		}

		$division = array();

		$result = $this->Division->get_by_id($id);

		$obj_division = new DtDivision($result[$this->modelClass]);
		$division[$this->modelClass] = $obj_division->get_field();

		$this->set(compact('division'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Division->set($this->request->data);
			if ($this->Division->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];				
				$data['description'] = $this->request->data[$this->modelClass]['description'];				

				if ($this->Division->create($data)) {
					$this->Session->setFlash(__('The division has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The division could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The division could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Division->id = $id;
		if (!$this->Division->exists()) {
			throw new NotFoundException(__('Invalid division'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Division->set($this->request->data);
			if ($this->Division->validates(array('fieldList' => array('name')))) {
				
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				
				if ($this->Division->update($data)) {
					$this->Session->setFlash(__('The division has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The division could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The division could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Division->get_by_id($id);
			$obj_division = new DtDivision($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_division->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Division->remove($data)) {
			$this->Session->setFlash(__('Division deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Division was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
