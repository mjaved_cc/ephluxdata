<?php

App::uses('AppController', 'Controller');

/**
 * Notifications Controller
 *
 * @property Notification $Notification
 */
class NotificationsController extends AppController {
	public $uses = array('Notification','WaitingListsTitle','User');
	/*public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Notification.name' => 'asc'
		),
		'conditions' => array('Notification.is_active' => IS_ACTIVE)
	);*/
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Notification->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Notification']['q'];
			$cond=array('or'=>array("Notification.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Notification.name' => 'asc'
				),
				'conditions' => array('Notification.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$avp_types = array();
		foreach ($data as $key => $avp_type) {
			$obj_avp_types = new DtNotification($avp_type[$this->modelClass]);
			$avp_types[$key][$this->modelClass] = $obj_avp_types->get_field();
		}

		$this->set(compact('avp_types'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Notification->id = $id;
		if (!$this->Notification->exists()) {
			throw new NotFoundException(__('Invalid avp_type'));
		}

		$avp_type = array();

		$result = $this->Notification->get_by_id($id);

		$obj_avp_type = new DtNotification($result[$this->modelClass]);
		$avp_type[$this->modelClass] = $obj_avp_type->get_field();

		$this->set(compact('avp_type'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {
			//pr($this->data);	
			
			if($this->data['send_to']=="individual"){
				foreach($this->data['Notification']['selected_emails'] as $email){
					$this->App->send_email($email, $this->data['Notification']['subject'], $this->data['Notification']['message'], $template = 'default', $layout = 'default');
				}
			}
			else{
				$email_users_arr = array();
				if($this->data['send_to']=="waiting_list") {
					$waiting_list_id = $this->data['Notification']['waiting_list'];
					$data = $this->Notification->get_user_emails_by_waiting_list_id($waiting_list_id);
					
					foreach($data as $user) {
						$email_users_arr[] = $user['User']['username'];	
					}
				}
				elseif($this->data['send_to']=="doctors") {
					$Emp_user = $this->User->find('all', array(
							'fields' => array('username'),
							'conditions' => array('User.user_type' => 'doctor','User.status' => 'active')
					));
					
					foreach($Emp_user as $user) {
						$email_users_arr[] = $user['User']['username'];	
					}
				}
				elseif($this->data['send_to']=="others") {
					$Emp_user = $this->User->find('all', array(
							'fields' => array('username'),
							'conditions' => array('User.user_type' => 'front_desk','User.status' => 'active')
					));
					//pr($Emp_user);exit;
					foreach($Emp_user as $user) {
						$email_users_arr[] = $user['User']['username'];	
					}
				}
				elseif($this->data['send_to']=="patients") {
					$Emp_user = $this->User->find('all', array(
							'fields' => array('username'),
							'conditions' => array('User.user_type' => 'patient','User.status' => 'active')
					));
					//pr($Emp_user);exit;
					foreach($Emp_user as $user) {
						$email_users_arr[] = $user['User']['username'];	
					}
				}
				elseif($this->data['send_to']=="all") {
					$Emp_user = $this->User->find('all', array(
							'fields' => array('username'),
							'conditions' => array('User.status' => 'active')
					));
					//pr($Emp_user);exit;
					foreach($Emp_user as $user) {
						$email_users_arr[] = $user['User']['username'];	
					}
				}
				
				if(!empty($email_users_arr)) {
					$this->loadModel('EmailTemplate');
					$data=array();
					$data['subject'] = $this->data['Notification']['subject'];
					$data['template'] = $this->data['Notification']['message'];
					$this->EmailTemplate->save($data);
					$template_id = $this->EmailTemplate->getInsertID();	
					
					$data=array();
					$i=0;
					foreach($email_users_arr as $email) {
						$data[$i]['target_id'] = $email;
						$data[$i]['type'] = 'email';
						$data[$i]['status'] = 'pending';
						$data[$i]['template_id'] = $template_id;	
						
						$i++;
					}
					
					$this->loadModel('EmailQueue');
					$this->EmailQueue->saveAll($data);
				}
			}
			
			$this->Session->setFlash(__('The Notifications are saved to queue.'));
			$this->redirect(array('action' => 'add'));
		}
		
		$waiting_lists = $this->WaitingListsTitle->find('list');
		$this->set('waiting_lists',$waiting_lists);
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Notification->id = $id;
		if (!$this->Notification->exists()) {
			throw new NotFoundException(__('Invalid avp_type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Notification->set($this->request->data);
			if ($this->Notification->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Notification->update($data)) {
					$this->Session->setFlash(__('The Notification has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Notification could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Notification could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Notification->get_by_id($id);
			$obj_avp_type = new DtNotification($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_avp_type->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Notification->remove($data)) {
			$this->Session->setFlash(__('Notification deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Notification was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
