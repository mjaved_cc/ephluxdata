<?php

App::uses('AppController', 'Controller');

/**
 * MpdActivities Controller
 *
 * @property MpdActivity $MpdActivity
 */
class MpdActivitiesController extends AppController {

	var $uses = array('MpdActivity', 'User', 'EmployeeDetail', 'MpdActivityList', 'Modality', 'Program');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'MpdActivity.id' => 'desc'
		),
		'conditions' => array('MpdActivity.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', 'Mandatory Activity');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index($id = null) {
		$this->MpdActivity->recursive = 0;

		if (($this->request->is('post') and isset($this->data['MpdActivity']['q'])) || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['MpdActivity']['q'];
			$id = isset($this->request->params['named']['id']) ? $this->request->params['named']['id'] : $this->data['MpdActivity']['emp_id'];

			$conditons = array('OR' => array(
					'Program.name LIKE' => '%' . $keyword . '%',
					'MpdActivityList.name LIKE' => '%' . $keyword . '%',
					'Modality.name LIKE' => '%' . $keyword . '%'
					));
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);

			$this->request->params['named']['q'] = $keyword;
		}
		$this->request->params['named']['id'] = $id;
		if (!$this->EmployeeDetail->exists($id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}

		$cond = array('MpdActivity.employee_detail_id' => $id);
		$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $cond);

		$data = $this->paginate();
		//pr($data);exit;	
		$mpdActivities = array();
		foreach ($data as $key => $mpdActivity) {
			$obj_user = new DtUser($mpdActivity['User']);
			$obj_user->add_employee_detail($mpdActivity['EmployeeDetail']);
			$obj_user->EmployeeDetail->add_mpdActivityList($mpdActivity['MpdActivityList']);
			$obj_user->EmployeeDetail->add_mpdActivity($mpdActivity[$this->modelClass]);
			$obj_user->EmployeeDetail->add_modality($mpdActivity['Modality']);
			$obj_user->EmployeeDetail->add_program($mpdActivity['Program']);

			/* Retrieve data from rendering logic */
			//$data = array();
			//$data['User'] = $obj_user->get_field();

			$mpdActivities[$key]['User'] = $obj_user->get_field();
			if ($obj_user->EmployeeDetail instanceof DtEmployeeDetail) {
				$mpdActivities[$key]['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
				$mpdActivities[$key]['MpdActivity'] = $obj_user->EmployeeDetail->get_mpdActivities();
				$mpdActivities[$key]['MpdActivityList'] = $obj_user->EmployeeDetail->get_mpdActivityLists();
				$mpdActivities[$key]['Modality'] = $obj_user->EmployeeDetail->get_modalities();
				$mpdActivities[$key]['Program'] = $obj_user->EmployeeDetail->get_programs();
			}
		}
		$this->set('emp_id', $id);
		$this->set(compact('mpdActivities'));
		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
			$this->render('/Elements/mdp_activity_listing');
		}
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->MpdActivity->id = $id;
		if (!$this->MpdActivity->exists()) {
			throw new NotFoundException(__('Invalid mpd activity'));
		}
		$this->set('mpdActivity', $this->MpdActivity->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @param string|int $id employee details id
	 * @return void
	 */
	public function add($id = null) {
		if ($this->request->is('post')) {
			$this->MpdActivity->create();
			if ($this->MpdActivity->save($this->request->data, array('validate' => 'first'))) {
				$this->Session->setFlash(__('The mpd activity has been saved'));
				$this->redirect(array('action' => 'index', $id));
			} else {
				$this->Session->setFlash(__('The mpd activity could not be saved. Please, try again.'));
			}
		}
		$employeeDetails = $this->MpdActivity->EmployeeDetail->find('list');
		$programs = $this->MpdActivity->Program->find('list');
		$mpdActivityLists = $this->MpdActivity->MpdActivityList->find('list');
		$modalities = $this->MpdActivity->Modality->find('list');
		$this->set('emp_id', $id);
		$this->set(compact('employeeDetails', 'programs', 'mpdActivityLists', 'modalities'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($emp_id = null, $id = null) {
		$this->MpdActivity->id = $id;
		if (!$this->MpdActivity->exists()) {
			throw new NotFoundException(__('Invalid mpd activity'));
		}
		if (!$this->EmployeeDetail->exists($emp_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->MpdActivity->save($this->request->data, array('validate' => 'first'))) {
				$this->Session->setFlash(__('The mpd activity has been saved'));
				$this->redirect(array('action' => 'index', $emp_id));
			} else {
				$this->Session->setFlash(__('The mpd activity could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->MpdActivity->read(null, $id);
		}
		$this->set('emp_id', $emp_id);
		$employeeDetails = $this->MpdActivity->EmployeeDetail->find('list');
		$programs = $this->MpdActivity->Program->find('list');
		$mpdActivityLists = $this->MpdActivity->MpdActivityList->find('list');
		$modalities = $this->MpdActivity->Modality->find('list');
		$this->set(compact('employeeDetails', 'programs', 'mpdActivityLists', 'modalities'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($emp_id = null, $id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MpdActivity->id = $id;
		if (!$this->MpdActivity->exists()) {
			throw new NotFoundException(__('Invalid mpd activity'));
		}
		if (!$this->EmployeeDetail->exists($emp_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		if ($this->MpdActivity->delete()) {
			$this->Session->setFlash(__('Mpd activity deleted'));
			$this->redirect(array('action' => 'index', $emp_id));
		}
		$this->Session->setFlash(__('Mpd activity was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
