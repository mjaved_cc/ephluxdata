<?php
App::uses('AppController', 'Controller');
/**
 * TemplateAttributes Controller
 *
 * @property TemplateAttribute $TemplateAttribute
 */
class TemplateAttributesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'TemplateAttribute.title' => 'asc'
		),
		'conditions' => array('TemplateAttribute.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', 'Template Attributes');
		
		$attr_types = array('text'=>'text', 'textarea'=>'textarea', 'checkbox'=>'checkbox', 'radio'=>'radio', 'texteditor'=>'texteditor');
		$this->set('attr_types',$attr_types);
		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->TemplateAttribute->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['TemplateAttribute']['q'];
			$conditons=array('or'=>array("TemplateAttribute.title LIKE '%$keyword%'","TemplateAttribute.attr_type LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			$this->request->params['named']['q'] = $keyword;
		}
		
		$data = $this->paginate();
			
		//pr($data);
		$templateAttributes = array();
		foreach ($data as $key => $templateAttribute) {
			$obj_templateAttribute = new DtTemplateAtrribute($templateAttribute[$this->modelClass]);
			$templateAttributes[$key][$this->modelClass] = $obj_templateAttribute->get_field();
			
			if(isset($templateAttribute["TemplateAtrributesCategory"])) {
				$obj_templateAttributeCategory = new DtTemplateAtrributesCategory($templateAttribute["TemplateAtrributesCategory"]);
				$templateAttributes[$key]["TemplateAtrributesCategory"] = $obj_templateAttributeCategory->get_field();
			}
		}
		$this->set(compact('templateAttributes'));
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->TemplateAttribute->id = $id;
		if (!$this->TemplateAttribute->exists()) {
			throw new NotFoundException(__('Invalid allowed service'));
		}
		$this->set('allowedService', $this->TemplateAttribute->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
 	public function add($id = null) {
		if ($this->request->is('post')) {
			$this->request->data['modified'] = $this->App->get_current_datetime();
			$this->request->data['ip_address'] = $this->App->get_numeric_ip_representation();
			$this->TemplateAttribute->create();
			if ($this->TemplateAttribute->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The attribute has been saved'));
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('The attribute could not be saved. Please, try again.'));
			}
		}
		$categories = $this->TemplateAttribute->TemplateAttributesCategory->find("list",array('conditions'=>array('TemplateAttributesCategory.is_active'=>1)));
		$this->set('categories',$categories);
		
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->TemplateAttribute->id = $id;
		if (!$this->TemplateAttribute->exists()) {
			throw new NotFoundException(__('Invalid template attribute'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TemplateAttribute->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The template attribute has been saved'));
				$this->redirect(array('action' => 'index',$emp_id));
			} else {
				$this->Session->setFlash(__('The template attribute could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->TemplateAttribute->read(null, $id);
			$obj_templateAttribute = new DtTemplateAtrribute($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_templateAttribute->get_field();
			
			$categories = $this->TemplateAttribute->TemplateAttributesCategory->find("list",array('conditions'=>array('TemplateAttributesCategory.is_active'=>1)));
			$this->set('categories',$categories);
		}
		
	}
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->TemplateAttribute->remove($data)) {
			$this->Session->setFlash(__('Template Attribute deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Template Attribute was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
