<?php

App::uses('AppController', 'Controller');

/**
 * ConfigOptions Controller
 *
 * @property ConfigOption $ConfigOption
 */
class ConfigOptionsController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'ConfigOption.name' => 'asc'
		),
		'conditions' => array('ConfigOption.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	/**
	 * index method
	 *
	 * @return void
	 */
	 public function index() {
		$this->ConfigOption->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['ConfigOption']['q'];
			$cond=array("ConfigOption.name LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'ConfigOption.name' => 'asc'
				),
				'conditions' => array('ConfigOption.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		
		$configOptions = array();
		foreach ($data as $key => $configOption) {
			$obj_configOptions = new DtConfigOption($configOption[$this->modelClass]);
			$configOptions[$key][$this->modelClass] = $obj_configOptions->get_field();
		}

		$this->set(compact('configOptions'));
	}
	
	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	 
	 public function view($id = null) {
		$this->ConfigOption->id = $id;
		if (!$this->ConfigOption->exists()) {
			throw new NotFoundException(__('Invalid division'));
		}

		$configOption = array();

		$result = $this->ConfigOption->get_by_id($id);

		$obj_ConfigOption = new DtConfigOption($result[$this->modelClass]);
		$configOption[$this->modelClass] = $obj_ConfigOption->get_field();

		$this->set(compact('configOption'));
	}
	
	/**
	 * add method
	 *
	 * @return void
	 */
	 
	public function add() {
		if ($this->request->is('post')) {

			$this->ConfigOption->set($this->request->data);
			if ($this->ConfigOption->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];				
				$data['value'] = $this->request->data[$this->modelClass]['value'];				

				if ($this->ConfigOption->create($data)) {
					$this->Session->setFlash(__('The ConfigOption has been saved'));
					$this->redirect(array('controller'=>'config_options', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('The ConfigOption could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The ConfigOption could not be saved. Please, try again.'));
			}
		}
	}
	 
	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	 
	 public function edit($id = null) {
		$this->ConfigOption->id = $id;
		if (!$this->ConfigOption->exists()) {
			throw new NotFoundException(__('Invalid ConfigOption'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ConfigOption->set($this->request->data);
			if ($this->ConfigOption->validates(array('fieldList' => array('name','value')))) {
				
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['value'] = $this->request->data[$this->modelClass]['value'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				
				if ($this->ConfigOption->update($data)) {
					$this->Session->setFlash(__('The ConfigOption has been saved'));
					$this->redirect(array('controller'=>'config_options','action' => 'index'));
				} else {
					$this->Session->setFlash(__('The ConfigOption could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The ConfigOption could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->ConfigOption->get_by_id($id);
			$obj_ConfigOption = new DtConfigOption($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_ConfigOption->get_field();
		}
	}
	

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	 
	 public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->ConfigOption->remove($data)) {
			$this->Session->setFlash(__('ConfigOption deleted'));
			$this->redirect(array('controller'=>'config_options','action' => 'index'));
		}

		$this->Session->setFlash(__('ConfigOption was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
}
