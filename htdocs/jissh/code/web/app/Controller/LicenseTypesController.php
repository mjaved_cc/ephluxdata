<?php

App::uses('AppController', 'Controller');

/**
 * LicenseTypes Controller
 *
 * @property LicenseType $LicenseType
 */
class LicenseTypesController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'LicenseType.name' => 'asc'
		),
		'conditions' => array('LicenseType.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->LicenseType->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['LicenseType']['q'];
			$cond=array('or'=>array("LicenseType.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'LicenseType.name' => 'asc'
				),
				'conditions' => array('LicenseType.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$license_types = array();
		foreach ($data as $key => $license_type) {
			$obj_license_types = new DtLicenseType($license_type[$this->modelClass]);
			$license_types[$key][$this->modelClass] = $obj_license_types->get_field();
		}

		$this->set(compact('license_types'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->LicenseType->id = $id;
		if (!$this->LicenseType->exists()) {
			throw new NotFoundException(__('Invalid License Type'));
		}
		$this->set('license_type', $this->LicenseType->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->LicenseType->set($this->request->data);
			if ($this->LicenseType->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->LicenseType->create($data)) {
					$this->Session->setFlash(__('The License Type has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The License Type could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The License Type could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->LicenseType->id = $id;
		if (!$this->LicenseType->exists()) {
			throw new NotFoundException(__('Invalid License Type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->LicenseType->set($this->request->data);
			if ($this->LicenseType->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->LicenseType->update($data)) {
					$this->Session->setFlash(__('The License Type has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The License Type could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The License Type could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->LicenseType->get_by_id($id);
			$obj_license_type = new DtLicenseType($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_license_type->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->LicenseType->remove($data)) {
			$this->Session->setFlash(__('LicenseType deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('LicenseType was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
