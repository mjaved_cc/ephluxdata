<?php

App::uses('AppController', 'Controller');

class AppointmentsController extends AppController {

	public $uses = array('Appointment', 'WaitingListsTitle', 'PatientDetail', 'EmployeeDetail', 'User', 'EmployeeWorkingHour', 'PatientWaitingList', 'CallLog', 'PatientMedication', 'PatientAttachmentReport', 'AllowedServiceGroup');
	public $components = array('ApiAppointment', 'ApiEmployeeDetail', 'Paginator');
	public $paginate = array(
		'Appointment' => array(
			'limit' => 1,
			'order' => array(
				'Appointment.created' => 'desc'
			),
			'conditions' => array('Appointment.status' => 'pending')
		)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);
		// form wizard
		$this->JCManager->add_js(array(MRIYA_JS_PATH . 'formToWizard.min', MRIYA_PATH . 'wizard', 'employee_detail', 'event'));

		parent::beforeRender();
	}

	function save_call_log() {
		if ($this->request->is('post')) {
			$data['created'] = $this->App->get_current_datetime();
			$data['ip_address'] = $this->App->get_numeric_ip_representation();
			$data['caller_id'] = $this->request->data['caller_id'];
			$data['call_answered_by'] = $this->request->data['receiver'];
			$data['call_subject'] = $this->request->data['call_subject'];
			$data['calling_date_time'] = $this->request->data['calling_date_time'];
			$data['appointment_id'] = $this->request->data['id'];

			if ($this->CallLog->save($data)) {
				$this->Session->setFlash(__('Call Log Added'));
				$this->redirect(array('action' => 'view', $data['appointment_id']));
			} else {
				$this->Session->setFlash(__('Error adding call log'));
				$this->redirect(array('action' => 'view', $data['appointment_id']));
			}
		}
	}

	function cancel_appointments() {

		$appointment_detail = $this->Appointment->find('all', array(
			'conditions' => array('EmployeeDetail.user_id' => $this->data['employee_detail_id'],
				'Appointment.appointment_date BETWEEN ? AND ?' => array($this->data['from_date'], $this->data['to_date']))
				));

		if (!empty($appointment_detail)) {
			//$appointment_data = array();
			foreach ($appointment_detail as $key => $appointment) {
				$appointment_data[] = $appointment['Appointment']['id'];
				//$appointment_data[$key]['status'] = 'cancelled';	
			}
			$ids = implode(',', $appointment_data);
			$this->Appointment->unbindModelAll();
			$this->Appointment->updateAll(
					array('Appointment.status' => "'cancelled'"), array('Appointment.id in' => $appointment_data)
			);

			$this->loadModel('EmailTemplate');
			$data = array();
			$data['template'] = $this->data['template'];
			$this->EmailTemplate->save($data);
			$template_id = $this->EmailTemplate->getInsertID();

			$email_users_arr = $this->get_appointment_members($appointment_detail);

			$data = array();
			$i = 0;
			foreach ($email_users_arr as $email) {
				$data[$i]['target_id'] = $email;
				$data[$i]['type'] = 'email';
				$data[$i]['status'] = 'pending';
				$data[$i]['template_id'] = $template_id;

				$i++;
			}

			$this->loadModel('EmailQueue');
			$this->EmailQueue->saveAll($data);
		}

		$this->Session->setFlash(__('Appointments canceled'));
		$this->redirect(array('controller' => 'employee_details', 'action' => 'doctor_details', $this->data['employee_detail_id']));
	}

	function re_schedule() {
		$data = array();
		$data['appointment_id'] = $this->data['appointment_id'];
		$data['appoitnment_date'] = date('Y-m-d h:i:s', strtotime($this->data['appointment_date']));
		$data['stat'] = 'pending';
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();

		if ($this->Appointment->update_appointment_reschedule($data)) {

			$appointment_detail = $this->Appointment->find('first', array(
				'conditions' => array('Appointment.id' => $this->data['appointment_id'])
					));
			$obj_appointment = new DtAppointment($appointment_detail['Appointment']);
			$appointment_detail['Appointment'] = $obj_appointment->get_field();

			$email_users = $this->get_appointment_members($appointment_detail);

			$subject = "Your Appintment Re-scheduled";
			$message = 'Hi,<br><br> The appointment shceduled on ' . $appointment_detail['Appointment']['appointment_date_formated'] . ' has been re-scheduled to ' . date('F j, Y H:i a', strtotime($this->data['appointment_date']));
			foreach ($email_users as $email) {
				$this->App->send_email($email, $subject, $message, $template = 'default', $layout = 'default');
			}
			$this->Session->setFlash(__('Appointment re-scheduled'));
			$this->redirect(array('action' => 'view', $this->data['appointment_id']));
		}
	}

	function get_appointment_members($appointment_detail) {
		//pr($appointment_detail);
		$email_users = array();
		foreach ($appointment_detail as $appointment) {
			if ($appointment['Appointment']['is_group']) {
				foreach ($appointment['AppointmentGroup'] as $key => $value) {
					if (!array_key_exists($value['employee_detail_id'], $email_users)) {
						$Emp_user = $this->User->find('first', array(
							'conditions' => array('User.id' => $value['employee_detail_id'])
								));

						if (!empty($Emp_user)) {
							$email_users[$value['employee_detail_id']] = $Emp_user['User']['username'];
						}
					}
					if (!array_key_exists($value['patient_detail_id'], $email_users)) {
						$Ptd_user = $this->PatientDetail->find('first', array(
							'conditions' => array('PatientDetail.id' => $value['patient_detail_id'])
								));

						if (!empty($Ptd_user)) {
							$email_users[$value['patient_detail_id']] = $Ptd_user['User']['username'];
						}
					}
				}
			}

			if (!array_key_exists($appointment['EmployeeDetail']['id'], $email_users)) {
				$Emp_user = $this->User->find('first', array(
					'conditions' => array('User.id' => $appointment['EmployeeDetail']['user_id'])
						));

				if (!empty($Emp_user)) {
					$email_users[$appointment['EmployeeDetail']['id']] = $Emp_user['User']['username'];
				}
			}
			if (!array_key_exists($appointment['PatientDetail']['user_id'], $email_users)) {

				$Ptd_user = $this->User->find('first', array(
					'conditions' => array('User.id' => $appointment['PatientDetail']['user_id'])
						));

				if (!empty($Ptd_user)) {
					$email_users[$appointment['PatientDetail']['id']] = $Ptd_user['User']['username'];
				}
			}
		}
		return $email_users;
	}

	function cancel($id = 0) {
		$data = array();
		$data['appointment_id'] = $id;
		$data['stat'] = 'cancelled';
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();

		if ($this->Appointment->update_appointment_status($data)) {

			$appointment_detail = $this->Appointment->find('first', array(
				'conditions' => array('Appointment.id' => $id)
					));
			$obj_appointment = new DtAppointment($appointment_detail['Appointment']);
			$appointment_detail['Appointment'] = $obj_appointment->get_field();

			$email_users = $this->get_appointment_members($appointment_detail);

			$subject = "Your Appintment Cancelled";
			$message = 'Hi,<br><br> The appointment shceduled on ' . $appointment_detail['Appointment']['appointment_date_formated'] . ' has been cancelled.';
			foreach ($email_users as $email) {
				$this->App->send_email($email, $subject, $message, $template = 'default', $layout = 'default');
			}
			$this->Session->setFlash(__('Appointment cancelled'));
			$this->redirect(array('action' => 'view', $id));
		}
	}

	/**
	 * search appointment
	 */
	function search($id = 0) {
		if ($id != 0) {
			$conditons = array(
				'Appointment.employee_detail_id' => $id
			);

			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
		}

		if ($this->request->is('post')) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Appointment']['q'];
			$fromDate = isset($this->request->params['named']['fromDate']) ? $this->request->params['named']['fromDate'] : $this->data['Appointment']['fromDate'];
			$toDate = isset($this->request->params['named']['toDate']) ? $this->request->params['named']['toDate'] : $this->data['Appointment']['toDate'];
			//$data = find('all',array('conditions')=>$cond);
			$conditons = array('OR' => array(
					'EmployeeDetail.first_name LIKE' => '%' . $keyword . '%',
					'EmployeeDetail.last_name LIKE' => '%' . $keyword . '%',
					'PatientDetail.first_name LIKE' => '%' . $keyword . '%',
					'PatientDetail.last_name LIKE' => '%' . $keyword . '%'
					));

			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);

			if ($fromDate != "") {
				$conditons = array('date(Appointment.created) >=' => $fromDate);
				$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			}
			if ($toDate != "") {
				$conditons = array('date(Appointment.created) <=' => $toDate);
				$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			}
			$this->request->params['named']['q'] = $keyword;
		}

		$appointments = $this->paginate();

		/* pr($appointments);
		  exit; */
		$data = array();
		foreach ($appointments as $key => $appointment) {
			$obj_appointment = new DtAppointment($appointment['Appointment']);
			$obj_appointment->add_employee_detail($appointment['EmployeeDetail']);
			$obj_appointment->add_patient_detail($appointment['PatientDetail']);
			$obj_appointment->add_division($appointment['Division']);

			$data[$key]['Appointment'] = $obj_appointment->get_field();
			$data[$key]['EmployeeDetail'] = $obj_appointment->get_employee_details();
			$data[$key]['PatientDetail'] = $obj_appointment->get_patient_details();
			$data[$key]['Division'] = $obj_appointment->get_divisions();
		}

		$this->set('appointments', $data);
	}

	public function getCallLogs($appointment_id = 0) {

		$page = $this->pageForPagination('CallLog');
		$this->paginate['CallLog'] = array('fields' => array('CallLog.*'),
			'limit' => 1,
			'order' => array(
				'CallLog.id' => 'desc'
			),
			'page' => $page,
			'conditions' => array('CallLog.appointment_id' => $appointment_id));

		$this->Paginator->settings = $this->paginate['CallLog'];

		// similar to findAll(), but fetches paged results
		$data = $this->Paginator->paginate('CallLog');
		//$data = $this->paginate('CallLog');

		if (!empty($data)) {
			foreach ($data as $key => $calllog) {
				$data[$key] = $calllog;
				$emp = $this->EmployeeDetail->find('first', array(
					'conditions' => array('EmployeeDetail.id' => $calllog['CallLog']['caller_id'])
						));

				if (!empty($emp)) {
					$data[$key]['EmployeeDetail'] = $emp['EmployeeDetail'];
				} else {
					$data[$key]['EmployeeDetail'] = array();
				}
			}
		}

		return $data;
	}

	public function pageForPagination($model) {
		$page = 1;
		$sameModel = isset($this->params['named']['model']) && $this->params['named']['model'] == $model;
		$pageInUrl = isset($this->params['named']['page']);
		if ($sameModel && $pageInUrl) {
			$page = $this->params['named']['page'];
		}

		$this->passedArgs['page'] = $page;

		return $page;
	}

	public function PatientAttachmentReports($appointment_id = null, $patient_id = null) {
		if (!$this->Appointment->exists($appointment_id)) {
			throw new NotFoundException(__('Invalid appointment id'));
		}
		if (!$this->PatientDetail->exists($patient_id)) {
			throw new NotFoundException(__('Invalid patient id'));
		}

		$page = $this->pageForPagination('PatientAttachmentReport');
		//$result = $this->PatientAttachmentReport->query("call `get_patient_attachment_reports`(".$appointment_id.", ".$patient_id.", 0, 3)");
		$this->paginate['PatientAttachmentReport'] = array('fields' => array('PatientAttachmentReport.*'),
			'limit' => 1,
			'order' => array(
				'PatientAttachmentReport.id' => 'desc'
			),
			'page' => $page,
			'conditions' => array('PatientAttachmentReport.appointment_id' => $appointment_id,
				'PatientAttachmentReport.patient_id' => $patient_id,
				'PatientAttachmentReport.attachment_file !=' => null)
		);
		$this->Paginator->settings = $this->paginate['PatientAttachmentReport'];

		// similar to findAll(), but fetches paged results
		$result = $this->Paginator->paginate('PatientAttachmentReport');

		//pr($result);
		/* $this->paginate['PatientAttachmentReport'] = array('fields'=>array('PatientAttachmentReport.*'), 
		  'limit'=>1,
		  'order' => array(
		  'PatientAttachmentReport.id' => 'desc'
		  ),
		  'page' => $page,
		  'conditions' => array('PatientAttachmentReport.appointment_id' => $appointment_id)
		  );

		  $result = $this->paginate('PatientAttachmentReport'); */

		return $result;
	}

	public function medication_history($appointment_id = null) {
		if (!$this->Appointment->exists()) {
			throw new NotFoundException(__('Invalid appointment id'));
		}

		$page = $this->pageForPagination('PatientMedication');
		$this->paginate['PatientMedication'] = array(
			'order' => array('PatientMedication.id' => 'desc'),
			'limit' => 1,
			'conditions' => array('PatientMedication.appointment_id' => $appointment_id),
			'page' => $page
		);
		$this->Paginator->settings = $this->paginate['PatientMedication'];

		// similar to findAll(), but fetches paged results
		$result = $this->Paginator->paginate('PatientMedication');
		//pr($this->paginate);
		//$result = $this->paginate('PatientMedication');
		//pr($result);
		$data = array();
		foreach ($result as $key => $patientDetail) {
			$obj_user = new DtUser($patientDetail['User']);
			$obj_user->add_patient_detail($patientDetail['PatientDetail']);
			$obj_user->add_employee_detail($patientDetail['EmployeeDetail']);
			/* Retrieve data from rendering logic */
			$data[$key]['User'] = $obj_user->get_field();
			$data[$key]['PatientDetail'] = $obj_user->PatientDetail->get_field();

			if (!empty($result[$key]['EmployeeDetail'])) {
				$data[$key]['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
			}

			if (!empty($result[$key]['PatientMedication'])) {
				$obj_user->PatientDetail->add_medication($patientDetail['PatientMedication']);
				$data[$key]['PatientMedication'] = $obj_user->PatientDetail->get_medication();
			}
			if (!empty($result[$key]['Appointment'])) {
				$obj_user->PatientDetail->add_appointment($patientDetail['Appointment']);
				$data[$key]['Appointment'] = $obj_user->PatientDetail->get_appointments();
			}
		}
		$this->set('patientMedications', $data);
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists()) {
			throw new NotFoundException(__('Invalid appointment id'));
		}
		$model = isset($this->request->params['named']['model']) ? $this->request->params['named']['model'] : "";

		$this->Appointment->unbindModel(
				array('hasMany' => array('CallLog'))
		);
		$result = $this->Appointment->read(null, $id);
		$result['PatientAttachmentReport'] = $this->PatientAttachmentReports($id, $result['PatientDetail']['id']);

		$result['CallLog'] = $this->getCallLogs($id);

		$obj_appointment = new DtAppointment($result['Appointment']);
		$obj_appointment->add_employee_detail($result['EmployeeDetail']);
		$obj_appointment->add_patient_detail($result['PatientDetail']);
		$obj_appointment->add_division($result['Division']);
		$obj_appointment->add_call_logs($result['CallLog']);
		$obj_appointment->add_patient_attachment_reports($result['PatientAttachmentReport']);

		$data['Appointment'] = $obj_appointment->get_field();
		$data['EmployeeDetail'] = $obj_appointment->get_employee_details();
		$data['PatientDetail'] = $obj_appointment->get_patient_details();
		$data['Division'] = $obj_appointment->get_divisions();
		$data['CallLog'] = $obj_appointment->get_call_logs();
		$data['PatientAttachmentReport'] = $obj_appointment->get_patient_attachment_reports();

		if ($data['Appointment']['is_group'] == 1) {
			$obj_appointment->add_appointment_groups($result['AppointmentGroup']);
			$data['AppointmentGroup'] = $obj_appointment->get_appointment_groups();
			$data_tmp = array();
			foreach ($data['AppointmentGroup'] as $key => $value) {

				$data_tmp[$key] = $value;
				$patient_detail = $this->PatientDetail->find('first', array(
					'conditions' => array('PatientDetail.id' => $value['patient_detail_id'])
						));

				if (!empty($patient_detail)) {
					$data_tmp[$key]['patient_name'] = $patient_detail['PatientDetail']['first_name'] . ' ' . $patient_detail['PatientDetail']['last_name'];
				} else {
					$data_tmp[$key]['patient_name'] = '';
				}

				$employee_detail = $this->EmployeeDetail->find('first', array(
					'conditions' => array('EmployeeDetail.user_id' => $value['employee_detail_id'])
						));

				if (!empty($employee_detail)) {
					$data_tmp[$key]['employee_name'] = $employee_detail['EmployeeDetail']['first_name'] . ' ' . $employee_detail['EmployeeDetail']['last_name'];
				} else {
					$data_tmp[$key]['employee_name'] = '';
				}
			}

			unset($data['AppointmentGroup']);
			$data['AppointmentGroup'] = $data_tmp;
			//pr($data);
		}

		if (!empty($data['PatientDetail'])) {
			$user = $this->User->find('first', array(
				'conditions' => array('User.id' => $data['PatientDetail']['user_id'])
					));

			if (!empty($user)) {
				$data['PatientDetail']['username'] = $user['User']['username'];
			} else {
				$data['PatientDetail']['username'] = '';
			}
		}


		if (!empty($data['EmployeeDetail'])) {
			$user = $this->User->find('first', array(
				'conditions' => array('User.id' => $data['EmployeeDetail']['user_id'])
					));

			if (!empty($user)) {
				$data['EmployeeDetail']['username'] = $user['User']['username'];
			} else {
				$data['EmployeeDetail']['username'] = '';
			}
		}

		$this->medication_history($id);

		$this->set('id', $id);
		$this->set('Appointment', $data);

		if ($this->request->is("ajax")) {
			if ($model == "CallLog") {
				$this->layout = 'ajax';
				$this->render("/Elements/call_logs_ajax_listing");
			} elseif ($model == "PatientMedication") {
				$this->layout = 'ajax';
				$this->render("/Elements/ajax_medication_history");
			} elseif ($model == "PatientAttachmentReport") {
				$this->layout = 'ajax';
				$this->render("/Elements/attachments_reports_ajax_listing");
			}
		}
	}

	function download($fileId) {
		if (!$this->PatientAttachmentReport->exists($fileId)) {
			throw new NotFoundException(__('Invalid attachment id'));
		}
		$fileData = $this->PatientAttachmentReport->find('first', array(
			'conditions' => array('PatientAttachmentReport.id' => $fileId)
				));
		//$file = $this->PatientAttachmentReport->read($fileId);
		$file = $fileData['PatientAttachmentReport'];

		$pathInfo = pathinfo($file['attachment_file']);
		//pr($pathInfo);
		$this->viewClass = 'Media';
		$params = array(
			'id' => $file['attachment_file'],
			'name' => $pathInfo['filename'],
			'extension' => $pathInfo['extension'],
			'download' => true,
			'path' => 'patient_attachments' . DS
		);
		$this->set($params);
	}

	function save_attachment_report() {
		if ($this->request->is('post')) {
			//pr($this->request->form);
			//pr($this->data);
			$data['created'] = $this->App->get_current_datetime();
			$data['attachment_title'] = $this->data['attachment_title'];
			$data['appointment_id'] = $this->data['appointment_id'];
			$data['patient_id'] = $this->data['patient_id'];
			$data['attachment_title'] = $this->data['attachment_title'];
			$attachment_file = $this->request->form['attachment_file'];

			if (!empty($attachment_file)) {
				if (move_uploaded_file($attachment_file['tmp_name'], 'patient_attachments/' . $attachment_file['name'])) {
					$data['attachment_file'] = $attachment_file['name'];
					$this->PatientAttachmentReport->save($data);

					$this->Session->setFlash(__('Attachment Added'));
					$this->redirect(array('action' => 'view', $data['appointment_id']));
				} else {
					$this->Session->setFlash(__('Error uploading file'));
					$this->redirect(array('action' => 'view', $data['appointment_id']));
				}
			}
		}
	}

	function create_group() {

		if ($this->request->is('post')) {
			$this->loadModel('AppointmentGroup');
			$response = $this->Appointment->find('first', array(
				'conditions' => array('Appointment.id' => $this->data['Appointment']['id'])
					));

			$data = array();
			$data['AppointmentGroup']['appointment_id'] = $this->data['Appointment']['id'];
			$data['AppointmentGroup']['patient_detail_id'] = $this->data['PatientDetail']['id'];
			$data['AppointmentGroup']['employee_detail_id'] = $response['EmployeeDetail']['user_id'];
			$data['AppointmentGroup']['created'] = $this->App->get_current_datetime();
			$data['AppointmentGroup']['modified'] = $this->App->get_current_datetime();

			$this->AppointmentGroup->save($data);
			$this->Session->setFlash("Session joined..");
			$this->redirect(array('action' => 'create_group'));
		}

		$waiting_list_titles = array();
		$patient_details = array();
		$all_patients = array();

		$response = $this->App->decoding_json($this->ApiAppointment->get_all());
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
			$waiting_list_titles = $this->App->format_drop_down($data);
		}

		$first_waiting_list_title_id = array_shift(array_keys($waiting_list_titles));

		$response = $this->App->decoding_json($this->ApiAppointment->get_by_waiting_lists_title_id($first_waiting_list_title_id));
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$patient_details = $response['body'];
		}

		$response = $this->App->decoding_json($this->ApiAppointment->get_all_patients());
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$all_patients = $response['body'];
		}

		$response = $this->Appointment->find('all', array(
			'conditions' => array('Appointment.is_group' => 1, 'Appointment.appointment_date >' => date('Y-m-d H:i:s'))
				));

		$group_appointments = array();
		foreach ($response as $key => $appointment) {
			$obj_appointment = new DtAppointment($appointment['Appointment']);
			$obj_appointment->add_employee_detail($appointment['EmployeeDetail']);
			$obj_appointment->add_patient_detail($appointment['PatientDetail']);
			$obj_appointment->add_division($appointment['Division']);

			$group_appointments[$key]['Appointment'] = $obj_appointment->get_field();
			$group_appointments[$key]['EmployeeDetail'] = $obj_appointment->get_employee_details();
			$group_appointments[$key]['PatientDetail'] = $obj_appointment->get_patient_details();
			$group_appointments[$key]['Division'] = $obj_appointment->get_divisions();
		}

		$this->set(compact('waiting_list_titles', 'patient_details', 'group_appointments', 'all_patients'));
	}

	/**
	 * Create appointment
	 */
	function create($patientId = 0, $employeeId = 0) {

		if ($this->request->is('post')) {

			$slot_data = $this->App->decoding_json($this->data['Appointment']['slots_data_json']);
			if (isset($this->data['Appointment']['waiting_list_title'])) {
				$this->PatientWaitingList->unbindModelAll();
				$pat_wtg_list = $this->PatientWaitingList->find('first', array(
					'conditions' => array(
						'patient_detail_id' => $this->data['PatientDetail']['id'],
						'waiting_list_title_id' => $this->data['Appointment']['waiting_list_title']
					)
						));

				$pt_wtg_list_id = $pat_wtg_list['PatientWaitingList']['id'];
			} else {
				$pt_wtg_list_id = 0;
			}

			if (!empty($slot_data)) {
				$i = 0;
				foreach ($slot_data as $key => $value) {
					$data[$key]['Appointment']['patient_waiting_list_id'] = $pt_wtg_list_id;
					$data[$key]['Appointment']['employee_working_hour_breakup_id'] = $value['emp_brkup_id'];
					$data[$key]['Appointment']['employee_detail_id'] = $this->data['EmployeeDetail'][$i]['id'];
					$data[$key]['Appointment']['division_id'] = $value['division_id'];
					$data[$key]['Appointment']['patient_detail_id'] = $this->data['PatientDetail']['id'];
					$data[$key]['Appointment']['appointment_date'] = $value['start'];
					$data[$key]['Appointment']['is_group'] = $this->data['Appointment']['is_group'];
					$data[$key]['Appointment']['created'] = $this->App->get_current_datetime();
					$data[$key]['Appointment']['modified'] = $this->App->get_current_datetime();

					$i++;
				}

				$this->Appointment->custom_saveAll($data);
			}
		}
		$waiting_list_titles = array();
		$patient_details = array();
		$all_patients = array();

		$response = $this->App->decoding_json($this->ApiAppointment->get_all());
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
			$waiting_list_titles = $this->App->format_drop_down($data);
		}

		$first_waiting_list_title_id = array_shift(array_keys($waiting_list_titles));

		$response = $this->App->decoding_json($this->ApiAppointment->get_by_waiting_lists_title_id($first_waiting_list_title_id));
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$patient_details = $response['body'];
		}

		$response = $this->App->decoding_json($this->ApiAppointment->get_all_patients());
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$all_patients = $response['body'];
		}

		if ($employeeId == 0) {
			$response = $this->App->decoding_json($this->ApiEmployeeDetail->get_available_doctors($first_waiting_list_title_id));
			if ($response['header']['code'] == ECODE_SUCCESS) {
				$available_doctors = $response['body'];
			}
		} else {
			//$this->EmployeeDetail->unbindModelAll();
			$employeeDetail = $this->EmployeeDetail->find('first', array(
				'conditions' => array(
					'EmployeeDetail.user_id' => $employeeId
				)
					));

			$obj_appointment = new DtEmployeeDetail($employeeDetail['EmployeeDetail']);

			$data = $obj_appointment->get_field();

			$this->set('employeeDetail', $data);
		}

		if ($patientId != 0) {
			$this->PatientDetail->unbindModelAll();
			$patientDetail = $this->PatientDetail->find('first', array(
				'conditions' => array(
					'PatientDetail.id' => $patientId
				)
					));

			$obj_appointment = new DtPatientDetail($patientDetail['PatientDetail']);

			$data = $obj_appointment->get_field();

			$this->set('patientDetail', $data);
		}

		$allowed_service_groups = $this->AllowedServiceGroup->find('list');

		$this->set('allowed_service_groups', $allowed_service_groups);
		$this->set('patientId', $patientId);
		$this->set('employeeId', $employeeId);
		$this->set(compact('waiting_list_titles', 'patient_details', 'available_doctors', 'all_patients'));
	}

	/**
	 * Get patient by waiting list title id
	 * 	 
	 */
	function get_patient_by_waiting_list_title_id() {

		if ($this->request->is('get')) {
			$waiting_list_title_id = $this->request->query['waiting_list_title_id'];
			$response = $this->App->decoding_json($this->ApiAppointment->get_by_waiting_lists_title_id($waiting_list_title_id));
			if ($response['header']['code'] == ECODE_SUCCESS) {
				$data = $response['body'];
			}
			$this->set(compact('data'));
			$this->render('/Elements/appointment_patient_listing');
		}
	}

	/**
	 * Get available doctors
	 * 	 
	 */
	function get_available_doctors() {

		if ($this->request->is('get')) {
			$waiting_list_title_id = $this->request->query['waiting_list_title_id'];
			$response = $this->App->decoding_json($this->ApiEmployeeDetail->get_available_doctors($waiting_list_title_id));
			if ($response['header']['code'] == ECODE_SUCCESS) {
				$available_doctors = $response['body'];
			}

			$this->set(compact('available_doctors'));
			$this->render('/Elements/appointment_doctor_listing');
		}
	}

	function get_available_doctors_by_service_group() {

		if ($this->request->is('get')) {
			$service_group_id = $this->request->query['service_group_id'];
			$available_doctors = $this->Appointment->get_available_doctors_by_service_group_id($service_group_id);
			foreach ($available_doctors as $key => $available_doctor) {
				$formatted_keys[$key]['id'] = $available_doctor['AvailableDoctor']['EmployeeId'];
				$formatted_keys[$key]['first_name'] = $available_doctor['AvailableDoctor']['EmployeeFName'];
				$formatted_keys[$key]['user_id'] = $available_doctor['AvailableDoctor']['UserId'];
			}

			foreach ($formatted_keys as $key => $value) {
				$obj_employee = new DtEmployeeDetail($value);
				$data[$key]['id'] = $obj_employee->id;
				$data[$key]['first_name'] = $obj_employee->first_name;
				$data[$key]['user_id'] = $obj_employee->user_id;
			}

			$this->set('available_doctors', $data);
			$this->render('/Elements/appointment_doctor_listing');
		}
	}

	/**
	 * Get available doctors
	 * 	 
	 */
	function get_all_patients() {

		if ($this->request->is('get')) {
			$keyword = $this->request->query['keyword'];
			$response = $this->App->decoding_json($this->ApiAppointment->get_all_patients($keyword));
			if ($response['header']['code'] == ECODE_SUCCESS) {
				$data = $response['body'];
			}

			$this->set(compact('data'));
			$this->render('/Elements/appointment_patient_listing');
		}
	}

	/**
	 * Get available working slots for given employee belongs to particular division.
	 * 
	 * @param array $params described above
	 * @return json $api_response 
	 */
	function get_available_working_slots() {

		if ($this->request->is('get')) {
			$response = $this->App->decoding_json($this->ApiEmployeeDetail->get_available_working_slots($this->request->query));

			$this->set(compact('response'));
			$this->render('/Elements/available_working_slots');
		}
	}

	function get_all_future_appointments() {

		$appointments = $this->ApiAppointment->get_all_future_appointsment();

		$this->set(compact('appointments'));
	}

	function search_future_appointment_of_doctor() {
		
		$this->render('get_all_future_appointments');

		if (!empty($this->data['Appointment']) && $this->request->is('post')) {
			
			
			$search_keyword = $this->data['Appointment']['search'];

			$search_result = $this->ApiAppointment->search_future_appointment_of_doctor( $search_keyword );
			
			$this->set(compact('search_result'));
			
			$this->render('get_all_future_appointments');
			
		}
	}

}

?>
