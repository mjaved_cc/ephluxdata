<?php

class AccessComponent extends Component {

	public $components = array('Session', 'Acl', 'Auth');
	private $_controller;
	private $_collection;
	private $_user;

	function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_collection = $collection;
	}

	function startup(Controller $controller) {

		$this->_controller = $controller;
		$this->_user = $this->Auth->User();
	}

	function beforeRender(Controller $controller) {
		parent::beforeRender($controller);
		$this->_controller->helpers['Access']['component_collection'] = $this->_collection;
	}

	/**
	 * Wrapper - for Acl check cakephp.
	 * 
	 * Main ACL check function. Checks to see if the ARO (access request object) has access to the ACO (access control object).
	 * 
	 * @param string $aco ControllerName/Action - ConfigOptions/index
	 * @return boolean
	 */
	function check($aco) {
		$user = $this->Auth->User();
		if (!empty($user) && $user['group_id'] == GROUP_ID_SUPER_ADMIN)
			return true;

		$aro = ARO_ALIAS . $this->Auth->User('group_id');

		return $this->Acl->check($aro, $aco);
	}

	/**
	 * Wrapper - for View.
	 * 
	 * Main ACL check function. Checks to see if the ARO (access request object) has access to the ACO (access control object).
	 * 
	 * Example controller name is basically prefix of Controller class. If class name is AuthMapComponent then name will be AuthMap
	 * 
	 * @param string $controller The name of this controller. Controller names are plural, named after the model they manipulate.
	 * @param string $action action
	 * @return boolean
	 */
	function check_helper($controller, $action = "*") {

		$aco = $controller . '/' . $action;
		return $this->check($aco);
	}

}

?>