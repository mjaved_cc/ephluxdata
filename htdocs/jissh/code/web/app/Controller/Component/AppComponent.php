<?php

App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Xml', 'Utility');
App::uses('CakeTime', 'Utility');

class AppComponent extends Component {

	/**
	 * encodes json
	 * @param array $data 
	 * @return string json
	 */
	function encoding_json($data) {

		return json_encode($data);
	}

	/**
	 * decodes json
	 * @param array $data 
	 * @return string json
	 */
	function decoding_json($data) {

		return json_decode($data, true);
	}

	/**
	 * get json that could be saved in db
	 * @param: $data
	 * @return json
	 */
	function get_db_save_json($data) {
		return mysql_real_escape_string(json_encode($data));
	}

	/**
	 * check for valid format of email
	 */
	function is_valid_email($email = null) {
		if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
			return true;
		return false;
	}

	function get_current_datetime() {
		return $this->format_as_sql();
	}

	/**
	 * get expiry date for expired_at field of Tmp table
	 */
	function get_tmp_expiry() {
		return CakeTime::format('Y-m-d', strtotime("+3 month"));
	}

	/**
	 * Send an email using the specified content, template and layout.
	 * 
	 * @param string $to email
	 * @param string $subject subject
	 * @param string $message message
	 * @param string $template template
	 * @param string $layout layout
	 * 
	 * @uses CakeEmail
	 */
	function send_email($to, $subject, $message, $template = 'default', $layout = 'default') {

		if (SEND_EMAIL_WITH_AUTH) {
			$email = new CakeEmail('gmail');
			$email->to($to);
			$email->subject($subject);
//			$email->template($template, $layout);
//			$email->emailFormat('html');
//			$email->viewVars(array('message' => $message));
//			$email->send();
		} else
			$email = CakeEmail::deliver($to, $subject, $message, array('from' => array(EMAIL_NOREPLY => SITE_NAME)), false);

		$email->template($template, $layout);
		$email->emailFormat('html');
		$email->viewVars(array('message' => $message));
		return $email->send();
	}

	/*	 * ***** How to save IP Address in DB ****** */
	/* INET_NTOA and INET_ATON functions in mysql. They convert between dotted notation IP address to 32 bit integers. This allows			you to store the IP in just 4 bytes rather than a 15 bytes 
	 */

	/**
	 * Get an (IPv4) Internet network address into a string in Internet standard dotted format.
	 * @param string $number numeric-ip-representation
	 * @return string ip-address-with-dotted-format
	 */
	function get_ip_address($number) {
		// analogous to INET_NTOA in mysql
		return sprintf("%s", long2ip($number));
	}

	/**
	 * Get string containing an (IPv4) Internet Protocol dotted address into a proper address 
	 * 
	 * @return int numeric-ip-representation 
	 * 
	 */
	function get_numeric_ip_representation() {
		// analogous to INET_ATON in mysql 
		return sprintf("%u", ip2long(getenv('REMOTE_ADDR')));
	}

	/*	 * ***** END: How to save IP Address in DB ***** */

	/**
	 * Return a well-formed XML string for given array 
	 * 
	 * @param array $data
	 * @return XML
	 */
	function get_xml($data) {

		if (!empty($data)) {
			$response[XML_ROOT_TAG] = $data;
		} else {
			$response = array(
				XML_ROOT_TAG => array(
					'code' => '400',
					'message' => 'Bad request'
				)
			);
		}

		$xml = Xml::fromArray($response);
		return $xml->asXML();
	}

	/**
	 * Generate random password
	 * 
	 * @return string password
	 */
	function random_password() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	/**
	 * Return as mysql datetime format
	 * 
	 * @param string $datetime Datetime
	 * @return string Datetime
	 */
	function format_as_sql($datetime = 'now') {
		return CakeTime::format('Y-m-d H:i:s', $datetime);
	}

	/**
	 * Return week day
	 * 
	 * @param string $datetime Datetime
	 * @return string Datetime
	 */
	function get_week_day($datetime = 'now') {
		return strtolower(CakeTime::format('l', $datetime));
	}

	/**
	 * Return week day
	 * 
	 * @param string $datetime Datetime
	 * @return string Datetime
	 */
	function get_unix_timestamp($datetime = 'now') {
		return CakeTime::toUnix($datetime);
	}

	/**
	 * Get next affected time with respect to duration.
	 * 
	 * @param string $datetime Datetime
	 * @param string $duration digit in minutes for e.g.; 30 means 30 min
	 */
	function get_next_time_by_duration($datetime, $duration) {
		return strtotime('+' . $duration . ' min', $this->get_unix_timestamp($datetime));
	}

	/**
	 * Return all the keys or a subset of the keys of an array.
	 * 
	 * Wrapper for PHP function array_keys(). 
	 */
	function get_array_keys($data) {
		if (is_array($data))
			return array_keys($data);
		return false;
	}

	/**
	 * Format result set with respect to drop down.
	 * 
	 * $data should be of the form
	 * 
	 * <code>
	 * <code>
	 * Array
	  (
	  [0] => Array
	  (
	  [id] => value1,
	  [name] => value2
	  ),
	  ....
	  [N] => Array
	  (
	  [id] => value1,
	  [name] => value2
	  )
	  )
	 * </code>
	 * 
	 * @param array $data
	 * @return array
	 */
	function format_drop_down($data) {
		$return_data = array();
		
		if (!empty($data)) {
			foreach ($data as $value) {				
				$return_data[$value['id']] = $value['name'];
			}
		}

		return $return_data;
	}

}

?>