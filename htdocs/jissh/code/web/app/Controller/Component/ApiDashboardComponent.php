<?php

class ApiDashboardComponent extends Component {

	const BASE_CODE = BASE_CODE_API_DASHBOARD_COMPONENT;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {
		$this->_controller = $controller;
	}

	/**
	 * Get all waiting lists titles
	 * 
	 * Error code : 1 - 30
	 * @return json
	 */
	function get_all() {
		$results = $this->_controller->WaitingListsTitle->get_all();
		$data = array();

		if (!empty($results)) {

			foreach ($results as $key => $value) {

				$obj_wlt = new DtWaitingListsTitle($value['WaitingListsTitle']);

				$data[$key]['id'] = $obj_wlt->id;
				$data[$key]['name'] = $obj_wlt->name;
			}

			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "records found";
			$this->ApiResponse->body = $data;
		} else {
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "No record found";
			unset($this->ApiResponse->body);
		}

		return $this->ApiResponse->get();
	}

	/**
	 * Get all waiting lists titles
	 * 
	 * Error code : 31 - 60
	 * @return json
	 */
	function get_by_waiting_lists_title_id($waiting_list_title_id) {

		if (!empty($waiting_list_title_id)) {
			$results = $this->_controller->PatientDetail->get_by_waiting_lists_title_id($waiting_list_title_id);
			$data = array();

			if (!empty($results)) {

				foreach ($results as $key => $value) {

					$obj_appointment = new DtPatientDetail($value['PatientDetail']);

					$data[$key]['id'] = $obj_appointment->id;
					$data[$key]['name'] = $obj_appointment->name;
				}
				// since format_drop_down already implements data rendering logic so we are ignoring here 
				// $data = $this->_controller->PatientDetail->format_drop_down($results);
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "records found";
				$this->ApiResponse->body = $data;
			} else {
				$this->ApiResponse->offset = '31';
				$this->ApiResponse->msg = "No record found";
				unset($this->ApiResponse->body);
			}
		} else {
			$this->ApiResponse->offset = '32';
			$this->ApiResponse->msg = "invalid waiting list title";
			unset($this->ApiResponse->body);
		}

		return $this->ApiResponse->get();
	}

	/**
	 * Get all active patients
	 * 
	 * Error code : 61 - 90
	 * @param string $keyword Search patient
	 * @return json
	 */
	function get_all_patients($keyword = '') {

		$results = $this->_controller->PatientDetail->get_all($keyword);
		$data = array();

		if (!empty($results)) {

			foreach ($results as $key => $value) {

				$obj_appointment = new DtPatientDetail($value['PatientDetail']);

				$data[$key]['id'] = $obj_appointment->id;
				$data[$key]['name'] = $obj_appointment->first_name;
			}

			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "records found";
			$this->ApiResponse->body = $data;
		} else {
			$this->ApiResponse->offset = '61';
			$this->ApiResponse->msg = "No record found";
			unset($this->ApiResponse->body);
		}


		return $this->ApiResponse->get();
	}

	function get_appointments_for_dashboard() {

		$user = $this->_controller->Auth->user();

		$param['user_type'] = $user['user_type']; //'doctor';
		$param['user_id'] = $user['id']; //'42';


		$appointment = $this->_controller->Appointment->get_appointment_for_dashboard($param);
		//pr($appointment);exit;
		$data = array();
		foreach ($appointment as $key => $app) {

			$obj_appointment = new DtAppointment($app['Appointment']);
			$obj_appointment->add_employee_detail($app['EmpDetail']);
			$obj_appointment->add_patient_detail($app['PatDetail']);

			$data[$key]['Appointment']['id'] = $obj_appointment->id;
			$data[$key]['Appointment']['employee_detail_id'] = $obj_appointment->employee_detail_id;
			$data[$key]['Appointment']['patient_detail_id'] = $obj_appointment->patient_detail_id;
			$data[$key]['Appointment']['appointment_date'] = $obj_appointment->appointment_date;
			$data[$key]['Appointment']['status'] = $obj_appointment->status;


			$data[$key]['EmpDetail']['user_id'] = $obj_appointment->EmployeeDetail->user_id;
			$data[$key]['EmpDetail']['first_name'] = $obj_appointment->EmployeeDetail->first_name;
			$data[$key]['EmpDetail']['last_name'] = $obj_appointment->EmployeeDetail->last_name;


			$data[$key]['PatDetail']['first_name'] = $obj_appointment->PatientDetail->first_name;
			$data[$key]['PatDetail']['last_name'] = $obj_appointment->PatientDetail->last_name;
		}

		$this->ApiResponse->base = ApiAppointmentComponent::BASE_CODE;
		$this->ApiResponse->offset = '0';
		$this->ApiResponse->msg = "Appointments for dashboard";
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		$this->ApiResponse->body = $data;
		return $this->ApiResponse->get();
	}

	function get_messages_for_dashboard() {

		$id = AuthComponent::user('id');

		$messages = $this->_controller->Appointment->get_messages_for_dashboard($id);

		$data = array();

		foreach ($messages as $key => $mess) {


			$obj_conversation = new DtConversation($mess['Conversation']);
			$obj_conversation->add_conversation_meassage($mess['ConversationMessage']);

			$data[$key]['Conversation'] = $obj_conversation->get_field();
			$data[$key]['ConversationMessage'] = $obj_conversation->get_conversation_details();
		}

		$this->ApiResponse->base = ApiAppointmentComponent::BASE_CODE;
		$this->ApiResponse->offset = '0';
		$this->ApiResponse->msg = "Messages for dashboard";
		$this->ApiResponse->format = ApiResponseComponent::JSON;
		$this->ApiResponse->body = $data;
		return $this->ApiResponse->get();
	}

	function get_patient_related_to_doctor() {
		
		$id = AuthComponent::user('id');
		$patient_detail = $this->_controller->PatientDetail->get_patient_related_to_doctor($id); 

		if (!empty($patient_detail)) {

			foreach ($patient_detail as $key => $value) {

				$obj_patient_detail = new DtPatientDetail($value['PatDetail']);

				$data[$key]['PatientDetail'] = $obj_patient_detail->get_field();
			}
			
			
			$this->ApiResponse->base = ApiAppointmentComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "records found";
			$this->ApiResponse->body = $data;
			return $this->ApiResponse->get();
		} else {
			$this->ApiResponse->base = ApiAppointmentComponent::BASE_CODE;
			$this->ApiResponse->offset = '31';
			$this->ApiResponse->msg = "No record found";
			return $this->ApiResponse->get();
		}
	}

	function get_dashboard_for_doctor() {

		$messages = $this->get_messages_for_dashboard();
		$appointments = $this->get_appointments_for_dashboard();
		$patients_detail = $this->get_patient_related_to_doctor();
		

		$this->_controller->set(compact('messages'));
		$this->_controller->set(compact('appointments'));
		$this->_controller->set(compact('patients_detail'));
		$this->_controller->render('doctor');
	}

}

?>
