<?php

App::uses('Component', 'Controller');

/**
 * Javascript & Css Manager. 
 */
class JCManagerComponent extends Component {

	private $_controller;
	private $_js_list;
	private $_css_list;

	function startup(Controller $controller) {
		$this->_controller = $controller;

		$this->_js_list = $this->get_default_backend_js();
		$this->_css_list = $this->get_default_backend_css();
	}

	/**
	 * Get all JS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_js() {

		return array(
			/** BASE UI PLUGINS | These plugins are required for basic functionality to work such as tabs, menus etc */
			// Modernizr and style switcher 
			MRIYA_JS_PATH . 'modernizr-latest.min',
			MRIYA_JS_PATH . 'styleswitcher.min',
			// jQuery
			MRIYA_JS_PATH . 'jquery-1.6.4.min',
			// Tabs
			MRIYA_JS_PATH . 'tabs.min',
			// Modal
			MRIYA_JS_PATH . 'overlay.min',
			// Expose
			MRIYA_JS_PATH . 'toolbox.expose',
			// Tooltip
			MRIYA_JS_PATH . 'tooltip.min',
			MRIYA_JS_PATH . 'tooltip.slide.min',
			MRIYA_JS_PATH . 'tooltip.dynamic.min',
			// Validator, HTML5 range and date control
			MRIYA_JS_PATH . 'validator.min',
			MRIYA_JS_PATH . 'rangeinput.min',
			MRIYA_JS_PATH . 'dateinput.min',
			// Super fish Menu
			MRIYA_JS_PATH . 'superfish.min',
			// Data Tables
			MRIYA_JS_PATH . 'jquery.dataTables.min',
			MRIYA_JS_PATH . 'TableTools.min',
			MRIYA_JS_PATH . 'ZeroClipboard.min',
			// Toggle Buttons
			MRIYA_JS_PATH . 'jquery.ezmark.min',
			// CSS & Images Preoloader
			MRIYA_JS_PATH . 'preload.css.images.min',
			/** END : BASE UI PLUGINS | These plugins are required for basic functionality to work such as tabs, menus etc */
			//full calender
			MRIYA_JS_PATH . 'fullcalendar.min',
			MRIYA_JS_PATH . 'jquery-ui-1.10.2.custom.min',
			MRIYA_JS_PATH . 'jquery-ui-1.10.3.custom.min',
			// Timepicker - http://trentrichardson.com/examples/timepicker/
			MRIYA_JS_PATH . 'jquery-ui-timepicker-addon.min',
			// dropdown check list - http://dropdown-check-list.googlecode.com/svn/trunk/doc/dropdownchecklist.html
			MRIYA_JS_PATH . 'ui.dropdownchecklist',	
			MRIYA_JS_PATH . 'jquery.ui.sortable',	
			'tinymce/tinymce',	
			// general functions 
			'app',
			// base class for all classes that helps class to create constructor in js
			'class',
			//CONFIG - always include this script after including all js plugins/lib
			MRIYA_PATH . 'script'
		);
	}

	/**
	 * Get all CSS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_css() {

		return array(
			MRIYA_PATH . 'core', 
			MRIYA_PATH . 'light',
			MRIYA_PATH . 'fonts.googleapis', // fonts - http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold
			'custom',
			MRIYA_PATH . 'fullcalendar',			
			// 'http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold', // fonts
			MRIYA_PATH . 'jquery-ui-1.10.3.custom', // jquery UI
			MRIYA_PATH . 'jquery-ui-timepicker-addon', // timepicker css			
			MRIYA_PATH . 'ui.dropdownchecklist.themeroller' // dropdownchecklist
		);
	}

	/**
	 * Push JS file(s) in stack/queue
	 * 
	 * @param string|array $file_names JS file name
	 * @return void
	 */
	function add_js($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			$this->_js_list = array_merge($this->_js_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_js_list[] = $file_names;
		}
	}

	/**
	 * Push CSS file(s) in stack/queue
	 * 
	 * @param string|array $file_names CSS file name
	 * @return void
	 */
	function add_css($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			$this->_css_list = array_merge($this->_css_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_css_list[] = $file_names;
		}
	}

	/**
	 * Remove JS file(s) in stack/queue
	 * 
	 * @param string|array $file_names CSS file name
	 * @return void
	 */
	function remove_js($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			foreach ($file_names as $file_name) {
				foreach (array_keys($this->_js_list, $file_name) as $key) {
					unset($this->_js_list[$key]);
				}
			}
			$this->_css_list = array_merge($this->_css_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			foreach (array_keys($this->_js_list, $file_names) as $key) {
				unset($this->_js_list[$key]);
			}
		}
	}

	/**
	 * Get all required JS & CSS file(s) 
	 * 
	 * @return array
	 */
	function get() {
		return array($this->_js_list, $this->_css_list);
	}

}

?>
