<?php

class ApiEmployeeDetailComponent extends Component {

	const BASE_CODE = BASE_CODE_API_EMPLOYEE_DETAIL_COMPONENT;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
		$this->ApiResponse->base = self::BASE_CODE;
		$this->ApiResponse->format = ApiResponseComponent::JSON;
	}

	/**
	 * Get doctor details by user id. This detail is fetching by joining tables
	 * 
	 * Error code range : 0-30
	 * 
	 * @param string|int $user_id System generated user id
	 * @return json doctor details
	 */
	function get_doctor_details_by_user_id($user_id) {
		$this->_controller->EmployeeDetail->User->id = $user_id;
		if (!$this->_controller->EmployeeDetail->User->exists()) {
			$this->ApiResponse->offset = '1';
			$this->ApiResponse->msg = "Invalid user id";
		} else {

			$user_details = $this->_controller->EmployeeDetail->User->get_doctor_details_by_user_id($user_id);
			
			/* Wrapping - Rendering Logic */
			$obj_user = new DtUser($user_details[0]['User']);
			$obj_user->add_employee_detail($user_details[0]['EmployeeDetail']);

			foreach ($user_details as $value) {
				$obj_user->add_language($value['Language']);
				$obj_user->EmployeeDetail->add_allowed_service($value['AllowedService']);
				$obj_user->EmployeeDetail->add_case_load($value['CaseLoad']);
				$obj_user->EmployeeDetail->add_division($value['Division']);
				$obj_user->EmployeeDetail->add_level($value['Level']);
				$obj_user->EmployeeDetail->add_official_title($value['OfficialTitle']);
				$obj_user->EmployeeDetail->add_license_type($value['LicenseType']);
			}
			/* END: Wrapping - Rendering Logic */
			/* Retrieve data from rendering logic */
			// user
			$data['User'] = $obj_user->get_field();
			// EmployeeDetail			
			$data['EmployeeDetail'] = $obj_user->get_employee_detail();
			// Language
			$data['Language'] = $obj_user->get_languages();
			// AllowedService
			$data['AllowedService'] = $obj_user->EmployeeDetail->get_allowed_services();
			// LicenseType
			$data['LicenseType'] = $obj_user->EmployeeDetail->get_license_type();
			// CaseLoad
			$data['CaseLoad'] = $obj_user->EmployeeDetail->get_case_loads();
			// Division
			$data['Division'] = $obj_user->EmployeeDetail->get_divisions();
			// Level
			$data['Level'] = $obj_user->EmployeeDetail->get_levels();
			// OfficialTitle
			$data['OfficialTitle'] = $obj_user->EmployeeDetail->get_official_titles();
			/* END: Retireving */

			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $data;
		}
		
		return $this->ApiResponse->get();
	}

	/**
	 * Get doctor working hours by user id.
	 * 
	 * Error code range : 31-60
	 * 
	 * @param int|string $user_id System generated User id
	 * @param boolean $editable Determines whether the events on the calendar can be modified.
	 * 
	 * @return json Event object - Full Calender doc
	 */
	function get_doctor_working_hours($user_id, $editable) {
		$this->_controller->EmployeeDetail->User->id = $user_id;
		if (!$this->_controller->EmployeeDetail->User->exists()) {
			$this->ApiResponse->offset = '31';
			$this->ApiResponse->msg = "Invalid user id";
		} else {

			$user_details = $this->_controller->EmployeeDetail->User->get_doctor_working_hours_by_user_id($user_id);

			if (!empty($user_details)) {
				$obj_doctor = new DtEmployeeDetail($user_details[0]);

				$data = array();
				foreach ($user_details as $key => $value) {
					$obj_doctor->add_division($value['Division']);
					$obj_doctor->add_employee_working_hour($value['EmployeeWorkingHour']);
				}

				foreach ($obj_doctor->Division as $key => $obj_division) {

					$data[$key]['id'] = $obj_doctor->get_emp_working_hr_obj_at_row($key)->id;
					$data[$key]['start'] = $obj_doctor->get_emp_working_hr_obj_at_row($key)->start_time;
					$data[$key]['end'] = $obj_doctor->get_emp_working_hr_obj_at_row($key)->end_time;
					$data[$key]['title'] = $obj_division->name;
					$data[$key]['division_id'] = $obj_division->id;
					$data[$key]['three_letter_day'] = $obj_doctor->get_emp_working_hr_obj_at_row($key)->get_three_letter_day_representation();
					$data[$key]['editable'] = ($editable) ? true : false; // since url uses 0s & 1s while passing as param
					$data[$key]['allDay'] = false;
					$data[$key]['cell_background'] = $obj_doctor->get_emp_working_hr_obj_at_row($key)->get_cell_bgcolor_full_calender();
				}

				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $data;
			} else {
				$this->ApiResponse->offset = '32';
				$this->ApiResponse->msg = "No record found";
			}
		}

		return $this->ApiResponse->get();
	}

	/**
	 * Save employee official titles.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 61-90
	 * 
	 * @param array $official_titles official_titles ids 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_official_titles($official_titles, $employee_id) {

		if (!empty($official_titles) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeOfficialTitle->remove_all_common_by_field_name($remove_data);

			foreach ($official_titles as $key => $official_title_id) {
				$data[$key]['EmployeeOfficialTitle']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeOfficialTitle']['official_title_id'] = $official_title_id;
				$data[$key]['EmployeeOfficialTitle']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeOfficialTitle']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeOfficialTitle->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '61';
				$this->ApiResponse->msg = "Unable to save employee official_titles";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '62';
			$this->ApiResponse->msg = "empty set of official titles recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee levels.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 91-120
	 * 
	 * @param array $levels level ids 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_levels($levels, $employee_id) {

		if (!empty($levels) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeLevel->remove_all_common_by_field_name($remove_data);

			foreach ($levels as $key => $level_id) {
				$data[$key]['EmployeeLevel']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeLevel']['level_id'] = $level_id;
				$data[$key]['EmployeeLevel']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeLevel']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeLevel->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '91';
				$this->ApiResponse->msg = "Unable to save employee levels";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '92';
			$this->ApiResponse->msg = "empty set of levels recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee divisions.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 121-150
	 * 
	 * @param array $divisions divisions ids 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_divisions($divisions, $employee_id) {

		if (!empty($divisions) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeDivision->remove_all_common_by_field_name($remove_data);

			foreach ($divisions as $key => $division_id) {
				$data[$key]['EmployeeDivision']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeDivision']['division_id'] = $division_id;
				$data[$key]['EmployeeDivision']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeDivision']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeDivision->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '121';
				$this->ApiResponse->msg = "Unable to save employee divisions";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '122';
			$this->ApiResponse->msg = "empty set of divisions recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee allowed services.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 151-180
	 * 
	 * @param array $allowed_services allowed_services ids 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_allowed_services($allowed_services, $employee_id) {

		if (!empty($allowed_services) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeAllowedService->remove_all_common_by_field_name($remove_data);

			foreach ($allowed_services as $key => $allowed_service_id) {
				$data[$key]['EmployeeAllowedService']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeAllowedService']['allowed_service_id'] = $allowed_service_id;
				$data[$key]['EmployeeAllowedService']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeAllowedService']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeAllowedService->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '151';
				$this->ApiResponse->msg = "Unable to save employee allowed_services";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '152';
			$this->ApiResponse->msg = "empty set of allowed services recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee case loads.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 181-210
	 * 
	 * @param array $case_loads case_loads ids 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_case_loads($case_loads, $employee_id) {

		if (!empty($case_loads) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeCaseLoad->remove_all_common_by_field_name($remove_data);

			foreach ($case_loads as $key => $case_load_id) {
				$data[$key]['EmployeeCaseLoad']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeCaseLoad']['case_load_id'] = $case_load_id;
				$data[$key]['EmployeeCaseLoad']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeCaseLoad']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeCaseLoad->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '181';
				$this->ApiResponse->msg = "Unable to save employee case_loads";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '182';
			$this->ApiResponse->msg = "empty set of case loads recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee working hours.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 211-240
	 * 
	 * @param json $events event (an entry on calendar) 
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_working_hours($events_json, $employee_id) {

		if (!empty($events_json) && !empty($employee_id)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';
			$this->_controller->EmployeeWorkingHour->remove_all_common_by_field_name($remove_data);

			$events = $this->App->decoding_json($events_json);
			$division_ids = array();
			foreach ($events as $key => $value) {
				$data[$key]['EmployeeWorkingHour']['employee_detail_id'] = $employee_id;
				$data[$key]['EmployeeWorkingHour']['division_id'] = $value['division_id'];
				$data[$key]['EmployeeWorkingHour']['working_day'] = $this->App->get_week_day($value['start']);
				$data[$key]['EmployeeWorkingHour']['start_time'] = $this->App->format_as_sql($value['start']);
				$data[$key]['EmployeeWorkingHour']['end_time'] = $this->App->format_as_sql($value['end']);

				list($total_possible_slots, $slots) = $this->generate_slots($data[$key]['EmployeeWorkingHour']['start_time'], $data[$key]['EmployeeWorkingHour']['end_time'], 15);

				$data[$key]['EmployeeWorkingHour']['total_possible_slots'] = $total_possible_slots;
				$data[$key]['EmployeeWorkingHour']['created'] = $this->App->get_current_datetime();
				$data[$key]['EmployeeWorkingHour']['modified'] = $this->App->get_current_datetime();

				$division_ids[$value['division_id']] = $employee_id;
			}

			if ($this->_controller->EmployeeWorkingHour->custom_saveAll($data)) {

				$response = $this->App->decoding_json($this->save_employee_working_per_week_slots($division_ids, $employee_id));
				if ($response['header']['code'] == ECODE_SUCCESS) {

					$response = $this->App->decoding_json($this->save_doctor_working_hour_breakups($employee_id));
					if ($response['header']['code'] == ECODE_SUCCESS) {
						$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
						$this->ApiResponse->offset = '0';
						$this->ApiResponse->msg = "Saved";
					} else {
						$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
						$this->ApiResponse->offset = '214';
						$this->ApiResponse->msg = $response['header']['message'];
					}
				} else {
					$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
					$this->ApiResponse->offset = '213';
					$this->ApiResponse->msg = $response['header']['message'];
				}
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '211';
				$this->ApiResponse->msg = "Unable to save employee working hours";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '212';
			$this->ApiResponse->msg = "empty set of working hours recieved and/or employee id invalid";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Save employee working slots weekly basis.
	 * 
	 * This function also updates user record by 1st removing & then adding records.
	 * 
	 * Records are temporary deleted i.e updating by making is_active = 0
	 * 
	 * Error code range : 241-270
	 * 
	 * @param array $division_ids division ids
	 * @param string|int $employee_id employee detail id
	 * @return json $api_response
	 */
	function save_employee_working_per_week_slots($division_ids, $employee_id) {
		if (!empty($division_ids)) {
			$remove_data['field_value'] = $employee_id;
			$remove_data['modified'] = $this->App->get_current_datetime();
			$remove_data['field_name'] = 'employee_detail_id';

			$this->_controller->EmployeeWorkingPerWeekSlot->remove_all_common_by_field_name($remove_data);

			foreach ($division_ids as $division_id => $employee_detail_id) {
				$data[$division_id]['EmployeeWorkingPerWeekSlot']['employee_detail_id'] = $employee_detail_id;
				$data[$division_id]['EmployeeWorkingPerWeekSlot']['division_id'] = $division_id;

				$data[$division_id]['EmployeeWorkingPerWeekSlot']['total_possible_slots'] = $this->_controller->EmployeeWorkingHour->get_per_week_slots($employee_detail_id, $division_id);
				;
				$data[$division_id]['EmployeeWorkingPerWeekSlot']['created'] = $this->App->get_current_datetime();
				$data[$division_id]['EmployeeWorkingPerWeekSlot']['modified'] = $this->App->get_current_datetime();
			}

			if ($this->_controller->EmployeeWorkingPerWeekSlot->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '242';
				$this->ApiResponse->msg = "Unable to save employee working slots per week";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '241';
			$this->ApiResponse->msg = "empty set of working hours recieved";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Get doctor annual vacation plans & appointments for given user id
	 * 
	 * Error code range : 271-300
	 * 
	 * @param int|string $user_id System generated User id	 
	 * 
	 * @return json Event object - Full Calender doc
	 */
	function get_doctor_avp_and_appointments($user_id) {
		$this->_controller->EmployeeDetail->User->id = $user_id;
		if (!$this->_controller->EmployeeDetail->User->exists()) {
			$this->ApiResponse->offset = '271';
			$this->ApiResponse->msg = "Invalid user id";
		} else {
			// avp & appointments details
			$avp_app_details = $this->_controller->EmployeeDetail->User->get_doctor_avp_and_appointments_by_user_id($user_id);
			
			if (!empty($avp_app_details)) {

				$obj_doctor = new DtEmployeeDetail($avp_app_details[0]);

				$avp = array();
				$appointments = array();
				foreach ($avp_app_details as $key => $value) {
					$obj_doctor->add_division($value['Division']);
					$obj_doctor->add_annual_vacation_plan($value['AnnualVacationPlan']);
					$obj_doctor->add_appointment($value['Appointment']);

					if ($obj_doctor->get_annual_vacation_plan_obj_at_row($value['AnnualVacationPlan']['id'])) {
						$avp[$value['AnnualVacationPlan']['id']]['id'] = $obj_doctor->get_annual_vacation_plan_obj_at_row($value['AnnualVacationPlan']['id'])->id;
						$avp[$value['AnnualVacationPlan']['id']]['start'] = $obj_doctor->get_annual_vacation_plan_obj_at_row($value['AnnualVacationPlan']['id'])->last_work_day;
						$avp[$value['AnnualVacationPlan']['id']]['end'] = $obj_doctor->get_annual_vacation_plan_obj_at_row($value['AnnualVacationPlan']['id'])->report_to_work;
						$avp[$value['AnnualVacationPlan']['id']]['title'] = $obj_doctor->get_annual_vacation_plan_obj_at_row($value['AnnualVacationPlan']['id'])->status;
						$avp[$value['AnnualVacationPlan']['id']]['backgroundColor'] = COLOR_CODE_LEAVES;
						$avp[$value['AnnualVacationPlan']['id']]['allDay'] = false;
					}

					if ($obj_doctor->get_appointment_obj_at_row($value['Appointment']['id'])) {
						$appointments[$value['Appointment']['id']]['id'] = $obj_doctor->get_appointment_obj_at_row($value['Appointment']['id'])->id;
						$appointments[$value['Appointment']['id']]['start'] = $obj_doctor->get_appointment_obj_at_row($value['Appointment']['id'])->appointment_date;
						$appointments[$value['Appointment']['id']]['title'] = $obj_doctor->get_division_obj_at_row($key)->name;
						$appointments[$value['Appointment']['id']]['division_id'] = $obj_doctor->get_division_obj_at_row($key)->id;
						$appointments[$value['Appointment']['id']]['backgroundColor'] = COLOR_CODE_APPOINTMENT;
						$appointments[$value['Appointment']['id']]['allDay'] = false;
					}
				}
				$data = array_merge($appointments, $avp);
				
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $data;
			} else {
				$this->ApiResponse->offset = '272';
				$this->ApiResponse->msg = "No record found";
			}
		}

		return $this->ApiResponse->get();
	}

	/**
	 * Get available doctors by no of weeks & waiting list title it
	 * 
	 * Error code range : 301-330
	 * 
	 * @param string|int $waiting_list_title_id Waiting list title id
	 * @param string|int $no_of_weeks No of weeks	
	 * @return json $api_response 
	 */
	function get_available_doctors($waiting_list_title_id, $no_of_weeks = DEFAULT_NO_OF_WEEKS) {
		$available_doctors = $this->_controller->EmployeeDetail->get_available_doctors_by_weeks_and_wlist($waiting_list_title_id, $no_of_weeks);
		$formatted_keys = array();
		$data = array();
		if (!empty($available_doctors)) {
			foreach ($available_doctors as $key => $available_doctor) {
				$formatted_keys[$key]['id'] = $available_doctor['AvailableDoctor']['EmployeeId'];
				$formatted_keys[$key]['first_name'] = $available_doctor['AvailableDoctor']['EmployeeFName'];
				$formatted_keys[$key]['user_id'] = $available_doctor['AvailableDoctor']['UserId'];
			}

			foreach ($formatted_keys as $key => $value) {
				$obj_employee = new DtEmployeeDetail($value);
				$data[$key]['id'] = $obj_employee->id;
				$data[$key]['first_name'] = $obj_employee->first_name;
				$data[$key]['user_id'] = $obj_employee->user_id;
			}

			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $data;
		} else {
			$this->ApiResponse->offset = '301';
			$this->ApiResponse->msg = "No record found";
		}
		return $this->ApiResponse->get();
	}

	/**
	 * Save doctor working hours break ups
	 * 
	 * Error code range : 331-360
	 * 
	 * @param string|int $employee_id Employee Detail id
	 * @return json $api_response 
	 */
	function save_doctor_working_hour_breakups($employee_id) {

		if (!empty($employee_id)) {

			$working_hours = $this->_controller->EmployeeWorkingHour->get_all_by_employee_detail_id($employee_id);

			$index = 0;
			$data = array();
			foreach ($working_hours as $value) {

				$remove_data['field_value'] = $value['EmployeeWorkingHour']['id'];
				$remove_data['modified'] = $this->App->get_current_datetime();
				$remove_data['field_name'] = 'employee_working_hour_id';

				$this->_controller->EmployeeWorkingHour->EmployeeWorkingHourBreakup->remove_all_common_by_field_name($remove_data);

				list($total_possible_slots, $slots) = $this->generate_slots($value['EmployeeWorkingHour']['start_time'], $value['EmployeeWorkingHour']['end_time'], 15);

				foreach ($slots as $key => $slot) {
					$data[$index]['EmployeeWorkingHourBreakup']['employee_working_hour_id'] = $value['EmployeeWorkingHour']['id'];
					$data[$index]['EmployeeWorkingHourBreakup']['start_time'] = $slot['start'];
					$data[$index]['EmployeeWorkingHourBreakup']['end_time'] = $slot['end'];
					$data[$index]['EmployeeWorkingHourBreakup']['created'] = $this->App->get_current_datetime();
					$data[$index++]['EmployeeWorkingHourBreakup']['modified'] = $this->App->get_current_datetime();
				}
			}

			if ($this->_controller->EmployeeWorkingHour->EmployeeWorkingHourBreakup->custom_saveAll($data)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Saved";
			} else {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '331';
				$this->ApiResponse->msg = "Unable to save employee working hour breakup";
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '332';
			$this->ApiResponse->msg = "invalid employee id";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Get available working slots for given employee belongs to particular division.
	 * 
	 * Error code range : 361-390
	 * 
	 * $params should be of the form
	 * 
	 * <code>
	 * Array
	  (
	  [waiting_list_id] => value1,
	  [employee_detail_id] => value2
	  [date_as_appointment] => value3
	  [working_day] => value4
	  )
	 * </code>
	 * 
	 * @param array $params described above
	 * @return json $api_response 
	 */
	function get_available_working_slots($params = array()) {

		$allowed_keys = array('waiting_list_title_id', 'employee_detail_id', 'date_as_appointment');

		if (!empty($params)) {
			$keys = array_keys($params);
			$is_valid_params = array_diff_assoc($allowed_keys, $keys);

			// parametre missing
			if (count($is_valid_params)) {
				$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
				$this->ApiResponse->offset = '361';
				$this->ApiResponse->msg = "required parametre missing";
			} else {

				$params['working_day'] = $this->App->get_week_day($params['date_as_appointment']);

				$is_on_leave = $this->_controller->EmployeeDetail->is_on_leave($params['employee_detail_id'], $params['date_as_appointment']);
				
				if ($is_on_leave) {
					$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
					$this->ApiResponse->offset = '362';
					$this->ApiResponse->msg = "Slots not avalible since clinician is on leave";
				} else {

					$is_consult = $this->_controller->EmployeeDetail->is_doctor_consult_division_for_given_working_day($params);
					
					if (empty($is_consult)) {
						$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
						$this->ApiResponse->offset = '363';
						$this->ApiResponse->msg = "Slots not avalible since clinician does not consult given day for selected waiting list";
					} else {
						$data = array();

						$available_slots = $this->_controller->EmployeeWorkingHour->get_available_working_slots($params);
						
						if (!empty($available_slots)) {
							// whole array contain single Division so we can get division @ 0th index
							$obj_division = new DtDivision($available_slots[0]['Division']);
							foreach ($available_slots as $key => $slot) {
								$obj_division->add_employee_working_hour_breakup($slot['EmployeeWorkingHourBreakup']);
								$data[$obj_division->name][$key]['EmployeeWorkingHourBreakup'] = $obj_division->get_employee_working_hour_breakup_obj_at_row($slot['EmployeeWorkingHourBreakup']['id'])->get_field();
								$data[$obj_division->name][$key]['Division']['id'] = $obj_division->id;
							}
							$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
							$this->ApiResponse->offset = '0';
							$this->ApiResponse->msg = "Available slots found";
							$this->ApiResponse->body = $data;
						} else {
							$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
							$this->ApiResponse->offset = '364';
							$this->ApiResponse->msg = "Slots not avalible";
						}
					} 
					
				}
			}
		} else {
			$this->ApiResponse->base = ApiEmployeeDetailComponent::BASE_CODE;
			$this->ApiResponse->offset = '365';
			$this->ApiResponse->msg = "invalid parametre";
		}

		$this->ApiResponse->format = ApiResponseComponent::JSON;
		return $this->ApiResponse->get();
	}

	/**
	 * Generate slot with respect to duration
	 * 
	 * @param string $start_time start time
	 * @param string $end_time end time
	 * @param string|int $duration interval/duration
	 * @return array total_possible_slots,slots
	 */
	function generate_slots($start_time, $end_time, $duration) {
		$slots = array();
		$index = 0;

		$ux_start_time = $this->App->get_unix_timestamp($start_time);
		$ux_end_time = $this->App->get_unix_timestamp($end_time);

		while ($ux_start_time < $ux_end_time) {

			if (empty($now)) {
				$now = $start_time;
			} else {
				$now = $this->App->format_as_sql($then);
			}
			$then = $this->App->get_next_time_by_duration($now, $duration);

			if ($then > $ux_end_time)
				$then = $ux_end_time;

			$slots[$index]['start'] = $now;
			$slots[$index++]['end'] = $this->App->format_as_sql($then);

			$ux_start_time = $this->App->get_unix_timestamp($then);
		}

		return array(count($slots), $slots);
	}

	/**
	 * Send welcome email on signup/register
	 * @param array $data 
	 */
	function send_welcome_email($data) {
		$message = $data;

		$this->App->send_email($data['username'], SITE_NAME . ': Welcome to ' . SITE_NAME, $message, 'doctor_registration');
	}

}