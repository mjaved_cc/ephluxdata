<?php

App::uses('Resize', 'Vendor');

class FileUploadComponent extends Component {

	private $_options;
	private $_controller;
	public $components = array('App');

	function startup(Controller $controller) {
		$this->_controller = $controller;

		$this->_options['upload_dir'] = DP_ORG_DIR;
		$this->_options['upload_url'] = DP_ORG_URL;
		$this->_options['thumbnail_dir'] = DP_THUMB_DIR;
		$this->_options['thumbnail_url'] = DP_THUMB_URL;
		$this->_options['thumbnail_max_width'] = THUMBNAIL_MAX_WIDTH;
		$this->_options['thumbnail_max_height'] = THUMBNAIL_MAX_HEIGHT;
		$this->_options['image_quality'] = 100;
		$this->_options['field_name'] = 'file';
	}

	public function post($file_data) {

		$upload = isset($file_data[$this->_options['field_name']]) ?
				$file_data[$this->_options['field_name']] : array(
			'tmp_name' => null,
			'name' => null,
			'size' => null,
			'type' => null,
			'error' => null
				);

		$info = $this->handle_file_upload(
				$upload['tmp_name'], isset($_SERVER['HTTP_X_FILE_NAME']) ? $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'], isset($_SERVER['HTTP_X_FILE_SIZE']) ? $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'], isset($_SERVER['HTTP_X_FILE_TYPE']) ? $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'], $upload['error']
		);

		return $this->App->encoding_json($info);
	}

	public function handle_file_upload($uploaded_file, $name, $size, $type, $error) {

		$file = new stdClass();
		$file->name = $this->get_unique_name(basename($name));
		$file->size = intval($size);
		$file->type = $type;
		$file->code = '0';

		if (!$error && $file->name) {
			$file_path = $this->_options['upload_dir'] . $file->name;
			$thumbnail_path = $this->_options['thumbnail_dir'] . $file->name;

			$move_result = move_uploaded_file($uploaded_file, $file_path);
			if ($move_result) {
				$file->upload_url = $this->_options['upload_url'] . $file->name;
				$resizeObj = new Resize($file_path);

				$flag = $resizeObj->resizeImage($this->_options['thumbnail_max_width'], $this->_options['thumbnail_max_height'], 'crop');
				$is_saved = $resizeObj->saveImage($thumbnail_path, $this->_options['image_quality']);
				$success = $flag && $is_saved;

				if (!$success) {
					$file->code = '9';
					$file->message = 'Unable to create thumbnail.';
				} else {
					$file->thumbnail_url = $this->_options['thumbnail_url'] . $file->name;
				}
			}
		} else {
			$file->code = $error;
			$file->message = $this->_code_to_message($error);
		}

		return $file;
	}

	public function get_unique_name($file_name) {

		$extension = pathinfo($file_name, PATHINFO_EXTENSION);
		return md5(time()) . '.' . $extension;
	}

	private function _code_to_message($code) {
		switch ($code) {
			case UPLOAD_ERR_INI_SIZE:
				$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
				break;
			case UPLOAD_ERR_PARTIAL:
				$message = "The uploaded file was only partially uploaded";
				break;
			case UPLOAD_ERR_NO_FILE:
				$message = "No file was uploaded. Please upload file.";
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$message = "Missing a temporary folder";
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$message = "Failed to write file to disk";
				break;
			case UPLOAD_ERR_EXTENSION:
				$message = "File upload stopped by extension";
				break;

			default:
				$message = "Unknown upload error";
				break;
		}
		return $message;
	}

}

?>
