<?php
App::uses('AppController', 'Controller');
/**
 * TemplateAttributes Controller
 *
 * @property TemplateAttribute $TemplateAttribute
 */
class TemplateAttributesCategoriesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'TemplateAttributesCategory.title' => 'asc'
		),
		'conditions' => array('TemplateAttributesCategory.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', 'Template Attribute Categories');
		
		$attr_types = array('text'=>'text', 'textarea'=>'textarea', 'checkbox'=>'checkbox', 'radio'=>'radio', 'texteditor'=>'texteditor');
		$this->set('attr_types',$attr_types);
		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->TemplateAttributesCategory->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['TemplateAttributesCategory']['q'];
			$conditons=array('or'=>array("TemplateAttributesCategory.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			$this->request->params['named']['q'] = $keyword;
		}
		
		$data = $this->paginate();
			
		//pr($data);
		$templateAttributesCategories = array();
		foreach ($data as $key => $templateAttributeCategory) {
			$obj_templateAttributesCategory = new DtTemplateAtrributesCategory($templateAttributeCategory[$this->modelClass]);
			$templateAttributesCategories[$key][$this->modelClass] = $obj_templateAttributesCategory->get_field();
			
		}
		$this->set(compact('templateAttributesCategories'));
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->TemplateAttribute->id = $id;
		if (!$this->TemplateAttribute->exists()) {
			throw new NotFoundException(__('Invalid allowed service'));
		}
		$this->set('allowedService', $this->TemplateAttribute->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
 	public function add($id = null) {
		if ($this->request->is('post')) {
			$this->TemplateAttributesCategory->create();
			if ($this->TemplateAttributesCategory->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The attribute category has been saved'));
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('The attribute category could not be saved. Please, try again.'));
			}
		}
		
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->TemplateAttributesCategory->id = $id;
		if (!$this->TemplateAttributesCategory->exists()) {
			throw new NotFoundException(__('Invalid template attribute'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TemplateAttributesCategory->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The template attribute category has been saved'));
				$this->redirect(array('action' => 'index',$emp_id));
			} else {
				$this->Session->setFlash(__('The template attribute category could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->TemplateAttributesCategory->read(null, $id);
			$obj_templateAttribute = new DtTemplateAtrributesCategory($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_templateAttribute->get_field();
		}
		
		
	}
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		if ($this->TemplateAttributesCategory->remove($data)) {
			$this->Session->setFlash(__('Template Attribute category deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Template Attribute category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
