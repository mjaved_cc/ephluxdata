<?php
App::uses('AppController', 'Controller');
/**
 * AllowedServices Controller
 *
 * @property AllowedService $AllowedService
 */
class AllowedServicesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'AllowedService.name' => 'asc'
		),
		'conditions' => array('AllowedService.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', 'Allowed Services');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	function search() {
        $this->set('results',$this->AllowedService->search($this->data['AllowedService']['q']));
    } 
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->AllowedService->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['AllowedService']['q'];
			$cond=array('or'=>array("AllowedService.name LIKE '%$keyword%'","AllowedServiceGroup.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'AllowedService.name' => 'asc'
				),
				'conditions' => array('AllowedService.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		//pr($data);
		$allowedServices = array();
		foreach ($data as $key => $allowedService) {
			$obj_allowedservice = new DtAllowedService($allowedService[$this->modelClass]);
			$obj_AllowedServiceGroup = new DtAllowedServiceGroup($allowedService['AllowedServiceGroup']);
			$allowedServices[$key][$this->modelClass] = $obj_allowedservice->get_field();
			$allowedServices[$key]['AllowedServiceGroup'] = $obj_AllowedServiceGroup->get_field();
			
		}
		$this->set(compact('allowedServices'));
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AllowedService->id = $id;
		if (!$this->AllowedService->exists()) {
			throw new NotFoundException(__('Invalid allowed service'));
		}
		$this->set('allowedService', $this->AllowedService->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
 	public function add() {
		if ($this->request->is('AllowedService')) {

			$this->AllowedService->set($this->request->data);
			if ($this->AllowedService->validates(array('fieldList' => array('title','division_id')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['allowed_service_group_id'] = $this->request->data[$this->modelClass]['allowed_service_group_id'];				
				$data['description'] = $this->request->data[$this->modelClass]['description'];				

				if ($this->AllowedService->create($data)) {
					$this->Session->setFlash(__('The allowed service has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The allowed service could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The allowed service could not be saved. Please, try again.'));
			}
		}
		$allowedServiceGroups = $this->AllowedService->AllowedServiceGroup->find('list');
		$this->set(compact('allowedServiceGroups'));
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->AllowedService->id = $id;
		if (!$this->AllowedService->exists()) {
			throw new NotFoundException(__('Invalid allowed service'));
		}
		if ($this->request->is('AllowedService') || $this->request->is('put')) {
			$this->AllowedService->set($this->request->data);
			if ($this->AllowedService->validates(array('fieldList' => array('title','division')))) {
				
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['allowed_service_group_id'] = $this->request->data[$this->modelClass]['allowed_service_group_id'];				
				$data['description'] = $this->request->data[$this->modelClass]['description'];				
				
				if ($this->AllowedService->update($data)) {
					$this->Session->setFlash(__('The allowed service has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The allowed service could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The allowed service could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->AllowedService->get_by_id($id);
			$obj_allowedService = new DtAllowedService($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_allowedService->get_field();
			
			$allowedServiceGroups = $this->AllowedService->AllowedServiceGroup->find('list');
			$this->set(compact('allowedServiceGroups'));
		}
	}
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->AllowedService->remove($data)) {
			$this->Session->setFlash(__('Allowed service deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Allowed service was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
