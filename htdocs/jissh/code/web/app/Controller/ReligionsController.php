<?php

App::uses('AppController', 'Controller');

/**
 * Religions Controller
 *
 * @property Religion $Religion
 */
class ReligionsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Religion.name' => 'asc'
		),
		'conditions' => array('Religion.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Religion->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Religion']['q'];
			$cond=array('or'=>array("Religion.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Religion.name' => 'asc'
				),
				'conditions' => array('Religion.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$religions = array();
		foreach ($data as $key => $religion) {
			$obj_religions = new DtReligion($religion[$this->modelClass]);
			$religions[$key][$this->modelClass] = $obj_religions->get_field();
		}

		$this->set(compact('religions'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid religion'));
		}
		$this->set('religion', $this->Religion->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Religion->set($this->request->data);
			if ($this->Religion->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Religion->create($data)) {
					$this->Session->setFlash(__('The Religion has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Religion could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Religion could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Religion->id = $id;
		if (!$this->Religion->exists()) {
			throw new NotFoundException(__('Invalid religion'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Religion->set($this->request->data);
			if ($this->Religion->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Religion->update($data)) {
					$this->Session->setFlash(__('The religion has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The religion could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The religion could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Religion->get_by_id($id);
			$obj_religion = new DtReligion($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_religion->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Religion->remove($data)) {
			$this->Session->setFlash(__('Religion deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Religion was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}