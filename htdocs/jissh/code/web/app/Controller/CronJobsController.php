<?php

App::uses('AppController', 'Controller');

/**
 * Notifications Controller
 *
 * @property Notification $Notification
 */
class CronJobsController extends AppController {
	public $uses = array('Cron','EmailQueue');
	
	function beforeRender() {
		$this->layout = 'ajax';	
	}
	
	public function send_emails_to_queue(){
		$emails = $this->Cron->get_email_queue_for_cron();
		//pr($emails);exit;
		set_time_limit(0);
		$success_ids=array();
		foreach($emails as $email){
			try {
				$status = $this->App->send_email($email['EmailQueue']['target_id'], $email['EmailTemplate']['subject'], $email['EmailTemplate']['template'], $template = 'default', $layout = 'default');
				if($status){
					$success_ids[] = $email['EmailQueue']['id'];
				}
			} catch ( Exception $e ) {
				continue;
			}
		}
		
		$this->EmailQueue->deleteAll(array('EmailQueue.id' => $success_ids));
		exit;
	}
	
	public function remove_email_templates() {
		$this->Cron->remove_email_templates();
		exit;
	}
	
	// copy emails to email_queue table before to send
	public function get_emails_before_day_appointment() {
		$emails = $this->Cron->get_emails_before_day_appointment();
		
		foreach($emails as $user) {
			$email_users_arr[] = $user['PtUser']['username'];
			$email_users_arr[] = $user['EmpUser']['username'];	
		}
		
		if(!empty($email_users_arr)) {
			$this->loadModel('EmailTemplate');
			$data=array();
			$data['subject'] = "Your Appointment is scheduled tomorrow.";
			$data['template'] = "Hi this is notification about your appointment that is scheduled tomorrow.";
			$this->EmailTemplate->save($data);
			$template_id = $this->EmailTemplate->getInsertID();	
			
			$data=array();
			$i=0;
			foreach($email_users_arr as $email) {
				$data[$i]['target_id'] = $email;
				$data[$i]['type'] = 'email';
				$data[$i]['status'] = 'pending';
				$data[$i]['template_id'] = $template_id;	
				
				$i++;
			}
			
			$this->loadModel('EmailQueue');
			$this->EmailQueue->saveAll($data);
		}
		exit;
	}
}