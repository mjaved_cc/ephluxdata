<?php

App::uses('AppController', 'Controller');

/**
 * Countries Controller
 *
 * @property Country $Country
 */
class CountriesController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Country.name' => 'asc'
		),
		'conditions' => array('Country.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Country->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Country']['q'];
			$cond=array('or'=>array("Country.name LIKE '%$keyword%'","Country.nationality LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Country.name' => 'asc'
				),
				'conditions' => array('Country.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$countries = array();
		foreach ($data as $key => $country) {
			$obj_countries = new DtCountries($country[$this->modelClass]);
			$countries[$key][$this->modelClass] = $obj_countries->get_field();
		}

		$this->set(compact('countries'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid division'));
		}

		$country = array();

		$result = $this->Country->get_by_id($id);

		$obj_country = new DtCountries($result[$this->modelClass]);
		$country[$this->modelClass] = $obj_country->get_field();

		$this->set(compact('country'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Country->set($this->request->data);
			if ($this->Country->validates(array('fieldList' => array('name', 'nationality')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['nationality'] = $this->request->data[$this->modelClass]['nationality'];
				
				if ($this->Country->create($data)) {
					$this->Session->setFlash(__('The Country has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Country could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Country could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid Country'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Country->set($this->request->data);
			if ($this->Country->validates(array('fieldList' => array('name', 'nationality')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();				
				$data['nationality'] = $this->request->data[$this->modelClass]['nationality'];

				if ($this->Country->update($data)) {
					$this->Session->setFlash(__('The Country has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Country could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Country could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Country->get_by_id($id);
			$obj_country = new DtCountries($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_country->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();

		if ($this->Country->remove($data)) {
			$this->Session->setFlash(__('Country deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Country was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
