<?php
App::uses('AppController', 'Controller');
/**
 * WaitingListsTitles Controller
 *
 * @property WaitingListsTitle $WaitingListsTitle
 */
class WaitingListsTitlesController extends AppController {
	
	public $uses = array('WaitingListsTitle', 'PatientWaitingList');
	//public $components = array('ApiWaitingListsTitle');
	
	public $paginate = array(
		'limit' => 2,
		'order' => array(
			'WaitingListsTitle.title' => 'asc'
		),
		'conditions' => array('WaitingListsTitle.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', 'Waiting List Titles');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->WaitingListsTitle->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['WaitingListsTitle']['q'];
			$conditons = array('OR' => array(
					'WaitingListsTitle.title LIKE' => '%' . $keyword . '%',
					'Division.name LIKE' => '%' . $keyword . '%'
					));
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			//$data = find('all',array('conditions')=>$cond);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		
		$waitingListsTitles = array();
		foreach ($data as $key => $waitingListsTitle) {
			$obj_waitingListsTitle = new DtWaitingListsTitle($waitingListsTitle[$this->modelClass]);
			$obj_waitingListsTitle->add_division($waitingListsTitle['Division']);
			$waitingListsTitles[$key][$this->modelClass] = $obj_waitingListsTitle->get_field();
			$waitingListsTitles[$key]['Division'] = $obj_waitingListsTitle->Division->get_field();
			//pr($waitingListsTitles[$key][$this->modelClass]);
			
		}
		
		$this->set(compact('waitingListsTitles'));
		
	}
	
	
	public function AddAndAssign() {
		if ($this->request->is('post')) {

			$this->WaitingListsTitle->set($this->request->data);
			if ($this->WaitingListsTitle->validates(array('fieldList' => array('title','division_id')))) {
				$patient_detail_id = $this->request->data[$this->modelClass]['patient_detail_id'];
				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['title'] = $this->request->data[$this->modelClass]['title'];
				$data['division_id'] = $this->request->data[$this->modelClass]['division_id'];				
				$data['description'] = $this->request->data[$this->modelClass]['description'];				

				if ($waiting_list_title_id = $this->WaitingListsTitle->create($data)) {
					
					//$waiting_list_title_id = $this->WaitingListsTitle->getLastInsertID();
					$data['patient_detail_id'] = $patient_detail_id;
					$data['waiting_list_title_id'] = $waiting_list_title_id;
					$data['created'] = $this->App->get_current_datetime();
					$data['ip_address'] = $this->App->get_numeric_ip_representation();
					
					$this->PatientWaitingList->create();
					$this->PatientWaitingList->save($data);
					
					$this->redirect(array('controller'=>'PatientDetails', 'action' => 'edit',$patient_detail_id));
				} else {
					$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
					$this->redirect(array('action' => 'assign',$patient_detail_id));
				}
			} else {
				$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
				$this->redirect(array('action' => 'index',$patient_detail_id));
			}
		}

	}
	
	
	public function addToList() {
		//pr($this->data);
		if ($this->request->is('post') and isset($this->data['WaitingListsTitle']['patient_detail_id']) and isset($this->data['WaitingListsTitle']['waiting_list_title_id'])) {
			$conditions = array(
				'PatientWaitingList.patient_detail_id' => $this->data['WaitingListsTitle']['patient_detail_id'],
				'PatientWaitingList.waiting_list_title_id' => $this->data['WaitingListsTitle']['waiting_list_title_id']
			);
			
			if ($this->PatientWaitingList->hasAny($conditions)){
				$this->Session->setFlash(__('Patient already added to this waiting list.'));
				$this->redirect(array('controller'=>'WaitingListsTitles', 'action' => 'assign',$this->data['WaitingListsTitle']['patient_detail_id']));
			}else {
				$data['patient_detail_id'] = $this->data['WaitingListsTitle']['patient_detail_id'];
				$data['waiting_list_title_id'] = $this->data['WaitingListsTitle']['waiting_list_title_id'];
				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				
				$this->PatientWaitingList->create();
				$this->PatientWaitingList->save($data);
				$this->Session->setFlash(__('Patient added to waiting list.'));
				$this->redirect(array('controller'=>'PatientDetails', 'action' => 'edit',$this->data['WaitingListsTitle']['patient_detail_id']));
			}
		}
	}
	
	public function assign($pid=0) {
		$this->WaitingListsTitle->recursive = 0;
		
		if (($this->request->is('post') and isset($this->data['WaitingListsTitle']['q'])) || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['WaitingListsTitle']['q'];
			$cond=array('or'=>array("WaitingListsTitle.title LIKE '%$keyword%'","Division.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 2,
				'order' => array(
					'AllowedService.name' => 'asc'
				),
				'conditions' => array('WaitingListsTitle.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		
		$waitingListsTitles = array();
		foreach ($data as $key => $waitingListsTitle) {
			$obj_waitingListsTitle = new DtWaitingListsTitle($waitingListsTitle[$this->modelClass]);
			$obj_waitingListsTitle->add_division($waitingListsTitle['Division']);
			$waitingListsTitles[$key][$this->modelClass] = $obj_waitingListsTitle->get_field();
			$waitingListsTitles[$key]['Division'] = $obj_waitingListsTitle->Division->get_field();
			//pr($waitingListsTitles[$key][$this->modelClass]);
			
		}
		
		$this->set(compact('waitingListsTitles'));
		$this->set(compact('pid'));
		$divisions = $this->WaitingListsTitle->Division->find('list');
		array_unshift($divisions, "Select");
		$this->set(compact('divisions'));
		if($this->request->is('ajax')) {
			$this->layout='ajax';
			
			$this->render('/Elements/assign_waiting_listing');
		}
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function view($id = null) {
		$this->WaitingListsTitle->id = $id;
		if (!$this->WaitingListsTitle->exists()) {
			throw new NotFoundException(__('Invalid waiting lists title'));
		}

		$waitingListsTitle = array();

		$result = $this->WaitingListsTitle->get_by_id($id);

		$obj_waitingListsTitle = new DtWaitingListsTitle($result[$this->modelClass]);
		$waitingListsTitle[$this->modelClass] = $obj_waitingListsTitle->get_field();

		$this->set(compact('waitingListsTitle'));
	}

/**
 * add method
 *
 * @return void
 */
 	public function add() {
		if ($this->request->is('post')) {

			$this->WaitingListsTitle->set($this->request->data);
			if ($this->WaitingListsTitle->validates(array('fieldList' => array('title','division_id')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['title'] = $this->request->data[$this->modelClass]['title'];
				$data['division_id'] = $this->request->data[$this->modelClass]['division_id'];				
				$data['description'] = $this->request->data[$this->modelClass]['description'];				

				if ($this->WaitingListsTitle->create($data)) {
					$this->Session->setFlash(__('The waiting lists title has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
			}
		}
		$divisions = $this->WaitingListsTitle->Division->find('list');
		array_unshift($divisions, "Select");
		$this->set(compact('divisions'));
	}
	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->WaitingListsTitle->id = $id;
		if (!$this->WaitingListsTitle->exists()) {
			throw new NotFoundException(__('Invalid waiting lists title'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->WaitingListsTitle->set($this->request->data);
			if ($this->WaitingListsTitle->validates(array('fieldList' => array('title','division')))) {
				
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['title'] = $this->request->data[$this->modelClass]['title'];
				$data['division_id'] = $this->request->data[$this->modelClass]['division_id'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				
				if ($this->WaitingListsTitle->update($data)) {
					$this->Session->setFlash(__('The waiting lists title has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The waiting lists title could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->WaitingListsTitle->get_by_id($id);
			$obj_waitinglisttitle = new DtWaitingListsTitle($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_waitinglisttitle->get_field();
			//$divisions=array('Select');
			$divisions = $this->WaitingListsTitle->Division->find('list');
			//$divisions['NULL'] = 'Select';
			array_unshift($divisions, "Select");
			
			$this->set(compact('divisions'));
		}
	}
	

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->WaitingListsTitle->remove($data)) {
			$this->Session->setFlash(__('Waiting lists title deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Waiting lists title was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	/**
	 * Get all waiting list title 
	 * 
	 * @return json
	 */
	function get_all(){
		$data = $this->ApiWaitingListsTitle->get_all();
		$this->set(compact('data'));
	}
	
}
