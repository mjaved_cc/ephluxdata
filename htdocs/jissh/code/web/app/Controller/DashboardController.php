<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class DashboardController extends AppController {

	public $name = 'Dashboard';
	public $uses = array('PatientDetail', 'User', 'Division', 'Appointment');
	public $components = array('ApiAppointment', 'Auth', 'ApiDashboard');

	function beforeFilter() {
		parent::beforeFilter();
		//$this->layout = 'admin';
	}

	function beforeRender() {
		$this->set('page_heading', 'Dashboard');
		$this->JCManager->add_js(array('event', MRIYA_JS_PATH . 'jquery.jcarousel.min'));
		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 * @link http://stackoverflow.com/questions/9488153/fullcalendar-how-do-i-allow-users-to-edit-delete-events-and-remove-them-from-th
	 */
	public function index() {
		$this->paginate = array(
			'limit' => 5,
			'order' => array(
				'PatientDetail.created' => 'desc'
			),
			'conditions' => array('User.status' => STATUS_ACTIVE, 'User.user_type' => USER_TYPE_PATIENT, 'PatientDetail.status' => STATUS_ACTIVE)
		);

		$logged_user = $this->Auth->user();

		$user_type = $logged_user['user_type'];
		
		$function_name = 'get_dashboard_for_' . $user_type ;
		$this->ApiDashboard->$function_name();
		
		
//		if( $user_type == 'doctor') {
//			
//			$this->ApiDashboard->get_dashboard_for_doctor();
//		}

		$data = $this->paginate();

		$patientDetails = array();
		foreach ($data as $key => $PatientDetail) {
			$obj_user = new DtUser($PatientDetail['User']);
			$obj_user->add_patient_detail($PatientDetail[$this->modelClass]);

			/* Retrieve data from rendering logic */

			$patientDetails[$key]['User'] = $obj_user->get_field();
			if ($obj_user->PatientDetail instanceof DtPatientDetail) {
				$patientDetails[$key][$this->modelClass] = $obj_user->PatientDetail->get_field();
			}
		}

//		$response = $this->ApiAppointment->get_appointments_for_dashboard();
//		$messages = $this->ApiAppointment->get_messages_for_dashboard();

		$this->set(compact('patientDetails'));
//		$this->set(compact('response'));
//		$this->set(compact('messages'));
	}

	function temp() {
		$this->autoRender = false;
		$data = array();
		foreach ($results as $key => $value) {
			$data[$key]['id'] = round(rand(0, 9));
			$data[$key]['title'] = 'losem';
			$data[$key]['start'] = date('Y-m-d H:i'); // yyyy-MM-dd HH:mm			
			$data[$key]['division_id'] = 1;
		}

		echo $this->App->encoding_json($data);
	}

	function update() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$event = $this->App->decoding_json($this->request->data['event']);
			$result = $this->Garbage->findById($event['id']);

			$result['Garbage']['id'] = $event['id'];
			$result['Garbage']['title'] = $event['title'];
			$result['Garbage']['start'] = $event['start'];
			$result['Garbage']['end'] = $event['end'];
			$result['Garbage']['division_id'] = $event['division_id'];

			$this->Garbage->save($result);
		}
	}

	function create() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$event = $this->App->decoding_json($this->request->data['event']);

			$result['Garbage']['title'] = $event['title'];
			$result['Garbage']['start'] = $event['start'];
			$result['Garbage']['end'] = $event['end'];
			$result['Garbage']['division_id'] = $event['division_id'];
			$this->Garbage->create();
			$this->Garbage->save($result);
		}
	}

	function test() {



		pr(json_decode($messages));
		exit;
	}

}

