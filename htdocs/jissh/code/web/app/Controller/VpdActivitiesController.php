<?php
App::uses('AppController', 'Controller');
/**
 * VpdActivities Controller
 *
 * @property VpdActivity $VpdActivity
 */
class VpdActivitiesController extends AppController {
	
	var $uses = array('VpdActivity','User','EmployeeDetail','VpdActivityType','Modality','Sponsor','Provider');
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'VpdActivity.id' => 'desc'
		),
		'conditions' => array('VpdActivity.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', 'Voluntary Activity');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index($id= null) {
		$this->VpdActivity->recursive = 0;
		if (($this->request->is('post') and isset($this->data['VpdActivity']['q'])) || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['VpdActivity']['q'];
			$id = isset($this->request->params['named']['id']) ? $this->request->params['named']['id'] : $this->data['VpdActivity']['emp_id'];
			
			$conditons = array('OR' => array(
					'Sponsor.name LIKE' => '%' . $keyword . '%',
					'VpdActivityType.name LIKE' => '%' . $keyword . '%',
					'Modality.name LIKE' => '%' . $keyword . '%',
					'VpdActivity.activity_name LIKE' => '%' . $keyword . '%',
					'Provider.name LIKE' => '%' . $keyword . '%'
					));
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			
			$this->request->params['named']['q'] = $keyword;
			
		}
		$this->request->params['named']['id'] = $id;
		if (!$this->EmployeeDetail->exists($id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		
		$cond = array('VpdActivity.employee_detail_id' => $id);
		$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $cond);
			
		$data = $this->paginate();
		//pr($data);exit;	
		$vpdActivities = array();
		foreach ($data as $key => $vpdActivity) {
			$obj_user = new DtUser($vpdActivity['User']);
			$obj_user->add_employee_detail($vpdActivity['EmployeeDetail']);
			$obj_user->EmployeeDetail->add_vpdActivityType($vpdActivity['VpdActivityType']);
			$obj_user->EmployeeDetail->add_vpdActivity($vpdActivity[$this->modelClass]);
			$obj_user->EmployeeDetail->add_modality($vpdActivity['Modality']);
			$obj_user->EmployeeDetail->add_sponsor($vpdActivity['Sponsor']);
			$obj_user->EmployeeDetail->add_provider($vpdActivity['Provider']);
			
			/* Retrieve data from rendering logic */
			//$data = array();
			//$data['User'] = $obj_user->get_field();

			$vpdActivities[$key]['User'] = $obj_user->get_field();
			if ($obj_user->EmployeeDetail instanceof DtEmployeeDetail) {
				$vpdActivities[$key]['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
				$vpdActivities[$key]['VpdActivity'] = $obj_user->EmployeeDetail->get_vpdActivities();
				$vpdActivities[$key]['VpdActivityType'] = $obj_user->EmployeeDetail->get_vpdActivityTypes();
				$vpdActivities[$key]['Modality'] = $obj_user->EmployeeDetail->get_modalities();
				$vpdActivities[$key]['Sponsor'] = $obj_user->EmployeeDetail->get_sponsors();
				$vpdActivities[$key]['Provider'] = $obj_user->EmployeeDetail->get_providers();
			}
			
		}
		
		$this->set('emp_id',$id);
		$this->set(compact('vpdActivities'));
		if($this->request->is('ajax')) {
			$this->layout='ajax';
			$this->render('/Elements/vdp_activity_listing');
		}
		
		//$this->set('vpdActivities', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->VpdActivity->id = $id;
		if (!$this->VpdActivity->exists()) {
			throw new NotFoundException(__('Invalid vpd activity'));
		}
		$this->set('vpdActivity', $this->VpdActivity->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {
		if ($this->request->is('post')) {
			$this->VpdActivity->create();
			if ($this->VpdActivity->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The vpd activity has been saved'));
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('The vpd activity could not be saved. Please, try again.'));
			}
		}
		$employeeDetails = $this->VpdActivity->EmployeeDetail->find('list');
		$providers = $this->VpdActivity->Provider->find('list');
		$vpdActivityTypes = $this->VpdActivity->VpdActivityType->find('list');
		$modalities = $this->VpdActivity->Modality->find('list');
		$sponsors = $this->VpdActivity->Sponsor->find('list');
		
		$this->set('emp_id',$id);
		$this->set(compact('employeeDetails', 'providers', 'vpdActivityTypes', 'modalities', 'sponsors'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($emp_id = null ,$id = null) {
		$this->VpdActivity->id = $id;
		if (!$this->VpdActivity->exists()) {
			throw new NotFoundException(__('Invalid vpd activity'));
		}
		if (!$this->EmployeeDetail->exists($emp_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VpdActivity->save($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The vpd activity has been saved'));
				$this->redirect(array('action' => 'index',$emp_id));
			} else {
				$this->Session->setFlash(__('The vpd activity could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->VpdActivity->read(null, $id);
		}
		
		$employeeDetails = $this->VpdActivity->EmployeeDetail->find('list');
		$providers = $this->VpdActivity->Provider->find('list');
		$vpdActivityTypes = $this->VpdActivity->VpdActivityType->find('list');
		$modalities = $this->VpdActivity->Modality->find('list');
		$sponsors = $this->VpdActivity->Sponsor->find('list');
		$this->set('emp_id',$emp_id);
		$this->set(compact('employeeDetails', 'providers', 'vpdActivityTypes', 'modalities', 'sponsors'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($emp_id = null ,$id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->VpdActivity->id = $id;
		if (!$this->VpdActivity->exists()) {
			throw new NotFoundException(__('Invalid vpd activity'));
		}
		if (!$this->EmployeeDetail->exists($emp_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		if ($this->VpdActivity->delete()) {
			$this->Session->setFlash(__('Vpd activity deleted'));
			$this->redirect(array('action' => 'index',$emp_id));
		}
		$this->Session->setFlash(__('Vpd activity was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
