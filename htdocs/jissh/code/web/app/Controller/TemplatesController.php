<?php
App::uses('AppController', 'Controller');
/**
 * Templates Controller
 *
 * @property Templates $Templates
 */
class TemplatesController extends AppController {
	
	var $uses = array('Template', 'TemplateAttribute', 'Division', 'TemplateAttributesItem', 'PatientDetail', 'EmployeeDetail', 'EmployeeDivision', 'Appointment', 'PatientMedication','PatientMedicationDetail', 'User');
	//public $helpers = array('Html');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Template.template_name' => 'asc'
		),
		'conditions' => array('Template.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {
		
		$this->set('page_heading', 'Template');
		
		$divisions = $this->Division->find('list');
		$this->set('divisions',$divisions);
			
		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);
		
		parent::beforeRender();
	}
	
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->Template->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Template']['q'];
			$conditons=array('or'=>array("Template.template_name LIKE '%$keyword%'","Division.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			$this->request->params['named']['q'] = $keyword;
		}
		
		$data = $this->paginate();
			
		///pr($data);
		$templates = array();
		foreach ($data as $key => $template) {
			$obj_template = new DtTemplate($template[$this->modelClass]);
			$obj_template->add_division($template['Division']);
			$templates[$key][$this->modelClass] = $obj_template->get_field();
			$templates[$key]['Division'] = $obj_template->get_divisions();
		}
		
		$this->set(compact('templates'));
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Template->id = $id;
		$this->recursive = 0;
		if (!$this->Template->exists()) {
			throw new NotFoundException(__('Invalid template id'));
		}
		
		//$result = $this->Template->read(null, $id);
		$result = $this->Template->Find('first',array('conditions' => array("Template.id" => $id)));
		$template = array();	
		//pr($result);exit;
		$obj_template = new DtTemplate($result[$this->modelClass]);
		$obj_template->add_division($result['Division']);
		$template[$this->modelClass] = $obj_template->get_field();
		$template['Division'] = $obj_template->get_divisions();
		
		$data = array();
		foreach($result['TemplateAttributesItem'] as $key => $val) {
			$templateAttribute = $this->TemplateAttribute->find('first', array(
				'conditions' => array('TemplateAttribute.id' => $val['template_attribute_id'])
			));
			$obj_template->add_template_attributes_item($val);
			$obj_template->add_template_attribute($templateAttribute['TemplateAttribute']);
			$data[$key]['TemplateAttributesItem'] = $obj_template->get_attribute_items();
			$data[$key]['TemplateAttribute'] = $obj_template->get_attributes();
			
		}
		unset($result['TemplateAttributesItem']);
		$template['TemplateAttributesItem'] = $data;
			
		$this->set('template', $template);
	}
	
/**
 * medication method
 *
 * @throws NotFoundException
 * @params int $patient_id, int $employee_id, int $appointment_id
 * @return void
 */
	public function medication($patient_id = null, $employee_id=null, $appointment_id=null) {
		
		$this->recursive = 0;
		if (!$this->PatientDetail->exists($patient_id)) {
			throw new NotFoundException(__('Invalid patient id'));
		}
		if (!$this->EmployeeDetail->exists($employee_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		if (!$this->Appointment->exists($appointment_id)) {
			throw new NotFoundException(__('Invalid appointment id'));
		}
		$this->request->params['named']['employee_id'] = $employee_id;
		$this->request->params['named']['patient_id'] = $patient_id;
		$this->request->params['named']['appointment_id'] = $appointment_id;

		$division = $this->EmployeeDivision->Find('all',array('fields'=>array('division_id'),'conditions'=>array('EmployeeDivision.employee_detail_id'=>$employee_id)));
		$division_ids = array();
		foreach($division as $k=>$div) {
			$division_ids[] = $div['EmployeeDivision']['division_id'];
		}
		
		//$result = $this->Template->read(null, $id);
		$result = $this->Template->Find('all',array('conditions'=>array('Template.division_id'=>$division_ids)));
		$templates = array();	
		
		foreach ($result as $key => $template) {
			$obj_template = new DtTemplate($template[$this->modelClass]);
			$obj_template->add_division($template['Division']);
			$templates[$key][$this->modelClass] = $obj_template->get_field();
			$templates[$key]['Division'] = $obj_template->get_divisions();
		}
		//pr($templates);
		$this->set('patient_id',$patient_id);
		$this->set('employee_id',$employee_id);
		$this->set('appointment_id',$appointment_id);
		$this->set(compact('templates'));
		
	}	
	
	public function selectTemplate() {
		
		$this->recursive = 0;
		$template_id = $this->request->data['template_id'];
		$patient_id = $this->request->data['patient_id'];
		$employee_id = $this->request->data['employee_id'];
		$appointment_id = $this->request->data['appointment_id'];
		
		//$result = $this->Template->read(null, $id);
		$result = $this->Template->Find('first',array('conditions' => array("Template.id" => $template_id)));
		$template = array();	
		//pr($result);exit;
		$obj_template = new DtTemplate($result[$this->modelClass]);
		$obj_template->add_division($result['Division']);
		$template[$this->modelClass] = $obj_template->get_field();
		$template['Division'] = $obj_template->get_divisions();
		
		$data = array();
		foreach($result['TemplateAttributesItem'] as $key => $val) {
			$templateAttribute = $this->TemplateAttribute->find('first', array(
				'conditions' => array('TemplateAttribute.id' => $val['template_attribute_id'])
			));
			$obj_template->add_template_attributes_item($val);
			$obj_template->add_template_attribute($templateAttribute['TemplateAttribute']);
			$data[$key]['TemplateAttributesItem'] = $obj_template->get_attribute_items();
			$data[$key]['TemplateAttribute'] = $obj_template->get_attributes();
			
		}
		unset($result['TemplateAttributesItem']);
		$template['TemplateAttributesItem'] = $data;
		
		$this->set('template_id', $template_id);
		$this->set('patient_id', $patient_id);
		$this->set('employee_id', $employee_id);
		$this->set('appointment_id', $appointment_id);	
		$this->set('template', $template);
		
	}	

	public function medicationSearch() {
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Template']['q'];
			$patient_id = isset($this->request->params['named']['patient_id']) ? $this->request->params['named']['patient_id'] : $this->data['Template']['patient_id'];
			$employee_id = isset($this->request->params['named']['employee_id']) ? $this->request->params['named']['employee_id'] : $this->data['Template']['employee_id'];
			$appointment_id = isset($this->request->params['named']['appointment_id']) ? $this->request->params['named']['appointment_id'] : $this->data['Template']['appointment_id'];
			
			$division = $this->EmployeeDivision->Find('all',array('fields'=>array('division_id'),'conditions'=>array('EmployeeDivision.employee_detail_id'=>$employee_id)));
			$division_ids = array();
			foreach($division as $div) {
				$division_ids[] = $div['EmployeeDivision']['division_id'];
			}
		
			$conditons=array('Template.division_id IN '=>$division_ids, 'or'=>array("Template.template_name LIKE '%$keyword%'","Division.name LIKE '%$keyword%'") );
			
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			$this->request->params['named']['q'] = $keyword;
			$this->request->params['named']['employee_id'] = $employee_id;
			$this->request->params['named']['patient_id'] = $patient_id;
			$this->request->params['named']['appointment_id'] = $appointment_id;
		}
		
		$result = $this->paginate();
		
		/*$this->recursive = 0;
		if (!$this->EmployeeDetail->exists($employee_id)) {
			throw new NotFoundException(__('Invalid employee id'));
		}
		
		$division = $this->EmployeeDivision->Find('all',array('fields'=>array('division_id'),'conditions'=>array('EmployeeDivision.employee_detail_id'=>$employee_id)));
		$division_ids = array();
		foreach($division as $div) {
			$division_ids[] = $div['EmployeeDivision']['division_id'];
		}
		
		//$result = $this->Template->read(null, $id);
		$result = $this->Template->Find('all',array('conditions'=>array('Template.division_id IN '=>$division_ids)));*/
		$templates = array();	
		
		foreach ($result as $key => $template) {
			$obj_template = new DtTemplate($template[$this->modelClass]);
			$obj_template->add_division($template['Division']);
			$templates[$key][$this->modelClass] = $obj_template->get_field();
			$templates[$key]['Division'] = $obj_template->get_divisions();
		}
		
		//pr($templates);
		$this->set('patient_id',$patient_id);
		$this->set('employee_id',$employee_id);
		$this->set('appointment_id',$appointment_id);
		$this->set(compact('templates'));
		$this->render('/Elements/medication_search_listing');
	}	
	
	public function saveTemplateData() {
		if ($this->request->is('post')) {
			
			$data = array();
			$data['patient_id'] = $this->request->data['patient_id'];
			$data['employee_id'] = $this->request->data['employee_id'];
			$data['appointment_id'] = $this->request->data['appointment_id'];
			$data['modified'] = $this->App->get_current_datetime();
			
			$this->PatientMedication->create();
			if ($this->PatientMedication->save($data,array('validate' => 'first'))) {
				$patient_medication_id = $this->PatientMedication->getLastInsertId();
				$data = array();
				foreach($this->request->data['attributes'] as $key => $val) {
					$data[$key]['patient_medication_id'] = $patient_medication_id;
					$data[$key]['attribute_name'] = $val['attribute_name'];
					$data[$key]['attribute_value'] = $val['attribute_value'];
					$data[$key]['attribute_type'] = $val['attribute_type'];
					
				}
				
				$this->PatientMedicationDetail->create();
				$this->PatientMedicationDetail->saveAll($data,array('validate' => 'first'));
				
				// send email to the patient and admin.
				
				$pt_user = $this->PatientDetail->find('first', array(
					'conditions' => array('PatientDetail.id' => $this->request->data['patient_id'])
				));
				//pr($pt_user);exit;
				
				$obj_user = new DtUser($pt_user['User']);
				$obj_user->add_patient_detail($pt_user['PatientDetail']);
				
				$pt_user = array();
				$pt_user['User'] = $obj_user->get_field();
				$pt_user['PatientDetail'] = $obj_user->PatientDetail->get_field();
				
				if(!empty($pt_user)) {
					$email_users[] = $pt_user['User']['username'];
				}
				
				$emp_user = $this->EmployeeDetail->find('first', array(
					'conditions' => array('EmployeeDetail.id' => $this->request->data['employee_id'])
				));
				
				$obj_user = new DtUser($emp_user['User']);
				$obj_user->add_employee_detail($emp_user['EmployeeDetail']);
				
				$emp_user = array();
				$emp_user['User'] = $obj_user->get_field();
				$emp_user['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
				
				/*if(!empty($emp_user)) {
					$email_users[] = $emp_user['User']['username'];
				}*/
				//pr($email_users);exit;
				
				$subject = "Doctor submitted medication history";
				$message = 'Hi,<br><br>'.$emp_user['EmployeeDetail']['display_name'].' has submitted medication for '.$pt_user['PatientDetail']['full_name'];
				
				// send email to patients
				
				foreach($email_users as $email) {
					$this->App->send_email($email, $subject, $message, $template = 'default', $layout = 'default');
				}
				
				// send email to admin
				$this->App->send_email(ADMIN_EMAIL, $subject, $message, $template = 'default', $layout = 'default');
				// email functionality end
				
				$this->Session->setFlash(__('The medication has been saved'));
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('The medication could not be saved. Please, try again.'));
			}
		}
	}
	
/**
 * add method
 *
 * @return void
 */
 	public function add($id = null) {
		if ($this->request->is('post')) {
			
			$this->request->data['Template']['modified'] = $this->App->get_current_datetime();
			$this->request->data['Template']['ip_address'] = $this->App->get_numeric_ip_representation();
			
			//pr($this->request->data);exit;
			$this->Template->create();
			if ($this->Template->save($this->request->data['Template'],array('validate' => 'first'))) {
				$template_id = $this->Template->getLastInsertId();
				foreach($this->request->data['TemplateAttributesItem'] as $key => $val) {
					if($val['template_attribute_id']!="") {
						$this->request->data['TemplateAttributesItem'][$key]['template_id'] = $template_id;
					}
				}
				$this->TemplateAttributesItem->create();
				$this->TemplateAttributesItem->saveAll($this->request->data['TemplateAttributesItem'],array('validate' => 'first'));
				$this->Session->setFlash(__('The attribute has been saved'));
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('The attribute could not be saved. Please, try again.'));
			}
		}
		
		$attributeCategories = $this->TemplateAttribute->TemplateAttributesCategory->find('all',array('conditions'=>array('TemplateAttributesCategory.is_active'=>1)));
		$this->set('attributeCategories',$attributeCategories);
		
		if(isset($attributeCategories[0]['TemplateAttributesCategory'])) {
			$this->loadAttributesByCatId( $attributeCategories[0]['TemplateAttributesCategory']['id'] );	
		}

		
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->Template->id = $id;
		if (!$this->Template->exists()) {
			throw new NotFoundException(__('Invalid template id'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Template']['modified'] = $this->App->get_current_datetime();
			$this->request->data['Template']['ip_address'] = $this->App->get_numeric_ip_representation();
			
			if ($this->Template->save($this->request->data['Template'],array('validate' => 'first'))) {
				$this->TemplateAttributesItem->deleteAll(array('TemplateAttributesItem.template_id' => $id), false);
				foreach($this->request->data['TemplateAttributesItem'] as $key => $val) {
					if($val['template_attribute_id']!="") {
						$this->request->data['TemplateAttributesItem'][$key]['template_id'] = $id;
					}
				}
				$this->TemplateAttributesItem->create();
				$this->TemplateAttributesItem->saveAll($this->request->data['TemplateAttributesItem'],array('validate' => 'first'));
				//pr($this->request->data);exit;
				$this->Session->setFlash(__('The template attribute has been saved'));
				$this->redirect(array('action' => 'index',$emp_id));
			} else {
				$this->Session->setFlash(__('The template attribute could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Template->read(null, $id);
			
			//pr($result);exit;
			$obj_template = new DtTemplate($result[$this->modelClass]);
			$obj_template->add_division($result['Division']);
			$this->request->data[$this->modelClass] = $obj_template->get_field();
			$this->request->data['Division'] = $obj_template->get_divisions();
			
			$data = array();
			foreach($result['TemplateAttributesItem'] as $key => $val) {
				$templateAttribute = $this->TemplateAttribute->find('first', array(
					'conditions' => array('TemplateAttribute.id' => $val['template_attribute_id'])
				));
				$obj_template->add_template_attributes_item($val);
				$obj_template->add_template_attribute($templateAttribute['TemplateAttribute']);
				$data[$key]['TemplateAttributesItem'] = $obj_template->get_attribute_items();
				$data[$key]['TemplateAttribute'] = $obj_template->get_attributes();
				
			}
			unset($result['TemplateAttributesItem']);
			$this->request->data['TemplateAttributesItem'] = $data;
			
			$attributeCategories = $this->TemplateAttribute->TemplateAttributesCategory->find('all',array('conditions'=>array('TemplateAttributesCategory.is_active'=>1)));
			$this->set('attributeCategories',$attributeCategories);
			
			if(isset($attributeCategories[0]['TemplateAttributesCategory'])) {
				$this->loadAttributesByCatId( $attributeCategories[0]['TemplateAttributesCategory']['id'] );	
			}

			//pr($this->request->data);exit;
		}
		
		
	}

	public function loadAttributesByCatId( $id = null ) {
		$this->autorender = false;
		$attributes = $this->TemplateAttribute->find('all',array('conditions'=>array('TemplateAttribute.is_active'=>1,'TemplateAttribute.category_id'=>$id)));
		
		$this->set('attributes',$attributes);
		
		if($this->request->is('ajax')){
			$this->layout = false;
			$this->render('/Elements/attributes-list');
		}
	}
	
/**
 * duplicate method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function duplicate($id = null) {
		$this->Template->id = $id;
		if (!$this->Template->exists()) {
			throw new NotFoundException(__('Invalid template id'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Template']['modified'] = $this->App->get_current_datetime();
			$this->request->data['Template']['ip_address'] = $this->App->get_numeric_ip_representation();
			$this->Template->create();
			if ($this->Template->save($this->request->data['Template'],array('validate' => 'first'))) {
				$template_id = $this->Template->getLastInsertId();
				foreach($this->request->data['TemplateAttributesItem'] as $key => $val) {
					if($val['template_attribute_id']!="") {
						$this->request->data['TemplateAttributesItem'][$key]['template_id'] = $template_id;
					}
				}
				$this->TemplateAttributesItem->create();
				$this->TemplateAttributesItem->saveAll($this->request->data['TemplateAttributesItem'],array('validate' => 'first'));
				//pr($this->request->data);exit;
				$this->Session->setFlash(__('The template attribute has been saved'));
				$this->redirect(array('action' => 'index',$emp_id));
			} else {
				$this->Session->setFlash(__('The template attribute could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Template->read(null, $id);
			
			//pr($result);exit;
			$obj_template = new DtTemplate($result[$this->modelClass]);
			$obj_template->add_division($result['Division']);
			$this->request->data[$this->modelClass] = $obj_template->get_field();
			$this->request->data['Division'] = $obj_template->get_divisions();
			
			$data = array();
			foreach($result['TemplateAttributesItem'] as $key => $val) {
				$templateAttribute = $this->TemplateAttribute->find('first', array(
					'conditions' => array('TemplateAttribute.id' => $val['template_attribute_id'])
				));
				$obj_template->add_template_attributes_item($val);
				$obj_template->add_template_attribute($templateAttribute['TemplateAttribute']);
				$data[$key]['TemplateAttributesItem'] = $obj_template->get_attribute_items();
				$data[$key]['TemplateAttribute'] = $obj_template->get_attributes();
				
			}
			unset($result['TemplateAttributesItem']);
			$this->request->data['TemplateAttributesItem'] = $data;
			
			//pr($this->request->data);exit;
		}
		
		
	}	
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Template->remove($data)) {
			$this->Session->setFlash(__('Template Attribute deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Template Attribute was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
