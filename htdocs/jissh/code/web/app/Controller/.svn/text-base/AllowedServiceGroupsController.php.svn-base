<?php

App::uses('AppController', 'Controller');

/**
 * AllowedServiceGroups Controller
 *
 * @property AllowedServiceGroup $AllowedServiceGroup
 */
class AllowedServiceGroupsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'AllowedServiceGroup.name' => 'asc'
		),
		'conditions' => array('AllowedServiceGroup.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->AllowedServiceGroup->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['AllowedServiceGroup']['q'];
			$cond=array('or'=>array("AllowedServiceGroup.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'AllowedServiceGroup.name' => 'asc'
				),
				'conditions' => array('AllowedServiceGroup.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$allowed_service_groups = array();
		foreach ($data as $key => $allowed_service_group) {
			$obj_allowed_service_groups = new DtAllowedServiceGroup($allowed_service_group[$this->modelClass]);
			$allowed_service_groups[$key][$this->modelClass] = $obj_allowed_service_groups->get_field();
		}

		$this->set(compact('allowed_service_groups'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->AllowedServiceGroup->id = $id;
		if (!$this->AllowedServiceGroup->exists()) {
			throw new NotFoundException(__('Invalid allowed_service_group'));
		}

		$allowed_service_group = array();

		$result = $this->AllowedServiceGroup->get_by_id($id);

		$obj_allowed_service_group = new DtAllowedServiceGroup($result[$this->modelClass]);
		$allowed_service_group[$this->modelClass] = $obj_allowed_service_group->get_field();

		$this->set(compact('allowed_service_group'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->AllowedServiceGroup->set($this->request->data);
			if ($this->AllowedServiceGroup->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->AllowedServiceGroup->create($data)) {
					$this->Session->setFlash(__('The AllowedServiceGroup has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The AllowedServiceGroup could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The AllowedServiceGroup could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->AllowedServiceGroup->id = $id;
		if (!$this->AllowedServiceGroup->exists()) {
			throw new NotFoundException(__('Invalid allowed_service_group'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->AllowedServiceGroup->set($this->request->data);
			if ($this->AllowedServiceGroup->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->AllowedServiceGroup->update($data)) {
					$this->Session->setFlash(__('The AllowedServiceGroup has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The AllowedServiceGroup could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The AllowedServiceGroup could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->AllowedServiceGroup->get_by_id($id);
			$obj_allowed_service_group = new DtAllowedServiceGroup($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_allowed_service_group->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->AllowedServiceGroup->remove($data)) {
			$this->Session->setFlash(__('AllowedServiceGroup deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('AllowedServiceGroup was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
