<?php

App::uses('AppController', 'Controller');

/**
 * RelationShips Controller
 *
 * @property RelationShip $RelationShip
 */
class RelationShipsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'RelationShip.name' => 'asc'
		),
		'conditions' => array('RelationShip.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->RelationShip->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['RelationShip']['q'];
			$cond=array("RelationShip.name LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'RelationShip.name' => 'asc'
				),
				'conditions' => array('RelationShip.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		
		$relation_ships = array();
		foreach ($data as $key => $relation_ship) {
			$obj_relation_ships = new DtRelationShip($relation_ship[$this->modelClass]);
			$relation_ships[$key][$this->modelClass] = $obj_relation_ships->get_field();
		}

		$this->set(compact('relation_ships'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->RelationShip->id = $id;
		if (!$this->RelationShip->exists()) {
			throw new NotFoundException(__('Invalid relation_ship'));
		}
		$this->set('relation_ship', $this->RelationShip->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->RelationShip->set($this->request->data);
			if ($this->RelationShip->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->RelationShip->create($data)) {
					$this->Session->setFlash(__('The RelationShip has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The RelationShip could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The RelationShip could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->RelationShip->id = $id;
		if (!$this->RelationShip->exists()) {
			throw new NotFoundException(__('Invalid relation_ship'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->RelationShip->set($this->request->data);
			if ($this->RelationShip->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->RelationShip->update($data)) {
					$this->Session->setFlash(__('The relation_ship has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The relation_ship could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The relation_ship could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->RelationShip->get_by_id($id);
			$obj_relation_ship = new DtRelationShip($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_relation_ship->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->RelationShip->remove($data)) {
			$this->Session->setFlash(__('RelationShip deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('RelationShip was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}