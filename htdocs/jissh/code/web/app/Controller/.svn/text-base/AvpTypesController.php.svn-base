<?php

App::uses('AppController', 'Controller');

/**
 * AvpTypes Controller
 *
 * @property AvpType $AvpType
 */
class AvpTypesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'AvpType.name' => 'asc'
		),
		'conditions' => array('AvpType.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->AvpType->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['AvpType']['q'];
			$cond=array('or'=>array("AvpType.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'AvpType.name' => 'asc'
				),
				'conditions' => array('AvpType.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$avp_types = array();
		foreach ($data as $key => $avp_type) {
			$obj_avp_types = new DtAvpType($avp_type[$this->modelClass]);
			$avp_types[$key][$this->modelClass] = $obj_avp_types->get_field();
		}

		$this->set(compact('avp_types'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->AvpType->id = $id;
		if (!$this->AvpType->exists()) {
			throw new NotFoundException(__('Invalid avp_type'));
		}

		$avp_type = array();

		$result = $this->AvpType->get_by_id($id);

		$obj_avp_type = new DtAvpType($result[$this->modelClass]);
		$avp_type[$this->modelClass] = $obj_avp_type->get_field();

		$this->set(compact('avp_type'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->AvpType->set($this->request->data);
			if ($this->AvpType->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->AvpType->create($data)) {
					$this->Session->setFlash(__('The AvpType has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The AvpType could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The AvpType could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->AvpType->id = $id;
		if (!$this->AvpType->exists()) {
			throw new NotFoundException(__('Invalid avp_type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->AvpType->set($this->request->data);
			if ($this->AvpType->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->AvpType->update($data)) {
					$this->Session->setFlash(__('The AvpType has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The AvpType could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The AvpType could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->AvpType->get_by_id($id);
			$obj_avp_type = new DtAvpType($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_avp_type->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->AvpType->remove($data)) {
			$this->Session->setFlash(__('AvpType deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('AvpType was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
