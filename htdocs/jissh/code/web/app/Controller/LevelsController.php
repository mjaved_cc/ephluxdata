<?php

App::uses('AppController', 'Controller');

/**
 * Levels Controller
 *
 * @property Level $Level
 */
class LevelsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Level.name' => 'asc'
		),
		'conditions' => array('Level.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Level->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Level']['q'];
			$cond=array('or'=>array("Level.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Level.name' => 'asc'
				),
				'conditions' => array('Level.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$levels = array();
		foreach ($data as $key => $level) {
			$obj_levels = new DtLevel($level[$this->modelClass]);
			$levels[$key][$this->modelClass] = $obj_levels->get_field();
		}

		$this->set(compact('levels'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Level->id = $id;
		if (!$this->Level->exists()) {
			throw new NotFoundException(__('Invalid level'));
		}
		$this->set('level', $this->Level->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Level->set($this->request->data);
			if ($this->Level->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Level->create($data)) {
					$this->Session->setFlash(__('The Level has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Level could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Level could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Level->id = $id;
		if (!$this->Level->exists()) {
			throw new NotFoundException(__('Invalid level'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Level->set($this->request->data);
			if ($this->Level->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Level->update($data)) {
					$this->Session->setFlash(__('The level has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The level could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The level could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Level->get_by_id($id);
			$obj_level = new DtLevel($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_level->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Level->remove($data)) {
			$this->Session->setFlash(__('Level deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Level was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
