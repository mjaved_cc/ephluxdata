<?php

App::uses('AppController', 'Controller');

/**
 * Features Controller
 *
 * @property Feature $Feature
 */
class FeaturesController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Feature.description' => 'asc'
		),
		'conditions' => array('Feature.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Feature->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Feature']['q'];
			$cond=array("Feature.description LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Feature.description' => 'asc'
				),
				'conditions' => array('Feature.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$features = array();
		foreach ($data as $key => $feature) {
			$obj_features = new DtFeature($feature[$this->modelClass]);
			$features[$key][$this->modelClass] = $obj_features->get_field();
		}

		$this->set(compact('features'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Feature->id = $id;
		if (!$this->Feature->exists()) {
			throw new NotFoundException(__('Invalid feature'));
		}
		$feature = array();

		$result = $this->Feature->get_details_by_id($id);

		$obj_feature = new DtFeature($result[0][$this->modelClass]);
		$feature[$this->modelClass] = $obj_feature->get_field();

		foreach ($result as $key => $value) {
			$obj_feature->add_auth_action_map($value['AuthActionMap']);
		}

		if (is_array($obj_feature->AuthActionMap)) {
			foreach ($obj_feature->AuthActionMap as $key => $value) {
				$feature['AuthActionMap'][$key] = $value->get_field();
			}
		} else if ($obj_feature->AuthActionMap instanceof DtAuthActionMap) {
			// just to have consistency in view
			$feature['AuthActionMap'][0] = $obj_feature->AuthActionMap->get_field();
		}
		
		$this->set(compact('feature'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Feature->set($this->request->data);
			if ($this->Feature->validates(array('fieldList' => array('description')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->Feature->create($data)) {
					$this->Session->setFlash(__('The feature has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The feature could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The feature could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Feature->id = $id;
		if (!$this->Feature->exists()) {
			throw new NotFoundException(__('Invalid feature'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Feature->set($this->request->data);
			if ($this->Feature->validates(array('fieldList' => array('description')))) {
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				if ($this->Feature->update($data)) {
					$this->Session->setFlash(__('The feature has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The feature could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The feature could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Feature->get_by_id($id);
			$obj_feature = new DtFeature($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_feature->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Feature->remove($data)) {
			$this->Session->setFlash(__('Feature deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Feature was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
