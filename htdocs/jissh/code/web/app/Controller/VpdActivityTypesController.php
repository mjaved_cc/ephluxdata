<?php
App::uses('AppController', 'Controller');
/**
 * VpdActivityTypes Controller
 *
 * @property VpdActivityType $VpdActivityType
 */
class VpdActivityTypesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'VpdActivityType.name' => 'asc'
		),
		'conditions' => array('VpdActivityType.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->VpdActivityType->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['VpdActivityType']['q'];
			$cond=array('or'=>array("VpdActivityType.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'VpdActivityType.name' => 'asc'
				),
				'conditions' => array('VpdActivityType.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$vpdActivityTypes = array();
		foreach ($data as $key => $VpdActivityType) {
			$obj_VpdActivityTypes = new DtVpdActivityType($VpdActivityType[$this->modelClass]);
			$vpdActivityTypes[$key][$this->modelClass] = $obj_VpdActivityTypes->get_field();
		}

		$this->set(compact('vpdActivityTypes'));
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	
	public function view($id = null) {
		$this->VpdActivityType->id = $id;
		if (!$this->VpdActivityType->exists()) {
			throw new NotFoundException(__('Invalid program'));
		}

		$vpdActivityType = array();

		$result = $this->VpdActivityType->get_by_id($id);

		$obj_DtVpdActivityType = new DtVpdActivityType($result[$this->modelClass]);
		$vpdActivityType[$this->modelClass] = $obj_DtVpdActivityType->get_field();

		$this->set(compact('vpdActivityType'));
	}
	
	

/**
 * add method
 *
 * @return void
 */
 	
	public function add() {
		if ($this->request->is('post')) {

			$this->VpdActivityType->set($this->request->data);
			if ($this->VpdActivityType->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->VpdActivityType->create($data)) {
					$this->Session->setFlash(__('The VpdActivityType has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The VpdActivityType could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The VpdActivityType could not be saved. Please, try again.'));
			}
		}
	}
	
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
 	public function edit($id = null) {
		$this->VpdActivityType->id = $id;
		if (!$this->VpdActivityType->exists()) {
			throw new NotFoundException(__('Invalid VpdActivityType'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->VpdActivityType->set($this->request->data);
			if ($this->VpdActivityType->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->VpdActivityType->update($data)) {
					$this->Session->setFlash(__('The VpdActivityType has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The VpdActivityType could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The VpdActivityType could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->VpdActivityType->get_by_id($id);
			$obj_VpdActivityType = new DtVpdActivityType($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_VpdActivityType->get_field();
		}
	}
	

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->VpdActivityType->remove($data)) {
			$this->Session->setFlash(__('VpdActivityType deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('VpdActivityType was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
