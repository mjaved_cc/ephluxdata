<?php
App::uses('AppController', 'Controller');
/**
 * CaseLoads Controller
 *
 * @property CaseLoad $CaseLoad
 */
class CaseLoadsController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'CaseLoad.name' => 'asc'
		),
		'conditions' => array('CaseLoad.is_active' => IS_ACTIVE)
	);
		
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->CaseLoad->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['CaseLoad']['q'];
			$cond=array('or'=>array("CaseLoad.name LIKE '%$keyword%'") );
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'CaseLoad.name' => 'asc'
				),
				'conditions' => array('CaseLoad.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$caseLoads = array();
		foreach ($data as $key => $caseLoad) {
			$obj_caseLoads = new DtCaseLoad($caseLoad[$this->modelClass]);
			$caseLoads[$key][$this->modelClass] = $obj_caseLoads->get_field();
		}

		$this->set(compact('caseLoads'));
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function view($id = null) {
		$this->CaseLoad->id = $id;
		if (!$this->CaseLoad->exists()) {
			throw new NotFoundException(__('Invalid CaseLoad'));
		}

		$CaseLoad = array();

		$result = $this->CaseLoad->get_by_id($id);

		$obj_CaseLoad = new DtCaseLoad($result[$this->modelClass]);
		$CaseLoad[$this->modelClass] = $obj_CaseLoad->get_field();

		$this->set(compact('CaseLoad'));
	}
	

/**
 * add method
 *
 * @return void
 */
 	public function add() {
		if ($this->request->is('post')) {

			$this->CaseLoad->set($this->request->data);
			if ($this->CaseLoad->validates(array('fieldList' => array('name', 'description')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->CaseLoad->create($data)) {
					$this->Session->setFlash(__('The CaseLoad has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The CaseLoad could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The CaseLoad could not be saved. Please, try again.'));
			}
		}
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->CaseLoad->id = $id;
		if (!$this->CaseLoad->exists()) {
			throw new NotFoundException(__('Invalid CaseLoad'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->CaseLoad->set($this->request->data);
			if ($this->CaseLoad->validates(array('fieldList' => array('name', 'description')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();				
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->CaseLoad->update($data)) {
					$this->Session->setFlash(__('The CaseLoad has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The CaseLoad could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The CaseLoad could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->CaseLoad->get_by_id($id);
			$obj_CaseLoad = new DtCaseLoad($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_CaseLoad->get_field();
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();

		if ($this->CaseLoad->remove($data)) {
			$this->Session->setFlash(__('CaseLoad deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('CaseLoad was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
}
