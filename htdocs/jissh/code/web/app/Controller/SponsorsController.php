<?php

App::uses('AppController', 'Controller');

/**
 * Sponsors Controller
 *
 * @property Sponsor $Sponsor
 */
class SponsorsController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Sponsor.name' => 'asc'
		),
		'conditions' => array('Sponsor.is_active' => IS_ACTIVE)
	);
	
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Sponsor->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['Sponsor']['q'];
			$cond=array("Sponsor.name LIKE '%$keyword%'");
			//$data = find('all',array('conditions')=>$cond);
			$this->paginate = array(
				'limit' => 25,
				'order' => array(
					'Sponsor.name' => 'asc'
				),
				'conditions' => array('Sponsor.is_active' => IS_ACTIVE, $cond)
			);
			$this->request->params['named']['q'] = $keyword;
			$data = $this->paginate();
		}
		else {
			$data = $this->paginate();
			
		}
		$sponsors = array();
		foreach ($data as $key => $sponsor) {
			$obj_sponsors = new DtSponsor($sponsor[$this->modelClass]);
			$sponsors[$key][$this->modelClass] = $obj_sponsors->get_field();
		}

		$this->set(compact('sponsors'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Sponsor->id = $id;
		if (!$this->Sponsor->exists()) {
			throw new NotFoundException(__('Invalid sponsor'));
		}
		
		$sponsor = array();

		$result = $this->Sponsor->get_by_id($id);

		$obj_sponsor = new DtSponsor($result[$this->modelClass]);
		$sponsor[$this->modelClass] = $obj_sponsor->get_field();

		$this->set(compact('sponsor'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Sponsor->set($this->request->data);
			if ($this->Sponsor->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Sponsor->create($data)) {
					$this->Session->setFlash(__('The sponsor has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The sponsor could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The sponsor could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Sponsor->id = $id;
		if (!$this->Sponsor->exists()) {
			throw new NotFoundException(__('Invalid sponsor'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Sponsor->set($this->request->data);
			if ($this->Sponsor->validates(array('fieldList' => array('name')))) {
				
				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				
				if ($this->Sponsor->update($data)) {
					$this->Session->setFlash(__('The sponsor has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The sponsor could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The sponsor could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Sponsor->get_by_id($id);
			$obj_sponsor = new DtSponsor($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_sponsor->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Sponsor->remove($data)) {
			$this->Session->setFlash(__('Sponsor deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Sponsor was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
