<?php
App::uses('AppController', 'Controller');
/**
 * OfficialTitles Controller
 *
 * @property OfficialTitle $OfficialTitle
 */
class OfficialTitlesController extends AppController {
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'OfficialTitle.name' => 'asc'
		),
		'conditions' => array('OfficialTitle.is_active' => IS_ACTIVE)
	);
		
	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OfficialTitle->recursive = 0;
		
		$data = $this->paginate();
		$officialTitles = array();
		foreach ($data as $key => $OfficialTitle) {
			$obj_OfficialTitles = new DtCaseLoad($OfficialTitle[$this->modelClass]);
			$officialTitles[$key][$this->modelClass] = $obj_OfficialTitles->get_field();
		}

		$this->set(compact('officialTitles'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function view($id = null) {
		$this->OfficialTitle->id = $id;
		if (!$this->OfficialTitle->exists()) {
			throw new NotFoundException(__('Invalid OfficialTitle'));
		}

		$CaseLoad = array();

		$result = $this->OfficialTitle->get_by_id($id);

		$obj_OfficialTitle = new DtOfficialTitle($result[$this->modelClass]);
		$OfficialTitle[$this->modelClass] = $obj_OfficialTitle->get_field();

		$this->set(compact('OfficialTitle'));
	}
	
/**
 * add method
 *
 * @return void
 */
 	public function add() {
		if ($this->request->is('post')) {

			$this->OfficialTitle->set($this->request->data);
			if ($this->OfficialTitle->validates(array('fieldList' => array('name', 'description')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->OfficialTitle->create($data)) {
					$this->Session->setFlash(__('The OfficialTitle has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The OfficialTitle could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The OfficialTitle could not be saved. Please, try again.'));
			}
		}
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->OfficialTitle->id = $id;
		if (!$this->OfficialTitle->exists()) {
			throw new NotFoundException(__('Invalid OfficialTitle'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->OfficialTitle->set($this->request->data);
			if ($this->OfficialTitle->validates(array('fieldList' => array('name', 'description')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();				
				$data['description'] = $this->request->data[$this->modelClass]['description'];

				if ($this->OfficialTitle->update($data)) {
					$this->Session->setFlash(__('The OfficialTitle has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The OfficialTitle could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The OfficialTitle could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->OfficialTitle->get_by_id($id);
			$obj_OfficialTitle = new DtOfficialTitle($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_OfficialTitle->get_field();
		}
	}
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();

		if ($this->OfficialTitle->remove($data)) {
			$this->Session->setFlash(__('OfficialTitle deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('OfficialTitle was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
}
