<?php
App::uses('AppController', 'Controller');
/**
 * PatientDetails Controller
 *
 * @property PatientDetail $PatientDetail
 */
class PatientDetailsController extends AppController {
	
	var $uses = array('PatientDetail','RelationShip','User','Religion','Country');
	
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'PatientDetail.first_name' => 'asc'
		),
		'conditions' => array('User.status' => STATUS_ACTIVE, 'User.user_type' => USER_TYPE_PATIENT)
	);
	
	function beforeRender() {

		$this->set('page_heading', 'Patient');

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}
	
/**
 * index method
 *
 * @return void
 */
 	public function index() {
		$this->PatientDetail->recursive = 0;
		
		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['PatientDetail']['q'];
			//$data = find('all',array('conditions')=>$cond);
			$conditons = array('OR' => array(
					'PatientDetail.first_name LIKE' => '%' . $keyword . '%',
					'PatientDetail.last_name LIKE' => '%' . $keyword . '%'
					));
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
			
			$this->request->params['named']['q'] = $keyword;
		}
		
		$data = $this->paginate();
			
		$patientDetails = array();
		foreach ($data as $key => $PatientDetail) {
			$obj_user = new DtUser($PatientDetail['User']);
			$obj_user->add_patient_detail($PatientDetail[$this->modelClass]);
			
			/* Retrieve data from rendering logic */
			$data = array();
			$data['User'] = $obj_user->get_field();

			$patientDetails[$key]['User'] = $obj_user->get_field();
			if ($obj_user->PatientDetail instanceof DtPatientDetail) {
				$patientDetails[$key][$this->modelClass] = $obj_user->PatientDetail->get_field();
			}
			
		}
		$this->set(compact('patientDetails'));
	}
	
	/**
	 * medication_history method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function medication_history($id=null) {
		if (!$this->PatientDetail->exists($id)) {
			throw new NotFoundException(__('Invalid patient detail'));
		}
		
		$this->loadModel('PatientMedication');
		//$this->loadModel('PatientMedicationDetail');
		$this->paginate = array(
			'PatientMedication' => array(
				'conditions' => array('PatientMedication.patient_id' => $id),
				'limit' => 1
			)
		
		);
		
		$result = $this->paginate('PatientMedication');
		//pr($result);
		$data = array();
		foreach($result as $key => $patientDetail) {
			$obj_user = new DtUser($patientDetail['User']);
			$obj_user->add_patient_detail($patientDetail['PatientDetail']);
			$obj_user->add_employee_detail($patientDetail['EmployeeDetail']);
			/* Retrieve data from rendering logic */
			$data[$key]['User'] = $obj_user->get_field();
			$data[$key]['PatientDetail'] = $obj_user->PatientDetail->get_field();
			
			if(!empty($result[$key]['EmployeeDetail'])) {
				$data[$key]['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
			}
			
			if(!empty($result[$key]['PatientMedication'])) {
				$obj_user->PatientDetail->add_medication($patientDetail['PatientMedication']);
				$data[$key]['PatientMedication'] = $obj_user->PatientDetail->get_medication();
			}
			if(!empty($result[$key]['Appointment'])) {
				$obj_user->PatientDetail->add_appointment($patientDetail['Appointment']);
				$data[$key]['Appointment'] = $obj_user->PatientDetail->get_appointments();
			}
		
		}
		$this->set('patientMedications',$data);
	}
	
	/**
	 * medication_details method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function medication_details($id=null) {
		$this->loadModel('PatientMedication');
		//$this->loadModel('PatientMedicationDetail');
		
		if (!$this->PatientMedication->exists($id)) {
			throw new NotFoundException(__('Invalid medication id'));
		}
		
		$result = $this->PatientMedication->read(null, $id);
		$data = array();
		
		$obj_user = new DtUser($result['User']);
		$obj_user->add_patient_detail($result['PatientDetail']);
		$obj_user->add_employee_detail($result['EmployeeDetail']);
		/* Retrieve data from rendering logic */
		$data['User'] = $obj_user->get_field();
		$data['PatientDetail'] = $obj_user->PatientDetail->get_field();
		
		if(!empty($result['EmployeeDetail'])) {
			$data['EmployeeDetail'] = $obj_user->EmployeeDetail->get_field();
		}
		
		if(!empty($result['PatientMedication'])) {
			$obj_user->PatientDetail->add_medication($result['PatientMedication']);
			$data['PatientMedication'] = $obj_user->PatientDetail->get_medication();
		}
		if(!empty($result['Appointment'])) {
			$obj_user->PatientDetail->add_appointment($result['Appointment']);
			$data['Appointment'] = $obj_user->PatientDetail->get_appointments();
		}
		if(!empty($result['PatientMedicationDetail'])) {
			$obj_user->PatientDetail->add_medication_details($result['PatientMedicationDetail']);
			$data['PatientMedicationDetail'] = $obj_user->PatientDetail->get_medication_detail();
		}
		
		
		$this->set('medication_details',$data);
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->PatientDetail->id = $id;
		if (!$this->PatientDetail->exists()) {
			throw new NotFoundException(__('Invalid patient detail'));
		}
		
		$this->loadModel('Appointment');
	
		//$this->loadModel('PatientMedicationDetail');
		$this->paginate = array(
			'Appointment' => array(
				'conditions' => array('Appointment.patient_detail_id' => $id,
									  'Appointment.appointment_date < '=>date('Y-m-d H:i:s')),
				'limit' => 1
			)
		
		);
		
		$appointments = $this->paginate('Appointment');
		$data = array();
		foreach($appointments as $key => $appointment) {
			$obj_appointment = new DtAppointment($appointment['Appointment']);
			$obj_appointment->add_employee_detail($appointment['EmployeeDetail']);
			$obj_appointment->add_patient_detail($appointment['PatientDetail']);
			$obj_appointment->add_division($appointment['Division']);
			
			$data[$key]['Appointment'] = $obj_appointment->get_field();
			$data[$key]['EmployeeDetail'] = $obj_appointment->get_employee_details();
			$data[$key]['PatientDetail'] = $obj_appointment->get_patient_details();
			$data[$key]['Division'] = $obj_appointment->get_divisions();
		}
		
		$this->set('appointments',$data);
		
		if(!isset($this->request->query['page'])) {
			$result = $this->PatientDetail->read(null, $id);
			
			$obj_user = new DtUser($result['User']);
			$obj_user->add_patient_detail($result[$this->modelClass]);
			
			/* Retrieve data from rendering logic */
			$data['User'] = $obj_user->get_field();
	
			$data[$this->modelClass] = $obj_user->PatientDetail->get_field();
			
			if(!empty($result['Country'])) {
				$obj_user->PatientDetail->add_country($result['Country']);
				$data['Country'] = $obj_user->PatientDetail->get_countries();
			}
			if(!empty($result['Religion'])) {
				$obj_user->PatientDetail->add_religion($result['Religion']);
				$data['Religion'] = $obj_user->PatientDetail->get_religions();
			}
			if(!empty($result['PatientFamilyDetail'])) {
				$obj_user->PatientDetail->add_family_details($result['PatientFamilyDetail']);
				$data['PatientFamilyDetail'] = $obj_user->PatientDetail->get_family_details();
			}
			
		
			//pr($data['PatientFamilyDetail']);exit;
			if(!empty($data['PatientFamilyDetail'])) {
				foreach($data['PatientFamilyDetail'] as $key => $val) {
					$relationship = $this->RelationShip->find('first', array(
						'conditions' => array('RelationShip.id' => $val['relation_ship_id'])
					));
					if(!empty($relationship)) {
						$data['PatientFamilyDetail'][$key]['relation_ship_name'] = $relationship['RelationShip']['name'];
					}
					else {
						$data['PatientFamilyDetail'][$key]['relation_ship_name'] = '';
					}
				}
			}
			else {
				$data['PatientFamilyDetail'] = array();	
			}
			
			$this->set('PatientDetail', $data);
		
		}
		//pr($this->request->is('ajax'));
		if($this->request->is('ajax')) {
			$this->layout = 'ajax';
			$this->render("/Elements/ajax_session_listing");	
		}
		
	}

	public function saveReligion() {
		$this->autoRender = false;
		$this->layout = false;
		
		if(!empty($this->request->data) and isset($this->request->data['name'])){
			$data['created'] = $this->App->get_current_datetime();
			$data['ip_address'] = $this->App->get_numeric_ip_representation();
			$data['name'] = $this->request->data['name'];

			if ($id = $this->Religion->create($data)) {
				echo __('success|The Religion has been saved|{"id":"'.$id.'","name":"'.$data['name'].'"}');
			} else {
				echo ('failed|The Religion could not be saved. Please, try again.');
			}
		}
	}
	
	public function saveCountry() {
		$this->autoRender = false;
		$this->layout = false;
		
		if(!empty($this->request->data) and isset($this->request->data['name'])){
			$data['created'] = $this->App->get_current_datetime();
			$data['ip_address'] = $this->App->get_numeric_ip_representation();
			$data['name'] = $this->request->data['name'];
			$data['nationality'] = $this->request->data['nationality'];

			if ($id = $this->Country->create($data)) {
				echo __('success|The Country has been saved|{"id":"'.$id.'","name":"'.$data['name'].'"}');
			} else {
				echo ('failed|The Country could not be saved. Please, try again.');
			}
		}
	}
/**
 * add method
 *
 * @return void
 */
 	public function add() {
		//pr($this->request);
		//echo 'adaf';exit;
		if ($this->request->is('post')) {
			
			$created = $this->App->get_current_datetime();
			$ip_address = $this->App->get_numeric_ip_representation();
			
			$this->request->data['PatientDetail']['created'] = $created;
			$this->request->data['PatientDetail']['ip_address'] = $ip_address;
			
			$this->request->data['User']['created'] = $created;
			$this->request->data['User']['ip_address'] = $ip_address;
			$patientFamilyDetail = array();
			foreach($this->request->data['PatientFamilyDetail'] as $key => $val) {
				if($val['name']!="") {
					$patientFamilyDetail[$key] = $val;
					$patientFamilyDetail[$key]['created'] = $created;
					$patientFamilyDetail[$key]['ip_address'] = $ip_address;
				}
			}
			unset($this->request->data['PatientFamilyDetail']);
			if(!empty($patientFamilyDetail)) {
				$this->request->data['PatientFamilyDetail'] = $patientFamilyDetail;
			}
			
			$this->request->data['User']['group_id'] = 1;
			$this->request->data['User']['user_type'] = 'patient';
				
			$this->PatientDetail->set($this->request->data);
			if ($this->PatientDetail->saveAll($this->request->data,array('validate' => 'only'))) {

				//pr($this->request->data);exit;
				$this->PatientDetail->create();
				if ($this->PatientDetail->saveAll($this->request->data)) {
					$this->Session->setFlash(__('The patient detail has been saved'));
					//pr($this->PatientDetail->getInsertID());
					if($this->request->data['next']=="waitinglists") {
						$this->redirect(array('controller'=>'WaitingListsTitles', 'action' => 'assign',$this->PatientDetail->getInsertID()));
					}
					else {
						$this->redirect(array('controller'=>'PatientDetails', 'action' => 'index'));
					}
					
				} else {
					$this->Session->setFlash(__('The patient detail could not be saved. Please, try again.'));
					$countries = $this->PatientDetail->Country->find('list');
					$this->set(compact('countries'));
					$countries = $this->PatientDetail->Country->find('list');
					$this->set(compact('countries'));
					$religions = $this->PatientDetail->Religion->find('list');
					$this->set(compact('religions'));
					$relationships = $this->RelationShip->find('list');
					
					$this->set(compact('relationships'));
				}
			} else {
				$this->Session->setFlash(__('The patient detail could not be saved. Please, try again.'));
				$countries = $this->PatientDetail->Country->find('list');
				$this->set(compact('countries'));
				$countries = $this->PatientDetail->Country->find('list');
				$this->set(compact('countries'));
				$religions = $this->PatientDetail->Religion->find('list');
				$this->set(compact('religions'));
				$relationships = $this->RelationShip->find('list');
				
				$this->set(compact('relationships'));
			}
		}
		$countries = $this->PatientDetail->Country->find('list');
		$this->set(compact('countries'));
		$countries = $this->PatientDetail->Country->find('list');
		$this->set(compact('countries'));
		$religions = $this->PatientDetail->Religion->find('list');
		$this->set(compact('religions'));
		$relationships = $this->RelationShip->find('list');
		
		$this->set(compact('relationships'));
	}
	
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function edit($id = null) {
		$this->PatientDetail->id = $id;
		
		if (!$this->PatientDetail->exists()) {
			throw new NotFoundException(__('Invalid patient detail'));
		}
		if ($this->request->is('PatientDetail') || $this->request->is('put')) {
			$next = $this->request->data['next'];
			unset($this->request->data['next']);
			$created = $this->App->get_current_datetime();
			$ip_address = $this->App->get_numeric_ip_representation();
			
			$this->request->data['PatientDetail']['modified'] = $created;
			$this->request->data['PatientDetail']['ip_address'] = $ip_address;
			
			$this->request->data['User']['modified'] = $created;
			$this->request->data['User']['ip_address'] = $ip_address;
			
			$patientFamilyDetail = array();
			foreach($this->request->data['PatientFamilyDetail'] as $key => $val) {
				if($val['name']!="") {
					$patientFamilyDetail[$key] = $val;
					$patientFamilyDetail[$key]['modified'] = $created;
					$patientFamilyDetail[$key]['ip_address'] = $ip_address;
				}
			}
			unset($this->request->data['PatientFamilyDetail']);
			if(!empty($patientFamilyDetail)) {
				$this->request->data['PatientFamilyDetail'] = $patientFamilyDetail;
			}
			
			//$this->PatientDetail->set($this->request->data);
			
			//pr($this->request->data);exit;
			if ($this->PatientDetail->saveAll($this->request->data,array('validate' => 'first'))) {
				$this->Session->setFlash(__('The patient detail has been saved'));
				if($next=="waitinglists") {
					$this->redirect(array('controller'=>'WaitingListsTitles', 'action' => 'assign',$this->request->data['PatientDetail']['id']));
				}
				else {
					$this->redirect(array('controller'=>'PatientDetails', 'action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('The patient detail could not be saved. Please, try again.'));
				$this->request->data['PatientDetail']['user_id'] = $this->request->data['User']['id'];
				
				$obj_user = new DtUser($result['User']);
				$obj_user->add_patient_detail($result[$this->modelClass]);
				$obj_user->PatientDetail->add_country($result['Country']);
				$obj_user->PatientDetail->add_religion($result['Religion']);
				$obj_user->PatientDetail->add_family_details($result['PatientFamilyDetail']);
				/* Retrieve data from rendering logic */
				$this->request->data['User'] = $obj_user->get_field();
	
				$this->request->data[$this->modelClass] = $obj_user->PatientDetail->get_field();
			
				$this->request->data['Country'] = $obj_user->PatientDetail->get_countries();
			
				$this->request->data['Religion'] = $obj_user->PatientDetail->get_religions();
			
				$this->request->data['PatientFamilyDetail'] = $obj_user->PatientDetail->get_family_details();
			
				$countries = $this->PatientDetail->Country->find('list');
				$this->set(compact('countries'));
				$countries = $this->PatientDetail->Country->find('list');
				$this->set(compact('countries'));
				$religions = $this->PatientDetail->Religion->find('list');
				$this->set(compact('religions'));
				$relationships = $this->RelationShip->find('list');
				$this->set(compact('relationships'));
			}
			
		} else {
			$result = $this->PatientDetail->read(null, $id);
			
			$obj_user = new DtUser($result['User']);
			$obj_user->add_patient_detail($result[$this->modelClass]);
			$obj_user->PatientDetail->add_country($result['Country']);
			$obj_user->PatientDetail->add_religion($result['Religion']);
			$obj_user->PatientDetail->add_family_details($result['PatientFamilyDetail']);
			/* Retrieve data from rendering logic */
			$this->request->data['User'] = $obj_user->get_field();

			$this->request->data[$this->modelClass] = $obj_user->PatientDetail->get_field();
		
			$this->request->data['Country'] = $obj_user->PatientDetail->get_countries();
		
			$this->request->data['Religion'] = $obj_user->PatientDetail->get_religions();
		
			$this->request->data['PatientFamilyDetail'] = $obj_user->PatientDetail->get_family_details();
			
			
			$countries = $this->PatientDetail->Country->find('list');
			$this->set(compact('countries'));
			$countries = $this->PatientDetail->Country->find('list');
			$this->set(compact('countries'));
			$religions = $this->PatientDetail->Religion->find('list');
			$this->set(compact('religions'));
			$relationships = $this->RelationShip->find('list');
			$this->set(compact('relationships'));
		}
	}
	
/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 	public function delete($id = NULL) {
		//pr($this->request->params['controller']);
		if (($this->request->params['controller'] != "patient_details") and ($this->request->params['action'] != "delete")) {
			throw new MethodNotAllowedException();
		}
		
		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		$data['status'] = 'discontinued';
		if ($this->PatientDetail->save($data)) {
			$this->Session->setFlash(__('patient detail deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('patient detail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
}
