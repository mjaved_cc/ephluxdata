<?php

App::uses('AppController', 'Controller');

/**
 * Modalities Controller
 *
 * @property Modality $Modality
 */
class ModalitiesController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Modality.name' => 'asc'
		),
		'conditions' => array('Modality.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Modality->recursive = 0;

		$data = $this->paginate();
		$modalities = array();
		foreach ($data as $key => $modality) {
			$obj_modalities = new DtModality($modality[$this->modelClass]);
			$modalities[$key][$this->modelClass] = $obj_modalities->get_field();
		}

		$this->set(compact('modalities'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Modality->id = $id;
		if (!$this->Modality->exists()) {
			throw new NotFoundException(__('Invalid modality'));
		}
		$this->set('modality', $this->Modality->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->Modality->set($this->request->data);
			if ($this->Modality->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->Modality->create($data)) {
					$this->Session->setFlash(__('The Modality has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Modality could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The Modality could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->Modality->id = $id;
		if (!$this->Modality->exists()) {
			throw new NotFoundException(__('Invalid modality'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Modality->set($this->request->data);
			if ($this->Modality->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->Modality->update($data)) {
					$this->Session->setFlash(__('The modality has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The modality could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The modality could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->Modality->get_by_id($id);
			$obj_modality = new DtModality($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_modality->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->Modality->remove($data)) {
			$this->Session->setFlash(__('Modality deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('Modality was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
