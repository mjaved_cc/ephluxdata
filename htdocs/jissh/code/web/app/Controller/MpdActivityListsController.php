<?php

App::uses('AppController', 'Controller');

/**
 * MpdActivityLists Controller
 *
 * @property MpdActivityList $MpdActivityList
 */
class MpdActivityListsController extends AppController {

	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'MpdActivityList.name' => 'asc'
		),
		'conditions' => array('MpdActivityList.is_active' => IS_ACTIVE)
	);

	function beforeRender() {

		$this->set('page_heading', $this->modelClass);

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->MpdActivityList->recursive = 0;

		$data = $this->paginate();
		$mpd_activity_lists = array();
		foreach ($data as $key => $mpd_activity_list) {
			$obj_mpd_activity_lists = new DtMpdActivityList($mpd_activity_list[$this->modelClass]);
			$mpd_activity_lists[$key][$this->modelClass] = $obj_mpd_activity_lists->get_field();
		}

		$this->set(compact('mpd_activity_lists'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->MpdActivityList->id = $id;
		if (!$this->MpdActivityList->exists()) {
			throw new NotFoundException(__('Invalid mpd_activity_list'));
		}
		$this->set('mpd_activity_list', $this->MpdActivityList->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {

			$this->MpdActivityList->set($this->request->data);
			if ($this->MpdActivityList->validates(array('fieldList' => array('name')))) {

				$data['created'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();
				$data['name'] = $this->request->data[$this->modelClass]['name'];

				if ($this->MpdActivityList->create($data)) {
					$this->Session->setFlash(__('The MpdActivityList has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The MpdActivityList could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The MpdActivityList could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->MpdActivityList->id = $id;
		if (!$this->MpdActivityList->exists()) {
			throw new NotFoundException(__('Invalid mpd_activity_list'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->MpdActivityList->set($this->request->data);
			if ($this->MpdActivityList->validates(array('fieldList' => array('name')))) {

				$data['id'] = $this->request->data[$this->modelClass]['id'];
				$data['name'] = $this->request->data[$this->modelClass]['name'];
				$data['description'] = $this->request->data[$this->modelClass]['description'];
				$data['modified'] = $this->App->get_current_datetime();
				$data['ip_address'] = $this->App->get_numeric_ip_representation();

				if ($this->MpdActivityList->update($data)) {
					$this->Session->setFlash(__('The mpd_activity_list has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The mpd_activity_list could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The mpd_activity_list could not be saved. Please, try again.'));
			}
		} else {
			$result = $this->MpdActivityList->get_by_id($id);
			$obj_mpd_activity_list = new DtMpdActivityList($result[$this->modelClass]);
			$this->request->data[$this->modelClass] = $obj_mpd_activity_list->get_field();
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}

		$data['id'] = $id;
		$data['modified'] = $this->App->get_current_datetime();
		$data['ip_address'] = $this->App->get_numeric_ip_representation();
		if ($this->MpdActivityList->remove($data)) {
			$this->Session->setFlash(__('MpdActivityList deleted'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Session->setFlash(__('MpdActivityList was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
