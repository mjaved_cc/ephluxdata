<?php

App::uses('AppController', 'Controller');

/**
 * EmployeeDetails Controller
 *
 * @property EmployeeDetail $EmployeeDetail
 */
class EmployeeDetailsController extends AppController {

	public $uses = array('EmployeeDetail', 'ConfigOption', 'OfficialTitle', 'LicenseType', 'Level', 'Language', 'Division', 'CaseLoad', 'AllowedService', 'User', 'EmployeeOfficialTitle', 'EmployeeCaseLoad', 'EmployeeDivision', 'EmployeeAllowedService', 'EmployeeLevel', 'UserLanguage', 'EmployeeWorkingHour', 'EmployeeWorkingPerWeekSlot', 'Tmp');
	public $components = array('ApiUser', 'ApiEmployeeDetail', 'FileUpload');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'EmployeeDetail.first_name' => 'asc'
		),
		'conditions' => array('User.status' => STATUS_ACTIVE, 'User.user_type' => USER_TYPE_DOCTOR, 'EmployeeDetail.status' => STATUS_ACTIVE)
	);

	function beforeFilter() {
		parent::beforeFilter();

		$this->set('page_heading', "Faculty");
	}

	function beforeRender() {

		// file name for indivisual views used for JS & CSS
		$file_name = $this->modelKey;
		$js_path = JS_URL . $file_name . EXTENSION_JS;
		$action_js = new File($js_path);

		// its not necessary that every view has js files
		if ($action_js->exists())
			$this->JCManager->add_js($file_name);

		parent::beforeRender();
	}

	function index() { 
		$this->EmployeeDetail->recursive = 0;

		if ($this->request->is('post') || isset($this->request->params['named']['q'])) {
			$keyword = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : $this->data['EmployeeDetail']['q'];
			$conditons = array('OR' => array(
					'EmployeeDetail.first_name LIKE' => '%' . $keyword . '%',
					'EmployeeDetail.last_name LIKE' => '%' . $keyword . '%'
					));
			$this->paginate['conditions'] = array_merge($this->paginate['conditions'], $conditons);
		}
		$data = $this->paginate();

		$employee_details = array();
		foreach ($data as $key => $employee_detail) {
			$obj_user = new DtUser($employee_detail['User']);
			$obj_user->add_employee_detail($employee_detail[$this->modelClass]);
			/* Retrieve data from rendering logic */
			$data = array();
			$data['User'] = $obj_user->get_field();

			$employee_details[$key]['User'] = $obj_user->get_field();
			if ($obj_user->EmployeeDetail instanceof DtEmployeeDetail) {
				$employee_details[$key][$this->modelClass] = $obj_user->EmployeeDetail->get_field();
			}
			/* END: Retireving */
		} 
		$this->set(compact('employee_details'));
	}

	/**
	 * Doctor profile view only
	 * 
	 * @param string|int $user_id System generated user id
	 */
	function doctor_details($user_id) {

		$json_data = $this->ApiEmployeeDetail->get_doctor_details_by_user_id($user_id);
		$response = $this->App->decoding_json($json_data);
		
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
		} else {
			$this->redirect(array('action' => 'index'));
		}
		
		// Determines whether the events on the calendar can be modified.
		// passed as param to url for fetching data from server
		$editable = 0;
		
		$this->set(compact('data', 'editable'));
		$this->JCManager->add_js(array(MRIYA_JS_PATH . 'jquery.prettyPhoto.min', MRIYA_PATH . 'gallery', 'doctor_working_hours'));
		$this->JCManager->add_css(MRIYA_PATH . 'pretty_photo');
	}

	/**
	 * Get doctor working hours by user id.
	 * 
	 * @param int|string $user_id System generated User id
	 * @param boolean $editable Determines whether the events on the calendar can be modified.
	 * 
	 * @return json Event object - Full Calender doc
	 */
	function get_doctor_working_hours($user_id, $editable = 1) {

		$json_data = $this->ApiEmployeeDetail->get_doctor_working_hours($user_id, $editable);
		$response = $this->App->decoding_json($json_data);
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
		} else {
			$data = $response['header'];
		}
		$this->set(compact('data'));
	}
	
	/**
	 * Get doctor annual vacation plans & appointments for given user id.
	 * 
	 * @param int|string $user_id System generated User id
	 * 
	 * @return json Event object - Full Calender doc
	 */
	function get_doctor_avp_and_appointments($user_id) {

		$json_data = $this->ApiEmployeeDetail->get_doctor_avp_and_appointments($user_id);
		$response = $this->App->decoding_json($json_data);
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
		} else {
			$data = $response['header'];
		}
		$this->set(compact('data'));
		$this->render('get_doctor_working_hours');
	}

	/**
	 * Register doctor
	 */
	function doctor_register() {

		if (!empty($this->request->data)) {

			$data_on_save = $this->_save_doctor_basic_info($this->request->data);
			$user_id = $data_on_save['user_id'];
			$employee_detail_id = $data_on_save['employee_detail_id'];
			if (!empty($user_id) && !empty($employee_detail_id)) {
				
				$response = $this->ApiUser->save_user_languages($this->request->data['EmployeeDetail']['languages'], $user_id);
				$response = $this->ApiEmployeeDetail->save_employee_official_titles($this->request->data['EmployeeDetail']['official_titles'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_levels($this->request->data['EmployeeDetail']['levels'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_divisions($this->request->data['EmployeeDetail']['divisions'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_allowed_services($this->request->data['EmployeeDetail']['allowed_services'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_case_loads($this->request->data['EmployeeDetail']['case_loads'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_working_hours($this->request->data['event_data_json'], $employee_detail_id);
				$email_data['first_name'] = $this->request->data['EmployeeDetail']['first_name'];
				$email_data['username'] = $this->request->data['User']['username'];
				$email_data['password'] = $data_on_save['password'];

				//$this->ApiEmployeeDetail->send_welcome_email($email_data);

				$this->redirect(array('action' => 'index'));
			}
		}
		$genders = $this->ConfigOption->get_value_by_name('gender');
		$official_titles = $this->OfficialTitle->format_drop_down();
		$divisions = $this->Division->format_drop_down();
		$languages = $this->Language->format_drop_down();
		$levels = $this->Level->format_drop_down();
		$case_loads = $this->CaseLoad->format_drop_down();
		$license_types = $this->LicenseType->format_drop_down();
		$allowed_services = $this->AllowedService->format_drop_down();

		$this->set(compact('genders', 'official_titles', 'license_types', 'levels', 'languages', 'divisions', 'case_loads', 'allowed_services'));

		$this->JCManager->add_js(array('event', 'file_upload', 'doctor_working_hours'));
	}

	/**
	 * Edit Doctor profile
	 * 
	 * @param string|int $user_id System generated user id
	 */
	function doctor_edit($user_id) {

		if (!empty($this->request->data)) {
			
			$data_on_save = $this->_save_doctor_basic_info($this->request->data);
			$user_id = $data_on_save['user_id'];
			$employee_detail_id = $data_on_save['employee_detail_id'];
			if (!empty($user_id) && !empty($employee_detail_id)) {
				$response = $this->ApiUser->save_user_languages($this->request->data['EmployeeDetail']['languages'], $user_id);
				$response = $this->ApiEmployeeDetail->save_employee_official_titles($this->request->data['EmployeeDetail']['official_titles'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_levels($this->request->data['EmployeeDetail']['levels'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_divisions($this->request->data['EmployeeDetail']['divisions'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_allowed_services($this->request->data['EmployeeDetail']['allowed_services'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_case_loads($this->request->data['EmployeeDetail']['case_loads'], $employee_detail_id);
				$response = $this->ApiEmployeeDetail->save_employee_working_hours($this->request->data['event_data_json'], $employee_detail_id);

				$this->redirect(array('action' => 'index'));
			}
		}

		$genders = $this->ConfigOption->get_value_by_name('gender');
		$official_titles = $this->OfficialTitle->format_drop_down();
		$divisions = $this->Division->format_drop_down();
		$languages = $this->Language->format_drop_down();
		$levels = $this->Level->format_drop_down();
		$case_loads = $this->CaseLoad->format_drop_down();
		$license_types = $this->LicenseType->format_drop_down();
		$allowed_services = $this->AllowedService->format_drop_down();

		$json_data = $this->ApiEmployeeDetail->get_doctor_details_by_user_id($user_id);
		
		$response = $this->App->decoding_json($json_data);
		
		if ($response['header']['code'] == ECODE_SUCCESS) {
			$data = $response['body'];
		} else {
			$this->redirect(array('action' => 'index'));
		}
		
		$this->request->data = $data;
		
		$this->request->data['EmployeeDetail']['license_type_id'] = $data['LicenseType']['id'];
		$data2 = array();
		$data2['EmployeeDetail']['employee_listing_relative_url'] = $data['EmployeeDetail']['employee_listing_relative_url'];
		$data2['EmployeeDetail']['doctor_edit_relative_url'] = '';
		$data2['EmployeeDetail']['mpd_activities_relative_url'] = $data['EmployeeDetail']['mpd_activities_relative_url'];
		$data2['EmployeeDetail']['vpd_activities_relative_url'] = $data['EmployeeDetail']['vpd_activities_relative_url'];
		$data2['EmployeeDetail']['avp_relative_url'] = $data['EmployeeDetail']['avp_relative_url'];
		
		$this->set('data',$data2);
		// selected values for several drop downs		
		$language_selected_val = $this->App->get_array_keys($data['Language']);

		$allowed_service_selected_val = $this->App->get_array_keys($data['AllowedService']);
		$case_load_selected_val = $this->App->get_array_keys($data['CaseLoad']);
		$division_selected_val = $this->App->get_array_keys($data['Division']);
		$level_selected_val = $this->App->get_array_keys($data['Level']);
		$off_title_selected_val = $this->App->get_array_keys($data['OfficialTitle']);

		$this->set(compact('genders', 'official_titles', 'license_types', 'levels', 'languages', 'divisions', 'case_loads', 'allowed_services', 'division_selected_val', 'language_selected_val', 'allowed_service_selected_val', 'case_load_selected_val', 'level_selected_val', 'off_title_selected_val'));
		$this->JCManager->add_js(array('event', 'file_upload', 'doctor_working_hours'));
		
		$this->render('/' . $this->name . '/doctor_register');
	}

	/**
	 * Save basic doctor information
	 * 
	 * @param array $data 
	 * @return int|string user id
	 */
	private function _save_doctor_basic_info($data) {

		$this->EmployeeDetail->set($data['EmployeeDetail']);
		$this->User->set($data['User']);
		$emp_detail_valid_flag = $this->EmployeeDetail->validates();
		$user_valid_flag = $this->User->validates(array('fieldList' => array('username')));
		$is_validate = $user_valid_flag && $emp_detail_valid_flag;
		
		if ($is_validate) {
			$password = $this->App->random_password();

			if (!empty($data['User']['id']))
				$this->User->id = $data['User']['id'];
			else {
				$this->User->create();
				$this->User->set('password', AuthComponent::password($password));
			}

			$this->User->set('username', $data['User']['username']);
			$this->User->set('group_id', GROUP_ID_DOCTOR);
			$this->User->set('user_type', USER_TYPE_DOCTOR);
			$this->User->set('ip_address', $this->App->get_numeric_ip_representation());
			$this->User->save();

			if (!empty($data['EmployeeDetail']['id']))
				$this->EmployeeDetail->id = $data['EmployeeDetail']['id'];
			else
				$this->EmployeeDetail->create();

			$this->EmployeeDetail->set('user_id', $this->User->id);
			$this->EmployeeDetail->set('first_name', $data['EmployeeDetail']['first_name']);
			$this->EmployeeDetail->set('last_name', $data['EmployeeDetail']['last_name']);
			$this->EmployeeDetail->set('middle_name', $data['EmployeeDetail']['middle_name']);
			$this->EmployeeDetail->set('picture_name', $data['EmployeeDetail']['picture_name']);
			$this->EmployeeDetail->set('mobile_saudi', $data['EmployeeDetail']['mobile_saudi']);
			$this->EmployeeDetail->set('landline_saudi', $data['EmployeeDetail']['landline_saudi']);
			$this->EmployeeDetail->set('mobile_homecountry', $data['EmployeeDetail']['mobile_homecountry']);
			$this->EmployeeDetail->set('landline_homecountry', $data['EmployeeDetail']['landline_homecountry']);
			$this->EmployeeDetail->set('jish_email', $data['EmployeeDetail']['jish_email']);
			$this->EmployeeDetail->set('license_type_id', $data['EmployeeDetail']['license_type_id']);
			$this->EmployeeDetail->set('license_number', $data['EmployeeDetail']['license_number']);
			$this->EmployeeDetail->set('joining_date', $data['EmployeeDetail']['joining_date']);
			$this->EmployeeDetail->set('contract_expiry_date', $data['EmployeeDetail']['contract_expiry_date']);
			$this->EmployeeDetail->set('gender', $data['EmployeeDetail']['genders']);
			$this->EmployeeDetail->set('dob', $data['EmployeeDetail']['dob']);
			$this->EmployeeDetail->set('ip_address', $this->App->get_numeric_ip_representation());
			$this->EmployeeDetail->save();

			$this->Tmp->remove_by_target_id($data['EmployeeDetail']['picture_name']);

			$return_array = array();
			$return_array['user_id'] = $this->User->id;
			$return_array['employee_detail_id'] = $this->EmployeeDetail->id;
			$return_array['password'] = $password;
			return $return_array;
		}
	}

	/**
	 * Upload profile image
	 * 
	 */
	function upload_profile_image() {
		$file = false;
		if ($this->request->is('post')) {
			$this->layout = 'ajax';
			$json_file = $this->FileUpload->post($this->data['FileUpload']);

			$file = $this->App->decoding_json($json_file);
			if (empty($file['code'])) {
				$data = $this->App->get_db_save_json($file);
				$status = $this->Tmp->save_tmp_record($file['name'], FILE_UPLOAD, $data, $this->App->get_tmp_expiry(), $this->App->get_current_datetime());  //save record in tmp table
				$this->set(compact('json_file'));
			}
		}
		$this->set(compact('file'));
	}
	
	function test(){
		
	}

}