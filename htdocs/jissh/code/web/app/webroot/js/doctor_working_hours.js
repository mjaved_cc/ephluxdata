$(document).ready(function () {
	
	// initialize validator and add the custom form submission logic
	$("#dr_save_btn").click(function(e) {		
		// client-side validation passed
		if (!e.isDefaultPrevented()) {

			EmployeeDetail.before_save();
				
			$('#validate').submit();
		}
	});
	
	EmployeeDetail.populate_divisions_for_working_hours();
	
	//FullCalendar
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
		
	App.fullCalendar = $('#calendar').fullCalendar({
		header: {
			left: '',
			title: false,			
			right: ''
			
		},
		defaultView: 'agendaWeek',		
		selectable: true,
		selectHelper: true,
		eventClick: function(event){
			
			var start = Event.format_date(event.start) ;
			var end = Event.format_date(event.end);

			$("#ue_start").val(start);   //this just populates the value into my dialog form
			$("#ue_end").val(end);
			$("#ue_title").html(event.title);
			$("#ue_id").val(event._id);
			$('#update_event .divisions').val(event.division_id);
			$('#update_event').overlay().load();
			
		},
		select: function(start, end, allDay) {
					
			$('#create_event').overlay().load();
			
			$('#ce_start').val(Event.format_date(start));
			$('#ce_end').val(Event.format_date(end));
			
			App.fullCalendar.fullCalendar('unselect');
		},
		loading: function(bool) {
			if (bool) {				
				$('#loading').show();
			}else{
				$('#loading').hide();				
			}
		},		
		editable: true,
		events: $('#url_doctor_working_hours').val()
	});		
	 
	$("#divisions").dropdownchecklist("destroy");
	$("#divisions").dropdownchecklist({ 
		width: 250,
		// The 'onComplete' option allows you to supply a callback function that is invoked when 
		// the user completes their selection and closes the dropdown
		onComplete: function(selector) {
			
			EmployeeDetail.populate_divisions_for_working_hours();
			
//			var dd_html = $('<select />').attr('class','divisions');
//
//			for( i = 0; i < selector.options.length; i++ ) {
//				if (selector.options[i].selected && (selector.options[i].value != "")) {					
//					$('<option />', {
//						value: selector.options[i].value, 
//						text: selector.options[i].text
//					}).appendTo(dd_html);
//				}
//			}
//			$('.ce_divisions').html(dd_html);
		} 
	});
	

});