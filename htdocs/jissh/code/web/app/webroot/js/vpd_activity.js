//PatientDetail = new Object();
var active = false;
var VpdActivity = { 
	initialize: function(options) { 
		
	},
	redirect: function(url){
		document.location.href=url;
	},
	clear: function(){
		$('#VpdActivityQ').val('');
	},
	search: function(obj){
		if (active) {
			return;
		}
		active = true;  
		var form = $(obj).closest('form');
		
		$.ajax({
			type: "POST",
			url: App.baseURL()+"VpdActivities/index",
			data:  form.serialize() 
			}).done(function( msg ) {
				$('#ajax_response').html(msg);
				active = false;
		});
	}
	
}

$(document).ready(function(){
	
	$('table th a, .pagination li a').live('click', function() {
		var url = $(this).attr("href");
		$('#ajax_response').load(url);
		return false;
	});	
})
