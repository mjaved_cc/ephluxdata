var Notification = Class({ 
	initialize: function(options) { 
		
	}	
});
Notification.before_save = function(){

	var client_events = Event.get_all_client_events('calendar');	
	$('#event_data_json').val("[" + client_events + "]");
	
}

Notification.change_cell_background_color = function(){
	
	$.ajax({
		type: "get",
		dataType: "json",		
		url: $('#url_doctor_working_hours').val(),
		success: function (response) {
			$(response).each(function(key , data){
				cell_class = App.full_calendar_cell_class_prefix + data.three_letter_day;				
				$('.' + App.full_calendar_cell_class_prefix + 'day.' + cell_class).addClass(data.cell_background);
			});
		}
	});
}

$(document).ready(function(){
	String.prototype.beginsWith = function (string) {
		return(this.indexOf(string) === 0);
	};
	
	$('input[name="send_to"]').click(function(){
		$('.selection_groups').hide();
		var id = $(this).val();
		$('#'+id).show();
	});	
	var users_list = '[{"name":"tahir","email":"tr.ephlux@gmail.com"},{"name":"tahir rasheed","email":"tr.ephlux@gmail.com"}]';
	obj_user = JSON.parse(users_list);
	$('#individual input[type="text"]').keyup(function(){
		var char = $(this).val();
		if(char.length>1) {
			var html='';
			for(var i=0;i<obj_user.length;i++){
				var name = obj_user[i].name;
				var email = obj_user[i].email;
				if (name.beginsWith(char)) {
					html+='<li><a href="#_" data="'+email+'" onclick="choose_email(this);">'+name+'</a></li>';
					//console.log(name);	
				}
			}
			$('#individual_list').html(html).show();
		}
	});
	
	
	
});

function choose_email(obj){
	var email = $(obj).attr('data');
	$('#emails_list').show().prepend('<span><input type="hidden" name="data[Notification][selected_emails][]" value="'+email+'" />'+email+' <a href="#_" onclick="remove_email(this);">X</a></span>');
	$('#individual input[type="text"]').val('');
	$('#individual_list').html('').hide();
}

function remove_email(obj){
	$(obj).closest('span').remove();	
}