$(window).load(function () { 
	//Setup Media Queries and load configurations for different plugins
	function setupMQ() {
	
	
		//For Tablet
		if (Modernizr.mq('screen and (min-width: 768px) and (max-width: 1024px)')) {
			$('.scrollable').jcarousel({
				visible: 6,
				scroll: 6
			});
				
			//Show menu and remove swap class
			$('#main_nav').show();
			$('.nav_toggle').removeClass("swap");
		}

		// For Phone
		else if (Modernizr.mq('screen and  (max-width: 767px)')) {
			//Scrollable
			$('.scrollable').jcarousel({
				visible: 3,
				scroll: 3
			});
				
			//Show menu and remove swap class
			$('#main_nav').hide();
			$('.nav_toggle').addClass("swap");

		}

		// For Desktops and Everything Else
		else {
			//Scrollable
			$('.scrollable').jcarousel({
				visible: 8,
				scroll: 8
			});

			//Show menu and remove swap class
			$('#main_nav').show();
			$('.nav_toggle').removeClass("swap");
		}

	}
	// Apply the MediaQuery function above
	setupMQ();

	// Change MediaQuery on resize
	$(window).resize(function () {
		setupMQ();
	});


});
