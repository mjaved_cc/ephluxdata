$(document).ready(function () {

	$("#appointment_type_mode [type=radio]").click(function(){
		
		$('div.appointment_selection_mode').hide();
		
		if($(this).val() == "0")
			$('.appointment_selection_mode').hide();
		else if($(this).val() == 1)
			$('.appointment_selection_mode').show();
		
	});
	
	$('#waiting_list_title').change(function(){
		Appointment.populate_patient_listing_via_waiting_list();
		Appointment.populate_clinician_listing_via_waiting_list();
	});	
	
	$('#search_patient').click(function(){
		Appointment.populate_patient_listing_via_keyword();
	});	
	
	$("#patient_selection_mode [type=radio]").click(function(){
		$('div.patient_selection_mode').hide();

		if($(this + ':checked').val() == Appointment.radio_wlist_value)
			Appointment.populate_clinician_listing_via_waiting_list();
		else if($(this + ':checked').val() == Appointment.radio_by_name)
			Appointment.populate_clinician_listing_via_get_all_available();
		
		$("#patient_listing_" + $(this).val()).parent().show();
	});
});