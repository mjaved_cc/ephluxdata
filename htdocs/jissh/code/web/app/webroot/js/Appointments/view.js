$(document).ready(function(){
	$('#ajax_response table th a, #ajax_response ul.pagination li a').live('click', function() {
		//alert('test');
		var url = $(this).attr("href");
		$('#ajax_response').load(url);
		return false;
	});	
	
	$('#call_logs_ajax_listing table th a, #call_logs_ajax_listing ul.pagination li a').live('click', function() {
		//alert('test');
		var url = $(this).attr("href");
		$('#call_logs_ajax_listing').load(url);
		return false;
	});	
	
	$('#ajax_response_reports table th a, #ajax_response_reports ul.pagination li a').live('click', function() {
		//alert('test');
		var url = $(this).attr("href");
		$('#ajax_response_reports').load(url);
		return false;
	});	
});

function validateAndSaveAttachment(){
	var attachment_title = $('#attachment_title').val();
	var attachment_file = $('#attachment_file').val();
	
	$('#attachment_report_form').find('.validator').find('p').html('');
	$('#attachment_report_form').find('.validator').hide();
	
	if(attachment_title==""){
		$('#attachment_title').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#attachment_title').parent().find('.validator').show();
		return false;
	}
	else if(attachment_file==""){
		$('#attachment_file').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#attachment_file').parent().find('.validator').show();
		return false;
	}
	else if (!attachment_file.match(/(?:gif|jpg|png|bmp|txt|pdf|doc|docx|xls|xlsx|rtf)$/)) {
		$('#attachment_file').parent().find('.validator').find('p').html('Please upload valid file (gif|jpg|png|bmp|txt|pdf|doc|docx|xls|xlsx|rtf).');
		$('#attachment_file').parent().find('.validator').show();
		return false;
	}
	else{
		$('#attachment_report_form').submit();
	}
}

function validateAndSaveAppointmentschedule(){
	var appointment_date = $('#appointment_date').val();
	
	$('#re_shcedule_form').find('.validator').find('p').html('');
	$('#re_shcedule_form').find('.validator').hide();
	
	if(appointment_date==""){
		$('#appointment_date').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#appointment_date').parent().find('.validator').show();
		return false;
	}
	
	else{
		$('#re_shcedule_form').submit();
	}
}

