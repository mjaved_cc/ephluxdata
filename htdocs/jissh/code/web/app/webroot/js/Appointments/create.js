$(document).ready(function () {
	
	// initialize validator and add the custom form submission logic
	$("#btn_finish").click(function(e) {		
		// client-side validation passed
		if (!e.isDefaultPrevented()) {
			if($('#appointment_doctor_listing [type=radio]:checked').length==0){
				alert('Please select doctor');
				return false;	
			}
			
			if($('#pat_selection_list [type=radio]:checked').length==0){
				alert('Please select patient');
				return false;	
			}
			
			Appointment.before_submit();
				
			 // $('#validate').submit();
		}
	});
	
	$('#waiting_list_title').change(function(){
		Appointment.populate_patient_listing_via_waiting_list();
		Appointment.populate_clinician_listing_via_waiting_list();
	});	
	
	$('#search_patient').click(function(){
		Appointment.populate_patient_listing_via_keyword();
	});	
	
	$("#patient_selection_mode [type=radio]").click(function(){
		$('div.patient_selection_mode').hide();

		if($(this + ':checked').val() == Appointment.radio_wlist_value)
			Appointment.populate_clinician_listing_via_waiting_list();
		else if($(this + ':checked').val() == Appointment.radio_by_name)
			Appointment.populate_clinician_listing_via_get_all_available();
		
		$("#patient_listing_" + $(this).val()).parent().show();
	});
	
	$('input[name="service_group"]').click(function(){
		
		$.ajax({
			type: "get",
			dataType: "html",		
			url: App.extendUrl(
				App.getUrl({
					controller:'appointments',
					action: 'get_available_doctors_by_service_group'
				}) , {
					service_group_id : $(this).val()
				}),
			success: function (response) {
				App.display_ajax_indicator($('input[name="service_group"]').parent());			
				$('#appointment_doctor_listing').html(response);
				App.remove_ajax_indicator();
			}
		});	
	})
	
	$('#cal_avp_appointments').fullCalendar({
		header: {			
			title: false			
		},
		defaultView: 'month',						
		loading: function(bool) {
			if (bool) {				
				$('#loading').show();
				EmployeeDetail.change_cell_background_color();
			}else{
				$('#loading').hide();				
			}
		},		
		editable: false,		
		//		events: $('#url_doctor_avp_appointments').val(),
		dayClick: function(date, allDay, jsEvent, view) {
			
			
			$('#slot_date').val(Appointment.format_date(date));	
			Appointment.get_available_slots(date);
			$('#appointment_selection').overlay().load();
		}
		
	});
});