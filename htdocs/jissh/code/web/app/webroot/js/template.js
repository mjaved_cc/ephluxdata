PatientDetail = new Object();
//var attributes = new Array();
var Template = { 
	initialize: function(options) { 
		
	}	
}

function submitMedication(template_id, patient_id, employee_id, appointment_id) {
	$('#template_id').val(template_id);
	$('#patient_id').val(patient_id);
	$('#employee_id').val(employee_id);
	$('#appointment_id').val(appointment_id);
	
	$('#templateAssign').submit();
}

function removeRowsa(obj) {
	//console.log($(obj).closest('tr'))
	$(obj).closest('tr').remove();
	changeOrder();
}

function changeOrder() {
	console.log('called');
	var i=1;
	$('#attributes_list tbody tr').each(function(){
		
		console.log($(this).find('.orderClass').val());
		$(this).find('td').eq(2).html(i);
		$(this).find('.orderClass').val(i);
		i++;
	})
}

$(function() {
	var foo = $("#attributes_list tbody").sortable({
		delay: 300,
		containerSelector: 'tbody',
		itemSelector: 'tr',
		placeholder: '<tr class="placeholder"/>',
		stop: function(event, ui) {
			//$('#lock-click').show();
			//var fruitOrder = $(this).sortable('toArray');
			changeOrder();
		}
	});	
});

tinyMCE.baseURL = App.baseURL()+"/js/tinymce/";

tinyMCE.init({
	theme : "modern",
    selector: ".tiny_text",
    plugins: [
        "table"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});

/*tinyMCE.init({
	selector: ".tiny_text",
	theme : "modern",
	mode : "textareas",
	convert_urls : false,
	theme_advanced_buttons1 : "bold,italic,underline,blockquote,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink",
	theme_advanced_buttons2: "table",
	theme_advanced_buttons3: "",
	theme_advanced_buttons4: "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left"
});*/
	

var active = false;
$(document).ready(function(){

	$('#ajax_search_btn,#cancel_search a').live('click',function(){
		if (active) {
			return;
		}
		active = true;  
		searchF();
	});
	
})

function loadAttributesByCatId(id,obj) {
	$(obj).closest('ul').find('li').each(function(){
		$(this).removeClass('attribute-cat-active');
	})
	$(obj).closest('li').addClass('attribute-cat-active');
	
	$.ajax({
		type: "POST",
		url: App.baseURL()+"Templates/loadAttributesByCatId/"+id,
		data:  {}
		}).done(function( msg ) {
			$('#ajax_attributes').html(msg);
	});
}

function searchF() {
	var form = $('#validate');
	
	$.ajax({
		type: "POST",
		url: App.baseURL()+"Templates/medicationSearch",
		data:  form.serialize() 
		}).done(function( msg ) {
			$('#ajax_response').html(msg);
			active = false;
	});	
}

function onSbumit() {
	if (active) {
		return;
	}
	active = true;
	searchF();
	
	return false;
}