var Event = Class({ 
	initialize: function(options) { 
		
		this.defaults = {
			start: null,
			end: null,
			title: null,
			division_id: "0",
			emp_brkup_id: "0",
			id: "0"
		}; 
		$.extend(this.defaults, options); 
		
	},	
	create: function(){
		
		if(this.defaults.division_id > 0){
			$('#calendar').fullCalendar('renderEvent',{
				title: this.defaults.title,
				start: this.defaults.start,
				end: this.defaults.end,
				division_id: this.defaults.division_id,
				allDay:false
			},true // make the event "stick"
			);
		}
	// $('#create_event').overlay().close();	
	
	},
	update: function(){
		
		if(this.defaults.division_id > 0){
			// I've not used updateEvent as it requires proper event object & I haven't stored it in this class object 
			// but necessary details. So its work around of update.
			$('#calendar').fullCalendar('removeEvents',this.defaults.id ).fullCalendar('renderEvent',{
				title: this.defaults.title,
				start: this.defaults.start,
				end: this.defaults.end,
				division_id: this.defaults.division_id,
				allDay:false
			},true // make the event "stick"
			);
		}
	// $('#update_event').overlay().close();	

	},
	get_json: function (){
		return App.encode_json(this.defaults);
	}
	
});
Event.before_create_event = function() {
	
	var obj_event = new Event({
		start: $("#ce_start").val(),
		end: $("#ce_end").val(),
		title:  $('#create_event .divisions :selected').text() ,
		division_id : $('#create_event .divisions :selected').val()
	});
	obj_event.create();
	$('#create_event').overlay().close();
	return false;
};	
Event.before_update_event = function() {
	
	var obj_event = new Event({
		start: $("#ue_start").val(),
		end: $("#ue_end").val(),
		title: $('#update_event .divisions :selected').text(),
		id: $("#ue_id").val(),
		division_id : $('#update_event .divisions :selected').val()
	});
	obj_event.update();
	$('#update_event').overlay().close();	
	return false;
};	
Event.format_date = function(date){
	return $.fullCalendar.formatDate(date, "yyyy-MM-dd HH:mm");
}
Event.get_json = function(event_obj){
	
	if(event_obj.division_id != undefined){
		var obj_event = new Event({
			start: Event.format_date(event_obj.start),
			end: Event.format_date(event_obj.end),
			title:  event_obj.title ,
			division_id : event_obj.division_id ,
			emp_brkup_id : event_obj.emp_brkup_id
		});
		return obj_event.get_json();
	}
	
}
Event.get_all_client_events = function (calender_selector){
	var client_events = [] ; // hold formatted data
	$('#' + calender_selector).fullCalendar('clientEvents', function(event) {		
		if(event.division_id != undefined){
			client_events.push(Event.get_json(event));
		}
	});
	
	return client_events ;
}

Event.get_all_slots = function (calender_selector){
	var client_events = [] ; // hold formatted data
	$('#' + calender_selector).fullCalendar('clientEvents', function(event) {			
		if(event.emp_brkup_id > 0){
			client_events.push(Event.get_json(event));
		}
	});
	
	return client_events ;
}
