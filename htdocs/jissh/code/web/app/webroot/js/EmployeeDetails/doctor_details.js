$(document).ready(function () {
		
	$('#cal_avp_appointments').fullCalendar({
		header: {			
			title: false			
		},
		defaultView: 'month',						
		loading: function(bool) {
			if (bool) {				
				$('#loading').show();
				EmployeeDetail.change_cell_background_color();
			}else{
				$('#loading').hide();				
			}
		},		
		editable: false,
		events: $('#url_doctor_avp_appointments').val()
	});
	

});

function validateAndSaveAppointments(){
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	
	$('#cancel_appointments_form').find('.validator').find('p').html('');
	$('#cancel_appointments_form').find('.validator').hide();
	
	if(from_date==""){
		$('#from_date').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#from_date').parent().find('.validator').show();
		return false;
	}
	if(to_date==""){
		$('#to_date').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#to_date').parent().find('.validator').show();
		return false;
	}
	
	else{
		$('#cancel_appointments_form').submit();
	}
}