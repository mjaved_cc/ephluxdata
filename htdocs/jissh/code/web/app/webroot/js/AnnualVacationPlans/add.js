$(document).ready(function () {
	$('#report_to_work').datepicker({						
		onSelect: function(dateText) {
			AnnualVacationPlan.validate_date_ctrl(dateText , this);
			
		}
	});
	
	$('#last_work_day').datepicker({						
		onSelect: function(dateText) {
			var date_unix = App.to_unix(dateText);			
			if(date_unix)				
				App.remove_error_message($(this).parent());			
		}
	});
	
});