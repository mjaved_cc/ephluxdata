var FileUpload = function (){}

/**
 * Upload file via iframe
 */
FileUpload.upload_via_iframe = function (form, action_url, div_id){
	
	// Create the iframe...
	var iframe = document.createElement("iframe");
	iframe.setAttribute("id", "upload_iframe");
	iframe.setAttribute("name", "upload_iframe");
	iframe.setAttribute("width", "0");
	iframe.setAttribute("height", "0");
	iframe.setAttribute("border", "0");
	iframe.setAttribute("style", "width: 0; height: 0; border: none;");
 
	// Add to document...
	form.parentNode.appendChild(iframe);
	window.frames['upload_iframe'].name = "upload_iframe";
 
	iframeId = document.getElementById("upload_iframe");
 
	// Add event...
	var eventHandler = function () {
 
		if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
		else iframeId.removeEventListener("load", eventHandler, false);
 
		// Message from server...
		if (iframeId.contentDocument) {
			content = iframeId.contentDocument.body.innerHTML;
		} else if (iframeId.contentWindow) {
			content = iframeId.contentWindow.document.body.innerHTML;
		} else if (iframeId.document) {
			content = iframeId.document.body.innerHTML;
		}
		
		document.getElementById(div_id).innerHTML = content;
		
		// Del the iframe...
		setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
		if(!$(content).hasClass('error')){
			FileUpload.copy_image_attr();
			FileUpload.close_overlay();
		}
	}
 
	if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
	if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
	
	
	// Set properties of form...
	form.setAttribute("target", "upload_iframe");
	form.setAttribute("action", action_url);
	form.setAttribute("method", "post");
	form.setAttribute("enctype", "multipart/form-data");
	form.setAttribute("encoding", "multipart/form-data");
 
	// Submit the form...
	form.submit();
 
	document.getElementById(div_id).innerHTML = "<img src='" + App.get_ajax_indicator() + "' />";
	
	return false;
}

/**
 * Copy image & other necessary attribute to required position.
 * 
 * Making it part of form submission.
 */
FileUpload.copy_image_attr = function(){
	$('#picture_name').val($('#temp_picture_name').val());
	$('#profile_pic').attr('src',$('#temp_profile_pic').attr('src'));	
}

/**
 * Closes overlay
 */
FileUpload.close_overlay = function(){
	// get access to the API
	var api_overlay = $(".attach_modal").data("overlay");
	
	if(api_overlay.isOpened()){
		api_overlay.close()
	}
}
