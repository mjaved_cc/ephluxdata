var EmployeeDetail = Class({ 
	initialize: function(options) { 
		
	}	
});
EmployeeDetail.before_save = function(){

	var client_events = Event.get_all_client_events('calendar');	
	$('#event_data_json').val("[" + client_events + "]");
	
}
/**
 * Get doctor/employee appointments & leaves.
 **/
//EmployeeDetail.get_employee_avp_appointments = function(){
//	
//	// EmployeeDetail.change_cell_background_color();
//	
//	// since tabbing js creates problem ; its work round
//	if(!$('#cal_avp_appointments').fullCalendar('clientEvents').length){
//		
//		$('#cal_avp_appointments').fullCalendar('addEventSource', $('#url_doctor_avp_appointments').val() );
//	}
//}
///**
// * Get doctor/employee working hours.
// **/
//EmployeeDetail.get_employee_working_hours = function(){ 
//	// since tabbing js creates problem ; its work round
//	if(!$('#calendar').fullCalendar('clientEvents').length){
//		$('#calendar').fullCalendar('addEventSource', $('#url_doctor_working_hours').val() );
//	}
//}
/**
 * Populate divisons based on selection for working hours form submission
 **/
EmployeeDetail.populate_divisions_for_working_hours = function(){
	
	var dd_html = $('<select />').attr('class','divisions');
	
	$('#divisions option').each(function(index,value){
		
		if($(value).attr('selected') && $(value).attr('selected') != ""){
			$('<option />', {
				value: $(value).val(), 
				text: $(value).text()
			}).appendTo(dd_html);
		}
		
	});
	
	if($('#divisions :selected').length > 0)
		$('.ce_divisions').html(dd_html);
	else 
		$('.ce_divisions').html('Please select division(s).');
}
EmployeeDetail.change_cell_background_color = function(){
	
	$.ajax({
		type: "get",
		dataType: "json",		
		url: $('#url_doctor_working_hours').val(),
		success: function (response) {
			$(response).each(function(key , data){
				cell_class = App.full_calendar_cell_class_prefix + data.three_letter_day;				
				$('.' + App.full_calendar_cell_class_prefix + 'day.' + cell_class).addClass(data.cell_background);
			});
		}
	});
}