$(document).ready(function () {
	
	setTimeout(function(){
		//Horizontal Tabs 
		$('.box ul.tabs').each(function () {
			var pane = $(this).closest(".box").find(".pane");
			$(this).tabs(pane, {
				effect: 'fade'
			});
		})
	}, App.timeout_interval); // after five seconds
	
});