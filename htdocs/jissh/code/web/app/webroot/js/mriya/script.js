$(document).ready(function () {

	// Highlight current page in menu
	var loc = window.location.toString().split("/")
	loc = loc[loc.length - 1]
	$("#main_nav li a[href=\"" + loc + "\"]").parents('li:not(ul ul li)').children('a').addClass("current");
	
	// user menu
	var user_menu = $("#user_menu .menu");
	var user_menu_btn = $("#user_menu .menu_btn");

	$(user_menu_btn).click(function () {
		if ($(user_menu).is(':hidden')) $(user_menu).stop(true, true).slideDown(200); //Slide down menu
		else {
			$(user_menu).fadeOut();
		}
		return false;
	});

	$(user_menu).click(function (e) {
		e.stopPropagation();
	});


	$(document).click(function () {
		$(user_menu).fadeOut();
	});
	
	//Tabs
	$('#user_menu ul.tabs').each(function () {
		var pane = $(this).closest(".menu").find(".pane");
		$(this).tabs(pane, {
			effect: 'fade'
		});
	});
		
	//Close button:
	$(".close_notification").click(function () {
		$(this).hide();
		$(this).parent().fadeTo('fast', 0, function () {
			$(this).slideUp('fast');
		});

	});
	
	//Main Navigation
	$("ul.sf-menu").supersubs({
		minWidth: 12,
		// minimum width of sub-menus in em units 
		maxWidth: 27,
		// maximum width of sub-menus in em units 
		extraWidth: 1 // extra width can ensure lines don't sometimes turn over 
	}).superfish({
		delay: 200,
		// one second delay on mouseout 
		animation: {
			opacity: 'show',
			height: 'show'
		},
		// fade-in and slide-down animation 
		speed: 200,
		// faster animation speed 
		dropShadows: false // disable drop shadows 
	});
		
	//Toggle Buttons
	$('.on_off').each(function () {
		$(this).ezMark({
			checkboxCls: 'itoggle',
			checkedCls: 'itoggle_off'
		});
	});

	//Expose on checkbox
	$('.expose input.on_off').change(function () {
		var box = $(this).closest(".box");
		if ($(this).is(':checked')) {
			// checked  
			$(box).expose({
				closeOnEsc: false,
				closeOnClick: false
			});
		} else {
			// un-checked  
			$.mask.close();
		}
	});
	
	//Tooltip -- Show only for non touch devices
	if (!Modernizr.touch){                   //Test if browser is touch enabled
		$('.enable_tip').tooltip({ 
			opacity: 1.0,
			effect: 'slide',
			predelay: 200,
			delay: 10,
			position: 'top center',
			layout: '<div><span class="arrow"></span></div>',
			offset: [0, 0]
		}).dynamic({
			bottom: {
				direction: 'down',
				bounce: true
			} //made it dynamic so it will show on bottom if there isnt space on the top
		});
	}  

	//Validator
	$("#validate").validator({
		speed: 1000,
		offset: [0, 10],
		position: 'bottom left',
		relative: true,
		messageClass: 'validator',
		message: '<div><span class="arrow"/></div>'
	}).attr('novalidate', 'novalidate'); //Disable Browser Validation since we validate with jquery tools
    
	//Preload CSS and Images
	$.preloadCssImages(); 
	
	//Expandable Tables 
	$('.expandable').each(function () {
		$(this).find("tr:odd").addClass("odd");
		$(this).find("tr:not(.odd)").hide().addClass("grid_dropdown");
		$(this).find("thead tr").show();
		$(this).find("tr.odd").click(function () {
			$(this).toggleClass("active");
			$(this).next("tr").toggle();
			$(this).find(".toggle").toggleClass("collapse");
		});
	});
	
	// Modal on click 
	$('.attach_modal').each(function () {
		var target=	$(this).attr('data-modal');		
		$(this).overlay({
			closeOnClick: true,
			target:target,
			mask: {
				loadSpeed: 1000,
				opacity: 0.8
			}
		});
	});
	
	$('.event').overlay({
		closeOnClick: true,
		mask: {
			loadSpeed: 1000,
			opacity: 0.8
		}
	});
	
	// time picker - uses jquery datepicker & timepicker	
	$.datepicker.regional['ru'] = {		
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: App.baseURL()+"/img/calendar_icon.png",
		buttonImageOnly: true		
	};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	
	$('.datepicker').datepicker({
		 dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: App.baseURL()+"/img/calendar_icon.png",
		buttonImageOnly: true
	});
	
	$('.datetime').datetimepicker({
		timeFormat: "HH:mm" // 2013-05-01 00:00
	});
	
	$('.dataTable').dataTable({
		"sPaginationType": "full_numbers",
		"bAutoWidth": false //setting width to 100%
	});
	
	$("#validate input:text, #formId textarea").first().focus();
	
	// convert multi-select to check list - always place this code before tabs functioning
	$(".dd_checklist").dropdownchecklist({ 
		width: 250
	});
	
	

});
