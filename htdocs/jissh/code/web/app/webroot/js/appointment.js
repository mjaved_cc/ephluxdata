var Appointment = new function(){}

Appointment.openModel= function(){
	$('#status_add_call_log').hide();
}

Appointment.validateAndSaveCallLog= function() {
	var receiver = $('#receiver').val();
	var call_subject = $('#call_subject').val();
	var calling_date_time = $('#calling_date_time').val();
	var reg = /^[a-zA-Z]+$/;
	
	if(receiver==""){
		$('#receiver').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#receiver').parent().find('.validator').show();
		return false;
	}
	else if(call_subject==""){
		$('#call_subject').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#call_subject').parent().find('.validator').show();
		return false;
	}
	else if(calling_date_time==""){
		$('#calling_date_time').parent().find('.validator').find('p').html('Please complete this mandatory field.');
		$('#calling_date_time').parent().find('.validator').show();
		return false;
	}
	else{
		$('#call_log_form').submit();
	}
}
	
/** Static Variable ***/
Appointment.radio_wlist_value = 0;
Appointment.radio_by_name = 1 ;
/** END: Static Variable ***/

/**
 * Populate listing of patient based on waiting list title
 **/
Appointment.populate_patient_listing_via_waiting_list = function (){
	
	$.ajax({
		type: "get",
		dataType: "html",		
		url: App.extendUrl(
			App.getUrl({
				controller:'appointments',
				action: 'get_patient_by_waiting_list_title_id'
			}) , {
				waiting_list_title_id : $('#waiting_list_title :selected').val()
			}),
		success: function (response) {
			App.display_ajax_indicator($('#waiting_list_title').parent());			
			$('#patient_listing_0').html(response);
			App.remove_ajax_indicator();
		}
	});
}

/**
 * Populate listing of patient using keyword
 **/
Appointment.populate_patient_listing_via_keyword = function (){
	
	$.ajax({
		type: "get",
		dataType: "html",		
		url: App.extendUrl(
			App.getUrl({
				controller:'appointments',
				action: 'get_all_patients'
			}) , {
				keyword : $('#AppointmentSearchPatient').val()
			}),
		success: function (response) {
			App.display_ajax_indicator($('#AppointmentSearchPatient').parent());			
			$('#patient_listing_1').html(response);
			$('#AppointmentSearchPatient').val('');
			App.remove_ajax_indicator();
		}
	});
}
/**
 * Populate listing of clinician based on waiting list title
 **/
Appointment.populate_clinician_listing_via_waiting_list = function (){
	
	$.ajax({
		type: "get",
		dataType: "html",		
		url: App.extendUrl(
			App.getUrl({
				controller:'appointments',
				action: 'get_available_doctors'
			}) , {
				waiting_list_title_id : $('#waiting_list_title :selected').val()
			}),
		success: function (response) {
			App.display_ajax_indicator($('#waiting_list_title').parent());			
			$('#appointment_doctor_listing').html(response);
			App.remove_ajax_indicator();
		}
	});
}
/**
 * Populate listing of clinician based on all available clinicians
 **/
Appointment.populate_clinician_listing_via_get_all_available = function (){
	
	$.ajax({
		type: "get",
		dataType: "html",		
		url: App.extendUrl(
			App.getUrl({
				controller:'appointments',
				action: 'get_available_doctors'
			}) , {
				waiting_list_title_id : App.error_code_success
			}),
		success: function (response) {
			App.display_ajax_indicator($('#waiting_list_title').parent());			
			$('#appointment_doctor_listing').html(response);
			App.remove_ajax_indicator();
		}
	});
}
/**
 * Display full calender with appointments & leaves
 **/
Appointment.get_doctor_appointment_schedule = function (){
	// remove all events from calender
	// when user switch between clinicians by radio button we need to remove previously rendered events
	// & only render events with selected clinicians
	$('#cal_avp_appointments').fullCalendar('removeEvents');
	
	// mark cell disable so next selected clinician working hours get affected
	App.disable_all_calender_cell();
	
	var user_id = $('#appointment_doctor_listing [type=radio]:checked').val();
	$('#url_doctor_avp_appointments').val(App.getUrl({
		controller:'employee_details',
		action: 'get_doctor_avp_and_appointments/' + user_id + App.extension_json
	}));
	$('#url_doctor_working_hours').val(App.getUrl({
		controller:'employee_details',
		action: 'get_doctor_working_hours/' + user_id + App.extension_json
	}));
	
	
	$('#cal_avp_appointments').fullCalendar('addEventSource', $('#url_doctor_avp_appointments').val() );
	
}
/**
 *	Get available working slots for given doctor (scenario based)
 **/
Appointment.get_available_slots = function (date){
	var user_id = $('#appointment_doctor_listing [type=radio]:checked').val();
	var patient_selection_mode = $('#patient_selection_mode [type=radio]:checked').val();
	
	if(patient_selection_mode == Appointment.radio_wlist_value )
		waiting_list_title = $('#waiting_list_title :selected').val() ;
	else if(patient_selection_mode == Appointment.radio_by_name )
		waiting_list_title = App.error_code_success  ;
	else
		waiting_list_title = App.error_code_success  ;	

	$.ajax({
		type: "get",
		dataType: "html",		
		url: App.extendUrl(
			App.getUrl({
				controller:'appointments',
				action: 'get_available_working_slots'
			}) , {
				waiting_list_title_id : waiting_list_title ,
				employee_detail_id : $('#employee_detail_id' + user_id).val(),
				date_as_appointment : Appointment.format_date(date)
			}),
		success: function (response) {
			App.display_ajax_indicator($('#app_slot_selection'));			
			$('#app_slot_selection').html(response);
			App.remove_ajax_indicator();
		}
	});	
}
/**
 *	Format as Date (Y-m-d)
 **/
Appointment.format_date = function(date){
	return $.fullCalendar.formatDate(date, "yyyy-MM-dd");
}

/**
 *	Format as Time (H-m-s)
 **/
Appointment.format_time = function(date){
	return $.fullCalendar.formatDate(date, "HH:mm:ss");
}

/**
 *	Format as Time (H-m-s)
 **/
Appointment.format_time_by_str_date = function(date){
	return Appointment.format_time($.fullCalendar.parseDate(date));
}

/**
 *	Format as mysql timestamp
 **/
Appointment.get_formatted_timestamp_wrt_selected_date = function(date){	
	return $("#slot_date").val() + ' ' + Appointment.format_time_by_str_date(date) ;
}


Appointment.render_slot_event = function(){
	
	var emp_brkup_id = $("#app_slot_selection [type=radio]:checked").val();	
	var all_entries = $('#cal_avp_appointments').fullCalendar('clientEvents');
	var flag = true ;

	$(all_entries).each(function(key , value){
		if(			
			emp_brkup_id != '' &&
			emp_brkup_id == value.emp_brkup_id && 
			$("#slot_date").val() == Appointment.format_date(value.start)
			){
			flag = false ;
		}
	});
	//console.log(flag);
	// ensure not to make duplicate entry
	if(flag){
		$('#cal_avp_appointments').fullCalendar('renderEvent',{
			title: $('#start_time' + emp_brkup_id).parent().text() ,
			start: Appointment.get_formatted_timestamp_wrt_selected_date($('#start_time' + emp_brkup_id).val()) ,
			end: Appointment.get_formatted_timestamp_wrt_selected_date($('#end_time' + emp_brkup_id).val()) ,	
			emp_brkup_id: emp_brkup_id,
			division_id: $('#division_id' + emp_brkup_id).val(),
			allDay:false
		},true // make the event "stick"
		);
	}
	
}
Appointment.before_submit = function (){
	
	var client_events = Event.get_all_slots('cal_avp_appointments');	
	
	$('#slots_data_json').val("[" + client_events + "]");
}