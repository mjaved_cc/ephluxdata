$(document).ready(function() {
	$('#UserSignupForm').validate({
		rules: {
			"data[User][first_name]": "required",
			"data[User][last_name]": "required",
			"data[User][username]": {
				required : true,
				email : true
			},
			"data[User][password]": {
				required : true,
				minlength : 8
			},
			"data[User][confirm_password]": {
				required : true,
				equalTo: "#UserPassword"
			}
		}
	});

	//$('#UserSignupForm').submit(function () { return false;})

});

