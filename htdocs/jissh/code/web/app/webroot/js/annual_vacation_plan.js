var AnnualVacationPlan = function (){}

/**
 * Validate date input control
 * 
 * Here is validation lists:
 * 
 * - Report To Work is dependent on Last Work Day. Its value should be greator than Last Work Day.
 * 
 * - Hence Last Work Day should not be empty while selection Report To Work
 * 
 * @param string report_to_work_date Date
 * @param DOM inst report_to_work DOM instance
 **/
AnnualVacationPlan.validate_date_ctrl = function(report_to_work_date , inst){
	var last_work_day = $("#last_work_day").datepicker("getDate");
	var last_work_day_unix = App.to_unix(last_work_day);
	
	if(last_work_day_unix){
		var selected_date_unix = App.to_unix(report_to_work_date);			 

		if(selected_date_unix < last_work_day_unix) {
			//If the selected date was before today, continue to show the datepicker
			AnnualVacationPlan.empty_and_hide_date_ctrl(inst);
			App.display_error_message('Should be greator than Last Work Day' , $(inst).parent());
		} else {
			App.remove_error_message($(inst).parent());	
		}
	} else {
		AnnualVacationPlan.empty_and_hide_date_ctrl(inst);		
		App.display_error_message('Please fill it first' , $("#last_work_day").parent());
	}
}

/**
 *	Empty datepicker text value & hide control
 *	
 * @param DOM inst DOM instance for datepicker control
 **/
AnnualVacationPlan.empty_and_hide_date_ctrl = function(inst){
	$(inst).val('');
	$(inst).datepicker('hide');
}