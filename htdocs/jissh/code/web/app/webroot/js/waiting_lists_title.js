//PatientDetail = new Object();
var WaitingListsTitle = { 
	initialize: function(options) { 
		
	},
	addToWaitingList: function(wid) {
		$('#WaitingListsTitleWaitingListTitleId').val(wid);
		$('#WaitingListsTitleAssignForm').submit();	
	},
	validateWaitingList: function() {
		var val = $('#WaitingListsTitleTitle').val();
		var reg = /^[a-zA-Z ]+$/;
		
		if(val==""){
			$('#WaitingListsTitleTitle').parent().next('.validator').find('p').html('Please complete this mandatory field.');
			$('#WaitingListsTitleTitle').parent().next('.validator').css('visibility','visible');
			return false;
		}
		else if(!reg.test(val)) {
			$('#WaitingListsTitleTitle').parent().next('.validator').find('p').html('Please enter characters only.');
			$('#WaitingListsTitleTitle').parent().next('.validator').css('visibility','visible');
			return false;
		}
		else{
			$('#waiting_list_model_form').submit();
		}
	},
	openModel: function(){
		$('#status_add_waiting_list').hide()
	}
}
var active = false;
$(document).ready(function(){

	$('#ajax_search_btn,#cancel_search a').live('click',function(){
		if (active) {
			return;
		}
		active = true;  
		searchF();
	});
	
	$('#ajax_response table th a, #ajax_response ul.pagination li a').live('click', function() {
		//alert('test');
		var url = $(this).attr("href");
		$('#ajax_response').load(url);
		return false;
	});	
})

function searchF() {
	var form = $('#validate');
	
	$.ajax({
		type: "POST",
		url: App.baseURL()+"WaitingListsTitles/assign",
		data:  form.serialize() 
		}).done(function( msg ) {
			$('#ajax_response').html(msg);
			active = false;
	});	
}

function onSbumit() {
	if (active) {
		return;
	}
	active = true;
	searchF();
	
	return false;
}
	
