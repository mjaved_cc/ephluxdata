<div class="officialTitles view">
<h2><?php  echo __('Official Title'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($officialTitle['OfficialTitle']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Official Title'), array('action' => 'edit', $officialTitle['OfficialTitle']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Official Title'), array('action' => 'delete', $officialTitle['OfficialTitle']['id']), null, __('Are you sure you want to delete # %s?', $officialTitle['OfficialTitle']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Official Titles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Official Title'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Employee Details'); ?></h3>
	<?php if (!empty($officialTitle['EmployeeDetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Middle Name'); ?></th>
		<th><?php echo __('Picture'); ?></th>
		<th><?php echo __('Mobile Saudi'); ?></th>
		<th><?php echo __('Landline Saudi'); ?></th>
		<th><?php echo __('Mobile Homecountry'); ?></th>
		<th><?php echo __('Landline Homecountry'); ?></th>
		<th><?php echo __('Jish Email'); ?></th>
		<th><?php echo __('Non Jish Email'); ?></th>
		<th><?php echo __('Licence Type Id'); ?></th>
		<th><?php echo __('Licence Number'); ?></th>
		<th><?php echo __('Joining Date'); ?></th>
		<th><?php echo __('Contract Expiry Date'); ?></th>
		<th><?php echo __('Official Title Id'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th><?php echo __('Dob'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($officialTitle['EmployeeDetail'] as $employeeDetail): ?>
		<tr>
			<td><?php echo $employeeDetail['id']; ?></td>
			<td><?php echo $employeeDetail['user_id']; ?></td>
			<td><?php echo $employeeDetail['first_name']; ?></td>
			<td><?php echo $employeeDetail['last_name']; ?></td>
			<td><?php echo $employeeDetail['middle_name']; ?></td>
			<td><?php echo $employeeDetail['picture']; ?></td>
			<td><?php echo $employeeDetail['mobile_saudi']; ?></td>
			<td><?php echo $employeeDetail['landline_saudi']; ?></td>
			<td><?php echo $employeeDetail['mobile_homecountry']; ?></td>
			<td><?php echo $employeeDetail['landline_homecountry']; ?></td>
			<td><?php echo $employeeDetail['jish_email']; ?></td>
			<td><?php echo $employeeDetail['non_jish_email']; ?></td>
			<td><?php echo $employeeDetail['licence_type_id']; ?></td>
			<td><?php echo $employeeDetail['licence_number']; ?></td>
			<td><?php echo $employeeDetail['joining_date']; ?></td>
			<td><?php echo $employeeDetail['contract_expiry_date']; ?></td>
			<td><?php echo $employeeDetail['official_title_id']; ?></td>
			<td><?php echo $employeeDetail['gender']; ?></td>
			<td><?php echo $employeeDetail['dob']; ?></td>
			<td><?php echo $employeeDetail['status']; ?></td>
			<td><?php echo $employeeDetail['created']; ?></td>
			<td><?php echo $employeeDetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'employee_details', 'action' => 'view', $employeeDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'employee_details', 'action' => 'edit', $employeeDetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'employee_details', 'action' => 'delete', $employeeDetail['id']), null, __('Are you sure you want to delete # %s?', $employeeDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
