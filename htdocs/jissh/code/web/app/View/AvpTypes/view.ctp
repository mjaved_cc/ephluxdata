<div class="avpTypes view">
<h2><?php  echo __('Avp Type'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($avp_type['AvpType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($avp_type['AvpType']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Avp Type'), array('action' => 'edit', $avp_type['AvpType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Avp Type'), array('action' => 'delete', $avp_type['AvpType']['id']), null, __('Are you sure you want to delete # %s?', $avp_type['AvpType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Avp Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Avp Type'), array('action' => 'add')); ?> </li>
<!--		<li><?php echo $this->Html->link(__('List Annual Vacation Plans'), array('controller' => 'annual_vacation_plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Annual Vacation Plan'), array('controller' => 'annual_vacation_plans', 'action' => 'add')); ?> </li>-->
	</ul>
</div>
<!--<div class="related">
	<h3><?php echo __('Related Annual Vacation Plans'); ?></h3>
	<?php if (!empty($avp_type['AnnualVacationPlan'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Employee Detail Id'); ?></th>
		<th><?php echo __('Avp Type Id'); ?></th>
		<th><?php echo __('Last Work Day'); ?></th>
		<th><?php echo __('Report To Work'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($avp_type['AnnualVacationPlan'] as $annualVacationPlan): ?>
		<tr>
			<td><?php echo $annualVacationPlan['id']; ?></td>
			<td><?php echo $annualVacationPlan['employee_detail_id']; ?></td>
			<td><?php echo $annualVacationPlan['avp_type_id']; ?></td>
			<td><?php echo $annualVacationPlan['last_work_day']; ?></td>
			<td><?php echo $annualVacationPlan['report_to_work']; ?></td>
			<td><?php echo $annualVacationPlan['status']; ?></td>
			<td><?php echo $annualVacationPlan['is_active']; ?></td>
			<td><?php echo $annualVacationPlan['created']; ?></td>
			<td><?php echo $annualVacationPlan['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'annual_vacation_plans', 'action' => 'view', $annualVacationPlan['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'annual_vacation_plans', 'action' => 'edit', $annualVacationPlan['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'annual_vacation_plans', 'action' => 'delete', $annualVacationPlan['id']), null, __('Are you sure you want to delete # %s?', $annualVacationPlan['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Annual Vacation Plan'), array('controller' => 'annual_vacation_plans', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div> -->
