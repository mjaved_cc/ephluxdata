<div class="mpdActivities view">
<h2><?php  echo __('Mpd Activity'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee Detail'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mpdActivity['EmployeeDetail']['id'], array('controller' => 'employee_details', 'action' => 'view', $mpdActivity['EmployeeDetail']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Program'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mpdActivity['Program']['name'], array('controller' => 'programs', 'action' => 'view', $mpdActivity['Program']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mpd Activity List'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mpdActivity['MpdActivityList']['name'], array('controller' => 'mpd_activity_lists', 'action' => 'view', $mpdActivity['MpdActivityList']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modality'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mpdActivity['Modality']['name'], array('controller' => 'modalities', 'action' => 'view', $mpdActivity['Modality']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Complition'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['date_complition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($mpdActivity['MpdActivity']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mpd Activity'), array('action' => 'edit', $mpdActivity['MpdActivity']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mpd Activity'), array('action' => 'delete', $mpdActivity['MpdActivity']['id']), null, __('Are you sure you want to delete # %s?', $mpdActivity['MpdActivity']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mpd Activities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mpd Activity'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Programs'), array('controller' => 'programs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Program'), array('controller' => 'programs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mpd Activity Lists'), array('controller' => 'mpd_activity_lists', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mpd Activity List'), array('controller' => 'mpd_activity_lists', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('controller' => 'modalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('controller' => 'modalities', 'action' => 'add')); ?> </li>
	</ul>
</div>
