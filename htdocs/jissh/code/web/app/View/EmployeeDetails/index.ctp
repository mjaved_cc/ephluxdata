<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" type="button" onclick="App.redirect({
				controller : 'EmployeeDetails',
				action : 'doctor_register'
			})"><?php echo __('Add Clinician') ?></button>
			<!--<button class="button dark" data-target="register"><?php echo __('Add Other Faculty') ?></button>-->
		</div>
		<!-- End Header -->
		<div class="body">
			<?php echo $this->Form->create("EmployeeDetail", array('action' => 'index', 'id' => 'validate')); ?>
			<span><?php echo $this->Form->input("q", array('label' => 'Search for', 'required' => 'required', 'div' => false, 'class' => 'med float-left-margin')); ?></span>
			<span><?php echo $this->Form->button('Search', array('type' => 'submit', 'escape' => true, 'class' => 'button green small float-left-margin')); ?></span>
			<?php
			echo $this->Form->end();
			if (!empty($employee_details)) {
				?> 
				<table class="datagrid wf">
					<thead>
						<tr>
							<th class="sorting"><?php echo $this->Paginator->sort('first_name', 'Employee Name'); ?></th>
							<th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
							<th class="sorting"><?php echo $this->Paginator->sort('status'); ?></th>
							<th ><?php echo __('Action'); ?></th>
						</tr>
					</thead>
					<tbody>
	<?php foreach ($employee_details as $employee_detail): ?>
							<tr>
								<td align="center"><?php echo h($employee_detail['EmployeeDetail']['first_name']); ?>&nbsp;<?php echo h($employee_detail['EmployeeDetail']['last_name']); ?></td>
								<td align="center"><?php echo h($employee_detail['EmployeeDetail']['created']); ?>&nbsp;</td>
								<td align="center"><?php echo h($employee_detail['EmployeeDetail']['status']); ?>&nbsp;</td>
								<td class="actions" align="center">
									<?php echo $this->Html->link(__('Details'), $this->Misc->get_fully_qualified_url($employee_detail['EmployeeDetail']['doctor_details_relative_url'])); ?> |
									<?php echo $this->Html->link(__('Edit'), $this->Misc->get_fully_qualified_url($employee_detail['EmployeeDetail']['doctor_edit_relative_url'])); ?> |
		<?php echo $this->Form->postLink(__('Delete'), array('action' => 'doctor_delete', $employee_detail['EmployeeDetail']['id']), null, __('Are you sure you want to delete # %s?', $employee_detail['EmployeeDetail']['id'])); ?>
								</td>
							</tr>

	<?php endforeach; ?>
					</tbody>
				</table>
				<?php
				echo $this->element('pagination', array('pagination_params' => $pagination_params));
			} else {

				$class = 'error';
				$image_name = 'error_icon.png';

				echo $this->element('notification', array(
					"message" => 'No match found',
					"class" => $class,
					"image_name" => $image_name
				));
			}
			?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>