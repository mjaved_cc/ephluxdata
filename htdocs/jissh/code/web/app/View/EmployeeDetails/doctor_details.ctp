<?php echo $this->element('faculty_quick_view_links', array(
	'employee_listing_relative_url' => $data['EmployeeDetail']['employee_listing_relative_url'],
	'doctor_edit_relative_url' => $data['EmployeeDetail']['doctor_edit_relative_url'],
	'mpd_activities_relative_url' => $data['EmployeeDetail']['mpd_activities_relative_url'],
	'vpd_activities_relative_url' => $data['EmployeeDetail']['vpd_activities_relative_url'],
	'create_appointment_url' => 'appointments/create/0/'.$data['User']['id']
));?>
<h1 class="border_bottom_1">Doctor</h1>
<section class="grid_12">
	<!-- Begin box -->
	<div class="box">

		<!-- End header -->
		<div class="body no_padding">
			<!-- Horizontal Navigation -->
			<ul class="tabs clearfix">
				<li class="active"><a href="javascript:void(0);">General</a></li>
				<li><a id="get_working_hr" href="javascript:void(0);" >Working Hours</a></li>				
				<li><a id="get_working_hr" href="javascript:void(0);" >Appointments & Leaves</a></li>				
			</ul>			
			<div class="pane padding20"><!-- Pane 1 -->
				<div class="clearfix">
					<div class="grid_10">
						<table class="wf">
							<tbody>
								<tr>
									<td><strong>First Name:</strong></td>
									<td><?php echo $data['EmployeeDetail']['first_name'] ?></td>
									<td><strong>Middle Name:</strong></td>
									<td><?php echo $data['EmployeeDetail']['middle_name'] ?></td>
								</tr>
								<tr>
									<td><strong>Last Name:</strong></td>
									<td><?php echo $data['EmployeeDetail']['last_name'] ?></td>
									<td><strong>Gender:</strong></td>
									<td><?php echo $data['EmployeeDetail']['gender'] ?></td>
								</tr>
								<tr>
									<td><strong>Email:</strong></td>
									<td><?php echo $data['User']['username'] ?></td>
									<td><strong>Jish Email:</strong></td>
									<td><?php echo $data['EmployeeDetail']['jish_email'] ?></td>
								</tr>
								<tr>
									<td><strong>Date of Birth:</strong></td>
									<td><?php echo $data['EmployeeDetail']['dob'] ?></td>
								</tr>									
								<tr>
									<td><strong>Joining Date:</strong></td>
									<td><?php echo $data['EmployeeDetail']['joining_date'] ?></td>
									<td><strong>Contract Expiry Date:</strong></td>
									<td><?php echo $data['EmployeeDetail']['contract_expiry_date'] ?></td>					
								</tr>
								<tr>
									<td><strong>Mobile Saudi:</strong></td>
									<td><?php echo $data['EmployeeDetail']['mobile_saudi'] ?></td>
									<td><strong>Land-line Saudi:</strong></td>
									<td><?php echo $data['EmployeeDetail']['landline_saudi'] ?></td>
								</tr>
								<tr>
									<td><strong>mobile Home Country:</strong></td>
									<td><?php echo $data['EmployeeDetail']['mobile_homecountry'] ?></td>
									<td><strong>Land-line Home Country:</strong></td>
									<td><?php echo $data['EmployeeDetail']['landline_homecountry'] ?></td>
								</tr>								
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>License</legend>
											<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
												<tbody>
													<tr>
														<td>&nbsp;</td>
														<td>Type: </td>
														<td><?php echo $data['LicenseType']['name'] ?></td>
														<td>&nbsp;</td>
														<td>Number: </td>
														<td><?php echo $data['EmployeeDetail']['license_number'] ?></td>
													</tr>
												</tbody>
											</table>    
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Language(s)</legend>
											<?php if ($data['Language']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['Language'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Allowed Service(s)</legend>
											<?php if ($data['AllowedService']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['AllowedService'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Case Load(s)</legend>
											<?php if ($data['CaseLoad']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['CaseLoad'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Division(s)</legend>
											<?php if ($data['Division']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['Division'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Level(s)</legend>
											<?php if ($data['Level']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['Level'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Official Title(s)</legend>
											<?php if ($data['OfficialTitle']) { ?>
												<table cellspacing="0" cellpadding="0" class="no_padding no_margin">
													<tbody>
														<tr>
															<?php foreach ($data['OfficialTitle'] as $value) { ?> 
																<td>&nbsp;</td>
																<td><?php echo $value['name']; ?></td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</fieldset>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="grid_2">
						<ul class="gallery">

							<!--Single Gallery image-->
							<li>
								<!--Thumbnail-->
								<?php echo $this->Html->image($this->Misc->get_fully_qualified_url($data['EmployeeDetail']['profile_thumb_relative_url']), array('alt' => 'Thumbnail', 'width' => THUMBNAIL_MAX_WIDTH, 'height' => THUMBNAIL_MAX_HEIGHT)) ?>
								<!--Controls for view, edit, delete-->
								<ul class="controls">
									<li title="View Image" class="enable_tip">
										<a href="<?php echo $this->Misc->get_fully_qualified_url($data['EmployeeDetail']['profile_org_relative_url']) ?>" data-gallery="prettyPhoto[gallery1]" title="<?php echo $data['EmployeeDetail']['display_name'] ?>" class="view ir">View</a>
									</li>									
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="pane padding20"><!-- Pane 2 -->
				<?php
				echo $this->element('doctor_working_hours', array(
					'user_id' => isset($data['User']['id']) ? $data['User']['id'] : 0,
					'editable' => isset($editable) ? $editable : true  // by default events are editable exept for views
				));
				?>
			</div>
			<div class="pane padding20"><!-- Pane 3 -->
				<?php
				echo $this->element('doctor_avp_appointments', array(
					'user_id' => isset($data['User']['id']) ? $data['User']['id'] : 0,
					'employee_id' => isset($data['EmployeeDetail']['id']) ? $data['EmployeeDetail']['id'] : 0					
				));
				?>
			</div>
		</div>
	</div>	
</section>