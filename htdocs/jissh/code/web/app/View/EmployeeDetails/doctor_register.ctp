<?php 
if(!empty($data)) {
echo $this->element('faculty_quick_view_links', array(
	'employee_listing_relative_url' => $data['EmployeeDetail']['employee_listing_relative_url'],
	'doctor_edit_relative_url' => $data['EmployeeDetail']['doctor_edit_relative_url'],
	'mpd_activities_relative_url' => $data['EmployeeDetail']['mpd_activities_relative_url'],
	'avp_relative_url' => $data['EmployeeDetail']['avp_relative_url'],
	'vpd_activities_relative_url' => $data['EmployeeDetail']['vpd_activities_relative_url']
)); 
}?>
<h1 class="border_bottom_1">Doctor</h1>
<section class="grid_12">
	<?php echo $this->Form->create('EmployeeDetail', array('id' => 'validate')); ?>
	<!-- Begin box -->
	<div class="box">

		<!-- End header -->
		<div class="body no_padding">
			<!-- Horizontal Navigation -->
			<ul class="tabs clearfix">
				<li class="active"><a href="javascript:void(0);">General</a></li>
				<li><a id="get_working_hr" href="javascript:void(0);" >Working Hours</a></li>				
			</ul>			
			<div class="pane padding20"><!-- Pane 1 -->
				<?php
				echo $this->element('doctor_basics', array(
					'genders' => $genders,
					'gender' => isset($this->request->data['EmployeeDetail']['gender']) ? $this->request->data['EmployeeDetail']['gender'] : false , // selected value for license type					
					'license_types' => $license_types,
					'license_type_id' => isset($this->request->data['EmployeeDetail']['license_type_id']) ? $this->request->data['EmployeeDetail']['license_type_id'] : false , // selected value for license type
					'profile_thumb_relative_url' => isset($this->request->data['EmployeeDetail']['profile_thumb_relative_url']) ? $this->request->data['EmployeeDetail']['profile_thumb_relative_url'] : false ,
					'official_titles' => $official_titles,
					'off_title_selected_val' => isset($off_title_selected_val) ? $off_title_selected_val : false ,
					'levels' => $levels,
					'level_selected_val' => isset($level_selected_val) ? $level_selected_val : false ,
					'languages' => $languages,
					'language_selected_val' => isset($language_selected_val) ? $language_selected_val : false ,
					'divisions' => $divisions,
					'division_selected_val' => isset($division_selected_val) ? $division_selected_val : false ,
					'case_loads' => $case_loads,
					'case_load_selected_val' => isset($case_load_selected_val) ? $case_load_selected_val : false ,
					'allowed_services' => $allowed_services,
					'allowed_service_selected_val' => isset($allowed_service_selected_val) ? $allowed_service_selected_val : false
				));
				?>
			</div>
			<div class="pane padding20"><!-- Pane 2 -->
				<?php
				echo $this->element('doctor_working_hours',array(
					'user_id' => isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : 0,
					'editable' => isset($editable) ? $editable : true  // by default events are editable exept for views
				));
				?>
			</div>
		</div>

	</div>
	<!-- End box -->
	<br/>

	<div class="clearfix">
		<div class="fr padding20">
			<?php
			echo $this->Form->button('Save', array('id' => 'dr_save_btn', 'type' => 'submit', 'escape' => true, 'class' => 'button green large')) . ' ';
			echo $this->Form->button('Cancel', array('type' => 'button', 'escape' => true, 'class' => 'button blue large', 'type' => 'button', 'onclick' => "App.redirect({
				controller : 'employee_details',
				action : 'index'
			})"));
			echo $this->Form->end();
			?>
		</div>
		<div class="padding20">
			<?php
			echo $this->element('working_hours_setting_form');
			?>
		</div>
	</div>
</section>
<?php echo $this->element('file_upload'); ?>