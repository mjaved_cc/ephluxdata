<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<div class="clear"></div>

<section class="grid_8">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2><?php echo h($feature['Feature']['description']); ?></h2>
		</div>
		<!-- End Header -->
		<div class="body">
            <table class="display dataTable">
				<thead>
					<tr>
						<th><?php echo __('Description'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($feature['AuthActionMap'] as $authActionMap): ?>
						<tr>
							<td><?php echo $authActionMap['description']; ?></td>
							<td><?php echo $authActionMap['created']; ?></td>
							<td>
								<?php echo $this->Html->link(__('View'), array('controller' => 'auth_action_maps', 'action' => 'view', $authActionMap['id'])); ?>
								<?php echo $this->Html->link(__('Edit'), array('controller' => 'auth_action_maps', 'action' => 'edit', $authActionMap['id'])); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'auth_action_maps', 'action' => 'delete', $authActionMap['id']), null, __('Are you sure you want to delete # %s?', $authActionMap['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination', array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
<section class="grid_4">
	<div class="box">
		<div class="header clearfix">
			<h2>Actions</h2>
		</div>
		<div class="body">
			<ul>
				<li><?php echo $this->Html->link(__('Edit Feature'), array('action' => 'edit', $feature['Feature']['id'])); ?> </li>
				<li><?php echo $this->Form->postLink(__('Delete Feature'), array('action' => 'delete', $feature['Feature']['id']), null, __('Are you sure you want to delete # %s?', $feature['Feature']['id'])); ?> </li>
				<li><?php echo $this->Html->link(__('List Features'), array('action' => 'index')); ?> </li>
				<li><?php echo $this->Html->link(__('New Feature'), array('action' => 'add')); ?> </li>
			</ul>

		</div>
	</div>
</section>
