<div class="caseLoads view">
<h2><?php  echo __('Case Load'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ip Address'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($caseLoad['CaseLoad']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Case Load'), array('action' => 'edit', $caseLoad['CaseLoad']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Case Load'), array('action' => 'delete', $caseLoad['CaseLoad']['id']), null, __('Are you sure you want to delete # %s?', $caseLoad['CaseLoad']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Case Loads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Case Load'), array('action' => 'add')); ?> </li>
	</ul>
</div>
