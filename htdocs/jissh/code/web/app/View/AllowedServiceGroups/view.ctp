<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<div class="clear"></div>
<h3 class="border_bottom_3"><?php echo h($allowed_service_group['AllowedServiceGroup']['name']); ?></h3>

<section class="grid_8">
  <div class="box">
    <div class="body">
      <pre><?php echo __('Created on '); ?><?php echo h($allowed_service_group['AllowedServiceGroup']['created']); ?></pre>
      <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
</section>

<section class="grid_4">
	<div class="box">
    <div class="header clearfix">
        <h2>Actions</h2>
    </div>
    <div class="body">
    	<ul>
            <li><?php echo $this->Html->link(__('Edit AllowedServiceGroup'), array('action' => 'edit', $allowed_service_group['AllowedServiceGroup']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Delete AllowedServiceGroup'), array('action' => 'delete', $allowed_service_group['AllowedServiceGroup']['id']), null, __('Are you sure you want to delete # %s?', $allowed_service_group['AllowedServiceGroup']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('List AllowedServiceGroups'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New AllowedServiceGroup'), array('action' => 'add')); ?> </li>
        </ul>

    </div>
  </div>
</section>

