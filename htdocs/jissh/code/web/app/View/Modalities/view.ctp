<div class="modalitys view">
<h2><?php  echo __('Modality'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($modality['Modality']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Modality'), array('action' => 'edit', $modality['Modality']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Modality'), array('action' => 'delete', $modality['Modality']['id']), null, __('Are you sure you want to delete # %s?', $modality['Modality']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalitys'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>