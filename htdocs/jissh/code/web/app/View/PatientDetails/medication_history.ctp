<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __("Medication History") ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="history.back()">Back</button>
		</div>
		<!-- End Header -->
		<div class="body">
            <table class="datagrid wf">
				<thead>
					<tr>
						<th class="sorting">Appointment Date</th>
						<th class="sorting">Medication Date</th>
                        <th class="sorting">Doctor</th>
                        <th class="sorting">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($patientMedications)) {
						foreach ($patientMedications as $patientMedication): ?>
						<tr>
							<td align="center">
                            	<?php echo $patientMedication['Appointment']['created'];?>
							</td>
							<td align="center"><?php echo h($patientMedication['PatientMedication']['created']); ?>&nbsp;</td>
                            <td align="center"><?php echo h($patientMedication['EmployeeDetail']['display_name']); ?>&nbsp;</td>
                            <td align="center"><?php echo $this->Html->link(__("Details"), array('action' => 'medication_details', $patientMedication['PatientMedication']['id'])); ?></td>
						</tr>
						
					<?php endforeach; 
					}
					else {
						?>
                        <tr>
                        	<td colspan="4">No medication history found</td>
                        </tr>
                        <?php	
					}
					?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
<?php //echo $this->element('sql_dump'); ?>
