<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'PatientDetails',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
        	<?php 
				echo $this->Form->create("PatientDetail",array('action' => 'index','id' => 'validate'));
				echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin'));
				echo $this->Form->button('Search', array('type' => 'submit', 'escape' => true, 'class' => 'button green small float-left-margin'));
				echo $this->Form->end();
			?> 
            <table class="datagrid wf">
				<thead>
					<tr>
						<th class="sorting"><?php echo $this->Paginator->sort('first_name', 'Patient Name'); ?></th>
						<th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
                        <th class="sorting"><?php echo $this->Paginator->sort('status'); ?></th>
						<th ><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($patientDetails)) {
						foreach ($patientDetails as $PatientDetail): ?>
						<tr>
							<td align="center"><?php echo h($PatientDetail['PatientDetail']['full_name']); ?></td>
							<td align="center"><?php echo h($PatientDetail['PatientDetail']['created']); ?>&nbsp;</td>
                            <td align="center"><?php echo h($PatientDetail['PatientDetail']['status']); ?>&nbsp;</td>
							<td class="actions" align="center">
                            	<?php echo $this->Html->link(__('Details'), array('action' => 'view', $PatientDetail['PatientDetail']['id'])); ?> |
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $PatientDetail['PatientDetail']['id'])); ?> |
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $PatientDetail['PatientDetail']['id']), null, __('Are you sure you want to delete # %s?', $PatientDetail['PatientDetail']['id'])); ?>
							</td>
						</tr>
						
					<?php endforeach; 
					}
					else {
					?>
                        <tr>
                        	<td colspan="4"><?php echo NO_PATIENTS_MESSAGE?></td>
                        </tr>
                        <?php	
					}
					?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
<?php //echo $this->element('sql_dump'); ?>
