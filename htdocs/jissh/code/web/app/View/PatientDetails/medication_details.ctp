<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __("Medication Detail") ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Detail</h2>
            <button class="button dark" data-target="register" onclick="history.back()">Back</button>
		</div>
		<!-- End Header -->
		<div class="body">
            <table class="wf">
				<tr>
                	<td><label>Patient Name</label></td>
                    <td><?php echo $medication_details['PatientDetail']['full_name'];?></td>
				</tr>
                <tr>
					<td><label>Doctor Name</label></td>
                    <td><?php echo $medication_details['EmployeeDetail']['display_name'];?></td>
                </tr>
                <tr>
                	<td><label>Appointment Date</label></td>
                    <td><?php echo $medication_details['Appointment']['appointment_date'];?></td>
				</tr>
                <?php
				if(!empty($medication_details['PatientMedicationDetail'])) {
					foreach($medication_details['PatientMedicationDetail'] as $medication_detail) {
					?>
					<tr>
						<td><?php echo $medication_detail['attribute_name'];?></td>
						<td><?php echo $medication_detail['attribute_value'];?></td>
					</tr>
					<?php	
					}
				}
				?>
            </table>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
<?php //echo $this->element('sql_dump'); ?>
