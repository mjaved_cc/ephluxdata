<h1 class="border_bottom_1">Add Patient 
</h1>
<?php echo $this->Form->create('PatientDetail',array('id' => 'validate')); ?>
<section class="grid_12">
	<!-- begin box -->
	
		<!-- Box Header -->
		
            <button type="submit" onclick="PatientDetail.save();" class="button green fr fr-margin-5 small">Save</button>
<!--        	<button type="submit" onclick="PatientDetail.addToWaitingList(this);" class="button green fr fr-margin-5 small">Add to waiting list</button>
			<button type="submit" onclick="PatientDetail.createAppointment(this);" class="button green fr fr-margin-5 small">Create Appointment</button>
-->            <button type="button" class="button dark small" onclick="App.redirect({
				controller : 'PatientDetails',
				action : 'index'
			})"><?php echo __('Back to ' . $page_heading . ' Listing') ?></button>
            <input type="hidden" name="next" id="next" value="" />
		
</section>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add Patient')?></h2>
		</div>
		<!-- End Header -->

		<div class="body">
        <fieldset>
              <legend>Basic</legend>          
						
			<p>
				<div class="fl padd"><?php echo $this->Form->input('first_name', array('pattern'=>'[a-zA-Z ]*','required' => 'required', 'class' => 'large', 'div' => false)); ?></div>
                <div class="fl padd"><?php echo $this->Form->input('last_name', array('pattern'=>'[a-zA-Z ]*','required' => 'required', 'class' => 'large', 'div' => false)); ?></div>
                <div class="fl padd"><?php echo $this->Form->input('middle_name', array('pattern'=>'[a-zA-Z ]*',  'class' => 'large', 'div' => false)); ?></div>
            	<div class="fl padd"><?php echo $this->Form->input('iqama_no', array('pattern'=>'[0-9]*','required' => 'required', 'class' => 'large', 'div' => false)); ?></div>
               
               	</p>
                <br clear="all" />
				<p>
                
                <div class="fl padd"><?php echo $this->Form->input('gardian_relationship', array( 'class' => 'large','div' => false)); ?></div>
				<div class="fl padd"><?php echo $this->Form->input('patient_lives_with', array('required' => 'required', 'class' => 'large', 'div' => false)); ?></div>
            	<div class="fl padd"><?php echo $this->Form->input('sibling_order', array('required' => 'required',  'class' => 'large', 'div' => false)); ?></div>
                <div class="fl padd"><?php echo $this->Form->input('User.username', array('type'=>'email', 'class' => 'large', 'required' => 'required','div' => false)); ?></div>
            </p>
            <br clear="all" />
		</fieldset>
<?php echo $this->element('add_religion_model');?>
<?php echo $this->element('add_country_model');?>

		<!-- Box Header -->
		<fieldset>
              <legend>Additional</legend>          
					
 		<!-- End Header -->

		
        	<p>
            	<div class="fl padd"><?php echo $this->Form->input('gender', array('options' => array('male'=>'male','female'=>'female'), 'class' => 'large', 'div' => false)); ?></div>
                
				<div class="fl padd"><?php echo $this->Form->input('marital_status', array('options'=>array('single'=>'single','married'=>'married'), 'class' => 'large', 'div' => false)); ?></div>
                
                <div class="fl padd"><?php echo $this->Form->input('dob', array('class'=>'datepicker vAlign mediam','readonly'=>'readonly', 'type'=>'text', 'div' => false)); ?></div>
                
               	<div class="fl padd"><?php echo $this->Form->input('prefered_contact', array('pattern'=>'[0-9]*', 'class' => 'large', 'div' => false)); ?></div>
                
            </p>
            <br clear="all" />
            <p>
                <div class="fl padd"><?php echo $this->Form->input('religion_id', array('options' => $religions,'class' => 'med', 'div' => false)); ?><a href="#_" id="add_religion_btn" onclick="PatientDetail.openModel();" class="attach_modal add-button" data-modal="#add_religion">Add</a></div>
            
                <div class="fl padd"><?php echo $this->Form->input('mobile', array('pattern'=>'[0-9]*','class' => 'large', 'div' => false)); ?></div>
                <div class="fl padd"><?php echo $this->Form->input('po_box', array('pattern'=>'[0-9]*','class' => 'large', 'div' => false)); ?></div>
            
                <div class="fl padd"><?php echo $this->Form->input('postal_code', array('class' => 'large', 'div' => false)); ?></div>
                </p>
            <br clear="all" />
            <p>
				<div class="fl padd"><?php echo $this->Form->input('country_id', array('options' => $countries, 'class' => 'med', 'div' => false)); ?><a href="#_" id="add_country_btn" onclick="PatientDetail.openModel();" class="attach_modal add-button" data-modal="#add_country">Add</a></div>
           
                <div class="fl padd"><?php echo $this->Form->input('district', array('class' => 'large', 'div' => false)); ?></div>
                <div class="fl padd"><?php echo $this->Form->input('city', array('pattern'=>'[a-zA-z ]*','class' => 'large', 'div' => false)); ?></div>
			
				<div class="fl padd"><?php echo $this->Form->input('residence_no', array('class' => 'large', 'div' => false)); ?></div>
                </p>
            <br clear="all" />
            <p>
                <div class="fl padd"><?php echo $this->Form->input('refered_by', array('class' => 'large', 'div' => false)); ?></div>
			</p>
            
            <br clear="all" />
	
	
    </fieldset>


<!-- Box Header -->
		<fieldset>
        <legend>Parent's/Guardian Details</legend> 
		<!-- End Header -->


        	<table class="display dataTable">
            	<tr>
                	<td></td>
                    <td>Email</td>
                    <td>Work No.</td>
                    <td>Mobile No.</td>
                    <td>Occupation</td>
                    <td>Name</td>
                </tr>
            	<?php 
				$i=0;
				foreach($relationships as $key => $val) {
				?>
            	<tr>
                	<td class="sorting_1">
                    <input type="hidden" name="data[PatientFamilyDetail][<?php echo $i;?>][relation_ship_id]" value="<?php echo $key;?>" /><?php echo $val;?>
                    </td>
                    <td>
                    	<?php echo $this->Form->input('PatientFamilyDetail.'.$i.'.email', array('type'=>'email','label' => false,'div' => false)); ?>
                    </td>
                    <td>
                    	<?php echo $this->Form->input('PatientFamilyDetail.'.$i.'.work_number', array('pattern'=>'[0-9]*','type'=>'text','label' => false,'div' => false)); ?>
					</td>
                    <td>
						<?php echo $this->Form->input('PatientFamilyDetail.'.$i.'.mobile_number', array('pattern'=>'[0-9]*','type'=>'text','label' => false,'div' => false)); ?>
					</td>
                    <td>
						<?php echo $this->Form->input('PatientFamilyDetail.'.$i.'.occupation', array('pattern'=>'[a-zA-z ]*','type'=>'text','label' => false,'div' => false)); ?>
					</td>
                    <td>
						<?php echo $this->Form->input('PatientFamilyDetail.'.$i.'.name', array('pattern'=>'[a-zA-z ]*', 'type'=>'text','label' => false,'div' => false)); ?>
					</td>
                </tr>
                <?php
					$i++;
				}
				?>
            </table>
	
	
    </fieldset>

		<!-- Box Header -->
		<fieldset>
              <legend>Declaration/Conset</legend>  
		
		<!-- End Header -->
		<?php
		$options = array('1' => 'Agree', '0' => 'Disagree');
		$attributes = array('legend' => false);
		?>
        	
                <div class="fl width-80">
                	<b>1.</b> I hereby authorize JISH to request information regarding the patient
                	if needed, from other professionals or professional agencies (i.e. referal sources).
                </div>
                <div class="frt">
                	<?php echo $this->Form->radio('allow_info_from_agencies', $options, $attributes); ?>
                 </div>
             
             <br clear="all" />
            <p>
                <div class="fl width-80">
                	<b>2.</b> Hereby authorize JISH to release information regarding the patirnt to other professionals or professional agenices (i.e. referal sources).</div>
                    
                <div class="frt">
                	<?php echo $this->Form->radio('allow_info_to_agencies', $options, $attributes); ?>
                 </div>
             </p>
             <br clear="all" />
            <p>
                <div class="fl width-80">
                	<b>3.</b> Sessions are audio/video taped for clinical purposes. These
                    recordings are confidentials medical documents and are exclusive JISH 
                    property and will be systematically erased. I have no objections to the
                    use of these recordings for the purpose of evaluation and treatment.</div>
                    
                <div class="frt">
                	<?php echo $this->Form->radio('multimedia_agreement', $options, $attributes); ?>
                 </div>
             </p>
             <br clear="all" />
            <p>
                <div class="fl width-80">
                	<b>4.</b> I also have no objection to the use of these recordings for reasearch and educational purposes.
                    All therapy sessions are subject to surveillance via CCTV cameras
                    (Closed Ciruits Television).</div>
                    
                <div class="frt">
                	<?php echo $this->Form->radio('multimedia_for_research', $options, $attributes); ?>
                 </div>
             </p>
             <br clear="all" />
</fieldset>

 
		
    </div>
</section>
<section class="grid_12">
	

		<div class="body">
            <button type="submit" onclick="PatientDetail.save();" class="button green fr fr-margin-5 small">Save</button>
<!--        	<button type="submit" onclick="PatientDetail.addToWaitingList(this);" class="button green fr fr-margin-5 small">Add to waiting list</button>
			<button type="submit" onclick="PatientDetail.createAppointment(this);" class="button green fr fr-margin-5 small">Create Appointment</button>
-->            <button type="button" class="button dark small" onclick="App.redirect({
				controller : 'PatientDetails',
				action : 'index'
			})"><?php echo __('Back to ' . $page_heading . ' Listing') ?></button>
		</div>

</section>
<?php echo $this->Form->end(); ?>