<?php //pr($PatientDetail);?>
<span class="fr">
<button class="button light small" data-target="register" onclick="App.redirect({
    controller : 'PatientDetails',
    action : 'index'
})"><?php echo __('Back to ' . $page_heading . ' Listing') ?></button>
<button class="button light small" data-target="register" onclick="PatientDetail.redirect('<?php echo $this->Html->url(array(
    "controller" => "PatientDetails",
    "action" => "medication_history",
    $PatientDetail['PatientDetail']['id']
));?>')"><?php echo __('View Medication Histrory ') ?></button>
<button class="button light smallk" onclick="PatientDetail.redirect('<?php echo $this->Html->url(array(
    "controller" => "appointments",
    "action" => "create",
    $PatientDetail['PatientDetail']['id']
));?>')"><?php echo __('Create Appointment') ?></button>
</span>
<h1 class="border_bottom_1">Patient Profile</h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		
		<!-- End Header -->

		<div class="body no_padding">
			<!-- Horizontal Navigation -->
			<ul class="tabs clearfix">
				<li class="active"><a href="javascript:void(0);">Basic</a></li>
				<li><a id="get_working_hr" href="javascript:void(0);" >Sessions</a></li>				
			</ul>
            <div class="pane padding20"><!-- Pane 1 -->
				<div class="clearfix">
					<section class="grid_10">
                    <div class="flt padd" style="padding-right:200px;">
                    <table class="wf">
                        <tr>
                            <td><label for="PatientDetailFirstName">First Name</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['first_name'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Last Name</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['last_name'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Middle Name</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['middle_name'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Iqama No.</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['iqama_no'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Gardian Relationship</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['gardian_relationship'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Username</label></td>
                            <td><?php echo $PatientDetail['User']['username'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Gender</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['gender'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Marital Status</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['marital_status'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Dob</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['dob'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Prefered Contact</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['prefered_contact'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Religion</label></td>
                            <td><?php echo $PatientDetail['Religion']['name'];?></td>
                        </tr>
                    </table>
                    </div>
                    <div class="flt padd">
                    <table class="wf">
                        <tr>
                            <td><label for="PatientDetailFirstName">Patient Lives With</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['patient_lives_with'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Sibling Order</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['sibling_order'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Username</label></td>
                            <td><?php echo $PatientDetail['User']['username'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Mobile</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['mobile'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Po Box</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['po_box'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Postal Code</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['postal_code'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Country</label></td>
                            <td><?php echo $PatientDetail['Country']['name'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">District</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['district'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">City</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['city'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Residence No</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['residence_no'];?></td>
                        </tr>
                        <tr>
                            <td><label for="PatientDetailFirstName">Refered By</label></td>
                            <td><?php echo $PatientDetail['PatientDetail']['refered_by'];?></td>
                        </tr>
                    </table>
                    </div>
                    </section>
                </div> 
                <div class="clearfix">
                    <section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __("Parient's/Guardian Details")?></h2>
		</div>
		<!-- End Header -->

		<div class="body">
        	<table class="wf display">
            	<tr>
                	<td></td>
                    <td>Email</td>
                    <td>Work No.</td>
                    <td>Mobile No.</td>
                    <td>Occupation</td>
                    <td>Name</td>
                </tr>
            	<?php 
				$i=0;
				if(!empty($PatientDetail['PatientFamilyDetail'])) {
					foreach($PatientDetail['PatientFamilyDetail'] as $key => $val) {
						$id = isset($val['id']) ? $val['id'] : "";
						$email = isset($val['email']) ? $val['email'] : "";
						$work_number = isset($val['work_number']) ? $val['work_number'] : "";
						$mobile_number = isset($val['mobile_number']) ? $val['mobile_number'] : "";
						$occupation = isset($val['occupation']) ? $val['occupation'] : "";
						$name = isset($val['name']) ? $val['name'] : "";
						$relation_ship_name = isset($val['relation_ship_name']) ? $val['relation_ship_name'] : "";
					?>
					<tr>
						<td class="sorting_1">
						<?php echo $relation_ship_name;?>
						</td>
						<td>
							<?php echo $email; ?>
						</td>
						<td>
							<?php echo $work_number; ?>
						</td>
						<td>
							<?php echo $mobile_number; ?>
						</td>
						<td>
							<?php echo $occupation; ?>
						</td>
						<td>
							<?php echo $name; ?>
						</td>
					</tr>
					<?php
						$i++;
					}
				}
				else {
					?>
                    <tr>
                    	<td colspan="6"><?php echo NO_PATIENT_FAMILY_DETAIL_MESSAGE;?></td>
                    </tr>
                    <?php	
				}
				?>
            </table>
		</div>
	</div>
</section>
				</div>
                <div class="clearfix">	
                    <section class="grid_12">
                        <!-- begin box -->
                        <div class="box">
                            <!-- Box Header -->
                            <div class="header clearfix">
                                <h2><?php echo __('Declaration/Conset')?></h2>
                            </div>
                            <!-- End Header -->
                            <?php
                            $options = array('1' => 'Agree', '0' => 'Disagree');
                            $attributes = array('legend' => false);
                            ?>
                            <div class="body">
                                <p>
                                    <div class="flt" style="width:800px;">
                                        <b>1.</b> I hereby authorize JISH to request information regarding the patient
                                        if needed, from other professionals or professional agencies (i.e. referal sources).
                                    </div>
                                    <div class="frt">
                                        <?php 
                                        echo ($PatientDetail['PatientDetail']['allow_info_from_agencies']==1) ? "Agree" : "Disagree";
                                        //echo $this->Form->radio('allow_info_from_agencies', $options, $attributes); ?>
                                     </div>
                                 </p>
                                 <br clear="all" />
                                <p>
                                    <div class="flt" style="width:800px;">
                                        <b>2.</b> Hereby authorize JISH to release information regarding the patirnt to other professionals or professional agenices (i.e. referal sources).</div>
                                    <div class="frt">
                                        <?php 
                                        echo ($PatientDetail['PatientDetail']['allow_info_to_agencies']==1) ? "Agree" : "Disagree";
                                        //echo $this->Form->radio('allow_info_from_agencies', $options, $attributes); ?>
                                     </div>
                                 </p>
                                 <br clear="all" />
                                <p>
                                    <div class="flt" style="width:800px;">
                                        <b>3.</b> Sessions are audio/video taped for clinical purposes. These
                                        recordings are confidentials medical documents and are exclusive JISH 
                                        property and will be systematically erased. I have no objections to the
                                        use of these recordings for the purpose of evaluation and treatment.</div>
                                    <div class="frt">
                                        <?php 
                                        echo ($PatientDetail['PatientDetail']['multimedia_agreement']==1) ? "Agree" : "Disagree";
                                        //echo $this->Form->radio('allow_info_from_agencies', $options, $attributes); ?>
                                     </div>
                                 </p>
                                 <br clear="all" />
                                <p>
                                    <div class="flt" style="width:800px;">
                                        <b>4.</b> I also have no objection to the use of these recordings for reasearch and educational purposes.
                                        All therapy sessions are subject to surveillance via CCTV cameras
                                        (Closed Ciruits Television).</div>
                                    <div class="frt">
                                        <?php 
                                        echo ($PatientDetail['PatientDetail']['multimedia_for_research']==1) ? "Agree" : "Disagree";
                                        //echo $this->Form->radio('allow_info_from_agencies', $options, $attributes); ?>
                                     </div>
                                 </p>
                                 <br clear="all" />
                            </div>
                        </div>
                    </section>
                </div>    
                    <?php /*?><section class="grid_12">
                        <!-- begin box -->
                    
                        <div class="box">
                            <!-- Box Header -->
                            <div class="header clearfix">
                                <h2><?php echo __('Actions')?></h2>
                            </div>
                            <!-- End Header -->
                    
                            <div class="body">
                                <button type="button" onclick="App.redirect({
                                    controller : 'PatientDetails',
                                    action : 'index'
                                })" class="button green small">Back</button>
                            </div>
                        </div>
                    </section><?php */?>
                   
			</div>
            <div class="pane padding20">
                <div id="ajax_response"> 
                <?php echo $this->element('ajax_session_listing'); ?>
				</div>
            </div>
		</div>
	</div>
</section>



