<div class="countries view">
<h2><?php  echo __('Country'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($country['Country']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($country['Country']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nationality'); ?></dt>
		<dd>
			<?php echo h($country['Country']['nationality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($country['Country']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($country['Country']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($country['Country']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Country'), array('action' => 'edit', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Country'), array('action' => 'delete', $country['Country']['id']), null, __('Are you sure you want to delete # %s?', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Patient Details'), array('controller' => 'patient_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Patient Detail'), array('controller' => 'patient_details', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Patient Details'); ?></h3>
	<?php if (!empty($country['PatientDetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Iqama No'); ?></th>
		<th><?php echo __('Marital Status'); ?></th>
		<th><?php echo __('Religion Id'); ?></th>
		<th><?php echo __('Allow Info From Agencies'); ?></th>
		<th><?php echo __('Allow Info To Agencies'); ?></th>
		<th><?php echo __('Multimedia Agreement'); ?></th>
		<th><?php echo __('Multimedia For Research'); ?></th>
		<th><?php echo __('Mobile'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Po Box'); ?></th>
		<th><?php echo __('Postal Code'); ?></th>
		<th><?php echo __('District'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th><?php echo __('Dob'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Middle Name'); ?></th>
		<th><?php echo __('Picture'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($country['PatientDetail'] as $patientDetail): ?>
		<tr>
			<td><?php echo $patientDetail['id']; ?></td>
			<td><?php echo $patientDetail['user_id']; ?></td>
			<td><?php echo $patientDetail['iqama_no']; ?></td>
			<td><?php echo $patientDetail['marital_status']; ?></td>
			<td><?php echo $patientDetail['religion_id']; ?></td>
			<td><?php echo $patientDetail['allow_info_from_agencies']; ?></td>
			<td><?php echo $patientDetail['allow_info_to_agencies']; ?></td>
			<td><?php echo $patientDetail['multimedia_agreement']; ?></td>
			<td><?php echo $patientDetail['multimedia_for_research']; ?></td>
			<td><?php echo $patientDetail['mobile']; ?></td>
			<td><?php echo $patientDetail['phone']; ?></td>
			<td><?php echo $patientDetail['po_box']; ?></td>
			<td><?php echo $patientDetail['postal_code']; ?></td>
			<td><?php echo $patientDetail['district']; ?></td>
			<td><?php echo $patientDetail['city']; ?></td>
			<td><?php echo $patientDetail['country_id']; ?></td>
			<td><?php echo $patientDetail['gender']; ?></td>
			<td><?php echo $patientDetail['dob']; ?></td>
			<td><?php echo $patientDetail['first_name']; ?></td>
			<td><?php echo $patientDetail['last_name']; ?></td>
			<td><?php echo $patientDetail['middle_name']; ?></td>
			<td><?php echo $patientDetail['picture']; ?></td>
			<td><?php echo $patientDetail['status']; ?></td>
			<td><?php echo $patientDetail['created']; ?></td>
			<td><?php echo $patientDetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'patient_details', 'action' => 'view', $patientDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'patient_details', 'action' => 'edit', $patientDetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'patient_details', 'action' => 'delete', $patientDetail['id']), null, __('Are you sure you want to delete # %s?', $patientDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Patient Detail'), array('controller' => 'patient_details', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
