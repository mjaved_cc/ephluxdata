<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
		</div>
		<!-- End Header -->
		<div class="body">        	
            <table class="datagrid wf">
				<thead>
					<tr>
						<th><?php echo __('Action'); ?></th>
						<th class="sorting"><?php echo $this->Paginator->sort('description'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($authActionMaps as $authActionMap): ?>
						<tr>							
							<td align="center"><?php echo $authActionMap['Controller']['alias'] . '/' . $authActionMap['Action']['alias']; ?></td>
							<td align="center"><?php echo h($authActionMap['AuthActionMap']['description']); ?></td>
							<td class="actions" align="center">
								<?php echo $this->Html->link(__('View'), array('action' => 'view', $authActionMap['AuthActionMap']['id'])); ?>
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $authActionMap['AuthActionMap']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination', array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>