<?php

class MiscHelper extends AppHelper {

	public function make_current_tab($controller) {

		return ($this->request->params['controller'] == $controller) ? 'class = "current"' : '';
	}

	/**
	 * Check if string is empty
	 * 
	 * @param string $text
	 * @return boolean true on success
	 */
	function is_valid_string($text) {

		if (is_string($text) && strlen($text) > 0)
			return true;

		return false;
	}

	/**
	 * Get string.
	 * 
	 * @param string $text
	 * @return string 
	 */
	function get_string($text) {
		return ($this->is_valid_string($text)) ? $text : '';
	}

	/**
	 * Check existance - isset , empty etc
	 * 
	 * @param mixed $data
	 * @return mixed $data on success else false
	 */
	function is_not_empty($data) {
		if (isset($data) && !empty($data))
			return $data;
		return false;
	}

	/**
	 * Get fully qualified url
	 * 
	 * @param string $rel_url relative url
	 * @return string
	 */
	function get_fully_qualified_url($rel_url) {
		return Router::url('/', true). $rel_url;
	}
	
	/**
	 * encodes json
	 * @param array $data 
	 * @return string json
	 */
	function encoding_json($data) {

		return json_encode($data);
	}
	
	/**
	 * decodes json
	 * @param array $data 
	 * @return string json
	 */
	function decoding_json($data) {

		return json_decode($data, true);
	}
	
	/**
	 * Get default profile picture url
	 * 
	 * @return string URL
	 */
	function get_default_dp(){
		return $this->get_fully_qualified_url(DEFAULT_THUMB_DP);
	}
	
	/**
	 * Get profile thumbnail URL.
	 * 
	 * If given param is empty it will return default profile thumbnail url.
	 * 
	 * @param string $profile_thumb_url Profile Thumbnail URL
	 */
	function get_profile_thumb_url($profile_thumb_url){
		if(!empty($profile_thumb_url)){
			return $this->get_fully_qualified_url($profile_thumb_url);
		} 
		
		return $this->get_default_dp();
	}
	
	/**
	 * Get checked tag
	 * 
	 * @param string|int $needle value used
	 * @param string|int $compare_against value compare against
	 * @return string
	 */
	function get_radio_checked_tag($needle , $compare_against){
		return ($needle == $compare_against) ? 'checked = "checked"' : '';
	}

	

}

?> 