<?php

class AccessHelper extends Helper {

	public $helpers = array('Session');
	private $_access;	
	private $_auth;	

	public function __construct(View $view, $settings = array()) {
		
		App::import('Component', 'Access');
		App::import('Component', 'Auth');
		
		$this->_access = new AccessComponent($settings['component_collection']);		
		$this->_auth = new AuthComponent($settings['component_collection']);		
	}

	/** 	 
	 * Main ACL check function. Checks to see if the ARO (access request object) has access to the ACO (access control object).
	 * 
	 * Example controller name is basically prefix of Controller class. If class name is ConfigOptionsController then name will be ConfigOptions.
	 * 
	 * @param string $controller The name of this controller. Controller names are plural, named after the model they manipulate.
	 * @param string $action action
	 * @return boolean
	 */
	function check($controller, $action) {

		return $this->_access->check_helper($controller, $action);
	}
	
	/**
	 * Get my given details
	 * 
	 * @param string $what Key value for $this->Auth->User()
	 * @return string
	 */
	function get_my($what){ 
		if($this->_auth->User())
			return $this->_auth->User($what) ;		
	}
}

?>