<?php

class FormWrapperHelper extends AppHelper {

	public $helpers = array('Form', 'Misc', 'Html');

	/**
	 * Get scrollable slider links on dashboard
	 * 
	 * @param string $text Alt text etc
	 * @param string $image_name image name only not source
	 * @param string $action format e.g; controller/action
	 * 
	 * @return string link
	 */
	function get_jcarousel_link($text, $image_name, $action) {

		return '<li class="content" title="' . $text . '">
				<a href="' . $this->Misc->get_fully_qualified_url($action) . '" class="thumb" ><img src="' . $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'carousel/' . $image_name) . '" alt="' . $text . '" width="64" height="64" /><span class="text">' . $text . '</span></a></li>';
	}

	/**
	 * Get drop down menu
	 * 
	 * @param string $field_name Field name
	 * @param array $options Options
	 * @param string $selected Mark selected
	 * @param string $empty blank option with an empty value in your drop down - Select Option
	 * 
	 * @return string Drop downm
	 */
	function get_drop_down($field_name, $options, $selected = false, $empty = false) {
		$settings = array(
			'type' => 'select',
			'options' => $options,
			'empty' => $empty,
			'selected' => $selected,
			'id' => $field_name
		);
		return $this->get_text_box($field_name, $settings);
	}

	/**
	 * Echo drop down check list along with Label. As multi-select does not support label hence using our own.
	 * 
	 * @param string $field_name Field name
	 * @param array $options Options
	 * @param string $selected Mark selected
	 * @param string $class default value dd_checklist for fancy multiple select box
	 * 
	 * @return void
	 */
	function echo_dd_checklist($field_name, $options, $selected = false, $class = 'dd_checklist') {
		$settings = array(
			'type' => 'select',
			'multiple' => 'multiple',
			'id' => $field_name,
			'selected' => $selected,
			'options' => $options,
			'class' => $class // drop down check list
		);

		echo $this->get_text_box($field_name, $settings);
	}

	/**
	 * Get datepicker control 
	 * 
	 * It will use class name datepicker.
	 * 
	 * @param string $field_name Field name
	 * @param array $options Cakephp input options
	 * @return HTML
	 */
	function get_datepicker($field_name, $options = array()) {

		$settings['class'] = 'datepicker';

		if (!empty($options))
			$settings = array_merge($settings, $options);

		return $this->get_server_side_txtbx($field_name, $settings);
	}

	/**
	 * Get textbox with field validation @ both sides (client & server).
	 * 
	 * The only difference between cakephp input & this server side input/text box is as how it displays validation msgs.
	 * 
	 * Here it is formatted by setting additional params.
	 * @param string $field_name Field name
	 * @param array $options Cakephp input options
	 * @return HTML	
	 */
	function get_server_side_txtbx($field_name, $options = array()) {

		$settings['required'] = 'required';

		if (!empty($options))
			$settings = array_merge($settings, $options);

		return $this->get_text_box($field_name, $settings);
	}

	/**
	 * Get textbox with field validation @ both sides (client & server).
	 * 
	 * The only difference between cakephp input & this server side input/text box is as how it displays validation msgs.
	 * 
	 * Here it is formatted by setting additional params.
	 * @param string $field_name Field name
	 * @param array $options Cakephp input options
	 * @return HTML	
	 */
	function get_text_box($field_name, $options = array()) {

		$settings['type'] = 'text';
		$settings['div'] = false;
		$settings['error'] = array('attributes' => array('wrap' => 'span', 'class' => 'form_error'));

		if (!empty($options))
			$settings = array_merge($settings, $options);

		return $this->Form->input($field_name, $settings);
	}

	/**
	 * Get radio button
	 * 
	 * Here it is formatted by setting additional params.
	 * 
	 * @param string $field_name Field name
	 * @param array $options Cakephp input options
	 * @param array $attributes Cakephp attributes
	 * @return HTML	
	 */
	function get_radio_button($field_name, $options, $attributes) {

		//return $this->Form->radio($field_name, $options, $attributes);
		return $this->Form->input($field_name, array(
				'before' => '',
				'after' => '',
				'between' => '',
				'separator' => '',
				'options' => $options,
				'type' => 'radio',
				'legend' => false,
				'div' => false,
				'label' => '',
				'value' => '0'
			));
	}

	/**
	 * Get radio button
	 * 
	 * Here it is formatted by setting additional params.
	 * 
	 * @param string $title The content to be wrapped by <a> tags.
	 * @param array $url Cake-relative URL or array of URL parameters
	 * @return HTML	
	 */
	function get_anchor($title, $url = array()) {

		$settings['full_base'] = true;

		if (!empty($url))
			$settings = array_merge($settings, $url);

		return $this->Html->link($title, $settings);
	}

	/**
	 * Get selection mode for patients
	 * 
	 * Modes : direct or from waiting list 
	 * 
	 * Used in appointments module
	 * 
	 * @return HTML
	 */
	function get_patient_selection_mode_radio() {

		$options = array('From Waiting List','By Name');
		
		// to set which value should be selected default.		
		$attributes = array('legend' => false , 'value' => '0');
		return $this->get_radio_button('patient_selection_mode', $options, $attributes);
	}
	
	function get_appointment_selection_mode_radio() {

		$options = array('Individual','Group');
		
		// to set which value should be selected default.		
		$attributes = array('legend' => false , 'value' => '0');
		return $this->get_radio_button('is_group', $options, $attributes);
	}
	
	/**
	 * Get button
	 * 
	 * @param string $title button text
	 * @param array $options Cakephp input options
	 * @return HTML
	 */
	function get_button($title , $options){
		
		$settings['type'] = 'button';
		$settings['div'] = false;
		$settings['class'] = 'button green small';		
		$settings['id'] = 'btn_' . strtolower($title) ;		

		if (!empty($options))
			$settings = array_merge($settings, $options);
		
		return $this->Form->button($title, $settings);
	}

}