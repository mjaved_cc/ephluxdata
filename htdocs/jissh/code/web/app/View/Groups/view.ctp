<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<div class="clear"></div>
<?php if (!empty($group)) { ?>
	<section class="grid_8">
		<!-- begin box -->
		<div class="box">
			<!-- Box Header -->
			<div class="header clearfix">
				<h2><?php echo h($group['Group']['name']); ?></h2>
			</div>
			<!-- End Header -->
			<div class="body">
				<table class="display dataTable">
					<thead>
						<tr>
							<th><?php echo __('First Name'); ?></th>
							<th><?php echo __('Last Name'); ?></th>
							<th><?php echo __('Username'); ?></th>							
							<th><?php echo __('Created'); ?></th>					
							<th><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($group['User'] as $user): ?>
							<tr>
								<td class="center"><?php echo $user['first_name']; ?></td>
								<td class="center"><?php echo $user['last_name']; ?></td>
								<td class="center"><?php echo $user['username']; ?></td>
								<td class="center"><?php echo $user['created']; ?></td>
								<td class="center">
									<?php echo $this->Html->link(__('Details'), array('controller' => 'users', 'action' => 'view', $user['id'], 'full_base' => true)); ?>
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'], 'full_base' => true)); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'auth_action_maps', 'action' => 'delete', $user['id'], 'full_base' => true), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php echo $this->element('pagination', array('pagination_params' => $pagination_params)); ?>
				<!--End Pagination-->
			</div>
			<!-- End box body -->
		</div>
		<!-- End box -->
	</section>
	<section class="grid_4">
		<div class="box">
			<div class="header clearfix">
				<h2>Actions</h2>
			</div>
			<div class="body">
				<ul>
					<li><?php echo $this->Html->link(__('Edit Group'), array('action' => 'edit', $group['Group']['id'], 'full_base' => true)); ?> </li>
					<li><?php echo $this->Form->postLink(__('Delete Group'), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?> </li>
					<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index', 'full_base' => true)); ?> </li>
					<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add', 'full_base' => true)); ?> </li>
					<li><?php echo $this->Html->link(__('List User'), array('controller' => 'users', 'action' => 'index', 'full_base' => true)); ?> </li>
					<li><?php echo $this->Html->link(__('Edit Permissions'), array('controller' => 'groups', 'action' => 'edit_permissions', $group['Group']['id'], 'full_base' => true)); ?> </li>
				</ul>

			</div>
		</div>
	</section>
	<?php
} else {
	echo $this->element('notification', array(
		"message" => "No user belongs to this group",
		"class" => "info",
		"image_name" => "info_icon.png"
	));
}
?>
