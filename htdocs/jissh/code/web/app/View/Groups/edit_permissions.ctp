<section class="grid_8">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Permissions'); ?></h2>
		</div>
		<!-- End Header -->
		<div class="body">
			<?php
			echo $this->Form->create('Group');
			if (!empty($permissions)) {
				?>
				<table class="datagrid wf">
					<thead>
						<tr>
							<th>Permissions</th>
							<th>Allow</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($permissions as $feature => $data) { ?>
							<tr>
								<td colspan="2"  class="left"><h3><?php echo $feature ?></h3></td>
							</tr>
							<?php foreach ($data as $permission) { ?>
								<tr>
									<td class="left"><?php echo $permission['AuthActionMap']['description'] ?></td>
									<td class="center"><?php
					echo $this->Form->checkbox('is_allow', array(
						'hiddenField' => false,
						'name' => 'is_allowed[' . $permission['Parent']['alias'] . '/' . $permission['Child']['alias'] . ']',
						'checked' => $permission['is_allowed']
					));
								?></td>
								</tr>
								<?php
							}
						}
						?>	
					</tbody>
				</table>
			<?php } ?>
			<p style="margin-top: 10px;"><?php echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small')); ?></p>
			<?php echo $this->Form->end(); ?>

<!--			<p style="margin: 1em 0;"><?php echo $this->Form->submit('Submit', array('class' => 'button green small', 'div' => false)); ?></p>-->
		</div>
	</div>
</section>

<section class="grid_4">
	<div class="box">
		<div class="header clearfix">
			<h2>Actions</h2>
		</div>
		<div class="body">
			<ul>					
				<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index', 'full_base' => true)); ?></li>
				<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index', 'full_base' => true)); ?> </li>
			</ul>
		</div>
	</div>
</section>

<!--<div class="main_column">
	<div class="box">
		<div class="body">
<?php
echo $this->Form->create('Group');
if (!empty($permissions)) {
	?>
							<table class="display dataTable">
								<thead>
									<tr>
										<th>Permissions</th>
										<th>Allow</th>
									</tr>
								</thead>
								<tbody>
	<?php foreach ($permissions as $feature => $data) { ?>
													<tr>
														<td colspan="2"><h3><?php echo $feature ?></h3></td>
													</tr>
		<?php foreach ($data as $permission) { ?>
																	<tr>
																		<td><?php echo $permission['AuthActionMap']['description'] ?></td>
																		<td><?php
			echo $this->Form->checkbox('is_allow', array(
				'hiddenField' => false,
				'name' => 'is_allowed[' . $permission['Parent']['alias'] . '/' . $permission['Child']['alias'] . ']',
				'checked' => $permission['is_allowed']
			));
			?></td>
																	</tr>
			<?php
		}
	}
	?>	
								</tbody>
							</table>
<?php } ?> 
			<p style="margin: 1em 0;"><?php echo $this->Form->submit('Submit', array('class' => 'button2', 'div' => false)); ?></p>
		</div>
	</div>
</div>-->
