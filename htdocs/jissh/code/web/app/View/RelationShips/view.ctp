<div class="relation_ships view">
<h2><?php  echo __('RelationShip'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($relation_ship['RelationShip']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($relation_ship['RelationShip']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($relation_ship['RelationShip']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($relation_ship['RelationShip']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($relation_ship['RelationShip']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit RelationShip'), array('action' => 'edit', $relation_ship['RelationShip']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete RelationShip'), array('action' => 'delete', $relation_ship['RelationShip']['id']), null, __('Are you sure you want to delete # %s?', $relation_ship['RelationShip']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List RelationShips'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New RelationShip'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>