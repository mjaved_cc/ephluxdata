<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Appointmetns</h2>
		</div>
		<!-- End Header -->
		<div class="body">
        	
            <div id="ajax_response"> 
            	<?php $pagination_params = $this->Paginator->params(); ?>
				<?php 
                    $q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
                    echo $this->Form->create("Appointment",array('action' => 'search','id' => 'validate'));
                    ?>
                    <?php echo $this->Form->input("q", array('label' => 'Search for', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
                    //echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
                    ?>
                    
                    <div id="advanceSearchPanel" style="display:none;"><br clear="all" />
                    	<p><?php echo $this->Form->input('fromDate', array('class'=>'datepicker vAlign mediam','readonly'=>'readonly', 'type'=>'text', 'div' => false)); ?></p>
                        <p><?php echo $this->Form->input('toDate', array('class'=>'datepicker vAlign mediam','readonly'=>'readonly', 'type'=>'text', 'div' => false)); ?></p>
                    
                    </div>
                    <button type="submit" class="button green small float-left-margin" id="ajax_search_btn">Search</button>
                    <div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>;position: absolute;margin-left: -45px;z-index: 6;">
                        <a href="#_" onclick="App.redirect({
                                controller : 'Appointments',
                                action : 'search'
                            })">X</a>
                    </div>
                    <p id="advanceSearchLink">
                    	<a href="#_" onclick="showAdvanceSearch();">Advanced</a>
                    </p>
                    <p id="basicSearchLink" style="display:none;">
                    	<a href="#_" onclick="showBasicSearch();">Basic</a>
                    </p>
                    <?php
                    echo $this->Form->end();
                ?> 
                
                <table class="datagrid wf">
                    <thead>
                        <tr>
                            <th><?php echo __('Patient Name'); ?></th>
                            <th><?php echo __('Employee Name'); ?></th>
                            <th><?php echo __('Appointment Date'); ?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($appointments)) {
                            foreach ($appointments as $appointment): ?>
                            <tr>
                                <td align="center"><?php echo $appointment['PatientDetail']['full_name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['EmployeeDetail']['display_name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['Appointment']['created']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['Appointment']['status']; ?>&nbsp;</td>
                                <td class="actions" align="center">		
                                	<?php echo $this->Html->link(__('Detail'), array('action' => 'view',$appointment['Appointment']['id'])); ?> 					
                                </td>
                            </tr>
                        <?php endforeach; 
                        }
                        else{
                            ?>
                            <tr>
                                <td colspan="9">No appointments found.</td>
                            </tr>
                            <?php		
                        }
                        ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            </div>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
