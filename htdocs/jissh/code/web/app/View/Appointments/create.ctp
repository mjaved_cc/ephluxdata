<!--========= Wizard  =========-->
<section class="grid_12">
	<!-- Begin box -->
	<div class="box">

		<!-- Begin box header -->
		<div class="header clearfix">
			<h2><?php echo $page_heading ?></h2>
		</div>
		<!-- End header -->

		<!-- Begin body -->
		<div class="body clearfix no_padding">
			<?php echo $this->Form->create('Appointment', array('id' => 'validate', 'class' => 'wizard padding20')); ?>
            <fieldset id="">
				<legend>Appointment type</legend>
				<p id="appointment_type_mode"><?php echo $this->FormWrapper->get_appointment_selection_mode_radio() ?></p>	
			</fieldset>
			<?php if(!$patientId) { ?>
			<fieldset id="">
				<legend>Patient Selection</legend>
				<p id="patient_selection_mode"><?php echo $this->FormWrapper->get_patient_selection_mode_radio() ?></p>	
                <div id="pat_selection_list">		
                    <div class="padding20 patient_selection_mode">
                        <p><?php echo $this->FormWrapper->get_drop_down('waiting_list_title', $waiting_list_titles) ?></p>
                        <div id="patient_listing_0">
    
                            <?php
                            echo $this->element('appointment_patient_listing', array(
                                'data' => $patient_details
                            ));
                            ?>	
                        </div>
                    </div>				
                    <div class="padding20 hide patient_selection_mode">
                        <p><?php
                            echo $this->FormWrapper->get_text_box('search_patient', array('class' => 'medium')) . ' ' .
                            $this->FormWrapper->get_button('Search', array('class' => 'button green', 'id' => 'search_patient'))
                            ?>
                        </p>
                        <div id="patient_listing_1">
    
                            <?php
                            echo $this->element('appointment_patient_listing', array(
                                'data' => $all_patients
                            ))
                            ?>
                        </div>
                    </div>
                </div>
			</fieldset>
            <?php
			}
			else{
				?>
                <fieldset id="pat_selection_list">
				<legend>Patient Selection</legend>
                <input name="data[PatientDetail][id]" value="<?php echo $patientId; ?>" type="radio" checked="checked" />
                <?php
				echo $patientDetail['full_name'];
				//echo $this->Form->hidden('data[PatientDetail][id]' , array('value' => $patientId)); 
				?>
                
                </fieldset>
                <?php	
			}
			
			?>
			<fieldset>
				<legend>Clinician Selection</legend>
                <div class="padding20">
                	<?php 
					foreach($allowed_service_groups as $key=>$value) {
						?>
                        <?php echo $value;?> <input type="radio" name="service_group" id="service_group" value="<?php echo $key;?>" />
                        <?php	
					}
					?>
                </div>
				<div id="appointment_doctor_listing" class="padding20">
					<?php 
					if($employeeId!=0) {
						
						?>
                        <input type="radio" checked="checked" value="<?php echo $employeeId;?>" name="data[User][id]"> <?php echo $employeeDetail['first_name'];?>
                        <script>
							setTimeout(function(){
							Appointment.get_doctor_appointment_schedule();	
							},5000)
						</script>
                        <?php
						echo $this->Form->hidden('EmployeeDetail.id' , array('value' => $employeeDetail['id'] , 'id' => 'employee_detail_id' . $employeeId));
					}
					else {
						echo $this->element('appointment_doctor_listing', array('available_doctors' => $available_doctors));
						
					}?>
				</div>
			</fieldset>
            
			<fieldset>
				<legend>Appointment Scheduling</legend>
				<?php
				echo $this->element('doctor_avp_appointments', array(
					'user_id' => isset($data['User']['id']) ? $data['User']['id'] : 0					
				));
				
				echo $this->Form->hidden('url_doctor_working_hours', array('id' => 'url_doctor_working_hours'));
				?>
				<p><?php echo $this->FormWrapper->get_button('Finish', array('type' => 'submit', 'escape' => true, 'class' => 'button green large fr')); ?></p>
			</fieldset>
            <p><?php echo $this->Form->hidden('date' , array('id' => 'slot_date')); ?></p>
            <p><?php echo $this->Form->hidden('slots_data_json' , array('id' => 'slots_data_json')); ?></p>

			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>

<?php echo $this->element('appointment_slot_selection'); ?>