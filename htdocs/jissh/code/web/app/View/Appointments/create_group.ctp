<!--========= Wizard  =========-->
<section class="grid_12">
	<!-- Begin box -->
	<div class="box">

		<!-- Begin box header -->
		<div class="header clearfix">
			<h2><?php echo $page_heading ?></h2>
		</div>
		<!-- End header -->

		<!-- Begin body -->
		<div class="body clearfix no_padding">
			<?php echo $this->Form->create('Appointment', array('id' => 'validate', 'class' => 'wizard padding20')); ?>
            <fieldset id="">
				<legend>Select Session</legend>
                    <div class="padding20 appointment_selection_mode">
                        <div id="group_appointments_0">
                            <?php
                            echo $this->element('group_appointments_listing', array(
                                'data' => $group_appointments
                            ));
                            ?>	
                        </div>				
                    </div>				
                    
			</fieldset>
			<fieldset id="">
				<legend>Patient Selection</legend>
				<p id="patient_selection_mode"><?php echo $this->FormWrapper->get_patient_selection_mode_radio(); ?></p>	
                <div id="pat_selection_list">		
                    <div class="padding20 patient_selection_mode">
                        <p><?php echo $this->FormWrapper->get_drop_down('waiting_list_title', $waiting_list_titles); ?></p>
                        <div id="patient_listing_0">
    
                            <?php
                            echo $this->element('appointment_patient_listing', array(
                                'data' => $patient_details
                            ));
                            ?>	
                        </div>				
                    </div>				
                    <div class="padding20 hide patient_selection_mode">
                        <p><?php
                            echo $this->FormWrapper->get_text_box('search_patient', array('class' => 'medium')) . ' ' .
                            $this->FormWrapper->get_button('Search', array('class' => 'button green', 'id' => 'search_patient'))
                            ?>
                        </p>
                        <div id="patient_listing_1">
    
                            <?php
                            echo $this->element('appointment_patient_listing', array(
                                'data' => $all_patients
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <p><?php echo $this->FormWrapper->get_button('Finish', array('type' => 'submit', 'escape' => true, 'class' => 'button green large fr')); ?></p>

			</fieldset>
            
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>
