<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'TemplateAttributesCategories',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            <div id="ajax_response"> 
            	<?php 
	$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
	echo $this->Form->create("TemplateAttributesCategory",array('action' => 'index','id' => 'validate'));
	echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
	//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
	?>
    <button type="submit" class="button green small float-left-margin" id="ajax_search_btn">Search</button>
	<div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
        <a href="#_" onclick="App.redirect({
				controller : 'TemplateAttributesCategories',
				action : 'index'
			})">X</a>
    </div>
	<?php
	echo $this->Form->end();
?> 

<table class="datagrid wf">
	<thead>
		<tr>
        	<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th ><?php echo __('Action'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($templateAttributesCategories)) {
			foreach ($templateAttributesCategories as $templateAttribute): ?>
			<tr>
            	<td align="center"><?php echo $templateAttribute['TemplateAttributesCategory']['name']; ?>&nbsp;</td>
				<td class="actions" align="center">								
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit',$templateAttribute['TemplateAttributesCategory']['id'])); ?> |
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $templateAttribute['TemplateAttributesCategory']['id']), null, __('Are you sure you want to delete # %s?', $templateAttribute['TemplateAttributesCategory']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; 
		}
		else{
			?>
			<tr>
				<td colspan="9">No template attribute category found</td>
			</tr>
			<?php		
		}
		?>
	</tbody>
</table>
<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            </div>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
