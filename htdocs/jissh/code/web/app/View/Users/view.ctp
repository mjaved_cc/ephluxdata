<div class="left_sidebar">
	<div class="small_box">
		<div class="header"> <img width="24" height="24" alt="History" src="<?php echo FULL_BASE_URL ?>/img/users_icon.png">Consumer</div>
		<div class="body">
			<div class="user_info">
				<img width="54" height="54" alt="Username" src="<?php echo FULL_BASE_URL ?>/img/user_placeholder.gif">
				<div class="name"> 
					<strong><?php echo __($user['User']['username']); ?></strong> <?php echo __($user['User']['username']); ?>
					<br/>
					<span><?php echo $this->Html->link(__('Edit', true), array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?></span>
				</div>				
				<hr />
				<p><strong>Email:</strong> <?php echo __($user['User']['username']); ?><br />
					<strong>Group:</strong> <?php echo __($user['Group']['name']); ?>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="main_column_right">
	<div class="box">
		<div class="header"> 
			<img width="28" height="29" alt="Accordion" src="<?php echo FULL_BASE_URL ?>/img/profile_setting_icon.png">Profile Settings
		</div>
		<div class="body_vertical_nav clearfix">
			<ul class="vertical_nav">				
				<li title="Social Accounts"><a href="#">Social Accounts</a></li>
				<li title="Social Accounts"><a href="#">New Nav</a></li>
			</ul>
			<div class="main_column">
				<div class="panes_vertical">
					<!-- Pane Social Account -->
					<div>
						<?php
						if (!empty($user['UserSocialAccount']) && isset($user['UserSocialAccount'])) {
							$social_accs = $user['UserSocialAccount'];
							?>
							<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
								<thead>
									<tr>
										<th><?php echo h('Logo'); ?></th>
										<th><?php echo h('Social Network'); ?></th>
										<th><?php echo h('Social Name'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($social_accs as $value): ?>
										<tr>							
											<td><img height="80" width="80" src="<?php echo h($value['image_url']); ?>" /></td>	
											<td><?php echo h($value['type_id']); ?></td> 
											<td><?php echo h($value['screen_name']); ?></td>					
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php
						} else {
							echo 'No social account is associated with current account.'; //MSG_RESULT_NOT_FOUND;
						}
						?>
					</div>
					<!-- Sample dummy  -->
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					<div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">
			<div class="actions">

				<ul>
					<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
					<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
					<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
				</ul>
			</div>
		</div>
	</div>

</div>


<div class="main_column">
	<div class="box">
		<div class="body">

			<h2><?php echo __('User'); ?></h2>

			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
				<thead>
					<tr>
						<th><?php echo __('Username'); ?></th>
						<th><?php echo __('Group'); ?></th>
						<th><?php echo __('Created'); ?></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td><?php echo h($user['User']['username']); ?></td>
						<td><?php echo h($user['Group']['name']); ?></td>
						<td><?php echo h($user['User']['created']); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>-->