<?php //pr($template);?>
<h1 class="border_bottom_1">Preview Template</h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Basic')?></h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'Templates',
				action : 'index'
			})"><?php echo __('Back to ' . $page_heading . ' Listing') ?></button>
		</div>
		<!-- End Header -->

		<div class="body">
        <form action="" onsubmit="return false;">
			<table class="wf">
            	<tr>
                	<td><label>Template Name</label></td>
                    <td><?php echo $template['Template']['template_name'];?></td>
				</tr>
				<tr>
                	<td><label for="TemplateDivision">Division</label></td>
                    <td><?php echo $template['Division']['name'];?></td>
				</tr>
                <?php
				foreach($template['TemplateAttributesItem'] as $item){
				?>
                <tr>
					<td><label><?php echo $item['TemplateAttribute']['title'];?></label></td>
                    <td>
						<?php 
						if($item['TemplateAttribute']['attr_type']=="textarea") {
						?>
                        <textarea class="large" rows="4"></textarea>
                        <?php	
						}
						else if($item['TemplateAttribute']['attr_type']=="texteditor"){
							?>
                            <textarea id="tiny_text" class="tiny_text"></textarea>
                            <?php
						}
						else{
						?>
                        <input type="<?php echo $item['TemplateAttribute']['attr_type'];?>" />
                        <?php	
						}
						?>
                    </td>
                </tr>
                <?php
				}
				?>
            </table>
			</form>
            <br clear="all" />
		</div>
	</div>
</section>

<section class="grid_12">
	<!-- begin box -->

		<div class="body">
        	<button type="button" onclick="App.redirect({
				controller : 'Templates',
				action : 'index'
			})" class="button green small">Back</button>
		</div>

</section>
