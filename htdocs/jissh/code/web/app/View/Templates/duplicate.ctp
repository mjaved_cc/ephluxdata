<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'Templates',
				action : 'index'
			})"><?php echo __('Back to ' . $page_heading.' listing') ?></button>
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('Template',array('id' => 'validate')); ?>
            <?php //echo $this->Form->input('id'); ?>
			<fieldset>
				<legend><?php echo __($page_heading)?></legend>
                <p><?php echo $this->Form->input('template_name',array('required'=>'required', 'type'=>'text', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('description',array('required'=>'required', 'type'=>'textarea', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('division_id',array('required'=>'required', 'options'=>$divisions, 'div' => false)); ?></p>
            </fieldset>
            
            <fieldset>
				<legend><?php echo __("Attributes");?></legend>
                <p>
				<select name="attributes" id="attributes">
                	<option value="">Select</option>
                    <?php
					foreach($attributes as $attribute) {
					?>
                    	<option value="<?php echo $attribute['TemplateAttribute']['id'];?>"><?php echo $attribute['TemplateAttribute']['title'];?></option>
                    <?php	
					}
					?>
                </select>
                <button type="button" class="button dark small" data-target="register" onclick="addAttr();"><?php echo __('Add') ?></button>
                </p>
                <p>
                <table class="datagrid wf" id="attributes_list">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Attribute Type</th>
                        <th>Order</th>
                        <th ><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
					$j = 0;
					foreach($this->request->data['TemplateAttributesItem'] as $val) {?>
                    <tr>
                    	<td align="center"><?php echo $val['TemplateAttribute']['title']; ?></td>
                        <td align="center"><?php echo $val['TemplateAttribute']['attr_type']; ?></td> 
                        <td align="center"><?php echo $val['TemplateAttributesItem']['order']; ?></td> 
                        <td align="center">
							<a href="#_" onclick="removeRowsa(this)">Remove</a>
                        </td>  
                        <input type="hidden" name="data[TemplateAttributesItem][<?php echo $j;?>][template_attribute_id]" value="<?php echo $val['TemplateAttributesItem']['template_attribute_id']; ?>" /> 
                        <input type="hidden" class="orderClass" name="data[TemplateAttributesItem][<?php echo $j;?>][order]" value="<?php echo $val['TemplateAttributesItem']['order']; ?>" />           	
                    </tr>
                    <?php 
						$j++;
					} ?>
                </tbody>
            </table>
            </p>
            </fieldset>
            
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>
<?php
echo $this->element('template_js');
?>
