<?php //pr($template);?>

<h1 class="border_bottom_1">Fill Template</h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Basic')?></h2>
            <button class="button dark" data-target="register" onclick="history.back()">Back</button>
		</div>
		<!-- End Header -->

		<div class="body">
		<?php echo $this->Form->create(null, array('id'=>'saveTemplateData', 'url' => array('controller' => 'Templates', 'action' => 'saveTemplateData'))); ?>
			<input type="hidden" name="template_id" id="template_id" value="<?php echo $template_id;?>" />
            <input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>" />
            <input type="hidden" name="employee_id" id="employee_id" value="<?php echo $employee_id;?>" />
            <input type="hidden" name="appointment_id" id="appointment_id" value="<?php echo $appointment_id;?>" />
            <table class="wf">
            	<tr>
                	<td><label>Template Name</label></td>
                    <td><?php echo $template['Template']['template_name'];?></td>
				</tr>
				<tr>
                	<td><label for="TemplateDivision">Division</label></td>
                    <td><?php echo $template['Division']['name'];?></td>
				</tr>
                <?php
				$i=0;
				foreach($template['TemplateAttributesItem'] as $item){
				?>
                <tr>
					<td><label><?php echo $item['TemplateAttribute']['title'];?></label></td>
                    <td>
                    	<input type="hidden" name="attributes[<?php echo $i;?>][attribute_type]" value="<?php echo $item['TemplateAttribute']['attr_type'];?>" />
                        <input type="hidden" name="attributes[<?php echo $i;?>][attribute_name]" value="<?php echo $item['TemplateAttribute']['title'];?>" />
						<?php 
						if($item['TemplateAttribute']['attr_type']=="textarea") {
						?>
                        	<textarea name="attributes[<?php echo $i;?>][attribute_value]"></textarea>
                        <?php	
						}
						else if($item['TemplateAttribute']['attr_type']=="texteditor"){
							?>
                            <textarea name="attributes[<?php echo $i;?>][attribute_value]" id="tiny_text" class="tiny_text"></textarea>
                            <?php
						}
						else{
						?>
                        <input name="attributes[<?php echo $i;?>][attribute_value]" type="<?php echo $item['TemplateAttribute']['attr_type'];?>" />
                        <?php	
						}
						?>
                    </td>
                </tr>
                <?php
					$i++;
				}
				?>
                <tr>
                	<td></td>
                	<td><button type="submit" onclick="" class="button green small">Save</button></td>
                </tr>
            </table>
			<?php echo $this->Form->end(); ?>
            <br clear="all" />
		</div>
	</div>
</section>
