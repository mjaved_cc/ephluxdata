<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'Templates',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            <div id="ajax_response"> 
            	<?php $pagination_params = $this->Paginator->params(); ?>
				<?php 
                    $q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
                    echo $this->Form->create("Template",array('action' => 'index','id' => 'validate'));
                    echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
                    //echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
                    ?>
                    <button type="submit" class="button green small float-left-margin" id="ajax_search_btn">Search</button>
                    <div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
                        <a href="#_" onclick="App.redirect({
                                controller : 'Templates',
                                action : 'index'
                            })">X</a>
                    </div>
                    <?php
                    echo $this->Form->end();
                ?> 
                
                <table class="datagrid wf">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('template_name'); ?></th>
                            <th><?php echo $this->Paginator->sort('division_id'); ?></th>
                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                            <th ><?php echo __('Action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($templates)) {
                            foreach ($templates as $template): ?>
                            <tr>
                                <td align="center"><?php echo $template['Template']['template_name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $template['Division']['name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $template['Template']['created']; ?>&nbsp;</td>
                                <td class="actions" align="center">		
                                	<?php echo $this->Html->link(__('Preview'), array('action' => 'view',$template['Template']['id'])); ?> | 						
                                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit',$template['Template']['id'])); ?> |
                                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $template['Template']['id']), null, __('Are you sure you want to delete # %s?', $template['Template']['id'])); ?> |
                                    <?php echo $this->Html->link(__('Duplicate'), array('action' => 'duplicate',$template['Template']['id'])); ?>
                                </td>
                            </tr>
                        <?php endforeach; 
                        }
                        else{
                            ?>
                            <tr>
                                <td colspan="9"><?php echo NO_TEMPLATE_ATTRIBUTE_MESSAGE;?></td>
                            </tr>
                            <?php		
                        }
                        ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            </div>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
