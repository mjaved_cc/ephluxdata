<div class="allowedServices view">
<h2><?php  echo __('Allowed Service'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($allowedService['AllowedService']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($allowedService['AllowedService']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Allowed Service Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($allowedService['AllowedServiceGroup']['name'], array('controller' => 'allowed_service_groups', 'action' => 'view', $allowedService['AllowedServiceGroup']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($allowedService['AllowedService']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($allowedService['AllowedService']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($allowedService['AllowedService']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Allowed Service'), array('action' => 'edit', $allowedService['AllowedService']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Allowed Service'), array('action' => 'delete', $allowedService['AllowedService']['id']), null, __('Are you sure you want to delete # %s?', $allowedService['AllowedService']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Allowed Services'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Allowed Service'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Allowed Service Groups'), array('controller' => 'allowed_service_groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Allowed Service Group'), array('controller' => 'allowed_service_groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Services'), array('controller' => 'employee_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Service'), array('controller' => 'employee_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Employee Services'); ?></h3>
	<?php if (!empty($allowedService['EmployeeService'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Employee Detail Id'); ?></th>
		<th><?php echo __('Allowed Service Id'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($allowedService['EmployeeService'] as $employeeService): ?>
		<tr>
			<td><?php echo $employeeService['id']; ?></td>
			<td><?php echo $employeeService['employee_detail_id']; ?></td>
			<td><?php echo $employeeService['allowed_service_id']; ?></td>
			<td><?php echo $employeeService['is_active']; ?></td>
			<td><?php echo $employeeService['created']; ?></td>
			<td><?php echo $employeeService['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'employee_services', 'action' => 'view', $employeeService['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'employee_services', 'action' => 'edit', $employeeService['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'employee_services', 'action' => 'delete', $employeeService['id']), null, __('Are you sure you want to delete # %s?', $employeeService['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Employee Service'), array('controller' => 'employee_services', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
