<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>

<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'AllowedServices',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
        	<?php 
				echo $this->Form->create("AllowedService",array('action' => 'index','id' => 'validate'));
				echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin'));
				echo $this->Form->button('Search', array('type' => 'submit', 'escape' => true, 'class' => 'button green small float-left-margin'));
				echo $this->Form->end();
			?> 
            <table class="datagrid wf expandable">
				<thead>
					<tr>
						<th colspan="2" class="sorting"><?php echo $this->Paginator->sort('name'); ?></th>
                        <th class="sorting"><?php echo $this->Paginator->sort('AllowedServiceGroup'); ?></th>
						<th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
						<th ><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($allowedServices as $allowedService): ?>
						<tr>
							<td  class="toggle"><span class="ir"> Expand/ Collapse</span></td>
							<td ><?php echo h($allowedService['AllowedService']['name']); ?>&nbsp;</td>
                            <td align="center"><?php echo h($allowedService['AllowedServiceGroup']['name']); ?>&nbsp;</td>
							<td align="center"><?php echo h($allowedService['AllowedService']['created']); ?>&nbsp;</td>
							<td class="actions" align="center">
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $allowedService['AllowedService']['id'])); ?> |
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $allowedService['AllowedService']['id']), null, __('Are you sure you want to delete # %s?', $allowedService['AllowedService']['id'])); ?>
							</td>
						</tr>
						<tr>
                          <td class="grid_dropdown"> </td>
                          <td colspan="4"><?php echo h($allowedService['AllowedService']['description']); ?></td>
                        </tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
<?php //echo $this->element('sql_dump'); ?>
