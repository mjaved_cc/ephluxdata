<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<div class="clear"></div>
<h3 class="border_bottom_3"><?php echo h($sponsor['Sponsor']['name']); ?></h3>

<section class="grid_8">
  <div class="box">
    <div class="body">
      <pre><?php echo __('Created on '); ?><?php echo h($sponsor['Sponsor']['created']); ?></pre>
      <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
</section>

<section class="grid_4">
	<div class="box">
    <div class="header clearfix">
        <h2>Actions</h2>
    </div>
    <div class="body">
    	<ul>
            <li><?php echo $this->Html->link(__('Edit Sponsor'), array('action' => 'edit', $sponsor['Sponsor']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Delete Sponsor'), array('action' => 'delete', $sponsor['Sponsor']['id']), null, __('Are you sure you want to delete # %s?', $sponsor['Sponsor']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('List Sponsors'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Sponsor'), array('action' => 'add')); ?> </li>
        </ul>

    </div>
  </div>
</section>
