<div class="annualVacationPlans view">
<h2><?php  echo __('Annual Vacation Plan'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee Detail'); ?></dt>
		<dd>
			<?php echo $this->Html->link($annualVacationPlan['EmployeeDetail']['id'], array('controller' => 'employee_details', 'action' => 'view', $annualVacationPlan['EmployeeDetail']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Avp Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($annualVacationPlan['AvpType']['name'], array('controller' => 'avp_types', 'action' => 'view', $annualVacationPlan['AvpType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Work Day'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['last_work_day']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Report To Work'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['report_to_work']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($annualVacationPlan['AnnualVacationPlan']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Annual Vacation Plan'), array('action' => 'edit', $annualVacationPlan['AnnualVacationPlan']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Annual Vacation Plan'), array('action' => 'delete', $annualVacationPlan['AnnualVacationPlan']['id']), null, __('Are you sure you want to delete # %s?', $annualVacationPlan['AnnualVacationPlan']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Annual Vacation Plans'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Annual Vacation Plan'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Avp Types'), array('controller' => 'avp_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Avp Type'), array('controller' => 'avp_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
