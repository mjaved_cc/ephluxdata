<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="document.location.href='<?php echo $this->Html->url(array("controller" => "annual_vacation_plans",
				"action" => "add",
				$employee_detail_id
			));?>
            '"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            
            <table class="datagrid wf">
				<thead>
					<tr>
						<th><?php echo $this->Paginator->sort('avp_type_id'); ?></th>
                        <th><?php echo $this->Paginator->sort('description'); ?></th>
                        <th><?php echo $this->Paginator->sort('last_work_day'); ?></th>
                        <th><?php echo $this->Paginator->sort('report_to_work'); ?></th>
						<th ><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($annualVacationPlans as $annualVacationPlan): ?>
						<tr>                        	
							<td align="center" ><?php echo h($annualVacationPlan['AvpType']['name']); ?></td>
							<td align="center" ><?php echo h($annualVacationPlan['AnnualVacationPlan']['description']); ?></td>
                            <td align="center"><?php echo h($annualVacationPlan['AnnualVacationPlan']['last_work_day_formated']); ?>&nbsp;</td>
                            <td align="center"><?php echo h($annualVacationPlan['AnnualVacationPlan']['report_to_work_formated']); ?>&nbsp;</td>
							<td class="actions" align="center">								
								
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $annualVacationPlan['AnnualVacationPlan']['id'],$employee_detail_id), null, __('Are you sure you want to delete # %s?', $annualVacationPlan['AnnualVacationPlan']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
