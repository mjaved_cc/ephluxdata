<div class="annualVacationPlans form">
<?php echo $this->Form->create('AnnualVacationPlan'); ?>
	<fieldset>
		<legend><?php echo __('Edit Annual Vacation Plan'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('employee_detail_id');
		echo $this->Form->input('avp_type_id');
		echo $this->Form->input('description');
		echo $this->Form->input('last_work_day');
		echo $this->Form->input('report_to_work');
		echo $this->Form->input('status');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AnnualVacationPlan.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AnnualVacationPlan.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Annual Vacation Plans'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Avp Types'), array('controller' => 'avp_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Avp Type'), array('controller' => 'avp_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
