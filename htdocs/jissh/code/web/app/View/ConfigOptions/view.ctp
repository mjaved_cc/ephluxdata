<div class="configOptions view">
<h2><?php  echo __('Config Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($configOption['ConfigOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Config Option'), array('action' => 'edit', $configOption['ConfigOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Config Option'), array('action' => 'delete', $configOption['ConfigOption']['id']), null, __('Are you sure you want to delete # %s?', $configOption['ConfigOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Config Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Config Option'), array('action' => 'add')); ?> </li>
	</ul>
</div>
