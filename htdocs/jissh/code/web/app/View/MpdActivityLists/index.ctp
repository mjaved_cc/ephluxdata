<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'MpdActivityLists',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            <table class="display dataTable">
				<thead>
					<tr>
						<th class="sorting"><?php echo $this->Paginator->sort('name'); ?></th>
						<th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
						<th ><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($mpd_activity_lists as $mpd_activity_list): ?>
						<tr>
							<td align="left"><?php echo h($mpd_activity_list['MpdActivityList']['name']); ?>&nbsp;</td>
							<td align="center"><?php echo h($mpd_activity_list['MpdActivityList']['created']); ?>&nbsp;</td>
							<td class="actions" align="center">								
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $mpd_activity_list['MpdActivityList']['id'])); ?> |
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mpd_activity_list['MpdActivityList']['id']), null, __('Are you sure you want to delete # %s?', $mpd_activity_list['MpdActivityList']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
