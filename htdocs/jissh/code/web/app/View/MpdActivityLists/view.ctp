<div class="mpd_activity_lists view">
<h2><?php  echo __('MpdActivityList'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mpd_activity_list['MpdActivityList']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($mpd_activity_list['MpdActivityList']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($mpd_activity_list['MpdActivityList']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mpd_activity_list['MpdActivityList']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($mpd_activity_list['MpdActivityList']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit MpdActivityList'), array('action' => 'edit', $mpd_activity_list['MpdActivityList']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete MpdActivityList'), array('action' => 'delete', $mpd_activity_list['MpdActivityList']['id']), null, __('Are you sure you want to delete # %s?', $mpd_activity_list['MpdActivityList']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List MpdActivityLists'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New MpdActivityList'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>