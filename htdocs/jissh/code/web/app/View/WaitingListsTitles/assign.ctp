<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __("Add to Waiting List"); ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark attach_modal" type="button" data-target="register" onclick="WaitingListsTitle.openModel();" data-modal="#add_waiting_list_model"><?php echo __('Add New'); ?></button>
		</div>
		<!-- End Header -->
		<div class="body">
            <div id="ajax_response"> 
            	<?php echo $this->element('assign_waiting_listing');?>
            </div>
			<?php 
				echo $this->Form->create(null, array('url' => array('controller' => 'WaitingListsTitles', 'action' => 'addToList')));
				//echo $this->Form->create("PatientWaitingList",array('controller'=>'WaitingListsTitles', 'action' => 'addToList'));
				echo $this->Form->input("patient_detail_id", array('type'=>'hidden','div' => false,'value'=>$this->request->params['pass'][0]));
				echo $this->Form->input("waiting_list_title_id", array('type'=>'hidden','div' => false));
				//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
				echo $this->Form->end();
			?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>

<section id="add_waiting_list_model" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Add Waiting List</h2> 
		</div>
		<div class="body">
        <?php echo $this->Form->create(null, array('id'=>'waiting_list_model_form', 'url' => array('controller' => 'WaitingListsTitles', 'action' => 'AddAndAssign'))); ?>
            <p><?php echo $this->Form->input('title', array('required' => 'required', 'div' => false)); ?></p>
            <div class="validator" style="visibility: hidden; position:relative;">
                <span class="arrow"></span><p>Please complete this mandatory field.</p>
            </div>
            <p><?php echo $this->Form->input('division_id', array('default'=>'0', 'div' => false)); ?></p>
            <p><?php echo $this->Form->input('description', array('type' => 'textarea','div' => false)); ?></p>
            <?php echo $this->Form->input("patient_detail_id", array('type'=>'hidden','div' => false,'value'=>$this->request->params['pass'][0]));?>
		<?php echo $this->Form->end(); ?>
         
		<hr />
		<button type="button" class="close button blue small"> Close </button> 
        <button id="sBtn_add_religion" type="button" class="button green small" onclick="WaitingListsTitle.validateWaitingList();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
		<span id="status_add_waiting_list">Processing...</span>
        </div>
	</div> 
</section>
