<?php $pagination_params = $this->Paginator->params(); ?>
<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'LicenseTypes',
				action : 'add'
			})"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            <?php 
					echo $this->Form->create("LicenseType",array('action' => 'index','id' => 'validate'));
					echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin'));
					echo $this->Form->button('Search', array('type' => 'submit', 'escape' => true, 'class' => 'button green small float-left-margin'));
					echo $this->Form->end();
				?> 
            <table class="datagrid wf">
				<thead>
					<tr>
						<th class="sorting"><?php echo $this->Paginator->sort('name'); ?></th>
						<th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
						<th ><?php echo __('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($license_types as $license_type): ?>
						<tr>
							<td align="center"><?php echo h($license_type['LicenseType']['name']); ?>&nbsp;</td>
							<td align="center"><?php echo h($license_type['LicenseType']['created']); ?>&nbsp;</td>
							<td class="actions" align="center">								
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $license_type['LicenseType']['id'])); ?> |
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $license_type['LicenseType']['id']), null, __('Are you sure you want to delete # %s?', $license_type['LicenseType']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
            </table>
			<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
