<div class="license_types view">
<h2><?php  echo __('LicenseType'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($license_type['LicenseType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($license_type['LicenseType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($license_type['LicenseType']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($license_type['LicenseType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($license_type['LicenseType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit LicenseType'), array('action' => 'edit', $license_type['LicenseType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete LicenseType'), array('action' => 'delete', $license_type['LicenseType']['id']), null, __('Are you sure you want to delete # %s?', $license_type['LicenseType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List LicenseTypes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New LicenseType'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>