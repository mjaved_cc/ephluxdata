<span class="fr">
	<button class="button light small" onclick="App.redirect('<?php echo $employee_listing_relative_url; ?>')">Back</button>
    <?php 
	if($doctor_edit_relative_url) {
	?>
	<button class="button light small" onclick="App.redirect('<?php echo $doctor_edit_relative_url ?>')">Edit</button>
    <?php
	}
	?>
	<button class="button light small" onclick="App.redirect('<?php echo $mpd_activities_relative_url ?>')">Mandatory Activities</button>
	<button class="button light small" onclick="App.redirect('<?php echo $vpd_activities_relative_url ?>')">Voluntary Activities</button>
    <?php 
	if(isset($create_appointment_url)){
	?>
	<button class="button light small" onclick="App.redirect('<?php echo $create_appointment_url; ?>')">Create Appointment</button>
	<?php
	}
	?>
    <?php 
	if(isset($avp_relative_url)){
	?>
	<button class="button light small" onclick="App.redirect('<?php echo $avp_relative_url; ?>')">Vacation Plan</button>
	<?php
	}
	?>
    
	<button class="button light small attach_modal" data-modal="#cancel_appoinments_model">Cancel Appointments</button>
	
</span>
<section id="cancel_appoinments_model" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Cancel Appointments</h2> 
		</div>
		<div class="body">
			<?php echo $this->Form->create('Appointment',array('action'=>'cancel_appointments','id' => 'cancel_appointments_form')); ?>
				<table width="100%">
					<tr>
						<td>From Date:</td> 
						<td>
						<input type="text" class="small datetime vAlign" readonly="readonly" name="from_date" id="from_date" />
                        <div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div>
						</td>
					</tr>
                    <tr>
						<td>To Date:</td> 
						<td>
						<input type="text" class="small datetime vAlign" readonly="readonly" name="to_date" id="to_date" />
                        <div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div>
						</td>
					</tr>	
					<tr>
						<td>Message to patients:</td> 
						<td>
						<textarea name="template" id="template"></textarea>
                        <div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<hr />
							<button type="button" class="close button blue small"> Close </button> 
							<button id="sBtn_add_country" type="button" class="button green small" onclick="validateAndSaveAppointments();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
							<!--<span id="status_add_call_log">Processing...</span>-->
						</td>
					</tr>
				</table>
				<input type="hidden" name="employee_detail_id" value="<?php echo $data['EmployeeDetail']['user_id'];?>" />
			<?php echo $this->Form->end(); ?>
        </div>
	</div> 
</section>  