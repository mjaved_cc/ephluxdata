<?php 
$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
$employee_id = isset($this->request->params['named']['employee_id']) ? $this->request->params['named']['employee_id'] : "";
$patient_id = isset($this->request->params['named']['patient_id']) ? $this->request->params['named']['patient_id'] : "";
$appointment_id = isset($this->request->params['named']['appointment_id']) ? $this->request->params['named']['appointment_id'] : "";
echo $this->Form->create("Template",array('action' => 'medicationSearch','id' => 'validate', 'onsubmit'=>'return onSbumit();'));
echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
echo $this->Form->input("employee_id", array('type'=>'hidden','value'=>$employee_id));
echo $this->Form->input("patient_id", array('type'=>'hidden','value'=>$patient_id));
echo $this->Form->input("appointment_id", array('type'=>'hidden','value'=>$appointment_id));
//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
?>
<button type="button" class="button green small float-left-margin" id="ajax_search_btn">Search</button>
<div class="clear"></div>
<div id="cancel_search" class="canel_serch_medication" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
	<a href="<?php echo $this->Html->url(array(
		"controller" => "Templates",
		"action" => "medication",
		$patient_id,
		$employee_id, 
		$appointment_id
	));?>">X</a>
</div>
<?php
echo $this->Form->end();
?>
<?php echo $this->Form->create(null, array('id'=>'templateAssign', 'url' => array('controller' => 'Templates', 'action' => 'selectTemplate'))); ?>
<input type="hidden" name="template_id" id="template_id" />
<input type="hidden" name="patient_id" id="patient_id" />
<input type="hidden" name="employee_id" id="employee_id" />
<input type="hidden" name="appointment_id" id="appointment_id" />
<table class="datagrid wf">
	 <thead>
		<tr>
			<th>Template Name</th>
			<th>Division</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($templates)) {
			foreach($templates as $item){
			?>
			<tr>
				<td align="center"><?php echo $item['Template']['template_name'];?></td>
				<td align="center"><?php echo $item['Division']['name'];?></td>
				<td align="center"><button class="button green small" type="button" onclick="submitMedication(<?php echo $item['Template']['id'];?>, <?php echo $patient_id;?>, <?php echo $employee_id;?>, <?php echo $appointment_id;?>);">Select</button></td>
			</tr>
			<?php
			}
		}
		else{
			?>
			<tr>
				<td colspan="3">No Templates found.</td>
			</tr>
			<?php	
		}
		?>
	<tbody>
</table>
<?php echo $this->Form->end(); ?>