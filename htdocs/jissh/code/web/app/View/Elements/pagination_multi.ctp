<?php
$prev_url = 'javascript:void(0)';
$next_url = 'javascript:void(0)';
$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
$url = array();

if($q!="") {
	$url['q'] = $q;	
}

/*if ($this->Paginator->hasPrev()) {
	$url['page'] = ($pagination_params['page'] - 1);
	$prev_url = $this->Paginator->url($url, false);
}
if ($this->Paginator->hasNext()) {
	$url['page'] = ($pagination_params['page'] + 1);
	$next_url = $this->Paginator->url($url, false);	
}*/

// means we have multiple pages
//if ($prev_url != $next_url) {
	?>
	<!--Pagination-->
    <ul class="pagination clearfix">
		<li><?php echo $this->Paginator->prev('<< '.__('previous', true), $pagination_params, null, array('class'=>'disabled'));?> </li>
		<?php
		echo $this->Paginator->numbers(array_merge(array(
			'separator' => '',
			'tag' => 'li',
			'currentClass' => 'active'
		),$pagination_params));
		?>				
		<li><?php echo $this->Paginator->next(__('next', true).' >>',$pagination_params, null, array('class' => 'disabled'));?></li>
	</ul>
<?php //} ?>