<?php $pagination_params = $this->Paginator->params(); ?>
<?php 
	$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
	echo $this->Form->create("MpdActivity",array('action' => 'index','id' => 'validate'));
	echo $this->Form->input('emp_id',array('type'=>'hidden','value'=>$emp_id));
	echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med  float-left-margin','value'=>$q));
	//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
	?>
    <button type="button" class="button green small" id="ajax_search_btn" onclick="MpdActivity.search(this);">Search</button>
	<div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
        <a href="#_" onclick="MpdActivity.clear();MpdActivity.search(this);">X</a>
    </div>
	<?php
	echo $this->Form->end();
?> 

<table class="datagrid wf">
	<thead>
		<tr>
			<th class="sorting"><?php echo $this->Paginator->sort('program_id'); ?></th>
			<th class="sorting"><?php echo $this->Paginator->sort('mpd_activity_list_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modality_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('date_complition'); ?></th>
			<th ><?php echo __('Action'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($mpdActivities)) {
			foreach ($mpdActivities as $mpdActivity): ?>
			<tr>
				<td align="center"><?php echo $mpdActivity['Program']['name']; ?>&nbsp;</td>
				<td align="center"><?php echo $mpdActivity['MpdActivityList']['name']; ?>&nbsp;</td>
				<td align="center"><?php echo $mpdActivity['Modality']['name']; ?>&nbsp;</td>
				<td align="center"><?php echo $mpdActivity['MpdActivity']['status']; ?>&nbsp;</td>
				<td align="center"><?php echo $mpdActivity['MpdActivity']['date_complition']; ?>&nbsp;</td>
				<td class="actions" align="center">								
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $emp_id,$mpdActivity['MpdActivity']['id'])); ?> |
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $emp_id,$mpdActivity['MpdActivity']['id']), null, __('Are you sure you want to delete # %s?', $mpdActivity['MpdActivity']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; 
		}
		else{
			?>
			<tr>
				<td colspan="6"><?php echo NO_MPD_ACTIVITY_MESSAGE?></td>
			</tr>
			<?php		
		}
		?>
	</tbody>
</table>
<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>