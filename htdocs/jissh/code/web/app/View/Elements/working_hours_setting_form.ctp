<!--============================Modal============================-->
<section id="update_event" class="grid_6 event" style="display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Event</h2> 
		</div>
		<div class="body">
			<form action="" id="update_event_popup">
				<fieldset>
					<legend>Edit</legend>
					<p>					
						<label for="ue_start">Start</label>
						<input type="text" name="ue_start" id="ue_start" class="datetime" />
					</p>
					<p>
						<label for="ue_end">End</label>
						<input type="text" name="ue_end" id="ue_end" class="datetime" />
					</p>
					<p>
						<label for="title">Title</label>						
						<span id="ue_title"></span>
					</p>
					<p class="ce_divisions">						
					</p>
					<p>
						<input type="hidden" id="ue_id" />
					</p>
					<p>
						<button class="close button green" onclick="return Event.before_update_event()">Submit</button> <!-- Close class on any element inside the modal box will close the modal. -->
					</p>
				</fieldset>
			</form> 			

		</div>
	</div> 
</section>

<!--============================Modal============================-->
<section id="create_event" class="grid_6 event" style="display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Event</h2> 
		</div>
		<div class="body">
			<form action="" id="create_event_popup">
				<fieldset>
					<legend>Add</legend>
					<p class="ce_divisions">Please select division ...</p>
					<p>						
						<input type="hidden" id="ce_start" />
						<input type="hidden" id="ce_end" />
					</p>
					<p>
						<button class="close button green" onclick="return Event.before_create_event()">Submit</button> <!-- Close class on any element inside the modal box will close the modal. -->
					</p>
				</fieldset>
			</form> 			

		</div>
	</div> 
</section>