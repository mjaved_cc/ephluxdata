<!--============================ CALENDAR ============================-->
<div class="clearfix">	
	<div class="grid_12">
		<div style="padding-bottom:15px; padding-left:10px;">
			<span style="padding-right:15px; position:relative; top:3px;">
				<img style="position: relative; top: 3px;" src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . IMAGE_INDICATOR_APPOINTMENT)?>">
				<b>Appointment</b>
			</span>
			<span style="padding-right:15px; position:relative; top:3px;">
				<img style="position: relative; top: 3px;" src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . IMAGE_INDICATOR_LEAVES)?>">
                <b>Leaves</b>
			</span>
            <?php 
			if(isset($employee_id)) {
			?>
            <a href="<?php 
			echo $this->Html->url(array("controller" => "appointments","action" => "search",$employee_id));
			?>" class="button dark small"><?php echo __('Search') ?></a>
            <?php
			}
			?>
		</div>
		<div id='loading' style='display:none'>loading...</div>
		<!-- Begin Calendar -->
		<div id='cal_avp_appointments'></div>
		<!-- End Calendar -->		
		<p><?php echo $this->Form->hidden('url_doctor_avp_appointments', array('id' => 'url_doctor_avp_appointments', 'value' => $this->Misc->get_fully_qualified_url('employee_details/get_doctor_avp_and_appointments/' . $user_id . EXTENSION_JSON))); ?>
		</p>

	</div>
</div>