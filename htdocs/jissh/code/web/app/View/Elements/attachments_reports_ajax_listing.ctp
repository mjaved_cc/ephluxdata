<div class="body">
	<?php echo $this->element('add_attachment_report_model',array('appointment_id'=>$id,'patient_id'=>$Appointment['PatientDetail']['id']));?>
    <table class="datagrid wf">
        <thead>
            <tr>
                <th class="sorting">File</th>
                <th class="sorting">Created Date</th>
                <th class="sorting">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
			//pr($Appointment['PatientAttachmentReport']);
            if(!empty($Appointment['PatientAttachmentReport'])) {
                foreach ($Appointment['PatientAttachmentReport'] as $PatientAttachmentReport): 
				?>
                <tr>
                    <td align="center"><?php echo $PatientAttachmentReport['attachment_title'];?></td>
                    <td align="center"><?php echo $PatientAttachmentReport['created'];?></td>
                    <td align="center"><?php echo $this->Html->link(__("Download"), array('controller'=>'appointments','action' => 'download',$PatientAttachmentReport['id'])); ?></td>
                </tr>
                
            <?php endforeach; 
            }
            else {
                ?>
                <tr>
                    <td colspan="2">No attachments found</td>
                </tr>
                <?php	
            }
            ?>
        </tbody>
    </table>
    <?php 
	$pagination_params = $this->Paginator->params();
	$pagination_params['model'] = 'PatientAttachmentReport';
	$pagination_params['url']['model'] = 'PatientAttachmentReport';
	//$paginator->__defaultModel = 'PatientAttachmentReport';
	
	?>
    
    <?php
	echo $this->element('pagination_multi',array('pagination_params' => $pagination_params)); ?>
    <!--End Pagination-->
</div>