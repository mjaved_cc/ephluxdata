<?php if (!empty($data)) { ?> 
	<table class="datagrid wf">					
		<tbody>
			<?php 
			$j=0;
			foreach ($data as $patient): 
					if($j==0){
						$sel='checked="true"';
					}
					else{
						$sel='';
					}

			?>
				<tr>
					<td align="center">
						<input name="data[PatientDetail][id]" <?php echo $sel;?> value="<?php echo $patient['id'] ?>" type="radio" />
					</td>
					<td align="center"><?php echo h($patient['name']); ?></td>								
					<td align="center"><?php echo $this->FormWrapper->get_anchor('View', array('controller' => 'patient_details', 'action' => 'view', $patient['id'])); ?></td>								
				</tr>

			<?php $j++; 
			endforeach; ?>
		</tbody>
	</table>
	<?php
} else {

	$class = 'error';
	$image_name = 'error_icon.png';

	echo $this->element('notification', array(
		"message" => 'No match found',
		"class" => $class,
		"image_name" => $image_name
	));
}
?>

