<?php $pagination_params = $this->Paginator->params(); ?>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Call Logs')?></h2>
            <button class="button dark attach_modal" data-modal="#add_call_log" onclick="Appointment.openModel();"><?php echo __('Log new call'); ?></button>
		</div>
		<!-- End Header -->

		<div class="body">
        <?php echo $this->element('add_call_log_model',array('EmployeeDetail'=>$Appointment['EmployeeDetail'],'id'=>$id));?>
			<table class="wf datagrid">
				<tr>
                	<th align="left">Caller</th>
                    <th align="left">Receiver</th>
                    <th align="left">Call Subject</th>
                    <th align="left">Call Date Time</th>
				</tr>
                <?php
				foreach($Appointment['CallLog'] as $calllog){
				?>
                <tr>
                	<td align="left"><?php echo $calllog['EmployeeDetail']['display_name'];?></td>
                    <td align="left"><?php echo $calllog['CallLog']['call_answered_by'];?></td>
                    <td align="left"><?php echo $calllog['CallLog']['call_subject'];?></td>
                    <td align="left"><?php echo $calllog['CallLog']['calling_date_time'];?></td>
                </tr>
                <?php
				}
				?>
            </table>
            <?php 
			$pagination_params = $this->Paginator->params();
			$pagination_params['model'] = 'CallLog';
			$pagination_params['url']['model'] = 'CallLog';
			//$paginator->__defaultModel = 'CallLog';
			
			?>
            <?php echo $this->element('pagination_multi',array('pagination_params' => $pagination_params)); ?>
		</div>
	</div>
</section>