<?php $pagination_params = $this->Paginator->params(); ?>
<?php 
	$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
	echo $this->Form->create("TemplateAttribute",array('action' => 'index','id' => 'validate'));
	echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
	//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
	?>
    <button type="submit" class="button green small float-left-margin" id="ajax_search_btn">Search</button>
	<div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
        <a href="#_" onclick="App.redirect({
				controller : 'TemplateAttributes',
				action : 'index'
			})">X</a>
    </div>
	<?php
	echo $this->Form->end();
?> 

<table class="datagrid wf">
	<thead>
		<tr>
        	<th><?php echo $this->Paginator->sort('title'); ?></th>
            <th><?php echo $this->Paginator->sort('attr_type'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th ><?php echo __('Action'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($templateAttributes)) {
			foreach ($templateAttributes as $templateAttribute): ?>
			<tr>
            	<td align="center"><?php echo $templateAttribute['TemplateAttribute']['title']; ?>&nbsp;</td>
                <td align="center"><?php echo $templateAttribute['TemplateAttribute']['attr_type']; ?>&nbsp;</td>
                <td align="center"><?php echo $templateAttribute['TemplateAttribute']['created']; ?>&nbsp;</td>
				<td class="actions" align="center">								
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit',$templateAttribute['TemplateAttribute']['id'])); ?> |
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $templateAttribute['TemplateAttribute']['id']), null, __('Are you sure you want to delete # %s?', $templateAttribute['TemplateAttribute']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; 
		}
		else{
			?>
			<tr>
				<td colspan="9"><?php echo NO_TEMPLATE_ATTRIBUTE_MESSAGE;?></td>
			</tr>
			<?php		
		}
		?>
	</tbody>
</table>
<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>