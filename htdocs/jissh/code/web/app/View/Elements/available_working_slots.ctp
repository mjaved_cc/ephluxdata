<?php if ($response['header']['code'] == ECODE_SUCCESS) { ?> 
	<table class="datagrid wf">					
		<tbody>
			<?php foreach ($response['body'] as $division => $slots): ?>
				<tr>
					<td colspan="2"  align="center"><h3><?php echo $division ?></h3></td>
				</tr>
				<?php foreach ($slots as $slot) : ?>
					<tr>
						<td align="center">
							<input name="data[EmployeeWorkingHourBreakup][id]" value="<?php echo $slot['EmployeeWorkingHourBreakup']['id'] ?>" type="radio"/>
						</td>
						<td align="center">
							<?php
							echo h($slot['EmployeeWorkingHourBreakup']['time_span']);
							echo $this->Form->hidden('EmployeeWorkingHourBreakup.start_time', array('value' => $slot['EmployeeWorkingHourBreakup']['start_time'], 'id' => 'start_time' . $slot['EmployeeWorkingHourBreakup']['id']));
							echo $this->Form->hidden('EmployeeWorkingHourBreakup.end_time', array('value' => $slot['EmployeeWorkingHourBreakup']['end_time'], 'id' => 'end_time' . $slot['EmployeeWorkingHourBreakup']['id']));
							echo $this->Form->hidden('Division.id', array('value' => $slot['Division']['id'], 'id' => 'division_id' . $slot['EmployeeWorkingHourBreakup']['id']));
							?>
						</td>																				
					</tr>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</tbody>
	</table>	
	<?php
} else {

	$class = 'error';
	$image_name = 'error_icon.png';

	echo $this->element('notification', array(
		"message" => $response['header']['message'],
		"class" => $class,
		"image_name" => $image_name
	));
}
?>