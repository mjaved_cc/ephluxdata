<section id="add_attachment_report_model" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Add Attachment</h2> 
		</div>
		<div class="body">
			<?php echo $this->Form->create('Appointment',array('action'=>'save_attachment_report','id' => 'attachment_report_form','type' => 'file')); ?>
				<table width="100%">
					<tr>
						<td>Attachment Title:</td> 
						<td>
						<input type="text" class="small" name="attachment_title" id="attachment_title" />
                        <div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div>
						</td>
					</tr>	
					<tr>
						<td>Attachment File:</td> 
						<td><input type="file" class="small" name="attachment_file" id="attachment_file" />
						<div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div></td>
					</tr>
					<tr>
						<td colspan="2">
							<hr />
							<button type="button" class="close button blue small"> Close </button> 
							<button id="sBtn_add_country" type="button" class="button green small" onclick="validateAndSaveAttachment();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
							<!--<span id="status_add_call_log">Processing...</span>-->
						</td>
					</tr>
				</table>	
				<input type="hidden" name="appointment_id" value="<?php echo $appointment_id;?>" />
                <input type="hidden" name="patient_id" value="<?php echo $patient_id;?>" />
			<?php echo $this->Form->end(); ?>
        </div>
	</div> 
</section>