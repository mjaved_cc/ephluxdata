<div class="body">
    <table class="datagrid wf">
        <thead>
            <tr>
                <th class="sorting">Appointment Date</th>
                <th class="sorting">Medication Date</th>
                <th class="sorting">Doctor</th>
                <th class="sorting">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(!empty($patientMedications)) {
                foreach ($patientMedications as $patientMedication): ?>
                <tr>
                    <td align="center">
                        <?php echo $patientMedication['Appointment']['created'];?>
                    </td>
                    <td align="center"><?php echo h($patientMedication['PatientMedication']['created']); ?>&nbsp;</td>
                    <td align="center"><?php echo h($patientMedication['EmployeeDetail']['display_name']); ?>&nbsp;</td>
                    <td align="center"><?php echo $this->Html->link(__("Details"), array('controller'=>'patient_details','action' => 'medication_details', $patientMedication['PatientMedication']['id'])); ?></td>
                </tr>
                
            <?php endforeach; 
            }
            else {
                ?>
                <tr>
                    <td colspan="4">No medication history found</td>
                </tr>
                <?php	
            }
            ?>
        </tbody>
    </table>
    <?php 
	$pagination_params = $this->Paginator->params();
	$pagination_params['model'] = 'PatientMedication';
	$pagination_params['url']['model'] = 'PatientMedication';
	//$paginator->__defaultModel = 'PatientMedication';
	
	?>
    
    <?php
	echo $this->element('pagination_multi',array('pagination_params' => $pagination_params)); ?>
    <!--End Pagination-->
</div>