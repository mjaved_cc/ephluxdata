<footer id="footer">
	<p class="tac">
		© Copyright <?php echo date('Y') . ' ' . SITE_NAME ?>. All Rights Reserved. Powered by <a href="#"><?php echo SITE_NAME ?></a>
	</p>

	<div class="push"></div>
</footer>