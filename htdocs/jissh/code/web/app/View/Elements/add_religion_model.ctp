<section id="add_religion" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Add Religion</h2> 
		</div>
		<div class="body">
		<p><input type="text" name="religion_new" id="religion_new" /></p>
        <div class="validator" style="visibility: hidden; position:relative;">
        	<span class="arrow"></span><p>Please complete this mandatory field.</p>
        </div> 
		<hr />
		<button type="button" class="close button blue small"> Close </button> 
        <button id="sBtn_add_religion" type="button" class="button green small" onclick="PatientDetail.validateReligion();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
		<span id="status_add_religion">Processing...</span>
        </div>
	</div> 
</section>