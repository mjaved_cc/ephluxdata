<!-- FORM FOR FORGOT PASSWORD -->
<?php echo $this->Form->create('User', array(
	'class' => 'forgot_password validate ' . $active_class,
	'url' => array('controller' => 'demos' , 'action' => 'forgot_password')
	)); ?>

<!-- Begin Header -->
<div class="header clearfix">
	<h2>Forgot Password</h2>		
</div>
<!-- End Header -->

<div class="body"><!-- Begin Body-->

	<?php
	if ($this->Misc->is_not_empty($api_response)) {

		$response = json_decode($api_response);

		if ($response->header->code == 0) {
			$class = 'success';
			$image_name = 'success_icon.png';
		} else {
			$class = 'error';
			$image_name = 'error_icon.png';
		}

		echo $this->element('notification', array(
			"message" => $response->header->message,
			"class" => $class,
			"image_name" => $image_name
		));
	}
	?>

	<!--========= Form Fields  =========-->
	<fieldset>
		<legend>Please Tell us your email address</legend>

		<p>
			<label>Email Address:</label>
			<small>We will send you a new password at this email Address</small>
			<?php
			echo $this->Form->input('username', array(
				'label' => false, 'class' => 'large', 'div' => false, 'required' => 'required' , 'type' => 'email'
			));
			?>
		</p>

	</fieldset>
	<div class="clear"></div>


</div> <!-- End Body -->

<!-- Begin Footer -->
<div class="footer">
	<button name="button" type="submit" class="button dark" >Reset Password</button>
	<a href="#" data-target="login" class="linkform fr"> Back to Login</a>
</div>
<!-- End Footer -->                
<?php echo $this->Form->end(); ?>