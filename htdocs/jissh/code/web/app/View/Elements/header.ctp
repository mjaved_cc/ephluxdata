<header id="header">
	<div class="container_12"> 


		<!--========= Logo =========--> 
		<a href="javascript:void(0);" class="logo ir">Logo Name</a> 

		<!--========= User menu =========-->
		<div id="user_menu">
			<a href="javascript:void(0);" class="menu_btn">
<!--				<img src="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'dp_small.jpg') ?>" alt="Display Picture" class="display_pic"/>-->
				<span class="user_id"><?php echo $this->Access->get_my('username') ?></span>
				<span class=" ir arrow">Show/Hide Menu</span>
			</a>
			<div class="menu">

				<!--Search Field-->
				<!--Search box-->

				<div class="search_container">
					<div class="search">
						<input name="" type="text">
						<a href="javascript:void(0);" class="search_icon ir enable_tip" title="Search">Search</a>

					</div>
				</div>

				<!--Logout Button-->
				<a href="<?php echo $this->Misc->get_fully_qualified_url('demos/logout') ?>" class="logout button red enable_tip" title="End this Session">Log Out</a>

				<!-- Tabs -->
				<ul class="tabs">
					<li><a class="enable_tip settings" title="Settings" ><span>Settings</span></a></li>
<!--					<li><a class="enable_tip emails" title="Emails"><span>Emails</span></a></li>
					<li><a class="enable_tip themes" title="Theme"><span>Messages</span></a></li>-->
				</ul>

				<!-- Settings-->
				<div class="pane settings">
					<ul>					
						<li><?php
echo $this->Html->link(
		$this->Html->tag('span', 'Icon', array('class' => 'icon password ir')) . 'Change Password', array('controller' => 'demos', 'action' => 'reset_account_password', 'full_base' => true), array('escape' => false));
?></li>						
<!--						<li><a href="javascript:void(0);"> <span class="icon account ir">Icon</span>Account Settings</a></li>
						<li><a href="javascript:void(0);"> <span class="icon profile ir">Icon</span>Profile Settings</a></li>
						<li><a href="javascript:void(0);"> <span class="icon notification ir">Icon</span>Notification Settings</a></li>-->
					</ul>
					<!--<div class="footer"><a href="javascript:void(0);">Go to Control Panel</a></div>-->
				</div>

				<!--Emails-->
				<!--				<div class="pane emails"><ul>
										<li><a href="javascript:void(0);">New Feature: I want to request a new feature <span class="icon ir">Icon</span><small>From Stacy, Today at 3:12 PM</small></a></li>
										<li><a href="javascript:void(0);" class="read">Bug Report: I came across this issue in the Nav. <span class="icon ir">Icon</span><small>From Mike on 25th October</small></a></li>
										<li><a href="javascript:void(0);" class="read">Status?: What is the staus on the new build?<span class="icon ir">Icon</span><small>From Gary on 22th October</small> </a></li>
										<li><a href="javascript:void(0);">Database Report: Database maintance schedule<span class="icon ir">Icon</span><small>From Brad on 21th October</small></a></li>
										<li><a href="javascript:void(0);">Account Credentials: Please reset my password<span class="icon ir">Icon</span><small>From Lucy on 15th October</small></a></li>
									</ul>
									<div class="footer"><a href="javascript:void(0);">Read All Emails</a></div>
								</div>-->

				<!-- Theme Switcher-->
				<!--				<div class="pane themes">
									  <ul id="css_switcher">
										  <li><a href="#" onClick="setActiveStyleSheet('light'); 
				return false;" data-theme="light">Light Theme</a></li>
										  <li><a href="#" data-theme="dark" onClick="setActiveStyleSheet('dark'); 
				return false;">Dark Theme</a></li>
									  </ul>
								</div>-->
			</div>
		</div>

		<!--========= Toggle Button for Navigation =========-->
		<a href="javascript:void(0);" class="nav_toggle ir">Show/Hide Menu</a>



		<!--========= Navigation =========-->
		<nav id="main_nav" class="clearfix">
			<ul class="sf-menu">
				<?php if ($this->Access->check('Dashboard', 'index')): ?>
					<li><?php echo $this->Html->link('Dashboard', array('controller' => 'Dashboard', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li><?php endif; ?>				
				<li><a href="#">Setup</a>
                    <ul>
                    	<li><?php echo $this->Html->link('Notifications', array('controller' => 'notifications', 'action' => 'add', 'full_base' => true, 'plugin' => false)); ?></li>
						<?php if ($this->Access->check('ConfigOptions', 'index')): ?>
							<li><?php echo $this->Html->link('Config Options', array('controller' => 'config_options', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li><?php endif; ?>
                        <li><a href="#">Locale</a>
                            <ul>
                                <li><?php echo $this->Html->link('Countries', array('controller' => 'countries', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Languages', array('controller' => 'languages', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Religions', array('controller' => 'religions', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                            </ul>
                        </li>
                        <li><a href="#">Patients</a>
                            <ul>
                                <li><?php echo $this->Html->link('Relationships', array('controller' => 'relation_ships', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                            </ul>
                        </li>
                        <li><a href="#">Faculty</a>
                            <ul>
								<li><?php echo $this->Html->link('Divisions', array('controller' => 'divisions', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Voluntary Act Sponsors', array('controller' => 'sponsors', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Programs', array('controller' => 'programs', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Providers', array('controller' => 'providers', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Voluntary Activity Types', array('controller' => 'vpd_activity_types', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Annual Vacation Types', array('controller' => 'avp_types', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>                                
								<li><a href="#">Allowed Services</a>
									<ul>
										<li><?php echo $this->Html->link('Allowed Services Groups', array('controller' => 'allowed_service_groups', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
										<li><?php echo $this->Html->link('Allowed Services', array('controller' => 'allowed_services', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                    </ul>
                                </li>
                                <li><?php echo $this->Html->link('Levels', array('controller' => 'levels', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('CaseLoads', array('controller' => 'caseLoads', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('License Types', array('controller' => 'license_types', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Offcial Titles', array('controller' => 'official_titles', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                            </ul>
                        </li>
                        <li><a href="#">Templates</a>
                            <ul>
                                <li><?php echo $this->Html->link('Attribute Categories', array('controller' => 'template_attributes_categories', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Attributes', array('controller' => 'template_attributes', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Templates', array('controller' => 'Templates', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                                <li><?php echo $this->Html->link('Doctor\'s view', array('controller' => 'Templates', 'action' => 'medication',4,13,1, 'full_base' => true, 'plugin' => false)); ?></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#">Patients Management</a>
                    <ul>
                        <li><?php echo $this->Html->link('Add Patient', array('controller' => 'patient_details', 'action' => 'add', 'full_base' => true, 'plugin' => false)); ?></li>
                        <li><?php echo $this->Html->link('List Patients', array('controller' => 'patient_details', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                        <li><?php echo $this->Html->link('Create Waiting Lists', array('controller' => 'waiting_lists_titles', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                    </ul>
                </li>
                <li><a href="#">Clinician Management</a>
                    <ul>
                        <li><?php echo $this->Html->link('Add Clinician', array('controller' => 'employee_details', 'action' => 'doctor_register', 'full_base' => true, 'plugin' => false)); ?></li>
                        <li><?php echo $this->Html->link('List Clinicans', array('controller' => 'employee_details', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                    </ul>
				</li>   
				<li><a href="#">User Management</a>
                <ul>
                    <li><?php echo $this->Html->link('List Users', array('controller' => 'users', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
					<li><?php echo $this->Html->link('User Groups', array('controller' => 'groups', 'action' => 'index', 'full_base' => true, 'plugin' => false)); ?></li>
                </ul>
            </li>
            
			</ul>
			</li>
            

			</ul>
			</li>

			</ul>
		</nav>
        
	</div>
</header>
<section class="grid_12 margin-top-2em">
<button class="button dark small" onclick="App.redirect({
    controller : 'employee_details',
    action : 'doctor_register'
})"><?php echo __('Add Clinician') ?></button>
<button class="button dark small" onclick="App.redirect({
    controller : 'PatientDetails',
    action : 'add'
})"><?php echo __('Add Patient') ?></button>
<button class="button dark small" onclick="App.redirect({
    controller : 'Templates',
    action : 'add'
})"><?php echo __('Create Template') ?></button>

<button class="button dark small" onclick="App.redirect({
    controller : 'conversations',
    action : 'index'
})"><?php echo __('Conversation') ?></button>


<button class="button dark small attach_modal" data-modal="#select_appointment_type_model"><?php echo __('Create Appointment') ?></button>
</section>
<section id="select_appointment_type_model" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Select Attachment Type</h2> 
		</div>
		<div class="body">
        	<table width="100%">
            	<tr>
                	<td>
                    	<input type="radio" name="appointment_type" id="appointment_type" checked="checked" value="0" /> Individual
                        <br />
                        <input type="radio" name="appointment_type" id="appointment_type" value="1" /> Join a Session
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                        <button type="button" class="close button blue small"> Close </button> 
                        <button id="sBtn_add_country" type="button" class="button green small" onclick="App.goto_appointment_create();"> Join </button> <!-- Close class on any element inside the modal box will close the modal. -->
                        <!--<span id="status_add_call_log">Processing...</span>-->
                    </td>
                </tr>
			</table>
        	
        </div>
	</div> 
</section>       