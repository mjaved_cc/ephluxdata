<!--============================ CALENDAR ============================-->
<div class="clearfix">	
	<div class="grid_12">
		<div id='loading' style='display:none'>loading...</div>
		<!-- Begin Calendar -->
		<div id='calendar'></div>
		<!-- End Calendar -->
		<p>
			<input type="hidden" id="event_data_json" name="event_data_json" />
		</p>
		<p><?php 
				echo $this->Form->hidden('url_doctor_working_hours', array('id' => 'url_doctor_working_hours','value' => $this->Misc->get_fully_qualified_url('employee_details/get_doctor_working_hours/' . $user_id . '/' . $editable . EXTENSION_JSON))); ?>
		</p>

	</div>
</div>