<section id="add_country" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Add Country</h2> 
		</div>
		<div class="body">
		<p><span style="vertical-align:sub;">Name:</span> <input type="text" name="country_new" id="country_new" /></p>
        <p><span style="vertical-align:sub;">Nationality:</span> <input type="text" name="nationality_new" id="nationality_new" /></p>
        <div class="validator" style="visibility: hidden; position:relative;">
        	<span class="arrow"></span><p>Please complete this mandatory field.</p>
        </div> 
		<hr />
		<button type="button" class="close button blue small"> Close </button> 
        <button id="sBtn_add_country" type="button" class="button green small" onclick="PatientDetail.validateCountry();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
		<span id="status_add_country">Processing...</span>
        </div>
	</div> 
</section>