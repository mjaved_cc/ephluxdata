<!-- FORM FOR LOGIN -->      
<?php echo $this->Form->create('User', array(
	'class' => 'login wf validate ' . $active_class,
	'url' => array('controller' => 'demos' , 'action' => 'login')
	)); ?>
<!-- Begin Header -->
<div class="header clearfix">
	<h2>Login</h2>	
</div>

<!-- End Header -->
<div class="body"><!-- Begin Body-->

	<?php
	if ($this->Misc->is_not_empty($api_response)) {

		$response = json_decode($api_response);

		if ($response->header->code == 0) {
			$class = 'success';
			$image_name = 'success_icon.png';
		} else {
			$class = 'error';
			$image_name = 'error_icon.png';
		}

		echo $this->element('notification', array(
			"message" => $response->header->message,
			"class" => $class,
			"image_name" => $image_name
		));
	}
	?>

	<!--========= Form Fields  =========-->
	<fieldset>
		<legend>Please login to continue</legend>

		<p><?php echo $this->Form->input('username', array('label' => __('Email'), 'class' => 'large', 'div' => false, 'required' => 'required')); ?></p>
		<p><?php echo $this->Form->input('password', array('class' => 'large', 'div' => false, 'required' => 'required')); ?></p>
		<div class="remember_password clearfix">
			<?php echo $this->Form->checkbox('remember_me', array('div' => false , 'class' => 'on_off' ,'hiddenField' => false)) . 'Remember Password'; ?>
		</div>
	</fieldset>
	<div class="clear"></div>


</div> <!-- End Body -->

<!-- Begin Footer -->
<div class="footer">
	<button type="submit"  class="button green large">Login</button>
	<a href="#" data-target="forgot_password" class="linkform fr"> Forgot Password?</a>
</div>
<!-- End Footer -->    
<?php echo $this->Form->end(); ?>