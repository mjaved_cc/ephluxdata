<?php $pagination_params = $this->Paginator->params(); 
?>
<table class="datagrid wf">
                    <thead>
                        <tr>
                            <th><?php echo __('Patient Name'); ?></th>
                            <th><?php echo __('Employee Name'); ?></th>
                            <th><?php echo __('Appointment Date'); ?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($appointments)) {
                            foreach ($appointments as $appointment): ?>
                            <tr>
                                <td align="center"><?php echo $appointment['PatientDetail']['full_name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['EmployeeDetail']['display_name']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['Appointment']['created']; ?>&nbsp;</td>
                                <td align="center"><?php echo $appointment['Appointment']['status']; ?>&nbsp;</td>
                                <td class="actions" align="center">		
                                	<?php echo $this->Html->link(__('Detail'), array('controller'=>'appointments','action' => 'view',$appointment['Appointment']['id'])); ?> 					
                                </td>
                            </tr>
                        <?php endforeach; 
                        }
                        else{
                            ?>
                            <tr>
                                <td colspan="9">No appointments found.</td>
                            </tr>
                            <?php		
                        }
                        ?>
                    </tbody>
                </table>
<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>