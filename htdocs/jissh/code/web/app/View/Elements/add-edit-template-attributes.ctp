<div style="float:left; display:block; width:38%;">
<strong>Categories</strong>
<ul class="box-template">
    
    <?php
    $i=0;
    foreach($attributeCategories as $attributeCategory) {
        if($i==0){
            $class = "attribute-cat-active";	
        }
        else {
            $class = "";	
        }
    ?>
    <li style="padding:0 10px;" class="<?php echo $class;?>">
        <a href="#_" onclick="loadAttributesByCatId('<?php echo $attributeCategory['TemplateAttributesCategory']['id']; ?>',this)"><?php echo $attributeCategory['TemplateAttributesCategory']['name']; ?></a>
    </li>
    <?php	
        $i++;
    }
    ?>
</ul>
</div>
<div style="float:left; display:block; width:50%; padding-left:30px;">
	<strong>Attributes</strong>
    <div id="ajax_attributes">
    	<?php echo $this->element('attributes-list');?>
    </div>
</div>
<br clear="all" /><br clear="all" />
<div style="float:right; display:block; width:10%; padding-right:40px;">
	<button type="button" class="button green small" data-target="register" onclick="addAttr()">Add</button>
</div>
<br clear="all" /><br clear="all" />
                