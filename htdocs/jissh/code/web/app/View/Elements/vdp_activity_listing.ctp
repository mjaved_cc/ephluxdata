<?php $pagination_params = $this->Paginator->params(); ?>
<?php 
	$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
	echo $this->Form->create("VpdActivity",array('action' => 'index','id' => 'validate'));
	echo $this->Form->input('emp_id',array('type'=>'hidden','value'=>$emp_id));
	echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
	//echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small'));
	?>
    <button type="button" class="button green small" id="ajax_search_btn" onclick="VpdActivity.search(this);">Search</button>
	<div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
        <a href="#_" onclick="VpdActivity.clear();VpdActivity.search(this);">X</a>
    </div>
	<?php
	echo $this->Form->end();
?> 

<table class="datagrid wf">
	<thead>
		<tr>
        	<th><?php echo $this->Paginator->sort('provider_id'); ?></th>
            <th><?php echo $this->Paginator->sort('vpd_activity_type_id'); ?></th>
            <th><?php echo $this->Paginator->sort('no_of_units'); ?></th>
			<th><?php echo $this->Paginator->sort('activity_name'); ?></th>
            <th><?php echo $this->Paginator->sort('modality_id'); ?></th>
            <th><?php echo $this->Paginator->sort('sponsor_id'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('date_complition'); ?></th>
            
			<th ><?php echo __('Action'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($vpdActivities)) {
			foreach ($vpdActivities as $vpdActivity): ?>
			<tr>
            	<td align="center"><?php echo $vpdActivity['Provider']['name']; ?>&nbsp;</td>
                <td align="center"><?php echo $vpdActivity['VpdActivityType']['name']; ?>&nbsp;</td>
                <td align="center"><?php echo $vpdActivity['VpdActivity']['no_of_units']; ?>&nbsp;</td>
                <td align="center"><?php echo $vpdActivity['VpdActivity']['activity_name']; ?>&nbsp;</td>
				<td align="center"><?php echo $vpdActivity['Modality']['name']; ?>&nbsp;</td>
                <td align="center"><?php echo $vpdActivity['Sponsor']['name']; ?>&nbsp;</td>
			
				<td align="center"><?php echo $vpdActivity['VpdActivity']['status']; ?>&nbsp;</td>
				<td align="center"><?php echo $vpdActivity['VpdActivity']['date_complition']; ?>&nbsp;</td>
				<td class="actions" align="center">								
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $emp_id,$vpdActivity['VpdActivity']['id'])); ?> |
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $emp_id,$vpdActivity['VpdActivity']['id']), null, __('Are you sure you want to delete # %s?', $vpdActivity['VpdActivity']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; 
		}
		else{
			?>
			<tr>
				<td colspan="9"><?php echo NO_VPD_ACTIVITY_MESSAGE?></td>
			</tr>
			<?php		
		}
		?>
	</tbody>
</table>
<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>