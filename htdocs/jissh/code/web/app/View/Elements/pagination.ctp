<?php
$prev_url = 'javascript:void(0)';
$next_url = 'javascript:void(0)';
$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
$url = array();

if($q!="") {
	$url['q'] = $q;	
}

if ($this->Paginator->hasPrev()) {
	$url['page'] = ($pagination_params['page'] - 1);
	$prev_url = $this->Paginator->url($url, false);
}
if ($this->Paginator->hasNext()) {
	$url['page'] = ($pagination_params['page'] + 1);
	$next_url = $this->Paginator->url($url, false);	
}
	//$next_url = $this->Paginator->url(array('page' => ($pagination_params['page'] + 1).$q), false);

// means we have multiple pages
if ($prev_url != $next_url) {
	?>
	<!--Pagination-->
	<ul class="pagination clearfix">
		<li><?php echo $this->Paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?> </li>
		<?php
		echo $this->Paginator->numbers(array(
			'separator' => '',
			'tag' => 'li',
			'currentClass' => 'active'
		));
		?>				
		<li><?php echo $this->Paginator->next(__('next', true).' >>', array('id'=>'next'), null, array('class' => 'disabled'));?></li>
	</ul>
<?php } ?>