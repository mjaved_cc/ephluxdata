<?php if (!empty($available_doctors)) { ?> 
	<table class="datagrid wf">					
		<tbody>
			<?php
			$i=0; 
			foreach ($available_doctors as $doctor): ?>
				<tr>
					<td align="center">
						<input name="data[User][id]" value="<?php echo $doctor['user_id'] ?>" type="radio" onclick="Appointment.get_doctor_appointment_schedule()"/>
					</td>
					<td align="center">
						<?php 
							echo h($doctor['first_name']);
							echo $this->Form->hidden('EmployeeDetail.'.$i.'.id' , array('value' => $doctor['id'] , 'id' => 'employee_detail_id' . $doctor['user_id']));
						?>
					</td>								
					<td align="center"><?php echo $this->FormWrapper->get_anchor('View', array('controller' => 'employee_details', 'action' => 'doctor_details', $doctor['user_id'])); ?></td>								
				</tr>

			<?php 
			$i++;
			endforeach; ?>
		</tbody>
	</table>
	<?php
} else {

	$class = 'error';
	$image_name = 'error_icon.png';

	echo $this->element('notification', array(
		"message" => 'No match found',
		"class" => $class,
		"image_name" => $image_name
	));
}
?>