<div class="clearfix">
	<div class="grid_10">
		<table class="wf">
			<tbody>
				<tr>
					<td><?php echo $this->FormWrapper->get_server_side_txtbx('first_name'); ?></td>
<!--					<td><?php echo $this->Form->input('first_name', array('required' => 'required', 'div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'form_error')))); ?></td>-->
					<td><?php echo $this->FormWrapper->get_server_side_txtbx('middle_name'); ?></td>
<!--					<td><?php echo $this->Form->input('middle_name', array('div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'form_error')))); ?></td>-->
					<td><?php echo $this->FormWrapper->get_server_side_txtbx('last_name'); ?></td>
<!--					<td><?php echo $this->Form->input('last_name', array('required' => 'required', 'div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'form_error')))); ?></td>-->
				</tr>
				<tr>			
					<td><?php echo $this->Form->input('license_number', array('div' => false)); ?></td>
					<td><?php echo $this->FormWrapper->get_drop_down('genders', $genders, $gender); ?></td>
					<td><?php echo $this->FormWrapper->get_drop_down('license_type_id', $license_types, $license_type_id, 'Please Select ...'); ?></td>					
				</tr>
				<tr>
					<td><?php echo $this->FormWrapper->get_server_side_txtbx('User.username', array('type' => 'email', 'label' => __('Email'))); ?></td>
					<td><?php echo $this->Form->input('jish_email', array('type' => 'email', 'div' => false)); ?></td>			
				</tr>
				<tr>
					<td><?php echo $this->Form->input('mobile_saudi', array('div' => false)); ?></td>
					<td><?php echo $this->Form->input('landline_saudi', array('div' => false)); ?></td>
					<td><?php echo $this->Form->input('mobile_homecountry', array('div' => false)); ?></td>
					<td><?php echo $this->Form->input('landline_homecountry', array('div' => false)); ?></td>
				</tr>
				<tr>
					<td><?php echo $this->FormWrapper->get_datepicker('joining_date'); ?></td>
					<td><?php echo $this->FormWrapper->get_datepicker('contract_expiry_date'); ?></td>
					<td><?php echo $this->FormWrapper->get_datepicker('dob',array('required' => false)); ?></td>
				</tr>
				<tr>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('languages', $languages, $language_selected_val); ?></td>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('official_titles', $official_titles, $off_title_selected_val); ?></td>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('levels', $levels, $level_selected_val); ?></td>

				</tr>
				<tr>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('divisions', $divisions, $division_selected_val); ?></td>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('case_loads', $case_loads, $case_load_selected_val); ?></td>
					<td><?php echo $this->FormWrapper->echo_dd_checklist('allowed_services', $allowed_services, $allowed_service_selected_val); ?></td>			
				</tr>
				<tr>
					<td><?php
					echo $this->Form->hidden('User.id');
					echo $this->Form->hidden('id');
					?></td>
				</tr>
			</tbody>
		</table>

	</div>
	<div class="grid_2">
		<table>
			<tbody>
				<tr>
					<td align="center"><?php echo $this->Html->image($this->Misc->get_profile_thumb_url($profile_thumb_relative_url), array('id' => 'profile_pic', 'alt' => 'Thumbnail', 'width' => THUMBNAIL_MAX_WIDTH, 'height' => THUMBNAIL_MAX_HEIGHT)) ?></td>			
					<td><?php echo $this->Form->hidden('picture_name', array('id' => 'picture_name')); ?></td>			
				</tr>	
				<tr>
					<td><a href="#" class="button attach_modal light" data-modal="#facebox">Upload Profile Picture</a></td>			
				</tr>
			</tbody>
		</table>
	</div>
</div>