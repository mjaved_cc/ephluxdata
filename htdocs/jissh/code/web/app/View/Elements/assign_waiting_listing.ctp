<?php $pagination_params = $this->Paginator->params(); ?>
<?php 
	$q = isset($this->request->params['named']['q']) ? $this->request->params['named']['q'] : "";
	echo $this->Form->create("WaitingListsTitle",array('action' => 'assign','id' => 'validate','onsubmit'=>'return onSbumit();'));
	//echo $this->Form->input("pid", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med'));
	echo $this->Form->input("q", array('label' => 'Search for','required' => 'required', 'div' => false,'class'=>'med float-left-margin','value'=>$q));
	echo $this->Form->button('Search', array('id'=>'ajax_search_btn','type' => 'button', 'escape' => true, 'class' => 'button green small float-left-margin'));
	echo $this->Form->end();
?>
<div id="cancel_search" style="display:<?php echo ($q!="") ? 'block' : 'none';?>">
	<a href="#_">X</a>
</div>
<table class="datagrid wf">
    <thead>
        <tr>
            <th class="sorting"><?php echo $this->Paginator->sort('name'); ?></th>
            <th class="sorting"><?php echo $this->Paginator->sort('division_id'); ?></th>
            <th class="sorting"><?php echo $this->Paginator->sort('created'); ?></th>
            <th ><?php echo __('Action'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach ($waitingListsTitles as $waitingListsTitle): ?>
            <tr>
                <td align="center"><?php echo h($waitingListsTitle['WaitingListsTitle']['title']); ?>&nbsp;</td>
                <td align="center"><?php echo h($waitingListsTitle['Division']['name']); ?>&nbsp;</td>
                <td align="center"><?php echo h($waitingListsTitle['WaitingListsTitle']['created']); ?>&nbsp;</td>
                <td class="actions" align="center">
                    <button type="submit" onclick="WaitingListsTitle.addToWaitingList('<?php echo $waitingListsTitle['WaitingListsTitle']['id'];?>');" class="button green small">Add to waiting list</button>
                </td>
            </tr>
            
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('pagination',array('pagination_params' => $pagination_params)); ?>