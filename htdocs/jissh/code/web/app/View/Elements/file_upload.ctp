<?php
if (isset($file['code']) && ($file['code'] > ECODE_SUCCESS)) {

	$class = 'error';
	$image_name = 'error_icon.png';

	echo $this->element('notification', array(
		"message" => $file['message'],
		"class" => $class,
		"image_name" => $image_name
	));
} else if (isset($file['code']) && ($file['code'] == ECODE_SUCCESS)) {
	?>
	<div id='filename'><?php echo $this->Html->image($this->Misc->get_fully_qualified_url($file['thumbnail_url']),array('id' => 'temp_profile_pic')) ?></div>
	<p><?php echo $this->Form->hidden('temp_picture_name', array('value' => $file['name'])); ?></p>
<?php } else {
	?>
	<!--============================Modal============================-->
	<section id="facebox" class="grid_6"> 
		<div class="box"> 
			<div class="header clearfix">
				<h2>Doctor</h2> 
			</div>
			<div class="body">
				<?php echo $this->Form->create('FileUpload', array('type' => 'file')); ?>
				<fieldset>
					<legend>Upload Picture</legend>
					<p>					
						<?php echo $this->Form->input('file', array('required' => 'required','label' => 'Picture', 'type' => 'file', 'div' => false)); ?>
					</p>				
					<p>
						<button class="button green" value="upload" onclick="return FileUpload.upload_via_iframe(this.form, App.getUrl({controller : 'employee_details',action : 'upload_profile_image'}),'upload');">Upload</button>
						<button type="button" class="close button blue">Cancel</button> <!-- Close class on any element inside the modal box will close the modal. -->					
					</p>					
					<div id="upload"></div>
				</fieldset>
				<?php echo $this->Form->end(); ?>			

			</div>
		</div> 
	</section>
<?php } /* if (!empty($file)) {
  ?>
  <div id='filename'><img src="<?php echo $this->Misc->get_fully_qualified_url($file['thumbnail_url']) ?>" /></div>
  <p><?php echo $this->Form->hidden('picture_name', array('value' => $file['name'])); ?></p>
  <?php
  } else {
  echo $this->Form->create('FileUpload', array('type' => 'file'));
  echo $this->Form->input('file', array('label' => 'Picture', 'type' => 'file'));
  ?>

  <button class="button blue large" value="upload" onclick="return FileUpload.upload_via_iframe(this.form, App.getUrl({controller : 'employee_details',action : 'upload_profile_image'}),'upload');">Upload</button>
  <div id="upload"></div>
  <?php
  echo $this->Form->end();
  } */
?>