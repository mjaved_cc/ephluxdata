<section id="add_call_log" class="grid_6" style="position: fixed; z-index: auto; top: 66.7px; left: 327.7578125px; display: none;"> 
	<div class="box"> 
		<div class="header clearfix">
			<h2>Log Calls</h2> 
		</div>
		<div class="body">
			<?php echo $this->Form->create('Appointment',array('action'=>'save_call_log','id' => 'call_log_form')); ?>
				<table width="100%">
					<tr>
						<td>Caller:</td> 
						<td>
						<input type="text" class="small" name="caller" id="caller" value="<?php echo $EmployeeDetail['display_name'];?>" readonly="readonly" />
						<input type="hidden" name="caller_id" id="caller_id" value="<?php echo $EmployeeDetail['id'];?>" />
						</td>
					</tr>	
					<tr>
						<td>Receiver:</td> 
						<td><input type="text" class="small" name="receiver" id="receiver" />
						<div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div></td>
					</tr>
					<tr>
						<td>Subject:</td> 
						<td><input type="text" class="small" name="call_subject" id="call_subject" />
						<div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div></td>
					</tr>
					<tr>
						<td>Calling Date:</td> 
						<td><input type="text" name="calling_date_time" class="datepicker vAlign mediam" id="calling_date_time" />
						<div class="validator" style="display:none; position:relative;">
							<span class="arrow"></span><p>Please complete this mandatory field.</p>
						</div></td>
					</tr>
					
					<tr>
						<td colspan="2">
							<hr />
							<button type="button" class="close button blue small"> Close </button> 
							<button id="sBtn_add_country" type="button" class="button green small" onclick="Appointment.validateAndSaveCallLog();"> Save </button> <!-- Close class on any element inside the modal box will close the modal. -->
							<!--<span id="status_add_call_log">Processing...</span>-->
						</td>
					</tr>
				</table>	
				<input type="hidden" name="id" value="<?php echo $id;?>" />
			<?php echo $this->Form->end(); ?>
        </div>
	</div> 
</section>