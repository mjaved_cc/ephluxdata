<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<button class="button dark" data-target="register" onclick="VpdActivity.redirect('<?php echo $this->Html->url(array(
				"controller" => "VpdActivities",
				"action" => "index",
				$emp_id
			)); ?>')"><?php echo __('Back to ' . $page_heading.' listing') ?></button>
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('VpdActivity',array('id' => 'validate')); ?>
			<fieldset>
				<legend><?php echo __($page_heading)?></legend>
				<p><?php echo $this->Form->input('employee_detail_id',array('type'=>'hidden', 'div' => false, 'value'=> $emp_id)); ?></p>
                <p><?php echo $this->Form->input('provider_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('sponsor_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('vpd_activity_type_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('modality_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('status',array('options'=>array('pending'=>'pending','complete'=>'complete'),'div' => false)); ?></p>
                <p><?php echo $this->Form->input('date_complition',array('required'=>'required', 'class'=>'datepicker', 'type'=>'text', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('no_of_units',array('pattern'=>'[1-9]+','required'=>'required', 'type'=>'text', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('activity_name',array('pattern'=>'[a-zA-Z ]+','required'=>'required', 'type'=>'text', 'div' => false)); ?></p>

            </fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>

