<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('Add') ?></h2>
			<button class="button dark" data-target="register" onclick="VpdActivity.redirect('<?php echo $this->Html->url(array(
				"controller" => "VpdActivities",
				"action" => "index",
				$emp_id
			)); ?>')"><?php echo __('Back to ' . $page_heading.' listing') ?></button>
		</div>
		<!-- End Header -->

		<div class="body">

			<?php echo $this->Form->create('VpdActivity',array('id' => 'validate')); ?>
			<fieldset>
				<legend><?php echo __($page_heading)?></legend>
                <?php echo $this->Form->input('id'); ?>
				<p><?php echo $this->Form->input('employee_detail_id',array('type'=>'hidden', 'div' => false, 'value'=> $emp_id)); ?></p>
                <p><?php echo $this->Form->input('provider_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('sponsor_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('vpd_activity_type_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('modality_id',array('div' => false)); ?></p>
                <p><?php echo $this->Form->input('status',array('options'=>array('pending'=>'pending','complete'=>'complete'),'div' => false)); ?></p>
                <p><?php echo $this->Form->input('date_complition',array('required'=>'required', 'class'=>'datepicker', 'type'=>'text', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('no_of_units',array('pattern'=>'[1-9]+','required'=>'required', 'type'=>'text', 'div' => false)); ?></p>
                <p><?php echo $this->Form->input('activity_name',array('pattern'=>'[a-zA-Z ]+','required'=>'required', 'type'=>'text', 'div' => false)); ?></p>
			</fieldset>
			<?php
			echo $this->Form->button('Submit', array('type' => 'submit', 'escape' => true, 'class' => 'button green small'));
			?>
			<?php echo $this->Form->end(); ?>

		</div>
	</div>
</section>


<div class="vpdActivities form">
<?php echo $this->Form->create('VpdActivity'); ?>
	<fieldset>
		<legend><?php echo __('Edit Vpd Activity'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('employee_detail_id');
		echo $this->Form->input('provider_id');
		echo $this->Form->input('vpd_activity_type_id');
		echo $this->Form->input('modality_id');
		echo $this->Form->input('status');
		echo $this->Form->input('sponsor_id');
		echo $this->Form->input('date_complition');
		echo $this->Form->input('no_of_units');
		echo $this->Form->input('activity_name');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VpdActivity.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('VpdActivity.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Vpd Activities'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Providers'), array('controller' => 'providers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Provider'), array('controller' => 'providers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vpd Activity Types'), array('controller' => 'vpd_activity_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vpd Activity Type'), array('controller' => 'vpd_activity_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('controller' => 'modalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('controller' => 'modalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sponsors'), array('controller' => 'sponsors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sponsor'), array('controller' => 'sponsors', 'action' => 'add')); ?> </li>
	</ul>
</div>
