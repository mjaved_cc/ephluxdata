<div class="vpdActivities view">
<h2><?php  echo __('Vpd Activity'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Employee Detail'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vpdActivity['EmployeeDetail']['id'], array('controller' => 'employee_details', 'action' => 'view', $vpdActivity['EmployeeDetail']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Provider'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vpdActivity['Provider']['name'], array('controller' => 'providers', 'action' => 'view', $vpdActivity['Provider']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vpd Activity Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vpdActivity['VpdActivityType']['name'], array('controller' => 'vpd_activity_types', 'action' => 'view', $vpdActivity['VpdActivityType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modality'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vpdActivity['Modality']['name'], array('controller' => 'modalities', 'action' => 'view', $vpdActivity['Modality']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sponsor'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vpdActivity['Sponsor']['name'], array('controller' => 'sponsors', 'action' => 'view', $vpdActivity['Sponsor']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Complition'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['date_complition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Units'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['no_of_units']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Activity Name'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['activity_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vpdActivity['VpdActivity']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vpd Activity'), array('action' => 'edit', $vpdActivity['VpdActivity']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vpd Activity'), array('action' => 'delete', $vpdActivity['VpdActivity']['id']), null, __('Are you sure you want to delete # %s?', $vpdActivity['VpdActivity']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vpd Activities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vpd Activity'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Employee Details'), array('controller' => 'employee_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Employee Detail'), array('controller' => 'employee_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Providers'), array('controller' => 'providers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Provider'), array('controller' => 'providers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vpd Activity Types'), array('controller' => 'vpd_activity_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vpd Activity Type'), array('controller' => 'vpd_activity_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('controller' => 'modalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('controller' => 'modalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sponsors'), array('controller' => 'sponsors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sponsor'), array('controller' => 'sponsors', 'action' => 'add')); ?> </li>
	</ul>
</div>
