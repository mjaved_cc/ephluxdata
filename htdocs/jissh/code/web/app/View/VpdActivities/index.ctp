<!--========= Page Heading  =========-->
<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_12">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
            <h2>Listings</h2>
            <button class="button dark" data-target="register" onclick="VpdActivity.redirect('<?php echo $this->Html->url(array(
				"controller" => "VpdActivities",
				"action" => "add",
				$emp_id
			)); ?>')"><?php echo __('New ' . $page_heading) ?></button>

		</div>
		<!-- End Header -->
		<div class="body">
            <div id="ajax_response"> 
            	<?php echo $this->element('vdp_activity_listing');?>
            </div>
            <!--End Pagination-->
		</div>
		<!-- End box body -->
	</div>
	<!-- End box -->
</section>
