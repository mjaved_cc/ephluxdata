<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$desc = __d('cake_dev', SITE_NAME);
?>
<!doctype html>

<!-- Coniditional CSS Hacks for IE. -->
<!--[if lt IE 8 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie"> <![endif]-->
<html lang="en" class="no-js">
	<!--<![endif]-->

	<!--============================ HEAD ============================-->
	<head>
		<meta charset="utf-8">

		<meta name="description" content="">
		<meta name="author" content="">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta name="robots" content="noindex,nofollow" /> 

		<!--  Mobile viewport optimized -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!--  Title & meta tags -->
		<title>
			<?php echo $desc ?>:
			<?php echo $title_for_layout; ?>
		</title>


		<meta name="description" content="Mriya: Responsive Admin Theme for Web Applications and Backend Interfaces">
		<meta name="author" content="Cosmive">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta name="robots" content="noindex,nofollow" /> 


		<!--  Favicons -->
		<link rel="shortcut icon"  href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'favicon.ico' ) ?>" type="image/ico">
		<link rel="shortcut icon"  href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'favicon.png' ) ?>" type="image/png">


		<!--  Icons for iPhone/iPad -->
		<link rel="apple-touch-icon" href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'apple-touch-icon.png' ) ?>" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'apple-touch-icon-57x57.png' ) ?>" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'apple-touch-icon-72x72.png' ) ?>" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Misc->get_fully_qualified_url(MRIYA_IMAGES_URL . 'apple-touch-icon-114x114.png' ) ?>" />

		<!--========= CSS  =========-->
		<?php echo $this->Minify->css($css_list); ?>



	</head>
	<!--============================ BODY ============================-->

	<body>

		<!--============================ MAIN WRAPPER - required for sticky footer ============================-->
		<div class="wrapper">


			<!--============================ CONTAINER ============================-->
			<div id="main">

				<!--========= LOGO  =========-->
				<div class="logo_login">
					<a href="#" class="ir">Shift Admin</a>
				</div>

				<!--========= Begin Forms  =========-->

				<!-- Begin Box -->
				<div class="box" id="login_container">
					<?php echo $this->fetch('content'); ?>        
				</div><!-- End Box -->


			</div>
			<!-- end of main container --> 

		</div>
		<!-- end of main wrapper --> 




		<!--============================ JAVASCRIPTS ============================--> 
		<?php echo $this->Minify->script($js_list); ?>
	</body>
</html>