<?php

class EmployeeWorkingHour extends AppModel {

	public $hasMany = array(
		'EmployeeWorkingHourBreakup'
	);

	/**
	 * Get total slots per week based on employee & division
	 * 
	 * @param string|int $employee_detail_id Employee Detail Id
	 * @param string|int $division_id Division Id
	 * @return string|int slots per week 
	 */
	function get_per_week_slots($employee_detail_id, $division_id) {

		$result = $this->query('call get_per_week_slots("' . $employee_detail_id . '" , "' . $division_id . '")');
		return $result[0][0]['TotalPossibleSlotsPerWeek'];
	}

	/**
	 * Get all records by employee detail id
	 * 
	 * @param string|int $employee_detail_id Employee Detail Id
	 * @return array 
	 */
	function get_all_by_employee_detail_id($employee_detail_id) {
		return $this->query('call get_all_emp_working_hours_by_emp_id(' . $employee_detail_id . ')');
	}

	/**
	 * Get available working slots for given employee belongs to particular division.
	 * 
	 * $data should be of the form
	 * 
	 * <code>
	 * Array
	  (
	  [waiting_list_title_id] => value1,
	  [employee_detail_id] => value2
	  [date_as_appointment] => value3
	  [working_day] => value4
	  )
	 * </code>
	 * 
	 * @param array $data described above
	 * @return array
	 */
	function get_available_working_slots($data) {
		return $this->query('call get_available_working_slots(
			"' . $data['waiting_list_title_id'] . '",
			"' . $data['employee_detail_id'] . '",
			"' . $data['date_as_appointment'] . '",
			"' . $data['working_day'] . '"
				)');
	}

}

?>
