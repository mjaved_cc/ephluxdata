<?php
App::uses('AppModel', 'Model');
/**
 * AllowedService Model
 *
 * @property AllowedServiceGroup $AllowedServiceGroup
 * @property EmployeeService $EmployeeService
 */
class PatientDetail extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'first_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'characters' => array(
				'rule' => '/^[a-zA-Z ]*$/',
				'message' => 'Alphabets only',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'iqama_no' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Numeirc value only',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'mobile' =>array(
				'rule' => array('numeric'),
				'message' => 'Numeirc value only',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
		'prefered_contact' =>array(
				'rule' => array('numeric'),
				'message' => 'Numeirc value only',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
		'district' => array(
			'rule' => '/^[a-zA-Z ]*$/',
			'message' => 'Alphabets only',
			//'allowEmpty' => false,
			'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
		'city' => array(
			'rule' => '/^[a-zA-Z ]*$/',
			'message' => 'Alphabets only',
			//'allowEmpty' => false,
			'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
		)
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Religion' => array(
			'className' => 'Religion',
			'foreignKey' => 'religion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'PatientFamilyDetail' => array(
			'className' => 'PatientFamilyDetail',
			'foreignKey' => 'patient_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * Remove relation_ship by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
	/**
	 * Get patient by waiting lists.
	 * 
	 * This function specially for drop down in appointments as it will fetch result as id & name
	 * 
	 * @param string $waiting_list_title_id waiting list title id
	 * @return array
	 */
	function get_by_waiting_lists_title_id($waiting_list_title_id){
		return $this->query('call get_patient_by_waiting_list_title_id(' . $waiting_list_title_id . ')');
	}
	
	/**
	 * Get all active patients
	 * 
	 * @param string $keyword Search patient
	 * @return array
	 */
	function get_all($keyword = ''){
		return $this->query('call get_all_patients("'. $keyword .'")');
	}
	
	function get_patient_related_to_doctor( $id ) {
		return $this->query('call get_patients_related_to_doctor("'. $id .'")');
	}
	
}
