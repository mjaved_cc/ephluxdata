<?php
App::uses('AppModel', 'Model');
/**
 * TemplateAttribute Model
 *
 */
class TemplateAttribute extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'attr_type' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public $belongsTo = array(
		'TemplateAttributesCategory' => array(
			'className' => 'TemplateAttributesCategory',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * Get details for given id. Its extracting data from allowed service table only.
	 * 
	 * @param int|string $div_id allowed service id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}


	/**
	 * Remove allowed service by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
