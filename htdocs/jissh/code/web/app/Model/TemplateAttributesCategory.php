<?php
App::uses('AppModel', 'Model');
/**
 * TemplateAttribute Model
 *
 */
class TemplateAttributesCategory extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	/**
	 * Get details for given id. Its extracting data from allowed service table only.
	 * 
	 * @param int|string $div_id allowed service id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}


	/**
	 * Remove allowed service by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
