<?php

App::uses('AppModel', 'Model');

/**
 * Division Model
 *
 * @property EmployeeDetail $EmployeeDetail
 * @property WaitingListsTitle $WaitingListsTitle
 */
class Division extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

//	/**
//	 * hasMany associations
//	 *
//	 * @var array
//	 */
//	public $hasMany = array(
//		'EmployeeDetail' => array(
//			'className' => 'EmployeeDetail',
//			'foreignKey' => 'division_id',
//			'dependent' => false,
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'exclusive' => '',
//			'finderQuery' => '',
//			'counterQuery' => ''
//		),
//		'WaitingListsTitle' => array(
//			'className' => 'WaitingListsTitle',
//			'foreignKey' => 'division_id',
//			'dependent' => false,
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'exclusive' => '',
//			'finderQuery' => '',
//			'counterQuery' => ''
//		)
//	);
	
	/**
	 * Create division
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		return $this->create_common_col_name($data);
	}

	/**
	 * Get details for given id. Its extracting data from division table only.
	 * 
	 * @param int|string $div_id Division id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update division
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common_with_col_name($data);
	}

	/**
	 * Remove division by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
	/**
	 * Get all active divisions. Its specifically used for Drop downs. 
	 * 
	 * @return array Divisions
	 */
	function get_all(){
		return $this->find('list',array(
			'conditions' => array('is_active' => IS_ACTIVE ),
			'fields' => array('id','name')
			));
	}

}
