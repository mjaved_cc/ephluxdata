<?php

App::uses('AppModel', 'Model');

/**
 * Modality Model
 *
 * @property MpdActivity $MpdActivity
 * @property VpdActivity $VpdActivity
 */
class Modality extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'MpdActivity' => array(
			'className' => 'MpdActivity',
			'foreignKey' => 'modality_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'VpdActivity' => array(
			'className' => 'VpdActivity',
			'foreignKey' => 'modality_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
	 * Create modality
	 * 
	 * @param array $data 
	 */
	function create($data) {

		return $this->create_common($data);
	}

	/**
	 * Get details for given id. Its extracting data from modality table only.
	 * 
	 * @param int|string $div_id modality id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update modality
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common($data);
	}

	/**
	 * Remove modality by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
