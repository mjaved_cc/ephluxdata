<?php

class Appointment extends AppModel {

	/**
	 * Check if appointments exists for given doctor for given interval
	 * 
	 * @param string $start_interval start time
	 * @param string $end_interval end time
	 * @param string $employee_detail_id employee detail id
	 * @return boolean
	 */
	function is_doctor_appointments_exists_between_intervals($start_interval, $end_interval, $employee_detail_id) {
		$result = $this->query('call get_doctor_appointments_count_between_intervals(
			"' . $start_interval . '",
			"' . $end_interval . '",
			"' . $employee_detail_id . '"
				)');

		return ($result[0][0]['AppointmentCount']) ? true : false;
	}

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'CallLog' => array(
			'className' => 'CallLog',
			'foreignKey' => 'appointment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AppointmentGroup' => array(
			'className' => 'AppointmentGroup',
			'foreignKey' => 'appointment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'employee_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Division' => array(
			'className' => 'Division',
			'foreignKey' => 'division_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PatientDetail' => array(
			'className' => 'PatientDetail',
			'foreignKey' => 'patient_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function update_appointment_status($data) {
		$this->query('call update_appointment_status(
			"' . $data['appointment_id'] . '",
			"' . $data['stat'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}

	function update_appointment_reschedule($data) {

		$this->query('call update_appointment_reschedule(
			"' . $data['appointment_id'] . '",
			"' . $data['stat'] . '",
			"' . $data['appoitnment_date'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}
		
		function get_available_doctors_by_service_group_id($id){
			
			return $this->query('call get_available_doctors_by_service_group_id(
			"' . $id . '")');
			
		}

	function get_appointment_for_dashboard( $data ) {

		return $this->query('call get_appointment_for_dashboard(
			"' . $data['user_type'] . '",
			"' . $data['user_id'] . '")'
		);
	}
	
	function get_messages_for_dashboard( $id ) {
		
		return $this->query( 'call get_messages_for_dashboard( "' . $id . '")' );
		
	}
	
	function get_all_future_appointsment( $user_type, $id ) {	
		
		return $this->query('call get_all_future_appointments(
			"' . $user_type . '",
			"' . $id . '")'
		);
				
	}
	
	function search_future_appointment_of_doctor($user_type, $search_keywork, $id ) {
		
		return $this->query('call search_patient_future_appoint_related_to_doctor(
			"' . $user_type . '",
			"' . $search_keywork . '",
			"' . $id . '")'
		);
		
	}

}

?>
