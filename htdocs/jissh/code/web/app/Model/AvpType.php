<?php
App::uses('AppModel', 'Model');
/**
 * AvpType Model
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class AvpType extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'is_active' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AnnualVacationPlan' => array(
			'className' => 'AnnualVacationPlan',
			'foreignKey' => 'avp_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/**
	 * Create avp type
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		return $this->create_common($data);
	}

	/**
	 * Get details for given id. Its extracting data from avp type table only.
	 * 
	 * @param int|string $div_id Division id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update avp type
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common($data);
	}

	/**
	 * Remove avp type by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
