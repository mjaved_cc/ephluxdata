<?php
App::uses('AppModel', 'Model');
/**
 * WaitingListsTitle Model
 *
 * @property Division $Division
 */
class WaitingListsTitle extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'division_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Division' => array(
			'className' => 'Division',
			'foreignKey' => 'division_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * Create waiting list
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		$this->query('call create_waiting_list_title(
			"' . $data['title'] . '",
			"' . $data['description'] . '",
			"' . $data['division_id'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['created'] . '",
				@status)');
				
		return $this->get_status();		
	}

	/**
	 * Get details for given id. Its extracting data from waiting list table only.
	 * 
	 * @param int|string $div_id waiting list id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update waiting list
	 * 
	 * @param array $data 
	 */
	function update($data) {

		$this->query('call update_waiting_list_title(
			"' . $data['title'] . '",
			"' . $data['description'] . '",
			"' . $data['division_id'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['modified'] . '",
			"' . $data['id'] . '",
				@status)');
				
		return $this->get_status();
	}

	/**
	 * Remove waiting list by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
	/**
	 * Get all waiting lists titles
	 * 
	 * @return array
	 */
	function get_all(){
		return $this->query('call get_all_waiting_list_titles()');		
	}

}
