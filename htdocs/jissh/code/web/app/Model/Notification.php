<?php

App::uses('AppModel', 'Model');

/**
 * AnnualVacationPlan Model
 *
 * @property EmployeeDetail $EmployeeDetail
 * @property AvpType $AvpType
 */
class Notification extends AppModel {
	var $useTable = false;
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
//		'employee_detail_id' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//			//'message' => 'Your custom message here',
//			//'allowEmpty' => false,
//			//'required' => false,
//			//'last' => false, // Stop validation after this rule
//			//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		
	);
	
	function get_user_emails_by_waiting_list_id($waiting_list_id) {
		return $this->query('call get_user_emails_by_waiting_list_id("'.$waiting_list_id.'")');	
	}


}
