<?php
App::uses('AppModel', 'Model');
/**
 * VpdActivity Model
 *
 * @property EmployeeDetail $EmployeeDetail
 * @property Provider $Provider
 * @property VpdActivityType $VpdActivityType
 * @property Modality $Modality
 * @property Sponsor $Sponsor
 */
class VpdActivity extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'employee_detail_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'employee_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'User.id = EmployeeDetail.user_id',
			'fields' => '',
			'order' => ''
		),
		'Provider' => array(
			'className' => 'Provider',
			'foreignKey' => 'provider_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'VpdActivityType' => array(
			'className' => 'VpdActivityType',
			'foreignKey' => 'vpd_activity_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Modality' => array(
			'className' => 'Modality',
			'foreignKey' => 'modality_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Sponsor' => array(
			'className' => 'Sponsor',
			'foreignKey' => 'sponsor_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
