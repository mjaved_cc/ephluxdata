<?php
App::uses('AppModel', 'Model');
/**
 * Provider Model
 *
 * @property VpdActivity $VpdActivity
 */
class Provider extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	/**
	 * Create provider
	 * 
	 * @param array $data 
	 */
	function create($data) {

		return $this->create_common($data);
	}

	/**
	 * Get details for given id. Its extracting data from provider table only.
	 * 
	 * @param int|string $div_id provider id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update provider
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common($data);
	}

	/**
	 * Remove provider by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
