<?php
App::uses('AppModel', 'Model');
/**
 * MpdActivityList Model
 *
 * @property MpdActivity $MpdActivity
 */
class MpdActivityList extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'is_active' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	/**
	 * Create mpd_activity_list
	 * 
	 * @param array $data 
	 */
	function create($data) {

		return $this->create_common($data);
	}

	/**
	 * Get details for given id. Its extracting data from mpd_activity_list table only.
	 * 
	 * @param int|string $div_id mpd_activity_list id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update mpd_activity_list
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common($data);
	}

	/**
	 * Remove mpd_activity_list by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
