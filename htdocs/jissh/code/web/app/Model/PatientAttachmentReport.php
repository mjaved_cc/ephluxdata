<?php
App::uses('AppModel', 'Model');
/**
 * AvpType Model
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class PatientAttachmentReport extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'appointment_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $belongsTo = array(
		'Appointment' => array(
			'className' => 'Appointment',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'caller_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);*/
	
	/**
	 * Remove avp type by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	

}
