<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * Synchronize Auth Action Map with Aco table. In case if any entry removed from Aco tables. 
	 * 
	 * @return boolean
	 */
	function sync_with_acos() {
		$this->query('call sync_aam_aco(@status)');

		return $this->get_status();
	}

	/**
	 * Get action items used in whole project. Query will return result only when auth_action_maps have is not sync with acos.
	 * 
	 * @return array actions
	 */
	function get_ctrl_actions() {

		$this->sync_with_acos();

		return $this->query('SELECT Child.* , ActionMapper.id
					FROM `acos` AS `Child` 
					LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)
					LEFT JOIN `auth_action_maps` AS `ActionMapper` ON (`ActionMapper`.`aco_id` = `Child`.`id`)
					WHERE `Parent`.`parent_id` IS NOT NULL AND ActionMapper.id IS NULL');
	}

	/**
	 * Get all action that belongs to any of CRUD operation. This mapping is stored in auth_action_maps
	 * 
	 * @return array actions
	 */
	function get_action_maps() {
		return $this->query('SELECT AuthActionMap.id, AuthActionMap.aco_id, AuthActionMap.crud, Action.id, Action.alias, 
						Controller.id, Controller.alias 
					FROM auth_action_maps AS AuthActionMap 
					LEFT JOIN acos AS Action ON (AuthActionMap.aco_id = Action.id) 
					LEFT JOIN acos AS Controller ON (Action.parent_id = Controller.id)');
	}

	/**
	 * Turn off all associations on the fly.
	 *
	 * Example: Turn off the associated Model Support request,
	 * to temporarily lighten the User model:
	 *
	 * `$this->User->unbindModelAll();`
	 */
	function unbindModelAll() {
		$unbind = array();
		foreach ($this->belongsTo as $model => $info) {
			$unbind['belongsTo'][] = $model;
		}
		foreach ($this->hasOne as $model => $info) {
			$unbind['hasOne'][] = $model;
		}
		foreach ($this->hasMany as $model => $info) {
			$unbind['hasMany'][] = $model;
		}
		foreach ($this->hasAndBelongsToMany as $model => $info) {
			$unbind['hasAndBelongsToMany'][] = $model;
		}
		parent::unbindModel($unbind);
	}
	
	function pageForPagination($model) {
		$page = 1;
		$sameModel = isset($this->params['named']['model']) && $this->params['named']['model'] == $model;
		$pageInUrl = isset($this->params['named']['page']);
		if ($sameModel && $pageInUrl) {
		  $page = $this->params['named']['page'];
		}
	
		$this->passedArgs['page'] = $page;
		
		return $page;
	}
	/**
	 * Bulk insert in one go. Cakephp saveAll method creates n INSERT query if array length is n which degrades performance.
	 * 
	 * You can also use this format to save several records with custom_saveAll(), using an array like the following:
	 * 
	 * <code>
	 * Array
	  (
	  [0] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  [1] => Array
	  (
	  [Model] => Array
	  (
	  [field1] => value1,
	  [field2] => value2
	  [fieldN] => valueN
	  )
	  )
	  )
	 * </code>
	 * 
	 * NOTE: created & modified datetime will not automatically save. and finish off with a single call to mysql_real_escape_string() while querying such as mysql_real_escape_string($json) / mysql_real_escape_string($serialize)
	 * 
	 * @param array $data it should be same as that of used in saveAll
	 * @return boolean
	 * 
	 */
	function custom_saveAll($data) {

		$first_idx = key($data);
		// check if data is not empty & is of proper format. i.e the one use in saveAll
		if (count($data) > 0 && is_numeric($first_idx)) {
			$value_array = array();

			// table & name defined in lib/Cake/Model/Model.php
			$table_name = $this->table;
			$model_name = $this->name;

			$fields = array_keys($data[$first_idx][$model_name]);

			foreach ($data as $key => $value)
				$value_array[] = "('" . implode('\',\'', $value[$model_name]) . "')";


			$sql = "INSERT INTO " . $table_name . " (" . implode(', ', $fields) . ") VALUES " . implode(',', $value_array);

			$this->query($sql);
			return true;
		}

		return false;
	}

	/**
	 * For debugging cakephp queries
	 * 
	 * @return string SQL-query
	 */
	function get_last_query() {
		$dbo = $this->getDatasource();
		$logs = $dbo->getLog();
		$lastLog = end($logs['log']);
		return $lastLog['query'];
	}

	/**
	 * Get status of stored procedures
	 * 
	 * @return string|int rows affected by insert/update/delete
	 */
	function get_status() {
		$status = $this->query('SELECT @status');
		return ($status['0']['0']['@status'] > 0) ? $status['0']['0']['@status'] : false;
	}

	/**
	 * Retrieve results for given table name & id having same schema such as divisions , avp_types etc
	 * 
	 * @param int|string $id primary key of given table
	 */
	function get_common_by_common_id($id) {

		$result = $this->query('call get_common_by_common_id(
			"' . $id . '",
			"' . $this->table . '",
			"' . $this->name . '"			
				)');

		return $result[0];
	}

	/**
	 * Update results for given table name & id having same schema such as divisions , avp_types etc
	 * 
	 * @param array $data id & name
	 */
	function update_common($data) {

		$this->query('call update_common(
			"' . $this->table . '",
			"' . $data['id'] . '",
			"' . $data['name'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}

	/**
	 * Update records for given table_name having same schema such as divisions , countries etc
	 * 
	 * These tables include description|nationality field too.
	 * 
	 * @param array $data
	 */
	function update_common_with_col_name($data) {

		// in case of failure returns boolean
		$col_name = $this->_get_required_col_name($data);

		if (is_string($col_name)) {
			$this->query('call update_common_with_col_name(
			"' . $this->table . '",
			"' . $col_name . '",
			"' . $data['id'] . '",
			"' . $data['name'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['modified'] . '",
			"' . $data[$col_name] . '",
				@status)');
		}
		return $this->get_status();
	}

	/**
	 * Create new records for given table_name & id having same schema such as divisions , avp_types etc
	 * 
	 * @param array $data
	 */
	function create_common($data) {

		$this->query('call create_common(
			"' . $this->table . '",
			"' . $data['name'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['created'] . '",
			@status)');
		return $this->get_status();
	}

	/**
	 * Create new records for given table_name having same schema such as divisions , countries etc
	 * 
	 * These tables include description|nationality field too.
	 * 
	 * @param array $data
	 */
	 
	function create_common_col_name($data) {
		
		// in case of failure returns boolean
		$col_name = $this->_get_required_col_name($data);
		
		if (is_string($col_name)) {
			$this->query('call create_common_with_col_name(
			"' . $this->table . '",
			"`' . $col_name . '`",
			"' . $data['name'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['created'] . '",
			"' . $data[$col_name] . '",
			@status)');
		}
		return $this->get_status();
	}

	/**
	 * Get column name either description or nationality . Tables such as divisions , countries has same schema but only 
	 * 
	 * exception is divisions contains description field where as countries contains nationality
	 * 
	 * @param array $data
	 * @return TRUE column name on success or FALSE on failure.
	 */
	private function _get_required_col_name($data) {

		$col_name_nationality = 'nationality';
		$col_name_desc = 'description';
		$col_name_value = 'value';
		if (array_key_exists($col_name_nationality, $data))
			return $col_name_nationality;
		else if (array_key_exists($col_name_desc, $data))
			return $col_name_desc;
		else if (array_key_exists($col_name_value, $data))
			return $col_name_value;
			
		return false;
	}

	/**
	 * Remove records by updating is_active 0 for given table_name & id (primary key).
	 * 
	 * @param array $data
	 */
	function remove_common($data) {

		$this->query('call remove_common(
			"' . $this->table . '",
			"' . $data['id'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}

	/**
	 * Remove all records by updating is_active 0 based on field name.
	 * 
	 * @param array $data
	 */
	function remove_all_common_by_field_name($data) {

		$this->query('call remove_all_common_by_field_name(
			"' . $this->table . '",
			"' . $data['field_value'] . '",
			"' . $data['modified'] . '",
			"' . $data['field_name'] . '",
				@status)');
		return $this->get_status();
	}

	/**
	 * Get all active records for tables like divisions , official titles etc
	 *
	 * @return mixed result	 
	 */
	function get_all_common() {

		return $this->query('call get_all_common(
			"' . $this->table . '",
			"' . $this->name . '"
				)');
	}

	/**
	 * Format DB result set with respect to drop down.
	 * 
	 * Will fetch result from DB with respect to instance its called.
	 * 
	 * Also it uses Datatypes (data rendering logic) too after retreiving result from DB.
	 * 
	 * <code>
	 * // will fetch result from language table & format it with drop down
	 * // this thing works fine if your table schema is similar to Language , Country i.e containing id , name fields
	 * 
	 * $this->Language->format_drop_down() 
	 * </code>
	 *
	 * <code>
	 * // in case if your table does not follow above mentioned schema then retireve result as id & name keys & pass it 
	 * // in return will format you drop down
	 * // here is sample code used in this project
	 * 
	 * $this->WaitingListsTitle->format_drop_down($this->WaitingListsTitleget_all()) 
	 * </code>
	 * 
	 * @param array $data Data with id & name as keys
	 * @uses get_all_common
	 */
	function format_drop_down($data = null) {
		
		if (empty($data))
			$data = $this->get_all_common();
		
		$return_data = array();

		if (!empty($data)) {
			foreach ($data as $value) {
				$class_name = PREFIX_DATATYPE_CLASSES . $this->name ;
				$obj_class_name = new $class_name($value[$this->name]);
				
				$return_data[$obj_class_name->id] = $obj_class_name->name ;				
			}
		}
		return $return_data;
	}

}
