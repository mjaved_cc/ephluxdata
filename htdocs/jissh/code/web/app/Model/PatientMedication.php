<?php
App::uses('AppModel', 'Model');
/**
 * PatientMedication Model
 *
 
 */
class PatientMedication extends AppModel {
	
	public $belongsTo = array(
		'PatientDetail' => array(
			'className' => 'PatientDetail',
			'foreignKey' => 'patient_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'PatientDetail.user_id=User.id',
			'fields' => '',
			'order' => ''
		),
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'employee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Appointment' => array(
			'className' => 'Appointment',
			'foreignKey' => 'appointment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'PatientMedicationDetail' => array(
			'className' => 'PatientMedicationDetail',
			'foreignKey' => 'patient_medication_id',
			'conditions' => '',
			'fields' => '',
			'order' => 'PatientMedicationDetail.attribute_order asc'
		)
	);
	
	/**
	 * Get details for given id. Its extracting data from patient_medications table only.
	 * 
	 * @param int|string $id patient medication id
	 */
	function get_by_id($id) {
		return $this->get_common_by_common_id($id);
	}


	/**
	 * Remove patient medication by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
