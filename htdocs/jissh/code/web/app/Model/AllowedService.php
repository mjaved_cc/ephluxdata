<?php
App::uses('AppModel', 'Model');
/**
 * AllowedService Model
 *
 * @property AllowedServiceGroup $AllowedServiceGroup
 * @property EmployeeService $EmployeeService
 */
class AllowedService extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'allowed_service_group_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AllowedServiceGroup' => array(
			'className' => 'AllowedServiceGroup',
			'foreignKey' => 'allowed_service_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'EmployeeService' => array(
			'className' => 'EmployeeService',
			'foreignKey' => 'allowed_service_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/**
	 * Create allowed service
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		$this->query('call create_allowed_services(
			"' . $data['name'] . '",
			"' . $data['description'] . '",
			"' . $data['allowed_service_group_id'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['created'] . '",
				@status)');
				
		return $this->get_status();		
	}

	/**
	 * Get details for given id. Its extracting data from allowed service table only.
	 * 
	 * @param int|string $div_id allowed service id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update allowed service
	 * 
	 * @param array $data 
	 */
	function update($data) {

		$this->query('call update_allowed_services(
			"' . $data['name'] . '",
			"' . $data['description'] . '",
			"' . $data['allowed_service_group_id'] . '",
			"' . $data['ip_address'] . '",
			"' . $data['modified'] . '",
			"' . $data['id'] . '",
				@status)');
				
		return $this->get_status();
	}

	/**
	 * Remove allowed service by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
