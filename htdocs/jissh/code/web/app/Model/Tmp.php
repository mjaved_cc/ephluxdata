<?php

class Tmp extends AppModel {

	public $name = 'Tmp';

	/**
	 * Check this email already exist in tmp table or not
	 * @param email $email Email address
	 * @return int no of records
	 */
	function is_unverified_email_exists($email) {

//        $is_unverified_exist = $this->find('count', array(
//			'conditions' => array('type' => 'registration', 'target_id' => $email)
//				));		

		$is_unverified_exist = $this->query('call is_unverified_email_exists("' . $email . '")');

		return $is_unverified_exist;
	}

	/**
	 * get record against the parameters $target_id and $type from tmp table
	 * @param string $target_id, string $type
	 * @return record
	 */
	function get_details($target_id, $type) {
		return $this->find('first', array(
					'conditions' => array(
						'target_id' => $target_id,
						'type' => $type
					)
				));
	}

	function get_user_tmp_data($target_id, $type) {
		$result = $this->query('call get_user_tmp_data("' . $target_id . '", "' . $type . '")');
		if (!empty($result))
			return $result['0'];

		return false;
	}
	
	/**
	 * Remove tmp records by id.
	 * 
	 * @param string|int $id
	 * @return int|string rows affected
	 */
	function remove_by_id($id) {
		$this->query('call remove_tmp_record_by_id(
			"' . $id . '",
			@status)');
		return $this->get_status();
	}
	
	/**
	 * Remove tmp records by target id.
	 * 
	 * @param string|int $target_id
	 * @return int|string rows affected
	 */
	function remove_by_target_id($target_id) {
		$this->query('call remove_tmp_record_by_target_id(
			"' . $target_id . '",
			@status)');
		return $this->get_status();
	}
	
	function save_tmp_record($target_id, $type, $data, $expire_at, $created) {
		$this->query('call save_tmp_record("' . $target_id . '", "' . $type . '", "' . $data . '", "' . $expire_at . '", "' . $created . '", @status)');
		return $this->get_status();
	}

}