<?php
App::uses('AppModel', 'Model');
/**
 * AvpType Model
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class EmailQueue extends AppModel {

	var $useTable = 'email_queue';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belongsTo = array(
		'EmailTemplate' => array(
			'className' => 'EmailTemplate',
			'foreignKey' => 'template_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	

}
