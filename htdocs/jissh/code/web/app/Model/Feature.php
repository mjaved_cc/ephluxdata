<?php

App::uses('AppModel', 'Model');

/**
 * Feature Model
 *
 * @property AuthActionMap $AuthActionMap
 */
class Feature extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'AuthActionMap' => array(
			'className' => 'AuthActionMap',
			'foreignKey' => 'feature_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
	 * Get details for given id. It will fetch result by joining with its relevant tables.
	 * 
	 * @param int|string $feature_id Feature id
	 */
	function get_details_by_id($feature_id) {
		$result = $this->query('call get_feature_details_by_feature_id("' . $feature_id . '")');

		if (!empty($result))
			return $result;
		return false;
	}

	/**
	 * Get details for given id. Its extracting data from features table only.
	 * 
	 * @param int|string $feature_id Feature id
	 */
	function get_by_id($feature_id) {
		$result = $this->query('call get_feature_by_feature_id("' . $feature_id . '")');

		if (!empty($result))
			return $result[0];
		return false;
	}

	/**
	 * Update feature
	 * 
	 * @param array $data id & description
	 */
	function update($data) {

		$this->query('call update_feature(
			"' . $data['id'] . '",
			"' . $data['description'] . '",
			"' . $data['modified'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}

	/**
	 * Create feature
	 * 
	 * @param array $data
	 */
	function create($data) {

		$this->query('call create_feature(
			"' . $data['description'] . '",
			"' . $data['created'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}
	
	/**
	 * Remove feature by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		$this->query('call remove_feature(
			"' . $data['id'] . '",
			"' . $data['modified'] . '",
			"' . $data['ip_address'] . '",
				@status)');
		return $this->get_status();
	}

}
