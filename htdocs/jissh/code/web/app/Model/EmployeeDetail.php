<?php

App::uses('AppModel', 'Model');

/**
 * EmployeeDetail Model
 *
 * @property User $User
 * @property LicenceType $LicenceType
 * @property OfficialTitle $OfficialTitle
 * @property AnnualVacationPlan $AnnualVacationPlan
 * @property AppointmentGroup $AppointmentGroup
 * @property Appointment $Appointment
 * @property EmployeeCaseLoad $EmployeeCaseLoad
 * @property EmployeeDivision $EmployeeDivision
 * @property EmployeeLevel $EmployeeLevel
 * @property EmployeeService $EmployeeService
 * @property EmployeeWorkingHour $EmployeeWorkingHour
 * @property MpdActivity $MpdActivity
 * @property VpdActivity $VpdActivity
 */
class EmployeeDetail extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'first_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is required field'
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'last_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is required field'
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'joining_date' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is required field'
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contract_expiry_date' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is required field'
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => array('id', 'username', 'user_type', 'group_id', 'created', 'status'),
			'order' => ''
		)
	);

	/**
	 * If employee exists 
	 * 
	 * @param string|int $id Employee Detail Id
	 * @return void
	 */
	function is_exists($id) {
		$result = $this->query('call is_employee_exists("' . $id . '")');
		return $result[0][0]['IsExists'];
	}

	/**
	 * Get available doctors by no of weeks & waiting list title it
	 * 
	 * @param strin|int $waiting_list_title_id Waiting list title id
	 * @param strin|int $no_of_weeks No of weeks	 
	 */
	function get_available_doctors_by_weeks_and_wlist($waiting_list_title_id, $no_of_weeks) {
		return $this->query('call get_available_doctors_lists_for_given_weeks_and_wlist(
			"' . $waiting_list_title_id . '",
			"' . $no_of_weeks . '"
				)');
	}

	/**
	 * If employee is on leave
	 * 
	 * @param string|int $employee_detail_id Employee Detail Id
	 * @param string $date_as_appointment Date
	 */
	function is_on_leave($employee_detail_id, $date_as_appointment) {
		$result = $this->query('call is_doctor_on_leave(
			"' . $employee_detail_id . '",
			"' . $date_as_appointment . '"
				)');
		
		if(!empty($result)){
			return $result[0]['AnnualVacationPlan']['id'] ;
		}
		
		return false ;
	}
	
	/**
	 * If employee/doctor consult division against given waiting list for given working day
	 * 
	 * @param array $data params	 
	 */
	function is_doctor_consult_division_for_given_working_day($data) {
		$result = $this->query('call is_doctor_consult_division_for_given_working_day(
			"' . $data['employee_detail_id'] . '",
			"' . $data['waiting_list_title_id'] . '",
			"' . $data['working_day'] . '"
				)');
		
		if(!empty($result)){
			return $result[0]['EmployeeWorkingHour']['id'] ;
		}
		
		return false ;
	}

}
