<?php

App::uses('AppModel', 'Model');

/**
 * AnnualVacationPlan Model
 *
 * @property EmployeeDetail $EmployeeDetail
 * @property AvpType $AvpType
 */
class AnnualVacationPlan extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
//		'employee_detail_id' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//			//'message' => 'Your custom message here',
//			//'allowEmpty' => false,
//			//'required' => false,
//			//'last' => false, // Stop validation after this rule
//			//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		'last_work_day' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
			)
		),
		'report_to_work' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
			),
			'rule2' => array(
				'rule' => array('is_greator_than_last_work_day'),
				'message' => 'Should be greator than Last Work Day'
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'employee_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AvpType' => array(
			'className' => 'AvpType',
			'foreignKey' => 'avp_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * Check if report_to_work is greator than last_work_day	 * 
	 * 
	 */
	public function is_greator_than_last_work_day($check) {
		$report_to_work_unix = CakeTime::toUnix($check['report_to_work']);
		$last_work_day_unix = CakeTime::toUnix($this->data[$this->alias]['last_work_day']);

		if ($report_to_work_unix < $last_work_day_unix) {
			return false;
		} else
			return true;
	}

	/**
	 * Check if leaves exists for given doctor for given interval
	 * 
	 * @param string $start_interval start time
	 * @param string $end_interval end time
	 * @param string $employee_detail_id employee detail id
	 * @return boolean
	 */
	function is_doctor_leaves_exists_between_intervals($start_interval, $end_interval, $employee_detail_id) {
		$result = $this->query('call get_doctor_leaves_count_between_intervals(
			"' . $start_interval . '",
			"' . $end_interval . '",
			"' . $employee_detail_id . '"
				)');

		return ($result[0][0]['LeavesCount']) ? true : false;
	}

	/**
	 * Create feature
	 * 
	 * @param array $data
	 */
	function create($data) {

		$this->query('call create_annual_vacation_plan(
			"' . $data['employee_detail_id'] . '",
			"' . $data['avp_type_id'] . '",
			"' . $data['description'] . '",
			"' . $data['last_work_day'] . '",
			"' . $data['report_to_work'] . '",
			"' . $data['created'] . '",
				@status)');
		return $this->get_status();
	}
	
	/**
	 * Get details for given id. Its extracting data from features table only.
	 * 
	 * @param int|string $avp_id Annual Vacation id
	 */
	function get_details_by_id($avp_id){
		$result = $this->query('call get_annual_vacation_plan_by_id(' . $avp_id . ')');
		return $result[0];
	}
	
	/**
	 * Get patient emails related to the employee appointment.
	 * 
	 * @param int|string $emp_id Employee_detail_id
	 */
	function get_patient_emails_by_avp_emp_id($emp_id){
		$result = $this->query('call get_patient_emails_by_avp_emp_id(' . $emp_id . ')');
		return $result[0];
	}
	
//	function update($data){
//		$this->query('call create_annual_vacation_plan(
//			"' . $data['employee_detail_id'] . '",
//			"' . $data['avp_type_id'] . '",
//			"' . $data['description'] . '",
//			"' . $data['last_work_day'] . '",
//			"' . $data['report_to_work'] . '",
//			"' . $data['created'] . '",
//				@status)');
//		return $this->get_status();
//	}

}
