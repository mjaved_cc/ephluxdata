<?php
App::uses('AppModel', 'Model');
/**
 * Template Model
 *
 
 */
class Template extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'template_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'division_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public $belongsTo = array(
		'Division' => array(
			'className' => 'Division',
			'foreignKey' => 'division_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $hasMany = array(
		'TemplateAttributesItem' => array(
			'className' => 'TemplateAttributesItem',
			'foreignKey' => 'template_id',
			'conditions' => '',
			'fields' => '',
			'order' => 'TemplateAttributesItem.order asc',
			'contain'=>array(       
				'TemplateAttribute'
			)
		)
	);
	
	/**
	 * Get details for given id. Its extracting data from allowed service table only.
	 * 
	 * @param int|string $id template id
	 */
	function get_by_id($id) {
		return $this->get_common_by_common_id($id);
	}


	/**
	 * Remove template by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {
		return $this->remove_common($data);
	}
	
}
