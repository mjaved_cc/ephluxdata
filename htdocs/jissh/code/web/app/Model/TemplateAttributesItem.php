<?php
App::uses('AppModel', 'Model');
/**
 * TemplateAttributesItem Model
 *
 
 */
class TemplateAttributesItem extends AppModel {

	public $belongsTo = array(
		'Template' => array(
			'className' => 'Template',
			'foreignKey' => 'template_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TemplateAttribute' => array(
			'className' => 'TemplateAttribute',
			'foreignKey' => 'template_attribute_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * Get details for given id. Its extracting data from TemplateAttributesItem table only.
	 * 
	 * @param int|string $id TemplateAttributesItem id
	 */
	function get_by_id($id) {
		return $this->get_common_by_common_id($id);
	}


	/**
	 * Remove TemplateAttributesItem by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
