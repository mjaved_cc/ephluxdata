<?php
App::uses('AppModel', 'Model');
/**
 * AllowedService Model
 *
 * @property AllowedServiceGroup $AllowedServiceGroup
 * @property EmployeeService $EmployeeService
 */
class PatientWaitingList extends AppModel {
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $hasMany = array(
		'PatientDetail' => array(
			'className' => 'PatientDetail',
			'foreignKey' => 'patient_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'WaitingListsTitle' => array(
			'className' => 'WaitingListsTitle',
			'foreignKey' => 'waiting_list_title_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
}
