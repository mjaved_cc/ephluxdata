<?php
App::uses('AppModel', 'Model');
/**
 * CaseLoad Model
 *
 * @property Employee $Employee
 */
class CaseLoad extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
//	public $hasAndBelongsToMany = array(
//		'Employee' => array(
//			'className' => 'Employee',
//			'joinTable' => 'employee_case_loads',
//			'foreignKey' => 'case_load_id',
//			'associationForeignKey' => 'employee_id',
//			'unique' => 'keepExisting',
//			'conditions' => '',
//			'fields' => '',
//			'order' => '',
//			'limit' => '',
//			'offset' => '',
//			'finderQuery' => '',
//			'deleteQuery' => '',
//			'insertQuery' => ''
//		)
//	);
	
	/**
	 * Create caseload
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		return $this->create_common_col_name($data);
	}

	/**
	 * Get details for given id. Its extracting data from caseload table only.
	 * 
	 * @param int|string $div_id caseload id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update caseload
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common_with_col_name($data);
	}

	/**
	 * Remove caseload by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
