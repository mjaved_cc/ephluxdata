<?php

App::uses('AppModel', 'Model');

/**
 * ConfigOption Model
 *
 */
class ConfigOption extends AppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'value' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	/**
	 * Create division
	 * 
	 * @param array $data 
	 */
	function create($data) {

		return $this->create_common_col_name($data);
	}

	/**
	 * Get details for given id. Its extracting data from division table only.
	 * 
	 * @param int|string $div_id Division id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update division
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common_with_col_name($data);
	}

	/**
	 * Remove division by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

	/**
	 * Get value by name
	 * 
	 * @param string $name key
	 * @return mixed 
	 */
	function get_value_by_name($name) {
		$result = $this->query('call get_config_option_by_name("' . $name . '")');

		if (!empty($result))
			return unserialize ($result[0][$this->alias]['value']);
		
		return false ;
	}

}
