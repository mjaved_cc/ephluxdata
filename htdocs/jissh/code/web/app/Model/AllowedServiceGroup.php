<?php
App::uses('AppModel', 'Model');
/**
 * AllowedServiceGroup Model
 *
 * @property AllowedService $AllowedService
 */
class AllowedServiceGroup extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AllowedService' => array(
			'className' => 'AllowedService',
			'foreignKey' => 'allowed_service_group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/**
	 * Create allowed service group
	 * 
	 * @param array $data 
	 */	
	function create($data) {

		return $this->create_common_col_name($data);
	}

	/**
	 * Get details for given id. Its extracting data from allowed service group table only.
	 * 
	 * @param int|string $div_id Division id
	 */
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}

	/**
	 * Update allowed service group
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common_with_col_name($data);
	}

	/**
	 * Remove allowed service group by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}

}
