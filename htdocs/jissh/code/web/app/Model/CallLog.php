<?php
App::uses('AppModel', 'Model');
/**
 * AvpType Model
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class CallLog extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'appointment_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Appointment' => array(
			'className' => 'Appointment',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'caller_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
	/**
	 * Remove avp type by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
	public function getCallLogs($appointment_id=0){
	
		$page = $this->pageForPagination('CallLog');
		$this->paginate = array('fields'=>array('CallLog.*'), 
										'limit'=>1,
										'order' => array(
											'CallLog.id' => 'desc'
										),
										'page' => $page,
										'conditions' => array('CallLog.appointment_id' => $appointment_id));
										
		$data = $this->paginate();
		
		if(!empty($data)) {
			foreach($data as $key => $calllog){
				$data[$key] = $calllog;
				$emp = $this->EmployeeDetail->find('first', array(
					'conditions' => array('EmployeeDetail.id' => $calllog['CallLog']['caller_id'])
				));
				
				if(!empty($emp)) {
					$data[$key]['EmployeeDetail'] = $emp['EmployeeDetail'];
				}
				else {
					$data[$key]['EmployeeDetail'] = array();
				}
			}
		}
		
		return $data;
		
	}

}
