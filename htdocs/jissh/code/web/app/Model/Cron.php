<?php
App::uses('AppModel', 'Model');
/**
 * AvpType Model
 *
 * @property AnnualVacationPlan $AnnualVacationPlan
 */
class Cron extends AppModel {
	var $useTable = false;
	function get_email_queue_for_cron() {
		return $this->query('call get_email_queue_for_cron()');	
	}
	
	function remove_email_templates() {
		return $this->query('call remove_email_templates()');	
	}
	
	function get_emails_before_day_appointment() {
		return $this->query('call get_emails_before_day_appointment()');	
	}
}
