<?php
App::uses('AppModel', 'Model');
/**
 * PatientMedicationDetails Model
 *
 
 */
class PatientMedicationDetail extends AppModel {
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'attribute_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'attribute_value' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'attribute_order' => array(
			'boolean' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public $belongsTo = array(
		'PatientMedication' => array(
			'className' => 'PatientMedication',
			'foreignKey' => 'patient_medication_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * Get details for given id. Its extracting data from allowed service table only.
	 * 
	 * @param int|string $id medication detail id
	 */
	function get_by_id($id) {
		return $this->get_common_by_common_id($id);
	}


	/**
	 * Remove medication by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}
	
}
