<?php
App::uses('AppModel', 'Model');
/**
 * Sponsor Model
 *
 * @property VpdActivity $VpdActivity
 */
class Sponsor extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This is requried field',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'is_active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'VpdActivity' => array(
			'className' => 'VpdActivity',
			'foreignKey' => 'sponsor_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/**
	 * Create Sponsor
	 * 
	 * @param array $data 
	 */
	function create($data) {

		return $this->create_common($data);
	}
	
	function get_by_id($div_id) {
		return $this->get_common_by_common_id($div_id);
	}
	
	/**
	 * Update Sponsor
	 * 
	 * @param array $data 
	 */
	function update($data) {

		return $this->update_common($data);
	}
	
	/**
	 * Remove Sponsor by updating is_active = 0
	 * 
	 * @param array $data
	 */
	function remove($data) {

		return $this->remove_common($data);
	}


}
