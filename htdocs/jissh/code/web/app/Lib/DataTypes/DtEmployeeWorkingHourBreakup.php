<?php

class DtEmployeeWorkingHourBreakup {

	private $_fields = array();
	private $_allowed_keys = array('id','start_time','end_time','created');

	public function __construct($data = null) {
		
		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'start_time':
			case 'end_time':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (in_array($key, array('start_time','end_time'))){ 
				return $this->_fields[$key]->dayAsSql();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'start_time' => $this->start_time,
			'end_time' => $this->end_time,
			'time_span' => $this->get_time_span(),
			'created' => $this->created
		);
	}
	
	/**
	 * A textual representation of a day, three letters
	 * 
	 * @param type $name Description
	 */
	function get_three_letter_day_representation(){
		return strtolower(date('D' , $this->start_time)) ;
	}
	
	function get_time_span(){
		return self::get_formatted_time($this->start_time) . ' - ' . self::get_formatted_time($this->end_time) ;
	}
	
	static function get_formatted_time($mysql_date_time){
		return date('H:i:s' , DtDate::toUnix($mysql_date_time));
	}
}

?>
