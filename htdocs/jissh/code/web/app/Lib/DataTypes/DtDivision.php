<?php

class DtDivision {

	private $_fields = array();
	private $_allowed_keys = array('id', 'name', 'description', 'created', 'is_active');

	public function __construct($data = null) {
		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if($key == 'name')
				return $this->_get_formatted_name();
			else if($key == 'description')
				return $this->_get_desc();
			else if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'name' => $this->name,
			'description' => $this->_get_desc(),
			'is_active' => $this->is_active,
			'created' => $this->created
		);
	}

	private function _get_desc() {
		return (isset($this->_fields['description']) && $this->_fields['description'] !== '0') ? $this->_fields['description'] : 'Not provided';
	}
	
	private function _get_formatted_name() {
		return ($this->_fields['name'] !== '0') ? ucfirst($this->_fields['name']) : 'Not provided';
	}

	/**
	 * Add single employee_working_hour_breakup to object
	 * 
	 * @param array $employee_working_hour_breakup 
	 */
	function add_employee_working_hour_breakup($employee_working_hour_breakup) {

		if (!empty($employee_working_hour_breakup['id'])) {
			if (isset($this->_fields["EmployeeWorkingHourBreakup"]) && !is_array($this->_fields["EmployeeWorkingHourBreakup"])) {
				$prev_value = $this->EmployeeWorkingHourBreakup; // restore previous value
				unset($this->_fields["EmployeeWorkingHourBreakup"]); // now unset it so that we can convert object to array
				$this->_fields["EmployeeWorkingHourBreakup"][$prev_value->id] = $prev_value;
				$this->_fields["EmployeeWorkingHourBreakup"][$employee_working_hour_breakup['id']] = new DtEmployeeWorkingHourBreakup($employee_working_hour_breakup);
			} else if (isset($this->_fields["EmployeeWorkingHourBreakup"]) && is_array($this->_fields["EmployeeWorkingHourBreakup"]))
				$this->_fields["EmployeeWorkingHourBreakup"][$employee_working_hour_breakup['id']] = new DtEmployeeWorkingHourBreakup($employee_working_hour_breakup);
			else
				$this->_fields["EmployeeWorkingHourBreakup"] = new DtEmployeeWorkingHourBreakup($employee_working_hour_breakup);
		}
	}

	/**
	 * Add multiple employee_working_hour_breakup to object
	 * 
	 * @param array $employee_working_hour_breakups
	 */
	function add_employee_working_hour_breakups($employee_working_hour_breakups) {
		if (!empty($employee_working_hour_breakups)) {
			foreach ($employee_working_hour_breakups as $value)
				$this->add_employee_working_hour_breakup($value);
		}
	}

	/**
	 * Get employee_working_hour_breakup object at given row 
	 * 
	 * @return object
	 */
	function get_employee_working_hour_breakup_obj_at_row($key) {

		if (empty($key))
			return false;

		if (is_array($this->_fields['EmployeeWorkingHourBreakup']) &&
				$this->_fields['EmployeeWorkingHourBreakup'][$key] instanceof DtEmployeeWorkingHourBreakup) {
			return $this->_fields['EmployeeWorkingHourBreakup'][$key];
		} else if ($this->_fields['EmployeeWorkingHourBreakup'] instanceof DtEmployeeWorkingHourBreakup) {
			return $this->_fields['EmployeeWorkingHourBreakup'];
		}

		return false;
	}

}

?>
