<?php
class DtConversation {

	private $_fields = array();
	public $myKey='';
	
	public function __construct($data = null) {
		//debug($data);	

		$this->listArrayRecursive($data);
		//pr($this->_fields);
		//$this->populate_data($data);
	}
	
	function listArrayRecursive($data,$nkey = ''){
		foreach($data as $key => $value) {
			foreach($value as $k=>$val) {
				if($k=="Conversation" || $k=="LastMessage" || $k=="inbox" || $k=="User") {
					$this->processConversation($val,$key,$k);	
				}
				else {
					$this->processConversations($val,$key,$k);
				}
			}
		}
	}
	
	function processConversation($value,$key,$subkey){
		$i=0;
		foreach($value as $k=>$val) {
			
			//echo $key.' - '.$subkey.' - '.$k.' - '.$val;echo '<br>';
			switch ($k) {
				case 'created';
				case 'modified';
					$this->_fields[$key][$subkey][$k] = new DtDate($val);
					break;
				
				case 'message':
					$content = new DtContent($val);
					$this->_fields[$key][$subkey][$k] = $content->textConent();
					break;
					
				default:
					$this->_fields[$key][$subkey][$k] = $val;
			}
				
			
		}$i++;
	}
	
	function processConversations($value,$key,$subkey){
		$i=0;
		foreach($value as $k=>$val) {
			foreach($val as $kk => $vv) {
				//echo $key.' - '.$subkey.' - '.$k.' - '.$kk.' - '.$vv;echo '<br>';
			
				switch ($kk) {
					case 'created';
					case 'modified';
						$this->_fields[$key][$subkey][$k][$kk] = new DtDate($vv);
						break;
					
					case 'message':
						$this->_fields[$key][$subkey][$k][$kk] = 'tahir message';
						break;
						
					default:
						$this->_fields[$key][$subkey][$k][$kk] = $vv;
				}
			}	
			
		}$i++;
	}

	function __set($key, $value) {
		debug($key); exit;
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			
			case 'message':
				$this->_fields[$key] = 'tahir message';
				break;
				
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key];//->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			$this->_controller->log($e->getMessage(), 'debug');
		}
	}

	function get() {

		return $this->_fields;
	}

	/**
	 * Add single card to user object
	 * 
	 * @param array $user_social_data user social information
	 */
	function add_user_social_account($user_social_data) {
		if (isset($this->_fields["UserSocialAccount"]) && !is_array($this->_fields["UserSocialAccount"])) {
			$prev_value = $this->UserSocialAccount; // restore previous value
			unset($this->_fields["UserSocialAccount"]); // now unset it so that we can convert object to array
			$this->_fields["UserSocialAccount"][] = $prev_value;
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		} else if (isset($this->_fields["UserSocialAccount"]) && is_array($this->_fields["UserSocialAccount"]))
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		else
			$this->_fields["UserSocialAccount"] = new DtUserSocialAccount($user_social_data);
	}

	/**
	 * Add multiple cards to user object
	 * 
	 * @param array $cards card information
	 */
	function add_user_social_accounts($user_social_data) {
		if (!empty($user_social_data)) {
			foreach ($user_social_data as $value)
				$this->add_user_social_account($value);
		}
	}

	function get_field() {
		
		return array(
			'id' => $this->id,
			'first_name' => $this->first_name,
			'first_name' => $this->first_name,
			'username' => $this->username,
			'created' => $this->created,
			'status' => $this->status,
			'group_id' => $this->group_id
		);
	}

}