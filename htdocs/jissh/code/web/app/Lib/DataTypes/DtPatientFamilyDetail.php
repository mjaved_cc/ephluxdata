<?php

class DtPatientFamilyDetail {

	private $_fields = array();
	private $_allowed_keys = array('id', 'patient_detail_id', 'name','email', 'work_number', 'mobile_number', 'occupation', 'relation_ship_id', 'is_active','created','modified');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			}
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'patient_detail_id' => $this->patient_detail_id,
			'name' => $this->name,
			'email' => $this->email,
			'work_number' => $this->work_number,
			'mobile_number' => $this->mobile_number,
			'occupation' => $this->occupation,
			'relation_ship_id' => $this->relation_ship_id,
			'is_active' => $this->is_active,
			'created' => $this->created
		);
	}
	
}

?>
