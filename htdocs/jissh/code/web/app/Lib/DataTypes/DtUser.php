<?php

class DtUser {

	private $_fields = array();
	private $_allowed_keys = array('id', 'username', 'user_type', 'group_id', 'created', 'status');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				if(!isset($this->_fields[$key])) return "";
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	/**
	 * Add employee to user object
	 * 
	 * @param array $employee_data employee information
	 */
	function add_employee_detail($employee_data) {

		$this->_fields["EmployeeDetail"] = new DtEmployeeDetail($employee_data);
	}

	/**
	 * Get employee details
	 * 
	 * @return array
	 */
	function get_employee_detail() {

		if (isset($this->_fields['EmployeeDetail']) && $this->_fields['EmployeeDetail'] instanceof DtEmployeeDetail)
			return $this->_fields['EmployeeDetail']->get_field();
	}

	/**
	 * Add patient to user object
	 * 
	 * @param array $patient_data patient information
	 */
	function add_patient_detail($patient_data) {

		$this->_fields["PatientDetail"] = new DtPatientDetail($patient_data);
	}

	/**
	 * Get patient details
	 * 
	 * @return array
	 */
	function get_patient_detail() {

		if (isset($this->_fields['PatientDetail']) && $this->_fields['PatientDetail'] instanceof DtPatientDetail)
			return $this->_fields['PatientDetail']->get_field();
	}

	/**
	 * Add single language to user object
	 * 
	 * @param array $language_data user language information
	 */
	function add_language($language_data) {
		if (isset($this->_fields["Language"]) && !is_array($this->_fields["Language"])) {
			$prev_value = $this->Language; // restore previous value
			unset($this->_fields["Language"]); // now unset it so that we can convert object to array
			$this->_fields["Language"][] = $prev_value;
			$this->_fields["Language"][] = new DtLanguage($language_data);
		} else if (isset($this->_fields["Language"]) && is_array($this->_fields["Language"]))
			$this->_fields["Language"][] = new DtLanguage($language_data);
		else
			$this->_fields["Language"] = new DtLanguage($language_data);
	}

	/**
	 * Add multiple languages to user object
	 * 
	 * @param array $language_data user language information
	 */
	function add_languages($language_data) {
		if (!empty($language_data)) {
			foreach ($language_data as $value)
				$this->add_language($value);
		}
	}

	/**
	 * Get language
	 * 
	 * @return array
	 */
	function get_languages() {
		$data = array();
		if (is_array($this->_fields['Language'])) {
			foreach ($this->_fields['Language'] as $language) {
				$data[$language->id] = $language->get_field();
			}
		} else if ($this->_fields['Language'] instanceof DtLanguage) {
			$data[$this->_fields['Language']->id] = $this->_fields['Language']->get_field();
		}

		return $data;
	}

	function get_field() {
		
		return array(
			'id' => $this->id,
			'username' => $this->username,
			'user_type' => $this->user_type,
			'group_id' => $this->group_id,
			'created' => $this->created,
			'status' => $this->status
		);
	}

}