<?php

class DtAnnualVacationPlan {

	public $_fields = array();
	private $_allowed_keys = array('id', 'employee_detail_id', 'avp_type_id', 'description', 'last_work_day', 'report_to_work', 'status', 'is_active', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'last_work_day':
				$this->_fields[$key] = new DtDate($value);
				break;
			case 'report_to_work': // we are adding a day because fullCalender has bug. if end date is 10 july it will consider it as 9 july as end date
				$this->_fields[$key] = new DtDate(DtDate::add_a_day($value));
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (in_array($key, array('last_work_day', 'report_to_work')))
				return $this->_fields[$key]->formatToUnix();
			else if ($key == 'last_work_day_formated')
				return $this->last_work_day_formated();	
			else if($key == 'report_to_work_formated')	
				return $this->report_to_work_formated();	
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'last_work_day' => $this->last_work_day,
			'report_to_work' => $this->report_to_work,
			'last_work_day_formated' => $this->last_work_day_formated,
			'report_to_work_formated' => $this->last_work_day_formated,
			'description' => $this->description,
			'status' => $this->status,
			'created' => $this->created
		);
	}
	
	function last_work_day_formated(){
		return 	$this->_fields['last_work_day']->getDate();
	}
	
	function report_to_work_formated(){
		return 	$this->_fields['report_to_work']->getDate();
	}
	
	/**
	 * Add single employee detail to object
	 * 
	 * @param array $employee_detail employee detail
	 */
	function add_employee_detail($employee_detail) {
		if (isset($this->_fields["EmployeeDetail"]) && !is_array($this->_fields["EmployeeDetail"])) {
			$prev_value = $this->EmployeeDetail; // restore previous value
			unset($this->_fields["EmployeeDetail"]); // now unset it so that we can convert object to array
			$this->_fields["EmployeeDetail"][] = $prev_value;
			$this->_fields["EmployeeDetail"][] = new DtEmployeeDetail($employee_detail);
		} else if (isset($this->_fields["EmployeeDetail"]) && is_array($this->_fields["EmployeeDetail"]))
			$this->_fields["EmployeeDetail"][] = new DtEmployeeDetail($employee_detail);
		else
			$this->_fields["EmployeeDetail"] = new DtEmployeeDetail($employee_detail);
	}
	
	/**
	 * Get family_detail
	 * 
	 * @return array
	 */
	function get_employee_details() {
		$data = array();
		if(isset($this->_fields['EmployeeDetail'])) {
			if (is_array($this->_fields['EmployeeDetail'])) {
				foreach ($this->_fields['EmployeeDetail'] as $employee_detail) {
					$data[] = $employee_detail->get_field();
				}
			} else if ($this->_fields['EmployeeDetail'] instanceof DtEmployeeDetail) {
				$data = $this->_fields['EmployeeDetail']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple paition family detail to object
	 * 
	 * @param array $family_data information
	 */
	function add_employee_details($employee_detail) {
		if (!empty($employee_detail)) {
			foreach ($employee_detail as $value)
				$this->add_employee_detail($value);
		}
	}
	
	/**
	 * Add single avp type to object
	 * 
	 * @param array $avp_type 
	 */
	function add_AvpType($avp_type) {
		if (isset($this->_fields["AvpType"]) && !is_array($this->_fields["AvpType"])) {
			$prev_value = $this->AvpType; // restore previous value
			unset($this->_fields["AvpType"]); // now unset it so that we can convert object to array
			$this->_fields["AvpType"][] = $prev_value;
			$this->_fields["AvpType"][] = new DtAvpType($avp_type);
		} else if (isset($this->_fields["AvpType"]) && is_array($this->_fields["AvpType"]))
			$this->_fields["AvpType"][] = new DtAvpType($avp_type);
		else
			$this->_fields["AvpType"] = new DtAvpType($avp_type);
	}
	
	/**
	 * Get avp type
	 * 
	 * @return array
	 */
	function get_AvpType() {
		$data = array();
		if(isset($this->_fields['AvpType'])) {
			if (is_array($this->_fields['AvpType'])) {
				foreach ($this->_fields['AvpType'] as $avp_type) {
					$data[] = $avp_type->get_field();
				}
			} else if ($this->_fields['AvpType'] instanceof DtAvpType) {
				$data = $this->_fields['AvpType']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple avp types to object
	 * 
	 * @param array $avp_type information
	 */
	function add_AvpTypes($avp_type) {
		if (!empty($avp_type)) {
			foreach ($avp_type as $value)
				$this->add_AvpType($value);
		}
	}

}

?>
