<?php

class DtWaitingListsTitle {

	private $_fields = array();
	// allowed_keys contains 'name' where as in db there is no column with field named 'name'.
	// reason is there is situation when we need drop down for waiting lists title & generic function named 
	// format_drop_down in AppModel class uses 'name' field . hence using 'name'
	private $_allowed_keys = array('id','name' ,'title', 'description','division_id', 'created', 'is_active');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			}
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'title' => $this->title,
			'division_id' => $this->division_id,
			'description' => $this->_get_desc(),
			'is_active' => $this->is_active,
			'created' => $this->created
		);
	}

	private function _get_desc() {
		return ($this->description !== '0') ? $this->description : 'Not provided' ;
	}
	
	/**
	 * Add single division to object
	 * 
	 * @param array $division_data division information
	 */
	function add_division($division_data) {
		if (isset($this->_fields["Division"]) && !is_array($this->_fields["Division"])) {
			$prev_value = $this->Division; // restore previous value
			unset($this->_fields["Division"]); // now unset it so that we can convert object to array
			$this->_fields["Division"][] = $prev_value;
			$this->_fields["Division"][] = new DtDivision($division_data);
		} else if (isset($this->_fields["Division"]) && is_array($this->_fields["Division"]))
			$this->_fields["Division"][] = new DtDivision($division_data);
		else
			$this->_fields["Division"] = new DtDivision($division_data);
	}
	
	/**
	 * Get divisions
	 * 
	 * @return array
	 */
	function get_divisions() {
		$data = array();
		if (is_array($this->_fields['Division'])) {
			foreach ($this->_fields['Division'] as $division) {
				$data[] = $division->get_field();
			}
		} else if ($this->_fields['Division'] instanceof DtDivision) {
			$data[] = $this->_fields['Division']->get_field();
		}
		return $data;
	}
	
	/**
	 * Add multiple division to object
	 * 
	 * @param array $division_data division information
	 */
	function add_divisions($division_data) {
		if (!empty($division_data)) {
			foreach ($religion_data as $value)
				$this->add_division($value);
		}
	}

}

?>
