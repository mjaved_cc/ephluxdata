<?php

class DtConversation {

	public $_fields = array();
	private $_allowed_keys = array('id','user_id','title', 'created', 'last_message_id', 'allow_add', 'count');

	public function __construct($data = null) {

		$this->populate_data($data);		
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} 
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'user_id' => $this->user_id,
			'title' => $this->title,
			'last_message_id' => $this->last_message_id,
			'allow_add' => $this->allow_add,
			'count' => $this->count,
			'created' => $this->created
		);
	}
	
	
	function is_group_text() {
		return ($this->_fields['is_group']) ? "Group" : "Individual";
	}
	
	function appointment_date_formated() {
		return $this->_fields['appointment_date']->getDateAndTime();
	}
	
	
	function add_conversation_meassage( $conversation_message ) {
	
		if (isset($this->_fields["ConversationMessage"]) && !is_array($this->_fields["ConversationMessage"])) {
			$prev_value = $this->ConversationMessage; // restore previous value
			unset($this->_fields["ConversationMessage"]); // now unset it so that we can convert object to array
			$this->_fields["ConversationMessage"][] = $prev_value;
			$this->_fields["ConversationMessage"][] = new DtConversationMessage($conversation_message);
		} else if (isset($this->_fields["ConversationMessage"]) && is_array($this->_fields["ConversationMessage"])){
			$this->_fields["ConversationMessage"][] = new DtConversationMessage($conversation_message);
		} else
			$this->_fields["ConversationMessage"] = new DtConversationMessage($conversation_message);
	}
	
	function add_conversation_user( $conversation_user ) { 
		if (isset($this->_fields["ConversationUser"]) && !is_array($this->_fields["ConversationUser"])) {
			$prev_value = $this->ConversationMessage; // restore previous value
			unset($this->_fields["ConversationUser"]); // now unset it so that we can convert object to array
			$this->_fields["ConversationUser"][] = $prev_value;
			$this->_fields["ConversationUser"][] = new DtConversationMessage($conversation_user);
		} else if (isset($this->_fields["ConversationUser"]) && is_array($this->_fields["ConversationUser"])){
			$this->_fields["ConversationUser"][] = new DtConversationUser($conversation_user);
		} else
			$this->_fields["ConversationUser"] = new DtConversationMessage($conversation_user);
	}
	
	/**
	 * Add multiple add_conversation_meassages to object
	 * 
	 * @param array $conversation_messages
	 */
	function add_conversation_meassages($conversation_messages) {
		if (!empty($conversation_messages)) {
			foreach ($conversation_messages as $conversation_message)
				$this->add_conversation_meassage($conversation_message);
		}
	}
	
	/**
	 * Get family_detail
	 * 
	 * @return array
	 */
	function get_conversation_details() {
		$data = array();
		
		if(isset($this->_fields['ConversationMessage'])) {
			if (is_array($this->_fields['ConversationMessage'])) {
				foreach ($this->_fields['ConversationMessage'] as $conversation_message) {
					$data[] = $conversation_message->get_field();
				}
			} else if ($this->_fields['ConversationMessage'] instanceof DtConversationMessage) { 
				$data = $this->_fields['ConversationMessage']->get_field();
			}
		}
	
	
		return $data;
	
	}
	

	function add_last_message($message ) {
		if (isset($this->_fields["LastMessage"]) && !is_array($this->_fields["LastMessage"])) {
			$prev_value = $this->LastMessage; // restore previous value
			unset($this->_fields["LastMessage"]); // now unset it so that we can convert object to array
			$this->_fields["LastMessage"][] = $prev_value;
			$this->_fields["LastMessage"][] = new DtLastMsg($message);
		} else if (isset($this->_fields["LastMessage"]) && is_array($this->_fields["LastMessage"]))
			$this->_fields["LastMessage"][] = new DtLastMsg($message);
		else
			$this->_fields["LastMessage"] = new DtLastMsg($message);
	}
	
	
	
	
		function get_last_message() {
		$data = array();
		if(isset($this->_fields['LastMessage'])) {
			if (is_array($this->_fields['LastMessage'])) {
				foreach ($this->_fields['LastMessage'] as $message) {
					$data[] = $message->get_field();
				}
			} else if ($this->_fields['LastMessage'] instanceof DtLastMsg) {
				$data = $this->_fields['LastMessage']->get_field();
			}
		}
		return $data;
	}
	
	
		function add_User($adduser) {
		if (isset($this->_fields["User"]) && !is_array($this->_fields["User"])) {
			$prev_value = $this->User; // restore previous value
			unset($this->_fields["User"]); // now unset it so that we can convert object to array
			$this->_fields["User"][] = $prev_value;
			$this->_fields["User"][] = new DtUser($adduser);
		} else if (isset($this->_fields["User"]) && is_array($this->_fields["User"]))
			$this->_fields["User"][] = new DtUser($adduser);
		else
			$this->_fields["User"] = new DtUser($adduser);
	}
	

		function get_user() {
		$data = array();
		if(isset($this->_fields['User'])) {
			if (is_array($this->_fields['User'])) {
				foreach ($this->_fields['User'] as $adduser) {
					$data[] = $adduser->get_field();
				}
			} else if ($this->_fields['User'] instanceof DtUser) {
				$data = $this->_fields['User']->get_field();
			}
		}
		return $data;
	}
}

?>
