<?php

class DtAppointment {

	private $_fields = array();
	private $_allowed_keys = array('id', 'patient_waiting_list_id', 'employee_detail_id', 'patient_detail_id', 'appointment_date', 'is_group', 'status', 'is_active', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'appointment_date':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if ($key == 'appointment_date')
				return $this->_fields[$key]->formatToUnix();
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'appointment_date' => $this->appointment_date,
			'appointment_date_formated' => $this->appointment_date_formated(),
			'is_group_text' => $this->is_group_text(),
			'is_group' => $this->is_group,
			'status' => $this->status,
			'created' => $this->created
		);
	}
	
	
	function is_group_text() {
		return ($this->_fields['is_group']) ? "Group" : "Individual";
	}
	
	function appointment_date_formated() {
		return $this->_fields['appointment_date']->getDateAndTime();
	}
	
	/**
	 * Add single employee detail to object
	 * 
	 * @param array $employee_detail employee detail
	 */
	function add_employee_detail($employee_detail) {
		if (isset($this->_fields["EmployeeDetail"]) && !is_array($this->_fields["EmployeeDetail"])) {
			$prev_value = $this->EmployeeDetail; // restore previous value
			unset($this->_fields["EmployeeDetail"]); // now unset it so that we can convert object to array
			$this->_fields["EmployeeDetail"][] = $prev_value;
			$this->_fields["EmployeeDetail"][] = new DtEmployeeDetail($employee_detail);
		} else if (isset($this->_fields["EmployeeDetail"]) && is_array($this->_fields["EmployeeDetail"]))
			$this->_fields["EmployeeDetail"][] = new DtEmployeeDetail($employee_detail);
		else
			$this->_fields["EmployeeDetail"] = new DtEmployeeDetail($employee_detail);
	}
	
	/**
	 * Get family_detail
	 * 
	 * @return array
	 */
	function get_employee_details() {
		$data = array();
		if(isset($this->_fields['EmployeeDetail'])) {
			if (is_array($this->_fields['EmployeeDetail'])) {
				foreach ($this->_fields['EmployeeDetail'] as $employee_detail) {
					$data[] = $employee_detail->get_field();
				}
			} else if ($this->_fields['EmployeeDetail'] instanceof DtEmployeeDetail) {
				$data = $this->_fields['EmployeeDetail']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple paition family detail to object
	 * 
	 * @param array $family_data information
	 */
	function add_employee_details($employee_detail) {
		if (!empty($employee_detail)) {
			foreach ($employee_detail as $value)
				$this->add_employee_detail($value);
		}
	}
	
	/**
	 * Add single attachment report to object
	 * 
	 * @param array $attachment_report
	 */
	function add_patient_attachment_report($attachment_report) {
		if (isset($this->_fields["PatientAttachmentReport"]) && !is_array($this->_fields["PatientAttachmentReport"])) {
			$prev_value = $this->PatientAttachmentReport; // restore previous value
			unset($this->_fields["PatientAttachmentReport"]); // now unset it so that we can convert object to array
			$this->_fields["PatientAttachmentReport"][] = $prev_value;
			$this->_fields["PatientAttachmentReport"][] = new DtPatientAttachmentReport($attachment_report);
		} else if (isset($this->_fields["PatientAttachmentReport"]) && is_array($this->_fields["PatientAttachmentReport"]))
			$this->_fields["PatientAttachmentReport"][] = new DtPatientAttachmentReport($attachment_report);
		else
			$this->_fields["PatientAttachmentReport"] = new DtPatientAttachmentReport($attachment_report);
	}
	
	/**
	 * Get attachment reports
	 * 
	 * @return array
	 */
	function get_patient_attachment_reports() {
		$data = array();
		if(isset($this->_fields['PatientAttachmentReport'])) {
			if (is_array($this->_fields['PatientAttachmentReport'])) {
				foreach ($this->_fields['PatientAttachmentReport'] as $attachment_report) {
					$data[] = $attachment_report->get_field();
				}
			} else if ($this->_fields['PatientAttachmentReport'] instanceof DtPatientAttachmentReport) {
				$data[] = $this->_fields['PatientAttachmentReport']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple attachment reports to object
	 * 
	 * @param array $attachment_report information
	 */
	function add_patient_attachment_reports($attachment_report) {
		if (!empty($attachment_report)) {
			foreach ($attachment_report as $value)
				$this->add_patient_attachment_report($value['PatientAttachmentReport']);
		}
	}
	
	/**
	 * Add single appointment group to object
	 * 
	 * @param array $appointment_group appointment group
	 */
	 function add_appointment_group($appointment_group) {
		if (isset($this->_fields["AppointmentGroup"]) && !is_array($this->_fields["AppointmentGroup"])) {
			$prev_value = $this->AppointmentGroup; // restore previous value
			unset($this->_fields["AppointmentGroup"]); // now unset it so that we can convert object to array
			$this->_fields["AppointmentGroup"][] = $prev_value;
			$this->_fields["AppointmentGroup"][] = new DtAppointmentGroup($appointment_group);
		} else if (isset($this->_fields["AppointmentGroup"]) && is_array($this->_fields["AppointmentGroup"]))
			$this->_fields["AppointmentGroup"][] = new DtAppointmentGroup($appointment_group);
		else
			$this->_fields["AppointmentGroup"][] = new DtAppointmentGroup($appointment_group);
	}
	
	/**
	 * Add multiple call logs to object
	 * 
	 * @param array $call_log information
	 */
	function add_appointment_groups($appointment_group) {
		if (!empty($appointment_group)) {
			foreach ($appointment_group as $key => $value){
				$this->add_appointment_group($value);
			}
		}
	}
	
	/**
	 * Get appointment group
	 * 
	 * @return array
	 */
	 
	 function get_appointment_groups() {
		$data = array();
		if(isset($this->_fields['AppointmentGroup'])) {
			if (is_array($this->_fields['AppointmentGroup'])) {
				foreach ($this->_fields['AppointmentGroup'] as $appointment_group) {
					$data[] = $appointment_group->get_field();
				}
			} else if ($this->_fields['AppointmentGroup'] instanceof DtAppointmentGroup) {
				$data = $this->_fields['AppointmentGroup']->get_field();
			}
		}
		return $data;
	}
	
	
	/**
	 * Add single call log to object
	 * 
	 * @param array $call_log call log
	 */
	function add_call_log($call_log) {
		if (isset($this->_fields["CallLog"]) && !is_array($this->_fields["CallLog"])) {
			$prev_value = $this->CallLog; // restore previous value
			unset($this->_fields["CallLog"]); // now unset it so that we can convert object to array
			$this->_fields["CallLog"][]["CallLog"] = $prev_value;
			$this->_fields["CallLog"][]["CallLog"] = new DtCallLog($call_log);
		} else if (isset($this->_fields["CallLog"]) && is_array($this->_fields["CallLog"]))
			$this->_fields["CallLog"][]["CallLog"] = new DtCallLog($call_log);
		else
			$this->_fields["CallLog"][]["CallLog"] = new DtCallLog($call_log);
	}
	
	function add_employee_detail_call_log($call_log_employee_detail, $index) {
		$this->_fields["CallLog"][$index]["EmployeeDetail"] = new DtEmployeeDetail($call_log_employee_detail);
	}
	
	/**
	 * Get family_detail
	 * 
	 * @return array
	 */
	function get_call_logs() {
		//pr($this->_fields["CallLog"]);exit;
		$data = array();
		if(isset($this->_fields['CallLog'])) {
			if (is_array($this->_fields['CallLog'])) {
				foreach ($this->_fields['CallLog'] as $key=>$call_log) {
					$data[$key]['CallLog'] = $call_log['CallLog']->get_field();
					$data[$key]['EmployeeDetail'] = $call_log['EmployeeDetail']->get_field();
				}
			} else if ($this->_fields['CallLog'] instanceof DtCallLog) {
				$data[] = $this->_fields['CallLog']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple call logs to object
	 * 
	 * @param array $call_log information
	 */
	function add_call_logs($call_log) {
		if (!empty($call_log)) {
			foreach ($call_log as $key => $value){
				$this->add_call_log($value['CallLog']);
				$this->add_employee_detail_call_log($value['EmployeeDetail'], $key);
			}
		}
	}
	
	/**
	 * Add single patient detail to object
	 * 
	 * @param array $patient_detail patient detail
	 */
	function add_patient_detail($patient_detail) {
		if (isset($this->_fields["PatientDetail"]) && !is_array($this->_fields["PatientDetail"])) {
			$prev_value = $this->PatientDetail; // restore previous value
			unset($this->_fields["PatientDetail"]); // now unset it so that we can convert object to array
			$this->_fields["PatientDetail"][] = $prev_value;
			$this->_fields["PatientDetail"][] = new DtPatientDetail($patient_detail);
		} else if (isset($this->_fields["PatientDetail"]) && is_array($this->_fields["PatientDetail"]))
			$this->_fields["PatientDetail"][] = new DtPatientDetail($patient_detail);
		else
			$this->_fields["PatientDetail"] = new DtPatientDetail($patient_detail);
	}
	
	/**
	 * Get patient_details
	 * 
	 * @return array
	 */
	function get_patient_details() {
		$data = array();
		if(isset($this->_fields['PatientDetail'])) {
			if (is_array($this->_fields['PatientDetail'])) {
				foreach ($this->_fields['PatientDetail'] as $patient_detail) {
					$data[] = $patient_detail->get_field();
				}
			} else if ($this->_fields['PatientDetail'] instanceof DtPatientDetail) {
				$data = $this->_fields['PatientDetail']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple patient detail to object
	 * 
	 * @param array $patient_detail information
	 */
	function add_patient_details($patient_detail) {
		if (!empty($patient_detail)) {
			foreach ($patient_detail as $value)
				$this->add_patient_detail($value);
		}
	}
	
	/**
	 * Add single division to object
	 * 
	 * @param array $division patient detail
	 */
	function add_division($division) {
		if (isset($this->_fields["Division"]) && !is_array($this->_fields["Division"])) {
			$prev_value = $this->Division; // restore previous value
			unset($this->_fields["Division"]); // now unset it so that we can convert object to array
			$this->_fields["Division"][] = $prev_value;
			$this->_fields["Division"][] = new DtDivision($division);
		} else if (isset($this->_fields["Division"]) && is_array($this->_fields["Division"]))
			$this->_fields["Division"][] = new DtDivision($division);
		else
			$this->_fields["Division"] = new DtDivision($division);
	}
	
	/**
	 * Get divisions
	 * 
	 * @return array
	 */
	function get_divisions() {
		$data = array();
		if(isset($this->_fields['Division'])) {
			if (is_array($this->_fields['Division'])) {
				foreach ($this->_fields['Division'] as $division) {
					$data[] = $division->get_field();
				}
			} else if ($this->_fields['Division'] instanceof DtDivision) {
				$data = $this->_fields['Division']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple division to object
	 * 
	 * @param array $division information
	 */
	function add_divisions($division) {
		if (!empty($division)) {
			foreach ($division as $value)
				$this->add_division($value);
		}
	}

}

?>
