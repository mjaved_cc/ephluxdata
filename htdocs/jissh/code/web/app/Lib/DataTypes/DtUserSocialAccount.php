<?php

class DtUserSocialAccount {

	private $_fields = array();
	private $_allowed_keys = array('id','first_name','last_name','username','group_id','created','status');
	
	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_field() {
		//return $this->_fields;

		return array(
			'id' => $this->id,
			'user_id' => $this->user_id,
			'id' => $this->id,
			'link_id' => $this->link_id,
			'email_address' => $this->email_address,
			'image_url' => $this->image_url,
			'type_id' => $this->type_id,
			'screen_name' => $this->screen_name,
			'status' => $this->status,
			'created' => $this->created
		);
	}

}