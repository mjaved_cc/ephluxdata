<?php

class DtMpdActivity {

	private $_fields = array();
	private $_allowed_keys = array('id', 'employee_detail_id', 'program_id', 'mpd_activity_list_id', 'modality_id', 'status', 'date_complition', 'is_active', 'created', 'modified');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			case 'date_complition':
				$this->_fields[$key] = new DtDate($value);
				break;	
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if ($key == 'date_complition')
				return $this->_fields[$key]->getDate();		
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'employee_detail_id' => $this->employee_detail_id,
			'program_id' => $this->program_id,
			'mpd_activity_list_id' => $this->mpd_activity_list_id,
			'modality_id' => $this->modality_id,
			'status' => $this->status,
			'date_complition' => $this->date_complition,
			'is_active' => $this->is_active,
			'created' => $this->created
		);
	}

}

?>
