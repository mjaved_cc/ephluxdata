<?php

class DtPatientAttachmentReport {

	private $_fields = array();
	private $_allowed_keys = array('id', 'patient_id', 'appointment_id','attachment_title','attachment_file','created','modified');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':			
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'patient_id' => $this->patient_id,
			'appointment_id' => $this->appointment_id,
			'attachment_title' => $this->attachment_title,
			'attachment_file' => $this->attachment_file,
			'created' => $this->created,
			'modified' => $this->modified
		);
	}
	

}

?>
