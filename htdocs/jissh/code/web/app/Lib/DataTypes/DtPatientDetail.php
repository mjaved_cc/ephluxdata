<?php
class DtPatientDetail {

	private $_fields = array();
	// allowed_keys contains 'name' where as in db there is no column with field named 'name'.
	// reason is there is situation when we need drop down for waiting lists title & generic function named 
	// format_drop_down in AppModel class uses 'name' field . hence using 'name'
	private $_allowed_keys = array('id','user_id', 'name', 'iqama_no', 'marital_status', 'religion_id', 'allow_info_from_agencies', 'allow_info_to_agencies', 'multimedia_agreement', 'multimedia_for_research', 'mobile', 'prefered_contact', 'po_box', 'postal_code', 'district', 'city', 'country_id', 'residence_no', 'gender', 'dob', 'first_name', 'last_name', 'middle_name', 'patient_lives_with','sibling_order','gardian_relationship','refered_by', 'status', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if ($key == 'first_name')
				return $this->_get_first_name();
			else if ($key == 'last_name')
				return $this->_get_last_name();
			else if ($key == 'full_name')
				return $this->_get_full_name();
			else if ($key == 'middle_name')
				return $this->_get_middle_name();
			else if ($key == 'po_box')
				return $this->_get_po_box();
			else if ($key == 'city')
				return $this->_get_city();
			else if ($key == 'dob')
				return $this->_get_dob();
			else if ($key == 'district')
				return $this->_get_district();
			else if ($key == 'refered_by')
				return $this->_get_refered_by();
			else if ($key == 'postal_code')
				return $this->_get_postal_code();
			else if ($key == 'mobile')
				return $this->_get_mobile();					
			else if ($key == 'residence_no')
				return $this->_get_residence_no();			
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'user_id' => $this->user_id,
			'iqama_no' => $this->iqama_no,
			'marital_status' => $this->marital_status,
			'religion_id' => $this->religion_id,
			'allow_info_from_agencies' => $this->allow_info_from_agencies,
			'allow_info_to_agencies' => $this->allow_info_to_agencies,
			'multimedia_agreement' => $this->multimedia_agreement,
			'multimedia_for_research' => $this->multimedia_for_research,
			'mobile' => $this->mobile,
			'prefered_contact' => $this->phone,
			'po_box' => $this->po_box,
			'postal_code' => $this->postal_code,
			'district' => $this->district,
			'city' => $this->city,
			'country_id' => $this->country_id,
			'residence_no' => $this->residence_no,
			'gender' => $this->gender,
			'dob' => $this->dob,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'middle_name' => $this->middle_name,
			'full_name' => $this->full_name,
			'patient_lives_with' => $this->patient_lives_with,
			'sibling_order' => $this->sibling_order,
			'gardian_relationship' => $this->gardian_relationship,
			'refered_by' => $this->refered_by,
			'status' => $this->status,
			'created' => $this->created
		);
	}

	private function _get_full_name() {
		 return $this->_get_first_name().' '.$this->_get_last_name();
	}
	
	private function _get_middle_name(){
		if ($this->_fields['middle_name'] === '0')
			return '';

		return $this->_fields['middle_name'];
	}
	
	private function _get_po_box(){
		if ($this->_fields['po_box'] === '0')
			return '';

		return $this->_fields['po_box'];
	}
	
	private function _get_city(){
		if ($this->_fields['city'] === '0')
			return '';

		return $this->_fields['city'];
	}
	
	private function _get_district(){
		if ($this->_fields['district'] === '0')
			return '';

		return $this->_fields['district'];
	}
	
	private function _get_postal_code(){
		if ($this->_fields['postal_code'] === '0')
			return '';

		return $this->_fields['postal_code'];
	}
	
	private function _get_refered_by(){
		if ($this->_fields['refered_by'] === '0')
			return '';

		return $this->_fields['refered_by'];
	}
	
	private function _get_dob(){
		if ($this->_fields['dob'] === '0')
			return '';

		return $this->_fields['dob'];
	}
	
	private function _get_mobile(){
		if ($this->_fields['mobile'] === '0')
			return '';

		return $this->_fields['mobile'];
	}
	
	private function _get_residence_no(){
		if ($this->_fields['residence_no'] === '0')
			return '';

		return $this->_fields['residence_no'];
	}
	
	private function _get_first_name() {

		if ($this->_fields['first_name'] === '0')
			return 'Anonymous';

		return $this->_fields['first_name'];
	}

	private function _get_last_name() {

		if ($this->_fields['last_name'] === '0')
			return 'User';

		return $this->_fields['last_name'];
	}
	
	/**
	 * Add single paition family detail to object
	 * 
	 * @param array $family_data family detail information
	 */
	function add_family_detail($family_data) {
		if (isset($this->_fields["PatientFamilyDetail"]) && !is_array($this->_fields["PatientFamilyDetail"])) {
			$prev_value = $this->PatientFamilyDetail; // restore previous value
			unset($this->_fields["PatientFamilyDetail"]); // now unset it so that we can convert object to array
			$this->_fields["PatientFamilyDetail"][] = $prev_value;
			$this->_fields["PatientFamilyDetail"][] = new DtPatientFamilyDetail($family_data);
		} else if (isset($this->_fields["PatientFamilyDetail"]) && is_array($this->_fields["PatientFamilyDetail"]))
			$this->_fields["PatientFamilyDetail"][] = new DtPatientFamilyDetail($family_data);
		else
			$this->_fields["PatientFamilyDetail"] = new DtPatientFamilyDetail($family_data);
	}
	
	/**
	 * Get family_detail
	 * 
	 * @return array
	 */
	function get_family_details() {
		$data = array();
		if(isset($this->_fields['PatientFamilyDetail'])) {
			if (is_array($this->_fields['PatientFamilyDetail'])) {
				foreach ($this->_fields['PatientFamilyDetail'] as $family_detail) {
					$data[] = $family_detail->get_field();
				}
			} else if ($this->_fields['PatientFamilyDetail'] instanceof DtPatientFamilyDetail) {
				$data[] = $this->_fields['PatientFamilyDetail']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add multiple paition family detail to object
	 * 
	 * @param array $family_data information
	 */
	function add_family_details($family_data) {
		if (!empty($family_data)) {
			foreach ($family_data as $value)
				$this->add_family_detail($value);
		}
	}
	
	/**
	 * Add single appointment to object
	 * 
	 * @param array $appointment_data appointment information
	 */
	function add_appointment($appointment_data) {
		if (isset($this->_fields["Appointment"]) && !is_array($this->_fields["Appointment"])) {
			$prev_value = $this->Appointment; // restore previous value
			unset($this->_fields["Appointment"]); // now unset it so that we can convert object to array
			$this->_fields["Appointment"][] = $prev_value;
			$this->_fields["Appointment"][] = new DtAppointment($appointment_data);
		} else if (isset($this->_fields["Appointment"]) && is_array($this->_fields["Appointment"]))
			$this->_fields["Appointment"][] = new DtAppointment($appointment_data);
		else
			$this->_fields["Appointment"] = new DtAppointment($appointment_data);
	}
	
	/**
	 * Get appointments
	 * 
	 * @return array
	 */
	function get_appointments() {
		$data = array();
		if(isset($this->_fields['Appointment'])) {
			if (is_array($this->_fields['Appointment'])) {
				foreach ($this->_fields['Appointment'] as $appointment_data) {
					$data[] = $appointment_data->get_field();
				}
			} else if ($this->_fields['Appointment'] instanceof DtAppointment) {
				$data = $this->_fields['Appointment']->get_field();
				$dtobj = new DtDate(date('Y-m-d H:i:s',$data['appointment_date']));
				$data['appointment_date'] = $dtobj->getDate();
			}
		}
		return $data;
	}
	
	/**
	 * Add single appointment to object
	 * 
	 * @param array $appointment_data appointment information
	 */
	function add_medication($medication_data) {
		if (isset($this->_fields["PatientMedication"]) && !is_array($this->_fields["PatientMedication"])) {
			$prev_value = $this->PatientMedication; // restore previous value
			unset($this->_fields["PatientMedication"]); // now unset it so that we can convert object to array
			$this->_fields["PatientMedication"][] = $prev_value;
			$this->_fields["PatientMedication"][] = new DtPatientMedication($medication_data);
		} else if (isset($this->_fields["PatientMedication"]) && is_array($this->_fields["PatientMedication"]))
			$this->_fields["PatientMedication"][] = new DtPatientMedication($medication_data);
		else
			$this->_fields["PatientMedication"] = new DtPatientMedication($medication_data);
	}
	
	/**
	 * Get appointments
	 * 
	 * @return array
	 */
	function get_medication() {
		$data = array();
		if(isset($this->_fields['PatientMedication'])) {
			if (is_array($this->_fields['PatientMedication'])) {
				foreach ($this->_fields['PatientMedication'] as $medication_data) {
					$data[] = $medication_data->get_field();
				}
			} else if ($this->_fields['PatientMedication'] instanceof DtPatientMedication) {
				$data = $this->_fields['PatientMedication']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add single medication_detail to object
	 * 
	 * @param array $medication_detail_data medication information
	 */
	function add_medication_detail($medication_detail_data) {
		if (isset($this->_fields["PatientMedicationDetail"]) && !is_array($this->_fields["PatientMedicationDetail"])) {
			$prev_value = $this->PatientMedicationDetail; // restore previous value
			unset($this->_fields["PatientMedicationDetail"]); // now unset it so that we can convert object to array
			$this->_fields["PatientMedicationDetail"][] = $prev_value;
			$this->_fields["PatientMedicationDetail"][] = new DtPatientMedicationDetail($medication_detail_data);
		} else if (isset($this->_fields["PatientMedicationDetail"]) && is_array($this->_fields["PatientMedicationDetail"]))
			$this->_fields["PatientMedicationDetail"][] = new DtPatientMedicationDetail($medication_detail_data);
		else
			$this->_fields["PatientMedicationDetail"] = new DtPatientMedicationDetail($medication_detail_data);
	}
	
	/**
	 * Add multiple medication details to object
	 * 
	 * @param array $medication_detail medication information
	 */
	function add_medication_details($medication_detail) {
		if (!empty($medication_detail)) {
			foreach ($medication_detail as $value)
				$this->add_medication_detail($value);
		}
	}
	
	/**
	 * Get medication detail
	 * 
	 * @return array
	 */
	function get_medication_detail() {
		$data = array();
		if(isset($this->_fields['PatientMedicationDetail'])) {
			if (is_array($this->_fields['PatientMedicationDetail'])) {
				foreach ($this->_fields['PatientMedicationDetail'] as $medication_data) {
					$data[] = $medication_data->get_field();
				}
			} else if ($this->_fields['PatientMedicationDetail'] instanceof DtPatientMedicationDetail) {
				$data = $this->_fields['PatientMedicationDetail']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Add single country to object
	 * 
	 * @param array $country_data country information
	 */
	function add_country($country_data) {
		if (isset($this->_fields["Country"]) && !is_array($this->_fields["Country"])) {
			$prev_value = $this->Country; // restore previous value
			unset($this->_fields["Country"]); // now unset it so that we can convert object to array
			$this->_fields["Country"][] = $prev_value;
			$this->_fields["Country"][] = new DtCountries($country_data);
		} else if (isset($this->_fields["Country"]) && is_array($this->_fields["Country"]))
			$this->_fields["Country"][] = new DtCountries($country_data);
		else
			$this->_fields["Country"] = new DtCountries($country_data);
	}
	
	/**
	 * Get countries
	 * 
	 * @return array
	 */
	function get_countries() {
		$data = array();
		if (is_array($this->_fields['Country'])) {
			foreach ($this->_fields['Country'] as $country) {
				$data[] = $country->get_field();
			}
		} else if ($this->_fields['Country'] instanceof DtCountries) {
			$data = $this->_fields['Country']->get_field();
		}
		return $data;
	}
	
	/**
	 * Add single country to object
	 * 
	 * @param array $country_data country information
	 */
	function add_religion($religion_data) {
		if (isset($this->_fields["Religion"]) && !is_array($this->_fields["Religion"])) {
			$prev_value = $this->Religion; // restore previous value
			unset($this->_fields["Religion"]); // now unset it so that we can convert object to array
			$this->_fields["Religion"][] = $prev_value;
			$this->_fields["Religion"][] = new DtReligion($religion_data);
		} else if (isset($this->_fields["Religion"]) && is_array($this->_fields["Religion"]))
			$this->_fields["Religion"][] = new DtReligion($religion_data);
		else
			$this->_fields["Religion"] = new DtReligion($religion_data);
	}
	
	/**
	 * Get religions
	 * 
	 * @return array
	 */
	function get_religions() {
		$data = array();
		if (is_array($this->_fields['Religion'])) {
			foreach ($this->_fields['Religion'] as $religion) {
				$data[] = $religion->get_field();
			}
		} else if ($this->_fields['Religion'] instanceof DtReligion) {
			$data = $this->_fields['Religion']->get_field();
		}
		return $data;
	}
	
	/**
	 * Add multiple religions to object
	 * 
	 * @param array $religion_data religion information
	 */
	function add_religions($religion_data) {
		if (!empty($religion_data)) {
			foreach ($religion_data as $value)
				$this->add_religion($value);
		}
	}

}

?>
