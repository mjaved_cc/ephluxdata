<?php

class DtTemplate {

	private $_fields = array();
	private $_allowed_keys = array('id', 'template_name', 'description', 'division_id', 'is_active', 'created', 'modified');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} 	
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'template_name' => $this->template_name,
			'description' => $this->description,
			'division_id' => $this->division_id,
			'is_active' => $this->is_active,
			'created' => $this->created
		);
	}
	
	/**
	 * Add single division to object
	 * 
	 * @param array $division_data division information
	 */
	function add_division($division_data) {
		if (isset($this->_fields["Division"]) && !is_array($this->_fields["Division"])) {
			$prev_value = $this->Division; // restore previous value
			unset($this->_fields["Division"]); // now unset it so that we can convert object to array
			$this->_fields["Division"][] = $prev_value;
			$this->_fields["Division"][] = new DtDivision($division_data);
		} else if (isset($this->_fields["Division"]) && is_array($this->_fields["Division"]))
			$this->_fields["Division"][] = new DtDivision($division_data);
		else
			$this->_fields["Division"] = new DtDivision($division_data);
	}
	
	/**
	 * Add single template attribute item to object
	 * 
	 * @param array $template_attribute_item_data template attribute information
	 */
	function add_template_attributes_item($template_attribute_data) {
		$this->_fields["TemplateAttributesItem"] = new DtTemplateAtrributesItem($template_attribute_data);
	}
	
	/**
	 * Add single template attribute to object
	 * 
	 * @param array $template_attribute_data template attribute information
	 */
	function add_template_attribute($template_attribute_data) {
		$this->_fields["TemplateAttribute"] = new DtTemplateAtrribute($template_attribute_data);
	}
	
	/**
	 * Get divsions
	 * 
	 * @return array
	 */
	function get_divisions() {
		$data = array();
		if(isset($this->_fields['Division'])) {
			if (is_array($this->_fields['Division'])) {
				foreach ($this->_fields['Division'] as $division) {
					$data[] = $division->get_field();
				}
			} else if ($this->_fields['Division'] instanceof DtDivision) {
				$data = $this->_fields['Division']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Get attribute items
	 * 
	 * @return array
	 */
	function get_attribute_items() {
		$data = array();
		if(isset($this->_fields['TemplateAttributesItem'])) {
			if (is_array($this->_fields['TemplateAttributesItem'])) {
				foreach ($this->_fields['TemplateAttributesItem'] as $template_attribute) {
					$data[] = $template_attribute->get_field();
				}
			} else if ($this->_fields['TemplateAttributesItem'] instanceof DtTemplateAtrributesItem) {
				$data = $this->_fields['TemplateAttributesItem']->get_field();
			}
		}
		return $data;
	}
	
	/**
	 * Get attributes
	 * 
	 * @return array
	 */
	function get_attributes() {
		$data = array();
		if(isset($this->_fields['TemplateAttribute'])) {
			if (is_array($this->_fields['TemplateAttribute'])) {
				foreach ($this->_fields['TemplateAttribute'] as $template_attribute) {
					$data[] = $template_attribute->get_field();
				}
			} else if ($this->_fields['TemplateAttribute'] instanceof DtTemplateAtrribute) {
				$data = $this->_fields['TemplateAttribute']->get_field();
			}
		}
		return $data;
	}

}

?>
