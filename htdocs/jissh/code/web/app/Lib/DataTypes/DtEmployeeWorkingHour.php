<?php

class DtEmployeeWorkingHour {

	private $_fields = array();
	private $_allowed_keys = array('id', 'working_day', 'start_time', 'end_time', 'total_possible_slots', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'start_time':
			case 'end_time':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (in_array($key, array('start_time', 'end_time'))) {
				return $this->_fields[$key]->formatToFullCalender();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'working_day' => $this->working_day,
			'three_letter_working_day' => $this->get_three_letter_day_representation(),
			'start_time' => $this->start_time,
			'end_time' => $this->end_time,
			'created' => $this->created
		);
	}

	/**
	 * A textual representation of a day, three letters
	 * 
	 * @param type $name Description
	 */
	function get_three_letter_day_representation() {
		return strtolower(date('D', $this->start_time));
	}

	/**
	 * Get cell background color in full calender. 
	 * 
	 * Visual representation of working hour
	 * 
	 */
	function get_cell_bgcolor_full_calender() {
		return CLASS_WORKING_HOUR;
	}

}

?>
