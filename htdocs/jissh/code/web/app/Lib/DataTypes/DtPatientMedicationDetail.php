<?php
class DtPatientMedicationDetail {

	private $_fields = array();
	private $_allowed_keys = array('patient_medication_id', 'attribute_name','attribute_value','attribute_order','attribute_type');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':			
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'patient_medication_id' => $this->patient_medication_id,
			'attribute_name' => $this->attribute_name,
			'attribute_value' => $this->attribute_value,
			'attribute_order' => $this->attribute_order,
			'attribute_type' => $this->attribute_type
		);
	}

}
