<?php

class DtEmployeeDetail {

	private $_fields = array();
	private $_allowed_keys = array('id', 'first_name', 'last_name', 'middle_name', 'picture_name', 'mobile_saudi', 'landline_saudi', 'mobile_homecountry', 'landline_homecountry', 'jish_email', 'license_type_id', 'license_number', 'joining_date', 'contract_expiry_date', 'gender', 'dob', 'status', 'created', 'user_id');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'dob':
			case 'joining_date':
			case 'contract_expiry_date':
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created') {
				return $this->_fields[$key]->getDate();
			} else if (in_array($key, array('dob', 'joining_date', 'contract_expiry_date'))) {
				return $this->_fields[$key]->formatToDatePicker();
			} else if ($key == 'first_name')
				return $this->_get_first_name();
			else if ($key == 'last_name')
				return $this->_get_last_name();
			else if ($key == 'display_name')
				return $this->_get_display_name();
			else if ($key == 'profile_thumb_relative_url')
				return $this->_get_relative_thumb_dp_url();
			else if ($key == 'profile_org_relative_url')
				return $this->_get_relative_orginial_dp_url();
			else if ($key == 'doctor_edit_relative_url')
				return $this->_get_relative_doctor_edit_url();
			else if ($key == 'doctor_details_relative_url')
				return $this->_get_relative_doctor_details_url();
			else if ($key == 'employee_listing_relative_url')
				return $this->_get_relative_employee_list_url();
			else if ($key == 'doctor_register_relative_url')
				return $this->_get_relative_doctor_register_url();
			else if ($key == 'mpd_activities_relative_url')
				return $this->_get_relative_mpd_activities_url();
			else if ($key == 'vpd_activities_relative_url')
				return $this->_get_relative_vpd_activities_url();
			else if ($key == 'avp_relative_url')
				return $this->_avp_relative_url();	
			else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'user_id' => $this->user_id,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'display_name' => $this->display_name,
			'middle_name' => $this->middle_name,
			'picture_name' => $this->picture_name,
			'profile_org_relative_url' => $this->profile_org_relative_url,
			'profile_thumb_relative_url' => $this->profile_thumb_relative_url,
			'doctor_edit_relative_url' => $this->doctor_edit_relative_url,
			'doctor_details_relative_url' => $this->doctor_details_relative_url,
			'employee_listing_relative_url' => $this->employee_listing_relative_url,
			'doctor_register_relative_url' => $this->doctor_register_relative_url,
			'mpd_activities_relative_url' => $this->mpd_activities_relative_url,
			'vpd_activities_relative_url' => $this->vpd_activities_relative_url,
			'avp_relative_url' => $this->avp_relative_url,
			'mobile_saudi' => $this->mobile_saudi,
			'landline_saudi' => $this->landline_saudi,
			'mobile_homecountry' => $this->mobile_homecountry,
			'landline_homecountry' => $this->landline_homecountry,
			'jish_email' => $this->jish_email,
			'license_type_id' => $this->license_type_id,
			'license_number' => $this->license_number,
			'joining_date' => $this->joining_date,
			'contract_expiry_date' => $this->contract_expiry_date,
			'gender' => $this->gender,
			'dob' => $this->dob,
			'status' => $this->status,
			'created' => $this->created
		);
	}

	private function _get_first_name() {

		if ($this->_fields['first_name'] === '0')
			return 'Anonymous';

		return $this->_fields['first_name'];
	}

	private function _get_display_name() {

		return $this->first_name . ' ' . $this->last_name;
	}

	private function _get_last_name() {

		if ($this->_fields['last_name'] === '0')
			return 'User';

		return $this->_fields['last_name'];
	}

	/**
	 * Get relative profile picture thumbnail url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_thumb_dp_url() {
		if ($this->picture_name === '0') {
			return DEFAULT_THUMB_DP;
		} else {
			return DP_THUMB_URL . $this->picture_name;
		}
	}

	/**
	 * Get relative profile picture thumbnail url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_orginial_dp_url() {
		if ($this->picture_name === '0') {
			return DEFAULT_ORG_DP;
		} else {
			return DP_ORG_URL . $this->picture_name;
		}
	}

	/**
	 * Get relative doctor profile edit url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_doctor_edit_url() {
		return 'employee_details/doctor_edit/' . $this->user_id;
	}
	
	private function _avp_relative_url() {
		return 'annual_vacation_plans/add/' . $this->id;
	}

	/**
	 * Get relative doctor profile details url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_doctor_details_url() {
		return 'employee_details/doctor_details/' . $this->user_id;
	}

	/**
	 * Get relative doctor listing url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_employee_list_url() {
		return 'employee_details/index';
	}

	/**
	 * Get relative doctor register url. 
	 * 
	 * Usage : $this->Misc->get_fully_qualified_url(RELATIVE_URL)
	 * 
	 * @return string Relative URL
	 */
	private function _get_relative_doctor_register_url() {
		return 'employee_details/doctor_register';
	}

	/**
	 * Add single division to object
	 * 
	 * @param array $division_data division information
	 */
	function add_division($division_data) {
		if (isset($this->_fields["Division"]) && !is_array($this->_fields["Division"])) {
			$prev_value = $this->Division; // restore previous value
			unset($this->_fields["Division"]); // now unset it so that we can convert object to array
			$this->_fields["Division"][] = $prev_value;
			$this->_fields["Division"][] = new DtDivision($division_data);
		} else if (isset($this->_fields["Division"]) && is_array($this->_fields["Division"]))
			$this->_fields["Division"][] = new DtDivision($division_data);
		else
			$this->_fields["Division"] = new DtDivision($division_data);
	}

	/**
	 * Add multiple divisions to object
	 * 
	 * @param array $division_data division information
	 */
	function add_divisions($division_data) {
		if (!empty($division_data)) {
			foreach ($division_data as $value)
				$this->add_division($value);
		}
	}

	/**
	 * Add single allowed_service to object
	 * 
	 * @param array $allowed_service_data allowed_service information
	 */
	function add_allowed_service($allowed_service_data) {
		if (isset($this->_fields["AllowedService"]) && !is_array($this->_fields["AllowedService"])) {
			$prev_value = $this->AllowedService; // restore previous value
			unset($this->_fields["AllowedService"]); // now unset it so that we can convert object to array
			$this->_fields["AllowedService"][] = $prev_value;
			$this->_fields["AllowedService"][] = new DtAllowedService($allowed_service_data);
		} else if (isset($this->_fields["AllowedService"]) && is_array($this->_fields["AllowedService"]))
			$this->_fields["AllowedService"][] = new DtAllowedService($allowed_service_data);
		else
			$this->_fields["AllowedService"] = new DtAllowedService($allowed_service_data);
	}

	/**
	 * Add multiple allowed_services to object
	 * 
	 * @param array $allowed_service_data allowed_service information
	 */
	function add_allowed_services($allowed_service_data) {
		if (!empty($allowed_service_data)) {
			foreach ($allowed_service_data as $value)
				$this->add_allowed_service($value);
		}
	}

	/**
	 * Add single case_load to object
	 * 
	 * @param array $case_load_data case_load information
	 */
	function add_case_load($case_load_data) {
		if (isset($this->_fields["CaseLoad"]) && !is_array($this->_fields["CaseLoad"])) {
			$prev_value = $this->CaseLoad; // restore previous value
			unset($this->_fields["CaseLoad"]); // now unset it so that we can convert object to array
			$this->_fields["CaseLoad"][] = $prev_value;
			$this->_fields["CaseLoad"][] = new DtCaseLoad($case_load_data);
		} else if (isset($this->_fields["CaseLoad"]) && is_array($this->_fields["CaseLoad"]))
			$this->_fields["CaseLoad"][] = new DtCaseLoad($case_load_data);
		else
			$this->_fields["CaseLoad"] = new DtCaseLoad($case_load_data);
	}

	/**
	 * Add multiple case_loads to object
	 * 
	 * @param array $case_load_data case_load information
	 */
	function add_case_loads($case_load_data) {
		if (!empty($case_load_data)) {
			foreach ($case_load_data as $value)
				$this->add_case_load($value);
		}
	}

	/**
	 * Add single level to object
	 * 
	 * @param array $level_data level information
	 */
	function add_level($level_data) {
		if (isset($this->_fields["Level"]) && !is_array($this->_fields["Level"])) {
			$prev_value = $this->Level; // restore previous value
			unset($this->_fields["Level"]); // now unset it so that we can convert object to array
			$this->_fields["Level"][] = $prev_value;
			$this->_fields["Level"][] = new DtLevel($level_data);
		} else if (isset($this->_fields["Level"]) && is_array($this->_fields["Level"]))
			$this->_fields["Level"][] = new DtLevel($level_data);
		else
			$this->_fields["Level"] = new DtLevel($level_data);
	}

	/**
	 * Add multiple levels to object
	 * 
	 * @param array $level_data level information
	 */
	function add_levels($level_data) {
		if (!empty($level_data)) {
			foreach ($level_data as $value)
				$this->add_level($value);
		}
	}

	/**
	 * Add single official_title to object
	 * 
	 * @param array $official_title_data official_title information
	 */
	function add_official_title($official_title_data) {
		if (isset($this->_fields["OfficialTitle"]) && !is_array($this->_fields["OfficialTitle"])) {
			$prev_value = $this->OfficialTitle; // restore previous value
			unset($this->_fields["OfficialTitle"]); // now unset it so that we can convert object to array
			$this->_fields["OfficialTitle"][] = $prev_value;
			$this->_fields["OfficialTitle"][] = new DtOfficialTitle($official_title_data);
		} else if (isset($this->_fields["OfficialTitle"]) && is_array($this->_fields["OfficialTitle"]))
			$this->_fields["OfficialTitle"][] = new DtOfficialTitle($official_title_data);
		else
			$this->_fields["OfficialTitle"] = new DtOfficialTitle($official_title_data);
	}

	/**
	 * Add multiple official_titles to object
	 * 
	 * @param array $official_title_data official_title information
	 */
	function add_official_titles($official_title_data) {
		if (!empty($official_title_data)) {
			foreach ($official_title_data as $value)
				$this->add_official_title($value);
		}
	}

	/**
	 * Add single employee_working_hour to object
	 * 
	 * @param array $employee_working_hour_data employee_working_hour information
	 */
	function add_employee_working_hour($employee_working_hour_data) {
		if (isset($this->_fields["EmployeeWorkingHour"]) && !is_array($this->_fields["EmployeeWorkingHour"])) {
			$prev_value = $this->EmployeeWorkingHour; // restore previous value
			unset($this->_fields["EmployeeWorkingHour"]); // now unset it so that we can convert object to array
			$this->_fields["EmployeeWorkingHour"][] = $prev_value;
			$this->_fields["EmployeeWorkingHour"][] = new DtEmployeeWorkingHour($employee_working_hour_data);
		} else if (isset($this->_fields["EmployeeWorkingHour"]) && is_array($this->_fields["EmployeeWorkingHour"]))
			$this->_fields["EmployeeWorkingHour"][] = new DtEmployeeWorkingHour($employee_working_hour_data);
		else
			$this->_fields["EmployeeWorkingHour"] = new DtEmployeeWorkingHour($employee_working_hour_data);
	}

	/**
	 * Add multiple employee_working_hours to object
	 * 
	 * @param array $employee_working_hour_data employee_working_hour information
	 */
	function add_employee_working_hours($employee_working_hour_data) {
		if (!empty($employee_working_hour_data)) {
			foreach ($employee_working_hour_data as $value)
				$this->add_employee_working_hour($value);
		}
	}

	/**
	 * Add license_type to object
	 * 
	 * @param array $license_type_data license_type information
	 */
	function add_license_type($license_type_data) {

		$this->_fields["LicenseType"] = new DtLicenseType($license_type_data);
	}

	/**
	 * Get license_type
	 * 
	 * @return array
	 */
	function get_license_type() {

		if ($this->_fields['LicenseType'] instanceof DtLicenseType)
			return $this->_fields['LicenseType']->get_field();
	}

	/**
	 * Get allowed services
	 * 
	 * @return array
	 */
	function get_allowed_services() {
		$data = array();
		if (is_array($this->_fields['AllowedService'])) {
			foreach ($this->_fields['AllowedService'] as $allowed_service) {
				$data[$allowed_service->id] = $allowed_service->get_field();
			}
		} else if ($this->_fields['AllowedService'] instanceof DtAllowedService) {
			$data[$this->_fields['AllowedService']->id] = $this->_fields['AllowedService']->get_field();
		}
		return $data;
	}

	/**
	 * Get case_loads
	 * 
	 * @return array
	 */
	function get_case_loads() {
		$data = array();
		if (is_array($this->_fields['CaseLoad'])) {
			foreach ($this->_fields['CaseLoad'] as $case_load) {
				$data[$case_load->id] = $case_load->get_field();
			}
		} else if ($this->_fields['CaseLoad'] instanceof DtCaseLoad) {
			$data[$this->_fields['CaseLoad']->id] = $this->_fields['CaseLoad']->get_field();
		}
		return $data;
	}

	/**
	 * Get divisions
	 * 
	 * @return array
	 */
	function get_divisions() {
		$data = array();
		if (is_array($this->_fields['Division'])) {
			foreach ($this->_fields['Division'] as $division) {
				$data[$division->id] = $division->get_field();
			}
		} else if ($this->_fields['Division'] instanceof DtDivision) {
			$data[$this->_fields['Division']->id] = $this->_fields['Division']->get_field();
		}
		return $data;
	}

	/**
	 * Get levels
	 * 
	 * @return array
	 */
	function get_levels() {
		$data = array();
		if (is_array($this->_fields['Level'])) {
			foreach ($this->_fields['Level'] as $level) {
				$data[$level->id] = $level->get_field();
			}
		} else if ($this->_fields['Level'] instanceof DtLevel) {
			$data[$this->_fields['Level']->id] = $this->_fields['Level']->get_field();
		}
		return $data;
	}

	/**
	 * Get official_titles
	 * 
	 * @return array
	 */
	function get_official_titles() {
		$data = array();
		if (is_array($this->_fields['OfficialTitle'])) {
			foreach ($this->_fields['OfficialTitle'] as $official_title) {
				$data[$official_title->id] = $official_title->get_field();
			}
		} else if ($this->_fields['OfficialTitle'] instanceof DtOfficialTitle) {
			$data[$this->_fields['OfficialTitle']->id] = $this->_fields['OfficialTitle']->get_field();
		}
		return $data;
	}

	/**
	 * Get employee_working_hours
	 * 
	 * @return array
	 */
	function get_employee_working_hours() {
		$data = array();
		if (is_array($this->_fields['EmployeeWorkingHour'])) {
			foreach ($this->_fields['EmployeeWorkingHour'] as $employee_working_hour) {
				$data[$employee_working_hour->id] = $employee_working_hour->get_field();
			}
		} else if ($this->_fields['EmployeeWorkingHour'] instanceof DtEmployeeWorkingHour) {
			$data[$this->_fields['EmployeeWorkingHour']->id] = $this->_fields['EmployeeWorkingHour']->get_field();
		}
		return $data;
	}

	/**
	 * Get employee_working_hour object at given row 
	 * 
	 * @return object
	 */
	function get_emp_working_hr_obj_at_row($key) {

		if (is_array($this->_fields['EmployeeWorkingHour']) &&
				$this->_fields['EmployeeWorkingHour'][$key] instanceof DtEmployeeWorkingHour) {
			return $this->_fields['EmployeeWorkingHour'][$key];
		} else if ($this->_fields['EmployeeWorkingHour'] instanceof DtEmployeeWorkingHour) {
			return $this->_fields['EmployeeWorkingHour'];
		}
	}

	/**
	 * Get division object at given row 
	 * 
	 * @return object
	 */
	function get_division_obj_at_row($key) {

		if (is_array($this->_fields['Division']) &&
				$this->_fields['Division'][$key] instanceof DtDivision) {
			return $this->_fields['Division'][$key];
		} else if ($this->_fields['Division'] instanceof DtDivision) {
			return $this->_fields['Division'];
		}
	}

	/**
	 * Add single program to object
	 * 
	 * @param array $program_data program information
	 */
	function add_program($program_data) {
		if (isset($this->_fields["Program"]) && !is_array($this->_fields["Program"])) {
			$prev_value = $this->Program; // restore previous value
			unset($this->_fields["Program"]); // now unset it so that we can convert object to array
			$this->_fields["Program"][] = $prev_value;
			$this->_fields["Program"][] = new DtProgram($program_data);
		} else if (isset($this->_fields["Program"]) && is_array($this->_fields["Program"]))
			$this->_fields["Program"][] = new DtProgram($program_data);
		else
			$this->_fields["Program"] = new DtProgram($program_data);
	}

	/**
	 * Get programs
	 * 
	 * @return array
	 */
	function get_programs() {
		$data = array();
		if (is_array($this->_fields['Program'])) {
			foreach ($this->_fields['Program'] as $program) {
				$data[] = $program->get_field();
			}
		} else if ($this->_fields['Program'] instanceof DtProgram) {
			$data = $this->_fields['Program']->get_field();
		}
		return $data;
	}

	/**
	 * Add single modality to object
	 * 
	 * @param array $modality_data modality information
	 */
	function add_modality($modality_data) {
		if (isset($this->_fields["Modality"]) && !is_array($this->_fields["Modality"])) {
			$prev_value = $this->Modality; // restore previous value
			unset($this->_fields["Modality"]); // now unset it so that we can convert object to array
			$this->_fields["Modality"][] = $prev_value;
			$this->_fields["Modality"][] = new DtModality($modality_data);
		} else if (isset($this->_fields["Modality"]) && is_array($this->_fields["Modality"]))
			$this->_fields["Modality"][] = new DtModality($modality_data);
		else
			$this->_fields["Modality"] = new DtModality($modality_data);
	}

	/**
	 * Get modalities
	 * 
	 * @return array
	 */
	function get_modalities() {
		$data = array();
		if (is_array($this->_fields['Modality'])) {
			foreach ($this->_fields['Modality'] as $modality) {
				$data[] = $modality->get_field();
			}
		} else if ($this->_fields['Modality'] instanceof DtModality) {
			$data = $this->_fields['Modality']->get_field();
		}
		return $data;
	}

	/**
	 * Add single mdp activity list to object
	 * 
	 * @param array $mpdActivityList_data modality information
	 */
	function add_mpdActivityList($mpdActivityList_data) {
		if (isset($this->_fields["MpdActivityList"]) && !is_array($this->_fields["MpdActivityList"])) {
			$prev_value = $this->MpdActivityList; // restore previous value
			unset($this->_fields["MpdActivityList"]); // now unset it so that we can convert object to array
			$this->_fields["MpdActivityList"][] = $prev_value;
			$this->_fields["MpdActivityList"][] = new DtMpdActivityList($mpdActivityList_data);
		} else if (isset($this->_fields["MpdActivityList"]) && is_array($this->_fields["MpdActivityList"]))
			$this->_fields["MpdActivityList"][] = new DtMpdActivityList($mpdActivityList_data);
		else
			$this->_fields["MpdActivityList"] = new DtMpdActivityList($mpdActivityList_data);
	}

	/**
	 * Get mpd Activity Lists
	 * 
	 * @return array
	 */
	function get_mpdActivityLists() {
		$data = array();
		if (is_array($this->_fields['MpdActivityList'])) {
			foreach ($this->_fields['MpdActivityList'] as $mpdActivityList) {
				$data[] = $mpdActivityList->get_field();
			}
		} else if ($this->_fields['MpdActivityList'] instanceof DtMpdActivityList) {
			$data = $this->_fields['MpdActivityList']->get_field();
		}
		return $data;
	}

	/**
	 * Add single mdp activity to object
	 * 
	 * @param array $mpdActivity_data mpd Activity information
	 */
	function add_mpdActivity($mpdActivity_data) {
		if (isset($this->_fields["MpdActivity"]) && !is_array($this->_fields["MpdActivity"])) {
			$prev_value = $this->MpdActivity; // restore previous value
			unset($this->_fields["MpdActivity"]); // now unset it so that we can convert object to array
			$this->_fields["MpdActivity"][] = $prev_value;
			$this->_fields["MpdActivity"][] = new DtMpdActivity($mpdActivity_data);
		} else if (isset($this->_fields["MpdActivity"]) && is_array($this->_fields["MpdActivity"]))
			$this->_fields["MpdActivity"][] = new DtMpdActivity($mpdActivity_data);
		else
			$this->_fields["MpdActivity"] = new DtMpdActivity($mpdActivity_data);
	}

	/**
	 * Get mpd Activity
	 * 
	 * @return array
	 */
	function get_mpdActivities() {
		$data = array();
		if (is_array($this->_fields['MpdActivity'])) {
			foreach ($this->_fields['MpdActivity'] as $mpdActivity) {
				$data[] = $mpdActivity->get_field();
			}
		} else if ($this->_fields['MpdActivity'] instanceof DtMpdActivity) {
			$data = $this->_fields['MpdActivity']->get_field();
		}
		return $data;
	}

	/**
	 * Add single vpd activity to object
	 * 
	 * @param array $vpdActivity_data vpd Activity information
	 */
	function add_vpdActivity($vpdActivity_data) {
		if (isset($this->_fields["VpdActivity"]) && !is_array($this->_fields["VpdActivity"])) {
			$prev_value = $this->VpdActivity; // restore previous value
			unset($this->_fields["VpdActivity"]); // now unset it so that we can convert object to array
			$this->_fields["VpdActivity"][] = $prev_value;
			$this->_fields["VpdActivity"][] = new DtVpdActivity($vpdActivity_data);
		} else if (isset($this->_fields["VpdActivity"]) && is_array($this->_fields["VpdActivity"]))
			$this->_fields["VpdActivity"][] = new DtVpdActivity($vpdActivity_data);
		else
			$this->_fields["VpdActivity"] = new DtVpdActivity($vpdActivity_data);
	}

	/**
	 * Get vpd Activity
	 * 
	 * @return array
	 */
	function get_vpdActivities() {
		$data = array();
		if (is_array($this->_fields['VpdActivity'])) {
			foreach ($this->_fields['VpdActivity'] as $vpdActivity) {
				$data[] = $vpdActivity->get_field();
			}
		} else if ($this->_fields['VpdActivity'] instanceof DtVpdActivity) {
			$data = $this->_fields['VpdActivity']->get_field();
		}
		return $data;
	}

	/**
	 * Add single vpd activity type to object
	 * 
	 * @param array $vpdActivityType_data vpd Activity information
	 */
	function add_vpdActivityType($vpdActivityType_data) {
		if (isset($this->_fields["VpdActivityType"]) && !is_array($this->_fields["VpdActivityType"])) {
			$prev_value = $this->VpdActivityType; // restore previous value
			unset($this->_fields["VpdActivityType"]); // now unset it so that we can convert object to array
			$this->_fields["VpdActivityType"][] = $prev_value;
			$this->_fields["VpdActivityType"][] = new DtVpdActivityType($vpdActivityType_data);
		} else if (isset($this->_fields["VpdActivityType"]) && is_array($this->_fields["VpdActivityType"]))
			$this->_fields["VpdActivityType"][] = new DtVpdActivityType($vpdActivityType_data);
		else
			$this->_fields["VpdActivityType"] = new DtVpdActivityType($vpdActivityType_data);
	}

	/**
	 * Get vpd Activity types
	 * 
	 * @return array
	 */
	function get_vpdActivityTypes() {
		$data = array();
		if (is_array($this->_fields['VpdActivityType'])) {
			foreach ($this->_fields['VpdActivityType'] as $vpdActivityType) {
				$data[] = $vpdActivityType->get_field();
			}
		} else if ($this->_fields['VpdActivityType'] instanceof DtVpdActivityType) {
			$data = $this->_fields['VpdActivityType']->get_field();
		}
		return $data;
	}

	/**
	 * Add single sponsor to object
	 * 
	 * @param array $sponsor_data sponsor information
	 */
	function add_sponsor($sponsor_data) {
		if (isset($this->_fields["Sponsor"]) && !is_array($this->_fields["Sponsor"])) {
			$prev_value = $this->Sponsor; // restore previous value
			unset($this->_fields["Sponsor"]); // now unset it so that we can convert object to array
			$this->_fields["Sponsor"][] = $prev_value;
			$this->_fields["Sponsor"][] = new DtSponsor($sponsor_data);
		} else if (isset($this->_fields["Sponsor"]) && is_array($this->_fields["Sponsor"]))
			$this->_fields["Sponsor"][] = new DtSponsor($sponsor_data);
		else
			$this->_fields["Sponsor"] = new DtSponsor($sponsor_data);
	}

	/**
	 * Get sponsor
	 * 
	 * @return array
	 */
	function get_sponsors() {
		$data = array();
		if (is_array($this->_fields['Sponsor'])) {
			foreach ($this->_fields['Sponsor'] as $sponsor) {
				$data[] = $sponsor->get_field();
			}
		} else if ($this->_fields['Sponsor'] instanceof DtSponsor) {
			$data = $this->_fields['Sponsor']->get_field();
		}
		return $data;
	}

	/**
	 * Add single provider to object
	 * 
	 * @param array $provider_data provider information
	 */
	function add_provider($provider_data) {
		if (isset($this->_fields["Provider"]) && !is_array($this->_fields["Provider"])) {
			$prev_value = $this->Provider; // restore previous value
			unset($this->_fields["Provider"]); // now unset it so that we can convert object to array
			$this->_fields["Provider"][] = $prev_value;
			$this->_fields["Provider"][] = new DtProvider($provider_data);
		} else if (isset($this->_fields["Provider"]) && is_array($this->_fields["Provider"]))
			$this->_fields["Provider"][] = new DtProvider($provider_data);
		else
			$this->_fields["Provider"] = new DtProvider($provider_data);
	}

	/**
	 * Get provider
	 * 
	 * @return array
	 */
	function get_providers() {
		$data = array();
		if (is_array($this->_fields['Provider'])) {
			foreach ($this->_fields['Provider'] as $provider) {
				$data[] = $provider->get_field();
			}
		} else if ($this->_fields['Provider'] instanceof DtProvider) {
			$data = $this->_fields['Provider']->get_field();
		}
		return $data;
	}

	function _get_relative_mpd_activities_url() {
		return 'mpd_activities/index/' . $this->id;
	}

	function _get_relative_vpd_activities_url() {
		return 'vpd_activities/index/' . $this->id;
	}

	/**
	 * Add single employee_working_hour to object
	 * 
	 * @param array $annual_vacation_plan annual vacation plan
	 */
	function add_annual_vacation_plan($annual_vacation_plan) {

		if (!empty($annual_vacation_plan['id'])) {
			if (isset($this->_fields["AnnualVacationPlan"]) && !is_array($this->_fields["AnnualVacationPlan"])) {
				$prev_value = $this->AnnualVacationPlan; // restore previous value
				unset($this->_fields["AnnualVacationPlan"]); // now unset it so that we can convert object to array
				$this->_fields["AnnualVacationPlan"][$prev_value->id] = $prev_value;
				$this->_fields["AnnualVacationPlan"][$annual_vacation_plan['id']] = new DtAnnualVacationPlan($annual_vacation_plan);
			} else if (isset($this->_fields["AnnualVacationPlan"]) && is_array($this->_fields["AnnualVacationPlan"]))
				$this->_fields["AnnualVacationPlan"][$annual_vacation_plan['id']] = new DtAnnualVacationPlan($annual_vacation_plan);
			else
				$this->_fields["AnnualVacationPlan"] = new DtAnnualVacationPlan($annual_vacation_plan);
		}
	}

	/**
	 * Add multiple annual_vacation_plan to object
	 * 
	 * @param array $annual_vacation_plans annual vacation plans
	 */
	function add_annual_vacation_plans($annual_vacation_plans) {
		if (!empty($annual_vacation_plans)) {
			foreach ($annual_vacation_plans as $value)
				$this->add_annual_vacation_plan($value);
		}
	}

	/**
	 * Get annual vacation plan object at given row 
	 * 
	 * @return object
	 */
	function get_annual_vacation_plan_obj_at_row($key) {

		if (empty($key))
			return false;

		if (is_array($this->_fields['AnnualVacationPlan']) &&
				$this->_fields['AnnualVacationPlan'][$key] instanceof DtAnnualVacationPlan) {
			return $this->_fields['AnnualVacationPlan'][$key];
		} else if ($this->_fields['AnnualVacationPlan'] instanceof DtAnnualVacationPlan) {
			return $this->_fields['AnnualVacationPlan'];
		}

		return false;
	}

	/**
	 * Add single employee_working_hour to object
	 * 
	 * @param array $appointment appointment
	 */
	function add_appointment($appointment) {
		if ($appointment['id'] > 0) {
			if (isset($this->_fields["Appointment"]) && !is_array($this->_fields["Appointment"])) {
				$prev_value = $this->Appointment; // restore previous value
				unset($this->_fields["Appointment"]); // now unset it so that we can convert object to array
				$this->_fields["Appointment"][$prev_value->id] = $prev_value;
				$this->_fields["Appointment"][$appointment['id']] = new DtAppointment($appointment);
			} else if (isset($this->_fields["Appointment"]) && is_array($this->_fields["Appointment"]))
				$this->_fields["Appointment"][$appointment['id']] = new DtAppointment($appointment);
			else
				$this->_fields["Appointment"] = new DtAppointment($appointment);
		}
	}

	/**
	 * Add multiple appointment to object
	 * 
	 * @param array $appointments appointments
	 */
	function add_appointments($appointments) {
		if (!empty($appointments)) {
			foreach ($appointments as $value)
				$this->add_appointment($value);
		}
	}

	/**
	 * Get appointment object at given row 
	 * 
	 * @return object
	 */
	function get_appointment_obj_at_row($key) {

		if (empty($key))
			return false;

		if (is_array($this->_fields['Appointment']) &&
				$this->_fields['Appointment'][$key] instanceof DtAppointment) {
			return $this->_fields['Appointment'][$key];
		} else if ($this->_fields['Appointment'] instanceof DtAppointment) {
			return $this->_fields['Appointment'];
		}
	}

}

?>
