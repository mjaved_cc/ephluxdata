<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class User extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'username';
	var $name = 'User';
	var $belongsTo = array(
		'Group'
	);
	public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));
	var $hasMany = array(
		'UserSocialAccount'
	);
	var $validate = array(
		'first_name' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minLength', 3),
				'message' => 'First Name must have atleast 3 characters',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Firstname must only contain letters and numbers.'
			)
		),
		'last_name' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minLength', 3),
				'message' => 'Last Name must have atleast 3 characters',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Lastname must only contain letters and numbers.'
			)
		),
		'password' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minlength', 8),
				'message' => 'Password must be between 8 to 15 characters'
			)
		),
		'username' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please supply a valid email address.',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'isUnique',
				'message' => 'Email already exists'
			)
		)
	);

	/**
	 * authorizes user credentials
	 * @param varchar $email email address
	 * @param varchar $password password
	 */
	function verify_login($email, $password) {

		$result = $this->query('call verify_login("' . $email . '", "' . $password . '")');

		if (!empty($result))
			return $result['0'];

		return false;
	}

	function get_details_by_id($id) {
		$result = $this->query('call get_user_details_by_user_id("' . $id . '")');

		return $result;
	}

//	function verify_email($email) {
//
//		$verify = $this->find('count', array(
//			'conditions' => array('username' => $email)
//				));
//		return $verify;
//	}
//	function get_user_info($email) {
//		$user_info = $this->find('first', array('conditions' => array('username' => $email)));
//		return $user_info;
//	}

	function reset_password($password, $user_id) {

		$this->query('call reset_password(
			"' . $password . '",
			"' . $user_id . '",
			@status )');

		return $this->get_status();
	}

	/**
	 * Get details of users by email
	 * @param array $query_params WHERE clause
	 * @return array result
	 */
	function get_by_email($email) {

		$result = $this->query('call get_user_by_email("' . $email . '")');

		if (!empty($result))
			return $result['0'];

		return false;
	}

	/**
	 * Create new user for given params
	 * 
	 * @param array $user_details User details
	 * @return type Description
	 */
	function create_user($user_details) {
		$this->query('call create_user(
			"' . $user_details['first_name'] . '", 
			"' . $user_details['last_name'] . '", 
			"' . $user_details['username'] . '", 
			"' . $user_details['password'] . '",
			"' . $user_details['group_id'] . '",
			"' . $user_details['created'] . '",
			"' . $user_details['ip_address'] . '",
			@status )');

		return $this->get_status();
	}

	public function beforeSave($options = array()) {
		if (!empty($this->data['User']['password']))
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		return true;
	}

	/**
	 * This is for Group only ACL.
	 * 
	 * @link http://stackoverflow.com/questions/6154285/aros-table-in-cakephp-is-still-including-users-even-after-bindnode
	 * 
	 * @param array $user User details
	 * @return array Group info
	 */
	function bindNode($user) {
		return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}

	public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['User']['group_id'])) {
			$groupId = $this->data['User']['group_id'];
		} else {
			$groupId = $this->field('group_id');
		}
		if (!$groupId) {
			return null;
		} else {
			return array('Group' => array('id' => $groupId));
		}
	}

	/**
	 * Signup facebook. Save details in User & UserSocialAcount tables.
	 * 
	 * @param xml $xml_data user details
	 */
	function signup_with_social_account($xml_data) { 
		
		$this->query("call signup_with_social_account(
			 '". $xml_data . "',
			@status )");

		return $this->get_status();
	}

}
