<?php

App::uses('Component', 'Controller');

/**
 * Javascript & Css Manager. 
 */
class JCManagerComponent extends Component {

	private $_controller;
	private $_js_list;
	private $_css_list;

	function startup(Controller $controller) {
		$this->_controller = $controller;

		if ($this->_controller->layout == LAYOUT_ADMIN) {
			$this->_js_list = $this->get_default_backend_js();
			$this->_css_list = $this->get_default_backend_css();
		} else { 
			$this->_js_list = $this->get_default_js();
			$this->_css_list = $this->get_default_css();
		}
	}

	/**
	 * Get all JS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_js() {

		return array(
			// Modernizr and style switcher 
			JS_LIBS_PATH . 'modernizr.custom.15286',
			JS_LIBS_PATH . 'jquery.tools.min', // include jquery & all tools 
			JS_LIBS_PATH . 'jquery.bxslider.min', // bxSlider
			JS_LIBS_PATH . 'jquery-ui',
			// JS_LIBS_PATH . 'google_map_api',// https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places
			'google_map_api',
			'script',
			'app'
		);
	}

	/**
	 * Get all Backend JS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_js() {

		return array(
			'admin/jquery.tools.min', // Import jquery library and jquery tools from a single file
			'admin/preloadCssImages.jQuery_v5', // Preload script for css and images
			'admin/script',
			'app',
			'jquery.validate'
		);
	}

	/**
	 * Get all CSS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_css() {

		return array(
			'style', 'custom'
		);
	}

	/**
	 * Get all backend CSS file(s), template dependent in short.
	 * 
	 * @return array 
	 */
	function get_default_backend_css() {

		return array(
			'admin/style', 'admin/simple', 'admin/invalid'
		);
	}

	/**
	 * Push JS file(s) in stack/queue
	 * 
	 * @param string|array $file_names JS file name
	 * @return void
	 */
	function add_js($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			$this->_js_list = array_merge($this->_js_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_js_list[] = $file_names;
		}
	}

	/**
	 * Push CSS file(s) in stack/queue
	 * 
	 * @param string|array $file_names CSS file name
	 * @return void
	 */
	function add_css($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			$this->_css_list = array_merge($this->_css_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			$this->_css_list[] = $file_names;
		}
	}

	/**
	 * Remove JS file(s) in stack/queue
	 * 
	 * @param string|array $file_names CSS file name
	 * @return void
	 */
	function remove_js($file_names = array()) {

		if (!empty($file_names) && is_array($file_names)) {
			foreach ($file_names as $file_name) {
				foreach (array_keys($this->_js_list, $file_name) as $key) {
					unset($this->_js_list[$key]);
				}
			}
			$this->_css_list = array_merge($this->_css_list, $file_names);
		} else if (!empty($file_names) && is_string($file_names)) {
			foreach (array_keys($this->_js_list, $file_names) as $key) {
				unset($this->_js_list[$key]);
			}
		}
	}

	/**
	 * Get all required JS & CSS file(s) 
	 * 
	 * @return array
	 */
	function get() {
		return array($this->_js_list, $this->_css_list);
	}

}

?>
