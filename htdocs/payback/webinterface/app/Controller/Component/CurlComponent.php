<?php

class CurlComponent extends Component {

	public $controller = null;
	public $url, $data;

	function startup(Controller $controller) {
		$this->controller = $controller;
	}

	/*
	 * @desc: http POST request using Curl.
	 * @params: null
	 * 
	 * @return : On success, "errno" is 0, "http_code" is 200, and "content" contains the web page.
	 * 
	 * On an error with a bad URL, unknown host, timeout, or redirect loop, 
	 * "errno" has a non-zero error code and "errmsg" has an error message (see the CURL error code list).
	 * 
	 * On an error with a missing web page or insufficient permissions, 
	 * "errno" is 0, "http_code" has a non-200 HTTP status code, and "content" contains the site’s error message page
	 */

	function post() {

		//url-ify the data for the POST
		$query_string = $this->_http_build_query();

		// set required POST option for CURL
		$options = array(
			CURLOPT_URL => $this->url, // set the url
			CURLOPT_POST => count($this->data), // number of POST params
			CURLOPT_POSTFIELDS => $query_string, // post data
			CURLOPT_RETURNTRANSFER => true, // return web page
			CURLOPT_HEADER => false, // don't return headers
			CURLOPT_ENCODING => "", // handle all encodings
			CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			CURLOPT_TIMEOUT => 120, // timeout on response	
			CURLOPT_USERAGENT => getenv('HTTP_USER_AGENT')   // timeout on response	
		);

		//open connection
		$ch = curl_init();
		curl_setopt_array($ch, $options);

		//execute post
		$content = curl_exec($ch);
		$error_code = curl_errno($ch);
		$error_msg = curl_error($ch);
		$header = curl_getinfo($ch);
		//close connection
		curl_close($ch);

		$header['error_code'] = $error_code;
		$header['error_msg'] = $error_msg;
		$header['content'] = $content;
		return $header;
	}

	/**
	 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
	 * array containing the HTTP server response header fields and content.
	 */
	function get() {
		
		//url-ify the data for the POST
		$query_string = $this->_http_build_query();
		
		// set required POST option for CURL
		$options = array(
			CURLOPT_URL => $this->url . '?' . $query_string , // set the url			
			CURLOPT_RETURNTRANSFER => true, // return web page
			CURLOPT_HEADER => false, // don't return headers
			CURLOPT_ENCODING => "", // handle all encodings
			CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			CURLOPT_TIMEOUT => 120   // timeout on response	
		);

		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		$error_code = curl_errno($ch);
		$error_msg = curl_error($ch);
		$header = curl_getinfo($ch);
		//close connection
		curl_close($ch);

		$header['error_code'] = $error_code;
		$header['error_msg'] = $error_msg;
		$header['content'] = $content;
		return $header;
	}

	/**
	 * Generate URL-encoded query string
	 * 
	 * @return string
	 */
	private function _http_build_query() {
		if (!empty($this->data) && is_array($this->data))
			return http_build_query($this->data);
		return ;
	}

}

?>