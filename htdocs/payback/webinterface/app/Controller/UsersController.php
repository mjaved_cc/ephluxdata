<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

	public $components = array('Facebook', 'Twitter', 'ApiUser', 'Curl', 'WrapperXML');
	public $name = 'Users';
	public $uses = array('User', 'Tmp', 'UserSocialAccount');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'User.created' => 'desc'
		),
		'conditions' => array('User.status' => STATUS_ACTIVE)
	);

	function beforeFilter() {
		parent::beforeFilter();

		$this->layout = LAYOUT_ADMIN;
	}

	/**
	 * index method
	 *
	 * @return void
	 */
//	public function index() {
//
//		$this->User->recursive = 0;
//
//		$data = $this->paginate();
//		
//		$users = array();
//		foreach ($data as $key => $user) {
//			$obj_user = new DtUser($user[$this->modelClass]);
//			$obj_grp = new DtGroup($user['Group']);
//			$users[$key][$this->modelClass] = $obj_user->get_field();
//			$users[$key]['Group'] = $obj_grp->get_field();
//		}
//		
//		$this->set(compact('users'));
//	}


	function index($start = null) {

//		if ($this->request->is('post')) {
//			$message = $this->data['ContactUs'];
//			$this->App->send_email(
//					EMAIL_NOREPLY, SITE_NAME . ' - Contact Us', $message, $this->data['ContactUs']['email'], 'contactus'
//			);
//			$this->redirect(BASE_URL . '/home/index');
//		}
		$this->JCManager->add_js('payback_webservice');
		$data = array();
		$this->Curl->url = PAYBACK_WEBSERVICE_URI;
		$this->Curl->data = array(
			'method' => PAYBACK_WEBSERVICE_METHOD_GET_ALL_ENTRIES
		);
		$response = $this->Curl->get();

		/*		 * ****************************** */

		// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
		// $response['http_code'] != 200 > error: no page, no permissions, no service

		/*		 * ****************************** */

		if ($response['error_code'] == ECODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
			$content = $this->App->decoding_json($response['content']);

			if (isset($content['code']) && $content['code'] == ECODE_SUCCESS) {
				$data = $content['body'];
			} else { // in case of failure
			}
		} else { // in case of failure
		}


		$count = count($data);

		if (empty($start)) {

			$start = 1;
		}

		$start = ($start - 1) * 10;
		$end = $start + 10;

		if ($count <= $end) {

			$end = $count;
		}



		$pages = $count / 10;

		$pages = ceil($pages);

		$this->set(compact('pages'));
		$this->set(compact('data'));
		$this->set(compact('start'));
		$this->set(compact('end'));
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($recordNo) {
//		$this->User->id = $id;
//		if (!$this->User->exists()) {
//			throw new NotFoundException(__('Invalid user'));
//		}
//
//		debug($this->User->read(null, $id));
//		exit;
//		$this->set('user', $this->User->read(null, $id));


		$this->JCManager->add_js('payback_webservice');
		$data = array();
		$this->Curl->url = PAYBACK_WEBSERVICE_URI;
		$this->Curl->data = array(
			'method' => PAYBACK_WEBSERVICE_METHOD_GET_ALL_ENTRIES
		);
		$response = $this->Curl->get();

		/*		 * ****************************** */

		// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
		// $response['http_code'] != 200 > error: no page, no permissions, no service

		/*		 * ****************************** */

		if ($response['error_code'] == ECODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
			$content = $this->App->decoding_json($response['content']);

			if (isset($content['code']) && $content['code'] == ECODE_SUCCESS) {
				$data = $content['body'];
			}
		}

		$data = $data[$recordNo];

		$this->set(compact('data'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}

		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}

		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($nodeIdentifier) {

		$data = array();
		$this->Curl->url = PAYBACK_WEBSERVICE_URI . '?method=4';
		$this->Curl->data = array(
			'nodeIdentifier' => $nodeIdentifier
		);
		$response = $this->Curl->post();
		/*		 * ****************************** */

		// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
		// $response['http_code'] != 200 > error: no page, no permissions, no service

		/*		 * ****************************** */
		

		if ($response['error_code'] == ECODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
			$content = $this->App->decoding_json($response['content']);

			if (isset($content['code']) && $content['code'] == ECODE_SUCCESS) {
				$data = $content['body'];
			}
		}

		/*

		  $doc = new DOMDocument;
		  $doc->load( BASE_URL.'/payback/paybackdb.xml');

		  $thedocument = $doc->documentElement;

		  $list = $thedocument->getElementsByTagName('record');

		  $nodeToRemove = null;
		  foreach ($list as $domElement) {
		  $attrValue = $domElement->nodeValue;


		  if (strpos($attrValue, $recordNo) == true) {
		  $nodeToRemove = $domElement;

		  }
		  }

		  if ($nodeToRemove != null) {
		  $thedocument->removeChild($nodeToRemove);
		  }

		  $doc->save('../../../paybackdb.xml');
		 */
		//$this->redirect(array("controller" => "users", "action" => "index"));
		header('Location: ' . BASE_URL . '/users/index');
	}

	/**
	 * User login 
	 * @return void
	 * Error code range : 1061-1090
	 */
	function login() {

		$this->layout = LAYOUT_LOGIN;

		$this->User->set($this->data);

		if (!empty($this->data)) {
			$login_info = $this->data['User'];

			$api_response = $this->ApiUser->login($login_info);
			$response = $this->App->decoding_json($api_response);

			//pr($response['header']['code']); exit;

			if ($response['header']['code'] == ECODE_SUCCESS) {
				//$this->redirect(array('controller'=>'users','action'=>'index'));
				//$this->redirect(array('action' => 'index')); 
				header('Location: ' . BASE_URL . '/users/index');
			} else {
				$this->set('api_response', $api_response);
			}
		}
	}

	function logout() {
		$this->layout = false;
		$this->Session->destroy();
		//$this->redirect($this->Auth->logout());
		header('Location: ' . BASE_URL . '/users/login');
	}

	function test() {

		$doc = new DOMDocument;
		$doc->load(BASE_URL . '/payback/paybackdb.xml');

		$thedocument = $doc->documentElement;

		$list = $thedocument->getElementsByTagName('record');

		$nodeToRemove = null;
		foreach ($list as $domElement) {
			$attrValue = $domElement->nodeValue;



			if (strpos($attrValue, '2013-05-27 14:11:06') == true) {
				$nodeToRemove = $domElement;
				
				$rawdata = preg_split("/((\r?\n)|(\r\n?))/", $attrValue);
				
				foreach ($rawdata as $data) {
				
					if(strpos($data, '/var/www') == true) {
						
						$image_path = $data;
						
						pr($image_path);
						
					}
				
				}
			}
		}
		
		 exit;

		if ($nodeToRemove != null) {
			$thedocument->removeChild($nodeToRemove);
		}

		$doc->save('../../../paybackdb.xml');
		
		
	}
	
	

}

