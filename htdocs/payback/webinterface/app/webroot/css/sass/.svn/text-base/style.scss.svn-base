@import "compass";
@import "_normalize";
@import "_jquery.bxslider";
@import "compass/css3/user-interface";
@import "_tabs.scss";
/* ====================== 
   Design Base Formatting
   ====================== */
html{
	font-family: Helvetica, Arial, sans-serif;
	font-weight: normal;
	height:100%;
}

body{
	background:#fff;
	color:#000;
	height:100%;
}

* {
  &::selection {
    color: #ffffff;
    background-color: #793081;
  }
  &::-moz-selection {
    color: white;
    background: #793081;
  }
  &::selection {
    color: white;
    background: #793081;
  }
}

h1, h2, h3, h4, h5, h6, h7{
	margin:0px 0px 10px 0px;
}
h2{
	font-family: "Helvetica LT Std"; font-size: 22px; text-transform: uppercase; color: #363636; font-weight:normal;
}
h3{
	font-family: "Helvetica LT Std"; font-size: 22px; color: #913e98; font-weight:normal;
}
h4{
	font-family: "Helvetica LT Std"; font-size: 18px; color: #434343; font-weight:normal;
}
p{
	margin:0px 0px 5px 0px;
	line-height:1.2em;
	font-family: "Helvetica LT Std"; font-size: 13px; color: #434343;
}

a{
	color:#913E98;
	cursor:pointer;
	@include transition(all 0.35s ease-in);
	text-decoration:none;
	
	&:hover{
		color:#c03fcc;
	}
}

ul{
	list-style:none;
	margin:0px 0px 10px 0px;
	padding:0px;
}

/* ====================== 
   End Design Base Formatting
   ====================== */
   
/* ====================== 
   Header
   ====================== */

header{
	background:url(../images/white-texture.png) repeat center center;
	@include box-shadow(0 1px 1px rgba(0,0,0,.31));
	padding:20px;
	z-index:10;
	position:relative;
	border-top:6px solid #793080;
	
	.logo{
		display:inline-block;
		width:100%;
		text-align:center;
		line-height:0px;
		margin-bottom:0px;
		
		a{
			display:inline-block;
		}
	}
}

.map{
	margin-top:-115px;
	margin-left:auto;
	margin-right:auto;
	width:100%;
	text-align:center;
	
	background:url('../images/dummy_map.png') repeat-x center center;
	height:608px;
}

.logo{
	a{
		@include hide-text;
		
		background: url(../images/logo.png) no-repeat center center;
		display:block;
		width:208px;
		height:69px;
	}
}
   
/* ====================== 
   Header
   ====================== */
   
.container{
	width:1000px;
	margin:0 auto;
}
section{
	h2{
		text-align:center;
	}
}
.section{
	position:relative;
	text-align:center;
	background:#ffffff;
	padding-top:100px;
	
	&:before{
		content:'';
		width: 140px;
		height:140px;
		position:absolute;
		@include border-radius(50%);
		background-color: #eee;
		@include box-shadow(0 0 5px rgba(0,0,0,.45));
		border: solid 6px #fff;
		
		left: 50%;
		margin-left: -75px;
		position: absolute;
		top: -70px;
		width: 140px;
	}
	&:after{
		content:'';
		left: 50%;
		margin-left: -70px;
		position: absolute;
		top: -65px;
		width: 140px;
		height:140px;
		z-index:5;
	}
}
*+html .recent-projects ul li {  margin-right:10px; }
.recent-projects{
	border-top:4px solid #51ae22;
	background:url(../images/dark-white-texture.png) repeat-x center top;
	padding-bottom:120px;
	
	&:after{
		background:url('../images/icon_payback-person.png') no-repeat center center;
	}
	h2{
		margin-bottom:30px;
	}
	ul{
		width:830px;
		display:block;
		margin:auto;
		&:before, &:after { content: " "; display: table; }
		&:after { clear: both; }
		*zoom: 1;
		
		li{
			float:left;
			@include border-radius(5px); background-color: #f5f5f5; @include box-shadow(0 0 9px rgba(0,0,0,.26)); border: solid 5px #fff;
			margin-right:40px;
			
			margin-bottom:50px;
			
			&:last-child{
				margin-right:0px;
			}
			&:nth-child(3n){
				clear:right;
			}
			
			.location{
				padding:10px 45px 10px 20px;
				text-align:left;
				font-family: "Helvetica LT Std"; font-size: 20px; color: #434343;
				position:relative;
				
				span{
					display:block;
					font-family: "Helvetica LT Std"; font-size: 12px; color: #9a9a9a;
				}
				&:after{
					position:absolute;
					content:'';
					top: 20px;
					right:20px;
					width: 17px;
					height:24px;
					background:url(../images/icon_venu.png) no-repeat center center;

				}
			}
		}
	}
}
.whats-in-the-app{
	background:url("../images/purple-map-bg.png") repeat-x center top;
	margin-bottom:40px;
	&:after{
		background:url('../images/icon_iphone-and-ipad.png') no-repeat center center;
	}
	h2{
		color:#fff;
	}
}
.app-stores{
	background:url('../images/deco_div.png') no-repeat;
	margin-top:-40px;
	height:120px;
	
	ul{
		position: relative;
		top: 54px;
		
		li{
			display:inline-block;
			margin-right:10px;
			&:last-child{
				margin-right:0px;
			}
			a{
				display:inline-block;
				@include hide-text;
				
				@include border-radius(50px );
				background-color: #fbfbfb;
				@include box-shadow(0 2px 2px rgba(0,0,0,.17));
				border: solid 1px rgba(168,168,168,.21);
				@include background-image(linear-gradient(bottom, rgba(0,0,0,.1), rgba(0,0,0,0)));
				
				width:126px;
				height:48px;
				padding:2px 30px;
				
				&:hover{
					@include background-image(linear-gradient(bottom, rgba(80,80,80,.1), rgba(255,255,255,0)));
				}
				&:active{
					@include background-image(linear-gradient(bottom, rgba(255,255,255,0), rgba(80,80,80,.1)));
				}
				
				&.google-play{
					&:before{					
						content:'';
						display:block;
						width:126px;
						height:48px;
						background:url(../images/icon_google-play.png) no-repeat center center;
					}
					&.coming-soon{
						position:relative;
						&:after{
							content:'';
							display:block;
							width:71px;
							height:52px;
							background:url(../images/icon_coming-soon.png) no-repeat center center;
							position:absolute;
							top:0px;
							left:5px;
						}
					}
				}
				&.app-store{
					&:before{
						content:'';
						display:block;
						width:126px;
						height:48px;
						background:url(../images/icon_app-store.png) no-repeat center center;
					}
				}
			}
		}
	}
}
.supported-region{
	margin-top:20px;
	background: url('../images/fade-texture.png') repeat-x center bottom;
	padding-bottom:70px;
	
	h2{
		margin-bottom:40px;
	}
	ul{
	&:before, &:after { content: " "; display: table; }
	&:after { clear: both; }
	*zoom: 1;
	
		li{
			float:left;
			margin-right:60px;
			width:270px;
			padding-bottom:40px;
			
			box-sizing:border-box;
			-moz-box-sizing:border-box;
			
			&:last-child{
				clear:right;
			}
		}
	}
}
.ui-state-active{
	.video-image{
		&:after{
			@include opacity(0);
			@include transition(all 0.35s ease-out);
		}
	}
}
.videos{
	margin-bottom:125px;
	h2{
		margin-bottom:30px;
	}
	h4{
		margin-bottom:0px;
		margin-top:6px;
	}
	.video-image{
		display:inline-block;
		float:left;
		position:relative;
		&:after{
			content:'';
			background:url('../images/icon_play.png') no-repeat center center;
			position:absolute;
			top:50%;
			left:50%;
			margin-left:-13px;
			margin-top:-9px;
			width:26px;
			height:16px;
			@include transition(all 0.35s ease-in);
		}
	}
	.name-and-info{
		display:block;
		margin-left:100px;
		padding-right:20px;
	}
	&:after{
		background:url('../images/icon_video.png') no-repeat center center;
	}
	
}
.box{
	@include border-radius(5px);
	background-color: #f0f0f0;
	@include box-shadow(0 0 9px rgba(0,0,0,.26));
	border: solid 2px #fff;
	width:886px;
	margin:0 auto;
}
.contact-us{
	height:424px;
	h2{
		color:#fff;
		margin-bottom:20px;
	}
	
	background:#429d1d url('../images/contact-us-bg.png') no-repeat center center;
	color:#fff;
	
	&:after{
		background:url('../images/icon_telephone.png') no-repeat center center;
	}
	label{
		display:block;
		text-align:left;
		margin-left:5px;
		font-size:18px;
		margin-bottom:8px;
		font-weight:normal;
	}
	input[type="text"]{
		fonts-size:12px;
		color:#000;
		@include border-radius(5px);
		background-color: #fff;
		@include box-shadow(inset 0 5px 5px rgba(0,0,0,.15));
		border:none;
		padding:10px;
		
		box-sizing:border-box;
		-moz-box-sizing:border-box; /* Firefox */
		
		width:100%;
		padding:5%;
		
		&.error{
			border:1px solid #f00;
		}
	}
	.btn_purple{
		@include border-radius(5px);
		background-color: #883390;
		@include box-shadow(inset 0 5px 5px rgba(0,0,0,.15));
		border:none;
		padding:10px 20px;;
		font-size:18px;
		color:#fff;
		text-transform:uppercase;
		&:active{
			@include box-shadow(0 5px 5px rgba(0,0,0,.15));
		}
	}
	table{
		table-layout:fixed;
		width:600px;
		margin:0 auto;
		text-align:left;
		
		td{
			padding-right:10px;
			padding-bottom:20px;
			&:last-child{
				padding-right:0px;
			}
		}
	}
	textarea{
	
		width:100%;
		max-width:100%;
		padding:10px;
		height:130px;
		max-height:130px;
		min-height:100px;
		
		box-sizing:border-box;
		-moz-box-sizing:border-box; /* Firefox */
		
		fonts-size:12px;
		color:#000;
		@include border-radius(5px);
		background-color: #fff;
		@include box-shadow(inset 0 5px 5px rgba(0,0,0,.15));
		border:none;
	}
	input[type="submit"], input[type="button"]{
		float:right;
	}
}
footer{
	background:url('../images/dark-white-texture.png') repeat center center;
	padding:20px 0px;
	
	.app-stores{
		background:none;
		float:right;
	}
	
	p{
		font-size:13px;
		color:#4d4d4d;
	}
	
	.copyright{
		.logo{
			@include hide-text;
			display:inline-block;
			width:158px;
			height:50px;
			background:url('../images/grey_logo.png') no-repeat left top;
			position:relative;
			
			&:before{
				content:'';
				position:absolute; top:0px; left:0px;
				background:url('../images/grey_logo.png') no-repeat right top;
				width:158px;
				height:50px;
				@include opacity(0);
				@include transition(all 0.35s ease-in);
			}
			&:hover{
				&:before{
					@include opacity(1);
					@include transition(all 0.35s ease-out);
				}
			}
		}
		display:inline-block;
	}
}
