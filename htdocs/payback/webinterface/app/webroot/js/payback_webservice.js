var PaybackWebservice = function (){}

/*** STATIC Method ***/

PaybackWebservice.get_image_path = function(){
	return 'http://paradex.co.uk/payback/images/';
} ;

PaybackWebservice.get_lat_lng = function (){
	$.ajax({
		type: "get",
		dataType: "json",		
		url: App.getUrl({
			controller: 'home',
			action: 'get_lat_lng.json'
		}),
		success: function (response) {						
			$.each(response,function(index , value ){	
				
				if(GoogleMapApi.map == undefined){
					//------- Google Maps ---------//
					GoogleMapApi.initialize(value.lat , value.lng);
				}
				
				GoogleMapApi.addMarker(value.lat,value.lng);				
				
				GoogleMapApi.bind_detail_window(GoogleMapApi.markers[index],value.address,value.picture_link);				
			});
			
		},
		error: function ( jqXHR, textStatus, errorThrown) {
			
		}
	});
}
/*** END: STATIC Method ***/