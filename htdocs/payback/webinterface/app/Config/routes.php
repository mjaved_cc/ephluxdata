<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'home', 'action' => 'index'));
	Router::connect('/about-us', array('controller' => 'home', 'action' => 'about_us'));
	Router::connect('/cookies', array('controller' => 'home', 'action' => 'cookies'));
	Router::connect('/privacy', array('controller' => 'home', 'action' => 'privacy'));
	Router::connect('/press', array('controller' => 'home', 'action' => 'press'));
	Router::connect('/terms-of-use', array('controller' => 'home', 'action' => 'terms_of_use'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/:controller/sync', array('controller' => 'auth_action_maps','action' => 'sync_action_maps'));
	Router::connect('/conversations/:controller/:action/*', array('plugin'=>'conversations'));
	Router::connect('/conversations/:action/*', array('controller'=>'conversations', 'plugin'=>'conversations'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
	
	Router::parseExtensions('json');