<?php
App::uses('AppModel', 'Model');
/**
 * ConversationMessage Model
 *
 * @property Conversation $Conversation
 * @property User $User
 */
class ConversationMessage extends ConversationsAppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
 	public $uses = array('Users.User','Conversation');
	public $belongsTo = array(
		'Conversation' => array(
			'className' => 'Conversation',
			'foreignKey' => 'conversation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => 'id,username',
			'order' => ''
		)
	);
	
	public function getConversationMessages($conversationId = NULL) {
		$messages = $this->find('all', array(
			'conditions' => array('ConversationMessage.conversation_id' => $conversationId),
			'contain' => array(
				'User' => array(
					'fields' => array('id','username'),
				)
			),
			'order' => array('ConversationMessage.created DESC')
		));
		
		//foreach($messages AS $i=>$message) {
			//$messages[$i]['User']['Details'] = $messages[$i]['User']['id'];
		//}
		
		return $messages;
	}
}
