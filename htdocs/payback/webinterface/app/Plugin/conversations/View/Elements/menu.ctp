<h3><?php echo __('Actions'); ?></h3>
<ul>
    <li><?php echo $this->Html->link('Inbox', '/conversations'); ?></li>
    <li><?php echo $this->Html->link('Sent', '/conversations/sent'); ?></li>
    <li><?php echo $this->Html->link('Compose', '/conversations/add'); ?> </li>
</ul>