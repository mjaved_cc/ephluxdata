<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">
				<ul class="bulleted_list">
					<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?></li>
					<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
				</ul>
		</div>
	</div>
</div>

<div class="main_column">
	<div class="box">
		<div class="body">
			<h2><?php echo __('Groups'); ?></h2>
			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
				<thead>
					<tr>
						<th><?php echo $this->Paginator->sort('name'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($groups as $group): ?>
						<tr>
							<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
							<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link(__('View'), array('action' => 'view', $group['Group']['id'])); ?>
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $group['Group']['id'])); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<p>
				<?php
				echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>	</p>

			<div class="paging">
				<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
			</div>

		
		</div>
	</div>
</div>