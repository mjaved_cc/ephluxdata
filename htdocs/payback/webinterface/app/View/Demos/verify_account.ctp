<?php

if (isset($api_response)) {
	$response = json_decode($api_response);

	$class = '';
	if ($response->header->code == 0) {
		$class = 'info';
	} else {
		$class = 'error';
	}

	echo $this->element('notification', array("message" => $response->header->message, "class" => $class));
	echo $this->Html->link('Login', array('controller' => 'demos', 'action' => 'user_login'));
}
?>