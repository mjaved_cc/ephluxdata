

<div class="main_column">
	<div class="box">
		<div class="body">
			<h2><?php echo __('Nominations'); ?></h2>
			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
				<thead>
					<tr>
						<th><?php echo __('Picture Link'); ?></th> 
						<th><?php echo __('Address'); ?></th>
						<th><?php echo __('Message'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>

					</tr>
				</thead>
				<tbody>
					<?php
					for ($j = $start; $j <= $end - 1; $j++) {

						$mess = substr($data[$j]['message'], 0, 20);
						$address = substr($data[$j]['address'], 0, 20); //foreach ($data as $dt):   
						?>

						<tr>
							<td><?php echo $this->Html->image($data[$j]['picture_link'], array('width' => '100', 'height' => '100')); ?></td>
							<td><?php echo $address . '...'; ?></td>
							<td><?php echo $mess . '...'; ?></td>
							<td class="actions">
								<?php // echo $this->Html->link(__('View'), array('action' => 'view', $j)); ?> <a href="<?php echo BASE_URL.'/users/view/'.$j ?>">View</a>  | 

								<?php  echo $this->Html->link('Delete', array('controller'=>'users','action' => 'delete', $data[$j]['created']), null, 'Are you sure ') ?> 


							</td>

						</tr>

					<?php }// endforeach;     ?>
				</tbody>
			</table>

			<p>
				
				<?php
				for ($i = 1; $i <= $pages; $i++) {
					
					//echo '<span style="margin-left:5px;">' . $this->Html->link(__($i), array('action' => 'index', $i)) . '</span>';
					?>
					
					
					<span style="margin-left:5px;"><a href="<?php echo BASE_URL.'/users/index/'.$i ?>"><?php echo $i; ?></a></span>
					
			<?php 	}
				?>	</p>





		</div>
	</div>
</div>