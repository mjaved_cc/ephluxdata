<?php
// Turn off all error reporting
error_reporting(0);

function pb_send_emails($to, $from, $subject, $message_actual)
{
	// $subject = "I'm a hardcoded subject test";
// 	$message_actual = "I'm a hardcoded message body test";


	$bound_text =	"jimmyP123";
	$bound =	"--".$bound_text."\r\n";
	$bound_last =	"--".$bound_text."--\r\n";
	 
	$headers =	"From: $from\r\n";
	$headers .= 'Cc: payback@paradex.co.uk' . "\r\n";

	$headers .=	"MIME-Version: 1.0\r\n"
		."Content-Type: multipart/mixed; boundary=\"$bound_text\"";
	 
	 $message .=	"If you can see this MIME than your client doesn't accept MIME types!\r\n"
		.$bound;
	 
	$message .=	"Content-Type: text/html; charset=\"iso-8859-1\"\r\n"
		."Content-Transfer-Encoding: 7bit\r\n\r\n"
		.$message_actual . "\r\n"
		.$bound;
	
	
	$emails = explode(";", $to);
	foreach($emails as $email){
		if(!empty($email)) {
			mail($email, $subject, $message, $headers);	
		}
	}
}

header ( 'Content-type: application/json; charset=utf-8' );
$response = array();
if (!( array_key_exists( 'from', $_POST ) 
&& array_key_exists( 'to', $_POST )
&& array_key_exists( 'subject', $_POST )
&& array_key_exists( 'message', $_POST )

)
) {
	$response = array('response'=>'false');
} else {
	//pb_send_emails("mfaizan.shaikh@gmail.com;faizan.shaikh@ephlux.com;", "no-reply@payback.com");
	pb_send_emails($_POST['to'], $_POST['from'],$_POST['subject'], $_POST['message'] );
	$response = array('response'=>'true');
}
echo json_encode($response);
