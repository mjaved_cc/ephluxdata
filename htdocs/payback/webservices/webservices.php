<?php

/** copied from index.php in admin */
require_once('CakeXMLUtility/Xml.php');

$method_names = array(
	1 => 'get_latlng',
	2 => 'get_recent_entries',
	3 => 'get_all_entries',
	4 => 'deletNode'
);

$webservice_controller = new webservice();
$method = webservice::get_query_string_value('method');
$function_name = $method_names[$method];

if (method_exists($webservice_controller, $function_name))
	$webservice_controller->$function_name();
else {
	$response['code'] = '400';
	$response['message'] = 'Bad Request';
	webservice::set_content_type_json();
	echo json_encode($response);
	exit;
}

class webservice {

	/**
	 *  hold base url
	 * @access private
	 */
	private $_uri_base;

	/**
	 * instance of SimpleXMLElement
	 * @access private
	 */
	private $_xml_object;

	/**
	 *  json response
	 * @access private
	 */
	private $_response = array();

	function __construct() {

		$this->_uri_base = self::get_base_url();
		// now is a instance of SimpleXMLElement
		$this->_xml_object = Xml::build(self::get_xml_path());
	}

	/**
	 * For debugging purpose
	 */
	private function _debug($data) {
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}

	/*	 * ************************* Services *************************** */

	/**
	 * Get lat & lng in xml file
	 * 
	 * Error code: 1 - 30
	 * 
	 * @return json
	 */
	function get_latlng() {

		try {
			$data = Xml::toArray($this->_xml_object);
			if (empty($data)) {
				$this->_response['code'] = '1';
				$this->_response['message'] = 'No record found';
			} else {
				$this->_response['code'] = '0';
				foreach ($data['root']['record'] as $key => $value) {
					$image_info = pathinfo($value['picture_link']);
					$this->_response['body'][$key] = $value;
					$this->_response['body'][$key]['picture_link'] = self::get_image_path($image_info['basename']);
				}
			}
			$this->_before_render();
		} catch (Exception $e) {
			echo 'Message: ' . $e->getMessage();
		}
	}

	/**
	 * Get top 3 entries
	 * 
	 * Error code: 31 - 60
	 * 
	 * @return json
	 */
	function get_recent_entries() {
		try {
			$data = Xml::toArray($this->_xml_object);
			if (empty($data)) {
				$this->_response['code'] = '1';
				$this->_response['message'] = 'No record found';
			} else {
				$this->_response['code'] = '0';
				$key_tounix_data = array();
				foreach ($data['root']['record'] as $key => $value) {
					$key_tounix_data[self::to_unix($value['created'])] = $value;
				}

				krsort($key_tounix_data);
				$output = array_slice($key_tounix_data, 0, 6);
				foreach ($output as $key => $value) {
					$image_info = pathinfo($value['picture_link']);
					$this->_response['body'][$key] = $value;
					// $this->_response['body'][$key]['address'] = self::get_formatted_address($value['address']);
					$this->_response['body'][$key]['picture_link'] = self::get_image_path($image_info['basename']);
				}
			}
			$this->_before_render();
		} catch (Exception $e) {
			echo 'Message: ' . $e->getMessage();
		}
	}

	/**
	 * Get all entries
	 * 
	 * Error code: 31 - 60
	 * 
	 * @return json
	 */
	function get_all_entries() {
		try {
			$data = Xml::toArray($this->_xml_object);
			if (empty($data)) {
				$this->_response['code'] = '1';
				$this->_response['message'] = 'No record found';
			} else {
				$this->_response['code'] = '0';
				$key_tounix_data = array();
				foreach ($data['root']['record'] as $key => $value) {
					$key_tounix_data[self::to_unix($value['created'])] = $value;
				}

				krsort($key_tounix_data);
				$output = array_slice($key_tounix_data, false);
				foreach ($output as $key => $value) {
					$image_info = pathinfo($value['picture_link']);
					$this->_response['body'][$key] = $value;
					// $this->_response['body'][$key]['address'] = self::get_formatted_address($value['address']);
					$this->_response['body'][$key]['picture_link'] = self::get_image_path($image_info['basename']);
				}
			}
			$this->_before_render();
		} catch (Exception $e) {
			echo 'Message: ' . $e->getMessage();
		}
	}

	/*	 * ************************* END: Services *************************** */

	/**
	 * Get values given key via URL params - query string
	 * 
	 * Wrapper of $_GET php .
	 * 
	 * Usage: if url is www.demo.com?q=google then get_query_string('q') will return google
	 * 
	 * @param string $key name of query string
	 * @return string
	 * @static
	 */
	static function get_query_string_value($key) {
		return isset($_GET[$key]) ? $_GET[$key] : 0;
	}

	/**
	 * Get path of xml file
	 * 
	 * @static
	 * @return string path
	 */
	static function get_xml_path() {
		return getcwd() . '/../paybackdb.xml';
		// return '/var/www/payback/paybackdb.xml';
	}

	/**
	 * Get base url
	 * 
	 * @access public
	 * @static
	 */
	static function get_base_url() {
		/* First we need to get the protocol the website is using */
		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'http://';

		/* returns /myproject/index.php */
		$path = $_SERVER['PHP_SELF'];

		/*
		 * returns an array with:
		 * Array (
		 *  [dirname] => /myproject/
		 *  [basename] => index.php
		 *  [extension] => php
		 *  [filename] => index
		 * )
		 */
		$path_parts = pathinfo($path);
		$directory = $path_parts['dirname'];
		/*
		 * If we are visiting a page off the base URL, the dirname would just be a "/",
		 * If it is, we would want to remove this
		 */
		$directory = ($directory == "/") ? "" : $directory;

		/* Returns localhost OR mysite.com */
		$host = $_SERVER['HTTP_HOST'];

		/*
		 * Returns:
		 * http://localhost/mysite
		 * OR
		 * https://mysite.com
		 */
		return $protocol . $host . $directory;
	}

	/**
	 * Get full qualified image path
	 * 
	 * @param string $images_name image name
	 * @return string path
	 */
	static function get_image_path($images_name) {
		$images_base_uri = self::get_base_url() . '/images/';
		$images_path = $images_base_uri . $images_name;
		// if (file_exists($images_path))
		return $images_path;
		// return $images_base_uri . 'default.png';
	}

	/**
	 * Get formatted address.
	 * 
	 * @param string $address address
	 * @return string 
	 */
	static function get_formatted_address($address) {
		return substr($address, 0, 9);
	}

	/**
	 * Set header information 
	 * 
	 * content type json
	 * 
	 * @return void
	 */
	static function set_content_type_json() {
		header('Content-type: application/json; charset=utf-8');
	}

	/**
	 * Wrapper of strtotime php doc 
	 * 
	 * @param string $time A date/time string. Valid formats are explained in Date and Time Formats.
	 * @return int
	 */
	static function to_unix($time) {
		return strtotime($time);
	}

	private function _before_render() {
		self::set_content_type_json();
		echo json_encode($this->_response);
		exit;
	}

	function deletNode() {
		$nodeIdentifier = $_POST['nodeIdentifier'];
		// now is a instance of domdocument
		$this->_xml_object = Xml::build(self::get_xml_path(), array('return' => 'domdocument'));

		// $doc = new DOMDocument;
		// $doc->load(self::get_xml_path());
		// $thedocument = $doc->documentElement;
		$thedocument = $this->_xml_object->documentElement;

		$list = $thedocument->getElementsByTagName('record');

		$image_path = null;
		$nodeToRemove = null;
		foreach ($list as $domElement) {
			$attrValue = $domElement->nodeValue;

			if (strpos($attrValue, $nodeIdentifier) == true) {
				$nodeToRemove = $domElement;
				$rawdata = preg_split("/((\r?\n)|(\r\n?))/", $attrValue);

				foreach ($rawdata as $data) {

					if (strpos($data, '/var/www') == true) { 
						
						$data = str_replace('https://maps.google.com/maps?z=12',' ',$data);
						
						
						$data_array = explode('/', $data);						
						$data_array = end($data_array);						
						$data_array = substr($data_array, 0,  strpos($data_array, '.')+4);										
						
						$image_path = trim(substr($data, strpos($data, '/var/www'), 54));
						
						$image_path = $image_path.$data_array;
						
						
					}
				}
			}
		}
		

		if ($nodeToRemove != null) {
			
			$thedocument->removeChild($nodeToRemove);
					
			
			if (file_exists($image_path)) { 
				chmod($image_path, '0777');
				unlink($image_path);
			}
		}

		$this->_xml_object->save('../paybackdb.xml');
	}

}