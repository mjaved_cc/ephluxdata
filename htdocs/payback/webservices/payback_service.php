<?php

// Turn off all error reporting
error_reporting(0);

$image_link = "";
header('Content-type: application/json; charset=utf-8');

if (!( array_key_exists('lat', $_POST) &&
		array_key_exists('lng', $_POST) &&
		array_key_exists('address', $_POST) &&
		array_key_exists('picture_link', $_FILES) &&
		array_key_exists('message', $_POST)
		)) {


	$response = array('response' => 'false');
} else {

	$file = getcwd() . '/../paybackdb.xml';
	if (file_exists($file)) {
		$sxe = simplexml_load_file($file);
		$record = $sxe->addChild('record');
		$record->addChild('lat', $_POST['lat']);
		$record->addChild('lng', $_POST['lng']);
		$record->addChild('address', $_POST['address']);
		$today = date("Y-m-d H:i:s");
		$record->addChild('created', $today);



		// upload the img
		if (isset($_FILES['picture_link'])) {
			$file_success = uploadFiles(getcwd() . "/images", array($_FILES['picture_link']));
			if (isset($file_success ['urls'])) {
				foreach ($file_success ['urls'] as $link) {
					$record->addChild('picture_link', $link);
					$image_link = get_filename($link);
				}
			}
		} else {
			$record->addChild('picture_link', '');
		}
		$record->addChild('message', $_POST['message']);


		if ($sxe->asXML($file)) {


			$image_url = get_base_url() . '/images/' . $image_link;


			$response = array('response' => 'true', 'image_link' => $image_url);
		} else {

			$response = array('response' => 'false');
		}
	}
}


echo json_encode($response);

function get_base_url() {
	/* First we need to get the protocol the website is using */
	$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'http://';

	/* returns /myproject/index.php */
	$path = $_SERVER['PHP_SELF'];

	/*
	 * returns an array with:
	 * Array (
	 *  [dirname] => /myproject/
	 *  [basename] => index.php
	 *  [extension] => php
	 *  [filename] => index
	 * )
	 */
	$path_parts = pathinfo($path);
	$directory = $path_parts['dirname'];
	/*
	 * If we are visiting a page off the base URL, the dirname would just be a "/",
	 * If it is, we would want to remove this
	 */
	$directory = ($directory == "/") ? "" : $directory;

	/* Returns localhost OR mysite.com */
	$host = $_SERVER['HTTP_HOST'];

	/*
	 * Returns:
	 * http://localhost/mysite
	 * OR
	 * https://mysite.com
	 */
	return $protocol . $host . $directory;
}

/**
 * uploads files to the server
 * @params:
 * $folder     = the folder to upload the files e.g. 'img/files'
 * $formdata   = the array containing the form files
 * @return:
 * will return an array with the success of each file upload
 */
function uploadFiles($folder, $formdata, $itemId = null) {
	// setup dir names absolute and relative
	$folder_url = $folder;
	$rel_url = $folder;

	// create the folder if it does not exist
	if (!is_dir($folder_url)) {
		mkdir($folder_url);
	}

	// if itemId is set create an item folder
	if ($itemId) {
		// set new absolute folder
		$folder_url = WWW_ROOT . $folder . '/' . $itemId;
		// set new relative folder
		$rel_url = $folder . '/' . $itemId;
		// create directory
		if (!is_dir($folder_url)) {
			mkdir($folder_url);
		}
	}

	// list of permitted file types, this is only images but documents can be added
	$permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/bmp');

	// loop through and deal with the files
	foreach ($formdata as $file) {
		// replace spaces with underscores
		$filename = str_replace(' ', '_', $file ['name']);
		// assume filetype is false
		$typeOK = false;
		// check filetype is ok
		foreach ($permitted as $type) {
			if ($type == $file ['type']) {
				$typeOK = true;
				break;
			}
		}

		// if file type ok upload the file
		if ($typeOK) {
			// switch based on error code
			switch ($file ['error']) {
				case 0 :
					// check filename already exists
					if (!file_exists($folder_url . '/' . $filename)) {
						// create full filename
						$full_url = $folder_url . '/' . $filename;
						$url = $rel_url . '/' . $filename;
						// upload the file
						$success = move_uploaded_file($file ['tmp_name'], $url);
					} else {
						// create unique filename and upload file
						//ini_set ( 'date.timezone', 'Europe/London' );
						$now = date('Y-m-d-His');
						$full_url = $folder_url . '/' . $now . $filename;
						$url = $rel_url . '/' . $now . $filename;
						$success = move_uploaded_file($file ['tmp_name'], $url);
					}
					// if upload was successful
					if ($success) {
						// save the url of the file
						$result ['urls'] [] = $url;
					} else {
						$result ['errors'] [] = "Error uploaded $filename. Please try again.";
					}
					break;
				case 3 :
					// an error occured
					$result ['errors'] [] = "Error uploading $filename. Please try again.";
					break;
				default :
					// an error occured
					$result ['errors'] [] = "System error uploading $filename. Contact webmaster.";
					break;
			}
		} elseif ($file ['error'] == 4) {
			// no file was selected for upload
			$result ['nofiles'] [] = "No file Selected";
		} else {
			// unacceptable file type
			$result ['errors'] [] = "$filename cannot be uploaded. Acceptable file types: gif, jpg, png.";
		}
	}
	return $result;
}

/**
 * 
 * Extracts the filename from the path
 * @param unknown_type $link
 */
function get_filename($link) {
	$pos = strrpos($link, "/");
	if ($pos !== false) {
		return substr($link, $pos + 1);
	}
}
