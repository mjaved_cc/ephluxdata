<?php
App::uses('ConversationsAppModel', 'conversations.Model');
/**
 * Conversation Model
 *
 * @property User $User
 * @property LastMessage $LastMessage
 * @property ConversationMessage $ConversationMessage
 * @property ConversationUser $ConversationUser
 */
class Conversation extends ConversationsAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'LastMessage' => array(
			'className' => 'Conversations.ConversationMessage',
			'foreignKey' => 'last_message_id',
			'conditions' => "",
			'fields' => array('message'),
			'order' => array('LastMessage.id desc')
		),
		'inbox' => array(
			'className' => 'Conversations.ConversationMessage',
			'foreignKey' => '',
			'fields' => array('inbox.id,inbox.user_id,inbox.conversation_id'),
			'conditions' => "inbox.conversation_id=Conversation.id"
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => '',
			'conditions' => 'LastMessage.user_id=User.id',
			'fields' => 'User.id,User.username',
			'order' => ''
		)
		
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ConversationMessage' => array(
			'className' => 'Conversations.ConversationMessage',
			'foreignKey' => 'conversation_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ConversationUser' => array(
			'className' => 'Conversations.ConversationUser',
			'foreignKey' => 'conversation_id',
			'dependent' => true,
			'conditions' => "",
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function ownConversations($id, $type = 'all') { 
     	$options = array(
     		'conditions' => array("ConversationUser.status <"=>1), 
            'group' => array('ConversationUser.conversation_id HAVING SUM(case when ConversationUser.`user_id` in (\''.$id.'\') then 1 else 0 end) = '.count($id).''), 
            'contain' => array('Conversation' => array('LastMessage')), 
            'order' => array('Conversation.last_message_id'=>'DESC')
			
        ); 
		//$this->paginate = $options;
		return $this->find('all');
		
		//return $this->ConversationUser->find($type, $options); 
	}
	
	public function getConversation($id) {
		$conversation = $this->find('first', array(
			'conditions' => array('Conversation.id' => $id),
			'contain' => array(
				'ConversationUser'
			)
		));
		
		if(!empty($conversation)) {			
			$conversation['Messages'] = $this->ConversationMessage->getConversationMessages($id);
		
			return $conversation;
		} else {
			return false;
		}
	}

}
