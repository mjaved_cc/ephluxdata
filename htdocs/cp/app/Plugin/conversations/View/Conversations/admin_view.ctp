<div class="conversations view">
<h2><?php  echo __('Conversation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($conversation['User']['username'], array('controller' => 'users', 'action' => 'view', $conversation['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Message'); ?></dt>
		<dd>
			<?php echo $this->Html->link($conversation['LastMessage']['id'], array('controller' => 'conversation_messages', 'action' => 'view', $conversation['LastMessage']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Allow Add'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['allow_add']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Count'); ?></dt>
		<dd>
			<?php echo h($conversation['Conversation']['count']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Conversation'), array('action' => 'edit', $conversation['Conversation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Conversation'), array('action' => 'delete', $conversation['Conversation']['id']), null, __('Are you sure you want to delete # %s?', $conversation['Conversation']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversation Messages'), array('controller' => 'conversation_messages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Last Message'), array('controller' => 'conversation_messages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conversation Users'), array('controller' => 'conversation_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conversation User'), array('controller' => 'conversation_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Conversation Messages'); ?></h3>
	<?php if (!empty($conversation['ConversationMessage'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Conversation Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Attachment Id'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($conversation['ConversationMessage'] as $conversationMessage): ?>
		<tr>
			<td><?php echo $conversationMessage['id']; ?></td>
			<td><?php echo $conversationMessage['conversation_id']; ?></td>
			<td><?php echo $conversationMessage['user_id']; ?></td>
			<td><?php echo $conversationMessage['attachment_id']; ?></td>
			<td><?php echo $conversationMessage['message']; ?></td>
			<td><?php echo $conversationMessage['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'conversation_messages', 'action' => 'view', $conversationMessage['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'conversation_messages', 'action' => 'edit', $conversationMessage['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'conversation_messages', 'action' => 'delete', $conversationMessage['id']), null, __('Are you sure you want to delete # %s?', $conversationMessage['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Conversation Message'), array('controller' => 'conversation_messages', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Conversation Users'); ?></h3>
	<?php if (!empty($conversation['ConversationUser'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Conversation Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Last View'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Folder'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($conversation['ConversationUser'] as $conversationUser): ?>
		<tr>
			<td><?php echo $conversationUser['id']; ?></td>
			<td><?php echo $conversationUser['conversation_id']; ?></td>
			<td><?php echo $conversationUser['user_id']; ?></td>
			<td><?php echo $conversationUser['status']; ?></td>
			<td><?php echo $conversationUser['last_view']; ?></td>
			<td><?php echo $conversationUser['created']; ?></td>
			<td><?php echo $conversationUser['folder']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'conversation_users', 'action' => 'view', $conversationUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'conversation_users', 'action' => 'edit', $conversationUser['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'conversation_users', 'action' => 'delete', $conversationUser['id']), null, __('Are you sure you want to delete # %s?', $conversationUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Conversation User'), array('controller' => 'conversation_users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
