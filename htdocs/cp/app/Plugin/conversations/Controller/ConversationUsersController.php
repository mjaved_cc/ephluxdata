<?php
App::uses('conversations.ConversationsAppController', 'Controller');
/**
 * ConversationUsers Controller
 *
 * @property ConversationUser $ConversationUser
 */
class ConversationUsersController extends ConversationsAppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ConversationUser->recursive = 0;
		$this->set('conversationUsers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		$this->set('conversationUser', $this->ConversationUser->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ConversationUser->create();
			if ($this->ConversationUser->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation user could not be saved. Please, try again.'));
			}
		}
		$conversations = $this->ConversationUser->Conversation->find('list');
		$users = $this->ConversationUser->User->find('list');
		$this->set(compact('conversations', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ConversationUser->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ConversationUser->read(null, $id);
		}
		$conversations = $this->ConversationUser->Conversation->find('list');
		$users = $this->ConversationUser->User->find('list');
		$this->set(compact('conversations', 'users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		if ($this->ConversationUser->delete()) {
			$this->Session->setFlash(__('Conversation user deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Conversation user was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ConversationUser->recursive = 0;
		$this->set('conversationUsers', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		$this->set('conversationUser', $this->ConversationUser->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ConversationUser->create();
			if ($this->ConversationUser->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation user could not be saved. Please, try again.'));
			}
		}
		$conversations = $this->ConversationUser->Conversation->find('list');
		$users = $this->ConversationUser->User->find('list');
		$this->set(compact('conversations', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ConversationUser->save($this->request->data)) {
				$this->Session->setFlash(__('The conversation user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The conversation user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ConversationUser->read(null, $id);
		}
		$conversations = $this->ConversationUser->Conversation->find('list');
		$users = $this->ConversationUser->User->find('list');
		$this->set(compact('conversations', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ConversationUser->id = $id;
		if (!$this->ConversationUser->exists()) {
			throw new NotFoundException(__('Invalid conversation user'));
		}
		if ($this->ConversationUser->delete()) {
			$this->Session->setFlash(__('Conversation user deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Conversation user was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
