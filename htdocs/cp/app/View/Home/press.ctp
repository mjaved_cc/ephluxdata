<?php echo $this->element('banner'); ?>

<!--- ====== Recent Projects ====== --->
<section class="section text">
	<div class="container">

		<h2>Press</h2>

		<ul class="download-screenshots">

			<li><a href="<?php echo $this->Misc->get_download_image_url('cp_poster.jpg') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'cp_poster_icon.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('app_icon_1024.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'app_icon_96.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('app_icon_text@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'app_icon_text.png') ?>"></a></li>

			<li><a href="<?php echo $this->Misc->get_download_image_url('iphone_screen_1@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'iphone_screen_1.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('iphone_screen_2@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'iphone_screen_2.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('ipad_screen_1@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'ipad_screen_1.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('ipad_screen_2@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'ipad_screen_2.png') ?>"></a></li>

<!--			<li><a href="<?php echo $this->Misc->get_download_image_url('google_play@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'google_play.png') ?>"></a></li>-->
			<li><a href="<?php echo $this->Misc->get_download_image_url('app_store@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'app_store.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('play_google@2x.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'play_google@2x.png') ?>"></a></li>

			<li><a href="<?php echo $this->Misc->get_download_image_url('google_play_large.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'icon_google-play_black.png') ?>"></a></li>
			<li><a href="<?php echo $this->Misc->get_download_image_url('app_store_large.png') ?>"><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'icon_app-store_black.png') ?>"></a></li>

		</ul>

	</div>
</section>
<!--- ====== / Recent Projects ====== --->