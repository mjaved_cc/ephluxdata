<!--- ====== MAP ====== --->
<section id="panel">
	<input id="search_place" type="text" placeholder="Enter your location...">
<!--	<button class="button" id="btn_search_place" name="button" onclick="GoogleMapApi.search_location()">Search</button>-->
</section>
<section id="map" class="map">
</section>
<!--- ====== / MAP ====== --->

<!--- ====== Recent Projects ====== --->
<section class="recent-projects section">
	<div class="container">
		<h2>Project Nominations</h2>

		<ul>
			<?php
			if (isset($data) && !empty($data)) {
				foreach ($data as $value) {
					?>
					<li>
						<div class="box">
							<div class="venu_image"><img width="238" height="177" src="<?php echo $value['picture_link'] ?>"></div>
							<div class="location"><?php echo $this->Text->excerpt($value['address'], 'method', 15, '...'); ?></div>
						</div>
					</li>
					<?php
				}
			}
			?>
		</ul>
		<div class="controlers">
			<!-- Controlers will spwan here on run time -->
		</div>
	</div>
</section>
<!--- ====== / Recent Projects ====== --->




<!--- ====== What�s in the app ====== --->
<section class="whats-in-the-app section">
	<div class="container">
		<h2>What's in the app</h2>

		<ul class="bxslider">
			<li><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'slider_img_1.png') ?>"/></li>
			<li><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'slider_img_2.png') ?>"/></li>
			<li><img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'slider_img_3.png') ?>"/></li>
		</ul>

		<div class="app-stores">
			<ul>
				<li><a href="https://play.google.com/store/apps/details?id=com.paradex.payback#?t=W251bGwsMSwyLDIxMiwiY29tLnBhcmFkZXgucGF5YmFjayJd" class="google-play coming-soon">Get it on Google Play</a></li>
				<li><a href="https://itunes.apple.com/pk/app/id615992844?mt=8" class="app-store">Available on the App Store</a></li>
			</ul>
		</div>
	</div>
</section>
<!--- ====== / What�s in the app ====== --->




<!--- ====== Supported Region ====== --->
<section class="supported-region">
	<div class="container">
		<h2>supported regions</h2>
		<ul>
			<li>
				<h3>London</h3>
				<p>Coming Soon...</p> <!-- Dummy Text :: Lorem Ipsum is simply dummy text of the printing and typesetting Lorem Ipsum is simply dummy text of the printing and typesetting -->
			</li>
			<li>
				<h3>Avon & Somerset</h3>
				<p>Coming Soon...</p>
			</li>
			<li>
				<h3>South Yorkshire</h3>
				<p>Coming Soon...</p>
			</li>

		</ul>
	</div>
</section>
<!--- ====== / Supported Region ====== --->


<!--- ====== Videos ====== --->
<section class="videos section">
	<div class="container">
		<h2>Videos</h2>
		<div class="box">
			<!--- Tabs --->
			<div id="tabs">
				<ul>
					<li>
						<a href="#a">
							<div class="video-image">
								<img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'video_img_1.png') ?>">
							</div>
							<div class="name-and-info">
								<h4>Justice Secretary Chri...</h4>
								<p>No description available.</p>
							</div>
						</a>
					</li>
<!--					<li>
						<a href="#b">
							<div class="video-image">
								<img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'video_img_2.png') ?>">
							</div>
							<div class="name-and-info">
								<h4>SWM Probation Com...</h4>
								<p>Staffordshire and West Midlands Probation Trust's Communi...</p>
							</div>
						</a>
					</li>-->
					<li>
						<a href="#c">
							<div class="video-image">
								<img src="<?php echo $this->Misc->get_fully_qualified_url(IMAGES_URL . 'video_img_3.png') ?>">
							</div>
							<div class="name-and-info">
								<h4>Crispin Blunt meets ...</h4>
								<p>Crispin Blunt meets offenders on community payback in Croyd...</p>
							</div>
						</a>
					</li>
				</ul>


				<div id="a">
					<iframe width="496" height="314" src="http://www.youtube.com/embed/http://www.vtubetools.com/watch?v=IYX_Ql-3U10?&playlist=qlzWvXJj-xY" frameborder="0" allowfullscreen></iframe>
				</div>
<!--				<div id="b">
					<iframe width="496" height="314" src="http://www.youtube.com/embed/http://www.vtubetools.com/watch?v=IYX_Ql-3U10?&playlist=kbwGukbfk20" frameborder="0" allowfullscreen></iframe>
				</div>-->
				<div id="c">
					<iframe width="496" height="314" src="http://www.youtube.com/embed/http://www.vtubetools.com/watch?v=IYX_Ql-3U10?&playlist=k-d65g-gTMM" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<!--- Tabs --->
		</div>
	</div>
</section>
<!--- ====== / Videos ====== --->


<!--- ====== Contact Us ====== --->
<section class="contact-us section">
	<div class="container">

		<h2>CONTACT US</h2>

		<?php
		echo $this->Form->create('ContactUs', array('id' => 'contact_us',
			'url' => $this->Misc->get_fully_qualified_url('home/index')
		));
		?><!-- Add Class 'error' on input fields for error -->
		<table cellspacing="0" cellpadding="0" width="600">
			<tr>
				<td><?php echo $this->Form->input('name', array('label' => 'Name <span class="required">*</span>', 'required' => 'required', 'div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'error')))); ?></td>
				<td><?php echo $this->Form->input('email', array('label' => 'Email <span class="required">*</span>', 'type' => 'email', 'required' => 'required', 'div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'error')))); ?></td>
				<td><?php echo $this->Form->input('phone', array('div' => false, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'error')))); ?></td>
			</tr>
			<tr>
				<td colspan="3">
					<?php
					echo $this->Form->label('Message', 'Message <span class="required">*</span>');
					echo $this->Form->textarea('message', array('required' => 'required', 'div' => false));
					?></td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="submit" class="btn_purple" value="Submit">
				</td>
			</tr>

		</table>
		</form>

	</div>
</section>
<!--- ====== / Contact Us ====== --->