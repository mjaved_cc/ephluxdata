<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$desc = __d('cake_dev', SITE_NAME);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<!--============================Head============================-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="noindex,nofollow" />


		<!--=========Title=========-->
		<title>
			<?php echo $desc ?>:
			<?php echo $title_for_layout; ?>
		</title>

		<!--=========Stylesheets=========-->
		<?php echo $this->Minify->css($css_list); ?>
	</head>
	<!--End Head-->

	<!--============================Begin Body============================-->
	<body id="login_page">

		<!--Wrapper of 450px-->
		<div class="wrapper content">
			<?php echo $this->fetch('content'); ?>
		</div>
		<!--End Wrapper-->

		<!--=========Load JS Files=========-->
		<?php echo $this->Minify->script($js_list); ?>
	</body>
	<!--End Body-->


</html>