<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$cakeDescription = __d('cake_dev', SITE_NAME);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $cakeDescription ?>
		</title>
		<?php
		echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1.0');

		echo $this->Html->meta('robots', 'index,follow,noodp,noydir');
		echo $this->Html->meta('description', 'Community Payback App, nominate a project for Community Service and Community Punishment in your local community.');
		echo $this->Html->meta('keywords', 'Community Payback, Community Service, Community Punishment, punish offenders');


		// Fav-Icon 
		echo $this->Html->meta('favicon.ico', $this->Misc->get_fully_qualified_url(IMAGES_URL . 'favicon.ico'), array('type' => 'icon', 'rel' => 'shortcut icon'));
		// Style
		echo $this->Minify->css($css_list);
		?>
		<!-- Css for no JS -->
		<noscript>
			<?php echo $this->Html->css('nojs'); ?>
		</noscript>
		<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>
		<link rel="canonical" href="http://www.communitypayback.org/" /> 
	</head>
	<body>
		<!--- ====== Header ====== --->
		<header>
			<div class="container">
				<h1 class="logo">
					<?php echo $this->Html->link(__(SITE_NAME), $this->Misc->get_fully_qualified_url('home/index') ); ?></h1>
			</div>
		</header>
		<!--- ====== / Header ====== --->

		<?php echo $this->fetch('content'); ?>

		<!--- ====== Footer ====== --->
		<footer>
			<div class="container">

				<div class="app-stores">
					<ul>
						<li><a href="https://play.google.com/store/apps/details?id=com.paradex.payback#?t=W251bGwsMSwyLDIxMiwiY29tLnBhcmFkZXgucGF5YmFjayJd" class="google-play coming-soon">Get it on Google Play</a></li>
						<li><a href="https://itunes.apple.com/pk/app/id615992844?mt=8" class="app-store">Available on the App Store</a></li>
					</ul>
				</div>

				<a href="<?php echo $this->Misc->get_fully_qualified_url('home/index') ?>"><h5 class="logo"><?php echo SITE_NAME; ?></h5></a><br />
			</div>
			<div class="copyright">

				<div class="container">

					<nav>
						<ul>
							<li><a href="<?php echo $this->Misc->get_fully_qualified_url('about-us') ?>">About</a></li>
							<li><a href="<?php echo $this->Misc->get_fully_qualified_url('terms-of-use') ?>">Terms of Use</a></li>
							<li><a href="<?php echo $this->Misc->get_fully_qualified_url('privacy') ?>">Privacy</a></li>
							<li><a href="<?php echo $this->Misc->get_fully_qualified_url('cookies') ?>">Cookies</a></li>
							<li><a href="<?php echo $this->Misc->get_fully_qualified_url('press') ?>">Press</a></li>
						</ul>
					</nav>	

					<p>&copy; <a href="#"><?php echo SITE_NAME ?></a> <?php date('Y') ?>. All Rights Reserved</p>

				</div>
			</div>
		</footer>
		<!--- ====== / Footer ====== --->

		<!--- ====== Scripts ====== --->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
		<?php echo $this->Minify->script($js_list); ?>
		<!--- ====== / Scripts ====== --->		

	</body>
</html>
