<?php

App::uses('AppModel', 'Model');

/**
 * ArosAco Model
 *
 */
class ArosAco extends AppModel {

	/**
	 * Remove all permissions for given Group
	 * 
	 * @param string|int $group_id Group id
	 * @return boolean True on success
	 */
	function remove_all_by_group_id($group_id) {

		if (!empty($group_id)) {
			
			$this->query('call remove_arosacos_by_group_id("' . $group_id . '" , @status)');
			
			return $this->get_status();
		} else {
			return false;
		}
	}
	
	/**
	 * Get all aco ids from ArosAco by group id
	 * 
	 * @param $group_id Group_id
	 * @return array Result
	 */
	function get_all_by_group_id($group_id){
		$result = $this->query('call get_all_arosacos_by_group_id("' . $group_id . '")');
		
		if(!empty($result))
			return $result;
		
		return false;
	}

}

?>
