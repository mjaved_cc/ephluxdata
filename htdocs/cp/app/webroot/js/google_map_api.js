var GoogleMapApi = new function(){}

GoogleMapApi.map;
GoogleMapApi.markers = [];
GoogleMapApi.info_window ;
GoogleMapApi.searchBox ;
GoogleMapApi.bounds = new google.maps.LatLngBounds();

GoogleMapApi.initialize = function (lat , lng ) {
	var haightAshbury = new google.maps.LatLng(lat , lng);
	var mapOptions = {
		zoom: 12,
		center: haightAshbury,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	GoogleMapApi.map = new google.maps.Map(document.getElementById('map'),
		mapOptions);
		
	GoogleMapApi.search_location();
	
	google.maps.event.addListener(GoogleMapApi.map, 'bounds_changed', function() {
		
		var bounds = GoogleMapApi.map.getBounds();
		GoogleMapApi.searchBox.setBounds(bounds);
	});
}

// Add a marker to the map and push to the array.
GoogleMapApi.addMarker = function (lat,lng) {
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat,lng),
		map: GoogleMapApi.map
	});
	 
	GoogleMapApi.markers.push(marker);
}

/**
 * Create pane/window using HTML
 * 
 * @return string
 **/
GoogleMapApi.detail_window = function(address, image_src) {
	return '<div class="info_window"><img src="' + image_src + '" /><span>'+ address +'</span><br /></div>';
}
/**
 *	Wrapper Googles Map InfoWindow
 **/
GoogleMapApi.wrapper_info_window = function(address, image_src) { 
	GoogleMapApi.info_window = new google.maps.InfoWindow({  
		content:  GoogleMapApi.detail_window(address, image_src)
	}); 
}
/**
 *	Show detail window on click 
 **/
GoogleMapApi.bind_detail_window = function(marker,address, image_src) {
	
	
	// Add listener for a click on the pin
	google.maps.event.addListener(marker, 'click', function() {  
		if (GoogleMapApi.info_window) GoogleMapApi.info_window.close();
		GoogleMapApi.wrapper_info_window(address, image_src)
		GoogleMapApi.info_window.open(GoogleMapApi.map, marker);  
	});
}
// Sets the map on all markers in the array.
GoogleMapApi.setAllMap = function (map) {
	for (var i = 0; i < GoogleMapApi.markers.length; i++) {
		GoogleMapApi.markers[i].setMap(map);
	}
}

// Removes the overlays from the map, but keeps them in the array.
GoogleMapApi.clearOverlays = function () {
	GoogleMapApi.setAllMap(null);	
}

// Shows any overlays currently in the array.
GoogleMapApi.showOverlays = function () {
	GoogleMapApi.setAllMap(GoogleMapApi.map);
}

// Deletes all markers in the array by removing references to them.
GoogleMapApi.deleteOverlays = function () {
	GoogleMapApi.clearOverlays();
	GoogleMapApi.markers = [];
}
GoogleMapApi.search_location = function (){
	var input = /** @type {HTMLInputElement} */(document.getElementById('search_place'));
	GoogleMapApi.searchBox = new google.maps.places.SearchBox(input);
				
	google.maps.event.addListener(GoogleMapApi.searchBox, 'places_changed', function() {
		var places = GoogleMapApi.searchBox.getPlaces();
		
		if(places != undefined){
			
			GoogleMapApi.clearOverlays();
				
			var bounds = new google.maps.LatLngBounds();
			
			for (var i = 0, place; place = places[i]; i++) {
				var image = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				var marker = new google.maps.Marker({
					map: GoogleMapApi.map,
					icon: image,
					title: place.name,
					position: place.geometry.location
				});

				GoogleMapApi.markers.push(marker);				
				bounds.extend(place.geometry.location);
				GoogleMapApi.deleteOverlays();
			}
		
			//			var image = {
			//				url: place.icon,
			//				size: new google.maps.Size(71, 71),
			//				origin: new google.maps.Point(0, 0),
			//				anchor: new google.maps.Point(17, 34),
			//				scaledSize: new google.maps.Size(25, 25)
			//			};
			//
			//			var marker = new google.maps.Marker({
			//				map: GoogleMapApi.map,
			//				icon: image,
			//				title: place.name,
			//				position: place.geometry.location
			//			});

//			GoogleMapApi.markers.push(marker);
//			GoogleMapApi.deleteOverlays();
//			bounds.extend(place.geometry.location);

			GoogleMapApi.map.fitBounds(bounds);
			GoogleMapApi.map.setZoom(16);
			PaybackWebservice.get_lat_lng();
		}
			
	});
}