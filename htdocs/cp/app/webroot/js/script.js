$(document).ready(function() {
	//bxslider enabled
	$('.bxslider').bxSlider({
		auto: true,
		mode: 'fade'
	});
	
	//TABS
	$('#tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
			
	//Validator	
	$("#contact_us").validator({
		speed: 1000,
		errorClass: 'error',
		offset: [0, 10],
		position: 'bottom left',
		messageClass:'validator'		
	});
	
});


