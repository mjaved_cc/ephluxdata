<?php

class DtUser {

	private $_fields = array();
	private $_allowed_keys = array('id','first_name','last_name','username','group_id','created','status');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get() {

		return $this->_fields;
	}

	/**
	 * Add single social account to user object
	 * 
	 * @param array $user_social_data user social information
	 */
	function add_user_social_account($user_social_data) {
		if (isset($this->_fields["UserSocialAccount"]) && !is_array($this->_fields["UserSocialAccount"])) {
			$prev_value = $this->UserSocialAccount; // restore previous value
			unset($this->_fields["UserSocialAccount"]); // now unset it so that we can convert object to array
			$this->_fields["UserSocialAccount"][] = $prev_value;
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		} else if (isset($this->_fields["UserSocialAccount"]) && is_array($this->_fields["UserSocialAccount"]))
			$this->_fields["UserSocialAccount"][] = new DtUserSocialAccount($user_social_data);
		else
			$this->_fields["UserSocialAccount"] = new DtUserSocialAccount($user_social_data);
	}

	/**
	 * Add multiple social accounts to user object
	 * 
	 * @param array $user_social_data user social information
	 */
	function add_user_social_accounts($user_social_data) {
		if (!empty($user_social_data)) {
			foreach ($user_social_data as $value)
				$this->add_user_social_account($value);
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'username' => $this->username,
			'created' => $this->created,
			'status' => $this->status,
			'group_id' => $this->group_id
		);
	}

}