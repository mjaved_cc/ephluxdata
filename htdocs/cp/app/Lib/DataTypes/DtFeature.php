<?php

class DtFeature {

	private $_fields = array();
	private $_allowed_keys = array('id', 'description', 'created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {
		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;
			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key]->getDate();
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	/**
	 * Add single auth_action_maps to features object
	 * 
	 * @param array $auth_action_maps
	 */
	function add_auth_action_map($auth_action_maps) {
		if (isset($this->_fields["AuthActionMap"]) && !is_array($this->_fields["AuthActionMap"])) {
			$prev_value = $this->AuthActionMap; // restore previous value
			unset($this->_fields["AuthActionMap"]); // now unset it so that we can convert object to array
			$this->_fields["AuthActionMap"][] = $prev_value;
			$this->_fields["AuthActionMap"][] = new DtAuthActionMap($auth_action_maps);
		} else if (isset($this->_fields["AuthActionMap"]) && is_array($this->_fields["AuthActionMap"]))
			$this->_fields["AuthActionMap"][] = new DtAuthActionMap($auth_action_maps);
		else
			$this->_fields["AuthActionMap"] = new DtAuthActionMap($auth_action_maps);
	}

	/**
	 * Add multiple auth_action_maps to features object
	 * 
	 * @param array $auth_action_maps
	 */
	function add_auth_action_maps($auth_action_maps) {
		if (!empty($auth_action_maps)) {
			foreach ($auth_action_maps as $value)
				$this->add_auth_action_map($value);
		}
	}

	function get_field() {

		return array(
			'id' => $this->id,
			'description' => $this->description,
			'created' => $this->created
		);
	}

}

?>
