<?php

App::uses('AppController', 'Controller');

/**
 * Homes Controller
 *
 * @property Home $Home
 */
class HomeController extends AppController {

	public $components = array('Curl','WrapperXML');
	public $helpers = array('Text');

	function beforeFilter() {

		parent::beforeFilter();
		$this->Auth->allow();
	}

	function index() {
		$this->layout = LAYOUT_HOME ;
		
		if ($this->request->is('post')) {
			$message = $this->data['ContactUs'];
			$this->App->send_email(
					EMAIL_NOREPLY, SITE_NAME . ' - Contact Us', $message, $this->data['ContactUs']['email'], 'contactus'
			);
			$this->redirect(BASE_URL . '/home/index');
		}
		$this->JCManager->add_js('payback_webservice');
		$data = array();
		$this->Curl->url = PAYBACK_WEBSERVICE_URI;
		$this->Curl->data = array(
			'method' => PAYBACK_WEBSERVICE_METHOD_GET_RECENT_ENTRIES
		);
		$response = $this->Curl->get();

		/*		 * ****************************** */

		// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
		// $response['http_code'] != 200 > error: no page, no permissions, no service

		/*		 * ****************************** */

		if ($response['error_code'] == ECODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
			$content = $this->App->decoding_json($response['content']);

			if (isset($content['code']) && $content['code'] == ECODE_SUCCESS) {
				$data = $content['body'];
			} else { // in case of failure
			}
		} else { // in case of failure
		}

		$this->set(compact('data'));
	}

	function get_lat_lng() {
		$data = array();
		$this->Curl->url = PAYBACK_WEBSERVICE_URI;
		$this->Curl->data = array(
			'method' => PAYBACK_WEBSERVICE_METHOD_GET_LATLNG
		);
		$response = $this->Curl->get();

		/*		 * ****************************** */

		// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
		// $response['http_code'] != 200 > error: no page, no permissions, no service

		/*		 * ****************************** */

		if ($response['error_code'] == ECODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
			$content = $this->App->decoding_json($response['content']);

			if (isset($content['code']) && $content['code'] == ECODE_SUCCESS) {
				$data = $content['body'];
			} else { // in case of failure
			}
		} else { // in case of failure
		}

		$this->set(compact('data'));
	}

	function about_us() {
		$body = $this->WrapperXML->get_about_us();
		$this->set(compact('body'));
	}

	function cookies() {
		
	}

	function privacy() {
		
	}

	function press() {
		
	}

	function terms_of_use() {
		$body = $this->WrapperXML->get_terms_of_use();
		$this->set(compact('body'));
	}

	/**
	 * Download image 
	 * 
	 * @param string $image_name
	 * @return void
	 */
	function download_image($image_name) {
		if (!empty($image_name)) {
			$image_path = IMAGES_URL . $image_name ; // $this->App->get_fully_qualified_url(IMAGES_URL . $image_name);

			$image_details = pathinfo($image_path);
			
			header("Content-type: image/" . $image_details['extension']);
			header('Content-Disposition: attachment; filename="' . $image_name . '"');
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header("Cache-Control: public");
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($image_path));
			readfile($image_path);
			exit;
		} 
	}

}