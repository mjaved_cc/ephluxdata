<?php

App::uses('AppController', 'Controller');

/**
 * AuthActionMaps Controller
 *
 * @property AuthActionMap $AuthActionMap
 */
class AuthActionMapsController extends AppController {

	private $_crud_actions;
	public $uses = array('AuthActionMap', 'Group', 'ArosAco', 'Aro');

	function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow(array('sync_action_maps'));
		$CRUD_ACTIONS = Configure::read('CRUD_ACTIONS');
		$this->_crud_actions = $CRUD_ACTIONS;
		$this->set('crud_actions', $this->_crud_actions);
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->AuthActionMap->recursive = 0;
		$this->set('authActionMaps', $this->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->AuthActionMap->id = $id;
		if (!$this->AuthActionMap->exists()) {
			throw new NotFoundException(__('Invalid auth action map'));
		}

		$authActionMap = $this->AuthActionMap->read(null, $id);
		$assigned_groups = $this->AuthActionMap->get_assigned_groups($authActionMap['Action']['id']);
		$this->set(compact('authActionMap', 'assigned_groups'));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AuthActionMap->create();
			if ($this->AuthActionMap->save($this->request->data)) {
				$this->Session->setFlash(__('The auth action map has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auth action map could not be saved. Please, try again.'));
			}
		}
		$acos = $this->AuthActionMap->Aco->find('list');
		$this->set(compact('acos'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->AuthActionMap->id = $id;
		if (!$this->AuthActionMap->exists()) {
			throw new NotFoundException(__('Invalid auth action map'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AuthActionMap->save($this->request->data)) {
				$this->Session->setFlash(__('The auth action map has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auth action map could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AuthActionMap->read(null, $id);
		}

		$features = $this->AuthActionMap->Feature->find('list', array(
			'fields' => array('id', 'description'),
			'order' => 'description asc'
				));
		$this->set(compact('features'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AuthActionMap->id = $id;
		if (!$this->AuthActionMap->exists()) {
			throw new NotFoundException(__('Invalid auth action map'));
		}
		if ($this->AuthActionMap->delete()) {
			$this->Session->setFlash(__('Auth action map deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Auth action map was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Populate data in auth_action_maps for Auth actionMap
	 */
	function sync_action_maps() {

		$this->autoRender = false;

		// sync actions in acos with that of auth map action so that we can map which action to treat which of crud operations
		$acos = $this->{$this->modelClass}->get_ctrl_actions();

		if (!empty($acos)) {
			$file = self::_get_am_handler();
			$file->delete();
			$data = array();
			foreach ($acos as $key => $value) {
				$data[$key][$this->modelClass]['aco_id'] = $value['Child']['id'];
				$data[$key][$this->modelClass]['created'] = $this->App->get_current_datetime();
				$data[$key][$this->modelClass]['modified'] = $this->App->get_current_datetime();
			}
			$this->AuthActionMap->custom_saveAll($data);
			$this->Session->setFlash(__('Aco nodes for all your controllers and actions has been synchronized'));
		} else {
			$this->Session->setFlash(__('Aco nodes is already synchronized'));
		}
		$this->redirect(array('action' => 'index'));
		
	}

}
