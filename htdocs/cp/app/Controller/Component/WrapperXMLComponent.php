<?php

class WrapperXMLComponent extends Component{
	
	private $_controller ;
	private $_xml ;
	
	function startup(Controller $controller) {
		$this->_controller = $controller;
	}
	
	function build($input){
		// New method using SimpleXML
		$this->_xml = Xml::build($input);
	}
	
	/**
	 * Returns this XML structure as a array.
	 */
	private function _to_array(){
		return Xml::toArray($this->_xml);
	}

	/**
	 * Get content of xml file as array for file named payback.xml
	 * 
	 */
	function get_payback_xml_content(){		
		return $this->_to_array();
		
	}
	
	/**
	 * Get about us content from xml file
	 * 
	 * @return string 
	 */
	function get_about_us(){
		$this->build(PAYBACK_STATIC_CONTENT_XML_URL);
		$content = $this->get_payback_xml_content();
		return $this->get_body_content($content['root']['AU']);		
	}
	
	/**
	 * Get about us content from xml file
	 * 
	 * @return string 
	 */
	function get_terms_of_use(){
		$this->build(PAYBACK_STATIC_CONTENT_XML_URL);
		$content = $this->get_payback_xml_content();
		return $this->get_body_content($content['root']['TC']);		
	}
	
	/**
	 * Get inner html enclosed with body tag.
	 * 
	 * @param string $content HTML
	 * @return string body-tag inner html
	 */
	function get_body_content($content){
		$body_tag = '<body>';
		$start = strpos($content, $body_tag ) + strlen($body_tag);		
		$end = strpos($content, '</body>');
		return substr($content, $start, ($end - $start));
	}
}
?>
