<?php
/**
* Module mod_sj_html5audio For Joomla 2.5.x
* Version		: 1.2
* Created by	: SuperJoom
* Email			: info@superjoom.com
* Created on	: 20 August 2012
* Last Modified : 28 Sptember 2012
* URL			: www.superjoom.com
* Copyright (C) 2012-2012  Super Joom
* License GPLv2.0 - http://www.gnu.org/licenses/gpl-2.0.html
*/
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');
require_once (dirname(__FILE__).DS.'helper.php');
$audio = modHtml5AudioHelper::getSJAudio($params);
$document = &JFactory::getDocument();
$document->addStyleSheet( JURI::root() . 'modules/mod_sj_html5audio/plugin/css/style.css' );
$false= '0';
for ($i=1;$i<21;$i++){
$radiosong="radiosong$i";
$ssong="song$i";
$ssongtitle="songtitle$i";
$ssongartist="songartist$i";
$ssongduration="songduration$i";
$ssongalbum="songalbum$i";
$songalbum[]=$params->get($ssongalbum);
$songduration[]=$params->get($ssongduration);
$songartist[]=$params->get($ssongartist);
$songtitle[]=$params->get($ssongtitle);
$song[]=$params->get($ssong);
$enablesong[]=$params->get($radiosong);
$backgroundaudio=$params->get('backgroundaudio');
$titlecolor=$params->get('titlecolor');
$artistcolor=$params->get('artistcolor');
$titleshadow=$params->get('titleshadow');
$tracklistcolor=$params->get('tracklistcolor');
$controlscolor=$params->get('controlscolor');
$titlefont=$params->get('titlefont');
$autoplay=$params->get('autoplay');
$titlehoover=$params->get('titlehoover');
$trackcolor=$params->get('trackcolor');
$radiohtml5audio=$params->get('radiohtml5audio');
$fontsizetitleplaylist=$params->get('fontsizetitleplaylist');
$radiohtml5audio2='1';
$songstobeshow=$params->get('songstobeshow');
$audioplayerwidth=$params->get('audioplayerwidth');
}
$adioplayerpath = JURI::base().'modules/mod_sj_html5audio/mix/';
if ($autoplay == '1')
{
	$autoplay2='true';
}else{
	$autoplay2='false';
	}
	$audioplayerwidth2='370';
if 	($audioplayerwidth != $audioplayerwidth2){
$audioplayerrest=$audioplayerwidth2 - $audioplayerwidth;
$albumcoverwh='125';
$albumcoverwh2=$albumcoverwh-($audioplayerrest)/2;
$tracklisttitle='250';
$tracklisttitle2=$tracklisttitle-($audioplayerrest)/1.08;
$playercontrol='215';
$playercontrol2=$playercontrol-($audioplayerrest)/2;
$playerprogress='135';
$playerprogress2=$playerprogress-($audioplayerrest)/2;
}else{
	$albumcoverwh2='125';
	$tracklisttitle2='250';
	$playercontrol2='215';
	$playerprogress2='135';
}
?>
<?php if ($radiohtml5audio == $radiohtml5audio2){
		echo "<!--"; }else{
		}?>
<script type="text/javascript" language="javascript" src="<?php echo $LiveSite; ?>modules/mod_sj_html5audio/js/jquery-1.6.1.min.js"></script>
<?php if ($radiohtml5audio == $radiohtml5audio2){
		echo "-->"; }else{
		}?>
<script type="text/javascript" language="javascript" src="<?php echo $LiveSite; ?>modules/mod_sj_html5audio/plugin/jquery-jplayer/jquery.jplayer.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $LiveSite; ?>modules/mod_sj_html5audio/plugin/ttw-music-player.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<?php
$audioentry ='';
$audioentry .='<div id="sj_audio_wrapper" >
<script type="text/javascript">
var $sjaudio = jQuery.noConflict();
        $sjaudio(document).ready(function($sjaudio){
		   var description = " ";
            $sjaudio("#sj_audio_wrapper").ttwMusicPlayer(myPlaylist, {
				autoplay:'.$autoplay2.',
               	description:description,
				tracksToShow:'.$songstobeshow.',
                jPlayer:{
                    swfPath:"modules/mod_sj_html5audio/plugin/jquery-jplayer" 
                }
            });
        });
    </script>
</div>
';
$songtitle = str_replace("'","\'",$songtitle);
$song = str_replace("'","\'",$song);
$songartist= str_replace("'","\'",$songartist);
$songalbum= str_replace("'","\'",$songalbum);
echo $audioentry;	
$playlist ='';
$playlist .='<script type="text/javascript">
var myPlaylist = [';
$i=-1;
foreach ($enablesong as $enablesng) {
$i=$i+1;
 if ($enablesng == $false){
		$playlist .= " "; }else{
		$playlist .= "
		 {
        mp3:'$adioplayerpath$song[$i].mp3',
        oga:'$adioplayerpath$song[$i].ogg',
        title:'$songtitle[$i]',
        artist:'$songartist[$i]',
        duration:'$songduration[$i] ',
        cover:'$adioplayerpath$songalbum[$i].jpg'
	},
		";
		}
		}		
$playlist .= ']
</script>';
echo $playlist;
?>
<style type="text/css">
		#sj_audio_wrapper{
			width: <?php echo $audioplayerwidth ?>px;
			background:<?php echo $backgroundaudio ?>;
		}
		.ttw-music-player .tracklist .title {
			width: <?php echo $tracklisttitle2 ?>px !important;
			font-size:<?php echo $fontsizetitleplaylist ?>px !important;
		}
		.ttw-music-player .tracklist {	
			color:#FFF;
			background-color:#494949;
		}
		.title:hover {	
			background-color:<?php echo $titlehoover ?>;
		}
		.ttw-music-player .player-controls {
			background-color:#000;
		}
		.ttw-music-player .track-info p {
			color: <?php echo $artistcolor ?>;
			font-weight: <?php echo $titlefont ?>;
		}
		.ttw-music-player .player .title {
				color: <?php echo $titlecolor ?>;
				font-weight: <?php echo $titlefont ?>;
				text-shadow: 0 <?php echo $titleshadow ?>px <?php echo $titleshadow ?>px rgba(0, 0, 0, 0.8);
		}
		.ttw-music-player .artist {
				color: <?php echo $artistcolor ?>;
		}
		.ttw-music-player {
			background-color: transparent;	
			margin: 0px;
			width:<?php echo $audioplayerwidth ?>px;
		}	
		.panel_audio {
			padding-top: 0px !important;
		}
		.ttw-music-player .tracklist {
			background-color: <?php echo $tracklistcolor ?> !important;
			padding-top:5px;
			margin-top: 25px;
		}
		.ttw-music-player .player-controls {
			background-color: <?php echo $controlscolor ?>;
		}
		.ttw-music-player li.playing{
			color: <?php echo $trackcolor ?> !important;
		}
		.ttw-music-player .tracklist ol li {
			margin-left:0px !important;
		}
		.ttw-music-player .description.showing {
			margin:0px !important;
		}
		.ttw-music-player .rating {
			display:none !important;
		}
		.ttw-music-player .album-cover img, .ttw-music-player .album-cover .img, .ttw-music-player .album-cover .highlight {
			height:<?php echo $albumcoverwh2 ?>px !important;
			width:<?php echo $albumcoverwh2 ?>px !important;
			background-color: transparent !important;
		}
		.ttw-music-player .album-cover, .ttw-music-player .album-cover .img, .ttw-music-player .album-cover .highlight {
			height:<?php echo $albumcoverwh2 ?>px !important;
			width:<?php echo $albumcoverwh2 ?>px !important;
		}
		.ttw-music-player .track-info {
			width:auto !important;
		}
		.ttw-music-player .player-controls{
			width:<?php echo $playercontrol2 ?>px!important;
		}
		.ttw-music-player .progress-wrapper{
			width:<?php echo $playerprogress2 ?>px!important;
		}
 </style>