<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */


// no direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_jartists')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

require_once(JPATH_ROOT.'/components/com_jartists/libraries/libraries.php');
require_once(dirname(__FILE__) . '/helpers/jartists.php');
// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JController::getInstance('Jartists');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
