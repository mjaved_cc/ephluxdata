<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
// Import CSS
$document = &JFactory::getDocument();
$document->addStyleSheet('components/com_jartists/assets/css/jartists.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'artist.cancel' || document.formvalidator.isValid(document.id('artist-form'))) {
			Joomla.submitform(task, document.getElementById('artist-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jartists&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="artist-form" class="form-validate" enctype="multipart/form-data">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_JARTISTS_LEGEND_ARTIST'); ?></legend>
			<ul class="adminformlist">

            
			<li><?php echo $this->form->getLabel('id'); ?>
			<?php echo $this->form->getInput('id'); ?></li>

            
			<li><?php echo $this->form->getLabel('title'); ?>
			<?php echo $this->form->getInput('title'); ?></li>
			
            <li><?php echo $this->form->getLabel('alias'); ?>
			<?php echo $this->form->getInput('alias'); ?></li>
            
            <li><?php echo $this->form->getLabel('featured'); ?>
			<?php echo $this->form->getInput('featured'); ?></li>
            
			<li><?php echo $this->form->getLabel('label_id'); ?>
			<?php echo $this->form->getInput('label_id'); ?></li>

            
			<li><?php echo $this->form->getLabel('link_facebook'); ?>
			<?php echo $this->form->getInput('link_facebook'); ?></li>

            
			<li><?php echo $this->form->getLabel('link_twitter'); ?>
			<?php echo $this->form->getInput('link_twitter'); ?></li>

            
			<li><?php echo $this->form->getLabel('link_myspace'); ?>
			<?php echo $this->form->getInput('link_myspace'); ?></li>

            

            <li><?php echo $this->form->getLabel('state'); ?>
                    <?php echo $this->form->getInput('state'); ?></li><li><?php echo $this->form->getLabel('checked_out'); ?>
                    <?php echo $this->form->getInput('checked_out'); ?></li><li><?php echo $this->form->getLabel('checked_out_time'); ?>
                    <?php echo $this->form->getInput('checked_out_time'); ?></li>
            
			<li><?php echo $this->form->getLabel('biography'); ?>
			<div class="clr"></div><?php echo $this->form->getInput('biography'); ?></li>


            </ul>
		</fieldset>
	</div>
	<div class="width-40 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_JARTISTS_LEGEND_ARTIST'); ?></legend>
			<ul class="adminformlist">
            <?php if($this->item->profile_picture): ?>
            <li>
			<div class="clr"></div><img src="<?php echo LBLM_IMAGE_S.$this->item->profile_picture; ?>" /></li>
            <?php endif; ?>
			<li><?php echo $this->form->getLabel('profile_picture'); ?>
			<?php echo $this->form->getInput('profile_picture'); ?></li>

            </ul>
		</fieldset>
	</div>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>