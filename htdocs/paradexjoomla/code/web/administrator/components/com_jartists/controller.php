<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */


// No direct access
defined('_JEXEC') or die;

class JartistsController extends JController
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/jartists.php';

		// Load the submenu.
		JartistsHelper::addSubmenu(JRequest::getCmd('view', 'labels'));

		$view		= JRequest::getCmd('view', 'labels');
        JRequest::setVar('view', $view);

		parent::display();

		return $this;
	}
}
