<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// No direct access
defined('_JEXEC') or die;

class JartistsControllerUpload extends JController
{

    public function upload()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$model = $this->getModel('upload');
		$files = $model->upload();
		$app = JFactory::getApplication();
		$redirect_url = JRoute::_('index.php?option=com_jartists&view=photos', false);
		$this->setRedirect($redirect_url);
	}
	
	function extract()
	{
		$archivename 	= JPATH_ROOT.'/components/com_jartists/tmp/SamplePictures.zip';
		$extractdir		= JPATH_ROOT.'/components/com_jartists/tmp/samples';
		$result = JArchive::extract($archivename, $extractdir);

		if ($result === false)
		{
			return false;
		}
		
	}
}