<?php
$dfcontact = array (
  'destinationMail' => '',
  'htmlMails' => '1',
  'addServerData' => '1',
  'displayUserInput' => '1',
  'links' => '1',
  'onlineStatus' => '0',
  'captcha' => '',
  'recaptcha_theme' => 'red',
  'pageTitle' =>
  array (
    'en-GB' => 'Contact',
    'de-DE' => 'Kontakt',
  ),
  'infoText' => 
  array (
    'en-GB' => 'If you have any ideas or questions, please feel free to contact us using our contact data or the form below.',
    'de-DE' => 'Falls Sie Ideen oder Anregungen haben, melden Sie sich gerne!',
  ),
  'formText' => 
  array (
    'en-GB' => 'Just a test blabla.',
    'de-DE' => 'Nur ein test blabla.',
  ),
  'buttonText' => 
  array (
    'en-GB' => 'Send',
    'de-DE' => 'Senden',
  ),
  'field' => 
  array (
    'company' => 
    array (
      'value' => 'Doe Inc.',
      'display' => '0',
    ),
    'title' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'name' => 
    array (
      'value' => 'Jane Doe',
      'display' => '1',
      'duty' => '1',
    ),
    'position' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'addition' => 
    array (
      'value' => 'Room 11',
      'display' => '1',
      'duty' => '0',
    ),
    'street' => 
    array (
      'value' => '1th Avenue 46 st Street',
      'display' => '1',
      'duty' => '0',
    ),
    'streetno' => 
    array (
      'value' => '23',
      'display' => '1',
      'duty' => '0',
    ),
    'postbox' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'zip' => 
    array (
      'value' => '10007',
      'display' => '1',
      'duty' => '0',
    ),
    'city' => 
    array (
      'value' => 'New York',
      'display' => '1',
      'duty' => '0',
    ),
    'state' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '0',
    ),
    'country' => 
    array (
      'value' => 'United States',
      'display' => '1',
      'duty' => '0',
    ),
    'geocoordinates' => 
    array (
      'value' => '',
      'display' => '1',
    ),
    'phone' => 
    array (
      'value' => '555-2345-2342',
      'display' => '1',
      'duty' => '0',
    ),
    'ipphone' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'mobile' => 
    array (
      'value' => '555-2345-2343',
      'display' => '1',
      'duty' => '0',
    ),
    'fax' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '0',
    ),
    'email' => 
    array (
      'value' => 'test@email.com',
      'display' => '1',
      'duty' => '1',
    ),
    'website' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'skype' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'aim' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'icq' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'yahoo' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'msn' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'linkedin' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'xing' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'facebook' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'googleplus' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'twitter' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'message' => 
    array (
      'display' => '1',
      'duty' => '1',
    ),
    'checkbox' => 
    array (
      'display' => '1',
      'duty' => '1',
      'text' => array (
	    'en-GB' => 'Dummytext.',
	    'de-DE' => 'Testtext.',
	  )
    ),
  ),
  'addressFormat' => 
  array (
    'selected' => 'de',
    'templates' => 
    array (
      'self' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREET] [STREETNO]{3}
[POSTBOX]
[ZIP]{5} [CITY]
[STATE]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'de' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREET] [STREETNO]{3}
[POSTBOX]
[ZIP]{5} [CITY]
[STATE]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'fr' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[ZIP]{5} [CITY]
[STATE], [COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'uk' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[CITY]
[STATE]
[ZIP]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'us' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[CITY], [STATE]{2} [ZIP]{4}
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'jp' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[ZIP] [STATE]
[CITY]
[STREETNO] [STREET]
[POSTBOX]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
    ),
  ),
);
?>