<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.plugin.plugin');
class plgSystemCustom_price extends JPlugin{
}

function hikashop_product_price_for_quantity_in_cart(&$product){
	$currencyClass = hikashop_get('class.currency');
	$quantity = @$product->cart_product_quantity;
	if(empty($product->prices)){
		$price= new stdClass();
		$price->price_currency_id = hikashop_getCurrency();
		$price->price_min_quantity = 1;
		$product->prices = array($price);
	}
	if(!empty($product->amount)){
		foreach($product->prices as $k => $price){
			$product->prices[$k]->price_value = $product->amount;
			$product->prices[$k]->price_value_with_tax = $product->amount;
		}
	}
	$currencyClass->quantityPrices($product->prices,$quantity,$product->cart_product_total_quantity);
}

function hikashop_product_price_for_quantity_in_order(&$product){
	$quantity = $product->order_product_quantity;
	if(!empty($product->amount)){
		$product->order_product_price = $product->amount;
		$product->order_product_tax = 0;
	}
	$product->order_product_total_price_no_vat = $product->order_product_price*$quantity;
	$product->order_product_total_price = ($product->order_product_price+$product->order_product_tax)*$quantity;
}
