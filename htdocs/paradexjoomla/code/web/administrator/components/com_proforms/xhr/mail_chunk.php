<?php
/**
* @name MOOJ Proforms 
* @version 1.0
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/
	
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );


include_once (M4J_INCLUDE_ELEMENT_HELPER);

// Needs to be done for step visualisation
 $fakeObGetClean = ob_get_clean();
 $fakeObGetClean = ob_get_clean();
  
//Params
$jid = JRequest::getInt("jid",null);
if(!$jid) die('No JID!');
$fid = JRequest::getString("fid", null);
if(!$fid) die('No FID!');

$start = JRequest::getInt("start",null);

if($start === null)  die('NO START!');



$to = JRequest::getInt("to",0);
$confirmed = JRequest::getInt("confirmed",0);
$address = JRequest::getInt("address",null);
$prio = JRequest::getInt("prio",0);
$time = JRequest::getInt("time",40);
$chunks = JRequest::getInt("chunks",50);
$html_mail = JRequest::getInt("html_mail",1);
$fromname = JRequest::getString("fromname", null);
$from = JRequest::getString("from", null);
$subject = JRequest::getString("subject", null);
$body = JRequest::getString("body", null, 'default', JREQUEST_ALLOWHTML);
$body_no_html = JRequest::getString("body_no_html", null);

$bodyDB = $html_mail ? $body : $body_no_html;

$search = JRequest::getString('search',null);
$col = JRequest::getInt("col",null);
$optinFilter = JRequest::getInt("optinfilter",0);
$totalcount = JRequest::getInt("totalcount",0);


$end = $start + $chunks;
$end = ($totalcount < $end ) ? $totalcount : $end; 
$userQuery = ($address == -1) ? " AND NOT `user_id` = '0' " : "";


$db = & JFactory::getDBO();
if(!$to || ($to && !$search) ){
	$confQuery = "";
	if(!$optinfilter || !$to){
		$confQuery = $confirmed ? "AND `optin` = '1'" : "";
	}else{
		$confQuery = ( ($optinfilter == 1) || $confirmed ) ? "AND `optin` = '1'" : "AND `optin` = '0'";
	}
	
	$query = $db->setQuery(
		"SELECT  `s`.`stid`, `s`.`jid`, `s`.`fid`, `s`.`date`, `s`.`tmp_dir`, `s`.`user_id`, `s`.`user_ip`, `s`.`optin`, `u`.`name`, `u`.`username`, `u`.`email`   
		FROM #__m4j_storage AS `s` 
		LEFT JOIN `#__users` AS `u` ON (`s`.`user_id` = `u`.`id`)  
		WHERE `jid` = '$jid' ".$confQuery.$userQuery." LIMIT $start,$chunks"
	);
	
}else{
	$confQuery = "";
	if(!$optinfilter || !$to){
		$confQuery = $confirmed ? "AND `optin` = '1'" : "";
	}else{
		$confQuery = ( ($optinfilter == 1) || $confirmed ) ? "AND `optin` = '1'" : "AND `optin` = '0'";
	}
	
	$eidQuery = $col ? "AND `si`.`eid` = '$col' " : "";
	$query = $db->setQuery(
		"SELECT `s`.`stid`, `s`.`jid`, `s`.`fid`, `s`.`date`, `s`.`tmp_dir`, `s`.`user_id`, `s`.`user_ip`, `s`.`optin`, `u`.`name`, `u`.`username`, `u`.`email` 
		FROM `#__m4j_storage` AS `s`
		LEFT JOIN `#__users` AS `u` ON (`s`.`user_id` = `u`.`id`)  
		LEFT JOIN `#__m4j_storage_items` AS `si` ON (`s`.`stid` = `si`.`stid`)
		WHERE `content` LIKE '%".trim(dbEscape($search))."%' AND `jid` = '$jid' ".$eidQuery.$confQuery.$userQuery." 
		GROUP BY `s`.`stid`  LIMIT $start,$chunks"
	);	
}
    
$rows = $db->loadObjectList();

$elements = new ElementHelper($jid, true, false);
// Getting the mail object
$mail =& JFactory::getMailer();
$mail->From 	= $from;
$mail->FromName = $fromname;

if($html_mail){
	$mail->IsHTML(true); 
}else{
	$mail->IsHTML(false);
}

$mail->CharSet = M4J_MAIL_ISO;
$mail->Priority  = $prio ? 1 : 3;

$sent = 0; $failed = 0; $noaddress = 0;

?>
<html>
<body style="margin:0; padding: 0; font-family: Arial; font-size: 10px; color: black;">
<?php 
	echo '<span style="position: absolute; padding-left: 5px; top: 0; left: 0; z-index: 10000; ">';
	echo $start." - ". $end; 
	echo '</span>';
	flush();

	
$newEnd = $end - $start; 
$t= 1;
$addtoPrgress = "";
foreach($rows as $row){
	
$user_name = $row->username;
$user_realname = $row->name;
$user_ip = $row->user_ip;	
$user_email = $row->email;
	
// Opt-In Actions
$optInIdent = md5($row->jid.$fid.$row->tmp_dir.$row->user_id.$row->user_ip).":".$row->stid;
$optInURL = JURI::root().'index.php?option=com_proforms&opt=in&ident='.$optInIdent;
$optOutURL = JURI::root().'index.php?option=com_proforms&opt=out&ident='.$optInIdent;
if($html_mail){
	$optInURL = '<a href="'.$optInURL.'">'.$optInURL.'</a>'."\n";
	$optOutURL = '<a href="'.$optOutURL.'">'.$optOutURL.'</a>'."\n";
}	

$string = preg_replace('/{(\s*)J_OPT_IN(\s*)}/' , $optInURL , $bodyDB);
$string = preg_replace('/{(\s*)J_OPT_OUT(\s*)}/' , $optOutURL , $string);
$string = preg_replace('/{(\s*)J_USER_IP(\s*)}/' , $user_ip , $string);
$string = preg_replace('/{(\s*)J_USER_NAME(\s*)}/' , $user_name , $string);
$string = preg_replace('/{(\s*)J_USER_REALNAME(\s*)}/' , $user_realname , $string);



$db->setQuery( "SELECT `eid`, `content` FROM `#__m4j_storage_items` WHERE `stid` = '".$row->stid."' " );
$items = $db->loadObjectList();

$currentSubject = $subject;
$elements->replaceByAlias($items, $string, $currentSubject);


$emailAddress =  "";

switch ($address){
		
	case -1: 
		$emailAddress = $user_email;
		break;
	case 0: 
		if( $uniqueEID = $elements->getUniqueEID() ){
			$db->setQuery( "SELECT `content` FROM `#__m4j_storage_items` WHERE `stid` = '".$row->stid."' AND `eid` = '$uniqueEID' LIMIT 1" );
			$eml = $db->loadObject();	
			$emailAddress = $eml->content;
		}
		
		break;
		
	default:
		$db->setQuery( "SELECT `content` FROM `#__m4j_storage_items` WHERE `stid` = '".$row->stid."' AND `eid` = '$address' LIMIT 1" );
		$eml = $db->loadObject();	
		$emailAddress = $eml->content;
		break;
	
}


if(preg_match('/^([a-zA-Z0-9!#?^_`.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,6})$/', $emailAddress)){
	$mail->Subject 	= $currentSubject;
	$mail->Body 	= $string;
	$mail->ClearAddresses();
	$mail->AddAddress($emailAddress);
	if($mail->Send()){
		$sent++;
	}else{
		$failed++;
	}
}else{
	// No valid email address
	$failed++;
	$noaddress++;
}



$percent = ( ($t++/$newEnd) * 150 );  
$addtoPrgress .= str_replace('"', '\"', $string ) ."<hr/>";
?>	
<div style="position: absolute; left:0; top:0; display:block; width: <?php echo $percent; ?>px; height: 12px; background-color: #88ff88; z-index: 1; color:white; text-align: right; "></div>
<?php 
flush();
}//EOF foreach rows
?>
<?php echo $addtoPrgress; ?>
</body>
<script type="text/javascript" >
	parent.promail.progress({start: <?php echo $start; ?>, end: <?php echo $end; ?>, sent : <?php echo $sent; ?>, failed: <?php echo $failed; ?>, noaddress: <?php echo $noaddress; ?> });
</script>
</html>
