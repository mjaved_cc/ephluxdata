<?php
/**
* @name MOOJ Proforms 
* @version 1.0
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/
	
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

//Params
$jid = JRequest::getInt("jid",null);
if(!$jid) die('{error: "NO_JID" }');
$fid = JRequest::getString("fid", null);
if(!$fid) die('{error: "NO_FID" }');

$to = JRequest::getInt("to",0);
$confirmed = JRequest::getInt("confirmed",0);
$address = JRequest::getInt("address",null);
$prio = JRequest::getInt("prio",0);
$time = JRequest::getInt("time",40);
$chunks = JRequest::getInt("chunks",50);
$html_mail = JRequest::getInt("html_mail",1);
$fromname = JRequest::getString("fromname", null);
$from = JRequest::getString("from", null);
$subject = JRequest::getString("subject", null);
$body = JRequest::getString("body", null, 'default', JREQUEST_ALLOWHTML);
$body_no_html = JRequest::getString("body_no_html", null);

$bodyDB = $html_mail ? $body : $body_no_html;
    
$search = JRequest::getString('search',null);
$col = JRequest::getInt("col",null);
$optinfilter = JRequest::getInt("optinfilter",0);

$userQuery = ($address == -1) ? " AND NOT `user_id` = '0' " : "";


$db = & JFactory::getDBO();
if(!$to || ($to && !$search) ){
	$confQuery = "";
	if(!$optinfilter || !$to){
		$confQuery = $confirmed ? "AND `optin` = '1'" : "";
	}else{
		$confQuery = ( ($optinfilter == 1) || $confirmed ) ? "AND `optin` = '1'" : "AND `optin` = '0'";
	}
	
	$query = $db->setQuery(
		"SELECT COUNT(`stid`) AS `count` FROM `#__m4j_storage` WHERE `jid` = '$jid' ".$confQuery.$userQuery." LIMIT 1"
	);
	
}else{
	$confQuery = "";
	if(!$optinfilter || !$to){
		$confQuery = $confirmed ? "AND `optin` = '1'" : "";
	}else{
		$confQuery = ( ($optinfilter == 1) || $confirmed ) ? "AND `optin` = '1'" : "AND `optin` = '0'";
	}
	
	$eidQuery = $col ? "AND `si`.`eid` = '$col' " : "";
	$query = $db->setQuery(
		"SELECT COUNT(*) AS `count` FROM
		( SELECT `s`.`stid` FROM `#__m4j_storage` AS `s`
		LEFT JOIN `#__m4j_storage_items` AS `si` ON (`s`.`stid` = `si`.`stid`)
		WHERE `content` LIKE '%".trim(dbEscape($search))."%' AND `jid` = '$jid' ".$eidQuery.$confQuery.$userQuery." 
		GROUP BY `s`.`stid` ) AS `allrecords` LIMIT 1"
	);	
}
    
$row = $db->loadObject();
?>
{
count: <?php echo (int) $row->count; ?>,
to: <?php echo $to; ?>,
optinfilter: <?php echo $optinfilter; ?>,
confQuery: "<?php echo $confQuery; ?>",
search: "<?php echo $search; ?>",
col: <?php echo $col; ?>

}
