<?php
/**
* @name MOOJ Proforms 
* @version 1.0
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/
	
    defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );  
    
    $getmt = JRequest::getInt("getmt",0);
    
	$mailTemplates = MDB::get('#__m4j_mail_templates',array("mtid","title"));
	foreach($mailTemplates as $mt){
?>
<?php if($getmt):?>
<span class="mailTemplateItem">
	<a class="mtUpdate" onclick="javascript: mtGet(<?php echo $mt->mtid; ?>); return false;"><?php echo $mt->title; ?></a> 
</span>		
<?php else: ?>
<span class="mailTemplateItem">
	<a class="mtUpdate" onclick="javascript: mtUpd(<?php echo $mt->mtid; ?>); return false;"><?php echo $mt->title; ?></a> 
	<img onclick="javascript: mtDelete(<?php echo $mt->mtid; ?>);" class="mtRemove" src="<?php echo M4J_IMAGES;?>remove.png" title="<?php echo M4J_LANG_DELETE; ?>" />
</span>		
<?php endif;?>
<?php }?>