<?php
/**
* @name MOOJ Proforms 
* @version 1.1
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/
	
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );


class Opt extends JObject{
	var $is_optin = 0;
	var $optin_email_after_confirmation = 0;  
	var $optinredirect = null;
	var	$optoutredirect = null;
	var $optin = null;
	var $optout = null;
	var $optin_no_confirmation_email = 0;
	var $optin_subject = null;
	var $optout_subject = null;
	
	var $keys = array("optin_email_after_confirmation","optinredirect","optoutredirect","optin","optout","optin_no_confirmation_email","optin_subject","optout_subject");
	
	function __construct($is_optin= 0, $optin_params = null){
		$this->setParams($is_optin,$optin_params);
	}//EOF constuct
	
	function setParams($is_optin= 0, $optin_params = null){
		$this->is_optin = $is_optin;
		if($optin_params){
			$params = explode("\n",$optin_params);
			foreach($params as $p){
				$split = explode("\t",$p);
				if(sizeof($split) == 2){
					$key = trim($split[0]);
					$value = base64_decode(trim($split[1]));
					if(in_array($key,$this->keys)){
						//write value
						$this->$key = $value;
					}//EOF is in keys
				}//EOF size equals 2
			}//EOF foreach			
		}//EOF is optin_params
	}
		
	
	function createParams($paramArray = null){
		if(!$paramArray) return null;
		$out = "";
		foreach($paramArray as $key => $value){
			$key = trim($key);
			$value = trim($value);
			$out .= $key . "\t" . base64_encode($value) . "\n"; 
		}//EOF foreach
		return $out;
	}//EOF createParams
	
	function optInMail($usermail= null,  $replace = null){
		if(!$usermail || $this->optin_no_confirmation_email) return null;
		$subject = ($this->optin_subject) ? $this->optin_subject : sprintf(M4J_LANG_OPTIN_CONFIRMATION_SUBJECT, substr_replace(JURI::root(), '', -1, 1));
		$body = (M4J_HTML_MAIL) ? $this->optin : strip_tags( str_replace( array("<br>","<br />","<br/>","</p>") ,"\n",$this->optin ) ) ;
		
		$mail = m4jCreateMail( M4J_FROM_EMAIL, M4J_FROM_NAME, $this->replaceByAlias($subject,$replace),  $this->replaceByAlias($body,$replace) );
		$mail->CharSet = M4J_MAIL_ISO;
		$mail->IsHTML(M4J_HTML_MAIL);
		$mail->AddAddress($usermail);
		return $mail->Send();
	}

	function optOutMail($usermail= null,  $replace = null){
		if(!$usermail || $this->optin_no_confirmation_email) return null;
		$subject = ($this->optout_subject) ? $this->optout_subject : sprintf(M4J_LANG_OPTOUT_CONFIRMATION_SUBJECT, substr_replace(JURI::root(), '', -1, 1));
		$body = (M4J_HTML_MAIL) ? $this->optout : strip_tags( str_replace( array("<br>","<br />","<br/>","</p>") ,"\n",$this->optout ) ) ;
		
		$mail = m4jCreateMail( M4J_FROM_EMAIL, M4J_FROM_NAME, $this->replaceByAlias($subject,$replace),  $this->replaceByAlias($body,$replace) );
		$mail->CharSet = M4J_MAIL_ISO;
		$mail->IsHTML(M4J_HTML_MAIL);
		$mail->AddAddress($usermail);
		return $mail->Send();
	}
	
	function replaceByAlias($body = null, $replace= null){
		if(!$replace) return $body;
		foreach($replace as $key => $value){
			$regex = '/{(\s*)'.$key.'(\s*)}/';
			$body = preg_replace($regex , $value , $body);
		}
		return $body;
	}
	
	function storeMail($fromMail,$fromName,$subject,$mailBody,$to,$stid=0){
		$stid = (int) $stid;
		$params = "fromMail"."\t".base64_encode($fromMail)."\n";
		$params .= "fromName"."\t".base64_encode($fromName)."\n";
		$params .= "subject"."\t".base64_encode($subject)."\n";
		$params .= "mailBody"."\t".base64_encode($mailBody)."\n";
		$params .= "to"."\t".base64_encode($to)."\n";
		
		$db = JFactory::getDBO();
		$db->setQuery("SELECT `parameters` FROM `#__m4j_storage` WHERE `stid` = '$stid' LIMIT 1");
		$rec = $db->loadObject();
		$parameters = ($rec && $rec->parameters ) ? $rec->parameters."\n".$params : $params;
		
		MDB::update("#__m4j_storage",array("parameters" => $parameters),"`stid` = '$stid'","LIMIT 1");
		
	}
	
}//EOF class OPT    
    
    
?>