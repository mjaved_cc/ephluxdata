<?php
/**
* @name MOOJ Proforms 
* @version 1.0
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/
	
    defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );
    require_once M4J_INCLUDE_FUNCTIONS;
    require_once M4J_INCLUDE_MSELECTION;
    
    $fid = JRequest::getInt("fid",null);

	
	function §($string){
		echo $string."\n";
	}
	
	if(!$fid) die();
	$lang =& JFactory::getLanguage();
	$lang_code = substr($lang->getTag(),0,2);
	
	if(! file_exists(JPATH_ROOT . '/components/com_proforms/js/calendar/lang/calendar-'.$lang_code.'.js') || M4J_FORCE_CALENDAR) $lang_code = "en";

		
	§('<div id="preview" style="display:none;">
		<h2>'.	M4J_LANG_PREVIEW.'</h2>	'.
		'<div style="width:100%;" id="proforms_proforms" class="m4j_form_wrap">
			<form id="m4jForm" action="" enctype="multipart/form-data" method="post" name="m4jForm">');
	
	$layout = MLayoutList::getLayoutById($fid);
	$eidHeap = array();
	// DB Query Drawing the Table
		$query = "SELECT * FROM #__m4j_formelements WHERE `fid` = '".$fid."' AND `active` = '1' ORDER BY `slot`,`sort_order` ASC";
		$database->setQuery( $query );
		$formElements = $database->loadObjectList();
		
		
		foreach($formElements as $element){
			
			if($element->active){
				//Add eid to eid heap
				array_push($eidHeap, $element->eid);
				
				if($element->form== 2){
					$element->html = str_replace("{M4J_YES}", M4J_LANG_YES, $element->html);
					$element->html = str_replace("{M4J_NO}", M4J_LANG_NO, $element->html);
				}
					
				if($element->form>=50 && $element->form<60){
					$layout->addHTMLRow($element->slot,$element->html);
				}else if($element->form>=30 && $element->form<40){
						$selo = new MSelections($element, null);
						$html = $selo->getBuffer();
						$layout->addRow($element->slot,$element->question,stripslashes($html),$element->required,$element->help,$element->align,$element->usermail,$element->eid ,$hidden);
				}else{
						$hidden = ($element->form == 23) ? 1 : 0;
						$strip = "{".$element->eid."}";
						$html = str_replace($strip,"",$element->html);
						$layout->addRow($element->slot,$element->question,stripslashes($html),$element->required,$element->help,$element->align,$element->usermail,$element->eid ,$hidden);
				}
			}//ELement Active
		}//EOF foreach loop
		$layout->render(true); 
		§('</div></div>');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head><meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript"><?php echo "\t var pfmFields = [" .implode(",", $eidHeap)."];\n"; ?></script>
</head>
<body style="margin:0; padding:0; width:100%; height: 100%;">
<iframe frameborder="0" src="<?php echo $m4jConfig_live_site?>/index.php?option=com_proforms&dummy=1" width="100%"  style="height: 100%; overflow: visible;" name="dummy"></iframe>
</body>
</html>


