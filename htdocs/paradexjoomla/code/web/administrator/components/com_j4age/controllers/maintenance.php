<?php

              

              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          
             
          

if( !defined( '_JS_STAND_ALONE' ) && !defined( '_JEXEC' ) )
{
	die( 'JS: No Direct Access to '.__FILE__ );
}


//require_once( dirname(__FILE__).'/base.classes.php' );
require_once( dirname(__FILE__) .DS.'..'.DS. 'libraries'.DS. 'util.classes.php' );
require_once( dirname(__FILE__) .DS.'..'.DS.'api'.DS. 'tools.php' );
jimport('joomla.application.component.controller');

require_once( dirname( __FILE__ ) .DS. 'main.php' );


/**
 *  Joomla Stats Tools class
 *
 *  This contain features from 'Tools' tab in 'Joomla Stats' Configuration panel.
 *  Basicly contain maintenance functions
 *
 *  NOTICE: This class should contain only set of static, argument less functions that are called by task/action
 */
class j4ageControllerMaintenance extends j4ageControllerMain
{
    /**
     * This function optimize all JoomlaStats database tables
     * new from v2.3.0.170, tested - OK
     *
     * return true on success
     */
	function doOptimizeDatabase() {

        //$this->JoomlaStatsEngine->FilterDomain->show_time_period_filter = false;

        JRequest::setVar('view', JRequest::getVar('view', 'tools'));

		$JSUtil = new js_JSUtil();
		$res = $JSUtil->optimizeAllJSTables();

		if ($res == false) {
                        $msg = JText::_( 'COM_J4AGE_DATABASE_OPTIMIZATION_FAILED' );
            JError::raiseNotice( 0, $msg );
		}
        else
        {
            $msg = JText::_( 'COM_J4AGE_DATABASE_SUCCESSFULLY_OPTIMIZED' );
//            $this->setMessage( $msg );
//            $this->setRedirect();
            JError::raiseNotice( 0, $msg );            
        }

        $this->display();
	}


   function doDropOldData()
   {
       JRequest::setVar('view', JRequest::getVar('view', 'tools'));
       $days = JRequest::getInt('periodIndays', 730);

       $JSUtil = new js_JSUtil();
       $res = $JSUtil->dropData("#__jstats_visits", "changed_at", $days);
       if($res == true) $res = $JSUtil->dropData("#__jstats_referrer", "timestamp", $days);
       if($res == true) $res = $JSUtil->dropData("#__jstats_keywords", "timestamp", $days);
       if($res == true) $res = $JSUtil->dropData("#__jstats_impressions", "timestamp", $days);
       //if($res == true) $res = $JSUtil->releaseUnusedData("#__jstats_pages", "#__jstats_impressions", "page_id", "page_id");
       //We should consider to also clean-up all clients & ipaddresses, which are no more used
       if($res == true) $res = $JSUtil->releaseUnusedData("#__jstats_clients", "#__jstats_visits", "client_id", "client_id");
       if($res == true) $res = $JSUtil->releaseUnusedData("#__jstats_ipaddresses", "#__jstats_visits", "ip", "ip");

       if ($res != true) {
           $msg = JText::_( $res );
           JError::raiseError( 0, $msg );
       }
       else
       {
           $msg = JText::_( 'COM_J4AGE_DATABASE_SUCCESSFULLY_DROPPED' );
           JError::raiseNotice( 0, $msg );
       }

       $this->display();

   }

    
	/**
	 *  backup database
	 *
	 *  function removed due to to many deprecated and not working code. Previus version do not make a backup! (in many cases it brake database!)
	 */
    function backupDatabase() {
    }


    function douninstall()
    {
        JRequest::setVar('view', 'uninstall');

        $JSTools = new js_JSTools();
        $JSTools->doJSUninstall();
        //$this->display(false);
    }


    function saveConfiguration()
    {
        //JRequest::setVar('view', JRequest::getVar('view', 'configuration'));
        //$this->display(false);
        $JSConf 	= js_JSConf::getInstance();
        $this->SetConfiguration( $JSConf->startoption );
    }

    function applyConfiguration()
    {
        //JRequest::setVar('view', JRequest::getVar('view', 'configuration'));
        //$this->display(false);
        $this->SetConfiguration( 'js_view_configuration' );
    }

    function setDefaultConfiguration()
    {
        JRequest::setVar('view', JRequest::getVar('view', 'configuration'));

        //convienient way to get default configuration within 'current configuration' object
        $JSConf    = new js_JSConf(false);

        $msg                = JText::_( 'COM_J4AGE_DEFAULT_CONFIGURATION_SET' );
        $err_msg	= '';
        $res		= $JSConf->storeConfigurationToDatabase( $err_msg );

        if( $res == false) {
            $msg		= JText::_( '' );//@todo missing message
            $this->setRedirect( 'index.php?option=com_j4age&task=js_view_configuration', $msg, 'error' );//third argument: 'message', 'notice', 'error'
            return false;
        }

        $msg                = JText::_( 'COM_J4AGE_CHANGES_SAVED' );
        $this->setRedirect( 'index.php?option=com_j4age&task=js_view_configuration', $msg, 'message' );//third argument: 'message', 'notice', 'error'
        //return true;


        //$this->display(false);
    }

    	/**
	 * Stores the JoomlaStats configuration
	 *
	 * @deprecated language since 2.3.x (mic)
	 * @param unknown_type $redirect_to_task
	 */
	function SetConfiguration( $redirect_to_task ) {
		$JSConfDef 	= new js_JSConfDef();
		$JSConf 	= js_JSConf::getInstance();

		$JSConf->onlinetime 		= isset($_POST['onlinetime'])			? $_POST['onlinetime']		: $JSConfDef->onlinetime;
        $JSConf->onlinetime_bots    = isset($_POST['onlinetime_bots'])	    ? $_POST['onlinetime_bots']	: $JSConfDef->onlinetime_bots;

		$JSConf->startoption		= isset($_POST['startoption'])			? $_POST['startoption'] 	: $JSConfDef->startoption;
		$JSConf->startdayormonth	= isset($_POST['startdayormonth'])		? $_POST['startdayormonth'] : $JSConfDef->startdayormonth;
		//$JSConf->language			= isset($_POST['language']) 			? $_POST['language']		: $JSConfDef->language;
		$JSConf->include_summarized	= isset($_POST['include_summarized'] )	? true						: false; //this is checkbox. It have to be serve in different way //$JSConfDef->include_summarized;
		{//temporary solution
			//$JSConf->show_summarized	= isset($_POST['show_summarized'] )	? true						: false; //this is checkbox. It have to be serve in different way //$JSConfDef->show_summarized;
			$JSConf->show_summarized	= isset($_POST['include_summarized'] )	? true					: false; //this is checkbox. It have to be serve in different way //$JSConfDef->show_summarized;
		}
		$JSConf->enable_whois		= isset($_POST['enable_whois']) 		? true						: false; //this is checkbox. It have to be serve in different way //$JSConfDef->enable_whois;
        $JSConf->enable_i18n		= isset($_POST['enable_i18n'])			? true						: false; //this is checkbox. It have to be serve in different way //$JSConfDef->enable_i18n;
        $JSConf->show_charts_within_reports	= isset($_POST['show_charts_within_reports']) ? true	    : false; 

        $enable_index_clients_useragent = JRequest::getBool('enable_index_clients_useragent', false);
        $enable_index_impressions_visit = JRequest::getBool('enable_index_impressions_visit', false);
        $enable_index_visits_changed_at = JRequest::getBool('enable_index_visits_changed_at', false);
        $enable_index_visits_ip         = JRequest::getBool('enable_index_visits_ip', false);

		$err_msg	= '';
		$res		= $JSConf->storeConfigurationToDatabase( $err_msg );

        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();
        if($enable_index_clients_useragent)
        {
            if(!$JSDatabaseAccess->hasTableIndexForColumn('#__jstats_clients', 'useragent'))
            {
                $JSDatabaseAccess->addIndex('#__jstats_clients', 'useragent');
            }
        }
        else
        {
            if($JSDatabaseAccess->hasTableIndexForColumn('#__jstats_clients', 'useragent'))
            {
                $JSDatabaseAccess->dropIndex('#__jstats_clients', 'useragent');
            }
        }

        if($enable_index_impressions_visit)
        {
            if(!$JSDatabaseAccess->hasTableIndexForColumn('#__jstats_impressions', 'visit_id'))
            {
                $JSDatabaseAccess->addIndex('#__jstats_impressions', 'visit_id');
            }
        }
        else
        {
            if($JSDatabaseAccess->hasTableIndexForColumn('#__jstats_impressions', 'visit_id'))
            {
                $JSDatabaseAccess->dropIndex('#__jstats_impressions', 'visit_id');
            }
        }

        if($enable_index_visits_changed_at)
        {
            if(!$JSDatabaseAccess->hasTableIndexForColumn('#__jstats_visits', 'changed_at'))
            {
                $JSDatabaseAccess->addIndex('#__jstats_visits', 'changed_at');
            }
        }
        else
        {
            if($JSDatabaseAccess->hasTableIndexForColumn('#__jstats_visits', 'changed_at'))
            {
                $JSDatabaseAccess->dropIndex('#__jstats_visits', 'changed_at');
            }
        }

        if($enable_index_visits_ip)
        {
            if(!$JSDatabaseAccess->hasTableIndexForColumn('#__jstats_visits', 'ip'))
            {
                $JSDatabaseAccess->addIndex('#__jstats_visits', 'ip');
            }
        }
        else
        {
            if($JSDatabaseAccess->hasTableIndexForColumn('#__jstats_visits', 'ip'))
            {
                $JSDatabaseAccess->dropIndex('#__jstats_visits', 'ip');
            }
        }

		if( $res == false) {
			$msg		= JText::_( '' );//@todo missing message
			$this->setRedirect( 'index.php?option=com_j4age&task='.$redirect_to_task, $msg, 'error' );//third argument: 'message', 'notice', 'error'
			return false;
		}

                $msg                = JText::_( 'COM_J4AGE_CHANGES_SAVED' );
		$this->setRedirect( 'index.php?option=com_j4age&task='.$redirect_to_task, $msg, 'message' );//third argument: 'message', 'notice', 'error'
		return true;
	}

    function applyParsedUseragents()
    {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $client_ids	= JRequest::getVar( 'cid', array() );
        $browser_ids	= JRequest::getVar( 'browser_id', array() );
        $browser_versions	= JRequest::getVar( 'browser_version', array() );
        $visitor_types	= JRequest::getVar( 'visitor_type', array() );
        $os_ids	= JRequest::getVar( 'os_id', array() );
        $afilter	= JRequest::getVar( 'afilter', '' );

        $this->setRedirect( 'index.php?option=com_j4age&view=debugbrowser&afilter='.$afilter, $msg, 'message' );//third argument: 'message', 'notice', 'error'
        
        foreach($client_ids as $index=>$client_id)
        {
            $browser_id = $browser_ids[$index];
            $browser_version = $browser_versions[$index];
            $visitor_type = $visitor_types[$index];
            $os_id = $os_ids[$index];
            $Client = new js_Client();
            $Client->client_id = $client_id;
            $Client->browser_version = $browser_version;
            $Client->client_type = $visitor_type;
            $Client->Browser = new js_Browser();
            $Client->Browser->browser_id = $browser_id;
            if($os_id != null)
            {   
                $Client->OS = new js_Os();
                $Client->OS->os_id = $os_id;
            }
            IPInfoHelper::updateClient($Client, true);
        }

        //$this->display(true);

    }

	/**
	 * This function include/exclude addresses that are in $_REQUEST['cid'] array
	 * old name excludeIpAddress
	 *
	 * @param string	$action = 'include': to include addresses; 'exclude': to exlcude addresses
	 */
	function excludeIpAddressArr( $exclude = false ) {
		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$cidv	= JRequest::getVar( 'cid', 0 );
		$vid	= JRequest::getVar( 'vid', array( 0 ) );
		$cid	= array( 0 );
        $block = $exclude;

		if( is_array( $cidv ) ) {
			$cid = $cidv;
		}else{
			if( $cidv !== 0 ) {
				$cid[0] = $cidv;
			}
		}

		if( ( count( $vid ) > 0 ) && ( $vid[0] != 0 ) ) {
			$cid[0] = $vid;
		}

		if( count( $cid ) < 1 ) {
			$task_name = $block ? 'js_do_ip_exclude' : 'js_do_ip_include';
                        echo '<script type="text/javascript">alert(\'' . JText::_( 'COM_J4AGE_CHOOSE_AN_ENTRY' ) . ': ' . $task_name . '\'); window.history.go(-1);</script>' . "\n";
			exit;
		}

		$cids = 0;
		if( count( $cid ) > 1 ) {
			$cids = implode( ',', $cid );
		}else{
			$cids = $cid[0];
		}

        //this is a better approach: $query = 'UPDATE #__jstats_ipaddresses SET ip_exclude = MOD(ip_exclude + 1, 2) WHERE ip IN (' . $cids . ')';

		$query = 'UPDATE #__jstats_ipaddresses'
		. ' SET ip_exclude = \'' . $block . '\''
		. ' WHERE ip IN (' . $cids . ')'
		;
		$JSDatabaseAccess->db->setQuery( $query );

		if( !$JSDatabaseAccess->db->query() ) {
			echo '<script type="text/javascript">alert(\''.$JSDatabaseAccess->db->getErrorMsg().'\');window.history.go(-1);</script>' ."\n";
			exit();
		}

		if( $block ) {
                        $msg = JText::_( 'COM_J4AGE_IP_ADDRESS_EXCLUDED' );
		}else{
                        $msg = JText::_( 'COM_J4AGE_IP_ADDRESS_INCLUDED' );
		}
       
		//redirect to approprate page
		$task = 'js_view_exclude'; // return to 'Exclude Manager' page

		// use vid parameter, because we didn't want to use extra paramater to parse
		if( ( count( $vid ) > 0 ) && ( $vid[0] !=0 ) ) {
			//in case if function is called from the statistics page return to it
			$task = 'r03';
		}

        $view = JRequest::getVar('returnView', null);
        if(empty($view))
        {
            $view = JRequest::getVar('view', 'visitors');
        }
        $returnTask = JRequest::getVar('returnTask', null);

        $controller = JRequest::getVar('controller', null);
        $tpl = JRequest::getVar('tpl', null);
        $d = JRequest::getVar( 'd', '' );
		$m = JRequest::getVar( 'm', '' );
		$y = JRequest::getVar( 'y', '' );

        $this->setRedirect( "index.php?option=com_j4age&view=$view".($controller == null ? '': '&controller='.$controller).($tpl == null ? '': '&tpl='.$tpl).($returnTask == null ? '': '&task='.$returnTask)."&d=$d&m=$m&y=$y", $msg, 'message' );
	}

	/**
	 * This function include/exclude clients which are in $_REQUEST['cid'] array
	 *
	 */
	function clientExclution( $exclude = false  ) {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $cidv	= JRequest::getVar( 'cid', 0 );
        $vid	= JRequest::getVar( 'vid', array( 0 ) );
        $cid	= array( 0 );

        if( is_array( $cidv ) ) {
            $cid = $cidv;
        }else{
            if( $cidv !== 0 ) {
                $cid[0] = $cidv;
            }
        }

        if( ( count( $vid ) > 0 ) && ( $vid[0] != 0 ) ) {
            $cid[0] = $vid;
        }

        if( count( $cid ) < 1 ) {
            $task_name = $exclude ? 'excludeClients' : 'includeClients';
            echo '<script type="text/javascript">alert(\'' . JText::_( 'COM_J4AGE_CHOOSE_AN_ENTRY' ) . ': ' . $task_name . '\'); window.history.go(-1);</script>' . "\n";
            exit;
        }

        $cids = '0';
        if( count( $cid ) > 1 ) {
            $cids = implode( ',', $cid );
        }else{
            $cids = $cid[0];
        }

        //this is a better approach: $query = 'UPDATE #__jstats_ipaddresses SET ip_exclude = MOD(ip_exclude + 1, 2) WHERE ip IN (' . $cids . ')';
        $query = 'UPDATE #__jstats_clients'
        . ' SET client_exclude = \'' . ($exclude? 1:0) . '\''
        . ' WHERE client_id IN (' . $cids . ')'
        ;

        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() ) {
            echo '<script type="text/javascript">alert(\''.$JSDatabaseAccess->db->getErrorMsg().'\');window.history.go(-1);</script>' ."\n";
            exit();
        }

        if( $exclude ) {
            $msg = JText::_( 'COM_J4AGE_CLIENTS_EXCLUDED' );
        }else{
            $msg = JText::_( 'COM_J4AGE_CLIENTS_INCLUDED' );
        }

        //redirect to approprate page

        $view = JRequest::getVar('returnView', null );
        if(empty($view))
        {
          $view = JRequest::getVar('view', 'visitors');
        }
        $returnTask = JRequest::getVar('returnTask', null);

        $controller = JRequest::getVar('controller', null);
        $tpl = JRequest::getVar('tpl', null);
        $d = JRequest::getVar( 'd', '' );
		$m = JRequest::getVar( 'm', '' );
		$y = JRequest::getVar( 'y', '' );

        $this->setRedirect( "index.php?option=com_j4age&view=$view".($controller == null ? '': '&controller='.$controller).($tpl == null ? '': '&tpl='.$tpl).($returnTask == null ? '': '&task='.$returnTask)."&d=$d&m=$m&y=$y", $msg, 'message' );
	}

    /**
	 * This function include/exclude clients which are in $_REQUEST['cid'] array
	 *
	 */
	function changeClientType( $type  ) {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $cidv	= JRequest::getVar( 'cid', array(  ) );
        $vid	= JRequest::getVar( 'vid', array(  ) );
        $cid	= array(  );

		if( is_array( $cidv ) ) {
			$cid = array_merge($cid, $cidv);
		}else{
			if( !empty($cidv) ) {
				$cid[] = $cidv;
			}
		}

		if( is_array( $vid ) ) {
			$cid = array_merge($cid, $vid);
		}else{
			if( !empty($vid) ) {
				$cid[] = $vid;
			}
		}

        if( count( $cid ) < 1 ) {
//            $task_name = $exclude ? 'excludeClients' : 'includeClients';
//            echo '<script type="text/javascript">alert(\'' . JText::_( 'Please choose an entry to' ) . ': ' . $task_name . '\'); window.history.go(-1);</script>' . "\n";
            exit;
        }

        $cids = 0;
        if( count( $cid ) > 1 ) {
            $cids = implode( ',', $cid );
        }else{
            $cids = $cid[0];
        }

        //this is a better approach: $query = 'UPDATE #__jstats_ipaddresses SET ip_exclude = MOD(ip_exclude + 1, 2) WHERE ip IN (' . $cids . ')';

        $query = 'UPDATE #__jstats_clients'
        . ' SET client_type = \'' . ($type) . '\''
        . ' WHERE client_id IN (' . $cids . ')'
        ;

        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() ) {
            echo '<script type="text/javascript">alert(\''.$JSDatabaseAccess->db->getErrorMsg().'\');window.history.go(-1);</script>' ."\n";
            exit();
        }
		$msg = "";
        if( $type == 0 ) {
            $msg .= JText::_( 'Type of Client(s) changed to unknown client' );
        }else if($type == 1){
            $msg .= JText::_( 'Type of Client(s) changed to browser' );
        }else{
            $msg .= JText::_( 'Type of Client(s) changed to bot' );
        }

        //redirect to approprate page
        $view = JRequest::getVar('returnView', 'visitors');

        // use vid parameter, because we didn't want to use extra paramater to parse
        if( ( count( $vid ) > 0 ) && ( $vid[0] !=0 ) ) {
            //in case if function is called from the statistics page return to it
          //  $view = 'r03';
        }

        $view = JRequest::getVar('returnView', JRequest::getVar('view', 'visitors'));
        $returnTask = JRequest::getVar('returnTask', null);

        $controller = JRequest::getVar('controller', null);
        $tpl = JRequest::getVar('tpl', null);
        $d = JRequest::getVar( 'd', '' );
		$m = JRequest::getVar( 'm', '' );
		$y = JRequest::getVar( 'y', '' );

        $this->setRedirect( "index.php?option=com_j4age&view=$view".($controller == null ? '': '&controller='.$controller).($tpl == null ? '': '&tpl='.$tpl).($returnTask == null ? '': '&task='.$returnTask)."&d=$d&m=$m&y=$y", $msg, 'message' );
	}

    function classifyAsBrowser()
    {
        $this->changeClientType( 1 );
    }

    function classifyAsBot()
    {
        $this->changeClientType( 2 );
    }
    function classifyAsGhost()
    {
        $this->changeClientType( 0 );
    }

        /**
	 * This function include/exclude clients which are in $_REQUEST['cid'] array
	 *
	 */
	function changeIPType( $type = 0  ) {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $cidv	= JRequest::getVar( 'cid', 0 );
        $vid	= JRequest::getVar( 'vid', array( 0 ) );
        $cid	= array( 0 );

        if( is_array( $cidv ) ) {
            $cid = $cidv;
        }else{
            if( $cidv !== 0 ) {
                $cid[0] = $cidv;
            }
        }

        if( ( count( $vid ) > 0 ) && ( $vid[0] != 0 ) ) {
            $cid[0] = $vid;
        }

        if( count( $cid ) < 1 ) {
//            $task_name = $exclude ? 'excludeClients' : 'includeClients';
//            echo '<script type="text/javascript">alert(\'' . JText::_( 'Please choose an entry to' ) . ': ' . $task_name . '\'); window.history.go(-1);</script>' . "\n";
            exit;
        }

        $cids = 0;
        if( count( $cid ) > 1 ) {
            $cids = implode( ',', $cid );
        }else{
            $cids = $cid[0];
        }

        //this is a better approach: $query = 'UPDATE #__jstats_ipaddresses SET ip_exclude = MOD(ip_exclude + 1, 2) WHERE ip IN (' . $cids . ')';

        $query = 'UPDATE #__jstats_ipaddresses'
        . ' SET ip_type = \'' . ($type) . '\''
        . ' WHERE ip IN (' . $cids . ')'
        ;

        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() ) {
            echo '<script type="text/javascript">alert(\''.$JSDatabaseAccess->db->getErrorMsg().'\');window.history.go(-1);</script>' ."\n";
            exit();
        }

        if( $type = 0 ) {
            $msg = JText::_( 'Type of IP(s) changed to unknown client' );
        }else if($type = 1){
            $msg = JText::_( 'Type of IP(s) changed to browser' );
        }else{
            $msg = JText::_( 'Type of IP(s) changed to bot' );
        }

        /*$query = 'UPDATE #__jstats_clients'
        . ' SET client_type = \'' . ($type) . '\''
        . ' WHERE client_id IN (' . $cids . ')'
        ;

        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() ) {
            echo '<script type="text/javascript">alert(\''.$JSDatabaseAccess->db->getErrorMsg().'\');window.history.go(-1);</script>' ."\n";
            exit();
        }*/

        //redirect to approprate page
        $view = JRequest::getVar('returnView', 'visitors');

        // use vid parameter, because we didn't want to use extra paramater to parse
        if( ( count( $vid ) > 0 ) && ( $vid[0] !=0 ) ) {
            //in case if function is called from the statistics page return to it
          //  $view = 'r03';
        }

        $view = JRequest::getVar('returnView', JRequest::getVar('view', 'visitors'));
        $returnTask = JRequest::getVar('returnTask', null);

        $controller = JRequest::getVar('controller', null);
        $tpl = JRequest::getVar('tpl', null);
        $d = JRequest::getVar( 'd', '' );
		$m = JRequest::getVar( 'm', '' );
		$y = JRequest::getVar( 'y', '' );

        $this->setRedirect( "index.php?option=com_j4age&view=$view".($controller == null ? '': '&controller='.$controller).($tpl == null ? '': '&tpl='.$tpl).($returnTask == null ? '': '&task='.$returnTask)."&d=$d&m=$m&y=$y", $msg, 'message' );
	}

    function classifyIPAsBrowser()
    {
        $this->changeIPType( 1 );
    }

    function classifyIPAsGhost()
    {
        $this->changeIPType( 0 );
    }

    function classifyIPAsBot()
    {
        $this->changeIPType( 2 );
    }


    function excludeClients()
    {
        $this->clientExclution( true );
    }
    
    function includeClients()
    {
        $this->clientExclution( false );
    }

    function doIpExclude()
    {
        $this->excludeIpAddressArr( true );
    }
    function doIpInclude()
    {
        $this->excludeIpAddressArr( false );
    }

    function doExportCSV()
    {
        require_once( dirname(__FILE__) .DS.'..'.DS. 'libraries'.DS. 'export.php' );
        $JSExport = new js_JSExport();
        echo $JSExport->exportJSToCsv();
    }

    function fixip2nationname()
    {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        //Some DBs do not mind the case-sensitive commands unless the name is really different
        $query = "ALTER IGNORE TABLE #__ip2nationcountries RENAME  `#__ip2nationCountries_temp`";

        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() )
        {
            JError::raiseNotice( 0, $JSDatabaseAccess->db->getErrorMsg() );
        }
        else
        {
            $query = "ALTER IGNORE TABLE #__ip2nationCountries_temp RENAME  `#__ip2nationCountries`";

            $JSDatabaseAccess->db->setQuery( $query );
            if( !$JSDatabaseAccess->db->query() )
            {
                JError::raiseNotice( 0, $JSDatabaseAccess->db->getErrorMsg() );
            }
            else
            {
            JError::raiseNotice( 0,  JText::_( 'COM_J4AGE_DATABASE_SUCCESSFULLY_OPTIMIZED' ));
            }
        }
        $this->status();
    }

    function handleip2nationupload_internal()
    {
        jimport('joomla.filesystem.*');
        jimport('joomla.filesystem.archive');

        $fieldName = "update_ip2nation";
        $fileName = $_FILES[$fieldName]['name'];
        $fileSize = $_FILES[$fieldName]['size'];
        //check the file extension is ok
        $uploadedFileNameParts = explode('.',$fileName);
        $uploadedFileExtension = array_pop($uploadedFileNameParts);

        $validFileExts = explode(',', 'zip');

        //assume the extension is false until we know its ok
        $extOk = false;

        //go through every ok extension, if the ok extension matches the file extension (case insensitive)
        //then the file extension is ok
        foreach($validFileExts as $key => $value)
        {
            if( preg_match("/$value/i", $uploadedFileExtension ) )
            {
                $extOk = true;
            }
        }

        if ($extOk == false)
        {
            JError::raiseWarning(1, JText::_('INVALID EXTENSION' ));
            return;
        }

        //the name of the file in PHP's temp directory that we are going to move to our folder
        $fileTemp = $_FILES[$fieldName]['tmp_name'];

        //we are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
        //types, where we might miss one (whitelisting is always better than blacklisting)
        $okMIMETypes = 'application/zip,application/x-zip,application/x-zip-compressed,application/octet-stream,application/x-compress,application/x-compressed,multipart/x-zip';
        $validFileTypes = explode(",", $okMIMETypes);

        $mimeType = $_FILES[$fieldName]['type'];

        //if the temp file does not have a width or a height, or it has a non ok MIME, return
        if( !in_array($mimeType, $validFileTypes) )
        {
            JError::raiseWarning(1, JText::_('INVALID FILETYPE' ));
            return;
        }

        //lose any special characters in the filename
        $fileName = preg_replace("/[^A-Za-z0-9.]/", "-", $fileName);

        //always use constants when making file paths, to avoid the possibilty of remote file inclusion
        $uploadDirectory = JPATH_SITE.DS.'tmp'.DS.'ip2nation';

        $uploadPath = $uploadDirectory.DS.$fileName;
        $cleanUploadPath = JPath::clean($uploadPath);

        $sqlfile = $uploadDirectory.DS."ip2nation.sql";

        if(!JFile::upload($fileTemp, $cleanUploadPath))
        {
            JError::raiseWarning(1, JText::_('ERROR UPLOAD FILE' ));
            return;
        }

        if (!is_file($cleanUploadPath)) {
           JError::raiseWarning(1, JText::_('ERROR COPYING FILE' ));
            return;
        }

        $result = JArchive::extract($cleanUploadPath, $uploadDirectory);
        if ($result === false)
        {
            JError::raiseWarning(1, JText::_('ERROR UNZIP FILE'));
            return false;
        }
        $buffer = file_get_contents($sqlfile);

        // Graceful exit and rollback if read not successful
        if ($buffer === false)
        {
            JError::raiseWarning(1, JText::_('JLIB_INSTALLER_ERROR_SQL_READBUFFER'));

            return false;
        }
        $splittedSQL = $this->splitSql($buffer);
        unset($buffer);//release memory
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $bufferCountries = array();
        $bufferIPs = array();

        $isInsert = false;
        $index = 0;
        foreach($splittedSQL as $splittedSQLEntry)
        {
            if(!$isInsert)
            {
                $isInsert = strpos($splittedSQLEntry, "INSERT ") > -1;
            }

            if($isInsert)
            {
                if(strlen($splittedSQLEntry) < 13){
                   continue;
                }
                //INSERT INTO ip2nation (ip, country) VALUES(n, 'xxx');
                //needs to be changed to INSERT DELAYED IGNORE INTO ip2nation (ip, country) VALUES(n, 'xxx');
                $isIp2NationTableName = 13;//is always 12 //strpos($splittedSQLEntry, "ip2nation"); //does also catch ip2NationCountries

                $isCountryTable = strrpos($splittedSQLEntry,'ip2nationCountries');

                $valuesIndex = strrpos($splittedSQLEntry,'VALUES(');
                $valuesIndexEnd =  strlen($splittedSQLEntry) - ( $valuesIndex+7 );
                $values = substr($splittedSQLEntry,$valuesIndex+6, $valuesIndexEnd );
                
                //$query = substr($splittedSQLEntry,$isIp2NationTableName);
                //$query = "INSERT DELAYED IGNORE INTO #__".$query;
                $isCountryTable = $isCountryTable > -1;
                 if($isCountryTable)
                 {
                     $this->internal_processingScript($JSDatabaseAccess,$values, $bufferCountries, $isCountryTable, false);
                 }else{
                     $this->internal_processingScript($JSDatabaseAccess,$values, $bufferIPs, $isCountryTable, false);
                 }
            }else{
                $query = str_replace("ip2nation", "#__ip2nation", $splittedSQLEntry);
                $this->internal_executeScript($JSDatabaseAccess, $query);
                //$this->internal_executeScript($JSDatabaseAccess,$query, $splittedSQLEntry, $index);
            }
            $index++;
        }

        $this->internal_processingScript($JSDatabaseAccess,null, $bufferIPs, false, true);
        $this->internal_processingScript($JSDatabaseAccess,null, $bufferCountries, true, true);


        if(!JFile::delete($uploadPath))
        {
            JError::raiseWarning( 0, JText::_( 'ERROR TEMP FILE DELETE' ) );
            return;
        }
        if(!JFile::delete($sqlfile))
        {
            JError::raiseWarning( 0, JText::_( 'ERROR TEMP SQL FILE DELETE' ) );
            return;
        }
        JError::raiseNotice( 0, $index." SQL updates performed" );

           // success, exit with code 0 for Mac users, otherwise they receive an IO Error
           //exit(0);
    }

    function internal_processingScript(&$JSDatabaseAccess, $entry, &$buffer, $isCountryTable, $execute )
    {
        if($entry != null)
        {
            $buffer[] = $entry;
        }

        $count = count($buffer);
        if(!$execute)
        {
           $execute = $count > 500;
        }
        if($count <= 0){
           $execute = false;
        }
        if($execute){
           $this->internal_createScript($JSDatabaseAccess,$isCountryTable, $buffer);
           $buffer = array();
        }
        return $buffer;
    }

    function internal_createScript(&$JSDatabaseAccess, $isCountryTable, $entries)
    {
        if($isCountryTable)
        {
            $query = 'INSERT INTO #__ip2nationCountries (code, iso_code_2, iso_code_3, iso_country, country, lat, lon) VALUES ';
        }
        else
        {
            $query = 'INSERT INTO #__ip2nation (ip, country) VALUES ';
        }
        $first = true;
        foreach($entries as $entry)
        {
           if(!$first)
           {
               $query = $query.",\n";
           }
           $query = $query . $entry;
           $first = false;
        }
        $this->internal_executeScript($JSDatabaseAccess, $query);
    }

    function internal_executeScript(&$JSDatabaseAccess, $query)
    {
        if(false)
        {
           echo '$query[] = "'.$query."\";\n";
           return;
        }
        $JSDatabaseAccess->db->setQuery( $query );
        if( !$JSDatabaseAccess->db->query() )
        {
            JError::raiseError( 0, $JSDatabaseAccess->db->getErrorMsg()." -  SQL: ".$query );
            return;
        }
    }


    	/**
	 * Splits contents of a sql file into array of discreet queries.
	 * Queries need to be delimited with end of statement marker ';'
	 *
	 * @param   string  $sql  The SQL statement.
	 *
	 * @return  array  Array of queries
	 *
	 * @since   11.1
	 */
	public function splitSql($sql)
	{
		$db = JFactory::getDbo();
		return $db->splitSql($sql);
	}

        function handleip2nationupload()
    {

        $this->handleip2nationupload_internal();
        $this->tools();



    }

    /**
     * Method to display the view
     *
     * @access    public
     */
    function display($surroundings = false)
    {
       //$this->JoomlaStatsEngine->FilterTimePeriod->hide = true;
       parent::display($surroundings);
    }
        /**
     * Method to display the view
     *
     * @access    public
     */
/*    function execute($task)
    {
       parent::execute($task);
    }*/
}

