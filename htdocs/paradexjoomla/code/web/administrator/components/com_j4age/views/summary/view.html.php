<?php


/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */



// Check to ensure this file is only called within the Joomla! application and never directly
if( !defined( '_JEXEC' ) ) {
	die( 'JS: No Direct Access to '.__FILE__ );
}


jimport( 'joomla.application.component.view' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'database' .DS.'select.one.value.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'api' .DS. 'general.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries'.DS. 'template.html.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries'.DS. 'statistics.common.html.php' );

/**
 * Hello View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class j4ageViewSummary extends JView
{
	/**
	 * display method of Hello view
	 * @return void
	 **/
	function display($tpl = null)
	{
	/*
		$model	  =& $this->getModel();
		$text = '';
		JToolBarHelper::title(   JText::_( 'Technical Expertise' ).': <small><small>[ ' . $text.' ]</small></small>', 'dvw' );
		JToolBarHelper::save('saveEntry');
		if ($isNew)  {
			JToolBarHelper::cancel('cancelEntry');
		} else {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancelEntry', 'Close' );
		}
        */
		//show the sub content

		$engine =  JoomlaStats_Engine::getInstance();
        $JSTemplate = new js_JSTemplate();
        $JSStatisticsTpl = new js_JSStatisticsCommonTpl();

        $JSDbSOV = new js_JSDbSOV();

        $prof = & JProfiler::getInstance( 'JS' );

        $this->assignRef("engine", $engine);
        $this->assignRef("JSTemplate", $JSTemplate);
        $this->assignRef("JSStatisticsTpl", $JSStatisticsTpl);
        $this->assignRef("JSDbSOV", $JSDbSOV);
        //$this->assignRef("JSApiGeneral", $JSApiGeneral);
        $this->assignRef("prof", $prof);

		js_echoJSDebugInfo($prof->mark('begin'), '');

		//$JSUtil = new js_JSUtil();

		$buid = 0;
		$JSDbSOV->getBuid( $buid );

		$info	= ''; // new mic

		$junk  = '%';
		$month = '%';
		$year  = '%';

		$this->engine->FilterTimePeriod->getDMY( $junk, $month, $year );

		{//set month and year when not selected
            $JSNowTimeStamp = js_getJSNowTimeStamp();
			if( $month == '%' ) {
				// user selected whole month ('-')
				$month	= js_gmdate( 'n', $JSNowTimeStamp );
                $info .= JText::_( 'COM_J4AGE_MSUMMARY_NOT_CHOOSEN_MONTH' ).': <strong>'. $JSTemplate->monthToString($month, false) . '</strong>';
			}

			if( $year == '%' ) {
				$year	= js_gmdate( 'Y', $JSNowTimeStamp );
                $info .= JText::_( 'COM_J4AGE_MSUMMARY_NOT_CHOOSEN_YEAR' ).': <strong>' . $year . '</strong>';
			}
		}
		if( $info ) {
            JError::raise(E_NOTICE, 0, $info);
		}

		$daysOfMonth = array(0,31,28 + date('L',mktime(0,0,0,(int)$month,(int)1,(int)$year)),31,30,31,30,31,31,30,31,30,31);

        $timestamp_from = mktime(0, 0, 0, $month, 1, $year);
        $timestamp_to = mktime(23, 59, 59, $month, $daysOfMonth[$month], $year);

        $this->engine->FilterTimePeriod->setTimePeriod( $timestamp_from, $timestamp_to );
        $this->engine->FilterTimePeriod->getTimePeriodsDatesAsTimestamp( $timestamp_from, $timestamp_to );

        js_echoJSDebugInfo($prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_CREATING_VARIABLES' ) ), '');


        $this->assignRef("dm", $daysOfMonth);
        $this->assignRef("buid", $buid);
        $this->assignRef("year", $year);
        $this->assignRef("month", $month);

        $resolution = 'day';

        $this->retrieveData($timestamp_from, $timestamp_to, 'hour');

        $data = $this->loadData($timestamp_from, $timestamp_to, "day", 2);//$resolution);

        $data = $this->postProcessData($resolution, $data);

        $this->assignRef("rows", $data);

        $total = new stdClass();


        $this->calculateTotals($data, $total, "deleteMe");

        $this->assignRef("total", $total);

		parent::display("hour");
	}

    function retrieveData( $timestamp_from, $timestamp_to, $resolution )
    {
        if ($resolution == 'hour') {
			$res_column = ' HOUR(FROM_UNIXTIME(v.changed_at)) AS hour, DAYOFMONTH(FROM_UNIXTIME(v.changed_at)) AS day, MONTH(FROM_UNIXTIME(v.changed_at)) AS month, YEAR(FROM_UNIXTIME(v.changed_at)) AS year';
			$res_group = ' HOUR(FROM_UNIXTIME(v.changed_at)) AS hour, DAYOFMONTH(FROM_UNIXTIME(v.changed_at)), MONTH(FROM_UNIXTIME(v.changed_at)), YEAR(FROM_UNIXTIME(v.changed_at))';
		}
        else if ($resolution == 'hour') {
            $cacheKey = "ignore";
		}
        else if ($resolution == 'day') {
            $cacheKey = $this->year."-".$this->month;
		}
        else if ($resolution == 'month') {
            $cacheKey = $this->year."-".$this->month;
		} else {
            $cacheKey = $this->year;
		}

        $rows = array();

        $this->determineRows($timestamp_from, $timestamp_to, $cacheKey, $resolution, $rows);
        $this->determineCharts($timestamp_from, $timestamp_to, $cacheKey, $resolution, $rows);
    }

    function determineRows($timestamp_from, $timestamp_to, $cacheKey, $resolution, &$rows)
    {
        $prof = & JProfiler::getInstance( 'JS' );
        $JSApiGeneral = new js_JSApiGeneral();


        $res_column = "";
        $res_group = "";
        $this->getDetailDegree($resolution, &$res_column, &$res_group);

        $res_columnTime = "";
        $res_groupTime = "";
        $this->getDetailDegree($resolution, &$res_columnTime, &$res_groupTime, "timestamp");

		$unique_visitors_array = array();
		{ //get unique users per period
            require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'database' .DS. 'select.many.rows.php' );

            $JSDbSMR = new js_JSDbSMR();
			$JSDbSMR->selectNumberVisitsOfUniqueVisitorsPerPeriod( "day", $timestamp_from, $timestamp_to, $unique_visitors_array );
            $this->prepareResults($unique_visitors_array, "timestamp", "value", "type", 2);
            $resultsGrouped = $this->groupResults(&$unique_visitors_array, "type_");

            $this->storeReports("v_unique_0", $resultsGrouped, 2, "type_0");
            $this->storeReports("v_unique_1", $resultsGrouped, 2, "type_1");
            $this->storeReports("v_unique_2", $resultsGrouped, 2, "type_2");
		}

		$visitor_array = array();
		{ //get visitors
			$visitors_type = _JS_DB_IPADD__TYPE_REGULAR_VISITOR;
			$arr_obj_result = null;

			$JSApiGeneral->getAmountOfVisitsByTimeframe( $resolution, $visitors_type, $timestamp_from, $timestamp_to, $arr_obj_result, $cacheKey."-v_arr", false );
            $this->prepareResults($arr_obj_result, "changed_at", "nbr_visitors");
            $this->storeReports("v_regular", $arr_obj_result);

			if ($arr_obj_result)
            {
				foreach($arr_obj_result as $obj)
                {
                   $visitor_array[$obj->day] = $obj->nbr_visitors;
                }
			}
		}

        js_echoJSDebugInfo( $prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_COLLECTED_DATA_VARR' ) ) );
        if( js_checkForTimeOut() ) return;


		$bots_array = array();
		{ //get bots
			$visitors_type = _JS_DB_IPADD__TYPE_BOT_VISITOR;
			$arr_obj_result = null;
			$JSApiGeneral->getAmountOfVisitsByTimeframe( $resolution, $visitors_type, $timestamp_from, $timestamp_to, $arr_obj_result, $cacheKey."-b_arr", false );

            $this->prepareResults($arr_obj_result, "changed_at", "nbr_visitors");
            $this->storeReports("v_bots", $arr_obj_result);

			if ($arr_obj_result) {
				foreach($arr_obj_result as $obj)
					$bots_array[$obj->day] = $obj->nbr_visitors;
			}
		}

        js_echoJSDebugInfo( $prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_COLLECTED_DATA_BARR' ) ) );
        if( js_checkForTimeOut() ) return;

		$notIdentifiedVisitor_array = array();
		{ // not identified visitors
			$visitors_type = _JS_DB_IPADD__TYPE_NOT_IDENTIFIED_VISITOR;
			$arr_obj_result = null;
			$JSApiGeneral->getAmountOfVisitsByTimeframe( $resolution, $visitors_type, $timestamp_from, $timestamp_to, $arr_obj_result, $cacheKey."-niv_arr", false );

            $this->prepareResults($arr_obj_result, "changed_at", "nbr_visitors");
            $this->storeReports("v_unknown", $arr_obj_result);

			if ($arr_obj_result) {
				foreach($arr_obj_result as $obj)
					$notIdentifiedVisitor_array[$obj->day] = $obj->nbr_visitors;
			}
		}

        js_echoJSDebugInfo( $prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_COLLECTED_DATA_NIVARR' ) ) );
        if( js_checkForTimeOut() ) return;

		$pages_array = array();
		{ // pages

            $query =
                    "
                    SELECT SQL_BIG_RESULT count(*) AS nbr_impressions, $res_columnTime,  timestamp
                        FROM `#__jstats_impressions`
                        WHERE ( timestamp >= $timestamp_from AND timestamp <= $timestamp_to )
                        GROUP BY $res_groupTime
                        ORDER BY timestamp
                    ";

            $arr_obj_result = js_Cache::cachedQuery($cacheKey."-p_arr", $query, $timestamp_from, $timestamp_to);

            $this->prepareResults($arr_obj_result, "timestamp", "nbr_impressions");
            $this->storeReports("i_all", $arr_obj_result);

			//$this->engine->db->setQuery( $query );
			//$arr_obj_result = $this->engine->db->loadObjectList();
			if ($arr_obj_result) {
				foreach($arr_obj_result as $obj)
					$pages_array[$obj->day] = $obj->nbr_impressions;
			}

		}

        {
            $query = "SELECT count(*) as referrercount, $res_columnTime,  timestamp FROM #__jstats_referrer WHERE timestamp >= $timestamp_from AND timestamp <= $timestamp_to GROUP BY $res_groupTime  ORDER BY timestamp";
            $arr_obj_result = js_Cache::cachedQuery($cacheKey."-p_rry", $query, $timestamp_from, $timestamp_to);

            $this->prepareResults($arr_obj_result, "timestamp", "referrercount");
            $this->storeReports("r_all", $arr_obj_result);
        }

        {
            $query = "SELECT count(*) as inquiries, $res_columnTime,  timestamp FROM #__jstats_keywords     WHERE referrer_id IS NOT NULL AND timestamp >= $timestamp_from AND timestamp <= $timestamp_to GROUP BY $res_groupTime  ORDER BY timestamp";
            $arr_obj_result = js_Cache::cachedQuery($cacheKey."-inquiries-linked", $query, $timestamp_from, $timestamp_to);

            $this->prepareResults($arr_obj_result, "timestamp", "inquiries");
            $this->storeReports("q_linked", $arr_obj_result);
        }

        {
            $query = "SELECT count(*) as inquiries, $res_columnTime,  timestamp FROM #__jstats_keywords     WHERE timestamp >= $timestamp_from AND timestamp <= $timestamp_to GROUP BY $res_groupTime  ORDER BY timestamp";
            $arr_obj_result = js_Cache::cachedQuery($cacheKey."-inquiries-all", $query, $timestamp_from, $timestamp_to);

            $this->prepareResults($arr_obj_result, "timestamp", "inquiries");
            $this->storeReports("q_all", $arr_obj_result);
        }


        js_echoJSDebugInfo( $prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_BEFORE_LOOP' ) ), '');

        if( js_checkForTimeOut() ) return;


		for( $i = 1; $i <= $this->dm[$this->month]; $i++) {

            $row = new stdClass();
			$day = $i;

            $timestamp_from = mktime(0,0,0,$this->month, $day, $this->year);
            $timestamp_to   = mktime(23,59,59,$this->month, $day, $this->year);


            $row->day = $day;
            $row->year = $this->year;
            $row->month = $this->month;

            $dayCachedKey = $cacheKey.'-'.$day;

            $this->determineRow($i, $row, $dayCachedKey, $timestamp_from, $timestamp_to, $visitor_array, $bots_array, $pages_array, $notIdentifiedVisitor_array);

            $rows[] = $row;
            if( js_checkForTimeOut() ) return;

		}

        js_echoJSDebugInfo( $prof->mark( JText::_( 'COM_J4AGE_MSUMMARY_AFTER_LOOP' ) ), '');

        $this->assignRef("rows", $rows);
    }

    function determineRow( $i, &$row, $cachedKey, $timestamp_from, $timestamp_to, $visitor_array, $bots_array, $pages_array, $notIdentifiedVisitor_array)
    {
        $visitors 			            = 0; // visitors
        $bots 			                = 0; // bots
        $pages			                = 0; // pages
        $referrers			            = 0; // referrer
        $inquiries	                    = 0; // search inquiries
        $uniqueBots 		            = 0; // unique bots
        $uniqueVisitors 		        = 0; // unique visitors
        $sum		                    = 0; // sum
        $uniqueSum		                = 0; // unique sum
        $uniqueNotIdentifiedVisitors	= 0; // unique not identified visitors
        $notIdentifiedVisitors		    = 0; // not identified visitors
        $inquiriesLinked                = 0;


        { // get Unique visitors
            $visitors_type = _JS_DB_IPADD__TYPE_REGULAR_VISITOR;
            $this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cachedKey."-uv", $this->buid, $timestamp_from, $timestamp_to, $uniqueVisitors );
        }


        { // get visitors
            $visitors = (isset($visitor_array[$i])) ? $visitor_array[$i] : 0;
        }

        { // get bots
            $bots = (isset($bots_array[$i])) ? $bots_array[$i] : 0;
        }


        { // get Unique bots
            $visitors_type = _JS_DB_IPADD__TYPE_BOT_VISITOR;
            $this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cachedKey."-ub", $this->buid, $timestamp_from, $timestamp_to, $uniqueBots );
        }

        { // get Pages
            $pages = (isset($pages_array[$i])) ? $pages_array[$i] : 0;
        }

        // get Referrers
        //$mytimestamp_from = mktime(0, 0, 0, $this->month, 1, $this->year);
        //$mytimestamp_to = mktime(23, 59, 59, $this->month, $i, $this->year);

        $query = "SELECT count(*) as referrercount FROM #__jstats_referrer WHERE timestamp >= $timestamp_from AND timestamp <= $timestamp_to";
        $rObject = js_Cache::cachedObjectQuery( $cachedKey."-r", $query, $timestamp_from, $timestamp_to);
        $referrers = $rObject->referrercount;

        $query = "SELECT count(*) as inquiries FROM #__jstats_keywords WHERE referrer_id IS NOT NULL AND timestamp >= $timestamp_from AND timestamp <= $timestamp_to";
        $rObject = js_Cache::cachedObjectQuery( $cachedKey."-inquiries", $query, $timestamp_from, $timestamp_to);
        $inquiriesLinked = $rObject->inquiries;

        $referrers = $referrers-$inquiriesLinked;

        $query = "SELECT count(*) as inquiries FROM #__jstats_keywords WHERE timestamp >= $timestamp_from AND timestamp <= $timestamp_to";
        $rObject = js_Cache::cachedObjectQuery( $cachedKey."-inquiries-unlinked", $query, $timestamp_from, $timestamp_to);
        $inquiries = $rObject->inquiries;


        { // not identified visitors
            $notIdentifiedVisitors = (isset($notIdentifiedVisitor_array[$i])) ? $notIdentifiedVisitor_array[$i] : 0;
        }


        {// unique not identified visitors
            $visitors_type = _JS_DB_IPADD__TYPE_NOT_IDENTIFIED_VISITOR;
            $this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cachedKey."-univ", $this->buid, $timestamp_from, $timestamp_to, $uniqueNotIdentifiedVisitors );
        }


        // sums
        $sum  = $visitors  + $bots  + $notIdentifiedVisitors;
        $uniqueSum = $uniqueVisitors + $uniqueBots + $uniqueNotIdentifiedVisitors;

        $row->inquiries = $inquiries;
        $row->inquiriesLinked = $inquiriesLinked;

        $row->i = $i;

        $row->uniqueVisitor = $uniqueVisitors;
        $row->visitors = $visitors;
        $row->pages = $pages;
        $row->referrerCount = $referrers;
        $row->uniqueBots = $uniqueBots;
        //$row->add = $add;
        $row->bots = $bots;
        $row->uniqueNotIdentifiedVisitors = $uniqueNotIdentifiedVisitors;
        $row->notIdentifiedVisitors = $notIdentifiedVisitors;
        $row->uniqueSum = $uniqueSum;
        $row->sum = $sum;

        $row->changed_at = mktime(0,0,0, $row->month, $row->day, $row->year);


    }

    function calculateTotals(&$rows, &$total, $cacheKey)
    {
        $totalUniqueNotIdentifiedVisitors	= 0; // total unique not identified visitors
        $totalBots 		                    = 0; // total bots
        $totalPages 		                = 0; // total pages
        $totalVisitors 		                = 0; // total visitors
        $totalReferrers 		            = 0; // total referrers
        $totalInquiries                     = 0;
        $totalNotIdentifiedVisitors		    = 0; // total not identified visitors
        $totalSum		                    = 0; // total sum
        $visits_average                     = "0.0";

        foreach($rows as $row)
        {
            $totalVisitors += $row->v_regular;
            $totalBots += $row->v_bots;
            $totalPages += $row->i_all;
			$totalReferrers += $row->r_all;
            $totalInquiries += $row->q_all;
            $totalNotIdentifiedVisitors += $row->v_unknown;
			$totalSum  += $row->sum;
        }

        $totalUniqueVisitors		        = 0; // total unique visitors
        $totalUniqueSum		                = 0; // total unique sum
        $totalUniqueBots		            = 0; // total unique bots

        { // Get the values for the totals line

			$timestamp_from = mktime(0,0,0,$this->month, 1, $this->year);
			$timestamp_to   = mktime(23,59,59,$this->month, $this->dm[$this->month], $this->year);

			{ // get Unique visitors
				$visitors_type = _JS_DB_IPADD__TYPE_REGULAR_VISITOR;
				$this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cacheKey."-tuv", $this->buid, $timestamp_from, $timestamp_to, $totalUniqueVisitors );
			}

			{ // get Unique bots
				$visitors_type = _JS_DB_IPADD__TYPE_BOT_VISITOR;
				$this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cacheKey."-tub", $this->buid, $timestamp_from, $timestamp_to, $totalUniqueBots );
			}

			{// unique not identified visitors
				$visitors_type = _JS_DB_IPADD__TYPE_NOT_IDENTIFIED_VISITOR;
				$this->JSDbSOV->selectNumberOfUniqueVisitors( $visitors_type, $cacheKey."-tuniv", $this->buid, $timestamp_from, $timestamp_to, $totalUniqueNotIdentifiedVisitors );
			}

			$totalUniqueSum = $totalUniqueVisitors + $totalUniqueBots + $totalUniqueNotIdentifiedVisitors;
		}


        $total->month_or_year = $this->month;
        $total->totalUniqueVisitors           = $totalUniqueVisitors;
        $total->totalVisitors            = $totalVisitors;
        $total->totalPages            = $totalPages;
        $total->totalReferrers            = $totalReferrers;
        $total->totalUniqueBots           = $totalUniqueBots;
        $total->totalBots            = $totalBots;
        $total->totalUniqueNotIdentifiedVisitors         = $totalUniqueNotIdentifiedVisitors;
        $total->totalNotIdentifiedVisitors          = $totalNotIdentifiedVisitors;
        $total->totalUniqueSum         = $totalUniqueSum;
        $total->totalSum          = $totalSum;
        $total->inquiries     = $totalInquiries;

		//Total line (last line, sum line)

		if( ( $total->totalUniqueVisitors != 0 ) && ( $total->totalVisitors != 0 ) ) {
			$format_token = '%01.2f';
			$visits_average = sprintf($format_token, ( $total->totalVisitors / $total->totalUniqueVisitors ));
		}

        $total->visits_average     = $visits_average;

        $this->assignRef("total", $total);
    }

    function determineCharts($timestamp_from, $timestamp_to, $cacheKey, $resolution, &$rows)
    {
        $chartView =  js_getView('amline', 'html');

        if(!empty($chartView))
        {
            $chartView->createGraph($rows, 'uniqueVisitor', array('title'=> JText::_( 'COM_J4AGE_UNIQUE_VISITORS' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'visitors', array('title'=> JText::_( 'COM_J4AGE_VISITORS' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'averageVisits', array('title'=> JText::_( 'COM_J4AGE_VISITS_AVERAGE' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'referrerCount', array('title'=> JText::_( 'COM_J4AGE_REFERRERS' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'uniqueBots', array('title'=> JText::_( 'COM_J4AGE_UNIQUE_BOTS_SPIDERS' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'bots', array('title'=> JText::_( 'COM_J4AGE_BOT_SPIDER' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'uniqueNotIdentifiedVisitors', array('title'=> JText::_( 'COM_J4AGE_UNIQUE_NIV' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'notIdentifiedVisitors', array('title'=> JText::_( 'COM_J4AGE_NIV' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'uniqueSum', array('title'=> JText::_( 'COM_J4AGE_UNIQUE_SUM' ), 'bullet'=> 'square_outlined' ));
            $chartView->createGraph($rows, 'sum', array('title'=> JText::_( 'COM_J4AGE_SUM' ), 'bullet'=> 'square_outlined' ));
        }
        $this->assignRef("chartView", $chartView);

    }

    function retrieveData_new(&$rows, &$total, &$chartView, $timestamp_from, $timestamp_to, $cacheKey )
    {
        $query =
            "
            SELECT data
                FROM `#__jstats_cache`
                WHERE type = '$cacheKey' ( timestamp >= $timestamp_from AND timestamp <= $timestamp_to )

            ";

          //          $arr_obj_result = js_Cache::cachedQuery($cacheKey."-p_arr", $query, $timestamp_from, $timestamp_to);
         // js_Cache::storeInCache("labla-2009-12-03-1", "", $from, $to, $ttl,  $domain = 'j4age', $cacheValue, 'number')
    }


    function getDetailDegree($resolution, &$res_column, &$res_group, $timeColumnName ="v.changed_at", $timezone_offsetInSeconds = 3800)
    {
        if ($resolution == 'hour') {
			$res_column = " HOUR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS hour, DAYOFMONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS day, MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS month, YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS year";
			$res_group = " HOUR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), DAYOFMONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds))";
		}
        else if ($resolution == 'day') {
			$res_column = " DAYOFMONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS day, MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS month, YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS year";
			$res_group = " DAYOFMONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds))";
		}
        else if ($resolution == 'month') {
			$res_column = " MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS month, YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS year";
			$res_group = " MONTH(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)), YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds))";
		} else {
			//year
			$res_column = " YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds)) AS year";
			$res_group = " YEAR(FROM_UNIXTIME($timeColumnName + $timezone_offsetInSeconds))";
		}
    }

    /**
     * @param  $resolution
     * @param string $timestampkey
     * @param int $timezone_offsetInSeconds timestamps are stored in GMT, converted to DB timezone for methods like MONTH(), but we actually like display it in the front-end timezone
     * @return string
     */
    function getHarmonizedTimestampStr($resolution, $timestampkey = "v.changed_at", $timezone_offsetInSeconds = 0)
    {
        if ($resolution == 'hour') {
            $res_harmonizedTime = " $timestampkey ";
		}
        else if ($resolution == 'day') {
            $res_harmonizedTime = "UNIX_TIMESTAMP(CONCAT(YEAR( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds ) ), '-', MONTH( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds ) ), '-', DAYOFMONTH( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds )), ' 00:00:00' ))";;
        }
        else if ($resolution == 'month') {
            $res_harmonizedTime = "UNIX_TIMESTAMP(CONCAT(YEAR( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds ) ), '-', MONTH( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds ) ), '-', 1, ' 00:00:00' ))";;
		} else {
			//year
            $res_harmonizedTime = "UNIX_TIMESTAMP(CONCAT(YEAR( FROM_UNIXTIME( $timestampkey + $timezone_offsetInSeconds ) ), '-', 1, '-', 1, ' 00:00:00' ))";;
		}
        return $res_harmonizedTime;
    }

    function prepareResults(&$results, $timestampKey = "changed_at", $valueKey = "value", $typeKey = null, $resolutionType = 1 )
    {

        foreach($results as &$result)
        {
            $value = $result->$valueKey;
            $timestamp = $result->$timestampKey;

            //We calculate in local time as this are the values shown
            $hour = gmdate('H', $timestamp);
            $day = gmdate('d', $timestamp);
            $month = gmdate('m', $timestamp);
            $year = gmdate('Y', $timestamp);

            //Clean the timestamp
            if($resolutionType == 1) //hourly
            {
                $timestampNew = gmmktime($hour, 0, 0, $month, $day, $year);
            }
            if($resolutionType == 2) //daily
            {
                $timestampNew = gmmktime(0, 0, 0, $month, $day, $year);
            }
            if($resolutionType == 3) //weekly
            {
              //not supported
            }
            if($resolutionType == 4) //monthly
            {
                $timestampNew = gmmktime(0, 0, 0, $month, 1, $year);
            }
            if($resolutionType == 5) //yearly
            {
                $timestampNew = gmmktime(0, 0, 0, 1, 1, $year);
            }

            $resultNew = new stdClass();
            $resultNew->timestamp = $timestampNew;
            $resultNew->hour = $hour;
            $resultNew->day = $day;
            $resultNew->month = $month;
            $resultNew->year = $year;
            $resultNew->value = $value;

            if($typeKey != null)
            {
                $type = $result->$typeKey;

                $resultNew->type = $type;
            }

            $result = $resultNew;
        }
        return $results;
    }

    function storeReports($type, &$results, $frequency = 1, $valueKey = "value")
    {
        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();
        $type = $JSDatabaseAccess->db->Quote( $type );
        $insertQuery = "INSERT INTO #__jstats_reports (type, timestamp, value, frequency ) VALUES ";

        $next = false;
        foreach($results as $result)
        {
            $value = $result->$valueKey;
            $timestamp = $result->timestamp;

            if($value === null)
            {
                continue;
            }

            if($next)
            {
              $insertQuery .= ",";
            }

            $insertQuery .= "($type, $timestamp, $value, $frequency )";

            $next = true;
        }
        $insertQuery .= " ON DUPLICATE KEY UPDATE value = VALUES(value)";

        $JSDatabaseAccess->db->execute( $insertQuery );
        if ($JSDatabaseAccess->db->getErrorNum() > 0)
            return false;

        return true;
    }

    function loadData($from, $to, $resolution, $max_frequency = 1 )
    {
        $res_columnTime = "";
        $res_groupTime = "";
        $this->getDetailDegree($resolution, &$res_columnTime, &$res_groupTime, "timestamp", 0);

        $JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $harmonizedTimestampStr = $this->getHarmonizedTimestampStr($resolution,  "timestamp", 0 );

        $query = "SELECT type, sum(value) as value,  $res_columnTime, $harmonizedTimestampStr as timestamp
                  FROM #__jstats_reports
                  WHERE timestamp >= $from AND timestamp <= $to AND frequency <= $max_frequency
                  GROUP BY type, $res_groupTime
                  ORDER BY timestamp

        ";
         echo $query;
        $JSDatabaseAccess->db->setQuery( $query );
        $results = $JSDatabaseAccess->db->loadObjectList();

        if ($JSDatabaseAccess->db->getErrorNum() > 0)
        {
            $msg = $JSDatabaseAccess->db->getErrorMsg();
             js_echoJSDebugInfo("".$msg, '');
            return false;
        }
        js_echoJSDebugInfo('report data loaded');


        $resultsGrouped = $this->groupResults(&$results);
        js_echoJSDebugInfo('report prepared');

        return $resultsGrouped;
    }

    public function groupResults(&$results, $typePrefix = "")
    {
        $resultsGrouped = array();

        foreach($results as $result)
        {
            $timestamp = 0 + $result->timestamp;
            $key = $timestamp;
            $type = $result->type;
            $typeVariable = $typePrefix.$type;
            $value = 0 + $result->value;

            //$entry = $resultsGrouped["dummy"];

            if( isset( $resultsGrouped[ $key ] ) )
            {
                $entry = &$resultsGrouped[ $key ];
                $entry->$typeVariable = $value;
            }
            else
            {
                $entry = new stdClass();
                $entry->$typeVariable = $value;
                $entry->timestamp = $timestamp;
                $resultsGrouped[ $key ] = $entry;
            }

        }
        return $resultsGrouped;
    }

    function getFrequency($resolution)
    {
        if ($resolution == 'hour') {
            return 1;
		}
        else if ($resolution == 'day') {
            return 2;
        }
        else if ($resolution == 'week') {
            return 3;
        }
        else if ($resolution == 'month') {
             return 4;
		} else if ($resolution == 'year') {
			//year
             return 5;
		}
        return 0;
    }

    function postProcessData($resolution, &$data)
    {
        $processesData = array();

        $frequency = $this->getFrequency($resolution);

        $previousHour = 0;
        $previousDay = 1;
        $previousMonth = 1;
        $previousYear = date('Y');

        foreach($data as &$row)
        {
            $hour = date('H', $row->timestamp);
            $day = date('d', $row->timestamp);
            $month = date('m', $row->timestamp);
            $year = date('Y', $row->timestamp);

            //if($frequency == 1 && $previousDay < $day && $day + 1 !=)
            {

            }

            $row->hour = $hour;
            $row->month = $month;
            $row->day = $day;
            $row->year = $year;

            $row->v_all = $row->v_regular  + $row->v_bots + $row->v_unknown;
            $row->uniqueSum = $row->v_unique_0  + $row->v_unique_1 + $row->v_unique_2;

            if( ( $row->v_unique_1 != 0 ) && ( $row->v_regular != 0 ) ) {
                $format_token = '%01.2f';
                $row->averageVisits = sprintf($format_token, ( $row->v_regular / $row->v_unique_1 ));
            }else{
                $row->averageVisits = '.';
            }

            $processesData[] = $row;
        }
        return $processesData;
    }
}




class js_ReportStatus
{
   var $entries = array();

   var $firstTransactionTimestamp = -1;

    function &getInstance()
    {
        global $js_reportStatus;
        if(!$js_reportStatus)
        {
           $js_reportStatus = new js_ReportStatus();
        }
        return $js_reportStatus;
    }

    function createStatusEntry($name, $timestamp )
    {
       $entry = new js_ReportStatusEntry();
       $entry->name = $name;
       $entry->timestamp = $timestamp;
       $this->putStatusEntry($entry);
    }

    function updateStatusEntry($name, $timestamp )
    {
       $entry = &$this->entries[$name];
       $entry->timestamp = $timestamp;
    }

   function putStatusEntry(js_ReportStatusEntry $statusEntry  )
   {
      $this->entries[$statusEntry->name] = $statusEntry;
   }

   function getStatusEntries()
   {
       return $this->entries;
   }
}

class js_ReportStatusEntry
{
   var $name;

    var $timestamp;

    var $temporaryTimestamp;
}


