<?php
/**
 * @version		$Id: default.php 12772 2009-09-18 02:23:53Z eddieajau $
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// Load the JavaScript behaviors.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');?>


<div id="stepbar">
	<div class="t">
		<div class="t">
			<div class="t"></div>
		</div>
	</div>
	<div class="m">
		<div class="box"></div>
  	</div>
	<div class="b">
		<div class="b">
			<div class="b"></div>
		</div>
	</div>
</div>

<div id="right">
	<div id="rightpad">
		<div id="step">
			<div class="t">
				<div class="t">
					<div class="t"></div>
				</div>
			</div>
			<div class="m">
				<div class="far-right">
                    <div class="button1-right"><div class="prev"><a href="index.php?option=com_j4age&controller=installer&task=default&view=install&layout=default" title="<?php echo JText::_('COM_J4AGE_BACK'); ?>"><?php echo JText::_('COM_J4AGE_BACK'); ?></a></div></div>
                                        <div class="button1-left"><div class="refresh"><a href="index.php?option=com_j4age&controller=installer&task=default&view=install&layout=steps" title="<?php echo JText::_('COM_J4AGE_REFRESH'); ?>"><?php echo JText::_('COM_J4AGE_CHECK_AGAIN'); ?></a></div></div>
                                        <div class="button1-left"><div class="next"><a href="javascript:document.adminForm.controller.value='installer';document.adminForm.view.value='install';submitbutton('executeAll');" title="<?php echo JText::_('COM_J4AGE_ALLSTEP_ATONCE'); ?>"><?php echo JText::_('COM_J4AGE_PERFORM_ATONCE'); ?></a></div></div>
                                        <div class="button1-left"><div class="next"><a href="javascript:document.adminForm.controller.value='installer';document.adminForm.view.value='install';submitbutton('executeStep');" title="<?php echo JText::_('COM_J4AGE_NEXT'); ?>"><?php echo JText::_('COM_J4AGE_STEPBYSTEP'); ?></a></div></div>
				</div>
                                <span class="step"><?php echo JText::_('COM_J4AGE_POST_INSTALLATION'); ?></span>
			</div>
			<div class="b">
				<div class="b">
					<div class="b"></div>
				</div>
			</div>
		</div>
		<div id="installer">
			<div class="t">
				<div class="t">
					<div class="t"></div>
				</div>
			</div>
			<div class="m">
                                <h2><?php echo JText::_( 'COM_J4AGE_DB_MIGRATION_UPDATE' ); ?>
                    <?php
                if( $this->currentVersion == $this->nextVersion)
                {
                    echo  JText::_( 'COM_J4AGE_FOR' ).' <strong>'.$this->nextVersion.'</strong> '.JText::_( 'COM_J4AGE_STEP' ).' '.$this->nextStep.' '.JText::_( 'COM_J4AGE_OF' ).' '.$this->nextVersionStepAmount.' - '.($this->totalAmountOfStepsLeft).' '.JText::_( 'COM_J4AGE_STEPS_REMAINING' ).' <strong>'.$this->versionToBeUpgraded.'</strong>';
                }
                else
                {
                    echo JText::_( 'COM_J4AGE_FROM_VERSION' ).' <strong>'.$this->currentVersion.'</strong> '.JText::_( 'COM_J4AGE_TO' ).' <strong>'.$this->nextVersion.'</strong> '.JText::_( 'COM_J4AGE_STEP' ).' '.$this->nextStep.' '.JText::_( 'COM_J4AGE_OF' ).' '.$this->nextVersionStepAmount.' - '.($this->totalAmountOfStepsLeft).' '.JText::_( 'COM_J4AGE_STEPS_REMAINING' ).' <strong>'.$this->versionToBeUpgraded.'</strong>';
                }

                ?>

                </h2>
				<div class="install-text">
                    <div>
                    <?php echo JText::_('COM_J4AGE_STEP_DB_MIGRATION_NOTICE_A'); ?>
                    </div>
                    <div>
                    <br/><?php echo JText::_('COM_J4AGE_STEP_DB_MIGRATION_NOTICE_B'); ?>
                    </div>

                    <div style="color:orange">
                    <br/><?php echo JText::_('COM_J4AGE_STEP_DB_MIGRATION_NOTICE_C'); ?>
                    </div>
                    <div style="color:red">
                    <br/><?php echo JText::_('COM_J4AGE_STEP_DB_MIGRATION_NOTICE_D'); ?>
                    </div>
				</div>
				<div class="install-body">
					<div class="t">
						<div class="t">
							<div class="t"></div>
						</div>
					</div>
					<div class="m">
						<fieldset>
                           <?php
                                 $versionIndex = -1;
                             foreach($this->steps_per_Versions as $version => $stepsPerVersion)
                             {
                                $versionStr =  str_replace(" ", "_", $version);
                                 $versionIndex += $versionIndex;

                                //$isNextStep = ( $version == $this->nextVersion);
                                ?>
                                   <p >
                                   <h1 <?php $versionIndex == 0 ? 'id="version"' : '' ?> ><?php echo ($this->buildVersion == $version? JText::_( 'COM_J4AGE_STATIC_DATA' ) : JText::_( 'COM_J4AGE_STEP_DB_MIGRATION_UPDATE_TO' )." $version") ?></h1>
                                    <br/>
                                   <?php

                                    foreach($stepsPerVersion as $index=>$step)
                                    {

                                      $isNextStep = ( $version == $this->nextVersion) && ($index == $this->nextStep -1 );
                                      if( $isNextStep ) echo '<font color="#0000ff">';
                                      echo "<br/><strong id=\"step".$versionStr."-".$index."\">".JText::_('COM_J4AGE_STEP_DB_MIGRATION_STEP').' '.($index + 1).' '.JText::_( 'COM_J4AGE_OF' ).' '.(count($stepsPerVersion))."</strong><br/>".$step->description." (".$step->number.")<br/>";
                                      echo "<strong>".JText::_('COM_J4AGE_STEP_DB_MIGRATION_SQL')."</strong><br/>";
                                      $querylimit = $isNextStep ? 100 : 10;
                                      foreach($step->query as $queryIndex=>$query)
                                      {
                                          if($querylimit-- < 0){
                                              echo '(n). ...<br/>';
                                              break;
                                          }
                                          $queryStr = $query;
                                          $queryLength = strlen($queryStr);
                                          if($queryLength > 600)
                                          {
                                             $queryStr = substr($queryStr, 0, 200).'...';
                                          }
                                          echo $queryIndex.'. '.$queryStr . '<br/>';
                                      }
                                       if( $isNextStep ) echo "</font>";
                                    };?>
                                   </p>
                                <?php
                             }
                           ?>
						</fieldset>
					</div>
					<div class="b">
						<div class="b">
							<div class="b"></div>
						</div>
					</div>

					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
			<div class="b">
				<div class="b">
					<div class="b"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>

<?php
$keepRedirecting = JRequest::getVar('rGo', 0);
if($keepRedirecting){
?>

<input type="hidden" name="layout" value="default"/>
<script language="JavaScript" type="text/javascript">
<!--
    document.adminForm.layout.value='steps';
    setTimeout("submitbutton('executeAll')",100);
// -->
</script>
<?php }?>

