<?php

              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          

if( !defined( '_JEXEC' ) ) {
	die( 'JS: No Direct Access to '.__FILE__ );
}

$JSTemplate = new js_JSTemplate();
$dbaccess =& js_JSDatabaseAccess::getInstance();

?>

<div style="text-align: left;">
	<!--div style="width:95%; border: 1px solid #EFEFEF; margin:5px auto 5px auto; padding:5px;"-->
	    <div style="text-align:center">
              <h3><?php echo JText::_( 'COM_J4AGE_CREDITS' ); ?></h3>
              <p><?php echo JText::_( 'COM_J4AGE_CREDITS_INFO' ); ?></p>
              <h3><?php echo JText::_( 'COM_J4AGE_SPECIAL_CREDITS' ); ?></h3>
              <p><?php echo JText::_( 'COM_J4AGE_SPECIAL_CREDITS_INFO' ); ?></p>
	    </div>
		<table style="width: 100%; padding: 0px; border-width: 0px; border-collapse: collapse; /*not working in IE 6.0, 7.0 use cellspacing=0 */ border-spacing: 0px; /* no difference */" cellspacing="0">
		<tr>
			<td style="padding: 0px; text-align: left;"></td>
			<td style="padding: 0px; text-align: right; vertical-align: top; font-weight: bold;">
                                <?php echo JText::_( 'COM_J4AGE_VERSION' ); ?>:&nbsp;
				<!-- JoomlaStats build version: '<?php echo $this->JSConf->BuildVersion; ?>' -->
				<?php
					if (strpos($this->JSConf->JSVersion, ' ') === false) {
						//for release, cut the build number (last digits) from version number
						$pos = strrpos($this->JSConf->JSVersion, '.');
						if ($pos === false) {
							//somethings goes wrong, echo all
							echo $this->JSConf->JSVersion;
						} else {
							echo substr($this->JSConf->JSVersion, 0, $pos);
						}
					} else {
						echo $this->JSConf->JSVersion;
					}
				?>
			</td>
		</tr>
		</table>
		<?php $JSTemplate->startBlock();?>
			<br/>
			<table width="550" align="center" style="border: 1px solid #CCCCCC; background-color: #F5F5F5;">
			<tr>
                                <td colspan="4" style="font-weight:bold; text-align:center;"><?php echo JText::_( 'COM_J4AGE_DATABASE_SUMMARY' );?><hr /></td>
            </tr>
			<tr>
                                <td width="220"><?php echo JText::_( 'COM_J4AGE_BOTS_SPIDERS' );?></td>
				<td width="150" align="left"><?php echo $this->StatusTData->totalbots;?></td>
                                <td width="220" align="left"><?php echo JText::_( 'COM_J4AGE_VISITED_PAGES' );?></td>
				<td width="150" align="left"><?php echo $this->StatusTData->totalpages;?></td>
			</tr>
			<tr>
                                <td><?php echo JText::_( 'COM_J4AGE_SEARCHES' );?></td>
				<td align="left"><?php echo $this->StatusTData->totalse;?></td>
                                <td align="left"><?php echo JText::_( 'COM_J4AGE_REFERRER' );?></td>
				<td align="left"><?php echo $this->StatusTData->totalpagereferrer;?></td>
			</tr>
			<tr>
                                <td><?php echo JText::_( 'COM_J4AGE_VISITOR_OS' );?></td>
				<td align="left"><?php echo $this->StatusTData->totalsys;?></td>
                                <td align="left"><?php echo JText::_( 'COM_J4AGE_VISITOR' );?></td>
				<td align="left"><?php echo $this->StatusTData->totalvisits;?></td>
			</tr>
			<tr>
                                <td><?php echo JText::_( 'COM_J4AGE_COUNTRIES' );?></td>
				<td align="left"><?php echo $this->StatusTData->totaltld;?></td>
                <td align="left"><?php echo JText::_( 'COM_J4AGE_TOTAL_SIZE' );?></td>
                <td align="left"><?php echo round( ( ( $this->StatusTData->dbsize / 1024 ) / 1024 ), 2 );?>MB</td>
			</tr>
            <tr>
                <td colspan="4" style="font-weight:bold; text-align:center;"><?php echo JText::_( 'COM_J4AGE_OTHER_DETAILS' );?><hr /></td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_J4AGE_GMT_DATE_J4AGE' );?></td>
                <td align="left"><?php echo js_gmdate("j.n.Y H:i:s");?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_J4AGE_GMT_DATE' );?></td>
                <td align="left"><?php echo gmdate("j.n.Y H:i:s");?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_SYSTEM_DATE' );?></td>
                    <td align="left"><?php echo date("j.n.Y H:i:s");?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'Database Time' );?></td>
                    <td align="left"><?php echo $this->StatusTData->dbtime;?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
            <tr>
                <td><?php echo JText::_( 'DB Timestamp' );?></td>
                <td align="left"><?php echo $this->StatusTData->dbtimestamp;?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
                <tr>
                       <td><?php echo JText::_( 'Joomla Timestamp' );?></td>
                       <td align="left"><?php echo $this->StatusTData->timestamp;?></td>
                       <td colspan="2">&nbsp;</td>
                   </tr>
                <tr>
                       <td><?php echo JText::_( 'JS Timezone' );?></td>
                       <td align="left"><?php echo js_getJSTimeZone()?></td>
                       <td colspan="2">&nbsp;</td>
                   </tr>

                <tr>
                    <td><?php echo JText::_( 'JOOMLA' );?></td>
                             <td align="left"><?php echo js_getDate("now")->toFormat();?></td>
                             <td colspan="2">&nbsp;</td>
                         </tr>
                <tr>
                    <td><?php echo JText::_( 'JOOMLA Timezone Offset' );?></td>
                             <td align="left"><?php echo JFactory::getApplication()->getCfg('offset');?></td>
                             <td colspan="2">&nbsp;</td>
                         </tr>
                 <tr>
                <td colspan="4" style="font-weight:bold; text-align:center;"><?php echo JText::_( 'COM_J4AGE_DBTABLE_COLLECTION_INFO' );?><hr /></td>
            </tr>
            <?php
             $columnsOfTable = $dbaccess->js_getTableColumns("#__jstats_keywords" );
             ;?>
            <?php $columnCollectionUTF = $dbaccess->js_getColumnCollation( $columnsOfTable, 'keywords' ) ; $isUTFType = ( strpos(strtolower($columnCollectionUTF), 'utf8') !== false);?>
            <tr>
                <td><?php echo JText::_( 'COM_J4AGE_JSTATS_KEYWORDS' );?></td>
                <td align="left"><?php echo $columnCollectionUTF;?></td>
                <td colspan="2"><?php echo (( $isUTFType != true ) ? ( JText::_( 'COM_J4AGE_UTF8_RECOMMENDED' ) ) : '' );?></td>
            </tr>

            <tr>
                <td colspan="4" style="font-weight:bold; text-align:center;"><?php echo JText::_( 'COM_J4AGE_SYSTEM_INFO' );?><hr /></td>
            </tr>
            <tr>
                <td><?php echo JText::_( 'COM_J4AGE_PHP_VERSION' );?></td>
                <td align="left"><?php echo phpversion();?></td>
                <td colspan="2"><?php echo ( js_JSUtil::JSVersionCompare( phpversion(), '5.0.0', '<') ? ( JText::_( 'COM_J4AGE_NOT_OFFICIALLY_SUPPORTED_INFO' ) ) : '' );?></td>
            </tr>
            <?php

            $dbversion = $dbaccess->db->getVersion();
            ;?>
                
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_PHP_VERSION' );?></td>
                    <td align="left"><?php echo phpversion();?></td>
                    <td colspan="2"><?php echo ( js_JSUtil::JSVersionCompare( phpversion(), '5.0.0', '<') ? ( JText::_( 'COM_J4AGE_NOT_OFFICIALLY_SUPPORTED_INFO' ) ) : '' );?></td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_DB_VERSION' );?></td>
                    <td align="left"><?php echo $dbversion;?></td>
                    <td colspan="2"><?php echo ( js_JSUtil::JSVersionCompare( $dbversion, '4.0.0', '<') ? ( JText::_( 'COM_J4AGE_NOT_OFFICIALLY_SUPPORTED_INFO' ) ) : '' );?></td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_PHP_MAX_EXECTIME' );?></td>
                    <td align="left"><?php echo ini_get('max_execution_time');?>s</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_PHP_MAX_SQL_CONNECTIME' );?></td>
                    <td align="left"><?php echo ini_get('mysql.connect_timeout');?>s</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_PHP_MAX_IMPUTIME' );?></td>
                    <td align="left"><?php echo ini_get('max_input_time');?>s</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_PHP_MEMORY_LIMIT' );?></td>
                    <td align="left"><?php echo ini_get('memory_limit');?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_MYSQLI_INTECTIVE_TIME' );?></td>
                    <td align="left"><?php echo (defined('MYSQLI_CLIENT_INTERACTIVE')?MYSQLI_CLIENT_INTERACTIVE:'-');?>s</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo JText::_( 'COM_J4AGE_MYSQLI_CONNECTIONS' );?></td>
                    <td align="left"><?php echo (defined('MYSQLI_OPT_CONNECT_TIMEOUT')?MYSQLI_OPT_CONNECT_TIMEOUT:'-');?>s</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <?php if(count($this->StatusTData->errorMsg) > 0)
            {
                ?>
                <tr>
                <td colspan="4" style="font-weight:bold; text-align:center;"><?php echo JText::_( 'Errors' );?><hr /></td>
                </tr>
                <?php
                  foreach($this->StatusTData->errorMsg as $msg)
                    {
                        ?>
                      <tr>
                        <td><?php echo $msg["name"];?></td>
                        <td align="left" colspan="2"><?php echo $msg["description"];?></td>
                        <td >&nbsp;</td>
                      </tr>
                   <?php
                    }
                ?>
                <?php
            }
            ?>
			</table>
			<br />
		<?php $JSTemplate->endBlock();?>
	<!--/div-->

	<div style="clear:both; margin-top:15px"></div>

</div>


