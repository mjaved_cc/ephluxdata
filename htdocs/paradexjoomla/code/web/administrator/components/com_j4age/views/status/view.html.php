<?php

              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          

// Check to ensure this file is only called within the Joomla! application and never directly
if( !defined( '_JEXEC' ) ) {
	die( 'JS: No Direct Access to '.__FILE__ );
}

jimport( 'joomla.application.component.view' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'database' .DS.'select.one.value.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'api' .DS. 'general.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries' .DS. 'template.html.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries' .DS. 'api.base.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries' .DS. 'base.classes.php' );
require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries' .DS. 'count.classes.php' );

/**
 * Hello View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class j4ageViewStatus extends JView
{                    
	/**
	 * display method of Hello view
	 * @return void
	 **/
	function display($tpl = null)
	{
	/*
		$model	  =& $this->getModel();
		$text = '';
		JToolBarHelper::title(   JText::_( 'Technical Expertise' ).': <small><small>[ ' . $text.' ]</small></small>', 'dvw' );
		JToolBarHelper::save('saveEntry');
		if ($isNew)  {
			JToolBarHelper::cancel('cancelEntry');
		} else {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancelEntry', 'Close' );
		}
        */
		//show the sub content

        $JSConf = js_JSConf::getInstance();
		$engine =  JoomlaStats_Engine::getInstance();
		$this->assignRef("engine", $engine);

        $isInstallPage = false;

        $Status = new js_JSStatus();

		$StatusTData = new js_JSStatusTData();

		$StatusTData->initalizeMembersFromDatabase();
		$StatusTData->showDbInfoTable = true;

		$Status->viewJSStatusPageBase( $isInstallPage, $StatusTData );
        $this->assignRef("StatusTData", $StatusTData);
        $this->assignRef("Status", $Status);
        $this->assignRef("JSConf", $JSConf);

		parent::display();
	}	
}

/**
 * Object of this class fill variable data in status template
 *
 * @todo Database querys should be moved to database layer
 */
class js_JSStatusTData
{
	/**
	 * if array is empty, error section will not be displayed
	 * elements should be arrays like: array( 'name' => $name, 'description' => $desc);
	 */
	var $errorMsg = array();

	/**
	 * if array is empty, string 'It seams that Your JoomlaStats works correctly.
	 * No Warnings at this moment.' will be printed
	 * elements should be arrays like: array( 'name' => $name, 'description' => $desc);
	 */
	var $warningMsg = array();

	/** if array is empty, string 'It seams that Your JoomlaStats works correctly.
	 * No Recommendations at this moment.' will be printed
	 * elements should be arrays like: array( 'name' => $name, 'description' => $desc);
	 */
	var $recommendationMsg = array();

	/**
	 * if array is empty, no string will be printed
	 * elements should be arrays like: array( 'name' => $name, 'description' => $desc);
	 * */
	var $infoMsg = array();

	/**
	 * if set to true 'Database Info Table' will be shown at info section
	 * */
	var $showDbInfoTable = false;

	/** data from database */
	var $LastSummarizationDate = false;
	var $totalbots		= '';
	var $totalbrowser	= '';
	var $totalse		= '';
	var $totalsys		= '';
	var $totaltld		= '';
	var $totalipc		= '';
    var $dbsize         = 0;

	function initalizeMembersFromDatabase() {

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$JSDbSOV = new js_JSDbSOV();
		$JSDbSOV->getJSLastSummarizationDate($this->LastSummarizationDate);

		$query = 'SELECT count(*) AS totalbots'
		. ' FROM #__jstats_browsers'
		. ' WHERE browser_type = 2'
		;
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalbots = $JSDatabaseAccess->db->loadResult();

		$query = 'SELECT count(*) AS totalbrowser'
		. ' FROM #__jstats_browsers'
		. ' WHERE browser_type = 1'
		;
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalbrowser = $JSDatabaseAccess->db->loadResult();

		$query = 'SELECT count(*) AS totalse'
		. ' FROM #__jstats_searchers';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalse = $JSDatabaseAccess->db->loadResult();

		$sql = 'SELECT count(*) AS totalsys'
		. ' FROM #__jstats_systems';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalsys = $JSDatabaseAccess->db->loadResult();

		$query = 'SELECT count(*) AS totaltld'
		. ' FROM #__ip2nationCountries';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totaltld = $JSDatabaseAccess->db->loadResult();

		// user called pages
		$query = 'SELECT count(*) totalpages'
		. ' FROM #__jstats_pages';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalpages = $JSDatabaseAccess->db->loadResult();

		// user page requests
		$query = 'SELECT count(*) totalpagerequest'
		. ' FROM #__jstats_impressions';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalpagerequest = $JSDatabaseAccess->db->loadResult();

		// user referer
		$query = 'SELECT count(*) totalreferrer'
		. ' FROM #__jstats_referrer';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalpagereferrer = $JSDatabaseAccess->db->loadResult();

		// user visits
		$query = 'SELECT count(*) totalvisits'
		. ' FROM #__jstats_visits';
		$JSDatabaseAccess->db->setQuery( $query );
		$this->totalvisits = $JSDatabaseAccess->db->loadResult();

		// db time
		$query = 'SELECT now() as now, UNIX_TIMESTAMP() AS timestamp';
		$JSDatabaseAccess->db->setQuery( $query );
        $temp = $JSDatabaseAccess->db->loadObject();
        $this->dbtime = $temp->now;
        $this->dbtimestamp = $temp->timestamp;
        $this->timestamp = time();

        //SELECT @ @global.time_zone , @ @session.time_zone

		$JSDbSOV->getJSDatabaseSize($this->dbsize);
	}
}

/**
 *  This class examin JS condition and generate status/info page
 *
 *  NOTICE: This class should contain only set of static, argument less functions that are called by task/action
 */
class js_JSStatus
{
	/**
	 * A hack to support __construct() on PHP 4
	 *
	 * Hint: descendant classes have no PHP4 class_name() constructors,
	 * so this constructor gets called first and calls the top-layer __construct()
	 * which (if present) should call parent::__construct()
	 *
	 * code from Joomla CMS 1.5.10 (thanks!)
	 *
	 * @access	public
	 * @return	Object
	 * @since	1.5
	 */
	function js_JSStatus()
	{
	}

	/**
	 * checks the activation method
	 *
	 * @param string $activeMethodType
	 * @return string
	 */
	function activeMethodTypeToText( $activeMethodType ) {
		switch( $activeMethodType) {
			case 'mod':
                                return JText::_( 'COM_J4AGE_MODULE_ACTIVATION_J4AGE' );
				break;

			case 'plg':
                                return JText::_( 'COM_J4AGE_PLUGIN_ACTIVATION_J4AGE' );
				break;

			case 'tpl':
                                return JText::_( 'COM_J4AGE_SITE_TEMPLATE_MODIFICATION' );
				break;

			case 'sa_tpl':
                                return JText::_( 'COM_J4AGE_PAGE_MODIFICATION_OUTSIDE_PAGES' );
				break;
			default:
                                return JText::_( 'COM_J4AGE_UNKNOWN_METHOD' );
				break;
		}
	}

	/**
	 * Checks if anonymous user (not logged user on front page) have set the same time zone as currently logged user
	 *
	 * Function can be called only on backend (it is propper performing)
	 *
	 * @return double - time offset in hours
	 */
	function getTimeZoneOffsetBetweenAnonymousUserAndCurrentlyLoggedUser() {

		$AUTZ = 0;
		{//get anonymous user time zone
			$AUTZ = js_getJSTimeZone();
                        js_echoJSDebugInfo( JText::_( 'COM_J4AGE_STATUS_ANONYMUS_TIME_ZONE' ), $AUTZ);
		}

		$CUTZ = 0;
		{//get current user time zone
			$user =& JFactory::getUser();
			$CUTZ = $user->getParam('timezone');
                        js_echoJSDebugInfo( JText::_( 'COM_J4AGE_STATUS_CURENTLY_TIME_ZONE' ), $CUTZ);
			if (is_null($CUTZ))
			{
                                js_echoJSDebugInfo( JText::_( 'COM_J4AGE_STATUS_ERROR_TIME_ZONE' ) );
				return 0; //could not get time zone
			}
		}

		//$config =& JFactory::getConfig();
		//$tzoffset = $config->getValue('config.offset');
		//$date =& JFactory::getDate($row->created, $tzoffset);

		return ($AUTZ - $CUTZ);
	}

	function checkAnonymousUserTimeZoneTheSameAsCurrentlyLoggedUserTimeZone( &$recommendationMsg ) {
		$TZOffset = $this->getTimeZoneOffsetBetweenAnonymousUserAndCurrentlyLoggedUser();
		if ($TZOffset != 0) {
			$txt = ''
                        . JText::sprintf( 'COM_J4AGE_TIME_ZONE_ANONYMOUS_USERS', $TZOffset )
			;

                        $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_TIME_ZONES' ), 'description' => $txt );
		}
	}

	function showAnonymousUserCurrentTime( $isInstallPage, &$infoMsg ) {
		if ($isInstallPage)
			return;

        $now_date_time_str = js_gmdate('Y-m-d H:i:s');

		$txt = ''
                . JText::sprintf( 'COM_J4AGE_ANONYMOUS_USERS_CURRENT_DATETIME', $now_date_time_str )
		;

                $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_CURRENT_TIME_J4AGE' ), 'description' => $txt );
	}

	/**
	 * Checks if module is installed and activated
	 *
	 * @todo check if module is activated on all pages and send message about it as recommendation
	 *
	 * @param unknown_type $isModInstalled
	 * @param unknown_type $isModActivated
	 * @param unknown_type $isModActivatedTwice
	 * @return bool
	 */
	function isJSActivationModuleInstalledAndActivated( &$isModInstalled, &$isModActivated, &$isModActivatedTwice ) {
		$isModInstalled			= false;
		$isModActivated			= false;
		$isModActivatedTwice	= false;

		$path = JPATH_SITE .DS. 'modules' .DS. 'mod_jstats_activate' .DS. 'mod_j4age_activate.php';

		if ( file_exists( $path ) ) {
			$isModInstalled = true;
		}

		if( !$isModInstalled ) {
			//function success, but module not installed - there is no need to check if it is activated
			return true;
		}

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$query = 'SELECT published'
		. ' FROM #__modules'
		. ' WHERE module = \'mod_jstats_activate\''
		;
		$JSDatabaseAccess->db->setQuery( $query );
		$rowList = $JSDatabaseAccess->db->loadAssocList();
		//user can have more than 1 activation module (one module and many copies)
		// >> mic: AND WHAT FOR????? how should this makes sense????
		// >> at: of course it not have sense, that is why we must check it!

		if( count( $rowList ) > 0 ) {
			foreach( $rowList as $row ) {
				if( ( $isModActivated == true ) && ( $row['published'] ) ) {
					$isModActivatedTwice = true;
				}
				if( $row['published'] ) {
					$isModActivated = true;
				}
			}
		}

		return true;
	}

	/**
	 * checks for installed plugin and if activated
	 *
	 * @param bool $isPlgInstalled
	 * @param bool $isPlgActivated
	 * @param bool $isPlgActivatedTwice
	 * @return bool
	 */
	function isJSActivationPluginInstalledAndActivated( &$isPlgInstalled, &$isPlgActivated, &$isPlgActivatedTwice ) {
		$isPlgInstalled			= false;
		$isPlgActivated			= false;
		$isPlgActivatedTwice	= false;

        $path = "";
        if( version_compare( JVERSION, '1.6.0', 'ge' ) )
        {
            $path = JPATH_SITE .DS. 'plugins' .DS. 'j4age' .DS. 'j4age.php';
        }
        else
        {
            $path = JPATH_SITE .DS. 'plugins' .DS. 'system' .DS. 'j4age.php';
        }

		if ( file_exists( $path ) ) {
			$isPlgInstalled = true;
		}

		if( !$isPlgInstalled ) {
			// function success, but plugin not installed - there is no need to check if it is activated
			return true;
		}

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$query = 'SELECT published'
		. ' FROM #__plugins'
		. ' WHERE element = \'bot_jstats_activate\''
		;
		$JSDatabaseAccess->db->setQuery( $query );
		$rowList = $JSDatabaseAccess->db->loadAssocList(); // plugin could be only one but for code clarity...

		if( count( $rowList ) > 0 ) {
			foreach( $rowList as $row ) {
				if( ( $isPlgActivated == true ) && ( $row['published'] ) ) {
					$isPlgActivatedTwice = true;
				}
				if( $row['published'] ) {
					$isPlgActivated = true;
				}
			}
		}

		return true;
	}

	/**
	 * return false when could not read template
	 *
	 * @param string $StrToFind
	 * @param integer $numberOfOcurances
	 * @return bool false
	 */
	function findStringInJoomlaActiveTemplate( $StrToFind, &$numberOfOcurances ) {
		$cur_template_file_name = '';

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$clientId = 0; // site template (not admin template)
		// Get the current default template
		$query = ' SELECT template'
		. ' FROM #__templates_menu'
		. ' WHERE client_id = ' . (int) $clientId
		. ' AND menuid = 0 ' //@At @bug? I do not know what it meas so I remove it - mic: NO, this means the FIRST template
		;
		$JSDatabaseAccess->db->setQuery( $query );
		$cur_template_name = $JSDatabaseAccess->db->loadResult();

		if( $cur_template_name == '' ) {
			//@todo trigger error. How it could be that there is no default template
			return false;
		}

		$cur_template_file_name = JPATH_SITE .DS. 'templates' .DS. $cur_template_name .DS. 'index.php';

		if( !is_readable( $cur_template_file_name ) ) {
			//@todo trigger error. Could not read template
		    return false;
		}

		$template_serialized = file_get_contents( $cur_template_file_name );

		if( $template_serialized === false ) {
			//@todo trigger error. Could not read template 2
		    return false;
		}

		$numberOfOcurances	= 0;
		$start_pos			= 0;

		// for security max ten times
		for( $i = 0; $i < 10; $i++ ) {
			$pos = strpos( $template_serialized, $StrToFind, $start_pos );
			if( $pos !== false ) {
				//found
				$numberOfOcurances++;
				$start_pos = $pos+1;
			}
		}

		return true;
	}

	/**
	 * checks for activation inside a template
	 *
	 * @param bool $isActivatedInTemplate
	 * @param bool $isActivatedInTemplateTwice
	 * @return bool true/false
	 */
	function isJSActivationDoneInTemplate( &$isActivatedInTemplate, &$isActivatedInTemplateTwice ) {
		$StrToFind			= 'j4age.inc.php';
		$numberOfOcurances	= 0;
		$Res				= $this->findStringInJoomlaActiveTemplate( $StrToFind, $numberOfOcurances );

		if( $Res == false ) {
			return false;
		}

		if( $numberOfOcurances > 0 ) {
			$isActivatedInTemplate = true;
		}

		//strange sytuation but do not rise warrning
		//if ($numberOfOcurances == 1)

		//One activation should have 2 file names, first in 'if', second in 'include'
		if( $numberOfOcurances > 2 ) {
			$isActivatedInTemplateTwice = true;
		}

		return true;
	}

	/**
	 * checks for a stand-a-lone activation
	 *
	 * @param bool $isStandAloneActivatedInTemplate
	 * @param bool $isStandAloneActivatedInTemplateTwice
	 * @return bool true/false
	 */
	function isJSStandAloneActivationDoneInTemplate( &$isStandAloneActivatedInTemplate, &$isStandAloneActivatedInTemplateTwice){
		$StrToFind			= 'sa.j4age.inc.php';
		$numberOfOcurances	= 0;
		$Res				= $this->findStringInJoomlaActiveTemplate( $StrToFind, $numberOfOcurances );

		if( $Res == false ) {
			return false;
		}

		if( $numberOfOcurances > 0 ) {
			$isActivatedInTemplate = true;
		}

		//strange sytuation but do not rise warrning
		//if ($numberOfOcurances == 1)

		//One activation should have 2 file names, first in 'if', second in 'include'
		if( $numberOfOcurances > 2 ) {
			$isActivatedInTemplateTwice = true;
		}

		return true;
	}

	/**
	 * This function analize front page - this make that JS count itself -
	 * because of that body of that function should be performed only once by one page refresh
	 *
	 * @param bool $isJSActivCheckFrontPage
	 * @return bool true/false
	 */
	function isJSActivCheckFrontPage( &$isJSActivCheckFrontPage, &$iMsg ) {

		$url_to_front_site = JURI::root(false);

		$allow_url_fopen = (bool)ini_get('allow_url_fopen');
		if ( $allow_url_fopen == false ) {
			//We are unable to parse front page to test JS itself
			$iMsg = ''
                        . JText::_( 'COM_J4AGE_UNABLE_PERFORM_PHP_ALLOW_URL_FOPEN_OFF' )
			. '<br/>'
                        . JText::_( 'COM_J4AGE_DONOT_MEAN_J4AGENOT_TEST_ITSELF' )
			;
			return false;
		}

		$front_site_serialized = file_get_contents( $url_to_front_site );

		if( $front_site_serialized === false ) {
			$iMsg = ''
                        . JText::_( 'COM_J4AGE_UNABLE_VERIFY_FRONT_SITE' )
			. '<br/>'
                        . JText::_( 'COM_J4AGE_DONOT_MEAN_J4AGENOT_TEST_ITSELF' )
			;
		    return false;
		}

		$JSSystemConst = new js_JSSystemConst();
		//there are many methods to activate JS. If anyone will be performed Write below string to page
                $strToCheck = JText::_( 'COM_J4AGE_ACTIVATED_YET' );
		//$strToCheck = preg_replace('/[^A-Za-z]/', '', $strToCheck);

		//$strToCheck = str_ireplace(array('<!--', '-->'), '', $strToCheck);
		$strToCheck = trim( $strToCheck );

		//check if something like JoomlaStatsActivated is in HTML code on front site
		$pos		= strpos( $front_site_serialized, $strToCheck );

		if( $pos === false ) {
			// not found
			$isJSActivCheckFrontPage = false;
		}else{
			// found
			$isJSActivCheckFrontPage = true;
		}

		return true;
	}

	/**
	 * generates some messages
	 *
	 * @param array $JSActivationStatus
	 */
	function getJSActivationStatus( &$JSActivationStatus ) {
		$JSActivationStatus = array(
			'mod'	=> array(
				'type'				=> 'mod',
				'is_installed'		=> false,
				'is_acivated'		=> false,
				'is_acivated_twice'	=> false
			),
			'plg'	=> array(
				'type'				=> 'plg',
				'is_installed'		=> false,
				'is_acivated'		=> false,
				'is_acivated_twice'	=> false
			),
			'tpl'	=> array(
				'type'				=> 'tpl',
				'is_installed'		=> false,
				'is_acivated'		=> false,
				'is_acivated_twice'	=> false
			),
			'sa_tpl'	=> array(
				'type'				=> 'sa_tpl',
				'is_installed'		=> false,
				'is_acivated'		=> false,
				'is_acivated_twice'	=> false
			)
		);

		$this->isJSActivationModuleInstalledAndActivated(
			$JSActivationStatus['mod']['is_installed'],
			$JSActivationStatus['mod']['is_acivated'],
			$JSActivationStatus['mod']['is_acivated_twice']
		);

		$this->isJSActivationPluginInstalledAndActivated(
			$JSActivationStatus['plg']['is_installed'],
			$JSActivationStatus['plg']['is_acivated'],
			$JSActivationStatus['plg']['is_acivated_twice']
		);

		$isStandAloneActivatedInTemplate = false;

		$this->isJSStandAloneActivationDoneInTemplate(
			$isStandAloneActivatedInTemplate,
			$JSActivationStatus['sa_tpl']['is_acivated_twice']
		);

		$JSActivationStatus['sa_tpl']['is_installed'] = $isStandAloneActivatedInTemplate;
		$JSActivationStatus['sa_tpl']['is_acivated']  = $isStandAloneActivatedInTemplate;

		$isActivatedInTemplate = false;
		$this->isJSActivationDoneInTemplate(
			$isActivatedInTemplate,
			$JSActivationStatus['tpl']['is_acivated_twice']
		);

		$JSActivationStatus['tpl']['is_installed'] = $isActivatedInTemplate;
		$JSActivationStatus['tpl']['is_acivated']  = $isActivatedInTemplate;

		return true;
	}

	/**
	 * $isJSActivated return true only if EVERY thing seems to be OK (e.g. JS is activated only once)
	 *
	 * @param in  array $JSActivationStatus
	 * @param out bool  $isJSActivated
	 * @return bool
	 */
	function isJSActivated( $JSActivationStatus, &$isJSActivated ) {
		$isJSActivated		= false;

		foreach( $JSActivationStatus as $method ) {
			if ( $method['is_acivated_twice'] == true ) {
				$isJSActivated = false;
				break;
			}
			if( ( $isJSActivated == true ) && ( $method['is_acivated'] == true ) ) {
				//JS are activated by 2 methods at the same time!
				$isJSActivated = false;
				break;
			}
			if( $method['is_acivated'] == true ) {
				$isJSActivated = true;
			}
		}

		return true;
	}

	function checkDatabaseAndPhpFilesVersions( &$warningMsg ) {
		$JSVersion_result = '';
		$JSApiGlobal = new js_JSApiGeneral();
		$JSVerResult = $JSApiGlobal->getJSVersion($JSVersion_result);
		if ($JSVerResult == false)
                        $warningMsg[] = array( 'name' => JText::_( 'COM_J4AGE_PHP_DATABASE_VERSION' ), 'description' => JText::_( 'COM_J4AGE_PHP_DATABASE_VERSION_DIFFER' ) );
	}

	/**
	 * checks for activation and generates message
	 *
	 * @param array $warningMsg
	 * @return bool
	 */
	function checkJSActivation( &$warningMsg, &$recommendationMsg, &$infoMsg ) {

		$bRes = true;

		$JSActivationStatus = array();
		$bRes &= $this->getJSActivationStatus( $JSActivationStatus );

		$isJSActivated = false;
		$bRes &= $this->isJSActivated( $JSActivationStatus, $isJSActivated );

		//additionaly we check if activation is visible on front page
		//sometimes it happens that module is activated but still not working (joomla has such bug :( )
		$isJSActivCheckFrontPage = false;
		$isJSActivCheckFrontPageiMsg = '';
		$isJSActivCheckFrontPageRes = true;
		if( $isJSActivated ) {
			$isJSActivCheckFrontPageRes = $this->isJSActivCheckFrontPage( $isJSActivCheckFrontPage, $isJSActivCheckFrontPageiMsg );
			$bRes &= $isJSActivCheckFrontPageRes;
		}

		if ( ($bRes == true) && ($isJSActivated == true) && ($isJSActivCheckFrontPage == true) ) {
			//everything is OK, so exit
			return true;
		}


		//something is wrong


		{//A: check if user acivated JS in joomla by using 'Stand Alone' Activation method

			$txt = '';
			if( $JSActivationStatus['sa_tpl']['is_acivated'] == true ) {
                                $txt .= JText::sprintf( 'COM_J4AGE_ACTIVATION_METHOD', $this->activeMethodTypeToText( $method['type'] ) )
				;
			}

			if ( $txt != '' ) {
                                $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_ACTIVATION' ), 'description' => $txt );
			}
		}

		//B: Generate more detailed info
		$activationWarningMsg = '';
		{
			//'check if JS is twice activated by the same method'
			foreach ( $JSActivationStatus as $method ) {
				if ( $method['is_acivated_twice'] == true ) {
                                        $activationWarningMsg .= JText::sprintf( 'COM_J4AGE_ACTIVATION_METHOD_MULTIPLE',
									$this->activeMethodTypeToText( $method['type'] ) )
					;

					return true;
				}
			}

			// check if JS is activated twice by 2 diferent methods' or 'check if it is shown at front page'
			$isJSActivatedTwice	= false;
			$isJSActivated		= false;
			$isJSActivatedBy	= array();

			foreach( $JSActivationStatus as $method ) {
				if( ( $isJSActivated == true ) && ( $method['is_acivated'] == true ) ) {
					//JS is activated by 2 methods at the same time!
					$isJSActivatedTwice = true;
				}
				if( $method['is_acivated'] == true ) {
					$isJSActivatedBy[]	= $method['type'];
					$isJSActivated		= true;
				}
			}

			// parse front page to check if JS is activated'
			if( $isJSActivated == true ) {
				if ( $isJSActivCheckFrontPageRes == false) {
                                        $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_ACTIVATION' ), 'description' => $isJSActivCheckFrontPageiMsg );
				} else {
					if( $isJSActivCheckFrontPage == false ) {
                                                $activationWarningMsg .= JText::_( 'COM_J4AGE_ACTIVATED_METHOD_METHODS' ) . ': ';
						foreach ( $isJSActivatedBy as $activeMethodType ) {
							$activationWarningMsg .= '['.$this->activeMethodTypeToText($activeMethodType).'], ';
						}
                                                $activationWarningMsg .= JText::_( 'COM_J4AGE_HOMEPAGE_VISITORS_NOT_COUNTED' )
						;

						return true;
					}
				}
			}

			//'check if JS is twice activated by 2 different methods'
			if ( $isJSActivatedTwice == true ) {
                                $activationWarningMsg .= JText::_( 'COM_J4AGE_ACTIVATED_MULTIPLE_TIMES' );
				foreach( $isJSActivatedBy as $activeMethodType ) {
					$activationWarningMsg .= ' ['.$this->activeMethodTypeToText($activeMethodType).']';
				}
				$activationWarningMsg .= '.'
				;

				return true;
			}

			//check if user install activation method
			$isJSActivationTypeInstalled = array();
			foreach( $JSActivationStatus as $method ) {
				if( $method['is_installed'] == true ) {
					$isJSActivationTypeInstalled[] = $method['type'];
				}

				if( $method['is_acivated'] == true ) {
					//@todo throw JS error here (what? we checked activated modules, if there are, function return earlier!)
					return false;
				}
			}

			if( count( $isJSActivationTypeInstalled ) > 0 ) {

				$meths = '';
				foreach( $isJSActivationTypeInstalled as $installedMethodType ) {
					$meths .= '['. $this->activeMethodTypeToText( $installedMethodType ) .'] ';
				}

                                $activationWarningMsg .= JText::sprintf( 'COM_J4AGE_SEEMS_APPLIED_ACTIVATION_METHOD', $meths )
				. '<br />'
                                . JText::_( 'COM_J4AGE_ACTIVATE_J4AGE_ENABLE_PUBLISH' ) . ' ';

				if( count( $isJSActivationTypeInstalled ) > 1 ) {
                                        $activationWarningMsg .= JText::_( 'COM_J4AGE_ONE_OF' ) . ' ';
				}

				foreach( $isJSActivationTypeInstalled as $installedMethodType ) {
					$activationWarningMsg .= ' ['. $this->activeMethodTypeToText($installedMethodType) .']';
				}

				$activationWarningMsg .= '.'
				. '<br />'
				;
			} else {
				// user did not install mod nor bot nor insert code to template

                                $activationWarningMsg .= JText::_( 'COM_J4AGE_ALREADY_INSTALLED_NEEDS_ACTIVATED' )
				.'<br /><br />'
                                . JText::_( 'COM_J4AGE_CHOOSE_METHODS_ACTIVATE_J4AGE' )
				. ':<br />';

				//we make a list in case of line break //we numerate list manualy to be sure that order is constant
				$activationWarningMsg .= ''
				. '<ul style="margin: 0px 0px 0px 30px; padding: 0px;">'
                                . '<li style="list-style-type: none;"><b>A)</b> ' . JText::_( 'COM_J4AGE_INSTALL_J4AGE_ACTIVATION_MODULE' ) . '</li>'
                                . '<li style="list-style-type: none;"><b>B)</b> ' . JText::_( 'COM_J4AGE_INSTALL_J4AGE_ACTIVATION_PLUGIN' ) . '</li>'
                                . '<li style="list-style-type: none;"><b>C)</b> ' . JText::sprintf( 'COM_J4AGE_MODIFY_TEMPLATE_INSERTING_TAG', '&lt;body&gt;')
					. '<pre style="margin: 0px; padding: 0px;">'
					. "   &lt;?php\n"
					. "   if (file_exists(JPATH_SITE.DS.'components'.DS.'com_j4age'.DS.'j4age.inc.php'))\n"
					. "       include_once(JPATH_SITE.DS.'components'.DS.'com_j4age'.DS.'j4age.inc.php');\n"
					. "    ?&gt;"
					. '</pre>'
				. '</li>'
				. '</ul>'
				;
			}
		}
                $warningMsg[] = array( 'name' => JText::_( 'COM_J4AGE_ACTIVATION' ), 'description' => $activationWarningMsg );

		return true;
	}

	/**
	 *  Check if WhoIs option is working.
	 *  Check access to RIPE servers. We check 3 different servers, if at least one connection was successful, that mean WhoIs will work!
	 *
	 *  NOTICE:
	 *    This function could not be called during installation process - performing it could take even 20[s]!
	 *
	 *  @return bool true when WhoIs is working
	 */
	function isWhoIsOptionWorking() {

		$isWorking = false;
		$server_arr = array('whois.ripe.net', 'whois.apnic.net', 'whois.arin.net');

		// fsockopen rise PHP warning. The only way to avid it is turn of rising warnings in PHP (for a while)
	    $err_rep_ory = error_reporting();
	    $err_rep = $err_rep_ory ^ E_WARNING;
	    error_reporting($err_rep);


		$errno = 0;
		$errstr = '';
		$timeout = 3;//give 3 [s] to connect - server could be bussy
		foreach( $server_arr as $server ) {
			$fp = fsockopen( gethostbyname( $server ), 43, $errno, $errstr, $timeout );
			if (!$fp) {
				// to avoid 100 questions in JoomlaStats support better do not echo enything here - notice with direct link to help, will be displayed later
				//js_echoJSDebugInfo('Connection to server \''.$server.'\' failed during checking if WhoIs option is accessable - it means that WhoIs option is not accessable on this machine.', '');
			    //echo "ERROR: $errno - $errstr<br />\n";
			} else {
			    fclose($fp);
				$isWorking = true;
				break;
			}
		}

		// bring back oryginal settings
	    error_reporting($err_rep_ory);

		return $isWorking;
	}

	function checkDevelopmentSnapshot( &$recommendationMsg ) {
		$JSVersion_result = '';
		$JSApiGlobal = new js_JSApiGeneral();
		$JSVerResult = $JSApiGlobal->getJSVersion($JSVersion_result);
		if ($JSVerResult == true) {
			$pos = strpos($JSVersion_result, ' '); //see description of JS version format
			if ($pos !== false)
                                $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_DEVELOPMENT_SNAPSHOT' ), 'description' => JText::_( 'COM_J4AGE_DEVELOPMENT_SNAPSHOT_DESC' ) );
		}
	}

	function checkMySqlVersion( &$recommendationMsg ) {
		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();
		$is40 = $JSDatabaseAccess->isMySql40orGreater();
		if ($is40 == false) {
                        $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_DATABASE' ), 'description' => JText::_( 'COM_J4AGE_DATABASE_3X' ) );
		}
	}

	function checkWhoIsSupport( $isInstallPage, &$infoMsg, &$recommendationMsg ) {
		$JSConf = js_JSConf::getInstance();
        if ($isInstallPage == false) { //we can not check WHOIS option during installation - it takes too much time!
			if ( $JSConf->enable_whois == true ) {
				$isWhoIsOptionWorking = $this->isWhoIsOptionWorking();
				if( $isWhoIsOptionWorking == false ) {
                                        $TLDrecommendationMsg = JText::_( 'COM_J4AGE_WHOIS_NOT_WORKING' ) . '<br/>'
                                        . JText::sprintf( 'COM_J4AGE_WORKS_CORRECTLY_FEATURE_UNAVAILABLE_LIMITED', JText::_( 'COM_J4AGE_RECOGNITION_VISITOR_COUNTRIES' ) )
					;

                                        $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_FIND_VISITOR_COUNTRY' ), 'description' => $TLDrecommendationMsg );
				}


				if ( $isWhoIsOptionWorking == false ) {
                                        $desc = JText::sprintf( 'COM_J4AGE_BECAUSE_WHOIS_OPTION_NOT_WORKING_CONFIGURATION', JText::_( 'COM_J4AGE_WHOIS_SUPPORT' ) );
                                        $recommendationMsg[] = array( 'name' => JText::_( 'COM_J4AGE_WHOIS_SUPPORT' ), 'description' => $desc );
				}
			}
		}

		if ( $JSConf->enable_whois == false ) {
                        $desc = JText::sprintf( 'COM_J4AGE_OPTION_TURNED_OFF', JText::_( 'COM_J4AGE_WHOIS_SUPPORT' ) ) . '<br/>'
                        . JText::sprintf( 'COM_J4AGE_WORKS_CORRECTLY_FEATURE_UNAVAILABLE_LIMITED', JText::_( 'COM_J4AGE_RECOGNITION_VISITOR_COUNTRIES' ) );
                        $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_WHOIS_SUPPORT' ), 'description' => $desc );
		}
	}

	function getDatabaseDetails( $isInstallPage, &$infoMsg ) {
		if ($isInstallPage == false) {
			$JSUtil = new js_JSUtil();
			$DatabaseSizeHtmlCode = $JSUtil->getJSDatabaseSizeHtmlCode();

                        $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_DATABASE' ), 'description' => JText::_( 'COM_J4AGE_DATABASE_SIZE' ) . ':&nbsp;<b>' . $DatabaseSizeHtmlCode . '</b>&nbsp;[' . JText::_( 'COM_J4AGE_MB' ) . ']' );
		}
	}

	function getTranslationDetails( $isInstallPage, &$infoMsg ) {
		if ($isInstallPage == false) {
                        $TranslationAuthorDetails = JText::_( 'COM_J4AGE_TRANS_AUTHOR_DETAILS' );

                        if ( strtolower($TranslationAuthorDetails) == strtolower('COM_J4AGE_TRANS_AUTHOR_DETAILS') )
                                $TranslationAuthorDetails = ''; //do not display 'Translation Author details' when author of translation do not fill it

                        $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_TRANS_AUTHOR' ), 'description' => $TranslationAuthorDetails );
		}
	}

	/**
	 *  list of available/recognized Operating Systems
	 */
	function getSupportedOSList( $isInstallPage, &$infoMsg ) {
		if ($isInstallPage == false) {
			$JSApiBase = new js_JSApiBase();
			$AvailableOperatingSystemListForHuman = array();
			$JSApiBase->getAvailableOperatingSystemListForHuman( $AvailableOperatingSystemListForHuman );

			$html_list = '<div style="padding-top: 3px">';
			foreach($AvailableOperatingSystemListForHuman as $row) {
				$html_list .= '<img src="'.$row->os_img_url.'" alt="'.$row->os_name.'" title="'.$row->os_name.'"/>' . '&nbsp;&nbsp;';
			}
			$html_list .= '</div>';

                        $infoMsg[] = array( 'name' => JText::_( 'COM_J4AGE_OPERATING_SYSTEMS' ), 'description' => $html_list );
		}
	}


	//update is performed on status page (just after install) because domain and Joomla CMS portal title could change at any time
	//table #__jstats_searchers
	//   - positions 1 and 2 are special! `searcher_name` and `searcher_domain` are replaced by 'user portal name' and by domain of server that Joomla CMS is installed
	//   - position 1 is special in additional way: It stores keywords that user use in "Joomla CMS search" feature even if no articles were found to this keywords (and even if user do not open those articles)
	//   - position 2 works like all others
	function updateJoomlaCmsEntriesInSearchersTable() {

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

		$portal_title = 'Joomla CMS';
		$portal_domain = 'localhost';

		{
			$conf =& JFactory::getConfig();
			$portal_title = $conf->getValue('config.sitename');

			$JSCountVisitor = new js_JSCountVisitor();
			$url_to_front_site = JURI::root(false);
			$portal_domain = $JSCountVisitor->getDomainFromUrl($url_to_front_site);
		}


		$query = 'UPDATE `#__jstats_searchers` SET'
		. '   `searcher_name`   = \''.$portal_title.'\', '
		. '   `searcher_domain` = \''.$portal_domain.'\' '
		. ' WHERE'
		. '  `searcher_id` = '._JS_DB_SERCH__ID_SEARCH_JOOMLA_CMS
		//. '  OR `searcher_id` = '._JS_DB_SERCH__ID_JOOMLA_CMS
		;

		$JSDatabaseAccess->db->setQuery( $query );
		$JSDatabaseAccess->db->query();
		if ($JSDatabaseAccess->db->getErrorNum() > 0) {
			$err_msg = $JSDatabaseAccess->db->getErrorMsg();
			return false;
		}

		return true;
	}

 	/**
	 * checks for installed plugin and if activated
	 *
	 * @param bool $isPlgInstalled
	 * @param bool $isPlgActivated
	 * @param bool $isPlgActivatedTwice
	 * @return bool
	 */
	function isIP4NationOk( $isInstallPage, &$StatusTData) {

		$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();

        $rowslowercase = $JSDatabaseAccess->getTableColumnDetails("#__ip2nationcountries");
        $rowsuppercase = $JSDatabaseAccess->getTableColumnDetails("#__ip2nationCountries");

        $lowercaseSupport = count($rowslowercase) > 0;
        $uppercaseSupport = count($rowsuppercase ) > 0;
        $caseinsensitiveSupport = $lowercaseSupport && $uppercaseSupport;

        if($caseinsensitiveSupport)
        {
            //case-insensitve DB, no issue
        }
        else if($uppercaseSupport)
        {
            //we request using the uppercase  => no issue
        }
        else{
            //Big issue, using the correct names do not work
            $html_list = '<div style="padding-top: 3px">';
		    $html_list .= 'Invalid ip2nation table-name __ip2nation<strong>c</strong>ountries. The "c" must be a capital letter. <a href="index.php?option=com_j4age&amp;controller=maintenance&amp;task=fixip2nationname">Click here</a> to fix the issue' ;
			$html_list .= '</div>';
            $StatusTData->errorMsg[] = array( 'name' => "Invalid ip2nation table name", 'description' => $html_list );
            return;
        }



		return true;
	}


	/**
	 *
	 */
	function viewJSStatusPageBase( $isInstallPage, $StatusTData ) {

		$this->updateJoomlaCmsEntriesInSearchersTable();

		$this->checkMySqlVersion( $StatusTData->recommendationMsg );
		$this->checkDevelopmentSnapshot( $StatusTData->recommendationMsg );

		$this->checkJSActivation( $StatusTData->warningMsg, $StatusTData->recommendationMsg, $StatusTData->infoMsg );
		$this->checkDatabaseAndPhpFilesVersions( $StatusTData->warningMsg );

		$this->checkWhoIsSupport( $isInstallPage, $StatusTData->infoMsg, $StatusTData->recommendationMsg );
		$this->checkAnonymousUserTimeZoneTheSameAsCurrentlyLoggedUserTimeZone( $StatusTData->recommendationMsg );

		$this->getDatabaseDetails( $isInstallPage, $StatusTData->infoMsg );
		$this->showAnonymousUserCurrentTime( $isInstallPage, $StatusTData->infoMsg );
		$this->getTranslationDetails( $isInstallPage, $StatusTData->infoMsg );
		$this->getSupportedOSList( $isInstallPage, $StatusTData->infoMsg );

		return true;
	}


}
