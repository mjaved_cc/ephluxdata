<?php defined('_JEXEC') or die('JS: No Direct Access');
echo JoomlaStats_Engine::renderFilters(false, true);
?>

		<table class="adminlist">
		<thead>
            <tr>
                <th style="width: 1%;">#</th>
                <th style="width: 1px; text-align: center;"><?php echo JText::_( 'COM_J4AGE_TLD' );?></th>
                <th style="width: 1px; text-align: center;"><?php echo JText::_( 'COM_J4AGE_COUNTRY_DOMAIN' );?></th>
                <th style="width: 1px;"><?php echo JText::_( 'COM_J4AGE_PAGES' );?></th>
                <th style="width: 1px;"><?php echo JText::_( 'COM_J4AGE_TIME' );?></th>
                <th style="width: 100%;"><?php echo JText::_( 'COM_J4AGE_BRCS_NAME' );?></th>
                <th align="left"><?php echo JText::_( 'COM_J4AGE_IP' );?></th>
                <th align="left"><?php echo JText::_( 'COM_J4AGE_NS_LOOKUP' );?></th>
                <th align="left"><?php echo JText::_( 'COM_J4AGE_ACTIONS' );?></th>
            </tr>
		</thead>
<?php
        $rowCount = count($this->rows);
 		if ( $rowCount > 0 )
{
			$k = 0;
			$order_nbr = $this->pagination->limitstart;
            $rowLimit = $this->pagination->limitstart+$this->pagination->limit;
			for ($i=$order_nbr; ($i<$rowCount && $i<($rowLimit)); $i++) {
                if(!js_JSDatabaseAccess::executionTimeAvailable(30) )
                {
                   break; //We avoid timeouts by executiong to many enquires
                }
				$row = $this->rows[$i];
				$order_nbr++;

				$time =& js_getDate($row->changed_at);
				//$time->setOffset($this->time_zone_offset);//no we are in local time zone
				$time_str = $time->toFormat();
                ?>

                                <tr class="row<?php echo $k ;?>" <?php echo ( ($row->pages_nbr === null) ? (' style="color:#666666" title="' . JText::_( 'COM_J4AGE_DATA_ALREADY_PURGED' ) . '"' ) : ('')) ;?> >
			  	<td style="text-align: right;"><em><?php echo $order_nbr;?></em></td>
				<td style="text-align: center;"><?php echo $row->code;?></td>
				<td align="left"><?php echo JText::_( $row->code );?></td>

                <?php
				// PAGES column
				if (!isset($row->pages_nbr) || $row->pages_nbr <= 0) {
	                ?><td style="text-align: center;">***</td><?php //*** placeholder for archived/purged items
				} else {
	                ?>
                                        <td style="text-align: right;" nowrap="nowrap" title="<?php echo JText::_( 'COM_J4AGE_FOR_ADDITIONAL_DETAILS' );?>">
                        <?php echo js_renderPopupIcon( _JSAdminImagePath."pathinfo.png", $row->pages_nbr, 'index.php?option=com_j4age&amp;header=0&amp;controller=main&amp;tmpl=component&amp;task=detailVisitInformation&amp;moreinfo='.$row->visit_id, JText::_( 'COM_J4AGE_PATH_INFO' ));?>
	                </td>

					<?php
				}

				?>
				<td nowrap="nowrap"><?php echo $time_str ;?></td>
				<td nowrap="nowrap">
					<?php echo $row->browser_name.' '.$row->browser_version. ( $row->useragent ? ' (' . substr( $row->useragent, 0, 70 ) . ')' : '' )?>
				</td>
                <td align="left" nowrap="nowrap" >
                    <div  style="float:left; text-align: left">
                        <a href="javascript:document.adminForm.returnTask.value='bots';document.adminForm.vid.value='<?php echo $row->ip ;?>';document.adminForm.controller.value='maintenance';submitbutton('<?php echo ($row->ip_exclude ? "js_do_ip_include" : "js_do_ip_exclude") ;?>');" title="<?php echo  ($row->ip_exclude ? JText::_( 'COM_J4AGE_CLICK_INCLUDE_IP' ) : JText::_( 'COM_J4AGE_CLICK_EXCLUDE_IP' )) ;?>">
                          <img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->ip_exclude ? 'cross.png' : 'tick.png');?>" border="0" alt="<?php echo  ($row->ip_exclude ? JText::_( 'COM_J4AGE_CLICK_INCLUDE_IP' ) : JText::_( 'COM_J4AGE_CLICK_EXCLUDE_IP' )) ;?>" />
                        </a>
                        <a href="javascript:document.adminForm.returnTask.value='bots';document.adminForm.vid.value='<?php echo $row->ip; ?>';document.adminForm.controller.value='maintenance';submitbutton('<?php echo ($row->ip_type != 2 ? "classifyIPAsBot" : "classifyIPAsBrowser") ;?>');" title="<?php echo  ($row->ip_type != 2 ? JText::_( 'COM_J4AGE_CLASSIFY_BOT' ) : JText::_( 'COM_J4AGE_CLASSIFY_BROWSER' )) ;?>">
                          <img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->ip_type != 2 ? 'user.png': 'bot-16.png');?>" border="0" width="16" alt="<?php echo  ($row->ip_type != 2 ? JText::_( 'COM_J4AGE_CLASSIFY_IP_BOT' ) : JText::_( 'COM_J4AGE_CLASSIFY_IP_BROWSER' )) ;?>" />
                        </a>
                        <?php echo long2ip($row->ip); ?>
                    </div>
                    <div style="float:right; width:16px; text-align: right">
                   </div>
                 </td>
				<td nowrap="nowrap"><?php echo empty($row->nslookup)? long2ip($row->ip) : $row->nslookup ;?></td>
                <td>
                    <a href="javascript:document.adminForm.returnTask.value='bots';document.adminForm.vid.value='<?php echo $row->client_id ;?>';document.adminForm.controller.value='maintenance';submitbutton('<?php echo ($row->client_exclude ? "includeClients" : "excludeClients") ;?>');" title="<?php echo  ($row->client_exclude ? JText::_( 'COM_J4AGE_CLICK_INCLUDE_USERAGENT' ) : JText::_( 'COM_J4AGE_CLICK_EXCLUDE_USERAGENT' )) ;?>">
                      <img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->ip_exclude ? 'cross.png' : 'tick.png');?>" border="0" alt="<?php echo  ($row->client_exclude ? JText::_( 'COM_J4AGE_CLICK_INCLUDE_USERAGENT' ) : JText::_( 'COM_J4AGE_CLICK_EXCLUDE_USERAGENT' )) ;?>" />
                    </a>
                    <a href="javascript:document.adminForm.returnTask.value='bots';document.adminForm.vid.value='<?php echo $row->client_id ;?>';document.adminForm.controller.value='maintenance';submitbutton('<?php echo ($row->client_type == 1 ? "classifyAsBot" : "classifyAsBrowser") ;?>');" title="<?php echo  ($row->client_type == 1 ? JText::_( 'COM_J4AGE_CLASSIFY_BOT' ) : JText::_( 'COM_J4AGE_CLASSIFY_BROWSER' )) ;?>">
                      <img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->client_type == 1 ? 'user.png': 'bot-16.png');?>" border="0" width="16" alt="<?php echo  ($row->client_type == 2 ? JText::_( 'COM_J4AGE_CLASSIFY_BOT' ) : JText::_( 'COM_J4AGE_CLASSIFY_BROWSER' )) ;?>" />
                    </a>
                </td>
				</tr>

                <?php
				$k = 1 - $k;
			}
		} else {
                        ?><tr><td colspan="9" style="text-align:center"><?php echo JText::_( 'COM_J4AGE_NO_DATA' );?></td></tr><?php
		}

		?>
		<tfoot>
            <tr>
                <td colspan="9"><?php echo $this->pagination->getListFooter();?></td>
            </tr>
		</tfoot>
</table>



