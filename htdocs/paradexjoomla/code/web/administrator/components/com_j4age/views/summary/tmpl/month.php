<?php defined('_JEXEC') or die('JS: No Direct Access');
echo JoomlaStats_Engine::renderFilters(false, false, true);
?>
<?php if(!empty($this->chartView)) { echo $this->chartView->display();}

$this->JSSystemConst = new js_JSSystemConst();
$this->JSUtil = new js_JSUtil();

function js_summary_trend_icon($current, $previous, $parameter)
{
    if(empty($current->$parameter) || empty($previous))
    {
        return '';
    }

    $html = '<img src="';
    if($current->$parameter == $previous->$parameter)
    {
        $html .= _JSAdminImagePath.'equal.png';
    }
    else if($current->$parameter > $previous->$parameter)
    {
        $html .= _JSAdminImagePath.'greater.png';
    }
    else if($current->$parameter < $previous->$parameter)
    {
        $html .= _JSAdminImagePath.'lower.png';
    }
    $html .='" border="0" height="15" width="15" title=""/>';
    return $html;
}
?>
<table class="adminlist">
    <thead>
        <tr>
            <th nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_DAY' )?></th>
            <th colspan="2" nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_UNIQUE_VISITORS' ) ;?>"><?php echo  JText::_( 'COM_J4AGE_UNIQUE_VISITORS' ) ;?></th>
            <th colspan="2" nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_OF_VISITORS' ) ;?>"><?php echo  JText::_( 'COM_J4AGE_VISITORS' ) ;?></th>
            <th nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_OF_VISITORS' ) . ' / ' . JText::_( 'COM_J4AGE_NUMBER_UNIQUE_VISITORS' ) ;?>"><?php echo  JText::_( 'COM_J4AGE_VISITS_AVERAGE' ) ;?></th>
            <th colspan="2" nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_OF_VISITED_PAGES' );?>"><?php echo JText::_( 'COM_J4AGE_PAGE_IMPRESSIONS' ) ;?></th>
            <th nowrap="nowrap"><?php echo  JText::_( 'COM_J4AGE_REFERRERS' ) ;?></th>
            <th nowrap="nowrap"><?php echo  JText::_( 'COM_J4AGE_SEARCH_ENGINES' ) ;?></th>
            <th colspan="2" nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_UNIQUE_BOTS_SPIDERS' );?>"><?php echo  JText::_( 'COM_J4AGE_UNIQUE_BOTS_SPIDERS' ) ;?></th>
            <th colspan="2" nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_OF_BOTS_SPIDERS' ) ;?>"><?php echo  JText::_( 'COM_J4AGE_BOTS_SPIDERS' ) ;?></th>
            <th nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_UNIQUE_NOT_IDENTIFIED_VISITORS' ) ;?>"><?php echo  JText::_( 'COM_J4AGE_UNIQUE_NIV' ) ;?></th>
            <th nowrap="nowrap" title="<?php echo  JText::_( 'COM_J4AGE_NUMBER_NOT_IDENTIFIED_VISITORS' );?>"><?php echo  JText::_( 'COM_J4AGE_NIV' ) ;?></th>
            <th nowrap="nowrap"><?php echo  JText::_( 'COM_J4AGE_UNIQUE_SUM' ) ;?></th>
            <th nowrap="nowrap"><?php echo  JText::_( 'COM_J4AGE_SUM' ) ;?></th>
        </tr>
    </thead>
        <?php
        $previousRow = null;

		foreach( $this->rows as $row ) {

			// now we have all values, now draw the row (day)
			if( date( 'w', strtotime( $row->year.'-'.$row->month.'-'.$row->day ) ) == 6 ) {
				$cls = 'row0'; // info: background-color: #F9F9F9;
			}elseif (date( 'w', strtotime( $row->year.'-'.$row->month.'-'.$row->day ) ) == 0 ) {
				$cls = 'row2" style="background-color:#efefef; border-bottom: 1px dotted #ff0000';
			}else{
				$cls = 'row1'; // info: background-color: #F1F1F1;
			}
            ?>
			<tr class="<?php echo $cls;?>">
                <td align="center"><?php echo $row->i;?></td>
                <td align="right"><?php echo  ($row->uniqueVisitor ? $row->uniqueVisitor : '.');?></td>
                <td align="left" style="width:10px"><?php echo js_summary_trend_icon($row, $previousRow, 'uv');?></td>
                <td align="right">
                    <a href="javascript:SelectMonth('.$row->month.');SelectDay(<?php echo $row->i;?>);submitbutton('visitors');" title="<?php echo JText::_( 'COM_J4AGE_CLICK_FOR_VISITORS_DETAILS' );?>">
                        <?php echo ($row->visitors ? $row->visitors : '.') ;?>
                    </a>
                </td>
                <td align="left" style="width:10px"><?php echo js_summary_trend_icon($row, $previousRow, 'v');?></td>
                <td align="center"><?php echo $row->averageVisits;?></td>
                <td align="right">
                    <?php echo ( $row->pages ? '<a href="javascript:SelectMonth('.$row->month.');SelectDay('.$row->i.');submitbutton(\'pageHits\');" title="'. JText::_( 'COM_J4AGE_CLICK_FOR_PAGE_DETAILS' ).'">'.  $row->pages .'</a>' : '.' ) ;?>
                </td>
                <td align="left" style="width:10px"><?php echo js_summary_trend_icon($row, $previousRow, 'p');?></td>
                <td align="center">
                    <?php echo ( $row->referrerCount ? '<a href="javascript:SelectMonth('.$row->month.');SelectDay('.$row->i.');submitbutton(\'referrersByDomain\');" title="'. JText::_( 'COM_J4AGE_CLICK_FOR_REFERRER_DETAILS' ).'">'.$row->referrerCount.'</a>' : '.' );?>
                </td>
                <td align="center"><?php echo ( $row->inquiries ? $row->inquiries : '.' );?></td>
                <td align="right"><?php echo  ( $row->uniqueBots ? $row->uniqueBots : '.' );?></td>
                <td align="left" style="width:10px"><?php echo js_summary_trend_icon($row, $previousRow, 'ub');?></td>
                <td align="right">
                    <?php echo ( $row->bots ? '<a href="javascript:SelectMonth('.$row->month.');SelectDay('.$row->i.');submitbutton(\'botsByDomain\');" title="'. JText::_( 'COM_J4AGE_FOR_ADDITIONAL_DETAILS' ).'">'. $row->bots.'</a>' : '.' );?>
                </td>
                <td style="width:10px"><?php echo js_summary_trend_icon($row, $previousRow, 'b');?></td>
                <td><?php echo  ( ($row->uniqueNotIdentifiedVisitors) ? ($row->uniqueNotIdentifiedVisitors ) : '.' );?></td>
                <td>
                    <?php echo  ( ($row->notIdentifiedVisitors) ? '<a href="javascript:SelectMonth('.$row->month.');SelectDay('.$row->i.');submitbutton(\'notidentifiedvisitors\');" title="' . JText::_( 'COM_J4AGE_FOR_ADDITIONAL_DETAILS' ) . '">'.$row->notIdentifiedVisitors.'</a>' : '.' );?>
                </td>
                <td><?php echo  ( $row->uniqueSum ? $row->uniqueSum : '.' );?></td>
                <td><?php echo  ( $row->sum ? $row->sum : '.' );?></td>
			</tr>

			<?php
            $previousRow = $row;

		}
 ?>
    <thead>
        <tr>
            <th align="center"><?php  echo $this->JSTemplate->monthToString($this->total->month_or_year, true) ;?></th>
            <th colspan="2" align="right"><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalUniqueVisitors, 0 ) ;?></th>
            <th colspan="2" align="right"><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalVisitors, 0 ) ;?></th>
            <th align="center"><?php  echo $this->total->visits_average;?></th>
            <th colspan="2" align="right"><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalPages, 0 ) ;?></th>
            <th align="center"><?php  echo $this->total->totalReferrers ;?></th>
            <th align="center"><?php  echo $this->total->inquiries ;?></th>
            <th colspan="2" align="right"><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalUniqueBots, 0 ) ;?></th>
            <th colspan="2" align="right"><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalBots, 0 ) ;?></th>
            <th><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalUniqueNotIdentifiedVisitors, 0 ) ;?></th>
            <th><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalNotIdentifiedVisitors,  0 ) ;?></th>
            <th><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalUniqueSum, 0 ) ;?></th>
            <th><?php  echo $this->JSStatisticsTpl->addSummStyleLine( false, $this->total->totalSum,  0 ) ;?></th>
        </tr>
    </thead>
</table>
<?php
                js_echoJSDebugInfo( $this->prof->mark( JText::_( 'COM_J4AGE_END' ) ), '');
?>



