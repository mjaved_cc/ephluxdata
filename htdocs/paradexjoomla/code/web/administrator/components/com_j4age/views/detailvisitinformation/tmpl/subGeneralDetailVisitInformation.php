<?php defined('_JEXEC') or die('JS: No Direct Access');


$visitor_type_name = JText::_('COM_J4AGE_NOT_IDENTIFIED'); //_JS_DB_IPADD__TYPE_NOT_IDENTIFIED_VISITOR
if ($this->VisitorObj->client_type == _JS_DB_IPADD__TYPE_REGULAR_VISITOR)
    $visitor_type_name = JText::_('COM_J4AGE_REGULAR');
else if ($this->VisitorObj->client_type == _JS_DB_IPADD__TYPE_BOT_VISITOR)
    $visitor_type_name = JText::_('COM_J4AGE_BOT');
$date = js_getDate($this->VisitObj->changed_at);
?>

<div style="text-align: center; font-weight: bold; font-size: larger;"><?php echo JText::_( 'COM_J4AGE_VISITOR_DETAILS' );?></div>
<div style="text-align: center;">
<table class="adminlist" style="width: 90%;"  align="center">
    <thead>
    <tr>
        <th>#</th>
        <th style="text-align: left;"><?php echo JText::_( 'COM_J4AGE_VISITOR' );?></th>
        <th style="text-align: left;"><?php echo JText::_( 'COM_J4AGE_VALUE' );?></th>
    </tr>
    </thead>
    <tbody>
		<tr class="row0">
			<td style="text-align: right;"><em>1.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_VISIT_DATE' );?>:</td>
			<td nowrap="nowrap"><?php echo $date->toFormat();?></td>
		</tr>
		<tr class="row1">
			<td style="text-align: right;"><em>2.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_USERNAME' );?>:</td>
                        <td nowrap="nowrap"><?php echo ( ($this->VisitObj->joomla_userid > 0) ? $this->VisitObj->joomla_username : JText::_( 'COM_J4AGE_NOT_LOGGET_IN' ) );?></td>
		</tr>
		<tr class="row0">
			<td style="text-align: right;"><em>2.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_VISITOR_ID' );?>:</td>
			<td nowrap="nowrap"><?php echo $this->VisitorObj->visitor_id;?></td>
		</tr>
		<tr class="row1">
			<td style="text-align: right;"><em>3.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_IP' );?>:</td>
			<td nowrap="nowrap"><?php echo long2ip($this->VisitorObj->visitor_ip);?></td>
		</tr>
		<tr class="row0">
			<td style="text-align: right;"><em>4.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_NS_LOOKUP' );?>:</td>
			<td nowrap="nowrap"><?php echo $this->VisitorObj->visitor_nslookup;?></td>
		</tr>
		<tr class="row1">
			<td style="text-align: right;"><em>5.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_SYSTEM' );?>:</td>
			<td nowrap="nowrap"><?php echo $this->VisitorObj->system;?></td>
		</tr>
		<tr class="row0">
			<td style="text-align: right;"><em>6.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_BROWSER' );?>:</td>
			<td nowrap="nowrap"><?php echo $this->VisitorObj->browser;?></td>
		</tr>
		<tr class="row1">
			<td style="text-align: right;"><em>7.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_VISITOR_TYPE' );?>:</td>
			<td nowrap="nowrap"><?php echo $visitor_type_name;?></td>
		</tr>
		<tr class="row0">
			<td style="text-align: right;"><em>8.</em></td>
                        <td style="padding-right: 20px;" nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_COUNTRY' );?>:</td>
			<td nowrap="nowrap"><?php echo $this->VisitorObj->country;?></td>
		</tr>
	</tbody>	
</table>
</div>
<div style="text-align: center; font-weight: bold; font-size: larger;"><br/><?php echo JText::_( 'COM_J4AGE_VISITOR_COMMENT' );?></div>
<div style="text-align: center;">
  <?php if($this->VisitorObj->visitor_id > 0){;?>
    <input type="text" name="note" size="50"/>
  <?php };?>
</div>
