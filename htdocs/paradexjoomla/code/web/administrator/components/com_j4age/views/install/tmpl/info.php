<?php
/**
 * @version		$Id: default.php 12772 2009-09-18 02:23:53Z eddieajau $
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// Load the JavaScript behaviors.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>

<script language="JavaScript" type="text/javascript">
<!--
	function validateForm(frm, task) {
		Joomla.submitform(task);
	}
// -->
</script>

<div id="stepbar">
	<div class="t">
		<div class="t">
			<div class="t"></div>
		</div>
	</div>
	<div class="m">
		<div class="box"></div>
  	</div>
	<div class="b">
		<div class="b">
			<div class="b"></div>
		</div>
	</div>
</div>

<div id="right">
	<div id="rightpad">
		<div id="step">
			<div class="t">
				<div class="t">
					<div class="t"></div>
				</div>
			</div>
			<div class="m">
				<div class="far-right">
                    <div class="button1-left"><div class="next"><a href="index.php?option=com_j4age" title="<?php echo JText::_('COM_J4AGE_CLOSE'); ?>"><?php echo JText::_('COM_J4AGE_CLOSE'); ?></a></div></div>
				</div>
                                <span class="step"><?php echo JText::_('COM_J4AGE_CONGRATULATIONS'); ?></span>
			</div>
			<div class="b">
				<div class="b">
					<div class="b"></div>
				</div>
			</div>
		</div>
		<div id="installer">
			<div class="t">
				<div class="t">
					<div class="t"></div>
				</div>
			</div>
			<div class="m">
                <div>
                    <?php echo JText::_('COM_J4AGE_CONGRATULATIONS_SUCCESSFULLY'); ?><br/><br/>
                </div>
                <h2><?php echo JText::sprintf('COM_J4AGE_ABOUT_ECOMIZE'); ?></h2>
                <div class="install-text">
                    <img src="<?php echo JURI::base(true) . '/components/com_j4age/images/logo.gif';?>" alt="ecomize" title="ecomize" />
                    <?php echo JText::_('COM_J4AGE_CONGRATULATIONS_NOTICE'); ?>
                    <p><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_SPECIAL_SAP'); ?></p>
                    <ul>
                        <li><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_CRM'); ?></li>
                        <li><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_WEB_CHANNEL'); ?></li>
                        <li><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_MOBILEAPPS'); ?></li>
                        <li><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_SPECIAL_NETWEAVER'); ?></li>
                    </ul>
                    <p><?php echo JText::_('COM_J4AGE_CONGRATULATIONS_INTERNET_SALES'); ?></p>
                </div>
                <div class="newsection"></div>
    
                                <h2><?php echo JText::sprintf('COM_J4AGE_YOUR_FEEDBACK'); ?></h2>
				<div class="install-text">
                                <?php echo JText::_('COM_J4AGE_NOTICE'); ?>
				</div>

				<div class="newsection"></div>

                                <h2><?php echo JText::_('COM_J4AGE_DONATE'); ?></h2>

				<div class="install-text">
                    <a href="http://www.ecomize.com/en/j4age.html" target="_blank" title="j4age"><img src="http://www.paypal.com/en_US/i/btn/x-click-but04.gif" alt="j4age" title="j4age" /></a>
                    <p><?php echo JText::_('COM_J4AGE_DONATE_NOTICE'); ?></p>
				</div>
				<div class="clr"></div>
			</div>
			<div class="b">
				<div class="b">
					<div class="b"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clr"></div>

