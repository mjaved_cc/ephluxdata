<?php defined('_JEXEC') or die('JS: No Direct Access');
$JSTemplate = new js_JSTemplate();
		?>
		<div style="font-size: 1px;">&nbsp;</div>
		<?php /*<!-- This div is needed to show content of tab correctly in 'IE 7.0' in 'j1.5.6 Legacy'. Tested in: FF, IE, j1.0.15, j1.5.6 and works OK --> */ ?>
		<table class="adminform" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td nowrap="nowrap"><?php echo JText::_( 'Onlinetime' ); ?> <?php echo JText::_( 'Visitors' ); ?></td>
			<td>
				<select name="onlinetime">
				<?php
                                echo '<option value=  "10"'. ($this->JSConf->onlinetime ==   10 ? ' selected="selected"' : '') .'>10 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "15"'. ($this->JSConf->onlinetime ==   15 ? ' selected="selected"' : '') .'>15 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "20"'. ($this->JSConf->onlinetime ==   20 ? ' selected="selected"' : '') .'>20 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "25"'. ($this->JSConf->onlinetime ==   25 ? ' selected="selected"' : '') .'>25 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "30"'. ($this->JSConf->onlinetime ==   30 ? ' selected="selected"' : '') .'>30 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "60"'. ($this->JSConf->onlinetime ==   60 ? ' selected="selected"' : '') .'>60 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "90"'. ($this->JSConf->onlinetime ==   90 ? ' selected="selected"' : '') .'>90 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value= "120"'. ($this->JSConf->onlinetime ==  120 ? ' selected="selected"' : '') .'>2 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "240"'. ($this->JSConf->onlinetime ==  240 ? ' selected="selected"' : '') .'>4 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "480"'. ($this->JSConf->onlinetime ==  480 ? ' selected="selected"' : '') .'>8 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "720"'. ($this->JSConf->onlinetime ==  720 ? ' selected="selected"' : '') .'>12 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                echo '<option value="1440"'. ($this->JSConf->onlinetime == 1440 ? ' selected="selected"' : '') .'>24 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                echo '<option value="0"'. ($this->JSConf->onlinetime == 0 ? ' selected="selected"' : '') .'>'. JText::_( 'COM_J4AGE_INFINITY' ) . '</option>' . "\n";
                ?>
				</select>
			</td>
			<td width="100%">
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_TIMEOUT_IN_MINUTES' ) ); ?>
			</td>
		</tr>
		<tr>
                        <td nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_ONLINETIME' ); ?> <?php echo JText::_( 'COM_J4AGE_BOTS' ); ?></td>
			<td>
				<select name="onlinetime_bots">
				<?php
                                echo '<option value=  "10"'. ($this->JSConf->onlinetime_bots ==   10 ? ' selected="selected"' : '') .'>10 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "15"'. ($this->JSConf->onlinetime_bots ==   15 ? ' selected="selected"' : '') .'>15 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "20"'. ($this->JSConf->onlinetime_bots ==   20 ? ' selected="selected"' : '') .'>20 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "25"'. ($this->JSConf->onlinetime_bots ==   25 ? ' selected="selected"' : '') .'>25 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "30"'. ($this->JSConf->onlinetime_bots ==   30 ? ' selected="selected"' : '') .'>30 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "60"'. ($this->JSConf->onlinetime_bots ==   60 ? ' selected="selected"' : '') .'>60 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value=  "90"'. ($this->JSConf->onlinetime_bots ==   90 ? ' selected="selected"' : '') .'>90 '. JText::_( 'COM_J4AGE_MIN' ) . '</option>' . "\n";
                                echo '<option value= "120"'. ($this->JSConf->onlinetime_bots ==  120 ? ' selected="selected"' : '') .'>2 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "240"'. ($this->JSConf->onlinetime_bots ==  240 ? ' selected="selected"' : '') .'>4 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "480"'. ($this->JSConf->onlinetime_bots ==  480 ? ' selected="selected"' : '') .'>8 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                                echo '<option value= "720"'. ($this->JSConf->onlinetime_bots ==  720 ? ' selected="selected"' : '') .'>12 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                echo '<option value="1440"'. ($this->JSConf->onlinetime_bots == 1440 ? ' selected="selected"' : '') .'>24 '. JText::_( 'COM_J4AGE_HRS' ) . '</option>' . "\n";
                echo '<option value="0"'. ($this->JSConf->onlinetime_bots == 0 ? ' selected="selected"' : '') .'>'. JText::_( 'COM_J4AGE_INFINITY' ) . '</option>' . "\n";
                ?>
				</select>
			</td>
			<td width="100%">
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_TIMEOUT_IN_MINUTES' ) ); ?>
			</td>
		</tr>
		<tr>
                        <td nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_STARTOPTION' ); ?></td>
			<td>
				<select name="startoption">
					<?php

					$select_html = '';

					$MenuArrIdAndText = js_JSStatisticsCommon::getJSStatisticsMenu();;
					foreach( $MenuArrIdAndText as $id => $menu_config ) {
						$select_html .= '<option value="'.$id.'"'. ( $this->JSConf->startoption == $id ? ' selected="selected"' : '' ) . '>'
						. $menu_config['label'] . '</option>' . "\n";
					}

					echo $select_html;
					?>
				</select>
			</td>
			<td>
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_STARTOPTION_FOR_J4AGE' ) ); ?>
			</td>
		</tr>
		<tr>
                        <td nowrap="nowrap"><?php echo JText::_( 'COM_J4AGE_STARTBY_DAYORMONTH' ); ?></td>
			<td>
				<select name="startdayormonth">
					<?php
					echo "<option value='d'". ( $this->JSConf->startdayormonth == 'd' ? ' selected="selected"' : '' ) .'>'
                                        . JText::_( 'COM_J4AGE_DAY' ) .'</option>' ."\n";
					echo "<option value='m'". ( $this->JSConf->startdayormonth == 'm' ? ' selected="selected"' : '' ) .'>'
                                        . JText::_( 'COM_J4AGE_MONTH' ) .'</option>' ."\n";
					?>
				</select>
			</td>
			<td>
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_SELECT_FIRST_VIEW' ) ); ?>
			</td>
		</tr>
		<tr>
                        <td nowrap="nowrap"><label for="enable_whois"><?php echo JText::_( 'COM_J4AGE_WHOIS_SUPPORT' ); ?></label></td>
			<td>
				<input type="checkbox" name="enable_whois" id="enable_whois" <?php echo ( $this->JSConf->enable_whois ? ' checked="checked"' : '' ); ?> />
			</td>
			<td>
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_DO_A_WHOIS_QUERY' ) ); ?>
			</td>
		</tr>
		<tr>
                        <td nowrap="nowrap"><label for="enable_i18n"><?php echo JText::_( 'COM_J4AGE_I18N_SUPPORT' ); ?></label></td>
			<td>
				<input type="checkbox" name="enable_i18n" id="enable_i18n" <?php echo ( $this->JSConf->enable_i18n ? ' checked="checked"' : '' ); ?> /></td>
			<td>
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_MULTITRANSLATION_ONE' ) ); ?>
                        </td>
                </tr>
                <tr>
                        <td nowrap="nowrap"><label for="show_charts_within_reports"><?php echo JText::_( 'COM_J4AGE_INTEGRATE_CHARTS_REPORTS' ); ?></label></td>
			<td>
				<input type="checkbox" name="show_charts_within_reports" id="show_charts_within_reports" <?php echo ( $this->JSConf->show_charts_within_reports ? ' checked="checked"' : '' ); ?> /></td>
			<td>
				<?php
                                echo $JSTemplate->jsToolTip( JText::_( 'COM_J4AGE_SWITCHED_CHARTS_REPORTS' ) ); ?>
			</td>
		</tr>
		</table>
