<?php defined('_JEXEC') or die('JS: No Direct Access');
?>
<!--?php echo JText::_( 'JoomlaStats Help - Whole Page');  ?-->

<div><a href="http://www.ecomize.com" target="_blank" title="<?php echo JText::_( 'COM_J4AGE_VISITE_HOME_PAGE'); ?>"><img src="<?php echo JURI::base(true);?>/components/com_j4age/images/logo.gif" alt="ecomize" title="ecomize" /></a></div>
<p><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_INFO'); ?></p>

<h3><?php echo JText::_( 'COM_J4AGE_STATISTICS'); ?></h3>
<?php echo JText::_( 'COM_J4AGE_HELP_PAGE_INFO2'); ?>

<h3><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_HISTORY'); ?></h3>
<p>
<?php echo JText::_( 'COM_J4AGE_HELP_PAGE_HISTORY_INFO'); ?><br /><br />
<?php echo JText::_( 'COM_J4AGE_HELP_PAGE_HISTORY_INFO2'); ?>
</p>

<h3><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS'); ?></h3>
<ul>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI1'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI2'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI3'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI4'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI5'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI6'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI7'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI8'); ?></li>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_SHORT_FACTS_LI9'); ?></li>
</ul>

<h3><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_GOOD_KNOW'); ?></h3>
<ul>
<li><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_GOOD_KNOW_LI'); ?></li>
</ul>
<h3><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_DONATE'); ?></h3>
<a href="http://www.ecomize.com/en/j4age.html" target="_blank" title="j4age"><img src="http://www.paypal.com/en_US/i/btn/x-click-but04.gif" alt="j4age" title="j4age" /></a><br /><br />
<p><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_DONATE_NOTICE'); ?></p>

<h3><?php echo JText::_( 'COM_J4AGE_HELP_PAGE_DATABASE'); ?></h3>

<div><img src="<?php echo JURI::base(true);?>/components/com_j4age/images/database.png" alt="database" title="database" /></div>



