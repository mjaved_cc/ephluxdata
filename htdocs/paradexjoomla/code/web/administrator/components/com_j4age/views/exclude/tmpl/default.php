<?php defined('_JEXEC') or die('JS: No Direct Access');
echo JoomlaStats_Engine::renderFilters(true, false);
?>
    <table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
        <thead>
        <tr>
            <th width="1%" class="title">#</th>
            <th width="1%" class="title">
                <input type="checkbox" name="toggle" id="toggle" value="" onClick="checkAll(<?php echo count($this->rows); ?>);" />
            </th>
            <th width="5%" class="title"><?php echo JText::_( 'COM_J4AGE_IP_ADDRESS' ); ?></th>
            <th width="1%" class="title"><?php echo JText::_( 'COM_J4AGE_TYPE' ); ?></th>
            <th width="100%" class="title"><?php echo JText::_( 'COM_J4AGE_NS_LOOKUP' ); ?></th>
            <th width="2%" class="title"><?php echo JText::_( 'COM_J4AGE_EXCLUDE' ); ?></th>
        </tr>
        </thead>
            <?php
            $k = 0;
            $n = count($this->rows);

            for ($i = 0; $i < $n; $i++) {
                $row	=& $this->rows[$i];
                $task	= $row->ip_exclude ? 'js_do_ip_include' : 'js_do_ip_exclude';
                $alt        = $row->ip_exclude ? JText::_( 'COM_J4AGE_CLICK_TO_INCLUDE' ) : JText::_( 'COM_J4AGE_CLICK_TO_EXCLUDE' );
                ?>
            <tr class="row<?php echo $k; ?>">
                <td><?php echo $i + 1 + $this->pagination->limitstart;?></td>
                <td>
                    <input type="checkbox" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $row->ip; ?>" onClick="isChecked(this.checked);" />
                </td>
                <td>
                    <a href="http://<?php echo long2ip($row->ip); ?>" target="_blank" title="<?php echo JText::_( 'COM_J4AGE_OPENS_NEW_WINDOW' ); ?>"><?php echo long2ip($row->ip); ?></a>
                </td>
                <td>
                    <img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->ip_type != 2 ? 'user.png': 'bot-16.png');?>" border="0" width="16" alt="<?php echo  ($row->ip_type == 2 ? JText::_( 'COM_J4AGE_BOT' ) : JText::_( 'COM_J4AGE_BROWSER' )) ;?>" />
                </td>
                <td><?php echo $row->nslookup; ?></td>
                <td width="10%" align="center">
                <a href="javascript:void(0);" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>');" title="<?php echo $alt; ?>"><img src="<?php echo  _JSAdminImagePath;?><?php echo ($row->ip_exclude ? 'cross.png' : 'tick.png');?>" border="0" alt="<?php echo $alt; ?>" /></a>
                </td>
            </tr>
            <?php
            $k = 1 - $k;
        } ?>
        <tfoot>
        <tr>
            <td colspan="6"><?php echo $this->pagination->getListFooter(); ?></td>
        </tr>
        </tfoot>
    </table>