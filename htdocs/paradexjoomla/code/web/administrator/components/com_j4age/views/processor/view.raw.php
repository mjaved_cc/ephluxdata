<?php
/**
 * Created by IntelliJ IDEA.
 * User: ahalbig
 * Date: 31.03.11
 * Time: 21:34
 * To change this template use File | Settings | File Templates.
 */


              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          

// Check to ensure this file is only called within the Joomla! application and never directly
if( !defined( '_JEXEC' ) ) {
	die( 'JS: No Direct Access to '.__FILE__ );
}

require_once( dirname(__FILE__) .DS. '..'.DS. '..' .DS. 'libraries'.DS. 'JSON.php' );

jimport( 'joomla.application.component.view' );

/**
 * This is the new summarisation process
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class j4ageViewProcessor extends JView
{
	/**
	 * This view is used as processor in combination using ajax
     *
     * This process can be seen as summarization process, which includes some analytical processes to evaluate information
     *
     * Calculations are performed in hour slots, which allows to present any information at any time based on the user� current timezone.
     *
     * We store only single "dots" per calculation to allow a generic graph presentation
     *
	 * @return void
	 **/
	function display($tpl = null)
	{

        $mainframe = JFactory::getApplication();

        // ###  Filters
        $date_from = '';
        $date_to = '';

        $step = 3600; //always one hour steps

        $now = time();
        $start = JRequest::getVar('start', 0);
        $currentTime = JRequest::getVar('current', 0);
        $stop = JRequest::getVar('stop', $time);

        $cache =  array();

        $from = 0;

        /**
         * If no start parameter is given, we can assume it is the first call.
         * We need to find therefore our start point based on the last performed calculation
         */
        if(empty($start))
        {
            //todo find last date
        }else
        {
            $from = $start;
        }

        $to = $now;

        js_profilerMarker('Start');
        js_PluginManager::fireEventUsingSource( 'processDB', $from, $to);
        js_PluginManager::fireEvent('afterLoad');
        js_profilerMarker('Stop');

		// Get the document object.
		$document =& JFactory::getDocument();

		// Set the MIME type for JSON output.
		$document->setMimeEncoding( 'application/json' );

		// Change the suggested filename.
		JResponse::setHeader( 'Content-Disposition', 'attachment; filename="'.$this->getName().'.json"' );

		$test = new stdClass();
		$test->bla = "red";

		$test1 = new stdClass();
		$test1->blub = "blue";

		$test->test = $test1;


		$stop = JRequest::getVar('data');

		$test->dat = $stop;
		$test->dat2 = "$stop";
		$test->dat5 =$_POST['data'];
		$test->dat6 =$_GET['data'];
		$test->dat3 = '{"apple":"red","lemon":"yellow", "obj": {"banana":"red","suggar":"yellow"} }';


		// Output the JSON data.
		echo json_encode( $test );


		//echo '{"apple":"red","lemon":"yellow", "obj": {"banana":"red","suggar":"yellow"} }';
		//parent::display();
	}

    function performAnalytics()
    {

    }

    function performAnalyticsByHour($timestamp)
    {

    }

    function processByDays($timestamp)
    {

    }


    function summarizeVisits($time)
    {
        $end = $time + 3600;
        $query = "SELECT count() from #__jstats_visits where timestamp >= $time AND timestamp < $end";

        //todo perform

        $result = null;

        //todo write to DB cache
        store('visits', '3600', $time, $end, $result, $end < time() );
    }

    function store($value, $slot, $from, $to, $history = false)
    {

    }
}
 
