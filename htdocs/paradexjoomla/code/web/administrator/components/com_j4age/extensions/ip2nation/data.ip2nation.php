<?php
if( !defined( '_JS_STAND_ALONE' ) && !defined( '_JEXEC' ) )
{
	die( 'JS: No Direct Access to '.__FILE__ );
}

/**
 *  Handling for the IP2Nation Integration
 *
 *  Attention this list is unlike the IP2Nation SQL a presorted list by IP.
 */
function js_retrieveIP2NationInstallSteps( &$installer, &$plugin, $current_version )
{
    if(empty($current_version))
    {
        $current_version = "0.0.0";
    }
    
$JSDatabaseAccess = js_JSDatabaseAccess::getInstance();
    
$JSConf =& js_JSConf::getInstance();
$query = array();
$query[] = "CREATE TABLE IF NOT EXISTS `#__ip2nationCountries` (
              `code` VARCHAR(4) NOT NULL DEFAULT '' ,
              `iso_code_2` VARCHAR(2) NOT NULL DEFAULT '' ,
              `iso_code_3` VARCHAR(3) NULL DEFAULT '' ,
              `iso_country` VARCHAR(255) NOT NULL DEFAULT '' ,
              `country` VARCHAR(255) NOT NULL DEFAULT '' ,
              `lat` FLOAT NOT NULL DEFAULT '0' ,
              `lon` FLOAT NOT NULL DEFAULT '0' ,
              PRIMARY KEY (`code`) );
            ;
";

/*$query[] = "CREATE TABLE IF NOT EXISTS `#__ip2nation` (
              `temp_index` INT UNSIGNED NOT NULL auto_increment,
              `ip` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
              `ip_next` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
              `country` VARCHAR(4) NOT NULL default '',
              PRIMARY KEY (`temp_index`) )
            ;
"; */

$query[] = "CREATE TABLE IF NOT EXISTS `#__ip2nation` (
              `ip` INT(11) UNSIGNED NOT NULL DEFAULT 0 ,
              `country` VARCHAR(4) NOT NULL default '',
              KEY (`ip`) );
            ;
";

$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Create ip2nation tables');

$query = array();
$query[] = "TRUNCATE TABLE #__ip2nation";
$query[] = "TRUNCATE TABLE #__ip2nationCountries";

$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Truncate ip2nation tables');


$insertIndex = 1;

include_once( dirname( __FILE__ ) .DS. 'data.php' );

$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Fill ip2nation-countries Table');

$query = array();
$query[] = "UPDATE #__ip2nationCountries SET iso_code_2  = UPPER(code);";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Fix ip2nation bug');

/*
$query = array();
$query[] = "ALTER IGNORE TABLE `#__ip2nation` ADD `ip_next` INT(11) UNSIGNED NOT NULL DEFAULT 0;";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Set flag to finish the update');

$query = array();
$query[] = "UPDATE #__ip2nation AS A, #__ip2nation AS B SET A.ip_next = B.ip WHERE A.temp_index +1 = B.temp_index;";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Fill our helper column');
*/

if (js_JSUtil::JSVersionCompare( $current_version, '1.0.1', '<') == true)
{
    $query = array();
    $query[] = "UPDATE `#__jstats_ipaddresses` as a SET a.`code` = NULL WHERE a.ip = 2130706433;";
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Set code column for localhost entries to NULL otherwise entries are wrongly listed assigned to Japan');
}

/**
 * Update data only if the topleveldomain exists.
 *
 * We make sure to drop all entries, which do not exist in our ip2nation-countries table
 */
$query = array();
$columns = $JSDatabaseAccess->js_getTableColumns("#__jstats_ipaddresses");
if($JSDatabaseAccess->js_hasTableColumn($columns, "tld_id") )
{
    $query[] = "UPDATE `#__jstats_ipaddresses` as A SET A.`code` = NULL WHERE 1=1";
    $query[] = "UPDATE  `#__jstats_ipaddresses` as a, #__ip2nationCountries as i2nc, #__jstats_topleveldomains as t SET a.code = i2nc.code WHERE  a.tld_id = t.tld_id AND t.tld = i2nc.code;";
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Copy value from tld to country field');

    /**
    * Now only update the remaining entries, which are still NULL. If we would do it for all, this SQL script would take hours in large DBs
    */
    $query = array();
    $query[] = "UPDATE `#__jstats_ipaddresses` as a SET a.`code` =
                (SELECT i2n.country
                        FROM #__ip2nation as i2n
                        WHERE i2n.ip < a.ip
                        ORDER BY i2n.ip DESC
                        LIMIT 0,1) WHERE a.code IS NULL;";
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Update all unset values using the ip2nation');

    $query = array();
    $query[] = "DROP TABLE `#__jstats_topleveldomains`";
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Drop __jstats_topleveldomains');

    $query = array();
    $query[] = "UPDATE `#__jstats_ipaddresses` as a SET a.`code` = NULL WHERE a.ip = 2130706433;";
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Set code column for localhost entries to NULL otherwise entries are wrongly listed assigned to Japan');

    $query = array();
    $query[] = "ALTER TABLE `#__jstats_ipaddresses` DROP `tld_id`";
    $query[] = $JSConf->getSQL('ip2nation_db_installed', $plugin->BuildVersion);
    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Drop tld_id');
}

/*
$query = array();
$query[] = "UPDATE `#__jstats_ipaddresses` as A, #__ip2nation as B SET A.`country` = B.`country` WHERE  A.`IP` >= B.`IP` AND A.`IP` < B.`ip_next`;";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Set flag to finish the update');

$query = array();
$query[] = "ALTER TABLE `#__ip2nation` DROP temp_index;";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Drop primary key from ip2nation table');

$query = array();
$query[] = "ALTER TABLE `#__ip2nation` ADD PRIMARY KEY (`ip`);";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Drop primary key from ip2nation table');

$query = array();
$query[] = "ALTER IGNORE TABLE `#__ip2nation` DROP `ip_next`;";
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Drop primary key from ip2nation table');

*/


if (js_JSUtil::JSVersionCompare( $current_version, '1.0.3', '<') == true)
{
    $query =  array();
    $query[] = "UPDATE `#__jstats_ipaddresses` as A SET A.`code` = ( SELECT i2n.country FROM #__ip2nation AS i2n WHERE i2n.ip < A.ip ORDER BY i2n.ip DESC LIMIT 0,1 ) WHERE A.`code` IS NULL";


    $installer->appendSQLStep($JSConf->BuildVersion, $query, 'Update unidentified IPs');
    $query =  array();
}



$query = array();
$query[] = $JSConf->getSQL('ip2nation_db_installed', $plugin->BuildVersion);
$installer->appendSQLStep($JSConf->BuildVersion, $query, 'Set flag to finish the update');
    
}