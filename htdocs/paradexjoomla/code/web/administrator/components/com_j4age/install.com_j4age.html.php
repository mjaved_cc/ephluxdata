<?php

              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          

if( !defined( '_JS_STAND_ALONE' ) && !defined( '_JEXEC' ) )
{
	die( 'JS: No Direct Access to '.__FILE__ );
}

require_once( dirname(__FILE__) .DS. 'libraries'.DS. 'template.html.php' );
$JSConf = js_JSConf::getInstance();

$JSTemplate = new js_JSTemplate();

$introduce_msg  = '<img src="'. JURI::base(true) . '/components/com_j4age/images/icon-48-js_js-logo.png" width="48" height="48" alt="j4age" title="j4age" /><br clear="all" />';
 ?>
<div style="text-align: left;">
	<div style="width:95%; margin:5px auto 5px auto; padding:5px;">
		<table style="width: 100%; padding: 0px; border-width: 0px; border-collapse: collapse; /*not working in IE 6.0, 7.0 use cellspacing=0 */ border-spacing: 0px; /* no difference */" cellspacing="0">
		<tr>
			<td style="padding: 0px; text-align: left;"><?php echo $introduce_msg; ?></td>
			<td style="padding: 0px; text-align: right; vertical-align: top; font-weight: bold;">
				j4age version:&nbsp;'<?php echo $JSConf->BuildVersion; ?>'
			</td>
		</tr>
		</table>	
	    <div>
            <h3><?php echo JText::_( 'COM_J4AGE_YOUR_FEEDBACK' ); ?></h3>
            <?php echo JText::_( 'COM_J4AGE_NOTICE' ); ?>
	    </div>
		<div>
			<?php $JSTemplate->startBlock();
			if( count( $StatusTData->errorMsg ) > 0 )
            {
                echo "<h3 style='color:red'>".JText::_( 'COM_J4AGE_ERRORS')."</h3>";
                foreach($StatusTData->errorMsg as $msg)
                    echo "<h4>".$msg['name']."</h4>"."<p>".$msg['description']."</p>";
			}
            if( count( $StatusTData->warningMsg ) > 0 )
            {
                echo "<h3 style='color:orange'>".JText::_( 'COM_J4AGE_WARNINGS')."</h3>";
                foreach($StatusTData->warningMsg as $msg)
                echo "<h4>".$msg['name']."</h4>"."<p>".$msg['description']."</p>";
            }
            if( count( $StatusTData->infoMsg ) > 0 )
            {
                echo "<h3 style='color:blue'>".JText::_( 'COM_J4AGE_INFO')."</h3>";
                foreach($StatusTData->infoMsg as $msg)
                echo "<h4>".$msg['name']."</h4>"."<p>".$msg['description']."</p>";
            }
             $JSTemplate->endBlock();
            ?>
		</div>
	</div>
</div>
<div style="text-align:center; color:#9F9F9F; font-size:0.9em">
   <a href="http://www.ecomize.com" target="_blank" title="<?php echo JText::_( 'COM_J4AGE_VISITE_HOME_PAGE' ); ?>"><img src="<?php echo JURI::base(true);?>/components/com_j4age/images/logo.gif" alt="ecomize" title="<?php echo JText::_( 'COM_J4AGE_ECOMIZE' ); ?>" /></a>
</div><div style="text-align:center; color:#9F9F9F; font-size:0.9em">
   j4age &copy;2009-<?php echo js_gmdate( 'Y' ); ?> <a href="http://www.ecomize.com" target="_blank" title="<?php echo JText::_( 'COM_J4AGE_VISITE_HOME_PAGE' ); ?>"><?php echo JText::_( 'COM_J4AGE_ECOMIZE_AG' ); ?></a> - <?php echo JText::_( 'COM_J4AGE_ECOMIZE_AG_RIGHTS' ); ?><br />
    <P><a href="http://www.ecomize.com" target="_blank" title="<?php echo JText::_( 'COM_J4AGE_VISITE_HOME_PAGE' ); ?>">j4age</a>
    <?php echo JText::_( 'COM_J4AGE_LICENSE' ); ?></P>
</div>