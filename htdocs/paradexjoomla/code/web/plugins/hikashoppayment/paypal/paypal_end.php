<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><div class="hikashop_paypal_end" id="hikashop_paypal_end">
	<span id="hikashop_paypal_end_message" class="hikashop_paypal_end_message">
		<?php echo JText::sprintf('PLEASE_WAIT_BEFORE_REDIRECTION_TO_X', $method->payment_name) . '<br/>' . JText::_('CLICK_ON_BUTTON_IF_NOT_REDIRECTED'); ?>
	</span>
	<span id="hikashop_paypal_end_spinner" class="hikashop_paypal_end_spinner hikashop_checkout_end_spinner">
	</span>
	<br/>
	<form id="hikashop_paypal_form" name="hikashop_paypal_form" action="<?php echo $method->payment_params->url; ?>" method="post">
		<div id="hikashop_paypal_end_image" class="hikashop_paypal_end_image">
			<input id="hikashop_paypal_button" type="submit" class="btn btn-primary" value="<?php echo JText::_('PAY_NOW'); ?>" name="" alt="<?php echo JText::_('PAY_NOW'); ?>" />
		</div>
		<?php
		
		foreach ($vars as $name => $value) {
			if( $name!="amount_1" || $name!="item_name_1") {
			echo '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars((string) $value) . '" />';
			}
		}
		?>

		<?php
		$cartClass = hikashop_get('class.cart');
		$productsArray = $cartClass->get();		
		$count = 1;

		$db = JFactory::getDBO();
		foreach ($productsArray as $key => $value) {

			$product_id = $value->product_id;
			$quantity = $value->cart_product_quantity;
			$product_name = $value->product_name;

			$query = 'SELECT * FROM ' . hikashop_table('price') . ' WHERE price_product_id =' . $product_id;
			$db->setQuery($query);
			$paymentData = $db->loadObjectList('price_product_id');
			$price = $paymentData[$product_id]->price_value;
			$price =  round($price, 2); 
			echo '<input type="hidden" name="amount_'.$count.'" value="'.$price.'">';
			echo '<input type="hidden" name="item_name_'.$count.'" value="'.$product_name.'">';
			echo '<input type="hidden" name="quantity_'.$count.'" value="'.$quantity.'">';
			$count++;
		}
		?>

		<?php JRequest::setVar('noform', 1); ?>
	</form>
	<script type="text/javascript">
		<!--
		document.getElementById('hikashop_paypal_form').submit();
		//-->
	</script>
</div>
