<?php

              
/**
 * @package j4age
 * @copyright Copyright (C) 2009-@THISYEAR@ j4age Team. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 * Thank you to the project j4age and it's team, on which roots this project is build on.
 */
             
          

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Joomla! System j4age Plugin
 *
 * @package		Joomla
 * @subpackage	System
 */
class plgSystemj4age extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @access	protected
	 * @param	object	$subject The object to observe
	 * @param 	array   $config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	function plgSystemj4age(& $subject, $config) {
		parent::__construct($subject, $config);
	}

	function onAfterDispatch()//onAfterInitialise()
	{
        global $j4age_performed;
        $mainframe = JFactory::getApplication();
        if($j4age_performed == true)
        {
           return;
        }

		// No counting process for admin page
		if ($mainframe->isAdmin()) {
			return;
		}


        $js_PathToJoomlaStatsCountClasses = JPATH_ADMINISTRATOR .DS. 'components' .DS. 'com_j4age' .DS. 'libraries' .DS. 'count.classes.php';
        if ( !is_readable($js_PathToJoomlaStatsCountClasses) || !include_once($js_PathToJoomlaStatsCountClasses) ) {
            echo "<strong>j4age</strong> component required, but not installed";
            return false;
        }

        $JSCountVisitor = new js_JSCountVisitor();
        $js_visit_id = $JSCountVisitor->countVisitor( );

        $isJ4ageActivated = JRequest::getBool('isJ4ageActivated', false);

        if($isJ4ageActivated)
        {
            echo '<!-- j4ageActivated -->';
        }


        $j4age_performed = true;

        js_debugEcho();
	}

    //function onAfterDispatch()
    //{
        //todo store the page visit here and not onAfterInitialise as we have only the page title now using Joomla 1.6
    //}
}