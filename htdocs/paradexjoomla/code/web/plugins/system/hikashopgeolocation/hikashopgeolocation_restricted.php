<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><html>
	<head>
		<?php
		$doc = JFactory::getDocument();
		$head = $doc->loadRenderer('head');
		echo $head->render(); ?>
	</head>
	<body>
		<?php echo hikashop_display(JText::_('RESSOURCE_NOT_ALLOWED'),'error'); ?>
	</body>
</html>
<?php exit;
