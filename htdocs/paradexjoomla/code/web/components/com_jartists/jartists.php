<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



defined('_JEXEC') or die;



// Include dependancies

jimport('joomla.application.component.controller');

$document = &JFactory::getDocument();

$document->addStyleSheet('components/com_jartists/assets/css/main.css');

require_once(JPATH_ROOT.'/components/com_jartists/libraries/libraries.php');

// Execute the task.

$controller	= JController::getInstance('Jartists');

$controller->execute(JRequest::getVar('task',''));

$controller->redirect();

