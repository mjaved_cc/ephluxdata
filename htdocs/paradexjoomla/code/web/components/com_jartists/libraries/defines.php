<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */

 

// No direct access

defined('_JEXEC') or die;



define('LBLM_SITE',		 	JPATH_ROOT.'/components/com_jartists');

define('LBLM_ADMIN',	 	JPATH_ADMINISTRATOR.'/components/com_jartists');

define('LBLM_LIBRARIES', 	LBLM_SITE . '/libraries');

define('LBLM_ASSETS', 	JURI::root(). 'components'.DS.'com_jartists'.DS.'assets'.DS);

define('LBLM_IMAGES', 	'components'.DS.'com_jartists'.DS.'images'.DS);
define('LBLM_IMAGE_LG', JURI::root(). 'images'.DS.'jartists'.DS.'lg'.DS);
define('LBLM_IMAGE_S', 	JURI::root(). 'images'.DS.'jartists'.DS.'s'.DS);
define('LBLM_IMAGE_T', 	JURI::root(). 'images'.DS.'jartists'.DS.'thumbs'.DS);
define('LBLM_IMAGE_M', 	JURI::root(). 'images'.DS.'jartists'.DS.'med'.DS);
define('LBLM_ICONS', 	JURI::base(). 'components'.DS.'com_jartists'.DS.'assets'.DS.'icons'.DS);