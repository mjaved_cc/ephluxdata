<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */





// no direct access

defined('_JEXEC') or die;

?>
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>


<?php if($this->items) : ?>
 <div class="discography_list">
    <?php foreach ($this->items as $item) :?>
 <div class="disc_item"> <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$item->slug); ?>"><img src="<?php echo LBLM_IMAGE_T.$item->cover; ?>" class="jartists-150" /></a>

                 <span class="title"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$item->slug); ?>"><?php echo $item->title; ?></a></span>
                 <span class="artist_title"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&id='.$item->artist_slug); ?>"><?php echo $item->artist; ?></a></span>
                 </div>
    <?php endforeach; ?>

            
			<div class="clr"></div>
            </div>

     <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>