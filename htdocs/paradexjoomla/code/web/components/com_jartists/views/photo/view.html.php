<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// No direct access

defined('_JEXEC') or die;



jimport('joomla.application.component.view');

/**

 * View to edit

 */
class jartistsViewphoto extends JView {

	protected $state;
	protected $item;
	protected $params;

	/**

	 * Display the view

	 */
	public function display($tpl = null) {



		$app = JFactory::getApplication();
		$dispatcher = JDispatcher::getInstance();
		$this->state = $this->get('State');

		$this->item = $this->get('Item');

		$this->params = $app->getParams('com_jartists');

		$this->other_images = $this->getOthers($this->item);


		JPluginHelper::importPlugin('jartists');
		$this->item->description = JHTML::_('content.prepare', $this->item->description);
		$this->item->event = new stdClass();
		$results = $dispatcher->trigger('onJArtistsAfterTitle', array($this->item, &$this->params));
		$this->item->event->onJArtistsAfterTitle = trim(implode("\n", $results));


		$results = $dispatcher->trigger('onJArtistsAfterDisplay', array($this->item, &$this->params));
		$this->item->event->onJArtistsAfterDisplay = trim(implode("\n", $results));
		// Check for errors.

		if (count($errors = $this->get('Errors'))) {

			JError::raiseError(500, implode("\n", $errors));

			return false;
		}

		$model = $this->getModel();
		$model->hit();



		$this->_prepareDocument();



		parent::display($tpl);
	}

	function getOthers($data) {

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);

		// Select the required fields from the table.

		$query->select('a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

		$query->from('`#__jartists_photos` AS a');

		$query->where('a.album_id = ' . (int) $data->album_id);

		$query->where('a.id <> ' . (int) $data->id);
		
		$query->where('a.image_file NOT like "%.mp3%"');

		$db->setQuery($query);

		$rows = $db->loadObjectList();

		return $rows;
	}

	/**

	 * Prepares the document

	 */
	protected function _prepareDocument() {

		$app = JFactory::getApplication();

		$menus = $app->getMenu();

		$title = null;



		// Because the application sets a default page title,
		// we need to get it from the menu item itself

		$menu = $menus->getActive();

		if ($menu) {

			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {

			$this->params->def('page_heading', JText::_('COM_JARTISTS_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title)) {

			$title = $app->getCfg('sitename');
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {

			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {

			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}

		$this->document->setTitle($title);



		if ($this->params->get('menu-meta_description')) {

			$this->document->setDescription($this->params->get('menu-meta_description'));
		}



		if ($this->params->get('menu-meta_keywords')) {

			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}



		if ($this->params->get('robots')) {

			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

}

