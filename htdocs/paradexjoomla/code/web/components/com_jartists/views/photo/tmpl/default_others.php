<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->other_images): ?>

        <?php foreach($this->other_images as $image): ?>

            <div class="carouselImages">

                 <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=photo&id=' . $image->slug); ?>"><img src="<?php echo LBLM_IMAGE_T.$image->image_file; ?>" class="jartists-100" /></a>

            </div>

        <?php endforeach; ?>	

        <div class="clr"></div>

<?php endif; ?>