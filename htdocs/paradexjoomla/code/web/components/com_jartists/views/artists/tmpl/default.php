<?php
/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// no direct access
ini_set('memory_limit', '120M');
include('simple_html_dom.php');
defined('_JEXEC') or die;
?>
<script>
document.title = "Artist & Quran Reciters";
</script>
<div class="lblArtist">

	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>

	<?php if ($this->items) : ?>
		<div id="system">
			<article class="item">
				<div class="content clearfix">


					<?php foreach ($this->items as $item) : ?>
					
					
						<?php $item->biography = JHTML::_('content.prepare', $item->biography); ?>


						<div class="gallery-column-4 gallery-1st gallery-column"><a  href="<?php echo JRoute::_('index.php?option=com_jartists&view=artist&id=' . $item->slug); ?>"> <img width="220px" height="145" border="0" alt="Spotlight Image" src="<?php echo LBLM_IMAGE_S . $item->profile_picture; ?>"></a>
							<span class="gallery-des">		
								<?php
								$html = str_get_html($item->biography);
								//echo $html->find('#profile_text', 1)->innertext;

								$player = '';

								foreach ($html->find('div#single-player') as $e)
									$player = $e->innertext;

//								foreach ($html->find('div#profile_text') as $e)
//									echo substr($e->innertext, 0, 250) . ' ... <br>';
								
								echo '<h3>'.$item->title.'</h3>';

								echo '<div id="player">' . $player . '</div>';

								echo '<div class="read_more"><a href="' . JRoute::_('index.php?option=com_jartists&view=artist&id=' . $item->slug) . '"><h3 style="text-align:right;">More...</h3></a></div>';

								//echo substr($item->biography, strrpos($item->biography, '}') + 1, 800); 
								?>
							</span>
						</div>






					<?php endforeach; ?>
				</div>
			</article>

		</div>

		<div class="clr"></div>





		<div class="pagination">

			<?php if ($this->params->def('show_pagination_results', 1)) : ?>

				<p class="counter">

					<?php echo $this->pagination->getPagesCounter(); ?>

				</p>

			<?php endif; ?>

			<?php echo $this->pagination->getPagesLinks(); ?>

		</div>





	<?php endif; ?>

</div>