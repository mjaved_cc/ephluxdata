<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// No direct access

defined('_JEXEC') or die;



jimport('joomla.application.component.view');



/**

 * View class for a list of Jartists.

 */

class JartistsViewNews extends JView

{

	protected $items;

	protected $pagination;

	protected $state;

    protected $params;



	/**

	 * Display the view

	 */

	public function display($tpl = null)

	{

        $app                = JFactory::getApplication();
		$this->state		= $this->get('State');

		$items				= $this->get('Items');

		$this->pagination	= $this->get('Pagination');

        $this->params       = $app->getParams('com_jartists');

		for ($i = 0, $n = count($items); $i < $n; $i++)
		{
			$item = &$items[$i];
			$item->slug = $item->alias ? ($item->id . ':' . $item->alias) : $item->id;

			$item->event = new stdClass();

			$dispatcher = JDispatcher::getInstance();

			$item->content = JHtml::_('content.prepare', $item->content);

			$results = $dispatcher->trigger('onJArtistsAfterTitle', array(&$item, &$this->params));
			$item->event->onJArtistsAfterTitle = trim(implode("\n", $results));

			$results = $dispatcher->trigger('onJArtistsAfterDisplay',array(&$item, &$this->params));
			$item->event->onJArtistsAfterDisplay = trim(implode("\n", $results));
		}

		// Check for errors.

		if (count($errors = $this->get('Errors'))) {

			JError::raiseError(500, implode("\n", $errors));

			return false;

		}


		$this->assignRef('items', $items);
        $this->_prepareDocument();

        

		parent::display($tpl);

	}





	/**

	 * Prepares the document

	 */

	protected function _prepareDocument()

	{

		$app	= JFactory::getApplication();

		$menus	= $app->getMenu();

		$title	= null;



		// Because the application sets a default page title,

		// we need to get it from the menu item itself

		$menu = $menus->getActive();

		if($menu)

		{

			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));

		} else {

			$this->params->def('page_heading', JText::_('COM_JARTISTS_DEFAULT_NEWS_PAGE_TITLE'));

		}

		$title = $this->params->get('page_title', '');

		if (empty($title)) {

			$title = $app->getCfg('sitename');

		}

		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {

			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);

		}

		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {

			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));

		}

		$this->document->setTitle($title);



		if ($this->params->get('menu-meta_description'))

		{

			$this->document->setDescription($this->params->get('menu-meta_description'));

		}



		if ($this->params->get('menu-meta_keywords'))

		{

			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));

		}



		if ($this->params->get('robots'))

		{

			$this->document->setMetadata('robots', $this->params->get('robots'));

		}

	}    

    	

}

