<?php
/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// no direct access

defined('_JEXEC') or die;
?>

<?php if ($this->photos): ?>

	<div class="display_row" id="left">

		<ul>

	<?php foreach ($this->photos as $image):  ?>

			<?php $ext = pathinfo($image->image_file, PATHINFO_EXTENSION);  
						if( $ext!='mp3') {  ?>
				<li>

					<a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=photo&id=' . $image->slug); ?>"><img src="<?php echo LBLM_IMAGE_T . $image->image_file; ?>" class="jartists-100" /></a>

				</li>

	<?php } endforeach; ?>

	        <div class="clr"></div>	

		</ul>



		<a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=photos&id=' . $this->item->slug); ?>" class="read-more button-highlight left-5"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

		<div class="clr"></div>
	</div>

<?php endif; ?>

<!--<ul class="social-icons" id="socialicons">
	<li><a href="#" title="Facebook"><img src="images/social/facebook.png" border="0" /></a></li>
	<li><a href="#" title="Website"><img src="images/social/web.png" border="0" /></a></li>
	<li><a href="index.php/contacts" title="Contact"><img src="images/social/contact.png" border="0" /></a></li>
	<li><a href="index.php/booking-form?param=Abdullah Rolle&amp;img=images/rolle2.jpg" title="Booking"><img src="images/social/booking.png" border="0" /></a></li>
	<li><a href="http://youtu.be/bDzYU3fL1X8" title="Youtube"><img src="images/social/youtube.png" border="0" /></a></li>
</ul>-->