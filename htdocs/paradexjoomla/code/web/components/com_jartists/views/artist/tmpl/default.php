<?php
/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// no direct access
defined('_JEXEC') or die;
ini_set('memory_limit', '120M');
include('simple_html_dom.php');
?>
<!--<h1><?php // echo JText::_('COM_JARTISTS_DEFAULT_BIOGRAPHY_PAGE_TITLE');         ?></h1>-->

<?php // echo $this->pageMenu(); ?>

<div>
	<?php // echo $this->item->biography;	print_r($this->item); ?>


	<h1><?php echo $this->item->title ?></h1>
	<div class="grid-box width100">
		<div class="about-variation1-style about_image">
			<img width="180" alt="Spotlight Image" src="<?php echo LBLM_IMAGE_S . $this->item->profile_picture; ?>">
				<?php
				$html = str_get_html($this->item->biography);

				foreach ($html->find('div#location') as $e)
					echo '<div>' . $e->innertext . '</div>';

				$artist_id = $this->item->id;
				$alias = $this->item->alias;
				$filename = $alias . '.xml';
				//$filename = 'abc.xml';
				$mp3_files = getmp3($artist_id);

				$xml = '';
				$xml = '<?xml version="1.0" encoding="UTF-8"?>';
				$xml .= '<playlist version="1" xmlns="http://xspf.org/ns/0/">';
				$xml .= '<title>' . $this->item->title . '</title>';
				$xml .= '<creator>' . $this->item->title . '</creator>';
				$xml .= '<link>http://www.paradex.co.uk/</link>';
				$xml .= '<info>Playlist</info>';
				$xml .= '<trackList>';

				$path = dirname($_SERVER['PHP_SELF']);
				$base_bath = explode("/",$path);
			



				foreach ($mp3_files as $item) {

					$xml .= '<track>';
					$xml .= '<location>'.JURI::root( true ).'/images/gallery_' . $artist_id . '/' . $item->image_file . '</location>';
					$xml .= '<creator>' . $item->title . '</creator>';
					$xml .= '<album>' . $item->title . '</album>';


					if (!empty($item->description)) {
						$descrip = strip_tags($item->description);
					} else {
						$descrip = 'Untitled';
					}

					$xml .= '<title>' . $descrip . '</title>';
					$xml .= '</track>';
				}

				$xml .= '</trackList>';
				$xml .= '</playlist>';


				$file_path = 'images/' . $filename;

				$fh = fopen($file_path, 'w') or die("can't open file");
				fwrite($fh, $xml);
				fclose($fh);
				?>




<?php
echo JHTML::_('content.prepare', '{play}' . $file_path . '{/play}');
//				foreach ($html->find('div#player') as $e)
//					echo '<div>' . $e->innertext . '</div>';
foreach ($html->find('div#buy') as $e)
	echo '<div id="buy">' . $e->innertext . '</div>';
?>
		</div>
		<div class="bfc-o padding-25">
<?php
foreach ($html->find('div#profile_text') as $e)
	echo $e->innertext;
?>
			<?php // echo $this->item->biography ?>


<?php echo $this->loadTemplate('photos'); ?>

			<?php
			foreach ($html->find('div#icons') as $e)
				echo $e->innertext;

			foreach ($html->find('div#artist_hidden_form') as $e)
				echo $e->innertext;
			?>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
			</script>
			<script>
				$(document).ready(function(){
					$("#artist_form").attr("action", "<?php echo $this->baseurl; ?>/index.php/component/hikashop/product/listing?Itemid=523");
					$("#buy").click(function(){
						$("#artist_form").submit();
					});
				});
			</script>

		</div>
	</div>
<?php // echo $this->loadTemplate('videos');  ?>
</div>



<?php

function getmp3($artist_id) {

	$db = JFactory::getDBO();

	$query = $db->getQuery(true);

	// Select the required fields from the table.

	$query->select('a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

	$query->from('`#__jartists_photos` AS a');


	$query->where('a.image_file like "%.mp3%" AND a.album_id =' . $artist_id);

	$db->setQuery($query);

	$rows = $db->loadObjectList();

	return $rows;
}
?>