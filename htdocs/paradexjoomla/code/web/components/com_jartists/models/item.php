<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// No direct access.

defined('_JEXEC') or die;



jimport('joomla.application.component.modelitem');



/**

 * Jartists model.

 */

class JartistsModelItem extends JModelItem

{

	/**

	 * Method to auto-populate the model state.

	 *

	 * Note. Calling getState in this method will result in recursion.

	 *

	 * @since	1.6

	 */

	protected function populateState()

	{

		$app = JFactory::getApplication('site');



		// Load state from the request.

		$pk = JRequest::getInt('id');

		$this->setState('news.id', $pk);



		$offset = JRequest::getUInt('limitstart');

		$this->setState('list.offset', $offset);



		// Load the parameters.

		$params = $app->getParams();

		$this->setState('params', $params);



		// TODO: Tune these values based on other permissions.

		$user		= JFactory::getUser();

		if ((!$user->authorise('core.edit.state', 'com_jartists')) &&  (!$user->authorise('core.edit', 'com_jartists'))){

			$this->setState('filter.published', 1);

			$this->setState('filter.archived', 2);

		}

	}

        

	/**

	 * Method to get a single record.

	 *

	 * @param	integer	The id of the primary key.

	 *

	 * @return	mixed	Object on success, false on failure.

	 * @since	1.6

	 */

	public function &getItem($pk = null)

	{

		// Initialise variables.

		$pk = (!empty($pk)) ? $pk : (int) $this->getState('news.id');



		if ($this->_item === null) {

			$this->_item = array();

		}



		if (!isset($this->_item[$pk])) {



                        $db = $this->getDbo();

                        $query = $db->getQuery(true);



                        $query->select($this->getState(

                                'item.select', 'a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug'

                                )

                        );

                        $query->from('#__jartists_news AS a');

                        // Join over the users for the checked out user.

						$query->select('s.title AS artist');
			$query->select('CASE WHEN CHAR_LENGTH(s.alias) THEN CONCAT_WS(":", s.id, s.alias) ELSE s.id END as artist_slug');
						$query->join('LEFT', '#__jartists_artists AS s ON s.id=a.artist_id');

                        $query->where('a.id = '. (int) $pk);



                        // Filter by published state.

                        $published = $this->getState('filter.published');

                        $archived = $this->getState('filter.archived');



                        if (is_numeric($published)) {

                                $query->where('(a.state = ' . (int) $published . ' OR a.state =' . (int) $archived . ')');

                        }



                        $db->setQuery($query);



                        $data = $db->loadObject();



                        if ($error = $db->getErrorMsg()) {

                                JError::raiseError(404, $error);

                                return false;

                        }



                        $this->_item[$pk] = $data;

			

		}



		return $this->_item[$pk];

	}

	public function hit($pk = 0)
	{
            $hitcount = JRequest::getInt('hitcount', 1);

            if ($hitcount)
            {
                // Initialise variables.
                $pk = (!empty($pk)) ? $pk : (int) $this->getState('news.id');
                $db = $this->getDbo();

                $db->setQuery(
                        'UPDATE #__jartists_news' .
                        ' SET hits = hits + 1' .
                        ' WHERE id = '.(int) $pk
                );

                if (!$db->query()) {
                        $this->setError($db->getErrorMsg());
                        return false;
                }
            }

            return true;
	}

}