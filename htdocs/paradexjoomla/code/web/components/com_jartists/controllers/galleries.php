<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// No direct access.

defined('_JEXEC') or die;



jimport('joomla.application.component.controller');



/**

 * Galleries list controller class.

 */

class JartistsControllerGalleries extends JController

{

	/**

	 * Proxy for getModel.

	 * @since	1.6

	 */

	public function &getModel($name = 'Galleries', $prefix = 'JartistsModel')

	{

		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;

	}

}