<?php

/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
  $("#system-message-container").addClass("green_notification");
  //$("#system-message dd").removeClass("message");
  
});
</script>
<?php $app = JFactory::getApplication();  ?>

	
		<?php $app->enqueueMessage(JText::_('THANK_YOU_FOR_PURCHASE')); ?>
<?php
$order_id = $_GET['order_id'];
$db = JFactory::getDBO();
$query = 'SELECT * FROM ' . hikashop_table('order_product') . ' WHERE order_id =' . $order_id;
$db->setQuery($query);
$orderData = $db->loadObjectList();

$total_item_price= '';

echo '<div class="hikashop_products_table">';
echo '<table width="100%" class="cozy_table">';
echo '<tr>';
echo '<th>';
echo 'Product';
echo '</th>';
echo '<th>';
echo 'Price';
echo '</th>';
echo '<th>';
echo 'Quantity';
echo '</th>';
echo '</tr>';
foreach ($orderData as $item) {
	echo '<tr>';
	echo '<td>';
	echo $item->order_product_name;
	echo '<br/>';
	echo '</td>';
	echo '<td>';
	echo round($item->order_product_price,2);
	$total_item_price = $item->order_product_price * $item->order_product_quantity + $total_item_price;
	echo '<br/>';
	echo '</td>';
	echo '<td>';
	echo $item->order_product_quantity;
	echo '<br/>';
	echo '</td>';
	echo '</tr>';
}
	echo '<tr class="total_cost">';
		echo '<td>';
		echo '</td>';
		echo '<td><strong>Total</strong></td>';
		echo '<td>';
		echo $total_item_price;
		echo '</td>';
	echo '<tr>';
echo '</table>';
echo '</div>';
?>
