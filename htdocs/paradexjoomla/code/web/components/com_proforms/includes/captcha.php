<?php
/**
 * @name MOOJ Proforms
 * @version 1.3
 * @package proforms
 * @copyright Copyright (C) 2008-2012 Mad4Media. All rights reserved.
 * @author Dipl. Inf.(FH) Fahrettin Kutyol
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Please note that some Javascript files are not under GNU/GPL License.
 * These files are under the mad4media license
 * They may edited and used infinitely but may not repuplished or redistributed.
 * For more information read the header notice of the js files.
 **/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class MCaptcha{
	var $align = "center";
	var $type = "RECAPTCHA";
	var $isCaptcha = 0;
	var $submitText, $resetText;
	var $useReset = 1;
	public function __construct($customize = null){
		
		// Get document
		$document=& JFactory::getDocument();
		// include the captcha CSS
		$document->addStyleSheet(M4J_CSS_CAPTCHA);
		
		
		if($customize && isset($customize->submit_align)){
			switch ($customize->submit_align){
				default:
				case 0: 
					$this->align = "center";
				break; 
				
				case 1: 
					$this->align = "left";
				break;
				
				case 2:
					$this->align = "right";
				break;
			}
		}
		
		if(M4J_IS_CAPTCHA && ! $GLOBALS["proforms_is_human"]){
			$this->isCaptcha = 1;
			$this->type = M4J_CAPTCHA;
			if($this->type == "CSS") $this->type = "SIMPLE";
			if(! file_exists(M4J_INCLUDE_CAPTCHA_TEMPLATES . strtolower($this->type) . ".php")) {
				$this->isCaptcha = 0;
				$this->type = "NOCAPTCHA";
			}
		}else{
			$this->isCaptcha = 0;
			$this->type = "NOCAPTCHA";
		}
		
		$this->submitText = (isset($customize->submit_text) && $customize->submit_text) ? trim($customize->submit_text) : M4J_LANG_SUBMIT;
		$this->resetText = (isset($customize->reset_text) && $customize->reset_text) ? trim($customize->reset_text) : M4J_LANG_RESET;
		$this->submitText = str_replace('"', '“', $this->submitText );
		$this->resetText = str_replace('"', '“', $this->resetText );
		if(isset($customize->use_reset)) $this->useReset = (int) $customize->use_reset;
	}//EOF construct
	
	public function render(){ 
		ob_start();
		include M4J_INCLUDE_CAPTCHA_TEMPLATES . strtolower($this->type) . ".php"; 
		$buffer = ob_get_clean();
		echo $buffer;
	}
	
	
}