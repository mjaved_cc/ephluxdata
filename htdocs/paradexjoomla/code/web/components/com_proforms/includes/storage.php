<?php
/**
* @name MOOJ Proforms 
* @version 1.0
* @package proforms
* @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
* @author Dipl. Inf.(FH) Fahrettin Kutyol
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* Please note that some Javascript files are not under GNU/GPL License.
* These files are under the mad4media license
* They may edited and used infinitely but may not repuplished or redistributed.  
* For more information read the header notice of the js files.
**/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );


class Storage{

	var $jid = null;
	var $fids = null;
	var $fid = array();
	var $rootDir = M4J_TMP;
	var $tmpDir = null;
	var $userEmail = null;
	var $optInIdent = null;
	var $insertID = null;
	var $lookUp = array();
	var $aliasLookUp = array();
	var $alias2id = array();
	var $stid = null;
	
	function Storage($jid=null,$fid){
		$this->tmpDir = md5(uniqid("",true));
		$this->jid = (int) $jid;
		$this->fids = $fid;
				
		$fids = explode(";",$fid);
		foreach($fids as $f){
			$this->fid[intval($f)] = array();
		}
	}

	function add($fid = null, $eid=null,$content=null,$alias = null){
		if(is_array($content)){
			$content = implode("\n",$content);
		}
		$this->fid[$fid][$eid] = $content;
		$this->lookUp[$eid] = & $this->fid[$fid][$eid];
		if($alias){
			$alias = md5($alias);
			$this->aliasLookUp[$alias] = & $this->fid[$fid][$eid];
			$this->alias2id[$alias] = $eid;
		}
	}
	
	function & get($fid = null, $eid=null){
		return $this->fid[$fid][$eid];
	}
	/**
	 * 
	 * @param int $fid
	 * @param int $eid
	 * @param Array|string $content
	 * 
	 * @deprecated 
	 */
	function set($fid = null, $eid=null, $content = null){
		if(is_array($content)){
			$content = implode("\n",$content);
		}
		$this->fid[$fid][$eid] = $content;
	}

	/**
	 * 
	 * @param int $eid
	 * @param Array|string $content
	 */
	function setById($eid = null, $content=null){
		if(is_array($content)){
			$content = implode("\n",$content);
		}
		if(isset($this->lookUp[$eid])){
			$this->lookUp[$eid] = $content;
		}
	}
	
	/**
	 *
	 * @param string $alias
	 * @param Array|string $content
	 */
	function setByAlias($alias = null, $content=null){
		$alias = md5($alias);
		if(is_array($content)){
			$content = implode("\n",$content);
		}
		if(isset($this->aliasLookUp[$alias])){
			$this->aliasLookUp[$alias] = $content;
		}
	}
	
	function getIdByAlias($alias = null){
		$alias = md5($alias);
		return isset($this->alias2id[$alias]) ? (int) $this->alias2id[$alias] : false;
	}
			
	function setRootDir($rootDir = M4J_TMP){
		$this->rootDir = $rootDir;		
	}
	
	function setStid($stid = null){
		$this->stid = $stid;
	}
	
	function addUserEmail($email){
		global $validate;

		if($validate->email($email)){
			$this->userEmail = $email;
			return true;
		} else{
			return false;
		}
	}

	function addTempDir($tempDir=null){
		$this->tmpDir = $tempDir;
	}
	
	
	function getDir(){
		return $this->rootDir . $this->tmpDir;
	}
	
	function replaceByAlias($string = null, $escape = 0){
		global $database;
		
	$user =& JFactory::getUser();
	$ip = strip_tags( array_key_exists('REMOTE_ADDR',$_SERVER) ? $_SERVER['REMOTE_ADDR'] : "") ;
		if(! preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/",$ip)){
			 	$ip = "UNKNOWN";
	 }
	$username = $user->username ? $user->username : "Guest";
	$userrealname = $user->name ? $user->name : "Guest"; 

	$string = preg_replace('/{(\s*)J_OPT_IN(\s*)}/' , "" , $string);
	$string = preg_replace('/{(\s*)J_OPT_OUT(\s*)}/' , "" , $string);
	$string = preg_replace('/{(\s*)J_USER_IP(\s*)}/' , $ip , $string);
	$string = preg_replace('/{(\s*)J_USER_NAME(\s*)}/' , $username , $string);
	$string = preg_replace('/{(\s*)J_USER_REALNAME(\s*)}/' , $userrealname , $string);
		
		foreach($this->fid as $element){			
			foreach($element AS $key => $value){
				$database->setQuery("SELECT `alias` FROM #__m4j_formelements WHERE `eid` = '".(int) $key."' LIMIT 1");
				$el = $database->loadObject();
				if($el){
					$value = $escape ? dbEscape($value) : $value;
					$string = preg_replace('/{(\s*)'.trim($el->alias).'(\s*)}/' , $value , $string);
				}//EOF if
				else{
					$string .= "<br />EID: " . $key . "has no Alias";
				}
			}// foreach key value
		}//EOF foreach
		return $string;
	}

}


?>