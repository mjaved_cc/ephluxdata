<?php
/**
 * @name MOOJ Proforms
 * @version 1.3
 * @package proforms
 * @copyright Copyright (C) 2008-2012 Mad4Media. All rights reserved.
 * @author Dipl. Inf.(FH) Fahrettin Kutyol
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Please note that some Javascript files are not under GNU/GPL License.
 * These files are under the mad4media license
 * They may edited and used infinitely but may not repuplished or redistributed.
 * For more information read the header notice of the js files.
 **/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class MSelections{
	protected  $eid, $fid, $required , $active, $form, $parameters, $options, $alias;
	
	protected $values = array(), $sendValues = array();
	protected $element_rows = 3, $width = "100%", $alignment = 1, $use_values = 0;
	protected $pleaseSelectOption = M4J_LANG_PLEASE_SELECT;
	protected $options_data_type = 0;
	protected $sql = null;
	
	protected $buffer = "";
	
	public function __construct($data = null, $sendValues = null){
		$this->eid = $data->eid;
		$this->fid = $data->fid;
		$this->required = $data->required;
		$this->active = $data->active;
		$this->form = $data->form;
		$this->parameters = $data->parameters;
		$params = parameters($this->parameters);
		foreach ($params as $key => & $value){
			if($key == "pleaseSelectOption"){
				$text = base64_decode($value);
				if(trim($text)) $this->pleaseSelectOption = $text;
			}else $this->$key = $value;
		}
		if($this->sql){
			$this->sql = base64_decode($this->sql);
			$this->sql = preg_replace("(\r\n|\n|\r)", "", $this->sql);
		}
		
		if($this->options_data_type && $this->sql){
			MDebug::pre("INIT SQL". $data->question);
			$db = & JFactory::getDbo();
			$db->setQuery(dbEscape($this->sql));
			$rows = $db->loadObjectList();
			$this->options = array();
			foreach($rows as & $row){
				$text = isset($row->text) ? str_replace('"','“',strip_tags(trim($row->text)) ): null;
				$value = isset($row->value) ? str_replace('"','“',strip_tags(trim($row->value)) ): null;
				$value = (!$value && $text) ? $text : $value;
				if($text && $value){
					array_push($this->options, $text);
					array_push($this->values, $value);
				}
			}
			
			
		}else{
			$options = explode("\n", $data->options);
			if(sizeof($options) == 1){
				$this->use_values = 0;
				$this->options = explode(";", $options[0]);
			}elseif(sizeof($options) == 2){
				$this->options = explode(";", $options[0]);
				$vals = explode(";", $options[1]);
				$isEmpty = 1;
				foreach($vals as &$val){
					if($val){
						$isEmpty = 0;
						break;
					}
				}//EOF foreach
					
				if($isEmpty){
					$this->use_values = 0;
				}else{
					$this->values = $vals;
				}
			}//EOF values existing
		}
		
		
		
		
		$this->alias = $data->alias;
		$this->sendValues = $sendValues ? $sendValues : null;
		
		$this->options($this->form, $this->eid, $this->options, $this->element_rows, $this->width, $this->alignment, $this->required, $this->values, $this->use_values);
	
	}
	
	
	
	function options($form,$eid,$options=null,$element_rows=3,$width='100%',$alignment=1, $required = 0,$values = null, $use_values = 0 )
	{
		$out=null;
		if($width==null || intval($width)==0) $width='100%';
		$add='';
		if( substr($width, -1, 1)=='%') $add='%';
		else $add='px';
		$width = intval($width);
		if ($width == 0 || $width == NULL) $width = "100%";
		else $width .= $add;
	
		$eval = ($required != 0)? 'lang="1000" ' : 'lang="0" ';
			
		if($options!=null)
		{
			$option = $this->options;
			$value = $use_values ? $this->values : $option;
	
			$count = sizeof($option);
	
			switch($form)
			{
				case 30:
					$out .= '<div class="m4jFormElementWrap">'."\n";
					$out .= '<select '.$eval.'class="m4jSelection" id="m4j-'.$eid.'" name="m4j-'.$eid.'" style="width: '.$width.';" >'."\n".
							'<option value="">'.$this->pleaseSelectOption.'</option>'."\n";
					for($t=0;$t<$count;$t++){
						if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
						$isSelected = $this->is_selected($this->sendValues, $value[$t], 30);
						$out .="\t".'<option value="'.$value[$t].'" '.$isSelected.'>'.$option[$t].'</option>'."\n";
					}
					$out .='</select>'."\n".'</div>'."\n";
					break;
							
				case 31:
							if($alignment==1)
							{
								$out .= '<div '.$eval.'class="m4jRadioWrap">'."\n".'<table style="width: '.$width.';" >'."\n".'<tbody>'."\n";
								for($t=0;$t<$count;$t++){
									if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
									$isSelected = $this->is_selected($this->sendValues, $value[$t], 31);
									$out .= '<tr>'."\n".'<td align="left" valign="top">'."\n".
									'<div class="m4jSelectItem m4jSelectItemVertical">'."\n".
									'<input class="m4jRadio" type="radio" id="m4j-'.$eid.'-'.$t.'" name="m4j-'.$eid.'" value="'.$value[$t].'" '.$isSelected.'></input>'.$option[$t]."\n".
									'</div>'."\n".
									'</td>'."\n".'</tr>'."\n";
								}
								$out .='</tbody>'."\n".'</table>'."\n".'</div>'."\n";
							}
							else
							{
								$out .= '<div '.$eval.'class="m4jRadioWrap">'."\n".'<div style="width:'.$width.';">'."\n";
								for($t=0;$t<$count;$t++){
									if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
									$isSelected = $this->is_selected($this->sendValues, $value[$t], 31);
									$out .= '<div class="m4jSelectItem">'."\n".
									'<input class="m4jRadio" type="radio" id="m4j-'.$eid.'-'.$t.'" name="m4j-'.$eid.'" value="'.$value[$t].'" '.$isSelected.'></input>'.$option[$t]."\n".
									'</div>'."\n";
								}
								$out .= '</div>'."\n".'</div>'."\n";
							}
							break;
								
				case 32:
					$out .= '<select '.$eval.'class="m4jSelection" id="m4j-'.$eid.'" name="m4j-'.$eid.'" size="'.$element_rows.'" style="width: '.$width.';">'."\n";
					for($t=0;$t<$count;$t++){
						if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
						$isSelected = $this->is_selected($this->sendValues, $value[$t], 32);
						$out .='<option value="'.$value[$t].'" '.$isSelected.'>'.$option[$t].'</option>'."\n";
					}
					$out .='</select>'."\n";
					break;
							
				case 33:
						if(! is_array($this->sendValues)) $this->sendValues = array();
							if($alignment==1)
							{
								$out .= '<div '.$eval.'class="m4jCheckboxWrap">'."\n".'<table style="width: '.$width.';" >'."\n".'<tbody>'."\n";
	
								for($t=0;$t<$count;$t++){
									if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
									$isSelected = $this->is_selected($this->sendValues, $value[$t], 33);
									$out .= '<tr>'."\n".'<td align="left" valign = "top">'."\n".
									'<div class="m4jSelectItem m4jSelectItemVertical">'."\n".
									'<input class="m4jCheckBox" type="checkbox" id="m4j-'.$eid.'-'.$t.'" name="m4j-'.$eid.'[]" value="'.$value[$t].'" '.$isSelected.'></input>'.$option[$t]."\n".
									'</div>'."\n".
									'</td>'."\n".'</tr>'."\n";
								}
							 $out .='</tbody>'."\n".'</table>'."\n".'</div>'."\n";
							}
							else
							{
								$out .= '<div '.$eval.'class="m4jCheckboxWrap">'."\n".'<div style="width:'.$width.';">'."\n";
								for($t=0;$t<$count;$t++){
									if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
									$isSelected = $this->is_selected($this->sendValues, $value[$t], 33);
									$out .= '<div class="m4jSelectItem">'."\n".
									'<input class="m4jCheckBox" type="checkbox" id="m4j-'.$eid.'-'.$t.'" name="m4j-'.$eid.'[]" value="'.$value[$t].'" '.$isSelected.'></input>'.$option[$t]."\n".
									'</div>'."\n";
								}
								$out .= '</div>'."\n".'</div>'."\n";
							}
							break;
								
				case 34:
					if(! is_array($this->sendValues)) $this->sendValues = array();
					$out .= '<select class="m4jMultipleSelection" id="m4j-'.$eid.'" name="m4j-'.$eid.'[]" size="'.$element_rows.'" multiple="multiple" style="width: '.$width.';" >'."\n";
					for($t=0;$t<$count;$t++){
						if(! trim($option[$t]) && ! trim($value[$t]) ) continue;
						$isSelected = $this->is_selected($this->sendValues, $value[$t], 34);
						$out .='<option value="'.$value[$t].'" '.$isSelected.'>'.$option[$t].'</option>'."\n";
					}
					$out .='</select>'."\n";
					break;
			}
			}
			$this->buffer = $out;
	}//EOF options
	 
	function is_selected(& $value,$option,$form)	{
		if($form == 33 || $form == 34){
			foreach ($value as $key=> & $val){
				if($val == $option){
					unset($value[$key]);
					return ($form == 34) ? 'selected="selected"' : 'checked="checked"';
				}
			}
			return '';
		}else{
			$mark = 'selected="selected"';
			if($form==31 || $form==33) $mark ='checked="checked"';
			if($value==$option) return $mark;
			else return '';
		}
	}
	
	function getBuffer(){
		return $this->buffer;
	}
	
}

