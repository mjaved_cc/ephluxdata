<?php
/**
 * @name MOOJ Proforms
 * @version 1.0
 * @package proforms
 * @copyright Copyright (C) 2008-2012 Mad4Media. All rights reserved.
 * @author Dipl. Inf.(FH) Fahrettin Kutyol
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Please note that some Javascript files are not under GNU/GPL License.
 * These files are under the mad4media license
 * They may edited and used infinitely but may not repuplished or redistributed.
 * For more information read the header notice of the js files.
 **/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class MPayPal {
	
	protected $casting = array(
			"quantity"=>"int",
			"amount"=>"float",
			"tax"=>"float"
	);
	
	protected $objectKeys = array("taxtype");
	
	protected $isSandbox = 0;
	protected $params = array();
	protected $storage = null;
	protected $taxtype = 0;
	
	public function __construct(& $jobs = null ,& $storage = null){
		$this->isSandbox = (int) $jobs->is_sandbox;
		if(isset($jobs->paypal) && $jobs->paypal){
			$chopped = explode(";", $storage->replaceByAlias($jobs->paypal) );
			foreach ($chopped as $atom){
				$pos = strpos($atom, "=");
				if($pos !== false){
					$key = trim(substr($atom, 0, $pos));
					$value = trim(substr($atom, ($pos+1), (strlen($atom) - $pos) ) );
					$this->add($key,$value);
				}
			}//EOF foreach $chopped
		}
		if($this->taxtype == 2){
			$this->params["tax"] =  round( ( $this->params["amount"] /100 ) * $this->params["tax"] * $this->params["quantity"]  , 2);
		}else if($this->taxtype == 1){
			$this->params["tax"] =  round( $this->params["tax"] * $this->params["quantity"]  , 2);
		}
		
		if(!$this->params["quantity"]) $this->params["quantity"] = 1;
	}
	
	public function add($key=null, $value= null){
		if(array_key_exists($key, $this->casting)){
			switch($this->casting[$key]){
				case "int":
					$value = intval($value);
					break;
				case "float":
					$value = round(floatval($value),2); 
					break;
			}
		}
		if(in_array($key, $this->objectKeys)){
			$this->$key = $value;
		}else{
			$this->params[$key] = $value;
		}
	}
	
	public function getParam($parameterName = null){
		if(!$parameterName) return null;
		if(isset($this->params[$parameterName])) return $this->params[$parameterName];
		else return null;
	}
	
	public function setParam($parameterName = null, $value = null){
		if(!$parameterName) return null;
		$this->params[$parameterName] = $value;		
	}
	
	public function get($key = null){
		if(!$key)return null;
		return isset($this->$key) ? $this->$key : null;
	}
	
	public function set($key = null, $value = null){
		if(!$key)return null;
		$this->$key = null;		
	}
	
	public function replaceByAlias($value){
		return $this->storage->replaceByAlias($value);
	}
	
	public function getURL(){
		$params = "";
		foreach($this->params as $key => & $value){
			$params .= ($key=="return" || $key=="cancel_return") ?  "&".$key."=".urlencode( JRoute::_($value) ) :  "&".$key."=".urlencode($value);
				
		}
		$url = ($this->isSandbox) ? "https://www.sandbox.paypal.com/cgi-bin/webscr":"https://www.paypal.com/cgi-bin/webscr";
		$url.= "?cmd=_xclick".$params."&rm=2";
		return $url;
	}
	
}
