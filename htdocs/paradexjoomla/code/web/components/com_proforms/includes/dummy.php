<?php
/**
 * @name MOOJ Proforms
 * @version 1.0
 * @package proforms
 * @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
 * @author Dipl. Inf.(FH) Fahrettin Kutyol
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Please note that some Javascript files are not under GNU/GPL License.
 * These files are under the mad4media license
 * They may edited and used infinitely but may not repuplished or redistributed.
 * For more information read the header notice of the js files.
 **/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );
echo '<script type="text/javascript"> var errorColor = "#'.M4J_ERROR_COLOR.'";</script>'."\n";
?>
<script type="text/javascript" > 
	var m4jShowTooltip = 1; 
	var pfmFields = window.parent.pfmFields;
	document.write(window.parent.document.getElementById('preview').innerHTML);
</script>
<div style="visibility: visible; display: block; height: 20px; text-align: right; margin: 0pt; padding: 0pt; background-color: transparent;"><a href="http://www.mad4media.de" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none; margin: inherit; padding: inherit; background-color: transparent;">mad4media </a><a href="http://www.mad4media.de/user-centered-design.html" rel="follow" style="visibility: visible; display: inline; font-size: 10px; font-weight: normal; text-decoration: none; margin: inherit; padding: inherit; background-color: transparent;">user interface design</a></div>


<?php 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// FOOTER JAVASCRIPT ++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
// include DOJO
echo '<script type="text/javascript" src="'.M4J_FRONTEND_DOJO.'"></script>'."\n";
//include balloontips
$document=& JFactory::getDocument();
$document->addStyleSheet(M4J_FRONTEND_BALOONTIP_CSS,"text/css","screen");
$document->addStyleSheet(M4J_CSS,"text/css","screen");
echo'<script type="text/javascript" src="'.M4J_FRONTEND_BALOONTIP.'"></script>';
// include underline
echo "\n".'<script type="text/javascript" src="'.M4J_FRONTEND_UNDERLINE.'"></script>'."\n";
// include mooj
echo '<script type="text/javascript" src="'.M4J_FRONTEND_MOOJ.'"></script>'."\n";
// include language text
include_once (M4J_INCLUDE_JSONTEXT);
	
if(M4J_USE_JS_VALIDATION){
	echo '<script type="text/javascript" src="'.M4J_FRONTEND_EVALUATION.'"></script>'."\n";
}
echo '<script type="text/javascript" src="'.M4J_FRONTEND_JS.'scrollconfirm.js"></script>'."\n";

//This is the main proforms object script. It must allways be the last script before the custom code
echo '<script type="text/javascript" src="'.M4J_FRONTEND_JS.'proforms.js"></script>'."\n";

?>