<?PHP
/**
 * @name MOOJ Proforms
 * @version 1.0
 * @package proforms
 * @copyright Copyright (C) 2008-2010 Mad4Media. All rights reserved.
 * @author Dipl. Inf.(FH) Fahrettin Kutyol
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Please note that some Javascript files are not under GNU/GPL License.
 * These files are under the mad4media license
 * They may edited and used infinitely but may not repuplished or redistributed.
 * For more information read the header notice of the js files.
 **/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class HTML_mad4jobs {

	function error_no_category()
	{
		echo '<div class ="'.M4J_CLASS_ERROR.'">'.M4J_LANG_ERROR_NO_CATEGORY.'</div>';
	}

	function error_no_form()
	{
		echo '<div class ="'.M4J_CLASS_ERROR.'">'.M4J_LANG_ERROR_NO_FORM.'</div>';
	}

	function link($link="",$core="",$class=null, $id=null)
	{
		global $itemID;
		if($itemID != NULL)	$link = $link.'&Itemid='.$itemID;
		$link = JRoute::_(str_replace('&amp;', '&',$link));
		$add = "";
		if ($class!=null) $add .= 'class="'.$class.'"';
		if ($id!=null) $add .= ' id="'.$id.'"';
		return '<a href="'.$link.'" '.$add.'>'.$core.'</a>';
	}//EOF LINK


	function stylesheet(){
		$pluginManager = & AppPluginManager::getInstance();	
		if(! $pluginManager->isStop("css")){
			$document=& JFactory::getDocument();
			$document->addStyleSheet(M4J_CSS);
		}
	}

	function catHeading($text){

		echo "\n".'<div class ="componentheading">'.$text.'</div>'."\n";
	}


	function heading($text)	{
		$pluginManager = & AppPluginManager::getInstance();			
		if(M4J_FORM_TITLE && ! $pluginManager->isStop("title") ){
			//App Plugin Alter title
			$pluginManager->onTitle($text);
			echo "\n".'<h2 class ="'.M4J_CLASS_HEADING.'">'.$text.'</h2>'."\n";
		}
	}

	function headertext($text)
	{
		$pluginManager = & AppPluginManager::getInstance();	
		
		echo "\n".'<!-- MOOJ PROFORMS START --> '."\n";
		$sec ="";
		
		//App Plugins before content
		echo $pluginManager->onBeforeContent();
		
		// App Plugin show content if plugins allow
		if(! $pluginManager->isStop("maintext")){
			// App Plugin Alter Content
			$pluginManager->onContent($text);
			// Render content
			echo '<div class ="'.M4J_CLASS_HEADER_TEXT.'">'."\n".$text."\n".'</div>'."\n";
		}
		//App Plugins after content
		echo $pluginManager->onAfterContent();
		
		// Spambot Trap 1
		$spambot_trap = "3c646976207374796c65203d22646973706c61793a6e6f6e653b223e47656e6572617465642077697468204d4f4f4a2050726f666f726d732042617369632056657273696f6e20312e333c2f6469763e";
		for($i=0;$i<strlen($spambot_trap );$i+=2)
		{
			$sec.=chr(hexdec(substr($spambot_trap ,$i,2)));
		}
		echo stripslashes($sec);
	}

	function listing($heading,$introtext,$link=null)
	{
		echo '<div class ="'.M4J_CLASS_LIST_WRAP.'">'."\n";
		echo '<div class ="'.M4J_CLASS_LIST_HEADING.'">'."\n";
		//contentpagetitle
		if($link!=null) echo $this->link($link,$heading);
		else echo $heading;

		echo '</div>'."\n";
		echo '<div class ="'.M4J_CLASS_LIST_INTRO.'">'."\n".$introtext."\n".'</div>'."\n";
		echo '</div>'."\n";
	}



	function replace_yes_no($html)
	{
		$html =  str_replace('{M4J_YES}',M4J_LANG_YES,$html);
		return str_replace('{M4J_NO}',M4J_LANG_NO,$html);
	}

	function form_head($table_width='100%',$send=null,$jid=null, $cid = null)
	{
		global $itemID;
		
		
		$pluginManager = & AppPluginManager::getInstance();				
		$add_query = "";
		
		// Ready for Takeover
		if(defined('M4J_TAKEOVER_URL')){
			$add_query = M4J_TAKEOVER_URL ;
		}
		
		echo '<script type="text/javascript"> var errorColor = "#'.M4J_ERROR_COLOR.'"; var m4jShowTooltip = '.$GLOBALS['isTooltip'].';</script>'."\n";
		echo '<div id="proforms_proforms" class ="'.M4J_CLASS_FORM_WRAP.'">'."\n";
		echo'<form id="m4jForm" name="m4jForm" method="post" enctype="multipart/form-data" action="'.$add_query.'">'."\n";
		
		//Plugins Form Head
		echo $pluginManager->formHead();
		
		if($send) {
			echo '<input type="hidden" name="send" value="'.$send.'"></input>'."\n";
		}
		
		// Ready for iFrame
		$tmpl = JRequest::getString("tmpl",null);
		if($tmpl == "component"){
			echo '<input type="hidden" name="tmpl" value="component"></input>'."\n";	
		}
		
		$this->trap();
	}

	function form_footer($code1 = null)
	{		
		$pluginManager = & AppPluginManager::getInstance();
		$pluginManager->onSubmitCallBacks();
		$stop = & $pluginManager->getStop();
		
		
		//Close Form
		echo '</form>'."\n" . '</div>'."\n" ;
		
		//Clear Both
		echo '<div style="clear:both;"></div>'."\n";
		
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// FOOTER JAVASCRIPT ++++++++++++++++++++++++++++++++++++++++++++++++++++
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			
		// include DOJO
		echo '<script type="text/javascript" src="'.M4J_FRONTEND_DOJO.'"></script>'."\n";
		//include balloontips
		$this->include_balloontips();
		// include underline
		echo "\n".'<script type="text/javascript" src="'.M4J_FRONTEND_UNDERLINE.'"></script>'."\n";
		// include mooj
		echo '<script type="text/javascript" src="'.M4J_FRONTEND_MOOJ.'"></script>'."\n";
		// include language text
		include_once (M4J_INCLUDE_JSONTEXT);
			
		if(M4J_USE_JS_VALIDATION){
			echo '<script type="text/javascript" src="'.M4J_FRONTEND_EVALUATION.'"></script>'."\n";
		}		
		echo '<script type="text/javascript" src="'.M4J_FRONTEND_JS.'scrollconfirm.js"></script>'."\n"; 
		echo '<script type="text/javascript" src="'.M4J_FRONTEND_JS.'textareamaxchars.js"></script>'."\n"; 
		
		//This is the main proforms object script. It must allways be the last script before the custom code
		echo '<script type="text/javascript" src="'.M4J_FRONTEND_JS.'proforms.js"></script>'."\n"; 
		
		//If not stopped by a plugin
		if(! $stop->customscript){
			// print additional code
			echo $code1;
		}
		
		//App Plugin at end
		echo $pluginManager->atEnd();
		
		
		//Render End Scripts
		renderEndScripts();
		//EOF Render End Scritps
		
		echo "\n".'<!-- MOOJ PROFORMS END --> '."\n";
		
		if(_IEFIREBUG){
			echo "<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js'></script>";		
		}
		
	}//EOF Form Footer

	function include_balloontips(){

		$document=& JFactory::getDocument();
		$document->addStyleSheet(M4J_FRONTEND_BALOONTIP_CSS,"text/css","screen");
		//		$document->addScript(M4J_FRONTEND_BALOONTIP);

		echo'<script type="text/javascript" src="'.M4J_FRONTEND_BALOONTIP.'"></script>';
	}

	function add_error($err)
	{
		return '<span class="'.M4J_CLASS_ERROR.'">'.$err.'<br/></span>';
	}

	function error($err)
	{
		echo '<span class="'.M4J_CLASS_ERROR.'">'.M4J_LANG_ERROR_IN_FORM.'</span>';
		echo $err.'<br/>';
	}

	function mail_error()
	{
		echo '<span class="'.M4J_CLASS_ERROR.'">'.M4J_LANG_ERROR_NO_MAIL_ADRESS.'</span><br/>';

	}

	function sent_success($isReturn = 0)
	{
		$text = '<h3>'.M4J_LANG_SENT_SUCCESS.'</h3><br/>';
		if($isReturn){
			return $text;
		}else{
			echo $text;
		}
		
	}

	function sent_error()
	{
		echo '<h3 style="color:red">'.M4J_LANG_SENT_ERROR.'</h3><br/>';
	}

	function is_selected($value,$option,$form)
	{
		$mark = 'selected = "selected" ';
		if($form==31 || $form==33) $mark ='checked="checked"';
		if($value==$option) return $mark;
		else return '';
	}

	function dbError($error)
	{
		echo "<script type='text/javascript'>alert('".$error."');</script>";
	}

	function textAreaMaxChars($maxchars = 0, $eid = null){
		return "\n". '<div class="pfmTAMaxChars m4jTextareaLimit">' . M4J_LANG_CHAR_LEFT . ': <b id="left_m4j-'.$eid.'"></b></div>' . "\n";
	}
	
	function body_header($hidden = "")
	{
		if(M4J_HTML_MAIL) return '<meta http-equiv="Content-Type" content="text/html; charset='.M4J_MAIL_ISO.'" />'.
		'<div style="font-family:Arial;">'.$hidden.'<br/>';
		else return strip_tags(str_replace(array("<br>","<br />", "<br/>", "</p>"),"\n",$hidden) )."\n\n";

	}

	function bodyHeadClean(){
		if(M4J_HTML_MAIL) return '<meta http-equiv="Content-Type" content="text/html; charset='.M4J_MAIL_ISO.'" />'.
		'<div style="font-family:Arial;">';
		else "";
	}


	function values_head()
	{
		if(M4J_HTML_MAIL) return '<table width ="100%" border="0" align="left" cellpadding="2" cellspacing="3" ><tbody>';
		else return "\n";
	}

	function values_footer()
	{
		if(M4J_HTML_MAIL) return '</tbody></table><br/></div>';
		else  return "----------------------------------------------------------------------------\n";
	}

	function values($question,$answer)
	{
		if(M4J_HTML_MAIL) return '<tr><td align="left" valign="top" width="46%">'.$question.'</td><td align="center" valign="top" width="8%" > : </td><td align="left" valign="top" width="46%" >'.str_replace("\n", "<br/>", $answer).'</td></tr>';
		else  return $question.": \n".$answer."\n\n";
	}
		

	function ask2confirm(){
		$pluginManager = & AppPluginManager::getInstance();
		// App Plugins show confirmation question if plugins are allowing
		if(! $pluginManager->isStop("confirmation")) {
			$value = JRequest::getInt("mailcopy",null);
			$checked = $value ? ' checked="checked" ' : '' ;
?>
			<div class="m4jCLR"></div>
			<div class="m4jAsk2Confirm" id="m4jAsk2Confirm">
			<div style="display: none" id="m4jAsk2ConfirmDesc"><?php echo M4J_LANG_ASK2CONFIRM_DESC; ?></div>
			<input type="checkbox" name="mailcopy" value="1" <?php echo $checked;?>></input>
			<label><?php echo M4J_LANG_ASK2CONFIRM; ?> <!--			<img class="m4jInfoImage" src="<?php echo M4J_FRONTEND; ?>images/help<?php echo M4J_HELP_ICON;?>.png" border="0" alt="<?php echo M4J_LANG_ASK2CONFIRM_DESC; ?>" align="top"></img>-->
			</label></div>
			<div class="m4jCLR"></div>
<?php 
		}//EOF App Pluginmanager is confirmation
	
	}//EOF function ask2Confirm

		function checkMailURL($value)
		{
			$validate = new M4J_validate();
			if(!M4J_HTML_MAIL) return $value;
			if ($validate->email($value))
			return '<a href="mailto:'.$value.'">'.$value.'</a>';
			elseif ($validate->url($value))
			{
				if(substr_count(strtolower($value),'http') == 1 || substr_count(strtolower($value),'https') == 1) return '<a href="'.$value.'">'.$value.'</a>';
				else  return '<a href="http://'.$value.'">'.$value.'</a>';
			}
			else return $value;
		}
			
		function format_value($value,$form=0)
		{
			if(!$value)return null;
			if($form>=33 && $form<40)
			{
				if(M4J_HTML_MAIL) return  stripslashes(implode("<br/>",$value));
				else return  stripslashes(implode("\n",$value));
			}
			if($form>=20 && $form<30)
			return stripslashes($this->checkMailURL($value));

			return stripslashes($value);
		}


		function server_data()
		{
			$br ="\n";
			$head = "----------------------------------------------------------------------------\n";
			$footer = "----------------------------------------------------------------------------\n";

			if(M4J_HTML_MAIL)
			{
				$br ='</td></tr><tr><td colspan="3" style="font-family: Arial; font-size:11px;">';
				$head ='<tr><td colspan="3" height="18" ><hr/></td></tr><tr><td colspan="3" style="font-family: Arial; font-size:11px;">';
				$footer ='</td></tr>';
			}


			$user =& JFactory::getUser();
			$userData = "User: ";
			if($user->guest==1){
				$userData .= " Guest";
			}else{
				$userData .= $user->username.$br."Real Name: ".$user->name.$br."Email: ". '<a href="mailto:'.$user->email.'">'.$user->email.'</a>'.$br;
			}

			$userAgent = $this->healEmail( strip_tags( array_key_exists('HTTP_USER_AGENT',$_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : "unknow") ) ;
			$host = $this->healEmail( strip_tags( array_key_exists('REMOTE_NAME',$_SERVER) ? $_SERVER['REMOTE_NAME'] : @ gethostbyaddr($_SERVER['REMOTE_ADDR'])) );
			$remoteAddress = strip_tags( array_key_exists('REMOTE_ADDR',$_SERVER) ? $_SERVER['REMOTE_ADDR'] : "") ;
			$remoteAddress = (preg_match(
					'/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/',
					$remoteAddress)) ? $remoteAddress : "CATCHED IP ADDRESS INFILTRATION";
			$remotePort = intval( array_key_exists('REMOTE_PORT',$_SERVER) ? $_SERVER['REMOTE_PORT'] : 0) ;
			
			return  $head.
					'Sending Time: '.date("Y-m-d H:i:s",time()). $br .
					'User Agent: '.$userAgent. $br .
			  	    'Host: '. $host . $br .
			   		'IP: '.$remoteAddress . $br .
					'PORT: '.$remotePort . $br . 
					$userData .
					$footer; 

		}

		function healEmail($str) {
			$injections = array('/(\n+)/i',
					'/(\r+)/i',
					'/(\t+)/i',
					'/(%0A+)/i',
					'/(%0D+)/i',
					'/(%08+)/i',
					'/(%09+)/i'
			);
			$str= preg_replace($injections,'',$str);
			return $str;
		}

		function required_advice()
		{
			if(!$GLOBALS['isRequiredAdvice']) return;
			echo '<div class="m4j_required_wrap"><span class="m4j_required">*</span> '.M4J_LANG_REQUIRED_DESC.'</div>';
		}

		function trap(){
			echo '<div  class="m4j-email"><label>For submitting your data don\'t fill this following email field:</label><input type="text" name="email" value=""></input></div>';
		}
		
		function iFrameRedirect($url){
			return "<html>\n<head>\n".			
				   '<script type="text/javascript"> self.parent.location = "'.$url.'"; </script>'."\n".
				   "</head>\n<body></body>\n</html>\n";
		}
}//EOF Class

function metaTitle( $title=null ){
	$title = htmlspecialchars($title);
	$document=& JFactory::getDocument();
	$document->setTitle($title);
}


$GLOBALS['m4jDebug'] = "";
class MDebug{
	static function _($string){
		$GLOBALS['m4jDebug'] .= $string."<br>";
	}

	static function pre($obj){
		$GLOBALS['m4jDebug'].= '<pre>';
		ob_start();
		print_r($obj);
		$GLOBALS['m4jDebug'] .= wordwrap( htmlspecialchars(ob_get_clean()) , 110, "\n", true);
		$GLOBALS['m4jDebug'] .= '</pre><br>';
	}

	static function preSpecialChars($obj){
		$GLOBALS['m4jDebug'].= '<pre>';
		ob_start();
		print_r($obj);
		$GLOBALS['m4jDebug'] .= htmlspecialchars ( ob_get_clean());
		$GLOBALS['m4jDebug'] .= '</pre><br>';
	}

	function out(){
		echo '<div style="clear:both;"></div>'."\n";
		echo '<center><div style="display:block;width:90%; text-align:left; padding:10px; color:black; border:1px solid #343434; background-color: #efefef; margin:10px;">'."\n";
		echo '<h3>Debug:</h3>'."\n";
		echo $GLOBALS['m4jDebug'];
		echo '</div></center>'."\n";

	}

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++

class MReady{
	
	function _($string){
		return str_replace('"',"&quot;",stripslashes($string) );
	}
	
	function change(& $string){
		$string = str_replace('"',"&quot;",stripslashes($string) );
	}
}
?>
