<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
class hikashopVolumeType{

	function display($map, $volume_unit){
		$config =& hikashop_config();
		$symbols = explode(',',$config->get('volume_symbols','m,cm'));

		if(empty($volume_unit)){
			$volume_unit = $symbols[0];
		}
		$this->values = array();

		if(!in_array($volume_unit,$symbols)){
			$this->values[] = JHTML::_('select.option', $volume_unit,JText::_($volume_unit) );
		}

		foreach($symbols as $symbol){
			$this->values[] = JHTML::_('select.option', $symbol,JText::_($symbol) );
		}

		return JHTML::_('select.genericlist', $this->values, $map, 'class="inputbox volumeselect" size="1"', 'value', 'text', $volume_unit);

	}
}
