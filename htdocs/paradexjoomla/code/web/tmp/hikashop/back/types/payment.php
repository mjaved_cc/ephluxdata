<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
class hikashopPaymentType{
	var $extra = '';
	function load($form){
		$this->values = array();
		$pluginsClass = hikashop_get('class.plugins');
		$methods = $pluginsClass->getMethods('payment');

		if(!$form){
			$this->values[] = JHTML::_('select.option', '', JText::_('ALL_PAYMENT_METHODS') );
		}

		if(!empty($methods)){
			foreach($methods as $method){
				$this->values[] = JHTML::_('select.option', $method->payment_type, $method->payment_name );
			}
		}
	}
	function display($map,$value,$form=true,$attribute='size="1"'){
		$this->load($form);
		if(!$form){
			$attribute .= ' onchange="document.adminForm.submit();"';
		}
		return JHTML::_('select.genericlist', $this->values, $map, 'class="inputbox" '.$this->extra.' '.$attribute, 'value', 'text', $value );
	}
}
