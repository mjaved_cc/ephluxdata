<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
class plgHikashopAcymailing extends JPlugin
{
	function plgHikashopAcymailing(&$subject, $config){
		parent::__construct($subject, $config);
		if(!isset($this->params)){
			$plugin = JPluginHelper::getPlugin('hikashop', 'history');
			jimport('joomla.html.parameter');
			if(version_compare(JVERSION,'2.5','<')){
				$this->params = new JParameter($plugin->params);
			} else {
				$this->params = new JRegistry($plugin->params);
			}
		}
		}

		function onAfterOrderCreate(&$order,&$send_email){
		return $this->onAfterOrderUpdate($order,$send_email);
		}

		function onAfterOrderUpdate(&$order,&$send_email){
			if(!empty($order->order_id) && !empty($order->order_status)){
				if(empty($order->order_user_id)){
				$class = hikashop_get('class.order');
				$old = $class->get($order->order_id);
				$order->order_user_id = $old->order_user_id;
			}
			$userClass = hikashop_get('class.user');
			$user = $userClass->get($order->order_user_id);
			if(!empty($user)){
				$helper = rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_acymailing'.DS.'helpers'.DS.'helper.php';
				if(file_exists($helper) && include_once($helper)){
						$subClass = acymailing::get('class.subscriber');
						$sub = $subClass->get($user->email);
					if(!empty($sub->subid)){
						if(file_exists(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_acymailing'.DS.'classes'.DS.'filter.php')){
							$filterClass = acymailing::get('class.filter');
							if($filterClass){
								$filterClass->subid = $sub->subid;
								$filterClass->trigger('hikaorder_'.$order->order_status);
							}
						}
					}
				}
			}
			}
		return true;
		}
}
