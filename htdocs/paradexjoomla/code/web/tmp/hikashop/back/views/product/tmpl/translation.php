<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.0.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2012 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php

if($this->config->get('multilang_display')=='popups'&&!empty($this->element->product_id)){
	echo '<div class="hikashop_multilang_buttons" id="hikashop_multilang_buttons">';
	foreach($this->element->translations as $language_id => $translation){
		echo '<a class="modal" rel="{handler: \'iframe\', size: {x: 760, y: 480}}" href="'.hikashop_completeLink("product&task=edit_translation&product_id=".@$this->element->product_id.'&language_id='.$language_id,true ).'"><div class="hikashop_multilang_button">'.$this->transHelper->getFlag($language_id).'</div></a>';
	}
	echo '</div>';
}
	echo $this->tabs->startPane( 'translations');
		echo $this->tabs->startPanel(JText::_('MAIN_INFORMATION'), 'main_translation');
			$this->setLayout('normal');
			echo $this->loadTemplate();
		echo $this->tabs->endPanel();
		if($this->config->get('multilang_display')!='popups' && !empty($this->element->translations)){
			foreach($this->element->translations as $language_id => $translation){
				$this->product_name_input = "translation[product_name][".$language_id."]";
				$this->element->product_name = @$translation->product_name->value;
				if(isset($translation->product_name->published)){
					$this->product_name_published = $translation->product_name->published;
					$this->product_name_id = $translation->product_name->id;
				}

				$this->editor->name = 'translation_product_description_'.$language_id;
				$this->element->product_description = @$translation->product_description->value;
				if(isset($translation->product_description->published)){
					$this->product_description_published = $translation->product_description->published;
					$this->product_description_id = $translation->product_description->id;
				}
				if($this->element->product_type=='main'){
					$this->product_url_input = "translation[product_url][".$language_id."]";
					$this->element->product_url = @$translation->product_url->value;
					if(isset($translation->product_url->published)){
						$this->product_url_published = $translation->product_url->published;
						$this->product_url_id = $translation->product_url->id;
					}

					$this->product_meta_description_input = "translation[product_meta_description][".$language_id."]";
					$this->element->product_meta_description = @$translation->product_meta_description->value;
					if(isset($translation->product_meta_description->published)){
						$this->product_meta_description_published = $translation->product_meta_description->published;
						$this->product_meta_description_id = $translation->product_meta_description->id;
					}

					$this->product_keywords_input = "translation[product_keywords][".$language_id."]";
					$this->element->product_keywords = @$translation->product_keywords->value;
					if(isset($translation->product_keywords->published)){
						$this->product_keywords_published = $translation->product_keywords->published;
						$this->product_keywords_id = $translation->product_keywords->id;
					}
				}
				echo $this->tabs->startPanel($this->transHelper->getFlag($language_id), 'translation_'.$language_id);
					$this->setLayout('normal');
					echo $this->loadTemplate();
				echo $this->tabs->endPanel();
			}
		}
	echo $this->tabs->endPane();
