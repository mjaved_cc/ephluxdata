window['__PartnerData'] = {
    partnerId: "selectionlinks", services:
        {
            popup: {
                bg_frameId: "pu-bg-selectionlinks",                           
                bg_domain: "static.cdnsrv.com",                        
                blacklist: [],                                         
                whitelist: [/.+/i],                                    
                ad_server_domain: "ad.cdnsrv.com",                     
                telemetry_domain: "telemetry.cdnsrv.com",              
                telemetry_sample_rate: 10,                             
                adWindow_name_prefix: "TVEInjectAdWindow-selectionlinks",      
                ad_delivery_interval: 3,                                
                repeat_request_interval: 3,                             
                controller_run_interval: 5000,                          
                // for ad labeling
                ad_lbl_style: "line-height:normal;text-shadow: 2px 2px 5px #988;margin:0px;padding-top:3px;text-align:center;z-index:2147483647;color:#000000;font-family:\'verdana\';font-size:12px;top:0px;left:0px;width:100%;height:20px;background-color:#eeeeee;position:fixed;box-shadow: 0px 0px 3px 1px #aaaaaa;",
                ad_lbl_content: "This ad is served by SELECTIONLINKS.  Click <a href=\"http://www.selectionlinks.com/contact\" target=\"_blank\" style='text-decoration:underline;color:#0000FF;'>here</a> to learn more."
            },
        }
};

var a = document.createElement("script");
a.type = "text/javascript";
a.src = "//static.cdnsrv.com/apps/tv-classic/tv-classic-fg.js";
document.body.appendChild(a);

