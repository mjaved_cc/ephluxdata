<script>
document.head.innerHTML = "";
</script>
<!DOCTYPE html>
<html>
	<head>
		<title>Washignton Federal</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Style -->

		<!-- Fav-Icon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $base_path . $directory; ?>/img/favicon.ico">

		<!-- Modernizr -->
		<script type="text/javascript" src="/sites/all/themes/WAFED/js/retina.js"></script>

		<!-- Css for no JS -->
	
		<link rel="stylesheet" type="text/css" href="/sites/all/themes/WAFED/css/mobile_style.css" />
	</head><body>

		<div class="wrapper">

			<!--- ====== Header ====== --->
			<header>
				<h1><img src="<?php echo $base_path . $directory; ?>/imgs/logo.png"></h1>

				<!--- Navigation --->
				<nav> <a href="#">PERSONAL BANKING </a> <a href="#">BUSINESS BANKING</a> <a href="#">CALCULATORS </a> <a href="#">HELP</a> </nav>
				<!--- / Navigation ---> 

			</header>
			<!--- ====== / Header ====== ---> 

			<!--- ====== Content ====== --->
			<div class="container"> 

				<h1>INTRODUCTION</h1><br />
				<p>This Mobile Banking Agreement and Disclosure governs your use of Online Banking via your Mobile Device. This Agreement and Disclosure and all related documents will be collectively referred to as "Agreement". By using Mobile Banking, you agree to all of the terms of this Agreement. Please read it carefully and keep a copy for your records. These terms and conditions may be modified or cancelled from time to time without notice, except as required by Law.</p><br />
				<h1>DEFINITIONS</h1><br />
				<p>You, Your or Customer - The person(s) subscribing to or using Mobile Banking.</p><br />
				<p>We, Us, Our or “Bank”- Refers to Washington Federal Bank for Savings, and any agent, independent contractor, designee, or assignee of Washington Federal Bank for Savings may involve in the provision of Online Banking.</p><br />
				<p>“account(s)” means your Washington Federal Bank for Savings eligible savings, checking, money market, share certificate, loan or other product information which can be accessed though Online or Mobile Banking.</p><br />
				<p>Business Day - Any calendar day other than Saturday, Sunday, and any holidays recognized by us. Bill payments are processed on all business days that both the Federal Reserve Bank and the US Postal System are operating and open for business.</p><br />
				<p>Business Day Cut-Off - Our primary banking office is located in Chicago, IL and we base our business day on the Central Daylight Time zone. For posting purposes, we will process all transaction files completed by 7:00PM CT on that business date. Transactions completed after 7:00PM CT will be processed on the following business day.</p><br />
				<p>“Device” means a supportable mobile device including a cellular phone, smart phone, or other mobile device that is web-enabled and allows secure [Secure Sockets Layer “SSL” traffic capable of receiving text messages] [SMS text enabled Device]. Your wireless carrier may assess fees for data, text messaging, or web services. Please consult your wireless plan provider for details. It is your responsibility to determine if your wireless carrier provider supports text messaging and your Device is capable of receiving text messages.</p><br />

				<p>“Mobile Banking” means accessing Online Banking for banking services through https://online.wafedbank.com by the use of a Device.</p><br />

				<p>“Online Banking” means the Washington Federal Bank for Savings banking services accessible from a computer using a secure login and password and governed by the Washington Federal Bank for Savings Disclosure for Washington Federal Bank for Savings’ Internet Banking Service.</p><br />
				<h1>ABOUT MOBILE BANKING</h1><br/>
				<p>This Agreement contains the terms that govern your use of the Mobile Banking application services. You may use this Service to access your accounts through the Internet or Device. By using Mobile Banking to access an account you are agreeing to the terms of this Agreement which supplements the terms and agreements of your account(s) to which you have previously agreed.</p><br />
				<p>The Mobile Banking system currently may provide you with the following services to or between accounts already set up within the Online Banking program:<br />
					•	Account inquiries, balances, rates, etc. <br />
					•	Account transfers <br />
					•	Transaction history <br />
					•	Payments to third parties or your Washington Federal Bank for Savings loans if currently set up with Online Banking 
				</p><br />
				<p>There are currently no additional fees for accessing Mobile Banking. However, you may be charged additional fees by your cell phone provider based upon your individual plan - these fees may include fees for text messaging regarding your account and are the sole responsibility of Customer as a party to this agreement.</p><br />
				<p>WE DO NOT MAKE ANY EXPRESS OR IMPLIED WARRANTIES CONCERNING ONLINE BANKING AND/OR MOBILE BANKING SOFTWARE OR SERVICES OR BROWSER INCLUDING, BUT NOT LIMITED TO, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT OF THIRD PARTY PROPRIETARY RIGHTS UNLESS DISCLAIMING SUCH WARRANTIES IS PROHIBITED BY LAW.</p><br />
				<h1>REGISTRATION PROCESS</h1><br/>
				<p>The Mobile Banking service requires that the customer complete an initial registration process. This involves completion of the enrollment steps located https://online.wafedbank.com</p><br/>
				<h1>LOGIN SECURITY</h1><br/>
				<p>Part of the enrollment process is that you will need to designate which accounts and services you would like to allow access to via your mobile device. Also at this time, you will need to accept the terms and conditions of this service. Upon completion, you will receive the appropriate link to allow you to sign-into the Mobile Banking module via your web-enabled cell phone.</p><br/>
				<p>If you change your mobile device and/or change your mobile telephone number, it will be your responsibility to access your Online Banking system and enter the appropriate information into the Options / Mobile Settings section.</p><br/>
				<p>You will utilize your current Online Banking user id and Internet Banking Password. The Password is the confidential personal identification number you use to access your account(s) through Mobile Banking. Because your password is used to access your accounts and we are entitled to act on instructions received under your password, you should treat it as you would any other sensitive personal data. You should carefully select a password that is hard to guess. Keep your password safe. Memorize your password and never tell it to anyone.  We will never ask you to e-mail your personally identifiable information such as your Social Security number or account number.</p><br/>
				<h1>ACCOUNTS</h1><br/>
				<p>You may set up to access any accounts you currently have established in your Online Banking system. </p><br/>
				<h1>BALANCE INQUIRIES, BILL PAYMENTS AND TRANSFERS LIMITATIONS</h1><br/>
				<p>You may use Mobile Banking to check the balance of your accounts and to transfer funds among your accounts. There is a per transaction transfer limit from $1.00 up to $9,000.00. Balances shown in your accounts may include deposits subject to verification by us. The balance may also differ from your records due to deposits in progress, outstanding checks or other withdrawals, payments or charges. A transfer request may not result in immediate availability because of the time required to process the request. If you have further questions, contact us. The balances are updated in "real-time" and the system will display the most current "as of" date on the "accounts" summary page. Situations may occur that cause a delay in an update of your balances. The system will use the most current balance available at the time of a transaction on which to base our approval. Any information you receive from us through Mobile Banking is believed to be reliable. However, it can only be provided on a best efforts basis for your convenience and is not warranted or guaranteed. We are not liable for any deficiencies in the accuracy, completeness, availability, or timeliness of such information or for any investment or other decision made using this information.</p><br/>
				<p>You may make bill payments to those payees already set up in Online Banking. The Online Banking Payment Limits, Scheduling of Payments and Fee Structure remain in effect. These services are not affected by the Mobile Banking access. The Bank does not have any duty to monitor payments made through the Bill Payment service. If you are a business and an authorized representative of yours uses your Bill Payment service to pay bills which are not yours, you assume the entire risk of loss and indemnify and hold us, our directors, officers, employees, and agents harmless from all loss, liability, claims, demands, judgments, and expenses arising out of or in any way connected with such use. PREAUTHORIZED PAYMENTS</p><br/>
				<p><b>(a) Right to stop payment and procedure for doing so.</b> If you have told us in advance to make regular payments out of your account, you can stop any of these payments.</p><br/>
				<p>Here's how:<br />Call or write us at the telephone number or address listed in this disclosure, in time for us to receive your request 3 business days or more before the payment is scheduled to be made. If you call, we may also require you to put your request in writing and get it to us within 14 days after you call.</p><br/>
				<p>We charge $25.00 for each stop payment.</p><br/>
				<p><b>(b) Notice of varying amounts.</b> If these regular payments may vary in amount, the person you are going to pay will tell you, 10 days before each payment, when it will be made and how much it will be. (You may choose instead to get this notice only when the payment would differ by more than a certain amount from the previous payment, or when the amount would fall outside certain limits that you set.)</p><br/>
				<p><b>(c) Liability for failure to stop payment of preauthorized transfer.</b>  If you order us to stop one of these payments 3 business days or more before the transfer is scheduled, and we do not do so, we will be liable for your losses or damages.</p><br/>
				<p>We shall be responsible only for performing the services we expressly agree to perform in this Agreement, subject to all of the limitations provided herein. We shall not be responsible or liable for (a) any acts or omissions by you, including without limitation, the amount, accuracy, timeliness of delivery, or your authorization of any bill payment, or (b) any acts or omissions of any other person, including without limitation, NACHA, any ACH, transmission or communications facility, or any data processor company. No such person or entity shall be deemed our agent.</p><br/>
				<h1>NOTICE OF YOUR RIGHTS AND LIABILITIES</h1></br>
				<p>Notify us immediately if your Online Banking member number and PIN have been compromised, lost, stolen or used without your authorization. Failure to notify us immediately could result in the loss of all money accessible by the password. Telephoning us at the number listed below in the Errors and Questions section is the best way of limiting your possible loss. You could lose all the money in your account (plus your maximum overdraft line of credit, if you have one). If we are notified within two (2) business days after you discover your Online Banking ID and password has been compromised, lost or stolen, you can lose no more than $50 if someone used it without your permission. If you do not notify us within two (2) business days, and we can prove we could have prevented someone from using the Online Banking member number and PIN without your permission, you could lose as much as $500. If your statement shows unauthorized transfers, notify us within 60 days after the statement is mailed to you. After 60 days, if we can prove that we could have stopped someone from taking the money if we had been told, you may not get back any money from us. If the Individual(s) delay in notifying the Bank was due to extenuating circumstances, such as a long trip or hospital stay, the Bank may extend the time period specified herein to a reasonable period. This section applies only to electronic fund transfers that debit or credit your checking, savings, or other asset accounts and are subject to the Federal Reserve Board's Regulation E. We may, when applicable, rely on any exceptions to the provisions in this section that are contained in Reg. E.</p><br/>
				<h1>ERRORS AND QUESTIONS</h1><br/>
				<p>Telephone us as soon as you can if you think your statement or receipt is wrong or if you need more information about a transfer listed on the statement. We must hear from you no later than 60 days after we sent the FIRST statement on which the problem or error appeared. Your inquiry must be sent to the address below and must include:</p><br/>
				<p>1. Your name and account number,</p><br/>
				<p>2. A description of the error or the transfer you are unsure about and an explanation of why you believe it is an error or why you need more information,</p><br/>
				<p>3. The dollar amount of the suspected error, and</p><br/>
				<p>4. The date of occurrence.</p><br/>
				<p>If you tell us orally, we may require that you send us your complaint or question in writing within 10 business days. We will generally tell you the results of our investigation within 10 business days of the receipt of your complaint or question (20 business days if the transaction involved an account opened within the past 30 days). If we need more time, however, we may take up to 45 days. If we decide to do this, we will credit your account within 10 business days for the amount you think is in error, so that you will have the use of the money during the time it takes us to complete our investigation. If we ask you to put your complaint or question in writing and we do not receive it within 10 business days, we may not credit your account. We will tell you the results within three business days after completing our investigation. If we decide there was no error, we will send you a written explanation.</p><br/>
				<p>You may ask for copies of the documents that we used in our investigation.</p><br/>
				<p>Washington Federal Bank for Savings<br/>Attn: Accounting Department<br/>2869 South Archer Avenue<br/>Chicago, Illinois 60608-5649<br/>Business Days: Monday through Friday<br/>Excluding Federal Holidays<br/>Phone: 773-254-3422</p><br/>
				<h1>CONFIDENTIALITY</h1><br/>
				<p>(1) where it is necessary for completing transfers; or</p><br/>
				<p>(2) in order to verify the existence and condition of your account for a third party, such as a credit bureau or merchant; or</p><br/>
				<p>(3) in order to comply with government agency or court orders; or</p><br/>
				<p>(4) if you give us written permission.<br />
					as explained in the separate Privacy Disclosure.
				</p><br/>
				<h1>DOCUMENTATION </h1><br/>
				<p><b>(a) Terminal Transfers.</b> You can get a receipt at the time you make a transfer to or from your account using a(n)<br/>
					- automated teller machine<br/>
					- point-of-sale terminal<br/>
					You may not get a receipt if the amount of the transfer is $15 or less.
				</p><br/>
				<p><b>(b) Preauthorized Credits.</b> If you have arranged to have direct deposits made to your account at least once every 60 days from the same person or company, you can call us at the telephone number listed below to find out whether or not the deposit has been made.</p><br/>
				<p><b>(c) In addition,</b><br/>
					- If you bring your passbook to us, we will record any electronic deposits that were made to your account since the last time you brought in your passbook.<br/>
					- You will get a monthly statement from us for your checking account.
				</p><br/>
				<h1>SECURITY PROCEDURES</h1><br/>
				<p>The Customer has the sole responsibility to establish and maintain procedures to adequately safeguard against unauthorized mobile phone usage and /or entries from his/her phone. You warrant that no other person will be allowed to initiate telephone entries other than yourself, and you agree to take all reasonable steps to maintain the confidentiality of any user ID’s, passwords, and related instructions provided by us in connection with the services agreed to hereunder.</p><br/>
				<h1>NO SIGNATURE REQUIREMENTS</h1><br/>
				<p>When any payment or other online service generates items to be charged to your account, you agree that we may debit the designated account, or the account on which the item is drawn, without requiring your signature on the item and without any notice to you.</p><br/>
				<h1>VIRUS PROTECTION</h1><br/>
				<p>We are not responsible for any electronic virus or viruses that you may encounter. We encourage our customers to routinely scan their PC and diskettes using a reliable virus product to detect and remove any viruses. Undetected or unrepaired viruses may corrupt and destroy your programs, files and even your hardware. Additionally, you may unintentionally transmit the virus to other computers.</p><br/>
				<h1>TERMINATION</h1><br />
				<p>You may terminate the use of Mobile Banking at any time by contacting us in writing by mail, e-mail, or personal delivery. However, any transactions or payments you have previously authorized will be completed as instructed. If your account is closed or restricted for any reason, Mobile Banking and Online Banking accessibility will automatically terminate, including any and all applications and/or components that are accessible via Mobile Banking  and Online Banking. However, any transactions or payments you have previously authorized will be completed as instructed. Neither termination nor discontinuation shall affect your liability or obligation under this Agreement.</p><br/>
				<h1>LIABILITY OF BANK; LIMITATIONS OF LIABILITY; INDEMNITY</h1><br/>
				<p>THE FOREGOING SHALL CONSTITUTE WASHINGTON FEDERAL BANK FOR SAVINGS’ (THE “BANK”) AND THE SERVICE'S ENTIRE LIABILITY AND YOUR EXCLUSIVE REMEDY. IN NO EVENT SHALL THE BANK OR THE SERVICE BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES, INCLUDING LOST PROFITS (EVEN IF ADVISED OF THE POSSIBILITY THEREOF) ARISING IN ANY WAY OUT OF THE INSTALLATION, USE, OR MAINTENANCE OF THE EQUIPMENT, SOFTWARE, AND/OR THE SERVICE.  YOU AGREE NEITHER THE BANK NOR THE SERVICE IS RESPONSIBLE FOR ENSURING THE ACCURACY OR COMPLETENESS OF THE INFORMATION YOU ACCESS PURSUANT TO THIS AGREEMENT, WHETHER PROVIDED BY THE BANK, ANY BILLER AND/OR ANY THIRD PARTY TO THIS AGREEMENT AND, FURTHER, ARE NOT RESPONSIBLE FOR ANY LIABILITY OTHER THAN THOSE EXPRESSLY ASSUMED HEREIN.</p><br/>
				<p>ACCESS TO OUR WEBSITE MAY BE INTERRUPTED BY HARDWARE OR SOFTWARE FAILURES OR MALFUNCTIONS, DISRUPTION IN PUBLIC UTILITY AVAILABILITY, IMPROPER MAINTENANCE, MALICIOUS, INTENTIONAL OR NEGLIGENT ACTS OR OMISSIONS, ACTS OF GOD, ACTS OF WAR, RIOT, CIVIL COMMOTION, FLOOD FIRE, EARTHQUAKE OR LIKE NATURAL DISASTERS, AND YOU AGREE THAT NEITHER THE BANK NOR THE SERVICE SHALL BE RESPONSIBLE FOR ANY DISRUPTION OF SERVICE, WHETHER DIRECT OR INDIRECT, OR ANY LOSS, COST OR EXPENSE, OR DAMAGE OF ANY KIND YOU CONTEND YOU INCURRED OR SUSTAINED AS A RESULT, DIRECT OR INDIRECT, OF SUCH DISRUPTION OF SERVICE.</p><br/>
				<p>Unless caused by our intentional misconduct or gross negligence, you agree to indemnify, defend and hold harmless Washington Federal Bank for Savings and its affiliates, officers, directors, employees, consultants, agents, service providers, and licensors from any and all third party claims, liability, damages, expenses and costs (including, but not limited to, reasonable attorneys’ fees) caused by or arising from third party claims, disputes, action or allegation of infringement, misuse, or misappropriation based on information, data, file, or otherwise in connection with the Online Banking or Mobile Banking; your violation or any law or rights of a third party or your use or use by a third party of Online Banking or Mobile Banking.</p><br/>
				<h1>GOVERNING LAW</h1><br/>
				<p>This Agreement shall be governed by and construed in accordance with the laws of the State of Illinois and by applicable Federal laws and regulations.</p><br/>
				<h1>ASSIGNMENT</h1><br/>
				<p>This agreement may not be assigned to any other party by you. The Bank may assign or delegate, in part or in whole, to any third party.</p><br/>
				<h1>AMENDMENTS</h1><br/>
				<p>Terms and conditions of this agreement may be amended in whole or part at any time. In such event, Washington Federal Bank for Savings shall provide online notice through Online Banking. If you do not agree with the change(s), you must notify us in writing prior to the effective date to cancel your access. If you use Mobile Banking or Online Banking after a change becomes effective, you have agreed to any and all changes. Amendments or changes to term(s) or condition(s) may be made without prior notice if it does not result in higher fees, more restrictive service use, or increased liability to you</p><br/>
				<h1>ENTIRE AGREEMENT</h1><br/>
				<p>Accounts and services provided will continue to be subject to any separate agreements governing them, except where noted in this Agreement, and the following:</p><br/>
				<p>•	Our rules, procedures, and policies applicable to each account and each service; <br/>
					•	The rules and regulations of any funds transfer system used in connection with Online Banking; and<br/> 
					•	Applicable state and federal laws and regulations. </p><br/>
				<p>This Agreement supplements those and any other agreements or disclosures related to your accounts(s). If there is a conflict between this Agreement and any others, or any statements made by employees or agents, this Agreement shall supersede. Your use of Online Banking and its related modules, including Mobile Banking, is considered your acceptance of these terms and conditions.</p><br/>
				<p>You agree to accept this disclosure online rather than a paper disclosure.</p><br/>
				<h1>DISPUTES</h1><br/>
				<p>In the event of a dispute regarding Mobile Banking, you and Washington Federal Bank for Savings agree to resolve the dispute by looking to this Agreement. You agree that this Agreement is the complete and exclusive statement of the agreement between you and Washington Federal Bank for Savings, which supersedes any proposal or prior agreement, oral or written, and any other communications between you and Washington Federal Bank for Savings relating to the subject matter of this Agreement. If there is a conflict between what one of Washington Federal Bank for Savings employees says and the terms of this Agreement, the terms of this Agreement have final control.</p><br/>
				<h1>NO WAIVER</h1><br/>
				<p>Washington Federal Bank for Savings shall not be deemed to have waived any of its rights or remedies hereunder unless such waiver is in writing and signed by Washington Federal Bank for Savings. No delay or omission on the part of Washington Federal Bank for Savings in exercising any right or remedy shall operate as a waiver of such right or remedy or any other rights or remedies. A waiver on any particular occasion shall not be construed as a bar or waiver of any rights or remedies on future occasions.</p><br/>
				<p>MAIN BRANCH<br/>
					2869 South Archer Avenue<br/>
					Chicago, Illinois 60608<br/>
					Phone: 773-254-3422<br/>
					Fax: 773-254-6022</p><br/>

				<p>TAYLOR ST. BRANCH<br/>
					1410 West Taylor Street<br/>
					Chicago, Illinois 60607<br/>
					Phone: 312-455-2940<br/>
					Fax: 312-455-2948</p><br/>
				



			</div>
		</div>

	</div>
	<!--- ====== Content ====== ---> 

	<!--- ====== Footer ====== --->
	<footer> 
		<p> &copy; Washington Federal Bank for Savings<br>
			2012 - 2013 | All Rights Reserved</p>

		<a href="#" class="btn">Jump to Desktop Site</a>
	</footer>

	<!--- ====== / Footer ====== ---> 

	<!--- Scripts ---> 
	<script src="http://code.jquery.com/jquery.js"></script> 

	<!--- Responsive Navigation ---> 
	<script src="js/retina.js"></script> 
	
	<!--- / Scripts --->

</body>
</html>