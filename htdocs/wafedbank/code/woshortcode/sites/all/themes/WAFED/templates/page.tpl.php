<?php

function is_mobile() {

	// Get the user agent

	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	// Create an array of known mobile user agents
	// This list is from the 21 October 2010 WURFL File.
	// Most mobile devices send a pretty standard string that can be covered by
	// one of these.  I believe I have found all the agents (as of the date above)
	// that do not and have included them below.  If you use this function, you 
	// should periodically check your list against the WURFL file, available at:
	// http://wurfl.sourceforge.net/


	$mobile_agents = Array(
		"240x320",
		"acer",
		"acoon",
		"acs-",
		"abacho",
		"ahong",
		"airness",
		"alcatel",
		"amoi",
		"android",
		"anywhereyougo.com",
		"applewebkit/525",
		"applewebkit/532",
		"asus",
		"audio",
		"au-mic",
		"avantogo",
		"becker",
		"benq",
		"bilbo",
		"bird",
		"blackberry",
		"blazer",
		"bleu",
		"cdm-",
		"compal",
		"coolpad",
		"danger",
		"dbtel",
		"dopod",
		"elaine",
		"eric",
		"etouch",
		"fly ",
		"fly_",
		"fly-",
		"go.web",
		"goodaccess",
		"gradiente",
		"grundig",
		"haier",
		"hedy",
		"hitachi",
		"htc",
		"huawei",
		"hutchison",
		"inno",
		"ipad",
		"ipaq",
		"ipod",
		"jbrowser",
		"kddi",
		"kgt",
		"kwc",
		"lenovo",
		"lg ",
		"lg2",
		"lg3",
		"lg4",
		"lg5",
		"lg7",
		"lg8",
		"lg9",
		"lg-",
		"lge-",
		"lge9",
		"longcos",
		"maemo",
		"mercator",
		"meridian",
		"micromax",
		"midp",
		"mini",
		"mitsu",
		"mmm",
		"mmp",
		"mobi",
		"mot-",
		"moto",
		"nec-",
		"netfront",
		"newgen",
		"nexian",
		"nf-browser",
		"nintendo",
		"nitro",
		"nokia",
		"nook",
		"novarra",
		"obigo",
		"palm",
		"panasonic",
		"pantech",
		"philips",
		"phone",
		"pg-",
		"playstation",
		"pocket",
		"pt-",
		"qc-",
		"qtek",
		"rover",
		"sagem",
		"sama",
		"samu",
		"sanyo",
		"samsung",
		"sch-",
		"scooter",
		"sec-",
		"sendo",
		"sgh-",
		"sharp",
		"siemens",
		"sie-",
		"softbank",
		"sony",
		"spice",
		"sprint",
		"spv",
		"symbian",
		"tablet",
		"talkabout",
		"tcl-",
		"teleca",
		"telit",
		"tianyu",
		"tim-",
		"toshiba",
		"tsm",
		"up.browser",
		"utec",
		"utstar",
		"verykool",
		"virgin",
		"vk-",
		"voda",
		"voxtel",
		"vx",
		"wap",
		"wellco",
		"wig browser",
		"wii",
		"windows ce",
		"wireless",
		"xda",
		"xde",
		"zte"
	);

	// Pre-set $is_mobile to false.

	$is_mobile = false;

	// Cycle through the list in $mobile_agents to see if any of them
	// appear in $user_agent.

	foreach ($mobile_agents as $device) {

		// Check each element in $mobile_agents to see if it appears in
		// $user_agent.  If it does, set $is_mobile to true.

		if (stristr($user_agent, $device)) {

			$is_mobile = true;

			// break out of the foreach, we don't need to test
			// any more once we get a true value.

			break;
		}
	}

	return $is_mobile;
}
?>


<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>


<?php if ((is_mobile()) && ($node->nid == 65)) { ?>


	<?php include 'mobile-banking.php'; ?>

<?php } else if ((is_mobile()) && ($node->nid == 27)) { ?>

	<?php include 'faqs.php'; ?>

<?php } else { ?>
	<link href="<?php echo $base_path . $directory; ?>/css/BenchNine.css" rel='stylesheet' type='text/css'>

	<? if (($title != 'Home')) { ?>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="/wafed/sites/all/themes/WAFED/js/jquery.mousewheel.min.js"></script>
		<script src="/wafed/sites/all/themes/WAFED/js/slickcustomscroll.js"></script>
		<script>
			$( document ).ready( function() {
				$( "div[rel='scrollArea']" ).customscroll( { direction: "vertical" } );
				$( "div[rel='scrollAreaLegal']" ).customscroll( { direction: "vertical" } );
				$(".translate-this-button").css("margin-left","137px");
				document.getElementById('translate-this').style.marginLeft = '137px';
			});
		</script>
		<?
	} else if ($base_path == '/wafed/') {
		//header( 'Location: node/1' ) ;
	}
	?>
	<script>
					
		var externalURL = 'http://';
		function leavingWAFED (url) {
			//alert(url);
			externalURL = url;
			document.getElementById('leaving_url').innerHTML = url;
			document.getElementById('leaving').style.display = 'block';
		}
		function leaving_cancel () {
			document.getElementById('leaving').style.display = 'none';
		}
		function leaving_cont () {
			document.location = externalURL;
		}
		var loginOpenFlag = false;
		function changeClass (elementID, newClass) {
			var element = document.getElementById(elementID);
			element.setAttribute("class", newClass); //For Most Browsers
			element.setAttribute("className", newClass); //For IE; harmless to other browsers.
		}
		function onDrupalControls () {
			document.getElementById('drupalControls').style.display = 'block';
		}
		function offDrupalControls () {
			document.getElementById('drupalControls').style.display = 'none';
		}
		function triggerLoginArea () {
			if (loginOpenFlag == false) {
				document.getElementById('loginArea').style.display = 'block';
				loginOpenFlag = true;
			} else {
				document.getElementById('loginArea').style.display = 'none';
				loginOpenFlag = false;
			}
		}
					
		//	function onCheckingHover() {
		//		document.getElementById('personal-saving').style.display = 'none';		
		//		document.getElementById('personal-borrowing').style.display = 'none';	
		//		document.getElementById('personal-checking').style.display = 'block';		
		//	}
		//		
		//	function offCheckingHover() {
		//		document.getElementById('personal-checking').style.display = 'none';		
		//	}	
		//	
		//	function onSavingHover() {
		//		document.getElementById('personal-checking').style.display = 'none';
		//		document.getElementById('personal-borrowing').style.display = 'none';		
		//		document.getElementById('personal-saving').style.display = 'block';	
		//		
		//	}	
		//	
		//	function offSavingHover() {
		//		document.getElementById('personal-saving').style.display = 'none';		
		//	}
		//	
		//	function onBorrowingHover() {
		//		document.getElementById('personal-borrowing').style.display = 'block';		
		//		document.getElementById('personal-checking').style.display = 'none';
		//		document.getElementById('personal-saving').style.display = 'none';	
		//	}
		//	
		//	function offBorrowingHover() {
		//		document.getElementById('personal-borrowing').style.display = 'none';		
		//	}
		//	
		//	function offAll() {
		//		document.getElementById('personal-checking').style.display = 'none';		
		//			
		//	}
					
					
		function onCalcNav() {
			document.getElementById('calc_subNav').style.display = 'block';
			//document.getElementById('sub_footer').style.marginTop = '235px';
			changeClass('calcFooterTab','onSub');
		}
		function offCalcNav() {
			document.getElementById('calc_subNav').style.display = 'none';
			document.getElementById('sub_footer').style.marginTop = '15px';
			changeClass('calcFooterTab','');
		}
		function onLegal() {
			document.getElementById('legal').style.display = 'block';
		}
		function offLegal() {
			document.getElementById('legal').style.display = 'none';
		}
					

					
					
					
					
					 

	</script>
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="/wafed/sites/all/themes/WAFED/ie7.css" />
	<![endif]-->






	<div id="bg-wrapper">
		<div id="page-wrapper"><div id="page">

				<header id="header" role="banner"><div class="section clearfix">



						<div id="header_common">
							<div id="main_logo">
								<a href="<? echo $base_path; ?>">
									<div id="wafed_logo"></div>
								</a>
							</div>

							<div id="common_nav_block">
								<?php if ($page['header']): ?>
									<div id="page_header"><?php print render($page['header']); ?></div>
								<?php endif; ?>
							</div>
						</div>

						<div id="header_personal">
							<div id="online_login" onmouseover="javascript:triggerLoginArea();" onmouseout="javascript:triggerLoginArea();">
								<a><img src="<? echo $base_path . $directory; ?>/graphics/nav_bankinglogin.png" alt="Online Login"></a>
								<div id="loginArea">
									<iframe src="https://www.online.wafedbank.com/servlet/SLogin?template=/c/custom/rloginsc.vm" frameborder="0" id="iFrameLogin" scrolling="no" ></iframe> <?php echo '<a id="demo_link" href="https://www.online.wafedbank.com/servlet/SLogin?template=/c/login/sloginsc.vm&demo=yes">Click For Demo</a>'; ?>
								</div>
							</div>
							<ul id="personal_nav">

								<?php if (is_mobile()) { ?>
									<li><a><img src="<? echo $base_path . $directory; ?>/graphics/nav_personal_banking.png" alt="Personal Banking"></a><?php print theme('links', array('links' => menu_navigation_links('menu-personal-nav-mobile'), 'attributes' => array('class' => array('links', 'menu-personal-nav')))); ?></li>
								<?php } else { ?>


									<li><a><img src="<? echo $base_path . $directory; ?>/graphics/nav_about_us.png" alt="About Us"></a><?php print theme('links', array('links' => menu_navigation_links('menu-main-menu-about'), 'attributes' => array('class' => array('links', 'menu-personal-nav')))); ?></li>


									<li><a><img src="<? echo $base_path . $directory; ?>/graphics/nav_personal_banking.png" alt="Personal Banking"></a><?php print theme('links', array('links' => menu_navigation_links('menu-personal-nav'), 'attributes' => array('class' => array('links', 'menu-personal-nav')))); ?></li>

								<?php }
								?>



								<li><a><img src="<? echo $base_path . $directory; ?>/graphics/nav_business_banking.png" alt="Business Banking"></a><?php print theme('links', array('links' => menu_navigation_links('menu-business-nav'), 'attributes' => array('class' => array('links', 'menu-business-nav')))); ?></li>
								<li><a><img src="<? echo $base_path . $directory; ?>/graphics/nav_tools-and-services.png" alt="Calculators"></a><?php print theme('links', array('links' => menu_navigation_links('menu-calculators'), 'attributes' => array('class' => array('links', 'menu-calculators')))); ?></li>
								<li><a href="/wafed/contactus"><img src="<? echo $base_path . $directory; ?>/graphics/nav_help.png" alt="Help"></a></li>
							</ul>
						</div>


					</div></header> 

				<?php if ($page['sidebar_first']): ?>
					<div id="sub-top-nav"><div class="section">
							<?php //print render($page['sidebar_first']);      ?>
						</div></div> <!-- /.section, /#sidebar-first -->
				<?php endif; ?>

				<div id="main-wrapper">
					<div id="main" class="clearfix<?php
			if ($main_menu) {
				print ' with-navigation';
			}
				?>">

						<div id="content" class="column" role="main">
							<div class="section">
								<?php print render($page['content']); ?>
								<?php print $feed_icons; ?>
							</div>
						</div>
					</div></div> <!-- /#main, /#main-wrapper -->

				<footer id="footer" role="contentinfo">





					<div class="section">
						<div id="services_subNav">

							<div class="subFooterNav first_subNavCol">
								<h5>About Us</h5>
								<div  id="personal-checking" >
									<?php print theme('links', array('links' => menu_navigation_links('menu-main-menu-about'), 'attributes' => array('class' => array('links', 'menu-footernav-personal-checking')))); ?>
								</div>
							</div>

							<div class="subFooterNav">
								<h5>Personal Banking</h5>
								<div  id="personal-saving">
									<?php print theme('links', array('links' => menu_navigation_links('menu-personal-nav'), 'attributes' => array('class' => array('links', 'menu-footernav-personal-savings')))); ?>
								</div>
							</div>



							<div class="subFooterNav">
								<h5>Business Banking</h5>
								<?php print theme('links', array('links' => menu_navigation_links('menu-footernav-business-checking'), 'attributes' => array('class' => array('links', 'menu-footernav-business-checking')))); ?>
							</div>

							<div class="subFooterNav">
								<h5>Tools & Services</h5>
								<div id="personal-borrowing"  >
									<?php print theme('links', array('links' => menu_navigation_links('menu-footer-nav-tools-and-servic'), 'attributes' => array('class' => array('links', 'menu-footernav-personal-borrow')))); ?>
									<?php print theme('links', array('links' => menu_navigation_links('menu-footer-nav-tools'), 'attributes' => array('class' => array('links', 'menu-footernav-personal-borrow')))); ?>
								</div>	
							</div>
							<!--											<div class="subFooterNav">
																			<h5>Other Services</h5>
							<?php //  print theme('links', array('links' => menu_navigation_links('menu-footernav-other-services'), 'attributes' => array('class' => array('links', 'menu-footernav-other-services')))); ?>
																		</div>-->
						</div>	

						<div id="main_footer">
							<div id="footer_links">
								<ul>
									<li><a>&copy; Washington Federal Bank for Savings 2012 - 2013 | All Rights Reserved </a></li>
									<li><a href="/wafed/privacy">Privacy &AMP; Security |</a></li>
									<li><a href="#">NMLS# 410352</a></li>
								</ul>
							</div>
							<div id="footer_fdic">
								<a href="http://portal.hud.gov/hudportal/HUD"><img src="<? echo $base_path . $directory; ?>/graphics/fdic_1.png" alt="FDIC"></a>
								<a href="http://www.fdic.gov"><img src="<? echo $base_path . $directory; ?>/graphics/fdic_2.png" alt="FDIC"></a>
								<!--							<map name="planetmap">
																<area shape="rect" coords="0,0,50,24" alt="Sun" href="http://portal.hud.gov/hudportal/HUD">
																<area shape="rect" coords="51,1,86,29" alt="Mercury" href="http://www.fdic.gov">
															</map>-->
							</div>
						</div>
						<div style="clear:both;"></div>
					</div></footer> <!-- /.section, /#footer -->

			</div></div> <!-- /#page, /#page-wrapper -->
	</div>


	<?php print render($page['footer']); ?>
	<a id="openDrupalControls" href="javascript:onDrupalControls();">+</a>
	<div id="drupalControls">
		<a id="closeDrupalControls" href="javascript:offDrupalControls();">x</a>
		<?php if ($main_menu): ?>
			<div class="section">
				<?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'clearfix')), 'heading' => array('text' => t('Main menu'), 'level' => 'h2', 'class' => array('element-invisible')))); ?>
				<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'clearfix')), 'heading' => array('text' => t('Secondary menu'), 'level' => 'h2', 'class' => array('element-invisible')))); ?>
			</div> <!-- /.section, /#navigation -->
		<?php endif; ?>
		<?php if ($page['highlighted']): ?>
			<div id="highlighted"><?php print render($page['highlighted']); ?></div>
		<?php endif; ?>
		<?php if ($breadcrumb): ?>
			<div id="breadcrumb"><?php print $breadcrumb; ?></div>
		<?php endif; ?>
		<?php print $messages; ?>
		<?php print render($title_prefix); ?>
		<?php if ($title): ?>
			<h1 class="title" id="page-title"><?php print $title; ?></h1>
		<?php endif; ?>
		<?php print render($title_suffix); ?>
		<?php if ($tabs): ?>
			<div class="tabs"><?php print render($tabs); ?></div>
		<?php endif; ?>
		<?php print render($page['help']); ?>
		<?php if ($action_links): ?>
			<ul class="action-links"><?php print render($action_links); ?></ul>
		<?php endif; ?>

		<?php if ($page['sidebar_second']): ?>
			<div id="sidebar-second" class="column sidebar" role="complementary"><div class="section">
					<?php print render($page['sidebar_second']); ?>
				</div></div> <!-- /.section, /#sidebar-second -->
		<?php endif; ?>

	</div>

	<div id="leaving">
		<img src="imgs/grn_bar.jpg" alt="Washington Federal Savings Bank" />
		<p>You are leaving the Washington Federal Bank for Savings website. The website you have selected ('<span id="leaving_url"></span>') is external and is located on another server. Washington Federal Bank for Savings has no responsibility for any external website. Thank you for visiting and we hope to see you again soon. </p>
		<span id="leaving_btns">
			<input type="button" id="leaving_cont" value="Continue" onclick="leaving_cont();"/>
			<input type="button" id="leaving_cancel" value="Cancel" onclick="leaving_cancel();" />
		</span>
	</div>

	<div id="legal">
		<div id="scrollBox" class="terms">
			<div id="ratesScrollArea" rel="scrollAreaLegal">
				<span id="termsClose"><a href="javascript:offLegal();">close</a> <a href="javascript:offLegal();"><img src="<? echo $base_path . $directory; ?>/graphics/x-close.gif" alt="x"></a></span>
				<h4>TERMS & CONDITIONS:</h4>
				<p>By accessing our web site, you agree to be bound by all of the terms and conditions contained in this disclosure. Please read these terms and conditions carefully. "We", "our" or "us" refers to Washington Federal Bank for Savings, its affiliates and its officers, directors, employees and agents. "You" and "your" refers to the user of this web site. </p>
				<p>You should always verify information on this web site with us directly before relying on it. We change our products and services from time to time, and this may affect the products and services described here. Products and services described here may not in fact be available, although we do try to keep this web site both accurate and current. We do not make any warranties or representations as to the accuracy of the information on our web site.</p>Â 
				<h4>LIMITATION OF LIABILITY</h4>
				<p>You agree that we will not be liable to you or anyone else for any damages of any kind which may result from your visiting this web site or providing any information by means of this web site. The damages for which we are not responsible include, but are not limited to, direct, indirect, special, incidental, consequential, initiative or exemplary damages regardless of how those damages might arise, whether by contract, negligence, tort, strict liability or any other theory. You accept all the risks associated with transmitting information to us, and us transmitting information to you.Â </p>
				<h4>LINKS</h4>
				<p>If we provide a link to any other site, and you use that link, you accept all responsibility for any damages resulting from your use of that linked site.</p>
				<h4>AUTHORIZED ACCESS</h4>
				<p>The contents of this web site are controlled exclusively by Washington Federal Bank for Savings.Â  Unauthorized access, modification or attempts to circumvent established security systems is expressly prohibited.Â  Any violations of this prohibition will be prosecuted to the fullest extent of the law.</p>
			</div>
		</div>
	</div>

	<!-- ====================================== 
		POPUP
	====================================== -->
	<!-- a href="#" class="trigger">Click Me for the modal</a -->
	<!-- JS for Modal Popup -->
	<script src="/wafed/sites/all/themes/WAFED/js/bootstrap-modal.js"></script>
	<script>
		$('.trigger').click(function(){
			$('#myModal').modal('toggle');
		});
	</script>
	<!-- / JS for Modal Popup -->

	<!-- Leaving Notification MODAL -->
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog">
		<div class="modal-header">
			<img src="<? echo $base_path . $directory; ?>/graphics/poup_header.png" alt="wafed">
		</div>
		<div class="modal-body">
			<p>You are leaving the Washington Federal Bank for Savings website. The website you have selected is external and is located on another server. Washington Federal Bank for Savings has no responsibility for any external website. Thank you for visiting and we hope to see you again soon.</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn_shiny">Continue</a>
			<a href="#" class="btn_shiny" data-dismiss="modal">Cancel</a>
		</div>
	</div>
	<!-- / Leaving Notification MODAL -->

	<!-- ====================================== 
		/ POPUP
	====================================== -->
<?php } ?>