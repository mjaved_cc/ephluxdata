function validateForm(){
// This function checks for empty required fields
// With Netscape focus is placed on empty fields
// Inputs are hard coded, nothing is passed to it
// It returns a true or false depending on validity 
	var amount = document.forms['cacl'].amount.value
	var payment = document.forms['cacl'].payment.value
	var rate = document.forms['cacl'].rate.value
	var months = document.forms['cacl'].months.value
	var comma = ","
	var temparry = new Array(10)

	temparray = amount.split(comma)
	amount = temparray.join("")
	temparray = payment.split(comma)
	payment = temparray.join("")
	temparray = rate.split(comma)
	rate = temparray.join("")
	temparray = months.split(comma)
	months = temparray.join("")


	if (isNaN(amount)) {
		alert("Amount must be a number!");
		document.forms['cacl'].amount.focus();
		return false;
	}
	if (isNaN(payment)) {
		alert("Payment must be a number!");
		document.forms['cacl'].payment.focus();
		return false;
	}
	if (isNaN(rate)) {
		alert("Rate must be a number!");
		document.forms['cacl'].rate.focus();
		return false;
	}
	if (isNaN(months)) {
		alert("Length of Loan must be a number!");
		document.forms['cacl'].months.focus();
		return false;
	}
	
	if(((amount != 0) && (amount != "")) &&
		((payment != 0) && (payment != ""))){ 
		alert("Please clear either the loan payment or the loan amount to continue.");
		document.forms['cacl'].amount.focus();
		return false;
	}
	if(((amount == 0) || (amount == "")) &&
		((payment == 0) || (payment == ""))){ 
		alert("You must select either the loan payment or the loan amount!");
		document.forms['cacl'].amount.focus();
		return false;
	}
	if((rate == 0) || (rate == "")){ 
		alert("You must select a loan rate!");
		document.forms['cacl'].rate.focus();
		return false;
		}
	else {
		rate = rate/1200;
	}

	if((months == 0) || (months == "")){
		alert("You must provide the term of the loan!");
		document.forms['cacl'].months.focus();
		return false;
		}
	else {
		if(document.forms['cacl'].frequency.options[document.forms['cacl'].frequency.selectedIndex].value == '1'){
			months = months * 12;
		}
	}

	if(payment == 0 || payment == ""){
		document.forms['cacl'].payment.value = parseInt(100 * ((amount * ( rate / (1 - (Math.pow(1 + rate, -months))))) + .005)) / 100;
	}
	else {
		document.forms['cacl'].amount.value = parseInt(100 * ((((Math.pow(1 + rate, -months)*(-payment + (Math.pow(1 + rate, months)* payment))))/rate) + .005)) / 100;
	}
	return false;
}