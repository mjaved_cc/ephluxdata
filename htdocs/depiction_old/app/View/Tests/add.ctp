<!-- File: /app/View/Posts/add.ctp -->

<h1>Add Post</h1>
<?php

echo $this->Form->create('Test');
echo $this->Form->input('number');
echo $this->Form->input('name', array('rows' => '1'));
echo $this->Form->input('data');
echo $this->Form->end('Save Post');   //generates a submit button and ends the form.
?>