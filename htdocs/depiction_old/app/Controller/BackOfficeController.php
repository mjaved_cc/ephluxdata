<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BackOfficeController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Tmp', 'UserSocialAccount', 'UserAssetsTag', 'UserAsset', 'UserAssetsUserAssetsTag', 'UserAssetsLike', 'UserAssetsComment', 'UserAssetsDislike', 'UserFollowership', 'TmpImage', 'UserNotification', 'UserAsset', 'UserAssetsMention', 'UserAssetTag');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('signin', 'signup','admin' );
	}

	function index() {
		
			die('Welcome');
	}

	/*
	 * @desc: login - for web interface
	 */

	function login() {
 
		$this->set('schema_user', $this->User->schema());
		if ($this->request->is('post')) {
			if ($this->Auth->login() && $this->Auth->User('role_id') == ROLE_ID_ADMIN) {

				if (empty($this->request->data['User']['remember_me'])) {
					$this->RememberMe->delete();
				} else {
					$this->RememberMe->remember(
							$this->request->data['User']['username'], $this->request->data['User']['password']
					);
				}

				unset($this->request->data['User']['remember_me']);
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
				$this->logout(); // in case if user other than admin logins in so we need to expire it session
			}
		}
	}

	
	function admin(){	
		
		
		//pr($this);
		
		//	die('Welcome');
	}

}

