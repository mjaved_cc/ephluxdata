<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Tmp', 'UserSocialAccount', 'UserAssetsTag', 'UserAsset', 'UserAssetsUserAssetsTag', 'UserAssetsLike', 'UserAssetsComment', 'UserAssetsDislike', 'UserFollowership', 'TmpImage', 'UserNotification', 'UserAsset', 'UserAssetsMention', 'UserAssetTag');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('signin', 'signup', 'verify_account', 'recover', 'facebook', 'dummy', 'get_all_posts', 'search', 'insert_posts', 'like_post', 'delete_dislike_post', 'dislike_post', 'unlike_post', 'follow', 'unfollow', 'get_post', 'get_user_profile', 'get_liked_posts', 'get_followers', 'test_pagination', 'get_following', 'tmp_image', 'get_notifications', 'allow_follow_request', 'remove_follow_request', 'get_tags','comment');
	}

	function index() {
		die('Welcome');
	}

	/*
	 * @desc: login - for web interface
	 */

	function login() {

		$this->set('schema_user', $this->User->schema());
		if ($this->request->is('post')) {
			if ($this->Auth->login() && $this->Auth->User('role_id') == ROLE_ID_ADMIN) {

				if (empty($this->request->data['User']['remember_me'])) {
					$this->RememberMe->delete();
				} else {
					$this->RememberMe->remember(
							$this->request->data['User']['username'], $this->request->data['User']['password']
					);
				}

				unset($this->request->data['User']['remember_me']);
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
				$this->logout(); // in case if user other than admin logins in so we need to expire it session
			}
		}
	}

	function check_email_availability() {
		$email = $this->request->query['email'];
		$this->set('is_already_exist', $this->User->is_email_already_exists($email));
	}

	function success() {
		die('Success');
	}

	/*
	 * @desc: Signup 
	 * @params: username , password
	 * @return: null
	 */

	function signup() {
		// http://book.cakephp.org/2.0/en/models/saving-your-data.html

		if ($this->request->is('post') && !empty($this->request->data['signup_data'])) {



			$json_data = $this->request->data['signup_data'];
			$data = json_decode($json_data, true);
			$file = array();
			$first_name = $data['first_name'];
			$last_name = $data['last_name'];
			$email = $data['email'];
			$password = AuthComponent::password($data['password']);
			$username = $data['username'];
			$image_url = '';


			if (!$this->is_valid_email($email))
				$error_code = ERR_CODE_INVALID_EMAIL;
			else if ($this->User->is_email_already_exists($email))
				$error_code = ERR_CODE_EMAIL_ALREADY_EXISTS;
			else if ($this->User->is_username_already_exists($username))
				$error_code = ERR_CODE_USERNAME_ALREADY_EXISTS;
			else if ($this->Tmp->is_unverified_email_exists($email))
				$error_code = ERR_CODE_UNVERIFIED_EMAIL;
			else {

				if (!empty($_FILES['file'])) {
					$file = $_FILES['file'];



					if (is_uploaded_file($file['tmp_name'])) {

						$image_url = $this->saveToFile($file, IMAGE_PROFILE); //file url
					}
				}

				$user_info['username'] = $username;
				$user_info['password'] = AuthComponent::password($password);
				$user_info['ip'] = $this->get_numeric_ip_representation();
				$user_info['email'] = $email;
				$user_info['first_name'] = $first_name;
				$user_info['last_name'] = $last_name;
				$user_info['url'] = $image_url;
				$this->Tmp->create();
				$this->Tmp->set('target_id', $email);
				$this->Tmp->set('data', json_encode($user_info));
				$this->Tmp->set('type', 'registration');
				$this->Tmp->set('expire_at', $this->get_tmp_expiry());

				if ($this->Tmp->save()) {

					$signup_link = $this->_get_signup_link($email);

					$this->send_email($email, SITE_NAME . ': Account Activation', $signup_link, 'signup');

					$error_code = ERR_CODE_SUCCESS;
				} else
					$error_code = ERR_CODE_FAILURE;
			}
		} else
			$error_code = ERR_CODE_INVALID_PARAMS;

		$error_msg = $this->get_error_msg($error_code);

		$this->set(compact('error_code', 'error_msg'));
	}

	/*
	 * @desc: get signup link for email verification
	 * @params: email <string>
	 * @return: signup_link <string>
	 */

	function _get_signup_link($email) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		$verification_code = substr(md5($code), 8, 5);

		$signup_link = DEPECTION_BASE_URI . '/' . $this->params['controller'] . '/' . "verify_account?v=$verification_code&email=" . $email;
		return $signup_link;
	}

	/*
	 * @desc: verify email & move it from Tmp table to user table
	 * @params: v (verification code) , email 
	 */

	function verify_account() {

		if (!empty($this->request->query['v']) && !empty($this->request->query['email']) && $this->is_valid_email($this->request->query['email'])) {
			$verification_code = $this->request->query['v'];
			$email = $this->request->query['email'];
			$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
			if (substr(md5($code), 8, 5) == $verification_code) {

				$tmp_details = $this->Tmp->get_details($email, 'registration');
				$data = json_decode($tmp_details['Tmp']['data']);

				if (!empty($data)) {

					$this->User->create();
					$this->User->set('first_name', $data->first_name);
					$this->User->set('last_name', $data->last_name);
					$this->User->set('email', $data->email);
					$this->User->set('ip_address', $data->ip);
					$this->User->set('username', $data->username);
					$this->User->set('password', $data->password);
					$this->User->set('role_id', ROLE_ID_USER);
					$this->User->set('url', $data->url);
					if ($this->User->save()) {
						$error_code = ERR_CODE_SUCCESS;
						$this->Tmp->delete($tmp_details['Tmp']['id']);
					} else
						$error_code = ERR_CODE_FAILURE;
				} else
					$error_code = ERR_CODE_ACCOUNT_ALREADY_ACTIVATED;
			} else
				$error_code = ERR_CODE_INVALID_VCODE;
		} else
			$error_code = ERR_CODE_INVALID_PARAMS;

		$error_msg = $this->get_error_msg($error_code);

		$this->set(compact('error_code', 'error_msg'));
	}

	/*
	 * @desc: signin service
	 * @param: 
	 * 	    type_id - can be facebook or twitter
	 * 		signin_data - data which is necessary for signin based on type_id
	 */

	function signin() {
		if ($this->request->is('post') && !empty($this->request->data['signin_data'])) {
			$json_data = $this->request->data['signin_data'];
			$user_info = json_decode($json_data, true);
			$type_id = strtolower($user_info['type_id']);
			// query_params is WHERE clause
			$query_params = array();


			if ($type_id == TYPE_ID_DEPICTION) {

				// query params is WHERE clause.
				$query_params['User.username'] = $user_info['username'];
				$query_params['User.password'] = AuthComponent::password($user_info['password']);
			} else if ($type_id == TYPE_ID_FACEBOOK) {

				// social network user id
				$query_params['UserSocialAccount.link_id'] = $user_info['data']['id'];
				$query_params['UserSocialAccount.type_id'] = $type_id;
				$social_details = $this->UserSocialAccount->get_details($query_params);

				$function_name = '_save_' . $type_id . '_info';
				$user_id = $this->$function_name($user_info, $type_id, $social_details);

				unset($query_params);
				$query_params['User.id'] = $user_id;
			}

			// Be sure to manually add the new User id to the array passed to the login method. Otherwise you wont have the user id available. 
			// Reference URL: http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#manually-logging-users-in
			$user_details = $this->User->get_details($query_params);
			if ($type_id == TYPE_ID_FACEBOOK) {
				$user_details['facebook_id'] = $user_info['data']['id'];  //facebook data in returned json
			}

			if ($user_details && $this->Auth->login($user_details['User']))
				$error_code = ERR_CODE_SUCCESS;
			else {
				$error_code = ERR_CODE_FAILURE;
				$this->Auth->logout();
			}

			$this->set(compact('user_details', 'type_id'));
		} else
			$error_code = ERR_CODE_INVALID_PARAMS;
		$error_msg = $this->get_error_msg($error_code);

		$this->set(compact('error_code', 'error_msg'));
	}

	/*
	 * @desc: save facebook information
	 * @params: 
	 * 		user_info - info get from twitter api
	 * 		type_id - facebook	
	 * 		social_details - social account details
	 */

	function _save_facebook_info($user_info, $type_id, $social_details) {

		$access_token = $user_info['data']['access_token'];

		if (empty($social_details)) {

			$this->User->create();
			$facebook_id = $user_info['data']['id'];

			if (!empty($user_info['data']['picture']['data']['url']))
				$profile_image_url = $user_info['data']['picture']['data']['url'];
			else
				$profile_image_url = null;

			$this->User->set('first_name', (!empty($user_info['data']['first_name'])) ? $user_info['data']['first_name'] : '');
			$this->User->set('last_name', (!empty($user_info['data']['last_name'])) ? $user_info['data']['last_name'] : '');
			$this->User->set('ip_address', $this->get_numeric_ip_representation());
			$this->User->set('role_id', ROLE_ID_USER);
			$this->User->set('url', $profile_image_url);
			$this->User->save();
			$user_id = $this->User->id;

			$this->UserSocialAccount->create();
			$this->UserSocialAccount->set('user_id', $user_id);
			$this->UserSocialAccount->set('image_url', $profile_image_url);
			$this->UserSocialAccount->set('link_id', $facebook_id);
			$this->UserSocialAccount->set('type_id', $type_id);
			$this->UserSocialAccount->set('screen_name', $user_info['data']['name']);
			$this->UserSocialAccount->set('access_token', $access_token);
			$this->UserSocialAccount->set('ip_address', $this->get_numeric_ip_representation());
			$this->UserSocialAccount->save();
		} else {
			// refreshing access token
			if ($access_token != $social_details['UserSocialAccount']['access_token']) {
				$this->UserSocialAccount->id = $social_details['UserSocialAccount']['id'];
				$this->UserSocialAccount->set('access_token', $access_token);
				$this->UserSocialAccount->save();
			}

			$user_id = $social_details['UserSocialAccount']['user_id'];
		}
		return $user_id;
	}

	/*
	 * @desc: check if social account already registered or not
	 * @params: 
	 * 		type_id - (facebook , twitter)
	 * 		link_id - social network user id
	 */

	function check_sacc_availability() {

		if ($this->request->is('post') && $this->request->data['json_data']) {
			$json_data = json_decode($this->request->data['json_data'], true);
			$user_id = $this->UserSocialAccount->get_userid_by_linkid($json_data['type_id'], $json_data['link_id']);

			if (empty($user_id))
				$error_code = ERR_CODE_SUCCESS;
			else
				$error_code = ERR_CODE_FAILURE;
		} else
			$error_code = ERR_CODE_INVALID_PARAMS;
		$error_msg = $this->get_error_msg($error_code);

		$this->set(compact('error_code', 'error_msg'));
	}

	function recover() {

		$this->layout = 'custom';
		$success = 0;
		if ($this->request->is('post')) {
			// in db email is basically refered as username
			$username = $this->request->data['email'];
			if (!empty($username) && $this->is_valid_email($username)) {
				$user_id = $this->User->is_email_already_exists($username);
				if ($user_id) {

					$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$'), 0, 10);
					$this->User->id = $user_id;
					$this->User->set('password', AuthComponent::password($password));
					if ($this->User->save()) {
						$message = "Your password got reset to \r\n" . $password;

						$this->send_email($username, SITE_NAME . ': Password Reset', $message);

						$msg = 'Check out your emails now to reset your password straight away.';
						$success = 1;
					} else {
						$msg = 'Due to an error your request cannot be fulfilled. Kindly try again.';
					}
				} else {
					$msg = "Hmmm. We don't recognise that email address. Give it another shot.";
				}
			} else {
				$msg = 'A valid email address is required.';
			}
		}
		$this->set(compact('msg', 'success'));
	}

	function logout() {

		$this->RememberMe->delete();
		$this->redirect($this->Auth->logout());
	}

	function signin1() {
		
	}

	function facebook() {
		
	}

	public function insert_posts() {

		//debug($_POST); die;

		$tags_new_arr = array();
		$tags_db_arr = array();
		$insert_post = array();
		$mentions = array();
		$sound_url = "(NULL)";
		$image_url = "(NULL)";
		$bool = TRUE;

		if (!empty($_FILES['sound_file']) && $this->request->is('post') && !empty($this->request->data['insert_data'])) {

			$sound = $_FILES['sound_file'];
			$json_insert = json_decode($this->request->data['insert_data'], true); //json recieved from post
			$hash = $json_insert['hash'];

			$tmp_image = $this->TmpImage->query("SELECT image_url,id from tmp_images where hash='{$hash}'");
			//pr($tmp_image[0]['tmp_images']['image_url']); 
			if (!empty($tmp_image)) {

				$image_url = $tmp_image[0]['tmp_images']['image_url'];   //tmp image url
				$image_id = $tmp_image[0]['tmp_images']['id'];   //tmp image id
				$new_image_url = str_replace(IMAGE_TMP, IMAGE_PATH, $image_url);  //create new directory path
				$image_name = explode("images/", $image_url);   //get image name

				$moveFrom = WWW_ROOT . 'uploads/' . IMAGE_TMP . DS . $image_name[1];  //path from where file is moved
				$moveTo = WWW_ROOT . 'uploads/' . IMAGE_PATH . DS . $image_name[1]; //path to where file is moved
				//pr($image_name[1]); pr($new_image_url); pr($moveFrom); pr($moveTo); pr($tmp_image);
			} else {
				die('no image found');
			}
			//die;

			if (is_uploaded_file($sound['tmp_name'])) {

				$sound_url = $this->saveToFile($sound, SOUND_PATH); //file url
			} else {
				die('sound file could not be uploaded');
			}
			//suppress the warning message using @
			if (@rename($moveFrom, $moveTo)) {
				$insert_post['user_id'] = $json_insert['user_id'];
				$insert_post['uploaded_sound_url'] = $sound_url;
				$insert_post['image_url'] = $new_image_url;
				$insert_post['sound_id'] = $json_insert['sound-id'];
				$insert_post['position'] = $json_insert['position'];
				$insert_post['hash_tags'] = $json_insert['hash_tags'];
				$insert_post['tags_mentions'] = $json_insert['tags_mentions'];
				$insert_post['mentions'] = $json_insert['mentions'];

				$this->UserAsset->create();
				$this->UserAsset->set('image_url', $insert_post['image_url']);
				$this->UserAsset->set('sound_bucket_id', $insert_post['sound_id']);
				$this->UserAsset->set('uploaded_sound_url', $insert_post['uploaded_sound_url']);
				$this->UserAsset->set('user_id', $insert_post['user_id']);
				$this->UserAsset->set('position', $insert_post['position']);
				$this->UserAsset->set('tags', $insert_post['tags_mentions']);

				if ($this->UserAsset->save()) {
					$user_asset_id = $this->UserAsset->id;

					$mentions_form = explode(',', $insert_post['mentions']);

					/* creating array for insertion of mentions in notifications */
					$mentions_notification = array();

					if (!empty($mentions_form)) {

						foreach ($mentions_form as $key => $value) {  //filling mentions array for bulk insertion 
							$mentions_notification[$key]['UserNotification']['source_id'] = $insert_post['user_id'];
							$mentions_notification[$key]['UserNotification']['target_id'] = $value;
							$mentions_notification[$key]['UserNotification']['user_asset_id'] = $user_asset_id;
							//$mentions_notification[$key]['UserNotification']['type'] = 1; //type 2 is for asset/posts in notifications
							$mentions_notification[$key]['UserNotification']['action'] = 'HAS_MENTIONED_ASSET';
							$mentions_notification[$key]['UserNotification']['created'] = date("Y-m-d H:i:s");
							$mentions_notification[$key]['UserNotification']['modified'] = date("Y-m-d H:i:s");
						}

						$this->insert_notifications('HAS_MENTIONED', $mentions_notification);
					}


					/* bulk insertion of mentions */
					/* 	if (!empty($mentions_form)) {

					  foreach ($mentions_form as $key => $value) {  //filling mentions array for bulk insertion
					  $mentions[$key]['UserAssetsMention']['user_id'] = $value;
					  $mentions[$key]['UserAssetsMention']['user_assets_id'] = $user_asset_id;
					  }
					  $this->UserAssetsMention->custom_saveAll($mentions);

					  } */

					$tags_form = explode(',', $insert_post['hash_tags']);

					if (!empty($tags_form)) {
						//pr($tags_form);
						$tags_db = $this->UserAssetsTag->find('all', array('fields' => 'UserAssetsTag.tag'));
						foreach ($tags_db as $index => $tmp) {
							$tags_db_arr[$index] = $tmp['UserAssetsTag']['tag'];
						}
						//pr($tags_db_arr);
						$tags_new = array_diff($tags_form, $tags_db_arr);  //match form arr with db and return new tags 
						//pr($tags_new);


						if (!empty($tags_new)) {

							foreach ($tags_new as $key => $tag) {
								$tags_new_arr[$key]['UserAssetsTag']['tag'] = $tag;  /* creating array for bulk insertion */
							}
							$this->UserAssetsTag->custom_saveAll($tags_new_arr); //bulk insert of new tags to db
						}


						//***********insertion logic of new table*******
						$a = '';

						foreach ($tags_form as $tg) {
							$a.= '\'' . $tg . '\',';
						}
						$b = substr($a, 0, -1); // removing , from the end
						//pr($b);
						$tag_idz = $this->UserAssetsTag->query("SELECT  id FROM user_assets_tags where BINARY tag  IN($b) ;"); //get idz of given tags				
						$idz = array();
						foreach ($tag_idz as $key => $value) {
							$idz[$key]['UserAssetsUserAssetsTag']['tag_id'] = $value['user_assets_tags']['id'];
							$idz[$key]['UserAssetsUserAssetsTag']['user_assets_id'] = $user_asset_id;
							$idz[$key]['UserAssetsUserAssetsTag']['type'] = 1; //type 1 is for asset-posts
						}
						//pr($idz);

						$this->UserAssetsUserAssetsTag->custom_saveAll($idz);  //bulk insertion of tags and postid to UserAssetsUserAssetsTag
						//******************
						if (!empty($image_id)) {
							$this->TmpImage->delete($image_id);
						}
						die('Post inserted successfully');
					} else {
						die('Post could not be inserted');
					}
				}
			}   //moving file
			else {
				die('system cannot find the file to move');
			}
		} else {
			die('invalid parameters');
		}



		/* 	if (!empty($this->request->data['insert']) && is_uploaded_file($this->request->data['insert']['File']['tmp_name'])) {
		  $fileData = fread(fopen($this->request->data['insert']['File']['tmp_name'], "r"), $this->request->data['insert']['File']['size']);

		  $file = $this->request->data['insert']['File'];
		  //pr($file);
		  $fileURL = $this->saveToFile($file); //file url
		  echo($fileURL);

		  //	$this->request->data['insert']['File']['name'];
		  //	$this->request->data['insert']['File']['type'];
		  //	$this->request->data['insert']['File']['size'];
		  //	$this->request->data['insert']['File']['data']=$fileData;

		  pr(json_encode($this->request->data['insert'], true));
		  //pr($fileData);
		  //pr($_FILES);
		  }
		 */
	}

	function dummy($email) {
		$this->autoRender = false;
		echo $this->_get_signup_link($email);
	}

	function get_all_posts() {
		if ($this->request->is('post') && is_numeric($this->request->data['get_data'])) {


			$id = $this->request->data['get_data'];
			$get_all_posts = $this->UserAsset->find('first');
			if (empty($get_all_posts)) {
				die('No posts to Display');
			}
			//$posts=json_encode($get_all_posts);
			//pr($posts);
//				echo $posts;
			//pr($get_all_posts);
			$getposts = $this->User->query("SELECT   IF(likes.user_id ={$id} ,'true','false')AS liked ,posts.user_id,posts.id AS
'user_asset_id',usrs.username,usrs.url AS 'profile_image',posts.image_url,posts.sound_bucket_id,posts.uploaded_sound_url,
posts.created AS created,posts.position,posts.tags ,posts.likes,posts.dislikes
FROM   user_assets posts
INNER JOIN users usrs ON usrs.id=posts.user_id
LEFT  JOIN (SELECT user_id,user_assets_id FROM user_assets_likes WHERE user_id={$id}) likes ON likes.user_assets_id=posts.id");

			if (!empty($getposts)) {
				foreach ($getposts as $key => $value) {
					$getposts[$key]['posts']['created'] = $this->facebook_style_date_time(strtotime($getposts[$key]['posts']['created']));
				}

				echo json_encode($getposts);
				die;
			} else {
				echo"some thing went wrong ";
				die;
			}
		} else {
			die('invalid parameter');
		}
	}

	function like_post() {


		if ($this->request->data['insert_like']) {

			$json_insert = json_decode($this->request->data['insert_like'], true);

			if (is_numeric($json_insert['user_id']) && is_numeric($json_insert['post_id'])) {

				$uid = $json_insert['user_id'];  /* user who liked the post */
				$pid = $json_insert['post_id']; /* asset id liked by the user */
				$target_id = $this->UserAsset->get_user_id($pid); /* get user id passing post id */
				$json_insert['target_id'] = $target_id[0]['user_assets']['user_id'];

				$this->UserAssetsLike->create();
				$liked = $this->UserAssetsLike->query("SELECT id FROM user_assets_likes WHERE user_assets_id={$pid}															AND user_id={$uid};");
				if (empty($liked)) {
					//pr($liked); die;
					$this->UserAssetsLike->set('user_id', $uid);
					$this->UserAssetsLike->set('user_assets_id', $pid);
					//$this->UserAssetsLike->set('created',$this->get_current_datetime);
					if ($this->UserAssetsLike->save()) {


						$likes_count = $this->UserAssetsLike->query(
								"SELECT COUNT(*) FROM user_assets_likes WHERE user_assets_id={$pid};");

						$count_likes = $likes_count[0][0]['COUNT(*)'];

						$this->UserAsset->create();
						$this->UserAsset->id = $pid;
						$this->UserAsset->set('likes', $count_likes);
						$this->UserAsset->save();
						//$post_id= $this->UserAsset->findById($insert_like['post_id']);
						$this->insert_notifications('HAS_LIKED', $json_insert); //inserting notification 
						die('post liked successfully');
					}
				} else {
					die('post already liked');
				}
			} else {
				die('invalid parameters');
			}
		} else {
			die('invalid parameters');
		}
	}

	function unlike_post() {


		if ($this->request->data['delete_like']) {

			$json_insert = json_decode($this->request->data['delete_like'], true);

			if (is_numeric($json_insert['user_id']) && is_numeric($json_insert['post_id'])) {

				$uid = $json_insert['user_id'];  /* user who liked the post */
				$pid = $json_insert['post_id']; /* asset id liked by the user */

				$this->UserAssetsLike->create();
				$like_id = $this->UserAssetsLike->query("SELECT id from user_assets_likes WHERE user_id={$uid} AND user_assets_id={$pid}");
				if (empty($like_id)) {
					die('post cannot be unlike');
				}
				//pr($like_id[0]['user_assets_likes']['id']);
				$delete_like_id = $like_id[0]['user_assets_likes']['id'];
				if ($this->UserAssetsLike->delete($delete_like_id)) {

					$likes_count = $this->UserAssetsLike->query(
							"SELECT COUNT(*) FROM user_assets_likes WHERE user_assets_id={$pid};");

					$count_likes = $likes_count[0][0]['COUNT(*)'];

					$this->UserAsset->create();
					$this->UserAsset->id = $pid;
					$this->UserAsset->set('likes', $count_likes);
					$this->UserAsset->save();


					die('user successfully unliked the post');
				}
			} else {
				die('invalid parameters');
			}
		} else {
			die('invalid parameters');
		}
	}

	function dislike_post() {


		if ($this->request->data['insert_dislike']) {

			$json_insert = json_decode($this->request->data['insert_dislike'], true);

			if (is_numeric($json_insert['user_id']) && is_numeric($json_insert['post_id'])) {

				$uid = $json_insert['user_id'];  /* user who liked the post */
				$pid = $json_insert['post_id']; /* asset id liked by the user */


				$this->UserAssetsDislike->create();
				$liked = $this->UserAssetsDislike->query("SELECT id FROM user_assets_dislikes WHERE user_assets_id={$pid}															AND user_id={$uid};");
				if (empty($liked)) {
					//pr($liked); die;
					$this->UserAssetsDislike->set('user_id', $uid);
					$this->UserAssetsDislike->set('user_assets_id', $pid);
					//$this->UserAssetsLike->set('created',$this->get_current_datetime);
					if ($this->UserAssetsDislike->save()) {


						$likes_count = $this->UserAssetsDislike->query(
								"SELECT COUNT(*) FROM user_assets_dislikes WHERE user_assets_id={$pid};");

						$count_likes = $likes_count[0][0]['COUNT(*)'];

						$this->UserAsset->create();
						$this->UserAsset->id = $pid;
						$this->UserAsset->set('dislikes', $count_likes);
						$this->UserAsset->save();
						//$post_id= $this->UserAsset->findById($insert_like['post_id']);
						die('post disliked successfully');
					}
				} else {
					die('post already disliked');
				}
			} else {
				die('invalid parameters');
			}
		} else {
			die('invalid parameters');
		}
	}

	function delete_dislike_post() {


		if ($this->request->data['delete_dislike']) {

			$json_insert = json_decode($this->request->data['delete_dislike'], true);

			if (is_numeric($json_insert['user_id']) && is_numeric($json_insert['post_id'])) {

				$uid = $json_insert['user_id'];  /* user who liked the post */
				$pid = $json_insert['post_id']; /* asset id liked by the user */

				$this->UserAssetsDislike->create();
				$like_id = $this->UserAssetsDislike->query("SELECT id from user_assets_dislikes WHERE user_id={$uid} AND user_assets_id={$pid}");
				if (empty($like_id)) {
					die('dislike cannot be remove from post');
				}
				//pr($like_id[0]['user_assets_likes']['id']);
				$delete_like_id = $like_id[0]['user_assets_dislikes']['id'];
				if ($this->UserAssetsDislike->delete($delete_like_id)) {

					$likes_count = $this->UserAssetsDislike->query(
							"SELECT COUNT(*) FROM user_assets_dislikes WHERE user_assets_id={$pid};");

					$count_likes = $likes_count[0][0]['COUNT(*)'];

					$this->UserAsset->create();
					$this->UserAsset->id = $pid;
					$this->UserAsset->set('dislikes', $count_likes);
					$this->UserAsset->save();


					die('user successfully removed dislike from post');
				}
			} else {
				die('invalid parameters');
			}
		} else {
			die('invalid parameters');
		}
	}

	function facebook_style_date_time($timestamp) {
		$difference = time() - $timestamp;
		$periods = array("sec", "min", "hour", "day", "week", "month", "years", "decade");
		$lengths = array("60", "60", "24", "7", "4.35", "12", "10");

		if ($difference > 0) { // this was in the past time
			$ending = "ago";
		} else { // this was in the future time
			$difference = -$difference;
			$ending = "to go";
		}
		for ($j = 0; $difference >= $lengths[$j]; $j++)
			$difference /= $lengths[$j];
		$difference = round($difference);
		if ($difference != 1)
			$periods[$j].= "s";
		$text = "$difference $periods[$j] $ending";
		return $text;
	}

	function get_tags() {
		if (!empty($this->request->data['hash_tag'])) {

			$hash_tag = $this->request->data['hash_tag'];
			$result = $this->UserAssetsTag->get_tags($hash_tag);

			if ($result == 0) {
				die('no tags found');
			} else {
				die(json_encode($result));
			}
		}
	}

	function search() {

		//echo xTimeAgo ('2012-10-12 11:25:36', time(),'s' ); die;
		if (!empty($this->request->data['search_data'])) {
			//pr($this->request->data);


			$json_data = $this->request->data['search_data'];
			$search_info = json_decode($json_data, true);

			if ($search_info['search_type'] == 'user') {


				$search_data = $search_info['data'];
				$user_id = $search_info['user_id'];

				$result = $this->User->search_user($user_id, $search_data);

				if ($result == 0) {
					die('no result found');
				} else {
					echo json_encode($result);
					die;
				}
			} elseif ($search_info['search_type'] == 'tags' && is_numeric($search_info['tag_id']) && is_numeric($search_info['page'])) {

				if ($search_info['page'] == 0) {
					die('page no cannot be zero');
				}

				$page_form = $search_info['page']; /* page no we get from form */
				$pageNo = $page_form - 1; //get this from form parameters --page #

				$tag_id = $search_info['tag_id'];
				$result = $this->UserAssetsTag->get_posts_tags($tag_id, $pageNo); //returns posts based on the basis of tags 

				if ($result == 0) {
					die('no tags found');
				} else {
					die(json_encode($result));
				}
			} else {
				die('invalid parameters');
			}

//			if ($search_info['search_type'] == 'tags') {
//
//
//				$search_data = $search_info['data'];
//				$search_type = $search_info['search_type'];
//				//echo 'Search=' . $search_type . '  Data=' . $search_data;
//
//				$tags_db = $this->UserAssetsTag->find('all', array('fields' => 'UserAssetsTag.tag'));
//				foreach ($tags_db as $index => $tmp) {
//					$tags_db_arr[$index] = $tmp['UserAssetsTag']['tag'];
//				}
//				//pr($tags_db_arr);
//				$input = preg_quote($search_data, '~'); /* don't forget to quote input string! */
//				$result = preg_grep('~' . $input . '~', $tags_db_arr); /* matches input tags with db and returns matched */
//				//pr($input);
//				//	pr($result);
//				$results_for_query = '';
//
//				foreach ($result as $tg) {
//					$results_for_query.= '\'' . $tg . '\',';
//				}
//				$formated_results_for_query = substr($results_for_query, 0, -1); /* removing "," from the end of string */
//
//				//	pr($formated_results_for_query);
//
//				if (!empty($formated_results_for_query)) {
//					$result_tags_images = $this->UserAssetsTag->query("SELECT tags.id ,tags.tag,posts.image_url,posts.id 
//								FROM user_assets_tags tags
//								INNER JOIN user_assets_user_assets_tags userassetstags ON userassetstags.tag_id=tags.id
//								INNER JOIN user_assets posts ON userassetstags.user_assets_id=posts.id
//								WHERE tags.tag IN ({$formated_results_for_query});");
//					//pr($result_tags_images);
//					echo json_encode($result_tags_images);
//					//$this->set(compact('formated_results_for_query', 'error_msg'));
//				} else {
//					echo "no tags matched";
//				}
//
//				//echo json_encode($result);
//				die;
//				/*
//				  $error_code = ERR_CODE_SUCCESS;
//				  $error_code = ERR_CODE_FAILURE;
//				  $error_code = ERR_CODE_INVALID_PARAMS;
//				  $error_msg = $this->get_error_msg($error_code);
//				  $this->set(compact('error_code', 'error_msg'));
//				 */
//				//array_intersect(array1,array2,array3...)
//			}
		}
	}

	function comment() {

		$tags_new_arr = array();
		$tags_db_arr = array();
		$insert_comment = array();

		if (!empty($_FILES['sound_file']) && $this->request->is('post') && !empty($this->request->data['insert_data'])) {

			$sound = $_FILES['sound_file'];
			$json_insert = json_decode($this->request->data['insert_data'], true); //json recieved from post

			if (is_uploaded_file($sound['tmp_name'])) {

				$sound_url = $this->saveToFile($sound, COMMENTS_PATH); //file url
			} else {
				die('sound file could not be uploaded');
			}


			$insert_comment['uploaded_sound_url'] = $sound_url;
			$insert_comment['user_id'] = $json_insert['user_id'];
			$insert_comment['user_assets_id'] = $json_insert['user_assets_id'];
			$insert_comment['sound_id'] = $json_insert['sound-id'];
			$insert_comment['position'] = $json_insert['position'];
			$insert_comment['hash_tags'] = $json_insert['hash_tags'];
			$insert_comment['tags_mentions'] = $json_insert['tags_mentions'];
			$insert_comment['mentions'] = $json_insert['mentions'];

			$this->UserAssetsComment->create();
			$this->UserAssetsComment->set('user_id', $insert_comment['user_id']);
			$this->UserAssetsComment->set('user_assets_id', $insert_comment['user_assets_id']);
			$this->UserAssetsComment->set('tags', $insert_comment['tags_mentions']); //dumping mentions and tags to comments
			$this->UserAssetsComment->set('audio_url', $insert_comment['uploaded_sound_url']);
			$this->UserAssetsComment->set('sound_bucket_id', $insert_comment['sound_id']);
			$this->UserAssetsComment->set('position', $insert_comment['position']);

			if ($this->UserAssetsComment->save()) {
				$comment_id = $this->UserAssetsComment->id;

				$mentions_form = explode(',', $insert_comment['mentions']);

				/* creating array for insertion of mentions in notifications */
				$mentions_notification = array();

				if (!empty($mentions_form)) {

					foreach ($mentions_form as $key => $value) {  //filling mentions array for bulk insertion 
						$mentions_notification[$key]['UserNotification']['source_id'] = $insert_comment['user_id'];
						$mentions_notification[$key]['UserNotification']['target_id'] = $value;
						$mentions_notification[$key]['UserNotification']['user_asset_id'] = $comment_id;
						//$mentions_notification[$key]['UserNotification']['type'] = 2; //type 2 is for comments in notifications
						$mentions_notification[$key]['UserNotification']['action'] = 'HAS_MENTIONED_COMMENT';
						$mentions_notification[$key]['UserNotification']['created'] = date("Y-m-d H:i:s");
						$mentions_notification[$key]['UserNotification']['modified'] = date("Y-m-d H:i:s");
					}

					$this->insert_notifications('HAS_MENTIONED', $mentions_notification);
				}


				$tags_form = explode(',', $insert_comment['hash_tags']);

				if (!empty($tags_form)) {
					//pr($tags_form);
					$tags_db = $this->UserAssetsTag->find('all', array('fields' => 'UserAssetsTag.tag'));
					foreach ($tags_db as $index => $tmp) {
						$tags_db_arr[$index] = $tmp['UserAssetsTag']['tag'];
					}
					//pr($tags_db_arr);
					$tags_new = array_diff($tags_form, $tags_db_arr);  //match form arr with db and return new tags 
					//pr($tags_new);


					if (!empty($tags_new)) {

						foreach ($tags_new as $key => $tag) {
							$tags_new_arr[$key]['UserAssetsTag']['tag'] = $tag;  /* creating array for bulk insertion */
						}
						$this->UserAssetsTag->custom_saveAll($tags_new_arr); //bulk insert of new tags to db
					}


					//***********insertion logic of new table*******
					$a = '';

					foreach ($tags_form as $tg) {
						$a.= '\'' . $tg . '\',';
					}
					$b = substr($a, 0, -1); // removing , from the end
					//pr($b);
					$tag_idz = $this->UserAssetsTag->query("SELECT  id FROM user_assets_tags where BINARY tag  IN($b) ;"); //get idz of given tags				
					$idz = array();
					foreach ($tag_idz as $key => $value) {
						$idz[$key]['UserAssetsUserAssetsTag']['tag_id'] = $value['user_assets_tags']['id'];
						$idz[$key]['UserAssetsUserAssetsTag']['user_assets_id'] = $comment_id;
						$idz[$key]['UserAssetsUserAssetsTag']['type'] = 2; //type 2 is for comment
					}
					//pr($idz);

					$this->UserAssetsUserAssetsTag->custom_saveAll($idz);  //bulk insertion of tags and commentid to UserAssetsUserAssetsTag
					//******************

					die('Comment inserted successfully');
				} else {
					die('Comment could not be inserted');
				}
			}
			//moving file
		} else {
			die('invalid parameters');
		}
	}

	function follow() {

		if (!empty($this->request->data['follow_data'])) {
			//pr($this->request->data);


			$json_data = $this->request->data['follow_data'];
			$follow_info = json_decode($json_data, true);
			$user_id = $follow_info['user_id'];
			$target_id = $follow_info['target_id'];

			if (is_numeric($target_id) && is_numeric($user_id)) {
				$following = $this->UserFollowership->is_following($follow_info);
				if (!empty($following)) {
					die('already following');
				}
				$this->UserFollowership->create();
				//$is_private = $this->UserFollowership->query("SELECT is_private FROM users WHERE id={$target_id};");
				$is_private = $this->UserFollowership->is_private($target_id);
				$iss_private = $is_private[0]['users']['is_private'];

				if (is_null($iss_private)) {
					die('user you are trying to follow does not exists');
				} //if user does not exists

				if (is_numeric($iss_private) && $iss_private == 0) { //0 means profile is not private and any one can follow
					if ($target_id == $user_id) {
						die('u cannot follow yourself');
					} //check for not allowing user to follow himself

					$this->UserFollowership->create();
					//$following = $this->UserFollowership->query("SELECT id FROM user_followerships WHERE user_id={$user_id}															AND target_id={$target_id};");
					$this->UserFollowership->set('user_id', $user_id);
					$this->UserFollowership->set('target_id', $target_id);
					$this->UserFollowership->set('type_id', 'follower');

					if ($this->UserFollowership->save()) {
						$this->insert_notifications('HAS_FOLLOWED', $follow_info);  //inserting notifications
						die('user followed successfully');
					}
				} else {
					$notification_id = $this->UserNotification->is_notification_request_follow($follow_info);
					if (empty($notification_id)) {
						$this->insert_notifications('REQUEST_FOLLOW', $follow_info);
						die('An approval request has been sent to the user you are trying to follow');
					} else {
						die('u have already requested to follow this user');
					}
				}
			} else {
				die('invalid parameters');
			}
		}
	}

	function unfollow() {

		if (!empty($this->request->data['unfollow_data'])) {
			//pr($this->request->data);


			$json_data = $this->request->data['unfollow_data'];
			$search_info = json_decode($json_data, true);
			$user_id = $search_info['user_id'];
			$target_id = $search_info['target_id'];
			if (is_numeric($target_id) && is_numeric($user_id)) {

				$this->UserFollowership->create();
				$following = $this->UserFollowership->query("SELECT id FROM user_followerships WHERE user_id={$user_id}															AND target_id={$target_id};");
				//pr($following[0]['user_followerships']['id']); 
				if (!empty($following) && $this->UserFollowership->delete($following[0]['user_followerships']['id'])) {

					die('user successfully unfollowed');
				} else {
					die('cannot unfollow user');
				}
			} else {
				die('invalid parameters');
			}
		} else {
			die('invalid parameters');
		}
	}

	function get_post() {
		if ($this->request->data['get_post']) {

			$get_post = json_decode($this->request->data['get_post'], true);

			if (is_numeric($get_post['user_id']) && is_numeric($get_post['post_id']) && isset($get_post['post_id'])) {

				$uid = $get_post['user_id'];  /* user who liked the post */
				$pid = $get_post['post_id']; /* asset id liked by the user */

				$this->UserAssetsLike->create();
				$post_info = $this->UserAssetsLike->get_single_post($uid, $pid);

				//pr($post_info); die;
				if (!empty($post_info) && $post_info != 0) {
					echo json_encode($post_info);
					die;
				} else {
					die('no results found');
				}
			} else {
				die('invalid parameters');
			}
		}
	}

	function get_user_profile() {
		if (is_numeric($this->request->data['get_user'])) {

			$uid = $this->request->data['get_user'];
			$user_profile = array();
			$post = array();
			$user_info = $this->User->get_user_info($uid); //getiing user info

			if (!empty($user_info) && $user_info != 0) {

				$user_posts_info = $this->User->get_user_posts($uid); //getting users posts
				if (!empty($user_posts_info)) {

					foreach ($user_posts_info as $key => $post_info) {
						$post[$key] = $post_info;
					}
					$user_profile['post_info'] = $post;
				}

				$likes_count = $this->User->get_likes_count($uid);   //count of posts liked by user
				$followers_count = $this->User->get_followers_count($uid); //	pr($followers_count[0][0]);
				$following_count = $this->User->get_following_count($uid); //get following users
				//	pr($following_count[0][0]);

				$user_profile['user_info'] = $user_info[0]['user'];
				$user_profile['followers_count'] = $followers_count[0][0]['followers_count'];
				$user_profile['following_count'] = $following_count[0][0]['following_count'];
				$user_profile['likes_count'] = $likes_count[0][0]['likes_count'];
				echo json_encode($user_profile);
				die;
			} else {
				die('invalid user');
			}
		} else {
			die('invalid parameters');
		}
	}

	function get_liked_posts() {
		if (is_numeric($this->request->data['get_liked_user'])) {

			$uid = $this->request->data['get_liked_user'];
			$liked_posts = $this->User->get_liked_posts($uid); //get posts liked by the user


			if ($liked_posts != 0) {

				echo json_encode($liked_posts);
				die;
			} else {
				die('No posts liked');
			}
		}
	}

	//get followers of the given id 
	/*
	  function get_followers(){
	  if (is_numeric($this->request->data['get_followers'])){

	  $uid=$this->request->data['get_followers'];
	  $followers=$this->User->get_followers($uid);


	  if($followers !=0){

	  echo json_encode($followers); die;

	  }
	  else{
	  die('No followers');
	  }
	  }
	  else{ die('invalid parameters');}
	  } */
	//get users following the given id 
	function get_following() {
		if (is_numeric($this->request->data['get_following'])) {

			$uid = $this->request->data['get_following'];
			$following = $this->User->get_following($uid);


			if ($following != 0) {

				echo json_encode($following);
				die;
			} else {
				die('No one following this user');
			}
		} else {
			die('invalid parameters');
		}
	}

	function get_followers() {
		if (is_numeric($this->request->data['get_followers'])) {

			$follower_data = array();
			$following_data = array(); //arrays for comparing results

			$uid = $this->request->data['get_followers'];
			$followers = $this->User->get_followers($uid, 'followers');  //returns followers of passed id

			if ($followers != 0) {
				//	pr($followers); 
				$following = $this->User->get_followers($uid, 'following'); //returns users passed id is following or followings
				//pr($following);
				foreach ($following as $key => $value) {
					$following_data[$key] = $value['user_followerships']['target_id'];
				}
				//	pr($followers);
				foreach ($followers as $key => $value) {
					$follower_data[$key] = $value['users']['id'];
					$followers[$key]['users']['is_following'] = 'false';
				}
				//	pr($follower_data);
				//	pr($following_data);
				$new_arr = array_intersect($follower_data, $following_data); //returns followers array
				//pr($followers); 
				//pr($new_arr);
				foreach ($new_arr as $key => $value) {
					$followers[$key]['users']['is_following'] = 'true';
				}
			} else {
				die('no followers');
			}
			//pr($followers);
			//die;



			if (!empty($followers)) {

				echo json_encode($followers);
				die;
			}
			if (empty($followers)) {
				die('No followers');
			}
		} else {
			die('invalid parameters');
		}
	}

	function test_pagination() {
		$page_form = $this->request->data['page'];
		$perPage = RESULTS_PER_PAGE;
		$page = $page_form - 1; //get this from form parameters --page #
		$tester = $this->User->query("select * from users LIMIT " . $page * $perPage . "," . $perPage . ";");
		$count = $this->User->query("select count(*) from users;");


		$count = (int) $count[0][0]['count(*)'];
		$pages = $count / RESULTS_PER_PAGE;
		if ($pages >= $page_form) {
			echo"<b>Pages: $page_form of $pages </b>";
			pr($tester);
		} else {
			die('no more pages to display');
		}
	}

	function tmp_image() {

		$image_url = '';
		if (!empty($_FILES['image_file']) && !empty($this->request->data['hash'])) {

			$image = $_FILES['image_file'];
			if (is_uploaded_file($image['tmp_name'])) {

				$image_url = $this->saveToFile($image, IMAGE_TMP); //file url
			}
			$hash = $this->request->data['hash'];
			$this->TmpImage->create();
			$this->TmpImage->set('image_url', $image_url);
			$this->TmpImage->set('hash', $hash);
			if ($this->TmpImage->save()) {
				die('image inserted successfully');
			} else {
				die('image could not be inserted inserted');
			}
		} else {
			die('invalid parameters');
		}
	}

	function insert_notifications($type, $data) {

		if (!empty($type) && !empty($data)) {


			if ($type == 'HAS_LIKED') {
				$uid = $data['user_id'];  /* user who liked the post */
				$pid = $data['post_id']; /* post liked by him */
				$tid = $data['target_id'];  /* id of the user who owns the post */

				$this->UserNotification->create();
				$this->UserNotification->set('source_id', $uid);
				$this->UserNotification->set('target_id', $tid);
				$this->UserNotification->set('user_asset_id', $pid);
				$this->UserNotification->set('action', $type);
				$this->UserNotification->save();
			}

			if ($type == 'HAS_FOLLOWED') {
				$uid = $data['user_id'];  /* user who liked the post */
				$tid = $data['target_id'];

				$this->UserNotification->create();
				$this->UserNotification->set('source_id', $uid);
				$this->UserNotification->set('target_id', $tid);
				$this->UserNotification->set('action', $type);
				$this->UserNotification->save();
			}

			if ($type == 'REQUEST_FOLLOW') {
				$uid = $data['user_id'];  /* user who requested to follow */
				$tid = $data['target_id'];

				$this->UserNotification->create();
				$this->UserNotification->set('source_id', $uid);
				$this->UserNotification->set('target_id', $tid);
				$this->UserNotification->set('action', $type);
				$this->UserNotification->save();
			}

			if ($type == 'HAS_MENTIONED') {

				//pr($data);
				$this->UserNotification->custom_saveAll($data);
			}


			if ($type == 'HAS_COMMENTED') {
				$uid = $data['user_id'];  /* user who requested to follow */
				$tid = $data['target_id'];
				$pid = $data['post_id'];

				$this->UserNotification->create();
				$this->UserNotification->set('source_id', $uid);
				$this->UserNotification->set('target_id', $tid);
				$this->UserNotification->set('user_asset_id', $pid);
				$this->UserNotification->set('action', $type);
				$this->UserNotification->save();
			}
		}
	}

	function get_notifications() {

		if (is_numeric($this->request->data['user_id'])) {

			$uid = $this->request->data['user_id'];

			$get_notifications = $this->UserNotification->get_user_notifications($uid);  /* get all notifications of a user passing the uid */
			if ($get_notifications != 0) {

				foreach ($get_notifications as $key => $noti) {
					$get_notifications[$key]['notifications']['created'] = $this->facebook_style_date_time(strtotime($noti['notifications']['created']));
				}

				echo json_encode($get_notifications);
				die;
			} else {
				die('no notifications');
			}
		} else {
			die('invalid parameters');
		}
	}

	function allow_follow_request() { //private user allowing to follow himself
		if (!empty($this->request->data['follow_data'])) {
			//pr($this->request->data);


			$json_data = $this->request->data['follow_data'];
			$follow_info = json_decode($json_data, true);
			$user_id = $follow_info['user_id'];
			$target_id = $follow_info['target_id'];


			if (is_numeric($target_id) && is_numeric($user_id)) {
				if ($target_id == $user_id) {
					die('u cannot follow yourself');
				} //check for not allowing user to follow himself

				$this->UserFollowership->create();
				//$following = $this->UserFollowership->query("SELECT id FROM user_followerships WHERE user_id={$user_id}															AND target_id={$target_id};");
				$following = $this->UserFollowership->is_following($follow_info);

				if (empty($following)) {
					$this->UserFollowership->set('user_id', $user_id);
					$this->UserFollowership->set('target_id', $target_id);
					$this->UserFollowership->set('type_id', 'follower');

					if ($this->UserFollowership->save()) {
						//$notification_id = $this->UserNotification->query("SELECT id FROM user_notifications WHERE source_id={$user_id} AND																			target_id={$target_id} AND ACTION='REQUEST_FOLLOW';");
						$notification_id = $this->UserNotification->is_notification_request_follow($follow_info);
						if (@is_numeric($notification_id[0]['user_notifications']['id'])) {
							$this->UserNotification->create();
							$this->UserNotification->id = $notification_id[0]['user_notifications']['id'];
							$this->UserNotification->set('action', 'HAS_FOLLOWED');
							$this->UserNotification->save();
							die('user followed successfully');
						} else {
							die('no such notification exists');
						}
					}
				} else {
					die('already following');
				}
			} else {
				die('invalid parameters');
			}
		}
	}

	function remove_follow_request() {

		if (!empty($this->request->data['follow_data'])) {
			//pr($this->request->data);


			$json_data = $this->request->data['follow_data'];
			$follow_info = json_decode($json_data, true);
			$user_id = $follow_info['user_id'];
			$target_id = $follow_info['target_id'];

			$this->UserFollowership->create();
			$notification_id = $this->UserNotification->query("SELECT id FROM user_notifications WHERE source_id={$user_id} AND																			target_id={$target_id} AND ACTION='REQUEST_FOLLOW';");
			if (@is_numeric($notification_id[0]['user_notifications']['id'])) {
				if ($this->UserNotification->delete($notification_id[0]['user_notifications']['id'])) {
					die('follow request removed successfully');
				} else {
					die('follow request could not be removed');
				}
			} else {
				die('no such notification found');
			}
		} else {
			die('invalid parameters');
		}
	}

	function change_profile_status() { /* make profile private or remove private profile */

		if (!empty($this->request->data['user_id']) && is_numeric($this->request->data['user_id'])) {
			//pr($this->request->data);
			$user_id = $this->request->data['user_id'];
			$status = $this->request->data['status'];


			$is_private = $this->User->query("SELECT is_private FROM users WHERE id={$user_id};");

			if (empty($is_private)) {
				die('invalid user id ');
			}

			if (@is_numeric($is_private[0]['users']['is_private'])) {


				$this->User->id = $user_id;
				$this->User->set('is_private', $status);
				if ($this->User->save()) {
					die('status changed successfully');
				} else {
					die('status could not be changed');
				}
			}
		} else {
			die('invalid parameters');
		}
	}

	function admin() {
		
	}

}

