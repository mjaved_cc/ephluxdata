<?php

class UserSocialAccount extends AppModel {

	public $name = 'UserSocialAccount';

	/*
	 * @desc: get user_id by link_id. link_id is ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]
	 * @params: type_id <string> , link_id <int> 
	 * @return: boolean
	 */

	function get_userid_by_linkid($type_id, $link_id) {

		$result = $this->find('first', array(
			'conditions' => array('type_id' => $type_id, 'link_id' => $link_id, 'status' => USA_STATUS_ACTIVE)
				));

		if (!empty($result) && isset($result)) {
			return $result['UserSocialAccount']['user_id'];
		}

		return 0;
	}

	function get_details($query_params) {

		$clause['UserSocialAccount.status'] = USA_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);
			
		return $this->find('first', array(
					'conditions' => array($clause
					)
				));
	}

}