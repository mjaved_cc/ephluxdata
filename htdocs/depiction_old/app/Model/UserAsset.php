<?php

class UserAsset extends AppModel {

	public $name = 'UserAsset';

	/*
	 * @desc: get user_id by link_id. link_id is ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]
	 * @params: type_id <string> , link_id <int> 
	 * @return: boolean
	 */

	 /*get userid passing the post id */
	function get_user_id($pid=0) {

		$result = $this->query("SELECT user_id from user_assets where id={$pid};");

		if (!empty($result) && isset($result)) {
			return $result;
		}

		return 0;
	}

	


}