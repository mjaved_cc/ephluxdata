<?php

class User extends AppModel {

	public $name = 'User';
	var $hasOne = 'UserSocialAccount';   
	/*
	 * @desc: check email existance
	 * @param: email <string>
	 * @return: user id if exists & 0 if not
	 */

	function is_email_already_exists($email) {
		$user = $this->find('first', array(
			'conditions' => array('User.status' => USER_STATUS_ACTIVE, 'User.email' => $email)
				));

		return (!empty($user['User']['id'])) ? $user['User']['id'] : 0;
	}
	
	function is_username_already_exists($username) {
		$user = $this->find('first', array(
			'conditions' => array('User.status' => USER_STATUS_ACTIVE, 'User.username' => $username)
				));

		return (!empty($user['User']['id'])) ? $user['User']['id'] : 0;
	}

	/*
	 * @desc: get details of users
	 * @param: query_params <array>
	 * @return: <array>
	 */

	function get_details($query_params = 0) {

		$clause['User.status'] = USER_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		return $this->find('first', array(
					'conditions' => array($clause
					)
				));
	}	
	
	
	function get_user_info($uid=0){
		if(is_numeric($uid)){
			$result = $this->query("SELECT * from users as user where id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_user_posts($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT * from user_assets posts where user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
	function get_followers_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS followers_count FROM user_followerships WHERE target_id={$uid}");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
 
	
	function get_following_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS following_count FROM user_followerships WHERE user_id={$uid}");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_liked_posts($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT posts.* FROM user_assets AS posts
INNER JOIN  (SELECT user_assets_id FROM user_assets_likes WHERE user_id={$uid}) AS likes ON likes.user_assets_id=posts.id;");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
		function get_likes_count($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT COUNT(*)AS likes_count FROM user_assets_likes  WHERE user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
		function get_followers($uid=0,$type){
		if(is_numeric($uid)&& $type=='followers'){
			$result =$this->query("SELECT `users`.* FROM users AS `users`
INNER JOIN  (SELECT target_id,user_id FROM user_followerships WHERE target_id={$uid}) AS followers ON followers.user_id=`users`.id;");
			if(!empty($result)){
				return $result;
			}
			}
			
			
		if(is_numeric($uid)&& $type=='following'){
			$result =$this->query("SELECT target_id FROM user_followerships WHERE user_id={$uid};");
			if(!empty($result)){
				return $result;
			}
			}	
	
			return 0;
	}
	
		function get_following($uid=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT `users`.* FROM users AS `users`
INNER JOIN  (SELECT target_id FROM user_followerships WHERE user_id={$uid}) AS following ON following.target_id=`users`.id;");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
	function search_user($uid=0,$search_data=0){
		if(is_numeric($uid)){
			$result =$this->query("SELECT `USERS`.username,`USERS`.first_name,`USERS`.last_name,`USERS`.url,`USERS`.id,if(follower.target_id > 0, 'TRUE','FALSE')as following 
from users `USERS`
left join (SELECT target_id,user_id 
		FROM user_followerships 
		WHERE user_id={$uid}) as follower  on `USERS`.id=follower.target_id
WHERE	BINARY `USERS`.first_name LIKE '{$search_data}%' OR 
	BINARY `USERS`.last_name LIKE '{$search_data}%' OR 
	BINARY `USERS`.username LIKE '{$search_data}%';");
			if(!empty($result)){
				return $result;
			}
			}
	
			return 0;
	}
	
	
}