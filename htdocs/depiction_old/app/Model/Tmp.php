<?php

class Tmp extends AppModel {

	public $name = 'Tmp';

	/*
	 * @desc: check email existance
	 * @param: email <string>
	 * @return: boolean
	 */

	function is_unverified_email_exists($email) {
		$is_unverified_exist = $this->find('count', array(
			'conditions' => array('type' => 'registration', 'target_id' => $email)
				));

		return $is_unverified_exist;
	}

	function get_details($target_id, $type) {
		return $this->find('first', array(
					'conditions' => array(
						'target_id' => $target_id,
						'type' => $type
					)
				));
	}
	

}