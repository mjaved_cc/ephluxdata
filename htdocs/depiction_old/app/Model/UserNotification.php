<?php

class UserNotification extends AppModel {

	public $name = 'UserNotification';

	

	 /*get all notifications of a user passing the uid */
	function get_user_notifications($uid=0) {

		$result = $this->query("SELECT notifications.*,`users`.id,`users`.username,`users`.url FROM user_notifications																notifications 
																INNER JOIN users `users` ON `users`.id=notifications.source_id
																WHERE notifications.target_id={$uid} ORDER BY DATE(notifications.modified) DESC;");

		if (!empty($result) && isset($result)) {
			return $result;
		}

		return 0;
	}

	function is_notification_request_follow($follow_info) {

		$result = $this->query("SELECT id FROM user_notifications WHERE source_id={$follow_info['user_id']} AND target_id={$follow_info['target_id']} AND ACTION='REQUEST_FOLLOW';");

		if (!empty($result) && isset($result)) {
			return $result;
		}

		return null;
	}
	

	
	

}