<?php

class UserAssetsLike extends AppModel {

	public $name = 'UserAssetsLike';

	/*
	 * @desc: get user_id by link_id. link_id is ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]
	 * @params: type_id <string> , link_id <int> 
	 * @return: boolean
	 */

	function get_single_post($uid, $pid) {

		$result = $this->query("SELECT IF(liked.id>0,'TRUE','FALSE')AS liked,`users`.first_name,`users`.last_name,`users`.username,`users`.url,`posts`.* 
FROM user_assets `posts`
INNER JOIN users `users` ON `users`.id=`posts`.user_id 
LEFT JOIN (SELECT id,user_id,user_assets_id FROM user_assets_likes WHERE user_id={$uid} AND user_assets_id={$pid})AS liked ON liked.user_assets_id=`posts`.id
WHERE `posts`.id={$pid};");

		if (!empty($result) && isset($result)) {
			return $result;
		}

		return 0;
	}

	


}