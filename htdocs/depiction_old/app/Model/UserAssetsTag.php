<?php

class UserAssetsTag extends AppModel {

	public $name = 'UserAssetsTag';

	/* get all notifications of a user passing the uid */

	function get_tags($tag = 0) {

		$result = $this->query("SELECT id,tag FROM user_assets_tags WHERE tag LIKE '{$tag}%'");

		if (!empty($result) && isset($result)) {
			return $result;
		}

		return 0;
	}

	function get_posts_tags($tid = 0, $pageNo = 0) {  //this functions get the posts based on tag id 
		$result = $this->query("SELECT tags.id ,tags.tag,posts.image_url,posts.id 
FROM user_assets_tags tags
INNER JOIN user_assets_user_assets_tags userassetstags ON userassetstags.tag_id=tags.id
INNER JOIN user_assets posts ON userassetstags.user_assets_id=posts.id
WHERE tags.id ={$tid} LIMIT " . $pageNo * RESULTS_PER_PAGE_TAGS . "," . RESULTS_PER_PAGE_TAGS . ";");


		if (!empty($result) && isset($result)) {


			$count = $this->query("SELECT count(*) 
			FROM user_assets_tags tags
			INNER JOIN user_assets_user_assets_tags userassetstags ON userassetstags.tag_id=tags.id
			INNER JOIN user_assets posts ON userassetstags.user_assets_id=posts.id
			WHERE tags.id ={$tid};");

			$page_count = $count[0][0]['count(*)'] / RESULTS_PER_PAGE_TAGS;
			if ($pageNo + 1 >= $page_count) {
				//$result['page'] = 'No more pages to display';
				$result['page'] = "0";
			} else {
				//$result['page'] = $pageNo + 1 . ' of ' . round($page_count);
				$pg=$pageNo + 1;
				$result['page'] ="{$pg}" ;
			}


			return $result;
		}

		return 0;
	}

	/* function get_posts_tags($tid=0) {  //this functions get the posts based on tag id 

	  $result = $this->query("SELECT tags.id ,tags.tag,posts.image_url,posts.id
	  FROM user_assets_tags tags
	  INNER JOIN user_assets_user_assets_tags userassetstags ON userassetstags.tag_id=tags.id
	  INNER JOIN user_assets posts ON userassetstags.user_assets_id=posts.id
	  WHERE tags.id ={$tid} ;");

	  if (!empty($result) && isset($result)) {
	  return $result;
	  }

	  return 0;
	  } */
}