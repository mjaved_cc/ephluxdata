<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	
	function custom_saveAll($data) {
				
		$first_idx = key($data);
		// check if data is not empty & is of proper format. i.e the one use in saveAll
		if (count($data) > 0 && is_numeric($first_idx)) {
			$value_array = array();

			// table & name defined in lib/Cake/Model/Model.php
			$table_name = $this->table;
			$model_name = $this->name;

			$fields = array_keys($data[$first_idx][$model_name]);
			
			foreach ($data as $key => $value)
				$value_array[] = "('" . implode('\',\'', $value[$model_name]) . "')";			
			
			
			$sql = "INSERT INTO " . $table_name . " (" . implode(', ', $fields) . ") VALUES " . implode(',', $value_array);
			
			$this->query($sql);
			return true;
		}

		return false;
	}
}
