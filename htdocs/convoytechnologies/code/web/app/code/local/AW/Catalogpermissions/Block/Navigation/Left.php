<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Block_Navigation_Left extends Mage_Catalog_Block_Navigation {

    public function getCurrentChildCategories() {

        $categories = parent::getCurrentChildCategories();
        foreach ($categories as $category) {
            $category->unsProductCount();
            $category->setProductCount(count($category->getProductCollection()));
        }

        return $categories;
    }

    public function getStoreCategories() {

        if (Mage::helper('catalog/category_flat')->isEnabled()) {  

            $categories = parent::getStoreCategories();

            $excludedCats = Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE);
            $keys = array_keys($categories);

            foreach ($keys as $key) {
                $i = 0;
                if (in_array($key, $excludedCats)) {
                    unset($categories[$key]);
                    unset($keys[$i]);
                }
                $i++;
            }

            foreach ($categories as $category) {
                if (count($category->getChildrenNodes() > 0)) {
                    $this->processCategory($category, $excludedCats);
                }
            }

            return $categories;
        }

        return parent::getStoreCategories();
    }

    public function processCategory($object, $cats) {
        $childrenNodes = $object->getChildrenNodes();
        if (is_array($childrenNodes) && count($childrenNodes) > 0) {
            foreach ($childrenNodes as $obj) {
                if (in_array($obj->getEntityId(), $cats)) {
                    $obj->setIsActive(0);
                }
                $this->processCategory($obj, $cats);
            }
        }
        return $object;
    }

}
