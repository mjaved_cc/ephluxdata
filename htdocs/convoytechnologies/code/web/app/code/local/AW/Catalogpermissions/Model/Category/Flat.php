<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Model_Category_Flat extends Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Flat {

    public function getCategories($parent, $recursionLevel = 0, $sorted=false, $asCollection=false, $toLoad=true) {

        return parent::getCategories($parent, $recursionLevel, $sorted, $asCollection, true);
    }

    public function getChildrenCategories($category) {

        if (!Mage::helper('catalog/category_flat')->isEnabled() || !is_array(Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE))) {
            return parent::getChildrenCategories($category);
        }
        $categories = parent::getChildrenCategories($category);
        $enabledCategories = array();
        foreach ($categories as $category) {
            if (!in_array($category->getId(), Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE))) {
                $enabledCategories[] = $category;
            }
        }

        return $enabledCategories;
    }

}