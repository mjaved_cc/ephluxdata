<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Model_AdminhtmlObserver {

    public function controllerPredispatch() {

        if (Mage::getStoreConfig('advanced/modules_disable_output/AW_Catalogpermissions')) { return; }
        if (!version_compare(Mage::getVersion(), "1.4.1.1", "<=")) { return; }

        if ($this->_getPathInfo() == 'catalog_product_action_attribute_save') {

            $attributes = array(
                "0" => "aw_catalogpermissions_disable_product",
                "1" => "aw_catalogpermissions_disable_price"
            );

            if ($this->_getPathInfo() == 'catalog_product_action_attribute_save') {

                foreach ($attributes as $attribute) {
                    if (isset($_POST['attributes'][$attribute])) {
                        if (is_array($_POST['attributes'][$attribute])) {
                            if (count($_POST['attributes'][$attribute]) > 0) {
                                $_POST['attributes'][$attribute] = implode(',', $_POST['attributes'][$attribute]);
                            }
                        }
                    }
                }
            }
        }
    }

    private function _getPathInfo($sep = "_") {

        $request = Mage::app()->getRequest();
        return "{$request->getControllerName()}$sep{$request->getActionName()}";
    }
    
    /*
     *  
     * Reinit customer cache sorted by groups of customers
     * 
     */
    
    public function massenablePostdispatch($observer) { 
        
        $types = $observer->getControllerAction()->getRequest()->getParam('types');        
        if (!empty($types)) {
            foreach ($types as $type) {
                if($type == AW_Catalogpermissions_Model_Cache::CACHE_TYPE && Mage::getModel('core/cache')->canUse(AW_Catalogpermissions_Model_Cache::CACHE_TYPE)) {
                    Mage::getModel('catalogpermissions/cache')->refreshCache();                    
                }
            }
        }
       
    }
    
    /*      
     * Reinit customer cache sorted by groups of customers
     */
    
    public function massrefreshPostdispatch($observer) 
    {  
         $this->massenablePostdispatch($observer);        
    }
    
    
    public function abstractModelSaveAfter($observer) {
          
        if($observer->getObject() instanceof Mage_Customer_Model_Group || $observer->getObject() instanceof Mage_Core_Model_Store) {
            if($observer->getObject()->isObjectNew()) {
                Mage::getModel('core/cache')->invalidateType(AW_Catalogpermissions_Model_Cache::CACHE_TYPE);                
            }         
        }
        
        if($observer->getObject() instanceof Mage_Catalog_Model_Product) {
            
             $observer->setProduct($observer->getObject());
             
             Mage::getModel('catalogpermissions/cache')->productSaveAfter($observer); 
             
        }
        
    }

}