<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Block_Product_PriceBundle extends Mage_Bundle_Block_Catalog_Product_Price {
    const DELIMITER=",";

    public function getProduct() {

        if (Mage::getStoreConfig('catalog/frontend/flat_catalog_product')) {
            $product = parent::getProduct();
            $product->load($product->getId());
            return $product;
        }

        return parent::getProduct();
    }

    protected function _toHtml() {
        if (AW_Catalogpermissions_Helper_Data::checkVisibility($this->getProduct(), true)) {
            return AW_Catalogpermissions_Helper_Data::PRICE_RESTRICTED_TEXT;
        }
        return parent::_toHtml();
    }

}
