<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Helper_Connection extends Mage_Core_Helper_Abstract
{
    /**
     * @var obj
     */
    protected $_resource = null;

    /**
     * @var obj
     */
    protected $_connection = null;

    /**
     *
     * @return type
     */
    public static $_lock = false;


    public function getConnection()
    {
        return $this->_initResourceConnection();
    }

    public function getSelect($reset = true)
    {
        $select = $this->_initResourceConnection()->select();
        if ($reset) {
            $select->reset();
        }
        return $select;
    }

    protected function _initResourceConnection()
    {
        if (!is_null($this->_resource) && !is_null($this->_connection)) {
            return $this->_connection;
        }

        $this->_resource = Mage::getSingleton('core/resource');

        return $this->_initConnection();
    }

    private function _initConnection()
    {
        $this->_connection = $this->_resource->getConnection('core_read');
        return $this->_connection;
    }

    public function getTable($name)
    {
        return $this->_resource->getTableName($name);
    }

    public function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    public function isDisabled($productId)
    {
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $keyField = 'entity_id';
        $attrCode = $eavAttribute->getIdByCode('catalog_product', AW_Catalogpermissions_Helper_Data::CP_DISABLE_PRODUCT);
        $attT = Mage::getSingleton('core/resource')->getTableName('catalog/product') . "_text";
        $collection = Mage::getModel('catalog/product')->getCollection();
        $cn = $this->_getCorrelationName($collection);
        $collection->getSelect()
            ->joinLeft(array('_cp_def' => $attT), "_cp_def.entity_id = {$cn}.{$keyField} AND _cp_def.attribute_id = {$attrCode} AND _cp_def.store_id = 0", array())
            ->joinLeft(array('_catalogpermissions_store' => $attT), "_catalogpermissions_store.entity_id = {$cn}.{$keyField} AND _catalogpermissions_store.attribute_id = {$attrCode} AND _catalogpermissions_store.store_id = {$this->getStoreId()}", array())
            ->where("IF(_catalogpermissions_store.value_id > 0, _catalogpermissions_store.value, _cp_def.value) NOT REGEXP '(^|,)" . AW_Catalogpermissions_Helper_Data::getCustomerGroup() . "(,|$)' OR (_catalogpermissions_store.value IS NULL AND _cp_def.value IS NULL)")
            ->where("{$cn}.{$keyField} = ?", $productId);
        if (!count($collection)) {
            return true;
        } else {
            return false;
        }

    }

    public function addDisabledAttrToFilter($collection, $keyField = 'entity_id', array $additional = array())
    {
        /**
         *
         * Product collection load invokes in Mage_Sales_Model_Mysql4_Quote_Item_Collection _afterLoad -> _assignProducts method
         * but we don't delete products which already in cart
         *
         */
        if (get_class($collection) == 'AW_Catalogpermissions_Model_Product_Collection') {
            if (self::$_lock === true) {
                self::$_lock = false;
                return;
            }
        }

        if ($this->_alreadyProcessed($collection)) {
            return false;
        }

        $cn = $this->_getCorrelationName($collection);
        $this->_initResourceConnection();

        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();

        $attrCode = $eavAttribute->getIdByCode('catalog_product', AW_Catalogpermissions_Helper_Data::CP_DISABLE_PRODUCT);
        $attT = Mage::getSingleton('core/resource')->getTableName('catalog/product') . "_text";

        $collection->getSelect()
            ->joinLeft(array('_cp_def' => $attT), "_cp_def.entity_id = {$cn}.{$keyField} AND _cp_def.attribute_id = {$attrCode} AND _cp_def.store_id = 0", array())
            ->joinLeft(array('_catalogpermissions_store' => $attT), "_catalogpermissions_store.entity_id = {$cn}.{$keyField} AND _catalogpermissions_store.attribute_id = {$attrCode} AND _catalogpermissions_store.store_id = {$this->getStoreId()}", array())
            ->where("IF(_catalogpermissions_store.value_id > 0, _catalogpermissions_store.value, _cp_def.value) NOT REGEXP '(^|,)" . AW_Catalogpermissions_Helper_Data::getCustomerGroup() . "(,|$)' OR (_catalogpermissions_store.value IS NULL AND _cp_def.value IS NULL)");

        if (Mage::app()->getRequest()->getModuleName() == 'rss' || (AW_Catalogpermissions_Helper_Data::_isSortByPrice() && !isset($additional['noPriceFilter']))) {
            $priceAttr = $eavAttribute->getIdByCode('catalog_product', AW_Catalogpermissions_Helper_Data::CP_DISABLE_PRICE);
            $collection->getSelect()
                ->joinLeft(array('_cp_price_def' => $attT), "_cp_price_def.entity_id = {$cn}.{$keyField} AND _cp_price_def.attribute_id = {$priceAttr} AND _cp_price_def.store_id = 0", array())
                ->joinLeft(array('_cp_price_store' => $attT), "_cp_price_store.entity_id = {$cn}.{$keyField} AND _cp_price_store.attribute_id = {$priceAttr} AND _cp_price_store.store_id = {$this->getStoreId()}", array())
                ->where("IF(_cp_price_store.value_id > 0, _cp_price_store.value, _cp_price_def.value) NOT REGEXP '(^|,)" . AW_Catalogpermissions_Helper_Data::getCustomerGroup() . "(,|$)' OR ( _cp_price_store.value IS NULL AND  _cp_price_def.value IS NULL)");
        }

        /* Get disabled categories */
        $catCode = $eavAttribute->getIdByCode('catalog_category', AW_Catalogpermissions_Helper_Data::CP_DISABLE_CATEGORY);
        $categoryEntityTable = Mage::getSingleton('core/resource')->getTableName('catalog/category');
        $categoryIndexEntity = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
        $categoryProductTable = Mage::getSingleton('core/resource')->getTableName('catalog/category_product');
        $attT = $categoryEntityTable . "_text";

        $disabledCategories = Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE);

        if (is_null($disabledCategories)) {
            $this->cacheDisabledCategories();
            $disabledCategories = Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE);
        }

        if (!empty($disabledCategories)) {

            $collection->getSelect()
                ->join(array('_cp_categoryIndex' => $categoryIndexEntity), "{$cn}.{$keyField} = _cp_categoryIndex.product_id AND _cp_categoryIndex.store_id = {$this->getStoreId()}", array())
                ->joinLeft(array('_cp_categoryProduct' => $categoryProductTable), '_cp_categoryIndex.category_id = _cp_categoryProduct.category_id AND _cp_categoryIndex.product_id = _cp_categoryProduct.product_id', array())
                ->where('_cp_categoryProduct.category_id IS NOT NULL')
                ->where('_cp_categoryIndex.category_id NOT IN (?)', $disabledCategories)
                ->group("{$cn}.{$keyField}");
            $collection->isGroupCount = true;
        }


        return $collection;
    }

    public function cacheDisabledPriceProducts($_storeId = null, $_customerGroup = null)
    {
        if (is_null($_storeId)) {
            $_storeId = $this->getStoreId();
        }
        if (is_null($_customerGroup)) {
            $_customerGroup = AW_Catalogpermissions_Helper_Data::getCustomerGroup();
        }

        if (AW_Catalogpermissions_Model_Observer::$useCache === true) {
            $_cacheTag = AW_Catalogpermissions_Model_Cache::getCacheTag($_customerGroup, $_storeId, AW_Catalogpermissions_Model_Cache::PRODUCT_CACHE_TAG);
            $cacheData = AW_Catalogpermissions_Model_Cache::loadCache($_cacheTag);
            $cacheData = @unserialize($cacheData);
            if (is_array($cacheData)) {
                Mage::register(AW_Catalogpermissions_Helper_Data::DISABLED_PRICE_PROD_SCOPE, $cacheData, true);
                return;
            }
        }

        $this->_initResourceConnection();
        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();

        $attrCode = $eavAttribute->getIdByCode('catalog_product', AW_Catalogpermissions_Helper_Data::CP_DISABLE_PRICE);
        $productEntityTable = $this->getTable('catalog/product');
        $attT = $this->getTable('catalog/product') . "_text";

        $select = $this->getSelect()
            ->from(array('e' => $productEntityTable), array('e.entity_id'))
            ->joinLeft(array('_cp_def' => $attT), "_cp_def.entity_id = e.entity_id AND _cp_def.attribute_id = {$attrCode} AND _cp_def.store_id = 0", array())
            ->joinLeft(array('_catalogpermissions_store' => $attT), "_catalogpermissions_store.entity_id = e.entity_id AND _catalogpermissions_store.attribute_id = {$attrCode} AND _catalogpermissions_store.store_id = {$_storeId}", array())
            ->where("IF(_catalogpermissions_store.value_id > 0, _catalogpermissions_store.value, _cp_def.value) REGEXP '(^|,){$_customerGroup}(,|$)'");


        $products = $this->_connection->fetchCol($select);
        Mage::register(AW_Catalogpermissions_Helper_Data::DISABLED_PRICE_PROD_SCOPE, $products, true);
    }

    public function cacheDisabledProducts()
    {
        Mage::register(AW_Catalogpermissions_Helper_Data::DISABLED_PROD_SCOPE, array(), true);

    }

    public function cacheDisabledCategories($_storeId = null, $_customerGroup = null)
    {
        if (is_null($_storeId)) {
            $_storeId = $this->getStoreId();
        }
        if (is_null($_customerGroup)) {
            $_customerGroup = AW_Catalogpermissions_Helper_Data::getCustomerGroup();
        }

        if (AW_Catalogpermissions_Model_Observer::$useCache === true) {
            $_cacheTag = AW_Catalogpermissions_Model_Cache::getCacheTag($_customerGroup, $_storeId, AW_Catalogpermissions_Model_Cache::CATEGORY_CACHE_TAG);
            $cacheData = AW_Catalogpermissions_Model_Cache::loadCache($_cacheTag);
            $cacheData = @unserialize($cacheData);
            if (is_array($cacheData)) {
                Mage::register(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE, $cacheData, true);
                return;
            }
        }

        $this->_initResourceConnection();

        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $catCode = $eavAttribute->getIdByCode('catalog_category', AW_Catalogpermissions_Helper_Data::CP_DISABLE_CATEGORY);
        $attT = $this->getTable('catalog/category') . "_text";
        $categoryEntityTable = $this->getTable('catalog/category');

        $select = $this->getSelect()
            ->from(array('e' => $categoryEntityTable), array('e.path'))
            ->joinLeft(array('_cp_def' => $attT), "_cp_def.entity_id = e.entity_id AND _cp_def.attribute_id = {$catCode} AND _cp_def.store_id = 0", array())
            ->joinLeft(array('_catalogpermissions_store' => $attT), "_catalogpermissions_store.entity_id = e.entity_id AND _catalogpermissions_store.attribute_id = {$catCode} AND _catalogpermissions_store.store_id = {$_storeId}", array())
            ->where("IF(_catalogpermissions_store.value_id > 0, _catalogpermissions_store.value, _cp_def.value) REGEXP '(^|,){$_customerGroup}(,|$)'");

        $categories = $this->_connection->fetchCol($select);

        $select = $this->getSelect()
            ->from(array('e' => $categoryEntityTable), array('e.entity_id'));
        if (empty($categories)) {
            return false;
        }
        $like = null;
        for ($i = 0; $i < count($categories); $i++) {
            $like .= "e.path LIKE '{$categories[$i]}%'";
            if ($i != (count($categories) - 1)) {
                $like .= ' OR ';
            }
        }
        $select->where($like);
        $disabledCategories = $this->_connection->fetchCol($select);
        Mage::register(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE, $disabledCategories, true);

        return $disabledCategories;
    }

    public function addDisCategoryAttrFilter($collection)
    {

        if ($this->_alreadyProcessed($collection)) {
            return false;
        }

        $this->_initResourceConnection();
        $cn = $this->_getCorrelationName($collection);

        $eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
        $attrCode = $eavAttribute->getIdByCode('catalog_category', AW_Catalogpermissions_Helper_Data::CP_DISABLE_CATEGORY);
        $attT = Mage::getSingleton('core/resource')->getTableName('catalog/category') . "_text";

        $collection->getSelect()
            ->joinLeft(array('_cp_def' => $attT), "_cp_def.entity_id = {$cn}.entity_id AND _cp_def.attribute_id = {$attrCode} AND _cp_def.store_id = 0", array())
            ->joinLeft(array('_catalogpermissions_store' => $attT), "_catalogpermissions_store.entity_id = {$cn}.entity_id AND _catalogpermissions_store.attribute_id = {$attrCode} AND _catalogpermissions_store.store_id = {$this->getStoreId()}", array())
            ->where("IF(_catalogpermissions_store.value_id > 0, _catalogpermissions_store.value, _cp_def.value) NOT REGEXP '(^|,)" . AW_Catalogpermissions_Helper_Data::getCustomerGroup() . "(,|$)' OR (_cp_def.value IS NULL AND _catalogpermissions_store.value IS NULL)");

        $collection->getSelect()->where("{$cn}.entity_id NOT IN (?)", Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE));

        return $collection;
    }

    private function _getCorrelationName($collection)
    {
        list($correlationName) = ($collection->getSelect()->getPart(Zend_Db_Select::COLUMNS));
        return array_shift($correlationName);
    }

    private function _alreadyProcessed($collection)
    {
        $where = $collection->getSelect()->getPart(Zend_Db_Select::WHERE);
        foreach ($where as $part) {
            if (preg_match('#_catalogpermissions_store\.value_id#is', $part)) {
                return true;
            }
        }
        return false;
    }

    public function isCategoryDisabled($categoryId)
    {
        if (($disabledCategories = Mage::registry(AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE)) === null) {
            $disabledCategories = $this->cacheDisabledCategories();
        }
        return is_array($disabledCategories) && in_array($categoryId, $disabledCategories);
    }
}
