<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Model_Observer extends AW_Catalogpermissions_Model_Observer_Abstract
{
    /**
     * @var int
     */
    public static $i;

    /**
     * @var array
     */
    public static $temp = array();

    /**
     * @var bool
     */
    public static $useCache = false;

    /**
     * Predespatch event
     * At this point we load from cache or get from database info about disabled products
     * and categories and register them in global scope (Mage::registry) for later use
     * Load disabled products and categories from cache or on the fly
     * @return type
     *
     */
    public function controllerActionPredispatch()
    {
        parent::prepareRewrites();
        if (!parent::_validateProcess()) {
            return;
        }

        Mage::helper('catalogpermissions')->clearWishlistCountInLinks();
        Mage::helper('catalogpermissions')->setCompareItemsCountInSession();

        /* Declare variables first as cache maybe removed manually */
        if (Mage::getModel('core/cache')->canUse(AW_Catalogpermissions_Model_Cache::CACHE_TYPE)) {
            self::$useCache = true;
        }

        Mage::helper('catalogpermissions/connection')->cacheDisabledCategories();
        /* Available in registry after by key AW_Catalogpermissions_Helper_Data::DIABLED_CATEGS_SCOPE */

        Mage::helper('catalogpermissions/connection')->cacheDisabledPriceProducts();
        /* Available in registry after by key AW_Catalogpermissions_Helper_Data::DISABLED_PRICE_PROD_SCOPE */

        /* Deprecated */
        Mage::helper('catalogpermissions/connection')->cacheDisabledPriceProducts();
        /* Available in registry after by key AW_Catalogpermissions_Helper_Data::DISABLED_PROD_SCOPE */
    }

    /**
     *  catalogProductCollectionLoadBefore
     *  preparePriceSelect
     *  Functions of this block [Block 1] are for dealing with product collection filters
     *
     * Event - catalog_product_collection_load_before
     * @param obj $Event
     *
     */
    public function catalogProductCollectionLoadBefore($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        Mage::helper('catalogpermissions/connection')->addDisabledAttrToFilter($event->getCollection());
        parent::regroupProductCollection($event->getCollection());

    }

    /**
     * Event - catalogindex_prepare_price_select
     * @param type $event
     *
     */

    public function preparePriceSelect($collection)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        Mage::helper('catalogpermissions/connection')->addDisabledAttrToFilter($collection);
        parent::regroupProductCollection($collection);
    }

    /**
     *
     * This event is for dealing with products in the wishlist
     * Event - abstract_collection_load_before
     * @param type $event
     *
     */
    public function abstractCollectionBeforeLoad($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if ($event->getCollection() instanceof Mage_Wishlist_Model_Mysql4_Item_Collection ||
            $event->getCollection() instanceof Mage_Wishlist_Model_Resource_Item_Collection
        ) {
            Mage::helper('catalogpermissions/connection')->addDisabledAttrToFilter($event->getCollection(), 'product_id', array('noPriceFilter' => true));
        }
        /**
         * Don't delete disabled products if they are already in cart
         */
        if ($event->getCollection() instanceof Mage_Sales_Model_Mysql4_Quote_Item_Collection) {
            AW_Catalogpermissions_Helper_Connection::$_lock = true;
        }
    }

    /**
     * Event - catalog_product_collection_apply_limitations_after
     * @param type $event
     */
    public function applyCatalogLimitations($event)
    {
        $this->catalogProductCollectionLoadBefore($event);
    }

    /*End of [Block 1] */

    /**
     *
     * catalogCategoryCollectionLoadBefore
     * pageTree
     * These functions are for dealing with category disabling process
     *
     * Event - catalog_category_collection_load_before
     * @param type $event
     *
     */
    public function catalogCategoryCollectionLoadBefore($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        Mage::helper('catalogpermissions/connection')->addDisCategoryAttrFilter($event->getCategoryCollection());

    }

    /**
     * Event - catalog_category_tree_init_inactive_category_ids
     * @param type $object
     */
    public function pageTree($object)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if (method_exists($object->getTree(), 'getCollection')) {
            Mage::helper('catalogpermissions/connection')->addDisCategoryAttrFilter($object->getTree()->getCollection());
        }
    }

    /****************************************************************************************************************/
    /**
     * Block [Block 3] of methods below observe blocks and add specific info to product data $Product->setAWDisableOutOfStock(true);
     * Event - core_block_abstract_to_html_before
     * @param type $event
     *
     */
    public function blockAbstractToHtmlBefore($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if ($product = $event->getBlock()->getProduct()) {
            $class = get_class($event->getBlock());
            if ($class == 'Mage_Wishlist_Block_Render_Item_Price' ||
                $class == 'Mage_Catalog_Block_Product_Price' ||
                $class == 'Mage_Bundle_Block_Catalog_Product_Price' ||
                $class == 'AW_Sarp_Block_Catalog_Product_Price' ||
                $class == 'AW_Sarp_Block_Product_Price'
            ) {
                $disabled = Mage::registry(AW_Catalogpermissions_Helper_Data::DISABLED_PRICE_PROD_SCOPE);
                $isDisabledPrice = true;
                if ($product->getTypeId() === Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
                    $product->setData('minimal_price', false);
                    $children = $product->getTypeInstance()->getAssociatedProducts();
                    foreach($children as $child) {
                        if (!in_array($child->getId(), $disabled)) {
                            $product->setData('minimal_price', $child->getData('price'));
                            $isDisabledPrice = false;
                        }
                    }
                }
                $currentGroupId = AW_Catalogpermissions_Helper_Data::getCustomerGroup();
                $globalRestrictedGroups = AW_Catalogpermissions_Helper_Data::getHidePriceGroupConfig();
                $isDisabledPrice = (($isDisabledPrice && in_array($product->getId(), $disabled)) || in_array($currentGroupId, $globalRestrictedGroups));
                if ($isDisabledPrice) {
                    AW_Catalogpermissions_Helper_Data::setVisibility($product, false);
                    self::$temp["aw_catalogpermissions_block_{$product->getId()}_" . get_class($event->getBlock())] = 1;
                }
            }
        }
    }

    /**
     *
     * Reset block price html if price attribute is selected
     * @param type $event
     * @return type
     *
     */
    public function blockAbstractToHtmlAfter($event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if (version_compare(Mage::getVersion(), '1.3.3.0', '<=')) {
            return;
        }
        if ($product = $event->getBlock()->getProduct()) {
            if (isset(self::$temp["aw_catalogpermissions_block_{$product->getId()}_" . get_class($event->getBlock())])) {
                $event->getTransport()->setHtml('');
            }
        }
    }

    public function catalogProductisSalableBefore($Event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        AW_Catalogpermissions_Helper_Data::checkVisibility($Event->getProduct(), false);
    }

    /**
     *  Event - catalog_controller_product_view
     *  Redirect to catalogProductisSalableBefore($Event)
     */
    public function catalogProductView($Event)
    {
        $this->catalogProductisSalableBefore($Event);
    }

    /**
     * Event - review_controller_product_init
     * Redirect to catalogProductisSalableBefore($Event)
     *
     */
    public function catalogProductReview($Event)
    {
        $this->catalogProductisSalableBefore($Event);
    }

    /* End of [Block 3] */

    public function wishlistItemsRenewed()
    {
        if (!parent::_validateProcess()) {
            return;
        }
        $_wishlistHelper = Mage::helper('wishlist');
        if (method_exists($_wishlistHelper, 'getWishlistItemCollection')) {
            $collection = Mage::helper('wishlist')->getWishlistItemCollection();
            $_switch = true;
        } else if (method_exists($_wishlistHelper, 'getItemCollection')) {
            $collection = Mage::helper('wishlist')->getItemCollection();
            $_switch = false;
        } else {
            return false;
        }
        Mage::helper('catalogpermissions/connection')->addDisabledAttrToFilter($collection, 'product_id', array('noPriceFilter' => true));
        /* Provide compatibility with CE versions 1.5.1.0 and older */
        if (!$_switch) {
            $count = $collection->count();
            Mage::getSingleton('customer/session')->setWishlistItemCount($count);
            return $this;
        }
        if (Mage::getStoreConfig(Mage_Wishlist_Helper_Data::XML_PATH_WISHLIST_LINK_USE_QTY)) {
            $count = $collection->getItemsQty();
        } else {
            $count = $collection->count();
        }
        Mage::getSingleton('customer/session')->setWishlistItemCount($count);
    }

    /**
     *
     * Fix for Magento EE versions 1.10.0.0 >= to be and CE >= 1.6.1.0
     * compatible with wishlist
     * According to Magento filters not salable products are not displayed in wishlist
     * At the moment of check our products are salable, but after there are not
     *
     */
    public static function wishlistAfterLoad($Event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if (Mage::helper('awall/versions')->getPlatform() === AW_All_Helper_Versions::CE_PLATFORM) {
            if (!version_compare(Mage::getVersion(), '1.6.0.0', '>=')) {
                return;
            }
        }
        self::$i++;
        foreach ($Event->getProductCollection() as $product) {
            if (AW_Catalogpermissions_Helper_Data::checkVisibility($product, true, array('Price'))) {
                Mage::register('aw_catalogpermissions_salable' . $product->getId(), $product->getId(), true);
                $product->isSalable();
            }
        }
    }

    public static function salableAfter($Event)
    {
        if (!parent::_validateProcess()) {
            return;
        }
        if (Mage::app()->getRequest()->getRequestString() == '/wishlist/index/allcart/') {
            return;
        }
        $module = Mage::app()->getRequest()->getModuleName();
        $controller = Mage::app()->getRequest()->getControllerName();
        $action = Mage::app()->getRequest()->getActionName();

        $fullPath = "{$module}/{$controller}/{$action}";
        $incr = $fullPath != 'wishlist/index/index' ? 2 : 4;

        $product = $Event->getProduct();
        $key = 'aw_catalogpermissions_salable' . $product->getId();
        $base = 'aw_catalogpermissions_salable' . $product->getId() . 'base';

        if (self::$i == 2 && $incr == 2) {
            Mage::unregister($base);
            self::$i = 1;
        }

        if (Mage::registry($key) !== NULL && Mage::registry($base) === NULL && $product->getStatus() != '2') {
            $salable = $Event->getSalable();
            $salable->setIsSalable(true);
            Mage::register($base, ($product->getId() + 1), true);
        }
        $pass = ((Mage::registry($key) + $incr) < Mage::registry($base));
        if (Mage::registry($key) !== NULL && Mage::registry($base) !== NULL && !$pass) {
            $salable = $Event->getSalable();
            $salable->setIsSalable(true);
            $old = Mage::registry($base);
            Mage::unregister($base);
            Mage::register($base, ($old + 1), true);
        } else {
            Mage::unregister($base);
        }
    }

    /**
     * @return AW_Catalogpermissions_Helper_Connection
     */
    protected function _getConnectionHelper()
    {
        return Mage::helper('catalogpermissions/connection');
    }

    public function checkBlock($observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Catalog_Block_Product_View) {
            $this->_checkProductViewBlock($block);
        } elseif ($block instanceof Mage_Catalog_Block_Category_View) {
            $this->_checkCategoryViewBlock($block);
        }
    }

    protected function _checkCategoryViewBlock(Mage_Catalog_Block_Category_View $block)
    {
        $categoryId = $block->getCurrentCategory()->getId();
        if ($this->_getConnectionHelper()->isCategoryDisabled($categoryId)) {
            $block->getLayout()->getMessagesBlock()->addNotice(Mage::helper('catalog')->__('You do not have permission to access this page'));
            $block->setTemplate('catalog/category/empty.phtml');
        }
    }

    protected function _checkProductViewBlock(Mage_Catalog_Block_Product_View $block)
    {
        /** @var $product Mage_Catalog_Model_Product */
        $product = $block->getProduct();
        $productId = $product->getId();
        $categoryId = $product->getCategoryId();
        if (!$categoryId && ($categoryIds = $block->getProduct()->getCategoryIds())) {
            $_canShow = false;
            foreach ($categoryIds as $_categoryId) {
                if (!$this->_getConnectionHelper()->isCategoryDisabled($_categoryId)) {
                    $_canShow = true;
                    break;
                }
            }
        }
        if ($this->_getConnectionHelper()->isDisabled($productId)
            || ($categoryId && $this->_getConnectionHelper()->isCategoryDisabled($categoryId))
            || (!$categoryId && !$_canShow)
        ) {
            $block->getLayout()->getMessagesBlock()->addNotice(Mage::helper('catalog')->__('You do not have permission to access this page'));
            $block->setTemplate('catalog/product/empty.phtml');
        }
    }
}
