<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Catalogpermissions
 * @version    1.2.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */


class AW_Catalogpermissions_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List
{
    protected function _getRestrictedIds(Mage_Catalog_Model_Layer $layer)
    {
        $currentCategory = $layer->getCurrentCategory();
        $allChildrenCategories = $currentCategory->getChildren();
        $restrictedIds = array();
        if ($allChildrenCategories) {
            $allChildrenCategories = explode(',', $allChildrenCategories);
            $childrenCategories = $currentCategory->getChildrenCategories();
            $allowedCategories = array();
            foreach ($childrenCategories as $_category) {
                /** @var $_category Mage_Catalog_Model_Category */
                $allowedCategories[] = $_category->getId();
            }
            unset($_category);
            $restrictedCategoriesIds = array_diff($allChildrenCategories, $allowedCategories);
            foreach ($restrictedCategoriesIds as $_categoryId) {
                $_category = Mage::getModel('catalog/category')->load($_categoryId);
                $restrictedIds = array_merge($restrictedIds, $_category->getProductCollection()->getAllIds());
            }
        }
        return $restrictedIds;
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            /* AW CP Part { */
            $restrictedIds = $this->_getRestrictedIds($layer);
            if ($restrictedIds) {
                $this->_productCollection->addFieldToFilter('entity_id', array('nin' => $restrictedIds));
            }
            /* } AW CP Part */

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        if (method_exists(get_parent_class($this), 'getLayer')) {
            return parent::getLayer();
        }
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }
}
