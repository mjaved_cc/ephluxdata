21.03.2012
----------
v1.0 release


17.04.2012
----------
v1.1 release

Minor CSS fixes.
Validation issue fixes.
Image proportions fixes.
Separate Slideshow for different stores.
Added checkboxes for Related and upsell products.
Added slider for Up-sell Products and Related Products blocks. Now you can show as much related and up-sell products as you want.
Added custom tab for product page, ready to display any content: info about shipping, returns, sales, etc.
Added custom block for drop-down menu. Now you can display your custom content as a drop-down block in navigation.
Static block for Logo image. No need to upload your logo using ftp client anymore, now you can change from admin panel.
Added previous/next functionality for the product view page. Now you can navigate to next and previous products without going back to listing page.
You can now add home page button in the main navigation menu.
Added �Sign Up� link in the Top Links block. It will help new customers to find the registration form.
Now you have possibility to display your custom link in the top Links block. You can set the path, anchor and title text.
Added Product labels (�new� and �sale�) on product page, in categories and on other product listings (editable from admin panel).


19.04.2012
----------
v1.11 release

Fix Inchoo prev/next bug
Minor CSS fixes
Fix IE8 state dropdown bug


01.05.2012
----------
v1.12 release

Fixed google font https bug
Fixed static logo block htpps bug
Moved crosssel container div to crosssel.phtml
Minor CSS fixes
Added sorting order for slides


14.05.2012
----------
v1.13 release

fix php notice in Navigation.php
add hoverintent to the top drop down menu
fix custom options on product page
fix fixed version css bug


04.06.2012
----------
v1.14 release

Added psd files for inner pages



07.08.2012
----------
v1.15 release

fix wislist page layout
fix downloadable product page
remove one instance of Compare block in the "My Account" section
fix cloudZoom - now Zoom appear only if the base image is larger than thumbnail
Add an option to the Products panel in the Theme Settings page to allow merchant to optionally display the product SKU on the product detail page
add option to show/hide slideshow timeline
update copyright text
fix prevnext extension bug
fix permissions for settings controllers
fix - celebrity settings take focus every time the merchant accesses the System / Configuration



19.10.2012
----------
v1.16 release

add theme version for enterprise 1.12
