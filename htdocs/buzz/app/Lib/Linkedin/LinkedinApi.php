<?php

// http://stackoverflow.com/questions/5552310/linkedin-php-sample-code
// callback : http://dev.socialmedia.com/users/callback

class LinkedinApi {

	private $consumer_key;
	private $consumer_key_secret;
	private $requesttokenurl;
	private $accesstokenurl;
	private $authurl;
	public $oauth_token;
	public $oauth_token_secret;
	public $linkedin_obj;
	public $site_base_url;

	const URI_BASE = 'http://api.linkedin.com/v1/';
	const LINKEDIN_COMPANY_DETAILS = 'companies/COMPANY_ID:(id,name,num-followers,employee-count-range,twitter-id,logo-url)?format=json';
	const LINKEDIN_PRODUCT_DETAILS = 'companies/COMPANY_ID/products:(id,name,num-recommendations,creation-timestamp,recommendations:(recommender,id,product-id,text,reply,timestamp,likes:(timestamp,person)))?format=json&start=0&count=10';
	const LINKEDIN_SEARCH_COMPANIES = 'company-search:(companies:(id,name))?keywords=COMPANY_NAME&count=20';
	const LINKEDIN_USER_DETAILS = 'people/~' ; // just to verify access token 

	function __construct() {

		$this->consumer_key = LINKEDIN_CONSUMER_KEY;
		$this->consumer_key_secret = LINKEDIN_CONSUMER_KEY_SECRET;
		$this->requesttokenurl = "https://api.linkedin.com/uas/oauth/requestToken";
		$this->accesstokenurl = "https://api.linkedin.com/uas/oauth/accessToken";
		$this->authurl = "https://api.linkedin.com/uas/oauth/authorize";

		$this->site_base_url = SMC_BASE_URI ;
		$this->linkedin_obj = new OAuth($this->consumer_key, $this->consumer_key_secret,
						OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
	}

	public function getAuthorizationUrl() {

		$request_token_info = $this->linkedin_obj->getRequestToken($this->requesttokenurl); //get request token
		$_SESSION['lrequest_token_secret'] = $request_token_info['oauth_token_secret'];
		return $this->authurl . '?oauth_token=' . $request_token_info['oauth_token'];
	}

	function getAccessToken() {

		//get the access token - dont forget to save it 
		$request_token_secret = $_SESSION['lrequest_token_secret'];
		$this->linkedin_obj->setToken($_REQUEST['oauth_token'], $request_token_secret); //user allowed the app, so u

		$access_token_info = $this->linkedin_obj->getAccessToken($this->accesstokenurl);

		$_SESSION['laccess_oauth_token'] = $access_token_info['oauth_token'];
		$_SESSION['laccess_oauth_token_secret'] = $access_token_info['oauth_token_secret'];

		return $access_token_info;
	}

	function getDataFromFeed($url) {

		//now fetch current users profile					
		$this->linkedin_obj->fetch(self::URI_BASE . $url, null, OAUTH_HTTP_METHOD_GET, array('x-li-format' => 'json'));
		$data = json_decode($this->linkedin_obj->getLastResponse());
		
		return $data;
	}
	
	function isConnected($access_token,$access_token_secret,$company_id){
		
		$this->linkedin_obj->setToken($access_token, $access_token_secret);
		$url = str_replace('COMPANY_ID', $company_id, LinkedinApi::LINKEDIN_COMPANY_DETAILS);
		return $this->getDataFromFeed($url);
	}

}

