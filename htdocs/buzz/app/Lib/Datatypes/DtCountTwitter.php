<?php

class DtCountTwitter extends DtUser {

	public $fields = array();
	
	// before altering _keys please review class code first as its being used
	private $_keys = array('count_followers', 'count_following', 'count_tweets', 'count_retweets', 'count_mention');

	const BENCHMARK_KEYWORD_PREFIX = 'tw_';

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			if (in_array($key, $this->_keys)) {
// below value will be used for benchmarked purpose & is used in other calculations
				$this->$key = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
// below value is actual value recieved from API & is stored as it is.
				$this->{'actual_' . $key} = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
			}
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
			case 'schedule_date':
				$this->fields[$key] = new DtDate($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields() {

		return $this->fields;
	}

	/*	 * *********** Garbage - depends on formulae so no need to save it in db **************** */

	public function get_engagement() {
		return $this->count_mention + $this->count_retweets;
	}

	public function get_influence() {
		return $this->count_retweets;
	}

	public function get_reach() {
		return $this->count_followers + $this->count_following + $this->count_retweets;
	}

	/*	 * *********** END: Garbage **************** */

	/**
	 * Get total score on already benchmarked values
	 * 
	 * @return float total score
	 */
	function get_score() {

		$score = $this->count_followers +
				$this->count_following +
				$this->count_tweets +
				$this->count_retweets +
				$this->count_mention;

		return $score;
	}

	/**
	 * Get tweets value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_tweets
	 */
	function get_bm_count_tweets($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_tweets / $bm_value['benchmark']))));
	}

	/**
	 * Get retweets value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_retweets
	 */
	function get_bm_count_retweets($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_retweets / $bm_value['benchmark']))));
	}

	/**
	 * Get followers value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_followers
	 */
	function get_bm_count_followers($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_followers / $bm_value['benchmark']))));
	}

	/**
	 * Get following value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_following
	 */
	function get_bm_count_following($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_following / $bm_value['benchmark']))));
	}

	/**
	 * Get mention value with respect to benchmarks
	 * 
	 * @param array $bm_value Holds data of benchmarks table
	 * @return float count_mention
	 */
	function get_bm_count_mention($bm_value) {
		return ($bm_value['weightage'] - ($bm_value['weightage'] * exp((-$this->count_mention / $bm_value['benchmark']))));
	}

	/**
	 * Get engagement with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float engagement
	 */
	function get_bm_engagement($benchmarks) {

		$engagement = ($benchmarks['tw_count_followers']['engagement'] * $this->count_followers) +
				($benchmarks['tw_count_following']['engagement'] * $this->count_following) +
				($benchmarks['tw_count_tweets']['engagement'] * $this->count_tweets) +
				($benchmarks['tw_count_retweets']['engagement'] * $this->count_retweets) +
				($benchmarks['tw_count_mention']['engagement'] * $this->count_mention);

		return $engagement;
	}

	/**
	 * Get reach with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float reach
	 */
	function get_bm_reach($benchmarks) {

		$reach = ($benchmarks['tw_count_followers']['reach'] * $this->count_followers) +
				($benchmarks['tw_count_following']['reach'] * $this->count_following) +
				($benchmarks['tw_count_tweets']['reach'] * $this->count_tweets) +
				($benchmarks['tw_count_retweets']['reach'] * $this->count_retweets) +
				($benchmarks['tw_count_mention']['reach'] * $this->count_mention);

		return $reach;
	}

	/**
	 * Get influence with respect to benchmarks
	 * 
	 * @param array $benchmarks Holds data of benchmarks table
	 * @return float influence
	 */
	function get_bm_influence($benchmarks) {

		$influence = ($benchmarks['tw_count_followers']['influence'] * $this->count_followers) +
				($benchmarks['tw_count_following']['influence'] * $this->count_following) +
				($benchmarks['tw_count_tweets']['influence'] * $this->count_tweets) +
				($benchmarks['tw_count_retweets']['influence'] * $this->count_retweets) +
				($benchmarks['tw_count_mention']['influence'] * $this->count_mention);

		return $influence;
	}

	/**
	 * Get total score & also apply benchmarks on required values/fields
	 * 
	 * @return float total score
	 */
	function get_bm_score($benchmarks) {
		
		$score = 0;
		
		// fields we need for benchmarking
		$bfields = $this->get_benchmarked_fields();

		foreach ($bfields as $field => $value) {
			if (
					is_numeric($value) &&
					!empty($benchmarks[self::BENCHMARK_KEYWORD_PREFIX . $field])
			) {
				$function_name = 'get_bm_' . $field;
				$score += DtUser::format_number($this->$function_name($benchmarks[self::BENCHMARK_KEYWORD_PREFIX . $field]));
			}
		}
		return DtUser::format_number($score);
	}

	/**
	 * Get necessary fields for display purpose. This function is specifically used for action `get_user_stats` in `Users` controller. To make logic generic.
	 * 
	 * @return array necessary fields
	 */
	function get_rendering_fields() {
		return array(
			'count_followers' => $this->actual_count_followers,
			'count_following' => $this->actual_count_following,
			'count_tweets' => $this->actual_count_tweets,
			'count_retweets' => $this->actual_count_retweets,
			'count_mention' => $this->actual_count_mention,
			'count_followers_trends' => $this->count_followers_trends,
			'count_following_trends' => $this->count_following_trends,
			'count_tweets_trends' => $this->count_tweets_trends,
			'count_retweets_trends' => $this->count_retweets_trends,
			'count_mention_trends' => $this->count_mention_trends
		);
	}
	
	/**
	 * Get actual values recieved from API
	 * 
	 * @return array actual fields (key-value pair)
	 */
	function get_actual() {
		$actual_fields = array();
		foreach ($this->_keys as $value) {
			$actual_fields['actual_' . $value] = $this->$value;
		}

		return $actual_fields;
	}
	
	/**
	 * Get benchmarked fields. Original any social param/fields store original value. We alter it when calc_benchmarks is called in DtUser class. 
	 * 
	 * @return array benchmarked fields (key-value pair)
	 */
	function get_benchmarked_fields() {
		$benchmark_fields = array();
		foreach ($this->_keys as $value) {
			$benchmark_fields[$value] = $this->$value;
		}

		return $benchmark_fields;
	}

}