<?php

class DtScore extends DtUser {

	public $fields = array();

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
			case 'schedule_date':
				$this->fields[$key] = new DtDate($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields($key = 0) {
		if (isset($this->fields[$key]))
			return $this->fields[$key];
		else
			return $this->fields;
	}

	public function get_total_engagement() {
		return $this->engagement_facebook + $this->engagement_twitter + $this->engagement_linkedin;
	}

	public function get_total_influence() {
		return $this->influence_facebook + $this->influence_twitter + $this->influence_linkedin;
	}

	public function get_total_reach() {
		return $this->reach_facebook + $this->reach_twitter + $this->reach_linkedin;
	}

	public function get_percentage($social_nw) {
		// to avoid divided by zero 
		$percentage = ($this->total_score > 0) ? (($this->{'round_score_' . $social_nw} / $this->total_score) * 100) : 0;
		return DtUser::format_number($percentage);
	}

}