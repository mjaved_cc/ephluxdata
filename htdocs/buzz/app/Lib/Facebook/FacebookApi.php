<?php

require_once('base_facebook.php');
require_once('facebook.php');

// http://stackoverflow.com/questions/5699270/how-to-get-share-counts-using-graph-api
// get share count
// https://developers.facebook.com/bugs/116612021798512?browse=search_4fc615377beb69e49469808

class FacebookApi {

	private $key = null;
	private $secret = null;
	private $limit; // limit is for fetching records.
	public $facebook_obj = null;
	public $params;
	public $redirect_uri;
	public $scope = null;
	public $site_base_url = null;
	public $message = '';
	public $name = '';

	const FACEBOOK_PAGE_FEEDS = 'PAGE_ID/feed?fields=comments,likes,shares&since=SINCE&until=UNTIL';
	const FACEBOOK_PAGE_POSTS = 'PAGE_ID/posts?fields=comments,likes,shares&since=SINCE&until=UNTIL';
	const FACEBOOK_PAGE_DETAILS = 'PAGE_ID?fields=likes,talking_about_count';
	const FACEBOOK_SEARCH_PAGES = 'search?q=KEYWORD&type=page&limit=20';
	
	function __construct() {

		$this->scope = 'publish_stream, offline_access, email, read_stream, manage_pages';
		$this->key = FACEBOOK_KEY;
		$this->secret = FACEBOOK_SECRET;
		$this->limit = 300;

		$this->site_base_url = SMC_BASE_URI . '/';

		$this->facebook_obj = new Facebook(array('appId' => $this->key, 'secret' => $this->secret));

		$this->redirect_uri = SMC_BASE_URI . '/users/index';
		$this->params = array('scope' => $this->scope, 'redirect_uri' => $this->redirect_uri);
	}

	function isConnected($token , $page_id) {

		$this->facebook_obj->setAccessToken($token);
		return $this->getDataFromFeed('/' . $page_id . '?fields=id,name,picture');
	}

	function Connect() {
		try {
			return $this->facebook_obj->getLoginUrl($this->params);
		} catch (FacebookApiException $e) {
			return false;
		}
	}

	function getFbLogoutUrl($url = null) {
		$this->facebook_obj->clearAllPersistentData();
	}

	function getAccessToken() {
		return $this->facebook_obj->getAccessToken();
	}

	function getDataFromFeed($url) {

		$feeds = $this->facebook_obj->api($url . '&date_format=Y-m-d&limit=' . $this->limit);
		return $feeds;		
	}
}
