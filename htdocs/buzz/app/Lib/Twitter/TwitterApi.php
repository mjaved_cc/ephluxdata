<?php

// http://stackoverflow.com/questions/5552310/linkedin-php-sample-code

class TwitterApi {

	private $consumer_key;
	private $consumer_key_secret;
	private $requesttokenurl;
	private $accesstokenurl;
	private $authurl;
	public $oauth_token;
	public $oauth_token_secret;
	public $twitter_obj;
	public $site_base_url;

	// if user wants to know its own stats - registered users

	const TWITTER_USER_DETAILS = 'https://api.twitter.com/1.1/account/verify_credentials.json';
	const TWITTER_RETWEETS_BY_ME = 'https://api.twitter.com/1.1/statuses/retweets_of_me.json?trim_user=1&count=100';
	const TWITTER_MENTIONS = 'https://api.twitter.com/1.1/statuses/mentions_timeline.json?trim_user=1&count=100';
	const TWITTER_SEARCH_USERS = 'https://api.twitter.com/1.1/users/search.json?q=KEYWORD&count=20';

	// if third party stats is concerned - non registered users
	const TWITTER_USER_LOOKUP = 'https://api.twitter.com/1.1/users/lookup.json?screen_name=SCREEN_NAME';
	const TWITTER_RETWEETS_BY_USER = 'https://api.twitter.com/1/statuses/retweeted_by_user.json?screen_name=SCREEN_NAME&count=100&trim_user=1';

	// here trailing slash is explicitly given otherwise through exception 'Invalid protected resource url, unable to generate signature base string' ...  Reference URL: http://www.lornajane.net/posts/2011/invalid-protected-resource-url-in-pecl_oauth
	const TWITTER_MENTIONS_BY_SCREEN_NAME = 'http://search.twitter.com/search.json?q=%40SCREEN_NAME&since=SINCE/';

	function __construct() {

		$this->consumer_key = TWITTER_CONSUMER_KEY;
		$this->consumer_key_secret = TWITTER_CONSUMER_KEY_SECRET;
		$this->requesttokenurl = "https://api.twitter.com/oauth/request_token";
		$this->accesstokenurl = "https://api.twitter.com/oauth/access_token";
		$this->authurl = "https://api.twitter.com/oauth/authorize";

		$this->site_base_url = SMC_BASE_URI;
		$this->twitter_obj = new OAuth($this->consumer_key, $this->consumer_key_secret,
						OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
	}

	function isConnected($oauth_token, $oauth_token_secret) {
		$this->twitter_obj->setToken($oauth_token, $oauth_token_secret);
		return $this->getDataFromFeed(self::TWITTER_USER_DETAILS);
	}

	public function getAuthorizationUrl() {

		$request_token_info = $this->twitter_obj->getRequestToken($this->requesttokenurl); //get request token
		$_SESSION['trequest_token_secret'] = $request_token_info['oauth_token_secret'];
		return $this->authurl . '?oauth_token=' . $request_token_info['oauth_token'];
	}

	function getAccessToken() {

		//get the access token - dont forget to save it 
		$request_token_secret = $_SESSION['trequest_token_secret'];
		$this->twitter_obj->setToken($_REQUEST['oauth_token'], $request_token_secret); //user allowed the app, so u
		$access_token_info = $this->twitter_obj->getAccessToken($this->accesstokenurl);

		$_SESSION['taccess_oauth_token'] = $access_token_info['oauth_token'];
		$_SESSION['taccess_oauth_token_secret'] = $access_token_info['oauth_token_secret'];

		return $access_token_info;
	}

	function getDataFromFeed($url) {

		//now fetch current users profile
		$response = $this->twitter_obj->fetch($url);
		$data = json_decode($this->twitter_obj->getLastResponse());
		return $data;
	}

	/**
	 * Get profile image url
	 * 
	 * @param string $profile_image url
	 * @return string url
	 */
	static function getImageUrl($profile_image) {
		
		$path = pathinfo($profile_image);
		$filename = str_replace('_normal', '_bigger', $path['filename']);
		$profile_image_url = $path['dirname'] . '/' . $filename . '.' . $path['extension'];
		
		return $profile_image_url;
	}

}

