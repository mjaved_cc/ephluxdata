<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BackofficesController extends AppController {

	public $name = 'Backoffices';
	public $uses = array('User', 'Tmp', 'UserSocialAccount', 'CountFacebook', 'CountTwitter', 'CountLinkedin', 'Batch', 'ProcessQueue', 'CronTrack', 'Score', 'SearchingQueue', 'Batch');
	private $social_networks;
	public $paginate = array(
		'limit' => 25,
		'conditions' => array('User.status' => USER_STATUS_ACTIVE)
	);

	function beforeFilter() {
		parent::beforeFilter();

		$this->social_networks = Configure::read('SOCIAL_NETWORKS');
	}

	/**
	 * List all users
	 * 
	 * @param array $query_params where clause for query
	 * @return void
	 */
	public function index($query_params = 0) {
		$this->User->recursive = 0;

		$users = $this->paginate('User', $query_params);
		$data = array();
		foreach ($users as $key => $value) {

			$obj_user = new DtUser($value);

			$data[$key]['screen_name'] = $obj_user->screen_name;
			$data[$key]['username'] = $obj_user->username;
			$data[$key]['score'] = $obj_user->score;
			$data[$key]['url'] = $obj_user->url;
			$data[$key]['image_url'] = $obj_user->image_url;
			$data[$key]['id'] = $obj_user->id;
			$data[$key]['status'] = $obj_user->status;
			$data[$key]['created'] = $obj_user->created->getDate();
		}

		$this->set(compact('data'));
	}

	/**
	 * Get details of given user id
	 * @param int|string $user_id system generated user id
	 * @return void
	 */
	public function details($user_id) {

		if (empty($user_id))
			$this->redirect(array('action' => 'index'));

		$user_details = $this->User->findById($user_id);
		$obj_user = new DtUser($user_details);
				
		$data['User']['screen_name'] = $obj_user->screen_name;
		if (count($obj_user->UserSocialAccount) > 0) {
			foreach ($obj_user->UserSocialAccount as $key => $social_detail) {

				$data['UserSocialAccount'][$key]['image_url'] = $social_detail->image_url;
				$data['UserSocialAccount'][$key]['type_id'] = $social_detail->type_id;
				$data['UserSocialAccount'][$key]['screen_name'] = $social_detail->screen_name;
				$data['UserSocialAccount'][$key]['user_id'] = $social_detail->user_id;
			}
		}
		
		$this->set(compact('data'));
	}

	/**
	 * Edit user information
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->set('USER_STATUS', Configure::read('USER_STATUS'));
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->User->set('status', $this->request->data['User']['status']);
			if ($this->User->save()) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
	}

	/**
	 * Delete given user
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param int|string $user_id system generated user id
	 * @return void
	 */
	public function delete($user_id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $user_id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}

		if ($this->User->saveField('status', USER_STATUS_DELETED)) {
			$this->UserSocialAccount->updateAll(
					array('UserSocialAccount.status' => "'" . USA_STATUS_DELETED . "'"), array('UserSocialAccount.user_id' => $user_id)
			);
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Search & browse users via email or screen name
	 * @param string $q screen_name or email via query string
	 * @return void
	 */
	function search() {

		if (!empty($this->request->query['q'])) {
			$keyword = $this->request->query['q'];

			$query_param['OR']['User.screen_name LIKE'] = $keyword . '%';
			$query_param['OR']['User.username LIKE'] = $keyword . '%';

			$this->index($query_param);
		}
	}
	
	/**
	 * User login web interface for admin
	 * @param string $username Username (is basically email)
	 * @param string $password password
	 * @param boolean $remember_me keep me logged in 
	 */
	function login() {

		$this->set('schema_user', $this->User->schema());
		if ($this->request->is('post')) {
			if ($this->Auth->login() && $this->Auth->User('role_id') == ROLE_ID_ADMIN) {

				if (empty($this->request->data['User']['remember_me'])) {
					$this->RememberMe->delete();
				} else {
					$this->RememberMe->remember(
							$this->request->data['User']['username'], $this->request->data['User']['password']
					);
				}

				unset($this->request->data['User']['remember_me']);
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
				// $this->logout(); // in case if user other than admin logins in so we need to expire it session
			}
		}
	}

}
