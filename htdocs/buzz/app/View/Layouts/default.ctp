<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$desc = __d('cake_dev', SITE_NAME);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $desc ?>:
			<?php echo $title_for_layout; ?>
		</title>
		<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->script(array('http://code.jquery.com/jquery-latest.min.js' , 'jquery.form' , 'jquery.validate'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		?>
	</head>
	<body>
		<div id="container">
			<div id="header"> 
				<?php if(!empty($is_loggedin)): ?>
				<h1 class="fr"> 
					<?php echo $this->Html->link('Logout', array('controller' => 'users' , 'action' => 'logout')); ?>
				</h1>
				<?php endif;?>
			</div>
			<div id="content">

				<?php echo $this->Session->flash(); ?>

				<?php echo $this->fetch('content'); ?>
			</div>
			<div id="footer">
				<p class="tac">
					&copy; <?php echo SITE_NAME . ' ' . date('Y', time()) ?><!--. All Rights Reserved. Powered by <a href="http://www.apppli.com/">Apppli.--></a>
				</p>
			</div>
		</div>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>
