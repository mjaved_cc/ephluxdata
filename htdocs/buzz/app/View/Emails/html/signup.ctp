<table width="650" cellpadding="0" cellspacing="0" border="0" align="center" 
       style="background-color:#e1e1d5; font-size:12px; color: #7c7c7c; margin-bottom:20px">
    <tr>
	<td height="20">
	</td>
    </tr>
    <tr>
	<td>
	    <table width="650" cellpadding="0" cellspacing="0" border="0">
		<tr>
		    <td valign="top">
			<table width="60" cellpadding="0" cellspacing="0" border="0">
			    <tr>
				<td width="60" height="52" align="right" 
				    style="background-color:#82738a; padding-right:10px; font-size:20px; color: #ffffff; line-height:0.7em">
				    Buzz<br /><span style="font-size:11px">Community</span>
				</td>
			    </tr>
			    <tr>
				<td>
				</td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		    <td valign="top">
			<table width="550" cellpadding="0" cellspacing="0" border="0" align="center">
			    <tr>
				<td valign="top" style="font-size:18px; color: #202020; background-image:url(images/px.gif);
				    background-repeat:repeat-x; background-position:0 10px">
				    <table cellpadding="0" cellspacing="0" border="0" align="left">
					<tr>
					    <td class="a1" valign="top" style="padding-right:10px">
						<span style="background-color:#e1e1d5; padding-right:10px"><a href="JavaScript:void(0);">Your Buzz Account needs Verification</a></span>
					    </td>
					    <td>
					    </td>
					</tr>
				    </table>
				</td>
			    </tr>
			    <tr>
				<td style="padding-top:13px">
				    Dear User, 

				    Thanks for registering an account at Buzz (a Social Media Calculator) where you can value your efforts on the social forums. 
				    Buzz will help you evaluate your social media activity on social forums such as Facebook, Twitter and Linkedin. To activate your account, please click or Copy & Paste the following link. 

				</td>
			    </tr>
			    <tr>
				<td style="padding-top:13px; padding-bottom:20px">
				    <a href="<?php echo $message; ?>" style="float:left"><img src="<?php echo SMC_BASE_URI; ?>/img/verify.jpg" border="0" width="76" height="30" alt="" style="display: block" /></a> OR <?php echo $message; ?>
				</td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		</tr>
	    </table>
	</td>
    </tr>
    <tr>
	<td height="2" style="background-color:#e1e1d5">
	</td>
    </tr>
</table>