<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php
$message = explode("\n", $message);
$render = '';
foreach ($message as $line):
    $render .= $line;
endforeach;
?>

<table width="650" cellpadding="0" cellspacing="0" border="0" align="center" 
       style="background-color:#e1e1d5; font-size:12px; color: #7c7c7c; margin-bottom:20px">
    <tr>
	<td height="20">
	</td>
    </tr>
    <tr>
	<td>
	    <table width="650" cellpadding="0" cellspacing="0" border="0">
		<tr>
		    <td valign="top">
			<table width="60" cellpadding="0" cellspacing="0" border="0">
			    <tr>
				<td width="60" height="52" align="right" 
				    style="background-color:#82738a; padding-right:10px; font-size:20px; color: #ffffff; line-height:0.7em">
				    Buzz<br /><span style="font-size:11px">Community</span>
				</td>
			    </tr>
			    <tr>
				<td>
				</td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		    <td valign="top">
			<table width="550" cellpadding="0" cellspacing="0" border="0" align="center">
			    <tr>
				<td valign="top" style="font-size:18px; color: #202020; background-image:url(images/px.gif);
				    background-repeat:repeat-x; background-position:0 10px">
				    <table cellpadding="0" cellspacing="0" border="0" align="left">
					<tr>
					    <td style="padding-top:13px">
						<?php echo $render ; ?>
					    </td>
					</tr>
				    </table>
				</td>
			    </tr>  			    
			    <tr>
				<td style="padding-top:13px; padding-bottom:20px"></td>
			    </tr>
			</table>
		    </td>
		    <td width="20">
		    </td>
		</tr>
	    </table>
	</td>
    </tr>
    <tr>
	<td height="2" style="background-color:#e1e1d5">
	</td>
    </tr>
</table>