<div class="users index">
	<?php if (isset($obj_user['UserSocialAccount']) && count($obj_user['UserSocialAccount']) > 0) { ?>
		<h2><?php echo __('Social Details > ' . $obj_user['User']->fields['screen_name']); ?></h2>	
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo h('Logo'); ?></th>
				<th><?php echo h('Social Network'); ?></th>
				<th><?php echo h('Social Name'); ?></th>
				<!--<th class="actions"><?php echo __('Actions'); ?></th>-->
			</tr>
			<?php foreach ($obj_user['UserSocialAccount'] as $social_detail): ?>
				<tr>
					<td><img height="80" width="80" src="<?php echo h($social_detail->fields['image_url']); ?>" />&nbsp;</td>	
					<td><?php echo h($social_detail->fields['type_id']); ?>&nbsp;</td> 
					<td><?php echo h($social_detail->fields['screen_name']); ?>&nbsp;</td>
					<td class="actions">					
						<?php
						$type_id = strtolower($social_detail->fields['type_id']);
						if (in_array($type_id, array(TYPE_ID_FACEBOOK, TYPE_ID_LINKEDIN))) {
							$hidden_fields = array();
							$hidden_fields['data']['type_id'] = $type_id;
							$hidden_fields['data']['user_id'] = $social_detail->fields['user_id'];
							echo $this->Form->postLink(__('Disconnect'), array('action' => 'dc_social_account'), $hidden_fields, __('Are you sure you want to disconnect %s?', $social_detail->fields['screen_name']));
						} else {
							echo $this->Form->postLink(__('Disconnect'), array('action' => 'delete', $social_detail->fields['user_id']), null, __('Twiiter is primary account. Disconnecting twitter account is equivalent to remove user.Are you sure you want to remove user # %s?', $social_detail->fields['user_id']));
						}
						?>
					</td>
				</tr>
			<?php
			endforeach;
		} else {
			echo 'No social association found.';
		}
		?>
	</table>	
</div>
<?php echo $this->element('sidebar'); ?>
