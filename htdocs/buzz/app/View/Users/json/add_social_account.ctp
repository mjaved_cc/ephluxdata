<?php

if (isset($param_empty) && $param_empty) {
    $data['Response'] = "0";
    $data['Message'] = 'Invalid parametres';
} else if (isset($assoc_error) && $assoc_error) {
    $data['Response'] = "0";
    $data['Message'] = 'You can only associate one ' . $type_id . ' acount.';
} else if (isset($success) && $success) {
    $data['Response'] = "1";
    $data['Data'] = array_merge($user_details, $stats);
} else {
    $data['Response'] = "0";
    $data['Message'] = 'Record already exists.';
}

echo json_encode($data);
?>