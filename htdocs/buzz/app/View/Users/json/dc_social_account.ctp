<?php

if (isset($param_empty) && $param_empty) {
	$data['Response'] = "0";
	$data['Message'] = 'Invalid parametres';
} else if(isset($success) && $success) {
	$data['Response'] = "1";
	$data['Message'] = 'Disconnected successfully';
}else {
	$data['Response'] = "0";
	$data['Message'] = 'Unable to disconnect. Please try again later.';
}

echo json_encode($data);
?>