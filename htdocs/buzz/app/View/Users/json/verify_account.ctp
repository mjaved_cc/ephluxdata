<?php

if (isset($param_empty) && $param_empty) {
	$data['Response'] = "0";
	$data['Message'] = 'Invalid parametres';
} else if(isset($invalid_vcode) && $invalid_vcode){	
	$data['Response'] = "0";
	$data['Message'] = 'verification code is invalid' ;
} else if(isset($already_activated) && $already_activated){
	$data['Response'] = "0";
	$data['Message'] = 'Account already activated' ;	
} else if(isset($success) && $success) {
	$data['Response'] = "1";
	$data['Message'] = 'Welcome to ' . SITE_NAME . '. Please login using your username and password.' ;	
}else {
	$data['Response'] = "0";
	$data['Message'] = 'Unable to verify your account. Please try again later.';
}

echo $data['Message'];
?>