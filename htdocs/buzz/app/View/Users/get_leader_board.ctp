<div class="users view">
	<h2><?php echo __('Leader Board'); ?></h2>
	<?php if (!empty($leader_board)) { ?>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th><?php echo h('Logo'); ?></th>	
				<th><?php echo $this->Paginator->sort('screen_name', 'Social Name'); ?></th>
				<th><?php echo $this->Paginator->sort('score'); ?></th>
			</tr>
			<?php foreach ($leader_board as $value): ?>
				<tr>
					<td><img height="80" width="80" src="<?php echo h($value['User']['image_url']); ?>" />&nbsp;</td>
					<td><?php echo h($value['User']['screen_name']); ?>&nbsp;</td>
					<td><?php echo h($value['User']['score']); ?>&nbsp;</td>
				</tr>
			<?php endforeach; ?>
		</table>
	<?php
	} else {
		echo 'No records found';
	}
	?>
</div>
<?php echo $this->element('sidebar'); ?>

