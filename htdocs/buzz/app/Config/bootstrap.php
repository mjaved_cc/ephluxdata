<?php

/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as 
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'File', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, // [optional]
 * 		'mask' => 0666, // [optional] permission mask to use when creating cache files
 * 	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Apc', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Xcache', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 * 		'user' => 'user', //user from xcache.admin.user settings
 * 		'password' => 'password', //plaintext password (xcache.admin.pass)
 * 	));
 *
 * Memcache (http://memcached.org/)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Memcache', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'persistent' => true, // [optional] set this to false for non-persistent connections
 * 		'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
 * 	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 * 		'engine' => 'Wincache', //[required]
 * 		'duration'=> 3600, //[optional]
 * 		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 	));
 */
Cache::config('default', array('engine' => 'File'));



/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models', '/next/path/to/models'),
 *     'Model/Behavior'            => array('/path/to/behaviors', '/next/path/to/behaviors'),
 *     'Model/Datasource'          => array('/path/to/datasources', '/next/path/to/datasources'),
 *     'Model/Datasource/Database' => array('/path/to/databases', '/next/path/to/database'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions', '/next/path/to/sessions'),
 *     'Controller'                => array('/path/to/controllers', '/next/path/to/controllers'),
 *     'Controller/Component'      => array('/path/to/components', '/next/path/to/components'),
 *     'Controller/Component/Auth' => array('/path/to/auths', '/next/path/to/auths'),
 *     'Controller/Component/Acl'  => array('/path/to/acls', '/next/path/to/acls'),
 *     'View'                      => array('/path/to/views', '/next/path/to/views'),
 *     'View/Helper'               => array('/path/to/helpers', '/next/path/to/helpers'),
 *     'Console'                   => array('/path/to/consoles', '/next/path/to/consoles'),
 *     'Console/Command'           => array('/path/to/commands', '/next/path/to/commands'),
 *     'Console/Command/Task'      => array('/path/to/tasks', '/next/path/to/tasks'),
 *     'Lib'                       => array('/path/to/libs', '/next/path/to/libs'),
 *     'Locale'                    => array('/path/to/locales', '/next/path/to/locales'),
 *     'Vendor'                    => array('/path/to/vendors', '/next/path/to/vendors'),
 *     'Plugin'                    => array('/path/to/plugins', '/next/path/to/plugins'),
 * ));
 *
 */
/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

// Site Name
define('SITE_NAME', 'SMC');

//***********************************/
// Email defualts
//***********************************/
//no reply email address
define('EMAIL_NOREPLY', 'dev.ephlux@gmail.com');
/*
 if (getenv('SERVER_ADDR') == '127.0.0.1') { // local
	
	define('SMC_BASE_URI', 'http://dev.socialmedia.com');
	define('SMC_SERVER_ADDR', '127.0.0.1');
	
// twitter keys
	define('TWITTER_CONSUMER_KEY', 'ocYydPeSkhYpShfRTKHpZw');
	define('TWITTER_CONSUMER_KEY_SECRET', 'vwsTMyzqRDTJAO9oixiaEDPvmGx4BeYFKAi3YUAULM');

// linkedin keys
	define('LINKEDIN_CONSUMER_KEY', 'szey7c90qhlv');
	define('LINKEDIN_CONSUMER_KEY_SECRET', '6MYQgAyJjIIagbj4');

// facebook
	define('FACEBOOK_KEY', '205713009494442');
	define('FACEBOOK_SECRET', '781cf398eef29d12f3b441148c3a95ef');
} else if (getenv('SERVER_ADDR') == '146.255.34.59') { // demo.apppli.com - staging 
	
//	define('SMC_BASE_URI', 'http://demo.apppli.com/smc');
//    define('SMC_SERVER_ADDR', '146.255.34.59');	
	define('SMC_BASE_URI', 'http://dev.socialmedia.com');
	define('SMC_SERVER_ADDR', '127.0.0.1');
 
	// twitter keys
	define('TWITTER_CONSUMER_KEY', 'GjQjOG6KHhOe3NNY2DOhw');
	define('TWITTER_CONSUMER_KEY_SECRET', 'pXIZLPIiDPYYD119gnMTb2KC3TM2Bk8Sh4x2GNV6c');

	// linkedin keys
	define('LINKEDIN_CONSUMER_KEY', '4b7a7hbo625p');
	define('LINKEDIN_CONSUMER_KEY_SECRET', '187T6nMqUaoSzxWN');

	// facebook
	define('FACEBOOK_KEY', '268758396558691');
	define('FACEBOOK_SECRET', '238d463b54c2095faa841c5b7fdcda77');
/*} else if (getenv('SERVER_ADDR') == '178.33.227.141') { // apps.themarketer.co.uk - live */
	
//	define('SMC_BASE_URI', 'http://apps.themarketer.co.uk' );
//	 define('SMC_SERVER_ADDR', '178.33.227.141');
	 define('SMC_BASE_URI', 'http://dev.socialmedia.com');
	define('SMC_SERVER_ADDR', '127.0.0.1');
	// twitter keys
	define('TWITTER_CONSUMER_KEY', 'WDWiwanz0HVneBrUWaFJjA');
	define('TWITTER_CONSUMER_KEY_SECRET', '3ckfhx3n8WJOSLKEgDCEDxTInDYPhqOdkZkvA5E34');

// linkedin keys
	define('LINKEDIN_CONSUMER_KEY', 'qx2i7zxsei1x');
	define('LINKEDIN_CONSUMER_KEY_SECRET', 'tRtZMz13OCejIhLa');

// facebook
	define('FACEBOOK_KEY', '288444814589121');
	define('FACEBOOK_SECRET', 'a530eb9d47585d18895f46f23b3983ef');
// } */

// searching result from API for unregistered account
Cache::config('searching', array(
	'engine' => 'Memcache', //[required]
	'duration' => '+1 week', //[optional]
	'probability' => 100, //[optional]
	'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
	'servers' => array(
		SMC_SERVER_ADDR . ':11211' // localhost, default port 11211
	), //[optional]
	'persistent' => true, // [optional] set this to false for non-persistent connections
	'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
	'path' => CACHE . 'searching' . DS
));

// fetch data from various social networks for how many days.
define('FETCH_STATS_FOR_DAYS', 7);
define('A_WEEK', 7);
define('FETCH_STATS_FOR_ONE_WEEKS', 1);
define('FETCH_STATS_FOR_THREE_WEEKS', 3);

$SOCIAL_NETWORKS = array('facebook', 'twitter', 'linkedin');
Configure::write('SOCIAL_NETWORKS', $SOCIAL_NETWORKS);

$SOCIAL_NETWORKS_MODELS = array('CountFacebook', 'CountTwitter', 'CountLinkedin');
Configure::write('SOCIAL_NETWORKS_MODELS', $SOCIAL_NETWORKS_MODELS);

// Process Queues status
define('PQUEUE_STATUS_COMPLETE', 'complete');
define('PQUEUE_STATUS_INPROGRESS', 'inprogress');
define('PQUEUE_STATUS_INPENDING', 'inpending');
define('PQUEUE_STATUS_HIGH_PRIORITY', 'highpriority');

// User Social Account status
define('USA_STATUS_ACTIVE', 'active');
define('USA_STATUS_DELETED', 'delete');
define('USA_IS_VALID_TOKEN', '0');

// User status
define('USER_STATUS_ACTIVE', 'active');
define('USER_STATUS_DISABLED', 'disabled');
define('USER_STATUS_DELETED', 'deleted');
define('USER_STATUS_SUSPENDED', 'suspended');
// User in_process_queue possible values
define('USER_IN_PQUEUE', '1');
define('USER_NOT_IN_PQUEUE', '0');

// Bechmark status
define('BM_STATUS_ACTIVE', '1');

$USER_STATUS = array(
	USER_STATUS_ACTIVE => USER_STATUS_ACTIVE,
	USER_STATUS_DISABLED => USER_STATUS_DISABLED,
	USER_STATUS_SUSPENDED => USER_STATUS_SUSPENDED
);
Configure::write('USER_STATUS', $USER_STATUS);

define('ROLE_ID_ADMIN', 1);
define('ROLE_ID_USER', 2);

// this is used when user perform search & want to know details (stats) of any of search result which is not registered in our system.
define('FETCH_STATS_NON_SMC', -1);

define('IGNORE_PROCESS_QUEUE', 0);
define('EMPTY_BATCH_ID', 0);
define('SLEEP_TIME', 10);
define('CACHE_CONFIG_SEARCHING', 'searching');

// during stats calcuation we have analyzed that approximate time for it is 10-12 mints
define('APPROX_FETCHING_TIME_SECONDS', 720);

define('TYPE_ID_TWITTER', 'twitter');
define('TYPE_ID_SMC', 'smc');
define('TYPE_ID_FACEBOOK', 'facebook');
define('TYPE_ID_LINKEDIN', 'linkedin');
define('TYPE_ID_TWITTER_SIGNUP', 'twitter_signup');

define('LAYOUT_JSON', 'json');
define('PREFIX_DATATYPE_CLASS', 'Dt');

// validate token of social network after given duration in days
define('VALIDATE_TOKEN_DURATION', '10'); // days

// custom error code for token expiration
define('ECODE_TOKEN_EXPIRE', '1'); // days
// error code for any success
define('ECODE_SUCCESS', '0'); 

// CRON 
define('CRON_EMAIL_ALERT', '1'); // turn email notification on
define('CRON_EMAIL_NOTIFIER', 'dev.ephlux@gmail.com'); // 
