<?php

class Benchmark extends AppModel {

	public $name = 'Benchmark';

	/*
	 * @desc: get details of users
	 * @param: query_params <array>
	 * @return: <array>
	 */

	function get_all($query_params = 0) {

		$clause['Benchmark.is_active'] = BM_STATUS_ACTIVE;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		return $this->find('all', array(
					'conditions' => array($clause),
					'fields' => array('benchmark','weightage','engagement','influence','reach','keyword')
				));
	}

}