<?php

class ProcessQueue extends AppModel {

	public $name = 'ProcessQueue';
	private $limit = 200;

	/*
	 * @desc: get all inpending records from queue and change its status to inprogress as its going for processing. & then return.
	 * @params: query_params <array>
	 * @return: <array>
	 */

	function get_all_inpending($query_params = 0) {

		$clause['ProcessQueue.status'] = PQUEUE_STATUS_INPENDING;

		if (!empty($query_params))
			$clause = array_merge($clause, $query_params);

		$ids_collection = $this->find('list', array(
			'conditions' => array($clause),
			'order' => 'schedule_date ASC',
			'limit' => $this->limit
				));

		$this->set_status_inprogess($ids_collection);

		// since status has been updated => inprogress
		$query_params['ProcessQueue.status'] = PQUEUE_STATUS_INPROGRESS;
		$clause = array_merge($clause, $query_params);

		return $this->find('all', array(
					'conditions' => array($clause),
					'order' => 'schedule_date ASC',
					'limit' => $this->limit
				));
	}

	/*
	 * @desc: update all status to inprogess with given process queue id
	 * @params: ids_collection <array>
	 * @return: null
	 */

	function set_status_inprogess($ids_collection) {

		// By default, updateAll() will automatically join any belongsTo association for databases that support joins. To prevent this, temporarily unbind the associations.

		$this->updateAll(
				array('ProcessQueue.status' => "'" . PQUEUE_STATUS_INPROGRESS . "'"), array('ProcessQueue.id ' => $ids_collection)
		);
	}

	/*
	 * @desc: update status to inpending with given process queue id
	 * @params: id <int>
	 * @return: null
	 */

	function set_status_inpending($id) {
		// id 0 is provided when user add any social account.
		if ($id) {
			// get last record of process queue based on date & update the date of given process queue id with last one. so that queue will not suffer starvation. Each will get its turn on time.

			$last_queue = $this->find('first', array(
				'conditions' => array('ProcessQueue.status' => "'" . PQUEUE_STATUS_INPENDING . "'"),
				'order' => 'schedule_date DESC'
					));


			$this->id = $id;
			$this->set('status', PQUEUE_STATUS_INPENDING);
			if (!empty($last_queue)) // only update schedule_date if records other than this $id exists
				$this->set('schedule_date', $last_queue['ProcessQueue']['schedule_date']);
			$this->save();
		}
	}

	/*
	 * @desc: get social network stats from temp table
	 * @param: null
	 * @return: null
	 */

	function get_tmp_social_stats($query_params = 0) {

		$this->bindModel(array(
			'hasOne' => array(
				'Tmp' => array(
					'className' => 'Tmp',
					'foreignKey' => 'target_id'
				)
			)
		));

		$clause['ProcessQueue.status'] = PQUEUE_STATUS_COMPLETE;

		if (count($query_params) > 0)
			$clause = array_merge($clause, $query_params);

		return $this->find('all', array(
					'conditions' => array($clause)
				));
	}

	/*
	 * @desc: get details of p.queue
	 * @param: query_params <array>
	 * @return: <array>
	 */

	function get_details($query_params = 0) {

		return $this->find('first', array(
					'conditions' => array($query_params
					)
				));
	}

}

?>