<?php

class CountTwitter extends AppModel {

	public $name = 'CountTwitter';

	function get_all_details($query_params) {
		
		return $this->find('all', array(
			'conditions' => array($query_params),
			'fields' => array('count_tweets','count_retweets','count_mention','batch_id')
		));
	}

}