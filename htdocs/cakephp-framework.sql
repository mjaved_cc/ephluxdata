-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2013 at 05:18 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cakephp-framework`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_tmp_record`(IN `id` INT(5))
    NO SQL
BEGIN 
    DELETE FROM tmps
    WHERE  tmps.id = id ; 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_details`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.*
FROM users AS User 
WHERE User.username = email AND User.status = 'active'; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_social_account_data`(IN `id` INT(5))
    NO SQL
BEGIN 
SELECT UserSocialAccount.*
FROM user_social_accounts AS UserSocialAccount
WHERE UserSocialAccount.user_id = id AND UserSocialAccount.status = 'active';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_tmp_data`(IN `email` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.*
FROM tmps AS Tmp 
WHERE Tmp.target_id = email AND Tmp.type = type; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 
SELECT Tmp.target_id
FROM tmps AS Tmp
WHERE Tmp.target_id = email AND Tmp.type = 'registration';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5))
    NO SQL
UPDATE users
    SET    
           users.password = password                    
    WHERE  users.id = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` VARCHAR(255))
    NO SQL
BEGIN 

DECLARE str VARCHAR(255);  
SET str = 'success';
SET status = str;
    INSERT INTO tmps
         (
           tmps.target_id                  , 
           tmps.type                       , 
           tmps.data                       , 
           tmps.expire_at                  ,
           tmps.created
         )
    VALUES 
         ( 
           target_id                    , 
           type                         , 
           data                         ,  
           expire_at                    ,
           created
          
         ) ; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `save_user_record`(IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `username` VARCHAR(100), IN `password` VARCHAR(100), IN `role_id` INT(5), IN `created` DATETIME, OUT `status` VARCHAR(255))
    NO SQL
BEGIN 

DECLARE str VARCHAR(255);  
SET str = 'success';
SET status = str;

    INSERT INTO users
         (
           users.first_name                  , 
           users.last_name                   , 
           users.username                    , 
           users.password                    ,
           users.role_id                     ,
           users.created
         )
    VALUES 
         ( 
           first_name                       , 
           last_name                        , 
           username                         ,  
           password                         ,
           role_id                          ,
           created
          
         ) ; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 
SELECT User.*
FROM users AS User 
WHERE User.username = email AND User.password = pass AND User.status = 'active'; 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created`, `modified`) VALUES
(1, 'The title', 'This is the post body.', '2012-12-18 14:28:30', '2012-12-18 10:14:07'),
(2, 'A title once again', 'And the post body follows.', '2012-12-18 14:28:30', NULL),
(3, 'Title strikes back', 'This is really exciting! Not.', '2012-12-18 14:28:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_active`, `created`, `modified`) VALUES
(1, 'Admin', 1, '2012-05-07 16:25:44', '2012-05-07 16:25:44'),
(2, 'User', 1, '2012-05-07 16:25:44', '2012-05-07 16:25:44');

-- --------------------------------------------------------

--
-- Table structure for table `tmps`
--

CREATE TABLE IF NOT EXISTS `tmps` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` varchar(255) NOT NULL COMMENT 'string field holding the unique ID of a record for e.g registration',
  `type` enum('registration','forgot password') NOT NULL,
  `data` mediumtext NOT NULL,
  `expire_at` date NOT NULL COMMENT 'Date on which the row will be expired / Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tmps`
--

INSERT INTO `tmps` (`id`, `target_id`, `type`, `data`, `expire_at`, `created`, `modified`) VALUES
(2, 'acdae', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"javed","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-01-30 08:20:21","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-17', '2013-03-14 18:12:12', '2013-03-14 18:12:12'),
(3, '223e4', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"javed","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-01-30 08:20:21","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-17', '2013-03-14 18:23:41', '2013-03-14 18:23:41'),
(4, 'f075c', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"javed","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-01-30 08:20:21","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-17', '2013-03-14 18:34:59', '2013-03-14 18:34:59'),
(5, 'e0982', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"javed","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-01-30 08:20:21","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-17', '2013-03-14 18:36:34', '2013-03-14 18:36:34'),
(7, '9175d', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"7a32af0d99fc99a69864da5df2f2806c","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 12:55:09","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 13:31:26', '2013-03-18 13:31:26'),
(8, 'b36a9', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 13:45:06","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 14:20:38', '2013-03-18 14:20:38'),
(9, '73cee', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 13:45:06","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 16:27:04', '2013-03-18 16:27:04'),
(10, '9dc89', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 13:45:06","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 16:28:44', '2013-03-18 16:28:44'),
(11, '22294', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 13:45:06","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 16:30:18', '2013-03-18 16:30:18'),
(12, '77dee', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 13:45:06","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 16:46:25', '2013-03-18 16:46:25'),
(13, 'c7669', 'forgot password', '{"User":{"id":"8","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-01-30 08:20:21","modified":"2013-03-18 16:51:07","status":"active"},"Role":{"id":"2","name":"User","is_active":true,"created":"2012-05-07 16:25:44","modified":"2012-05-07 16:25:44"}}', '2013-03-21', '2013-03-18 16:53:21', '2013-03-18 16:53:21'),
(14, '11269', 'forgot password', '{"User":{"id":"37","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":"2013-03-22 19:59:10","modified":"2013-03-22 19:59:10","status":"active"}}', '2013-03-31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '8e6d1', 'forgot password', '{"User":{"id":"17","first_name":"azhar","last_name":"javed","username":"javed.nedian@hotmail.com","password":"b5ae6faf8e81146538383bd8e1be90f2","role_id":"2","ip_address":null,"created":"2013-03-22 19:59:10","modified":"2013-03-22 19:59:10","status":"active"}}', '2013-03-31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '36a58', 'forgot password', '{"User":{"id":"17","first_name":"azhar","last_name":"javed","username":"javed.nedian@hotmail.com","password":"b5ae6faf8e81146538383bd8e1be90f2","role_id":"2","ip_address":null,"created":"2013-03-22 19:59:10","modified":"2013-03-22 19:59:10","status":"active"}}', '2013-04-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '91e60', 'forgot password', '{"User":{"id":"17","first_name":"azhar","last_name":"javed","username":"javed.nedian@hotmail.com","password":"b5ae6faf8e81146538383bd8e1be90f2","role_id":"2","ip_address":null,"created":"2013-03-22 19:59:10","modified":"2013-03-22 19:59:10","status":"active"}}', '2013-04-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '64010', 'forgot password', '{"User":{"id":"19","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":null,"modified":null,"status":"active"}}', '2013-04-04', '2013-04-01 00:00:00', '0000-00-00 00:00:00'),
(45, '120e0', 'forgot password', '{"User":{"id":"19","first_name":"azhar","last_name":"javed","username":"javed_neduet@yahoo.com","password":"54b4e5b5ff0b9c146b5c696a3587ad82","role_id":"2","ip_address":null,"created":null,"modified":null,"status":"active"}}', '2013-04-04', '2013-04-01 12:37:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tusers`
--

CREATE TABLE IF NOT EXISTS `tusers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `facebook_id` varchar(20) DEFAULT NULL,
  `twitter_id` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tusers`
--

INSERT INTO `tusers` (`id`, `username`, `facebook_id`, `twitter_id`, `password`, `role`, `created`, `modified`) VALUES
(1, 'admin', '0', NULL, 'admin', 'admin', NULL, '2013-01-10 08:36:28'),
(2, 'tahir', '0', NULL, 'tahir', 'admin', '2012-12-31 13:12:14', '2012-12-31 13:12:14'),
(7, 'Ayan Ahmed', '100004317392698', NULL, NULL, NULL, '2013-01-16 13:08:28', '2013-01-16 13:08:28'),
(6, 'Muhammad Javed', '100000691275728', NULL, NULL, NULL, '2013-01-16 12:32:12', '2013-01-16 12:32:12'),
(9, 'Muhammad Javed', NULL, '430408044', NULL, NULL, '2013-01-22 08:22:29', '2013-01-22 08:22:29'),
(10, 'fahad tester', NULL, '714112616', NULL, NULL, '2013-01-22 10:23:32', '2013-01-22 10:23:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL COMMENT 'username is basically email address',
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `ip_address` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` enum('active','disabled','deleted','suspended') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `role_id`, `ip_address`, `created`, `modified`, `status`) VALUES
(14, 'javed', 'ashraf', 'javed.nedian@gmail.com', '54b4e5b5ff0b9c146b5c696a3587ad82', 2, NULL, '2013-03-21 14:47:57', '2013-03-21 14:47:57', 'active'),
(17, 'azhar', 'javed', 'javed.nedian@hotmail.com', 'b5ae6faf8e81146538383bd8e1be90f2', 2, NULL, '2013-03-22 19:59:10', '2013-03-22 19:59:10', 'active'),
(20, 'azhar', 'javed', 'javed_neduet@yahoo.com', '48c8a167a6b0f218927cf7bcd8536a98', 2, NULL, '2013-04-01 12:46:52', NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `user_social_accounts`
--

CREATE TABLE IF NOT EXISTS `user_social_accounts` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) unsigned NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `type_id` enum('facebook','twitter','linkedin') NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `access_token_secret` varchar(255) DEFAULT NULL,
  `is_valid_token` tinyint(1) NOT NULL DEFAULT '1',
  `screen_name` varchar(255) NOT NULL,
  `extra` mediumtext COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') NOT NULL DEFAULT 'active',
  `ip_address` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `user_social_accounts`
--

INSERT INTO `user_social_accounts` (`id`, `user_id`, `link_id`, `email_address`, `image_url`, `type_id`, `access_token`, `access_token_secret`, `is_valid_token`, `screen_name`, `extra`, `status`, `ip_address`, `created`, `modified`) VALUES
(10, 37, 100000691275728, 'javed.nedian@gmail.com', 'http://profile.ak.fbcdn.net/hprofile-ak-ash4/370785_100000691275728_1063599593_q.jpg', 'facebook', 'AAAHWj2l5sVQBAP9KpywNFLRKLCIeDFgYLB7RKagWklEuvuukoFBtyv6c6PhyNTgpKfX10WUo2ZBZBZBO7NO8ZAlvZB13nYBdijKkWamuiAQZDZD', NULL, 1, 'javed.nedian', NULL, 'active', 2130706433, '2013-01-24 16:26:30', '2013-01-24 16:26:30'),
(27, 37, 430408044, NULL, 'http://a0.twimg.com/sticky/default_profile_images/default_profile_3_normal.png', 'twitter', '430408044-h186zYMS3J0F8g7qypy8QbiZu92VlysQGPIsfsCr', 'Vt78kKAvp5Q4X43we2aLjgrVczDvMC2wgKbgIfxQj0c', 1, 'javednedian', NULL, 'active', 2130706433, '2013-01-24 16:08:15', '2013-01-24 16:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_subjects`
--

CREATE TABLE IF NOT EXISTS `user_subjects` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) NOT NULL,
  `total_marks` int(5) NOT NULL,
  `marks_obtained` float NOT NULL,
  `grade` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
