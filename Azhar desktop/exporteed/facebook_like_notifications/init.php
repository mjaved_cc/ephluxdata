<?php

  /**
   * My Reports module initialisation file
   */
  
  define('FACEBOOK_LIKE_NOTIFICATIONS_MODULE', 'facebook_like_notifications');
  define('FACEBOOK_LIKE_NOTIFICATIONS_MODULE_PATH', __DIR__);
  define('FACE_TYPE_INFO', 'info');
  
AngieApplication::setForAutoload(array(
    'FacebookLikeNotification' => FACEBOOK_LIKE_NOTIFICATIONS_MODULE_PATH . '/models/facebook_like_notifications/FacebookLikeNotification.class.php',
	
));
  
  ?>
