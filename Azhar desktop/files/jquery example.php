<!DOCTYPE html>
<html>
<head>
  <style>
  p { font-size:20px; width:200px; cursor:default; 
      color:blue; font-weight:bold; margin:0 10px; }
  .hilite { background:yellow; }
  </style>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<body >
  <p>
  When the day is short
  find that which matters to you
  or stop believing
  </p>
<script>
  var newText = $("p").text().split(" ").join("</span> <span>");
  newText = "<span>" + newText + "</span>";

  $("p").html( newText )
    .find('span')
    .hover(function() { 
      $(this).addClass("hilite"); 
    },
      function() { $(this).removeClass("hilite"); 
    })
  .end()
    .find(":contains('t')").css({"font-style":"italic", "font-weight":"bolder"});

</script>

</body>
</html>


function get($url) {
  
  // set required POST option for CURL
  $options = array(
   CURLOPT_URL => $url, // set the url   
   CURLOPT_RETURNTRANSFER => true, // return web page
   CURLOPT_HEADER => false, // don't return headers
   CURLOPT_ENCODING => "", // handle all encodings
   CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
   CURLOPT_TIMEOUT => 120   // timeout on response 
  );
 
  $ch = curl_init();
  curl_setopt_array($ch, $options);
  $content = curl_exec($ch);
  $error_code = curl_errno($ch);
  $error_msg = curl_error($ch);
  $header = curl_getinfo($ch);
  //close connection
  curl_close($ch);

  $header['error_code'] = $error_code;
  $header['error_msg'] = $error_msg;
  $header['content'] = $content;
  return $header;
 }

 <?php
                        $url = get_post_meta($post->ID, "url", true);

                        if ($url != "") {

                            $response = get($url);
                            $website_content = $response['content'];
                            preg_match_all('/<img[^>]+>/i', $website_content, $html);

                            //echo $html[0][9];  
                            ?>
                            <div id="image" style="height: 250px; width: 200px; padding: 10px;">
                                <?php echo $html[0][9]; ?>

                                <script> 
                                    $("#image > img").css("height", "100px"); 
                                    $("#image > img").css("width", "100px");  
                                </script>
                                <?php
                                $content = explode("<p>", $website_content);
                                //echo $content[19];
                                
                                $paragraph = substr($content[20], 0, 200);
                                //echo $content[20];
                                echo $paragraph;
                                
                                ?>
</div>