<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

// custom includes
App::uses('DtUser',					'Lib/Datatypes');
App::uses('DtUserSocialAccount',	    'Lib/Datatypes');
App::uses('DtDate',					'Lib/Datatypes');
App::uses('DtUserSetting',			'Lib/Datatypes');
App::uses('DtUserAddress',			'Lib/Datatypes');
App::uses('DtUserCard',				'Lib/Datatypes');
App::uses('DtSecureNumber',			'Lib/Datatypes');
App::uses('DtTransaction',			'Lib/Datatypes');
App::uses('DtOrder',			        'Lib/Datatypes');
App::uses('DtOrderItem',	   		    'Lib/Datatypes');

App::import('vendor', 'Qrcode');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $name = 'App';
	public $helpers = array('Html', 'Session', 'Form','Misc');
	public $components = array('Session', 'Cookie', 'RequestHandler', 'Auth', 'RememberMe','Curl');
	public $error_msg, $error_code, $user_id;

	function beforeFilter() {
		
		$this->Auth->allow('index');
		
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'prefix' => false);
		$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'generate_qrcode', 'prefix' => false);
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'index', 'prefix' => false);
		$this->Auth->loginError = 'Hmmm. We didn\'t recognise your login details. Want to try again?';

		// Add a pattern value detector.
		// $this->request->addDetector('iphone', array('env' => 'HTTP_USER_AGENT', 'pattern' => '/iPhone/i'));

		$this->Auth->authenticate = array(
			'Form' => array(
				'scope' => array('User.status' => 'active')
			)
		);

		// without hashing Password (cakephp default ) and use only md5
		Security::setHash("md5");

		// again, you can customize the component settings here, like this:  
		// $this->RememberMe->period = '+2 months';  
		$this->RememberMe->cookieName = 'SMC';

		$this->RememberMe->check();
		$this->user_id = $this->Auth->User('id');

		$this->set('is_loggedin', $this->Auth->User());
	}

	function is_valid_email($email = null) {
		if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
			return true;
		return false;
	}

	function get_current_datetime() {
		return date('Y-m-d H:i:s');
	}

	/*
	 * @desc: get expiry date from expired_at field of Tmp table
	 */

	function get_tmp_expiry() {
		return date('Y-m-d', strtotime("+3 month"));
	}

	function send_email($to, $subject, $message, $template = 'default', $layout = 'default') {
		
		if (IS_LOCAL) {
			$email = new CakeEmail('dev');
			$email->to($to);
			$email->template($template, $layout);
			$email->emailFormat('html');
			$email->viewVars(array('message' => $message));
			$email->subject($subject);
			$email->send();
		} else
			$email =  CakeEmail::deliver($to, $subject, $message, array('from' => EMAIL_NOREPLY) , false);
			$email->template($template, $layout);
			$email->emailFormat('html');
			$email->viewVars(array('message' => $message));
			$email->send();
	}

	/*	 * ***** How to save IP Address in DB ****** */
	/* INET_NTOA and INET_ATON functions in mysql. They convert between dotted notation IP address to 32 bit integers. This allows you to store the IP in just 4 bytes rather than a 15 bytes 
	 */

	/*
	  @desc: get an (IPv4) Internet network address into a string in Internet standard dotted format.
	  @param: number <string>
	  @return dotted ip address <string>
	 */

	function get_ip_address($number) {
		// analogous to INET_NTOA in mysql
		return sprintf("%s", long2ip($number));
	}

	/*
	  @desc: get string containing an (IPv4) Internet Protocol dotted address into a proper address
	  @param: null
	  @return number <unsigned-int>
	 */

	function get_numeric_ip_representation() {
		// analogous to INET_ATON in mysql 
		return sprintf("%u", ip2long(getenv('REMOTE_ADDR')));
	}

	/*	 * ***** END: How to save IP Address in DB ***** */

	function get_error_msg($error_code) {
		switch ($error_code) {
			case ERR_CODE_SUCCESS:
				return 'Success!';
			case ERR_CODE_FAILURE:
				return 'Something went wrong. Please try again later.';
			case ERR_CODE_INVALID_PARAMS:
				return 'Invalid parameters.';
			case ERR_CODE_INVALID_EMAIL:
				return 'Invalid Email Address.';
			case ERR_CODE_EMAIL_ALREADY_EXISTS:
				return 'Email Already Exist.';
			case ERR_CODE_UNVERIFIED_EMAIL:
				return 'We have found an unverified email. Please check your email.';
			case ERR_CODE_INVALID_VCODE:
				return 'Verification code is invalid.';
			case ERR_CODE_ACCOUNT_ALREADY_ACTIVATED:
				return 'Account already activated.';
			case ERR_CODE_SACC_ALREADY_EXISTS:
				return 'Social accout already exists.';
			case ERR_CODE_VALID_PWD:
				return 'Password must be between 8-20 characters.';
			case ERR_CODE_EMAIL_NOT_FOUND:
				return "We don't recognise that email address. Give it another shot.";
			case ERR_CODE_LOCATION_SERVICE:
				return 'Please enable Location Services.';
			case ERR_CODE_INVALID_USER:
				return "Invalid User.";
			case ERR_CODE_INVALID_CARD :
				return 'Invalid Card.';
			case ERR_CODE_INVALID_CARD_TYPE :
				return 'Invalid Card Type.';
			case ERR_CODE_RESULT_NOT_FOUND :
				return 'No result found.';
			case ERR_CODE_TRANS_INPENDING :
				return 'Transaction is in pending state.';
			case ERR_CODE_SHOPPING_CART_EMPTY :
				return 'Shopping cart is invalid';
			case ERR_CODE_DUPLICATE_SHOPPING_CART :
				return 'Transaction already proceed with your shopping cart.';			
		}
	}

}
