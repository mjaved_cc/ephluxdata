<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	public $name = 'Users';
	public $uses = array('User', 'Tmp', 'UserSocialAccount', 'UserSetting', 'UserAddress', 'UserCard','Transaction');
	public $paginate = array(
		'limit' => 25,
		'conditions' => array('User.status' => USER_STATUS_ACTIVE)
	);

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login','signin', 'signup', 'register', 'verify_account', 'recover', 'check_sacc_availability', 'get_terms_conditions');
		$this->_save_cart_in_session();
	}

	/*
	 * @desc: Called after controller action logic, but before the view is rendered.
	 */

	function beforeRender() {

		if (isset($this->error_code))
			$this->error_msg = $this->get_error_msg($this->error_code);
		$this->set('error_code', $this->error_code);
		$this->set('error_msg', $this->error_msg);
	}

	/*
	 * @desc: save card info in session
	 * @param: json_data <json>
	 * @return: null
	 */

	function _save_cart_in_session() {
		if ($this->request->is('post') && !empty($this->request->data['cart_data'])) {
			$this->Session->write('Data.CartInfo', $this->request->data['cart_data']);
		}

		// if user is logged in 
		if ($this->Auth->User('id'))
			$this->_save_cart_info();
	}

	function index() {
		$this->layout = LAYOUT_LOGIN;
	}

	/**
	 * Login - for web interface
	 */
	function login() {
		$this->layout = LAYOUT_LOGIN;

		$this->set('schema_user', $this->User->schema());
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->user_id = $this->Auth->User('id');
				if (empty($this->request->data['User']['remember_me'])) {
					$this->RememberMe->delete();
				} else {
					$this->RememberMe->remember(
							$this->user_id,
							$this->request->data['User']['username'], 
							$this->request->data['User']['password'],
							$this->Auth->User('role_id')
					);
				}
				$this->_save_cart_info();
				unset($this->request->data['User']['remember_me']);
				if ($this->Auth->User('role_id') == ROLE_ID_ADMIN)
					$this->redirect(array('controller' => 'backoffices', 'action' => 'list', 'consumers' => true));
				else
					$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
				// $this->logout(); // in case if user other than admin logins in so we need to expire it session
			}
		}
	}

	/*
	 * @desc: save cart info in tmp table
	 * @param: null
	 * @return: null
	 */

	function _save_cart_info() {
		$json_data = $this->Session->read('Data.CartInfo');
		$tmp_details = $this->Tmp->get_details($this->user_id, TMP_TYPE_CART);
		// check if session is not empty
		if (!empty($json_data)) {
			// update existing data
			if (!empty($tmp_details)) {
				$this->Tmp->id = $tmp_details['Tmp']['id'];
			} else { // create new entry for cart
				$this->Tmp->create();
				$this->Tmp->set('type', TMP_TYPE_CART);
				$this->Tmp->set('target_id', $this->user_id);
			}
			$this->Tmp->set('data', $json_data);
			$this->Tmp->set('expire_at', $this->get_current_datetime());
			$this->Tmp->save();
		}
	}

	function check_email_availability() {
		$email = $this->request->query['email'];
		$this->set('is_already_exist', $this->User->is_email_already_exists($email));
	}

	function success() {
		die('Success');
	}

	/**
	 * Register - for web interface
	 *  
	 * @uses signup Webservice 
	 */
	function register() {
		$this->layout = LAYOUT_LOGIN;
		$this->set('schema_user', $this->User->schema());
		if ($this->request->is('post')) {
			$this->request->data['signup_data'] = json_encode($this->request->data);
			$this->signup();
		}
	}

	/**
	 * Signup 
	 * @param username , password
	 * @return null
	 */
	function signup() {
		// http://book.cakephp.org/2.0/en/models/saving-your-data.html

		if ($this->request->is('post') && !empty($this->request->data['signup_data'])) {
			$json_data = $this->request->data['signup_data'];
			$user_info = json_decode($json_data, true);
			$email = $user_info['username'];
			$password = $user_info['password'];

			if (!$this->is_valid_email($email))
				$this->error_code = ERR_CODE_INVALID_EMAIL;
			else if ($this->User->is_email_already_exists($email))
				$this->error_code = ERR_CODE_EMAIL_ALREADY_EXISTS;
			else if (!$this->_is_valid_pasword($password))
				$this->error_code = ERR_CODE_VALID_PWD;
			else if (!$this->_is_enabled_loc_service($user_info) && $this->_is_json_layout())
				$this->error_code = ERR_CODE_LOCATION_SERVICE;
			else if ($this->Tmp->is_unverified_email_exists($email))
				$this->error_code = ERR_CODE_UNVERIFIED_EMAIL;
			else {
				// updating password field - use hashing/encryption
				$user_info['password'] = AuthComponent::password($password);
				$user_info['ip'] = $this->get_numeric_ip_representation();

				$this->Tmp->create();
				$this->Tmp->set('target_id', $email);
				$this->Tmp->set('data', json_encode($user_info));
				$this->Tmp->set('type', TMP_TYPE_REG);
				$this->Tmp->set('expire_at', $this->get_tmp_expiry());

				if ($this->Tmp->save()) {

					$ver_link = $this->_get_verification_link($email);
					$first_name = $user_info['first_name'];

					$message = $first_name . '|' . $ver_link;
					$this->send_email($email, SITE_NAME . ': Account Activation', $message, 'signup');

					$this->error_code = ERR_CODE_SUCCESS;
				} else
					$this->error_code = ERR_CODE_FAILURE;
			}
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: get link for email verification
	 * @params: email <string>
	 * @return: signup_link <string>
	 */

	function _get_verification_link($email, $type = VEMAIL_TYPE_SIGNUP) {

		$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
		$verification_code = substr(md5($code), 8, 5);

		$signup_link = POBOQ_BASE_URI . '/' . $this->params['controller'] . '/' . 'verify_account?v=' . $verification_code . '&email=' . $email . '&type=' . $type;

		return $signup_link;
	}

	/*
	 * @desc: verify email & move it from Tmp table to user table
	 * @params: v (verification code) , email 
	 */

	function verify_account() {
		$this->layout = 'web_email';
		if (!empty($this->request->query['v']) && !empty($this->request->query['email']) && $this->is_valid_email($this->request->query['email']) && !empty($this->request->query['type'])) {
			$verification_code = $this->request->query['v'];
			$email = $this->request->query['email'];
			$type = $this->request->query['type'];
			$code = 'Secret Code for ' . SITE_NAME . ' is ' . $email;
			if (substr(md5($code), 8, 5) == $verification_code) {

				$tmp_details = $this->Tmp->get_details($email, TMP_TYPE_REG);
				$data = json_decode($tmp_details['Tmp']['data']);

				if (!empty($data)) {
					$function_name = '_save_user_on_' . $type;
					$this->$function_name($data, $tmp_details);
				} else
					$this->error_code = ERR_CODE_ACCOUNT_ALREADY_ACTIVATED;
			} else
				$this->error_code = ERR_CODE_INVALID_VCODE;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: save user info when user just signup & verifies email
	 * @params: data <array>
	 * @return: null
	 */

	function _save_user_on_signup($data, $tmp_details) {

		$this->User->create();
		$this->User->set('username', $data->username);
		$this->User->set('password', $data->password);
		$this->User->set('first_name', $data->first_name);
		$this->User->set('last_name', $data->last_name);
		$this->User->set('country', (!empty($data->country)) ? $data->country : 0 );
		$this->User->set('city', (!empty($data->city)) ? $data->city : 0 );
		$this->User->set('lat', (!empty($data->lat)) ? $data->lat : 0 );
		$this->User->set('lng', (!empty($data->lng)) ? $data->lng : 0 );
		$this->User->set('role_id', ROLE_ID_USER);
		$this->User->set('ip_address', $data->ip);
		if ($this->User->save()) {
			$this->error_code = ERR_CODE_SUCCESS;
			$this->Tmp->delete($tmp_details['Tmp']['id']);

			$this->UserSetting->create();
			$this->UserSetting->set('user_id', $this->User->id);
			$this->UserSetting->save();
		} else
			$this->error_code = ERR_CODE_FAILURE;
	}

	/*
	 * @desc: save user info when user just signup & verifies email
	 * @params: data <array>
	 * @return: null
	 */

	function _save_user_on_change_email($data, $tmp_details) {

		$this->User->id = $data->user_id;
		if ($this->User->exists()) {
			$this->User->set('username', $data->username);
			$this->User->set('first_name', $data->first_name);
			$this->User->set('last_name', $data->last_name);
			$this->User->set('ip_address', $data->ip);
			if ($this->User->save()) {
				$this->error_code = ERR_CODE_SUCCESS;
				$this->Tmp->delete($tmp_details['Tmp']['id']);
			} else
				$this->error_code = ERR_CODE_FAILURE;
		} else
			$this->error_code = ERR_CODE_INVALID_USER;
	}

	/*
	 * @desc: signin service
	 * @param: 
	 * 	    type_id - can be facebook or twitter
	 * 		signin_data - data which is necessary for signin based on type_id
	 */

	function signin() {
		if ($this->request->is('post') && !empty($this->request->data['signin_data'])) {
			$json_data = $this->request->data['signin_data'];
			$user_info = json_decode($json_data, true);
			$type_id = strtolower($user_info['type_id']);

			if (!empty($type_id)) {
				// query_params is WHERE clause
				$query_params = array();

				if ($type_id == TYPE_ID_POBOQ) {
					// query params is WHERE clause.
					$query_params['User.username'] = $user_info['data']['username'];
					$query_params['User.password'] = AuthComponent::password($user_info['data']['password']);
				} else if (in_array($type_id, array(TYPE_ID_FACEBOOK, TYPE_ID_TWITTER))) {

					// social network user id
					$query_params['UserSocialAccount.link_id'] = $user_info['data']['id'];
					$query_params['UserSocialAccount.type_id'] = $type_id;
					$social_details = $this->UserSocialAccount->get_details($query_params);

					$function_name = '_save_' . $type_id . '_info';
					$user_id = $this->$function_name($user_info, $type_id, $social_details);

					unset($query_params);
					$query_params['User.id'] = $user_id;
				}

				// Be sure to manually add the new User id to the array passed to the login method. Otherwise you wont have the user id available. 
				// Reference URL: http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#manually-logging-users-in
				$user_details = $this->User->get_details($query_params);

				if ($user_details && $this->Auth->login($user_details['User'])) {
					$this->error_code = ERR_CODE_SUCCESS;
					$obj_user = new DtUser($user_details);

					if ($type_id == TYPE_ID_POBOQ) {
						if ($this->_is_enabled_loc_service($user_info['data'])) {
							$this->User->id = $this->Auth->User('id');
							$this->User->set('country', $user_info['data']['country']);
							$this->User->set('city', $user_info['data']['city']);
							$this->User->set('lat', $user_info['data']['lat']);
							$this->User->set('lng', $user_info['data']['lng']);
							$this->User->save();
						} else
							$this->error_code = ERR_CODE_LOCATION_SERVICE;
					}
				} else
					$this->error_code = ERR_CODE_FAILURE;

				$this->set(compact('obj_user', 'type_id'));
			} else
				$this->error_code = ERR_CODE_INVALID_PARAMS;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;

		// in case any error simply logout
		if (!empty($this->error_code))
			$this->Auth->logout();
	}

	/*
	 * @desc: save twitter information
	 * @params: 
	 * 		user_info - info get from twitter api
	 * 		type_id - twitter	
	 * 		social_details - social account details
	 */

	function _save_twitter_info($user_info, $type_id, $social_details) {

		$access_token = $user_info['data']['access_token'];
		$access_token_secret = $user_info['data']['access_token_secret'];
		if (empty($social_details)) {

			$email = $user_info['data']['email'];
			$user_id = $this->User->is_email_already_exists($email);

			// if email address found in our system is same as that of social network then we update user info. & associate 
			// social account with current profile
			if (empty($user_id)) // fresh user
				$this->User->create();
			else // associate social account with currrent profile
				$this->User->id = $user_id;

			$twitter_id = $user_info['data']['id'];

			if (!empty($user_info['data']['profile_image_url'])) {
				$normal_image_url = $user_info['data']['profile_image_url'];
				$path = pathinfo($normal_image_url);
				$filename = str_replace('_normal', '_bigger', $path['filename']);
				$profile_image_url = $path['dirname'] . '/' . $filename . '.' . $path['extension'];
			} else
				$profile_image_url = null;

			$this->User->set('country', (!empty($user_info['data']['country'])) ? $user_info['data']['country'] : 0);
			$this->User->set('city', (!empty($user_info['data']['city'])) ? $user_info['data']['city'] : 0);
			$this->User->set('lat', (!empty($user_info['data']['lat'])) ? $user_info['data']['lat'] : 0);
			$this->User->set('lng', (!empty($user_info['data']['lng'])) ? $user_info['data']['lng'] : 0);
			$this->User->set('username', $user_info['data']['email']);
			$this->User->set('password', AuthComponent::password($user_info['data']['password']));
			$this->User->set('ip_address', $this->get_numeric_ip_representation());
			$this->User->set('role_id', ROLE_ID_USER);
			$this->User->save();

			$this->UserSocialAccount->create();
			$this->UserSocialAccount->set('user_id', $this->User->id);
			$this->UserSocialAccount->set('image_url', $profile_image_url);
			$this->UserSocialAccount->set('link_id', $twitter_id);
			$this->UserSocialAccount->set('type_id', $type_id);
			$this->UserSocialAccount->set('email_address', $user_info['data']['email']);
			$this->UserSocialAccount->set('screen_name', $user_info['data']['screen_name']);
			$this->UserSocialAccount->set('access_token', $access_token);
			$this->UserSocialAccount->set('access_token_secret', $access_token_secret);
			$this->UserSocialAccount->set('ip_address', $this->get_numeric_ip_representation());
			$this->UserSocialAccount->save();

			// saving user settings @ signup only not for association. 
			if (empty($user_id)) {
				$this->UserSetting->get_details();
				$this->UserSetting->create();
				$this->UserSetting->set('user_id', $this->User->id);
				$this->UserSetting->save();
			}

			$user_id = $this->User->id;
		} else {
			// refreshing access token & access_token_secret
			if ($access_token != $social_details['UserSocialAccount']['access_token']) {
				$this->UserSocialAccount->id = $social_details['UserSocialAccount']['id'];
				$this->UserSocialAccount->set('access_token', $access_token);
				$this->UserSocialAccount->set('access_token_secret', $access_token_secret);
				$this->UserSocialAccount->save();
			}

			$user_id = $social_details['UserSocialAccount']['user_id'];

			// update location services for every signin - require up to date info for location
			$this->User->id = $user_id;
			$this->User->set('country', (!empty($user_info['data']['country'])) ? $user_info['data']['country'] : 0);
			$this->User->set('city', (!empty($user_info['data']['city'])) ? $user_info['data']['city'] : 0);
			$this->User->set('lat', (!empty($user_info['data']['lat'])) ? $user_info['data']['lat'] : 0);
			$this->User->set('lng', (!empty($user_info['data']['lng'])) ? $user_info['data']['lng'] : 0);
			$this->User->save();
		}

		return $user_id;
	}

	/*
	 * @desc: save facebook information
	 * @params: 
	 * 		user_info - info get from facebook api
	 * 		type_id - facebook	
	 */

	function _save_facebook_info($user_info, $type_id, $social_details) {

		$access_token = $user_info['data']['access_token'];

		if (empty($social_details)) {
			$email = $user_info['data']['email'];
			$user_id = $this->User->is_email_already_exists($email);

			// if email address found in our system is same as that of social network then we update user info. & associate 
			// social account with current profile
			if (empty($user_id)) // fresh user
				$this->User->create();
			else // associate social account with currrent profile
				$this->User->id = $user_id;

			$facebook_id = $user_info['data']['id'];

			if (!empty($user_info['data']['picture']['data']['url']))
				$profile_image_url = $user_info['data']['picture']['data']['url'];
			else
				$profile_image_url = null;

			$this->User->set('first_name', (!empty($user_info['data']['first_name'])) ? $user_info['data']['first_name'] : '');
			$this->User->set('last_name', (!empty($user_info['data']['last_name'])) ? $user_info['data']['last_name'] : '');
			$this->User->set('country', (!empty($user_info['data']['country'])) ? $user_info['data']['country'] : 0);
			$this->User->set('city', (!empty($user_info['data']['city'])) ? $user_info['data']['city'] : 0);
			$this->User->set('lat', (!empty($user_info['data']['lat'])) ? $user_info['data']['lat'] : 0);
			$this->User->set('lng', (!empty($user_info['data']['lng'])) ? $user_info['data']['lng'] : 0);
			$this->User->set('username', $user_info['data']['email']);
			$this->User->set('password', AuthComponent::password($user_info['data']['password']));
			$this->User->set('ip_address', $this->get_numeric_ip_representation());
			$this->User->set('role_id', ROLE_ID_USER);
			$this->User->save();

			$this->UserSocialAccount->create();
			$this->UserSocialAccount->set('user_id', $this->User->id);
			$this->UserSocialAccount->set('image_url', $profile_image_url);
			$this->UserSocialAccount->set('link_id', $facebook_id);
			$this->UserSocialAccount->set('type_id', $type_id);
			$this->UserSocialAccount->set('email_address', $user_info['data']['email']);
			$this->UserSocialAccount->set('screen_name', $user_info['data']['name']);
			$this->UserSocialAccount->set('access_token', $access_token);
			$this->UserSocialAccount->set('ip_address', $this->get_numeric_ip_representation());
			$this->UserSocialAccount->save();

			// saving user settings @ signup only not for association. 
			if (empty($user_id)) {
				$this->UserSetting->get_details();
				$this->UserSetting->create();
				$this->UserSetting->set('user_id', $this->User->id);
				$this->UserSetting->save();
			}

			$user_id = $this->User->id;
		} else {
			// refreshing access token
			if ($access_token != $social_details['UserSocialAccount']['access_token']) {
				$this->UserSocialAccount->id = $social_details['UserSocialAccount']['id'];
				$this->UserSocialAccount->set('access_token', $access_token);
				$this->UserSocialAccount->save();
			}

			$user_id = $social_details['UserSocialAccount']['user_id'];

			// update location services for every signin - require up to date info for location
			$this->User->id = $user_id;
			$this->User->set('country', (!empty($user_info['data']['country'])) ? $user_info['data']['country'] : 0);
			$this->User->set('city', (!empty($user_info['data']['city'])) ? $user_info['data']['city'] : 0);
			$this->User->set('lat', (!empty($user_info['data']['lat'])) ? $user_info['data']['lat'] : 0);
			$this->User->set('lng', (!empty($user_info['data']['lng'])) ? $user_info['data']['lng'] : 0);
			$this->User->save();
		}
		return $user_id;
	}

	/*
	 * @desc: check if social account already registered or not
	 * @params: 
	 * 		type_id - (facebook , twitter)
	 * 		link_id - social network user id
	 */

	function check_sacc_availability() {

		$data = json_decode($this->request->data['json_data'], true);
		if ($this->request->is('post') && !empty($data['type_id']) && !empty($data['link_id'])) {
			$user_id = $this->UserSocialAccount->get_userid_by_linkid($data['type_id'], $data['link_id']);
			if (empty($user_id))
				$this->error_code = ERR_CODE_SUCCESS;
			else
				$this->error_code = ERR_CODE_SACC_ALREADY_EXISTS;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	function recover() {
		$this->layout = LAYOUT_LOGIN;
		if ($this->request->is('post') && !empty($this->request->data['email'])) {
			// in db email is basically refered as username
			$username = $this->request->data['email'];
			if ($this->is_valid_email($username)) {
				$user_id = $this->User->is_email_already_exists($username);
				if ($user_id) {
					$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$'), 0, 10);
					$this->User->id = $user_id;
					$this->User->set('password', AuthComponent::password($password));

					if ($this->User->save()) {

						$user_details = $this->User->findById($user_id);

						$message = $user_details['User']['first_name'] . '|' . "Your password got reset to \r\n" . $password;

						$this->send_email($username, SITE_NAME . ': Password Reset', $message);

						$this->error_code = ERR_CODE_SUCCESS;
					} else
						$this->error_code = ERR_CODE_FAILURE;
				} else
					$this->error_code = ERR_CODE_EMAIL_NOT_FOUND;
			} else
				$this->error_code = ERR_CODE_INVALID_EMAIL;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	function logout() {

		$this->RememberMe->delete();
		$this->Session->delete('Data.CartInfo');

		if (!empty($this->request->params['ext']) && $this->request->params['ext'] == LAYOUT_JSON) {
			$this->Auth->logout();
			$this->error_code = ERR_CODE_SUCCESS;
		} else
			$this->redirect($this->Auth->logout());
	}

	/*
	 * @desc: check if character limit is between 8 & 20.
	 * @params: password
	 * @return: boolean
	 */

	function _is_valid_pasword($pwd) {

		$length = strlen($pwd);

		if ($length >= 8 && $length <= 20)
			return true;

		return false;
	}

	/*
	 * @desc: check if location service is enabled
	 * @params: password
	 * @return: boolean
	 */

	function _is_enabled_loc_service($user_info) {

		if (
				!empty($user_info['country']) &&
				!empty($user_info['city']) &&
				!empty($user_info['lat']) &&
				!empty($user_info['lng'])
		) {
			return true;
		}

		return false;
	}

	/**
	 * Set account settings
	 * @param json json_data
	 * @return null
	 */
	function set_account_settings() {
		if ($this->request->is('post') && !empty($this->request->data['json_data'])) {
			$data = json_decode($this->request->data['json_data'], true);
			$user_id = $data['user_info']['user_id'];

			foreach ($data as $key => $value) {
				$function_name = 'set_' . $key;
				$this->$function_name($user_id, $value);
				// in case of any error break
				if (!empty($this->error_code))
					break;
			}
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	function set_login_password_settings($user_id, $data) {
		$query_params['UserSetting.user_id'] = $user_id;
		$setting_details = $this->UserSetting->get_details($query_params);

		foreach ($data as $key => $value)
			$setting_details['UserSetting'][$key] = $value;

		if ($this->UserSetting->save($setting_details))
			$this->error_code = ERR_CODE_SUCCESS;
		else
			$this->error_code = ERR_CODE_FAILURE;
	}

	function set_shipping_address($user_id, $data) {
		$query_params['UserAddress.user_id'] = $user_id;
		$query_params['UserAddress.type_id'] = UA_TYPE_SHIPPING;
		$addr_details = $this->UserAddress->get_details($query_params);

		if (empty($addr_details))
			$this->UserAddress->create();
		else
			$this->UserAddress->id = $addr_details['UserAddress']['id'];

		foreach ($data as $key => $value)
			$this->UserAddress->set($key, $value);
		$this->UserAddress->set('type_id', UA_TYPE_SHIPPING);
		$this->UserAddress->set('user_id', $user_id);

		if ($this->UserAddress->save())
			$this->error_code = ERR_CODE_SUCCESS;
		else
			$this->error_code = ERR_CODE_FAILURE;
	}

	function set_billing_address($user_id, $data) {
		$query_params['UserAddress.user_id'] = $user_id;
		$query_params['UserAddress.type_id'] = UA_TYPE_BILLING;
		$addr_details = $this->UserAddress->get_details($query_params);

		if (empty($addr_details))
			$this->UserAddress->create();
		else
			$this->UserAddress->id = $addr_details['UserAddress']['id'];

		foreach ($data as $key => $value)
			$this->UserAddress->set($key, $value);
		$this->UserAddress->set('type_id', UA_TYPE_BILLING);
		$this->UserAddress->set('user_id', $user_id);

		if ($this->UserAddress->save())
			$this->error_code = ERR_CODE_SUCCESS;
		else
			$this->error_code = ERR_CODE_FAILURE;
	}

	function set_user_info($user_id, $data) {

		list($data['first_name'], $data['last_name']) = explode(' ', $data['name'], 2);
		$data['ip'] = $this->get_numeric_ip_representation();
		$email = $data['username'];
		$data['last_name'] = (!empty($data['last_name'])) ? $data['last_name'] : '';
		// already_exist basically contains user_id
		$already_exist = $this->User->is_email_already_exists($email);

		if (!$this->is_valid_email($email))
			$this->error_code = ERR_CODE_INVALID_EMAIL;
		else if (!empty($already_exist) && $user_id != $already_exist) // email does not belong to given user_id & is already exist
			$this->error_code = ERR_CODE_EMAIL_ALREADY_EXISTS;
		else if ($this->Tmp->is_unverified_email_exists($email))
			$this->error_code = ERR_CODE_UNVERIFIED_EMAIL;
		else if (empty($already_exist)) { // user has changed his email & is unique for system
			$this->Tmp->create();
			$this->Tmp->set('target_id', $email);
			$this->Tmp->set('data', json_encode($data));
			$this->Tmp->set('type', TMP_TYPE_REG);
			$this->Tmp->set('expire_at', $this->get_tmp_expiry());

			if ($this->Tmp->save()) {

				$ver_link = $this->_get_verification_link($email, VEMAIL_TYPE_CEMAIL);
				$message = $data['first_name'] . '|' . $ver_link;
				$this->send_email($email, SITE_NAME . ': Account Activation', $message, 'signup');

				$this->error_code = ERR_CODE_SUCCESS;
			} else
				$this->error_code = ERR_CODE_FAILURE;
		} else { // user has just change his name not email
			$this->User->id = $user_id;
			$this->User->set('first_name', $data['first_name']);
			$this->User->set('last_name', $data['last_name']);

			if ($this->User->save())
				$this->error_code = ERR_CODE_SUCCESS;
			else
				$this->error_code = ERR_CODE_FAILURE;
		}
	}

	/**
	 * Reset/Change password - web interface
	 * 
	 * @uses change_pwd Webservice
	 */
	function reset_pwd($hash_uid = 0) {		
		$this->User->unbindModelAll();

		if (!empty($hash_uid)) {
			$data = array();
			$user = $this->User->find('first', array('conditions' => array('md5(id)' => $hash_uid)));
			$obj_user = new DtUser($user);

			if (!empty($obj_user)) {
				$data['id'] = $obj_user->id;
				$data['display_name'] = $obj_user->display_name;
				$this->set(compact('data'));
			} else {
				$this->Session->setFlash(__('Bad request'));
				$this->redirect(array('controller' => 'backoffices', 'action' => 'users_list'));
			}
		} else {
			$this->Session->setFlash(__('Bad request'));
			$this->redirect(array('controller' => 'backoffices', 'action' => 'users_list'));
		}

		if ($this->request->is('post')) {
			$this->request->data['json_data'] = json_encode($this->request->data);
			$this->change_pwd();
		}
	}

	/*
	 * @desc: Change password service
	 * @params: null
	 * @return: null
	 */

	function change_pwd() {

		if ($this->request->is('post') && !empty($this->request->data['json_data'])) {
			$data = json_decode($this->request->data['json_data'], true);

			$user_id = $data['id'];
			$password = AuthComponent::password($data['password']);
			$new_password = AuthComponent::password($data['new_password']);

			if ($this->_is_valid_pasword($data['password'])) {
				$query_params['User.id'] = $user_id;
				$query_params['User.password'] = $password;
				$this->User->unbindModelAll();
				$user = $this->User->get_details($query_params);

				if (!empty($user)) {

					$email = $user['User']['username'];
					$user['User']['password'] = $new_password;

					if ($this->User->save($user)) {

						$message = $user['User']['first_name'] . '|' . 'Your password has been changed successfully';

						$this->send_email($email, SITE_NAME . ' Account password changed', $message, 'change_pwd');

						$this->error_code = ERR_CODE_SUCCESS;
					} else
						$this->error_code = ERR_CODE_FAILURE;
				} else
					$this->error_code = ERR_CODE_INVALID_USER;
			} else
				$this->error_code = ERR_CODE_VALID_PWD;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: add card for given user_id
	 * @param: json_data <json>
	 * @return: null
	 */

	function add_card() {
		if ($this->request->is('post') && !empty($this->request->data['json_data'])) {
			$data = json_decode($this->request->data['json_data'], true);
			$user = $this->User->custom_exists($data['user_id'], USER_STATUS_ACTIVE);
			$USER_CARD_TYPES = Configure::read('USER_CARD_TYPES');

			if (in_array($data['type_id'], $USER_CARD_TYPES)) {
				if (!empty($user)) {

					$this->UserCard->create();
					foreach ($data as $key => $value)
						$this->UserCard->set($key, $value);

					$saved_card = $this->UserCard->save();
					if (!empty($saved_card)) {
						$obj_card = new DtUserCard($saved_card['UserCard']);

						$this->error_code = ERR_CODE_SUCCESS;
						$this->set(compact('obj_card'));
					} else
						$this->error_code = ERR_CODE_FAILURE;
				} else
					$this->error_code = ERR_CODE_INVALID_USER;
			} else
				$this->error_code = ERR_CODE_INVALID_CARD_TYPE;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: edit card for given user_id
	 * @param: json_data <json>
	 * @return: null
	 */

	function edit_card() {

		$data = json_decode($this->request->data['json_data'], true);
		if ($this->request->is('post') && !empty($data['card_id']) && !empty($data['user_id'])) {
			$query_params['UserCard.id'] = $data['card_id'];
			$query_params['User.id'] = $data['user_id'];
			$card_details = $this->UserCard->get_details($query_params);

			if (!empty($card_details)) {
				$obj_card = new DtUserCard($card_details['UserCard']);
				$this->set(compact('obj_card'));
				$this->error_code = ERR_CODE_SUCCESS;
			} else
				$this->error_code = ERR_CODE_INVALID_CARD;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: update card for given user_id
	 * @param: json_data <json>
	 * @return: null
	 */

	function update_card() {
		if ($this->request->is('post') && !empty($this->request->data['json_data'])) {
			$data = json_decode($this->request->data['json_data'], true);

			$USER_CARD_TYPES = Configure::read('USER_CARD_TYPES');

			if (in_array($data['type_id'], $USER_CARD_TYPES)) {

				$query_params['UserCard.id'] = $data['card_id'];
				$query_params['User.id'] = $data['user_id'];
				$card_details = $this->UserCard->get_details($query_params);

				if (!empty($card_details)) {

					$this->UserCard->id = $card_details['UserCard']['id'];
					unset($data['card_id']); // removing unnecessary index 
					foreach ($data as $key => $value)
						$this->UserCard->set($key, $value);

					if ($this->UserCard->save())
						$this->error_code = ERR_CODE_SUCCESS;
					else
						$this->error_code = ERR_CODE_FAILURE;
				} else
					$this->error_code = ERR_CODE_INVALID_CARD;
			} else
				$this->error_code = ERR_CODE_INVALID_CARD_TYPE;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: delete card for given user_id
	 * @param: json_data <json>
	 * @return: null
	 */

	function delete_card() {

		$data = json_decode($this->request->data['json_data'], true);
		if ($this->request->is('post') && !empty($data['card_id']) && !empty($data['user_id'])) {
			$query_params['UserCard.id'] = $data['card_id'];
			$query_params['User.id'] = $data['user_id'];
			$card_details = $this->UserCard->get_details($query_params);

			if (!empty($card_details)) {
				$card_details['UserCard']['status'] = UC_STATUS_DELETED;
				if ($this->UserCard->save($card_details))
					$this->error_code = ERR_CODE_SUCCESS;
				else
					$this->error_code = ERR_CODE_FAILURE;
			} else
				$this->error_code = ERR_CODE_INVALID_CARD;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	/*
	 * @desc: encrypt information.
	 * @params: data - 1D array
	 * @return: encrypted data  
	 */

//	function _get_encrypted($data) {
//
//		if (!empty($data)) {
//			foreach ($data as &$value)
//				$value = Security::cipher($value, Configure::read('Security.salt'));
//			// pr($data); // FOR DEBUG ; debug() not working
//			return $data;
//		} else
//			return false;
//	}

	function _is_json_layout() {
		if (!empty($this->request->params['ext']) && $this->request->params['ext'] == LAYOUT_JSON)
			return true;
		return false;
	}

	function generate_qrcode() {
		$this->layout = LAYOUT_LOGIN;
		$controller = $this->request->params['controller'];
		$url = POBOQ_BASE_URI . '/' . $controller . '/get_shopping_cart/' . $this->user_id . '.json';
		$tmp_details = $this->Tmp->get_details($this->user_id, TMP_TYPE_CART);

		if (!empty($tmp_details)) {
			$img_name = md5($tmp_details['Tmp']['id']) . '.png';

			$cart_details = json_decode($tmp_details['Tmp']['data'], true);
			$cart_id = $cart_details['shopping_cart']['cart_id'];
			
			$query_params['Transaction.cart_id'] = $cart_id;

			// if cart already exists
			$cart_exists = $this->Transaction->get_details($query_params);
			
			if (empty($cart_exists)) {
				$this->error_code = ERR_CODE_SUCCESS ;
				if (is_dir(QR_CODE_IMAGE_PATH)) {
					$file_path = QR_CODE_IMAGE_PATH . $img_name;
					QRcode::png($url, $file_path, QR_ECLEVEL_L, 6); // creates code image and outputs it directly into browser
				}
				$this->set(compact('img_name'));
			} else {
				$this->error_code = ERR_CODE_DUPLICATE_SHOPPING_CART ;
			}
		} else {
			$this->error_code = ERR_CODE_SHOPPING_CART_EMPTY ;
		}
	}

	/*
	 * @desc: get shopping cart 
	 * @param: $user_id 
	 * @return: null
	 */

	function get_shopping_cart($user_id) {
		if (!empty($user_id)) {
			$tmp_details = $this->Tmp->get_details($user_id, TMP_TYPE_CART);
			
			if (!empty($tmp_details)) { 
				$cart_details = json_decode($tmp_details['Tmp']['data'], true);
				unset($cart_details['authentication']['callback_url']);

				// Secret key from 3rd party to poboq
				$sp_merchant = $cart_details['authentication']['secret_key'];
				
				// Secret key from poboq to iphone
				// secret key = user_id + tmp id + salt 
				$cart_details['authentication']['secret_key'] = md5($tmp_details['Tmp']['target_id'] . $tmp_details['Tmp']['id'] . APP_CUSTOM_SALT);
				$cart_details['authentication']['tmp_id'] = $tmp_details['Tmp']['id'];

				$post_values['sp_merchant'] = $sp_merchant;
				$post_values['method'] = PAYONESECURE_METHOD_GET_ALLOWED_CARDS;
				$this->Curl->url = PAYONESECURE_WEBSERVICE_URI;
				$this->Curl->data = $post_values;
				$response = $this->Curl->post();
				
				/*				 * ****************************** */

				// $response['error_code'] != 0 > error: bad url, timeout, redirect loop
				// $response['http_code'] != 200 > error: no page, no permissions, no service

				/*				 * ****************************** */

				if ($response['error_code'] == ERR_CODE_SUCCESS && $response['http_code'] == HTTP_STATUS_SUCCESS) {
					$content = json_decode($response['content'], true);
					
					if (empty($content['code']) && isset($content['code'])) {

						$this->error_code = ERR_CODE_SUCCESS;
						$allowed_cards = $content['data'];

						$cart_details['allowed_cards'] = $allowed_cards;

						$this->set(compact('cart_details'));
					} else // in case of failure						
						$this->error_code = ERR_CODE_FAILURE;
				} else // in case of failure					
					$this->error_code = ERR_CODE_FAILURE;
			} else
				$this->error_code = ERR_CODE_FAILURE;
		} else
			$this->error_code = ERR_CODE_INVALID_PARAMS;
	}

	function get_terms_conditions() {
		$this->error_code = ERR_CODE_SUCCESS;
		$terms_conditions = file_get_contents(WWW_ROOT . 'terms_conditions.html');
		$this->set(compact('terms_conditions'));
	}

}