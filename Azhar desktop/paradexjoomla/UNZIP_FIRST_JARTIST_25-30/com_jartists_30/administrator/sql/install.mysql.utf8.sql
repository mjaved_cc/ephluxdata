CREATE TABLE IF NOT EXISTS `#__jartists_albums` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `created` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__jartists_artists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `label_id` int(11) NOT NULL,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `biography` mediumtext NOT NULL,
  `genre` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `link_facebook` varchar(255) NOT NULL,
  `link_twitter` varchar(255) NOT NULL,
  `link_myspace` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__jartists_discography` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `cover` varchar(255) NOT NULL,
  `back_cover` varchar(255) NOT NULL,
  `itunes_link` varchar(255) NOT NULL,
  `amazon_link` varchar(255) NOT NULL,
  `date_release` date NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__jartists_genres` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `logo` varchar(255) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `genre` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jartists_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `logo` varchar(255) NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__jartists_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `web_address` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__jartists_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `image_file` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `hits` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `artist_id` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__jartists_photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `image_file` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `album_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__jartists_tour` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `event` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `tickets` varchar(255) NOT NULL,
  `poster_front` varchar(255) NOT NULL,
  `poster_back` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `hits` int(11) NOT NULL,
  `date` date NOT NULL,
  `created` datetime NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__jartists_videos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `featured` tinyint(255) NOT NULL,
  `alpha` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

