<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// No direct access
defined('_JEXEC') or die;
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
 # ========================================================================#
   #
   #  Author:    Jarrod Oberto
   #  Version:	 1.0
   #  Date:      17-Jan-10
   #  Purpose:   Resizes and saves image
   #  Requires : Requires PHP5, GD library.
   #  Usage Example:
   #                     include("classes/resize_class.php");
   #                     $resizeObj = new resize('images/cars/large/input.jpg');
   #                     $resizeObj -> resizeImage(150, 100, 0);
   #                     $resizeObj -> saveImage('images/cars/large/output.jpg', 100);
   #
   #
   # ========================================================================#


		Class resize
		{
			// *** Class variables
			private $image;
		    private $width;
		    private $height;
			private $imageResized;

			function __construct($fileName)
			{
				// *** Open up the file
				$this->image = $this->openImage($fileName);

			    // *** Get width and height
			    $this->width  = imagesx($this->image);
			    $this->height = imagesy($this->image);
			}

			## --------------------------------------------------------

			private function openImage($file)
			{
				// *** Get extension
				$extension = strtolower(strrchr($file, '.'));

				switch($extension)
				{
					case '.jpg':
					case '.jpeg':
						$img = @imagecreatefromjpeg($file);
						break;
					case '.gif':
						$img = @imagecreatefromgif($file);
						break;
					case '.png':
						$img = @imagecreatefrompng($file);
						break;
					default:
						$img = false;
						break;
				}
				return $img;
			}

			## --------------------------------------------------------

			public function resizeImage($newWidth, $newHeight, $option="auto")
			{
				// *** Get optimal width and height - based on $option
				$optionArray = $this->getDimensions($newWidth, $newHeight, $option);

				$optimalWidth  = $optionArray['optimalWidth'];
				$optimalHeight = $optionArray['optimalHeight'];


				// *** Resample - create image canvas of x, y size
				$this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
				imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);


				// *** if option is 'crop', then crop too
				if ($option == 'crop') {
					$this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
				}
			}

			## --------------------------------------------------------
			
			private function getDimensions($newWidth, $newHeight, $option)
			{

			   switch ($option)
				{
					case 'exact':
						$optimalWidth = $newWidth;
						$optimalHeight= $newHeight;
						break;
					case 'portrait':
						$optimalWidth = $this->getSizeByFixedHeight($newHeight);
						$optimalHeight= $newHeight;
						break;
					case 'landscape':
						$optimalWidth = $newWidth;
						$optimalHeight= $this->getSizeByFixedWidth($newWidth);
						break;
					case 'auto':
						$optionArray = $this->getSizeByAuto($newWidth, $newHeight);
						$optimalWidth = $optionArray['optimalWidth'];
						$optimalHeight = $optionArray['optimalHeight'];
						break;
					case 'crop':
						$optionArray = $this->getOptimalCrop($newWidth, $newHeight);
						$optimalWidth = $optionArray['optimalWidth'];
						$optimalHeight = $optionArray['optimalHeight'];
						break;
				}
				return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
			}

			## --------------------------------------------------------

			private function getSizeByFixedHeight($newHeight)
			{
				$ratio = $this->width / $this->height;
				$newWidth = $newHeight * $ratio;
				return $newWidth;
			}

			private function getSizeByFixedWidth($newWidth)
			{
				$ratio = $this->height / $this->width;
				$newHeight = $newWidth * $ratio;
				return $newHeight;
			}

			private function getSizeByAuto($newWidth, $newHeight)
			{
				if ($this->height < $this->width)
				// *** Image to be resized is wider (landscape)
				{
					$optimalWidth = $newWidth;
					$optimalHeight= $this->getSizeByFixedWidth($newWidth);
				}
				elseif ($this->height > $this->width)
				// *** Image to be resized is taller (portrait)
				{
					$optimalWidth = $this->getSizeByFixedHeight($newHeight);
					$optimalHeight= $newHeight;
				}
				else
				// *** Image to be resizerd is a square
				{
					if ($newHeight < $newWidth) {
						$optimalWidth = $newWidth;
						$optimalHeight= $this->getSizeByFixedWidth($newWidth);
					} else if ($newHeight > $newWidth) {
						$optimalWidth = $this->getSizeByFixedHeight($newHeight);
						$optimalHeight= $newHeight;
					} else {
						// *** Sqaure being resized to a square
						$optimalWidth = $newWidth;
						$optimalHeight= $newHeight;
					}
				}

				return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
			}

			## --------------------------------------------------------

			private function getOptimalCrop($newWidth, $newHeight)
			{

				$heightRatio = $this->height / $newHeight;
				$widthRatio  = $this->width /  $newWidth;

				if ($heightRatio < $widthRatio) {
					$optimalRatio = $heightRatio;
				} else {
					$optimalRatio = $widthRatio;
				}

				$optimalHeight = $this->height / $optimalRatio;
				$optimalWidth  = $this->width  / $optimalRatio;

				return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
			}

			## --------------------------------------------------------

			private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
			{
				// *** Find center - this will be used for the crop
				$cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
				$cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );

				$crop = $this->imageResized;
				//imagedestroy($this->imageResized);

				// *** Now crop from center to exact requested size
				$this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
				imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);
			}

			## --------------------------------------------------------

			public function saveImage($savePath, $imageQuality="100")
			{
				// *** Get extension
        		$extension = strrchr($savePath, '.');
       			$extension = strtolower($extension);

				switch($extension)
				{
					case '.jpg':
					case '.jpeg':
						if (imagetypes() & IMG_JPG) {
							imagejpeg($this->imageResized, $savePath, $imageQuality);
						}
						break;

					case '.gif':
						if (imagetypes() & IMG_GIF) {
							imagegif($this->imageResized, $savePath);
						}
						break;

					case '.png':
						// *** Scale quality from 0-100 to 0-9
						$scaleQuality = round(($imageQuality/100) * 9);

						// *** Invert quality setting as 0 is best, not 9
						$invertScaleQuality = 9 - $scaleQuality;

						if (imagetypes() & IMG_PNG) {
							 imagepng($this->imageResized, $savePath, $invertScaleQuality);
						}
						break;

					// ... etc

					default:
						// *** No extension - No save.
						break;
				}

				imagedestroy($this->imageResized);
			}


			## --------------------------------------------------------

		}

/**
 * Jartists helper.
 */
class JartistsHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		/*JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_GENRES'),
			'index.php?option=com_jartists&view=genres',
			$vName == 'genres'
		);*/
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_ARTISTS'),
			'index.php?option=com_jartists&view=artists',
			$vName == 'artists'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_DISCOGRAPHIES'),
			'index.php?option=com_jartists&view=discographies',
			$vName == 'discographies'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_NEWS'),
			'index.php?option=com_jartists&view=news',
			$vName == 'news'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_ALBUMS'),
			'index.php?option=com_jartists&view=albums',
			$vName == 'albums'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_PHOTOS'),
			'index.php?option=com_jartists&view=photos',
			$vName == 'photos'
		);

		if ($vName == 'photos')
		{
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_BATCH'),
			'index.php?option=com_jartists&view=upload',
			$vName == 'upload'
		);
		}
		
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_VIDEOS'),
			'index.php?option=com_jartists&view=videos',
			$vName == 'videos'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_EVENTS'),
			'index.php?option=com_jartists&view=events',
			$vName == 'events'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_LINKS'),
			'index.php?option=com_jartists&view=links',
			$vName == 'links'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_JARTISTS_TITLE_LABELS'),
			'index.php?option=com_jartists&view=labels',
			$vName == 'labels'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_jartists';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	
	function delPhotos($data)
	{
		$filename = $data->image_file;
		$original = '../images/jartists/gallery_'.$data->album_id;
		$thumbpath_n =   '../images/jartists/n';
		$thumbpath_s =   '../images/jartists/s';
		$thumbpath_m =   '../images/jartists/med';
		$thumbpath_l =   '../images/jartists/lg';
		$thumbpath_t =   '../images/jartists/thumbs';
		if(JFile::exists($original.DS.$filename)){
			JFile::delete($original.DS.$filename);
		}
		if(JFile::exists($thumbpath_s.DS.$filename)){
			JFile::delete($thumbpath_s.DS.$filename);
		}
		if(JFile::exists($thumbpath_m.DS.$filename)){
			JFile::delete($thumbpath_m.DS.$filename);
		}
		if(JFile::exists($thumbpath_n.DS.$filename)){
			JFile::delete($thumbpath_n.DS.$filename);
		}
		if(JFile::exists($thumbpath_l.DS.$filename)){
			JFile::delete($thumbpath_l.DS.$filename);
		}
	}
	
	function delFolders($data)
	{
		
		if(JFolder::delete('../images/jartists/gallery_'.DS.$data->id))
		{
			$db = JFactory::getDBO();
			$query = 'DELETE FROM `#__jartists_photos` WHERE album_id = '.$db->Quote($data->id);
			$db->setQuery($query);
			$db->query();
		}else{
			return false;
		}
	}
	
	function deletePhotos($filename)
	{
		$thumbpath_n =   '../images/jartists/n';
		$thumbpath_s =   '../images/jartists/s';
		$thumbpath_m =   '../images/jartists/med';
		$thumbpath_l =   '../images/jartists/lg';
		$thumbpath_t =   '../images/jartists/thumbs';
		if(JFile::exists($thumbpath_s.DS.$filename)){
			JFile::delete($thumbpath_s.DS.$filename);
		}
		if(JFile::exists($thumbpath_m.DS.$filename)){
			JFile::delete($thumbpath_m.DS.$filename);
		}
		if(JFile::exists($thumbpath_n.DS.$filename)){
			JFile::delete($thumbpath_n.DS.$filename);
		}
		if(JFile::exists($thumbpath_l.DS.$filename)){
			JFile::delete($thumbpath_l.DS.$filename);
		}
	}
	
	public function copyIndexHtml($folder)
	  {
		jimport('joomla.filesystem.file');
	
		$src  = JPATH_ROOT.DS.'components/com_jartists/assets/index.html';
		$dest = JPath::clean($folder.DS.'index.html');
	
		return JFile::copy($src, $dest);
	  }
	  
	public function createFolder($path)
	{
		if(!JFolder::exists($path)){
				JFolder::create($path);
				JartistsHelper::copyIndexHtml($path);
		}
	}
		function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $thumbWidth)
		{
		$srcImg = imagecreatefromjpeg("$imageDirectory/$imageName");
		$origWidth = imagesx($srcImg);
		$origHeight = imagesy($srcImg);		
		$ratio = $origHeight/ $origWidth;
		$thumbHeight = $thumbWidth * $ratio;		
		$thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight);
		imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);
		imagejpeg($thumbImg, "$thumbDirectory/$imageName");
		}
	function runGalleryThumbnail($oldImage=0, $filename, $imageDirectory)
	{
		$thumbpath_s =  '../images/jartists/s';
		JartistsHelper::createFolder($thumbpath_s);
		$thumbpath_m =   '../images/jartists/med';
		JartistsHelper::createFolder($thumbpath_m);
		$thumbpath_l =   '../images/jartists/lg';
		JartistsHelper::createFolder($thumbpath_l);
		$thumbpath_t =   '../images/jartists/thumbs';
		JartistsHelper::createFolder($thumbpath_t);
	   
	   // *** 1) Initialize / load image
		$resizeObj = new resize($imageDirectory.DS.$filename);
		$resizeObj -> resizeImage(120, 90, 'crop');
		// *** 3) Save image
		$resizeObj -> saveImage($thumbpath_s.DS.$filename, 100);
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage(200, 200, 'crop');
		$resizeObj -> saveImage($thumbpath_t.DS.$filename, 100);
		
		$resizeObj -> resizeImage(400, 250);
		$resizeObj -> saveImage($thumbpath_m.DS.$filename, 100);
		
		if($oldImage != 0){
			JartistsHelper::deletePhotos($oldImage);
		}
	}
	function runThumbnail($oldImage=0, $filename, $imageDirectory)
	{
		$thumbpath_s =  '../images/jartists/s';
		JartistsHelper::createFolder($thumbpath_s);
		$thumbpath_m =   '../images/jartists/med';
		JartistsHelper::createFolder($thumbpath_m);
		$thumbpath_l =   '../images/jartists/lg';
		JartistsHelper::createFolder($thumbpath_l);
		$thumbpath_t =   '../images/jartists/thumbs';
		JartistsHelper::createFolder($thumbpath_t);
	   
	   // *** 1) Initialize / load image
		$resizeObj = new resize($thumbpath_l.DS.$filename);
		$resizeObj -> resizeImage(120, 90, 'crop');
		// *** 3) Save image
		$resizeObj -> saveImage($thumbpath_s.DS.$filename, 100);
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage(200, 200, 'crop');
		$resizeObj -> saveImage($thumbpath_t.DS.$filename, 100);
		
		$resizeObj -> resizeImage(400, 250);
		$resizeObj -> saveImage($thumbpath_m.DS.$filename, 100);
		if($oldImage != 0){
		JartistsHelper::deletePhotos($oldImage);
		}
	}
}
