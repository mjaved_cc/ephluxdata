<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');


JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
// Import CSS
$document = &JFactory::getDocument();
$document->addStyleSheet('components/com_jartists/assets/css/jartists.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(pressbutton) {
		var form = document.getElementById('adminForm');

		// do field validation
		if (form.install_package.value == ""){
			alert("<?php echo JText::_('COM_LBL_UPLOAD_PLEASE_UPLOAD_ZIP', true); ?>");
		} else {
			form.installtype.value = 'upload';
			form.submit();
		}
	}
}
</script>
<form enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_jartists&view=upload');?>" method="post" name="adminForm" id="adminForm">
	<div class="width-70 fltlft">
		<fieldset class="uploadform">
			<legend><?php echo JText::_('COM_BATCH_UPLOAD_PHOTOS'); ?></legend>
			<label for="install_package"><?php echo JText::_('GALLERY'); ?></label>
			<?php echo $this->getGalleries(); ?>
            <label for="install_package"><?php echo JText::_('COM_LBL_PHOTOS_GENERIC_TITLE'); ?></label>
			<input class="input_box" id="title" name="title" type="text" size="57" />
            <label for="install_package"><?php echo JText::_('COM_LBL_UPLOAD_PHOTOS'); ?></label>
			<input class="input_box" id="install_package" name="install_package" type="file" size="57" />
			<input class="button" type="button" value="<?php echo JText::_('COM_LBL_UPLOAD'); ?>" onclick="Joomla.submitbutton()" />
		</fieldset>
		<input type="hidden" name="task" value="upload.upload" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

