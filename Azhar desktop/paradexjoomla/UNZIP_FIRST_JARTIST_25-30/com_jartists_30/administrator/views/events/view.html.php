<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Jartists.
 */
class JartistsViewEvents extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		JartistsHelper::addSubmenu('events');
        
		$this->addToolbar();
        
        $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/jartists.php';

		$state	= $this->get('State');
		$canDo	= JartistsHelper::getActions($state->get('filter.category_id'));

		JToolBarHelper::title(JText::_('COM_JARTISTS_TITLE_EVENTS'), 'events.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/event';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
			    JToolBarHelper::addNew('event.add','JTOOLBAR_NEW');
		    }

		    if ($canDo->get('core.edit') && isset($this->items[0])) {
			    JToolBarHelper::editList('event.edit','JTOOLBAR_EDIT');
		    }

        }

		if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
			    JToolBarHelper::divider();
			    JToolBarHelper::custom('events.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
			    JToolBarHelper::custom('events.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'events.delete','JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
			    JToolBarHelper::divider();
			    JToolBarHelper::archiveList('events.archive','JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
            	JToolBarHelper::custom('events.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
		}
        
        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
		    if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			    JToolBarHelper::deleteList('', 'events.delete','JTOOLBAR_EMPTY_TRASH');
			    JToolBarHelper::divider();
		    } else if ($canDo->get('core.edit.state')) {
			    JToolBarHelper::trash('events.trash','JTOOLBAR_TRASH');
			    JToolBarHelper::divider();
		    }
        }

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_jartists');
		}
        
        //Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_jartists&view=events');
        
        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);

        
	}
    
	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.featured' => JText::_('COM_JARTISTS_EVENTS_FEATURED'),
		'a.artist_id' => JText::_('COM_JARTISTS_EVENTS_ARTIST_ID'),
		'a.alpha' => JText::_('COM_JARTISTS_EVENTS_ALPHA'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		'a.checked_out' => JText::_('COM_JARTISTS_EVENTS_CHECKED_OUT'),
		'a.checked_out_time' => JText::_('COM_JARTISTS_EVENTS_CHECKED_OUT_TIME'),
		'a.event' => JText::_('COM_JARTISTS_EVENTS_EVENT'),
		'a.alias' => JText::_('COM_JARTISTS_EVENTS_ALIAS'),
		'a.venue' => JText::_('COM_JARTISTS_EVENTS_VENUE'),
		'a.location' => JText::_('COM_JARTISTS_EVENTS_LOCATION'),
		'a.tickets' => JText::_('COM_JARTISTS_EVENTS_TICKETS'),
		'a.poster_front' => JText::_('COM_JARTISTS_EVENTS_POSTER_FRONT'),
		'a.poster_back' => JText::_('COM_JARTISTS_EVENTS_POSTER_BACK'),
		'a.hits' => JText::_('COM_JARTISTS_EVENTS_HITS'),
		'a.date' => JText::_('COM_JARTISTS_EVENTS_DATE'),
		'a.created' => JText::_('COM_JARTISTS_EVENTS_CREATED'),
		'a.params' => JText::_('COM_JARTISTS_EVENTS_PARAMS'),
		'a.created_by' => JText::_('COM_JARTISTS_EVENTS_CREATED_BY'),
		);
	}

    
}
