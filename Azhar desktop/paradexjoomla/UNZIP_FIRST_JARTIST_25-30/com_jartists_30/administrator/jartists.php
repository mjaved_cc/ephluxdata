<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */


// no direct access
defined('_JEXEC') or die;
define("DS","/");
require_once(JPATH_ROOT.'/components/com_jartists/libraries/libraries.php');
require_once(dirname(__FILE__) . '/helpers/jartists.php');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_jartists')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JControllerLegacy::getInstance('Jartists');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
