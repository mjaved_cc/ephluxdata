<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */

 

// No direct access

defined('_JEXEC') or die;

require_once JPATH_ROOT.'/components/com_jartists/libraries/defines.php';

require_once JPATH_ROOT.'/components/com_jartists/libraries/audio.lib.php';

require_once JPATH_ROOT.'/components/com_jartists/libraries/form.lib.php';

require_once JPATH_ROOT.'/components/com_jartists/libraries/image.lib.php';