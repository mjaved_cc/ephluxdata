<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */

 

// No direct access

defined('_JEXEC') or die;



abstract class  LblmVideo

{





	function video($url) {

	$video = array();
    $image_url = JString::parse_url($url);
    if(@$image_url['host'] == 'www.youtube.com' || @$image_url['host'] == 'youtube.com'){
		 $url_string = parse_url($url, PHP_URL_QUERY);
  		parse_str($url_string, $args);
        $array = explode("&", $image_url['query']);
        $video['video_type'] = 'youtube';
		$video['video_id'] = $args['v'];
		$video['thumb'] = "http://img.youtube.com/vi/".$args['v']."/0.jpg";
		$video['url'] 	= "http://www.youtube.com/watch?v=".$args['v'];
    } else if(@$image_url['host'] == 'www.vimeo.com' || @$image_url['host'] == 'vimeo.com'){
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
		$video['video_type'] = 'vimeo';
		$video['video_id'] = substr($image_url['path'], 1);
		$video['thumb'] =  $hash[0]["thumbnail_small"];
		$video['url'] 	= '';
    }
	
	return $video;



}





}