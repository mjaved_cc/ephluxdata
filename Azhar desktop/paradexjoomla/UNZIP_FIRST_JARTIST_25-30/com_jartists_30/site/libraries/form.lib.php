<?php
/**
 * @version     0.8.2
 * @package     com_joomhrm
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4ucode <info@b4ucode.com> - http://www.b4ucode.com
 */
// No direct access
defined('_JEXEC') or die;
 abstract class LblForm
{
	public static function artist2($name,$value,$attribs = null)
	{
		if(empty($attribs)){
			$attribs = 'class="inputbox"';
			}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		 ->from('#__jartists_artists')
		 ->order('title ASC');
		$db->setQuery($query);
		
		$rows = $db->loadObjectlist();
			
		 $options 	= array();
   		$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_LBL_SELECT_ARTISTS' ) );
    		foreach ( $rows as $row ) {
    			$options[] = JHTML::_('select.option',  $row->id, $row->title );
    		}
			return JHTML::_('select.genericlist', $options, $name, array(
    		        'option.text.toHtml' => false ,
    		        'list.attr' => $attribs,
    				'option.text' => 'text' ,    
    				'option.key' => 'value',
    				'list.select' => $value, 		    		    		    		    
    		    )
			); 
	}
	
	public static function artist($name='',$value='',$attribs = null)
	{
		if(empty($attribs)){
			$attribs = 'class="inputbox"';
			}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		 ->from('#__jartists_artists')
		 ->order('title ASC');
		$db->setQuery($query);
		
		$rows = $db->loadObjectlist();
			
		 $options 	= array();
   		//$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_JARTISTS_SELECT_ARTISTS' ) );
    		foreach ( $rows as $row ) {
    			$options[] = JHTML::_('select.option',  $row->id, $row->title );
    		}
			return $options;
			/*return JHTML::_('select.genericlist', $options, $name, array(
    		        'option.text.toHtml' => false ,
    		        'list.attr' => $attribs,
    				'option.text' => 'text' ,    
    				'option.key' => 'value',
    				'list.select' => $value, 		    		    		    		    
    		    )
			); */
	}
public static function labels($name='',$value='',$attribs = null)
	{
		if(empty($attribs)){
			$attribs = 'class="inputbox"';
			}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		 ->from('#__jartists_labels')
		 ->order('title ASC');
		$db->setQuery($query);
		
		$rows = $db->loadObjectlist();
			
		 $options 	= array();
   		//$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_JARTISTS_SELECT_ARTISTS' ) );
    		foreach ( $rows as $row ) {
    			$options[] = JHTML::_('select.option',  $row->id, $row->title );
    		}
			return $options;
			/*return JHTML::_('select.genericlist', $options, $name, array(
    		        'option.text.toHtml' => false ,
    		        'list.attr' => $attribs,
    				'option.text' => 'text' ,    
    				'option.key' => 'value',
    				'list.select' => $value, 		    		    		    		    
    		    )
			); */
	}
	public static function albums($name,$value,$attribs = null)
	{
		if(empty($attribs)){
			$attribs = 'class="inputbox"';
			}
			
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		 ->from('#__jartists_albums')
		 ->order('title ASC');
		$db->setQuery($query);
		$rows = $db->loadObjectlist();
		 $options 	= array();
   		$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_LBL_SELECT_ALBUM' ) );
    		foreach ( $rows as $row ) {
    			$options[] = JHTML::_('select.option',  $row->id, $row->title );
    		}
			return JHTML::_('select.genericlist', $options, $name, array(
    		        'option.text.toHtml' => false ,
    		        'list.attr' => $attribs,
    				'option.text' => 'text' ,    
    				'option.key' => 'value',
    				'list.select' => $value, 		    		    		    		    
    		    )
			); 
	}
}