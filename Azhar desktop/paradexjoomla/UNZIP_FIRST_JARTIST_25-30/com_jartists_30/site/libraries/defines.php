<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */

 

// No direct access

defined('_JEXEC') or die;



define('LBLM_SITE',		 	JPATH_ROOT.'/components/com_jartists');

define('LBLM_ADMIN',	 	JPATH_ADMINISTRATOR.'/components/com_jartists');

define('LBLM_LIBRARIES', 	LBLM_SITE . '/libraries');

define('LBLM_ASSETS', 	JURI::root(). 'components/com_jartists/assets/');

define('LBLM_IMAGES', 	'components/com_jartists/images//');
define('LBLM_IMAGE_LG', JURI::root(). 'images/jartists/lg/');
define('LBLM_IMAGE_S', 	JURI::root(). 'images/jartists/s/');
define('LBLM_IMAGE_T', 	JURI::root(). 'images/jartists/thumbs/');
define('LBLM_IMAGE_M', 	JURI::root(). 'images/jartists/med/');
define('LBLM_ICONS', 	JURI::base(). 'components/com_jartists/assets/icons/');