<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * @param	array	A named array
 * @return	array
 */
function JartistsBuildRoute(&$query)
{
	$segments = array();

       if(isset($query['view']))

       {

                $segments[] = $query['view'];

                unset( $query['view'] );

       }

       if(isset($query['id']))
       {

		$segments[] = $query['id'];
		unset( $query['id'] );

       }

     if(isset($query['layout']))
     {
		$segments[] = $query['layout'];
		unset( $query['layout'] );
     }
    if(isset($query['label_id']))
     {
		$segments[] = $query['label_id'];
		unset( $query['label_id'] );
     }
	if (isset($query['task'])) {
		$segments[] = implode('/',explode('.',$query['task']));
		unset($query['task']);
	}

	return $segments;
}

/**
 * @param	array	A named array
 * @param	array
 *
 * Formats:
 *
 * index.php?/jartists/task/id/Itemid
 *
 * index.php?/jartists/id/Itemid
 */
function JartistsParseRoute($segments)
{
    
	 $vars = array();
	 $count = count( $segments );
	if($segments[0]=='artists'){
		$id = explode( ':', $segments[1] );
		$vars['label_id'] = (int) $id[0];
	}else{
		$id = explode( ':', $segments[1] );
		$vars['id'] = (int) $id[0];
	}
	$vars['view'] = $segments[0];

	

	if($count == 3) {
		$vars['layout'] = $segments[2];
	}
	
	if($count == 4) {
		$vars['task'] = $segments[4];
	}
	return $vars;
}
