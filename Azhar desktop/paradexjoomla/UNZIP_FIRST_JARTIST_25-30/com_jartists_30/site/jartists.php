<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);

// Include dependancies
jimport('joomla.application.component.controller');
$document = &JFactory::getDocument();

$document->addStyleSheet('components/com_jartists/assets/css/main.css');

require_once(JPATH_ROOT.'/components/com_jartists/libraries/libraries.php');
// Execute the task.
$controller	= JControllerLegacy::getInstance('Jartists');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
