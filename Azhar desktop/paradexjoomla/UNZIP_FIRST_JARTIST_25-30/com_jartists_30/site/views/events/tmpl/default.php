<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */


// no direct access
defined('_JEXEC') or die;
?>
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php if($this->items) : ?>
<table class="category">
	<thead>
    <tr>
    	 <th class="title" width="100">&nbsp;</th>
    	<th class="title" width="30%"><?php echo JText::_('COM_JARTISTS_HEAD_EVENT'); ?></th>
        <th class="title" width="100"><?php echo JText::_('COM_JARTISTS_HEAD_VENUE'); ?></th>
        <th class="title" width="100"><?php echo JText::_('COM_JARTISTS_HEAD_LOCATION'); ?></th>
        <th class="title" width="100"><?php echo JText::_('Artist'); ?></th>
    </tr>
    </thead>
	<tbody>         
    <?php foreach ($this->items as $item) :
		$day = date('D', strtotime($item->date));
		$month = date('M', strtotime($item->date));
		
	?>             
        <tr>
        <td align="center">
        <div class="date" style="margin-left:14px;">
			<p><?php echo $day; ?> <span><?php echo $month; ?></span></p>
		</div>
        </td>
        <td><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=event&id=' .$item->slug); ?>"><?php echo $item->event; ?></a></td>
        
        <td><?php echo $item->venue; ?></td>
        <td><?php echo $item->location; ?></td>
        <td align="center"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&id='.$item->artist_slug); ?>"><?php echo $item->artist; ?></a></td>
</tr>
    <?php endforeach; ?>
</tbody>
</table>
     <div class="pagination">
        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>