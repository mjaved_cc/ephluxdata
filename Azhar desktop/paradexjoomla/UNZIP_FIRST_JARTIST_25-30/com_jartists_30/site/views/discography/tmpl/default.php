<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>
<?php if( $this->item ) : ?>
<h2><?php echo $this->item->title; ?></h2>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
<?php if($this->item->cover):  ?>
<div class="d_image">
	<img src="<?php echo LBLM_IMAGE_M.$this->item->cover; ?>" class="jartists-450" />
</div>
<?php endif; ?>
<?php if($this->item->back_cover):  ?>
<div class="d_image">
    <img src="<?php echo LBLM_IMAGE_M.$this->item->back_cover; ?>" class="jartists-450" />
</div>
<?php endif; ?>
<?php if($this->item->artist || $this->item->date_release): ?>
<dl>
  <?php if($this->item->artist): ?>	
  <dt class="s_label"><?php JText::_('COM_JARTISTS_ARTIST'); ?></dt>
  <dd class="s_label"><?php echo $this->item->artist; ?></dd>
  <?php endif; ?>	
  <?php if($this->item->date_release && $this->item->date_release != '0000-00-00'): ?>	
  <dt class="s_releaseDate"><?php JText::_('COM_JARTISTS_RELEASED'); ?></dt>
  <dd class="s_releaseDate"><?php echo $this->item->date_release; ?></dd>
   <?php endif; ?>
</dl>
<?php endif; ?>
<div class="s_links">
	<?php if($this->item->itunes_link): ?>
	<a href="<?php echo $this->item->itunes_link; ?>" target="_blank"><?php echo JText::_('COM_JARTISTS_DISCOGRAPHY_ITUNES'); ?></a>
    <?php endif; ?>
    <?php if($this->item->amazon_link): ?>
	<a href="<?php echo $this->item->amazon_link; ?>" target="_blank"><?php echo JText::_('COM_JARTISTS_DISCOGRAPHY_AMAZON'); ?></a>
    <?php endif; ?>
</div>
<div class="clr"></div>
<div>
	<?php echo $this->item->description; ?>
</div>
<?php endif; ?>
 <div class="clr"></div>
<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
