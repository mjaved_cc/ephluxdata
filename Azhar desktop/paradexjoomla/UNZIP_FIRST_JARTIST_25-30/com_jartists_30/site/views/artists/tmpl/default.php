<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */





// no direct access

defined('_JEXEC') or die;

?>

<div class="lblArtist">

<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>

<?php if($this->items) : ?>

    <div class="items">

            <div class="items_list">

    <?php foreach ($this->items as $item) :?>

                

                <div class="lblBox artists"><a  href="<?php echo JRoute::_('index.php?option=com_jartists&view=artist&id=' .$item->slug ); ?>"><img src="<?php echo LBLM_IMAGE_S.$item->profile_picture; ?>" class="jartists-120"/></a>
                <span class="title"><a  href="<?php echo JRoute::_('index.php?option=com_jartists&view=artist&id=' .$item->slug ); ?>"><?php echo $item->title; ?></a></span>
                </div>



    <?php endforeach; ?>

            

            </div>

			<div class="clr"></div>

    </div>



     <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>

</div>