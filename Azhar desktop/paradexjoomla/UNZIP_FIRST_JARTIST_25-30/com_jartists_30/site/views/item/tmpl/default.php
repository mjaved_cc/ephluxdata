<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>

<div class="item-page">

<h1><?php echo $this->item->title; ?></h1>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
	<?php if($this->item->image_file): ?>
	<div class="image_file">		
        <img src="<?php echo LBLM_IMAGE_M.$this->item->image_file; ?>" alt="<?php echo $this->item->title; ?>" />
		
    </div>
	<?php endif; ?>
    <div class="image_desc">

    	<?php echo $this->item->content; ?>

    </div>
	 <div class="clr"></div>
	<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
</div>