<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>
<h1><?php echo $this->params->get('page_heading'); ?></h1>
<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
<?php 

	$video = LblmVideo::video($this->item->video_url);

			if($video['video_type']=='youtube'){

				$this->item->thumb =  'http://i.ytimg.com/vi/'.$video['video_id'].'/0.jpg';

				$this->item->vid =  	$video['video_id'];

			}

?>

<div class="item-page">

<h2><?php echo $this->item->title; ?></h2>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
	<div class="image_file">
	<?php if($video['video_type']=='youtube'){ ?>
       <iframe width="560" height="315" src="http://www.youtube.com/embed/<?php echo $video['video_id']; ?>" frameborder="0" allowfullscreen></iframe>
	<?php }else{ ?>
    	<iframe src="http://player.vimeo.com/video/<?php echo $video['video_id']; ?>" width="560" height="315" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
    <?php } ?>
    </div>

    <div class="image_meta">

    	<dl class="article-info">

        <dt class="article-info-term"><?php  echo JText::_('Details'); ?></dt>

        <dd class="createdby">Name: <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&id='.$this->item->artist_slug); ?>"><?php echo $this->item->artist; ?></a></dd>

        <dd class="hits">Hits <?php echo $this->item->hits; ?></dd>

        </dl>

    </div>

    <div class="image_desc">

    	<?php echo $this->item->description; ?>

    </div>
    <div class="clr"></div>
	<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
</div>