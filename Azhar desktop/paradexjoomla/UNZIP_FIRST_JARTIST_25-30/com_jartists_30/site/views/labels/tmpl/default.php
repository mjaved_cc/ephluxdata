<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */


// no direct access
defined('_JEXEC') or die;
?>
<div class="lblArtist">
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>

<?php if($this->items) : ?>



    <div class="items">              
	<div class="items_list">
    <?php foreach ($this->items as $item) :?>

    
	<div class="lblBox artists">
    <?php if($item->logo): ?>
		
                <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' . $item->slug); ?>"><img alt="<?php echo $item->title; ?>" src="<?php echo LBLM_IMAGE_S.$item->logo; ?>"></a>
               
        <?php endif; ?>
		 <span class="title">
         <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' . $item->slug); ?>">
		 	<?php echo $item->title; ?></a>
         </span>
            
        <div class="description">
            <?php if($item->description): ?>
            <p>
                <?php echo substr($item->description,0,200); ?>
		    </p>
       <?php endif; ?>
      <div class="links">
      <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' .$item->slug); ?>">
      View <?php echo $item->title; ?></a>
      </div>
    </div>
	</div>
    <?php endforeach; ?>
	<div class="clr"></div>
    </div>
    </div>



     <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>
</div>