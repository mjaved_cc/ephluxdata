<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>
<?php if( $this->item ) : ?>

    <div class="item_fields">
        
        <ul class="fields_list">

			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_ID'); ?>:
			<?php echo $this->item->id; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_ALIAS'); ?>:
			<?php echo $this->item->alias; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_STATE'); ?>:
			<?php echo $this->item->state; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_LOGO'); ?>:
			<?php echo $this->item->logo; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_CHECKED_OUT'); ?>:
			<?php echo $this->item->checked_out; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_CHECKED_OUT_TIME'); ?>:
			<?php echo $this->item->checked_out_time; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_GENRE'); ?>:
			<?php echo $this->item->genre; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_PARAMS'); ?>:
			<?php echo $this->item->params; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_ORDERING'); ?>:
			<?php echo $this->item->ordering; ?></li>
			<li><?php echo JText::_('COM_JARTISTS_FORM_LBL_GENRE_CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>


        </ul>
        
    </div>
    <?php if(JFactory::getUser()->authorise('core.edit.own', 'com_jartists')): ?>
		<a href="<?php echo JRoute::_('index.php?option=com_jartists&task=genre.edit&id='.$this->item->id); ?>">Edit</a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_jartists')):
								?>
									<a href="javascript:document.getElementById('form-genre-delete-<?php echo $this->item->id ?>').submit()">Delete</a>
									<form id="form-genre-delete-<?php echo $this->item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_jartists&task=genre.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
										<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
										<input type="hidden" name="jform[alias]" value="<?php echo $this->item->alias; ?>" />
										<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
										<input type="hidden" name="jform[logo]" value="<?php echo $this->item->logo; ?>" />
										<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
										<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
										<input type="hidden" name="jform[genre]" value="<?php echo $this->item->genre; ?>" />
										<input type="hidden" name="jform[params]" value="<?php echo $this->item->params; ?>" />
										<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
										<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
										<input type="hidden" name="option" value="com_jartists" />
										<input type="hidden" name="task" value="genre.remove" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								<?php
								endif;
							?>
<?php else: ?>
    Could not load the item
<?php endif; ?>
