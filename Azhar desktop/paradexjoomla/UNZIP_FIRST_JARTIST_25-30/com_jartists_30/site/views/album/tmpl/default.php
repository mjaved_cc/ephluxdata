<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>
<h1><?php echo $this->items[0]->gallery; ?></h1>
<?php if($this->items) : ?>



    <div class="image_items">



            <ul class="items_list">

                

    <?php foreach ($this->items as $item) :?>

                
                <div class="disc_item"><a  href="<?php echo JRoute::_('index.php?option=com_jartists&view=photo&id=' .$item->slug ); ?>"><img src="<?php echo LBLM_IMAGE_T.$item->image_file; ?>" class="jartists-150" /></a>

                <span class="title"><?php echo $item->title; ?></span>

			</div>
    <?php endforeach; ?>

            
			<div class="clr"></div>
            </ul>



    </div>



     <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>