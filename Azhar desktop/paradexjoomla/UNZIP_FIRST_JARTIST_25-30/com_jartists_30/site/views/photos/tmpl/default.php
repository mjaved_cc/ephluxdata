<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */


// no direct access
defined('_JEXEC') or die;
?>

<div class="items">
    <ul class="items_list">
        <?php $show = false; ?>
        <?php foreach ($this->items as $item) :?>

                
				<?php
					if($item->state == 1 || ($item->state == 0 && JFactory::getUser()->authorise('core.edit.own',' com_jartists'))):
						$show = true;
						?>
							<li>
								<a href="<?php echo JRoute::_('index.php?option=com_jartists&view=photo&id=' . (int)$item->id); ?>"><?php echo $item->alpha; ?></a>
								<?php
									if(JFactory::getUser()->authorise('core.edit.state','com_jartists')):
									?>
										<a href="javascript:document.getElementById('form-photo-state-<?php echo $item->id; ?>').submit()"><?php if($item->state == 1):?>Unpublish<?php else:?>Publish<?php endif; ?></a>
										<form id="form-photo-state-<?php echo $item->id ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_jartists&task=photo.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[featured]" value="<?php echo $item->featured; ?>" />
											<input type="hidden" name="jform[alpha]" value="<?php echo $item->alpha; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo (int)!((int)$item->state); ?>" />
											<input type="hidden" name="jform[checked_out]" value="<?php echo $item->checked_out; ?>" />
											<input type="hidden" name="jform[checked_out_time]" value="<?php echo $item->checked_out_time; ?>" />
											<input type="hidden" name="jform[title]" value="<?php echo $item->title; ?>" />
											<input type="hidden" name="jform[alias]" value="<?php echo $item->alias; ?>" />
											<input type="hidden" name="jform[image_file]" value="<?php echo $item->image_file; ?>" />
											<input type="hidden" name="jform[description]" value="<?php echo $item->description; ?>" />
											<input type="hidden" name="jform[album_id]" value="<?php echo $item->album_id; ?>" />
											<input type="hidden" name="jform[artist_id]" value="<?php echo $item->artist_id; ?>" />
											<input type="hidden" name="jform[hits]" value="<?php echo $item->hits; ?>" />
											<input type="hidden" name="jform[params]" value="<?php echo $item->params; ?>" />
											<input type="hidden" name="option" value="com_jartists" />
											<input type="hidden" name="task" value="photo.save" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
									if(JFactory::getUser()->authorise('core.delete','com_jartists')):
									?>
										<a href="javascript:document.getElementById('form-photo-delete-<?php echo $item->id; ?>').submit()">Delete</a>
										<form id="form-photo-delete-<?php echo $item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_jartists&task=photo.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[featured]" value="<?php echo $item->featured; ?>" />
											<input type="hidden" name="jform[alpha]" value="<?php echo $item->alpha; ?>" />
											<input type="hidden" name="jform[ordering]" value="<?php echo $item->ordering; ?>" />
											<input type="hidden" name="jform[state]" value="<?php echo $item->state; ?>" />
											<input type="hidden" name="jform[checked_out]" value="<?php echo $item->checked_out; ?>" />
											<input type="hidden" name="jform[checked_out_time]" value="<?php echo $item->checked_out_time; ?>" />
											<input type="hidden" name="jform[title]" value="<?php echo $item->title; ?>" />
											<input type="hidden" name="jform[alias]" value="<?php echo $item->alias; ?>" />
											<input type="hidden" name="jform[image_file]" value="<?php echo $item->image_file; ?>" />
											<input type="hidden" name="jform[description]" value="<?php echo $item->description; ?>" />
											<input type="hidden" name="jform[album_id]" value="<?php echo $item->album_id; ?>" />
											<input type="hidden" name="jform[artist_id]" value="<?php echo $item->artist_id; ?>" />
											<input type="hidden" name="jform[hits]" value="<?php echo $item->hits; ?>" />
											<input type="hidden" name="jform[params]" value="<?php echo $item->params; ?>" />
											<input type="hidden" name="jform[created_by]" value="<?php echo $item->created_by; ?>" />
											<input type="hidden" name="option" value="com_jartists" />
											<input type="hidden" name="task" value="photo.remove" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
								?>
							</li>
						<?php endif; ?>

        <?php endforeach; ?>
        <?php if(!$show): ?>
            There are no items in the list
        <?php endif; ?>
    </ul>
</div>
<?php if($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>


									<?php if(JFactory::getUser()->authorise('core.create','com_jartists')): ?><a href="<?php echo JRoute::_('index.php?option=com_jartists&task=photo.edit&id=0'); ?>">Add</a>
	<?php endif; ?>