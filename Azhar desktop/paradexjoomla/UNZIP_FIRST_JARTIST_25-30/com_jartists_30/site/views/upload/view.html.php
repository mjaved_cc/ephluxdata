<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class JartistsViewUpload extends JViewLegacy
{

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		//$this->addToolbar();
		parent::display($tpl);
	}
	function getGalleries()
	{
		if(empty($attribs)){
			$attribs = 'class="inputbox"';
			}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		 ->from('#__jartists_albums')
		 ->order('title ASC');
		$db->setQuery($query);
		
		$rows = $db->loadObjectlist();
			
		 $options 	= array();
   		$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_JARTISTS_SELECT_ALBUM' ) );
    		foreach ( $rows as $row ) {
    			$options[] = JHTML::_('select.option',  $row->id, $row->title );
    		}
			return JHTML::_('select.genericlist', $options, 'gallery_id', array(
    		        'option.text.toHtml' => false ,
    		        'list.attr' => $attribs,
    				'option.text' => 'text' ,    
    				'option.key' => 'value',
    				'list.select' => 0, 		    		    		    		    
    		    )
			); 
	}
	/**
	 * Add the page title and toolbar.
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.DS.'helpers'.DS.'jartists.php';
		JToolBarHelper::title(JText::_('COM_BATCH_UPLOAD_PHOTOS'), 'bacth-upload.png');
	}
}
