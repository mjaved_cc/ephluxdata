<?php

/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class JartistsViewArtist extends JViewLegacy {

    protected $state;
    protected $item;
    protected $form;
    protected $params;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        
		$app	= JFactory::getApplication();
        $user		= JFactory::getUser();
        
        $this->state = $this->get('State');
        $this->item = $this->get('Item');
        $this->params = $app->getParams('com_jartists');
   		$this->form		= $this->get('Form');$this->form		= $this->get('Form');$this->form		= $this->get('Form');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }
        
		$this->photos = $this->getPhotos($this->item);

		$this->releases = $this->getReleases($this->item);

		$this->events = $this->getEvents($this->item);

		$this->press = $this->getPress($this->item);

		$this->videos = $this->getVideos($this->item);

		$this->links = $this->getLinks($this->item);

        

		$links_model =  JModelLegacy::getInstance('links', 'JartistsModel'); 

		$this->link->items = 	$links_model->getItems();

		$this->link->state		= $links_model->getState();

		$this->link->pagination	= $links_model->getPagination();



		$videos_model = & JModelLegacy::getInstance('videos', 'JartistsModel'); 

		$this->video->items = 	$videos_model->getItems();

		$this->video->state		= $videos_model->getState();

		$this->video->pagination	= $videos_model->getPagination();

		
		//$photos_model = & JModel::getInstance('gallery', 'JartistsModel'); 
		// Get an instance of the generic articles model
		$photos_model = JModelLegacy::getInstance('photos', 'JartistsModel');
		$photos_model->setState('artist.id', $this->item->id);

		$this->photo->state		= $photos_model->getState();
		$this->photo->items = 	$photos_model->getItems();

		

		$this->photo->pagination	= $photos_model->getPagination();
		

		$events_model = & JModelLegacy::getInstance('events', 'JartistsModel'); 

		$this->event->items 	= 	$events_model->getItems();

		$this->event->state		= 	$events_model->getState();

		$this->event->pagination= $events_model->getPagination();

		

		

		$press_model = & JModelLegacy::getInstance('news', 'JartistsModel'); 

		$this->news->items 	= 	$press_model->getItems();

		$this->news->state		= 	$press_model->getState();

		$this->news->pagination= $press_model->getPagination();

		

		

		$disc_model = & JModelLegacy::getInstance('discographies', 'JartistsModel'); 

		$this->realeases->items 	= 	$disc_model->getItems();

		$this->realeases->state		= 	$disc_model->getState();

		$this->realeases->pagination= $disc_model->getPagination();

		$this->item->biography = JHTML::_('content.prepare', $this->item->biography );
        
        
        if($this->_layout == 'edit') {
            
            $authorised = $user->authorise('core.create', 'com_jartists');

            if ($authorised !== true) {
                throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }
        
        $this->_prepareDocument();

        parent::display($tpl);
    }

	function pageMenu()
	{
		$html='';
		$html.='<div class="profile_nav">
            	<ul>';
                	$html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_HOME_PAGE_TITLE').'</a></li>';

                    $html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=biography&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_BIOGRAPHY_PAGE_TITLE').'</a></li>';
					if($this->photo->items):	                    
					$html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=photos&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_PHOTOS_PAGE_TITLE').'</a></li>';
					endif;
					if($this->realeases->items):	
                    $html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=discography&id='.$this->item->slug).'">Discography</a></li>';
					endif;
					if($this->video->items):	
                    $html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=videos&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_VIDEOS_PAGE_TITLE').'</a></li>';
					endif;
					if($this->news->items):	
                    $html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=press&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_PRESS_PAGE_TITLE').'</a></li>';
					endif;
					if($this->event->items):					
                    $html.='<li><a href="'.JROUTE::_('index.php?option=com_jartists&view=artist&layout=events&id='.$this->item->slug).'">'.JText::_('COM_JARTISTS_DEFAULT_EVENTS_PAGE_TITLE').'</a></li>';
					endif;
                $html.='</ul>
			<div class="clr"></div>
            </div>';
			return $html;
	}
	

	function getPhotos($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_photos` AS a');

		$query->where('g.artist_id = '.(int) $data->id);

		$query->join('LEFT','`#__jartists_albums` AS g ON a.album_id=g.id');

		$db->setQuery($query,0,4);

		$rows = $db->loadObjectList();

		return $rows;

	}



	function getEvents($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_tour` AS a');

		$query->where('a.artist_id = '.(int) $data->id);

		$db->setQuery($query,0,3);
		$rows = $db->loadObjectList();

		return $rows;

	}



	function getReleases($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_discography` AS a');

		$query->where('a.artist_id = '.(int) $data->id);

		$db->setQuery($query,0,4);

		$rows = $db->loadObjectList();

		return $rows;

	}

	

	function getPress($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_news` AS a');

		$query->where('a.artist_id = '.(int) $data->id);

		$db->setQuery($query,0,3);

		$rows = $db->loadObjectList();

		return $rows;

	}

	function getVideos($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_videos` AS a');

		$query->where('a.artist_id = '.(int) $data->id);

		$db->setQuery($query,0,4);

		$rows = $db->loadObjectList();

		return $rows;

	}

	

	function getLinks($data)

	{

		$db = JFactory::getDBO(); 

        $query = $db->getQuery(true);

        // Select the required fields from the table.

        $query->select('a.*,CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug');

        $query->from('`#__jartists_links` AS a');

		$query->where('a.artist_id = '.(int) $data->id);

		$db->setQuery($query,0,20);

		$rows = $db->loadObjectList();
		return $rows;

	}
	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$menus	= $app->getMenu();
		$title	= null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', JText::_('COM_JARTISTS_DEFAULT_ARTISTS_PAGE_TITLE'));
		}
		$title = $this->params->get('page_title', '');
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}        
    
}
