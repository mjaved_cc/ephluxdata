<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?><h1><?php echo JText::_('COM_JARTISTS_DEFAULT_EVENTS_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

            <table class="category">
	<thead>
    <tr>
     	<th class="title" width="15%"><?php echo JText::_('COM_JARTISTS_HEAD_DATE'); ?></th>
    	<th class="title" width="60%"><?php echo JText::_('COM_JARTISTS_HEAD_EVENT'); ?></th>
        <th class="title" width="15%"><?php echo JText::_('COM_JARTISTS_HEAD_VENUE'); ?></th>
        <th class="title" width="15%"><?php echo JText::_('COM_JARTISTS_HEAD_LOCATION'); ?></th>
    </tr>
    </thead>
	<tbody>         
    <?php foreach ($this->event->items as $item) :
	$day = date('D', strtotime($item->date));
	$month = date('M', strtotime($item->date));
	?>             
                <tr>
                <td align="center"> <div class="date" align="center">
			<p><?php echo $day; ?> <span><?php echo $month; ?></span></p>
		</div>
        </td>
                <td><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=event&id=' .$item->slug); ?>"><?php echo $item->event; ?></a></td>
        <td><?php echo $item->venue; ?></td>
        <td><?php echo $item->location; ?></td>
</tr>
    <?php endforeach; ?>
</tbody>
</table>
 <div class="pagination">
        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->event->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->event->pagination->getPagesLinks(); ?>

    </div>