<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;
?>

<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_PHOTOS_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

<div class="image_items">



            <ul class="items_list">

                

    <?php foreach ($this->photo->items as $item) :?>

                
                <div class="disc_item"><a  href="<?php echo JRoute::_('index.php?option=com_jartists&view=photo&id=' .$item->slug ); ?>"><img src="<?php echo LBLM_IMAGE_T.$item->image_file; ?>" class="jartists-150" /></a>
				<?php if($item->gallery): ?>
                <span class="title"><?php echo $item->gallery; ?></span>
				<?php endif; ?>
			</div>
    <?php endforeach; ?>

            

            </ul>
	<div class="clr"></div>


    </div>
<div class="pagination">

    <p class="counter">

        <?php echo $this->photo->pagination->getPagesCounter(); ?>

    </p>

    <?php echo $this->photo->pagination->getPagesLinks(); ?>

</div>