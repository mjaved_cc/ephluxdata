<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>


<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_NEWS_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

            <div class="press_list">

		<?php foreach($this->news->items as $item): ?>

           <div class="articleBlock">

			   <h2><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=item&id='.$item->slug); ?>"><?php echo $item->title; ?></a></h2>

               <div class="articleContent"><?php echo substr(strip_tags($item->content),0,300); ?></div>

               <div class="readmore"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=item&id='.$item->slug); ?>">Read More</a></div>
		   </div>

        <?php endforeach; ?>
         <div class="clr"></div>
        </div>

<div class="pagination">

    <p class="counter">

        <?php echo $this->news->pagination->getPagesCounter(); ?>

    </p>

    <?php echo $this->news->pagination->getPagesLinks(); ?>

</div>