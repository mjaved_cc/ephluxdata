<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_VIDEOS_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

            <div class="video_list">

            	<?php foreach($this->video->items as $item):

				$video = LblmVideo::video($item->video_url);

			if($video['video_type']=='youtube'){

				$item->thumb =  'http://i.ytimg.com/vi/'.$video['video_id'].'/0.jpg';

				$item->vid =  	$video['video_id'];

			}

				 ?>

                    <div class="video_item"><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=video&id=' . $item->slug); ?>"><img src="<?php echo $item->thumb; ?>" width="200" height="150" border="0" /></a><span class="title" style="display:block; font-weight:bold;"><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=video&id=' . $item->slug); ?>"><?php echo substr($item->title,0,20); ?></a></span>

                    </div>

                <?php endforeach; ?>
			<div class="clr"></div>
            </div>

<div class="pagination">

    <p class="counter">

        <?php echo $this->video->pagination->getPagesCounter(); ?>

    </p>

    <?php echo $this->video->pagination->getPagesLinks(); ?>

</div>