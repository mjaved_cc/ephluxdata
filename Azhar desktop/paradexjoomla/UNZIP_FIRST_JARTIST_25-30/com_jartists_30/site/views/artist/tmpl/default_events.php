<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->events): ?>

	<div class="display_row">

        <table cellpadding="0" cellspacing="2" border="0">

        <?php foreach($this->events as $item):
		$day = date('D', strtotime($item->date));
		$month = date('M', strtotime($item->date));
		 ?>

            <tr>
					<td><div class="date" style="margin-right:14px;">
			<p><?php echo $day; ?> <span><?php echo $month; ?></span></p>
		</div></td>
                 <td width="190"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=event&id='.$this->item->slug); ?>"><?php echo $item->event; ?></td>
		</tr>

        <?php endforeach; ?>

        </table>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=events&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>

    </div>

<?php endif; ?>