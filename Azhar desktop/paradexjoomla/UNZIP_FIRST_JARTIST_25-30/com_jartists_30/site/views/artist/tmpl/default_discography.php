<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->releases): ?>

	<div class="display_row">

        <ul>

        <?php foreach($this->releases as $release): ?>

            <li>

                 <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$release->slug); ?>"><img src="<?php echo LBLM_IMAGE_T.$release->cover; ?>" class="jartists-100" /></a>

                 <span class="title"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$release->slug); ?>"><?php echo $release->title; ?></a></span>

            </li>

        <?php endforeach; ?>	

        <div class="clr"></div>

        </ul>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=discography&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>

    </div>

<?php endif; ?>