<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// no direct access
defined('_JEXEC') or die;
?>
<div class="item-page">
<h2><?php echo $this->item->title; ?></h2>
<div><?php echo $this->pageMenu(); ?></div>
	<?php if($this->item->profile_picture || $this->item->biography): ?>
	<div class="topArtistBar">
		<?php if($this->item->profile_picture): ?>
        <div class="main_pic">
			<img src="<?php echo LBLM_IMAGE_T.$this->item->profile_picture; ?>" class="artist-thumb" />
        </div>
        <?php endif; ?>
		<?php if($this->item->biography): ?>
        <div class="image_file">

           <div class="biography">

            <h3>Biography</h3>

            <?php echo $this->loadTemplate('biography'); ?>

            <div class="clr"></div>

        </div>

        </div>
		<?php endif; ?>
         <div class="clr"></div>

    </div>
	<?php endif; ?>
      <div>
	<?php if($this->videos): ?>
    <div class="videos agrid">

    	<h3>Videos</h3>

        <?php echo $this->loadTemplate('videos'); ?>

    </div>
	<?php endif; ?>
  
	<?php if($this->releases): ?>
    <div class="discography agrid">
    	<h3>Discography</h3>
        <?php echo $this->loadTemplate('discography'); ?>
    </div>
	<?php endif; ?>
    
	<?php if($this->photos): ?>
    <div class="photos agrid">
    	<h3>Photos</h3>
        <?php echo $this->loadTemplate('photos'); ?>
    </div>
	<?php endif; ?>
    
	<?php if($this->press): ?>
    <div class="press_releases agrid">
    	<h3>Press Releases</h3>
    	<?php echo $this->loadTemplate('press'); ?>
    </div>
	<?php endif; ?>

    
	<?php if($this->events): ?>
    <div class="events agrid">
    	<h3>Events</h3>
    	<?php echo $this->loadTemplate('events'); ?>
    </div>
	<?php endif; ?>
    
	<?php if($this->links): ?>
    <div class="links agrid">
    	<h3>Links</h3>
    	<?php echo $this->loadTemplate('links'); ?>
    </div>
	<?php endif; ?>
    

    <div class="clr"></div>

    </div>

    <div class="clr"></div>

</div>