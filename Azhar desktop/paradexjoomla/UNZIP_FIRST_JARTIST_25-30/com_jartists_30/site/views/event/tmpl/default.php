<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_jartists', JPATH_ADMINISTRATOR);
?>
<h1><?php echo $this->item->event; ?></h1>
<?php echo $this->item->events->onJArtistsAfterTitle; ?>
<?php if($this->item->poster_front):  ?>
<div class="d_image">
	<div class="s_label"><?php JText::_('COM_JARTISTS_POSTER_FRONT'); ?></div>
    <img src="<?php echo LBLM_IMAGE_M.$this->item->poster_front; ?>" class="jartists-450" />
</div>
<?php endif; ?>
<?php if($this->item->poster_back):  ?>
<div class="d_image">
	<div class="s_label"><?php JText::_('COM_JARTISTS_POSTER_BACK'); ?></div>
     <img src="<?php echo LBLM_IMAGE_M.$this->item->poster_back; ?>" class="jartists-450" />
</div>
<?php endif; ?>
<dl>
  <?php if($this->item->date && $this->item->date != '0000-00-00'): ?>	
  <dt class="s_label"><?php JText::_('COM_JARTISTS_DATE'); ?></dt>
  <dd class="s_label"><?php echo $this->item->date; ?></dd>
  <?php endif; ?>	
  <?php if($this->item->venue): ?>	
  <dt class="s_label"><?php JText::_('COM_JARTISTS_VENUE'); ?></dt>
  <dd class="s_label"><?php echo $this->item->venue; ?></dd>
  <?php endif; ?>	
  <?php if($this->item->location): ?>	
  <dt class="s_label"><?php JText::_('COM_JARTISTS_LOCATION'); ?></dt>
  <dd class="s_label"><?php echo $this->item->location; ?></dd>
   <?php endif; ?>
   <?php if($this->item->tickets): ?>	
  <dt class="s_label"><?php JText::_('COM_JARTISTS_TICKETS'); ?></dt>
  <dd class="s_label"><?php echo $this->item->tickets; ?></dd>
   <?php endif; ?>
</dl>
	<div class="clr"></div>
<div>
	<?php echo $this->item->description; ?>
</div>
 <div class="clr"></div>
<?php echo $this->item->events->onJArtistsAfterDisplay; ?>