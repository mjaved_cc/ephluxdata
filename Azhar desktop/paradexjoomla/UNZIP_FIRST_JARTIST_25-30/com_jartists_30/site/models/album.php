<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Jartists model.
 */
class JartistsModelAlbum extends JModelList
{
    
    var $_item = null;
    
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_jartists');

		// Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit') {
            $id = JFactory::getApplication()->getUserState('com_jartists.edit.album.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_jartists.edit.album.id', $id);
        }
		$this->setState('album.id', $id);


		$artist_id = JRequest::getInt('artist_id');

		$this->setState('artist.id', $artist_id);
		
		// Load the parameters.
		$params = $app->getParams();
        $params_array = $params->toArray();
        if(isset($params_array['item_id'])){
            $this->setState('album.id', $params_array['item_id']);
        }
		$this->setState('params', $params);

	}
        
	 protected function getListQuery() {

		                 $db = $this->getDbo();

                        $query = $db->getQuery(true);

						$pk = $this->getState('gallery.id');

                        $query->select($this->getState(

                                'item.select', 'a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug'

                                )

                        );

						 $query->from('`#__jartists_photos` AS a');
					 	$query->select('ab.title AS gallery');

						$query->join('LEFT', '#__jartists_albums AS ab ON a.album_id=ab.id');
					

						if (is_numeric($pk)) {
							 $query->where('a.album_id = '. (int) $pk);
						}
						$artist_id = $this->getState('artist.id');
                       if (is_numeric($artist_id) && $artist_id != 0) {
							 $query->where('ab.artist_id = '. (int) $artist_id);
						}

						// Join over the users for the checked out user.

						$query->select('s.title AS artist');

						$query->join('LEFT', '#__jartists_artists AS s ON s.id=ab.artist_id');

                        // Filter by published state.

                        $published = $this->getState('filter.published');

                        $archived = $this->getState('filter.archived');



                        if (is_numeric($published)) {

                                $query->where('(a.state = ' . (int) $published . ' OR a.state =' . (int) $archived . ')');

                        }
		
                       return $query;

	}

	public function hit($pk = 0)
	{
            $hitcount = JRequest::getInt('hitcount', 1);

            if ($hitcount)
            {
                // Initialise variables.
                $pk = (!empty($pk)) ? $pk : (int) $this->getState('gallery.id');
                $db = $this->getDbo();

                $db->setQuery(
                        'UPDATE #__jartists_albums' .
                        ' SET hits = hits + 1' .
                        ' WHERE id = '.(int) $pk
                );

                if (!$db->query()) {
                        $this->setError($db->getErrorMsg());
                        return false;
                }
            }

            return true;
	}
	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id)) {
				$id = $this->getState('album.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}
    
	public function getTable($type = 'Album', $prefix = 'JartistsTable', $config = array())
	{   
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        return JTable::getInstance($type, $prefix, $config);
	}     

    
	
    
    function getCategoryName($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query 
            ->select('title')
            ->from('#__categories')
            ->where('id = ' . $id);
        $db->setQuery($query);
        return $db->loadObject();
    }
    
}