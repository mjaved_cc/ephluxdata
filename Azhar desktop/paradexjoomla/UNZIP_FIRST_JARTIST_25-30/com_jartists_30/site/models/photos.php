<?php

/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Jartists records.
 */
class JartistsModelPhotos extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since	1.6
     */
    protected function populateState($ordering = null, $direction = null) {
        
        // Initialise variables.
        $app = JFactory::getApplication();
		// Load state from the request.

		$pk = JRequest::getInt('id');
		$this->setState('gallery.id', $pk);


		$artist_id = JRequest::getInt('artist_id');

		$this->setState('artist.id', $artist_id);
        // List state information
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = JFactory::getApplication()->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);
        
        
		if(empty($ordering)) {
			$ordering = 'a.ordering';
		}
        
        // List state information.
        parent::populateState($ordering, $direction);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);
		$pk = $this->getState('gallery.id');
        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug'
                )
        );
        
		 $query->from('`#__jartists_photos` AS a');
					 	$query->select('ab.title AS gallery');

						$query->join('LEFT', '#__jartists_albums AS ab ON a.album_id=ab.id');
					

						if (is_numeric($pk)) {
							 $query->where('a.album_id = '. (int) $pk);
						}
	$artist_id = $this->getState('artist.id');
                       if (is_numeric($artist_id) && $artist_id != 0) {
							 $query->where('ab.artist_id = '. (int) $artist_id);
						}

						// Join over the users for the checked out user.

						$query->select('s.title AS artist');

						$query->join('LEFT', '#__jartists_artists AS s ON s.id=ab.artist_id');					
    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');
    
		// Join over the created by field 'created_by'
		$query->select('created_by.name AS created_by');
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');


		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
                
			}
		}
        
        
        
        return $query;
    }

}
