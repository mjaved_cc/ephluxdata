<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://www.b4ucode.com
 */
 
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class JartistsController extends JControllerLegacy
{

}