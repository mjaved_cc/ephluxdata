CREATE TABLE IF NOT EXISTS `#__jartists_genres` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`alias` VARCHAR(255)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`logo` VARCHAR(255)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`genre` VARCHAR(255)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_artists` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`label_id` INT(11)  NOT NULL ,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`biography` TEXT(65535)  NOT NULL ,
`genre` VARCHAR(255)  NOT NULL ,
`profile_picture` VARCHAR(255)  NOT NULL ,
`link_website` VARCHAR(255)  NOT NULL ,
`link_facebook` VARCHAR(255)  NOT NULL ,
`link_twitter` VARCHAR(255)  NOT NULL ,
`link_myspace` VARCHAR(255)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_discography` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`description` TEXT(65535)  NOT NULL ,
`cover` VARCHAR(255)  NOT NULL ,
`back_cover` VARCHAR(255)  NOT NULL ,
`itunes_link` VARCHAR(255)  NOT NULL ,
`amazon_link` VARCHAR(255)  NOT NULL ,
`date_release` DATE NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_news` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`content` TEXT(65535)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`created` DATETIME NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_albums` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`description` TEXT(65535)  NOT NULL ,
`created` DATETIME NOT NULL ,
`hits` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_photos` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`image_file` VARCHAR(255)  NOT NULL ,
`description` TEXT(65535)  NOT NULL ,
`album_id` INT(11)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_videos` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`video_url` VARCHAR(255)  NOT NULL ,
`video_type` VARCHAR(255)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_tour` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`event` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`venue` VARCHAR(255)  NOT NULL ,
`location` VARCHAR(255)  NOT NULL ,
`tickets` VARCHAR(255)  NOT NULL ,
`poster_front` VARCHAR(255)  NOT NULL ,
`poster_back` VARCHAR(255)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`date` DATE NOT NULL ,
`created` DATETIME NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_links` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`link_title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`web_address` VARCHAR(255)  NOT NULL ,
`artist_id` INT(11)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__jartists_labels` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`featured` TINYINT(255)  NOT NULL ,
`alpha` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`description` TEXT(65535)  NOT NULL ,
`logo` VARCHAR(255)  NOT NULL ,
`hits` INT(11)  NOT NULL ,
`params` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;

