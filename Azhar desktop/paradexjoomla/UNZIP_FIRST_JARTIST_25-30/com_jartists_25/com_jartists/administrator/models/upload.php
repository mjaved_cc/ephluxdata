<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');
jimport( 'joomla.filter.output' );
/**
 * Jartists model.
 */
class JartistsModelUpload extends JModel
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_JARTISTS';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	 
	 function upload()
	 {
	 	$app = JFactory::getApplication();
	 	$package = $this->_getPackageFromUpload();
		$title = JRequest::getVar('title','', 'post');
		$gallery_id = JRequest::getVar('gallery_id','', 'post');
		$db = JFactory::getDBO();
		// Was the package unpacked?
		if (!$package) {
			$app->setUserState('com_installer.message', JText::_('COM_JARTIST_UNABLE_TO_FIND_ZIP'));
			return false;
		}
		$allowedFileTypes = "jpg|png|gif";
		$allowedFileTypes = explode('|', $allowedFileTypes) ;
		for ($i = 0 , $n = count($allowedFileTypes) ; $i < $n ; $i++) {
			$allowedFileTypes[$i] = strtolower(trim($allowedFileTypes[$i])) ;	
		}
		 $files = JFolder::files($package['extractdir']);
		 $i = 1;
		 foreach($files as $file)
		 {
		 	$ext = JFile::getExt($file) ;
			$ext = strtolower($ext) ;
		 	if (!in_array($ext, $allowedFileTypes)) {
				JFile::delete($package['extractdir'] .DS. $file);		
			}else{
				$image              = new stdClass();
				$image->title        = $title.' '.$i;
				$image->alias        = JFilterOutput::stringURLSafe($image->title);
				$image->image_file   = $file;
				$image->album_id   	 = $gallery_id;
				$image->state   = 1;
				$db->insertObject('#__jartists_photos', $image, 'id');
				JartistsHelper::runGalleryThumbnail($oldImage,$file, $package['extractdir']);
				$i++;
			}
		 }
		$this->copyIndexHtml($package['extractdir']);
		//$this->SavePhotos()
		return $package;
	 }
	 
	 
	protected function _getPackageFromUpload()
	{
		// Get the uploaded file information
		$userfile = JRequest::getVar('install_package', null, 'files', 'array');
		$gallery_id = JRequest::getVar('gallery_id','', 'post');
		$filename = JFile::makeSafe($userfile['name']);
		// Make sure that file uploads are enabled in php
		if (!(bool) ini_get('file_uploads')) {
			JError::raiseWarning('', JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLFILE'));
			return false;
		}

		// Make sure that zlib is loaded so that the package can be unpacked
		if (!extension_loaded('zlib')) {
			JError::raiseWarning('', JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLZLIB'));
			return false;
		}

		// If there is no uploaded file, we have a problem...
		if (!is_array($userfile)) {
			JError::raiseWarning('', JText::_('COM_INSTALLER_MSG_INSTALL_NO_FILE_SELECTED'));
			return false;
		}

		// Check if there was a problem uploading the file.
		if ($userfile['error'] || $userfile['size'] < 1) {
			JError::raiseWarning('', JText::_('COM_INSTALLER_MSG_INSTALL_WARNINSTALLUPLOADERROR'));
			return false;
		}
		$path = '..'.DS.'images'.DS.'jartists'.DS.'gallery_'.$gallery_id;

			if(!JFolder::exists($path)){

				JFolder::create($path);

			}
		// Build the appropriate paths
		//$tmp_dest	= JPATH_ROOT.'/components/com_jartists/tmp' . '/' . $userfile['name'];
		$tmp_dest	= $path. DS .$filename;
		$tmp_src	= $userfile['tmp_name'];
		
		// Move uploaded file
		jimport('joomla.filesystem.file');
		$uploaded = JFile::upload($tmp_src, $path. DS .$filename);
		$package = $this->unpack($path. DS. $filename);
		return $package;
	}
	function CleanUpFolder($package)
	{
	}
	public function unpack($p_filename)
	{
		// Path to the archive
		$archivename = $p_filename;
		$retval = array();
		// Temporary folder to extract the archive into
		//$tmpdir = uniqid('install_');

		// Clean the paths to use for archive extraction
		//$extractdir = JPath::clean(dirname($p_filename) . '/' . $tmpdir);
		$extractdir = JPath::clean(dirname($p_filename));
		$archivename = JPath::clean($archivename);

		// Do the unpacking of the archive
		$result = JArchive::extract($archivename, $extractdir);

		if ($result === false)
		{
			return false;
		}

		/*
		 * Let's set the extraction directory and package file in the result array so we can
		 * cleanup everything properly later on.
		 */
		$retval['extractdir'] = $extractdir;
		$retval['packagefile'] = $archivename;

		/*
		* Remove Sub Folders
		*
		*/
		$folders = JFolder::folders($extractdir);
			foreach($folders as $folder){
				JFolder::delete($extractdir.DS.$folder);
			}
			$i = 0;
		$files = JFolder::files($extractdir);
			foreach($files as $file){
				$time = time(); 
				$file_ext = JFile::getExt($file);
				$file_name = $time.$i.'.'.$file_ext;
				JFile::move($extractdir.DS.$file,$extractdir.DS.$file_name);
				$i++;
			}	
		/*
		 * We have found the install directory so lets set it and then move on
		 * to detecting the extension type.
		 */
		$retval['dir'] = $extractdir;

		/*
		 * Get the extension type and return the directory/type array on success or
		 * false on fail.
		 */
		 return $retval;
		/*if ($retval['type'] = self::detectType($extractdir))
		{
			return $retval;
		}
		else
		{
			return false;
		}*/
	}
	
	public static function removeZip($package, $resultdir)
	{
		//$config = JFactory::getConfig();

		// Does the unpacked extension directory exist?
		/*if (is_dir($resultdir))
		{
			JFolder::delete($resultdir);
		}*/

		// Is the package file a valid file?
		if (is_file($resultdir . $package))
		{
			// It might also be just a base filename
			JFile::delete($resultdir . $package);
		}
	}
	  function copyIndexHtml($folder)
	  {
		jimport('joomla.filesystem.file');
	
		$src  = JPATH_ROOT.DS.'components'.DS.'com_jartists'.DS.'assets'.DS.'index.html';
		$dest = JPath::clean($folder.DS.'index.html');
	
		return JFile::copy($src, $dest);
	  }
	function save_photos($data=array(),$path='')
	{
		$files = JFolder::files($path, $filter = '.', $recurse = false, $full = false, $exclude);
		
		return $files;
	}

}