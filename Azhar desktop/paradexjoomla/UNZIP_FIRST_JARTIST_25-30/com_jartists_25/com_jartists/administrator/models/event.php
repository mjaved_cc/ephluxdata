<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Jartists model.
 */
class JartistsModelevent extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_JARTISTS';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Event', $prefix = 'JartistsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_jartists.event', 'event', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_jartists.edit.event.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed

		}

		return $item;
	}

	function upload2($oldImage)
		{
		 $file = JRequest::getVar('jform', null, 'files', 'array'); 
		//$album_id = JRequest::getVar('album_id','', 'post');
		$post = JRequest::getVar('jform', array(), 'post', 'array');
		//Import filesystem libraries. Perhaps not necessary, but does not hurt
		jimport('joomla.filesystem.file');
	 
		//Clean up filename to get rid of strange characters like spaces etc
		$filename = JFile::makeSafe($file['name']['back_cover']);
	 	
		$path = '..'.DS.'images'.DS.'jartists'.DS.'lg';
			if(!JFolder::exists($path)){
				JartistsHelper::createFolder($path);
			}
			
			
		$time = time(); 
		$file_ext = JFile::getExt($filename);
		$file_name = $time.$i.'.'.$file_ext;
		//Set up the source and destination of the file
		$src =  $file['tmp_name']['back_cover']; 
		$dest = $path. DS . $file_name;
		//First check if the file has the right extension, we need jpg only
		if ( strtolower(JFile::getExt($filename) ) == 'jpg' || strtolower(JFile::getExt($filename) ) == 'png' || strtolower(JFile::getExt($filename) ) == 'gif') {
		   if ( JFile::upload($src, $dest) ) {
			  //Redirect to a page of your choice
		 JartistsHelper::runThumbnail($oldImage, $file_name, $path);
			  return $file_name;
		   } else {
				echo 'There was an error during upload';
			  //Redirect and throw an error message
		   }
		} else {
		echo 'Please upload appropriate file extension';
		 
		   //Redirect and notify user file is not right extension
		}
		}
	
	function upload($oldImage)
		{
		 $file = JRequest::getVar('jform', null, 'files', 'array'); 
		//$album_id = JRequest::getVar('album_id','', 'post');
		$post = JRequest::getVar('jform', array(), 'post', 'array');
		//Import filesystem libraries. Perhaps not necessary, but does not hurt
		jimport('joomla.filesystem.file');
	 
		//Clean up filename to get rid of strange characters like spaces etc
		$filename = JFile::makeSafe($file['name']['poster_front']);
	 	
		$path = '..'.DS.'images'.DS.'jartists'.DS.'lg';
			if(!JFolder::exists($path)){
				JartistsHelper::createFolder($path);
			}
			
			
		$time = time(); 
		$file_ext = JFile::getExt($filename);
		$file_name = $time.$i.'.'.$file_ext;
		//Set up the source and destination of the file
		$src =  $file['tmp_name']['poster_front']; 
		$dest = $path. DS . $file_name;
		//First check if the file has the right extension, we need jpg only
		if ( strtolower(JFile::getExt($filename) ) == 'jpg' || strtolower(JFile::getExt($filename) ) == 'png' || strtolower(JFile::getExt($filename) ) == 'gif') {
		   if ( JFile::upload($src, $dest) ) {
			  //Redirect to a page of your choice
		 JartistsHelper::runThumbnail($oldImage, $file_name, $path);
			  return $file_name;
		   } else {
				echo 'There was an error during upload';
			  //Redirect and throw an error message
		   }
		} else {
		echo 'Please upload appropriate file extension';
		 
		   //Redirect and notify user file is not right extension
		}
		}
		
	public function save($data)
	{
		$file = JRequest::getVar('jform', null, 'files', 'array'); 
		
		if(!empty($file)){
		$id = $data['id'];
		if($id != 0  && !empty($file)){
			$cur_data = $this->getItem($id);
			$oldImage = $cur_data->poster_front;
			$oldImage2 = $cur_data->poster_back;
			//JartistsHelper::delPhotos($cur_data);
		//$this->delPhotos($data);
		}
		$data['poster_front'] = $this->upload($oldImage); 
		$data['back_cover'] = $this->upload2($oldImage2); 
		}
		return parent::save($data);
	}
	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__jartists_tour');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}

}