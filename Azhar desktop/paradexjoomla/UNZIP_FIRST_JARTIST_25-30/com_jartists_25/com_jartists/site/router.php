<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// No direct access

defined('_JEXEC') or die;



/**

 * @param	array	A named array

 * @return	array

 */

function JartistsBuildRoute(&$query)

{

	$segments = array();

       if(isset($query['view']))

       {

                $segments[] = $query['view'];

                unset( $query['view'] );

       }

       if(isset($query['id']))

       {

                $segments[] = $query['id'];

                unset( $query['id'] );

       };

        if(isset($query['layout']))

       {

                $segments[] = $query['layout'];

                unset( $query['layout'] );

       };

        return $segments;

}



/**

 * @param	array	A named array

 * @param	array

 *

 * Formats:

 *

 * index.php?/jartists/task/id/Itemid

 *

 * index.php?/jartists/id/Itemid

 */

function JartistsParseRoute($segments)

{

	 $vars = array();

	 $vars['view'] = $segments[0];

	 		$id = explode( ':', $segments[1] );

                       $vars['id'] = (int) $id[0];

                        if($segments[2]){

                       $vars['layout'] = $segments[2];

                       }

	  /*if($segments[2]){

                       $vars['layout'] = $segments[2];

                       }

       switch($segments[0])

       {

               case 'categories':

                       $vars['view'] = 'categories';

                       break;

               case 'category':

                       $vars['view'] = 'category';

                       $id = explode( ':', $segments[1] );

                       $vars['id'] = (int) $id[0];

                       break;

               case 'artist':

                       $vars['view'] = 'artist';

               

                       $id = explode( ':', $segments[1] );

                       $vars['id'] = (int) $id[0];

                        if($segments[2]){

                       $vars['layout'] = $segments[2];

                       }

                       break;

       }*/

       return $vars;

}