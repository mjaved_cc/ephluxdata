<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<div class="item-page">

<h1><?php echo $this->item->title; ?></h1>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
	<?php if($this->item->image_file): ?>
	<div class="image_file">		
        <?php echo LblmImage::image($this->item->image_file); ?>
		
    </div>
	<?php endif; ?>
    <div class="image_desc">

    	<?php echo $this->item->content; ?>

    </div>
	 <div class="clr"></div>
	<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
</div>