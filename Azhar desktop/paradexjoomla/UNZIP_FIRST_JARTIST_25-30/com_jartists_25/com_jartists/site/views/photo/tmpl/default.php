<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<div class="item-page">

<h2><?php echo $this->item->title; ?></h2>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
	<div class="image_file">

       <img src="<?php echo LBLM_IMAGE_M.$this->item->image_file; ?>" class="jartists-450" />

    </div>

    <div class="image_meta">

    	<dl class="meta">

        <dt class="meta"><?php  echo JText::_('Details'); ?></dt>

        <dd class="meta">Name: <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&id='.$this->item->artist_slug); ?>"><?php echo $this->item->artist; ?></a></dd>

        <dd class="meta">Album: <?php echo $this->item->gallery; ?></dd>

        <dd class="meta">Hits <?php echo $this->item->hits; ?></dd>

         <div class="clr"></div>

        </dl>

    </div>

    <div class="image_desc">

    	<?php echo $this->item->description; ?>

    </div>
	 <div class="clr"></div>
	<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
    <div class="other_images">

		<?php echo $this->loadTemplate('others'); ?>

    </div>
	
</div>