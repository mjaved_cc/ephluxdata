<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */





// no direct access

defined('_JEXEC') or die;
?>
<div>
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<div>
<?php if($this->items) : ?>
<table class="category" cellspacing="4">
	<thead>
    <tr>
    	<th class="title"  width="200"><?php echo JText::_('Link'); ?></th>
        <th class="title" width="100"><?php echo JText::_('Artist'); ?></th>
    </tr>
    </thead>
	<tbody>
    <?php foreach ($this->items as $item) :?>
		<tr class="cat-list-row">
                <td class="title" width="200">
			<a href="<?php echo $item->web_address; ?>"><?php echo $item->link_title; ?></a></td>
            <td class="title" width="100">
			<a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&id='.$item->artist_slug); ?>"><?php echo $item->artist; ?></a></td>
            </tr>
    <?php endforeach; ?>
</tbody>
</table>
    </div>
     <div class="pagination">
        <?php if ($this->params->def('show_pagination_results', 1)) : ?>
            <p class="counter">
                <?php echo $this->pagination->getPagesCounter(); ?>
            </p>
        <?php endif; ?>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>
</div>