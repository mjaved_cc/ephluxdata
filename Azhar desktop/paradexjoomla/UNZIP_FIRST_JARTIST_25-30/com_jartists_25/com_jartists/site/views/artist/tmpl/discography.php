<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_DISCOGRAPHY_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

 <div class="discography_list">

            	<?php foreach($this->realeases->items as $item): ?>

                    <div class="disc_item"> <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$item->slug); ?>"><img src="<?php echo LBLM_IMAGE_T.$item->cover; ?>" class="jartists-150" /></a>

                 <span class="title"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=discography&id='.$item->slug); ?>"><?php echo $item->title; ?></a></span>

                    </div>

                <?php endforeach; ?>

            </div>

<div class="pagination">

    <p class="counter">

        <?php echo $this->realeases->pagination->getPagesCounter(); ?>

    </p>

    <?php echo $this->realeases->pagination->getPagesLinks(); ?>

</div>