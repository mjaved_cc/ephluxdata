<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->press): ?>

	<div class="display_row">

        <?php foreach($this->press as $item): ?>

           <div class="articleBlock">

			   <div class="aticleTitle"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=item&id='.$item->slug); ?>"><?php echo $item->title; ?></a></div>

               <div class="articleContent"><?php echo substr(strip_tags($item->content),0,100); ?></div>

               <div class="read-more"><a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=item&id='.$item->slug); ?>">Read More</a></div>

               <div class="clr"></div>

		   </div>

        <?php endforeach; ?>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=press&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>

    </div>

<?php endif; ?>