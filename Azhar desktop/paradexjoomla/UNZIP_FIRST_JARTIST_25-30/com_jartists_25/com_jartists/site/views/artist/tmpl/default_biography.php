<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php echo substr(strip_tags($this->item->biography,'<p><a><br />'),0,600); ?>

<div class="clr"></div>

<a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=biography&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_READ_MORE'); ?></a>

