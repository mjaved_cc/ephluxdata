<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_LINKS_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

            <div class="links_list">

	<?php foreach($this->link->items as $item): ?>

		<div class="artistLink"><a href="<?php echo $item->web_address;?>" target="_blank"><?php echo  $item->link_title; ?></a></div>

	<?php endforeach; ?>

</div>

<div class="pagination">

            <p class="counter">

                <?php echo$this->link->pagination->getPagesCounter(); ?>

            </p>

        <?php echo $this->link->pagination->getPagesLinks(); ?>

</div>