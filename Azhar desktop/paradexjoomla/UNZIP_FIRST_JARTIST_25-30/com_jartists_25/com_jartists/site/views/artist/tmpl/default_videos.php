<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->videos): ?>

	<div class="display_row">

        <ul>

        <?php foreach($this->videos as $item):

			$video = WpaVideo($item->video_url);

			if($video['video_type']=='youtube'){

				$item->thumb =  'http://i.ytimg.com/vi/'.$video['video_id'].'/0.jpg';

				$item->vid =  	$video['video_id'];

			}

		 ?>

            <li>

                 <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=video&id=' . $item->slug); ?>"><img src="<?php echo $item->thumb; ?>" width="104" height="79" border="0" /></a><span class="title" style="display:block; font-weight:bold;"><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=video&id=' .$item->slug); ?>"><?php echo substr($item->title,0,20); ?></a></span>

            </li>

        <?php endforeach; ?>	

        <div class="clr"></div>

        </ul>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=videos&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>

    </div>

<?php endif; ?>