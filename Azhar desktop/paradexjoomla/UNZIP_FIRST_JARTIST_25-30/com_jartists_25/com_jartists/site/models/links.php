<?php



/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */

defined('_JEXEC') or die;



jimport('joomla.application.component.modellist');



/**

 * Methods supporting a list of Jartists records.

 */

class JartistsModelLinks extends JModelList {



    /**

     * Constructor.

     *

     * @param    array    An optional associative array of configuration settings.

     * @see        JController

     * @since    1.6

     */

    public function __construct($config = array()) {

        parent::__construct($config);

    }



    /**

     * Method to auto-populate the model state.

     *

     * Note. Calling getState in this method will result in recursion.

     *

     * @since	1.6

     */

    protected function populateState($ordering = null, $direction = null) {

        

        // Initialise variables.

        $app = JFactory::getApplication();



        // List state information

        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));

        $this->setState('list.limit', $limit);





		// Load state from the request.

		$artist = JRequest::getInt('id');

		$this->setState('artist.id', $artist);

		

		

        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');

        $this->setState('list.start', $limitstart);



        // List state information.

        parent::populateState();

    }



    /**

     * Build an SQL query to load the list data.

     *

     * @return	JDatabaseQuery

     * @since	1.6

     */

    protected function getListQuery() {

        // Create a new query object.

        $db = $this->getDbo();

        $query = $db->getQuery(true);



        // Select the required fields from the table.

        $query->select(

                $this->getState(

                        'list.select', 'a.*, CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug'

                )

        );

        $query->from('`#__jartists_links` AS a');

		// Join over the users for the checked out user.

        $query->select('s.title AS artist');
		$query->select('CASE WHEN CHAR_LENGTH(s.alias) THEN CONCAT_WS(":", s.id, s.alias) ELSE s.id END as artist_slug');
        $query->join('LEFT', '#__jartists_artists AS s ON s.id=a.artist_id');

        

       // Filter by Artist

        $artist = $this->getState('artist.id');

			if (is_numeric($artist) && $artist != 0) {

               $query->where('a.artist_id = '.(int) $artist);

        } 

	   

	   

	    // Filter by published state

        $published = $this->getState('filter.state');

        if (is_numeric($published)) {

            $query->where('a.state = '.(int) $published);

        } else if ($published === '') {

            $query->where('(a.state IN (0, 1))');

        }

        





        return $query;

    }



}

