<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->links): ?>

	<div class="display_row">

        <ul>

        <?php foreach($this->links as $item): ?>

            <li>

                 <a href="<?php echo $item->web_address;?>" target="_blank"><?php echo  $item->link_title; ?></a>

            </li>

        <?php endforeach; ?>	

        <div class="clr"></div>

        </ul>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=links&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>

    </div>

<?php endif; ?>