<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<h1><?php echo JText::_('COM_JARTISTS_DEFAULT_BIOGRAPHY_PAGE_TITLE'); ?></h1>

<?php echo $this->pageMenu(); ?>

            <div>

<?php echo $this->item->biography; ?>

</div>