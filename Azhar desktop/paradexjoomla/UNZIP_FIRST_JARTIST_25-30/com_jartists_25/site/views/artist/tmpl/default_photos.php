<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<?php if($this->photos): ?>

	<div class="display_row">

        <ul>

        <?php foreach($this->photos as $image): ?>

            <li>

                 <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=photo&id='.$image->slug); ?>"><img src="<?php echo LBLM_IMAGE_T.$image->image_file; ?>" class="jartists-100" /></a>

            </li>

        <?php endforeach; ?>

        <div class="clr"></div>	

        </ul>

        <a href="<?php echo JROUTE::_('index.php?option=com_jartists&view=artist&layout=photos&id='.$this->item->slug); ?>" class="read-more"><?php echo JText::_('COM_JARTISTS_VIEW_MORE'); ?></a>

        <div class="clr"></div>
    </div>

<?php endif; ?>