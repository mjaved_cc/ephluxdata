<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */



// no direct access

defined('_JEXEC') or die;

?>

<div class="item-page">

<h2><?php echo $this->item->title; ?></h2>
<?php echo $this->item->event->onJArtistsAfterTitle; ?>
	<?php if($this->item->logo): ?>

        <div class="image_file">

                <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' .$this->item->slug); ?>"><img alt="<?php echo $item->logo; ?>" src="<?php echo $this->item->logo; ?>"></a>

        </div>

        <?php endif; ?>

    <div class="image_desc">

    	<?php echo $this->item->description; ?>

    </div>

    <div>

    	<a class="read-more"><a href="<?php echo JRoute::_('index.php?option=com_jartists&view=artists&label_id=' .$this->item->slug); ?>"><?php echo JText::_('COM_JARTISTS_VIEW_ARTISTS'); ?></a>

</a>

    </div>
	 <div class="clr"></div>
	<?php echo $this->item->event->onJArtistsAfterDisplay; ?>
</div>

