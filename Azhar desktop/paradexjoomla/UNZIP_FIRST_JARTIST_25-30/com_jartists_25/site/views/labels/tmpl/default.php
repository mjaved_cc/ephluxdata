<?php

/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */





// no direct access

defined('_JEXEC') or die;

?>
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>

<?php if($this->items) : ?>



    <div class="items">              

    <?php foreach ($this->items as $item) :?>

    <article class="label">

    <?php if($item->logo): ?>

        <div class="logo">

                <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' . $item->slug); ?>"><img alt="<?php echo $item->title; ?>" src="<?php echo $item->logo; ?>"></a>

        </div>

        <?php endif; ?>

        <section class="description">

            <h1 class="title">

                <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' . $item->slug); ?>"><?php echo $item->title; ?></a>

		    </h1>

            <?php if($item->description): ?>

            <p>

                <?php echo substr($item->description,0,200); ?>

		    </p>

            <?php endif; ?>

                <div class="links">

                    <a href="<?php echo JRoute::_('index.php?option=com_jartists&view=label&id=' .$item->slug); ?>" >View <?php echo $item->title; ?></a>

                    </div>

        </section>

    </article>

    <?php endforeach; ?>

    </div>



     <div class="pagination">

        <?php if ($this->params->def('show_pagination_results', 1)) : ?>

            <p class="counter">

                <?php echo $this->pagination->getPagesCounter(); ?>

            </p>

        <?php endif; ?>

        <?php echo $this->pagination->getPagesLinks(); ?>

    </div>





<?php endif; ?>