<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldDiscography extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'text';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		if(empty($attribs)){

			$attribs = 'class="inputbox"';

			}

		$db = JFactory::getDbo();



		$query = $db->getQuery(true);



		$query->select('*')



		 ->from('#__jartists_discography')
		 ->order('title ASC');
		$db->setQuery($query);

		

		$rows = $db->loadObjectlist();

			

		 $options 	= array();



   		$options[] 	= JHTML::_('select.option',  '0', JText::_( 'COM_JARTISTS_SELECT_DISCOGRAPHY' ) );

    		foreach ( $rows as $row ) {

    			$options[] = JHTML::_('select.option',  $row->id,$row->title );

    		}

			return JHTML::_('select.genericlist', $options, $this->name, array(

    		        'option.text.toHtml' => false ,

    		        'list.attr' => $attribs,

    				'option.text' => 'text' ,    

    				'option.key' => 'value',

    				'list.select' => $this->value, 		    		    		    		    

    		    )

			); 
	}
}