<?php
/**
 * @version     1.0.0
 * @package     com_jartists
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
// Import CSS
$document = &JFactory::getDocument();
$document->addStyleSheet('components/com_jartists/assets/css/jartists.css');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'event.cancel' || document.formvalidator.isValid(document.id('event-form'))) {
			Joomla.submitform(task, document.getElementById('event-form'));
		}
		else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jartists&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="event-form" class="form-validate" enctype="multipart/form-data">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_JARTISTS_LEGEND_EVENT'); ?></legend>
			<ul class="adminformlist">

            
			<li><?php echo $this->form->getLabel('id'); ?>
			<?php echo $this->form->getInput('id'); ?></li>
            <li><?php echo $this->form->getLabel('event'); ?>
			<?php echo $this->form->getInput('event'); ?></li>
            <li><?php echo $this->form->getLabel('alias'); ?>
			<?php echo $this->form->getInput('alias'); ?></li>
            
            
			<li><?php echo $this->form->getLabel('artist_id'); ?>
			<?php echo $this->form->getInput('artist_id'); ?></li>

                        
            <li><?php echo $this->form->getLabel('featured'); ?>
			<?php echo $this->form->getInput('featured'); ?></li>
            
			

            
			<li><?php echo $this->form->getLabel('venue'); ?>
			<?php echo $this->form->getInput('venue'); ?></li>

            
			<li><?php echo $this->form->getLabel('location'); ?>
			<?php echo $this->form->getInput('location'); ?></li>

            
			<li><?php echo $this->form->getLabel('tickets'); ?>
			<?php echo $this->form->getInput('tickets'); ?></li>

            
			<li><?php echo $this->form->getLabel('poster_front'); ?>
			<?php echo $this->form->getInput('poster_front'); ?></li>

            
			<li><?php echo $this->form->getLabel('poster_back'); ?>
			<?php echo $this->form->getInput('poster_back'); ?></li>

            
			<li><?php echo $this->form->getLabel('date'); ?>
			<?php echo $this->form->getInput('date'); ?></li>

            

            <li><?php echo $this->form->getLabel('state'); ?>
                    <?php echo $this->form->getInput('state'); ?></li><li><?php echo $this->form->getLabel('checked_out'); ?>
                    <?php echo $this->form->getInput('checked_out'); ?></li><li><?php echo $this->form->getLabel('checked_out_time'); ?>
                    <?php echo $this->form->getInput('checked_out_time'); ?></li>

            </ul>
		</fieldset>
	</div>


	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
	<div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>