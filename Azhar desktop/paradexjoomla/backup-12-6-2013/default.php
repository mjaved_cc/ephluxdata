<?php
/**

 * @version     1.0.0

 * @package     com_jartists

 * @copyright   Copyright (C) 2011. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      B4uCode <info@b4ucode.com> - http://b4ucode.com

 */
// no direct access
defined('_JEXEC') or die;
ini_set('memory_limit', '120M');
include('simple_html_dom.php');
?>
<!--<h1><?php // echo JText::_('COM_JARTISTS_DEFAULT_BIOGRAPHY_PAGE_TITLE');       ?></h1>-->

<?php // echo $this->pageMenu(); ?>

<div>
	<?php // echo $this->item->biography;	print_r($this->item); ?>


	<h1><?php echo $this->item->title ?></h1>
	<div class="grid-box width100">
		<div class="about-variation1-style about_image">
			<img width="180" alt="Spotlight Image" src="<?php echo LBLM_IMAGE_S . $this->item->profile_picture; ?>">
			<?php
			$html = str_get_html($this->item->biography);

			foreach ($html->find('div#location') as $e)
				echo '<div>' . $e->innertext . '</div>';

			foreach ($html->find('div#player') as $e)
				echo '<div>' . $e->innertext . '</div>';

			foreach ($html->find('div#buy') as $e)
				echo '<div id="buy">'.$e->innertext.'</div>';
			?>
		</div>
		<div class="bfc-o padding-25">
			<?php
			foreach ($html->find('div#profile_text') as $e)
				echo $e->innertext;
			?>
			<?php // echo $this->item->biography ?>


			<?php echo $this->loadTemplate('photos'); ?>

			<?php
			foreach ($html->find('div#icons') as $e)
				echo $e->innertext;

			foreach ($html->find('div#artist_hidden_form') as $e)
				echo $e->innertext;
			?>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
			</script>
			<script>
				$(document).ready(function(){
					$("#artist_form").attr("action", "http://apptestonline.com/paradexjoomla/index.php/component/hikashop/product/listing?Itemid=523");
					$("#buy").click(function(){
						$("#artist_form").submit();
					});
				});
			</script>

		</div>
	</div>
	<?php // echo $this->loadTemplate('videos'); ?>
</div>
