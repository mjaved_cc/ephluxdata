<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author: shan
 */

class DtInvoice {
	var $fields = array(); 
	
	public function __construct($rowInvoice) {
//		$this->id		= $rowInvoice["id"]; 
//		$this->order_id = $rowInvoice["order_id"]; 
//		$this->created	= $rowInvoice["created"]; 
//		$this->modified = $rowInvoice['modified']; 
		
		
		foreach ($rowInvoice as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0';
		}
	}
	
	public function __set($name, $value) {
		$this->fields[$name] = $value;
	}
	
	public function __get($name) {
		return $this->fields[$name];
	}
}