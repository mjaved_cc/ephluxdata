<?php

class DtUserSocialAccount extends DtUser {

	public $fields = array();

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			case 'image_url':
				$this->fields[$key] = $this->get_image($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields($key = 0) {
		if (isset($this->fields[$key]))
			return $this->fields[$key];
		else
			return $this->fields;
	}

}