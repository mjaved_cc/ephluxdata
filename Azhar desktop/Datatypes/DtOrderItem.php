<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author: Furqan Ahmed
 */

class DtOrderItem {

	var $fields = array();

	public function __construct($rowOrderItem) {

//		$this->id				= $rowOrderItem['id'];
//		$this->shipping_total	= $rowOrderItem['shipping_total'];
//		$this->tax_total		= $rowOrderItem['tax_total'];
//		$this->total			= $rowOrderItem['total'];
//		$this->discount			= $rowOrderItem['discount'];
//		$this->net_total		= $rowOrderItem['net_total'];
//		$this->shipping_address	= $rowOrderItem['shipping_address'];
//		$this->merchant_id		= $rowOrderItem['merchant_id'];
//		$this->user_id			= $rowOrderItem['user_id'];
//		$this->user_cart_id		= $rowOrderItem['user_cart_id'];
//		$this->source_url		= $rowOrderItem['source_url'];
//		$this->created			= $rowOrderItem['created'];
//		$this->modified			= $rowOrderItem['modified'];
//		$this->status			= $rowOrderItem['status'];

		foreach ($rowOrderItem as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	public function __set($key, $value) {

		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {
		return $this->fields[$key];
	}
	
	public function get_fields() {
		return $this->fields;
	}

}