<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author: Furqan Ahmed
 */

class DtTransaction {

	var $fields = array();

	public function __construct($rowTrans) {

		$this->id = $this->_filter_value($rowTrans['id']);
		$this->data = $this->_filter_value($rowTrans['data']);
		$this->user_id = $this->_filter_value($rowTrans['user_id']);
		$this->cart_id = $this->_filter_value($rowTrans['cart_id']);
		$this->merchant_id = $this->_filter_value($rowTrans['merchant_id']);
		$this->user_card_id = $this->_filter_value($rowTrans['user_card_id']);
		$this->source_url = $this->_filter_value($rowTrans['source_url']);
		$this->error_status = $this->_filter_value($rowTrans['error_status']);
		$this->error_msg = $this->_filter_value($rowTrans['error_msg']);
		$this->status = $this->_filter_value($rowTrans['status']);
		$this->created = $this->_filter_value($rowTrans['created']);
		$this->modified = $this->_filter_value($rowTrans['modified']);
	}

	public function __set($key, $value) {

		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			case 'source_url':
				$this->fields[$key] = $this->_get_domain_name($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		if ($key == 'net_total')
			return $this->_get_net_total();
		if ($key == 'secret_key')
			return $this->_get_secret_key();
		else
			return $this->fields[$key];
	}

	private function _filter_value($value) {

		return (!empty($value)) ? $value : '';
	}

	private function _get_domain_name($url) {

		$parsed_url = parse_url($url);
		return (!empty($parsed_url['host'])) ? $parsed_url['host'] : '';
	}

	function get_fields() {
		return $this->fields;
	}

	private function _get_net_total() {
		$data = json_decode($this->data, true);

		if (!empty($data)) {
			return $data['shopping_cart']['net_total'];
		} else
			return '0';
	}
	
	private function _get_secret_key() {
		$data = json_decode($this->data, true);

		if (!empty($data)) {
			return $data['authentication']['secret_key'];
		} else
			return '0';
	}

}