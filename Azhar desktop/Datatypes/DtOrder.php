<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author: Furqan Ahmed
 */

class DtOrder {

	var $fields = array();

	public function __construct($rowOrder) {

//		$this->id				= $rowOrder['id'];
//		$this->shipping_total	= $rowOrder['shipping_total'];
//		$this->tax_total		= $rowOrder['tax_total'];
//		$this->total			= $rowOrder['total'];
//		$this->discount			= $rowOrder['discount'];
//		$this->net_total		= $rowOrder['net_total'];
//		$this->shipping_address	= $rowOrder['shipping_address'];
//		$this->merchant_id		= $rowOrder['merchant_id'];
//		$this->user_id			= $rowOrder['user_id'];
//		$this->user_cart_id		= $rowOrder['user_cart_id'];
//		$this->source_url		= $rowOrder['source_url'];
//		$this->created			= $rowOrder['created'];
//		$this->modified			= $rowOrder['modified'];
//		$this->status			= $rowOrder['status'];

		foreach ($rowOrder as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	public function __set($key, $value) {

		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			case 'source_url':
				$this->fields[$key] = $this->_get_domain_name($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {
		return $this->fields[$key];
	}
	
	public function get_fields() {
		return $this->fields;
	}

	private function _get_domain_name($url) {

		$parsed_url = parse_url($url);
		return (!empty($parsed_url['host'])) ? $parsed_url['host'] : '';
	}

}