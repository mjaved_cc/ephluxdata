<?php

class DtUserCard extends DtUser {

	public $fields = array();

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			case 'expiration_date':
			case 'cardholder_name':
				$this->fields[$key] = $value;
				break;
			case 'card_number':
			case 'cvv_code':
				$this->fields[$key] = new DtSecureNumber($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if ($key == 'card_name')
				return $this->_get_card_name() ;
			else if ($key == 'formatted_cc')
				return $this->card_number->get_formatted_num();
			else if ($key == 'two_dgt_month')
				return $this->_get_two_dgt_month() ;
			else if ($key == 'two_dgt_year')
				return $this->_get_two_dgt_year() ;
			else if ($key == 'full_year')
				return $this->_get_full_year() ;
			else if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields($key = 0) {
		if (isset($this->fields[$key]))
			return $this->fields[$key];
		else
			return $this->fields;
	}

	private function _get_card_name() {
		
		return $this->_get_formatted_card_type($this->type_id) ;
	}
	
	private function _get_two_dgt_month(){
		list($month , $year) = $this->_split_exp_date();
		
		return str_pad($month , 2 , 0 , STR_PAD_LEFT) ;
	}
	
	private function _get_two_dgt_year(){
		list($month , $year) = $this->_split_exp_date();
		
		return substr($year, -2) ; 
	}
	
	/**
	 * A full numeric representation of a year, 4 digits 
	 * 
	 * Examples: 1999 or 2003
	 */
	private function _get_full_year(){
		list($month , $year) = $this->_split_exp_date();
		
		return $year ; 
	}
	
	/**
	 * Split expiration date (mm/yyyy) to their corresponding month & year
	 * 
	 * @return array month & year
	 */
	private function _split_exp_date(){
		list($month , $year) = explode('/', $this->expiration_date);
		
		return array($month , $year) ;
	}

	/**
	 * Convert string foo_bar to FooBar|fooBar.
	 * 
	 * @param string $string Self-explanatory
	 * @param boolean $capitalize_first_char Self-explanatory
	 * @return string camel case
	 */
	private function _get_formatted_card_type($string) {

		return ucwords(str_replace('_', ' ', $string));
		
	}

}