<?php

class DtUserSetting extends DtUser {

	public $fields = array();

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0'; // iphone developer ask for sending it as string value.
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'created':
			case 'modified':
				$this->fields[$key] = new DtDate($value);
				break;
			case 'remember_pwd':
			case 'remember_email':
			case 'confirm_order_pwd':
				$this->fields[$key] = (!empty($value)) ? '1' : '0'; 
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	public function get_fields($key = 0) {
		if (isset($this->fields[$key]))
			return $this->fields[$key];
		else
			return $this->fields;
	}

}