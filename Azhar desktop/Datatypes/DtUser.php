<?php

class DtUser {

	public $fields = array();

	public function __construct($data = null) {

		foreach ($data as $key => $value) {
			$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	public function __set($key, $value) {
		switch ($key) {
			case 'User';
				$this->set_user($value);
				break;
			case 'UserSocialAccount';
				$this->set_user_social_acc($value, $key);
				break;
			case 'UserSetting';
				$this->fields[$key] = new DtUserSetting($value);
				break;
			case 'UserCard';
				$this->set_user_card($value, $key);
				break;
			case 'UserAddress';
				$this->set_user_addr($value, $key);
				break;
			case 'created';
			case 'modified';
				$this->fields[$key] = new DtDate($value);
				break;
			default:
				$this->fields[$key] = $value;
		}
	}

	public function __get($key) {

		try {
			if ($key == 'display_name')
				return $this->get_display_name();
			else if ($key == 'last_name')
				return $this->get_last_name();
			else if (isset($this->fields[$key]))
				return $this->fields[$key];
			else
				throw new Exception();
		} catch (Exception $e) {
			return null;
		}
	}

	private function set_user($data) {

		// $date_fields = array('created', 'modified');
		$ignore_fields = array('role_id', 'ip_address', 'password');
		foreach ($data as $key => $value) {
			/* if (in_array($key, $date_fields))
			  $this->$key = $value ;
			  else */ if (in_array($key, $ignore_fields))
				continue;
			else
				$this->$key = $value;
		}
	}

	private function set_user_social_acc($data, $model) {
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$this->fields[$model][$key] = new DtUserSocialAccount($value);
			}
		}
	}

	private function set_user_card($data, $model) {
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$this->fields[$model][$key] = new DtUserCard($value);
			}
		}
	}

	private function set_user_addr($data, $model) {
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$this->fields[$model][$key] = new DtUserAddress($value);
			}
		}
	}

	public function get_fields() {
		return $this->fields;
	}

	function get_image($image_url) {
		if (!empty($image_url)) {
			return $image_url;
		} else {
			return POBOQ_BASE_URI . '/img/default.png';
		}
	}

	function get_display_name() {
		return $this->first_name . ' ' . $this->last_name;
	}
	
	function get_last_name() {
		
		$last_name = $this->fields['last_name'] ;
		if(empty($last_name)){
			return ' ' ;  // return empty last name if last name is empty
		} else			
			return $last_name;
	}

	function add_user_setting($settings) {
		$this->UserSetting = new DtUserSetting($settings);
	}

	/**
	 * Add single card to user object
	 * 
	 * @param array $card card information
	 */
	function add_user_card($card) {
		if (isset($this->fields["UserCard"]) && !is_array($this->fields["UserCard"])) {

			$prev_value = $this->UserCard; // restore previous value
			unset($this->fields["UserCard"]); // now unset it so that we can convert object to array
			$this->fields["UserCard"][] = $prev_value;
			$this->fields["UserCard"][] = new DtUserCard($card);
		} else if (isset($this->fields["UserCard"]) && is_array($this->fields["UserCard"]))
			$this->fields["UserCard"][] = new DtUserCard($card);
		else
			$this->fields["UserCard"] = new DtUserCard($card);
	}

	/**
	 * Add multiple cards to user object
	 * 
	 * @param array $cards card information
	 */
	function add_user_cards($cards) {
		if (!empty($cards)) {
		foreach ($cards as $value)
			$this->add_user_card($value);
		}
	}

	/**
	 * Add single address to user object
	 * 
	 * @param array $address address information
	 */
	function add_user_address($address) {
		if (isset($this->fields["UserAddress"]) && !is_array($this->fields["UserAddress"])) {

			$prev_value = $this->UserAddress; // restore previous value
			unset($this->fields["UserAddress"]); // now unset it so that we can convert object to array
			$this->fields["UserAddress"][] = $prev_value;
			$this->fields["UserAddress"][] = new DtUserAddress($address);
		} else if (isset($this->fields["UserAddress"]) && is_array($this->fields["UserAddress"]))
			$this->fields["UserAddress"][] = new DtUserAddress($address);
		else
			$this->fields["UserAddress"] = new DtUserAddress($address);
	}

	/**
	 * Add multiple addresses to user object
	 * 
	 * @param array $addresses address information
	 */
	function add_user_addresses($addresses) {
		if (!empty($addresses)) {
			foreach ($addresses as $value)
				$this->add_user_address($value);
		}
	}
	
	/**
	 * Add single social account to user object
	 * 
	 * @param array $saccount social account information
	 */
	function add_user_saccount($saccount) {
		if (isset($this->fields["UserSocialAccount"]) && !is_array($this->fields["UserSocialAccount"])) {

			$prev_value = $this->UserSocialAccount; // restore previous value
			unset($this->fields["UserSocialAccount"]); // now unset it so that we can convert object to array
			$this->fields["UserSocialAccount"][] = $prev_value;
			$this->fields["UserSocialAccount"][] = new DtUserSocialAccount($saccount);
		} else if (isset($this->fields["UserSocialAccount"]) && is_array($this->fields["UserSocialAccount"]))
			$this->fields["UserSocialAccount"][] = new DtUserSocialAccount($saccount);
		else
			$this->fields["UserSocialAccount"] = new DtUserSocialAccount($saccount);
	}

	/**
	 * Add multiple social account to user object
	 * 
	 * @param array $saccounts address information
	 */
	function add_user_saccounts($saccounts) {
		if (!empty($saccounts)) {
			foreach ($saccounts as $value)
				$this->add_user_saccount($value);
		}
	}
	
	/**
	 * Add single transaction to user object
	 * 
	 * @param array $transaction transaction information
	 */
	function add_user_transaction($transaction) {
		if (isset($this->fields["Transaction"]) && !is_array($this->fields["Transaction"])) {

			$prev_value = $this->Transaction; // restore previous value
			unset($this->fields["Transaction"]); // now unset it so that we can convert object to array
			$this->fields["Transaction"][] = $prev_value;
			$this->fields["Transaction"][] = new DtTransaction($transaction);
		} else if (isset($this->fields["Transaction"]) && is_array($this->fields["Transaction"]))
			$this->fields["Transaction"][] = new DtTransaction($transaction);
		else
			$this->fields["Transaction"] = new DtTransaction($transaction);
	}

	/**
	 * Add multiple transactions to user object
	 * 
	 * @param array $transactions transaction information
	 */
	function add_user_transactions($transactions) {
		if (!empty($transactions)) {
			foreach ($transactions as $value)
				$this->add_user_transaction($value);
		}
	}
	
	/**
	 * Add single order to user object
	 * 
	 * @param array $order order information
	 */
	function add_user_order($order) {
		if (isset($this->fields["Order"]) && !is_array($this->fields["Order"])) {

			$prev_value = $this->Order; // restore previous value
			unset($this->fields["Order"]); // now unset it so that we can convert object to array
			$this->fields["Order"][] = $prev_value;
			$this->fields["Order"][] = new DtOrder($order);
		} else if (isset($this->fields["Order"]) && is_array($this->fields["Order"]))
			$this->fields["Order"][] = new DtOrder($order);
		else
			$this->fields["Order"] = new DtOrder($order);
	}

	/**
	 * Add multiple orders to user object
	 * 
	 * @param array $orders order information
	 */
	function add_user_orders($orders) {
		if (!empty($orders)) {
			foreach ($orders as $value)
				$this->add_user_order($value);
		}
	}
	
	/**
	 * Add single order item to user object
	 * 
	 * @param array $order_item order item information
	 */
	function add_user_order_item($order_item) {
		if (isset($this->fields["OrderItem"]) && !is_array($this->fields["OrderItem"])) {

			$prev_value = $this->OrderItem; // restore previous value
			unset($this->fields["OrderItem"]); // now unset it so that we can convert object to array
			$this->fields["OrderItem"][] = $prev_value;
			$this->fields["OrderItem"][] = new DtOrderItem($order_item);
		} else if (isset($this->fields["OrderItem"]) && is_array($this->fields["OrderItem"]))
			$this->fields["OrderItem"][] = new DtOrderItem($order_item);
		else
			$this->fields["OrderItem"] = new DtOrderItem($order_item);
	}

	/**
	 * Add multiple order items to user object
	 * 
	 * @param array $order_items order item information
	 */
	function add_user_order_items($order_items) {
		if (!empty($order_items)) {
			foreach ($order_items as $value)
				$this->add_user_order_item($value);
		}
	}

}