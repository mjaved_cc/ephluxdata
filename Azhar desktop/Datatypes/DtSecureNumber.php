<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DtSecureNumber
 *
 * @author shan
 * @since: 30th October, 2012
 * @
 */
class DtSecureNumber {
	const SecureNumber_CUSTOM = 1;
	const SecureNumber_MD5 = 2;
	const SecureNumber_NONE = 3;

	private $number;
	private $encType;
	private $salt;
	private $algo = array(
		array('s', 'S', 't', 'T'),
		array('a', 'A', 'b', 'B'),
		array('q', 'Q', 'r', 'R'),
		array('c', 'C', 'd', 'D'),
		array('o', 'O', 'p', 'P'),
		array('e', 'E', 'f', 'F'),
		array('m', 'M', 'n', 'N'),
		array('g', 'G', 'h', 'H'),
		array('k', 'K', 'l', 'L'),
		array('i', 'I', 'j', 'J')		
	);

	/*
	public function __construct($data, $type = self::SecureNumber_CUSTOM, $salt = null) {
		$this->data = $data;
		$this->encType = $type;
		$this->salt = $salt;

		switch ($this->encType) {
			case self::SecureNumber_CUSTOM:
				$this->encryptCustom();
				break;

			case self::SecureNumber_MD5:
				$this->encData = md5($this->data . $this->salt);
				break;
		}
	}
	 */
	
	public function __construct($number, $encrypted=true, $encryptionType=self::SecureNumber_CUSTOM) {
		$this->number	= $number;
		$this->encType	= $encryptionType;
		
		if (!$encrypted)
			$this->encryptCustom();
	}

	private function encryptCustom() {
		$strData = str_split($this->number);

		$encData = "";

		foreach ($strData as $value) {
			if ($value >= 0 && $value <= 9)
				$encData .= $this->algo[$value][round(rand(0, 3), 0)];
			else 
				$encData .= $value;
		}
		
		$this->number = $encData;
	}

	public function decrypt() {
		$strData = str_split($this->number);
		
		$decData = "";
		
		foreach($strData as $value) 
			for($index=0; $index<count($this->algo); $index++) 
				if (in_array($value, $this->algo[$index]))
						$decData .= $index;
				
		return $decData;
	}
	
	public function getEncrypted() {
		return $this->number;
	}
	
	/**
	 * Get formatted card number as asked by client - XXXX-1234
	 */
	function get_formatted_num(){
		
		$prefix = 'XXXX-' ;
		$card_num = $this->decrypt();
		
		// get last four digit
		$last_four_dgt = substr($card_num , -4);
		
		return $prefix . $last_four_dgt ;		
	}
}