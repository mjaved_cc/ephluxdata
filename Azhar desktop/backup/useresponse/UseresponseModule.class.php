<?php

/**
 * UseResponse module defintiion
 *
 * @package activeCollab.modules.useresponse
 * @subpackage models
 */
class UseresponseModule extends AngieModule {

    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'useresponse';

    /**
     * Module version
     *
     * @var string
     */
    protected $version = '1.0';

    /**
     * Define module routes
     */
    private $module_id = null;
    private $check_version_url = 'http://www.uswebstyle.com/status/versions.xml';
    private $products_url = 'http://www.uswebstyle.com/products.html';

    function getCheckVersionUrl() {
        return $this->check_version_url;
    }

    function getInternalId() {
        return $this->module_id;
    }

    function defineRoutes() {

        Router::map('useresponse_settings', 'admin/useresponse_settings', array('controller' => 'useresponse_settings_admin', 'action' => 'index'));
        Router::map('useresponse', 'useresponse', array('controller' => 'useresponse', 'action' => 'index'));
        Router::map('useresponse_view', 'useresponse/:id', array('controller' => 'useresponse', 'action' => 'view'), array('id' => Router::MATCH_ID));
        Router::map('useresponse_response_convert', 'useresponse/:id/convert', array('controller' => 'useresponse', 'action' => 'convert'), array('id' => Router::MATCH_ID));
        Router::map('useresponse_response_convert_continue', 'useresponse/:id/convert_continue', array('controller' => 'useresponse', 'action' => 'convert_continue'), array('id' => Router::MATCH_ID));
    }

// defineRoutes

    function defineHandlers() {
        EventsManager::listen('on_admin_panel', 'on_admin_panel');
        EventsManager::listen('on_system_permissions', 'on_system_permissions');
        EventsManager::listen('on_main_menu', 'on_main_menu');
    }

// defineHandlers

    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
        return lang('UseResponse');
    }

// getDisplayName

    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
        return lang('Web Connector of your activecollab environment with UseResponse - Customer Feedback Software');
    }

// getDescription

    /**
     * Return module uninstallation message
     *
     *
     * @return string
     */
    function getUninstallMessage() {
        return lang('');
    }

    function uninstall() {
        parent::uninstall();
    }

// getUninstallMessage
}