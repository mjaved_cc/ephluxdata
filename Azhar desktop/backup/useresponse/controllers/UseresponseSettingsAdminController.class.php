<?php

// We need admin controller
AngieApplication::useController('admin', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

/**
 * Admin vacation settings controller
 *
 * @package activeCollab.modules.hr_manager
 * @subpackage controllers
 */
class UseresponseSettingsAdminController extends AdminController {

    /**
     * Prepare controller
     */
    function __before() { 
        parent::__before();
    }

// __construct

    /**
     * Predefined items main page
     */
    function index() {
        if ($this->request->isAsyncCall()) {

            if ($this->request->isSubmitted()) {
                
                try {
                    $settings = $this->request->post('settings');

                    if ((trim($settings['useresponse_url']))){
                        $settings['useresponse_url'] = filter_var($settings['useresponse_url'], FILTER_VALIDATE_URL);
                        if (!$settings['useresponse_url']){
                            $this->response->operationFailed(array('message' => lang('Invalid URL')));
                        }
                    }

                    ConfigOptions::setValue('useresponse_url', $settings['useresponse_url']);
                    ConfigOptions::setValue('useresponse_api_key', trim($settings['useresponse_api_key']));

                    $this->response->ok();
                } catch (Exception $e) {
                    $this->response->exception($e);
                } // try
            } // if

            $this->response->assign(array('settings' => array(
                    'useresponse_url' => ConfigOptions::getValue('useresponse_url'),
                    'useresponse_api_key' => ConfigOptions::getValue('useresponse_api_key')
                    )));
        } else {
            $this->response->badRequest();
        }
    }

}