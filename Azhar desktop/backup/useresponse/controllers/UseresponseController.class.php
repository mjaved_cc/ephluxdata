<?php

// Build on top of backend controller
AngieApplication::useController('backend', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

/**
 * System level calendar
 *
 * @package activeCollab.modules.useresponse
 * @subpackage controllers
 */
class UseresponseController extends BackendController {

    /**
     * Active module
     *
     * @var string
     */
    protected $active_module = USERESPONSE_MODULE;
    protected $active_response_id = 0;

    /**
     * Prepare controller
     */
    function __before() {
        parent::__before();

        if (!$this->logged_user->getSystemPermission('can_use_useresponse_module')) {
            $this->response->forbidden();
        }
		
        $this->wireframe->tabs->clear();
        $this->wireframe->tabs->add('useresponse', lang('UseResponse'), Router::assemble('useresponse'), null, true);

        $this->wireframe->breadcrumbs->add('useresponse', lang('UseResponse'), Router::assemble('useresponse'));
        $this->wireframe->setCurrentMenuItem('useresponse');
    }

// __construct

    /**
     * UseResponse
     */
    function index() {

        $useresponse_url = ConfigOptions::getValue('useresponse_url');
        $useresponse_api_key = ConfigOptions::getValue('useresponse_api_key');

        
        if (!($useresponse_url && $useresponse_api_key)){
            $this->response->assign(array(
                'no_config_options' => true
            ));
            $responses = array();
            $statistic = array();
        }else{
            $useresponse = new Useresponse();
            $responses = $useresponse->findForObjectsList();
            $response_types = $useresponse->getResponseTypesMap();
            $statistic = $useresponse->getStatistic();
        }

        $this->wireframe->list_mode->enable();

        $this->active_project = Projects::findById(1);

//        $this->active_discussion = new Discussion();
//        $this->active_discussion->setProject($this->active_project);

        $this->response->assign(array(
            'active_response_id' => $this->active_response_id,
            'responses' => $responses,
            'response_types' => $response_types,
            'milestones' => Milestones::getIdNameMap($this->active_project),
            'categories' => Categories::getidNameMap($this->active_project, 'DiscussionCategory'),
            'read_statuses' => array(0 => lang('Unread'), 1 => lang('Read')),
            'manage_categories_url' => $this->active_project->availableCategories()->canManage($this->logged_user, 'DiscussionCategory') ? Router::assemble('project_discussion_categories', array('project_slug' => $this->active_project->getSlug())) : null,
            'can_manage_discussions' => Discussions::canManage($this->logged_user, $this->active_project),
            'in_archive' => false,
            'statistic' => $statistic
        ));

        $this->response->assign(array(
//            'active_project' => $this->active_project,
//            'acactive_projecttive_discussion' => $this->active_discussion,
//            'discussions_url' => 'zzz',
//            'manage_categories_url' => 'sdfsdf1',
        ));
    }

    function view() {

        $this->active_response_id = $this->request->getId();

        if ($this->request->isSingleCall()) {

            $useresponse = new Useresponse();
            $response = $useresponse->findResponseById($this->active_response_id);

            if (!empty($response)) {
                $this->wireframe->setPageTitle($response['title']);
            } else {
                $this->wireframe->setPageTitle(lang('No response found'));
            }

            $this->response->assign(array(
                'response' => $response
            ));
        } else {
            $this->__forward('index');
        }
    }

    function convert() {

        $response_id = $this->request->getId();

        $post = $this->request->post();

        if (!empty($post)) {
            $post = $this->request->post();

            $useresponse = new Useresponse();
            $response = $useresponse->findResponseById($response_id);

            if (!empty($response)) {
                $project = Projects::findById($post['project']);

                $task_data = array(
                    'visibility' => STATE_VISIBLE,
                    'name' => $response['title'],
                    'parent_id' => $post['project']
                );

                try {

                    DB::beginWork('Creating task');

                    $task = new Task();

                    $task->attachments()->attachUploadedFiles($this->logged_user);

                    $task->setBody($response['content']);

                    $task->setAttributes($task_data);
                    $task->setProject($project);
                    $task->setCreatedBy($this->logged_user);

                    $task->setState(STATE_VISIBLE);

                    $task->save();

                    DB::commit('Task created');

                    $this->response->respondWithData($task->getViewUrl());
                } catch (Exception $e) {
                    DB::rollback('Failed to create a task');

                    $this->response->exception($e);
                }
            } else {
                $this->response->operationFailed(array('message' => lang('No response found')));
            }
        } else {
            $this->response->assign(array(
                'projects' => Projects::findForQuickJump($this->logged_user),
                'avatar_size' => IProjectAvatarImplementation::SIZE_SMALL,
                'url' => Router::assemble('useresponse_response_convert', array('id' => $response_id)),
                'response_id' => $response_id
            ));
        }
    }

    function convert_continue() {

        $response_id = $this->request->getId();
        $useresponse = new Useresponse();
        $response = $useresponse->findResponseById($response_id);
        if (!empty($response)) {

            $project_id = $this->request->getId('project');
            $project = Projects::findById($project_id);
            $task = new Task();

            $task->setProject($project);

            $task_data = array(
                'visibility' => $project->getDefaultVisibility(),
                'milestone_id' => null,
                'category_id' => null,
                'name' => $response['title'],
                'body' => $response['content']
            );

            $this->response->assign(array(
                'active_project' => $project,
                'active_task' => $task,
                'task_data' => $task_data,
                'add_task_url' => Router::assemble('project_tasks_add', array('project_slug' => $project->getSlug()))
            ));
        }else{
            $this->response->operationFailed(array('message' => lang('No response found')));
        }
    }

}