<?php

/**
 * UseResponse
 *
 * @package activeCollab.modules.useresponse
 */
define('USERESPONSE_MODULE', 'useresponse');
define('USERESPONSE_MODULE_PATH', CUSTOM_PATH . '/modules/useresponse');

AngieApplication::setForAutoload(array(
    'URAPIConnector' => USERESPONSE_MODULE_PATH . '/models/api_connector/URAPIConnector.class.php',
    'URAPIResponse' => USERESPONSE_MODULE_PATH . '/models/api_connector/URAPIResponse.class.php',
    'URAPIXml' => USERESPONSE_MODULE_PATH . '/models/api_connector/URAPIXml.class.php',
    'Useresponse' => USERESPONSE_MODULE_PATH . '/models/Useresponse.class.php',
));