<?php
/**************************************************************
 * Copyright USWebStyle Inc., 2012. All Rights Reserved.
 **************************************************************
 * NOTICE:  This source code is the property of USWebStyle, Inc.
 * In no case shall you rent, lease, lend, redistribute
 * any of below source code to a 3rd party individual or entity.
 *
 * @category    URAPI
 * @subcategory URAPI_Lib
 * @package     URAPI_Lib_Xml
 * @copyright   USWebStyle Inc., 2012 (http://www.uswebstyle.com)
 * @license     http:/www.useresponse.com/licensing (Commercial)
 */

/**
 * XML to array converter class.
 *
 * @category    URAPI
 * @subcategory URAPI_Lib
 * @package     URAPI_Lib_Xml
 */
class URAPIXml
{
    /**
     * Converts xml string to php array
     *
     * @param  string $xmlstr
     *
     * @return array
     */
    public static function toArray($xmlstr)
    {
        $doc = new DOMDocument();
        $doc->loadXML($xmlstr);

        return self::_domnodeToArray($doc->documentElement);
    }

    /**
     * @param  DOMElement $node
     *
     * @return array
     */
    protected static function _domnodeToArray($node)
    {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                if (is_numeric($node->textContent) && 0 == $node->textContent) {
                    $output = '00';
                } else {
                    $output = trim($node->textContent);
                }
                break;
            case XML_ELEMENT_NODE:
                for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = self::_domnodeToArray($child);
                    if (isset($child->tagName)) {
                        $t = $child->tagName;
                        if (!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    } elseif ($v) {
                        if (is_numeric($v)) {
                            $output = (int) $v;
                        } else {
                            $output = (string) $v;
                        }
                    }
                }
                if (is_array($output)) {
                    if (empty($output) && !is_numeric($output)) {
                        $output = '';
                        break;
                    }
                    if ($node->attributes->length) {
                        $a = array();
                        foreach ($node->attributes as $attrName => $attrNode) {
                            $a[$attrName] = (string)$attrNode->value;
                        }
                        $output['@attributes'] = $a;
                    }
                    foreach ($output as $t => $v) {
                        if (is_array($v) && count($v) == 1 && $t != '@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }
        return $output;
    }
}