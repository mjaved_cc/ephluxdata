<?php
/**************************************************************
 * Copyright USWebStyle Inc., 2012. All Rights Reserved.
 **************************************************************
 * NOTICE:  This source code is the property of USWebStyle, Inc.
 * In no case shall you rent, lease, lend, redistribute
 * any of below source code to a 3rd party individual or entity.
 *
 * @category   URAPI
 * @package    URAPIResponse
 * @copyright  USWebStyle Inc., 2012 (http://www.uswebstyle.com)
 * @license    http:/www.useresponse.com/licensing (Commercial)
 */

/**
 * Useresponse API SDK Response class.
 *
 * @final
 * @category URAPI
 * @package  URAPIResponse
 */
final class URAPIResponse
{
    /**
     * Response class version
     */
    const VERSION = '1.0';

    /**
     * Connector
     *
     * @var URAPIConnector
     */
    private $_connector = null;

    /**
     * Response array returned by CURL
     *
     * @var array
     */
    private $_responseData = null;

    /**
     * Formatted response data array
     *
     * @var array
     */
    private $_formattedData = null;

    /**
     * Class constructor
     *
     * @param URAPIConnector $connector
     * @param array          $response
     */
    public function __construct (URAPIConnector $connector, $response)
    {
        $this->_connector = $connector;
        if (!is_array($response)) {
            $this->_responseError('Response data is not valid');
        }
        $this->_responseData = $response;
    }

    /**
     * Returns API Connector
     *
     * @return URAPIConnector
     * @throws Exception
     */
    public function getConnector ()
    {
        if (!$this->_connector instanceof URAPIConnector) {
            throw new Exception(
                'Invalid API connector. Must be instance of URAPIConnector'
            );
        }
        return $this->_connector;
    }

    /**
     * Returns fully qualified API request URL.
     *
     * @return string
     */
    public function getRequestedUrl ()
    {
        return $this->_responseData['url'];
    }

    /**
     * Returns true if error occured during request.
     *
     * @return bool
     */
    public function isError ()
    {
        return (0 != $this->_responseData['errno'] || 200 != $this->getCode());
    }

    /**
     * Returns error message.
     *
     * @return string
     */
    public function getError ()
    {
        if (!$this->isError()) return null;
        return ('' != $this->_responseData['errmsg'])
            ? $this->_responseData['errmsg']
            : current($this->_formatData());
    }

    /**
     * Returns response code.
     *
     * @return int
     */
    public function getCode ()
    {
        return (int )$this->_responseData['http_code'];
    }

    /**
     * Returns not formatted API response data.
     *
     * @return string
     */
    public function getRawData ()
    {
        return $this->_responseData['content'];
    }

    /**
     * Formats API response data into array.
     *
     * @return array
     */
    protected function _formatData ()
    {
        if (is_array($this->_formattedData)) return $this->_formattedData;

        $responseFormat = $this->getConnector()->getFormat();
        switch ($responseFormat) {
            case 'xml':
                $formatted = $this->_sanitizeFromXmlArray(
                    URAPIXml::toArray($this->_responseData['content'])
                );
                break;

            case 'json':
                $formatted = json_decode($this->_responseData['content'], true);
                break;
        }
        $this->_formattedData = $formatted;

        return $this->_formattedData;
    }

    /**
     * Corrects and typifies formatted response array.
     *
     * @param $array
     *
     * @return array
     */
    private function _sanitizeFromXmlArray ($array)
    {
        if (!is_array($array)) return $array;
        $unitemized = array();
        $token = 'item';
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $val = $this->_sanitizeFromXmlArray($val);
            }
            if ('items' == $key) {
                $val = $this->_sanitizeFromXmlArray($val);
            }
            if (preg_match('/^' . $token . '(\d+)/', $key)) {
                $unitemized[ltrim($key, $token)] = $val;
            } else {
                $unitemized[$key] = $val;
            }
        }
        if (empty($unitemized)) return $array;

        return $unitemized;
    }

    /**
     * Parse and returns response data as native php array.
     *
     * @return array
     */
    public function toArray ()
    {
        $data = $this->_formatData();
        if ($this->isError()) {
            $data = array('error' => current($data));
        }
        return $data;
    }

    /**
     * Error helper method.
     *
     * @param string $msg
     *
     * @throws Exception
     */
    private function _responseError ($msg)
    {
        throw new Exception("API response error: {$msg}");
    }
}