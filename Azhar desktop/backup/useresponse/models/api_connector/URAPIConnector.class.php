<?php
/**************************************************************
 * Copyright USWebStyle Inc., 2012. All Rights Reserved.
 **************************************************************
 * NOTICE:  This source code is the property of USWebStyle, Inc.
 * In no case shall you rent, lease, lend, redistribute
 * any of below source code to a 3rd party individual or entity.
 *
 * @category   URAPI
 * @package    URAPIConnector
 * @copyright  USWebStyle Inc., 2012 (http://www.uswebstyle.com)
 * @license    http:/www.useresponse.com/licensing (Commercial)
 */

/**
 * Useresponse API SDK Connector class.
 *
 * @final
 * @category URAPI
 * @package  URAPIConnector
 */
final class URAPIConnector
{
    /**
     * Connector class version
     */
    const VERSION = '1.0';

    /**
     * Default response data format
     */
    const DEFAULT_FORMAT = 'xml';

    /**
     * Options
     *
     * @var array
     */
    private $_options = array();

    /**
     * API URL
     *
     * @var string|null
     */
    protected $_apiUrl = null;

    /**
     * API key
     *
     * @var string|null
     */
    protected $_apiKey = null;

    /**
     * Response data format
     *
     * @var string|null
     */
    protected $_format = null;

    /**
     * Requested module
     *
     * @var string|null
     */
    protected $_requestModule = null;

    /**
     * Requested module
     *
     * @var string|null
     */
    protected $_requestMethod = null;

    /**
     * Request parameters
     *
     * @var array
     */
    protected $_requestParams = array();

    /**
     * Initializes object.
     *
     * @param string|array|null $options
     *        if string - sets API URl;
     *        if array  - sets options
     *        if null   - nothing will be set by __constructor()
     */
    public function __construct ($options = null)
    {
        if (is_string($options)) {
            $this->setApiUrl($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Returns current version of API SDK
     *
     * @return string
     */
    public function getVersion ()
    {
        return self::VERSION;
    }

    /**
     * Sets options.
     *
     * Array must be an assoc in the following format:
     *     'option' => 'value'
     *
     * @param   array $options
     * @example Available options:
     *          array (
     *              'api_url'         => 'http://{SITE_NAME}/api',
     *              'format'          => 'xml',
     *              'request_module'  => 'system',
     *              'request_method'  => 'responses',
     *              'request_params'  => array(
     *                  'status' => 'new',
     *                  'limit'  => 5
     *               )
     *          )
     *
     * @return URAPIConnector
     */
    public function setOptions ($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        if ($options instanceof ArrayAccess || is_array($options)) {
            foreach ($options as $name => $value) {
                $this->setOption($name, $value);
            }
        }

        return $this;
    }

    /**
     * Sets option.
     *
     * @param string     $name
     * @param string|int $value
     *
     * @return URAPIConnector
     */
    public function setOption ($name, $value)
    {
        switch (strtolower($name)) {
            case 'api_url':
                $this->setApiUrl($value);
                break;
            case 'format':
                $this->setFormat($value);
                break;
            case 'request_module':
                $this->setRequestModule($value);
                break;
            case 'request_method':
                $this->setRequestMethod($value);
                break;
            case 'request_params':
                $this->setRequestParams($value);
                break;
            default:
                if (!is_numeric($name)) {
                    $this->_options[strtolower($name)] = $value;
                }
                break;
        }

        return $this;
    }

    /**
     * Returns option value or null if not exists.
     *
     * @param string $name
     *
     * @return null
     */
    public function getOption ($name)
    {
        return ($this->hasOption($name)) ? $this->_options[$name] : null;
    }

    /**
     * Return true if option is exists otherwise false.
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasOption ($name)
    {
        return array_key_exists($name, $this->_options);
    }

    /**
     * Clears all options.
     *
     * @return URAPIConnector
     */
    public function clearOptions ()
    {
        $this->_options = array();
        return $this;
    }

    /**
     * Sets API url that should started with http:// or https://.
     *
     * @param  string $url
     *
     * @return URAPIConnector
     * @throws Exception
     */
    public function setApiUrl ($url)
    {
        if (!preg_match("^(http(s?))://^", $url)) {
            throw new Exception('Invalid argument: $url is not valid API url');
        }
        $this->_apiUrl = rtrim($url, '/');
        return $this;
    }

    /**
     * Returns API url.
     *
     * @return string|null
     */
    public function getApiUrl ()
    {
        return $this->_apiUrl;
    }

    /**
     * Sets response data format.
     *
     * Currently supported formats are: "xml" and "json".
     *
     * @param string $format
     *
     * @return URAPIConnector
     * @throws Exception
     */
    public function setFormat ($format)
    {
        $valid = array('xml', 'json');
        if (!in_array($format, $valid)) {
            throw new Exception(
                'Invalid format has provided. Valid formats are ' . implode(', ', $valid)
            );
        }
        $this->_format = $format;
        return $this;
    }

    /**
     * Returns response data format.
     *
     * @return null|string
     */
    public function getFormat ()
    {
        return (null !== $this->_format) ? $this-> _format : self::DEFAULT_FORMAT;
    }

    /**
     * Defines API key.
     *
     * @param string $key
     *
     * @return URAPIConnector
     */
    public function setApiKey ($key)
    {
        $this->_apiKey = (string) $key;
        return $this;
    }

    /**
     * Returns API key.
     *
     * @return string|null
     */
    public function getApiKey()
    {
        return $this->_apiKey;
    }

    /**
     * Defines requested module.
     *
     * @param string $module
     *
     * @return URAPIConnector
     */
    public function setRequestModule ($module)
    {
        $this->_requestModule = (string) $module;
        return $this;
    }

    /**
     * Returns requested module.
     *
     * @return string|null
     */
    public function getRequestModule ()
    {
        return $this->_requestModule;
    }

    /**
     * Sets requested module method.
     *
     * @param string $method
     *
     * @return URAPIConnector
     */
    public function setRequestMethod ($method)
    {
        $this->_requestMethod = (string) $method;
        return $this;
    }

    /**
     * Returns requested module method.
     *
     * @return string null
     */
    public function getRequestMethod ()
    {
        return $this->_requestMethod;
    }

    /**
     * Sets additional request parameters.
     *
     * Must be an assoc array in the following format:
     *     'option' => 'value'
     *
     * @param array|ArrayObject $params
     *
     * @return URAPIConnector
     */
    public function setRequestParams ($params)
    {
        if (!is_array($params)) {
            throw new Exception('Invalid argument: $params must be an array');
        }
        $this->clearRequestParams();
        foreach ($params as $name => $value) {
            if (is_numeric($name)) continue;
            $this->addRequestParam($name, $value);
        }
        return $this;
    }

    /**
     * Sets additional request parameter.
     *
     * @param string     $name
     * @param string|int $value
     *
     * @return URAPIConnector
     */
    public function addRequestParam ($name, $value)
    {
        $this->_requestParams[$name] = $value;
        return $this;
    }

    /**
     * Returns request parameter by name.
     *
     * @param string $name
     *
     * @return string|int|null
     */
    public function getRequestParam ($name)
    {
        return (isset($this->_requestParams[$name])) ? $this->_requestParams[$name] : null;
    }

    /**
     * Returns all additional request parameters.
     *
     * @return array
     */
    public function getAllRequestParams ()
    {
        return $this->_requestParams;
    }

    /**
     * Clears all additional request parameters.
     *
     * @return URAPIConnector
     */
    public function clearRequestParams ()
    {
        $this->_requestParams = array();
        return $this;
    }

    /**
     * Implements API request to specified API url.
     *
     * As result returns URAPIResponse object with parsed response data.
     * NOTE: Before makeRequest() calling you must specify:
     *       API url, API key, requested module, module method
     *       and required request parameters.
     *
     * @param string|null  $module  Optional, otherwise takes already set
     * @param string|null  $method  Optional, otherwise takes already set
     * @param array        $params  Optional, if an empty array then takes already set
     *
     * @return URAPIResponse
     */
    public function makeRequest ($module = null, $method = null, array $params = array())
    {
        if (null !== $module)  $this->setRequestModule($module);
        if (null !== $method)  $this->setRequestMethod($method);
        if (is_array($params) && !empty($params)) $this->setRequestParams($params);

        $url         = $this->_assembleUrl();
        $responseRaw = $this->_request($url);

        return new URAPIResponse($this, $responseRaw);
    }

    /**
     * Assembles requested URL.
     *
     * @return string
     */
    private function _assembleUrl ()
    {
        if (null == $requestModule = $this->getRequestModule()) {
            $this->_requestError('Request module is not defined');
        }
        if (null == $requestMethod = $this->getRequestMethod()) {
            $this->_requestError('Request method is not defined');
        }
        if (null == $apiUrl = $this->getApiUrl()) {
            $this->_requestError('API URL is not defined');
        }
        if (null == $apiKey = $this->getApiKey()) {
            $this->_requestError('API key is not defined');
        }

        $requestUrl = "{$apiUrl}/{$this->getFormat()}/{$requestModule}/{$requestMethod}";
        if (null != $requestParams = $this->_assembleRequestParams()) {
            $requestUrl .= '/' . $requestParams;
        }
        $requestUrl .= "/api_key/{$apiKey}";

        return $requestUrl;
    }

    /**
     * Assembles request parameters.
     *
     * @return null|string
     */
    private function _assembleRequestParams ()
    {
        $params = array();
        foreach ($this->getAllRequestParams() as $pName => $pValue) {
            $params[] = "{$pName}/{$pValue}";
        }
        if (empty($params)) return null;
        return implode('/', $params);
    }

    /**
     * Makes API request using CURL.
     *
     * @param string $url  Requested API URL
     *
     * @return array
     */
    private function _request ($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_USERAGENT      => $this->_getUserAgent(),
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_MAXREDIRS      => 2
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errMsg  = curl_error($ch);
        $header  = curl_getinfo($ch);
        curl_close($ch);

        $header['errno']   = $err;
        $header['errmsg']  = $errMsg;
        $header['content'] = $content;
        return $header;
    }

    /**
     * Returns user agent for API request.
     *
     * @return string
     */
    protected function _getUserAgent ()
    {
        return 'UseResponse-API-SDK-v.' . self::VERSION;
    }

    /**
     * Error helper method.
     *
     * @param string $msg
     *
     * @throws Exception
     */
    private function _requestError ($msg)
    {
        throw new Exception("API request error: {$msg}");
    }
}