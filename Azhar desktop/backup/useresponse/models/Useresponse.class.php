<?php

class Useresponse {

    private $_connector;
    private $_default_request_module = 'system';
    private $_default_request_method = 'responses';
    private $_responses;
    private $_response_types = array(
        1 => 'Idea',
        2 => 'Problem',
        3 => 'Question',
        4 => 'Thanks'
    );

    function __construct() {
        $this->_connector = new URAPIConnector(ConfigOptions::getValue('useresponse_url'));
        $this->_connector->setApiKey(ConfigOptions::getValue('useresponse_api_key'))
                ->setFormat('xml');
    }

    function getConnector() {
        return $this->_connector;
    }

    function getResponses() {

        $this->setRequestModule('system');
        $this->setRequestMethod('responses');

        $responses = $this->_connector->setRequestModule($this->_default_request_module)
                ->setRequestMethod($this->_default_request_method)
                ->setRequestParams(array(
                    'limit' => 50
                ))
                ->makeRequest()
                ->toArray();

        $this->_responses = $responses;

        return $this->_responses;
    }

    function getStatistic() {

        $this->setRequestModule('system');
        $this->setRequestMethod('statistic');

        $responses = $this->_connector->setRequestModule($this->_default_request_module)
                ->setRequestMethod($this->_default_request_method)
                ->makeRequest()
                ->toArray();

        $this->_responses = $responses;

        return $this->_responses;
    }

    function findForObjectsList() {
        $this->getResponses();
        $responses_list = array();
        if (!empty($this->_responses['items'])) {
            foreach ($this->_responses['items'] as $key => $response) {
                $responses_list[$key] = array(
                    'id' => $response['id'],
                    'name' => $response['title'],
                    'category_id' => 0,
                    'milestone_id' => 0,
                    'icon' => AngieApplication::getImageUrl('icons/16x16/' . strtolower($response['type']) . '.png', USERESPONSE_MODULE),
                    'is_read' => 1,
                    'is_pinned' => 0,
                    'permalink' => Router::assemble('useresponse_view', array('id' => $response['id'])),
                    'is_favorite' => false,
                    'is_archived' => 0,
                    'visibility' => 1,
                    'type' => $response['type'],
                    'type_id' => array_search($response['type'], $this->_response_types),
                    'comments_count' => $response['comments_count'],
                    'votes' => $response['votes']['all']
                );
            }
        }

        return $responses_list;
    }

    function findResponseById($id) {
        $this->setRequestModule('system');
        $this->setRequestMethod('show-response');

        $response = $this->_connector->setRequestModule($this->_default_request_module)
                ->setRequestMethod($this->_default_request_method)
                ->setRequestParams(array(
                    'response_id' => $id,
                ))
                ->makeRequest()
                ->toArray();

        if (!empty($response)) {
            $response_date = new DateValue($response['created_at']);
            $response['created_at_formatted'] = $response_date->formatForUser();
        }

        $this->_responses = $response;

        return $this->_responses;
    }

    function getResponseTypesMap() {
        return $this->_response_types;
    }

    function setRequestModule($request_module) {
        $this->_default_request_module = $request_module;
    }

    function getRequestModule() {
        return $this->_default_request_module;
    }

    function setRequestMethod($request_method) {
        $this->_default_request_method = $request_method;
    }

    function getRequestMethod() {
        return $this->_default_request_method;
    }

}