<?php

/**
 * Model options for hr manager module
 *
 * @package activeCollab.modules.hr_manager
 * @subpackage resources
 */
$this->setTableOptions(array('vacation'), array('module' => 'hr_manager', 'object_extends' => 'ApplicationObject')
);