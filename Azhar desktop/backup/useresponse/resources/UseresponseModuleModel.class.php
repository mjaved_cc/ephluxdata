<?php

// Include applicaiton specific model base
require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

/**
 * HR Manager module model definition
 *
 * @package activeCollab.modules.hr_manager
 * @subpackage models
 */
class UseresponseModuleModel extends ActiveCollabModuleModel {

    /**
     * Construct auto login module model definition
     *
     * @param TodosModule $parent
     */
    function __construct(TodosModule $parent) {
        parent::__construct($parent);
    }

// __construct

    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {

        $this->setConfigOptionValue('useresponse_url');
        $this->setConfigOptionValue('useresponse_api_key');

        $roles = Roles::find();
        foreach ($roles as $role) {
            if ($role->isAdministrator()) {
                $permissions = $role->getPermissions();
                $permissions['can_use_useresponse_module'] = 1;
                $role->setPermissions($permissions);
                $role->save();
            }
        }

        parent::loadInitialData($environment);
    }

// loadInitialData
}