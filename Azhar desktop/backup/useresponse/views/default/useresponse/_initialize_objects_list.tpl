<script type="text/javascript">

  $('#responses').each(function() {
    var wrapper = $(this);

    var categories_map = {$categories|map nofilter};
    var milestones_map = {$milestones|map nofilter};
    var read_status_map = {$read_statuses|map nofilter};
    var response_types_map = {$response_types|map nofilter};

    var type_filter = [];
    type_filter.push({ label : App.lang('All types'), value : '', 'default' : true, icon : App.Wireframe.Utils.imageUrl('objects-list/all-assets.png', 'files')});
    App.each(response_types_map, function (type, title) {
      type_filter.push({
        'label' : title,
        'value' : type,
        //'icon' : types_detailed[type].icon
        'icon' : App.Wireframe.Utils.imageUrl('icons/16x16/' + title.toLowerCase() + '.png', 'useresponse')
      });
    });

    var init_options = {
      'id' : 'useresponses_list',
      'refresh_url' : '',
      'items' : {$responses|json nofilter},
      //'required_fields' : ['id', 'name', 'category_id', 'milestone_id', 'icon', 'is_read', 'is_pinned', 'permalink', 'is_archived'],
      'required_fields' : ['id', 'name', 'category_id', 'milestone_id', 'icon', 'is_read', 'is_pinned', 'permalink', 'is_archived'],
      'requirements' : {},
      'objects_type' : 'discussions',
      'events' : App.standardObjectsListEvents(),
      'multi_title' : App.lang(':num Responses Selected'),
      'print_url' : '',
      'prepare_item' : function (item) {
        var result = {
          'id'            : item['id'],
          'name'          : item['name'],
          'icon'          : item['icon'],
          'is_read'       : item['is_read'],
          'is_pinned'     : item['is_pinned'],
          'permalink'     : item['permalink'],
          'is_favorite'   : item['is_favorite'],
          'is_archived'   : item['state'] == '2' ? '1' : '0',
          'is_trashed'    : item['state'] == '1' ? 1 : 0,
          'visibility'    : item['visibility']
        };

        if (typeof(item['category']) == 'undefined') {
          result['category_id'] = item['category_id'];
        } else {
          result['category_id'] = item['category'] ? item['category']['id'] : 0;
        } // if

        if(typeof(item['milestone']) == 'undefined') {
          result['milestone_id'] = item['milestone_id'];
        } else {
          result['milestone_id'] = item['milestone'] ? item['milestone']['id'] : 0;
        } // if

        return result;
      },

      'render_item' : function (item) {
        return '<td class="icon"><img src="' + item['icon'] + '" alt=""></td><td class="name" is_pinned="' + (item['is_pinned'] ? 1 : 0) + '">' + item['name'].clean() + App.Wireframe.Utils.renderVisibilityIndicator(item['visibility']) + '</td><td class="response_options"><span class="response_comments_count" title="Votes count">' + item['votes'] + '</span></td>';
      },

      'grouping' : [{
        'label' : App.lang("Don't group"),
        'property' : '', icon : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      },
//      {
//        'label' : App.lang('By Category'),
//        'property' : 'category_id',
//        'map' : categories_map,
//        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-category.png', 'categories')
//      },
//      {
//        'label' : App.lang('By Milestone'),
//        'property' : 'milestone_id',
//        'map' : milestones_map ,
//        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-milestones.png', 'system'),
//        'uncategorized_label' : App.lang('Unknown Milestone')
//      },
//      {
//        'label' : App.lang('By Read Status'),
//       'property' : 'is_read',
//        'map' : read_status_map ,
//        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-status.png', 'environment'),
//        //'default' : true,
//        'uncategorized_label' : App.lang('Unread')
//      },
      {
        'label' : App.lang('By Response Type'),
        'property' : 'type_id',
        'map' : response_types_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-category.png', 'categories'),
        'default' : true
        //'uncategorized_label' : App.lang('Unread')
      }],
      'filtering' : [{
        'label' : App.lang('Type'),
        'property'  : 'type_id',
        'values'  : type_filter
      }]
    };

    if ({$in_archive|json nofilter}) {
      init_options.requirements.is_archived = 1;
    } else {
      init_options.requirements.is_archived = 0;
    } // if

    wrapper.objectsList(init_options);


  {if $active_response_id}
    wrapper.objectsList('load_item', {$active_response_id}, '{assemble route=useresponse_view id=$active_response_id}'); // Pre select item if this is permalink
  {/if}
  });
</script>