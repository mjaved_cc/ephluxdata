{title}Converter{/title}
<form action="{$url}" id="converter_form" method="post">
    <div class="content_stack_element" >
        <div class="content_stack_element_info">
            <p><strong>{lang}Select Project{/lang}</strong></p>
        </div>
        <div class="content_stack_element_body">

            <div class="control_holder projects_list">
                <ul class="quick_jump_main_list">
                    {foreach from=$projects item=project name=projects}
                    <li class="redirect_to_main_item{if $smarty.foreach.projects.first} redirect_to_selected{/if}">
                        <label><input{if ($smarty.foreach.projects.first)} checked="checked"{/if} type="radio" value="{$project->getId()}" name="project" class="redirect_to_main_item_check"><img src="{$project->avatar()->getUrl($avatar_size)}" alt=""><span>{$project->getName()}</span></label>
                    </li>
                    {/foreach}
                </ul>
                {if (!$projects)}
                    <p class="empty_page">{lang}There are no active projects{/lang}</p>
                {/if}
            </div>

            <!--div class="actions_right">
                <div class="control_holder control_holder_inline">
                    <label for="converter_direction" class="main_label">{lang}Select Action:{/lang}</label>
                    <select id="converter_direction" name="converter_direction">
                        <option value="convert_task_to_discussion">{lang}Task -&gt; Discussion{/lang}</option>
                        <option value="convert_task_to_page">{lang}Task -&gt; Page{/lang}</option>
                        <option value="convert_discussion_to_task">{lang}Discussion -&gt; Task{/lang}</option>
                        <option value="convert_discussion_to_page">{lang}Discussion -&gt; Page{/lang}</option>
                        <option value="convert_page_to_task">{lang}Page -&gt; Task{/lang}</option>
                        <option value="convert_page_to_discussion">{lang}Page -&gt; Discussion{/lang}</option>
                    </select>
                </div>

                <div class="control_holder converter_objects_direction">
                    <label for="converter_task" class="main_label">{lang}Select Task:{/lang}</label>
                    <select id="converter_task" name="converter_task">
                    </select>
                </div>

                <div style="display: none" class="control_holder converter_objects_direction">
                    <label for="converter_discussion" class="main_label">{lang}Select Discussion:{/lang}</label>
                    <select id="converter_discussion" name="converter_discussion">
                    </select>
                </div>

                <div style="display: none" class="control_holder converter_objects_direction">
                    <label for="converter_page" class="main_label">{lang}Select Page:{/lang}</label>
                    <select id="converter_page" name="converter_page">
                    </select>
                </div>
                
                <div style="display: none" class="control_holder converter_objects_direction">
                    <label for="notebook_select" class="main_label">{lang}Select Notebook:{/lang}</label>
                    <select id="converter_notebook" name="notebook_select">                              
                    </select>
                </div>
            </div-->
        </div>
    </div>

    <a style="display: none" id="response_continue_link" href="{assemble route=useresponse_response_convert_continue id=$response_id}"></a>

    {wrap_buttons}
        {submit id="response_continue"}Continue{/submit}
    {/wrap_buttons}

</form>

<script type="text/javascript">
    $('document').ready(function(){

        App.Wireframe.Events.bind('task_created', function(event, data){
            App.widgets.FlyoutDialog.front().close();
            App.Wireframe.Content.setFromUrl(data.permalink);
        });

        $('#response_continue_link').click(function(){
            var continue_link = $(this).attr('href');
            continue_link = App.extendUrl(continue_link, {
                project : $('input[name="project"]:checked').val()
            });
            $(this).attr('href', continue_link);

            return false;
        });

        $('#response_continue_link').flyoutForm({
              //width : 850,
              //success_message : App.lang('New task created'),
              //success: function(response){
              //    App.Wireframe.Content.setFromUrl(response);
              //}
              success_event : 'task_created'
        });

        

        $('#response_continue').click(function(){
            $('#response_continue_link').click();
            return false;
        });

        $('.redirect_to_main_item_check').click(function(){
            $(this).parent().parent().parent().find('.redirect_to_selected').removeClass('redirect_to_selected');
            $(this).parent().parent().addClass('redirect_to_selected');
        });
    });
    
</script>