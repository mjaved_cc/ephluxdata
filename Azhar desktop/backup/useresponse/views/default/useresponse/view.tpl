{if $response}
<div class="object_wrapper"><div class="wireframe_content_wrapper first">
        <div id="" class="object_inspector with_actions with_bars" object_id="2" object_class="Response">
            <div class="head">
                <div class="bars">
                    <div class="bar" style="display: none;">
                    </div>
                </div>
                <table cellspacing="0" class="inspector_table">
                        <tr>
                            <td class="properties">
                                <div class="property">
                                    <div class="label">{lang}Created{/lang}</div>
                                    <div class="content">
                                        {lang created_at=$response.created_at_formatted user_email=$response.author.email author=$response.author.name}:created_at by <a href="mailto::user_email">:author</a>{/lang}
                                    </div>
                                </div>
                                <div class="property">
                                    <div class="label">{lang}Response Type{/lang}</div>
                                    <div class="content">
                                        {$response.type}
                                    </div>
                                </div>
                                <div class="property">
                                    <div class="label">{lang}Response Status{/lang}</div>
                                    <div class="content">
                                        {$response.status}
                                    </div>
                                </div>
                                    <div class="property">
                                    <div class="label">{lang}Comments{/lang}</div>
                                    <div class="content">
                                        {$response.comments_count}
                                    </div>
                                </div>
                                    <div class="property">
                                    <div class="label">{lang}Votes{/lang}</div>
                                    <div class="content">
                                        {$response.votes.all}
                                    </div>
                                </div>
                            </td>
                            <!--td class="indicators">
                                <ul>
                                    <li id="discussion_2_details_inspector_indicator_visibility" style="display: none;">
                                        <span class="content" check_string="public"></span>
                                    </li>
                                    <li id="discussion_2_details_inspector_indicator_favorite">
                                        <span class="content" check_string="is_favorite_false">
                                            <a href="http://ac/people/1/users/1/favorites/add?object_type=Discussion&amp;object_id=2" title="Object is not on your list of favorite objects. Click to add it.">
                                                <img alt="" src="http://ac/public/assets/images/favorites/default/icons/12x12/favorite-off.png">
                                            </a>
                                        </span>
                                    </li>
                                    <li id="discussion_2_details_inspector_indicator_subscribed">
                                        <span class="content" check_string="is_subscribed_true">
                                            <a href="http://ac/projects/project-1/discussions/2/unsubscribe?user_id=1" title="You are subscribed to receive email notifications regarding this discussion. Click to unsubscribe!">
                                                <img alt="" src="http://ac/public/assets/images/subscriptions/default/icons/16x16/object-subscription-active.png">
                                            </a>
                                        </span>
                                    </li>
                                    <li id="discussion_2_details_inspector_indicator_sharing" style="display: none;">
                                        <span class="content" check_string="is_not_shared"></span>
                                    </li>
                                </ul>
                            </td-->
                        </tr>
                </table>
                <ul class="actions">
                    <!--li id="inspector_object_action_archive">
                        <a title="Move to Archive" href="http://ac/projects/project-1/discussions/2/archive">Move to Archive</a>
                    </li>
                    <li id="inspector_object_action_pin_unpin">
                        <a href="http://ac/projects/project-1/discussions/2/pin" is_on="0">Pin</a>
                    </li>
                    <li id="inspector_object_action_edit">
                        <a title="Edit" href="http://ac/projects/project-1/discussions/2/edit">Edit</a>
                    </li-->
                    <li id="response_to_task">
                        <a title="{lang}Convert to Task{/lang}" href="{assemble route=useresponse_response_convert id=$response.id}">{lang}Convert to Task{/lang}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="object_content">
        <div class="wireframe_content_wrapper">
    <div class="object_body with_shadow">
      <div class="object_content_wrapper">
          <div class="object_body_content formatted_content">
              {$response.content nofilter}
          </div>
      </div>
    </div>
  </div>
  </div>
</div>

<script type="text/javascript">
    $('document').ready(function(){
        $('#response_to_task a').flyoutForm({
              width : 520,
              success_message : App.lang('New task created'),
              success: function(response){
                  App.Wireframe.Content.setFromUrl(response);
              }
        });
    });
</script>
{else}
    <p class="empty_slate">{lang}No response found{/lang}</p>
{/if}