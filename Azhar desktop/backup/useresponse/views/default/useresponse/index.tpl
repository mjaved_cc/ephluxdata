{title}Community{/title}
{add_bread_crumb}{lang}Overview{/lang}{/add_bread_crumb}

<div id="responses">
    <div class="empty_content">

        <!--div class="objects_list_title">{lang}Statistics{/lang}</div>
        <div class="objects_list_icon"><img src="{image_url name='full_logo.png' module=useresponse}" alt=""/></div-->

        {if ($no_config_options)}
        <p class="empty_slate">{lang}Please enter in Settings valid API Key and API URL of installed UseResponse!{/lang}</p>
        {else}

        <div id="project_at_a_glance">

            <div class="" id="project_at_a_glance_card">
                <div class="project_at_a_glance_header">

                    <table>
                        <tr>
                            <td class="logo">
                                <img src="{image_url name='full_logo.png' module=useresponse}" alt="{lang}UseResponse{/lang}"/>
                            </td>
                            <td align="right" class="main">
                                <h2>{lang}Statistics{/lang}</h2>
                            </td>
                            <!--td class="progress">  <div class="project_progress">
                                    <div class="progress_wrapper">
                                        <div style="width: 12%" class="progress"><span>12%</span></div>
                                        <div class="progress_label">12%</div>
                                    </div>
                                    <p><strong>2</strong> of <strong>16</strong> tasks completed</p>
                                </div>
                            </td-->
                        </tr>
                    </table>
                </div>

                <table class="project_at_a_glance_other responses_stats">
                    <tr>
                        <td class="responses_img">
                            <img src="{image_url name='icons/24x24/idea.png' module=useresponse}" title="{lang}Ideas{/lang}" alt="{lang}Ideas{/lang}"/>
                        </td>
                        <td class="responses_label">
                            <span>{lang}Ideas{/lang}</span>
                        </td>
                        <td class="responses_count">
                            {$statistic.ideas}
                        </td>
                    </tr>
                    <tr class="resp_col">
                        <td class="responses_img">
                            <img src="{image_url name='icons/24x24/problem.png' module=useresponse}" title="{lang}Ideas{/lang}" alt="{lang}Ideas{/lang}"/>
                        </td>
                        <td class="responses_label">
                            <span>{lang}Problems{/lang}</span>
                        </td>
                        <td class="responses_count">
                            {$statistic.problems}
                        </td>
                    </tr>
                    <tr>
                        <td class="responses_img">
                            <img src="{image_url name='icons/24x24/question.png' module=useresponse}" title="{lang}Ideas{/lang}" alt="{lang}Ideas{/lang}"/>
                        </td>
                        <td class="responses_label">
                            <span>{lang}Questions{/lang}</span>
                        </td>
                        <td class="responses_count">
                            {$statistic.questions}
                        </td>
                    </tr>
                    <tr class="resp_col">
                        <td class="responses_img">
                            <img src="{image_url name='icons/24x24/thanks.png' module=useresponse}" title="{lang}Ideas{/lang}" alt="{lang}Ideas{/lang}"/>
                        </td>
                        <td class="responses_label">
                            <span>{lang}Thanks{/lang}</span>
                        </td>
                        <td class="responses_count">
                            {$statistic.thanks}
                        </td>
                    </tr>
                    <tr>
                        <td class="responses_img">
                            <img width="24" height="24" src="{image_url name='icons/32x32/user.png' module=system}" title="{lang}Ideas{/lang}" alt="{lang}Ideas{/lang}"/>
                        </td>
                        <td class="responses_label">
                            <span>{lang}Users{/lang}</span>
                        </td>
                        <td class="responses_count">
                            {$statistic.users}
                        </td>
                    </tr>
                </table>

                <!--div class="project_at_a_glance_people">
                    <ul>
                        <li><a title="admin" href="http://ac/people/1/users/1"><img src="http://ac/avatars/default.40x40.png"></a></li>
                        <li><a title="user1 user1" href="http://ac/people/1/users/3"><img src="http://ac/avatars/default.40x40.png"></a></li>
                        <li><a title="user2 user2" href="http://ac/people/1/users/4"><img src="http://ac/avatars/default.40x40.png"></a></li>
                        <li><a title="user3 user3" href="http://ac/people/1/users/5"><img src="http://ac/avatars/default.40x40.png"></a></li>
                        <li><a title="user user" href="http://ac/people/1/users/2"><img src="http://ac/avatars/default.40x40.png"></a></li>
                    </ul>

                    <div class="project_at_a_glance_people_manage_people">
                        <a href="http://ac/projects/new-project/people">Manage People on this Project</a>
                    </div>
                </div>

                <div class="section_button_wrapper brief_action_button">
                    <a href="http://ac/projects/new-project" class="section_button"><span><img src="http://ac/public/assets/images/environment/default/icons/16x16/go-to-project.png">Go to the Project</span></a>
                </div-->
            </div>



            <!--ul class="project_at_glance_links">
                <li id="show_me_assignments">
                    <a id="" href="http://ac/projects/new-project/user-tasks">My Assignments</a>
                </li>
                <li id="show_me_subscriptions">
                    <a id="" href="http://ac/projects/new-project/user-subscriptions">My Subscriptions</a>
                </li>
                <li id="show_me_ical">
                    <a id="ActiveCollab_element_1349874610_1619782716_button_409454987" href="http://ac/projects/new-project/ical-subscribe">iCalendar Feed</a>
                </li>
                <li id="show_me_rss">
                    <a id="" target="_blank" href="http://ac/projects/new-project/activity-log/rss?auth_api_token=1-8vmdkpsket5hTW34TuI7knXnPj6oqwX7MIUWV7MV">RSS Feed</a>
                </li>
            </ul-->
        </div>

        {/if}


        <!--div class="objects_list_details_actions">
            <ul>
                {*if $add_discussion_url}<li><a href="{assemble route='project_discussions_add' project_slug=$active_project->getSlug()}" id="new_discussion">{lang}New Discussion{/lang}</a></li>{/if}
                {if $can_manage_discussions}<li><a href="{assemble route='project_discussions_archive' project_slug=$active_project->getSlug()}">{lang}Archive{/lang}</a></li>{/if}
                {if $manage_categories_url}<li><a href="{$manage_categories_url}" class="manage_objects_list_categories" title="{lang}Manage Discussion Categories{/lang}">{lang}Manage Categories{/lang}</a></li>{/if*}
            </ul>
        </div-->

        

        {if (!$no_config_options)}
        <div class="object_lists_details_tips" style="position: inherit">
            <h3>{lang}Tips{/lang}:</h3>
            <ul>
                <li>{lang}To select a response and load its details, please click on it in the list on the left{/lang}</li>
                <!--li>{lang}It is possible to select multiple discussions at the same time. Just hold Ctrl key on your keyboard and click on all the discussions that you want to select{/lang}</li-->
            </ul>
        </div>
        {/if}
    </div>
</div>

{include file=get_view_path('_initialize_objects_list', 'useresponse', $smarty.const.USERESPONSE_MODULE)}