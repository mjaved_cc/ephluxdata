{title}{lang}Useresponse Settings{/lang}{/title}

<div id="crm_settings">
  {form action=Router::assemble('useresponse_settings')}
  
     <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}UseResponse API URL{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
        {wrap field='settings[crm_mailbox_sender_name]'}
          {text_field name='settings[useresponse_url]' value=$settings.useresponse_url label='Enter your UseResponse API URL'}
        {/wrap}
        </div>
     </div>

     <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3>{lang}UseResponse API Key{/lang}</h3>
        </div>
        <div class="content_stack_element_body">
        {wrap field='settings[crm_mailbox_sender_name]'}
          {text_field name='settings[useresponse_api_key]' value=$settings.useresponse_api_key label='Enter the API Key provided by your UseResponse installation'}
        {/wrap}
        </div>
     </div>

    {wrap_buttons}
      {submit}Save Changes{/submit}
    {/wrap_buttons}
    {/form}
</div>