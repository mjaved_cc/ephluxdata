<?php

/**
 * UseResponse module on_main_menu event handler
 *
 * @package activeCollab.modules.useResponse
 * @subpackage handlers
 */

/**
 * Add options to main menu
 *
 * @param MainMenu $menu
 * @param User $user
 */
function useresponse_handle_on_main_menu(MainMenu &$menu, User &$user) {
    if ($user->getSystemPermission('can_use_useresponse_module')) {
        $menu->addBefore('useresponse', lang('Community'), Router::assemble('useresponse'), AngieApplication::getImageUrl('module.png', USERESPONSE_MODULE, AngieApplication::INTERFACE_DEFAULT), null, 'admin');
    }
}

// useresponse_handle_on_main_menu