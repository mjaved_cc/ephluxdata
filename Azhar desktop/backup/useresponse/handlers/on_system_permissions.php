<?php

/**
 * UseResponse on_system_permissions handler
 *
 * @package activeCollab.modules.useresponse
 * @subpackage handlers
 */

/**
 * Handle on_system_permissions
 *
 * @param NamedList $permissions
 */
function useresponse_handle_on_system_permissions(NamedList &$permissions) {
    $permissions->add('can_use_useresponse_module', array(
        'name' => lang('Use UseResponse'),
        'description' => lang('Set to Yes to let people use UseResponse module'),
        'depends_on' => 'has_system_access',
    ));
}

// status_handle_on_system_permissions