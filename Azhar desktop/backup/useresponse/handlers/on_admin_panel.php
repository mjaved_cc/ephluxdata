<?php

/**
 * on_admin_panel event handler
 * 
 * @package activeCollab.modules.useresponse
 * @subpackage handlers
 */

/**
 * Handle on_admin_panel event
 * 
 * @param AdminPanel $admin_panel
 */
function useresponse_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToOther('useresponse_settings', lang('UseResponse Settings'), Router::assemble('useresponse_settings'), AngieApplication::getImageUrl('module.png', USERESPONSE_MODULE), array('onclick' => new FlyoutFormCallback(array('title' => lang('UseResponse Settings')))));
}

// converter_handle_on_admin_panel