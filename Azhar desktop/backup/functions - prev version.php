<?php
##################################################################
# AVIA FRAMEWORK by Kriesi

# this include calls a file that automatically includes all 
# the files within the folder framework and therefore makes 
# all functions and classes available for later use

/* 
error_reporting(E_ALL);

ini_set('display_errors', '1');
*/

function plugin_form(){

    wp_enqueue_script( 'my-register.js', get_bloginfo('template_directory') . "/js/my-register.js", array( 'jquery' ) );
	
}

function check_fields($login, $email, $errors) {
	global $firstname, $lastname;
	if ($_POST['first'] == '') {
		$errors->add('empty_realname', "<strong>ERROR</strong>: Please Enter in First Name");
	} else {
		$firstname = $_POST['first'];
	}
	if ($_POST['last'] == '') {
		$errors->add('empty_realname', "<strong>ERROR</strong>: Please Enter in Last Name");
	} else {
		$firstname = $_POST['last'];
	}
}

// This is where the magiv happens
function register_extra_fields($user_id, $password="", $meta=array())  {

// Gotta put all the info into an array
$userdata = array();
$userdata['ID'] = $user_id;

// First name
$userdata['first_name'] = $_POST['first'];

// Last Name
$userdata['last_name'] = $_POST['last'];

// Enters into DB
wp_update_user($userdata);

update_user_meta( $user_id, 'user_type', $_POST['user_type'] );

// This is for custom meta data "gender" is the custom key and M is the value
// update_usermeta($user_id, 'gender','M');

}


add_action('register_post','check_fields',10,3);

// This inserts the data
add_action('user_register', 'register_extra_fields');
add_action('register_form','plugin_form');

function myAjax(){
	global $wpdb;
    //get data from our ajax() call
    $product_id = $_POST['product_id'];
	$user_id = get_current_user_id();
	$mylink = $wpdb->get_row("SELECT * FROM wp_users_wishlist WHERE product_id='$product_id' and user_id='$user_id'", ARRAY_N);
	$mWishListCount = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM wp_users_wishlist WHERE user_id=$user_id;"));	
	
    if($mylink==null)
	{
		$wpdb->query("insert into wp_users_wishlist set product_id='$product_id',user_id='$user_id'");
		$mWishListCount = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM wp_users_wishlist WHERE user_id=$user_id;"));	
		
		echo json_encode(array("msg" => "Product added to your wishlist.", "count" => $mWishListCount));
		exit;
	}
	else
	{
		echo json_encode(array("msg" => "This Product already added to your wishlist.", "count" => $mWishListCount));
		exit;
	
	}
    // Return String
    //die($results);
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_myAjax', 'myAjax' );
add_action( 'wp_ajax_myAjax', 'myAjax' );

function myAjaxRemove(){
	global $wpdb;
    //get data from our ajax() call
    $product_id = $_POST['product_id'];
	$user_id = get_current_user_id();
	$mylink = $wpdb->query("delete FROM wp_users_wishlist WHERE product_id='$product_id' and user_id='$user_id'");
    
    // Return String
    //die($results);
}

// create custom Ajax call for WordPress
add_action( 'wp_ajax_nopriv_myAjaxRemove', 'myAjaxRemove' );
add_action( 'wp_ajax_myAjaxRemove', 'myAjaxRemove' );
							
add_theme_support('avia_mega_menu');


require_once( 'framework/avia_framework.php' );

##################################################################


//register additional image thumbnail sizes that should be generated when user uploads an image:
global $avia_config;

$avia_config['imgSize']['widget'] 		= array('width'=>36,  'height'=>36 );		// small preview pics eg sidebar news
$avia_config['imgSize']['related'] 		= array('width'=>130, 'height'=>95 );		// small images for related items
$avia_config['imgSize']['column2'] 		= array('width'=>460, 'height'=>300);		// medium preview pic for 2 column portfolio and small 3d slider
$avia_config['imgSize']['column3'] 		= array('width'=>300, 'height'=>200);		// medium preview pic for 3 column portfolio
$avia_config['imgSize']['column4'] 		= array('width'=>220, 'height'=>160);		// medium preview pic for 4 column portfolio
$avia_config['imgSize']['feature_thumb']= array('width'=>184, 'height'=>90 );		// small preview pic for feature thumbnails
$avia_config['imgSize']['page'] 		= array('width'=>620, 'height'=>200);		// image for pages
$avia_config['imgSize']['page_big'] 	= array('width'=>620, 'height'=>350);		// image for pages (slightly bigger version)
$avia_config['imgSize']['full'] 		= array('width'=>940, 'height'=>390);		// big images for fullsize pages and fullsize 2D & 3D slider


/*preview images for special column sizes of the dynamic template. you can remove those if you dont use them, it will save performance while uploading images and will also save ftp storage*/
$avia_config['imgSize']['grid6'] 		= array('width'=>460, 'height'=>160); 		// half sized images when using 4 columns
$avia_config['imgSize']['grid8'] 		= array('width'=>620, 'height'=>200);		// two/third image	
$avia_config['imgSize']['grid9'] 		= array('width'=>700, 'height'=>160);		// three/fourth image
$avia_config['imgSize']['grid_fifth1'] 	= array('width'=>172, 'height'=>150);		// one fifth
$avia_config['imgSize']['grid_fifth2'] 	= array('width'=>364, 'height'=>150);		// two fifth
$avia_config['imgSize']['grid_fifth3'] 	= array('width'=>556, 'height'=>150);		// three fifth
$avia_config['imgSize']['grid_fifth4'] 	= array('width'=>748, 'height'=>150);	    // four fifth


avia_backend_add_thumbnail_size($avia_config);






##################################################################
# Frontend Stuff necessary for the theme:
##################################################################


$lang = TEMPLATEPATH . '/lang';
load_theme_textdomain('avia_framework', $lang);


/* Register frontend javascripts: */
if(!is_admin()){
	add_action('init', 'avia_frontend_js');
}


if(!function_exists('avia_frontend_js'))
{
	function avia_frontend_js()
	{
		wp_register_script( 'avia-ui', AVIA_BASE_URL.'js/jquery-ui-1.8.14.custom.min.js', 'jquery', 1, false );
		wp_register_script( 'avia-default', AVIA_BASE_URL.'js/avia.js', array('jquery','avia-html5-video','avia-ui'), 1, false );
		wp_register_script( 'avia-prettyPhoto',  AVIA_BASE_URL.'js/prettyPhoto/js/jquery.prettyPhoto.js', 'jquery', "3.0.1", false);
		wp_register_script( 'avia-html5-video',  AVIA_BASE_URL.'js/projekktor/projekktor.min.js', 'jquery', "1", false);
		wp_register_script( 'avia-fade-slider',  AVIA_BASE_URL.'js/avia-fade-slider.js', 'jquery', "1.0.0", false);
	}
}






/* Activate native wordpress navigation menu and register a menu location */

add_theme_support('nav_menus');
register_nav_menu('avia', THEMENAME.' Main Menu');
register_nav_menu('avia2', THEMENAME.' Small Sub Menu');



//load some frontend functions in folder include:

require_once( 'includes/admin/register-widget-area.php' );		// register sidebar widgets for the sidebar and footer
require_once( 'includes/admin/register-styles.php' );			// register the styles for dynamic frontend styling
require_once( 'includes/admin/register-shortcodes.php' );		// register wordpress shortcodes
require_once( 'includes/loop-comments.php' );					// necessary to display the comments properly
require_once( 'includes/helper-slideshow.php' ); 				// holds the class that generates the slideshows, as well as feature images
require_once( 'includes/helper-featured-posts.php' ); 			// holds some helper functions necessary for featured posts
require_once( 'includes/helper-templates.php' ); 				// holds some helper functions necessary for dynamic templates
require_once( 'includes/admin/compat.php' );					// compatibility functions for 3rd party plugins

//activate framework widgets
register_widget( 'avia_tweetbox');
register_widget( 'avia_newsbox' );
//register_widget( 'avia_portfoliobox' );
register_widget( 'avia_socialcount' );
register_widget( 'avia_combo_widget' );
register_widget( 'avia_partner_widget' );
register_widget( 'avia_one_partner_widget' );

//call functions for the theme
add_filter('the_content_more_link', 'avia_remove_more_jump_link');
add_post_type_support('page', 'excerpt');


//allow mp4, webm and ogv file uploads
add_filter('upload_mimes','avia_upload_mimes');
function avia_upload_mimes($mimes){ return array_merge($mimes, array ('mp4' => 'video/mp4', 'ogv' => 'video/ogg', 'webm' => 'video/webm')); }



//remove post thumbnails from pages, posts and various custom post types
if(!function_exists('avia_remove_post_thumbnails'))
{
	add_theme_support( 'post-thumbnails' );
	
	add_action('posts_selection', 'avia_remove_post_thumbnails');
	add_action('init', 'avia_remove_post_thumbnails');
	add_filter('post_updated_messages','avia_remove_post_thumbnails');
	function avia_remove_post_thumbnails($msg) 
	{
		global $post_type;
		$remove_when = array('post','page');

		if(is_admin())
		{
			foreach($remove_when as $remove)
			{
				if($post_type == $remove || (isset($_GET['post_type']) && $_GET['post_type'] == $remove)) { remove_theme_support( 'post-thumbnails' ); };
			}
		}
		
		return $msg;
	}
}

register_sidebar(array(
  'name' => __( 'Navigation Bar For Dashboard' ),
  'id' => 'dashboard-navigation',
  'description' => __( 'Widgets in this area will be shown on the left-hand side.' ),
  'before_title' => '<h1>',
  'after_title' => '</h1>'
));
function add_myscript(){
    wp_enqueue_script( 'my-ajax.js', get_bloginfo('template_directory') . "/js/my-ajax.js", array( 'jquery' ) );
}
add_action( 'init', 'add_myscript' );


function logout_and_redirect_to_homepage() {
	// Finally, destroy the session.
	session_destroy();
	wp_redirect( home_url() );	
	exit;
}

add_action( 'wp_logout', 'logout_and_redirect_to_homepage' );

// custom sharethis shortcode 
if (function_exists('st_makeEntries')){ add_shortcode('sharethis', 'st_makeEntries'); }


/**
* action hook for wp-e-commerce to provide our own AJAX cart updates
*/
function theme_cart_update() {
    $cart_count = wpsc_cart_item_count();
    //$total = wpsc_cart_total_widget();
    echo <<<HTML
jQuery("#shoppingbagcount").html("$cart_count");
 
HTML;
}
 
add_action('wpsc_alternate_cart_html', 'theme_cart_update');

function checkCustomAuthentication($username) {
	if(!username_exists($username)) {
		return;
	}	
	$userinfo = get_userdatabylogin($username);	
	
	$rslt = get_user_meta( 34, '_is_user_ban', true ); 	
	if($rslt){	
		if($rslt=='yes'){
			wp_logout();
			return;
		}
	}
}

add_action('wp_authenticate','checkCustomAuthentication');

/* 

START OF RELATED PRODUCTS META BOX


Fire our meta box setup function on the product editor screen. */
add_action( 'load-post.php', 'smashing_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'smashing_post_meta_boxes_setup' );


/* Meta box setup function. */
function smashing_post_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'smashing_add_post_meta_boxes' );
	
		/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'smashing_save_post_class_meta', 10, 2 );
	
	
}


/* Create one or more meta boxes to be displayed on the post editor screen. */
function smashing_add_post_meta_boxes() {

	add_meta_box(
		'smashing-post-class',			// Unique ID
		esc_html__( 'Related Products', 'example' ),		// Title
		'smashing_post_class_meta_box',		// Callback function
		'wpsc-product',					// Admin page (or post type)
		'side',					// Context
		'default'					// Priority
	);
}

/* Display the post meta box. */
function smashing_post_class_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'smashing_post_class_nonce' ); ?>
	<p>
	<label for="smashing-post-class"><?php _e( "Please select related products", 'example' ); ?></label>
	<br />
<?php
		global $wpdb;
		$sqlt = "SELECT * FROM wp_posts where post_status='publish' and post_type='wpsc-product' and ID!=".$object->ID." order by id desc";
		
		$products = $wpdb->get_results($sqlt, OBJECT);
		
		$sqlt = "SELECT * FROM wp_fs_related_products where product_id=".$object->ID;
		
		$m_related_products = $wpdb->get_results($sqlt, OBJECT);
		$my_rel_prod = array();
		foreach($m_related_products as $mprod){
			$my_rel_prod[] = $mprod->related_product_id;
		}
		
		?>
		<div class="tabs-panel" style="overflow-y: scroll;overflow-x: hidden;height: 236px;">
		<ul>	
			<?php foreach($products as $prod){ ?>	
			<li><label><input type="checkbox" name="smashing-post-class[]" value="<?php echo $prod->ID; ?>" <?php echo in_array($prod->ID,$my_rel_prod) ? 'checked="checked"' : '' ;?> /><?php echo $prod->post_title; ?></label></li>
		<?php } ?>
		</ul>
		</div>
		
	</p>
<?php } 




/* Save post meta on the 'save_post' hook. */
add_action( 'save_post', 'smashing_save_post_class_meta', 10, 2 );


/* Save the meta box's post metadata. */
function smashing_save_post_class_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['smashing_post_class_nonce'] ) || !wp_verify_nonce( $_POST['smashing_post_class_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['smashing-post-class'] ) ? sanitize_html_class( $_POST['smashing-post-class'] ) : '' );
	
global $wpdb;


// delete the previous related product entries of this product
$wpdb->query($wpdb->prepare('DELETE FROM `wp_fs_related_products` WHERE `product_id` = %d or `related_product_id` = %d;', $post_id,$post_id));


foreach($new_meta_value as $val) {
	$wpdb->query($wpdb->prepare('insert into wp_fs_related_products (product_id, related_product_id)values(%d, %d)', $post_id, $val));
	$wpdb->query($wpdb->prepare('insert into wp_fs_related_products (product_id, related_product_id)values(%d, %d)', $val, $post_id));
}



/*
	$com_value = array();
	foreach($new_meta_value as $val){
		$com_value  .= $val;
	}
	$new_meta_value = $com_value;
	// Get the meta key. 
	$meta_key = 'smashing_post_class';

	// Get the meta value of the custom field key. 
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	// If a new meta value was added and there was no previous value, add it. 
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	// If the new meta value does not match the old value, update it. 
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	// If there is no new meta value but an old value exists, delete it. 
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
		
*/
}
/* END OF RELATED PRODUCTS META BOX */

/* 

START OF EDITOR NOTES META BOX


Fire our meta box setup function on the product editor screen. */
add_action( 'load-post.php', 'editors_notes_meta_boxes_setup' );
add_action( 'load-post-new.php', 'editors_notes_meta_boxes_setup' );


/* Meta box setup function. */
function editors_notes_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'editors_add_post_meta_boxes' );
	
		/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'editors_save_post_class_meta', 10, 2 );
	
	
}


/* Create one or more meta boxes to be displayed on the post editor screen. */
function editors_add_post_meta_boxes() {

	add_meta_box(
		'editor-post-class',			// Unique ID
		esc_html__( 'Editors Notes', 'example' ),		// Title
		'editors_notes_class_meta_box',		// Callback function
		'wpsc-product',					// Admin page (or post type)
		'normal',					// Context
		'default'					// Priority
	);
}

/* Display the post meta box. */
function editors_notes_class_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'editors_notes_class_nonce' ); ?>
	<?php 
	
		// Get the meta value of the custom field key. 
		//$editors_notes_class = get_post_meta( $object->ID, 'editors_notes_class', true );
		$editors_notes_detail = get_post_meta( $object->ID, 'editors_notes_detail', true );
		$editors_notes_size_and_fit = get_post_meta( $object->ID, 'editors_notes_size_and_fit', true );

	?>
	<p>
		<label for="editor-post-class"><?php _e( "Please enter the information for the editor tab box at product detail page", 'example' ); ?></label>
		<br />
			<div >
				<table>	
					<!--<tr>
						<td><label>Editor's Notes</label></td>
						<td><textarea rows="5" cols="100" name="editors_notes" > <?php //echo $editors_notes_class; ?> </textarea>	</td>
					</tr>-->
					<tr>
						<td><label>Details</label></td>
						<td><textarea rows="5" cols="100" name="details" > <?php echo $editors_notes_detail ?></textarea></td>
					</tr>
					<tr>
						<td><label>Size & Fit</label></td>
						<td><textarea rows="5" cols="100" name="size_and_fit" ><?php echo $editors_notes_size_and_fit ?> </textarea></td>
					</tr>
				</table>
			</div>		
	</p>
<?php } 

//////////////

/* Save post meta on the 'save_post' hook. */
add_action( 'save_post', 'editors_save_post_class_meta', 10, 2 );


/* Save the meta box's post metadata. */
function editors_save_post_class_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['editors_notes_class_nonce'] ) || !wp_verify_nonce( $_POST['editors_notes_class_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	//$meta_keys = array('editors_notes_class'=>$_POST['editors_notes'],'editors_notes_detail'=>$_POST['details'],'editors_notes_size_and_fit'=>$_POST['size_and_fit']);
	$meta_keys = array('editors_notes_detail'=>$_POST['details'],'editors_notes_size_and_fit'=>$_POST['size_and_fit']);

	foreach($meta_keys as $meta_key=>$new_meta_value){
	
		// Get the meta value of the custom field key. 
		$meta_value = get_post_meta( $post_id, $meta_key, true );

		// If a new meta value was added and there was no previous value, add it. 
		if ( $new_meta_value && '' == $meta_value )
			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		// If the new meta value does not match the old value, update it. 
		elseif ( $new_meta_value && $new_meta_value != $meta_value )
			update_post_meta( $post_id, $meta_key, $new_meta_value );

		// If there is no new meta value but an old value exists, delete it. 
		elseif ( '' == $new_meta_value && $meta_value )
			delete_post_meta( $post_id, $meta_key, $meta_value );
		
	}
}

/** END OF EDITOR NOTES META BOX */

/**
 *Remove the WordPress logo...
 *
 **/
 function my_tweaked_admin_bar() {
        global $wp_admin_bar;

        //Remove the WordPress logo...
        $wp_admin_bar->remove_menu('wp-logo');
 }
 
add_action( 'wp_before_admin_bar_render', 'my_tweaked_admin_bar' ); 


/**
 * the function to remove the admin menus from the left vertical bar
 **/
function remove_menus () {
	if(get_current_user_role()!='administrator'){
		$user_id = get_current_user_id();

		$payment_expiry_date = get_user_meta($user_id, 'payment_expiry_date', true);
		$payment_date = get_user_meta($user_id, 'payment_date', true);
		
		$key = 'user_type';
		$single = true;
		$user_type = get_user_meta( $user_id, $key, $single ); 
		if($user_type=='designer' && $payment_expiry_date>date('Y-m-d')){
			$restricted = array(__('GD'),__('Comments'),__('Posts'),__('Tools'),__('Profile'),__('Dashboard'));
		} else {
			$restricted = array(__('Products'),__('GD'),__('Comments'),__('Posts'),__('Tools'),__('Profile'),__('Dashboard'));
		}
			global $menu;		
			
			//$restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
			end ($menu);
			while (prev($menu)){
				$value = explode(' ',$menu[key($menu)][0]);
				if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}					
	}
}

add_action('admin_menu', 'remove_menus');

/* 

START OF PRODUCT SHIPMENT TYPE META BOX (DOCUMENT, NONDOCUMENT)


Fire our meta box setup function on the product editor screen. */
add_action( 'load-post.php', 'product_ship_type_notes_meta_boxes_setup' );
add_action( 'load-post-new.php', 'product_ship_type_notes_meta_boxes_setup' );


/* Meta box setup function. */
function product_ship_type_notes_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'product_ship_type_add_post_meta_boxes' );
	
		/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'product_ship_type_save_post_class_meta', 10, 2 );
	
	
}


/* Create one or more meta boxes to be displayed on the post editor screen. */
function product_ship_type_add_post_meta_boxes() {

	add_meta_box(
		'product_ship_type_class',			// Unique ID
		esc_html__( 'Product Shipment Type', 'example' ),		// Title
		'product_ship_type_notes_class_meta_box',		// Callback function
		'wpsc-product',					// Admin page (or post type)
		'side',					// Context
		'default'					// Priority
	);
}

/* Display the post meta box. */
function product_ship_type_notes_class_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'product_ship_type_notes_class_nonce' ); ?>
	<?php 
	
		// Get the meta value of the custom field key. 
		$product_ship_type_notes_class = get_post_meta( $object->ID, 'product_ship_type_notes', true );

	?>
	<p>
		<label for="product_ship_type_class"><?php _e( "Please select the product shipment type", 'example' ); ?></label>
		<br />
			<!--<div >
				<table>	
					<tr>
						<td><label>Editor's Notes</label></td>
						<td><textarea rows="5" cols="100" name="product_ship_type_notes" > <?php // echo $product_ship_type_notes_class; ?> </textarea>	</td>
					</tr>					
				</table>
			</div>		-->
		<div class="tabs-panel" style="overflow: hidden;height: 80px;">
			<ul>					
			<?php if($product_ship_type_notes_class){ ?>
				<li><label><input type="radio" name="product_ship_type_notes" value="document" <?php echo $product_ship_type_notes_class=='document' ? 'checked="checked"' : '' ;?> />Document</label></li>
				<li><label><input type="radio" name="product_ship_type_notes" value="nondocument" <?php echo $product_ship_type_notes_class=='nondocument' ? 'checked="checked"' : '' ;?> />Non Document</label></li>
			<?php } else { ?>
				<li><label><input type="radio" name="product_ship_type_notes" value="nondocument" checked="checked" />Non Document</label></li>
				<li><label><input type="radio" name="product_ship_type_notes" value="document" />Document</label></li>
			<?php } ?>
			</ul>
		</div>
	</p>
<?php } 

//////////////

/* Save post meta on the 'save_post' hook. */
add_action( 'save_post', 'product_ship_type_save_post_class_meta', 10, 2 );


/* Save the meta box's post metadata. */
function product_ship_type_save_post_class_meta( $post_id, $post ) {
	
	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['product_ship_type_notes_class_nonce'] ) || !wp_verify_nonce( $_POST['product_ship_type_notes_class_nonce'], basename( __FILE__ ) ) )
		return $post_id;

		
	
	
	
	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

		$new_meta_value = $_POST['product_ship_type_notes'];
		
		$meta_key = 'product_ship_type_notes';
	
		// Get the meta value of the custom field key. 
		$meta_value = get_post_meta( $post_id, $meta_key, true );

		// If a new meta value was added and there was no previous value, add it. 
		if ( $new_meta_value && '' == $meta_value )
			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		// If the new meta value does not match the old value, update it. 
		elseif ( $new_meta_value && $new_meta_value != $meta_value )
			update_post_meta( $post_id, $meta_key, $new_meta_value );

		// If there is no new meta value but an old value exists, delete it. 
		elseif ( '' == $new_meta_value && $meta_value )
			delete_post_meta( $post_id, $meta_key, $meta_value );
		
	
}

/** END OF PRODUCT SHIPMENT TYPE META BOX (DOCUMENT, NONDOCUMENT) */


/** Remove the entry from edit your profile link from admin header **/
function ya_do_it_admin_bar_remove() {
        global $wp_admin_bar;
        /* **edit-profile is the ID** */
        $wp_admin_bar->remove_menu('edit-profile');
 }

add_action('wp_before_admin_bar_render', 'ya_do_it_admin_bar_remove', 0);

add_action('admin_init', 'remove_theme_submenus');
function remove_theme_submenus() {
    global $submenu, $current_user;
    get_currentuserinfo();
	
    if($current_user->user_login != 'admin') {
        unset($submenu['edit.php?post_type=wpsc-product'][5]);
    }
}