<?php

 /**
   * My Reports module definition class
   */
  class MyReportsModule extends AngieModule{
  
      /**
       * Short module name (should be the same as module directory name)
       *
       * @var string
       */
      protected $name = 'my_reports';

      /**
       * Module version
       *
       * @var string
       */  
      protected $version = '1.0';

      /**
       * Return module name (displayed in activeCollab administration panel)
       *
       * @return string
       */
      function getDisplayName() {
        return lang('My Reports');
      }
  
      /**
       * Return module description (displayed in activeCollab administration panel)
       *
       * @return string
       */
      function getDescription() {
        return lang('An example module');
      }

      /**
       * List events that this module listens to and define event handlers
       */
      function defineHandlers() {
        // Place where you can define your event handlers
          EventsManager::listen('on_homescreen_widget_types', 'on_homescreen_widget_types');
          EventsManager::listen('on_status_bar', 'on_status_bar');
          //EventsManager::listen('on_main_menu', 'on_main_menu');
      }

      /**
       * List routes defined and used by this module
       */
      function defineRoutes() {
        // Place where you can define your routes
      }
  
  }
  
  ?>
