<?php

  /**
   * My Reports module initialisation file
   */
  
  define('MY_REPORTS_MODULE', 'my_reports');
  define('MY_REPORTS_MODULE_PATH', __DIR__);
  
  
  AngieApplication::setForAutoload(array(
    'MyReportsHomeScreenWidget' => MY_REPORTS_MODULE_PATH.'/models/homescreen_widgets/MyReportsHomeScreenWidget.class.php'));
  
  ?>
