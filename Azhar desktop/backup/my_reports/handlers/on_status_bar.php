<?php

  /**
   * otifications_plus module on_status_bar event handler
   *
   * @package custom.modules.notifications_plus
   * @subpackage handlers
   */

  /**
   * Register status bar items
   *
   * @param StatusBar $status_bar
   * @param IUser $logged_user
   */
  function my_reports_handle_on_status_bar(StatusBar &$status_bar, IUser &$user) {
  
//	  $show_notifications = ConfigOptions::getValueFor('show_notifications', $user) ? 'true' : 'false';
//	  $notifications_plus_enabled_activity_updates = ConfigOptions::getValue('notifications_plus_enabled_activity_updates') ? 'true' : 'false';

      $status_bar->beginWith('my_reports', lang('Notify'), '', AngieApplication::getImageUrl('status-bar/notify-globe.png', MY_REPORTS_MODULE), array(
        'group' => StatusBar::GROUP_LEFT,
      	// the onclick is eval'd in JS - so putting a function here will execute it
      ));
  } // notifications_plus_handle_on_status_bar