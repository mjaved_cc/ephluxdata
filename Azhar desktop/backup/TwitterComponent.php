<?php

App::uses('OAuth', 'Lib/Twitter');
App::uses('TwitterOAuth', 'Lib/Twitter');
App::uses('Component', 'Controller');

class TwitterComponent extends Component {

    public $components = array('Session');
    private $consumer_key;
    private $consumer_key_secret;
    private $requesttokenurl;
    private $accesstokenurl;
    private $authurl;
//    private $oauth_token;
//    private $oauth_token_secret;
    private $twitter_obj;
    private $controller;

    // oauth status

    const TWITTER_OAUTH_STATUS_OLD_TOKEN = 'oldtoken';
    const TWITTER_USER_DETAILS = 'account/verify_credentials';

    function startup(Controller $controller) {

        Configure::load('twitter');
        $consumer_key = Configure::read('Twitt.CONSUMER_KEY');
        $consumer_secret = Configure::read('Twitt.CONSUMER_SECRET');

        $this->controller = $controller;
        $this->consumer_key = $consumer_key;
        $this->consumer_key_secret = $consumer_secret;
        $this->requesttokenurl = "https://api.twitter.com/oauth/request_token";
        $this->accesstokenurl = "https://api.twitter.com/oauth/access_token";
        $this->authurl = "https://api.twitter.com/oauth/authorize";
        $this->twitter_obj = new TwitterOAuth($this->consumer_key, $this->consumer_key_secret);
    }

    function getAuthorize() {
        $request_token = $this->twitter_obj->getRequestToken();

        /* Save temporary credentials to session. */
        //$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
        //$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        $this->Session->write('Twitter.oauth_token' , $request_token['oauth_token']);
        $this->Session->write('Twitter.oauth_token_secret' , $request_token['oauth_token_secret']);

        /* If last connection failed don't display authorization link. */
        switch ($this->twitter_obj->http_code) {
            case 200:
                /* Build authorize URL and redirect user to Twitter. */
                return $this->twitter_obj->getAuthorizeURL($request_token['oauth_token']);
                break;
            default:
                /* Show notification if something went wrong. */
                $this->log('Could not connect to Twitter. Refresh the page or try again later.', 'debug');
        }
    }

    function callback() {

         $session_oauth_token = $this->Session->read('Twitter.oauth_token'); // get from session 
         $session_oauth_token_secret = $this->Session->read('Twitter.oauth_token_secret');
          
          $query_oauth_token = $_GET['oauth_token']; // get from query string
          if (isset($query_oauth_token) && $session_oauth_token !== $query_oauth_token) {
          $this->Session->write('Twitter.oauth_status', TWITTER_OAUTH_STATUS_OLD_TOKEN);
          $this->controller->redirect(array(
          'controller' => 'users', 'action' => 'TWlogin'
          ));
          } 

       /* if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
            $_SESSION['oauth_status'] = 'oldtoken';
            $this->controller->redirect(array(
                'controller' => 'users', 'action' => 'TWlogin'
            ));
        }*/

       // $this->twitter_obj = new TwitterOAuth($this->consumer_key, $this->consumer_key_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
          
          $this->twitter_obj = new TwitterOAuth($this->consumer_key, $this->consumer_key_secret, $session_oauth_token, $session_oauth_token_secret);

        $access_token = $this->twitter_obj->getAccessToken($_REQUEST['oauth_verifier']);


        //$_SESSION['access_token'] = $access_token;
        $this->Session->write('Twitter.access_token' , $access_token);

        //unset($_SESSION['oauth_token']);
        //unset($_SESSION['oauth_token_secret']);
        $this->Session->delete('Twitter.oauth_token');
        $this->Session->delete('Twitter.oauth_token_secret');


        if (200 == $this->twitter_obj->http_code) {

            //$_SESSION['status'] = 'verified';
            $this->Session->write('Twitter.status' , 'verified');
            return true;
        } else {
            $this->controller->redirect(array(
                'controller' => 'users', 'action' => 'TWlogin'
            ));
        }
    }

    function index() {
        
        $access_token = $this->Session->read('Twitter.access_token');
        if (empty($access_token) || empty($access_token['oauth_token']) || empty($access_token['oauth_token_secret'])) {
            //header('Location: ./clearsessions.php');
            //$this->controller->redirect($this->getAuthorize());
            $this->controller->redirect(array(
                'controller' => 'users', 'action' => 'TWlogin'
            ));
        }

        //$access_token = $_SESSION['access_token'];
        //$access_token = $this->Session->read('Twitter.access_token');


        $this->twitter_obj = new TwitterOAuth($this->consumer_key, $this->consumer_key_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);


        $content = Set::reverse($this->twitter_obj->get(self::TWITTER_USER_DETAILS));
        return $content;


        //debug($content);        exit();
        //$this->Session->write('user_data', $content);
    }

    function save($user) {
        $this->User = ClassRegistry::init('User');
        $id = $user['id'];
        $user_name = $user['name'];

        $query = $this->User->find('first', array('conditions' => array('User.twitter_id' => $id)));

        if (empty($query)) {

            $data = array(
                'User' => array(
                    'twitter_id' => $id,
                    'username' => $user_name
                )
            );

            $this->User->save($data);
        }
    }

}

?>