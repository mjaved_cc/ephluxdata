<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class Role extends AppModel {
    var $name = 'Role';
    var $displayField = 'role';

    var $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'role_id',
            'dependent' => false
        )
    );

} 
