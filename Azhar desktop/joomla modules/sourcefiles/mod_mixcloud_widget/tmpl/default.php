<?php
/**
 * @package		B4ucode
 * @subpackage	mod_mixcloud_widget
 * @copyright	Copyright (C) 2011 - 2012 B4ucode, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<div><object width="<?php echo $width; ?>" height="<?php echo $height; ?>">
<param name="movie" value="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed=<?php echo $feed; ?>&amp;embed_uuid=&amp;stylecolor=<?php echo $color; ?>&amp;embed_type=widget_standard"></param><param name="allowFullScreen" value="true"></param><param name="wmode" value="opaque"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed=<?php echo $feed; ?>&amp;embed_uuid=&amp;stylecolor=<?php echo $color; ?>&amp;embed_type=widget_standard" type="application/x-shockwave-flash" wmode="opaque" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $width; ?>" height="<?php echo $height; ?>"></embed></object>
</div>