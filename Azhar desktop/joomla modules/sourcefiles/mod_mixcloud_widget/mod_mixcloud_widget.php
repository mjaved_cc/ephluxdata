<?php
/**
 * @package		B4ucode
 * @subpackage	mod_mixcloud_widget
 * @copyright	Copyright (C) 2011 - 2012 B4ucode, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$width = $params->get('width',300);
$height = $params->get('height',300);
$feed = $params->get('feed');
$color = $params->get('color');
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));


require JModuleHelper::getLayoutPath('mod_mixcloud_widget', $params->get('layout', 'default'));
