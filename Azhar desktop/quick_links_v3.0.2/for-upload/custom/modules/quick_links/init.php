<?php

  /**
   * Init Quick Links module
   *
   * @package customs.modules.quick_links
   */
  
   if(!defined('IS_AC_GREATER_THAN_33')) {
    define('IS_AC_GREATER_THAN_33', version_compare(APPLICATION_VERSION, '3.3', '>='));
  }
  
  define('QUICK_LINKS_MODULE', 'quick_links');
  define('QUICK_LINKS_MODULE_PATH', CUSTOM_PATH . '/modules/quick_links');

    
  AngieApplication::setForAutoload(array(
  'QuickLinks' => QUICK_LINKS_MODULE_PATH . '/models/quick_links/QuickLinks.class.php',
  ));
  
  
  