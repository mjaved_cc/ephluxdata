{title}Quick Links Settings{/title} 
{add_bread_crumb}Quick Links{/add_bread_crumb}

<div id="workweek_settings">
  {form action=Router::assemble('quick_links_admin') method=post}
    <div class="content_stack_wrapper">
      
      <div class="content_stack_element">
        <div class="content_stack_element_info"><h3>{label}Links{/label}</h3></div>
        
		<!-- Quick Links -->
        <div id="quick_links_wrapper" class="content_stack_element_body">
          {wrap field=links}
            <table class="form form_field validate_callback validate_days_off" id="workweek_days_off" >
              <tr>
                <th>{label required=yes}Icon{/label}</th>
                <th>{label required=yes}Label{/label}</th>
                <th>{label required=yes}URL{/label}</th>
                <th>{label for=fieldEditRoles}Viewable by Roles{/label}</th>                
                <th>{label required=yes}Open in New Window{/label}</th>
                <th></th>
              </tr>
            {if is_foreachable($links)}
              {foreach $links as $key => $row}
                <tr>
                  <td class="td_link_icon">{select_link_icon value=$row.icon class=quickLinkType key=$key}</td>
                  <td class="td_link_label">{text_field class="link_label" name="links[$key][label]" value=$row.label}</td>
                  <td class="td_url_name">{text_field class="url_name" name="links[$key][url]" value=$row.url}</td>
                  <td class="td_visible_roles">
				      {select_role_multiple name="links[$key][visible_by_roles][]" optional=true value=$row.visible_by_roles style="width:170px;"}
				  </td>
                  <td class="td_new_window"><input type="checkbox" name="links[{$key}][new_window]" value="1" {if $row.new_window}checked{/if}></td>
                </tr>
              {/foreach}
            {/if}
            </table>
          {/wrap}
        </div>
      </div>
     </div>
    
    {wrap_buttons}
  	  {submit}Save Changes{/submit}
    {/wrap_buttons}
  {/form}
</div>

{literal}
<script type="text/javascript">
	$(".quickLinkType").msDropDown({visibleRows: 7});
	$('.ddChild .enabled').click(function() {
		return false;
	});
</script>
{/literal}