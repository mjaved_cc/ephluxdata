<?php

  /**
   * Quick Links module on_main_menu event handler
   *
   * @package activeCollab.modules.documents
   * @subpackage handlers
   */
  
  /**
   * Handle on_main_menu event
   *
   */
  function quick_links_handle_on_main_menu(MainMenu &$menu, User &$user) {
  		$links = ConfigOptions::getValue('quick_links');
  		if(is_foreachable($links)) {
	  		foreach($links as $link_id => $link ){
  				if($link['url'] != '' && (in_array($user->getRoleId(), $link['visible_by_roles'] ) || in_array(-1, $link['visible_by_roles']) || !array_key_exists('visible_by_roles', $link)) ){
	  				$image_url = AngieApplication::getImageUrl('types/'.$link['icon'], QUICK_LINKS_MODULE) . '.png';
	  				$additional = null;
	  				if (!empty($link['new_window'])) {
						$url = $link['url'];
						$additional = array('target' => '_blank');	  					
	  				} else {
	  					$url = Router::assemble('quick_links', array('link_id' => $link_id));
	  				}
					$menu->add('quick_links_' . $link_id, lang($link['label']), $url, $image_url, $additional);
	  			}
			}//for
		}
  } // quick_links_on_main_menu