<?php

  /**
   * Quick Links on_admin_panel event handler
   * 
   * @package custom.modules.tasks_plus
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function quick_links_handle_on_admin_panel(AdminPanel &$admin_panel) {
	$admin_panel->addToTools ( 'quick_links', lang ( 'Quick Links' ), Router::assemble ( 'quick_links_admin'), AngieApplication::getImageUrl ( 'module.png', QUICK_LINKS_MODULE ) );
  } // quick_links_on_admin_panel