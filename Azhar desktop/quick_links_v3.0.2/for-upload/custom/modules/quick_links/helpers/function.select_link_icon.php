<?php

  /**
  * Render select box for QuickLinks type
  * 
  * Parameters:
  * 
  * - all HTML attributes
  * - value - value of selected type
  * @param array $params
  * @param Smarty $smarty
  * @return string
  */
  function smarty_function_select_link_icon($params, &$smarty) {
    
    $value = null;
    if(isset($params['value'])) {
      $value = (string) array_var($params, 'value');
      unset($params['value']);
    } // if
    
    // Special case for no value
    if ($value == '') { $value = 'none'; }
    
    $key = (int) array_var($params, 'key');
    
    $params['name'] = 'links[' . $key . '][icon]';
    
    $types = QuickLinks::getTypes();
    
    $options = array();
    foreach($types as $type) {
      $option_attributes = array();
      $option_attributes['title'] = AngieApplication::getImageUrl('types/'.$type['file'], QUICK_LINKS_MODULE);
      if ($value == $type['value']) {
      	$option_attributes['selected'] = true;
      } 
      $options[] = option_tag($type['label'], $type['value'], $option_attributes);
    } // if
    
    return select_box($options, $params);
  } // smarty_function_select_country

?>