<?php

/**
   * Select multiple roles helper implementation
   *
   * @package custom.modules.quick_links
   * @subpackage helpers
   */


  function smarty_function_select_role_multiple($params, &$smarty) {
    $params['multiple'] = true;
    
    $value = array_var($params, 'value', null, true);
    $optional = array_var($params, 'optional', false, true);

    if($optional) {
    	if(is_array($value) && in_array(-1, $value)){
        	$option_attributes = array('selected' => true);
      	} else {
        	$option_attributes = null;
      	}
      	$options = array(
        option_tag(lang('All'), -1, $option_attributes));
    } else {
      $options = array();
    } 
    
	$all_roles = Roles::find(array('order' => 'name'));
    if(is_foreachable($all_roles)) {
    	foreach($all_roles as $role) {
	          if(is_array($value) && in_array($role->getId(), $value)){
	            $option_attributes = array('selected' => true);
	          } else {
	            $option_attributes = null;
	          }
	          $options[] = option_tag($role->getName(), $role->getId(), $option_attributes);
    	} 
    } // if
    
    return select_box($options, $params);
  } // smarty_function_select_role_multiple