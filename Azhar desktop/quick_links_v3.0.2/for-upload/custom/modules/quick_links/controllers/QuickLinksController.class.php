<?php

 AngieApplication::useController('backend', ENVIRONMENT_FRAMEWORK_INJECT_INTO);
  /**
   * Quick Links
   *
   * @package activeCollab.modules
   * @subpackage controllers
   */
 
  class QuickLinksController extends BackendController {

	function index() {
		
		$link_id = $this->request->getId('link_id');
		$links = ConfigOptions::getValue('quick_links');
		
		$selected_link_data = array();
		if(is_foreachable($links) && !is_null($link_id) && isset($links[$link_id])) {
			$selected_link_data = $links[$link_id];
			if(!isset($selected_link_data['new_window'])){
				//Set current tab
				$this->wireframe->tabs->add('quick_links_' . $link_id , $selected_link_data['label'], $selected_link_data['url'], null, true);
				$this->wireframe->tabs->setCurrentTab('quick_links_' . $link_id);
			}
		} else {
			throw new Error("That seems to be an invalid link");
		}
		
		$this->smarty->assign('selected_link_data', $selected_link_data);
	}
  }
  
