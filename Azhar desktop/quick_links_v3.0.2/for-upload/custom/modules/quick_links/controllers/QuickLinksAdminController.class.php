<?php

  // Build on top of administration controller
  AngieApplication::useController('admin', SYSTEM_MODULE);
  
  /**
   * QuickLinks administration controller
   * 
   * @package custom.modules.quick_links
   * @subpackage controllers
   */
  class QuickLinksAdminController extends AdminController {
  	
  	
  	var $no_of_links = 5;
  	
  	function index() {
		
  		if($this->request->isSubmitted()) {
  			$links = $this->request->post('links');
  			if(is_foreachable($links)){
  				for($i = 0; $i < $this->no_of_links; $i++) {
  					if( !isset($links[$i]['url']) || empty($links[$i]['url']) || !isset($links[$i]['label']) || empty($links[$i]['label']) ){
						unset($links[$i]);
					}
  				}
  				$links = array_values($links);
  				$pad = array('icon' => '', 'label' => '', 'url' => '', 'visible_by_roles' => array(), 'new_window' => 0);
  				$links = array_pad($links, $this->no_of_links, $pad);
				ConfigOptions::setValue('quick_links',$links, true);
			}
		}
		$links = ConfigOptions::getValue('quick_links');
		$this->smarty->assign('links',$links);
  	}
  }