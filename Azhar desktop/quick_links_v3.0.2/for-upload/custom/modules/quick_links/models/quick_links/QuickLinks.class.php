<?php

  /**
   * Qucik Links
   *
   * @package activeCollab.modules.tasks
   * @subpackage models
   */
  class QuickLinks {
  	
	  function getTypes() {
	    	$cache_id = QUICK_LINKS_MODULE.'_types';
                $cached =IS_AC_GREATER_THAN_33 ? AngieApplication::cache()->get($cache_id) : cache_get($cache_id);
	      	if($cached) {
	      		$types = $cached;	
	      	} else {
	      		$types = array();
	      		$d = dir(QUICK_LINKS_MODULE_PATH.'/assets/default/images/types/');
			    while(($entry = $d->read()) !== false) {
			        if(str_starts_with($entry, '.')) {
			          continue;
			        } // if
			        $name = substr($entry, 0, strpos($entry, '.'));
			        $types[$name] = array('label'=>Inflector::humanize($name), 'file'=>$entry, 'value'=>$name);
			      } // if
			    $d->close();
			    ksort($types);
                        IS_AC_GREATER_THAN_33 ? AngieApplication::cache()->set($cache_id, $types) : cache_set($cache_id, $types);
	      	}
	    	return $types;
	    }
  }
