<?php
  /**
   * Quick Links definition
   *
   * @package customs.modules.quick_links
   * 
   */
  class QuickLinksModule extends AngieModule {
    
    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'quick_links';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '3.0.2';
    
    /**
     * Define module routes
     */
    function defineRoutes() {
      // Admin Route
      	Router::map('quick_links_admin', 'admin/tools/quick-links', array('controller' => 'quick_links_admin', 'action' => 'index'));
     	Router::map('quick_links', 'quick-links/:link_id', array('controller' => 'quick_links', 'action' => 'index'), array('link_id' => '\d+'));
    } // defineRoutes
    
    /**
     * Define event handlers
     */
    function defineHandlers() {
    	EventsManager::listen('on_main_menu', 'on_main_menu');
     	EventsManager::listen('on_admin_panel', 'on_admin_panel');
    } // defineHandlers

   
    function install() {
    	parent::install();
		// Create 5 blank links    	
    	for($i = 0 ; $i < 5; $i++) {
    		$links[] = array('icon' => '', 'label' => '', 'url' => '', 'visible_by_roles' => array(), 'new_window' => 0);
    	}
    	ConfigOptions::addOption('quick_links', 'quick_links', $links);
    }
    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Quick Links');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
      return lang('Custom links for aC main menu - easily integrate with external systems or offer quick jump to important aC page.');
    } // getDescription
    
    /**
     * Return module uninstallation message
     *
     * @return string
     */
    function getUninstallMessage() {
      return lang('Module will be deactivated.');
    } // getUninstallMessage
    
  }