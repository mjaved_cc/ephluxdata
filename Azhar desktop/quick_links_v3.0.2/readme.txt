================================================================================
= README.TXT                                                                   =
================================================================================

Thank you for purchasing Quick Links Module for activeCollab.


------------------------------------------------
Installation instructions:
------------------------------------------------

1. Unzip the Quick Links Module zip file. And upload contents of "for-upload" 
folder to your activeCollab server using FTP or SFTP client. Make sure you 
upload the files in your activeCollab root folder. Match activeCollab folder 
hierarchy to extracted folders to confirm.

2. Make sure you upload all folders and files within "for-upload" folder.

3. Go to activeCollab administration -> Modules. Install Quick Links
module. 

4. Go to activeCollab administration -> Quick Links to add links.

Here's a video tutorial on installing custom modules in activeCollab 3
http://www.appsmagnet.com/module-install-guide

------------------------------------------------
Uninstallation / Removal instructions:
------------------------------------------------

- Uninstall Quick Links module from activeCollab admin


------------------------------------------------
Support, Further Documentation and Tutorials:
------------------------------------------------
Support: support@appsmagnet.com

================================================================================

Thank you for purchasing Quick Links Module. We hope it helps you integrate with
external systems and provide a jump start to your team.

- Apps Magnet Team
http://www.appsmagnet.com/
