<?php // pr($data);  ?>


<div class="main_column">
	<div class="box">
		<div class="body">
			<h2><?php echo __('Record'); ?></h2>
			<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">
				<thead>
					<tr>
						<th><?php echo __('Picture Link'); ?></th> 
						<th><?php echo __('Address'); ?></th>
						<th><?php echo __('Message'); ?></th>
						<th><?php echo __('lat'); ?></th>
						<th><?php echo __('lang'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>

					</tr>
				</thead>
				<tbody>
					

						<tr>
							<td><?php echo $this->Html->image($data['picture_link'], array('width' => '100', 'height' => '100')); ?></td>
							<td><?php echo $data['address'] ?></td>
							<td><?php echo $data['message'] ?></td>
							<td><?php echo $data['lat'] ?></td>
							<td><?php echo $data['lng'] ?></td>
							

						</tr>

					
				</tbody>
			</table>

			





		</div>
	</div>
</div>