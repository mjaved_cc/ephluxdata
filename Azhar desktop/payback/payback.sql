/*
SQLyog - Free MySQL GUI v5.16
Host - 5.0.45-community-nt : Database - payback
*********************************************************************
Server version : 5.0.45-community-nt
*/


SET NAMES utf8;

SET SQL_MODE='';

USE `cp-cms-db`;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `acos` */

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

/*Data for the table `acos` */

insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,NULL,NULL,'controllers',1,164);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (2,1,NULL,NULL,'AuthActionMaps',2,19);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (3,2,NULL,NULL,'index',3,4);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (4,2,NULL,NULL,'view',5,6);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (5,2,NULL,NULL,'add',7,8);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (6,2,NULL,NULL,'edit',9,10);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (7,2,NULL,NULL,'delete',11,12);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (8,2,NULL,NULL,'assign_permission',13,14);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (16,2,NULL,NULL,'sync_action_maps',15,16);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (17,1,NULL,NULL,'Demos',20,49);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (21,17,NULL,NULL,'facebook_connect',21,22);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (25,17,NULL,NULL,'twitter_connect',23,24);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (27,17,NULL,NULL,'signup',25,26);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (28,17,NULL,NULL,'verify_account',27,28);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (30,17,NULL,NULL,'forgot_password',29,30);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (31,17,NULL,NULL,'reset_account_password',31,32);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (40,1,NULL,NULL,'Groups',50,63);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (41,40,NULL,NULL,'index',51,52);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (42,40,NULL,NULL,'view',53,54);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (43,40,NULL,NULL,'add',55,56);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (44,40,NULL,NULL,'edit',57,58);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (45,40,NULL,NULL,'delete',59,60);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (54,1,NULL,NULL,'Users',64,77);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (55,54,NULL,NULL,'index',65,66);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (56,54,NULL,NULL,'view',67,68);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (57,54,NULL,NULL,'add',69,70);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (58,54,NULL,NULL,'edit',71,72);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (59,54,NULL,NULL,'delete',73,74);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (82,1,NULL,NULL,'AclExtras',78,79);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (83,1,NULL,NULL,'Minify',80,85);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (84,83,NULL,NULL,'Minify',81,84);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (85,84,NULL,NULL,'index',82,83);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (86,17,NULL,NULL,'index',33,34);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (87,17,NULL,NULL,'secure',35,36);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (88,2,NULL,NULL,'isAuthorized',17,18);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (89,17,NULL,NULL,'isAuthorized',37,38);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (90,40,NULL,NULL,'isAuthorized',61,62);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (91,54,NULL,NULL,'isAuthorized',75,76);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (92,1,NULL,NULL,'Conversations',86,143);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (93,92,NULL,NULL,'ConversationMessages',87,92);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (94,93,NULL,NULL,'add',88,89);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (102,93,NULL,NULL,'isAuthorized',90,91);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (103,92,NULL,NULL,'ConversationUsers',93,116);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (104,103,NULL,NULL,'index',94,95);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (105,103,NULL,NULL,'view',96,97);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (106,103,NULL,NULL,'add',98,99);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (107,103,NULL,NULL,'edit',100,101);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (108,103,NULL,NULL,'delete',102,103);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (109,103,NULL,NULL,'admin_index',104,105);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (110,103,NULL,NULL,'admin_view',106,107);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (111,103,NULL,NULL,'admin_add',108,109);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (112,103,NULL,NULL,'admin_edit',110,111);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (113,103,NULL,NULL,'admin_delete',112,113);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (121,103,NULL,NULL,'isAuthorized',114,115);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (122,92,NULL,NULL,'Conversations',117,142);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (123,122,NULL,NULL,'index',118,119);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (124,122,NULL,NULL,'sent',120,121);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (125,122,NULL,NULL,'view',122,123);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (126,122,NULL,NULL,'add',124,125);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (127,122,NULL,NULL,'edit',126,127);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (128,122,NULL,NULL,'delete',128,129);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (129,122,NULL,NULL,'admin_index',130,131);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (130,122,NULL,NULL,'admin_view',132,133);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (131,122,NULL,NULL,'admin_add',134,135);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (132,122,NULL,NULL,'admin_edit',136,137);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (133,122,NULL,NULL,'admin_delete',138,139);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (141,122,NULL,NULL,'isAuthorized',140,141);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (157,17,NULL,NULL,'login',39,40);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (161,17,NULL,NULL,'logout',41,42);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (165,17,NULL,NULL,'login_with_facebook',43,44);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (166,17,NULL,NULL,'login_with_twitter',45,46);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (173,17,NULL,NULL,'test',47,48);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (174,1,NULL,NULL,'Features',144,157);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (175,174,NULL,NULL,'index',145,146);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (176,174,NULL,NULL,'view',147,148);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (177,174,NULL,NULL,'add',149,150);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (178,174,NULL,NULL,'edit',151,152);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (179,174,NULL,NULL,'delete',153,154);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (180,174,NULL,NULL,'isAuthorized',155,156);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (181,1,NULL,NULL,'conversations',158,163);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (182,181,NULL,NULL,'ConversationsApp',159,162);
insert into `acos` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (183,182,NULL,NULL,'isAuthorized',160,161);

/*Table structure for table `aros` */

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL auto_increment,
  `parent_id` int(10) default NULL,
  `model` varchar(255) default NULL,
  `foreign_key` int(10) default NULL,
  `alias` varchar(255) default NULL,
  `lft` int(10) default NULL,
  `rght` int(10) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `aros` */

insert into `aros` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (1,NULL,'Group',1,'group-1',1,2);
insert into `aros` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (2,NULL,'Group',2,'group-2',3,4);
insert into `aros` (`id`,`parent_id`,`model`,`foreign_key`,`alias`,`lft`,`rght`) values (3,NULL,'Group',4,'group-4',5,6);

/*Table structure for table `aros_acos` */

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL auto_increment,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL default '0',
  `_read` varchar(2) NOT NULL default '0',
  `_update` varchar(2) NOT NULL default '0',
  `_delete` varchar(2) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

/*Data for the table `aros_acos` */

insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (110,2,21,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (111,2,25,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (112,2,27,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (113,2,28,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (114,2,30,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (115,2,31,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (116,2,55,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (117,2,56,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (118,2,57,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (119,2,58,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (120,2,59,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (121,2,157,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (122,2,161,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (123,2,165,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (124,2,166,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (135,3,21,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (136,3,25,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (137,3,27,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (138,3,28,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (139,3,30,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (140,3,31,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (141,3,157,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (142,3,161,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (143,3,165,'1','1','1','1');
insert into `aros_acos` (`id`,`aro_id`,`aco_id`,`_create`,`_read`,`_update`,`_delete`) values (144,3,166,'1','1','1','1');

/*Table structure for table `auth_action_maps` */

DROP TABLE IF EXISTS `auth_action_maps`;

CREATE TABLE `auth_action_maps` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL default '5',
  `crud` enum('create','read','update','delete') NOT NULL default 'read',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;

/*Data for the table `auth_action_maps` */

insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (2,'Display mapping information',4,2,'read','2013-04-10 20:21:58','2013-04-19 21:18:37');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (3,'Adding mapping information',5,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:06');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (4,'Editing mapping information',6,2,'read','2013-04-10 20:21:58','2013-04-19 21:19:23');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (5,'Deleting mapping information',7,2,'read','2013-04-10 20:21:58','2013-04-22 22:05:17');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (6,'Assign permission',8,2,'read','2013-04-10 20:21:58','2013-04-19 21:20:56');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (14,'Add/Synchronise all acos with auth action mapper',16,2,'read','2013-04-10 20:21:58','2013-04-19 21:21:52');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (17,'callback action for facebook',21,1,'read','2013-04-10 20:21:58','2013-04-19 21:14:23');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (21,'callback action for twitter',25,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:11');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (23,'Register user',27,1,'create','2013-04-10 20:21:58','2013-04-22 22:05:47');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (24,'Email verification',28,1,'read','2013-04-10 20:21:58','2013-04-22 22:06:07');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (26,'Forgot password',30,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:18');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (27,'Reset password',31,1,'read','2013-04-10 20:21:58','2013-04-22 22:07:23');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (35,'List all groups',41,3,'read','2013-04-10 20:21:58','2013-04-19 21:22:45');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (36,'Display group',42,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:01');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (37,'Add group',43,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:15');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (38,'Edit group',44,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:36');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (39,'Delete group',45,3,'read','2013-04-10 20:21:58','2013-04-19 21:23:52');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (47,'List all users',55,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:37');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (48,'Display user',56,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:48');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (49,'Add user',57,1,'read','2013-04-10 20:21:58','2013-04-19 21:24:57');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (50,'Edit user',58,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:26');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (51,'Delete user',59,1,'read','2013-04-10 20:21:58','2013-04-19 21:25:36');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (73,'Minify plugin',84,4,'read','2013-04-10 20:21:58','2013-04-19 21:27:01');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (74,'Minify plugin',85,4,'read','2013-04-10 20:21:58','2013-04-19 21:29:09');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (75,'Dummy index action',86,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:04');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (76,'Action after successful login',87,5,'read','2013-04-10 20:21:58','2013-04-19 20:57:42');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (77,'',88,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (78,'',89,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (79,'',90,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (80,'',91,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (81,'ConversationMessages',93,4,'read','2013-04-10 20:21:58','2013-04-22 22:10:27');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (82,'ConversationMessages -> add',94,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:12');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (90,'',102,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (91,'Conversations -> ConversationUsers',103,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:42');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (92,'ConversationUsers -> index',104,4,'read','2013-04-10 20:21:58','2013-04-22 22:11:55');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (93,'ConversationUsers -> view',105,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:12');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (94,'ConversationUsers -> add',106,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:31');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (95,'ConversationUsers -> edit',107,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:42');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (96,'ConversationUsers -> delete',108,4,'read','2013-04-10 20:21:58','2013-04-22 22:12:53');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (97,'ConversationUsers -> admin_index',109,4,'read','2013-04-10 20:21:58','2013-04-22 22:13:10');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (98,'ConversationUsers -> admin_view',110,4,'read','2013-04-10 20:21:58','2013-04-22 22:17:48');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (99,'ConversationUsers -> admin_add',111,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:00');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (100,'ConversationUsers -> admin_edit',112,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:10');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (101,'ConversationUsers -> admin_delete',113,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:20');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (109,'',121,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (110,'Conversations -> Conversations',122,4,'read','2013-04-10 20:21:58','2013-04-22 22:18:37');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (111,'Conversations -> index',123,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:00');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (112,'Conversations -> sent',124,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:13');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (113,'Conversations -> view',125,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:27');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (114,'Conversations -> add',126,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:37');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (115,'Conversations -> edit',127,4,'read','2013-04-10 20:21:58','2013-04-22 22:19:49');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (116,'ConversationUsers -> delete',128,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:01');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (117,'Conversations -> admin_index',129,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:14');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (118,'Conversations -> admin_view',130,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:33');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (119,'Conversations -> admin_add',131,4,'read','2013-04-10 20:21:58','2013-04-22 22:20:51');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (120,'Conversations -> admin_edit',132,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:06');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (121,'Conversations -> admin_delete',133,4,'read','2013-04-10 20:21:58','2013-04-22 22:21:26');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (129,'',141,5,'read','2013-04-10 20:21:58','2013-04-10 20:21:58');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (141,'Login',157,1,'read','2013-04-12 21:53:51','2013-04-19 21:30:07');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (144,'Logout',161,1,'read','2013-04-15 20:01:29','2013-04-19 21:30:12');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (147,'Login action for facebook',165,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:19');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (148,'Login action for twitter',166,1,'read','2013-04-17 22:22:47','2013-04-19 21:30:25');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (153,'List all auth action maps',3,2,'read','2013-04-19 20:03:52','2013-04-22 16:11:29');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (154,'',173,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (155,'Display features',175,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:00');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (156,'View Feature',176,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:14');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (157,'Add new Feature',177,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:23');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (158,'Edit Feature',178,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:33');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (159,'Delete Feature',179,8,'read','2013-04-19 21:56:05','2013-04-24 21:55:43');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (160,'',180,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (161,'',182,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05');
insert into `auth_action_maps` (`id`,`description`,`aco_id`,`feature_id`,`crud`,`created`,`modified`) values (162,'',183,5,'read','2013-04-19 21:56:05','2013-04-19 21:56:05');

/*Table structure for table `conversation_groups` */

DROP TABLE IF EXISTS `conversation_groups`;

CREATE TABLE `conversation_groups` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `conversation_groups` */

insert into `conversation_groups` (`id`,`title`) values (1,'doctors');
insert into `conversation_groups` (`id`,`title`) values (2,'reception');

/*Table structure for table `conversation_messages` */

DROP TABLE IF EXISTS `conversation_messages`;

CREATE TABLE `conversation_messages` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `conversation_id` int(11) NOT NULL,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `attachment_id` int(10) unsigned NOT NULL default '0',
  `message` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `conversation_messages` */

insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (1,1,'20',0,'<p>losem iprem ASASSAs</p>','2013-04-10 18:50:03');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (2,2,'20',0,'<p>javed</p>','2013-04-10 18:52:55');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (6,2,'21',0,'<p>reply from javed</p>','2013-04-11 12:31:54');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (7,2,'21',0,'<p>reply from javed</p>','2013-04-11 12:35:34');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (8,2,'21',0,'<p>afda</p>','2013-04-11 12:39:41');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (9,3,'20',0,'<p>aaaaa</p>','2013-04-11 14:46:06');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (10,4,'21',0,'THis is sampe content','2013-04-11 19:59:06');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (11,4,'20',0,'<p>afdaf</p>','2013-04-11 15:04:59');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (12,4,'20',0,'<p>testssss</p>','2013-04-11 15:05:28');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (13,4,'20',0,'Well done','2013-04-11 20:08:23');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (14,4,'21',0,'werewrwrw','2013-04-11 20:08:48');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (15,5,'21',0,'1st msg','2013-04-11 20:14:27');
insert into `conversation_messages` (`id`,`conversation_id`,`user_id`,`attachment_id`,`message`,`created`) values (16,5,'20',0,'2nd msg','2013-04-11 20:14:40');

/*Table structure for table `conversation_users` */

DROP TABLE IF EXISTS `conversation_users`;

CREATE TABLE `conversation_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `conversation_id` int(11) NOT NULL,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `status` tinyint(2) unsigned NOT NULL default '0' COMMENT '0=ok,\n1=deleted,2=removed',
  `last_view` int(10) unsigned NOT NULL default '0',
  `created` datetime NOT NULL,
  `folder` enum('inbox','sent') default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `conversation_users` */

insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (1,1,'14',0,0,'2013-04-10 18:50:03','inbox');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (2,1,'20',0,0,'2013-04-10 18:50:03','sent');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (3,2,'14',0,0,'2013-04-10 18:52:55','inbox');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (4,2,'20',0,0,'2013-04-10 18:52:55','sent');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (5,3,'22',0,0,'2013-04-11 14:46:06','inbox');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (6,3,'20',0,0,'2013-04-11 14:46:06','sent');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (7,4,'20',0,0,'2013-04-11 19:59:06','inbox');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (8,4,'21',0,0,'2013-04-11 19:59:06','sent');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (9,5,'20',0,0,'2013-04-11 20:14:27','inbox');
insert into `conversation_users` (`id`,`conversation_id`,`user_id`,`status`,`last_view`,`created`,`folder`) values (10,5,'21',0,0,'2013-04-11 20:14:27','sent');

/*Table structure for table `conversations` */

DROP TABLE IF EXISTS `conversations`;

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` char(36) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `title` varchar(60) character set utf8 collate utf8_unicode_ci NOT NULL default '',
  `created` datetime NOT NULL,
  `last_message_id` int(10) unsigned NOT NULL default '0',
  `allow_add` tinyint(1) unsigned NOT NULL default '0',
  `count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `conversations` */

insert into `conversations` (`id`,`user_id`,`title`,`created`,`last_message_id`,`allow_add`,`count`) values (1,'20','This is test','2013-04-10 18:50:03',1,0,0);
insert into `conversations` (`id`,`user_id`,`title`,`created`,`last_message_id`,`allow_add`,`count`) values (2,'20','javed','2013-04-10 18:52:55',8,0,0);
insert into `conversations` (`id`,`user_id`,`title`,`created`,`last_message_id`,`allow_add`,`count`) values (3,'20','ree','2013-04-11 14:46:06',9,0,0);
insert into `conversations` (`id`,`user_id`,`title`,`created`,`last_message_id`,`allow_add`,`count`) values (4,'21','User to Admin','2013-04-11 19:59:06',14,0,0);
insert into `conversations` (`id`,`user_id`,`title`,`created`,`last_message_id`,`allow_add`,`count`) values (5,'21','User to Admin 1','2013-04-11 20:14:27',16,0,0);

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `id` int(11) NOT NULL auto_increment,
  `description` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (1,'User Management',2130706433,1,'2013-04-19 20:33:14','2013-04-24 14:44:06');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (2,'Acl Management',2130706433,1,'2013-04-19 21:18:13','2013-04-24 14:44:20');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (3,'Group Management',2130706433,1,'2013-04-19 21:22:18','2013-04-24 14:44:23');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (4,'Plugins',2130706433,1,'2013-04-19 21:26:44','2013-04-24 14:44:26');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (5,'Others',2130706433,1,'2013-04-22 16:13:27','2013-04-24 14:44:29');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (7,'Test Feature',2130706433,0,'2013-04-24 16:05:54','2013-04-24 16:11:26');
insert into `features` (`id`,`description`,`ip_address`,`is_active`,`created`,`modified`) values (8,'Feature Management',2130706433,1,'2013-04-24 21:54:03','2013-04-24 21:54:03');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `is_active` tinyint(4) NOT NULL default '1',
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

insert into `groups` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (1,'Super Admin',2130706433,1,'2013-04-04 15:59:31','2013-04-24 20:15:22');
insert into `groups` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (2,'Admin',2130706433,1,'2013-04-04 15:59:41','2013-04-24 20:15:35');
insert into `groups` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (4,'User',2130706433,1,'2013-04-22 22:34:21','2013-04-24 20:15:39');
insert into `groups` (`id`,`name`,`ip_address`,`is_active`,`created`,`modified`) values (5,'testing',2130706433,0,'2013-04-24 19:52:50','2013-04-24 20:18:44');

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(50) default NULL,
  `body` text,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert into `posts` (`id`,`title`,`body`,`created`,`modified`) values (1,'The title','This is the post body.','2012-12-18 14:28:30','2012-12-18 10:14:07');
insert into `posts` (`id`,`title`,`body`,`created`,`modified`) values (2,'A title once again','And the post body follows.','2012-12-18 14:28:30',NULL);
insert into `posts` (`id`,`title`,`body`,`created`,`modified`) values (3,'Title strikes back','This is really exciting! Not.','2012-12-18 14:28:30',NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert into `roles` (`id`,`name`,`is_active`,`created`,`modified`) values (1,'Admin',1,'2012-05-07 16:25:44','2012-05-07 16:25:44');
insert into `roles` (`id`,`name`,`is_active`,`created`,`modified`) values (2,'User',1,'2012-05-07 16:25:44','2012-05-07 16:25:44');

/*Table structure for table `tmps` */

DROP TABLE IF EXISTS `tmps`;

CREATE TABLE `tmps` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `target_id` varchar(255) NOT NULL COMMENT 'string field holding the unique ID of a record for e.g registration',
  `type` enum('registration','forgot password') NOT NULL,
  `data` mediumtext NOT NULL,
  `expire_at` date NOT NULL COMMENT 'Date on which the row will be expired / Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `tmps` */

insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (21,'abc@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"abc@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:31:16','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (23,'def@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"def@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:32:50','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (24,'dsf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"dsf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:36:38','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (25,'sdf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:38:36','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (26,'sdd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:38:45','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (27,'sdfsd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfsd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:39:30','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (28,'dsfd@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"dsfd@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:43:48','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (29,'sdfsad@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfsad@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:44:37','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (30,'sadfsdf@gmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sadfsdf@gmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 18:47:33','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (31,'sdfsad@yahoo.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdf\",\"username\":\"sdfsad@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 19:17:19','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (32,'dsfsdaf@yahoo.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdf\",\"username\":\"dsfsdaf@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-12','2013-04-12 19:19:59','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (33,'sdfasdfsda@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfasdfsda@sdkfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"aksdfj\"}','2013-07-12','2013-04-12 19:20:55','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (34,'asdfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"asdfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:23:56','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (35,'sdfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:26:36','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (36,'ssadfasdf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"ssadfasdf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:28:30','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (37,'sdfsadf@sdkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfsadf@sdkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:29:23','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (38,'sdfsadf@sddkfj.com','registration','{\"first_name\":\"sdf\",\"last_name\":\"sdfsadf\",\"username\":\"sdfsadf@sddkfj.com\",\"password\":\"40fe417d6f742f7744155370ee86e32b\",\"confirm_password\":\"sdfasdfasdf\"}','2013-07-12','2013-04-12 19:35:45','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (39,'sdfjsadf@gkasdfj.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkasdfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-15','2013-04-15 12:51:09','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (40,'sdfjsadf@gkadfj.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadfj.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-15','2013-04-15 12:52:12','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (41,'sdfjsadf@gkadf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadf.com\",\"password\":\"fd7db3c4491f86d551d7512440b64be9\",\"confirm_password\":\"ashraf123\"}','2013-07-15','2013-04-15 12:52:45','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (42,'sdfjsadf@gkadsfdf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadsfdf.com\",\"password\":\"45e60bbcc2fb9461ceb909c1536d304a\",\"confirm_password\":\"shoaib124\"}','2013-07-15','2013-04-15 12:53:23','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (43,'sdfjsadf@gkadsdfsfdf.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"sdfjsadf@gkadsdfsfdf.com\",\"password\":\"45e60bbcc2fb9461ceb909c1536d304a\",\"confirm_password\":\"shoaib124\"}','2013-07-15','2013-04-15 13:44:54','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (44,'','registration','{\"confirm_password\":\"\",\"password\":\"2c6269575b291018dc91ed7ed4158603\"}','2013-07-15','2013-04-15 17:11:55','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (45,'tr.ephlux@gmail.com','registration','{\"first_name\":\"Tahir\",\"last_name\":\"Rasheed\",\"username\":\"tr.ephlux@gmail.com\",\"password\":\"68c91a594329677d6041c1c899c85272\"}','2013-07-17','2013-04-17 08:19:56','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (46,'tahir_uf@hotmail.com','registration','{\"first_name\":\"Tahir\",\"last_name\":\"Rasheed\",\"username\":\"tahir_uf@hotmail.com\",\"password\":\"68c91a594329677d6041c1c899c85272\"}','2013-07-17','2013-04-17 08:24:07','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (47,'javeed.nedian@hotmail.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"javeed.nedian@hotmail.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-17','2013-04-17 16:17:39','0000-00-00 00:00:00');
insert into `tmps` (`id`,`target_id`,`type`,`data`,`expire_at`,`created`,`modified`) values (48,'javeeed_neduet@yahoo.com','registration','{\"first_name\":\"javed\",\"last_name\":\"javed\",\"username\":\"javeeed_neduet@yahoo.com\",\"password\":\"54b4e5b5ff0b9c146b5c696a3587ad82\",\"confirm_password\":\"javed1234\"}','2013-07-17','2013-04-17 16:23:59','0000-00-00 00:00:00');

/*Table structure for table `tusers` */

DROP TABLE IF EXISTS `tusers`;

CREATE TABLE `tusers` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(50) default NULL,
  `facebook_id` varchar(20) default NULL,
  `twitter_id` varchar(20) default NULL,
  `password` varchar(50) default NULL,
  `role` varchar(20) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tusers` */

insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (1,'admin','0',NULL,'admin','admin',NULL,'2013-01-10 08:36:28');
insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (2,'tahir','0',NULL,'tahir','admin','2012-12-31 13:12:14','2012-12-31 13:12:14');
insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (6,'Muhammad Javed','100000691275728',NULL,NULL,NULL,'2013-01-16 12:32:12','2013-01-16 12:32:12');
insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (7,'Ayan Ahmed','100004317392698',NULL,NULL,NULL,'2013-01-16 13:08:28','2013-01-16 13:08:28');
insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (9,'Muhammad Javed',NULL,'430408044',NULL,NULL,'2013-01-22 08:22:29','2013-01-22 08:22:29');
insert into `tusers` (`id`,`username`,`facebook_id`,`twitter_id`,`password`,`role`,`created`,`modified`) values (10,'fahad tester',NULL,'714112616',NULL,NULL,'2013-01-22 10:23:32','2013-01-22 10:23:32');

/*Table structure for table `user_social_accounts` */

DROP TABLE IF EXISTS `user_social_accounts`;

CREATE TABLE `user_social_accounts` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `user_id` bigint(11) unsigned NOT NULL,
  `link_id` bigint(11) NOT NULL COMMENT 'ID of that Application, e.g: Facebook ID, Twitter ID, LinkedIn ID[99999999999]',
  `email_address` varchar(50) default NULL,
  `image_url` varchar(255) default NULL,
  `type_id` enum('facebook','twitter','linkedin') NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `access_token_secret` varchar(255) default NULL,
  `is_valid_token` tinyint(1) NOT NULL default '1',
  `screen_name` varchar(255) NOT NULL,
  `extra` mediumtext COMMENT 'String field to store JSON to store any extra information',
  `status` enum('active','delete') NOT NULL default 'active',
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `user_social_accounts` */

insert into `user_social_accounts` (`id`,`user_id`,`link_id`,`email_address`,`image_url`,`type_id`,`access_token`,`access_token_secret`,`is_valid_token`,`screen_name`,`extra`,`status`,`ip_address`,`created`,`modified`) values (1,35,100001124133650,'yousuf.qureshi@ephlux.com','http://profile.ak.fbcdn.net/hprofile-ak-prn1/274101_100001124133650_382477_q.jpg','facebook','BAAHWj2l5sVQBAB1Mbp5Sn9pUIX9FZCZBBpCopPOWBUsinuGdny76MCmxwFWMraYf0SFCUbgwRtToE9lRQORn09F1WTLkIYCDhvhZBvOeqbmmJmL09663MnsJsQItdFDLKWaPQHX3jmLCwQmEQdZBnjF6GCWgEKx2ACAQaYUtlyELbnhaJbNrUrdJI8VyAJ8ZD',NULL,1,'ephlux.pwm',NULL,'active',2130706433,'2013-04-11 20:53:11','2013-04-11 20:53:11');
insert into `user_social_accounts` (`id`,`user_id`,`link_id`,`email_address`,`image_url`,`type_id`,`access_token`,`access_token_secret`,`is_valid_token`,`screen_name`,`extra`,`status`,`ip_address`,`created`,`modified`) values (2,35,84262925,NULL,'http://a0.twimg.com/profile_images/713544938/lala_peshowri_normal.jpg','twitter','84262925-3oNKnXAW0mocvtOZQHn2FEToBZAuLEdbXcHGm5TY','fIc7oNmGU1HBzLKPWJCy1JEchhgJRKhbK1Cu5hqUjVU',1,'ephlux_shahzad',NULL,'active',2130706433,'2013-04-11 21:00:09','2013-04-11 21:00:09');
insert into `user_social_accounts` (`id`,`user_id`,`link_id`,`email_address`,`image_url`,`type_id`,`access_token`,`access_token_secret`,`is_valid_token`,`screen_name`,`extra`,`status`,`ip_address`,`created`,`modified`) values (9,53,100001124133650,'yousuf.qureshi@ephlux.com','http://profile.ak.fbcdn.net/hprofile-ak-prn1/274101_100001124133650_382477_q.jpg','facebook','AAAHWj2l5sVQBACMDwvQwhq88LMEs3CkPGsx8gEgPEAdZCOEL8AZBJwyCB274pZBzP4Hkm8wo3MqUFPFIzIyiLp0tY3rT07eTUgStHzzLQZDZD',NULL,1,'ephlux.pwm',NULL,'active',2130706433,'2013-04-17 21:01:18','2013-04-17 21:01:18');
insert into `user_social_accounts` (`id`,`user_id`,`link_id`,`email_address`,`image_url`,`type_id`,`access_token`,`access_token_secret`,`is_valid_token`,`screen_name`,`extra`,`status`,`ip_address`,`created`,`modified`) values (13,55,225699082,'a6@test.com','http://a0.twimg.com/profile_images/3442078187/79033acbc1832b385b76f77b40383c70_bigger.jpeg','twitter','225699082-USF4rxtWTiWTknSBMGo3zbFefWeqznVF9aHjTr8g',NULL,1,'anj_kh3',NULL,'active',2130706433,'2013-04-18 20:23:17','2013-04-18 20:23:17');

/*Table structure for table `user_subjects` */

DROP TABLE IF EXISTS `user_subjects`;

CREATE TABLE `user_subjects` (
  `id` int(5) NOT NULL auto_increment,
  `subject` varchar(50) NOT NULL,
  `total_marks` int(5) NOT NULL,
  `marks_obtained` float NOT NULL,
  `grade` varchar(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_subjects` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `username` varchar(50) NOT NULL COMMENT 'username is basically email address',
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `ip_address` int(10) unsigned default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  `status` enum('active','disabled','deleted','suspended') NOT NULL default 'active',
  `conversation_group_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (17,'azhar','javed','hussainadse@gmail.com','dcd7f7b999bd8b85358fd30f8545437f',4,2130706433,'2013-03-22 19:59:10','2013-04-16 18:19:08','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (20,'Shakeel','Uddin','shakeel@test.com','68c91a594329677d6041c1c899c85272',1,2130706433,'2013-04-01 12:46:52','2013-04-09 16:41:50','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (21,'ahmed',NULL,'user@test.com','68c91a594329677d6041c1c899c85272',4,2130706433,'2013-04-08 15:00:07','2013-04-08 15:00:07','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (31,'javed','javed','javed_neduet@yahoo.com','54b4e5b5ff0b9c146b5c696a3587ad82',4,2130706433,'2013-04-12 16:45:23','2013-04-12 16:45:23','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (35,'Jason','Borne','anasanjaria@gmail.com','604a62677a8d14ad85b7bbf703b535e1',4,2130706433,'2013-04-12 17:17:32','2013-04-12 17:17:32','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (53,'Ephlux','Pwm','yousuf.qureshi@ephlux.com','0212ba62093c988a2a7af82ed35bf2a5',4,2130706433,'2013-04-17 21:01:18','2013-04-17 21:01:18','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (55,'John','Mike','a6@test.com','61c209e62381891077d1782e179f2230',4,2130706433,'2013-04-18 20:23:17','2013-04-18 20:23:17','active',0);
insert into `users` (`id`,`first_name`,`last_name`,`username`,`password`,`group_id`,`ip_address`,`created`,`modified`,`status`,`conversation_group_id`) values (57,'Shakeel','Uddin','zuhair@test.com','68c91a594329677d6041c1c899c85272',2,2130706433,'2013-04-01 12:46:52','2013-04-09 16:41:50','active',0);

/* Procedure structure for procedure `create_group` */

drop procedure if exists `create_group`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_group`(IN name VARCHAR(100) ,IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN

insert into groups(

groups.name,

groups.ip_address,

groups.created,

groups.modified

) values (

name,

ip_address,

created,

created 
);

SET `status` = LAST_INSERT_ID();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `create_user` */

drop procedure if exists `create_user`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user`(IN `first_name` VARCHAR(255), IN `last_name` VARCHAR(255), IN `username` VARCHAR(50), IN `password` VARCHAR(255), IN `group_id` INT(11), IN `created` DATETIME,IN `ip_address` INT(10), OUT `status` INT)
BEGIN

INSERT INTO users

(

users.first_name                  , 

users.last_name                   , 

users.username                    , 

users.password                    ,

users.group_id                    ,

users.ip_address                  ,

users.created	             ,

users.modified

)

VALUES 

( 

first_name                       , 

last_name                        , 

username                         ,  

password                         ,

group_id                         ,

ip_address                       ,

created                          ,

created

) ; 

SET `status` = LAST_INSERT_ID();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `delete_tmp_record` */

drop procedure if exists `delete_tmp_record`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_tmp_record`(IN `id` INT(5),OUT `status` INT)
    NO SQL
BEGIN 

DELETE FROM tmps

WHERE  tmps.id = id ; 

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_all_arosacos_by_group_id` */

drop procedure if exists `get_all_arosacos_by_group_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_arosacos_by_group_id`(IN group_id INT(10))
BEGIN

SELECT 	ArosAco.id,ArosAco.aco_id

FROM groups AS `Group`

LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key

LEFT JOIN `aros_acos` AS ArosAco ON ArosAco.aro_id = Aro.id

WHERE Group.id = group_id;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_all_permissions` */

drop procedure if exists `get_all_permissions`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_permissions`()
BEGIN

SELECT 

Child.id,Child.alias,

AuthActionMap.description,AuthActionMap.id,

Feature.id,Feature.description,

Parent.id,Parent.alias

FROM `acos` AS Child

LEFT JOIN `acos` AS `Parent` ON (`Parent`.`id` = `Child`.`parent_id`)

LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.aco_id = Child.id

LEFT JOIN features AS Feature ON Feature.id = AuthActionMap.feature_id AND Feature.is_active = 1

WHERE `Parent`.`parent_id` IS NOT NULL;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_feature_by_feature_id` */

drop procedure if exists `get_feature_by_feature_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_feature_by_feature_id`(IN feature_id INT(11))
BEGIN

select 	Feature.id, Feature.description, Feature.created

from `features` AS Feature

WHERE Feature.id = feature_id;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_feature_details_by_feature_id` */

drop procedure if exists `get_feature_details_by_feature_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_feature_details_by_feature_id`(IN feature_id INT(11))
BEGIN

SELECT 	

Feature.id,Feature.description,Feature.created,

AuthActionMap.id,AuthActionMap.description,AuthActionMap.created

FROM features AS Feature

LEFT JOIN auth_action_maps AS AuthActionMap ON AuthActionMap.feature_id = Feature.id

WHERE Feature.id = feature_id AND Feature.is_active = 1;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_group_by_group_id` */

drop procedure if exists `get_group_by_group_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_group_by_group_id`(IN group_id INT(11))
BEGIN

select 	Group.id, Group.name, Group.created

from `groups` AS `Group`

WHERE Group.id = group_id;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_group_details_by_group_id` */

drop procedure if exists `get_group_details_by_group_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_group_details_by_group_id`(IN group_id INT(11))
BEGIN

SELECT 	

Group.id,Group.name,Group.created,

User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status

FROM groups AS `Group`

LEFT JOIN users AS `User` ON User.group_id = Group.id AND User.status = 'active'

WHERE Group.id = group_id AND Group.is_active = 1;

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_user_by_email` */

drop procedure if exists `get_user_by_email`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_by_email`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 

SELECT User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status

FROM users AS `User` 

WHERE User.username = email AND User.status = 'active'; 

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_user_details` */

drop procedure if exists `get_user_details`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_details`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 

SELECT User.*

FROM users AS User 

WHERE User.username = email AND User.status = 'active'; 

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_user_details_by_user_id` */

drop procedure if exists `get_user_details_by_user_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_details_by_user_id`(IN `user_id` INT(5))
    NO SQL
BEGIN 

SELECT 

User.id,User.first_name,User.last_name,User.username,User.group_id,User.created,User.status , 

UserSocialAccount.id, UserSocialAccount.link_id, UserSocialAccount.email_address, UserSocialAccount.image_url, 

UserSocialAccount.type_id,UserSocialAccount.is_valid_token, UserSocialAccount.screen_name, UserSocialAccount.status, 

UserSocialAccount.created,

Group.id, Group.name, Group.created

FROM users AS `User`

LEFT JOIN user_social_accounts AS UserSocialAccount ON UserSocialAccount.user_id = `User`.id AND UserSocialAccount.status = 'active'

LEFT JOIN groups AS `Group` ON Group.id = User.group_id AND Group.is_active = 1

WHERE `User`.id = user_id AND `User`.`status` = 'active';

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_user_social_account_data` */

drop procedure if exists `get_user_social_account_data`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_social_account_data`(IN `id` INT(5))
    NO SQL
BEGIN 

SELECT UserSocialAccount.*

FROM user_social_accounts AS UserSocialAccount

WHERE UserSocialAccount.user_id = id AND UserSocialAccount.status = 'active';

END$$

DELIMITER ;$$

/* Procedure structure for procedure `get_user_tmp_data` */

drop procedure if exists `get_user_tmp_data`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_tmp_data`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100))
    NO SQL
BEGIN 

SELECT Tmp.*

FROM tmps AS Tmp 

WHERE Tmp.target_id = target_id AND Tmp.type = type; 

END$$

DELIMITER ;$$

/* Procedure structure for procedure `is_unverified_email_exists` */

drop procedure if exists `is_unverified_email_exists`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `is_unverified_email_exists`(IN `email` VARCHAR(100))
    NO SQL
BEGIN 

SELECT Tmp.target_id

FROM tmps AS Tmp

WHERE Tmp.target_id = email AND Tmp.type = 'registration';

END$$

DELIMITER ;$$

/* Procedure structure for procedure `remove_arosacos_by_group_id` */

drop procedure if exists `remove_arosacos_by_group_id`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_arosacos_by_group_id`(IN group_id INT(11) , OUT `status` INT)
BEGIN

DELETE FROM `aros_acos` 

WHERE aros_acos. aro_id = (

SELECT 	Aro.id

FROM groups AS `Group`

LEFT JOIN aros AS Aro ON Group.id = Aro.foreign_key

WHERE Group.id = group_id

);

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `remove_feature` */

drop procedure if exists `remove_feature`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_feature`(

IN feature_id int(11),

IN `modified` DATETIME,

IN `ip_address` INT(10),

OUT `status` INT)
BEGIN

IF EXISTS (SELECT COUNT(*) AS `count` FROM `features` AS `Feature` WHERE `Feature`.`id` = feature_id AND Feature.is_active = 1) THEN	

UPDATE `features` AS Feature

SET Feature.is_active = 0,

Feature.modified = modified,

Feature.ip_address = ip_address

WHERE Feature.id = feature_id ;

END IF;

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `remove_group` */

drop procedure if exists `remove_group`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_group`(

IN group_id int(11),

IN `modified` DATETIME,

IN `ip_address` INT(10),

OUT `status` INT)
BEGIN

IF EXISTS (SELECT COUNT(*) AS `count` FROM `groups` AS `Group` WHERE `Group`.`id` = group_id AND Group.is_active = 1) THEN	

UPDATE `groups` AS `Group`

SET Group.is_active = 0,

Group.modified = modified,

Group.ip_address = ip_address

WHERE Group.id = group_id ;

END IF;

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `reset_password` */

drop procedure if exists `reset_password`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `reset_password`(IN `password` VARCHAR(100), IN `id` INT(5), OUT `status` INT)
    NO SQL
BEGIN

UPDATE users

SET    

users.password = `password`                    

WHERE  users.id = id;

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `save_tmp_record` */

drop procedure if exists `save_tmp_record`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `save_tmp_record`(IN `target_id` VARCHAR(100), IN `type` VARCHAR(100), IN `data` VARCHAR(500), IN `expire_at` DATE, IN `created` DATETIME, OUT `status` INT)
    NO SQL
BEGIN 

INSERT INTO tmps

(

tmps.target_id                  , 

tmps.type                       , 

tmps.data                       , 

tmps.expire_at                  ,

tmps.created

)

VALUES 

( 

target_id                    , 

type                         , 

data                         ,  

expire_at                    ,

created

) ; 

SET `status` = LAST_INSERT_ID();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `save_user_record` */

drop procedure if exists `save_user_record`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `save_user_record`(IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `username` VARCHAR(100), IN `password` VARCHAR(100), IN `role_id` INT(5), IN `created` DATETIME, OUT `status` VARCHAR(255))
    NO SQL
BEGIN 

DECLARE str VARCHAR(255);  

SET str = 'success';

SET status = str;

INSERT INTO users

(

users.first_name                  , 

users.last_name                   , 

users.username                    , 

users.password                    ,

users.role_id                     ,

users.created

)

VALUES 

( 

first_name                       , 

last_name                        , 

username                         ,  

password                         ,

role_id                          ,

created

) ; 

END$$

DELIMITER ;$$

/* Procedure structure for procedure `signup_with_social_account` */

drop procedure if exists `signup_with_social_account`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `signup_with_social_account`(IN xml_data varchar(1000), OUT `status` INT)
BEGIN

DECLARE user_id BIGINT(11);

DECLARE first_name VARCHAR(255);

DECLARE last_name VARCHAR(255);

DECLARE username VARCHAR(50); 
DECLARE pwd VARCHAR(255);

DECLARE group_id INT(11);

DECLARE ip_address  INT(10); 
DECLARE created DATETIME; 
DECLARE link_id BIGINT(11); 
DECLARE image_url VARCHAR(255);

DECLARE type_id VARCHAR(20);

DECLARE access_token VARCHAR(255);

DECLARE screen_name VARCHAR(255);

SELECT ExtractValue(xml_data, '/response/User/first_name') INTO first_name;

SELECT ExtractValue(xml_data, '/response/User/last_name') INTO last_name;

SELECT ExtractValue(xml_data, '/response/User/username') INTO username;

SELECT ExtractValue(xml_data, '/response/User/password') INTO pwd;

SELECT ExtractValue(xml_data, '/response/User/group_id') INTO group_id;

SELECT ExtractValue(xml_data, '/response/User/ip_address') INTO ip_address;

SELECT ExtractValue(xml_data, '/response/User/created') INTO created;

SELECT ExtractValue(xml_data, '/response/UserSocialAccount/link_id') INTO link_id;

SELECT ExtractValue(xml_data, '/response/UserSocialAccount/image_url') INTO image_url;

SELECT ExtractValue(xml_data, '/response/UserSocialAccount/type_id') INTO type_id;

SELECT ExtractValue(xml_data, '/response/UserSocialAccount/access_token') INTO access_token;

SELECT ExtractValue(xml_data, '/response/UserSocialAccount/screen_name') INTO screen_name;

call `create_user`(first_name,last_name,username,pwd,group_id,created,ip_address,@status);

SELECT @status INTO user_id;

INSERT INTO `user_social_accounts` 

(	

user_social_accounts.user_id,

user_social_accounts.link_id,

user_social_accounts.email_address,

user_social_accounts.image_url,

user_social_accounts.type_id,

user_social_accounts.access_token,

user_social_accounts.screen_name,

user_social_accounts.ip_address,

user_social_accounts.created,

user_social_accounts.modified

)values (

user_id,

link_id,

username,

image_url,

type_id,

access_token,

screen_name,

ip_address,

created,

created

);

SET `status` = LAST_INSERT_ID();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `sync_aam_aco` */

drop procedure if exists `sync_aam_aco`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sync_aam_aco`(OUT `status` INT)
BEGIN

DELETE FROM auth_action_maps WHERE NOT aco_id IN ( SELECT Aco.id FROM `acos` AS Aco );

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `test_multi_sets` */

drop procedure if exists `test_multi_sets`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin

select user() as first_col;

select user() as first_col, now() as second_col;

select user() as first_col, now() as second_col, now() as third_col;

end$$

DELIMITER ;$$

/* Procedure structure for procedure `update_feature` */

drop procedure if exists `update_feature`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_feature`(

IN feature_id INT(11),

IN description VARCHAR(100),

IN `modified` DATETIME,

IN `ip_address` INT(10),

OUT `status` INT)
BEGIN

UPDATE `features` AS Feature

SET Feature.description = description,

Feature.modified = modified,

Feature.ip_address = ip_address

WHERE Feature.id = feature_id AND Feature.is_active = 1;

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `update_group` */

drop procedure if exists `update_group`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_group`(

IN group_id INT(11),

IN name VARCHAR(100),

IN `modified` DATETIME,

IN `ip_address` INT(10),

OUT `status` INT)
BEGIN

UPDATE `groups` AS `Group`

SET Group.name = name,

Group.modified = modified,

Group.ip_address = ip_address

WHERE Group.id = group_id AND Group.is_active = 1;

SET `status` = ROW_COUNT();

END$$

DELIMITER ;$$

/* Procedure structure for procedure `verify_login` */

drop procedure if exists `verify_login`;

DELIMITER $$;

CREATE DEFINER=`root`@`localhost` PROCEDURE `verify_login`(IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
BEGIN 

SELECT User.id

FROM users AS User 

WHERE User.username = email AND User.password = pass AND User.status = 'active'; 

END$$

DELIMITER ;$$

SET SQL_MODE=@OLD_SQL_MODE;