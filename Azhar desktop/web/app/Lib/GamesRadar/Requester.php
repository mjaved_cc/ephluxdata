<?php
require_once "Client.php";

/**
 * RESTful client to GamesRadar resources
 */
class Requester
{
	/**
	 * GamesRadarAPI endpoint URL
	 */
	const ENDPOINT_URL = 'http://api.gamesradar.com/';

	/**
	 * CURL handler
	 *
	 * @var resource
	 */
	private $ch;
	
	/**
	 * Free CURL handler resource
	 */
	public function __desctruct()
	{
		if ($this->ch) {
			curl_close($this->ch);
		}
	}

	/**
	 * Lazy access to CURL handler.
	 *
	 * By default CURL not initialized, it will be initialized before first
	 * request.
	 *
	 * @return resource
	 */
	private function getCurl()
	{
		if (null === $this->ch) {
			$this->ch = curl_init();
		}
		return $this->ch;
	}

	/**
	 * @param string $resource
	 * @param array $args
	 */
	
	public function request($resource, array $args = array())
	{
/*		$filepath = self::ENDPOINT_URL . $resource . '?' . http_build_query($args);
		$dom = new DOMDocument();
		$dom->load($filepath);
		$dom->saveXML();
 		//$x = $dom->documentElement;
      	$resource = $dom->getElementsByTagName($resource);
		$data = $resource->childNodes;
   		print_r( $filepath );
        foreach($resource as $items){
				print $items->nodeName . ' - ' . $items->nodeValue.' <br />';
		}
*/		
		// make URL
		$url = self::ENDPOINT_URL . $resource . '?' . http_build_query($args);
		//print $url; die();
		// set up CURL handler
		$ch = $this->getCurl();

		curl_setopt_array($ch, array(
			CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => true,
		));

		// execute request
		$response = curl_exec($ch);
		//print '<pre>'; print_r($response); die();
		$xml = simplexml_load_string($response,null, LIBXML_NOCDATA);
		
		return $xml;

	}
	
}