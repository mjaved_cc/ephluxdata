<?php
require_once "Game.php";

/**
* @package Media Entity
*/

class Media extends ApiGame
{
	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var Platform
	 */
	public $platform;
}