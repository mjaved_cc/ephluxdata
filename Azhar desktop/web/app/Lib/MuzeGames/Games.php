<?php
/**
* @package Game Entity
*/

class Games
{
	/**
	 * @var int
	 */
	public $api;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var int
	 */
	public $titleID;

	/**
	 * @var string
	 */
	public $desc;

	/**
	 * @var string
	 */
	public $price;

	/**
	 * @var int
	 */
	public $console;

	/**
	 * @var int
	 */
	public $genre;

	/**
	 * @var string
	 */
	public $screenshot;

	/**
	 * @var string
	 */
	public $publisher;

	/**
	 * @var string
	 */
	public $developers;

	/**
	 * @var int
	 */
	public $barcode;

	/**
	 * @var string
	 */
	public $image;

	/**
	 * @var datetime
	 */
	public $releaseDate;

}