<?php

class DtUserChat {

	private $_fields = array();
	private $_allowed_keys = array('id','chatlog_id','sender_id','receiver_id','message','status','created');

	public function __construct($data = null) {

		$this->populate_data($data);
	}

	function populate_data($data) {
		foreach ($data as $key => $value) {
			if (in_array($key, $this->_allowed_keys))
				$this->$key = (!empty($value)) ? $value : '0';
		}
	}

	function __set($key, $value) {

		switch ($key) {
			case 'created';
			case 'modified';
				$this->_fields[$key] = new DtDate($value);
				break;


			default:
				$this->_fields[$key] = $value;
		}
	}

	function __get($key) {

		try {
			if ($key == 'created' || $key == 'modified') {
				return $this->_fields[$key];
			} else if (isset($this->_fields[$key]))
				return $this->_fields[$key];
		} catch (Exception $e) {
			CakeLog::write('debug', $e->getMessage());
		}
	}

	function get() {

		return $this->_fields;
	}



	function get_field() {

		return array(
			'id' => $this->id,
			'chatlog_id' => $this->chatlog_id,
			'sender_id' => $this->sender_id,
			'receiver_id' => $this->receiver_id,	
			'message' => $this->message,	
			'status' => $this->status,	
			'created' => $this->created
		);
	}

}