<?php

App::uses('AppModel', 'Model');
App::uses('GCM', 'Lib');

/**
 * Deal Model
 *
 */
class DealsChatlog extends AppModel {

	function save_deal_message($data) {

		$this->query("call save_user_deal_message( '" . $data['deal_id'] . "','" . $data['id_sender'] . "','" . $data['id_receiver'] . "','" . $data['user_message'] . "',@status )");

		$status = $this->get_status();

		if (!empty($status)) {
			
			$sender = $this->query("call get_user_by_id( '" . $data['id_sender'] . "' )");
			$regid = $this->query("call get_user_gcm_regid( '" . $data['id_receiver'] . "' )");
			
			if(!empty($regid[0]['users']['gcm_regid'])) {
			
			$gcm = new GCM();

			$gcm->send_notification($regid[0]['users']['gcm_regid'], '{"header":{"action":"deal_message",  "message":"' . $sender[0]['users']['nickname'] . ' sent a new deal message"},"body":{"deal_id":"' . $data['deal_id'] . '","member_id":"' . $data['id_sender'] . '", "user_name":"' . $sender[0]['users']['nickname'] . '", "image_url":"' . SITE_URI . $sender[0]['users']['image_url'] . '"}}');
			
			}

		}
		
		return $this->get_status();
	}

	function user_deal_chat($deal_id) {

		App::uses('DtDealChatlog', 'Lib/DataTypes');

		$data = array();

		$deal_chat = $this->query("call list_deal_chat( '" . $deal_id . "',@status )");

		if (!empty($deal_chat) && $deal_chat != 1) {

			foreach ($deal_chat as $key => $value) {  //pr($value); exit;
				$datetime = explode(' ', $value['deals_chatlogs']['created']);

				$obj_deal_chat = new DtDealChatlog($value['deals_chatlogs']);
				$data[$key]['DealChat'] = $obj_deal_chat->get_field();
				$data[$key]['DealChat']['date'] = $datetime[0];
				$data[$key]['DealChat']['time'] = $datetime[1];
			}
		}

		return $data;
	}

	function unread_msg_count($receiver_id) {

		$count = $this->query("call count_user_unread_deal_messages('" . $receiver_id . "' )");

		return $count[0][0];
	}

	function mark_msg_read($deal_id, $receiver_id) {

		$this->query("call user_mark_deal_message_read('" . $deal_id . "','" . $receiver_id . "')");

		return 'success';
	}

}
