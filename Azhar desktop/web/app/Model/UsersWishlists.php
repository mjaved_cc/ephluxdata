<?php

App::uses('AppModel', 'Model');

/**

 * User Model

 *

 */
class UsersWishlists extends AppModel {

	function get_user_wishlists($user_id) {



		App::uses('DtGame', 'Lib/DataTypes');

		App::uses('DtUserWishlist', 'Lib/DataTypes');

		App::uses('DtGenre', 'Lib/DataTypes');



		$user_wishlists = $this->query("call get_user_all_wishlist( '" . $user_id . "' )"); //pr($user_wishlists); exit;



		$data = array();



		if (!empty($user_wishlists)) {



			foreach ($user_wishlists as $key => $value) {



				$obj_game = new DtGame($value['games']);

				$obj_game->add_wishlist($value['users_wishlists']);

				$obj_game->add_genre($value['genres']);



				$data[$key]['Game'] = $obj_game->get_field();

				$data[$key]['Wishlist'] = $obj_game->get_wishlist();

				$data[$key]['Genre'] = $obj_game->get_genre();
			}
		}



		return $data;
	}

	function save_wishlist_record($user_id, $trade, $game_ids) {

		$count = count($game_ids);

		$msg = "";

		$msg .= "Your request has been completed successfully.";

		$not_saved = 0;

		$id = '';
		$titles = '';
		$wish = '';
		$have = '';

		for ($i = 0; $i < $count; $i++) {

			$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "', '" . $game_ids[$i] . "')");

			if ($existance[0][0]['UserWish'] == 0 && $existance[0][0]['UserGame'] == 0) {

				$this->query("call save_user_wishlist_record(

			   '" . $user_id . "',

			   '" . $game_ids[$i] . "',

			   '" . $trade . "',

				   @status	

				   )");
			} else {

				$not_saved++;

				$title = $this->query("call get_game_detail_by_id('" . $game_ids[$i] . "')");

				$id .= $game_ids[$i] . ' ';
				//$titles .= $title[0]['Game']['title'].','; 

				if ($existance[0][0]['UserWish'] == 1) {
					$wish .= $title[0]['Game']['title'] . ',';
				}

				if ($existance[0][0]['UserGame'] == 1) {
					$have .= $title[0]['Game']['title'] . ',';
				}
			}
		}

		$wish = substr_replace($wish, " ", -1);
		$have = substr_replace($have, " ", -1);

		if (!empty($wish) && $not_saved > 0) {

			$msg .= "$wish Already exists in your wishlist. ";
		}

		if (!empty($have) && $not_saved > 0) {

			$msg .= "$have Already exists in your games";
		}

		return $msg;
	}

	function top_wanted_games($web, $start = null, $limit = null, $user_id = 0) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		App::uses('DtGame', 'Lib/DataTypes');



		$top_games = $this->query("call most_wanted_games('" . $web . "', '" . $start . "', '" . $limit . "', '" . $user_id . "')");



		$data = array();



		if (!empty($top_games)) {



			foreach ($top_games as $key => $value) {



				$obj_game = new DtGame($value['Game']);



				$data[$key]['Game'] = $obj_game->get_field();

				$data[$key]['Game']['id'] = $value['UserWishlist']['game_id'];

				//$data[$key]['Game']['release_date'] = $value[0]['release_date'];



				$merge_array = array('users_wanting' => $value[0]['Count']);



				$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}



		return $data;
	}

	function top_owned_games($web, $start = null, $limit = null, $user_id = 0) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		App::uses('DtGame', 'Lib/DataTypes');

		$top_games = $this->query("call most_owned_games('" . $web . "', '" . $start . "', '" . $limit . "', '" . $user_id . "')"); //pr($top_games); exit;

		$data = array();

		if (!empty($top_games)) {

			foreach ($top_games as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();

				$data[$key]['Game']['id'] = $value['UserGame']['game_id'];

				//$data[$key]['Game']['release_date'] = $value[0]['release_date'];

				$merge_array = array('users_owning' => $value[0]['Count']);

				$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}

		return $data;
	}

	function selling_game($user_id, $start = null, $limit = null, $dist = null) {

		$data = array();

		$user_games = $this->query("call get_all_games_by_user_id('" . $user_id . "')");

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		if ($dist == null) {
			$dist = 9999;
		}

		if (!empty($user_games)) {

			App::uses('DtUserWishlist', 'Lib/DataTypes');
			App::uses('DtUser', 'Lib/DataTypes');
			App::uses('DtGame', 'Lib/DataTypes');
			App::uses('DtUserGame', 'Lib/DataTypes');

			$game_count = 0;

			$games = $this->query("call deals_for_you_selling('" . $user_id . "', 0,'" . $start . "','" . $limit . "','" . $dist . "', @status)"); //pr($games); exit;

			$status = $this->query('SELECT @status');

			if ($status[0][0]['@status'] == 1) {

				foreach ($games as $key => $value) {

					$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $value['users_wishlists']['user_id'] . "', 
																						 '" . $value['users_wishlists']['game_id'] . "')");
					if ($existance[0][0]['UserGame'] == 0) {

						$obj_wishlist = new DtUserWishlist($value['users_wishlists']);
						$obj_user = new DtUser($value['users']);
						$obj_game = new DtGame($value['games']);
						$obj_user_game = new DtUserGame($value['users_games']);

						$data[$game_count]['SellingGame'] = array_merge($obj_user_game->get_field(), $obj_wishlist->get_field(), $obj_user->get_field(), $obj_game->get_field());

						$score = 500;

						if (!empty($data[$game_count]['SellingGame']['condition'])) {

							if ($data[$game_count]['SellingGame']['condition'] == 'poor') {

								$score = $score * 0.5;
							}

							if ($data[$game_count]['SellingGame']['condition'] == 'good') {

								$score = $score * 1;
							}

							if ($data[$game_count]['SellingGame']['condition'] == 'mint') {

								$score = $score * 1.5;
							}
						}

						if (!empty($data[$game_count]['SellingGame']['comes_with'])) {



							if ($data[$game_count]['SellingGame']['comes_with'] == 'box + manual + dvd') {

								$score = $score * 2;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'box + dvd') {

								$score = $score * 1;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'dvd only') {

								$score = $score * 0.5;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'manual + dvd') {

								$score = $score * 1;
							}
						}

						$amount = array('amount' => $score = $score / 50);

						//$data[$game_count]['SellingGame'] = array_merge($data[$game_count]['SellingGame'], $amount);

						$distance = $this->get_distance($user_id, $data[$key]['SellingGame']['user_id']);

						$data[$key]['SellingGame']['distance'] = $distance;

						$game_count++;
					}
				}
			}

			if ($game_count > 0) {

				return $data;
			} else {

				$game_count = 0;

				$data = array();

				$games = $this->query("call deals_for_you_selling('" . $user_id . "', 1, '" . $start . "', '" . $limit . "','" . $dist . "', @status)");

				$status = $this->query('SELECT @status');

				if ($status[0][0]['@status'] == 1) {

					foreach ($games as $key => $value) {

						$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $value['users_wishlists']['user_id'] . "',
																						 '" . $value['users_wishlists']['game_id'] . "')");

						if ($existance[0][0]['UserGame'] == 0) {

							$obj_wishlist = new DtUserWishlist($value['users_wishlists']);
							$obj_user = new DtUser($value['users']);
							$obj_game = new DtGame($value['games']);
							$obj_user_game = new DtUserGame($value['users_games']);

							$data[$key]['SellingGame'] = array_merge($obj_user_game->get_field(), $obj_wishlist->get_field(), $obj_user->get_field(), $obj_game->get_field());

							$score = 500;

							if (!empty($data[$key]['SellingGame']['condition'])) {

								if ($data[$key]['SellingGame']['condition'] == 'poor') {

									$score = $score * 0.5;
								}

								if ($data[$key]['SellingGame']['condition'] == 'good') {

									$score = $score * 1;
								}

								if ($data[$key]['SellingGame']['condition'] == 'mint') {

									$score = $score * 1.5;
								}
							}



							if (!empty($data[$key]['SellingGame']['comes_with'])) {

								if ($data[$key]['SellingGame']['comes_with'] == 'box + manual + dvd') {

									$score = $score * 2;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'box + dvd') {

									$score = $score * 1;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'dvd only') {

									$score = $score * 0.5;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'manual + dvd') {

									$score = $score * 1;
								}
							}

							$amount = array('amount' => $score = $score / 50);

							//$data[$key]['SellingGame'] = array_merge($data[$key]['SellingGame'], $amount);

							$distance = $this->get_distance($user_id, $data[$key]['SellingGame']['user_id']);

							$data[$key]['SellingGame']['distance'] = $distance;

							$game_count++;
						}
					}
				}

				if ($game_count > 0) {

					return $data;
				}
			}
		}



		return $data;
	}

	function ongoing_offers_for_have_game($user_id, $game_id) {

		$games = $this->selling_game($user_id);

		$data = array();

		$user_ids = array();

		foreach ($games as $key => $value) {

			if ($value['SellingGame']['game_id'] == $game_id) {

				$user_ids[] = $value['SellingGame']['user_id'];
			}
		}



		$count = count($user_ids);



		for ($i = 0; $i <= $count - 1; $i++) {



			foreach ($games as $key => $val) {



				if ($val['SellingGame']['user_id'] == $user_ids[$i]) {



					$data[$key] = $val;
				}
			}
		}



		return $data;
	}

	function update_wishlist_record($user_id, $game_id, $trade) {

		$this->query("call update_user_wishlist_record(

			   '" . $user_id . "',

			   '" . $game_id . "',

			   '" . $trade . "'

				   )");

		return 'success';
	}

}

