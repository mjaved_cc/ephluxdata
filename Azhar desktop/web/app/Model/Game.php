<?php

App::uses('AppModel', 'Model');
App::uses('GCM', 'Lib');

/**
 * User Model
 *
 */
class Game extends AppModel {
	/* public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {


	  //$order_clause = 'title DESC';

	  $results = $this->query('call get_all_games_web( "' . $page . '", "' . $limit . '")');
	  pr($results);
	  return $results;
	  }

	  /**
	 * Overridden paginateCount method
	 */
	/* public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
	  $this->recursive = $recursive;
	  $results = $this->query('call get_games_count()');
	  return count($results[0][0]['gameCount']);
	  } */

	function get_game_detail_by_id($game_id, $user_id) {

		$game_detail = $this->query("call get_game_detail_by_id(
			 '" . $game_id . "' )");

		$game_detail = $this->parse_gamedata($game_detail, $user_id);

		return $game_detail;
	}

	function get_game_detail_by_barcode($barcode, $user_id) {

		$game_detail = $this->query("call get_game_detail_by_barcode(
			 '" . $barcode . "' )");

		$game_detail = $this->parse_gamedata($game_detail, $user_id);

		return $game_detail;
	}

	function get_game_detail_by_title($title, $user_id, $start = null, $limit = null) {

		App::uses('DtGame', 'Lib/DataTypes');
		$data = array();

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}
		//print $title;
		$game_detail = $this->query("call get_game_detail_by_title('" . $title . "', '" . $start . "', '" . $limit . "' )");
		//pr($game_detail); die();

		if (!empty($game_detail)) {

			foreach ($game_detail as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();
				$data[$key]['Game']['release_date'] = $value[0]['release_date'];


				$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "', '" . $data[$key]['Game']['id'] . "')");

				$merge_array = array('in_user_games' => $existance[0][0]['UserGame'], 'in_user_wishlist' => $existance[0][0]['UserWish']);

				$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}

		return $data;
	}

	function parse_gamedata($game_detail, $user_id) {

		App::uses('DtGame', 'Lib/DataTypes');

		$data = array();

		if (!empty($game_detail)) {

			foreach ($game_detail as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();


				$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "', '" . $data[$key]['Game']['id'] . "')");

				$merge_array = array('in_user_games' => $existance[0][0]['UserGame'], 'in_user_wishlist' => $existance[0][0]['UserWish']);

				$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}

		return $data;
	}

	function get_all_games($web = null) {

		App::uses('DtGame', 'Lib/DataTypes');

		if ($web == 'true') {

			$game_detail = $this->query("call get_all_games_web()");
		} else {

			$game_detail = $this->query("call get_all_games(NULL)");
		}

		$data = array();

		if (!empty($game_detail)) {

			foreach ($game_detail as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();
			}
		}

		return $data;
	}

	function search_game_detail_by_title($search_keyword) {

		App::uses('DtGame', 'Lib/DataTypes');

		$game_detail = $this->query("call search_game_by_title('" . $search_keyword . "')");

		$data = array();

		if (!empty($game_detail)) {

			foreach ($game_detail as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();

				//$merge_array = array('users_wanting' => $value[0]['Count']);
				//$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}

		return $data;
	}

	function update_game_status($id, $status) {

		$this->query("call update_game_status('" . $status . "', '" . $id . "')");
	}

	function update_recored($data) {

		$this->query("call update_game_record('" . $data['id'] . "', '" . Sanitize::escape($data['title']) . "', '" . Sanitize::escape($data['desc']) . "', '" . Sanitize::escape($data['publisher']) . "', '" . Sanitize::escape($data['developers']) . "', '" . Sanitize::escape($data['google_play']) . "', '" . Sanitize::escape($data['amazon']) . "', '" . Sanitize::escape($data['jiggster_pick']) . "')");

		return 'success';
	}

	function delete_game_recored($id) {

		$this->query("call delete_game_record('" . $id . "')");

		return 'success';
	}

	function upcoming_and_new_releases($param, $start = null, $limit = null, $user_id = 0) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$data = array();

		App::uses('DtGame', 'Lib/DataTypes');

		$games = $this->query("call upcoming_and_new_releases('" . $param . "','" . $start . "','" . $limit . "','" . $user_id . "')");


		if (!empty($games)) {

			foreach ($games as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();
				$data[$key]['Game']['release_date'] = $value[0]['release_date'];
			}
		}

		return $data;
	}

	function save_game($data) {

		foreach ($data as $gameArrays) {
			foreach ($gameArrays as $gameArray) {

				$api = $gameArray->api;
				$title = Sanitize::escape($gameArray->title);
				$titleID = Sanitize::escape($gameArray->titleID);
				$desc = Sanitize::escape($gameArray->desc);
				$console = Sanitize::escape($gameArray->console);
				$console_id = $this->query("call get_genre_or_console(NULL, '" . $console . "')");
				$genre = Sanitize::escape($gameArray->genre);
				$genre_id = $this->query("call get_genre_or_console('" . $genre . "', NULL)");
				$screenshot = $gameArray->screenshot;
				$publisher = Sanitize::escape($gameArray->publisher);
				$developers = Sanitize::escape($gameArray->developers);
				$barcode = $gameArray->barcode;
				$image = $gameArray->image;
				$releaseDate = date("Y-m-d H:m:s", $gameArray->releaseDate);

				$api_id = $this->query("call check_api_id('" . $api . "', 'games')");
				//pr($console_id);
				if (empty($api_id)) {

					$this->query("call game_cron('" . $api . "',
													'" . $title . "',
													'" . $titleID . "',
													'" . $desc . "',
													'" . $console_id[0]['consoles']['id'] . "',
													'" . $genre_id[0]['genres']['id'] . "',
													'" . $publisher . "',
													'" . $developers . "',
													'" . $barcode . "',
													'" . $image . "',
													'" . $screenshot . "',
													'" . $releaseDate . "'
													)");
				}
			}
		}
	}

	function buying_game($user_id, $start = null, $limit = null, $dist = null) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtGame', 'Lib/DataTypes');
		App::uses('DtUserGame', 'Lib/DataTypes');

		$game_count = 0;
		$data = array();

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		if ($dist == null) {
			$dist = 9999;
		}

		$games = $this->query("call deals_for_you_buying('" . $user_id . "', 0 , '" . $start . "' ,'" . $limit . "', '" . $dist . "', @status1, @status2)");

		$status = $this->query('SELECT @status1');

		if ($status[0][0]['@status1'] == 1) {

			foreach ($games as $key => $value) {

				$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "',
												'" . $value['users_games']['game_id'] . "')");

				if ($existance[0][0]['UserGame'] == 0) {

					$obj_user_game = new DtUserGame($value['users_games']);
					$obj_user = new DtUser($value['users']);
					$obj_game = new DtGame($value['games']);

					$data[$key]['BuyingGame'] = array_merge($obj_user->get_field(), $obj_game->get_field(), $obj_user_game->get_field());

					$score = 500;

					if (!empty($data[$key]['BuyingGame']['condition'])) {

						if ($data[$key]['BuyingGame']['condition'] == 'poor') {
							$score = $score * 0.5;
						}
						if ($data[$key]['BuyingGame']['condition'] == 'good') {
							$score = $score * 1;
						}
						if ($data[$key]['BuyingGame']['condition'] == 'acceptable') {
							$score = $score * 1;
						}
						if ($data[$key]['BuyingGame']['condition'] == 'mint') {
							$score = $score * 1.5;
						}
					}

					if (!empty($data[$key]['BuyingGame']['comes_with'])) {

						if ($data[$key]['BuyingGame']['comes_with'] == 'box + manual + dvd') {
							$score = $score * 2;
						}
						if ($data[$key]['BuyingGame']['comes_with'] == 'box + dvd') {
							$score = $score * 1;
						}
						if ($data[$key]['BuyingGame']['comes_with'] == 'dvd only') {
							$score = $score * 0.5;
						}
						if ($data[$key]['BuyingGame']['comes_with'] == 'manual + dvd') {
							$score = $score * 1;
						}
					}

					$amount = array('amount' => $score = $score / 50);

					//$data[$key]['BuyingGame'] = array_merge($data[$key]['BuyingGame'], $amount);

					$distance = $this->get_distance($user_id, $data[$key]['BuyingGame']['user_id']);

					$data[$key]['BuyingGame']['distance'] = $distance;


					$game_count++;
				}
			}
		}
		if ($game_count > 0) {

			return $data;
		} else {

			$game_count = 0;
			$data = array();
			$games = $this->query("call deals_for_you_buying('" . $user_id . "', 1, '" . $start . "' ,'" . $limit . "','" . $dist . "', @status1, @status2)");

			$status = $this->query('SELECT @status2');

			if ($status[0][0]['@status2'] == 1) {

				foreach ($games as $key => $value) {

					$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "',
												'" . $value['users_games']['game_id'] . "')");

					if ($existance[0][0]['UserGame'] == 0) {

						$obj_user_game = new DtUserGame($value['users_games']);
						$obj_user = new DtUser($value['users']);
						$obj_game = new DtGame($value['games']);

						$data[$key]['BuyingGame'] = array_merge($obj_user->get_field(), $obj_game->get_field(), $obj_user_game->get_field());

						$score = 500;

						if (!empty($data[$key]['BuyingGame']['condition'])) {

							if ($data[$key]['BuyingGame']['condition'] == 'poor') {
								$score = $score * 0.5;
							}
							if ($data[$key]['BuyingGame']['condition'] == 'good') {
								$score = $score * 1;
							}
							if ($data[$key]['BuyingGame']['condition'] == 'acceptable') {
								$score = $score * 1;
							}
							if ($data[$key]['BuyingGame']['condition'] == 'mint') {
								$score = $score * 1.5;
							}
						}

						if (!empty($data[$key]['BuyingGame']['comes_with'])) {

							if ($data[$key]['BuyingGame']['comes_with'] == 'box + manual + dvd') {
								$score = $score * 2;
							}
							if ($data[$key]['BuyingGame']['comes_with'] == 'box + dvd') {
								$score = $score * 1;
							}
							if ($data[$key]['BuyingGame']['comes_with'] == 'dvd only') {
								$score = $score * 0.5;
							}
							if ($data[$key]['BuyingGame']['comes_with'] == 'manual + dvd') {
								$score = $score * 1;
							}
						}

						$amount = array('amount' => $score = $score / 50);

						//$data[$key]['BuyingGame'] = array_merge($data[$key]['BuyingGame'], $amount);

						$distance = $this->get_distance($user_id, $data[$key]['BuyingGame']['user_id']);

						$data[$key]['BuyingGame']['distance'] = $distance;


						$game_count++;
					}
				}
			}
			if ($game_count > 0) {

				return $data;
			}
		}


		return $data;
	}

	function ongoing_offers_for_wish_game($user_id, $game_id) {

		$games = $this->buying_game($user_id);
		$data = array();
		$user_ids = array();
		$counter = 0;

		foreach ($games as $key => $value) {
			if ($value['BuyingGame']['game_id'] == $game_id) {
				$user_ids[] = $value['BuyingGame']['user_id'];
			}
		}

		$count = count($user_ids);

		for ($i = 0; $i <= $count - 1; $i++) {

			foreach ($games as $key => $val) {

				if ($val['BuyingGame']['user_id'] == $user_ids[$i]) {

					$data[$counter] = $val;
					$counter++;
				}
			}
		}

		return $data;
	}

	function for_you($user_id = '0', $start = null, $limit = null) {

		App::uses('DtGame', 'Lib/DataTypes');
		App::uses('DtTopMobileGame', 'Lib/DataTypes');

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$data = array();
		$recommended = $this->query("call recommended_for_you('" . $user_id . "', NULL, 'recommended', NULL, '" . $start . "' ,'" . $limit . "', @out_genre_ids)");

		$genre = $this->query("select @out_genre_ids");
		$genres = $genre[0][0]['@out_genre_ids'];

		if (!empty($recommended)) {

			foreach ($recommended as $key => $value) {

				$obj_game = new DtGame($value['games']);
				$data['RecommendedGame'][$key] = $obj_game->get_field();
				$data['RecommendedGame'][$key]['genre'] = $value['genres']['genre'];
				$data['RecommendedGame'][$key]['console'] = $value['consoles']['console'];
			}
		} else {
			$data['RecommendedGame'] = array();
		}

		if (!empty($genres)) {
			$genre_ids = explode(',', $genres);

			$count = count($genre_ids);

			for ($i = 0; $i <= $count - 1; $i++) {

				$genre_games = $this->query("call recommended_for_you('" . $user_id . "', '" . $genre_ids[$i] . "',NULL, NULL, '" . $start . "' ,'" . $limit . "', @out_genre_ids)");
				if (!empty($genre_games)) {

					foreach ($genre_games as $key => $value) {

						$obj_game = new DtGame($value['games']);
						$data['GenreGame'][$value['genres']['genre']][$key] = $obj_game->get_field();
						$data['GenreGame'][$value['genres']['genre']][$key]['console'] = $value['consoles']['console'];
					}
				}
			}
		} else {
			$data['GenreGame'] = array();
		}

		// Jiggster Pick is only for users who are not logged in
		/* if (empty($user_id)) {
		  $jigg_pick = $this->query("call recommended_for_you('" . $user_id . "', NULL, NULL, 'jiggster_pick', '" . $start . "' ,'" . $limit . "', @out_genre_ids)");

		  if (!empty($jigg_pick)) {

		  foreach ($jigg_pick as $key => $value) {

		  $obj_game = new DtGame($value['games']);
		  $data['JiggsterPick'][$key] = $obj_game->get_field();
		  $data['JiggsterPick'][$key]['genre'] = $value['genres']['genre'];
		  $data['JiggsterPick'][$key]['console'] = $value['consoles']['console'];
		  }
		  } else {
		  $data['JiggsterPick'] = array();
		  }
		  } else {
		  $data['JiggsterPick'] = array();
		  } */

		return $data;
	}

	function for_you_in_genre_ids($genre_ids, $console_ids, $start = null, $limit = null) {

		App::uses('DtGame', 'Lib/DataTypes');

		$data = array();

		$recommended = $this->query("call recommended_for_you_genre_ids('" . $genre_ids . "', '" . $console_ids . "', NULL, '" . $start . "' ,'" . $limit . "')"); //pr($recommended); exit;

		if (!empty($recommended)) {

			foreach ($recommended as $key => $value) {

				$obj_game = new DtGame($value['games']);
				$data['RecommendedGame'][$key] = $obj_game->get_field();
				$data['RecommendedGame'][$key]['genre'] = $value['genres']['genre'];
				$data['RecommendedGame'][$key]['console'] = $value['consoles']['console'];
			}
		} else {
			$data['RecommendedGame'] = array();
		}

		$genre_ids = explode(',', $genre_ids);

		$count = count($genre_ids);

		for ($i = 0; $i <= $count - 1; $i++) {

			$genre_games = $this->query("call recommended_for_you_genre_ids(NULL, NULL, '" . $genre_ids[$i] . "', '" . $start . "' ,'" . $limit . "')");
			if (!empty($genre_games)) {

				foreach ($genre_games as $key => $value) {

					$obj_game = new DtGame($value['games']);
					$data['GenreGame'][$value['genres']['genre']][$key] = $obj_game->get_field();
					$data['GenreGame'][$value['genres']['genre']][$key]['console'] = $value['consoles']['console'];
				}
			}
		}

		return $data;
	}

	function trading_game($user_id, $dist = null) {

		$data = array();
		$data1 = array();
		$data2 = array();

		if ($dist == null) {
			$dist = 9999;
		}

		$games = $this->query("call deals_for_you_trading('" . $user_id . "', 0, '" . $dist . "')");  //pr($games); exit;

		if (!empty($games) && $games != 1) {

			foreach ($games as $key => $value) {

				$game_conditions = explode(',', $value[0]['login_user_game_condition']);
				$game_comes_with = explode(',', $value[0]['login_user_game_comes_with']);
				$game_ids = explode(',', $value[0]['login_user_game_ids']);


				$count = count($game_conditions);

				$amount = 0;
				$login_user_image_url = '';
				$login_user_game_title = '';
				$login_user_game_consoles = '';


				for ($i = 0; $i < $count; $i++) {

					$score = 500;

					if (!empty($game_conditions)) {

						if ($game_conditions[$i] == 'poor') {
							$score = $score * 0.5;
						}
						if ($game_conditions[$i] == 'good') {
							$score = $score * 1;
						}
						if ($game_conditions[$i] == 'acceptable') {
							$score = $score * 1;
						}
						if ($game_conditions[$i] == 'mint') {
							$score = $score * 1.5;
						}
					}

					if (!empty($game_comes_with)) {

						if ($game_comes_with[$i] == 'box + manual + dvd') {
							$score = $score * 2;
						}
						if ($game_comes_with[$i] == 'box + dvd') {
							$score = $score * 1;
						}
						if ($game_comes_with[$i] == 'dvd only') {
							$score = $score * 0.5;
						}
						if ($game_comes_with[$i] == 'manual + dvd') {
							$score = $score * 1;
						}
					}

					$amount = $amount + $score;

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");
					$login_user_image_url .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ',';
					$login_user_game_title .= $image_url[0]['games']['title'] . ',';
					$login_user_game_consoles .= $image_url[0]['consoles']['console'] . ',';
				}

				$other_user_name = $this->query("call get_username_or_image('" . $value['users_wishlists']['other_user'] . "', NULL )");

				$image = '';

				if (strpos($other_user_name[0]['users']['image_url'], 'https') === FALSE && strpos($other_user_name[0]['users']['image_url'], 'http') === FALSE) {

					$image = SITE_URI . $other_user_name[0]['users']['image_url'];
				} else {

					$image = $other_user_name[0]['users']['image_url'];
				}

				$login_user_image_url = substr_replace($login_user_image_url, "", -1);
				$login_user_game_title = substr_replace($login_user_game_title, "", -1);
				$login_user_game_consoles = substr_replace($login_user_game_consoles, "", -1);


				$data1[$key]['UserTrading']['login_user_id'] = $value['users_games']['login_user'];
				$data1[$key]['UserTrading']['other_user_id'] = $value['users_wishlists']['other_user'];
				$data1[$key]['UserTrading']['other_user_name'] = $other_user_name[0]['users']['nickname'];
				$data1[$key]['UserTrading']['other_user_profile_image_url'] = $image;
				$data1[$key]['UserTrading']['login_user_games'] = $value[0]['login_user_game_ids'];
				$data1[$key]['UserTrading']['login_user_game_image_url'] = $login_user_image_url;
				$data1[$key]['UserTrading']['login_user_game_title'] = $login_user_game_title;
				$data1[$key]['UserTrading']['login_user_game_console'] = $login_user_game_consoles;
				$data1[$key]['UserTrading']['login_user_game_cost'] = $value[0]['login_user_amount'];
			}
		}


		$games = $this->query("call deals_for_you_trading('" . $user_id . "', 1, '" . $dist . "')"); //pr($games); exit;

		if (!empty($games) && $games != 1 && !empty($data1)) {

			foreach ($games as $key => $value) {

				$game_conditions = explode(',', $value[0]['other_user_game_condition']);
				$game_comes_with = explode(',', $value[0]['other_user_game_comes_with']);
				$game_ids = explode(',', $value[0]['other_user_game_ids']);

				$count = count($game_conditions);

				$amount = 0;
				$other_user_image_url = '';
				$other_user_game_title = '';
				$other_user_game_consoles = '';

				for ($i = 0; $i < $count; $i++) {

					$score = 500;

					if (!empty($game_conditions)) {

						if ($game_conditions[$i] == 'poor') {
							$score = $score * 0.5;
						}
						if ($game_conditions[$i] == 'good') {
							$score = $score * 1;
						}
						if ($game_conditions[$i] == 'acceptable') {
							$score = $score * 1;
						}
						if ($game_conditions[$i] == 'mint') {
							$score = $score * 1.5;
						}
					}

					if (!empty($game_comes_with)) {

						if ($game_comes_with[$i] == 'box + manual + dvd') {
							$score = $score * 2;
						}
						if ($game_comes_with[$i] == 'box + dvd') {
							$score = $score * 1;
						}
						if ($game_comes_with[$i] == 'dvd only') {
							$score = $score * 0.5;
						}
						if ($game_comes_with[$i] == 'manual + dvd') {
							$score = $score * 1;
						}
					}

					$amount = $amount + $score;

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");
					$other_user_image_url .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ',';
					$other_user_game_title .= $image_url[0]['games']['title'] . ',';
					$other_user_game_consoles .= $image_url[0]['consoles']['console'] . ',';
				}

				$other_user_game_title = substr_replace($other_user_game_title, "", -1);
				$other_user_game_consoles = substr_replace($other_user_game_consoles, "", -1);
				$other_user_image_url = substr_replace($other_user_image_url, "", -1);

				$data2[$key]['UserTrading']['other_user_id'] = $value['users_games']['other_user'];
				$data2[$key]['UserTrading']['other_user_games'] = $value[0]['other_user_game_ids'];
				$data2[$key]['UserTrading']['other_user_game_image_url'] = $other_user_image_url;
				$data2[$key]['UserTrading']['other_user_game_image_title'] = $other_user_game_title;
				$data2[$key]['UserTrading']['other_user_game_console'] = $other_user_game_consoles;
				$data2[$key]['UserTrading']['other_user_game_cost'] = $value[0]['other_user_amount'];
			}
		} else {

			return $data;
		}

		$count = 0;

		foreach ($data1 as $key => $value) {

			foreach ($data2 as $k => $val) {

				if ($data1[$key]['UserTrading']['other_user_id'] == $data2[$k]['UserTrading']['other_user_id']) {

					$distance = $this->get_distance($data1[$key]['UserTrading']['login_user_id'], $data2[$k]['UserTrading']['other_user_id']);

					$data[$count]['UserTrading'] = array_merge(array_merge($data1[$key]['UserTrading'], $data2[$k]['UserTrading'])
							, $this->trading_subset($data1[$key], $data2[$k]));
					
					$data[$count]['UserTrading']['distance'] = $distance;
					
					$count++;
				}
			}
		}

		return $data;
	}

	function trading_subset($loginObj, $otherObj) {

		$loginGames = $this->powerSet(explode(',', $loginObj['UserTrading']['login_user_games']));
		$otherGames = $this->powerSet(explode(',', $otherObj['UserTrading']['other_user_games']));

		$login_count = count($loginGames);
		$other_count = count($otherGames);



		for ($i = 0; $i < $login_count; $i++) {

			$loginGamesIDs = implode(',', $loginGames[$i]);
			$login_subset = $this->query("call get_subset_sum( '" . $loginObj['UserTrading']['login_user_id'] . "', '" . $loginGamesIDs . "' )");

			$login_user_game_image_url = '';
			$login_imgUrl = explode(',', $login_subset[0][0]['imgUrl']);

			foreach ($login_imgUrl as $imgUrl) {
				$login_user_game_image_url .= GAME_COVER_IMAGE_URI . $imgUrl . ',';
			}

			$login_user_game_image_url = substr_replace($login_user_game_image_url, "", -1);

			for ($j = 0; $j < $other_count; $j++) {


				$otherGamesIDs = implode(',', $otherGames[$j]);
				$other_subset = $this->query("call get_subset_sum( '" . $otherObj['UserTrading']['other_user_id'] . "', '" . $otherGamesIDs . "' )");

				if ($login_subset[0][0]['amountSum'] == $other_subset[0][0]['amountSum'] ||
						($login_subset[0][0]['amountSum'] + 2) == $other_subset[0][0]['amountSum'] ||
						($login_subset[0][0]['amountSum'] - 2) == $other_subset[0][0]['amountSum'] ||
						$login_subset[0][0]['amountSum'] == ($other_subset[0][0]['amountSum'] + 2) ||
						$login_subset[0][0]['amountSum'] == ($other_subset[0][0]['amountSum'] - 2)) {

					$other_user_game_image_url = '';
					$other_imgUrl = explode(',', $other_subset[0][0]['imgUrl']);

					foreach ($other_imgUrl as $imgUrl) {
						$other_user_game_image_url .= GAME_COVER_IMAGE_URI . $imgUrl . ',';
					}

					$other_user_game_image_url = substr_replace($other_user_game_image_url, "", -1);


					$subset['subset'][] = array('other_user_name' => $loginObj['UserTrading']['other_user_name'],
						'login_user_games' => $loginGamesIDs,
						'login_user_game_image_url' => $login_user_game_image_url,
						'other_user_games' => $otherGamesIDs,
						'other_user_game_image_url' => $other_user_game_image_url
					);
				}
			}
		}

		if (empty($subset)):
			$subset['subset'] = array();
		endif;

		return $subset;
	}

	function powerSet($in, $minLength = 1) {
		$count = count($in);
		$members = pow(2, $count);
		$return = array();
		for ($i = 0; $i < $members; $i++) {
			$b = sprintf("%0" . $count . "b", $i);
			$out = array();
			for ($j = 0; $j < $count; $j++) {
				if ($b{$j} == '1')
					$out[] = $in[$j];
			}
			if (count($out) >= $minLength) {
				$return[] = $out;
			}
		}
		return $return;
	}

	function selling_game($user_id, $start = null, $limit = null, $dist = null) {

		$data = array();

		$user_games = $this->query("call get_all_games_by_user_id('" . $user_id . "')");

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		if ($dist == null) {
			$dist = 9999;
		}

		if (!empty($user_games)) {

			App::uses('DtUserWishlist', 'Lib/DataTypes');
			App::uses('DtUser', 'Lib/DataTypes');
			App::uses('DtGame', 'Lib/DataTypes');
			App::uses('DtUserGame', 'Lib/DataTypes');

			$game_count = 0;

			$games = $this->query("call deals_for_you_selling('" . $user_id . "', 0,'" . $start . "','" . $limit . "','" . $dist . "', @status)"); //pr($games); exit;

			$status = $this->query('SELECT @status');

			if ($status[0][0]['@status'] == 1) {

				foreach ($games as $key => $value) {

					$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $value['users_wishlists']['user_id'] . "',
																						 '" . $value['users_wishlists']['game_id'] . "')");
					if ($existance[0][0]['UserGame'] == 0) {

						$obj_wishlist = new DtUserWishlist($value['users_wishlists']);
						$obj_user = new DtUser($value['users']);
						$obj_game = new DtGame($value['games']);
						$obj_user_game = new DtUserGame($value['users_games']);

						$data[$game_count]['SellingGame'] = array_merge($obj_user_game->get_field(), $obj_wishlist->get_field(), $obj_user->get_field(), $obj_game->get_field());

						$score = 500;

						if (!empty($data[$game_count]['SellingGame']['condition'])) {

							if ($data[$game_count]['SellingGame']['condition'] == 'poor') {

								$score = $score * 0.5;
							}

							if ($data[$game_count]['SellingGame']['condition'] == 'good') {

								$score = $score * 1;
							}

							if ($data[$game_count]['SellingGame']['condition'] == 'mint') {

								$score = $score * 1.5;
							}
						}

						if (!empty($data[$game_count]['SellingGame']['comes_with'])) {



							if ($data[$game_count]['SellingGame']['comes_with'] == 'box + manual + dvd') {

								$score = $score * 2;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'box + dvd') {

								$score = $score * 1;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'dvd only') {

								$score = $score * 0.5;
							}

							if ($data[$game_count]['SellingGame']['comes_with'] == 'manual + dvd') {

								$score = $score * 1;
							}
						}

						$amount = array('amount' => $score = $score / 50);

						//$data[$game_count]['SellingGame'] = array_merge($data[$game_count]['SellingGame'], $amount);

						$distance = $this->get_distance($user_id, $data[$key]['SellingGame']['user_id']);

						$data[$key]['SellingGame']['distance'] = $distance;

						$game_count++;
					}
				}
			}

			if ($game_count > 0) {

				return $data;
			} else {

				$game_count = 0;

				$data = array();

				$games = $this->query("call deals_for_you_selling('" . $user_id . "', 1, '" . $start . "', '" . $limit . "','" . $dist . "', @status)");

				$status = $this->query('SELECT @status');

				if ($status[0][0]['@status'] == 1) {

					foreach ($games as $key => $value) {

						$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $value['users_wishlists']['user_id'] . "',
																						 '" . $value['users_wishlists']['game_id'] . "')");

						if ($existance[0][0]['UserGame'] == 0) {

							$obj_wishlist = new DtUserWishlist($value['users_wishlists']);
							$obj_user = new DtUser($value['users']);
							$obj_game = new DtGame($value['games']);
							$obj_user_game = new DtUserGame($value['users_games']);

							$data[$key]['SellingGame'] = array_merge($obj_user_game->get_field(), $obj_wishlist->get_field(), $obj_user->get_field(), $obj_game->get_field());

							$score = 500;

							if (!empty($data[$key]['SellingGame']['condition'])) {

								if ($data[$key]['SellingGame']['condition'] == 'poor') {

									$score = $score * 0.5;
								}

								if ($data[$key]['SellingGame']['condition'] == 'good') {

									$score = $score * 1;
								}

								if ($data[$key]['SellingGame']['condition'] == 'mint') {

									$score = $score * 1.5;
								}
							}



							if (!empty($data[$key]['SellingGame']['comes_with'])) {

								if ($data[$key]['SellingGame']['comes_with'] == 'box + manual + dvd') {

									$score = $score * 2;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'box + dvd') {

									$score = $score * 1;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'dvd only') {

									$score = $score * 0.5;
								}

								if ($data[$key]['SellingGame']['comes_with'] == 'manual + dvd') {

									$score = $score * 1;
								}
							}

							$amount = array('amount' => $score = $score / 50);

							//$data[$key]['SellingGame'] = array_merge($data[$key]['SellingGame'], $amount);

							$distance = $this->get_distance($user_id, $data[$key]['SellingGame']['user_id']);

							$data[$key]['SellingGame']['distance'] = $distance;

							$game_count++;
						}
					}
				}

				if ($game_count > 0) {

					return $data;
				}
			}
		}



		return $data;
	}

	function all($user_id) {

		$data = array(
			'Buying' => $this->buying_game($user_id),
			'Selling' => $this->selling_game($user_id),
			'Trading' => $this->trading_game($user_id)
		);

		return $data;
	}

	function ongoing_offers_for_have_game($user_id, $game_id) {

		$games = $this->selling_game($user_id);

		$data = array();

		$user_ids = array();

		foreach ($games as $key => $value) {

			if ($value['SellingGame']['game_id'] == $game_id) {

				$user_ids[] = $value['SellingGame']['user_id'];
			}
		}

		$count = count($user_ids);
		$key = 0;

		for ($i = 0; $i <= $count - 1; $i++) {

			foreach ($games as $val) {

				if ($val['SellingGame']['user_id'] == $user_ids[$i]) {

					$data[$key] = $val;
					$key++;
				}
			}
		}

		return $data;
	}

	function all_wish($user_id, $game_id) {

		$games = $this->trading_game($user_id);

		$data1 = array();
		$count = 0;

		foreach ($games as $value) {

			$games1 = explode(',', $value['UserTrading']['login_user_games']);
			$games2 = explode(',', $value['UserTrading']['other_user_games']);

			$merge_array = array_merge($games1, $games2);

			if (in_array($game_id, $merge_array)) {

				$data1[$count] = $value;
				$count++;
			}
		}

		$data = array(
			'wish_buy' => $this->ongoing_offers_for_wish_game($user_id, $game_id),
			'wish_exchange' => $data1
		);

		return $data;
	}

	function all_have($user_id, $game_id) {

		$games = $this->trading_game($user_id);

		$data1 = array();
		$count = 0;

		foreach ($games as $key => $value) {

			$games1 = explode(',', $value['UserTrading']['login_user_games']);
			$games2 = explode(',', $value['UserTrading']['other_user_games']);

			$merge_array = array_merge($games1, $games2);

			if (in_array($game_id, $merge_array)) {

				$data1[$count] = $value;
				$count += 1;
			}
		}

		$data = array(
			'have_sell' => $this->ongoing_offers_for_have_game($user_id, $game_id),
			'have_exchange' => $data1
		);

		return $data;
	}

	function user_current_deals($user_id) {

		$data_buy = array();
		$data_sell = array();
		$data_exchange = array();
		$image_url = '';

		$buy = $this->query("call my_current_deals('" . $user_id . "', 'buy', NULL)");  //pr($buy); exit;

		if (!empty($buy) && $buy != 1) {

			foreach ($buy as $key => $val) { //pr($val); exit;
				$data_buy[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_buy[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_buy[$key]['games_to_buy'] = $val['users_deals']['game'];
				$data_buy[$key]['deal_insured'] = $val['users_deals']['is_ensured'];
				$data_buy[$key]['amount'] = $val['deals']['amount'];
				$data_buy[$key]['last_action_by'] = $val['deals']['last_action_by'];
				$data_buy[$key]['last_action_date'] = $val['deals']['last_action_date'];
				$data_buy[$key]['is_archive'] = $val['deals']['is_archive'];

				$data_buy[$key]['comment'] = '';
				$data_buy[$key]['comment_date'] = '';
				$data_buy[$key]['comment_time'] = '';

				$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

				if (!empty($deal_chat)) {

					$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
					$data_buy[$key]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
					$data_buy[$key]['comment_date'] = $date_time[0];
					$data_buy[$key]['comment_time'] = $date_time[1];
				}


				$data_buy[$key]['seller_id'] = $val['users']['id'];
				$data_buy[$key]['seller_name'] = $val['users']['nickname'];

				$seller_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$seller_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$seller_profile_image = $val['users']['image_url'];
				}

				$data_buy[$key]['seller_profile_image'] = $seller_profile_image;
				$data_buy[$key]['seller_trust'] = $val['users']['trusted_user'];
				$data_buy[$key]['image_url'] = '';
				$data_buy[$key]['game_title'] = '';
				$data_buy[$key]['consoles'] = '';

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {
						$data_buy[$key]['image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_buy[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_buy[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_buy[$key]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$sell = $this->query("call my_current_deals('" . $user_id . "', 'sell', NULL)");

		if (!empty($sell) && $sell != 1) {

			foreach ($sell as $key => $val) {
				//pr($val['users_deals']['deal_id']); exit;
				$users = $this->query("call my_current_deals('" . $user_id . "', NULL, '" . $val['users_deals']['deal_id'] . "' )");

				$data_sell[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_sell[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_sell[$key]['games_to_sell'] = $val['users_deals']['game'];
				$data_sell[$key]['deal_insured'] = $val['users_deals']['is_ensured'];
				$data_sell[$key]['amount'] = $val['deals']['amount'];
				$data_sell[$key]['last_action_by'] = $val['deals']['last_action_by'];
				$data_sell[$key]['last_action_date'] = $val['deals']['last_action_date'];
				$data_sell[$key]['is_archive'] = $val['deals']['is_archive'];
				$data_sell[$key]['comment'] = '';
				$data_sell[$key]['comment_date'] = '';
				$data_sell[$key]['comment_time'] = '';

				$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

				if (!empty($deal_chat)) {

					$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
					$data_sell[$key]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
					$data_sell[$key]['comment_date'] = $date_time[0];
					$data_sell[$key]['comment_time'] = $date_time[1];
				}


				$data_sell[$key]['buyer_id'] = $users[0]['users']['id'];
				$data_sell[$key]['buyer_name'] = $users[0]['users']['nickname'];

				$buyer_profile_image = '';

				if (strpos($users[0]['users']['image_url'], 'https') === FALSE && strpos($users[0]['users']['image_url'], 'http') === FALSE) {

					$buyer_profile_image = SITE_URI . $users[0]['users']['image_url'];
				} else {

					$buyer_profile_image = $users[0]['users']['image_url'];
				}

				$data_sell[$key]['buyer_profile_image'] = $buyer_profile_image;
				$data_sell[$key]['buyer_trust'] = $users[0]['users']['trusted_user'];
				$data_sell[$key]['image_url'] = '';
				$data_sell[$key]['game_title'] = '';
				$data_sell[$key]['consoles'] = '';

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_sell[$key]['image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_sell[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_sell[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_sell[$key]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$exchange = $this->query("call my_current_deals('" . $user_id . "', 'exchange', NULL)"); // pr($exchange);		exit;

		if (!empty($exchange) && $exchange != 1) {

			$count = 0;
			$counter = 0;

			foreach ($exchange as $key => $val) {

				if ($count == 2) {
					$count = 0;
					$counter++;
				}

				if ($val['users']['id'] == $user_id) {

					$data_exchange[$counter]['login_user_id'] = $val['users']['id'];
					$data_exchange[$counter]['login_user_name'] = $val['users']['nickname'];

					$login_user_profile_image = '';

					if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

						$login_user_profile_image = SITE_URI . $val['users']['image_url'];
					} else {

						$login_user_profile_image = $val['users']['image_url'];
					}

					$data_exchange[$counter]['login_user_profile_image'] = $login_user_profile_image;
					$data_exchange[$counter]['login_user_trust'] = $val['users']['trusted_user'];
					$data_exchange[$counter]['login_user_game_ids'] = $val['users_deals']['game'];
					$data_exchange[$counter]['deal_insured'] = $val['users_deals']['is_ensured'];
					$data_exchange[$counter]['login_user_trade'] = $val['users_deals']['trade'];
					$data_exchange[$counter]['deal_id'] = $val['users_deals']['deal_id'];
					$data_exchange[$counter]['last_action_by'] = $val['deals']['last_action_by'];
					$data_exchange[$counter]['last_action_date'] = $val['deals']['last_action_date'];
					$data_exchange[$counter]['is_archive'] = $val['deals']['is_archive'];

					$data_exchange[$counter]['comment'] = '';
					$data_exchange[$counter]['comment_date'] = '';
					$data_exchange[$counter]['comment_time'] = '';

					$deal_chat = $this->query("call last_deal_msg('" . $val['users_deals']['deal_id'] . "')");

					if (!empty($deal_chat)) {

						$date_time = explode(' ', $deal_chat[0]['deals_chatlogs']['created']);
						$data_exchange[$counter]['comment'] = $deal_chat[0]['deals_chatlogs']['message'];
						$data_exchange[$counter]['comment_date'] = $date_time[0];
						$data_exchange[$counter]['comment_time'] = $date_time[1];
					}


					$data_exchange[$counter]['deal_creater'] = $val['deals']['offered_by'];
					$data_exchange[$counter]['deal_amount'] = $val['deals']['amount'];
					$data_exchange[$counter]['login_user_game_image_url'] = '';
					$data_exchange[$counter]['login_user_game_title'] = '';
					$data_exchange[$counter]['login_user_game_consoles'] = '';


					$game_ids = explode(',', $val['users_deals']['game']);

					$count_game = count($game_ids);

					for ($i = 0; $i < $count_game; $i++) {

						$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

						if (!empty($image_url)) {

							$data_exchange[$counter]['login_user_game_image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
							$data_exchange[$counter]['login_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
							$data_exchange[$counter]['login_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
						}
					}

					$count++;
					continue;
				}

				$data_exchange[$counter]['other_user_id'] = $val['users']['id'];
				$data_exchange[$counter]['other_user_name'] = $val['users']['nickname'];

				$other_user_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$other_user_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$other_user_profile_image = $val['users']['image_url'];
				}

				$data_exchange[$counter]['other_user_profile_image'] = $other_user_profile_image;
				$data_exchange[$counter]['other_user_trust'] = $val['users']['trusted_user'];
				$data_exchange[$counter]['other_user_game_ids'] = $val['users_deals']['game'];
				$data_exchange[$counter]['other_user_trade_opt'] = $val['users_deals']['trade'];
				$data_exchange[$counter]['other_user_game_image_url'] = '';
				$data_exchange[$counter]['other_user_game_title'] = '';
				$data_exchange[$counter]['other_user_game_consoles'] = '';

				$game_ids = explode(',', $val['users_deals']['game']);

				$count_game = count($game_ids);

				for ($i = 0; $i < $count_game; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_exchange[$counter]['other_user_game_image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_exchange[$counter]['other_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_exchange[$counter]['other_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$count++;

				$deal_msg_count = $this->query("call deal_msg_count( '" . $val['users_deals']['deal_id'] . "', '" . $user_id . "' )");

				if (isset($deal_msg_count)) {
					$data_exchange[$counter]['msg_count'] = $deal_msg_count[0][0]['msg_count'];
				}
			}
		}

		$data = array(
			'buy' => $data_buy,
			'sell' => $data_sell,
			'exchange' => $data_exchange
		);

		return $data;
	}

	function user_current_deals_on_specific_game($user_id, $game_id) {

		$deals_data = $this->user_current_deals($user_id);

		$exchange = array();
		$sell = array();

		if (!empty($deals_data['exchange'])) {

			$count = 0;

			foreach ($deals_data['exchange'] as $key => $val) {

				$game_ids1 = explode(',', $val['login_user_game_ids']);
				$game_ids2 = explode(',', $val['other_user_game_ids']);

				$game_ids = array_merge($game_ids1, $game_ids2);

				if (in_array($game_id, $game_ids)) {

					$exchange[$count] = $val;
					$count++;
				}
			}
		}

		if (!empty($deals_data['sell'])) {

			$count = 0;

			foreach ($deals_data['sell'] as $key => $val) {

				$game_ids = explode(',', $val['games_to_sell']);

				if (in_array($game_id, $game_ids)) {

					$sell[$count] = $val;
					$count++;
				}
			}
		}

		$data = array(
			'sell' => $sell,
			'exchange' => $exchange
		);

		return $data;
	}

	function user_send_receive_deals($user_id) {

		$data_buy = array();
		$data_sell = array();
		$data_exchange = array();
		$image_url = '';

		$buy = $this->query("call send_and_receive('" . $user_id . "', 'buy', NULL)");

		if (!empty($buy) && $buy != 1) {

			foreach ($buy as $key => $val) { //pr($val); exit;
				$status1 = true;
				$status2 = true;

				if ($val[0]['loginsendConfirm'] == 0) {
					$status1 = false;
				}
				if ($val[0]['loginreceiveConfirm'] == 0) {
					$status2 = false;
				}

				$seller_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$seller_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$seller_profile_image = $val['users']['image_url'];
				}

				$unread_deal_msg_count = $this->query("call specific_deal_unread_msg_count('" . $val['users_deals']['deal_id'] . "','" . $user_id . "')");

				$data_buy[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_buy[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_buy[$key]['games_to_buy'] = $val['users_deals']['game'];
				$data_buy[$key]['loginsendConfirm'] = $status1;
				$data_buy[$key]['loginreceiveConfirm'] = $status2;
				$data_buy[$key]['send_confirm'] = $val['users_deals']['send_confirm'];
				$data_buy[$key]['receive_confirm'] = $val['users_deals']['receive_confirm'];
				$data_buy[$key]['amount'] = $val['deals']['amount'];
				$data_buy[$key]['seller_id'] = $val['users']['id'];
				$data_buy[$key]['seller_name'] = $val['users']['nickname'];
				$data_buy[$key]['seller_profile_image'] = $seller_profile_image;
				$data_buy[$key]['seller_trust'] = $val['users']['trusted_user'];
				$data_buy[$key]['image_url'] = '';
				$data_buy[$key]['game_title'] = '';
				$data_buy[$key]['consoles'] = '';
				$data_buy[$key]['UnreadDealMsgCount'] = $unread_deal_msg_count[0][0]['UnreadDealMessages'];

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {
						$data_buy[$key]['image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_buy[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_buy[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}
			}
		}

		$sell = $this->query("call send_and_receive('" . $user_id . "', 'sell', NULL)"); //pr(); exit;

		if (!empty($sell) && $sell != 1) {

			foreach ($sell as $key => $val) {

				$status1 = true;
				$status2 = true;

				if ($val[0]['othersendConfirm'] == 0) {
					$status1 = false;
				}
				if ($val[0]['otherreceiveConfirm'] == 0) {
					$status2 = false;
				}

				$users = $this->query("call send_and_receive('" . $user_id . "', NULL, '" . $val['users_deals']['deal_id'] . "' )");

				$buyer_profile_image = '';

				if (strpos($users[0]['users']['image_url'], 'https') === FALSE && strpos($users[0]['users']['image_url'], 'http') === FALSE) {

					$buyer_profile_image = SITE_URI . $users[0]['users']['image_url'];
				} else {

					$buyer_profile_image = $users[0]['users']['image_url'];
				}

				$unread_deal_msg_count = $this->query("call specific_deal_unread_msg_count('" . $val['users_deals']['deal_id'] . "','" . $user_id . "')");

				$data_sell[$key]['deal_id'] = $val['users_deals']['deal_id'];
				$data_sell[$key]['deal_creater_id'] = $val['deals']['offered_by'];
				$data_sell[$key]['games_to_sell'] = $val['users_deals']['game'];
				$data_sell[$key]['send_confirm'] = $val['users_deals']['send_confirm'];
				$data_sell[$key]['receive_confirm'] = $val['users_deals']['receive_confirm'];
				$data_sell[$key]['othersendConfirm'] = $status1;
				$data_sell[$key]['otherreceiveConfirm'] = $status2;
				$data_sell[$key]['amount'] = $val['deals']['amount'];
				$data_sell[$key]['buyer_id'] = $users[0]['users']['id'];
				$data_sell[$key]['buyer_name'] = $users[0]['users']['nickname'];
				$data_sell[$key]['buyer_profile_image'] = $buyer_profile_image;
				$data_sell[$key]['buyer_trust'] = $users[0]['users']['trusted_user'];
				$data_sell[$key]['image_url'] = '';
				$data_sell[$key]['game_title'] = '';
				$data_sell[$key]['consoles'] = '';
				$data_sell[$key]['UnreadDealMsgCount'] = $unread_deal_msg_count[0][0]['UnreadDealMessages'];

				$game_ids = explode(',', $val['users_deals']['game']);

				$count = count($game_ids);

				for ($i = 0; $i < $count; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_sell[$key]['image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_sell[$key]['game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_sell[$key]['consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}
			}
		}

		$exchange = $this->query("call send_and_receive('" . $user_id . "', 'exchange', NULL)");  //pr($exchange);		exit;

		if (!empty($exchange) && $exchange != 1) {

			$count = 0;
			$counter = 0;

			foreach ($exchange as $key => $val) {

				if ($count == 2) {
					$count = 0;
					$counter++;
				}

				if ($val['users']['id'] == $user_id) {

					$login_user_profile_image = '';

					if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

						$login_user_profile_image = SITE_URI . $val['users']['image_url'];
					} else {

						$login_user_profile_image = $val['users']['image_url'];
					}

					$unread_deal_msg_count = $this->query("call specific_deal_unread_msg_count('" . $val['users_deals']['deal_id'] . "','" . $user_id . "')");

					$data_exchange[$counter]['login_user_id'] = $val['users']['id'];
					$data_exchange[$counter]['login_user_name'] = $val['users']['nickname'];
					$data_exchange[$counter]['login_user_profile_image'] = $login_user_profile_image;
					$data_exchange[$counter]['login_user_trust'] = $val['users']['trusted_user'];
					$data_exchange[$counter]['login_user_game_ids'] = $val['users_deals']['game'];
					$data_exchange[$counter]['send_confirm'] = $val['users_deals']['send_confirm'];
					$data_exchange[$counter]['receive_confirm'] = $val['users_deals']['receive_confirm'];
					$data_exchange[$counter]['login_user_trade'] = $val['users_deals']['trade'];
					$data_exchange[$counter]['deal_id'] = $val['users_deals']['deal_id'];
					$data_exchange[$counter]['deal_creater'] = $val['deals']['offered_by'];
					$data_exchange[$counter]['deal_amount'] = $val['deals']['amount'];
					$data_exchange[$counter]['login_user_game_image_url'] = '';
					$data_exchange[$counter]['login_user_game_title'] = '';
					$data_exchange[$counter]['login_user_game_consoles'] = '';
					$data_exchange[$counter]['UnreadDealMsgCount'] = $unread_deal_msg_count[0][0]['UnreadDealMessages'];


					$game_ids = explode(',', $val['users_deals']['game']);

					$count_game = count($game_ids);

					for ($i = 0; $i < $count_game; $i++) {

						$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

						if (!empty($image_url)) {

							$data_exchange[$counter]['login_user_game_image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
							$data_exchange[$counter]['login_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
							$data_exchange[$counter]['login_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
						}
					}

					$count++;
					continue;
				}

				$other_user_profile_image = '';

				if (strpos($val['users']['image_url'], 'https') === FALSE && strpos($val['users']['image_url'], 'http') === FALSE) {

					$other_user_profile_image = SITE_URI . $val['users']['image_url'];
				} else {

					$other_user_profile_image = $val['users']['image_url'];
				}

				$data_exchange[$counter]['other_user_id'] = $val['users']['id'];
				$data_exchange[$counter]['other_user_name'] = $val['users']['nickname'];
				$data_exchange[$counter]['other_user_profile_image'] = $other_user_profile_image;
				$data_exchange[$counter]['other_user_trust'] = $val['users']['trusted_user'];
				$data_exchange[$counter]['other_user_game_ids'] = $val['users_deals']['game'];
				$data_exchange[$counter]['othersendConfirm'] = $val['users_deals']['send_confirm'];
				$data_exchange[$counter]['otherreceiveConfirm'] = $val['users_deals']['receive_confirm'];
				$data_exchange[$counter]['other_user_trade_opt'] = $val['users_deals']['trade'];
				$data_exchange[$counter]['other_user_game_image_url'] = '';
				$data_exchange[$counter]['other_user_game_title'] = '';
				$data_exchange[$counter]['other_user_game_consoles'] = '';

				$game_ids = explode(',', $val['users_deals']['game']);

				$count_game = count($game_ids);

				for ($i = 0; $i < $count_game; $i++) {

					$image_url = $this->query("call get_username_or_image( NULL, '" . $game_ids[$i] . "' )");

					if (!empty($image_url)) {

						$data_exchange[$counter]['other_user_game_image_url'] .= GAME_COVER_IMAGE_URI . $image_url[0]['games']['image_url'] . ', ';
						$data_exchange[$counter]['other_user_game_title'] .= $image_url[0]['games']['title'] . ', ';
						$data_exchange[$counter]['other_user_game_consoles'] .= $image_url[0]['consoles']['console'] . ', ';
					}
				}

				$count++;
			}
		}

		$data = array(
			'receive_confirm' => array(
				'buy' => $data_buy,
				'sell' => $data_sell,
				'exchange' => $data_exchange
			),
			'send_confirm' =>
			array(
				'buy' => $data_buy,
				'sell' => $data_sell,
				'exchange' => $data_exchange
			)
		);

		return $data;
	}

	function user_send_receive_action($data) {

		$action = $this->query("call send_receive_action('" . $data->deal_id . "', '" . $data->user_id . "', '" . $data->receive_action . "', '" . $data->send_action . "', @status )");


		$user_notifyID = $this->query("call send_receive_notification( '" . $data->deal_id . "', '" . $data->user_id . "' )");

		$actionBy = $this->query("call get_user_by_id( '" . $data->user_id . "' )");

		$regid = $this->query("call get_user_gcm_regid( '" . $user_notifyID[0]['users_deals']['user_id'] . "' )");

		$message = '';

		if ($data->action == "receive_confirm"):

			$message = $actionBy[0]['users']['nickname'] . " has confirmed receive of goods/payments";

		elseif ($data->action == "send_confirm"):

			$message = $actionBy[0]['users']['nickname'] . " has confirmed sending the goods/payments";

		endif;

		if (!empty($regid[0]['users']['gcm_regid'])) {

			$gcm = new GCM();

			$gcm->send_notification($regid[0]['users']['gcm_regid'], '{"header":{"action":"open_action", "message":"' . $message . '"},"body":{"deal_id":"' . $data->deal_id . '"}}');
		}

		return $this->get_status();
	}

	function detail_extra($user_id, $game_id) {

		$data = array();

		$screenshot = $this->query("call game_screenshot('" . $game_id . "', NULL, NULL)"); //pr($screenshot); exit;

		$screenshot_names = explode(",", $screenshot[0]['games']['screenshot']);

		$count = count($screenshot_names);

		$data['screenshot'] = '';

		if (!empty($screenshot) && $count > 1) {

			for ($i = 0; $i < $count; $i++) {

				$data['screenshot'][$i] = GAME_SCREENSHOT_URI . $screenshot_names[$i];
			}
		}

		$data['amazon'] = $screenshot[0]['games']['amazon'];
		$data['google_play'] = $screenshot[0]['games']['google_play'];
		$data['desc'] = utf8_encode($screenshot[0]['games']['desc']);

		$data['similarGames'] = array();


		$similar_games = $this->query("call game_screenshot( NULL, '" . $screenshot[0]['games']['titleID'] . "', '" . $user_id . "')"); //pr($similar_games); exit;

		if (!empty($similar_games)) {

			foreach ($similar_games as $key => $value) {
				//pr($value); die();
				//if (!empty($value['games']['title'])) {

				$data['similarGames'][$key]['id'] = $value['games']['id'];
				$data['similarGames'][$key]['title'] = $value['games']['title'];
				$data['similarGames'][$key]['publisher'] = $value['games']['publisher'];
				$data['similarGames'][$key]['developers'] = $value['games']['developers'];
				$data['similarGames'][$key]['release_date'] = $value[0]['release_date'];

				//}
				//if (!empty($value['games']['image_url'])) {

				$data['similarGames'][$key]['image_url'] = GAME_COVER_IMAGE_URI . $value['games']['image_url'];
				//}
				//if (!empty($value['consoles']['console'])) {

				$data['similarGames'][$key]['console'] = $value['consoles']['console'];
				//}
			}
		}
//pr($data); exit;
		return $data;
	}

	function genre_games($genre_id, $start = null, $limit = null) {

		$data = array();
		App::uses('DtGame', 'Lib/DataTypes');

		$genre_games = $this->query("call genre_specific_games('" . $genre_id . "', '" . $start . "' ,'" . $limit . "')"); // pr($genre_games); exit;

		if (!empty($genre_games)) {

			foreach ($genre_games as $key => $value) {

				$obj_game = new DtGame($value['games']);
				$data['GenreGame'][$value['genres']['genre']][$key] = $obj_game->get_field();
				$data['GenreGame'][$value['genres']['genre']][$key]['console'] = $value['consoles']['console'];
			}
		}

		return $data;
	}

	function recomend_wish($user_id) {

		$data = array();
		App::uses('DtGame', 'Lib/DataTypes');

		$result = $this->query("call wishlist_recommended('" . $user_id . "')");
		$count = 0;
		if (!empty($result)) {

			foreach ($result as $key => $value) {

				$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "','" . $value['games']['id'] . "')");

				if ($existance[0][0]['UserGame'] == 0 && $existance[0][0]['UserWish'] == 0) {

					$obj_game = new DtGame($value['games']);
					$data['RecommendedGame'][$count] = $obj_game->get_field();
					$data['RecommendedGame'][$count]['genre'] = $value['genres']['genre'];
					$data['RecommendedGame'][$count]['console'] = $value['consoles']['console'];
					$count++;
				}
			}
		}

		return $data;
	}

	function get_game_autocomplete($title) {

		App::uses('DtGame', 'Lib/DataTypes');
		$data = array();

		$game_detail = $this->query("call get_game_autocomplete('" . $title . "')");

		if (!empty($game_detail)) {

			foreach ($game_detail as $key => $value) {

				$obj_game = new DtGame($value['Game']);

				$data[$key]['Game'] = $obj_game->get_field();


				//$existance = $this->query("call check_game_existance_in_wishlist_or_have('" . $user_id . "', '" . $data[$key]['Game']['id'] . "')");
				//$merge_array = array('in_user_games' => $existance[0][0]['UserGame'], 'in_user_wishlist' => $existance[0][0]['UserWish']);
				//$data[$key]['Game'] = array_merge($data[$key]['Game'], $merge_array);
			}
		}
		//pr($data); die();
		return $data;
	}

	function top_mobile_games($start, $limit) {

		App::uses('DtTopMobileGame', 'Lib/DataTypes');

		$data = array();

		$top_mobile_games = $this->query("call top_mobile_games('" . $start . "', '" . $limit . "')");

		if (!empty($top_mobile_games)) {

			foreach ($top_mobile_games as $key => $top_mobile_game) {

				$obj_game = new DtTopMobileGame($top_mobile_game['top_mobile_games']);
				$data['TopMobileGames'][$key] = $obj_game->get_field();
			}
		} else {
			$data['TopMobileGames'] = array();
		}

		return $data;
	}

	/**
	 * Get jiggster pick
	 */
	function get_jiggster_picks($start = 0, $limit = DEFAULT_LIMIT_FOR_YOU_GAMES) {

		App::uses('DtGame', 'Lib/DataTypes');

		$data = array();

		$jigg_pick = $this->query("call recommended_for_you('0', NULL, NULL, 'jiggster_pick', '" . $start . "' ,'" . $limit . "', @out_genre_ids)");

		if (!empty($jigg_pick)) {

			foreach ($jigg_pick as $key => $value) {

				$obj_game = new DtGame($value['games']);
				$data['JiggsterPick'][$key] = $obj_game->get_field();
				$data['JiggsterPick'][$key]['genre'] = $value['genres']['genre'];
				$data['JiggsterPick'][$key]['console'] = $value['consoles']['console'];
			}
		} else {
			$data['JiggsterPick'] = array();
		}

		return $data;
	}

	function see_more_recommended($param) {

		App::uses('DtGame', 'Lib/DataTypes');

		$data = array();

		if (empty($param['genre_ids']) || empty($param['console_ids'])) {


			$recommended = $this->query("call recommended_for_you('" . $param['user_id'] . "', NULL, 'recommended', NULL, '" . $param['start'] . "' ,'" . $param['end'] . "', @out_genre_ids)");

			if (!empty($recommended)) {

				foreach ($recommended as $key => $value) {

					$obj_game = new DtGame($value['games']);
					$data['RecommendedGame'][$key] = $obj_game->get_field();
					$data['RecommendedGame'][$key]['genre'] = $value['genres']['genre'];
					$data['RecommendedGame'][$key]['console'] = $value['consoles']['console'];
				}
			} else {
				$data['RecommendedGame'] = array();
			}
		} else {

			$recommended = $this->query("call recommended_for_you_genre_ids('" . $param['genre_ids'] . "', '" . $param['console_ids'] . "', NULL, '" . $param['start'] . "' ,'" . $param['end'] . "')"); //pr($recommended); exit;

			if (!empty($recommended)) {

				foreach ($recommended as $key => $value) {

					$obj_game = new DtGame($value['games']);
					$data['RecommendedGame'][$key] = $obj_game->get_field();
					$data['RecommendedGame'][$key]['genre'] = $value['genres']['genre'];
					$data['RecommendedGame'][$key]['console'] = $value['consoles']['console'];
				}
			} else {
				$data['RecommendedGame'] = array();
			}
		}

		return $data;
	}

	function get_see_more_genre_games($genreid, $start, $end, $consoles, $userid = "NULL") {
		App::uses('DtGame', 'Lib/DataTypes');

		$data = array();

		$more_games = $this->query("CALL genre_see_more_games($genreid, $userid, '$consoles',$start , $end )");

		if (!empty($more_games)) {

			foreach ($more_games as $key => $value) {

				$obj_game = new DtGame($value['games']);
				$data['GenreGame'][$key] = $obj_game->get_field();
				$data['GenreGame'][$key]['genre'] = $value['genres']['genre'];
				$data['GenreGame'][$key]['console'] = $value['consoles']['console'];
			}
		} else {
			$data['GenreGame'] = array();
		}

		return $data;
	}

}

