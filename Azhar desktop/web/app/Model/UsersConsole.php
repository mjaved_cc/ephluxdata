<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class UsersConsole extends AppModel {

	function save_user_console($user_id, $console_ids) {
		
		$this->query('call user_delete_consoles_genres("' . $user_id . '", "' . TABLE_CONSOLE . '")');

		$count = count($console_ids);

		for ($i = 0; $i < $count; $i++) {

			$this->query('call save_console("' . $user_id . '", "' . $console_ids[$i] . '", @status)');
		}

		return $this->get_status();
	}

}
