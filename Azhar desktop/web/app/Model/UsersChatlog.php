<?php

App::uses('AppModel', 'Model');
App::uses('GCM', 'Lib');

/**

 * User Model

 *

 */
class UsersChatlog extends AppModel {

	function save_message($data) {

		$this->query("call save_user_message(

			 '" . $data['id_sender'] . "',

			 '" . $data['id_receiver'] . "',

			 '" . $data['user_message'] . "',

              @status	 )");

		$status = $this->get_status();

		if (!empty($status)) {

			$sender = $this->query("call get_user_by_id( '" . $data['id_sender'] . "' )");
			$regid = $this->query("call get_user_gcm_regid( '" . $data['id_receiver'] . "' )");
			
			$image = '';

			if (strpos($sender[0]['users']['image_url'], 'https') === FALSE && strpos($sender[0]['users']['image_url'], 'http') === FALSE) {

				$image = SITE_URI . $sender[0]['users']['image_url'];
			} else {

				$image = $sender[0]['users']['image_url'];
			}

			if (!empty($regid[0]['users']['gcm_regid'])) {

				$gcm = new GCM();

				$gcm->send_notification($regid[0]['users']['gcm_regid'], '{"header":{"action":"user_message",  "message":"' . $sender[0]['users']['nickname'] . ' sent a new message"},"body":{"member_id":"' . $data['id_sender'] . '", "user_name":"' . $sender[0]['users']['nickname'] . '", "image_url":"' . $image . '"}}');
			}
		}

		return $this->get_status();
	}

	function unread_count($user_id, $receiver_id) {



		$count = $this->query("call count_user_unread_messages('" . $user_id . "','" . $receiver_id . "' )");



		return $count[0][0];
	}

	function user_chat($data) {

		App::uses('DtUserChat', 'Lib/DataTypes');

		$chat = $this->query("call list_chat_between_two_users('" . $data['id_sender'] . "','" . $data['id_receiver'] . "')");

		$data = array();

		if (!empty($chat)) {

			foreach ($chat as $key => $value) {

				$datetime = explode(' ', $chat[$key]['UserChatlog']['created']);

				$obj_chat = new DtUserChat($value['UserChatlog']);
				$data[$key]['UserChatlog'] = $obj_chat->get_field();
				$data[$key]['UserChatlog']['date'] = trim($datetime[0]);
				$data[$key]['UserChatlog']['time'] = trim($datetime[1]);
				unset($data[$key]['UserChatlog']['created']);
			}
		}


		return $data;
	}

	function mark_read($data) {



		$this->query("call user_mark_message_read(

			 '" . $data['id_chatlog'] . "',

			 '" . $data['id_receiver'] . "'

			)");



		return 'success';
	}

}

