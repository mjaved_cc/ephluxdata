<div class="authActionMaps index">
	<h2><?php echo __('Auth Action Maps'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo __('Action'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<!--<th><?php echo $this->Paginator->sort('crud','Treated as'); ?></th>-->
			<!--<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($authActionMaps as $authActionMap): ?>
	<tr>
		<td><?php echo h($authActionMap['AuthActionMap']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $authActionMap['Controller']['alias'] .'/'. $authActionMap['Action']['alias'] ; // $this->Html->link($authActionMap['Aco']['id'], array('controller' => 'acos', 'action' => 'view', $authActionMap['Aco']['id'])); ?>
		</td>
		<td><?php echo h($authActionMap['AuthActionMap']['description']); ?>&nbsp;</td>
		<!--<td><?php echo h($authActionMap['AuthActionMap']['crud']); ?>&nbsp;</td>-->
<!--		<td><?php echo h($authActionMap['AuthActionMap']['created']); ?>&nbsp;</td>
		<td><?php echo h($authActionMap['AuthActionMap']['modified']); ?>&nbsp;</td>-->
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $authActionMap['AuthActionMap']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $authActionMap['AuthActionMap']['id'])); ?>
			<?php // echo $this->Html->link(__('Edit Permission'), array('action' => 'assign_permission', $authActionMap['AuthActionMap']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $authActionMap['AuthActionMap']['id']), null, __('Are you sure you want to delete # %s?', $authActionMap['AuthActionMap']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Sync Auth Action Map'), array('action' => 'sync')); ?></li>
		<li><?php echo $this->Html->link(__('List Acos'), array('controller' => 'acos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aco'), array('controller' => 'acos', 'action' => 'add')); ?> </li>
	</ul>
</div>
