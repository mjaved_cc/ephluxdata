<?php
if (!empty($game)) {
	$game = json_decode($game);

	//pr($game); 	exit;
}
?>

<div class="main_column">
	<div class="box">
		<div class="body">

			<h2><?php echo __('Game Details'); ?></h2>

			<?php
			$image_url = $game->body[0]->Game->image_url;

			if (!empty($image_url)) {

				echo $this->Html->image($image_url);
			} else {

				echo 'No image found';
			}
			?>

			<?php if (!empty($game->body[0]->Game->title)) { ?>

				<h2>Title</h2>   <?php echo $game->body[0]->Game->title; ?> <br/>

			<?php } if (!empty($game->body[0]->Game->description)) { ?>

				<h2>description</h2>   <?php echo $game->body[0]->Game->description ?> <br/>

			<?php } if (!empty($game->body[0]->Game->publisher)) { ?>

				<h2>Publisher</h2>       <?php echo $game->body[0]->Game->publisher ?> <br/>

			<?php } if (!empty($game->body[0]->Game->developers)) { ?>

				<h2>developers</h2>       <?php echo $game->body[0]->Game->developers ?> <br/>

			<?php } ?>

			<h2>Rating</h2>   <?php echo $game->body[0]->Game->rating; ?> <br/>

		</div>
	</div>
</div>