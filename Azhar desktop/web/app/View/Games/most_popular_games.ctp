<?php
if(!empty($wanted_games)) {
	
	$wanted_games = json_decode($wanted_games); //pr($wanted_games); 
}

if(!empty($owned_games)){
	
	$owned_games = json_decode($owned_games); 
}
?>

<div class="main_column">
	<div class="box">
		<div class="body">
			
			<?php if (!empty($wanted_games->body)) { ?>

				<h2>Most Wanted Games</h2>			

				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

					<thead>
						<tr>
							
							<th><?php echo __('S.NO'); ?></th>
							<th><?php echo __('Title'); ?></th>
							<th><?php echo __('Image'); ?></th>
							<th><?php echo __('Users Wanting'); ?></th>
							<th><?php echo __('Rating'); ?></th>
							<th><?php echo __('Action'); ?></th>			
						</tr>
					</thead>
					<tbody>
						<?php
						
						$count = 1;
						
						if (!empty($wanted_games->body)) {
							foreach ($wanted_games->body as $game): //pr($game);
								?>
								<tr>
									<td align="center"><?php echo $count; ?>&nbsp;</td>
									<td align="center"><?php echo $game->Game->title ?>&nbsp;</td>
									<?php $image = $game->Game->image_url ?>
									<td align="center"><?php
						if (!empty($image)) {
							echo $this->Html->image($image,array('width'=>'100px'));
						} else {
							echo 'No Image found';
						}
									?></td>
									<td align="center"><?php echo $game->Game->users_wanting ?>&nbsp;</td>
									<td align="center"><?php echo $game->Game->rating ?>&nbsp;</td>

									<td class="actions" align="center">		
										<?php echo $this->Html->link(__('View'), array('controller' => 'games', 'action' => 'view', $game->Game->id)); ?> 
									</td>
								</tr>
								
								<?php $count = $count +1; ?>

								<?php
							endforeach;
						} else {
							?>
							<tr>
								<td colspan="9">No Wanted Games found.</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>


			<?php }  ?>
				
				
				
				<?php if (!empty($owned_games->body)) { ?>

				<h2>Most Owned Games</h2>			

				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

					<thead>
						<tr>
							<th><?php echo __('S.NO'); ?></th>
							<th><?php echo __('Title'); ?></th>
							<th><?php echo __('Image'); ?></th>
							<th><?php echo __('Users Owning'); ?></th>
							<th><?php echo __('Rating'); ?></th>
							<th><?php echo __('Action'); ?></th>			
						</tr>
					</thead>
					<tbody>
						<?php 
						
						$count = 1;
						
						if (!empty($owned_games->body)) {
							foreach ($owned_games->body as $game): //pr($game);
								?>
								<tr>
									<td align="center"><?php echo $count; ?>&nbsp;</td>
									<td align="center"><?php echo $game->Game->title ?>&nbsp;</td>
									<?php $image = $game->Game->image_url ?>
									<td align="center"><?php
						if (!empty($image)) {
							echo $this->Html->image($image,array('width'=>'100px'));
						} else {
							echo 'No Image found';
						}
									?></td>
									<td align="center"><?php echo $game->Game->users_owning ?>&nbsp;</td>
									<td align="center"><?php echo $game->Game->rating ?>&nbsp;</td>

									<td class="actions" align="center">		
										<?php echo $this->Html->link(__('View'), array('controller' => 'games', 'action' => 'view', $game->Game->id)); ?> 
									</td>
								</tr>
								
								<?php $count = $count + 1; ?>

								<?php
							endforeach;
						} else {
							?>
							<tr>
								<td colspan="9">No Owned Games found.</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>


			<?php }  ?>

		</div>
	</div>
</div>