<div class="sidebar">
	<div class="small_box">
		<div class="header">
			<img src="<?php WEBROOT_DIR ?>/img/history_icon.png" alt="History" width="24" height="24" />Actions
		</div>
		<div class="body">
			<div class="actions">				
				<ul>
					<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
					<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>


				</ul>
			</div>
		</div>
	</div>
</div>

<div class="main_column">
	<div class="box">
		<div class="body">

			<?php echo $this->Form->create('Group'); ?>
			<fieldset>
				<legend><?php echo __('Add Group'); ?></legend>
				<?php
				echo $this->Form->input('name', array('class' => 'textfield large'));
				?>
			<p>	<?php echo $this->Form->submit('Submit', array('class' => 'button2')); ?></p>
			</fieldset>
		</div>
	</div>
</div>

