<?php
if (!empty($offers)) {

	$offers = json_decode($offers);
	//pr($offers);
}
?>

	<h2>Buying Deals</h2>
<?php
if (!empty($offers->body->buy)) {
	$count = 1;
	?>

	<?php
	foreach ($offers->body->buy as $key => $val) {

		$game_image_url = explode(',', substr_replace($val->image_url, " ", -2));
		echo $count . ')';
		?>


		Buying From : <?php echo $val->seller_name; ?><br/>
		Amount to Pay : <?php echo $val->amount; ?><br/>
		Games to Buy <br/> : <?php foreach ($game_image_url as $value) { ?>

			<?php echo $this->Html->image($value, array('width' => '150px', 'height' => '150px')); ?>

		<?php } ?>

		<?php $count++ ?>
		<br/><br/>

	<?php } ?>

<?php } else {
	
	echo 'No Buying Deal Found';
		
	} ?>

<br/>

<h2>Selling Deals</h2>
<?php
if (!empty($offers->body->sell)) {
	$count = 1;
	?>

	<?php
	foreach ($offers->body->sell as $key => $val) {

		$game_image_url = explode(',', substr_replace($val->image_url, " ", -2));

		echo $count . ')';
		?>


		Selling To : <?php echo $val->buyer_name; ?><br/>
		Amount to Receive : <?php echo $val->amount; ?><br/>
		Games to Sell <br/>: <?php foreach ($game_image_url as $value) { ?>

			<?php echo $this->Html->image($value, array('width' => '150px', 'height' => '150px')); ?>

		<?php } ?>
		<?php $count++ ?>
		<br/><br/>

	<?php } ?>

<?php } else {
	
	echo 'No Selling Deal Found';
			
		} ?>

		
		<h2>Exchange Deals</h2>
<?php
if (!empty($offers->body->exchange)) {
	$count = 1;
	?>

	<?php
	foreach ($offers->body->exchange as $key => $val) { 

		$login_user_game_image_url = explode(',', substr_replace($val->login_user_game_image_url, " ", -2));
		$other_user_game_image_url = explode(',', substr_replace($val->other_user_game_image_url, " ", -2));
		echo $count . ')';
		?>


		Exchange with : <?php echo $val->other_user_name; ?><br/>
		
		<?php if($val->login_user_trade == 'exchange and pay') { 
			
			echo 'Amount to Pay :  '.$val->deal_amount.'<br/>';			
		}
		
		elseif ($val->login_user_trade == 'exchange and charge') {
			
			echo 'Amount to Receive :  '.$val->deal_amount.'<br/>';
		
	}
?>
		
		
		Games to send <br/> : <?php foreach ($login_user_game_image_url as $value) { ?>

			<?php echo $this->Html->image($value, array('width' => '150px', 'height' => '150px')); ?>

		<?php } ?>
		
		<br/>
		
		Games to receive <br/> : <?php foreach ($other_user_game_image_url as $value) { ?>

			<?php echo $this->Html->image($value, array('width' => '150px', 'height' => '150px')); ?>

		<?php } ?>
		
		

		<?php $count++ ?>
		<br/><br/>

	<?php } ?>

<?php } else {

	echo 'No Exchange Deal Found';
	
} ?>