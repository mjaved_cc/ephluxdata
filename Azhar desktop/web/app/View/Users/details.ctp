<?php
if (!empty($user)) {
	$user = json_decode($user);

	//pr($user);  //exit;
}
?>

<div class="main_column">
	<div class="box">
		<div class="body">

			<h2><?php echo __('User Details'); ?></h2>

			<div class="profile_img">

				<?php
				$image_url = $user->body->User->profile_image_url;

				if (!empty($image_url)) {

					echo $this->Html->image($image_url);
				} else {

					echo 'No image found';
				}
				?>

			</div>

			<div class="profile_detail">
				<p>

					<?php
					$dob = date('d-M-Y', $user->body->User->dob);
					?>


					<b>User Name :</b>   <?php echo $user->body->User->nickname ?> <br/>
					<b>Email        :</b><?php echo $user->body->User->email ?> <br/>



					<b>Date of Birth:</b>   <?php echo $dob ?> <br/>
					
					<?php if (!empty($user->body->User->gaming_id)) { ?>
					
					<b>Gaming ID    :</b><?php echo $user->body->User->gaming_id; ?> <br/>
					
					<?php } if (!empty($user->body->User->shipping_address)) { ?>
					
					<b>Shipping Address    :</b><?php echo $user->body->User->shipping_address; ?> <br/>
					
					<?php } if (!empty($user->body->User->location)) { ?>
					
					<b>Location    :</b><?php echo $user->body->User->location; ?> <br/>

					<?php } if (!empty($user->body->User->paypal_id)) { ?>

						<b>Paypal ID    :</b><?php echo $user->body->User->paypal_id; ?> <br/>

					<?php } if (!empty($user->body->User->contact)) { ?>

					<b>Contact    :</b><?php echo $user->body->User->contact; ?> <br/>
					
					<?php } if (!empty($user->body->User->created)) { ?>
					
					<b>Member Since    :</b><?php echo $user->body->User->created; ?> <br/>
					
					<?php } if (!empty($user->body->User->profile_text)) { ?>
					
					<b>Profile Text    :</b><?php echo $user->body->User->profile_text; ?> <br/>
					
					<?php } ?>

				</p>
			</div>



			<div class="clear">

			</div>

			<div class="listing">
				<ul>
					<li><?php echo $this->Html->link(__('View User Games'), array('controller' => 'users', 'action' => 'games', $user->body->User->id)); ?></li>
				</ul>
				<ul>
					<li><?php echo $this->Html->link(__('Offers'), array('controller' => 'users', 'action' => 'offers', $user->body->User->id)); ?></li>
				</ul>
			</div>

		</div>
	</div>
</div>