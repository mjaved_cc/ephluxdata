
<div class="main_column">
	<div class="box">
		<div class="body">

			<?php echo $this->Form->create('Page', array('controller' => 'pages', 'action' => 'add_page')); ?>

			<p> <?php echo $this->Form->input('title', array('label' => 'Title')); ?> <p>
			<p> <?php echo $this->Form->input('desc', array('label' => 'Description')); ?> <p>

				<?php $options = array('1' => 'Enable','0' => 'Disable' ); ?>
				
				<p><?php echo $this->Form->input('status', array('options' => $options, 'label' => 'Status')); ?></p>
				
				<?php $options = array('page' => 'Page','email_template' => 'Email Template' ); ?>
				
				<p><?php echo $this->Form->input('page_type', array('options' => $options, 'label' => 'Page Type')); ?></p>

			<p>  <?php echo $this->Form->submit('Submit'); ?> </p>


		</div>
	</div>
</div>