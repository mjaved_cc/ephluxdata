<?php 

if(!empty($pages)) {
	
	$pages = json_decode($pages); //pr($pages);
	
}

if(!empty($status)){
	
	echo $status;
}

?>

<div class="main_column">
	<div class="box">
		<div class="body">
			
			<?php if (!empty($pages->body)) { ?>

				<h2>Email Templates</h2>			

				<table border="0" cellpadding="0" cellspacing="0" class="grid_table wf">

					<thead>
						<tr>
							<th><?php echo __('S.NO'); ?></th>
							<th><?php echo __('Title'); ?></th>
							<th><?php echo __('Action'); ?></th>			
						</tr>
					</thead>
					
					<tbody>
						<?php 
						
						$count = 1;
						if (!empty($pages->body)) {
							foreach ($pages->body as $page): 
								?>
								<tr>
									<td align="center"><?php echo $count; ?>&nbsp;</td>
									<td align="center"><?php echo $page->Page->title ?>&nbsp;</td>
									<td align="center"><?php echo $this->Html->link(__('Edit'), array('controller' => 'pages', 'action' => 'edit', $page->Page->id)); ?>&nbsp;</td>
									
								</tr>
								
								<?php $count = $count + 1; ?>

								<?php
							endforeach;
						} else {
							?>
							<tr>
								<td colspan="9">No Pages found.</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>


			<?php }  ?>
			

		</div>
	</div>
</div>