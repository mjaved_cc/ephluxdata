<?php

class ApiGameComponent extends Component {

	const BASE_CODE = API_GAME_BASE_CODE;

	public $components = array('ApiResponse', 'App');
	private $_controller;

	function startup(Controller $controller) {

		$this->_controller = $controller;
	}

	/**
	 * Get Game Detail By ID
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 1-10
	 */
	function game_detail_by_id($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_id) && !empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$game_detail = $this->_controller->Game->get_game_detail_by_id($data->body->game_id, $data->body->user_id);

			if (!empty($game_detail)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game Details";
				$this->ApiResponse->body = $game_detail;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '1';
				$this->ApiResponse->msg = "No Game Details found against this id";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '2';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get Game Detail By Barcode
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 11-20
	 */
	function game_detail_by_barcode($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_barcode) && !empty($data->body->user_id) || $data->body->user_id == 0 && md5(SIGNATURE) == $data->body->key) {

			$game_detail = $this->_controller->Game->get_game_detail_by_barcode($data->body->game_barcode, $data->body->user_id);

			if (!empty($game_detail)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game Details";
				$this->ApiResponse->body = $game_detail;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '11';
				$this->ApiResponse->msg = "No Game Details found against this barcode";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '12';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get Game Detail By Title
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 21-30
	 */
	function game_detail_by_title($json_data) {

		$data = json_decode($json_data); //pr($data); exit

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->game_title) &&
				(!empty($data->body->user_id) || $data->body->user_id == 0) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			$game_detail = $this->_controller->Game->get_game_detail_by_title(Sanitize::escape($data->body->game_title), $data->body->user_id, $data->body->start, $data->body->end);

			if (!empty($game_detail)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game Details";
				$this->ApiResponse->body = $game_detail;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '21';
				$this->ApiResponse->msg = "No Game Details found against this title";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '22';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get All User Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 31-40
	 */
	function games_by_user_id($json_data) {

		$data = json_decode($json_data);


		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$user_games = $this->_controller->UsersGame->get_all_games_by_user_id($data->body->user_id);

			if (!empty($user_games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User Games";
				$this->ApiResponse->body = $user_games;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '31';
				$this->ApiResponse->msg = "No Games Found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '32';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get All Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 41-50
	 */
	function list_all_games($json_data, $web = null) {

		$data = json_decode($json_data);

		if (md5(SIGNATURE) == $data->body->key) {

			$game_detail = $this->_controller->Game->get_all_games($web);

			if (!empty($game_detail)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->body = $game_detail;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '41';
				$this->ApiResponse->msg = "No Games found found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '42';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save User Game Record
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 51-60
	 */
	function save_user_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) &&
				!empty($data->body->game_id) &&
				isset($data->body->amount) &&
				!empty($data->body->trade) &&
				!empty($data->body->condition) &&
				!empty($data->body->comes_with) &&
				md5(SIGNATURE) == $data->body->key) {

			$data = array(
				'user_id' => $data->body->user_id,
				'game_id' => $data->body->game_id,
				'amount' => $data->body->amount,
				'trade' => $data->body->trade,
				'condition' => $data->body->condition,
				'comes_with' => $data->body->comes_with,
				'comment' => isset($data->body->comment) ? Sanitize::escape($data->body->comment) : '',
				'status' => 1
			);

			$status = $this->_controller->UsersGame->save_user_game_record($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "User Game Record Inserted";
				$this->ApiResponse->body = array('user_game_id' => $status);
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '51';
				$this->ApiResponse->msg = "User Game can not be saved. It is already exists in your game list or wishlist";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '52';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Save User Wish List Record
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 61-70
	 */
	function save_user_wishlist($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && !empty($data->body->trade) && md5(SIGNATURE) == $data->body->key) {

			$game_ids = $data->body->game_id;

			$array_game_ids = explode(',', $game_ids);

			$status = $this->_controller->UsersWishlists->save_wishlist_record($data->body->user_id, $data->body->trade, $array_game_ids);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = $status;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '61';
				$this->ApiResponse->msg = "Whislists Record could not Saved";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '62';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get All Wishlists of User
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 71-80
	 */
	function get_user_all_wishlist($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$wishlists = $this->_controller->UsersWishlists->get_user_wishlists($data->body->user_id);

			if (!empty($wishlists)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $wishlists;
				$this->ApiResponse->msg = "Whislists found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '71';
				$this->ApiResponse->msg = "No wishlist found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '72';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get who possess a game and want to sell or exchage
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 81-90
	 */
	function users_who_want_to_sell_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$users = $this->_controller->UsersGame->users_who_sell_specific_game($data->body->game_id);

			if (!empty($users)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $users;
				$this->ApiResponse->msg = "Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '81';
				$this->ApiResponse->msg = "No Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '82';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get who wish to buy specific game and want to buy or exchage
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 91-100
	 */
	function users_who_want_to_buy_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$users = $this->_controller->UsersGame->users_who_buy_specific_game($data->body->game_id);

			if (!empty($users)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $users;
				$this->ApiResponse->msg = "Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '91';
				$this->ApiResponse->msg = "No Users found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '92';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Search Game by title
	 * @param $search_keyword
	 * @return array $api_response
	 * Error code range : 101-110
	 */
	function search_game($search_keyword) {

		$search_result = $this->_controller->Game->search_game_detail_by_title(Sanitize::escape($search_keyword));

		if (!empty($search_result)) {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->body = $search_result;
			$this->ApiResponse->msg = "Search Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '101';
			$this->ApiResponse->msg = "No Search Result found";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Update Game Record
	 * @param $data
	 * @return array $api_response
	 * Error code range : 101-110
	 */
	function Update_game_record($data) {

		$status = $this->_controller->Game->update_recored($data);

		if (!empty($status)) {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Game record updated";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '101';
			$this->ApiResponse->msg = "Game Record could not updated";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Delete Game Record
	 * @param $id
	 * @return array $api_response
	 * Error code range : 110-120
	 */
	function delete_record($id) {

		$status = $this->_controller->Game->delete_game_recored($id);

		if (!empty($status)) {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '0';
			$this->ApiResponse->msg = "Game record Deleted";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '101';
			$this->ApiResponse->msg = "Game Record could not deleted";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Most Wanted Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 121-130
	 */
	function most_wanted_games($json_data, $web = null) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if ((!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				isset($data->body->user_id) &&
				md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->UsersWishlists->top_wanted_games($web, $data->body->start, $data->body->end, $data->body->user_id);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '121';
				$this->ApiResponse->msg = "No games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '122';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Most Owned Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 131-140
	 */
	function most_owned_games($json_data, $web = null) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if ((!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key &&
				isset($data->body->user_id)
		) {

			$games = $this->_controller->UsersWishlists->top_owned_games($web, $data->body->start, $data->body->end, $data->body->user_id);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '131';
				$this->ApiResponse->msg = "No games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '132';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Upcoming and New Released Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 141-150
	 */
	function upcoming_new_releases($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->search_param) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				isset($data->body->user_id) &&
				md5(SIGNATURE) == $data->body->key) {

			if ($data->body->search_param == 1) {

				$param = UPCOMING_RELEASES;
				$games = $this->_controller->Game->upcoming_and_new_releases($param, $data->body->start, $data->body->end, $data->body->user_id);
			} elseif ($data->body->search_param == 2) {

				$param = NEW_RELEASES;
				$games = $this->_controller->Game->upcoming_and_new_releases($param, $data->body->start, $data->body->end, $data->body->user_id);
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '141';
				$this->ApiResponse->msg = "Search Parameter invalid";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '141';
				$this->ApiResponse->msg = "No games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '142';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User selling his Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 151-160
	 */
	function selling_game_user($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->user_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			if (!empty($data->body->game_id)) {
				$games = $this->_controller->UsersWishlists->ongoing_offers_for_have_game($data->body->user_id, $data->body->game_id);
			} else {
				$games = $this->_controller->UsersWishlists->selling_game($data->body->user_id, $data->body->start, $data->body->end, $data->body->dist);
			}

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found for selling";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '151';
				$this->ApiResponse->msg = "No games found for selling";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '152';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Buying Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 161-170
	 */
	function buying_game_user($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (!empty($data->body->user_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			if (!empty($data->body->game_id)) {

				$games = $this->_controller->Game->ongoing_offers_for_wish_game($data->body->user_id, $data->body->game_id);
			} else {

				$games = $this->_controller->Game->buying_game($data->body->user_id, $data->body->start, $data->body->end, $data->body->dist);
			}

			$games = array_slice($games, 0);  // JUGAT

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found for buying";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '161';
				$this->ApiResponse->msg = "No games found for buying";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '162';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Games for you
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 171-180
	 */
	function games_for_you($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = DEFAULT_LIMIT_FOR_YOU_GAMES;
		}

		if (isset($data->body->user_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				isset($data->body->genre_ids) &&
				isset($data->body->console_ids) &&
				md5(SIGNATURE) == $data->body->key) {

			if(empty($data->body->genre_ids) || empty($data->body->console_ids)) {

				$games = $this->_controller->Game->for_you($data->body->user_id, $data->body->start, $data->body->end);
			} else {
				
				$games = $this->_controller->Game->for_you_in_genre_ids($data->body->genre_ids, $data->body->console_ids, $data->body->start, $data->body->end);
				
			}

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games for you found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '171';
				$this->ApiResponse->msg = "Games for you not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '172';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Trading Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 181-190
	 */
	function trading_game_user($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->trading_game($data->body->user_id, $data->body->dist);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found for trading";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '181';
				$this->ApiResponse->msg = "No Games found for trading";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '182';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User All Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 191-200
	 */
	function all_games($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->all($data->body->user_id);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found for All";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '191';
				$this->ApiResponse->msg = "No Games found for All";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '192';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Wish Specific Game Exchange and Buy combination
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 201-210
	 */
	function all_wish_games($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->all_wish($data->body->user_id, $data->body->game_id);

			if (!empty($games['wish_buy']) || !empty($games['wish_exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '201';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '202';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Have Specific Game Exchange and Sell combination
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 211-220
	 */
	function all_have_games($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->all_have($data->body->user_id, $data->body->game_id);

			if (!empty($games['have_sell']) || !empty($games['have_exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '211';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '212';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User ongoing Buy, Sell and Exchange deals
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 221-230
	 */
	function current_deals($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->user_current_deals($data->body->user_id);

			if (!empty($games['buy']) || !empty($games['sell']) || !empty($games['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '221';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '222';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Have Specific Game Exchange and Sell combination
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 231-240
	 */
	function send_receive_deals($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->user_send_receive_deals($data->body->user_id);

			if (!empty($games['receive_confirm']['buy']) || !empty($games['receive_confirm']['sell']) || !empty($games['receive_confirm']['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '231';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '232';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Current Deals on Specific Game
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 241-250
	 */
	function current_deals_on_specific_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->user_current_deals_on_specific_game($data->body->user_id, $data->body->game_id);

			if (!empty($games['sell']) || !empty($games['exchange'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '241';
				$this->ApiResponse->msg = "No Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '242';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Game Screenshot
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 251-260
	 */
	function extra_detail($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_id) && md5(SIGNATURE) == $data->body->key) {

			$screenshots = $this->_controller->Game->detail_extra($data->body->user_id, $data->body->game_id);

			if ($screenshots) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $screenshots;
				$this->ApiResponse->msg = "Game extra details found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '251';
				$this->ApiResponse->msg = "No Game extra details found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '252';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Update Game Record
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 261-270
	 */
	function update_have_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) &&
				!empty($data->body->game_id) &&
				isset($data->body->amount) &&
				!empty($data->body->trade) &&
				!empty($data->body->condition) &&
				!empty($data->body->comes_with) &&
				md5(SIGNATURE) == $data->body->key) {

			$data = array(
				'user_id' => $data->body->user_id,
				'game_id' => $data->body->game_id,
				'amount' => $data->body->amount,
				'trade' => $data->body->trade,
				'condition' => $data->body->condition,
				'comes_with' => $data->body->comes_with,
				'comment' => Sanitize::escape($data->body->comment)
			);

			$status = $this->_controller->UsersGame->update_user_game_record($data);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game Record Updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '261';
				$this->ApiResponse->msg = "Game Record could not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '262';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Update Wish Record
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 271-280
	 */
	function update_wish_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && !empty($data->body->trade) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->UsersWishlists->update_wishlist_record($data->body->user_id, $data->body->game_id, $data->body->trade);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Whislists Record updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '271';
				$this->ApiResponse->msg = "Whislists Record could not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '272';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Delete User Wish or Mygame
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 281-290
	 */
	function delete_game($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->user_id) && !empty($data->body->game_id) && !empty($data->body->game_type) && md5(SIGNATURE) == $data->body->key) {

			$status = $this->_controller->UsersGame->delete_have_or_wish($data->body->user_id, $data->body->game_id, $data->body->game_type);

			if (!empty($status)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game deleted successfully";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '281';
				$this->ApiResponse->msg = "Game could not deleted";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '282';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * User Action for Send and Receive Deal
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 291-300
	 */
	function send_receive_action($json_data) {

		$data = json_decode($json_data);

		if (isset($data->body->user_id) && isset($data->body->deal_id) && isset($data->body->receive_action) && isset($data->body->send_action) && isset($data->body->action) && md5(SIGNATURE) == $data->body->key) {

			$action = $this->_controller->Game->user_send_receive_action($data->body);

			//pr($action); die();
			if (!empty($action['status'])) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $action;
				$this->ApiResponse->msg = "Action updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '291';
				$this->ApiResponse->msg = "Action not updated";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '292';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Genre Specific Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 301-310
	 */
	function specific_games_genre($json_data) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if (isset($data->body->genre_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->genre_games($data->body->genre_id, $data->body->start, $data->body->end);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Genre Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '301';
				$this->ApiResponse->msg = "Genre Games could not found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '302';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Recommended Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 311-320
	 */
	function recommended_wishlist($json_data) {

		$data = json_decode($json_data);

		if (isset($data->body->user_id) && md5(SIGNATURE) == $data->body->key) {

			$result = $this->_controller->Game->recomend_wish($data->body->user_id);

			if (!empty($result)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $result;
				$this->ApiResponse->msg = "Recommended Games Found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '291';
				$this->ApiResponse->msg = "Recommended Games Could not Found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '292';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Get Game AutoComplete
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 321-330
	 */
	function game_autocomplete($json_data) {

		$data = json_decode($json_data);

		if (!empty($data->body->game_title) && md5(SIGNATURE) == $data->body->key) {

			$game_detail = $this->_controller->Game->get_game_autocomplete(Sanitize::escape($data->body->game_title));

			if (!empty($game_detail)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->msg = "Game match found";
				$this->ApiResponse->body = $game_detail;
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '321';
				$this->ApiResponse->msg = "No match found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '322';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

	/**
	 * Top Mobile Games
	 * @param $json_data
	 * @return array $api_response
	 * Error code range : 331-340
	 */
	function top_mobile_games($json_data, $web = null) {

		$data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = 100;
		}

		if ((!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				md5(SIGNATURE) == $data->body->key) {

			$games = $this->_controller->Game->top_mobile_games($data->body->start, $data->body->end);

			if (!empty($games)) {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '0';
				$this->ApiResponse->body = $games;
				$this->ApiResponse->msg = "Games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			} else {

				$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
				$this->ApiResponse->offset = '331';
				$this->ApiResponse->msg = "No games found";
				$this->ApiResponse->format = ApiResponseComponent::JSON;
				return $this->ApiResponse->get();
			}
		} else {

			$this->ApiResponse->base = ApiGameComponent::BASE_CODE;
			$this->ApiResponse->offset = '332';
			$this->ApiResponse->msg = "invalid parameters";
			$this->ApiResponse->format = ApiResponseComponent::JSON;
			return $this->ApiResponse->get();
		}
	}

    /**
     * See more recommended
     * @param $json_data
     * @return ApiResponse
     *
     * Error code range : 341-370
     */
    function recommended_see_more($json_data) {

        $data = json_decode($json_data);

		if (!array_key_exists('start', $data->body)) {

			$data->body->start = 0;
		}

		if (!array_key_exists('end', $data->body)) {

			$data->body->end = DEFAULT_LIMIT_FOR_YOU_GAMES;
		}

		if (isset($data->body->user_id) &&
				(!empty($data->body->start) || $data->body->start == 0) &&
				!empty($data->body->end) &&
				isset($data->body->genre_ids) &&
				isset($data->body->console_ids) &&
				md5(SIGNATURE) == $data->body->key) {
			
			$param = array(
				
				'user_id' => $data->body->user_id,
				'start' => $data->body->start,
				'end' => $data->body->end,
				'genre_ids' => $data->body->genre_ids,
				'console_ids' => $data->body->console_ids
				
			);

            $recommended_games = $this->_controller->Game->see_more_recommended($param);

            if(is_array($recommended_games) && !empty($recommended_games)){

                $this->ApiResponse->offset = '0';
                $this->ApiResponse->msg = "Recommended Games Found";
                $this->ApiResponse->body = $recommended_games;

            } else {
                $this->ApiResponse->offset = '1';
                $this->ApiResponse->msg = "Recommended Games Found";
            }

        } else {

            $this->ApiResponse->offset = '341';
            $this->ApiResponse->msg = "invalid parameters";
        }

        $this->ApiResponse->base = ApiGameComponent::BASE_CODE;
        $this->ApiResponse->format = ApiResponseComponent::JSON;
        return $this->ApiResponse->get();
    }

    /**
     * See more jiggster pick
     * @param $json_data
     * @return ApiResponse
     *
     * Error code range : 341-370
     */
    function jiggster_pick_see_more($json_data) {

        $data = $this->App->decoding_json($json_data);

        if (isset($data['body']['key']) && md5(SIGNATURE) == $data['body']['key']) {

            $body = $data['body'];

            /** START: default case  **/
            $end = DEFAULT_LIMIT_FOR_YOU_GAMES ;
            $start = 0;
            $jiggster_picks = array(
                'JiggsterPick' => array()
            );
            /** END: default case  **/

            if(isset($data['body']['user_id']) && empty($data['body']['user_id'])){
                if( isset($body['end']) )
                    $end = $body['end'] ;


                if( isset($body['start']) )
                {
                    $start = $body['start'] ;
                }

                $jiggster_picks = $this->_controller->Game->get_jiggster_picks($start , $end);

                if(is_array($jiggster_picks) && !empty($jiggster_picks)){

                    $this->ApiResponse->offset = '0';
                    $this->ApiResponse->msg = "Jiggster Picks Game(s) found.";
                    $this->ApiResponse->body = $jiggster_picks;

                } else {
                    $this->ApiResponse->offset = '1';
                    $this->ApiResponse->msg = "Jiggster Picks Game(s) not found.";
                }
            } else {
                $this->ApiResponse->offset = '0';
                $this->ApiResponse->msg = "Jiggster Picks Game(s) is public only.";
                $this->ApiResponse->body = $jiggster_picks;
            }

        } else {

            $this->ApiResponse->offset = '341';
            $this->ApiResponse->msg = "invalid parameters";
        }

        $this->ApiResponse->base = ApiGameComponent::BASE_CODE;
        $this->ApiResponse->format = ApiResponseComponent::JSON;
        return $this->ApiResponse->get();
    }
    function genre_see_more($json_data)
    {
        $data = $this->App->decoding_json($json_data);
        $data=$data['body'];
        if (isset($data['key']) && md5(SIGNATURE) == $data['key'] &&
            isset($data['genre_id'])) {

            $body = $data;

            /** START: default case  **/
            $end = DEFAULT_LIMIT_FOR_YOU_GAMES;
            $start = 0;
            /** END: default case  **/

            if (isset($body['end']))
                $end = $body['end'];


            if (isset($body['start'])) {
                $start = $body['start'];
            }
            $genreid = $data['genre_id'];
            $consoles = "";
            if (isset($data['console_ids']))
                $consoles = $data['console_ids'];
            $userid = "0";
            if (isset($data['user_id']))
                $userid = $data['user_id'];
            $genre_games = $this->_controller->Game->get_see_more_genre_games($genreid, $start,
                $end, $consoles, $userid);
           
            if (is_array($genre_games) && !empty($genre_games)) {

                $this->ApiResponse->offset = '0';
                $this->ApiResponse->msg = "Game(s) found.";
                $this->ApiResponse->body = $genre_games;

            } else {
                $this->ApiResponse->offset = '1';
                $this->ApiResponse->msg = "Game(s) not found.";
            }

        } else {

            $this->ApiResponse->offset = '341';
            $this->ApiResponse->msg = "invalid parameters";
        }

        $this->ApiResponse->base = ApiGameComponent::BASE_CODE;
        $this->ApiResponse->format = ApiResponseComponent::JSON;
        return $this->ApiResponse->get();
    }

}

?>