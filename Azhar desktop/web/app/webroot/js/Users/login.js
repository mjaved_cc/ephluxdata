$(document).ready(function() {
	$('#UserLoginForm').validate({
		rules: {
			
			"data[User][username]": {
				required : true,
				email : true
			},
			"data[User][password]": {
				required : true,
				minlength : 8
			}
		}
	});

	//$('#UserSignupForm').submit(function () { return false;})

});

