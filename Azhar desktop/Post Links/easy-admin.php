<?php
/*
  Plugin Name: Post Links
  Description: Plugin for scroller
  Version: 1.0

 */
global $wpdb;

if (isset($_REQUEST['action']) and $_REQUEST['action'] == "del") {
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
    $wpdb->query("delete from wp_custom_slider where id=$id");

    header("Location:" . get_admin_url() . 'admin.php?page=mt-top-level-handle&msg=Image deleted');
    exit;
}

if (isset($_POST['edit_image'])) {
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
    $image_path = $_POST['image_path'];
    $image_link = $_POST['image_link'];
    $target = $_POST['target'];

    $wpdb->update(
            'wp_custom_slider', array(
        'image_path' => $image_path,
        'image_link' => $image_link,
        'target' => $target,
        'image_order' => getMaxOrder()
            ), array('id' => $id), array(
        '%s',
        '%s',
        '%s'
            ), array('%d')
    );

    header("Location:" . get_admin_url() . 'admin.php?page=mt-top-level-handle&msg=Image updated.');
    exit;
}
if (isset($_POST['add_image'])) {
    $image_path = $_POST['image_path'];
    $image_link = $_POST['image_link'];
    $target = $_POST['target'];

    $wpdb->insert(
            'wp_custom_slider', array(
        'image_path' => $image_path,
        'image_link' => $image_link,
        'target' => $target,
        'image_order' => getMaxOrder()
            ), array(
        '%s',
        '%s',
        '%s'
            )
    );

    header("Location:" . get_admin_url() . 'admin.php?page=mt-top-level-handle&msg=New image saved.');
    exit;
    //echo get_admin_url().'admin.php?page=mt-top-level-handle';
    //wp_redirect(get_admin_url().'?page=mt-top-level-handle');exit;
}

if (isset($_POST['change-order'])) {
    $image_orders = $_POST['image_order'];
    foreach ($image_orders as $k => $v) {
        $wpdb->update(
                'wp_custom_slider', array(
            'image_order' => $v
                ), array('id' => $k), array(
            '%s'
                ), array('%d')
        );
    }
    header("Location:" . get_admin_url() . 'admin.php?page=mt-top-level-handle&msg=New order saved.');
    exit;
}

function getMaxOrder() {
    global $wpdb;
    $user_count = $wpdb->get_var("SELECT max(image_order) FROM wp_custom_slider");
    return $user_count + 1;
}

function wp_myCustomPlugin_install() {
    
}

function wp_myCustomPlugin_add_pages() {
    add_menu_page(__('Post Links', 'menu-test'), __('Post List', 'menu-test'), 'manage_options', 'mt-top-level-handle', 'mt_toplevel_page');
    //add_submenu_page('mt-top-level-handle', __('Test Sublevel','menu-test'), __('Test Sublevel','menu-test'), 'manage_options', 'sub-page', 'mt_sublevel_page');
    // add_submenu_page('mt-top-level-handle', __('Add Images', 'menu-test'), __('Add Slider Images', 'menu-test'), 'manage_options', 'add-custom-img', 'mt_sublevel_page2');
    //add_options_page('My Custom Plugin', 'My Custom Plugin', 8, __FILE__, 'wp_myCustomPluginOptions');
}

//if (isset($_GET['page']) && $_GET['page'] == 'wp-custom/easy-admin.php'){
wp_enqueue_script('jquery');
wp_register_script('my-upload', plugins_url("wp-custom/script.js"), array('jquery', 'media-upload', 'thickbox'));
wp_register_script('jcarousel', plugins_url("wp-custom/lib/jquery.jcarousel.min.js"));
wp_register_style('jcarousel-skins', plugins_url("wp-custom//skins/tango/skin.css"));
wp_enqueue_script('jcarousel');
wp_enqueue_style('jcarousel-skins');
wp_enqueue_script('my-upload');
wp_enqueue_style('thickbox');
wp_enqueue_script('thickbox');

//}
// mt_toplevel_page() displays the page content for the custom Test Toplevel menu
function mt_toplevel_page() {
    global $wpdb;
    $url = admin_url(); 
    $url .= 'admin.php?page=mt-top-level-handle' ;
    
    $archive_query = new WP_Query('showposts=1000');
    ?>

    <script>
        function validateForm()
        {
            var x=document.forms["form1"]["postname"].value;
            var y=document.forms["form1"]["url"].value;
            if (x==null || x=="")
            {
                alert("First name must be filled out");
                return false;
            }
          
            if (y==null || y=="")
            {
                alert("First name must be filled out");
                return false;
            }
        }
                
    </script>

    <form id="form1" method="post" onsubmit="return validateForm()"  >

        <select name="postname">
            <!--  <?php /*  while  ($archive_query->have_posts()) : $archive_query->the_post(); ?>
      <option value="<?php the_permalink() ?>"><?php the_title(); ?></option>
      <?php endwhile; */ ?> -->

            <?php $archive_query = new WP_Query(array('post_type' => 'page')); ?>   
            <?php while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
                <option value="<?php the_ID(); ?>"><?php the_title(); ?></option> 

               
                            <?php endwhile; ?>
        </select>


        Page URL :  <input type="text" name="url">
        <input type="submit" name="Submit" value="Submit">
    </form>  

    <?php
    if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {

        if (isset($_POST['postname'])) {
            echo "Page ID: " . htmlspecialchars($_POST['postname']);
            echo "<br />URL : " . $_POST['url'];
        }

        


       /* $wpdb->insert(
                'wp_post_links', array(
            'source_url' => $_POST['postname'],
            'des_url' => $_POST['url'],
                ), array(
            '%s',
            '%s',
                )
        ); */
        
        wp_redirect( $url );
        exit;
    }
}

function pagination($rows, $per_page, $current_page, $page_link) {
    global $core, $C;
    // Create a Page Listing
    $page = ceil($rows / $per_page);
    // If there's only one page, return now and don't bother
    /* if($page == 1) {
      return;
      } */
    // Pagination Prefix
    $per = ($rows <= $per_page) ? $rows : $per_page;
    $output = "Showing $current_page - $per out of $rows records &nbsp;&nbsp;&nbsp;";
    $output .= "Pages: ";
    // Should we show the FIRST PAGE link?
    /* if($current_page > 2) {
      $output .= "<a href=\"". $page_link ."&page=1/\" title=\"First Page\"><<</a>";
      } */
    // Should we show the PREVIOUS PAGE link?
    if ($current_page > 1) {
        $previous_page = $current_page - 1;
        $output .= " <a href=\"" . $page_link . "&pg=" . $previous_page . "\" title=\"Previous Page\"><< Pre</a>";
    }
    // Current Page Number
    $output .= "<strong>[ " . $current_page . " ]</strong>";
    // Should we show the NEXT PAGE link?
    if ($current_page < $page) {
        $next_page = $current_page + 1;
        $output .= "<a href=\"" . $page_link . "&pg=" . $next_page . "\" title=\"Next Page\">Next >></a>";
    }
    // Should we show the LAST PAGE link?
    /* if($current_page < $page - 1) {
      $output .= " <a href=\"". $page_link ."?page=". $page ."/\" title=\"Last Page\">>></a>";
      } */
    // Return the output.
    return $output;
}

// mt_sublevel_page2() displays the page content for the second submenu
// of the custom Test Toplevel menu
function mt_sublevel_page2() {
    global $wpdb;
    ?>
    <div class="wrap">
        <?php
        echo "<h2>" . __("Add Slider Images", "menu-test") . '</h2>';

        if (isset($_REQUEST['edit']) and $_REQUEST['edit'] == true) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
            $image_path = "";
            $image_link = "";
            $target = "";

            $myrow = $wpdb->get_row("select * from wp_custom_slider WHERE id = $id", ARRAY_A);
            if (!$myrow) {
                ?>
                <div class="updated" id="message">
                    <p> No image found </p>
                </div>
                <?php
                exit;
            } else {
                $image_path = $myrow['image_path'];
                $image_link = $myrow['image_link'];
                $target = $myrow['target'];
            }
        }
        ?>
        <div id="imgArea"> </div>
        <form name="image_form" method="post" action="" enctype="multipart/form-data" onsubmit="return validateImgForm();">
            <table class="form-table">
                <tbody>
                    <tr class="form-field form-required">
                        <th scope="row"><label>Image <span class="description">(required)</span></label></th>
                        <td><input type="text" aria-required="true" value="<?php echo $image_path; ?>" id="image_path" style="width:60%;" name="image_path">
                            <input type="button" class="button button-primary" style="width:91px;" name="image_path_btn" id="image_path_btn" value="Upload New" /></td>
                    </tr>
                    <tr class="form-field form-required">
                        <th scope="row"><label>Image Link</label></th>
                        <td><input type="text" value="<?php echo $image_link; ?>" id="image_link" style="width:71%;" name="image_link"></td>
                    </tr>
                    <!--<tr class="form-field">
                      <th scope="row"><label>order</label></th>
                      <td><input type="text" value="" id="image_order" name="image_order"></td>
                    </tr>-->
                    <tr class="form-field">
                        <th scope="row"><label>Target </label></th>
                        <td><select name="target" id="target" style="width:auto;">
                                <option value="_blank" <?php if ($target == "_blank") { ?> selected="selected" <?php } ?>>_blank</option>
                                <option value="_top" <?php if ($target == "_top") { ?> selected="selected" <?php } ?>>_top</option>
                                <option value="_self" <?php if ($target == "_self") { ?> selected="selected" <?php } ?>>_self</option>
                            </select></td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <?php
                if (isset($_REQUEST['edit']) and $_REQUEST['edit'] == true) {
                    ?>
                    <input type="hidden" value="<?php echo $id; ?>" name="id" />
                    <input type="submit" value="Save" class="button button-primary" name="edit_image">
                    <?php
                } else {
                    ?>
                    <input type="submit" value="Save" class="button button-primary" name="add_image">
                    <?php
                }
                ?>    
            </p>
        </form>
    </div>
    <?php
}

function wp_imageShortCode($atts) {

    global $post;
    global $wpdb;
    $thePostID = $post->ID;

extract(shortcode_atts(array(
      'url' => '',
   ), $atts));

    //$data = "dfssdafjsdj";
   // $myrow = $wpdb->get_row("select * from wp_post_links WHERE source_url = $thePostID", ARRAY_A);
   // $url = $myrow['des_url'];
    

    if (isset($url)) {
        $response = get($url);
        $website_content = $response['content'];
        preg_match_all('/<img[^>]+>/i', $website_content, $html);

        $content = explode("<p>", $website_content);
        $paragraph = substr($content[20], 0, 200);


        //return $url;
        //return $html;
        //return $html[0][9];
        return '<div id="image123" style="width:300px">' . $html[0][9] . '<script> 
            $("#image123").css("width", "300px"); 
        </script>' . $paragraph . '</div>';
    } 
}

add_shortcode('postlink', 'wp_imageShortCode');
add_action('admin_menu', 'wp_myCustomPlugin_add_pages');
register_activation_hook(__FILE__, 'wp_myCustomPlugin_install');
?>