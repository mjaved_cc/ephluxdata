jQuery(document).ready(function() {
	jQuery('#image_path_btn').click(function() {
		 formfield = jQuery('#image_path').attr('name');
		 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=1');
		 return false;
	});
 
	window.send_to_editor = function(html) {
		 imgurl = jQuery('img',html).attr('src');
		 jQuery('#image_path').val(imgurl);
		 tb_remove();
	}
	
	jQuery('.my-delete').click(function(){
		var res = confirm("Are you sure you want to delete?");
		if(res)
		{
			var id = jQuery(this).attr('id');
			document.location.href='admin.php?page=mt-top-level-handle&action=del&id='+id;	
		}
	})
	
	jQuery('#mycarousel').jcarousel({
        auto: 2,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
});

function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

function validateImgForm()
{
	jQuery('.cus-error').remove();
	var image_path = jQuery('#image_path').val();
	var image_link = jQuery('#image_link').val();
	//alert(validateURL(image_link))
	if(image_path=="")
	{
		jQuery('#image_path').closest('td').append('<p style="color:red;" class="cus-error">Please upload image</p>');
		return false;
	}
	else
	{
		jQuery('#image_path').next('.cus-error').remove();
	}
	
	if(image_link!="" && validateURL(image_link)==false)
	{
		jQuery('#image_link').closest('td').append('<p style="color:red;" class="cus-error">Please enter valid link</p>');
		return false;
	}
	else
	{
		jQuery('#image_link').next('.cus-error').remove();
	}
}

function validateURL(textval) {
  var urlregex = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  return urlregex.test(textval);
}


