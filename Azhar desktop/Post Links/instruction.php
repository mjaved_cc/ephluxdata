
Add this function to functions.php
function get($url) {

    // set required POST option for CURL
    $options = array(
        CURLOPT_URL => $url, // set the url   
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120   // timeout on response 
    );

    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $error_code = curl_errno($ch);
    $error_msg = curl_error($ch);
    $header = curl_getinfo($ch);
    //close connection
    curl_close($ch);

    $header['error_code'] = $error_code;
    $header['error_msg'] = $error_msg;
    $header['content'] = $content;
    return $header;
}


Add this into header
<script>

$(document).ready(function() {

$("#image123 img").attr('width','100'); 
$("#image123 img").attr('height','100');  
$("#image123").css("width", "300px");  
});



</script>