<?
/* (c) Samuel Borgman, Al Ostoura */

define('EHSW_MODULE', 'example_homescreen_widget'); // Too lazy to type EXAMPLE_HOMESCREEN_WIDGET fifteen times :)
define('EHSW_MODULE_PATH', CUSTOM_PATH . '/modules/'.EHSW_MODULE);


AngieApplication::setForAutoload(array(
	'ExampleHomescreenWidget' => EHSW_MODULE_PATH.'/models/homescreen_widgets/ExampleHomescreenWidget.class.php')
);


?>
