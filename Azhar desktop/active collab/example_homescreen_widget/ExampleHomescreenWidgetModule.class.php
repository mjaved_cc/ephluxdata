<?
/* (c) Samuel Borgman, Al Ostoura */

class ExampleHomescreenWidgetModule extends AngieModule {

	protected $name = 'example_homescreen_widget';

	protected $version = '0.2';

	public function defineHandlers() {
		EventsManager::listen('on_homescreen_widget_types', 'on_homescreen_widget_types');
	}

	public function getDisplayName() {
		return lang('Example Homescreen Widget');
	}

	public function getDescription() {
		return lang('An example Homescreen Widget, it kicks all kinds of bad ass');
	}
}

?>

