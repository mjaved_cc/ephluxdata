<?
/* (c) Samuel Borgman, Al Ostoura */

class ExampleHomescreenWidget extends HomescreenWidget {

	function getName() {
		return lang('Awesome example widget');
	}

	function getGroupName() {
		return lang('Examples');
	}

	function getDescription() {
		return lang('Example homescreen widget');
	}

	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = NULL) {
		return lang('Hello world!');
	}
 
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = NULL) {
		return 'Welcome to my humble widget';
	}
}



