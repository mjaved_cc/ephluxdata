<?
/* (c) Samuel Borgman, Al Ostoura */

function example_homescreen_widget_handle_on_homescreen_widget_types(&$types, IUser &$user) {
	// If we want to only add certain homescreens for users with certain privileges/roles, looking a $user will probably be a good idea
	
	// For now lets just register our to be created homescreen class

	$types[] = new ExampleHomescreenWidget();
}
