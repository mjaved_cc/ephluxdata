jQuery.fn.getUserNotificationsActivityLog = function (s) { 
    var settings = jQuery.extend({
        load_more_url: null,
        entries: null,
        entries_per_load: 30,
        total_entries: 0
    }, s);
    return this.pagedObjectsList({
        load_more_url: settings.load_more_url,
        items: settings.entries,
        items_per_load: settings.entries_per_load,
        total_items: settings.total_entries,
        list_items_are: "tr",
        list_item_attributes: {
            "class": "mailing_activity_log"
        },
        columns: {			
            from: App.lang("From"),
            info: App.lang("Message"),
            created_on: App.lang("Created On")
        },
        on_add_item: function (item) {
            
            var log_entry = $(this);
            log_entry.append('<td class="from"></td><td class="info"></td><td class="created_on"></td>');

            log_entry.find("td.created_on").text(item.created_on["formatted"]);
            if (item.from) {
                $("<a></a>").attr("href", item.from["urls"]["view"]).text(item.from["display_name"]).appendTo(log_entry.find("td.from"))
            }			
            if (item.has_details) {			
                first_occurence = item.name.indexOf('{');
                // starting with index 1 means we are omitting inverted comma @ beginning
                // but end before first_occurence i.e omit braces onwards content
                desc = item.name.substring(1,first_occurence) ;
				
                $('<a href="' + item.urls["view"] + '" class="view_details" title="' + App.lang("View Details") + '">' + desc + '</a>').flyout({
                    title: App.lang("Details")
                }).appendTo(log_entry.find("td.info"))
            }
        } 
    })
};

$(document).ready(function() {
    $('#statusbar_item_facebook_like_notifications > a').append('<span id="notif_count"></span>');
    
  
    
    count_notifications();
    $("#statusbar_item_facebook_like_notifications > a").click(function(){
        $("#notif_count").html("\u00a0");   // inserting space in #notif_count
        var index = $("title").text().indexOf(")");							
        var mytitle = $("title").text().substring(index+1);								
        $(document).attr('title', mytitle);
            
    });
    
    window.setInterval(function () {
        count_notifications(); 
    }, 61000); // repeat forever, polling every 61 seconds
    
    //$('#statusbar_item_facebook_like_notifications > a').prepend('<span id="notif_count"></span>')
    
    function count_notifications(){
        $.ajax({
            type: "POST",
            url: "/projectier/public/index.php?path_info=notifications-count"
            
        }).done(function( notifications_count ) {
            //alert( $("title").text() );
            //$('#statusbar_item_facebook_like_notifications > a').prepend('<span class="notify_bg">'+msg+'</span>')
            
            if( notifications_count > 0 ) {
                
                alert($("#notif_count"));
                
                $("#notif_count").html("<span class='notify_bg'>" + notifications_count + "</span>");
                var index = $("title").text().indexOf(")");
                var mytitle = $("title").text().substring(index+1);
                mytitle = " (" + notifications_count + ") " + mytitle;									
                $(document).attr('title', mytitle) ; // inserting task count in the title
            }
            else {
                var index = $("title").text().indexOf(")");							
                var mytitle = $("title").text().substring(index+1);								
                $(document).attr('title', mytitle);								
            }
        });   
    }   
});