<?php

  /**
   * on_admin_panel event handler
   * 
   * @package 
   * @subpackage handlers
   */

  /**
   * Handle on_admin_panel event
   * 
   * @param AdminPanel $admin_panel
   */
  function facebook_like_notifications_handle_on_admin_panel(AdminPanel &$admin_panel) {
    $admin_panel->addToTools('facebook_like_', lang('Facebook Like Notifications'), Router::assemble('facebook_like_notifications_admin'), AngieApplication::getImageUrl('module.png', FACEBOOK_LIKE_NOTIFICATIONS_MODULE));
  } // on_admin_panel