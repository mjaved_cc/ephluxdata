<?php
/*
  Template Name: about
 */
?>
<?php global $post;
setup_postdata($post); ?>

<?php get_header(); ?>

<?php $page = get_page_by_title('ABOUT US', 'ARRAY_A', 'page'); ?>
<?php $feat_image = wp_get_attachment_url(get_post_thumbnail_id($page['ID'])); ?>
<div class="b">
    <ul id="menu2">
        <li><a href="#" class="active">The Company</a></li>						
        <li><a href="#">Social Responsibility</a></li>
        <li><a href="#">Careers</a></li>
    </ul>
</div>
<section id="content" role="main">	
    <div class="wrapper-g">
       <!-- <img src="<?php bloginfo('template_directory'); ?>/images/inner.jpg" width="997" /> -->
        <img src="<?php if (isset($feat_image)) echo $feat_image; ?>" width="997" /> 
    </div>
    <div class="cols-two-a wrapper-d">
        <section class="primary-ta">
            <!--    <div class="hbox-a">
                    <h1 class="header-b">Main Menu</h1>
                    <div class="hbox-inner">                            
                        <ul>
                            <li><a href="#">The Company</a></li>
                            <li><a href="#">Social Responsibility</a></li>
                            <li><a href="#">Careers</a></li>
                        </ul>
    
                    </div>
    
                    <img src="<?php /* bloginfo('template_directory'); */ ?>/images/sign.jpg" style="margin-top:10px;" />
                </div>
                <br /> -->

            <?php if (!dynamic_sidebar('sidebar-5')) : ?>

                <?php
                the_widget('Twenty_Eleven_Ephemera_Widget', '', array('before_title' => '<h1 class="widget-title">', 'after_title' => '</h1>'));
                ?>

<?php endif; // end sidebar widget area  ?>



            <img src="<?php bloginfo('template_directory'); ?>/images/twitter.jpg" width="290" />

        </section>
        <section class="primary-te panes">
            <div class="hbox-a">
                <h1 class="header-b hc-c">About Us</h1>

            </div>



            <div class="content">				                                                                        
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile;
                endif;
                ?>


            </div><!-- .content -->



        </section>
    </div>
</section><!-- #content -->

<?php get_footer(); ?>