<footer id="footer" role="contentinfo">
    
		<div class="wrapper-a">
        	<div class="col-wrapper">
            <?php if ( ! dynamic_sidebar( 'sidebar-2' ) ) : ?>

						<?php
						the_widget( 'Twenty_Eleven_Ephemera_Widget', '', array( 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
						?>

					<?php endif; // end sidebar widget area ?>
                                            
            <div class="col nomargin">
            	<img src="<?php bloginfo('template_directory'); ?>/images/rhymetech.png" alt="RYHME TECH" />
                							
			</div>
        	</div>
			<p class="col-a">Copyright © 2012 Rhyme Tech - All rights reserved.<br /> Web Design By SEO Local</p>
			<div class="col-b">
                	<a href="https://www.facebook.com"><img style="margin-right:5px; margin-top:8px;" src="<?php bloginfo('template_directory'); ?>/images/f-icon.png" alt="" /></a>
                	<a href="http://twitter.com"><img style="margin-right:5px; margin-top:8px;" src="<?php bloginfo('template_directory'); ?>/images/t-icon.png" alt="" /></a>
					<a href="http://www.youtube.com"><img style="margin-top:8px;" src="<?php bloginfo('template_directory'); ?>/images/y-icon.png" alt="" /></a>
					</div><!-- .col-2 -->
		</div><!-- .warapper-a -->
	</footer><!-- #footer -->
	
		
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/scripts.js?v=1.4"></script>		
</body>
</html>