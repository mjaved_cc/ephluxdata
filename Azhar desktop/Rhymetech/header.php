<!--<!DOCTYPE html>
<html <?php /* language_attributes(); ?>>

  <head>
  <meta charset="<?php  bloginfo('charset'); ?>" />

  <?php if (is_search()) { ?>
  <meta name="robots" content="noindex, nofollow" />
  <?php } ?>

  <title>
  <?php
  if (function_exists('is_tag') && is_tag()) {
  single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
  elseif (is_archive()) {
  wp_title(''); echo ' Archive - '; }
  elseif (is_search()) {
  echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
  elseif (!(is_404()) && (is_single()) || (is_page())) {
  wp_title(''); echo ' - '; }
  elseif (is_404()) {
  echo 'Not Found - '; }
  if (is_home()) {
  bloginfo('name'); echo ' - '; bloginfo('description'); }
  else {
  bloginfo('name'); }
  if ($paged>1) {
  echo ' - page '. $paged; }
  ?>
  </title>

  <link rel="shortcut icon" href="/favicon.ico">

  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

  <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

  <div id="page-wrap">

  <div id="header">
  <h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
  <div class="description"><?php bloginfo('description'); */ ?></div>
                </div> -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Welcome to Rhymetech</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/style.css" />
        
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <script>document.documentElement.className += " js";</script>		
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->	
 <?php wp_head(); ?>
    </head>

    <body class="home">	
        <header id="top" role="banner">		
            <div class="wrapper-a">			

                <h1 id="logo"><a href="index.htm"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="RYHME TECH" /></a></h1>

                <?php wp_nav_menu( array( 'menu' => 'Main Navigation Menu' )); ?>
                
                <div class="social">
                    <img src="<?php bloginfo('template_directory'); ?>/images/mp3.png" alt=""/>
                </div>


                <form action="#" name="search" id="search" method="get">
                    <div class="search01">
                        <div class="clear">
                            <input type="text" class="field" name="search-string" id="search-string"/>
                            <input type="submit" title="Search" alt="Search" class="submit" id="search-submit" value="" />
                        </div>
                    </div>
                </form>


            </div><!-- .wrapper-a -->
        </header><!-- #top -->
      