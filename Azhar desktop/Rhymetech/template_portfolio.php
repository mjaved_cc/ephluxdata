<?php
/*
Template Name: Portfolio
*/
?>


<?php get_header(); ?>

<section id="content" role="main">	
	
	<nav id="portfolio-nav-container" class="box-e">
	<h2 class="header-c">Portfolio</h2>

	<p id="portfolio-back-btn" class="more-d"><a href="portfolio.html" data-id="portfolio-all" class="link-c"><span>Back to projects list</span></a></p>
	
	<ul class="list-b lb-a" id="portfolio-nav">				
		<li class="fastpr"><a class="tooltip-trigger" href="portfolio/fastpr.html" data-id="fastpr">FastPr</a></li>
		<li class="thinkmedia"><a class="tooltip-trigger" href="portfolio/thinkmedia.html" data-id="thinkmedia">Thinkmedia</a></li>
		<li class="arkana"><a class="tooltip-trigger" href="portfolio/arkana.html" data-id="arkana">Arkana</a></li>
		<li class="bioway"><a class="tooltip-trigger" href="portfolio/bioway.html" data-id="bioway">Bioway</a></li>
		<li class="nana"><a class="tooltip-trigger" href="portfolio/nana.html" data-id="nana">Nanaform</a></li>
		<li class="akademia"><a class="tooltip-trigger" href="portfolio/akademia.html" data-id="akademia">Social Media Academy</a></li>
		<li class="ancora"><a class="tooltip-trigger" href="portfolio/ancora.html" data-id="ancora">Ancora</a></li>
		<li class="pixad"><a class="tooltip-trigger" href="portfolio/pixad.html" data-id="pixad">Pixad</a></li>
		<li class="other"><a class="tooltip-trigger" href="portfolio/others.html" data-id="others">Other projects</a></li>
	</ul>			
</nav>		
	<div class="box-c bc-a" id="page-cycle">			
		<section class="box-d portfolio-all" id="portfolio-all" data-href="portfolio.html" data-title="Portfolio - Hitmo - hittin' the web">				
			<div class="wrapper-e">
				<ul id="portfolio-items-list" class="list-i" data-state="implode">
					<li>
						<a href="portfolio/fastpr.html" data-id="fastpr">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/fastpr_thumb_1.png" width="180" height="186" alt=""/></p>							
							</div>
							<div class="l-content">
								<h2>Fast PR</h2>
								<p>The fastest PR on the web.</p>
							</div>
						</a>							
					</li>	
					<li>							
						<a href="portfolio/thinkmedia.html" data-id="thinkmedia">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/thinkmedia_thumb_1.png" width="180" height="186" alt=""/></p>									
							</div>
							<div class="l-content">
								<h2>Thinkmedia</h2>
								<p>Not only for geeks.</p>
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/arkana.html" data-id="arkana">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/arkana_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Arkana</h2>
								<p>Law for Business.</p>									
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/bioway.html" data-id="bioway">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/bioway_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Bioway</h2>
								<p>Tasty way, healthy way.</p>									
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/nana.html" data-id="nana">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/nana_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>NanaForm</h2>
								<p>Interior and form.</p>									
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/akademia.html" data-id="akademia">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/akademia_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Social Media Academy</h2>
								<p>Social Media at one's fingertips.</p>									
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/ancora.html" data-id="ancora">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/ancora_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Ancora</h2>
								<p>Successful debt collection.</p>
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/pixad.html" data-id="pixad">
							<div class="image-container">
								<p class="more-c">See details</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/pixad_thumb_1.png" width="180" height="186" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Pixad</h2>
								<p>In-Image Advertising.</p>									
							</div>
						</a>
					</li>
					<li>							
						<a href="portfolio/others.html" class="misc" data-id="others">
							<div class="image-container">
								<p class="more-c">See projects</p>
								<p class="image"><img src="<?php bloginfo('template_directory'); ?>/images/project-thumbs/other_thumb_1.png" width="175" height="156" alt=""/></p>
							</div>
							<div class="l-content">
								<h2>Various</h2>
								<p>Other projects.</p>									
							</div>
						</a>
					</li>
				</ul>						
			</div><!-- .wrapper-d -->
		</section>			
	</div><!-- .box-c -->			
</section><!-- #content -->

<?php get_footer(); ?>