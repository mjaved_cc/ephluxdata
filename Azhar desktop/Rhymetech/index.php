
<?php
/*
  Template Name: Main_template
 */
?>

<?php global $post;
setup_postdata($post); ?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <!-- <div/>  <?php /* post_class() ?> id="post-<?php the_ID(); ?>">

          <h2><?php the_title(); ?></h2>

          <?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

          <div class="entry">

          <?php the_content(); ?>

          <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

          <?php the_tags( 'Tags: ', ', ', ''); ?>

          </div>

          <?php edit_post_link('Edit this entry','','.'); ?>

          </div>

          <?php comments_template(); */ ?> -->

    <?php endwhile;
endif; ?>


<?php $page = get_page_by_title('HOME', 'ARRAY_A', 'page'); ?>


<div class="b">
    <ul id="menu2">
       <li><a href="#" class="active">Company</a></li>						
        <li><a href="#">Vision</a></li>
        <li><a href="#">Mission</a></li>
    </ul> 
</div> 
          
          
<section id="content" role="main">	
    <nav class="cycle-nav-container" role="navigation">			
        <h2 class="offset">Navigation between portfolio projects</h2>
        <ul class="list-b" id="cycle-1-nav">
            <li class="rhymtech2"><a href="#rhymtech2">1</a></li>
            <li class="rhymtech"><a href="#rhymtech">2</a></li>
            <li class="fastpr"><a href="#bioway">3</a></li>
            <li class="bioway"><a href="#nana">4</a></li>
            <li class="nana"><a href="#rhymtech3">5</a></li>



        </ul>
    </nav><!-- .cycle-nav-container -->

    <section class="box-c cycle-a">


        <article class="cycle-item rhymtech2" id="rhymtech2" data-thumbnail="rhymtech_thumb.png">
            <div class="noizzz">
                <div class="wrapper-a">
                    <div class="content">														
                        <h1 class="header-a">RhymePad &reg;</h1>
                        <p>lets you write your lyrics on-the-move,<br /> and contains features that help you<br />when recording song lyrics in the studio.</p>
                        <p class="more-a"><a href="#" class="link-a">Download for iPhone now</a></p>
                        <br /><br /><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/appstore.png" width="311" height="95" alt=""/></a>
                    </div><!-- .content -->	
                    <div class="vis-box">
                        <p class="image-a"><img src="<?php bloginfo('template_directory'); ?>/images/project-visuals/slide3.png" width="526" height="488" alt=""/></p>							
                    </div>
                </div><!-- .wrapper-a -->
            </div><!-- .noizzz -->
        </article>

        <article class="cycle-item rhymtech" id="rhymtech" data-thumbnail="rhymtech_thumb.png">
            <div class="noizzz">
                <div class="wrapper-a">	
                    <div class="content">														
                        <h1 class="header-a">RhymePad &reg;</h1>
                        <p> First app ever for recording artists; Insert Adlibs,Stabs and FX</p>
                        <p class="more-a"><a href="#" class="link-a">Read more</a></p>
                        <br /><br /><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/appstore.png" width="311" height="95" alt=""/></a>
                    </div><!-- .content -->
                    <div class="vis-box">
                        <p class="image-a"><img src="<?php bloginfo('template_directory'); ?>/images/project-visuals/vis-rhymtech.png" width="526" height="488" alt=""/></p>							
                    </div>
                </div><!-- .wrapper-a -->
            </div><!-- .noizzz -->
        </article>


        <article class="cycle-item bioway" id="bioway" data-thumbnail="fastpr_thumb.png">
            <div class="noizzz">
                <div class="wrapper-a">	
                    <div class="content">														
                        <h1 class="header-a">RhymePad &reg;</h1>
                        <p>Writing Lyrics On The Move <br />Watch the demo video here</p>
                        <p class="more-a"><a href="#" class="link-a">See live demo</a></p>
                    </div><!-- .content -->
                    <div class="vis-box">
                        <p class="image-a">
                            <img src="<?php bloginfo('template_directory'); ?>/images/project-visuals/video.png" style="margin-top:80px;" width="526" height="321" alt=""/>

                        </p>							
                    </div>
                </div><!-- .wrapper-a -->
            </div><!-- .noizzz -->
        </article>
        <article class="cycle-item nana" id="nana" data-thumbnail="rhymtech_thumb.png">
            <div class="noizzz">
                <div class="wrapper-a">	
                    <div class="content">														
                        <h1 class="header-a">Convenience - Customisation</h1>
                        <p>Easy to write lyrics â€“ 1 lyric per line/box Long Press - move lyrics between songs with one finger!</p>
                        <p class="more-a"><a href="#" class="link-a">Download for iPhone now</a></p>
                        <br /><br /><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/appstore.png" width="311" height="95" alt=""/></a>
                    </div><!-- .content -->
                    <div class="vis-box">
                        <p class="image-a"><img src="<?php bloginfo('template_directory'); ?>/images/project-visuals/vis-rhymtech.png" width="526" height="488" alt=""/></p>							
                    </div>
                </div><!-- .wrapper-a -->
            </div><!-- .noizzz -->
        </article>
        <article class="cycle-item rhymtech3" id="rhymtech3" data-thumbnail="rhymtech_thumb.png">
            <div class="noizzz">
                <div class="wrapper-a">	
                    <div class="content">														
                        <h1 class="header-a">RhymePad &reg;</h1>
                        <p>Want to record a track in a professional<br /> studio? Enter our Free Studio Time <br />Competition Now.</p>
                        <p class="more-a"><a href="#" class="link-a">Read more</a></p>
                        <br /><br /><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/appstore.png" width="311" height="95" alt=""/></a>
                    </div><!-- .content -->
                    <div class="vis-box">
                        <p class="image-a"><img src="<?php bloginfo('template_directory'); ?>/images/project-visuals/vis-rhymtech.png" width="526" height="488" alt=""/></p>							
                    </div>
                </div><!-- .wrapper-a -->
            </div><!-- .noizzz -->
        </article>			


    </section><!-- .cycle-a -->	
    <section class="box-a" role="complementary">
        <div class="wrapper-b">
        
        
        		
            <ul class="list-c" id="navBoxes" role="navigation">
                <li class="what-we-do">
                    <a href="#">
                        <h2>Competition</h2>
                        <?php echo get_post_meta($page['ID'], 'Competition', true); ?>

<?php /* the_widget('WP_Widget_Text', 'Competition', array( 'before_title' => '<h2 class="widget-title">', 'after_title' => '</h2>' )); */ ?> 

                    </a>
                </li>
                <li class="team">
                    <a href="#">
                        <h2>Coming Soon</h2>
<?php echo get_post_meta($page['ID'], 'Coming Soon', true); ?>

                    </a>						
                </li>
                <li class="faq">
                    <a href="#">
                        <h2>Videos</h2>

<?php echo get_post_meta($page['ID'], 'Videos', true); ?>

                    </a>
                </li>

            </ul>
        </div><!-- .wrapper-b -->
        <div class="wrapper-a">


            <h1>Welcome to RHYME TECH </h1>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <p style="text-align:justify"><?php the_content(); ?></p>

    <?php endwhile;
endif; ?>
        </div>


        </div>
        <div class="cols-two-a wrapper-d">
            <section class="primary-ta">
                <div class="hbox-a">
                    <h1 class="header-b">News</h1>
                    <div class="hbox-inner">
                   <!--     <h3>RHYMETECH News</h3> -->
                       <!-- <p><?php /* if ( function_exists('insert_newsticker') ) { insert_newsticker();  } */ ?></p>
                       <p>Find out about the latest developments at Rhyme Tech. Keep up to date with press coverage and our blog.</p> -->
                        
                        <?php  if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>

						<?php
						the_widget( 'Twenty_Eleven_Ephemera_Widget', '', array( 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
						?>

					<?php endif; // end sidebar widget area  ?>
                        
                        <a href="#" style="font-size:12px; padding:0 10px;">Readmore</a>
                    </div>
                </div>

            </section>
            <section class="primary-ta">
               <!-- <div class="hbox-a">
                    <h1 class="header-b">Download Our App</h1>
                    <div class="hbox-inner">
                        <p>Know which app you like? Download it now!! Lite versions or Premium versions, your choice.</p>
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/appstore.png" style="margin-left:35px;" width="200"  alt=""/></a>
                    </div>
                </div> -->
               
               
               <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

						<?php
						the_widget( 'Twenty_Eleven_Ephemera_Widget', '', array( 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
						?>

					<?php endif; // end sidebar widget area ?>

            </section>
            <section class="secondary-ta">
                <div class="hbox-a">
                    <h1 class="header-b hb-a">Testimonials 
                        <ul class="list-e" id="news-cycle-nav" role="navigation">
                            <li><a href="#" class="active">1</a></li>
                            <li><a href="#">2</a></li>							
                        </ul>
                    </h1>

                </div>
                <section class="box-b cycle-d" id="news-cycle">							
                    <article class="art-a" id="news1">
                        <div class="wrapper-c">

                            <div class="content">								
                              <!--  <h1>Help us to improve</h1> -->
                                <p>
                                  <!--  Help shape the next app. What's the best feature, what feature would you want to change? We're all ears. -->
                                 <?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?>

						<?php
						the_widget( 'Twenty_Eleven_Ephemera_Widget', '', array( 'before_title' => '<h1 class="widget-title">', 'after_title' => '</h1>' ) );
						?>

					<?php endif; // end sidebar widget area ?>

	        
                                </p>
                            </div><!-- .content -->
                        </div><!-- .wrapper-c -->
                    </article>
                    <article class="art-a" id="news2">							
                        <div class="wrapper-c">								

                            <div class="content">								
                                <h1>Help us to improve Rhymepad</h1>
                                <p>
                                    We understand the music and recording process so this gives the company an unparalleled understanding
                                </p>
                            </div><!-- .content -->
                        </div><!-- .wrapper-c -->
                    </article>
                </section><!-- #cycle-4 -->
            </section>
        </div><!-- .cols-two-a -->			
    </section><!-- .box-a -->
</section><!-- #content -->	


<?php get_footer(); ?>