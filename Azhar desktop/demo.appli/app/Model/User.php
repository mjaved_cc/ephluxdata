<?php

App::uses('AppModel', 'Model');

/**

 * User Model

 *

 */
class User extends AppModel {

	/**

	 * Display field

	 *

	 * @var string

	 */
	public $displayField = 'username';
	var $name = 'User';
//	var $belongsTo = array(
//		'Group'
//	);
//	public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

	var $hasMany = array(
		'UserSocialAccount'
	);
	var $validate = array(
		'password' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => array('minlength', 8),
				'message' => 'Password must be between 8 to 15 characters'
			)
		),
		'email' => array(
			'rule1' => array(
				'rule' => 'notEmpty',
				'message' => 'This is required field',
				'last' => false
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please supply a valid email address.',
				'last' => false
			),
			'rule3' => array(
				'rule' => 'isUnique',
				'message' => 'Email already exists'
			)
		)
	);

	/**

	 * authorizes user credentials

	 * @param varchar $email email address

	 * @param varchar $password password

	 */
	function verify_login($email, $password, $gcm_regid) {



		$result = $this->query('call verify_login("' . $email . '", "' . $password . '", "' . $gcm_regid . '")');



		if (!empty($result))
			return $result['0'];



		return false;
	}

	function get_details_by_id($id) {



		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');



		$user = $this->query('call get_user_details_by_user_id("' . $id . '")');


		$data = array();
		$user_consoles = '';
		$user_genres = '';



		if (!empty($user)) {



			foreach ($user as $key => $value) {



				$obj_user = new DtUser($value['User']);



				$data[$key]['User'] = $obj_user->get_field();
			}

			$user_consoles = $this->query("call get_user_all_consoles('" . $data[0]['User']['id'] . "')");

			$user_genres = $this->query("call get_user_all_genres('" . $data[0]['User']['id'] . "')");
		}


		if (!empty($user_consoles)) {



			foreach ($user_consoles as $key => $value) {



				$data[0]['UserConsole'][$key]['user_id'] = $value['users_consoles']['user_id'];

				$data[0]['UserConsole'][$key]['console_id'] = $value['consoles']['id'];

				$data[0]['UserConsole'][$key]['console'] = $value['consoles']['console'];
			}
		} else {



			$data[0]['UserConsole'] = array();
		}



		if (!empty($user_genres)) {



			foreach ($user_genres as $key => $value) {



				$data[0]['UserGenre'][$key]['user_id'] = $value['users_genres']['user_id'];

				$data[0]['UserGenre'][$key]['genre_id'] = $value['genres']['id'];

				$data[0]['UserGenre'][$key]['genre'] = $value['genres']['genre'];
			}
		} else {



			$data[0]['UserGenre'] = array();
		}



		return $data;
	}

	function save_image($user_id, $image_path) {

		$this->query("call user_save_profile_image_path('" . $user_id . "', '" . $image_path . "')");
	}

	function is_unverified_email_exists($email) {



		$is_unverified_exist = $this->query('call is_unverified_email_exists("' . $email . '")');



		return $is_unverified_exist;
	}

	function save_record($email, $nickname, $password, $dob, $gcm_regid, $created) {

		$this->query('call save_record("' . $email . '", "' . $nickname . '", "' . $password . '", "' . $dob . '" ,"' . $gcm_regid . '","' . $created . '", @status)');

		return $this->get_status();
	}

	function verify_account($email) {

		$this->query('call verify_account("' . $email . '", @status)');

		return $this->get_status();
	}

	function save_user_social_data($data) {

		$this->query("call user_signup_social(

			   '" . $data['link_id'] . "',

			   '" . $data['email_address'] . "',

			   '" . $data['image_url'] . "',

			   '" . $data['type_id'] . "',

			   '" . $data['access_token'] . "',

			   '" . $data['access_token_secret'] . "',

			   '" . $data['is_valid_token'] . "',

			   '" . $data['screen_name'] . "',

			   '" . $data['status'] . "',

			   '" . $data['ip_address'] . "',

				   @status 

				 )");

		$user_id = $this->get_status();

		$data = $this->get_details_by_id($user_id);

		return $data;
	}

	function save_profile_info($data) {



		$this->query("call save_user_profile_info(

			   '" . $data['gaming_id'] . "',

			   '" . $data['shipping_address'] . "',

			   '" . $data['location'] . "',

			   '" . $data['paypal_id'] . "',

			   '" . $data['contact'] . "',

			   '" . $data['profile_text'] . "',

			   '" . $data['user_id'] . "',

			   '" . $data['status'] . "'

				 )");



		return 'success';
	}

	function get_profile_info($user_id) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$user = $this->query("call get_user_profile_info('" . $user_id . "')"); //pr($user); exit;

		$user_consoles = $this->query("call get_user_all_consoles('" . $user_id . "')");

		$user_genres = $this->query("call get_user_all_genres('" . $user_id . "')");  //pr($user_genres); 		exit;



		$data = array();



		if (!empty($user)) {



			foreach ($user as $key => $value) {



				$obj_user = new DtUser($value['users']);



				$data[$key]['User'] = $obj_user->get_field();
			}
		}



		if (!empty($user_consoles)) {



			foreach ($user_consoles as $key => $value) {



				$data[0]['UserConsole'][$key]['user_id'] = $value['users_consoles']['user_id'];

				$data[0]['UserConsole'][$key]['console_id'] = $value['consoles']['id'];

				$data[0]['UserConsole'][$key]['console'] = $value['consoles']['console'];
			}
		}



		if (!empty($user_genres)) {



			foreach ($user_genres as $key => $value) {



				$data[0]['UserGenre'][$key]['user_id'] = $value['users_genres']['user_id'];

				$data[0]['UserGenre'][$key]['genre_id'] = $value['genres']['id'];

				$data[0]['UserGenre'][$key]['genre'] = $value['genres']['genre'];
			}
		}



		return $data;
	}

	function get_user_data_by_id($user_id) {

		App::uses('DtUser', 'Lib/DataTypes');

		$user = $this->query('call get_user_by_id("' . $user_id . '", @status)');
		$game_count = $this->get_status();

		$data = array();

		if (!empty($user)) {

			$obj_game = new DtGame($value['games']);
			$data['games'] = $obj_game->get_field();
		}
	}

	function gcm_regid_update($user_id, $gcm_regid) {

		$this->query('call update_gcm_regid("' . $user_id . '", "' . $gcm_regid . '")');

		return 'success';
	}

	function get_all() {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query('call get_all_users(NULL)');

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data[$key]['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function details($id) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query('call get_all_users("' . $id . '")');

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function update_user_status($id, $status) {

		$this->query("call update_user_status('" . $status . "', '" . $id . "')");
	}

	function make_user_trusted($user_id, $transaction_id) {

		$this->query("call mark_user_trusted('" . $user_id . "', '" . $transaction_id . "')");

		return 'success';
	}

	function search($keyword) {

		App::uses('DtUser', 'Lib/DataTypes');
		App::uses('DtDate', 'Lib/DataTypes');

		$users = $this->query("call search_user_by_nickname_or_email('" . $keyword . "')");

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$obj_user = new DtUser($value['users']);

				$data[$key]['User'] = $obj_user->get_field();
			}
		}

		return $data;
	}

	function selling_particular_game($game_id, $start = null, $limit = null) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$users = $this->query("call users_selling_specific_game('" . $game_id . "','" . $start . "', '" . $limit . "')"); //pr($users); exit;

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {
				
				$image_url = '';

				if (substr_count($value['users']['image_url'], 'http') == 0) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$key]['User']['user_id'] = $value['users_games']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url ;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_games']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		return $data;
	}

	function buying_particular_game($game_id, $start = null, $limit = null) {

		if ($start == null) {
			$start = 0;
		}

		if ($limit == null) {
			$limit = 100;
		}

		$users = $this->query("call users_buying_specific_game('" . $game_id . "','" . $start . "', '" . $limit . "')"); //pr($users); exit;

		$data = array();

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$image_url = '';

				if (substr_count($value['users']['image_url'], 'http') == 0) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$key]['User']['user_id'] = $value['users_wishlists']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_wishlists']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		return $data;
	}

	function trading_particular_game($game_id) {

		$users = $this->query("call users_trading_specific_game('" . $game_id . "', 1)"); //pr($users); exit;

		$data = array();
		$count = 0;

		if (!empty($users)) {

			foreach ($users as $key => $value) {

				$image_url = '';

				if (substr_count($value['users']['image_url'], 'http') == 0) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {
					$image_url = $value['users']['image_url'];
				}

				$data[$key]['User']['user_id'] = $value['users_games']['user_id'];
				$data[$key]['User']['nickname'] = $value['users']['nickname'];
				$data[$key]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$key]['User']['profile_image_url'] = $image_url;
				$data[$key]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$key]['User']['created'] = $value['users']['created'];
				$data[$key]['User']['trade'] = $value['users_games']['trade'];
				$data[$key]['User']['selling'] = $value['games']['selling'];
				$data[$key]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$key]['User']['GamesOwned'] = '';

				$games_owned = $this->query("call count_user_have_games('" . $data[$key]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$key]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}
			}
		}

		if (!empty($data)) {

			$count = count($data);
		}

		$user = $this->query("call users_trading_specific_game('" . $game_id . "', 2)"); //pr($user); exit;

		if (!empty($user)) {

			foreach ($user as $key => $value) {

				$image_url = '';

				if (substr_count($value['users']['image_url'], 'http') == 0) {

					$image_url = SITE_URI . $value['users']['image_url'];
				} else {

					$image_url = $value['users']['image_url'];
				}

				$data[$count]['User']['user_id'] = $value['users_wishlists']['user_id'];
				$data[$count]['User']['nickname'] = $value['users']['nickname'];
				$data[$count]['User']['gaming_id'] = $value['users']['gaming_id'];
				$data[$count]['User']['profile_image_url'] = $image_url;
				$data[$count]['User']['trusted_user'] = $value['users']['trusted_user'];
				$data[$count]['User']['created'] = $value['users']['created'];
				$data[$count]['User']['trade'] = $value['users_wishlists']['trade'];
				$data[$count]['User']['selling'] = $value['games']['selling'];
				$data[$count]['User']['trading'] = $value['games']['trading'];
				$data[$key]['User']['buying'] = $value['games']['buying'];
				$data[$count]['User']['GamesOwned'] = '';

				$games_owned = $this->query("call count_user_have_games('" . $data[$count]['User']['user_id'] . "')");

				if (!empty($games_owned[0][0]['GamesOwned'])) {

					$data[$count]['User']['GamesOwned'] = $games_owned[0][0]['GamesOwned'];
				}

				$count++;
			}
		}

		return $data;
	}

	function games_and_deals_count($user_id) {

		$data = array();
		$image_url = '';

		$count = $this->query('call count_games_and_deals("' . $user_id . '")');  //pr($count); exit;

		if (substr_count($count[0]['users']['image_url'], 'http') == 0) {

			$image_url = SITE_URI . $count[0]['users']['image_url'];
		} else {
			$image_url = $count[0]['users']['image_url'];
		}

		if (!empty($count[0][0])) {

			$data['GamesCount'] = array(
				'MyGames' => $count[0][0]['mygames'],
				'WishGames' => $count[0][0]['mywish'],
				'Deals' => $count[0][0]['mydeal'],
				'profile_image_url' => $image_url
			);
		}

		return $data;
	}

	function save_paypal_data($data) {

		$this->query('call save_payapl_return_data("' . $data->body->user_id . '", "' . $data->body->deal_id . '", "' . $data->body->txn_id . '")');


		return 'success';
	}

	function reset_password($password, $user_id) {



		$this->query('call reset_password(

			"' . $password . '",

			"' . $user_id . '",

			@status )');



		return $this->get_status();
	}

	/**

	 * Get details of users by email

	 * @param array $query_params WHERE clause

	 * @return array result

	 */
	function get_by_email($email) {



		$result = $this->query('call get_user_by_email("' . $email . '")');



		if (!empty($result))
			return $result['0'];



		return false;
	}

	/**

	 * Create new user for given params

	 * 

	 * @param array $user_details User details

	 * @return type Description

	 */
	function create_user($user_details) {

		$this->query('call create_user(

			"' . $user_details['first_name'] . '", 

			"' . $user_details['last_name'] . '", 

			"' . $user_details['username'] . '", 

			"' . $user_details['password'] . '",

			"' . $user_details['group_id'] . '",

			"' . $user_details['created'] . '",

			"' . $user_details['ip_address'] . '",

			@status )');



		return $this->get_status();
	}

	public function beforeSave($options = array()) {

		if (!empty($this->data['User']['password']))
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);

		return true;
	}

	/**

	 * Signup facebook. Save details in User & UserSocialAcount tables.

	 * 

	 * @param xml $xml_data user details

	 */
	function signup_with_social_account($xml_data) {



		$this->query("call signup_with_social_account(

			 '" . $xml_data . "',

			@status )");



		return $this->get_status();
	}

}

