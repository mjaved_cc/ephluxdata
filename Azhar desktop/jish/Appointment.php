<?php

class Appointment extends AppModel {

	/**
	 * Check if appointments exists for given doctor for given interval
	 * 
	 * @param string $start_interval start time
	 * @param string $end_interval end time
	 * @param string $employee_detail_id employee detail id
	 * @return boolean
	 */
	function is_doctor_appointments_exists_between_intervals($start_interval, $end_interval, $employee_detail_id) {
		$result = $this->query('call get_doctor_appointments_count_between_intervals(
			"' . $start_interval . '",
			"' . $end_interval . '",
			"' . $employee_detail_id . '"
				)');

		return ($result[0][0]['AppointmentCount']) ? true : false;
	}

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'CallLog' => array(
			'className' => 'CallLog',
			'foreignKey' => 'appointment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AppointmentGroup' => array(
			'className' => 'AppointmentGroup',
			'foreignKey' => 'appointment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'EmployeeDetail' => array(
			'className' => 'EmployeeDetail',
			'foreignKey' => 'employee_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Division' => array(
			'className' => 'Division',
			'foreignKey' => 'division_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PatientDetail' => array(
			'className' => 'PatientDetail',
			'foreignKey' => 'patient_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function update_appointment_status($data) {
		$this->query('call update_appointment_status(
			"' . $data['appointment_id'] . '",
			"' . $data['stat'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}

	function update_appointment_reschedule($data) {

		$this->query('call update_appointment_reschedule(
			"' . $data['appointment_id'] . '",
			"' . $data['stat'] . '",
			"' . $data['appoitnment_date'] . '",
			"' . $data['modified'] . '",
				@status)');
		return $this->get_status();
	}

	function get_appointment_for_dashboard( $data ) {

		return $this->query('call get_appointment_for_dashboard(
			"' . $data['user_type'] . '",
			"' . $data['user_id'] . '")'
		);
	}

}

?>
