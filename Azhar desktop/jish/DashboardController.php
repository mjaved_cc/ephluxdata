<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class DashboardController extends AppController {

	public $name = 'Dashboard';
	public $uses = array('PatientDetail', 'User', 'Division', 'Appointment');

	function beforeFilter() {
		parent::beforeFilter();
		//$this->layout = 'admin';
	}

	function beforeRender() {
		$this->set('page_heading', 'Dashboard');
		$this->JCManager->add_js(array('event', MRIYA_JS_PATH . 'jquery.jcarousel.min'));
		parent::beforeRender();
	}

	/**
	 * index method
	 *
	 * @return void
	 * @link http://stackoverflow.com/questions/9488153/fullcalendar-how-do-i-allow-users-to-edit-delete-events-and-remove-them-from-th
	 */
	public function index() {
		$this->paginate = array(
			'limit' => 5,
			'order' => array(
				'PatientDetail.created' => 'desc'
			),
			'conditions' => array('User.status' => STATUS_ACTIVE, 'User.user_type' => USER_TYPE_PATIENT, 'PatientDetail.status' => STATUS_ACTIVE)
		);

		$data = $this->paginate();

		$patientDetails = array();
		foreach ($data as $key => $PatientDetail) {
			$obj_user = new DtUser($PatientDetail['User']);
			$obj_user->add_patient_detail($PatientDetail[$this->modelClass]);

			/* Retrieve data from rendering logic */

			$patientDetails[$key]['User'] = $obj_user->get_field();
			if ($obj_user->PatientDetail instanceof DtPatientDetail) {
				$patientDetails[$key][$this->modelClass] = $obj_user->PatientDetail->get_field();
			}
		}
		$this->set(compact('patientDetails'));
	}

	function temp() {
		$this->autoRender = false;
		$data = array();
		foreach ($results as $key => $value) {
			$data[$key]['id'] = round(rand(0, 9));
			$data[$key]['title'] = 'losem';
			$data[$key]['start'] = date('Y-m-d H:i'); // yyyy-MM-dd HH:mm			
			$data[$key]['division_id'] = 1;
		}

		echo $this->App->encoding_json($data);
	}

	function update() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$event = $this->App->decoding_json($this->request->data['event']);
			$result = $this->Garbage->findById($event['id']);

			$result['Garbage']['id'] = $event['id'];
			$result['Garbage']['title'] = $event['title'];
			$result['Garbage']['start'] = $event['start'];
			$result['Garbage']['end'] = $event['end'];
			$result['Garbage']['division_id'] = $event['division_id'];

			$this->Garbage->save($result);
		}
	}

	function create() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$event = $this->App->decoding_json($this->request->data['event']);

			$result['Garbage']['title'] = $event['title'];
			$result['Garbage']['start'] = $event['start'];
			$result['Garbage']['end'] = $event['end'];
			$result['Garbage']['division_id'] = $event['division_id'];
			$this->Garbage->create();
			$this->Garbage->save($result);
		}
	}

	function test() {

		$user = $this->Auth->user();

		$param['user_type'] = 'doctor';
		$param['user_id'] = '42';


		$appointment = $this->Appointment->get_appointment_for_dashboard($param);

		foreach ($appointment as $app) {

			$obj_appointment = new DtAppointment($app['Appointment']);
			$obj_appointment->add_employee_detail($app['EmpDetail']);
			$obj_appointment->add_patient_detail($app['PatDetail']);
		}

		pr($appointment);
		exit;
	}

}

