<?php $appoint = json_decode($response); 
      $mess = json_decode($messages);
	  //pr($mess); exit;
?>


<h1 class="border_bottom_1"><?php echo __($page_heading) ?></h1>
<section class="grid_6">
	<!-- begin box -->
	<div class="box">
		<!-- Box Header -->
		<div class="header clearfix">
			<h2><?php echo __('New Added Patients') ?></h2>
            <button class="button dark" data-target="register" onclick="App.redirect({
				controller : 'PatientDetails',
				action : 'index'
			})"><?php echo __('View All') ?></button>
		</div>
		<!-- End Header -->

		<div class="body">
			<?php
			foreach ($patientDetails as $patientDetail) {
				?>
				<p><?php echo $this->Html->link($patientDetail['PatientDetail']['full_name'], array('controller' => 'PatientDetails', 'action' => 'view', $patientDetail['PatientDetail']['id'], 'full_base' => true)); ?></p>
				<?php
			}
			?>
		</div>
	</div>



	<table class="datagrid wf">
		<div class="header clearfix">
			<h2><?php echo __('Today Appointments') ?></h2>
		</div>
		<thead>
			<tr>
				<th><?php echo __('Patient Name'); ?></th>
				<th><?php echo __('Employee Name'); ?></th>
				<th><?php echo __('Appointment Date'); ?></th>
				<th><?php echo __('Status'); ?></th>
				<th><?php echo __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (!empty($appoint->body)) {
				foreach ($appoint->body as $app):
					?>
					<tr>
						<td align="center"><?php echo $app->PatDetail->first_name; ?>&nbsp;</td>
						<td align="center"><?php echo $app->EmpDetail->first_name; ?>&nbsp;</td>
						<td align="center"><?php echo date('d/m/Y H:i', $app->Appointment->appointment_date); ?>&nbsp;</td>
						<td align="center"><?php echo $app->Appointment->status;  ?>&nbsp;</td>
						<td class="actions" align="center">		
                                	<?php echo $this->Html->link(__('Detail'), array('controller' => 'appointments','action' => 'view',$app->Appointment->id)); ?> 					
                                </td>
					</tr>
				<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No appointments found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	
	
	<table class="datagrid wf">
		<div class="header clearfix">
			<h2><?php echo __('Today Messages') ?></h2>
		</div>
		<thead>
			<tr>
				<th><?php echo __('User'); ?></th>
				<th><?php echo __('Title'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th><?php echo __('Last Message'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (!empty($mess->body)) { 
				foreach ($mess->body as $messs): 
					?>
					<tr>
						<td align="center"><?php echo $messs->Conversation->title; ?>&nbsp;</td>
						<td align="center"><?php echo $messs->Conversation->title; ?>&nbsp;</td>
						<td align="center"><?php echo $messs->ConversationMessage->created ?>&nbsp;</td>
						<td align="center"><?php echo $messs->ConversationMessage->message;  ?>&nbsp;</td>
						<td class="actions" align="center">		
                                	<?php echo $this->Html->link(__('Detail'), array('controller' => 'conversations','action' => 'index')); ?> 					
                                </td>
					</tr>
				<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No Messages found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	

</section>