<?php
$appoint = json_decode($appointments);
$mess = json_decode($messages);
$patient_detail = json_decode($patients_detail);
?>
<section class="grid_6">

	<table class="datagrid wfdashboard">
		<div class="header clearfix">
			<h2><?php echo __('Today Messages') ?></h2>
			<?php echo $this->Html->link(__('View All'), array('controller' => 'conversations', 'action' => 'index', 'plugin' => true)); ?> 
		</div>
		<thead>
			<tr>

				<th><?php echo __('Title'); ?></th>
				<th><?php echo __('Created'); ?></th>


			</tr>
		</thead>
		<tbody>
			<?php
			if (!empty($mess->body)) {
				foreach ($mess->body as $messs):
					?>
					<tr>
						<td align="center"><?php echo $messs->Conversation->title; ?>&nbsp;</td>					
						<td align="center"><?php echo $messs->ConversationMessage->created ?>&nbsp;</td>					
						<td class="actions" align="center">		
							<?php echo $this->Html->link(__('Detail'), array('controller' => 'conversations', 'action' => 'view', $messs->Conversation->id, 'plugin' => true)); ?> 					
						</td>
					</tr>
					<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No Messages found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>

	<table class="datagrid wfdashboard">
		<div class="header clearfix">
			<h2><?php echo __('Today Appointments') ?></h2>
			<?php echo $this->Html->link(__('View All'), array('controller' => 'appointments', 'action' => 'get_all_future_appointments')); ?> 
		</div>
		<thead>
			<tr>
				<th><?php echo __('Patient Name'); ?></th>
				<th><?php echo __('Appointment Time'); ?></th>
				<th><?php echo __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			if (!empty($appoint->body)) {
				foreach ($appoint->body as $app):
					?>
					<tr>
						<td align="center"><?php echo $app->PatDetail->first_name; ?>&nbsp;</td>
						<td align="center"><?php echo date('H:i', $app->Appointment->appointment_date); ?>&nbsp;</td>
						<td class="actions" align="center">		
							<?php echo $this->Html->link(__('Detail'), array('controller' => 'appointments', 'action' => 'view', $app->Appointment->id)); ?> 					
						</td>
					</tr>
					<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No appointments found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	
	<table class="display dataTable">

		<div class="header clearfix">
			<h2><?php echo __('Today Appointments') ?></h2>
			<?php echo $this->Html->link(__('View All'), array('controller' => 'appointments', 'action' => 'get_all_future_appointments')); ?> 
		</div>
		<thead>
			<tr>
				<th><?php echo __('Patient Name'); ?></th>
				<th><?php echo __('Appointment Time'); ?></th>
				<th><?php echo __('Actions'); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			if (!empty($appoint->body)) {
				foreach ($appoint->body as $app):
					?>
					<tr>
						<td align="center"><?php echo $app->PatDetail->first_name; ?>&nbsp;</td>
						<td align="center"><?php echo date('H:i', $app->Appointment->appointment_date); ?>&nbsp;</td>
						<td class="actions" align="center">		
							<?php echo $this->Html->link(__('Detail'), array('controller' => 'appointments', 'action' => 'view', $app->Appointment->id)); ?> 					
						</td>
					</tr>
					<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No appointments found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>




	<table class="display dataTable">

		<div class="header clearfix">
			<h2><?php echo __('Patients') ?></h2>
		</div>
		<thead>
			<tr>

				<th><?php echo __('Patient ID'); ?></th>
				<th><?php echo __('First Name'); ?></th>
				<th><?php echo __('Last Name'); ?></th>
				<th><?php echo __('DOB'); ?></th>
				<th><?php echo __('Mobile'); ?></th>
				<th><?php echo __('Prefered Contact'); ?></th>
				<th><?php echo __('City'); ?></th>


			</tr>
		</thead>

		<tbody>
			<?php
			if (!empty($patient_detail->body)) {
				foreach ($patient_detail->body as $patient):
					?>
					<tr>
						<td align="center"><?php echo $patient->PatientDetail->user_id; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->first_name; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->last_name; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->dob; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->mobile; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->prefered_contact; ?>&nbsp;</td>					
						<td align="center"><?php echo $patient->PatientDetail->city; ?>&nbsp;</td>					

					</tr>
					<?php
				endforeach;
			}
			else {
				?>
				<tr>
					<td colspan="9">No Patients found.</td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>



</section>
